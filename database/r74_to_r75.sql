-------------------------------------------------------------------------------
-- 
-- correct column on Accounts
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (75);

alter table accounts drop column billingaddresssameascompany;
alter table accounts drop column billingcontactsameascompany;
alter table accounts add column billingcontactsameaspersonal int not null default 0;
alter table accounts add column billingaddresssameascompany int not null default 0;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;