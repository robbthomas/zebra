
-- fn_appuser_login.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
---------------------------------------------------------------------

-- Function: fn_appuser_resetPassword(text,text,text,text,text)

CREATE OR REPLACE FUNCTION fn_get_credentials_id(in_credentials_email text, in_credentials_password text)
  RETURNS text AS
$BODY$
  DECLARE
    var_credentialsId text;
    var_loginEmail text;
    var_adminEmail text;
  BEGIN

    var_credentialsId := id FROM App_User WHERE (in_credentials_email = email OR in_credentials_email = displayName)
            AND passwordHash = encode(digest(in_credentials_password || COALESCE(passwordSalt,''),'sha1'),'hex');

    IF var_credentialsId IS NOT NULL THEN
      RETURN var_credentialsId;
    END IF;

    var_adminEmail := split_part(in_credentials_email, '@@@', 1);
    var_loginEmail := split_part(in_credentials_email, '@@@', 2);


    IF var_adminEmail IS NULL OR var_adminEmail = '' THEN
      RETURN NULL;
    END IF;

    var_credentialsId := loginUser.id FROM App_User loginUser, App_User adminUser, SystemAdministrators sa
            WHERE (var_loginEmail = loginUser.email OR var_loginEmail = loginUser.displayName)
              AND (var_adminEmail = adminUser.email OR var_adminEmail = adminUser.displayName)
              AND (sa.memberid = adminUser.id)
              AND (adminUser.passwordHash = encode(digest(in_credentials_password || COALESCE(adminUser.passwordSalt,''),'sha1'),'hex'));

    RETURN var_credentialsId;
  END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION fn_appuser_resetPassword(in_credentials_email text, in_credentials_password text, in_email text, in_new_password text, in_new_salt text)
  RETURNS integer AS
$BODY$
    DECLARE
        new_passwordHash text;
        var_credentialsId text;

    BEGIN
        var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            LIMIT 1;

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        new_passwordHash := encode(digest(in_new_password || coalesce(in_new_salt,''), 'sha1'), 'hex');

        UPDATE App_User SET
            passwordHash = new_passwordHash,
            passwordSalt = in_new_salt
        WHERE id = (SELECT id from app_user where (email ILIKE in_email OR displayName = in_email) limit 1);

        -- be sure to update systemadministrators row if the target user is a sysadmin
        update systemadministrators set
            passwordHash = new_passwordHash,
            passwordSalt = in_new_salt
        where memberId = (SELECT id from app_user where (email ILIKE in_email OR displayName = in_email) limit 1);
        
        return 1;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_appuser_updatePassword(text,text) OWNER TO postgres;


-- Function: fn_appuser_login(text,text)

DROP FUNCTION IF EXISTS fn_appuser_updatePassword(text,text,text, text);

CREATE OR REPLACE FUNCTION fn_appuser_updatePassword(in_credentials_email text, in_credentials_password text, in_new_password text, in_new_salt text)
  RETURNS integer AS
$BODY$
    DECLARE
        new_passwordHash text;
        var_credentialsId text;

    BEGIN
         var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        new_passwordHash := encode(digest(in_new_password || coalesce(in_new_salt,''), 'sha1'), 'hex');

        UPDATE App_User SET
            passwordHash = new_passwordHash,
            passwordSalt = in_new_salt
        WHERE id = var_credentialsId;

        return 1;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_appuser_updatePassword(text,text) OWNER TO postgres;

-- Function: fn_appuser_login(text,text)

DROP FUNCTION IF EXISTS fn_appuser_login(text,text);

CREATE OR REPLACE FUNCTION fn_appuser_login(in_email text, in_password text)
  RETURNS SETOF App_User AS
$BODY$
    DECLARE
        rec App_User%ROWTYPE;

    BEGIN
        -- NOTE that in org.springframework.security.authentication.encoding.BasePasswordEncoder
        -- the format for adding salt is "password{salt}" and {} chars are illegal in the salt
        -- so the following may need to be altered if generating different hashes than Spring
        FOR rec IN
            SELECT *
            FROM App_User
            WHERE id = fn_get_credentials_id(in_email, in_password)
        LOOP
            RETURN NEXT rec;
        END LOOP;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_appuser_login(text,text) OWNER TO postgres;


---------------------------------------------------------------------
-- Function: fn_appuser_login2(text,text)
--
-- Variation on the above function which returns ALL AccountMemberRoles,
-- ProjectMemberRoles, and SystemAdministrators settings for a user.
--
-- TBD: Add paging controls and AppUser/AppUserDao support.

DROP FUNCTION IF EXISTS fn_appuser_login2(text,text,integer, integer);

DROP TYPE IF EXISTS _type_appuser_login2_ CASCADE;

CREATE TYPE _type_appuser_login2_ AS (
    id text,                -- App_User
    email text,
    displayName text,
    description text,
    firstName text,
    lastName text,
    nameFormat text,
    website text,
    twitter text,
    accountId bigint,
    roleTypeId int,
    accountName text,
    accountDescription text,
    accountTypeId int,
    accountTermDueDateTime timestamp,
    accountGoodStanding int,
    accountTypeTag text,
    accountTypeName text,
    customerprofileid text,
    allowSubscriptionPayment int,
    systemAdministrator boolean,
    avatarId int4,
    bannerId int4
);

CREATE OR REPLACE FUNCTION fn_appuser_login2(in_email text, in_password text, in_count integer, in_offset integer)
  RETURNS SETOF _type_appuser_login2_ AS
$BODY$
    DECLARE
        rec _type_appuser_login2_%ROWTYPE;
        var_credentialsId text;

    BEGIN
        --RAISE NOTICE 'EXECUTE %', var_query; -- debugging aid when running function from psql command line

        var_credentialsId := fn_get_credentials_id(in_email, in_password);

        -- NOTE that in org.springframework.security.authentication.encoding.BasePasswordEncoder
        -- the format for adding salt is "password{salt}" and {} chars are illegal in the salt
        -- so the following may need to be altered if generating different hashes than Spring
        FOR rec IN
            SELECT 
                au.id
                , au.email
                , au.displayName
                , au.description
                , au.firstName
                , au.lastName
                , au.nameFormat
                , au.website
                , au.twitter
                , amr.accountId
                , MIN(amr.typeId) As roleTypeId
                , a.name as accountName
                , a.description as accountDescription
                , a.typeId as accountTypeId
                , a.termDueDateTime as accountTermDueDateTime
                , a.goodStanding as accountGoodStanding
                , at.tag as accountTypeTag
                , at.name as accountTypeName
                , cc.customerprofileid
                , a.allowSubscriptionPayment
                , sa.memberId is not null as systemAdministrator
                , au.avatarid
                , a.bannerId
			FROM App_User au
			     INNER JOIN AccountMemberRoles amr 
			         ON amr.memberid = au.id
			         INNER JOIN Accounts a 
			             ON a.accountid = amr.accountid
			             INNER JOIN AccountTypes at 
			                 ON a.typeid = at.accounttypeid
			         LEFT JOIN CreditCards cc 
			             ON amr.accountId = cc.accountId 
			             AND cc.retired = 0
			     LEFT JOIN SystemAdministrators sa 
			         on sa.memberid = au.id
			WHERE au.id = var_credentialsId
			GROUP BY au.id, au.email, au.displayName, au.description, au.firstName, au.lastName, au.nameFormat, 
				 au.website, au.twitter,
			     amr.accountId, a.name, a.description, at.tag, at.name, a.typeId, a.termDueDateTime,
			     a.goodStanding, cc.customerprofileid, a.allowSubscriptionPayment, sa.memberid, au.avatarId, a.bannerId
			LIMIT in_count OFFSET in_offset
        LOOP
            RETURN NEXT rec;
        END LOOP;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_appuser_login2(text,text,integer, integer) OWNER TO postgres;


---------------------------------------------------------------------
-- Function: fn_appuser_getbyemail(text)

DROP FUNCTION IF EXISTS fn_appuser_getbyemail(text);

CREATE OR REPLACE FUNCTION fn_appuser_getbyemail(in_credentials_email text, in_credentials_password text, in_email text)
  RETURNS SETOF App_User AS
$BODY$
    DECLARE
        rec App_User%ROWTYPE;

    BEGIN
        /*
         var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;
        */

        FOR rec IN
            SELECT *
            FROM App_User
            WHERE lower(in_credentials_email)=lower(email)
        LOOP
            RETURN NEXT rec;
        END LOOP;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_appuser_getbyemail(text) OWNER TO postgres;
