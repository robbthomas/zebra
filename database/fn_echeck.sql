--------------------------------------------------------------------------------
-- fn_echecks.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- EChecks functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='EChecks' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('EChecks','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('EChecks','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('EChecks','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('EChecks','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('EChecks','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('EChecks','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_echeck_create(text, text, integer, text, text, text, text, text, text, text)

-- DROP FUNCTION fn_echeck_create(text, text, integer, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_echeck_create(in_credentials_email text, in_credentials_password text, in_accountId integer, in_bankRoutingNumber text, in_bankAccountNumber text, in_bankName text, in_bankAccountName text, in_customerProfileId text, in_customerPaymentProfileId text, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'EChecks';
        var_functionName text := 'CREATE';

        new_eCheckId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into EChecks(
            accountId,
            bankRoutingNumber,
            bankAccountNumber,
            bankName,
            bankAccountName,
            customerProfileId,
            customerPaymentProfileId,
            editedById
        )
        values(
            in_accountId,
            in_bankRoutingNumber,
            in_bankAccountNumber,
            in_bankName,
            in_bankAccountName,
            in_customerProfileId,
            in_customerPaymentProfileId,
            in_editedById
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_eCheckId := currval('seq_EChecks_eCheckId');
        END IF;

        return new_eCheckId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_echeck_create(text, text, integer, text, text, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_echeck_update(text, text, integer, integer, text, text, text, text, text, text, text, text)

-- DROP FUNCTION fn_echeck_update(text, text, integer, integer, text, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_echeck_update(in_credentials_email text, in_credentials_password text, in_eCheckId integer, in_accountId integer, in_bankRoutingNumber text, in_bankAccountNumber text, in_bankName text, in_bankAccountName text, in_customerProfileId text, in_customerPaymentProfileId text, in_editedById text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'EChecks';
        var_functionName text := 'UPDATE';

        rec EChecks%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from EChecks where eCheckId = in_eCheckId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_accountId' THEN
                rec.accountId := in_accountId;
            ELSEIF param_name = 'in_bankRoutingNumber' THEN
                rec.bankRoutingNumber := in_bankRoutingNumber;
            ELSEIF param_name = 'in_bankAccountNumber' THEN
                rec.bankAccountNumber := in_bankAccountNumber;
            ELSEIF param_name = 'in_bankName' THEN
                rec.bankName := in_bankName;
            ELSEIF param_name = 'in_bankAccountName' THEN
                rec.bankAccountName := in_bankAccountName;
            ELSEIF param_name = 'in_customerProfileId' THEN
                rec.customerProfileId := in_customerProfileId;
            ELSEIF param_name = 'in_customerPaymentProfileId' THEN
                rec.customerPaymentProfileId := in_customerPaymentProfileId;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE EChecks SET
            accountId = rec.accountId,
            bankRoutingNumber = rec.bankRoutingNumber,
            bankAccountNumber = rec.bankAccountNumber,
            bankName = rec.bankName,
            bankAccountName = rec.bankAccountName,
            customerProfileId = rec.customerProfileId,
            customerPaymentProfileId = rec.customerPaymentProfileId,
            editedById = rec.editedById
        WHERE eCheckId = in_eCheckId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_echeck_update(text, text, integer, integer, text, text, text, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_echeck_retire(text, text, integer, text)

-- DROP FUNCTION fn_echeck_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_echeck_retire(in_credentials_email text, in_credentials_password text, in_eCheckId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'EChecks';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE EChecks SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE eCheckId = in_eCheckId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_echeck_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_echeck_filter(text, text, integer, integer, text, integer, text, text, text, text, text, text, text)

-- DROP FUNCTION fn_echeck_filter(text, text, integer, integer, text, integer, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_echeck_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_accountId integer, in_bankRoutingNumber text, in_bankAccountNumber text, in_bankName text, in_bankAccountName text, in_customerProfileId text, in_customerPaymentProfileId text, in_filter_list text)
  RETURNS SETOF EChecks AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'EChecks';
        var_functionName text := 'FILTER';

        rec EChecks%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM EChecks WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_eCheckId' THEN
            ELSEIF filter_name = 'in_accountId' THEN
                var_query := var_query || ' AND accountId = ' || in_accountId;
            ELSEIF filter_name = 'in_bankRoutingNumber' THEN
                var_query := var_query || ' AND bankRoutingNumber ILIKE ''%' || in_bankRoutingNumber || '%''';
            ELSEIF filter_name = 'in_bankAccountNumber' THEN
                var_query := var_query || ' AND bankAccountNumber ILIKE ''%' || in_bankAccountNumber || '%''';
            ELSEIF filter_name = 'in_bankName' THEN
                var_query := var_query || ' AND bankName ILIKE ''%' || in_bankName || '%''';
            ELSEIF filter_name = 'in_bankAccountName' THEN
                var_query := var_query || ' AND bankAccountName ILIKE ''%' || in_bankAccountName || '%''';
            ELSEIF filter_name = 'in_customerProfileId' THEN
                var_query := var_query || ' AND customerProfileId ILIKE ''%' || in_customerProfileId || '%''';
            ELSEIF filter_name = 'in_customerPaymentProfileId' THEN
                var_query := var_query || ' AND customerPaymentProfileId ILIKE ''%' || in_customerPaymentProfileId || '%''';
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            ELSEIF filter_name = 'in_retired' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM EChecks;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_echeck_filter(text, text, integer, integer, text, integer, text, text, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_echeck_find(text, text, integer)

-- DROP FUNCTION fn_echeck_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_echeck_find(in_credentials_email text, in_credentials_password text, in_eCheckId integer)
  RETURNS SETOF EChecks AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'EChecks';
        var_functionName text := 'FIND';

        rec EChecks%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM EChecks
                WHERE eCheckId = in_eCheckId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_echeck_find(text, text, integer) OWNER TO postgres;
