SELECT * INTO TEMPORARY TABLE temp_invoicelineitems_Autohistory FROM invoicelineitems_Autohistory;

-- Recreate autohistory for invoicelineitems.
DROP SEQUENCE seq_invoicelineitems_AutoHistory CASCADE;
DROP TABLE invoicelineitems_AutoHistory CASCADE;

CREATE SEQUENCE seq_invoicelineitems_AutoHistory;
CREATE TABLE invoicelineitems_AutoHistory (
    LIKE invoicelineitems,
    invoicelineitems_AutoHistory_Id bigint DEFAULT nextval('seq_invoicelineitems_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

insert into invoicelineitems_autohistory 
    (invoicelineitemid,
     invoiceid,
     linenumber,
     quantity,
     amounttotal,
     description,
     publishid,
     autohistorydate) 
    (
    select invoicelineitemid,
     invoiceid,
     linenumber,
     quantity,
     amounttotal,
     description,
     publishid,
     autohistorydate from temp_invoicelineitems_autohistory);
     
drop table temp_invoicelineitems_Autohistory;