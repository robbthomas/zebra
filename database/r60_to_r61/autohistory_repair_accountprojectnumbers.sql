-- Save existing autohistory
SELECT * INTO TEMPORARY TABLE temp_accountprojectnumbers_autohistory
    FROM accountprojectnumbers_autohistory;
    
-- Recreate autohistory for accountprojectnumbers
DROP SEQUENCE seq_accountprojectnumbers_autohistory CASCADE;
DROP TABLE accountprojectnumbers_AutoHistory CASCADE;

CREATE SEQUENCE seq_accountprojectnumbers_autohistory;
CREATE TABLE accountprojectnumbers_autohistory (
    LIKE accountprojectnumbers,
    accountprojectnumbers_autohistory_id bigint DEFAULT nextval('seq_accountprojectnumbers_autohistory')
        NOT NULL PRIMARY KEY,
    autohistorydate timestamp with time zone DEFAULT now()
);

insert into accountprojectnumbers_autohistory
(
    accountid,
    publishid,
    invoicelineitemid,
    retired,
    retiredbyid,
    retireddatetime,
    retiredreasonid,
    accountprojectnumberid,
    autohistorydate
) (
    select 
    accountid,
    publishid,
    invoicelineitemid,
    retired,
    retiredbyid,
    retireddatetime,
    retiredreasonid,
    accountprojectnumberid,
    autohistorydate
   from temp_accountprojectnumbers_autohistory
);

drop table temp_accountprojectnumbers_autohistory;