-- Save existing autohistory.
SELECT * INTO TEMPORARY TABLE temp_paymentlineitems_Autohistory FROM paymentlineitems_Autohistory;

-- Recreate autohistory for paymentlineitems.
DROP SEQUENCE seq_paymentlineitems_AutoHistory CASCADE;
DROP TABLE paymentlineitems_AutoHistory CASCADE;

CREATE SEQUENCE seq_paymentlineitems_AutoHistory;
CREATE TABLE paymentlineitems_AutoHistory (
    LIKE paymentlineitems,
    paymentlineitems_AutoHistory_Id bigint DEFAULT nextval('seq_paymentlineitems_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

insert into paymentlineitems_autohistory 
    (paymentlineitemid,
    paymentid,
    linenumber,
    quantity,
    amounttotal,
    description,
    publishid,
    autohistorydate) 
    (
    select paymentlineitemid,
    paymentid,
    linenumber,
    quantity,
    amounttotal,
    description,
    publishid,
    autohistorydate 
    from temp_paymentlineitems_autohistory);
    
drop table temp_paymentlineitems_Autohistory;