-------------------------------------------------------------------------------
-- create productcodes table 
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (66);

insert into ProductCodes(ProductCodeId, tag, name, description) values (111, 'ACCOUNT_LEARNER_25', '25 Free Learners', 'set maximum number of learners to 25');
insert into ProductCodes(ProductCodeId, tag, name, description) values (112, 'ACCOUNT_LEARNER_100', '100 Learners', 'set maximum number of learners to 25 ZebraZapps.com account');
insert into ProductCodes(ProductCodeId, tag, name, description) values (113, 'ACCOUNT_LEARNER_500', '500 Learners', 'set maximum number of learners to 25 for ZebraZapps.com account');
insert into ProductCodes(ProductCodeId, tag, name, description) values (114, 'ACCOUNT_LEARNER_1000', '1000 Learners', 'set maximum number of learners to 25 for ZebraZapps.com account');
insert into ProductCodes(ProductCodeId, tag, name, description) values (115, 'ACCOUNT_LEARNER_1K_PLUS', '100 Learners', 'set maximum number of learners to 25 for ZebraZapps.com account');

insert into ProductCodePrices (productcodeid, price, effectivedate, termdate) values (111, 0, date '1970-01-01', date '2038-01-19');
insert into ProductCodePrices (productcodeid, price, effectivedate, termdate) values (112, 10000, date '1970-01-01', date '2038-01-19');
insert into ProductCodePrices (productcodeid, price, effectivedate, termdate) values (113, 50000, date '1970-01-01', date '2038-01-19');
insert into ProductCodePrices (productcodeid, price, effectivedate, termdate) values (114, 10000, date '1970-01-01', date '2038-01-19');
insert into ProductCodePrices (productcodeid, price, effectivedate, termdate) values (115, 0, date '1970-01-01', date '2038-01-19');

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;