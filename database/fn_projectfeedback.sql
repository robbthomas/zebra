--------------------------------------------------------------------------------
-- fn_projectfeedbacks.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- ProjectFeedbacks functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='ProjectFeedbacks'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ProjectFeedbacks','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_projectfeedback_create(text, text, text, text, text, text, text, timestamp)

-- DROP FUNCTION fn_projectfeedback_create(text, text, text, text, text, text, text, timestamp);

CREATE OR REPLACE FUNCTION fn_projectfeedback_create(in_credentials_email text, in_credentials_password text, in_projectID text, in_feedbackType text, in_memberID text, in_flagReason text, in_retiredById text, in_retiredDateTime timestamp)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProjectFeedbacks';
        var_functionName text := 'CREATE';
        var_feedbackTypeID int4 := feedbackTypeID FROM feedbackTypes WHERE lower(tag) = lower(in_feedbackType);
--        var_editedById text;

        new_projectFeedbackId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO ProjectFeedbacks(
            projectID,
            feedbackTypeID,
            memberID,
            flagReason,
            retiredById,
            retiredDateTime
        )
        VALUES(
            in_projectID,
            var_feedbackTypeID,
            var_credentialsId,
            in_flagReason,
            in_retiredById,
            in_retiredDateTime
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_projectFeedbackId := currval('seq_ProjectFeedbacks_projectFeedbackId');
        END IF;

        return new_projectFeedbackId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_projectfeedback_create(text, text, text, text, text, text, text, timestamp) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_projectfeedback_update(text, text, integer, text, text, text, text, text, timestamp, text)

-- DROP FUNCTION fn_projectfeedback_update(text, text, integer, text, text, text, text, text, timestamp, text);

CREATE OR REPLACE FUNCTION fn_projectfeedback_update(in_credentials_email text, in_credentials_password text, in_projectFeedbackId integer, in_projectID text, in_feedbackType text, in_memberID text, in_flagReason text, in_retiredById text, in_retiredDateTime timestamp, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProjectFeedbacks';
        var_functionName text := 'UPDATE';
		var_feedbackTypeID int4 := feedbackTypeID FROM feedbackTypes WHERE lower(tag) = lower(in_feedbackType);
        
        rec ProjectFeedbacks%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from ProjectFeedbacks where projectFeedbackId = in_projectFeedbackId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_projectID' THEN
                rec.projectID := in_projectID;
            ELSEIF param_name = 'in_feedbackTypeID' THEN
                rec.feedbackTypeID := var_feedbackTypeID;
            ELSEIF param_name = 'in_memberID' THEN
                rec.memberID := in_memberID;
            ELSEIF param_name = 'in_flagReason' THEN
                rec.flagReason := in_flagReason;
            ELSEIF param_name = 'in_retiredById' THEN
                rec.retiredById := in_retiredById;
            ELSEIF param_name = 'in_retiredDateTime' THEN
                rec.retiredDateTime := in_retiredDateTime;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE ProjectFeedbacks SET
            projectID = rec.projectID,
            feedbackTypeID = rec.feedbackTypeID,
            memberID = rec.memberID,
            flagReason = rec.flagReason,
            updateddatetime = now(),
            retiredById = rec.retiredById,
            retiredDateTime = rec.retiredDateTime
        WHERE projectFeedbackId = in_projectFeedbackId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_projectfeedback_update(text, text, integer, text, text, text, text, text, timestamp, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_projectfeedback_retire(text, text, integer, text)

-- DROP FUNCTION fn_projectfeedback_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_projectfeedback_retire(in_credentials_email text, in_credentials_password text, in_projectFeedbackId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProjectFeedbacks';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE ProjectFeedbacks SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        WHERE projectFeedbackId = in_projectFeedbackId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_projectfeedback_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_projectfeedback_filter(text, text, integer, integer, text, text, text, text, text, text, timestamp, text)

-- DROP FUNCTION fn_projectfeedback_filter(text, text, integer, integer, text, text, text, text, text, text, timestamp, text);

CREATE OR REPLACE FUNCTION fn_projectfeedback_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_projectID text, in_feedbackTypeID text, in_memberID text, in_flagReason text, in_retiredById text, in_retiredDateTime timestamp, in_filter_list text)
  RETURNS SETOF ProjectFeedbacks AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProjectFeedbacks';
        var_functionName text := 'FILTER';

        rec ProjectFeedbacks%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM ProjectFeedbacks WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_projectFeedbackID' THEN
            ELSEIF filter_name = 'in_projectID' THEN
                var_query := var_query || ' AND projectID ILIKE ''%' || in_projectID || '%''';
            ELSEIF filter_name = 'in_feedbackTypeID' THEN
                var_query := var_query || ' AND feedbackTypeID ILIKE ''%' || in_feedbackTypeID || '%''';
            ELSEIF filter_name = 'in_memberID' THEN
                var_query := var_query || ' AND memberID ILIKE ''%' || in_memberID || '%''';
            ELSEIF filter_name = 'in_flagReason' THEN
                var_query := var_query || ' AND flagReason ILIKE ''%' || in_flagReason || '%''';
            ELSEIF filter_name = 'in_createddatetime' THEN
            ELSEIF filter_name = 'in_updateddatetime' THEN
            ELSEIF filter_name = 'in_retired' THEN
            ELSEIF filter_name = 'in_retiredById' THEN
                var_query := var_query || ' AND retiredById ILIKE ''%' || in_retiredById || '%''';
            ELSEIF filter_name = 'in_retiredDateTime' THEN
                var_query := var_query || ' AND retiredDateTime = ''' || in_retiredDateTime || '''';
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM ProjectFeedbacks;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_projectfeedback_filter(text, text, integer, integer, text, text, text, text, text, text, timestamp, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_projectfeedback_find(text, text, integer)

-- DROP FUNCTION fn_projectfeedback_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_projectfeedback_find(in_credentials_email text, in_credentials_password text, in_projectFeedbackId integer)
  RETURNS SETOF ProjectFeedbacks AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProjectFeedbacks';
        var_functionName text := 'FIND';

        rec ProjectFeedbacks%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM ProjectFeedbacks
                WHERE projectFeedbackId = in_projectFeedbackId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_projectfeedback_find(text, text, integer) OWNER TO postgres;
