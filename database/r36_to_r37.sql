-------------------------------------------------------------------------------
-- r36_to_r37.sql
--
-- correcting code column type
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

ALTER TABLE CouponCodes
	ALTER COLUMN code TYPE varchar(255);

-------------------------------------------------------------------------------
-- Update database version.
-------------------------------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES(37);

-------------------------------------------------------------------------------
-- End transaction wrapper for this update.
-------------------------------------------------------------------------------
COMMIT;
