-------------------------------------------------------------------------------
-- created_date fields formerly managed by a Spring pointcut/asset.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES(81);

alter table asset alter column date_created drop not null;
alter table asset alter column date_created set default now();


alter table asset alter column last_updated drop not null;
alter table asset alter column last_updated set default now();

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
