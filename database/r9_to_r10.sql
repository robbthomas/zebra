-------------------------------------------------------------------------------
-- r9_to_r10.sql
--
-- Redo Published Autohistory to include screenShotId column.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (10);
-------------------------------------------------------------------------------

-- Recreate autohistory for Published. DO NOT save and restore any of the data in old table this time.
DROP SEQUENCE IF EXISTS seq_Published_AutoHistory CASCADE;
DROP TABLE IF EXISTS Published_AutoHistory CASCADE;

CREATE SEQUENCE seq_Published_AutoHistory;
CREATE TABLE Published_AutoHistory (
    LIKE Published,
    Published_AutoHistory_Id bigint DEFAULT nextval('seq_Published_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_Published_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO Published_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
