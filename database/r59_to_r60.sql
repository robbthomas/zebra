-------------------------------------------------------------------------------
-- r53_to_r54.sql
--
-- create tables for PRO accounts
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (60);
-------------------------------------------------------------------------------

-- PRO projects
alter table parent add column accountTypeId int default 2;
alter table parent add constraint "fk_parent_accountTypeId" foreign key(accountTypeId) references accounttypes(accountTypeId);

-- add new license types
insert into LicenseTypes (licensetypeid, tag, name, displayorder, republishable, editable, embedable, projecttypeid)
	values (7, 'license_type_guest_list', 'Guest List Access License', -1, 'f', 'f', 'f', 2);

insert into LicenseTypes (licensetypeid, tag, name, displayorder, republishable, editable, embedable, projecttypeid)
	values (8, 'license_type_rcv_xfer', 'Receive Project Transfer', -1, 't', 't', 't', 2);

	
insert into LicenseTypes (licensetypeid, tag, name, displayorder, republishable, editable, embedable, projecttypeid)
	values (-1, 'license_type_none', 'Unpublished project', -1, 'f', 'f', 'f', 2);

insert into RetiredReasonTypes (retiredreasontypeid, name) values (5, 'transfer_canceled');
insert into RetiredReasonTypes (retiredreasontypeid, name) values (6, 'transfer_completed');

-- Pending transfers
create sequence seq_ProjectMemberAccess_projectMemberAccessId start with 6502;
create table ProjectMemberAccess
(
	projectMemberAccessId int8 not null primary key default nextval('seq_ProjectMemberAccess_projectMemberAccessId'),
	senderAccountId int8,
	senderMemberId varchar(255),
	receiverAccountId int8,
	receiverMemberId varchar(255),
	createdDateTime timestamp default now(),
	projectId varchar(255),
	retired int4,
	retiredById varchar(255),
	retiredDateTime timestamp,
	retiredreasontypeid int4,
	newToReceiver int4,
	viewedDateTime  timestamp,
	licenseTypeId int4
);

alter table ProjectMemberAccess add constraint fk_projecttransfers_senderaccountid foreign key(senderAccountId) references accounts(accountid);
alter table ProjectMemberAccess add constraint fk_projecttransfers_receiveraccountid foreign key(receiverAccountId) references accounts(accountid);

alter table ProjectMemberAccess add constraint fk_projecttransfers_sendermemberid foreign key(senderMemberId) references app_user(id);
alter table ProjectMemberAccess add constraint fk_projecttransfers_receivermemberid foreign key(receiverMemberId) references app_user(id);

alter table ProjectMemberAccess add constraint fk_projecttransfers_retiredById foreign key(retiredById) references app_user(id);
alter table ProjectMemberAccess add constraint fk_projecttransfers_licenseTypeId foreign key(licenseTypeId) references LicenseTypes(licenseTypeid);

alter table ProjectMemberAccess add constraint fk_ProjectMemberAccess_retiredReasonCode foreign key (retiredreasontypeid) references RetiredReasonTypes(retiredreasontypeid);
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;