--------------------------------------------------------------------------------
-- fn_couponcodetypes.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- CouponCodeTypes functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='CouponCodeTypes'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodeTypes','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_couponcodetype_create(text, text, text)

-- DROP FUNCTION fn_couponcodetype_create(text, text, text);

CREATE OR REPLACE FUNCTION fn_couponcodetype_create(in_credentials_email text, in_credentials_password text, in_description text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CouponCodeTypes';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_couponCodeTypeId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO CouponCodeTypes(
            description
        )
        VALUES(
            in_description
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_couponCodeTypeId := currval('seq_CouponCodeTypes_couponCodeTypeId');
        END IF;

        return new_couponCodeTypeId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_couponcodetype_create(text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_couponcodetype_update(text, text, integer, text, text)

-- DROP FUNCTION fn_couponcodetype_update(text, text, integer, text, text);

CREATE OR REPLACE FUNCTION fn_couponcodetype_update(in_credentials_email text, in_credentials_password text, in_couponCodeTypeId integer, in_description text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CouponCodeTypes';
        var_functionName text := 'UPDATE';

        rec CouponCodeTypes%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from CouponCodeTypes where couponCodeTypeId = in_couponCodeTypeId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_description' THEN
                rec.description := in_description;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE CouponCodeTypes SET
            description = rec.description
        WHERE couponCodeTypeId = in_couponCodeTypeId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_couponcodetype_update(text, text, integer, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_couponcodetype_retire(text, text, integer, text)

-- DROP FUNCTION fn_couponcodetype_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_couponcodetype_retire(in_credentials_email text, in_credentials_password text, in_couponCodeTypeId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CouponCodeTypes';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE CouponCodeTypes SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        WHERE couponCodeTypeId = in_couponCodeTypeId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_couponcodetype_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_couponcodetype_filter(text, text, integer, integer, text, text, text)

-- DROP FUNCTION fn_couponcodetype_filter(text, text, integer, integer, text, text, text);

CREATE OR REPLACE FUNCTION fn_couponcodetype_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_description text, in_filter_list text)
  RETURNS SETOF CouponCodeTypes AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CouponCodeTypes';
        var_functionName text := 'FILTER';

        rec CouponCodeTypes%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM CouponCodeTypes WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_couponCodeTypeId' THEN
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM CouponCodeTypes;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_couponcodetype_filter(text, text, integer, integer, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_couponcodetype_find(text, text, integer)

-- DROP FUNCTION fn_couponcodetype_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_couponcodetype_find(in_credentials_email text, in_credentials_password text, in_couponCodeTypeId integer)
  RETURNS SETOF CouponCodeTypes AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CouponCodeTypes';
        var_functionName text := 'FIND';

        rec CouponCodeTypes%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM CouponCodeTypes
                WHERE couponCodeTypeId = in_couponCodeTypeId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_couponcodetype_find(text, text, integer) OWNER TO postgres;
