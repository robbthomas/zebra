-------------------------------------------------------------------------------
-- r107_to_r108.sql
--
-- Author: Joe Bodell
-- Notes:  Backfill credit card records for accounts that don't already have them
--
-------------------------------------------------------------------------------

INSERT INTO creditCards (
      accountid
    , addressid
    , nameoncreditcard
    , maskednumber
    , expirationmonth
    , expirationyear
    , customerprofileid
    , customerpaymentprofileid
    , editedbyid
    , editeddatetime
    , retired
    , retiredbyid
    , retireddatetime
    , creditcardtype
    , firstname
    , lastname
    , email
    , phone
)
    SELECT
          a.accountId
        , addresses.addressId
        , NULL
        , 'NULL'
        , NULL
        , NULL
        , NULL
        , NULL
        , au.id
        , NULL
        , 0
        , NULL
        , NULL
        , NULL
        , au.firstName
        , au.lastName
        , au.email
        , NULL
    FROM app_user au
        INNER JOIN accountMemberRoles amr
            ON au.id = amr.memberId
            INNER JOIN accounts a
                ON amr.accountId = a.accountId
                INNER JOIN addresses
                    ON a.accountId = addresses.accountId
                    INNER JOIN AddressTelephoneTypes att
                        ON addresses.typeId = att.addressTelephoneTypeId
    WHERE a.retired = 0
    AND att.tag ILIKE 'billing'
    AND NOT EXISTS (
        SELECT 'x'
        FROM creditCards cc
        WHERE cc.accountId = a.accountId
    );

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (108);
------------------------------------------------------
         