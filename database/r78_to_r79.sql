-------------------------------------------------------------------------------
-- r78_to_r79.sql
--
-- Fix duplicate urlNames by properly unpublishing projects that had been marked as
-- retired in the 'published' table.
--
--
-- Create unique index on urlName by project type, published, retired, etc.
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
-------------------------------------------------------------------------------
-- Update database version.
-------------------------------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES(79);

select projectid as "projects unpublished by r78_to_r79" from projects
    where published = true and dep_publishedretired = true;

-- fix (almost all) duplicate urlnames, by properly unpublishing projects that had been deleted in the published table
update projects
    set published = false
    where published = true and dep_publishedretired = true;


 -- just in case, at least hudson will tell us where the dupes are, to fix by hand.
 select  urlName, count(*)
        from projects
        where published = true 
        and retired = false 
        and nextpublishedversionid is null
        and urlName is not null
        and urlName <> ''
        and projecttypeid = 1
        group by urlName
        having count(*) > 1;

  select  urlName, count(*)
        from projects
        where published = true 
        and retired = false 
        and nextpublishedversionid is null
        and urlName is not null
        and urlName <> ''
        and projecttypeid = 2
        group by urlName
        having count(*) > 1;
       
 -- create the indexi
create unique index ix_unique_urlname_gadget on projects (urlName)
     where published = true 
        and retired = false 
        and nextpublishedversionid is null 
        and urlName is not null
        and urlName <> ''
        and projecttypeid = 1;

create unique index ix_unique_urlname_zapp on projects (urlName)
     where published = true 
        and retired = false 
        and nextpublishedversionid is null 
        and urlName is not null
        and urlName <> ''
        and projecttypeid = 2;

        
-------------------------------------------------------------------------------
-- End transaction wrapper for this update.
-------------------------------------------------------------------------------
commit;
