-------------------------------------------------------------------------------
-- r25_to_r26.sql
-- 
-- fix sysadmin table where it had been messed up byupdate_password
-- update pswdAdmin password
--
-- add on_update trigger for "parent" table.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (27);
-------------------------------------------------------------------------------
update systemadministrators sa                                                                                                                                                  
set
    passwordHash = au.passwordhash,
    passwordSalt = au.passwordsalt
from app_user au where sa.memberid = au.id;



-- reset pswdAdmin password
CREATE FUNCTION tmpfn_reset_pwdadmin_passwd()
RETURNS integer AS
$BODY$
    DECLARE
        new_passwordHash text;

    BEGIN
        new_passwordHash := encode(digest('o5vBnE5Q22' || coalesce('',''), 'sha1'), 'hex');

        UPDATE App_User SET
           passwordHash = new_passwordHash,
           passwordSalt = ''
           WHERE id = (SELECT id from app_user where email ILIKE 'pswdAdmin@zebrazapps.com');

        update systemadministrators set
            passwordHash = new_passwordHash,
            passwordSalt = ''
            where memberId = (SELECT id from app_user where email ILIKE 'pswdAdmin@zebrazapps.com');

        RETURN 1;
    END;
$BODY$
LANGUAGE 'plpgsql';

SELECT * FROM tmpfn_reset_pwdadmin_passwd();

DROP FUNCTION tmpfn_reset_pwdadmin_passwd();







-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
