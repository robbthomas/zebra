-------------------------------------------------------------------------------
-- r99_to_r100.sql
--
-- Author:  David Hoyt
-- Date:  11-02-2012
-- Notes:  Add asyncInProgress column to Projects table
--
-------------------------------------------------------------------------------

ALTER TABLE Projects add column asyncInProgress boolean default false;

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (100);
------------------------------------------------------