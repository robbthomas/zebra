-------------------------------------------------------------------------------
-- Add third project type for events
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES(84);

alter table PublishedPurchasesAcquisitions alter column publishid drop not null;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;