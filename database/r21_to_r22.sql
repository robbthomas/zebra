-------------------------------------------------------------------------------
-- r21_to_r22.sql
--
-- Add support for multiple image/thumbnail/caption per Zapp/Gadget
-- in an ordered list.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (22);
-------------------------------------------------------------------------------

CREATE TABLE NameFormat (
    nameFormatId int NOT NULL PRIMARY KEY,
    tag text,
    name text,
    displayOrder int
);
INSERT INTO NameFormat(nameFormatId,tag,name,displayOrder) VALUES (1,'displayName','DisplayName',10);
INSERT INTO NameFormat(nameFormatId,tag,name,displayOrder) VALUES (2,'firstLast','First and Last Name',20);


ALTER TABLE app_user ADD COLUMN nameFormat text;
CREATE UNIQUE INDEX ix_NameFormat_tag ON NameFormat(tag);
ALTER TABLE app_user ADD CONSTRAINT fk_app_user_nameFormat FOREIGN KEY(nameFormat) REFERENCES NameFormat(tag);

\i fn_appuser.sql;
\i fn_appuser_login.sq;

-- Remove old functions with one fewer parameter
DROP FUNCTION fn_appuser_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_displayName text, in_description text, in_email text, in_firstName text, in_lastName text, in_editedById text, in_editedDateTime timestamp with time zone, in_retiredById text, in_retiredDateTime timestamp with time zone, in_filter_list text);

DROP FUNCTION fn_appuser_create(
    in_credentials_email text, in_credentials_password text,
    in_displayName text, in_description text, in_email text, in_firstName text, in_lastName text, in_password text, in_passwordSalt text, in_editedById text);

DROP FUNCTION fn_appuser_update(in_credentials_email text, in_credentials_password text, in_appUserId text, in_version integer, in_displayName text, in_description text, in_email text, in_firstName text, in_lastName text, in_website text, in_twitter text, in_password text, in_editedById text, in_retiredById text, in_param_list text);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

