-------------------------------------------------------------------------------
-- r46_to_r47.sql
--
-- Create tables for tracking sharing of zapps, and email invites
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (47);
-------------------------------------------------------------------------------
create table ShareTypes (
	shareTypeId 	SERIAL not null PRIMARY KEY,
	tag			text,
	"name"		text,
	displayorder	int4,
	updateddatetime	timestamp not null default now()
);


create table SharedProjects (
	sharedprojectid	SERIAL not null PRIMARY KEY,
	sharetypeid		int8,
	"publishid"		int8,
	updateddatetime	timestamp not null default now(),
	retireddatetime	timestamp,
	retiredbyid		text,
	retiredreasonid	int8
);

alter table SharedProjects
	ADD CONSTRAINT fk_sharedprojects_sharetypes FOREIGN KEY (sharetypeid) REFERENCES sharetypes (sharetypeid) MATCH FULL;

alter table SharedProjects
	ADD CONSTRAINT fk_sharedprojects_publishid FOREIGN KEY (publishid) REFERENCES published (publishid) MATCH FULL;

alter table SharedProjects
	ADD CONSTRAINT fk_sharedprojects_retiredbyappuser FOREIGN KEY (retiredbyid) references app_user(id) MATCH FULL;

-- insert some data for sharetypes
insert into sharetypes (tag, "name", displayorder) values ('twitter', 'Twitter', 1);
insert into sharetypes (tag, "name", displayorder) values ('linkedin', 'Linked In', 2);
insert into sharetypes (tag, "name", displayorder) values ('facebook', 'Facebook', 3);
insert into sharetypes (tag, "name", displayorder) values ('email', 'Email List', 4);
insert into sharetypes (tag, "name", displayorder) values ('lms', 'Learning Management System', 5);
insert into sharetypes (tag, "name", displayorder) values ('embed', 'Generic HTML Embed', 7);


-- email invites
-- no fk into app_user; we decide whether to display 'join' or 'login' dialogs
-- by performing an inner join on app_user.email when the invitee views the zapp.
create table SharedProjectInvitees (
	sharedProjectInviteeId 	SERIAL not null PRIMARY KEY,
	"publishid"			int8 references published(publishid),
	"email"				text,
	updateddatetime	timestamp not null default now(),
	retireddatetime	timestamp,
	retiredbyid		text references app_user(id),
	retiredreasontypeid	int8 references retiredreasontypes(retiredreasontypeid),
	uuid			text not null default replace((uuid_generate_v4())::text, '-'::text, ''::text)
);

-- add new customer type
insert into accounttypes (accounttypeid, tag, "name", description, terms, price, displayorder)
	values (3, 'pro', 'Pro', 'Professional Zapp Account', '1 month', 50000, 200);
	
	
-- add functions
\i fn_sharedproject.sql
\i fn_sharedprojectinvitee.sql
\i fn_sharetype.sql

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

