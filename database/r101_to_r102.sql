-------------------------------------------------------------------------------
-- r101_to_102.sql
--
-- Author:  Joe Bodell
-- Date:  11-28-2012
-- Notes:  Populate UploadableAssetType metadata with values for Avatars and Banner images
--
-------------------------------------------------------------------------------


DELETE FROM uploadableAssetTypeAttributeValue;
DELETE FROM uploadableAssetTypeAttribute;
DELETE FROM uploadableAssetType;
INSERT INTO UploadableAssetType (tag, description) VALUES ('ICON', 'The icon to be displayed in app/gadget lists');
INSERT INTO UploadableAssetType (tag, description) VALUES ('SCREENSHOT', 'Screenshots associated with a given project');
INSERT INTO UploadableAssetType (tag, description) VALUES ('AVATAR', 'User-uploaded image which appears on author profile');
INSERT INTO UploadableAssetType (tag, description) VALUES ('PROJECT_ASSET', 'An asset to be associated with a projects');
INSERT INTO UploadableAssetType (tag, description) VALUES ('BANNER', 'An image to be displayed at the top of an author profile or private shopp');

DELETE FROM UploadableAssetS3Bucket;
INSERT INTO UploadableAssetS3Bucket (bucketPath) VALUES ('com.alleni.public.image/avatar');
INSERT INTO UploadableAssetS3Bucket (bucketPath) VALUES ('com.alleni.public.image/banner');

DELETE FROM UploadableAssetTypeSupportedMimeType;
INSERT INTO UploadableAssetTypeSupportedMimeType (uploadableAssetTypeId, mimeTypeId, uploadableAssetS3BucketId) 
    SELECT uploadableAssetType.uploadableAssetTypeId, mimeType.mimeTypeId, uploadableAssetS3Bucket.uploadableAssetS3BucketId
    FROM uploadableAssetType, mimeType, mimeCategory, uploadableAssetS3Bucket
    WHERE mimeType.mimeCategoryId = mimeCategory.mimeCategoryId
    AND uploadableAssetType.tag = 'AVATAR'
    AND mimecategory.tag = 'IMAGE'
    AND uploadableAssetS3Bucket.bucketPath = 'com.alleni.public.image/avatar';

INSERT INTO UploadableAssetTypeSupportedMimeType (uploadableAssetTypeId, mimeTypeId, uploadableAssetS3BucketId) 
    SELECT uploadableAssetType.uploadableAssetTypeId, mimeType.mimeTypeId, uploadableAssetS3Bucket.uploadableAssetS3BucketId
    FROM uploadableAssetType, mimeType, mimeCategory, uploadableAssetS3Bucket
    WHERE mimeType.mimeCategoryId = mimeCategory.mimeCategoryId
    AND uploadableAssetType.tag = 'BANNER'
    AND mimecategory.tag = 'IMAGE'
    AND uploadableAssetS3Bucket.bucketPath = 'com.alleni.public.image/banner';

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (102);
------------------------------------------------------