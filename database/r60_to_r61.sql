-------------------------------------------------------------------------------
-- Merge Published and Parent tables
--
-- 
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (61);

create table projects as
select parent.id as projectid, parent.version, parent.content, parent.date_created as createddatetime, parent.deleted as retired, parent.initialvalues, parent.last_updated as editeddatetime,
    parent.meta_data as projectmetadata, parent.name as projectname, parent.nextversionid, parent.ownerid as authormemberid, parent.gadgetrefid as dep_gadgetrefid, parent.initialvalue,
    parent.instanceid as dep_instanceid, parent.projecttypeid, parent.gadgetsize, parent.autosave,

    published.publishid as dep_publishid, published.publishid is not null as published,  published.accountid, published.versionmajor, published.versionminor, published.versiondot, published.versionbuildnumber,
    published.categoryid, published.name as publishedName, published.description, published.statusid, published.licensetypeid, published.price, published.currencytypeid,
    published.hideinstorelist, published.featured > 0 as featured, published.metadata as publishedmetadata, published.selfprice, published.inviteonly, published.guestlistid,
    published.retired > 0 as dep_publishedretired

 from parent
    left join published
        on published.projectid = parent.id
;


-- null constraints
--alter table projects alter column projectid set not null;

alter table projects alter column createddatetime set default now();
alter table projects alter column createddatetime set not null;

alter table projects alter column retired set default false;
alter table projects alter column retired set not null;

alter table projects alter column projectname set not null;
alter table projects alter column projecttypeid set not null;

update projects set autosave = false where autosave is null;
alter table projects alter column autosave set default false;
alter table projects alter column autosave set not null;

update projects set published = false where published is null;
alter table projects alter column published set default false;
alter table projects alter column published set not null;

update projects set accountid = accounts.accountid
	from accounts 
		inner join accountmemberroles on accountmemberroles.accountid = accounts.accountid
	where accountmemberroles.memberid = projects.authormemberid
		and projects.accountid is null;
	
alter table projects alter column accountid set not null;

alter table projects alter column authormemberid set not null;

update projects set inviteonly = false where inviteonly is null;
alter table projects alter column inviteonly set default true;
alter table projects alter column inviteonly set not null;

-- indexes
alter table projects add PRIMARY KEY (projectid);
create index "ix_projects_accountid" on projects (accountid);
create index "ix_projects_categoryid" on projects (categoryid);
create index "ix_projects_currencytypeid" on projects (currencytypeid);
create index "ix_projects_licensetypeid" on projects (licensetypeid);
create index "ix_projects_statusid" on projects (statusid);

create index "ix_projects_projecttypeid" on projects (projecttypeid);
create index "ix_projects_authormemberid" on projects (authormemberid);
create index "ix_projects_nextversionid" on projects (nextversionid);
create index "ix_projects_createddatetime" on projects (createddatetime);
create index "ix_projects_retired" on projects (retired);


-- foreign keys after indexes, because fk_nextversionid requires the uniqe key set up first
alter table projects add constraint "fk_projects_projecttypeid" FOREIGN KEY (projecttypeid) REFERENCES projecttypes(projecttypeid);
alter table projects add constraint "fk_projects_nextversionid" FOREIGN KEY (nextversionid) REFERENCES projects(projectid);
alter table projects add constraint "fk_projects_authormemberid" FOREIGN KEY (authormemberid) REFERENCES app_user(id);

alter table projects add constraint "fk_projects_accountid" FOREIGN KEY (accountid) REFERENCES accounts(accountid);
alter table projects add constraint "fk_projects_categoryid" FOREIGN KEY (categoryid) REFERENCES categories(categoryid);
alter table projects add constraint "fk_projects_currencytypeid" FOREIGN KEY (currencytypeid) REFERENCES currencytypes(currencytypeid);
alter table projects add constraint "fk_projects_licensetypeid" FOREIGN KEY (licensetypeid) REFERENCES licensetypes(licensetypeid);
alter table projects add constraint "fk_projects_statusid" FOREIGN KEY (statusid) REFERENCES publishstatuses(publishstatusid);
alter table projects add constraint "fk_projects_guestlistid" FOREIGN KEY (guestlistid) REFERENCES guestlists(guestlistid);


-- recreate foreign keys into "parent" table
alter table gadget_state rename column root_gadget_id to root_project_id;
alter TABLE "gadget_state" add CONSTRAINT "fk_gadgetstate_root_project_id" FOREIGN KEY (root_project_id) REFERENCES projects(projectid);

alter TABLE "parent_child" add CONSTRAINT "fk_parent_child_parentchildren_projectid" FOREIGN KEY (parent_children_id) REFERENCES projects(projectid);
alter TABLE "parent_child" add CONSTRAINT "fk_parent_child_child_projectid" FOREIGN KEY (child_id) REFERENCES projects(projectid);



--
-- create new foreign key columns for tables linking to "published"
--
-- be sure to fix history tables for ones whose source table we will be adding columns to:
--
--   accountprojectnumbers_autohistory
--   invoicelineitems_autohistory
--   paymentlineitems_autohistory
-- 


alter table accountprojectnumbers add column projectid varchar(255);
\i r60_to_r61/autohistory_repair_accountprojectnumbers.sql;
update accountprojectnumbers set projectid = projects.projectid
    from projects
    where projects.dep_publishid = accountprojectnumbers.publishid;

alter TABLE "accountprojectnumbers" add CONSTRAINT "fk_accountprojectnumbers_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);
alter table "accountprojectnumbers" alter column publishid drop not null;

alter table gadget_tag add column projectid varchar(255);
update gadget_tag set projectid = projects.projectid
    from projects
    where projects.dep_publishid = gadget_tag.publishedid;

alter TABLE "gadget_tag" add CONSTRAINT "fk_gadget_tags_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);


alter table invoicelineitems add column projectid varchar(255);
\i r60_to_r61/autohistory_repair_invoicelineitems.sql
update invoicelineitems set projectid = projects.projectid
    from projects
    where projects.dep_publishid = invoicelineitems.publishid;
 
alter TABLE "invoicelineitems" add CONSTRAINT "fk_invoicelineitems_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);


alter table paymentlineitems add column projectid varchar(255);
\i r60_to_r61/autohistory_repair_paymentlineitems.sql
update paymentlineitems set projectid = projects.projectid
    from projects
    where projects.dep_publishid = paymentlineitems.publishid;

alter TABLE "paymentlineitems" add CONSTRAINT "fk_paymentlineitems_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);


alter table publishedpurchasesacquisitions add column projectid varchar(255);
update publishedpurchasesacquisitions set projectid = projects.projectid
    from projects
    where projects.dep_publishid = publishedpurchasesacquisitions.publishid;

alter TABLE "publishedpurchasesacquisitions" add CONSTRAINT "fk_publishedpurchasesacquisitions_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);


alter table publishimages add column projectid varchar(255);
update publishimages set projectid = projects.projectid
    from projects
    where projects.dep_publishid = publishimages.publishid;

alter TABLE "publishimages" add CONSTRAINT "fk_publishimages_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);


alter table publishlaunchcounts add column projectid varchar(255);
update publishlaunchcounts set projectid = projects.projectid
    from projects
    where projects.dep_publishid = publishlaunchcounts.publishid;

alter TABLE "publishlaunchcounts" add CONSTRAINT "fk_publishlaunchcounts_parentprojectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);



alter table sharedprojects add column projectid varchar(255);
update sharedprojects set projectid = projects.projectid
    from projects
    where projects.dep_publishid = sharedprojects.publishid;

alter TABLE "sharedprojects" add CONSTRAINT "fk_sharedprojects_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);


alter table zappratingcaches add column projectid varchar(255);
update zappratingcaches set projectid = projects.projectid
    from projects
    where projects.dep_publishid = zappratingcaches.publishid;

alter TABLE "zappratingcaches" add CONSTRAINT "fk_zappratingcaches_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);


--alter table PublishLaunchCounts add column projectid varchar(255);
update PublishLaunchCounts set projectid = projects.projectid
    from projects
    where projects.dep_publishid = PublishLaunchCounts.publishid;
alter TABLE PublishLaunchCounts add CONSTRAINT "fk_publishlaunchcounts_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);

-- publishtypes is duplicate to projecttypes.  Make projecttypes canonical.
--drop table publishtypes;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;