-- Enclose in a transaction so the DB doesn't get created at all if anything fails.
--
-- You can run this script from the command line in a terminal window
-- and pipe stdout to your preferred viewing utility to check for ERROR.
--
-- Example:
-- psql boreal < all.sql 2>&1 | less
--
-- Create the structure of the database and insert initial default data.
--\i r0_to_r1.sql
--\i r1_to_r2.sql

-- NOTE that autohistory.sql has been moved to bottom of r1_to_r2

-- DO NOT ADD any more rX_to_rY scripts above autohistory.sql. AI has committed
-- r0 -> r2 changes to QA at this point. Going forwards, any tables with Autohistory
-- which are altered in subsequent rX_to_rY scripts will need to have the Autohistory
-- mechanism dropped and recreated and possibly rehydrated from the old. To save
-- and restore existing Autohistory data, drop the triggers and rename the Autohistory
-- table first. Then recreate Autohistory table and triggers with new table schema
-- and then restore old records to new.

--\i r2_to_r3.sql
--\i r3_to_r4.sql
--\i r4_to_r5.sql
--\i r5_to_r6.sql
--\i r6_to_r7.sql
--\i r7_to_r8.sql
--\i r8_to_r9.sql
--\i r9_to_r10.sql
--\i r10_to_r11.sql
--\i r11_to_r12.sql
--\i r12_to_r13.sql
--\i r13_to_r14.sql
\i r14_to_r15.sql
\i r15_to_r16.sql

-- PLEASE DO NOT ADD ADDITIONAL RxToRy's to this script, whether they exist
-- or not, until they are READY TO BE APPLIED TO PRODUCTION. Else they will
-- get applied BEFORE they are ready or even when they are still EMPTY!!!

-- ALWAYS load functions last.
\i all_functions.sql
