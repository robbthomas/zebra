-------------------------------------------------------------------------------
-- r38_to_r39.sql
--
-- updating plans and pricing information (creator)
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;


UPDATE accounttypes SET name = 'Creator', description = 'FREE for a 60 Day Trial, normally $8.99/month', price = 8.99 WHERE tag = 'creator';

-------------------------------------------------------------------------------
-- Update database version.
-------------------------------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES(39);

-------------------------------------------------------------------------------
-- End transaction wrapper for this update.
-------------------------------------------------------------------------------
COMMIT;
