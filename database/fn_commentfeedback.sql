--------------------------------------------------------------------------------
-- fn_commentfeedbacks.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- CommentFeedbacks functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='CommentFeedbacks'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('CommentFeedbacks','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_commentfeedback_create(text, text, integer, text, text)

-- DROP FUNCTION fn_commentfeedback_create(text, text, integer, text, text);

CREATE OR REPLACE FUNCTION fn_commentfeedback_create(in_credentials_email text, in_credentials_password text, in_commentId integer, in_memberId text, in_feedback text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CommentFeedbacks';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_commentFeedbackId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO CommentFeedbacks(
            commentId,
            memberId,
            feedback
        )
        VALUES(
            in_commentId,
            in_memberId,
            in_feedback
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_commentFeedbackId := currval('seq_CommentFeedbacks_commentFeedbackId');
        END IF;

        return new_commentFeedbackId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_commentfeedback_create(text, text, integer, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_commentfeedback_update(text, text, integer, integer, text, text, text)

-- DROP FUNCTION fn_commentfeedback_update(text, text, integer, integer, text, text, text);

CREATE OR REPLACE FUNCTION fn_commentfeedback_update(in_credentials_email text, in_credentials_password text, in_commentFeedbackId integer, in_commentId integer, in_memberId text, in_feedback text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CommentFeedbacks';
        var_functionName text := 'UPDATE';

        rec CommentFeedbacks%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from CommentFeedbacks where commentFeedbackId = in_commentFeedbackId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_commentId' THEN
                rec.commentId := in_commentId;
            ELSEIF param_name = 'in_memberId' THEN
                rec.memberId := in_memberId;
            ELSEIF param_name = 'in_feedback' THEN
                rec.feedback := in_feedback;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE CommentFeedbacks SET
            commentId = rec.commentId,
            memberId = rec.memberId,
            feedback = rec.feedback,
            updatedDateTime = now()
        WHERE commentFeedbackId = in_commentFeedbackId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_commentfeedback_update(text, text, integer, integer, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_commentfeedback_retire(text, text, integer, text)

-- DROP FUNCTION fn_commentfeedback_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_commentfeedback_retire(in_credentials_email text, in_credentials_password text, in_commentFeedbackId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CommentFeedbacks';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE CommentFeedbacks SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        WHERE commentFeedbackId = in_commentFeedbackId;

        GET DIAGNOSTICS var_count := Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_commentfeedback_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_commentfeedback_filter(text, text, integer, integer, text, integer, text, text, text)

-- DROP FUNCTION fn_commentfeedback_filter(text, text, integer, integer, text, integer, text, text, text);

CREATE OR REPLACE FUNCTION fn_commentfeedback_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_commentId integer, in_memberId text, in_feedback text, in_filter_list text)
  RETURNS SETOF CommentFeedbacks AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CommentFeedbacks';
        var_functionName text := 'FILTER';

        rec CommentFeedbacks%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM CommentFeedbacks WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_Id' THEN
            ELSEIF filter_name = 'in_commentId' THEN
                var_query := var_query || ' AND commentId = ' || in_commentId;
            ELSEIF filter_name = 'in_memberId' THEN
                var_query := var_query || ' AND memberId ILIKE ''%' || in_memberId || '%''';
            ELSEIF filter_name = 'in_feedback' THEN
                var_query := var_query || ' AND feedback ILIKE ''%' || in_feedback || '%''';
            ELSEIF filter_name = 'in_createdDateTime' THEN
            ELSEIF filter_name = 'in_updatedDateTime' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM CommentFeedbacks;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_commentfeedback_filter(text, text, integer, integer, text, integer, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_commentfeedback_find(text, text, integer)

-- DROP FUNCTION fn_commentfeedback_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_commentfeedback_find(in_credentials_email text, in_credentials_password text, in_commentFeedbackId integer)
  RETURNS SETOF CommentFeedbacks AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CommentFeedbacks';
        var_functionName text := 'FIND';

        rec CommentFeedbacks%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM CommentFeedbacks
                WHERE commentFeedbackId = in_commentFeedbackId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_commentfeedback_find(text, text, integer) OWNER TO postgres;
