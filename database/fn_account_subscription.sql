-- fn_account_subscription.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
---------------------------------------------------------------------
-- Function: fn_account_filter_term_due(text, text, integer, integer, text, integer, text, timestamp, integer, text, text, integer, text)

-- DROP FUNCTION fn_account_filter_term_due(text, text, integer, integer, text, integer, text, timestamp, integer, text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_account_filter_term_due(
    -- credentials
    in_credentials_email text, in_credentials_password text,
    -- range
    in_count integer, in_offset integer, in_order_list text,
    -- filter
    in_companyId integer, in_type text, in_termDueDateTime timestamp, in_goodStanding integer, in_name text, in_description text, in_tokenBalance integer, in_filter_list text)
  RETURNS SETOF Accounts AS
$BODY$
    DECLARE
        var_credentialsId text;
        rec Accounts%ROWTYPE;
        filter_name text;
        var_typeId int;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM Accounts WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

      IF var_credentialsId IS NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_accountId' THEN
            ELSEIF filter_name = 'in_companyId' THEN
                var_query := var_query || ' AND companyId = ' || in_companyId;
            ELSEIF filter_name = 'in_type' THEN
                var_typeId := accountTypeId FROM AccountTypes WHERE tag ILIKE in_type;
                var_query := var_query || ' AND typeId = ' || var_typeId;
            ELSEIF filter_name = 'in_termDueDateTime' THEN
                var_query := var_query || ' AND termDueDateTime < ''' || in_termDueDateTime || '''';
            ELSEIF filter_name = 'in_goodStanding' THEN
                var_query := var_query || ' AND goodStanding = ' || in_goodStanding;
            ELSEIF filter_name = 'in_name' THEN
                var_query := var_query || ' AND name ILIKE ''%' || in_name || '%''';
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_tokenBalance' THEN
                var_query := var_query || ' AND tokenBalance = ' || in_tokenBalance;
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            ELSEIF filter_name = 'in_retired' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- include only paying accounts
        var_query := var_query || ' AND typeId IN (SELECT accountTypeId FROM AccountTypes WHERE price > 0)';

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM Accounts;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

      END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_account_filter_term_due(text, text, integer, integer, text, integer, text, timestamp, integer, text, text, integer, text) OWNER TO postgres;



CREATE OR REPLACE FUNCTION fn_accounts_demote_charges_declined(in_credentials_email text, in_credentials_password text)
  RETURNS SETOF Accounts AS
$BODY$
    DECLARE
        var_credentialsId text;
        rec Accounts%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

      IF var_credentialsId IS NULL THEN
         -- hollowed out. The functionality has been moved from cron3 to cron2
      END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_accounts_demote_charges_declined(text, text) OWNER TO postgres;


