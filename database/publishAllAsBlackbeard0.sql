insert into published(accountId,projectId,categoryId,name,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,editedById)
select 1, pa.id, 6005, pa.name, 2, pa.projectTypeId, 1, 99, 1, '8a8181da2e0186f4012e018709010001'
from parent pa, app_user au
where pa.ownerid = au.id
and displayName not ilike '%test%'
and deleted = false
and projecttypeid in (1,2) 
and length(name) > 10
and length(name) < 30
and nextversionid is null
;

