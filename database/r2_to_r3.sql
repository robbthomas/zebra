-------------------------------------------------------------------------------
-- r2_to_r3.sql
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (3);
-------------------------------------------------------------------------------


-- Add AccountProjectNumberId column, prime any existing rows, and then add
-- primary key and default value constraints.

ALTER TABLE AccountProjectNumbers ADD COLUMN AccountProjectNumberId varchar(255);

CREATE OR REPLACE FUNCTION tmpfn_prime_AccountProjectNumberIds()
RETURNS integer AS
$BODY$
    DECLARE
        cnt int := 0;
        tries int;
        new_id text;
        rec AccountProjectNumbers%ROWTYPE;

    BEGIN
        FOR rec IN SELECT * FROM AccountProjectNumbers LOOP
            tries := 10;
            LOOP
                new_id := replace(cast(uuid_generate_v4() As text),'-','');
                IF NOT EXISTS (select AccountProjectNumberId from AccountProjectNumbers_User where AccountProjectNumberId = new_id) THEN
                    EXIT;
                END IF;
                tries := tries - 1;
                IF tries <= 0 THEN
                    RETURN -1;
                END IF;
            END LOOP;
            cnt := cnt + 1;
        END LOOP;

        RETURN cnt;
    END;
$BODY$
LANGUAGE 'plpgsql';

SELECT * FROM tmpfn_prime_AccountProjectNumberIds();

DROP FUNCTION tmpfn_prime_AccountProjectNumberIds();

ALTER TABLE AccountProjectNumbers ALTER COLUMN AccountProjectNumberId SET NOT NULL;

ALTER TABLE AccountProjectNumbers ALTER COLUMN AccountProjectNumberId SET DEFAULT replace(cast(uuid_generate_v4() As text),'-','');

ALTER TABLE AccountProjectNumbers ADD CONSTRAINT pkey_AccountProjectNumbers_AccountProjectNumberId
    PRIMARY KEY (AccountProjectNumberId);


-- Recreate autohistory for AccountProjectNumbers. DO NOT save and restore any of the data in old table this time.
DROP TRIGGER IF EXISTS trg_AccountProjectNumbers_Update ON AccountProjectNumbers_AutoHistory CASCADE;
DROP SEQUENCE IF EXISTS seq_AccountProjectNumbers_AutoHistory CASCADE;
DROP TABLE IF EXISTS AccountProjectNumbers_AutoHistory CASCADE;

CREATE SEQUENCE seq_AccountProjectNumbers_AutoHistory;
CREATE TABLE AccountProjectNumbers_AutoHistory (
    LIKE AccountProjectNumbers,
    AccountProjectNumbers_AutoHistory_Id bigint DEFAULT nextval('seq_AccountProjectNumbers_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

/*
CREATE OR REPLACE FUNCTION fn_trg_AccountProjectNumbers_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO AccountProjectNumbers_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

CREATE TRIGGER trg_AccountProjectNumbers_Update
    AFTER UPDATE ON AccountProjectNumbers
    FOR EACH ROW EXECUTE PROCEDURE fn_trg_AccountProjectNumbers_Update();
*/



-- Revise LicenseTypes table per Gadget Zapp Publishing Commerce 1.0 spec.
-- Assume existing license types are not currently used anywhere.

ALTER TABLE LicenseTypes ADD COLUMN republishable boolean;
ALTER TABLE LicenseTypes ADD COLUMN editable boolean;
ALTER TABLE LicenseTypes ADD COLUMN embedable boolean;
ALTER TABLE LicenseTypes ADD COLUMN projectTypeId integer; -- 1=Gadget, 2=Zapp

UPDATE LicenseTypes
SET tag='license_type_zapp_minimal',name='Minimal Zapp',republishable=false,editable=false,embedable=false,projectTypeId=2 -- none-z
WHERE licenseTypeId = 1;

UPDATE LicenseTypes
SET tag='license_type_gadget_minimal',name='Minimal Gadget',republishable=false,editable=false,embedable=false,projectTypeId=1 -- none-g
WHERE licenseTypeId = 2;

UPDATE LicenseTypes
SET tag='license_type_gadget_republish_only',name='Republish Only Gadget',republishable=true,editable=false,embedable=false,projectTypeId=1 -- repub-only
WHERE licenseTypeId = 3;

UPDATE LicenseTypes
SET tag='license_type_gadget_republish_edit',name='Republish and Edit Gadget',republishable=true,editable=true,embedable=false,projectTypeId=1 -- repub-edit
WHERE licenseTypeId = 4;

INSERT INTO LicenseTypes(licenseTypeId,displayOrder,tag,name,republishable,editable,embedable,projectTypeId)
VALUES (5,50,'license_type_gadget_edit_only','Edit Only Gadget',false,true,false,1); -- edit-only

INSERT INTO LicenseTypes(licenseTypeId,displayOrder,tag,name,republishable,editable,embedable,projectTypeId)
VALUES (6,60,'license_type_zapp_embed_only','Embeddable Zapp',false,false,true,2); -- embed-only


ALTER TABLE LicenseTypes ALTER COLUMN republishable SET NOT NULL;
ALTER TABLE LicenseTypes ALTER COLUMN editable SET NOT NULL;
ALTER TABLE LicenseTypes ALTER COLUMN embedable SET NOT NULL;
ALTER TABLE LicenseTypes ALTER COLUMN projectTypeId SET NOT NULL;


-- Do some minor house keeping.
UPDATE PublishTypes SET tag = 'publish_type_zapp', name = 'Zapp' WHERE tag = 'publish_type_app';


-- Add ROLE_TYPE_ANONYMOUS so that ALL db function access MUST match a permission.
INSERT INTO RoleTypes(roleTypeId,tag,name,displayOrder) VALUES (10000,'ROLE_TYPE_ANONYMOUS','Anonymous', 10000);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
