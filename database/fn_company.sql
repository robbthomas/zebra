--------------------------------------------------------------------------------
-- fn_companys.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- Companies functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Companies' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('Companies','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Companies','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Companies','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Companies','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Companies','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Companies','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Companies','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Companies','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Companies','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Companies','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Companies','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Companies','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Companies','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('Companies','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Companies','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Companies','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Companies','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_company_create(text, text, text, text, text, text)

-- DROP FUNCTION fn_company_create(text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_company_create(in_credentials_email text, in_credentials_password text, in_name text, in_description text, in_companyUrl text, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Companies';
        var_functionName text := 'CREATE';

        new_companyId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into Companies(
            name,
            description,
            companyUrl,
            editedById
        )
        values(
            in_name,
            in_description,
            in_companyUrl,
            in_editedById
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_companyId := currval('seq_Companies_companyId');
        END IF;

        return new_companyId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_company_create(text, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_company_update(text, text, integer, text, text, text, text, text)

-- DROP FUNCTION fn_company_update(text, text, integer, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_company_update(in_credentials_email text, in_credentials_password text, in_companyId integer, in_name text, in_description text, in_companyUrl text, in_editedById text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Companies';
        var_functionName text := 'UPDATE';

        rec Companies%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from Companies where companyId = in_companyId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_name' THEN
                rec.name := in_name;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_companyUrl' THEN
                rec.companyUrl := in_companyUrl;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE Companies SET
            name = rec.name,
            description = rec.description,
            companyUrl = rec.companyUrl,
            editedById = rec.editedById
        WHERE companyId = in_companyId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_company_update(text, text, integer, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_company_retire(text, text, integer, text)

-- DROP FUNCTION fn_company_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_company_retire(in_credentials_email text, in_credentials_password text, in_companyId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Companies';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE Companies SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE companyId = in_companyId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_company_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_company_filter(text, text, integer, integer, text, text, text, text, text)

-- DROP FUNCTION fn_company_filter(text, text, integer, integer, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_company_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_name text, in_description text, in_companyUrl text, in_filter_list text)
  RETURNS SETOF Companies AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Companies';
        var_functionName text := 'FILTER';

        rec Companies%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM Companies WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_companyId' THEN
            ELSEIF filter_name = 'in_name' THEN
                var_query := var_query || ' AND name ILIKE ''%' || in_name || '%''';
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_companyUrl' THEN
                var_query := var_query || ' AND companyUrl ILIKE ''%' || in_companyUrl || '%''';
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM Companies;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_company_filter(text, text, integer, integer, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_company_find(text, text, integer)

-- DROP FUNCTION fn_company_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_company_find(in_credentials_email text, in_credentials_password text, in_companyId integer)
  RETURNS SETOF Companies AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Companies';
        var_functionName text := 'FIND';

        rec Companies%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM Companies
                WHERE companyId = in_companyId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_company_find(text, text, integer) OWNER TO postgres;
