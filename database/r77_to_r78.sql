-------------------------------------------------------------------------------
-- r77_to_r78.sql
--
-- Adjusting plans and pricing information
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

UPDATE accounttypes SET description = 'FREE', price = 0 WHERE accounttypeid = 1;
UPDATE accounttypes SET description = '$8.95 per Month', price = 895 WHERE accounttypeid = 2;
UPDATE accounttypes SET description = '$39.95 per Month', price = 3995 WHERE accounttypeid = 3;

-------------------------------------------------------------------------------
-- Update database version.
-------------------------------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES(78);

-------------------------------------------------------------------------------
-- End transaction wrapper for this update.
-------------------------------------------------------------------------------
COMMIT;
