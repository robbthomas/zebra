--------------------------------------------------------------------------------
-- autohistory.sql
--
-- Allen Interactoins Copyright 2011
--
-- Create auto-history mechanism for select tables for which we wish
-- to preserve a complete history of all changes. This mechanism works
-- in the background independent of other database functions and changes
-- freeing the developers to add, update, and even delete database table
-- rows as they see fit to implement functionality without having to
-- worry about losing data or extra overhead and complexity of updating
-- histories.
--
-- Implementation Notes:
--
-- 1. Name the history tables <parenttable>_AutoHistory where <parenttable>
--    is the table for which you are creating the history table and
--    update trigger.
--
-- 2. Declare the history table columns to _begin_ with the identical
--    columns from the parent table. See refs below for more information.
--
--    PostgreSQL 8.1.9 Documentation, INSERT
--    http://www.postgresql.org/docs/8.1/static/sql-insert.html
--
--    The target column names may be listed in any order. If no list 
--    of column names is given at all, the default is all the columns 
--    of the table in their declared order; or the first N column names, 
--    if there are only N columns supplied by the VALUES clause or query. 
--    The values supplied by the VALUES clause or query are associated with 
--    the explicit or implicit column list left-to-right.
--
--------------------------------------------------------------------------------
-- FinancialTransactions history table and UPDATE trigger to move over
-- and save copy of rows before modification.
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS FinancialTransactions_AutoHistory CASCADE;

CREATE SEQUENCE seq_FinancialTransactions_AutoHistory;
CREATE TABLE FinancialTransactions_AutoHistory (
    LIKE FinancialTransactions,
    FinancialTransactions_AutoHistory_Id bigint DEFAULT nextval('seq_FinancialTransactions_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_FinancialTransactions_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO FinancialTransactions_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

CREATE TRIGGER trg_FinancialTransactions_Update
    AFTER UPDATE ON FinancialTransactions
    FOR EACH ROW EXECUTE PROCEDURE fn_trg_FinancialTransactions_Update();


--------------------------------------------------------------------------------
-- Invoices history table and UPDATE trigger to move over
-- and save copy of rows before modification.
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS Invoices_AutoHistory CASCADE;

CREATE SEQUENCE seq_Invoices_AutoHistory;
CREATE TABLE Invoices_AutoHistory (
    LIKE Invoices,
    Invoices_AutoHistory_Id bigint DEFAULT nextval('seq_Invoices_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_Invoices_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO Invoices_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$ 
Language 'plpgsql';

CREATE TRIGGER trg_Invoices_Update
    AFTER UPDATE ON Invoices
    FOR EACH ROW EXECUTE PROCEDURE fn_trg_Invoices_Update();


--------------------------------------------------------------------------------
-- InvoiceLineItems history table and UPDATE trigger to move over
-- and save copy of rows before modification.
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS InvoiceLineItems_AutoHistory CASCADE;

CREATE SEQUENCE seq_InvoiceLineItems_AutoHistory;
CREATE TABLE InvoiceLineItems_AutoHistory (
    LIKE InvoiceLineItems,
    InvoiceLineItems_AutoHistory_Id bigint DEFAULT nextval('seq_InvoiceLineItems_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_InvoiceLineItems_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO InvoiceLineItems_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

CREATE TRIGGER trg_InvoiceLineItems_Update
    AFTER UPDATE ON InvoiceLineItems
    FOR EACH ROW EXECUTE PROCEDURE fn_trg_InvoiceLineItems_Update();


--------------------------------------------------------------------------------
-- Payments history table and UPDATE trigger to move over
-- and save copy of rows before modification.
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS Payments_AutoHistory CASCADE;

CREATE SEQUENCE seq_Payments_AutoHistory;
CREATE TABLE Payments_AutoHistory (
    LIKE Payments,
    Payments_AutoHistory_Id bigint DEFAULT nextval('seq_Payments_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_Payments_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO Payments_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

CREATE TRIGGER trg_Payments_Update
    AFTER UPDATE ON Payments
    FOR EACH ROW EXECUTE PROCEDURE fn_trg_Payments_Update();


--------------------------------------------------------------------------------
-- PaymentLineItems history table and UPDATE trigger to move over
-- and save copy of rows before modification.
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS PaymentLineItems_AutoHistory CASCADE;

CREATE SEQUENCE seq_PaymentLineItems_AutoHistory;
CREATE TABLE PaymentLineItems_AutoHistory (
    LIKE PaymentLineItems,
    PaymentLineItems_AutoHistory_Id bigint DEFAULT nextval('seq_PaymentLineItems_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_PaymentLineItems_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO PaymentLineItems_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

CREATE TRIGGER trg_PaymentLineItems_Update
    AFTER UPDATE ON PaymentLineItems
    FOR EACH ROW EXECUTE PROCEDURE fn_trg_PaymentLineItems_Update();


--------------------------------------------------------------------------------
-- AccountProjectNumbers history table and UPDATE trigger to move over
-- and save copy of rows before modification.
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS AccountProjectNumbers_AutoHistory CASCADE;

CREATE SEQUENCE seq_AccountProjectNumbers_AutoHistory;
CREATE TABLE AccountProjectNumbers_AutoHistory (
    LIKE AccountProjectNumbers,
    AccountProjectNumbers_AutoHistory_Id bigint DEFAULT nextval('seq_AccountProjectNumbers_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_AccountProjectNumbers_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO AccountProjectNumbers_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

CREATE TRIGGER trg_AccountProjectNumbers_Update
    AFTER UPDATE ON AccountProjectNumbers
    FOR EACH ROW EXECUTE PROCEDURE fn_trg_AccountProjectNumbers_Update();


--------------------------------------------------------------------------------
-- Published history table and UPDATE trigger to move over
-- and save copy of rows before modification.
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS Published_AutoHistory CASCADE;

CREATE SEQUENCE seq_Published_AutoHistory;
CREATE TABLE Published_AutoHistory (
    LIKE Published,
    Published_AutoHistory_Id bigint DEFAULT nextval('seq_Published_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_Published_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO Published_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

CREATE TRIGGER trg_Published_Update
    AFTER UPDATE ON Published
    FOR EACH ROW EXECUTE PROCEDURE fn_trg_Published_Update();


