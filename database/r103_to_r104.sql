-------------------------------------------------------------------------------
-- r103_to_r104.sql
--
-- Author:  David Hoyt
-- Notes:  Add createddatetime to AccountProjectNumbers, back-populate
--         from Invoices and (where that doesn't exist) Projects.
--
-------------------------------------------------------------------------------

create table AccountProjectNumbers_AutoHistory_bak (
    accountId               bigint,
    publishId               bigint,
    invoicelineitemid       bigint,
    retired                 int,
    retiredbyid             text,
    retireddatetime         timestamp,
    retiredreasonid         int,
    accountprojectnumberid  varchar(255),
    projectId               varchar(255),
    createddatetime         timestamp,
    accountprojectnumbers_autohistory_id bigint primary key not null default nextval('seq_accountprojectnumbers_autohistory'),
    autohistorydate         timestamp
);


insert into AccountprojectNumbers_AutoHistory_bak (
    accountid,
    publishId,
    invoiceLineItemId,
    retired,
    retiredById,
    retiredDateTime,
    retiredReasonId,
    accountprojectnumberId,
    projectId,
    createdDateTime,
    accountprojectnumbers_autohistory_id,
    autohistorydate
)

    select     accountid,
    publishId,
    invoiceLineItemId,
    retired,
    retiredById,
    retiredDateTime,
    retiredReasonId,
    accountprojectnumberId,
    projectId,
    null,
    accountprojectnumbers_autohistory_id,
    autohistorydate
    from accountprojectnumbers_autohistory
;

drop table accountprojectnumbers_autohistory;
alter table accountprojectnumbers_autohistory_bak
    rename to accountprojectnumbers_autohistory;

alter table accountprojectnumbers
    add column createdDateTime timestamp;

update accountprojectnumbers
    set createddatetime = invoices.createddatetime
     from invoicelineitems inner join invoices
        on invoices.invoiceid = invoicelineitems.invoiceid
where
       invoicelineitems.invoicelineitemid = accountprojectnumbers.invoicelineitemid
;


update accountprojectnumbers
    set createddatetime = projects.createddatetime
     from projects
where
       projects.projectid = accountprojectnumbers.projectid
       and accountprojectnumbers.createddatetime is null
;

create index ix_accountprojectnumbers_createddatetime on accountprojectnumbers(createddatetime);

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (104);
------------------------------------------------------
