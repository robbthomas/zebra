#!/bin/bash

DB_NAME="boreal"

# check for a different database name
if [ "$1" != "" ]; then
    DB_NAME="$1"
fi

# create a new one as of r0
createdb ${DB_NAME}
