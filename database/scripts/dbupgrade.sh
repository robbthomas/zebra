#!/bin/bash

DB_NAME="boreal"
SQLFILE="/tmp/r0_all.sql"

# check for a different database name
if [ "$1" != "" ]; then
    DB_NAME="$1"
fi

#
# find out the current version of the database
#
DBV=`psql ${DB_NAME} -t -c "SELECT MAX(versionNumber) FROM DatabaseVersion;"`
#echo DBV=\"${DBV}\"

# use Arithmetic Evaluation guarantee a numeric
# value, without any leading or trailing spaces.
DBV=$[ ${DBV} ]
#echo DBV=${DBV}

#
# calculate the next version, one step up from the current version.
#
DBNEXT=$[ ${DBV} + 1 ]
#echo DBNEXT=${DBNEXT}

#
# create the filename of the first upgrade file. They are always in the form of rX_to_rY.sql.
#
FNAME=r${DBV}_to_r${DBNEXT}.sql

#
# clear the sql file
#
rm -f $SQLFILE

#
# process each file in ascending numerical order until no more files are found
#
while [ -e $FNAME ]
do
    #
    # process the file.
    #
    #echo FNAME=$FNAME
    echo "\\i $FNAME" >> $SQLFILE

    #
    # go on to the next one.
    #
    DBV=$DBNEXT
    DBNEXT=$[ ${DBNEXT} + 1 ]
    FNAME=r${DBV}_to_r${DBNEXT}.sql
done

#
# apply all the latest functions to the database
#
echo "\\i all_functions.sql" >> $SQLFILE

#
# execute all the new upgrade SQL in a single transaction
#
psql ${DB_NAME} -f $SQLFILE 2>&1

#
# find out the current version of the database
#
DBUPV=`psql ${DB_NAME} -t -c "SELECT MAX(versionNumber) FROM DatabaseVersion;"`
#echo DBUPV=\"${DBUPV}\"
DBUPV=$[ ${DBUPV} ]
#echo DBUPV=${DBUPV}

#
# similar to above, databases are always built to
# include the upgrades to version 1, so an empty
# version number (or zero) is equivalent to version 1.
#
if [ ${DBUPV} = 0 ]
then
    DBUPV=1
fi
#echo DBUPV=${DBUPV}

if [ ${DBUPV} = ${DBV} ]
then
    #
    # all done.
    #
    echo "The database is at version ${DBV}."
else
    # 
    # didn't work.
    #
    echo "ERROR: Unable to upgrade database to version ${DBV}. Remaining at version ${DBUPV}."
fi
