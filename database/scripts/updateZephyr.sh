echo ~/src/zephyr
cd ~/src/zephyr

echo svn checkout https://dev.alleni.com/svn/dev/zephyr/trunk
svn checkout https://dev.alleni.com/svn/dev/zephyr/trunk

echo cd ~/src/zephyr/trunk/build
cd ~/src/zephyr/trunk/build

echo ant all -Ddeploy.environment=qpistaging
ant all -Ddeploy.environment=qpistaging

echo cd /var/lib/tomcat6/webapps
cd /var/lib/tomcat6/webapps

echo sudo cp ~/src/zephyr/trunk/build/build/dist/zephyr.war
sudo cp ~/src/zephyr/trunk/build/build/dist/zephyr.war .

echo sudo service tomcat6 restart
sudo service tomcat6 restart

echo ALL DONE!!