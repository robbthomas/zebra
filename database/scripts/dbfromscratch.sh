#!/bin/bash
#
# Run this script from the parent folder, so we have access to all the *.sql files.
# E.g. ./scripts/dbfromscratch.sh
#

DB_NAME="boreal"

# check for a different database name
if [ "$1" != "" ]; then
    DB_NAME="$1"
fi

# remove the old database
dropdb ${DB_NAME}

# create the new one with initial values.
./scripts/dbbasic.sh

# upgrade it to the current version
./scripts/dbupgrade.sh
