drop function fn_Project_List_small (
      in_projectTypeId     INT4
    , in_isPublished       BOOLEAN
    , in_limit             INT4
    , in_offset            INT4
    , in_searcherAccountId INT8
    , in_searcherMemberId  TEXT
    , in_categoryId        BIGINT
    , in_orderBy           TEXT
    , in_genericSearch     TEXT
);

drop function fn_Project_List2 (
      in_projectTypeId  INT4
    , in_featured BOOLEAN
    , in_limit INT4
    , in_offset INT4
    , in_searcherAccountId INT8
    , in_orderBy VARCHAR
    , in_categoryId INT4
    , in_authorMemberId TEXT
    , in_searchText TEXT
    , in_genericSearch TEXT
    , in_ownerAccountId INT4
    , in_showContent BOOLEAN
    , in_isPublished BOOLEAN
    , in_hideInStoreList BOOLEAN
    , in_parentProjectId VARCHAR(255)
    , in_isForLatest BOOLEAN
    , in_searcherMemberId TEXT
    , in_projectName VARCHAR(255)
) ;

drop function fn_Project_List_Small_Count (
      in_projectTypeId     INT4
    , in_isPublished       BOOLEAN
    , in_limit             INT4
    , in_searcherAccountId INT8
    , in_searcherMemberId  TEXT
    , in_categoryId        BIGINT
    , in_genericSearch     TEXT
);

drop function joefoo(projecttypeid integer, projectlimit integer, projectoffset integer, accountid integer, featured boolean);
