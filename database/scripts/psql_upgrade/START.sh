#!/bin/sh 
BIN_9_2=/usr/lib/postgresql/9.2/bin/
BIN_8_4=/usr/lib/postgresql/8.4/bin/

DATA_9_2=/mnt/pgsql/data/main-9.2
DATA_8_4=/mnt/pgsql/data/main-8.4

PORT_9_2=5432
PORT_8_4=5433

echo "***********************************************"
echo "*                                             *"
echo "* Zebra(tm) Postgres 8.4 - 9.2 Migration Tool *"
echo "*                                             *"
echo "***********************************************"
echo ""
echo "Press ENTER to begin"
read A

echo "------------------------------------------------"
echo "NEXT: installing postgres 9.2"
echo "Press ENTER to continue"
read A
add-apt-repository ppa:pitti/postgresql
apt-get update
apt-get install postgresql-9.2 postgresql-contrib-9.2

echo "------------------------------------------------"
echo "NEXT: deleting fn_project_*"
echo "Press ENTER to continue"
read A
sudo -u postgres psql -d boreal < drop_fn_projects.sql

echo "------------------------------------------------"
echo "NEXT: stopping database servers"
echo "Press ENTER to continue"
read A
/etc/init.d/postgresql stop

echo "---------------------------------------------------"
echo "NEXT: moving 8.2 data to new (versioned) directory"
echo "  ${DATA_8_4}"
echo "Press ENTER to continue"
read A
mv /mnt/pgsql/data/main ${DATA_8_4}

echo "------------------------------------------------"
echo "NEXT: initializing base db for postgresql 9.2"
echo "Press ENTER to continue"
read A
sudo -u postgres ${BIN_9_2}/initdb -D ${DATA_9_2}

echo "------------------------------------------------"
echo "NEXT: copying config files"
echo "Press ENTER to continue"
read A
cp pg_hba.conf-9.2 ${DATA_9_2}/pg_hba.conf
cp postgresql.conf-9.2 ${DATA_9_2}/postgresql.conf

cp pg_hba.conf-8.4 ${DATA_8_4}/pg_hba.conf
cp postgresql.conf-8.4 ${DATA_8_4}/postgresql.conf

echo "------------------------------------------------"
echo "NEXT: fixing symbolic links"
echo "Press ENTER to continue"
read A
mv /var/run/postgresql /var/run/postgresql_tmp
mkdir /tmp/postgresql_run
chown postgres /tmp/postgresql_run
ln -s /tmp/postgresql_run /var/run/postgresql

ln -s /usr/bin/pg_config ${BIN_9_2}/pg_config

echo "*-----------------------------------------------*"
echo "* NEXT: BEGIN DATA MIGRATION                    *"
echo "*     Logs in ./log                             *"
echo "*-----------------------------------------------*"
echo "Press ENTER to continue"
read A
mkdir -p log
chmod ugo+rwx log
rm log/*
cd log
UPGRADE_CMD="sudo -u postgres ${BIN_9_2}/pg_upgrade -b ${BIN_8_4} -B ${BIN_9_2} -d ${DATA_8_4} -D ${DATA_9_2} -p ${PORT_8_4} -P ${PORT_9_2}"
echo $UPGRADE_CMD

${UPGRADE_CMD}


echo "*-----------------------------------------------*"
echo "* NEXT: Cleaning Up                             *"
echo "*-----------------------------------------------*"
echo "Press ENTER to continue"
read A
echo "removing tmp config files in new data directories"
rm ${DATA_9_2}/postgresql.conf
rm ${DATA_9_2}/pg_hba.conf
rm ${DATA_8_4}/postgresql.conf
rm ${DATA_8_4}/pg_hba.conf

echo "copy new config files into postgres conf directories"
cd ..
cp postgresql.conf-9.2_complete /etc/postgresql/9.2/main/postgresql.conf
cp postgresql.conf-8.4_complete /etc/postgresql/8.4/main/postgresql.conf

echo "*-----------------------------------------------*"
echo "* NEXT: Restarting Postgresq and Tomcat6        *"
echo "*     Logs in ./log                             *"
echo "*-----------------------------------------------*"
echo "Press ENTER to continue"
read A
sudo /etc/init.d/postgresql restart
sudo /etc/init.d/tomcat6 restart

echo "***********************************************"
echo "*                                             *"
echo "* DATABASE UPGRADE COMPLETE!                  *"
echo "*                                             *"
echo "***********************************************"
