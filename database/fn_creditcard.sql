--------------------------------------------------------------------------------
-- fn_creditcards.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- CreditCards functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='CreditCards' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('CreditCards','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CreditCards','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CreditCards','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CreditCards','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('CreditCards','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('CreditCards','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_creditcard_create(text, text, integer, integer, text, text, text, text, text, text, text)

-- DROP FUNCTION fn_creditcard_create(text, text, integer, integer, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_creditcard_create(in_credentials_email text, in_credentials_password text, in_accountId integer, in_addressId integer, in_nameOnCreditCard text, in_maskedNumber text, in_expirationMonth text, in_expirationYear text, in_customerProfileId text, in_customerPaymentProfileId text, in_firstName text, in_lastName text, in_email text, in_type text, in_expirationNoticeSent int4, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CreditCards';
        var_functionName text := 'CREATE';

        var_editedById text;
        new_creditCardId int := -1;
        var_count int := -1;

        var_creditCardType int;

    BEGIN
        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = in_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- set editing user
        var_editedById := COALESCE(in_editedById,'');
        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        var_creditCardType := CreditCardTypeId FROM CreditCardTypes WHERE tag = in_type;

        INSERT INTO CreditCards(
            accountId,
            addressId,
            nameOnCreditCard,
            maskedNumber,
            expirationMonth,
            expirationYear,
            customerProfileId,
            customerPaymentProfileId,
            firstName,
            lastName,
            email,
            creditCardType,
            expirationNoticeSent,
            editedById
        )
        VALUES(
            in_accountId,
            in_addressId,
            in_nameOnCreditCard,
            in_maskedNumber,
            in_expirationMonth,
            in_expirationYear,
            in_customerProfileId,
            in_customerPaymentProfileId,
            in_firstName,
            in_lastName,
            in_email,
            var_creditCardType,
            in_expirationNoticeSent,
            var_editedById
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_creditCardId := currval('seq_CreditCards_creditCardId');
        END IF;

        RETURN new_creditCardId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_creditcard_create(text, text, integer, integer, text, text, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_creditcard_update(text, text, integer, integer, integer, text, text, text, text, text, text, text, text)

-- DROP FUNCTION fn_creditcard_update(text, text, integer, integer, integer, text, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_creditcard_update(
    in_credentials_email text
    , in_credentials_password text
    , in_creditCardId integer
    , in_accountId integer
    , in_addressId integer
    , in_nameOnCreditCard text
    , in_maskedNumber text
    , in_expirationMonth text
    , in_expirationYear text
    , in_customerProfileId text
    , in_customerPaymentProfileId text
    , in_firstName text
    , in_lastName text
    , in_email text
    , in_type text
    , in_expirationNoticeSent int4
    , in_editedById text
    , in_param_list text)
    -- 18
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CreditCards';
        var_functionName text := 'UPDATE';

        var_creditCardType int;
        var_accountId bigint;

        rec CreditCards%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        --RAISE NOTICE 'in_creditCardId=%',in_creditCardId;

        var_accountId := COALESCE(in_accountId,0);
        IF var_accountId = 0 THEN
            var_accountId := accountId FROM CreditCards WHERE creditCardId = in_creditCardId;
        END IF;

        --RAISE NOTICE 'var_accountId=%',var_accountId;

        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = var_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        --RAISE NOTICE 'var_credentialsId=%',var_credentialsId;

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from CreditCards where creditCardId = in_creditCardId;

        var_creditCardType := CreditCardTypeId FROM CreditCardTypes WHERE tag = in_type;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_accountId' THEN
                rec.accountId := in_accountId;
            ELSEIF param_name = 'in_addressId' THEN
                rec.addressId := in_addressId;
            ELSEIF param_name = 'in_nameOnCreditCard' THEN
                rec.nameOnCreditCard := in_nameOnCreditCard;
            ELSEIF param_name = 'in_maskedNumber' THEN
                rec.maskedNumber := in_maskedNumber;
            ELSEIF param_name = 'in_expirationMonth' THEN
                rec.expirationMonth := in_expirationMonth;
            ELSEIF param_name = 'in_expirationYear' THEN
                rec.expirationYear := in_expirationYear;
            ELSEIF param_name = 'in_customerProfileId' THEN
                rec.customerProfileId := in_customerProfileId;
            ELSEIF param_name = 'in_customerPaymentProfileId' THEN
                rec.customerPaymentProfileId := in_customerPaymentProfileId;
            ELSEIF param_name = 'in_firstName' THEN
                rec.firstName := in_firstName;
            ELSEIF param_name = 'in_lastName' THEN
                rec.lastName := in_lastName;
            ELSEIF param_name = 'in_email' THEN
                rec.email := in_email;
            ELSEIF param_name = 'in_type' THEN
                rec.creditCardType := var_creditCardType;
            ELSEIF param_name = 'in_expirationNoticeSent' THEN
                rec.expirationNoticeSent := in_expirationNoticeSent;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE CreditCards SET
            accountId = rec.accountId,
            addressId = rec.addressId,
            nameOnCreditCard = rec.nameOnCreditCard,
            maskedNumber = rec.maskedNumber,
            expirationMonth = rec.expirationMonth,
            expirationYear = rec.expirationYear,
            customerProfileId = rec.customerProfileId,
            customerPaymentProfileId = rec.customerPaymentProfileId,
            firstName = rec.firstName,
            lastName = rec.lastName,
            email = rec.email,
            creditcardtype = rec.creditcardtype,
            expirationNoticeSent = rec.expirationNoticeSent,
            editedById = rec.editedById,
            editedDateTime = now()
        WHERE creditCardId = in_creditCardId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_creditcard_update(text, text, integer, integer, integer, text, text, text, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_creditcard_retire(text, text, integer, text)

-- DROP FUNCTION fn_creditcard_retire(text, text, integer);

CREATE OR REPLACE FUNCTION fn_creditcard_retire(in_credentials_email text, in_credentials_password text, in_creditCardId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CreditCards';
        var_functionName text := 'RETIRE';

        var_accountId bigint;
        var_retiredById text;
        var_count int := -1;

    BEGIN
        var_accountId := accountId FROM CreditCards WHERE creditCardId = in_creditCardId;

        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = var_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE CreditCards SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        WHERE creditCardId = in_creditCardId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_creditcard_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_creditcard_filter(text, text, integer, integer, text, integer, integer, text, text, text, text, text, text, text)

-- DROP FUNCTION fn_creditcard_filter(text, text, integer, integer, text, integer, integer, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_creditcard_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_accountId integer, in_addressId integer, in_nameOnCreditCard text, in_maskedNumber text, in_expirationMonth text, in_expirationYear text, in_customerProfileId text, in_customerPaymentProfileId text, in_firstName text, in_lastName text, in_email text, in_type text, in_filter_list text)
  RETURNS SETOF CreditCards AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CreditCards';
        var_functionName text := 'FILTER';

        rec CreditCards%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM CreditCards WHERE retired = 0';

        var_creditCardType int;

    BEGIN
        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = in_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        IF var_credentialsId IS NULL THEN
            -- check for sys admin login
            var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
                LIMIT 1;
        END IF;

        var_creditCardType := CreditCardTypeId FROM CreditCardTypes WHERE tag = in_type;

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_creditCardId' THEN
            ELSEIF filter_name = 'in_accountId' THEN
                var_query := var_query || ' AND accountId = ' || in_accountId;
            ELSEIF filter_name = 'in_addressId' THEN
                var_query := var_query || ' AND addressId = ' || in_addressId;
            ELSEIF filter_name = 'in_nameOnCreditCard' THEN
                var_query := var_query || ' AND nameOnCreditCard ILIKE ''%' || in_nameOnCreditCard || '%''';
            ELSEIF filter_name = 'in_maskedNumber' THEN
                var_query := var_query || ' AND maskedNumber ILIKE ''%' || in_maskedNumber || '%''';
            ELSEIF filter_name = 'in_expirationMonth' THEN
                var_query := var_query || ' AND expirationMonth ILIKE ''%' || in_expirationMonth || '%''';
            ELSEIF filter_name = 'in_expirationYear' THEN
                var_query := var_query || ' AND expirationYear ILIKE ''%' || in_expirationYear || '%''';
            ELSEIF filter_name = 'in_customerProfileId' THEN
                var_query := var_query || ' AND customerProfileId ILIKE ''%' || in_customerProfileId || '%''';
            ELSEIF filter_name = 'in_customerPaymentProfileId' THEN
                var_query := var_query || ' AND customerPaymentProfileId ILIKE ''%' || in_customerPaymentProfileId || '%''';
            ELSEIF filter_name = 'in_firstName' THEN
                var_query := var_query || ' AND firstName ILIKE ''%' || in_firstName || '%''';
            ELSEIF filter_name = 'in_lastName' THEN
                var_query := var_query || ' AND lastName ILIKE ''%' || in_lastName || '%''';
            ELSEIF filter_name = 'in_email' THEN
                var_query := var_query || ' AND email ILIKE ''%' || in_email || '%''';
            ELSEIF filter_name = 'in_type' THEN
                var_query := var_query || ' AND creditCardType = ' || var_creditCardType;
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            ELSEIF filter_name = 'in_retired' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM CreditCards;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_creditcard_filter(text, text, integer, integer, text, integer, integer, text, text, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_creditcard_find(text, text, integer)

-- DROP FUNCTION fn_creditcard_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_creditcard_find(in_credentials_email text, in_credentials_password text, in_creditCardId integer)
  RETURNS SETOF CreditCards AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CreditCards';
        var_functionName text := 'FIND';
        var_accountId bigint;

        rec CreditCards%ROWTYPE;

    BEGIN
        var_accountId := accountId FROM CreditCards WHERE creditCardId = in_creditCardId;

        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = var_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM CreditCards
                WHERE creditCardId = in_creditCardId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_creditcard_find(text, text, integer) OWNER TO postgres;
