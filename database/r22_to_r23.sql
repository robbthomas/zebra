-------------------------------------------------------------------------------
-- r22_to_r23.sql
--
-- Add PublishedPurchasesAcquisitions table.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (23);
-------------------------------------------------------------------------------

DROP TABLE IF EXISTS PublishedPurchasesAcquisitions CASCADE;

CREATE TABLE PublishedPurchasesAcquisitions (
    publishId bigint NOT NULL,
    cachedPurchaseAcquisitionCount bigint default(0),

    CONSTRAINT fk_PublishedPurchasesAcquisitions_publishId FOREIGN KEY
    (publishId) REFERENCES Published(publishId)
);

-- Hydrate our table for the first time. After deployment, function fn_publishedpurchasesacquisitions_rehydrate
-- can be run at any time to truncate and rehydrate the entire table.

INSERT INTO PublishedPurchasesAcquisitions(publishId, cachedPurchaseAcquisitionCount)
SELECT publishId, COUNT(*)
FROM AccountProjectNumbers
WHERE Retired = 0
GROUP BY publishId;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

