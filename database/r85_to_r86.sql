-------------------------------------------------------------------------------
-- Add PreviewImage table joining Projects and Asset
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES(86);

CREATE SEQUENCE seq_PreviewImage_previewImageId START WITH 2000;

CREATE TABLE public.PreviewImage (
    previewImageId int NOT NULL PRIMARY KEY DEFAULT nextval('seq_PreviewImage_previewImageId'),
    projectId varchar(255),
    assetId varchar(255),
    CONSTRAINT fk_PreviewImage_projectId FOREIGN KEY (projectId) REFERENCES Projects(projectId),
    CONSTRAINT fk_PreviewImage_assetId   FOREIGN KEY (assetId)   REFERENCES Asset(id)
);
create unique index ix_PreviewImage_projectId on PreviewImage (projectId);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;