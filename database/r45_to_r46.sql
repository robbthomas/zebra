-------------------------------------------------------------------------------
-- r45_to_r46.sql
--
-- Update published.editeddatetime with some sensible values where null, make it
--  not-null in the future.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (46);
-------------------------------------------------------------------------------
update published
	set editeddatetime = parent.date_created
	from parent
	where parent.id = published.projectid
	and published.editeddatetime is null;

alter table published
	alter column editeddatetime set not null;
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

