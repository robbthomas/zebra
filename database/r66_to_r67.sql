-------------------------------------------------------------------------------
-- fix duplicate rows in publishimages:  
-- rows should be unique by projectid + displayorder where deleted = false
--  
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (67);
-- toggle pager off for results:
-- there are two select statements to save "undo data" in the hudson logs.
\pset pager

-- first for screenshots
create temporary table tmp_goodrows as (
    select projectid, displayorder, max(publishimageid) as publishimageid
        from publishimages
        where retired = false
        and screenshotfileid is not null
        group by projectid, displayorder
);

select publishimageid as screenshot_retired_by_fix 
    from publishimages
    where publishimageid not in (select publishimageid from tmp_goodrows) 
        and screenshotfileid is not null
        and retired = false;

update publishimages set retired = true
    where publishimageid not in (select publishimageid from tmp_goodrows)
        and screenshotfileid is not null
        and retired = false;
    
create unique index ix_projectid_screenshot_displayorder on publishimages(projectid, displayorder) where (retired = false and screenshotfileid is not null);
drop table tmp_goodrows;

-- then for icons
create temporary table tmp_goodrows as (
    select projectid, displayorder, max(publishimageid) as publishimageid
        from publishimages
        where retired = false
        and iconfileid is not null
        group by projectid, displayorder
);

select publishimageid as icon_retired_by_fix 
    from publishimages
    where publishimageid not in (select publishimageid from tmp_goodrows) 
        and iconfileid is not null
        and retired = false;

update publishimages set retired = true
    where publishimageid not in (select publishimageid from tmp_goodrows)
        and iconfileid is not null
        and retired = false;

create unique index ix_projectid_icon_displayorder on publishimages(projectid, displayorder) where (retired = false and iconfileid is not null);
drop table tmp_goodrows;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;