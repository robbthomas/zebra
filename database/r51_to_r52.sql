-------------------------------------------------------------------------------
-- r50_to_r51.sql
--
-- Create new tables for guest-list normalization
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (52);
-------------------------------------------------------------------------------

create table GuestLists(
    guestListId  SERIAL not null PRIMARY KEY,
    emailText    text,
    updateddatetime timestamp not null default now(),
    retireddatetime timestamp,
    retiredbyid     text references app_user(id),
    retiredreasontypeid int8 references retiredreasontypes(retiredreasontypeid)
);
\i fn_guestlist.sql

alter table published add column guestListId int8 references GuestLists(guestListId);

-- email invites
-- no fk into app_user; we decide whether to display 'join' or 'login' dialogs
-- by performing an inner join on app_user.email when the invitee views the zapp.
create table GuestListInvitees (
    guestListInviteeId  SERIAL not null PRIMARY KEY,
    guestListId         int8 references GuestLists(guestListId),
    "email"             text,
    firstName           text,
    lastName            text,
    updateddatetime timestamp not null default now(),
    retireddatetime timestamp,
    retiredbyid     text references app_user(id),
    retiredreasontypeid int8 references retiredreasontypes(retiredreasontypeid),
    uuid            text not null default replace((uuid_generate_v4())::text, '-'::text, ''::text)
);
\i fn_guestlistinvitee.sql

drop table sharedprojectinvitees cascade;
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
