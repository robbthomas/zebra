DROP TYPE IF EXISTS _type_project_list_ CASCADE; 
CREATE TYPE _type_project_list_ AS (
      totalCount BIGINT
    , projectId VARCHAR(255)
    , version INT8
    , createdDateTime TIMESTAMP
    , retired BOOLEAN
    , editedDateTime TIMESTAMP
    , projectMetadata TEXT
    , projectName VARCHAR(255)
    , nextVersionId VARCHAR(255)
    , dep_gadgetRefId VARCHAR(255) -- 10
    , dep_instanceId VARCHAR(255)
    , projectTypeId INT4
    , gadgetSize INT4
    , autoSave BOOLEAN
    , dep_publishId INT8
    , versionMajor INT4
    , versionMinor INT4
    , versionDot INT4
    , versionBuildNumber INT4
    , statusId INT4                --20
    , licenseTypeId INT4
    , hideInStoreList BOOLEAN
    , featured BOOLEAN
    , published BOOLEAN
    , publishedMetadata VARCHAR(1024)
    , selfPrice INT4
    , inviteOnly BOOLEAN
    , guestListId INT8
    , dep_publishedRetired BOOLEAN
    , width INT4                   --30
    , height INT4
    , nextPublishedVersionId VARCHAR(255)
    , republishable BOOLEAN
    , editable BOOLEAN
    , embedable BOOLEAN
    , authorFirstName VARCHAR(128)
    , authorLastName VARCHAR(128)
    , authorWebsite VARCHAR(255)
    , authorDisplayName VARCHAR(255)
    , authorNameFormat TEXT             --40
    , authorCompanyName TEXT
    , authorAccountName TEXT
    , categoryId INT8
    , publishedName VARCHAR(256)
    , description TEXT
    , price INT4
    , currencyTypeId INT4
    , urlName VARCHAR(256)
    , authorMemberId VARCHAR(255)
    , accountId BIGINT               --50
    , accountTypeId INT4
    , projectAccountTypeName TEXT
    , owned BOOLEAN
    , content TEXT
    , initialValues TEXT
    , accountProjectNumberId VARCHAR(255)
    , playerId VARCHAR(255)
    , newerThanWhatSearcherOwns BOOLEAN
    , olderThanLatestVersion BOOLEAN
    , cachedPurchaseAcquisitionCount BIGINT  --60
    , ratingTotal INT4
    , numRatings INT4
    , commentCount INT8
    , icon_publishImageId INT8
    , icon_projectId VARCHAR(255)
    , icon_caption TEXT
    , icon_displayOrder INT4
    , icon_iconFileId TEXT
    , icon_renderingComplete BOOLEAN
    , icon_retired BOOLEAN               --70
    , icon_label TEXT
    , scrn_publishImageId0 TEXT
    , scrn_projectId0 VARCHAR(255)
    , scrn_caption0 TEXT
    , scrn_displayOrder0 INT4
    , scrn_screenshotFileId0 TEXT
    , scrn_renderingComplete0 BOOLEAN
    , scrn_retired0 BOOLEAN
    , scrn_label0 TEXT
    , scrn_publishImageId1 TEXT    --80
    , scrn_projectId1 VARCHAR(255)
    , scrn_caption1 TEXT
    , scrn_displayorder1 INT4
    , scrn_screenshotFileId1 TEXT
    , scrn_renderingComplete1 BOOLEAN
    , scrn_retired1 BOOLEAN
    , scrn_label1 TEXT
    , scrn_publishImageId2 TEXT
    , scrn_projectId2 VARCHAR(255)
    , scrn_caption2 TEXT            --90
    , scrn_displayOrder2 INT4
    , scrn_screenshotFileId2 TEXT
    , scrn_renderingComplete2 BOOLEAN
    , scrn_retired2 BOOLEAN
    , scrn_label2 TEXT
    , scrn_publishImageId3 TEXT
    , scrn_projectId3 VARCHAR(255)
    , scrn_caption3 TEXT
    , scrn_displayOrder3 INT4
    , scrn_screenshotFileId3 TEXT      --100
    , scrn_renderingComplete3 BOOLEAN
    , scrn_retired3 BOOLEAN
    , scrn_label3 TEXT
    , scrn_publishImageId4 TEXT
    , scrn_projectId4 VARCHAR(255)
    , scrn_caption4 TEXT
    , scrn_displayOrder4 INT4
    , scrn_screenshotFileId4 TEXT
    , scrn_renderingComplete4 BOOLEAN
    , scrn_retired4 BOOLEAN            --110
    , scrn_label4 TEXT
    , scrn_publishImageId5 TEXT    --80
    , scrn_projectId5 VARCHAR(255)
    , scrn_caption5 TEXT
    , scrn_displayorder5 INT4
    , scrn_screenshotFileId5 TEXT
    , scrn_renderingComplete5 BOOLEAN
    , scrn_retired5 BOOLEAN
    , scrn_label5 TEXT
    , scrn_publishImageId6 TEXT
    , scrn_projectId6 VARCHAR(255)
    , scrn_caption6 TEXT            --90
    , scrn_displayOrder6 INT4
    , scrn_screenshotFileId6 TEXT
    , scrn_renderingComplete6 BOOLEAN
    , scrn_retired6 BOOLEAN
    , scrn_label6 TEXT
    , scrn_publishImageId7 TEXT
    , scrn_projectId7 VARCHAR(255)
    , scrn_caption7 TEXT
    , scrn_displayOrder7 INT4
    , scrn_screenshotFileId7 TEXT      --100
    , scrn_renderingComplete7 BOOLEAN
    , scrn_retired7 BOOLEAN
    , scrn_label7 TEXT
);

/*
--Apps I own
SELECT * FROM fn_Project_List_small(
      2
    , TRUE
    , 35
    , 0
    , 4360
    , '064a029aabdc47eeb87ddd69d7aa4096'
    , NULL
    , 'createdDateTime desc'
    , NULL
    , NULL
    , NULL
)



--Gadgets I own
SELECT * FROM fn_Project_List_small(
      1
    , TRUE
    , 9
    , 0
    , 4360
    , '064a029aabdc47eeb87ddd69d7aa4096'
    , NULL
    , 'createdDateTime desc'
    , NULL
)

SELECT projects.projectId, projects.publishedName, projects.projectName, accountProjectNumberId 
FROM accountProjectNumbers 
    INNER JOIN projects
        ON accountProjectNumbers.projectId = projects.projectId
WHERE accountProjectNumbers.accountId = 4360 
AND accountProjectNumbers.retired = 0
AND projects.retired = false
ORDER BY createdDateTime DESC

--My projects
    SELECT createdDateTime, projectId, projectName, publishedName, apnUUID FROM fn_Project_List_small(
          2
        , FALSE
        , 5
        , 0
        , 4360
        , '064a029aabdc47eeb87ddd69d7aa4096'
        , NULL
        , 'createdDateTime desc'
        , NULL
    )

SELECT *  FROM accountProjectNumbers WHERE accountProjectNumberId IN ('7f32a85f0dda46f79b2e259187de28c9', '09cf23c8d66f41ec94b4448f2e8ca7d1')
*/


DROP TYPE IF EXISTS _type_project_list_small_ CASCADE;
CREATE TYPE _type_project_list_small_ AS (
      totalCount BIGINT
    , accountId bigint
    , projectId VARCHAR(255)
    , description TEXT
    , categoryId BIGINT
    , createdDateTime TIMESTAMP
    , editedDateTime TIMESTAMP
    , projectName VARCHAR(255)
    , owned BOOLEAN
    , publishedName VARCHAR(256)
    , urlName VARCHAR(256)
    , commentCount BIGINT
    , apnUUID VARCHAR(255)
    , playerId VARCHAR(255)
    , licenseTypeId INT4
    , inviteOnly BOOLEAN
    , republishable BOOLEAN
    , editable BOOLEAN
    , embedable BOOLEAN
    , olderThanLatestVersion BOOLEAN
    , cachedPurchaseAcquisitionCount BIGINT
    , ratingTotal INT4
    , numRatings INT4
    , hideInStoreList BOOLEAN
    , published BOOLEAN
    , retired BOOLEAN
    , nextPublishedVersionId VARCHAR(255)
    , authorDisplayName TEXT
    , price INT4
    , icon_publishImageId INT8
    , icon_projectId VARCHAR(255)
    , icon_caption TEXT
    , icon_displayOrder INT4
    , icon_iconFileId TEXT
    , icon_renderingComplete BOOLEAN
    , icon_retired BOOLEAN
    , icon_label TEXT
    , scrn_publishImageId0 TEXT
    , scrn_projectId0 VARCHAR(255)
    , scrn_caption0 TEXT
    , scrn_displayOrder0 INT4
    , scrn_screenshotFileId0 TEXT
    , scrn_renderingComplete0 BOOLEAN
    , scrn_retired0 BOOLEAN
    , scrn_label0 TEXT
    , scrn_publishImageId1 TEXT    --80
    , scrn_projectId1 VARCHAR(255)
    , scrn_caption1 TEXT
    , scrn_displayorder1 INT4
    , scrn_screenshotFileId1 TEXT
    , scrn_renderingComplete1 BOOLEAN
    , scrn_retired1 BOOLEAN
    , scrn_label1 TEXT
    , scrn_publishImageId2 TEXT
    , scrn_projectId2 VARCHAR(255)
    , scrn_caption2 TEXT            --90
    , scrn_displayOrder2 INT4
    , scrn_screenshotFileId2 TEXT
    , scrn_renderingComplete2 BOOLEAN
    , scrn_retired2 BOOLEAN
    , scrn_label2 TEXT
    , scrn_publishImageId3 TEXT
    , scrn_projectId3 VARCHAR(255)
    , scrn_caption3 TEXT
    , scrn_displayOrder3 INT4
    , scrn_screenshotFileId3 TEXT      --100
    , scrn_renderingComplete3 BOOLEAN
    , scrn_retired3 BOOLEAN
    , scrn_label3 TEXT
    , scrn_publishImageId4 TEXT
    , scrn_projectId4 VARCHAR(255)
    , scrn_caption4 TEXT
    , scrn_displayOrder4 INT4
    , scrn_screenshotFileId4 TEXT
    , scrn_renderingComplete4 BOOLEAN
    , scrn_retired4 BOOLEAN            --110
    , scrn_label4 TEXT
    , scrn_publishImageId5 TEXT    --80
    , scrn_projectId5 VARCHAR(255)
    , scrn_caption5 TEXT
    , scrn_displayorder5 INT4
    , scrn_screenshotFileId5 TEXT
    , scrn_renderingComplete5 BOOLEAN
    , scrn_retired5 BOOLEAN
    , scrn_label5 TEXT
    , scrn_publishImageId6 TEXT
    , scrn_projectId6 VARCHAR(255)
    , scrn_caption6 TEXT            --90
    , scrn_displayOrder6 INT4
    , scrn_screenshotFileId6 TEXT
    , scrn_renderingComplete6 BOOLEAN
    , scrn_retired6 BOOLEAN
    , scrn_label6 TEXT
    , scrn_publishImageId7 TEXT
    , scrn_projectId7 VARCHAR(255)
    , scrn_caption7 TEXT
    , scrn_displayOrder7 INT4
    , scrn_screenshotFileId7 TEXT      --100
    , scrn_renderingComplete7 BOOLEAN
    , scrn_retired7 BOOLEAN
    , scrn_label7 TEXT
);

CREATE OR REPLACE FUNCTION fn_Project_List_small (
      in_projectTypeId     INT4
    , in_isPublished       BOOLEAN
    , in_limit             INT4
    , in_offset            INT4
    , in_searcherAccountId INT8
    , in_searcherMemberId  TEXT
    , in_categoryId        BIGINT
    , in_orderBy           TEXT
    , in_genericSearch     TEXT
    , in_projectName       TEXT
    , in_projectIdList     TEXT
    , in_urlName           TEXT
) RETURNS SETOF _type_project_list_small_ AS $$ 
    DECLARE totalCount BIGINT :=    fn_project_list_small_count(
                                          in_projectTypeId
                                        , in_isPublished
                                        , in_limit 
                                        , in_searcherAccountId 
                                        , in_searcherMemberId
                                        , in_categoryId
                                        , in_genericSearch
                                        , in_projectName
                                        , in_projectIdList
                                        , in_urlName
                                    );
    BEGIN
        --raise exception 'projecttypeid: ?, isPublished: ?, limit: ?, offset: ?, searcherAccountId: ?, searcherMemberId: ?, categoryId: ?, orderBy: ?, genericSearch: ?', in_projectTypeId, in_isPublished, in_limit, in_offset, in_searcherAccountId, in_searcherMemberId, in_categoryId, in_orderBy, in_genericSearch;
        RETURN QUERY 
        SELECT
              totalCount
            , projects.accountid
            , projects.projectId
            , projects.description
            , projects.categoryId
            , projects.createdDateTime
            , projects.editedDateTime
            , projects.projectName
            , projects.owned
            , projects.publishedName
            , projects.urlname
            , CAST(fn_zappcomment_countcommentsforprojectid('', '', projects.projectId) AS BIGINT)
            , projects.accountProjectNumberId -- apnUUID
            , projects.accountProjectNumberId -- playerId
            , projects.licenseTypeId
            , projects.inviteOnly
            , LicenseTypes.republishable
            , LicenseTypes.editable
            , LicenseTypes.embedable
            , EXISTS (  
                SELECT 'x'
                FROM projects p3
                WHERE 1=1
                AND p3.retired = false
                AND p3.publishedName = projects.publishedName 
                AND p3.createdDateTime > projects.createdDateTime
                AND p3.nextpublishedversionid IS NULL 
                AND p3.hideInStoreList = FALSE
                AND p3.published = TRUE
              ) AS olderThanLatestVersion
            , ppa.cachedPurchaseAcquisitionCount
            , ( fn_zappratingcache_filter(
                    'foo'::text
                    , 'bar'::text
                    , 1::integer
                    , 0::integer
                    , null::text
                    , projects.projectId
                    , null::integer
                    , null::integer
                    , 'in_projectId'::text)
              ).numratings
            , ( fn_zappratingcache_filter(
                    'foo'::text
                    , 'bar'::text
                    , 1::integer
                    , 0::integer
                    , null::text
                    , projects.projectId
                    , null::integer
                    , null::integer
                    , 'in_projectId'::text)
              ).ratingtotal
            , projects.hideInStoreList
            , projects.published
            , projects.retired
            , projects.nextPublishedVersionId
            , projects.authorDisplayName
            , projects.price
            , icon.publishImageId
            , icon.projectId
            , icon.caption
            , icon.displayOrder
            , icon.iconFileId
            , icon.renderingComplete
            , icon.retired
            , icon.label
            , CAST(screenshot0.publishImageId AS TEXT)       
            , screenshot0.projectId
            , screenshot0.caption         
            , screenshot0.displayOrder
            , screenshot0.screenshotFileId 
            , screenshot0.renderingComplete     
            , screenshot0.retired               
            , screenshot0.label               
            , CAST(screenshot1.publishImageId AS TEXT)  
            , screenshot1.projectId 
            , screenshot1.caption           
            , screenshot1.displayOrder  
            , screenshot1.screenshotFileId 
            , screenshot1.renderingComplete   
            , screenshot1.retired            
            , screenshot1.label                
            , CAST(screenshot2.publishImageId AS TEXT) 
            , screenshot2.projectId   
            , screenshot2.caption 
            , screenshot2.displayOrder 
            , screenshot2.screenshotFileId 
            , screenshot2.renderingComplete     
            , screenshot2.retired             
            , screenshot2.label                
            , CAST(screenshot3.publishImageId AS TEXT)
            , screenshot3.projectId
            , screenshot3.caption
            , screenshot3.displayOrder
            , screenshot3.screenshotFileId 
            , screenshot3.renderingComplete     
            , screenshot3.retired              
            , screenshot3.label                 
            , CAST(screenshot4.publishImageId AS TEXT)
            , screenshot4.projectId
            , screenshot4.caption
            , screenshot4.displayOrder
            , screenshot4.screenshotFileId 
            , screenshot4.renderingComplete     
            , screenshot4.retired              
            , screenshot4.label                
            , CAST(screenshot5.publishImageId AS TEXT)  
            , screenshot5.projectId 
            , screenshot5.caption           
            , screenshot5.displayOrder  
            , screenshot5.screenshotFileId 
            , screenshot5.renderingComplete   
            , screenshot5.retired            
            , screenshot5.label                
            , CAST(screenshot6.publishImageId AS TEXT) 
            , screenshot6.projectId   
            , screenshot6.caption 
            , screenshot6.displayOrder 
            , screenshot6.screenshotFileId 
            , screenshot6.renderingComplete     
            , screenshot6.retired             
            , screenshot6.label                
            , CAST(screenshot7.publishImageId AS TEXT)
            , screenshot7.projectId
            , screenshot7.caption
            , screenshot7.displayOrder
            , screenshot7.screenshotFileId 
            , screenshot7.renderingComplete     
            , screenshot7.retired              
            , screenshot7.label     
        FROM (

            SELECT 
                  innerProjects.projectId
                , userApn.accountProjectNumberId IS NOT NULL AS owned
                , userApn.accountProjectNumberId
                , innerProjects.accountid
                , innerProjects.description
                , innerProjects.categoryId
                , innerProjects.createdDateTime
                , innerProjects.editedDateTime
                , innerProjects.projectName
                , innerProjects.publishedName
                , innerProjects.urlname
                , innerProjects.licenseTypeId
                , innerProjects.inviteOnly
                , innerProjects.hideInStoreList
                , innerProjects.published
                , innerProjects.retired
                , innerProjects.nextPublishedVersionId
                , CAST(CASE WHEN app_user.nameFormat = 'firstLast' THEN app_user.firstName || ' ' || app_user.lastName ELSE app_user.displayName END AS TEXT) AS authorDisplayName
                , innerProjects.price
                , innerProjects.projectTypeId
                , innerProjects.authorMemberId
                , innerProjects.nextVersionId
            FROM projects AS innerProjects
                INNER JOIN app_user
                    ON innerProjects.authorMemberId = app_user.id
                LEFT OUTER JOIN accountProjectNumbers userApn
                    ON innerProjects.projectId = userApn.projectId
                    AND userApn.retired = 0
                    AND in_IsPublished = true
                LEFT OUTER JOIN PublishedPurchasesAcquisitions ppa 
                    ON innerProjects.projectId = ppa.projectId 
                LEFT OUTER JOIN Categories
                    ON innerProjects.categoryId = Categories.categoryId
            WHERE 1=1 
            AND CASE 
                WHEN in_projectIdList IS NOT NULL THEN
                    innerProjects.projectId IN (in_projectIdList)
                ELSE 1=1
                    AND innerProjects.retired = false
                    AND CASE WHEN in_urlName IS NOT NULL THEN UPPER(innerProjects.urlName) = UPPER(in_urlName) ELSE TRUE END
                    AND CASE 
                        WHEN in_isPublished THEN 
                            CASE 
                                WHEN in_searcherAccountId IS NOT NULL THEN userApn.accountId = in_searcherAccountId
                                ELSE innerProjects.authorMemberId = in_searcherMemberId
                            END
                            AND innerProjects.published = TRUE
                            AND innerProjects.nextPublishedVersionId IS NULL
                        ELSE 
                            innerProjects.authorMemberId = in_searcherMemberId 
                            AND innerProjects.nextVersionID IS NULL
                    END
                    AND CASE WHEN in_projectTypeId IS NOT NULL THEN innerProjects.projectTypeId = in_projectTypeId ELSE TRUE END
                    AND CASE
                        WHEN in_categoryId IS NOT NULL THEN innerProjects.categoryId = in_categoryId
                        ELSE TRUE
                    END
                    AND CASE 
                            WHEN in_genericSearch IS NOT NULL THEN 
                                UPPER(innerProjects.publishedName) LIKE '%' || UPPER(in_genericSearch) || '%' 
                                OR UPPER(innerProjects.description) LIKE '%' || UPPER(in_genericSearch) || '%'
                                OR UPPER(app_user.firstname) LIKE '%' || UPPER(in_genericSearch) || '%'
                                OR UPPER(app_user.lastname) LIKE '%' || UPPER(in_genericSearch) || '%'
                                OR UPPER(app_user.displayname) LIKE '%' || UPPER(in_genericSearch) || '%'
                                OR UPPER(app_user.firstName) || ' ' || UPPER(app_user.lastName) LIKE '%' || UPPER(in_genericSearch) || '%'
                            ELSE TRUE 
                        END
                    AND CASE WHEN in_projectName IS NOT NULL THEN UPPER(innerProjects.projectName) LIKE '%' || UPPER(in_projectName) || '%' ELSE TRUE END
            END -- end of in_projectIdList CASE statement. It's a biggie, because that parameter is a master override.

            ORDER BY 
                      CASE WHEN in_orderBy = 'createdDateTime asc' THEN userApn.createdDateTime ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'createdDateTime desc' THEN userApn.createdDateTime ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'editedDateTime asc' THEN innerProjects.editedDateTime ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'editedDateTime desc' THEN innerProjects.editedDateTime ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'publishedName asc' THEN innerProjects.publishedName ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'publishedName desc' THEN innerProjects.publishedName ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'price asc' THEN innerProjects.price ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'price desc' THEN innerProjects.price ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'categoryId asc' THEN Categories.name ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'categoryId desc' THEN Categories.name ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'description asc' THEN innerProjects.description ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'description desc' THEN innerProjects.description ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'projectTypeId asc' THEN innerProjects.projectTypeId ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'projectTypeId desc' THEN innerProjects.projectTypeId ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'projectName asc' THEN innerProjects.projectName ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'projectName desc' THEN innerProjects.projectName ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount asc' THEN ppa.cachedPurchaseAcquisitionCount ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount desc' THEN ppa.cachedPurchaseAcquisitionCount ELSE NULL END DESC
            LIMIT in_limit
            OFFSET in_offset

        ) AS projects
            LEFT OUTER JOIN accountProjectNumbers userApn
                ON projects.projectId = userApn.projectId
                AND userApn.retired = 0
            LEFT OUTER JOIN publishImages icon
                ON projects.projectId = icon.projectId
                AND icon.iconFileId IS NOT NULL
                AND icon.retired = FALSE
            LEFT OUTER JOIN projectTypes 
                ON projects.projectTypeId = projectTypes.projectTypeId
            LEFT OUTER JOIN LicenseTypes
                ON projects.licenseTypeId = LicenseTypes.licenseTypeId
            LEFT OUTER JOIN PublishedPurchasesAcquisitions ppa 
                ON projects.projectId = ppa.projectId 
            LEFT OUTER JOIN Categories
                ON projects.categoryId = Categories.categoryId
            LEFT OUTER JOIN publishimages screenshot0 
                ON screenshot0.projectId = projects.projectId 
                AND screenshot0.displayOrder = 0 
                AND screenshot0.screenshotfileid IS NOT NULL 
                AND screenshot0.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot1 
                ON screenshot1.projectId = projects.projectId 
                AND screenshot1.displayOrder = 1 
                AND screenshot1.screenshotfileid IS NOT NULL 
                AND screenshot1.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot2 
                ON screenshot2.projectId = projects.projectId 
                AND screenshot2.displayOrder = 2 
                AND screenshot2.screenshotfileid IS NOT NULL 
                AND screenshot2.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot3 
                ON screenshot3.projectId = projects.projectId 
                AND screenshot3.displayOrder = 3 
                AND screenshot3.screenshotfileid IS NOT NULL 
                AND screenshot3.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot4
                ON screenshot4.projectId = projects.projectId 
                AND screenshot4.displayOrder = 4 
                AND screenshot4.screenshotfileid IS NOT NULL 
                AND screenshot4.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot5 
                ON screenshot5.projectId = projects.projectId 
                AND screenshot5.displayOrder = 5
                AND screenshot5.screenshotfileid IS NOT NULL 
                AND screenshot5.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot6 
                ON screenshot6.projectId = projects.projectId 
                AND screenshot6.displayOrder = 6 
                AND screenshot6.screenshotfileid IS NOT NULL 
                AND screenshot6.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot7 
                ON screenshot7.projectId = projects.projectId 
                AND screenshot7.displayOrder = 7 
                AND screenshot7.screenshotfileid IS NOT NULL 
                AND screenshot7.retired = FALSE
        WHERE 1=1 
            AND CASE 
                    WHEN in_searcherAccountId IS NOT NULL THEN userApn.accountId = in_searcherAccountId
                    ELSE projects.authorMemberId = in_searcherMemberId
                END
            ORDER BY 
                      CASE WHEN in_orderBy = 'createdDateTime asc' THEN userApn.createdDateTime ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'createdDateTime desc' THEN userApn.createdDateTime ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'editedDateTime asc' THEN projects.editedDateTime ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'editedDateTime desc' THEN projects.editedDateTime ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'publishedName asc' THEN projects.publishedName ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'publishedName desc' THEN projects.publishedName ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'price asc' THEN projects.price ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'price desc' THEN projects.price ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'categoryId asc' THEN Categories.name ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'categoryId desc' THEN Categories.name ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'description asc' THEN projects.description ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'description desc' THEN projects.description ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'projectTypeId asc' THEN projects.projectTypeId ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'projectTypeId desc' THEN projects.projectTypeId ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'projectName asc' THEN projects.projectName ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'projectName desc' THEN projects.projectName ELSE NULL END DESC

                    , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount asc' THEN ppa.cachedPurchaseAcquisitionCount ELSE NULL END ASC
                    , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount desc' THEN ppa.cachedPurchaseAcquisitionCount ELSE NULL END DESC
        ;
    END
$$ LANGUAGE 'plpgsql' VOLATILE
COST 100;


/*--
Parameters --> 
  in_projectTypeId: 2
, in_featured: <NULL>
, in_limit: <NULL>
, in_offset: <NULL>
, in_searcherAccountId: 4360
, in_orderBy: createdDateTime desc
, in_categoryId: <NULL>
, in_authorMemberId: 064a029aabdc47eeb87ddd69d7aa4096
, in_searchText: 120913-12
, in_genericSearch: <NULL>
, in_ownerAccountId: <NULL>
, in_showContent: t
, in_isPublished: f
, in_hideInStoreList: t
, in_parentProjectId: <NULL>
, in_isForLatest: f 
, in_searcherMemberId: 064a029aabdc47eeb87ddd69d7aa4096
, in_projectName: 120913-12


FEATURED APPS
---------------------------------------------------------
SELECT *  FROM fn_project_list2(
      2
    , true
    , 3
    , 0
    , 4360
    , 'createdDateTime desc'
    , NULL
    , NULL
    , NULL
    , NULL
    , NULL
    , false
    , TRUE
    , false
    , NULL
    , TRUE
    , '064a029aabdc47eeb87ddd69d7aa4096'
    , NULL
    , NULL
)

SHOPPS
---------------------------------------------------------
SELECT *  FROM fn_project_list2(
      2
    , false
    , 9
    , 0
    , 4360
    , 'createdDateTime desc'
    , NULL
    , NULL
    , NULL
    , NULL
    , NULL
    , false
    , TRUE
    , false
    , NULL
    , TRUE
    , '064a029aabdc47eeb87ddd69d7aa4096'
    , NULL
    , NULL
    , NULL
)


PROJECT TRANSFER NAME COLLISION CHECK
-----------------------------------------------------------
SELECT * FROM fn_project_list2(
      0
    , false
    , NULL
    , 0
    , 4360
    , 'createdDateTime desc'
    , NULL
    , NULL
    , NULL
    , NULL
    , NULL
    , true
    , false
    , true
    , null
    , true
    , '40ebd1d51ec6427baa09bd231218e0be'
    , 'Test Proect 061103(1)'
    , NULL
)

*/




CREATE OR REPLACE FUNCTION fn_Project_List2 (
      in_projectTypeId  INT4
    , in_featured BOOLEAN
    , in_limit INT4
    , in_offset INT4
    , in_searcherAccountId INT8
    , in_orderBy VARCHAR
    , in_categoryId INT4
    , in_authorMemberId TEXT
    , in_searchText TEXT
    , in_genericSearch TEXT
    , in_ownerAccountId INT4
    , in_showContent BOOLEAN
    , in_isPublished BOOLEAN
    , in_hideInStoreList BOOLEAN
    , in_parentProjectId VARCHAR(255)
    , in_isForLatest BOOLEAN
    , in_searcherMemberId TEXT
    , in_projectName VARCHAR(255)
    , in_urlName VARCHAR(256)
    , in_projectIdList TEXT
) 
RETURNS SETOF _type_project_list_ AS $$ 
    DECLARE totalCount BIGINT :=    fn_project_list_count(
                                          in_projectTypeId
                                        , in_featured 
                                        , in_limit 
                                        , in_offset 
                                        , in_searcherAccountId 
                                        , in_orderBy 
                                        , in_categoryId 
                                        , in_authorMemberId 
                                        , in_searchText 
                                        , in_genericSearch 
                                        , in_ownerAccountId 
                                        , in_showContent 
                                        , in_isPublished 
                                        , in_hideInStoreList
                                        , in_parentProjectId 
                                        , in_isForLatest 
                                        , in_searcherMemberId
                                        , in_projectName
                                        , in_urlName
                                        , in_projectIdList
                                    );
    BEGIN
        --RAISE EXCEPTION 'Parameters --> in_projectTypeId: %, in_featured: %, in_limit: %, in_offset: %, in_searcherAccountId: %, in_orderBy: %, in_categoryId: %, in_authorMemberId: %, in_searchText: %, in_genericSearch: %, in_ownerAccountId: %, in_showContent: %, in_isPublished: %, in_hideInStoreList: %, in_parentProjectId: %, in_isForLatest: % , in_searcherMemberId: %, in_projectName: %', in_projectTypeId, in_featured , in_limit , in_offset , in_searcherAccountId , in_orderBy , in_categoryId , in_authorMemberId , in_searchText , in_genericSearch , in_ownerAccountId , in_showContent , in_isPublished , in_hideInStoreList, in_parentProjectId , in_isForLatest , in_searcherMemberId, in_projectName;

        RETURN QUERY 
        
        SELECT
              totalCount                          
            , a.projectId     
            , OuterProjects.version
            , OuterProjects.createdDateTime
            , OuterProjects.retired
            , OuterProjects.editedDateTime
            , OuterProjects.projectmetadata
            , OuterProjects.projectName
            , OuterProjects.nextVersionId
            , OuterProjects.dep_gadgetRefId        --10
            , OuterProjects.dep_instanceId
            , OuterProjects.projectTypeId
            , OuterProjects.gadgetSize
            , OuterProjects.autoSave
            , OuterProjects.dep_publishId
            , OuterProjects.versionMajor
            , OuterProjects.versionMinor
            , OuterProjects.versionDot
            , OuterProjects.versionBuildNumber
            , OuterProjects.statusId               --20
            , OuterProjects.licenseTypeId
            , OuterProjects.hideInStoreList
            , OuterProjects.featured
            , OuterProjects.published
            , OuterProjects.publishedMetadata
            , OuterProjects.selfPrice
            , OuterProjects.inviteOnly
            , OuterProjects.guestListId
            , OuterProjects.dep_publishedRetired
            , OuterProjects.width                 --30
            , OuterProjects.height
            , OuterProjects.nextPublishedVersionId
            , a.republishable
            , a.editable
            , a.embedable
            , a.firstName 
            , a.lastName 
            , a.website
            , a.displayName 
            , a.nameFormat                        --40
            , a.authorCompanyName
            , a.authorAccountName
            , OuterProjects.categoryId
            , OuterProjects.publishedName
            , OuterProjects.description
            , OuterProjects.price
            , OuterProjects.currencyTypeId
            , OuterProjects.urlName
            , OuterProjects.authorMemberId
            , OuterProjects.accountId             --50
            , OuterProjects.accountTypeId
            , a.projectAccountTypeName
            , (userApn.accountProjectNumberId IS NOT NULL OR a.accountId = in_searcherAccountId) AS owned
            , CASE WHEN in_showContent THEN OuterProjects.content ELSE NULL END AS content
            , CASE WHEN in_showContent THEN OuterProjects.initialValues ELSE NULL END AS initialValues
            , COALESCE(userApn.accountProjectNumberId, a.accountProjectNumberId) AS accountProjectNumberId
            , COALESCE(userApn.accountProjectNumberId, a.accountProjectNumberId) AS accountProjectNumberId           
            , EXISTS (                                        -- If there exists an APN record such that
                SELECT userApn2.projectId 
                FROM accountprojectnumbers userApn2 
                    INNER JOIN projects p2 
                        ON p2.projectid = userApn2.projectId
                WHERE p2.publishedname = a.publishedname      -- the project referenced by the APN has the same publishedName as the project in the query
                AND userApn2.accountId = in_searcherAccountId -- the APN record is owned by the searcher
                AND userApn2.accountId <> a.accountId         -- the searcher is not the author of the app
                AND userApn2.projectId <> a.projectId         -- the project referenced by the APN is NOT the same project in the query
                AND userApn2.retired = 0
                AND p2.createdDateTime < a.createdDateTime    -- the project owned by the searcher was created before the project that's being returned in the query
            ) AS newerThanWhatSearcherOwns                    -- return true
            , (userApn.accountProjectNumberId IS NOT NULL OR a.accountId = in_searcherAccountId) AND EXISTS (   
                SELECT 'x'
                FROM projects p3
                WHERE 1=1
                AND p3.retired = false
                AND p3.publishedName = a.publishedName 
                AND p3.createdDateTime > OuterProjects.createdDateTime
                AND p3.nextpublishedversionid IS NULL 
                AND p3.hideInStoreList = FALSE
                AND p3.published = TRUE
              ) AS olderThanLatestVersion
            , a.cachedPurchaseAcquisitionCount    --60
            , ( fn_zappratingcache_filter(
                'foo'::text
                , 'bar'::text
                , 0::integer
                , 0::integer
                , null::text
                , a.projectId
                , null::integer
                , null::integer
                , 'in_projectId'::text)
            ).ratingtotal
            , ( fn_zappratingcache_filter(
                'foo'::text
                , 'bar'::text
                , 0::integer
                , 0::integer
                , null::text
                , a.projectId
                , null::integer
                , null::integer
                , 'in_projectId'::text)
            ).numratings
            , CAST(fn_zappcomment_countcommentsforprojectid('', '', a.projectId) AS BIGINT)
            , icon.publishImageId                
            , icon.projectId                     
            , icon.caption                       
            , icon.displayOrder                  
            , icon.iconFileId                    
            , icon.renderingComplete             
            , icon.retired                       
            , icon.label                         
            
            , CAST(screenshot0.publishImageId AS TEXT)       
            , screenshot0.projectId
            , screenshot0.caption         
            , screenshot0.displayOrder
            , screenshot0.screenshotFileId 
            , screenshot0.renderingComplete     
            , screenshot0.retired               
            , screenshot0.label               

            , CAST(screenshot1.publishImageId AS TEXT)  
            , screenshot1.projectId 
            , screenshot1.caption           
            , screenshot1.displayOrder  
            , screenshot1.screenshotFileId 
            , screenshot1.renderingComplete   
            , screenshot1.retired            
            , screenshot1.label                

            , CAST(screenshot2.publishImageId AS TEXT) 
            , screenshot2.projectId   
            , screenshot2.caption 
            , screenshot2.displayOrder 
            , screenshot2.screenshotFileId 
            , screenshot2.renderingComplete     
            , screenshot2.retired             
            , screenshot2.label                

            , CAST(screenshot3.publishImageId AS TEXT)
            , screenshot3.projectId
            , screenshot3.caption
            , screenshot3.displayOrder
            , screenshot3.screenshotFileId 
            , screenshot3.renderingComplete     
            , screenshot3.retired              
            , screenshot3.label                 

            , CAST(screenshot4.publishImageId AS TEXT)
            , screenshot4.projectId
            , screenshot4.caption
            , screenshot4.displayOrder
            , screenshot4.screenshotFileId 
            , screenshot4.renderingComplete     
            , screenshot4.retired              
            , screenshot4.label                

            , CAST(screenshot5.publishImageId AS TEXT)  
            , screenshot5.projectId 
            , screenshot5.caption           
            , screenshot5.displayOrder  
            , screenshot5.screenshotFileId 
            , screenshot5.renderingComplete   
            , screenshot5.retired            
            , screenshot5.label                

            , CAST(screenshot6.publishImageId AS TEXT) 
            , screenshot6.projectId   
            , screenshot6.caption 
            , screenshot6.displayOrder 
            , screenshot6.screenshotFileId 
            , screenshot6.renderingComplete     
            , screenshot6.retired             
            , screenshot6.label                

            , CAST(screenshot7.publishImageId AS TEXT)
            , screenshot7.projectId
            , screenshot7.caption
            , screenshot7.displayOrder
            , screenshot7.screenshotFileId 
            , screenshot7.renderingComplete     
            , screenshot7.retired              
            , screenshot7.label      
        FROM (
            SELECT 
                  InnerProjects.projectId
                , InnerProjects.createdDateTime
                , InnerProjects.publishedName
                , authorAccount.accountId
                , LicenseTypes.republishable
                , LicenseTypes.editable
                , LicenseTypes.embedable
                , app_user.firstName 
                , app_user.lastName 
                , app_user.website
                , app_user.displayName 
                , app_user.nameFormat 
                , authorCompany.name AS authorCompanyName
                , authorAccount.name AS authorAccountName
                , projectAccountType.name AS projectAccountTypeName
                , ppa.cachedPurchaseAcquisitionCount
                , publisherApn.accountProjectNumberId
                , InnerProjects.categoryId
            FROM projects InnerProjects
                INNER JOIN app_user 
                    ON app_user.id = InnerProjects.authorMemberId 
                LEFT JOIN parent_child 
                    ON parent_child.child_id = InnerProjects.projectId
                    AND parent_child.parent_children_id = in_parentProjectId
                INNER JOIN accounts authorAccount 
                    LEFT OUTER JOIN companies AS authorCompany
                        ON authorAccount.companyId = authorCompany.companyId
                    ON authorAccount.accountId = InnerProjects.accountId 
                    INNER JOIN AccountTypes authorAccountType 
                        ON authorAccount.typeId = authorAccountType.accounttypeid 
                        AND authorAccountType.tag <> 'collector'
                    LEFT OUTER JOIN AccountProjectNumbers publisherApn 
                        ON publisherApn.accountId = authorAccount.accountId
                        AND publisherApn.projectId = InnerProjects.projectId
                        AND publisherApn.retired = 0
                LEFT OUTER JOIN accountTypes projectAccountType 
                    ON InnerProjects.accountTypeId = projectAccountType.accountTypeId 
                LEFT OUTER JOIN LicenseTypes
                    ON InnerProjects.licenseTypeId = LicenseTypes.licenseTypeId
                LEFT OUTER JOIN PublishedPurchasesAcquisitions ppa 
                    ON InnerProjects.projectId = ppa.projectId 
                LEFT OUTER JOIN Categories
                    ON InnerProjects.categoryId = Categories.categoryId
            WHERE 1=1
            AND InnerProjects.retired = false
            AND CASE WHEN in_projectIdList IS NOT NULL THEN InnerProjects.projectId IN (in_projectIdList)
            ELSE (
                1=1
                AND CASE WHEN in_projectTypeId IS NOT NULL   THEN InnerProjects.projectTypeId     = in_projectTypeId   ELSE TRUE END
                AND CASE WHEN in_featured                    THEN InnerProjects.featured          = TRUE               ELSE TRUE END
                AND CASE WHEN in_isPublished                 THEN InnerProjects.published         = TRUE               ELSE TRUE END
                AND CASE WHEN in_categoryId IS NOT NULL      THEN InnerProjects.categoryId        = in_categoryId      ELSE TRUE END
                AND CASE WHEN in_authorMemberId IS NOT NULL  THEN InnerProjects.authorMemberId    = in_authorMemberId  ELSE TRUE END
                AND CASE WHEN in_parentProjectId IS NOT NULL THEN parent_child.parent_children_id IS NOT NULL AND InnerProjects.retired = FALSE ELSE TRUE END
                AND CASE WHEN in_hideInStoreList IS NOT NULL 
                          AND in_hideInStoreList = FALSE     THEN InnerProjects.hideInStoreList   = FALSE              ELSE TRUE END
                AND CASE 
                    WHEN in_isForLatest THEN 
                        CASE
                            WHEN in_isPublished THEN InnerProjects.nextPublishedVersionId IS NULL
                            ELSE InnerProjects.nextVersionId IS NULL
                        END 
                        AND InnerProjects.retired = FALSE
                        AND (publisherApn.retired IS NULL OR publisherApn.retired = 0)
                        AND CASE
                            WHEN in_authorMemberId IS NOT NULL AND NOT in_authorMemberId = in_searcherMemberId THEN InnerProjects.hideInStoreList = false
                            ELSE TRUE
                        END
                    ELSE TRUE
                END
                AND CASE WHEN in_projectName IS NOT NULL THEN UPPER(innerProjects.projectName) = UPPER(in_projectName) ELSE TRUE END
                AND CASE WHEN in_urlName IS NOT NULL THEN UPPER(innerProjects.urlName) = UPPER(in_urlName) ELSE TRUE END
                AND CASE 
                        WHEN in_searchText IS NOT NULL AND in_searchText <> '' THEN 
                            UPPER(innerProjects.publishedName) = UPPER(in_searchText)
                            OR UPPER(innerProjects.urlName) = UPPER(in_searchText)
                        ELSE TRUE 
                    END
                AND CASE 
                    WHEN in_genericSearch IS NOT NULL THEN 
                        UPPER(innerProjects.publishedName) LIKE '%' || UPPER(in_genericSearch) || '%' 
                        OR UPPER(innerProjects.description) LIKE '%' || UPPER(in_genericSearch) || '%'
                        OR UPPER(app_user.firstname) LIKE '%' || UPPER(in_genericSearch) || '%'
                        OR UPPER(app_user.lastname) LIKE '%' || UPPER(in_genericSearch) || '%'
                        OR UPPER(app_user.displayname) LIKE '%' || UPPER(in_genericSearch) || '%'
                        OR UPPER(app_user.firstName) || ' ' || UPPER(app_user.lastName) LIKE '%' || UPPER(in_genericSearch) || '%'
                    ELSE TRUE 
                END
            )
            END

            ORDER BY 
                  CASE WHEN in_orderBy = 'createdDateTime asc' THEN InnerProjects.createdDateTime ELSE NULL END ASC
                , CASE WHEN in_orderBy = 'createdDateTime desc' THEN InnerProjects.createdDateTime ELSE NULL END DESC
                
                , CASE WHEN in_orderBy = 'editedDateTime asc' THEN InnerProjects.editedDateTime ELSE NULL END ASC
                , CASE WHEN in_orderBy = 'editedDateTime desc' THEN InnerProjects.editedDateTime ELSE NULL END DESC
                
                , CASE WHEN in_orderBy = 'publishedName asc' THEN InnerProjects.publishedName ELSE NULL END ASC
                , CASE WHEN in_orderBy = 'publishedName desc' THEN InnerProjects.publishedName ELSE NULL END DESC

                , CASE WHEN in_orderBy = 'price asc' THEN InnerProjects.price ELSE NULL END ASC
                , CASE WHEN in_orderBy = 'price desc' THEN InnerProjects.price ELSE NULL END DESC

                , CASE WHEN in_orderBy = 'categoryId asc' THEN Categories.name ELSE NULL END ASC
                , CASE WHEN in_orderBy = 'categoryId desc' THEN Categories.name ELSE NULL END DESC

                , CASE WHEN in_orderBy = 'description asc' THEN InnerProjects.description ELSE NULL END ASC
                , CASE WHEN in_orderBy = 'description desc' THEN InnerProjects.description ELSE NULL END DESC

                , CASE WHEN in_orderBy = 'projectTypeId asc' THEN InnerProjects.projectTypeId ELSE NULL END ASC
                , CASE WHEN in_orderBy = 'projectTypeId desc' THEN InnerProjects.projectTypeId ELSE NULL END DESC

                , CASE WHEN in_orderBy = 'projectName asc' THEN InnerProjects.projectName ELSE NULL END ASC
                , CASE WHEN in_orderBy = 'projectName desc' THEN InnerProjects.projectName ELSE NULL END DESC

                , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount asc' THEN ppa.cachedPurchaseAcquisitionCount ELSE NULL END ASC
                , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount desc' THEN ppa.cachedPurchaseAcquisitionCount ELSE NULL END DESC
            LIMIT in_limit
            OFFSET COALESCE(in_offset, 0)

        ) a
            INNER JOIN projects OuterProjects
                ON a.projectId = OuterProjects.projectId
            LEFT OUTER JOIN AccountProjectNumbers userApn 
                ON a.projectId = userApn.projectId 
                AND userApn.retired = 0
                AND userApn.accountId = COALESCE(in_ownerAccountId, in_searcherAccountId)
            LEFT OUTER JOIN publishimages icon 
                ON icon.projectId = a.projectId 
                AND icon.iconfileid IS NOT NULL 
                AND icon.retired = FALSE

            LEFT OUTER JOIN publishimages screenshot0 
                ON screenshot0.projectId = a.projectId 
                AND screenshot0.displayOrder = 0 
                AND screenshot0.screenshotfileid IS NOT NULL 
                AND screenshot0.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot1 
                ON screenshot1.projectId = a.projectId 
                AND screenshot1.displayOrder = 1 
                AND screenshot1.screenshotfileid IS NOT NULL 
                AND screenshot1.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot2 
                ON screenshot2.projectId = a.projectId 
                AND screenshot2.displayOrder = 2 
                AND screenshot2.screenshotfileid IS NOT NULL 
                AND screenshot2.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot3 
                ON screenshot3.projectId = a.projectId 
                AND screenshot3.displayOrder = 3 
                AND screenshot3.screenshotfileid IS NOT NULL 
                AND screenshot3.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot4
                ON screenshot4.projectId = a.projectId 
                AND screenshot4.displayOrder = 4 
                AND screenshot4.screenshotfileid IS NOT NULL 
                AND screenshot4.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot5 
                ON screenshot5.projectId = a.projectId 
                AND screenshot5.displayOrder = 5
                AND screenshot5.screenshotfileid IS NOT NULL 
                AND screenshot5.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot6 
                ON screenshot6.projectId = a.projectId 
                AND screenshot6.displayOrder = 6 
                AND screenshot6.screenshotfileid IS NOT NULL 
                AND screenshot6.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot7 
                ON screenshot7.projectId = a.projectId 
                AND screenshot7.displayOrder = 7 
                AND screenshot7.screenshotfileid IS NOT NULL 
                AND screenshot7.retired = FALSE
            LEFT OUTER JOIN Categories outerCategories
                ON a.categoryId = outerCategories.categoryId
        WHERE 1=1
        AND CASE WHEN in_ownerAccountId IS NOT NULL THEN userApn.accountProjectNumberId IS NOT NULL ELSE TRUE END
        ORDER BY 
              CASE WHEN in_orderBy = 'createdDateTime asc' THEN OuterProjects.createdDateTime ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'createdDateTime desc' THEN OuterProjects.createdDateTime ELSE NULL END DESC
            
            , CASE WHEN in_orderBy = 'editedDateTime asc' THEN OuterProjects.editedDateTime ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'editedDateTime desc' THEN OuterProjects.editedDateTime ELSE NULL END DESC
            
            , CASE WHEN in_orderBy = 'publishedName asc' THEN OuterProjects.publishedName ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'publishedName desc' THEN OuterProjects.publishedName ELSE NULL END DESC
            
            , CASE WHEN in_orderBy = 'price asc' THEN OuterProjects.price ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'price desc' THEN OuterProjects.price ELSE NULL END DESC

            , CASE WHEN in_orderBy = 'categoryId asc' THEN outerCategories.name ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'categoryId desc' THEN outerCategories.name ELSE NULL END DESC

            , CASE WHEN in_orderBy = 'description asc' THEN OuterProjects.description ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'description desc' THEN OuterProjects.description ELSE NULL END DESC

            , CASE WHEN in_orderBy = 'projectTypeId asc' THEN OuterProjects.projectTypeId ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'projectTypeId desc' THEN OuterProjects.projectTypeId ELSE NULL END DESC

            , CASE WHEN in_orderBy = 'projectName asc' THEN OuterProjects.projectName ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'projectName desc' THEN OuterProjects.projectName ELSE NULL END DESC

            , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount asc' THEN a.cachedPurchaseAcquisitionCount ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount desc' THEN a.cachedPurchaseAcquisitionCount ELSE NULL END DESC

;
    END
$$ LANGUAGE 'plpgsql' VOLATILE
COST 100;






CREATE OR REPLACE FUNCTION fn_Project_Single (
      in_searcherAccountId INT8     -- 1.  accountId of SEARCHER
    , in_projectId VARCHAR(255)     -- 2.  projectId
    , in_apnId VARCHAR(255)         -- 3. APNID
    , in_urlName TEXT               -- 4. URLName
    , in_isForLatestApnUUID BOOLEAN -- 5. isForLatestAPNUUID
    , in_depPubId INT8              -- 6. dep_publishId
    , in_showContent BOOLEAN        -- 7. in_showContent
) 
RETURNS SETOF _type_project_list_ AS $$
    BEGIN
        RETURN QUERY
        SELECT
              CAST(1 AS BIGINT)
            , a.projectId     
            , a.version
            , a.createdDateTime
            , a.retired
            , a.editedDateTime
            , a.projectmetadata
            , a.projectName
            , a.nextVersionId
            , a.dep_gadgetRefId
            , a.dep_instanceId
            , a.projectTypeId
            , a.gadgetSize
            , a.autoSave
            , a.dep_publishId
            , a.versionMajor
            , a.versionMinor
            , a.versionDot
            , a.versionBuildNumber
            , a.statusId
            , a.licenseTypeId
            , a.hideInStoreList
            , a.featured
            , a.published
            , a.publishedMetadata
            , a.selfPrice
            , a.inviteOnly
            , a.guestListId
            , a.dep_publishedRetired
            , a.width
            , a.height
            , a.nextPublishedVersionId
            , a.republishable
            , a.editable
            , a.embedable
            , a.authorfirstName 
            , a.authorlastName 
            , a.authorwebsite
            , a.authordisplayName 
            , a.authornameFormat 
            , a.authorCompanyName
            , a.authorAccountName
            , a.categoryId
            , a.publishedName
            , a.description
            , a.price
            , a.currencyTypeId
            , a.urlName
            , a.authorMemberId
            , a.accountId
            , a.accountTypeId
            , a.projectAccountTypeName
            , a.owned
            , a.content
            , a.initialValues
            , a.accountProjectNumberId
            , a.accountProjectNumberId
            , a.newerThanWhatSearcherOwns 
            , a.olderThanLatestVersion 
            , ppa.cachedPurchaseAcquisitionCount
            , a.ratingtotal                    
            , a.numRatings   
            , CAST(0 AS BIGINT)
            , icon.publishImageId                
            , icon.projectId                     
            , icon.caption                       
            , icon.displayOrder                 
            , icon.iconFileId                    
            , icon.renderingComplete            
            , icon.retired                      
            , icon.label                       
            , CAST(screenshot0.publishImageId AS TEXT)       
            , screenshot0.projectId
            , screenshot0.caption         
            , screenshot0.displayOrder
            , screenshot0.screenshotFileId 
            , screenshot0.renderingComplete     
            , screenshot0.retired               
            , screenshot0.label               
            , CAST(screenshot1.publishImageId AS TEXT)  
            , screenshot1.projectId 
            , screenshot1.caption           
            , screenshot1.displayOrder  
            , screenshot1.screenshotFileId 
            , screenshot1.renderingComplete   
            , screenshot1.retired            
            , screenshot1.label                
            , CAST(screenshot2.publishImageId AS TEXT) 
            , screenshot2.projectId   
            , screenshot2.caption 
            , screenshot2.displayOrder 
            , screenshot2.screenshotFileId 
            , screenshot2.renderingComplete     
            , screenshot2.retired             
            , screenshot2.label                
            , CAST(screenshot3.publishImageId AS TEXT)
            , screenshot3.projectId
            , screenshot3.caption
            , screenshot3.displayOrder
            , screenshot3.screenshotFileId 
            , screenshot3.renderingComplete     
            , screenshot3.retired              
            , screenshot3.label                 
            , CAST(screenshot4.publishImageId AS TEXT)
            , screenshot4.projectId
            , screenshot4.caption
            , screenshot4.displayOrder
            , screenshot4.screenshotFileId 
            , screenshot4.renderingComplete     
            , screenshot4.retired              
            , screenshot4.label                 
            , CAST(screenshot5.publishImageId AS TEXT) 
            , screenshot5.projectId   
            , screenshot5.caption 
            , screenshot5.displayOrder 
            , screenshot5.screenshotFileId 
            , screenshot5.renderingComplete     
            , screenshot5.retired             
            , screenshot5.label                
            , CAST(screenshot6.publishImageId AS TEXT)
            , screenshot6.projectId
            , screenshot6.caption
            , screenshot6.displayOrder
            , screenshot6.screenshotFileId 
            , screenshot6.renderingComplete     
            , screenshot6.retired              
            , screenshot6.label                 
            , CAST(screenshot7.publishImageId AS TEXT)
            , screenshot7.projectId
            , screenshot7.caption
            , screenshot7.displayOrder
            , screenshot7.screenshotFileId 
            , screenshot7.renderingComplete     
            , screenshot7.retired              
            , screenshot7.label               
        FROM (
            SELECT 
                  InnerProjects.projectId     
                , InnerProjects.version
                , InnerProjects.createdDateTime
                , InnerProjects.retired
                , InnerProjects.editedDateTime
                , InnerProjects.projectmetadata
                , InnerProjects.projectName
                , InnerProjects.nextVersionId
                , InnerProjects.dep_gadgetRefId
                , InnerProjects.dep_instanceId
                , InnerProjects.projectTypeId
                , InnerProjects.gadgetSize
                , InnerProjects.autoSave
                , InnerProjects.dep_publishId
                , InnerProjects.versionMajor
                , InnerProjects.versionMinor
                , InnerProjects.versionDot
                , InnerProjects.versionBuildNumber
                , InnerProjects.statusId
                , InnerProjects.licenseTypeId
                , InnerProjects.hideInStoreList
                , InnerProjects.featured
                , InnerProjects.published
                , InnerProjects.publishedMetadata
                , InnerProjects.selfPrice
                , InnerProjects.inviteOnly
                , InnerProjects.guestListId
                , InnerProjects.dep_publishedRetired
                , InnerProjects.width
                , InnerProjects.height
                , InnerProjects.nextPublishedVersionId
                , LicenseTypes.republishable
                , LicenseTypes.editable
                , LicenseTypes.embedable
                , authorCompany.name AS authorCompanyName
                , authorAccount.name AS authorAccountName
                , InnerProjects.categoryId
                , InnerProjects.publishedName
                , InnerProjects.description
                , InnerProjects.price
                , InnerProjects.currencyTypeId
                , InnerProjects.urlName
                , InnerProjects.authorMemberId
                , InnerProjects.accountId
                , InnerProjects.accountTypeId
                , CASE WHEN in_showContent THEN InnerProjects.content ELSE NULL END AS content
                , CASE WHEN in_showContent THEN InnerProjects.initialValues ELSE NULL END AS initialValues
                , app_user.firstName AS authorFirstName
                , app_user.lastName AS authorLastName
                , app_user.displayName AS authorDisplayName
                , app_user.website AS authorWebsite
                , app_user.nameFormat AS authorNameFormat
                , projectAccountType.name AS projectAccountTypeName
                
, ( fn_zappratingcache_filter(
        'foo'::text
        , 'bar'::text
        , 0::integer
        , 0::integer
        , null::text
        , InnerProjects.projectId
        , null::integer
        , null::integer
        , 'in_projectId'::text)
).numratings

, ( fn_zappratingcache_filter(
        'foo'::text
        , 'bar'::text
        , 0::integer
        , 0::integer
        , null::text
        , InnerProjects.projectId
        , null::integer
        , null::integer
        , 'in_projectId'::text)
).ratingtotal
                
                , (userApn.accountprojectnumberid IS NOT NULL OR ProjectAPN.accountId = in_searcherAccountId) AS owned
                , COALESCE(userApn.accountprojectnumberid, projectApn.accountprojectnumberid) AS accountProjectNumberId
                , EXISTS (                                        -- If there exists an APN record such that
                    SELECT userApn2.projectId 
                    FROM accountprojectnumbers userApn2 
                        INNER JOIN projects p2 
                            ON p2.projectid = userApn2.projectId
                    WHERE p2.publishedname = innerProjects.publishedname      -- the project referenced by the APN has the same publishedName as the project in the query
                    AND userApn2.accountId = in_searcherAccountId             -- the APN record is owned by the searcher
                    AND userApn2.accountId <> innerProjects.accountId         -- the searcher is not the author of the app
                    AND userApn2.projectId <> innerProjects.projectId         -- the project referenced by the APN is NOT the same project in the query
                    AND userApn2.retired = 0
                  ) AS newerThanWhatSearcherOwns                              -- return true
                , EXISTS (  
                    SELECT 'x'
                    FROM projects p2 
                    WHERE p2.publishedName = InnerProjects.publishedName 
                    AND p2.nextpublishedversionid IS NULL 
                    AND p2.createdDateTime > InnerProjects.createdDateTime 
                    AND p2.hideInStoreList = FALSE
                    AND p2.published = TRUE
                  ) AS olderThanLatestVersion
            FROM projects InnerProjects
                INNER JOIN app_user 
                    ON app_user.id = InnerProjects.authorMemberId 
                INNER JOIN accounts authorAccount 
                    LEFT OUTER JOIN companies AS authorCompany
                        ON authorAccount.companyId = authorCompany.companyId
                    ON authorAccount.accountId = InnerProjects.accountId 
                    INNER JOIN AccountTypes authorAccountType 
                        ON authorAccount.typeId = authorAccountType.accounttypeid 
                LEFT OUTER JOIN AccountProjectNumbers projectApn 
                    ON projectApn.projectId = InnerProjects.projectId
                    AND projectApn.retired = 0
                LEFT OUTER JOIN accountTypes projectAccountType 
                    ON InnerProjects.accountTypeId = projectAccountType.accountTypeId 
                LEFT OUTER JOIN AccountProjectNumbers userApn 
                    ON InnerProjects.projectId = userApn.projectId 
                    AND userApn.retired = 0
                    AND userApn.accountId = in_searcherAccountId
                LEFT OUTER JOIN LicenseTypes
                    ON InnerProjects.licenseTypeId = LicenseTypes.licenseTypeId
            WHERE 1=1 
            AND CASE 
                WHEN (in_apnId IS NOT NULL AND in_apnId <> '') OR (in_depPubId IS NOT NULL AND in_depPubId > 0) THEN (
                    CASE 
                        WHEN in_isForLatestApnUUID THEN 
                            InnerProjects.projectId = (
                                SELECT apnp.projectId 
                                FROM projects apnp 
                                    INNER JOIN AccountProjectNumbers apnapn 
                                        ON apnapn.projectId = apnp.projectid 
                                WHERE 1=1
                                AND CASE 
                                    WHEN in_apnId IS NOT NULL AND in_apnId <> '' THEN apnapn.accountprojectnumberid = in_apnId 
                                    WHEN in_depPubId > 0 THEN apnp.dep_publishId = in_depPubId
                                    ELSE TRUE 
                                END
                                AND apnp.nextPublishedVersionId IS NULL 
                                AND apnp.published = TRUE
                                LIMIT 1
                            )
                        ELSE
                            CASE 
                                WHEN in_apnId IS NOT NULL AND in_apnId <> '' THEN projectApn.accountProjectNumberId = in_apnId 
                                WHEN in_depPubId IS NOT NULL AND in_depPubId > 0 THEN InnerProjects.dep_publishId = in_depPubId
                                ELSE TRUE 
                            END 
                    END
              )
                WHEN in_projectId IS NOT NULL AND in_projectId <> '' THEN 
                    InnerProjects.projectId = in_projectId
                WHEN in_urlName IS NOT NULL AND in_urlName <> '' THEN 
                    InnerProjects.urlname = in_urlName 
                    AND InnerProjects.nextPublishedVersionId IS NULL 
                    AND InnerProjects.published = TRUE
                ELSE TRUE
            END

        ) a
            LEFT OUTER JOIN PublishedPurchasesAcquisitions ppa 
                ON a.projectId = ppa.projectId 
            LEFT OUTER JOIN publishimages icon 
                ON icon.projectId = a.projectId 
                AND icon.iconfileid IS NOT NULL 
                AND icon.retired = FALSE 
            LEFT OUTER JOIN publishimages screenshot0 
                ON screenshot0.projectId = a.projectId 
                AND screenshot0.displayOrder = 0 
                AND screenshot0.screenshotfileid IS NOT NULL 
                AND screenshot0.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot1 
                ON screenshot1.projectId = a.projectId 
                AND screenshot1.displayOrder = 1 
                AND screenshot1.screenshotfileid IS NOT NULL 
                AND screenshot1.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot2 
                ON screenshot2.projectId = a.projectId 
                AND screenshot2.displayOrder = 2 
                AND screenshot2.screenshotfileid IS NOT NULL 
                AND screenshot2.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot3 
                ON screenshot3.projectId = a.projectId 
                AND screenshot3.displayOrder = 3 
                AND screenshot3.screenshotfileid IS NOT NULL 
                AND screenshot3.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot4
                ON screenshot4.projectId = a.projectId 
                AND screenshot4.displayOrder = 4 
                AND screenshot4.screenshotfileid IS NOT NULL 
                AND screenshot4.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot5 
                ON screenshot5.projectId = a.projectId 
                AND screenshot5.displayOrder = 5
                AND screenshot5.screenshotfileid IS NOT NULL 
                AND screenshot5.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot6 
                ON screenshot6.projectId = a.projectId 
                AND screenshot6.displayOrder = 6
                AND screenshot6.screenshotfileid IS NOT NULL 
                AND screenshot6.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot7
                ON screenshot7.projectId = a.projectId 
                AND screenshot7.displayOrder = 7 
                AND screenshot7.screenshotfileid IS NOT NULL 
                AND screenshot7.retired = FALSE
        LIMIT 1;
    END
$$ LANGUAGE 'plpgsql' VOLATILE
COST 100;


























DROP TYPE IF EXISTS _type_project_list_my_ CASCADE; 
CREATE TYPE _type_project_list_my_ AS (
      totalCount BIGINT
    , projectId VARCHAR(255)
    , createdDateTime TIMESTAMP
    , editedDateTime TIMESTAMP
    , projectName VARCHAR(255)
    , projectTypeId INT4
    , hideInStoreList BOOLEAN
    , featured BOOLEAN
    , published BOOLEAN
    , republishable BOOLEAN
    , editable BOOLEAN
    , embedable BOOLEAN
    , authorFirstName VARCHAR(128)
    , authorLastName VARCHAR(128)
    , authorWebsite VARCHAR(255)
    , authorDisplayName VARCHAR(255)
    , authorNameFormat TEXT             --40
    , authorCompanyName TEXT
    , authorAccountName TEXT
    , categoryId INT8
    , publishedName VARCHAR(256)
    , description TEXT
    , urlName VARCHAR(256)
    , authorMemberId VARCHAR(255)
    , accountId BIGINT               --50
    , accountTypeId INT4
    , projectAccountTypeName TEXT
    , accountProjectNumberId VARCHAR(255)
    , playerId VARCHAR(255)
    , olderThanLatestVersion BOOLEAN
    , commentCount BIGINT
    , icon_publishImageId INT8
    , icon_projectId VARCHAR(255)
    , icon_caption TEXT
    , icon_displayOrder INT4
    , icon_iconFileId TEXT
    , icon_renderingComplete BOOLEAN
    , icon_retired BOOLEAN               --70
    , icon_label TEXT
    , numRatings Integer
    , ratingtotal Integer
);

/*
SELECT * FROM fn_Project_List_My(
      2
    , false
    , 200
    , 0
    , 'createdDateTime desc'
    , 4360
    , '064a029aabdc47eeb87ddd69d7aa4096'
    , null
    , null
)
*/


CREATE OR REPLACE FUNCTION fn_Project_List_My (
      in_projectTypeId INT4
    , in_isPublished BOOLEAN
    , in_limit INT4
    , in_offset INT4
    , in_orderBy VARCHAR
    , in_searcherAccountId INT8
    , in_authorMemberId VARCHAR(255)
    , in_categoryId INT4
    , in_genericSearch TEXT
) 
RETURNS SETOF _type_project_list_my_ AS $$
    DECLARE totalCount BIGINT :=    fn_project_list_My_count(
                                          in_projectTypeId
                                        , in_isPublished
                                        , in_searcherAccountId 
                                        , in_authorMemberId
                                        , in_categoryId
                                        , in_genericSearch
                                    );
    BEGIN
        RETURN QUERY
SELECT
      totalCount
    , InnerProjects.projectId
    , InnerProjects.createdDateTime 
    , InnerProjects.editedDateTime
    , InnerProjects.projectName
    , InnerProjects.projectTypeId
    , InnerProjects.hideInStoreList
    , InnerProjects.featured
    , InnerProjects.published
    , LicenseTypes.republishable
    , LicenseTypes.editable
    , LicenseTypes.embedable
    , InnerProjects.authorFirstName
    , InnerProjects.authorLastName
    , InnerProjects.authorWebsite
    , InnerProjects.authorDisplayName
    , InnerProjects.authorNameFormat 
    , InnerProjects.authorCompanyName
    , InnerProjects.authorAccountName
    , InnerProjects.categoryId
    , InnerProjects.publishedName 
    , InnerProjects.description
    , InnerProjects.urlName 
    , InnerProjects.authorMemberId 
    , InnerProjects.accountId 
    , InnerProjects.accountTypeId 
    , InnerProjects.projectAccountTypeName
    , InnerProjects.accountProjectNumberId 
    , InnerProjects.accountProjectNumberId
    , EXISTS (
        SELECT 'x'
        FROM projects p2 
        WHERE p2.publishedName = InnerProjects.publishedName 
        AND p2.nextpublishedversionid IS NULL 
        AND p2.createdDateTime > InnerProjects.createdDateTime
        AND p2.hideInStoreList = FALSE
        AND p2.published = TRUE
    ) AS olderThanLatestVersion
    , CAST(fn_zappcomment_countcommentsforprojectid('', '', InnerProjects.projectId) AS BIGINT)
    , icon.publishImageId                
    , icon.projectId                     
    , icon.caption                       
    , icon.displayOrder                 
    , icon.iconFileId                    
    , icon.renderingComplete            
    , icon.retired                      
    , icon.label
    , ( fn_zappratingcache_filter(
            'foo'::text
            , 'bar'::text
            , 0::integer
            , 0::integer
            , null::text
            , InnerProjects.projectId
            , null::integer
            , null::integer
            , 'in_projectId'::text)
    ).numratings

    , ( fn_zappratingcache_filter(
            'foo'::text
            , 'bar'::text
            , 0::integer
            , 0::integer
            , null::text
            , InnerProjects.projectId
            , null::integer
            , null::integer
            , 'in_projectId'::text)
    ).ratingtotal
FROM (
    SELECT
          projects.projectId
        , projects.createdDateTime 
        , projects.editedDateTime
        , projects.projectName
        , projects.projectTypeId
        , projects.hideInStoreList
        , projects.featured
        , projects.published
        , app_user.firstName AS authorFirstName
        , app_user.lastName AS authorLastName
        , app_user.website AS authorWebsite
        , app_user.displayName AS authorDisplayName
        , app_user.nameFormat AS authorNameFormat
        , authorCompany.name AS authorCompanyName
        , authorAccount.name AS authorAccountName
        , projects.categoryId
        , projects.publishedName 
        , projects.description
        , projects.authorMemberId 
        , projects.accountId 
        , projects.accountTypeId 
        , projectAccountType.name AS projectAccountTypeName
        , userApn.accountProjectNumberId
        , projects.licenseTypeId
        , projects.urlName
        , projects.price
        , ppa.cachedPurchaseAcquisitionCount
        , userApn.createdDateTime as apnCreatedDateTime
    FROM projects
        INNER JOIN app_user
            ON projects.authorMemberId = app_user.id
        INNER JOIN accounts authorAccount 
            LEFT OUTER JOIN companies AS authorCompany
                ON authorAccount.companyId = authorCompany.companyId
            ON authorAccount.accountId = projects.accountId 
            INNER JOIN AccountTypes authorAccountType 
                ON authorAccount.typeId = authorAccountType.accounttypeid 
        LEFT JOIN accountTypes projectAccountType
            ON projects.accountTypeId = projectAccountType.accountTypeId
        LEFT OUTER JOIN AccountProjectNumbers userApn 
            ON projects.projectId = userApn.projectId 
            AND userApn.retired = 0
            AND userApn.accountId = in_searcherAccountId
        LEFT OUTER JOIN PublishedPurchasesAcquisitions ppa 
            ON projects.projectId = ppa.projectId 
    WHERE 1=1
    AND projects.projectTypeId = in_projectTypeId
    AND CASE 
        WHEN in_isPublished THEN userApn.accountId = in_searcherAccountId
        ELSE projects.authorMemberId = in_authorMemberId 
             AND projects.nextVersionId IS NULL 
             AND projects.retired = FALSE
    END
    AND CASE WHEN in_categoryId IS NOT NULL      THEN projects.categoryId        = in_categoryId      ELSE TRUE END
    AND CASE 
        WHEN in_genericSearch IS NOT NULL THEN 
            UPPER(projects.publishedName) LIKE '%' || UPPER(in_genericSearch) || '%' 
            OR UPPER(projects.projectName) LIKE '%' || UPPER(in_genericSearch) || '%'
            OR UPPER(projects.description) LIKE '%' || UPPER(in_genericSearch) || '%'
            OR UPPER(app_user.firstname) LIKE '%' || UPPER(in_genericSearch) || '%'
            OR UPPER(app_user.lastname) LIKE '%' || UPPER(in_genericSearch) || '%'
            OR UPPER(app_user.displayname) LIKE '%' || UPPER(in_genericSearch) || '%'
            OR UPPER(app_user.firstName) || ' ' || UPPER(app_user.lastName) LIKE '%' || UPPER(in_genericSearch) || '%'
        ELSE TRUE 
    END
    ORDER BY 
          CASE WHEN in_orderBy = 'createdDateTime asc' AND in_isPublished THEN userApn.createdDateTime ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'createdDateTime desc' AND in_isPublished THEN userApn.createdDateTime ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'createdDateTime asc' AND NOT in_isPublished THEN projects.createdDateTime ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'createdDateTime desc' AND NOT in_isPublished THEN projects.createdDateTime ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'editedDateTime asc' AND NOT in_isPublished THEN projects.editedDateTime ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'editedDateTime desc' AND NOT in_isPublished THEN projects.editedDateTime ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'publishedName asc' THEN projects.publishedName ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'publishedName desc' THEN projects.publishedName ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'price asc' THEN projects.price ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'price desc' THEN projects.price ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'projectName asc' THEN projects.projectName ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'projectName desc' THEN projects.projectName ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount asc' THEN ppa.cachedPurchaseAcquisitionCount ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount desc' THEN ppa.cachedPurchaseAcquisitionCount ELSE NULL END DESC
    LIMIT in_limit
    OFFSET in_offset
) AS InnerProjects
    LEFT OUTER JOIN LicenseTypes
        ON InnerProjects.licenseTypeId = LicenseTypes.licenseTypeId
    LEFT OUTER JOIN PublishedPurchasesAcquisitions ppa 
        ON InnerProjects.projectId = ppa.projectId 
    LEFT OUTER JOIN ZappRatingCaches zrc 
        ON InnerProjects.projectId = zrc.projectId 
    LEFT OUTER JOIN publishimages icon 
        ON icon.projectId = InnerProjects.projectId 
        AND icon.iconfileid IS NOT NULL 
        AND icon.retired = FALSE     
    ORDER BY 
          CASE WHEN in_orderBy = 'createdDateTime asc' AND in_isPublished THEN apnCreatedDateTime ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'createdDateTime desc' AND in_isPublished THEN apnCreatedDateTime ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'createdDateTime asc' AND NOT in_isPublished THEN InnerProjects.createdDateTime ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'createdDateTime desc' AND NOT in_isPublished THEN InnerProjects.createdDateTime ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'editedDateTime asc' AND NOT in_isPublished THEN InnerProjects.editedDateTime ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'editedDateTime desc' AND NOT in_isPublished THEN InnerProjects.editedDateTime ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'publishedName asc' THEN InnerProjects.publishedName ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'publishedName desc' THEN InnerProjects.publishedName ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'price asc' THEN InnerProjects.price ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'price desc' THEN InnerProjects.price ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'projectName asc' THEN InnerProjects.projectName ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'projectName desc' THEN InnerProjects.projectName ELSE NULL END DESC

        , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount asc' THEN InnerProjects.cachedPurchaseAcquisitionCount ELSE NULL END ASC
        , CASE WHEN in_orderBy = 'cachedPurchaseAcquisitionCount desc' THEN InnerProjects.cachedPurchaseAcquisitionCount ELSE NULL END DESC   
;
    END
$$ LANGUAGE 'plpgsql' VOLATILE
COST 100; 





DROP TYPE IF EXISTS _type_project_list_tiny_ CASCADE; 
CREATE TYPE _type_project_list_tiny_ AS (
      projectId VARCHAR(255)
    , projectName VARCHAR(255)
    , publishedName VARCHAR(256)
    , createdDateTime TIMESTAMP
    , description TEXT
    , icon_publishImageId INT8
    , icon_projectId VARCHAR(255)
    , icon_caption TEXT
    , icon_displayOrder INT4
    , icon_iconFileId TEXT
    , icon_renderingComplete BOOLEAN
    , icon_retired BOOLEAN               --70
    , icon_label TEXT
);

/*
SELECT * FROM fn_Project_list_tiny(
      2
    , false
    , 'createdDateTime desc'
    , '064a029aabdc47eeb87ddd69d7aa4096'
    , NULL
);
*/

CREATE OR REPLACE FUNCTION fn_Project_List_Tiny (
      in_projectTypeId INT4
    , in_isPublished BOOLEAN
    , in_orderBy VARCHAR
    , in_authorMemberId VARCHAR(255)
    , in_projectName VARCHAR(255)
    , in_urlName_search TEXT
    , in_Retired BOOLEAN
    , in_projectNameList TEXT
) 
RETURNS SETOF _type_project_list_tiny_ AS $$
    BEGIN
        RETURN QUERY

        SELECT
              projects.projectId
            , projectName
            , publishedName
            , createdDateTime
            , description
            , icon.publishImageId                
            , icon.projectId                     
            , icon.caption                       
            , icon.displayOrder                 
            , icon.iconFileId                    
            , icon.renderingComplete            
            , icon.retired                      
            , icon.label 
        FROM projects
            LEFT OUTER JOIN publishimages icon 
                ON icon.projectId = projects.projectId 
                AND icon.iconfileid IS NOT NULL 
                AND icon.retired = FALSE 
        WHERE 1=1 
        AND CASE 
           WHEN in_projectTypeId > 0 THEN projects.projectTypeId  = in_projectTypeId ELSE TRUE END
        AND   CASE WHEN in_isPublished THEN projects.published = TRUE ELSE TRUE END
        AND   projects.authorMemberId = in_authorMemberId
        AND CASE 
            WHEN in_projectName IS NOT NULL THEN UPPER(projects.projectName) LIKE '%' || UPPER(in_projectName) || '%'
            WHEN in_urlName_search IS NOT NULL THEN UPPER(projects.urlName) LIKE '%' || UPPER(in_urlName_search) || '%' 
            ELSE TRUE 
        END
        AND CASE
            WHEN in_isPublished THEN projects.nextPublishedVersionId IS NULL
            ELSE projects.nextVersionId IS NULL
        END
        
        -- In this context, if the retired flag is either false ("We REALLY want only non-retired projects") or null ("We just want a Tiny list")
        -- then the filter is defaulted to FALSE. Only if the flag is provided with a true value will we include retired projects
        -- in the result set. This means there is no way to get a Tiny list containing both retired and unretired projects.
        
        AND CASE 
            WHEN in_retired IS NOT NULL AND in_retired = TRUE THEN projects.retired = TRUE 
            ELSE projects.retired = FALSE 
        END
        AND CASE 
            WHEN in_projectNameList IS NOT NULL THEN
                projects.projectName IN (in_projectNameList)                
            ELSE TRUE
        END
        ORDER BY 
              CASE WHEN in_orderBy = 'createdDateTime asc' THEN projects.createdDateTime ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'createdDateTime desc' THEN projects.createdDateTime ELSE NULL END DESC
            
            , CASE WHEN in_orderBy = 'editedDateTime asc' THEN projects.editedDateTime ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'editedDateTime desc' THEN projects.editedDateTime ELSE NULL END DESC

            , CASE WHEN in_orderBy = 'publishedName asc' THEN projects.publishedName ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'publishedName desc' THEN projects.publishedName ELSE NULL END DESC

            , CASE WHEN in_orderBy = 'projectName asc' THEN projects.projectName ELSE NULL END ASC
            , CASE WHEN in_orderBy = 'projectName desc' THEN projects.projectName ELSE NULL END DESC
;
    END
$$ LANGUAGE 'plpgsql' VOLATILE
COST 100; 




/*
SELECT * FROM fn_Project_List_History(
      '139eac2790bb43bb9912ced58f8ac29f'
    , false
    , '064a029aabdc47eeb87ddd69d7aa4096'
    , 4360
)
*/

DROP TYPE IF EXISTS _type_project_list_history_ CASCADE; 
CREATE TYPE _type_project_list_history_ AS (
      projectId VARCHAR(255)
    , projectName VARCHAR(255)
    , publishedName VARCHAR(256)
    , createdDateTime TIMESTAMP
    , description TEXT
    , accountId BIGINT
    , accountProjectNumberId VARCHAR(255)
    , editable BOOLEAN
    , editedDateTime TIMESTAMP
    , embedable BOOLEAN
    , playerId VARCHAR(255)
    , published BOOLEAN
    , republishable BOOLEAN
    , urlName VARCHAR(256)
    , inviteOnly BOOLEAN
    , licenseTypeId INT4
    , nextPublishedVersionId VARCHAR(255)
    , owned BOOLEAN
    , retired BOOLEAN
    , price INT4
    , autoSave BOOLEAN
    , guestListId INT8
    , nextVersionId VARCHAR(255)
    , icon_publishImageId INT8
    , icon_projectId VARCHAR(255)
    , icon_caption TEXT
    , icon_displayOrder INT4
    , icon_iconFileId TEXT
    , icon_renderingComplete BOOLEAN
    , icon_retired BOOLEAN               --70
    , icon_label TEXT
    , scrn_publishImageId0 TEXT
    , scrn_projectId0 VARCHAR(255)
    , scrn_caption0 TEXT
    , scrn_displayOrder0 INT4
    , scrn_screenshotFileId0 TEXT
    , scrn_renderingComplete0 BOOLEAN
    , scrn_retired0 BOOLEAN
    , scrn_label0 TEXT
    , scrn_publishImageId1 TEXT    --80
    , scrn_projectId1 VARCHAR(255)
    , scrn_caption1 TEXT
    , scrn_displayorder1 INT4
    , scrn_screenshotFileId1 TEXT
    , scrn_renderingComplete1 BOOLEAN
    , scrn_retired1 BOOLEAN
    , scrn_label1 TEXT
    , scrn_publishImageId2 TEXT
    , scrn_projectId2 VARCHAR(255)
    , scrn_caption2 TEXT            --90
    , scrn_displayOrder2 INT4
    , scrn_screenshotFileId2 TEXT
    , scrn_renderingComplete2 BOOLEAN
    , scrn_retired2 BOOLEAN
    , scrn_label2 TEXT
    , scrn_publishImageId3 TEXT
    , scrn_projectId3 VARCHAR(255)
    , scrn_caption3 TEXT
    , scrn_displayOrder3 INT4
    , scrn_screenshotFileId3 TEXT      --100
    , scrn_renderingComplete3 BOOLEAN
    , scrn_retired3 BOOLEAN
    , scrn_label3 TEXT
    , scrn_publishImageId4 TEXT
    , scrn_projectId4 VARCHAR(255)
    , scrn_caption4 TEXT
    , scrn_displayOrder4 INT4
    , scrn_screenshotFileId4 TEXT
    , scrn_renderingComplete4 BOOLEAN
    , scrn_retired4 BOOLEAN            --110
    , scrn_label4 TEXT
    , scrn_publishImageId5 TEXT    --80
    , scrn_projectId5 VARCHAR(255)
    , scrn_caption5 TEXT
    , scrn_displayorder5 INT4
    , scrn_screenshotFileId5 TEXT
    , scrn_renderingComplete5 BOOLEAN
    , scrn_retired5 BOOLEAN
    , scrn_label5 TEXT
    , scrn_publishImageId6 TEXT
    , scrn_projectId6 VARCHAR(255)
    , scrn_caption6 TEXT            --90
    , scrn_displayOrder6 INT4
    , scrn_screenshotFileId6 TEXT
    , scrn_renderingComplete6 BOOLEAN
    , scrn_retired6 BOOLEAN
    , scrn_label6 TEXT
    , scrn_publishImageId7 TEXT
    , scrn_projectId7 VARCHAR(255)
    , scrn_caption7 TEXT
    , scrn_displayOrder7 INT4
    , scrn_screenshotFileId7 TEXT      --100
    , scrn_renderingComplete7 BOOLEAN
    , scrn_retired7 BOOLEAN
    , scrn_label7 TEXT
);



CREATE OR REPLACE FUNCTION fn_Project_List_History (
      in_projectId VARCHAR(255)
    , in_projectName TEXT
    , in_publishedName TEXT
    , in_urlName TEXT
    , in_isPublished BOOLEAN
    , in_searcherMemberId VARCHAR(255)
    , in_searcherAccountId INT4
    , in_retired BOOLEAN
) 
RETURNS SETOF _type_project_list_history_ AS $$
    BEGIN
        RETURN QUERY
        SELECT 
              innerProjects.projectId
            , innerProjects.projectName
            , innerProjects.publishedName
            , innerProjects.createdDateTime
            , innerProjects.description
            , innerProjects.accountId
            , historyApn.accountProjectNumberId
            , LicenseTypes.editable
            , innerProjects.editedDateTime 
            , LicenseTypes.embedable
            , historyApn.accountProjectNumberId
            , innerProjects.published 
            , LicenseTypes.republishable
            , innerProjects.urlName 
            , innerProjects.inviteOnly
            , innerProjects.licenseTypeId
            , innerProjects.nextPublishedVersionId
            , (historyApn.accountProjectNumberId IS NOT NULL OR innerProjects.accountId = in_searcherAccountId) AS owned
            , innerProjects.retired 
            , innerProjects.price 
            , innerProjects.autoSave 
            , innerProjects.guestListId
            , innerProjects.nextVersionId
            , icon.publishImageId                
            , icon.projectId                     
            , icon.caption                       
            , icon.displayOrder                 
            , icon.iconFileId                    
            , icon.renderingComplete            
            , icon.retired                      
            , icon.label            
            , CAST(screenshot0.publishImageId AS TEXT)       
            , screenshot0.projectId
            , screenshot0.caption         
            , screenshot0.displayOrder
            , screenshot0.screenshotFileId 
            , screenshot0.renderingComplete     
            , screenshot0.retired               
            , screenshot0.label               
            , CAST(screenshot1.publishImageId AS TEXT)  
            , screenshot1.projectId 
            , screenshot1.caption           
            , screenshot1.displayOrder  
            , screenshot1.screenshotFileId 
            , screenshot1.renderingComplete   
            , screenshot1.retired            
            , screenshot1.label                
            , CAST(screenshot2.publishImageId AS TEXT) 
            , screenshot2.projectId   
            , screenshot2.caption 
            , screenshot2.displayOrder 
            , screenshot2.screenshotFileId 
            , screenshot2.renderingComplete     
            , screenshot2.retired             
            , screenshot2.label                
            , CAST(screenshot3.publishImageId AS TEXT)
            , screenshot3.projectId
            , screenshot3.caption
            , screenshot3.displayOrder
            , screenshot3.screenshotFileId 
            , screenshot3.renderingComplete     
            , screenshot3.retired              
            , screenshot3.label                 
            , CAST(screenshot4.publishImageId AS TEXT)
            , screenshot4.projectId
            , screenshot4.caption
            , screenshot4.displayOrder
            , screenshot4.screenshotFileId 
            , screenshot4.renderingComplete     
            , screenshot4.retired              
            , screenshot4.label                 
            , CAST(screenshot5.publishImageId AS TEXT) 
            , screenshot5.projectId   
            , screenshot5.caption 
            , screenshot5.displayOrder 
            , screenshot5.screenshotFileId 
            , screenshot5.renderingComplete     
            , screenshot5.retired             
            , screenshot5.label                
            , CAST(screenshot6.publishImageId AS TEXT)
            , screenshot6.projectId
            , screenshot6.caption
            , screenshot6.displayOrder
            , screenshot6.screenshotFileId 
            , screenshot6.renderingComplete     
            , screenshot6.retired              
            , screenshot6.label                 
            , CAST(screenshot7.publishImageId AS TEXT)
            , screenshot7.projectId
            , screenshot7.caption
            , screenshot7.displayOrder
            , screenshot7.screenshotFileId 
            , screenshot7.renderingComplete     
            , screenshot7.retired              
            , screenshot7.label 
        FROM (
            SELECT
                  historyProjects.projectId
                , historyProjects.projectName
                , historyProjects.publishedName
                , historyProjects.createdDateTime
                , historyProjects.description
                , historyProjects.accountId
                , historyProjects.editedDateTime 
                , historyProjects.published 
                , historyProjects.urlName 
                , historyProjects.inviteOnly
                , historyProjects.licenseTypeId
                , historyProjects.nextPublishedVersionId
                , historyProjects.retired 
                , historyProjects.price 
                , historyProjects.autoSave 
                , historyProjects.guestListId
                , historyProjects.nextVersionId
            FROM projects historyProjects
            WHERE 1=1
            AND CASE 
                WHEN in_isPublished THEN 
                    historyProjects.published   = TRUE 
                    AND historyProjects.urlName = (
                        SELECT urlName 
                        FROM projects 
                        WHERE 1=1
                        AND published = true
                        AND retired = false
                        AND nextPublishedVersionId IS NULL
                        AND CASE WHEN in_projectId     IS NOT NULL THEN projectId = in_projectId ELSE TRUE END
                        AND CASE WHEN in_publishedName IS NOT NULL THEN publishedName = in_publishedName ELSE TRUE END
                        AND CASE WHEN in_urlName       IS NOT NULL THEN urlName = in_urlName ELSE TRUE END
                        LIMIT 1
                    )
                ELSE
                    historyProjects.projectName = (
                        SELECT projectName 
                        FROM projects 
                        WHERE 1=1
                        AND CASE WHEN in_projectId         IS NOT NULL THEN projectId = in_projectId             ELSE TRUE END
                        AND CASE WHEN in_projectName       IS NOT NULL THEN projectName = in_projectName         ELSE TRUE END
                        AND CASE WHEN in_retired           IS NOT NULL THEN retired = in_retired                 ELSE TRUE END
                        AND CASE WHEN in_searcherMemberId  IS NOT NULL THEN authorMemberId = in_searcherMemberId ELSE TRUE END
                        AND CASE WHEN in_searcherAccountId IS NOT NULL THEN accountId = in_searcherAccountId     ELSE TRUE END
                        AND nextVersionId IS NULL
                        LIMIT 1
                    )
            END
        ) AS innerProjects
            LEFT OUTER JOIN LicenseTypes
                ON innerProjects.licenseTypeId = LicenseTypes.licenseTypeId
            LEFT OUTER JOIN accountProjectNumbers historyApn
                ON innerProjects.projectId = historyApn.projectId
                AND historyApn.accountId = in_searcherAccountId
            LEFT OUTER JOIN publishimages icon 
                ON icon.projectId = innerProjects.projectId 
                AND icon.iconfileid IS NOT NULL 
                AND icon.retired = FALSE 
            LEFT OUTER JOIN publishimages screenshot0 
                ON screenshot0.projectId = innerProjects.projectId 
                AND screenshot0.displayOrder = 0 
                AND screenshot0.screenshotfileid IS NOT NULL 
                AND screenshot0.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot1 
                ON screenshot1.projectId = innerProjects.projectId 
                AND screenshot1.displayOrder = 1 
                AND screenshot1.screenshotfileid IS NOT NULL 
                AND screenshot1.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot2 
                ON screenshot2.projectId = innerProjects.projectId 
                AND screenshot2.displayOrder = 2 
                AND screenshot2.screenshotfileid IS NOT NULL 
                AND screenshot2.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot3 
                ON screenshot3.projectId = innerProjects.projectId 
                AND screenshot3.displayOrder = 3 
                AND screenshot3.screenshotfileid IS NOT NULL 
                AND screenshot3.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot4
                ON screenshot4.projectId = innerProjects.projectId 
                AND screenshot4.displayOrder = 4 
                AND screenshot4.screenshotfileid IS NOT NULL 
                AND screenshot4.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot5 
                ON screenshot5.projectId = innerProjects.projectId 
                AND screenshot5.displayOrder = 5
                AND screenshot5.screenshotfileid IS NOT NULL 
                AND screenshot5.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot6 
                ON screenshot6.projectId = innerProjects.projectId 
                AND screenshot6.displayOrder = 6
                AND screenshot6.screenshotfileid IS NOT NULL 
                AND screenshot6.retired = FALSE
            LEFT OUTER JOIN publishimages screenshot7
                ON screenshot7.projectId = innerProjects.projectId 
                AND screenshot7.displayOrder = 7 
                AND screenshot7.screenshotfileid IS NOT NULL 
                AND screenshot7.retired = FALSE
       WHERE CASE WHEN in_searcherAccountId IS NOT NULL THEN innerProjects.accountId = in_searcherAccountId ELSE TRUE END
ORDER BY innerProjects.createdDateTime DESC

;
    END
$$ LANGUAGE 'plpgsql' VOLATILE
COST 100; 
