-------------------------------------------------------------------------------
-- I'm only 99% certain that editeddatetime is default now().
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES(91);

alter table projects alter column editeddatetime set default now();

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
