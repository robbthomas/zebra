-------------------------------------------------------------------------------
-- Enforce not-null on createdDateTime, and fix columns with null values there.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES(88);

update projects set editeddatetime = now() where editeddatetime is null;
update projects set createddatetime = now() where createddatetime is null;
alter table projects alter column createddatetime set not null;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
