-------------------------------------------------------------------------------
-- r108_to_r109.sql
--
-- Author:  David Hoyt
-- Notes:  add column and values for default values for accountTypes.initialProductCodeId and recurringProductId
--
-------------------------------------------------------------------------------

alter table AccountTypes
    add column initialProductCodeId int4;
    
alter table AccountTypes
    add column recurringProductCodeId int4;

update AccountTypes
    set initialProductCodeId     = (select min(productCodeId) from ProductCodes where tag = '101-001-1' and effdate < now() and termdate > now())
        , recurringProductCodeId = (select min(productCodeId) from ProductCodes where tag = '102-001-1' and effdate < now() and termdate > now())
where tag = 'collector';

update AccountTypes
    set initialProductCodeId     = (select min(productCodeId) from ProductCodes where tag = '101-001-1' and effdate < now() and termdate > now())
        , recurringProductCodeId = (select min(productCodeId) from ProductCodes where tag = '102-002-1' and effdate < now() and termdate > now())
where tag = 'creator';

update AccountTypes
    set initialProductCodeId     = (select min(productCodeId) from ProductCodes where tag = '101-001-1' and effdate < now() and termdate > now())
        , recurringProductCodeId = (select min(productCodeId) from ProductCodes where tag = '102-003-1' and effdate < now() and termdate > now())
where tag = 'pro';


------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (109);
------------------------------------------------------