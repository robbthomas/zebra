

-- Function to truncate and rehydrate entire table.

CREATE OR REPLACE FUNCTION fn_publishedpurchasesacquisitions_rehydrate(in_credentials_email text, in_credentials_password text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;

    BEGIN
        var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN

            TRUNCATE TABLE PublishedPurchasesAcquisitions;

            INSERT INTO PublishedPurchasesAcquisitions(projectId, cachedPurchaseAcquisitionCount)
            SELECT projectId, COUNT(*)
            FROM AccountProjectNumbers
            WHERE Retired = 0
            GROUP BY projectId;

            RETURN 1;
        END IF;

        RETURN 0;

    END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;


-- Trigger to add new row to table any time row added to Projects.

DROP TRIGGER IF EXISTS trg_Projects_Insert ON Projects;

CREATE OR REPLACE FUNCTION fn_trg_Projects_Insert()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO PublishedPurchasesAcquisitions(projectId)
    SELECT NEW.projectId
    where NEW.published = true and NEW.hideInStoreList = false;
    RETURN NEW;
END;
$BODY$
Language 'plpgsql';

CREATE TRIGGER trg_Projects_Insert
    AFTER INSERT ON Projects
    FOR EACH ROW EXECUTE PROCEDURE fn_trg_Projects_Insert();


-- Trigger to update cached count any time row added to AccountProjectNumbers.

DROP TRIGGER IF EXISTS trg_AccountProjectNumbers_Insert ON AccountProjectNumbers;

CREATE OR REPLACE FUNCTION fn_trg_AccountProjectNumbers_Insert()
RETURNS TRIGGER AS
$BODY$
BEGIN
    UPDATE PublishedPurchasesAcquisitions SET cachedPurchaseAcquisitionCount = cachedPurchaseAcquisitionCount + 1
    WHERE projectId = NEW.projectId;
    RETURN NEW;
END;
$BODY$
Language 'plpgsql';

CREATE TRIGGER trg_AccountProjectNumbers_Insert
    AFTER INSERT ON AccountProjectNumbers
    FOR EACH ROW EXECUTE PROCEDURE fn_trg_AccountProjectNumbers_Insert();


