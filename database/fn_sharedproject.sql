--------------------------------------------------------------------------------
-- fn_sharedprojects.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- SharedProjects functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='SharedProjects'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('SharedProjects','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('SharedProjects','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('SharedProjects','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('SharedProjects','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('SharedProjects','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('SharedProjects','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_sharedproject_create(text, text, integer, text, timestamp, integer)

-- DROP FUNCTION fn_sharedproject_create(text, text, integer, text, timestamp, integer);

CREATE OR REPLACE FUNCTION fn_sharedproject_create(in_credentials_email text, in_credentials_password text, in_publishId integer, in_retiredById text, in_retiredDateTime timestamp, in_retiredReasonId integer)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'SharedProjects';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_sharedProjectId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO SharedProjects(
            publishId,
            retiredById,
            retiredDateTime,
            retiredReasonId
        )
        VALUES(
            in_publishId,
            in_retiredById,
            in_retiredDateTime,
            in_retiredReasonId
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_sharedProjectId := currval('seq_SharedProjects_sharedProjectId');
        END IF;

        return new_sharedProjectId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_sharedproject_create(text, text, integer, text, timestamp, integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_sharedproject_update(text, text, integer, integer, text, timestamp, integer, text)

-- DROP FUNCTION fn_sharedproject_update(text, text, integer, integer, text, timestamp, integer, text);

CREATE OR REPLACE FUNCTION fn_sharedproject_update(in_credentials_email text, in_credentials_password text, in_sharedProjectId integer, in_publishId integer, in_retiredById text, in_retiredDateTime timestamp, in_retiredReasonId integer, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'SharedProjects';
        var_functionName text := 'UPDATE';

        rec SharedProjects%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from SharedProjects where sharedProjectId = in_sharedProjectId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_publishId' THEN
                rec.publishId := in_publishId;
            ELSEIF param_name = 'in_retiredById' THEN
                rec.retiredById := in_retiredById;
            ELSEIF param_name = 'in_retiredDateTime' THEN
                rec.retiredDateTime := in_retiredDateTime;
            ELSEIF param_name = 'in_retiredReasonId' THEN
                rec.retiredReasonId := in_retiredReasonId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE SharedProjects SET
            publishId = rec.publishId,
            updatedDateTime = now(),
            retiredById = rec.retiredById,
            retiredDateTime = rec.retiredDateTime,
            retiredReasonId = rec.retiredReasonId
        WHERE sharedProjectId = in_sharedProjectId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_sharedproject_update(text, text, integer, integer, text, timestamp, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_sharedproject_retire(text, text, integer, text)

-- DROP FUNCTION fn_sharedproject_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_sharedproject_retire(in_credentials_email text, in_credentials_password text, in_sharedProjectId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'SharedProjects';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE SharedProjects SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        WHERE sharedProjectId = in_sharedProjectId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_sharedproject_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_sharedproject_filter(text, text, integer, integer, text, integer, text, timestamp, integer, text)

-- DROP FUNCTION fn_sharedproject_filter(text, text, integer, integer, text, integer, text, timestamp, integer, text);

CREATE OR REPLACE FUNCTION fn_sharedproject_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_publishId integer, in_retiredById text, in_retiredDateTime timestamp, in_retiredReasonId integer, in_filter_list text)
  RETURNS SETOF SharedProjects AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'SharedProjects';
        var_functionName text := 'FILTER';

        rec SharedProjects%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM SharedProjects WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_sharedProjectId' THEN
            ELSEIF filter_name = 'in_publishId' THEN
                var_query := var_query || ' AND publishId = ' || in_publishId;
            ELSEIF filter_name = 'in_retired' THEN
            ELSEIF filter_name = 'in_updatedDateTime' THEN
            ELSEIF filter_name = 'in_retiredById' THEN
                var_query := var_query || ' AND retiredById ILIKE ''%' || in_retiredById || '%''';
            ELSEIF filter_name = 'in_retiredDateTime' THEN
                var_query := var_query || ' AND retiredDateTime = ''' || in_retiredDateTime || '''';
            ELSEIF filter_name = 'in_retiredReasonId' THEN
                var_query := var_query || ' AND retiredReasonId = ' || in_retiredReasonId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM SharedProjects;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_sharedproject_filter(text, text, integer, integer, text, integer, text, timestamp, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_sharedproject_find(text, text, integer)

-- DROP FUNCTION fn_sharedproject_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_sharedproject_find(in_credentials_email text, in_credentials_password text, in_sharedProjectId integer)
  RETURNS SETOF SharedProjects AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'SharedProjects';
        var_functionName text := 'FIND';

        rec SharedProjects%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM SharedProjects
                WHERE sharedProjectId = in_sharedProjectId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_sharedproject_find(text, text, integer) OWNER TO postgres;
