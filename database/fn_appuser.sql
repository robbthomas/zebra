
-- fn_appuser.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
---------------------------------------------------------------------
-- App_User functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='App_User' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('App_User','CREATE','ROLE_TYPE_ANONYMOUS');
select * from fn_TableFunctionRoleMap_insert('App_User','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('App_User','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');

select * from fn_TableFunctionRoleMap_insert('App_User','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('App_User','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');

select * from fn_TableFunctionRoleMap_insert('App_User','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('App_User','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');

select * from fn_TableFunctionRoleMap_insert('App_User','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('App_User','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');

select * from fn_TableFunctionRoleMap_insert('App_User','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('App_User','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');



---------------------------------------------------------------------
-- Function: fn_appuser_create(text, text, text, text, text, text, text, text, text, text)

-- DROP FUNCTION fn_appuser_create(text, text, text, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_appuser_create(
    in_credentials_email text, in_credentials_password text,
    in_displayName text, in_description text, in_email text, in_firstName text, in_lastName text, in_nameFormat text, in_password text, in_passwordSalt text, in_editedById text)
  RETURNS text AS
$BODY$
    Declare
        var_tableName text := 'App_User';
        var_functionName text := 'CREATE';

        tries int := 10;
        new_nameFormat text;
        new_appUserId text;
        new_passwordHash text;
        nullable_editedById text;

        -- registration pages currently do not include a displayName (aka, username)
        -- so will use the email if no displayName is specified
        new_displayName text := in_email;

    BEGIN
/*
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;
*/

        -- generate an as yet unused random UUID-based Id for new entry but try
        -- only tries times before bailing and failing!?
        LOOP
            new_appUserId := replace(cast(uuid_generate_v4() As text),'-','');
            IF NOT EXISTS (select Id from App_User where Id = new_appUserId) THEN
                EXIT;
            END IF;
            tries := tries - 1;
            IF tries <= 0 THEN
                RETURN '';
            END IF;
        END LOOP;

        -- check for displayName parameter
        IF length(in_displayName) > 0 THEN
            new_displayName := in_displayName;
        END IF;

        -- NOTE that in org.springframework.security.authentication.encoding.BasePasswordEncoder
        -- the format for adding salt is "password{salt}", where {} chars are illegal in the salt,
        -- so the following may need to be altered if failing authentication in Spring
        new_passwordHash := encode(digest(in_password || coalesce(in_passwordSalt,''), 'sha1'), 'hex');

        -- editedByUserId can be null if new user creating own account so check for zero
        -- length and set to null if so
        nullable_editedById := in_editedById;
        IF LENGTH(TRIM(both from in_editedById)) = 0 THEN
            nullable_editedById := NULL;
        END IF;

        -- use default value
        new_nameFormat := in_nameFormat;
        IF (length(new_nameFormat) = 0) THEN
            new_nameFormat = 'displayName';
        END IF;

        insert into App_User(
            id,
            displayName,
            description,
            email,
            firstName,
            lastName,
            nameFormat,
            passwordHash,
            passwordSalt --,
            --editedById
        )
        values(
            new_appUserId,
            new_displayName,
            in_description,
            in_email,
            in_firstName,
            in_lastName,
            new_nameFormat,
            new_passwordHash,
            in_passwordSalt --,
            --nullable_editedById
        );

        return new_appUserId;
    End;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_appuser_create(text, text, text, text, text, text, text, text, text, text) OWNER TO postgres;

---------------------------------------------------------------------
-- Function: fn_appuser_update(text, text, text, integer, text, text, text, text, text, text, text, text, text, text, int,text)

-- DROP FUNCTION fn_appuser_update(text, text, text, integer, text, text, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_appuser_update(in_credentials_email text, in_credentials_password text, in_appUserId text, in_version integer, in_displayName text, in_description text, in_email text, in_firstName text, in_lastName text, in_nameFormat text, in_website text, in_twitter text, in_password text, in_editedById text, in_retiredById text, in_param_list text)
  RETURNS integer AS
$BODY$
    Declare
        var_credentialsId text;
        var_tableName text := 'App_User';
        var_functionName text := 'UPDATE';

        rec App_User%ROWTYPE;
        param_name text;
        var_count int := 1;
        dtnow timestamp := now();

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from App_User where Id = in_appUserId;

        -- optimistic locking check -- verify row to be updated matches incoming data
--        IF rec.version <> in_version THEN
--            RETURN -1;
--        END IF;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_displayName' THEN
                rec.displayName := in_displayName;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_email' THEN
                rec.email := in_email;
            ELSEIF param_name = 'in_firstName' THEN
                rec.firstName := in_firstName;
            ELSEIF param_name = 'in_lastName' THEN
                rec.lastName := in_lastName;
            ELSEIF param_name = 'in_nameFormat' THEN
                rec.nameFormat := in_nameFormat;
            ELSEIF param_name = 'in_password' THEN
                rec.passwordHash := encode(digest(in_password || coalesce(rec.passwordSalt,''), 'sha1'), 'hex');
            ELSEIF param_name = 'in_website' THEN
                rec.website := in_website;
            ELSEIF param_name = 'in_twitter' THEN
                rec.twitter := in_twitter;
/*
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
                rec.editedDateTime := dtnow;
*/
            ELSEIF param_name = 'in_retiredById' THEN
                rec.retiredById := in_retiredById;
                rec.retiredDateTime := dtnow;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE App_User SET
            version = rec.version + 1,
            displayName = rec.displayName,
            description = rec.description,
            email = rec.email,
            firstName = rec.firstName,
            lastName = rec.lastName,
            nameFormat = rec.nameFormat,
            passwordHash = rec.passwordHash,
            website = rec.website,
            twitter = rec.twitter,
            --editedById = rec.editedById,
            --editedDateTime = rec.editedDateTime,
            retiredById = rec.retiredById,
            retiredDateTime = rec.retiredDateTime
        WHERE Id = in_appUserId; -- AND version = rec.version;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    End;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_appuser_update(text, text, text, integer, text, text, text, text, text, text, text, text, text) OWNER TO postgres;

---------------------------------------------------------------------
-- Function: fn_appuser_retire(text, text, text, text)

-- DROP FUNCTION fn_appuser_retire(text, text, text, text);

CREATE OR REPLACE FUNCTION fn_appuser_retire(in_credentials_email text, in_credentials_password text, in_appUserId text, in_retiredById text)
  RETURNS integer AS
$BODY$
    Declare
        var_credentialsId text;
        var_tableName text := 'App_User';
        var_functionName text := 'RETIRE';
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE App_User SET retired = 't', retiredById = in_retiredById, retiredDateTime = now()
        WHERE Id = in_appUserId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    End;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_appuser_retire(text, text, text, text) OWNER TO postgres;

---------------------------------------------------------------------
-- Function: fn_appuser_filter(text, text, integer, integer, text, text, text, text, text, text, text, timestamp with time zone, text, timestamp with time zone, text)

-- DROP FUNCTION fn_appuser_filter(text, text, integer, integer, text, text, text, text, text, text, text, timestamp with time zone, text, timestamp with time zone, text);

DROP TYPE IF EXISTS _type_AppUser_Filter_ CASCADE;

/*
CREATE TYPE _type_AppUser_Filter_ AS (
    displayName text, -- App_User
    email text,
    firstName text,
    lastName text,
    nameFormat text,
    description text,
    accountName text, -- Accounts
    accountDescription text,
    --accountTypeTag text, -- AccountTypes
    --accountTypeName text,
    --roleTypeTag text, -- RoleTypes
    --roleTypeName text,
    --companyName text, -- Companies.name corresponding to Accounts.companyId
    --addressTypeTag text, -- AddressTelephoneTypes -- should be for "primary"
    --addressTypeName text,
    addressName text, -- Addresses -- optional name to use with address (should be blank for primary address)
    addressCompany text, -- optional company name for address (should be blank for primary address)
    address1 text,
    address2 text,
    city text,
    stateProvince text,
    zipPostalCode text,
    country text
);
*/

CREATE OR REPLACE FUNCTION fn_appuser_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_displayName text, in_description text, in_email text, in_firstName text, in_lastName text, in_nameFormat text, in_editedById text, in_editedDateTime timestamp with time zone, in_retiredById text, in_retiredDateTime timestamp with time zone, in_filter_list text)
  RETURNS SETOF App_User AS
  --RETURNS SETOF _type_AppUser_Filter_ AS
$BODY$
    DECLARE
--        var_credentialsId text;
        var_tableName text := 'App_User';
        var_functionName text := 'FILTER';

        rec App_User%ROWTYPE;
        --rec _type_AppUser_Filter_%ROWTYPE;
        filter_name text;
        var_order text;
        var_count int := 1;
        var_query text := 'SELECT * FROM App_User au WHERE retired = ''f''';
/*
        var_query text :=
                    'SELECT displayName, email, firstName, lastName, nameFormat, au.description, '
                 || ' acct.name as accountName, acct.description as accountDescription, '
                 || ' addr.name as addressName, company, address1, address2, city, stateProvince, zipPostalCode, country '
                 || ' FROM AccountMemberRoles amr, App_User au, Accounts acct, Addresses addr '
                 || ' WHERE au.retired = ''f'' AND amr.memberId = au.id AND amr.typeId = 1 AND amr.accountId = acct.accountId AND amr.accountId = addr.accountId ';
*/
    BEGIN
--      var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

--      IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_displayName' THEN
                var_query := var_query || ' AND au.displayName = ''' || in_displayName || '''';   -- must be exact match
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND au.description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_email' THEN
                var_query := var_query || ' AND  au.email ILIKE ''%' || in_email || '%''';
            ELSEIF filter_name = 'in_firstName' THEN
                var_query := var_query || ' AND  au.firstName ILIKE ''%' || in_firstName || '%''';
            ELSEIF filter_name = 'in_lastName' THEN
                var_query := var_query || ' AND  au.lastName ILIKE ''%' || in_lastName || '%''';
            ELSEIF filter_name = 'in_nameFormat' THEN
                var_query := var_query || ' AND  au.nameFormat ILIKE ''%' || in_nameFormat || '%''';
/*
            ELSEIF param_name = 'in_editedById' THEN
                var_query := var_query || ' AND  au.editedById ILIKE ''%' || in_editedById || '%''';
            ELSEIF param_name = 'in_editedDateTime' THEN
                var_query := var_query || ' AND  au.editedDateTime = ' || in_editedDateTime;
*/
            ELSEIF param_name = 'in_retiredById' THEN
                var_query := var_query || ' AND  au.retiredById ILIKE ''%' || in_retiredById || '%''';
            ELSEIF param_name = 'in_retiredDateTime' THEN
                var_query := var_query || ' AND  au.retiredDateTime = ' || in_retiredDateTime;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM App_User;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

--      END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_appuser_filter(text, text, integer, integer, text, text, text, text, text, text, text, timestamp with time zone, text, timestamp with time zone, text) OWNER TO postgres;

---------------------------------------------------------------------
-- Function: fn_appuser_getbyid(text, text, text)

DROP TYPE IF EXISTS _type_App_User_Custom_ CASCADE; 

CREATE TYPE _type_App_User_Custom_ AS (
      id VARCHAR(255)
    , version INT8
    , date_created TIMESTAMP
    , retired BOOLEAN
    , description VARCHAR(255)
    , email VARCHAR(255)
    , emailshow BOOLEAN
    , enabled BOOLEAN
    , last_updated TIMESTAMP
    , passwordhash VARCHAR(255)
    , displayname VARCHAR(255)
    , editedById VARCHAR(255)
    , retiredById VARCHAR(255)
    , retiredDateTime TIMESTAMP
    , passwordSalt VARCHAR(128)
    , firstName VARCHAR(128)
    , lastName VARCHAR(128)
    , website VARCHAR(255)
    , twitter VARCHAR(255)
    , nameFormat TEXT
    , avatarId INT4
    , bannerId INT4
    , addressCompany TEXT
);

DROP FUNCTION IF EXISTS fn_appuser_getbyid(text, text, text);

CREATE OR REPLACE FUNCTION fn_appuser_getbyid(in_credentials_email text, in_credentials_password text, in_appUserId text)
  RETURNS SETOF _type_App_User_Custom_ AS
$BODY$
    DECLARE
--        var_credentialsId text;
        var_tableName text := 'App_User';
        var_functionName text := 'FIND';

        rec _type_App_User_Custom_;

    BEGIN
--        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

--        IF var_credentialsId IS NOT NULL THEN

            FOR rec IN
                SELECT DISTINCT App_User.*, accounts.bannerId, addresses.company AS addressCompany
                FROM App_User
                    LEFT JOIN accountMemberRoles
                        ON App_User.id = accountMemberRoles.memberId
                        LEFT JOIN accounts
                            ON accountMemberRoles.accountId = accounts.accountId
                            LEFT JOIN addresses
                                INNER JOIN addressTelephoneTypes
                                    ON addresses.typeId = addressTelephoneTypes.addressTelephoneTypeId
                                    AND addressTelephoneTypes.name = 'Primary'
                                ON accounts.accountId = addresses.accountId
                                
                WHERE Id = in_appUserId
            LOOP
                RETURN NEXT rec;
            END LOOP;

--        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION fn_appuser_getbyid(text, text, text) OWNER TO postgres;