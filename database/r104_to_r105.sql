-------------------------------------------------------------------------------
-- r104_to_r105.sql
--
-- Author:  David Hoyt
-- Notes:  Fix not-null on new column in accountprojectnumbers
--
-------------------------------------------------------------------------------

update accountprojectnumbers
    set createddatetime = projects.createddatetime
     from projects
where
       projects.projectid = accountprojectnumbers.projectid
       and accountprojectnumbers.createddatetime is null
;


alter table accountprojectnumbers alter column createddatetime set default now();
--alter table accountprojectnumbers alter column createddatetime set not null;

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (105);
------------------------------------------------------

