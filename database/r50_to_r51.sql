-------------------------------------------------------------------------------
-- r50_to_r51.sql
--
-- Create tables for tracking sharing of zapps, and email invites
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (51);
-------------------------------------------------------------------------------

alter table SharedProjectInvitees add column userBody text;
alter table SharedProjectInvitees add column firstName text;
alter table SharedProjectInvitees add column lastName text;

\i fn_sharedprojectinvitee.sql
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
