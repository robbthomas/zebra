-------------------------------------------------------------------------------
-- r109_to_r110.sql
--
-- Author:  Jesse Coyle
-- Notes:
--
-------------------------------------------------------------------------------

-- term out invalid credit card rows
update creditcards set retired = 1
where customerpaymentprofileid is null;

-- update professional price to reflect actual desired price
update productcodes
set price = 3995
where price = 3499;

update productcodes
set price = -3995
where price = -3499;

-- update creator price to reflect actual desired price
update productcodes
set price = 895
where price = 899;

update productcodes
set price = -895
where price = -899;


-- Fix account type productcodeid for initial account creation
update accounttypes
set initialproductcodeid = 100
where accounttypeid = 1;

update accounttypes
set initialproductcodeid = 103
where accounttypeid = 2;

update accounttypes
set initialproductcodeid = 106
where accounttypeid = 3;



alter table productcodes drop column if exists terms;
alter table productcodes add column terms text;

update productcodes
set terms = atype.terms
from AccountTypes atype
where productCodeId = atype.initialproductcodeid
      or productCodeId = atype.recurringProductCodeId
;


-- Fix duplicate apn rows as keyed by (accountid, projectid) by retiring all but one of the duplicates
with duplicateApn(accountid, projectid, accountprojectnumberid, total) as (
    select apn.accountid, apn.projectid, max(accountprojectnumberid), count(*)
    from accountprojectnumbers apn
    where apn.retired = 0
    group by apn.accountid, apn.projectid
    having count(*) > 1
)
update accountprojectnumbers
set retired = case when accountprojectnumbers.accountprojectnumberid = duplicateApn.accountprojectnumberid then 0 else 1 end
from duplicateApn
where accountprojectnumbers.retired = 0
      and duplicateApn.accountid = accountprojectnumbers.accountid
      and duplicateApn.projectid = accountprojectnumbers.projectid;


-- Fix duplicate parent_child rows by copying distinct rows to a temp table and back again
select distinct * into r111_temp_parent_child from parent_child;
truncate table parent_child;
insert into parent_child select * from r111_temp_parent_child;
drop table r111_temp_parent_child;

------------------------------------------------------
-- Increment the database version - DO NOT remove this line
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (111);
------------------------------------------------------