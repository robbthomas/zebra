-------------------------------------------------------------------------------
-- created_date fields formerly managed by a Spring pointcut/asset.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES(90);

----------------------
-- prep
----------------------

-- current data is useless: start fresh
delete from invoicelineitems where productcodeid in (select productcodeid from productcodes);
delete from productcodeprices;
delete from productcodes;

drop table if exists productcategories;
drop sequence if exists seq_productcategoryid;

----------------------
-- product categories
----------------------
drop sequence if exists seq_productcategoryid;
create sequence seq_productcategoryid start with 100 increment by 1;
create table ProductCategories (
    productCategoryId integer  primary key default nextval('seq_productcategoryid'),
    parentCategoryId  integer  references ProductCategories(productcategoryId),
    tag               varchar(32)
);
create unique index ix_productcategories_tag on ProductCategories(tag);

insert into productcategories(tag) values('SUBSCRIPTION');
insert into productcategories(tag) values('SHOPPS');
insert into productcategories(tag) values('CLOUD_SERVICES');

insert into productcategories(tag, parentcategoryid) values('SUBSCRIPTION_INITIAL', (select productcategoryid from productcategories where tag = 'SUBSCRIPTION'));
insert into productcategories(tag, parentcategoryid) values('SUBSCRIPTION_RECURRING', (select productcategoryid from productcategories where tag = 'SUBSCRIPTION'));
insert into productcategories(tag, parentcategoryid) values('SUBSCRIPTION_UPGRADE', (select productcategoryid from productcategories where tag = 'SUBSCRIPTION'));
insert into productcategories(tag, parentcategoryid) values('SUBSCRIPTION_DOWNGRADE', (select productcategoryid from productcategories where tag = 'SUBSCRIPTION'));

insert into productcategories(tag, parentCategoryId) values('SUBSCRIPTION_CANCEL_INITIAL', (select productcategoryid from productcategories where tag = 'SUBSCRIPTION_INITIAL'));
insert into productcategories(tag, parentCategoryId) values('SUBSCRIPTION_CANCEL_RECURRING', (select productcategoryid from productcategories where tag = 'SUBSCRIPTION_RECURRING'));
insert into productcategories(tag, parentCategoryId) values('SUBSCRIPTION_CANCEL_UPGRADE',  (select productcategoryid from productcategories where tag = 'SUBSCRIPTION_UPGRADE'));
insert into productcategories(tag, parentCategoryId) values('SUBSCRIPTION_CANCEL_DOWNGRADE', (select productcategoryid from productcategories where tag = 'SUBSCRIPTION_DOWNGRADE'));

insert into productcategories(tag, parentCategoryId) values('SHOPPS_RETURN', (select productcategoryid from productcategories where tag = 'SHOPPS'));
insert into productcategories(tag, parentCategoryId) values('SHOPPS_RECURRING', (select productcategoryid from productcategories where tag = 'SHOPPS'));
insert into productcategories(tag, parentCategoryId) values('SHOPPS_RETURN_RECURRING', (select productcategoryid from productcategories where tag = 'SHOPPS_RECURRING'));

insert into productcategories(tag, parentCategoryId) values('CLOUD_SERVICES_INITIAL', (select productcategoryid from productcategories where tag = 'CLOUD_SERVICES'));
insert into productcategories(tag, parentCategoryId) values('CLOUD_SERVICES_RECURRING', (select productcategoryid from productcategories where tag = 'CLOUD_SERVICES'));
insert into productcategories(tag, parentCategoryId) values('CLOUD_SERVICES_UPGRADE', (select productcategoryid from productcategories where tag = 'CLOUD_SERVICES'));
insert into productcategories(tag, parentCategoryId) values('CLOUD_SERVICES_DOWNGRADE', (select productcategoryid from productcategories where tag = 'CLOUD_SERVICES'));

insert into productcategories(tag, parentCategoryId) values('CLOUD_SERVICES_CANCEL_INITIAL', (select productcategoryid from productcategories where tag = 'CLOUD_SERVICES_INITIAL'));
insert into productcategories(tag, parentCategoryId) values('CLOUD_SERVICES_CANCEL_RECURRING', (select productcategoryid from productcategories where tag = 'CLOUD_SERVICES_RECURRING'));
insert into productcategories(tag, parentCategoryId) values('CLOUD_SERVICES_CANCEL_UPGRADE', (select productcategoryid from productcategories where tag = 'CLOUD_SERVICES_UPGRADE'));
insert into productcategories(tag, parentCategoryId) values('CLOUD_SERVICES_CANCEL_DOWNGRADE', (select productcategoryid from productcategories where tag = 'CLOUD_SERVICES_DOWNGRADE'));


------------------------
-- ProductTypes table
------------------------
drop table if exists ProductTypes;
create table ProductTypes (
    productTypeId integer primary key not null,
    tag varchar(16)
);
create unique index ix_productTypes_tag on ProductTypes(tag);

insert into ProductTypes (productTypeId, tag) values (1, 'COM');
insert into ProductTypes (productTypeId, tag) values (2, 'EDU');
insert into ProductTypes (productTypeId, tag) values (3, 'GOV');


------------------------
-- ProductCodes table
------------------------

-- since there will be many rows in this table, use a sequence to manage ID
drop sequence if exists seq_productcodeid;
create sequence seq_productcodeid start with 100;
alter table productcodes alter column productcodeid set default nextval('seq_productcodeid');

-- new columns
alter table productcodes add column productCategoryId   integer not null references ProductCategories(productCategoryId);
alter table productcodes add column refundDays          integer;
alter table productcodes add column termDate            date;
alter table productcodes add column effDate             date;
alter table productcodes add column price               int;
alter table productcodes add column productTypeId       integer not null references ProductTypes(productTypeId);
alter table productcodes add column discountType        varchar(256);

-- obsoleted by eff and term
alter table productcodes drop column retired;
alter table productcodes drop column retiredDateTime;
alter table productcodes drop column retiredById;
alter table productcodes drop column retiredreasontypeid;

-- these were created as 'text' by the QPI framework tools.  make them varchar.
alter table productcodes drop column tag;
alter table productcodes drop column name;
alter table productcodes drop column description;
alter table productcodes add column tag varchar(16);
alter table productcodes add column description varchar(64);

-- never used
alter table productcodes drop column displayorder;

-- create indexes:
-- FIXME:  can't create an index against a dynamic value, such as the results of now()
--create index ix_unique_productcode on ProductCodes(tag, productCategoryId, productTypeId, discountType) where effDate < now() and termDate > now();
create index ix_productcode_tag on ProductCodes(tag);

-- table columns at this point:
--  productcodeid     productcategoryid  tag    description     discounttype    producttypeid   price     refunddays     termdate     effdate                                  
\i r84_to_r85/fn_tmp_create_productcode.sql


-- data entry
-- 
-- reference:  select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_INITIAL','101-001-1','Initial Subscription: Collector','Individual','COM',0,0 );

select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_INITIAL','101-001-1','Initial Subscription: Collector','INDIVIDUAL','COM',0,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_INITIAL','101-002-1','Initial Subscription: Creator','INDIVIDUAL','COM',899,30);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_INITIAL','101-003-1','Initial Subscription: Professional','INDIVIDUAL','COM',3499,30);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_INITIAL','101-003-2','Initial Subscription: Professional (Org)','ORGANIZATION','COM',3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_INITIAL','101-004-1','Initial Subscription: Enterprise','INDIVIDUAL','COM',6499,30);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_INITIAL','101-004-2','Initial Subscription: Enterprise (Org)','ORGANIZATION','COM',6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING','102-001-1','Recurring Subscription: Collector','INDIVIDUAL','COM',0,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING','102-002-1','Recurring Subscription: Creator','INDIVIDUAL','COM',899,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING','102-003-1','Recurring Subscription: Professional','INDIVIDUAL','COM',3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING','102-003-2','Recurring Subscription: Professional (Org)','ORGANIZATION','COM',3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING','102-004-1','Recurring Subscription: Enterprise','INDIVIDUAL','COM',6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING','102-004-2','Recurring Subscription: Enterprise (Org)','ORGANIZATION','COM',6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING','102-101-2','Consumer Subscription (Org)','ORGANIZATION','COM',100,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-012-1','Subscription Upgrade: Collector to Creator','INDIVIDUAL','COM',899,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-013-1','Subscription Upgrade: Collector to Professional','INDIVIDUAL','COM',3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-013-2','Subscription Upgrade: Collector to Professional','ORGANIZATION','COM',3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-014-1','Subscription Upgrade: Collector e to Enterprise','INDIVIDUAL','COM',6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-014-2','Subscription Upgrade: Collector to Enterprise (Org)','ORGANIZATION','COM',6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-023-1','Subscription Upgrade: Creator to Professional','INDIVIDUAL','COM',3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-023-2','Subscription Upgrade: Creator to Professional (Org)','ORGANIZATION','COM',3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-024-1','Subscription Upgrade: Creator to Enterprise','INDIVIDUAL','COM',6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-024-2','Subscription Upgrade: Creator to Enterprise (Org)','ORGANIZATION','COM',6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-034-1','Subscription Upgrade: Professional to Enterprise','INDIVIDUAL','COM',6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_UPGRADE','103-034-2','Subscription Upgrade: Professional to Enterprise (Org)','ORGANIZATION','COM',6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_DOWNGRADE','104-021-1','Subscription Downgrade: Creator  to Collector','INDIVIDUAL','COM',0,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_DOWNGRADE','104-031-1','Subscription Downgrade: Professional to Collector','INDIVIDUAL','COM',0,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_DOWNGRADE','104-041-1','Subscription Downgrade: Enterprise to Collector','INDIVIDUAL','COM',0,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_DOWNGRADE','104-032-1','Subscription Downgrade: Professional to Creator','INDIVIDUAL','COM',0,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_DOWNGRADE','104-042-1','Subscription Downgrade: Enterprise to Creator','INDIVIDUAL','COM',899,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_DOWNGRADE','104-043-1','Subscription Downgrade: Enterprise to Professional','INDIVIDUAL','COM',3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_DOWNGRADE','104-043-2','Subscription Downgrade: Enterprise to Professional','ORGANIZATION','COM',3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_INITIAL','105-001-1','Cancel Subscription: Collector','INDIVIDUAL','COM',0,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_INITIAL','105-002-1','Cancel Subscription: Creator','INDIVIDUAL','COM',-899,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_INITIAL','105-003-1','Cancel Subscription: Professional','INDIVIDUAL','COM',-3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_INITIAL','105-003-2','Cancel Subscription: Professional (Org)','ORGANIZATION','COM',-3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_INITIAL','105-004-1','Cancel Subscription: Enterprise','INDIVIDUAL','COM',-6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_INITIAL','105-004-2','Cancel Subscription: Enterprise (Org)','ORGANIZATION','COM',-6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_RECURRING','106-001-1','Cancel Subscription: Collector','INDIVIDUAL','COM',0,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_RECURRING','106-002-1','Cancel Subscription: Creator','INDIVIDUAL','COM',-899,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_RECURRING','106-003-1','Cancel Subscription: Professional','INDIVIDUAL','COM',-3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_RECURRING','106-003-2','Cancel Subscription: Professional (Org)','ORGANIZATION','COM',-3499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_RECURRING','106-004-1','Cancel Subscription: Enterprise','INDIVIDUAL','COM',-6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_RECURRING','106-004-2','Cancel Subscription: Enterprise (Org)','ORGANIZATION','COM',-6499,0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_CANCEL_RECURRING','106-004-2','Cancel Consumer Subscription (Org)','ORGANIZATION','COM',-100,0);
select * from fn_tmp_insert_prodcode ('SHOPPS_RECURRING','202-301-0','Zapp','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('SHOPPS_RECURRING','202-302-0','Gadget','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('SHOPPS_RETURN_RECURRING','206-305-0','Returned Zapp','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('SHOPPS_RETURN_RECURRING','206-306-0','Returned Gadget','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_RECURRING','302-501-0','Additional Storage ____ MB (1)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_RECURRING','302-502-0','Additional Storage ____ MB (2)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_RECURRING','302-503-0','Additional Storage ____ MB (3)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_RECURRING','302-551-0','Additional Bandwidth ____ MB (1)','NA','COM',0,0);

select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_RECURRING','302-552-0','Additional Bandwidth ____ MB (2)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_RECURRING','302-553-0','Additional Bandwidth ____ MB (3)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_UPGRADE','303-511-0','Additional Storage Upgrade:  ____ MB (1) to ____ MB (2)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_UPGRADE','303-512-0','Additional Storage Upgrade:  ____ MB (1) to ____ MB (3)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_UPGRADE','303-513-0','Additional Storage Upgrade:  ____ MB (2) to ____ MB (3)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_UPGRADE','303-561-0','Additional Bandwidth Upgrade:  ____ MB (1) to ____ MB (2)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_UPGRADE','303-562-0','Additional Bandwidth Upgrade:  ____ MB (1) to ____ MB (3)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_UPGRADE','303-563-0','Additional Bandwidth Upgrade:  ____ MB (2) to ____ MB (3)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_DOWNGRADE','304-521-0','Additional Storage Downgrade:  ____ MB (2) to ____ MB (1)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_DOWNGRADE','304-522-0','Additional Storage Downgrade:  ____ MB (3) to ____ MB (1)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_DOWNGRADE','304-523-0','Additional Storage Downgrade:  ____ MB (3) to ____ MB (2)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_DOWNGRADE','304-571-0','Additional Bandwidth Downgrade:  ____ MB (2) to ____ MB (1)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_DOWNGRADE','304-572-0','Additional Bandwidth Downgrade:  ____ MB (3) to ____ MB (1)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_DOWNGRADE','304-573-0','Additional Bandwidth Downgrade:  ____ MB (3) to ____ MB (2)','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_CANCEL_RECURRING','306-501-0','Additional Storage ____ MB','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_CANCEL_RECURRING','306-502-0','Additional Storage ____ MB','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_CANCEL_RECURRING','306-503-0','Additional Storage ____ MB','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_CANCEL_RECURRING','306-551-0','Additional Bandwidth ____ MB','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_CANCEL_RECURRING','306-552-0','Additional Bandwidth ____ MB','NA','COM',0,0);
select * from fn_tmp_insert_prodcode ('CLOUD_SERVICES_CANCEL_RECURRING','306-553-0','Additional Bandwidth ____ MB','NA','COM',0,0);

select * from fn_tmp_insert_prodcode ('SHOPPS', '202-401-0', 'Transferred Project', 'NA', 'COM', 0, 0);
select * from fn_tmp_insert_prodcode ('SHOPPS', '202-402-0', 'Auto-Purchase Nested Gadget in Transferred Project', 'NA', 'COM', 0, 0);
select * from fn_tmp_insert_prodcode ('SHOPPS_RETURN', '202-401-1', 'Return of Transferred Project', 'NA', 'COM', 0, 0);
select * from fn_tmp_insert_prodcode ('SHOPPS_RETURN', '202-402-1', 'Return of Auto-Purchase Nested Gadget in Transferred Project', 'NA', 'COM', 0, 0);

select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING', '401-100-0', 'Account Learners FREE', 'INDIVIDUAL', 'COM', 0, 0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING', '401-101-0', 'Account Learners 100', 'INDIVIDUAL', 'COM', 5000, 0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING', '401-102-0', 'Account Learners 500', 'INDIVIDUAL', 'COM', 25000, 0);
select * from fn_tmp_insert_prodcode ('SUBSCRIPTION_RECURRING', '401-103-0', 'Account Learners 1000', 'INDIVIDUAL', 'COM', 50000, 0);

drop function fn_tmp_insert_prodcode(varchar(16), varchar(16), varchar(64), varchar(16), varchar(16), integer, integer);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
