--------------------------------------------------------------------------------
-- fn_financialtransactions.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- FinancialTransactions functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='FinancialTransactions' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('FinancialTransactions','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_financialtransaction_create(text, text, integer, integer, integer, integer, text, text, integer, text, text, integer, text)

-- DROP FUNCTION fn_financialtransaction_create(text, text, integer, integer, integer, integer, text, text, integer, text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_financialtransaction_create(in_credentials_email text, in_credentials_password text, in_accountId integer, in_amount integer, in_perTransactionFee integer, in_currencyTypeId integer, in_label text, in_type text, in_agentId integer, in_cimTransactionId text, in_status text, in_statusReasonCode integer, in_statusReasonText text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'FinancialTransactions';
        var_functionName text := 'CREATE';

        new_financialTransactionId int := -1;
        var_count int := -1;
        var_typeId int;
        var_statusId int;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_typeId := financialTransactionTypeId FROM FinancialTransactionTypes WHERE tag ILIKE in_type;
        var_statusId := financialTransactionStatusId FROM FinancialTransactionStatus WHERE tag ILIKE in_status;

        insert into FinancialTransactions(
            accountId,
            amount,
            perTransactionFee,
            currencyTypeId,
            label,
            typeId,
            agentId,
            cimTransactionId,
            statusId,
            statusReasonCode,
            statusReasonText
        )
        values(
            in_accountId,
            in_amount,
            in_perTransactionFee,
            in_currencyTypeId,
            in_label,
            var_typeId,
            in_agentId,
            in_cimTransactionId,
            var_statusId,
            in_statusReasonCode,
            in_statusReasonText
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_financialTransactionId := currval('seq_FinancialTransactions_financialTransactionId');
        END IF;

        return new_financialTransactionId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_financialtransaction_create(text, text, integer, integer, integer, integer, text, text, integer, text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_financialtransaction_update(text, text, integer, integer, integer, integer, integer, text, text, integer, text, text, integer, text, text)

-- DROP FUNCTION fn_financialtransaction_update(text, text, integer, integer, integer, integer, integer, text, text, integer, text, text, integer, text, text);

CREATE OR REPLACE FUNCTION fn_financialtransaction_update(in_credentials_email text, in_credentials_password text, in_financialTransactionId integer, in_accountId integer, in_amount integer, in_perTransactionFee integer, in_currencyTypeId integer, in_label text, in_type text, in_agentId integer, in_cimTransactionId text, in_status text, in_statusReasonCode integer, in_statusReasonText text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'FinancialTransactions';
        var_functionName text := 'UPDATE';

        rec FinancialTransactions%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from FinancialTransactions where financialTransactionId = in_financialTransactionId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_accountId' THEN
                rec.accountId := in_accountId;
            ELSEIF param_name = 'in_amount' THEN
                rec.amount := in_amount;
            ELSEIF param_name = 'in_perTransactionFee' THEN
                rec.perTransactionFee := in_perTransactionFee;
            ELSEIF param_name = 'in_currencyTypeId' THEN
                rec.currencyTypeId := in_currencyTypeId;
            ELSEIF param_name = 'in_label' THEN
                rec.label := in_label;
            ELSEIF param_name = 'in_type' THEN
                rec.typeId := financialTransactionTypeId FROM FinancialTransactionTypes WHERE tag ILIKE in_type;
            ELSEIF param_name = 'in_agentId' THEN
                rec.agentId := in_agentId;
            ELSEIF param_name = 'in_cimTransactionId' THEN
                rec.cimTransactionId := in_cimTransactionId;
            ELSEIF param_name = 'in_status' THEN
                rec.statusId := financialTransactionStatusId FROM FinancialTransactionStatus WHERE tag ILIKE in_status;
            ELSEIF param_name = 'in_statusReasonCode' THEN
                rec.statusReasonCode := in_statusReasonCode;
            ELSEIF param_name = 'in_statusReasonText' THEN
                rec.statusReasonText := in_statusReasonText;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE FinancialTransactions SET
            accountId = rec.accountId,
            amount = rec.amount,
            perTransactionFee = rec.perTransactionFee,
            currencyTypeId = rec.currencyTypeId,
            label = rec.label,
            typeId = rec.typeId,
            agentId = rec.agentId,
            cimTransactionId = rec.cimTransactionId,
            statusId = rec.statusId,
            statusReasonCode = rec.statusReasonCode,
            statusReasonText = rec.statusReasonText
        WHERE financialTransactionId = in_financialTransactionId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_financialtransaction_update(text, text, integer, integer, integer, integer, integer, text, text, integer, text, text, integer, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_financialtransaction_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, text, integer, text, text, integer, text, text)

-- DROP FUNCTION fn_financialtransaction_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, text, integer, text, text, integer, text, text);

CREATE OR REPLACE FUNCTION fn_financialtransaction_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_accountId integer, in_amount integer, in_perTransactionFee integer, in_currencyTypeId integer, in_label text, in_type text, in_agentId integer, in_cimTransactionId text, in_status text, in_statusReasonCode integer, in_statusReasonText text, in_filter_list text)
  RETURNS SETOF FinancialTransactions AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'FinancialTransactions';
        var_functionName text := 'FILTER';

        rec FinancialTransactions%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM FinancialTransactions WHERE 0 = 0';
        var_typeId int;
        var_statusId int;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_financialTransactionId' THEN
            ELSEIF filter_name = 'in_accountId' THEN
                var_query := var_query || ' AND accountId = ' || in_accountId;
            ELSEIF filter_name = 'in_amount' THEN
                var_query := var_query || ' AND amount = ' || in_amount;
            ELSEIF filter_name = 'in_perTransactionFee' THEN
                var_query := var_query || ' AND perTransactionFee = ' || in_perTransactionFee;
            ELSEIF filter_name = 'in_currencyTypeId' THEN
                var_query := var_query || ' AND currencyTypeId = ' || in_currencyTypeId;
            ELSEIF filter_name = 'in_label' THEN
                var_query := var_query || ' AND label ILIKE ''%' || in_label || '%''';
            ELSEIF filter_name = 'in_type' THEN
                var_typeId := financialTransactionTypeId FROM FinancialTransactionTypes WHERE tag ILIKE in_type;
                var_query := var_query || ' AND typeId = ' || var_typeId;
            ELSEIF filter_name = 'in_agentId' THEN
                var_query := var_query || ' AND agentId = ' || in_agentId;
            ELSEIF filter_name = 'in_cimTransactionId' THEN
                var_query := var_query || ' AND cimTransactionId ILIKE ''%' || in_cimTransactionId || '%''';
            ELSEIF filter_name = 'in_status' THEN
                var_statusId := financialTransactionStatusId FROM FinancialTransactionStatuses WHERE tag ILIKE in_type;
                var_query := var_query || ' AND statusId = ' || var_statusId;
            ELSEIF filter_name = 'in_statusReasonCode' THEN
                var_query := var_query || ' AND statusReasonCode = ' || in_statusReasonCode;
            ELSEIF filter_name = 'in_statusReasonText' THEN
                var_query := var_query || ' AND statusReasonText ILIKE ''%' || in_statusReasonText || '%''';
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM FinancialTransactions;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_financialtransaction_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, text, integer, text, text, integer, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_financialtransaction_find(text, text, integer)

-- DROP FUNCTION fn_financialtransaction_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_financialtransaction_find(in_credentials_email text, in_credentials_password text, in_financialTransactionId integer)
  RETURNS SETOF FinancialTransactions AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'FinancialTransactions';
        var_functionName text := 'FIND';

        rec FinancialTransactions%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM FinancialTransactions
                WHERE financialTransactionId = in_financialTransactionId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_financialtransaction_find(text, text, integer) OWNER TO postgres;
