-- Maintain and return Zapp/Gadget statistics for account

DROP TYPE IF EXISTS _type_zapp_statistics_ CASCADE;

CREATE TYPE _type_zapp_statistics_ AS (
    accountId bigint,
    zappsCreated integer,
    zappsPublished integer,
    zappsSold integer,
    zappsRevenue integer,
    gadgetsCreated integer,
    gadgetsPublished integer,
    gadgetsSold integer,
    gadgetsRevenue integer,
    totalRevenue integer,
    convertedCreditBalance integer
);

CREATE OR REPLACE FUNCTION fn_zappstatistics(in_credentials_email text, in_credentials_password text)
  RETURNS SETOF _type_zapp_statistics_ AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Accounts';
        var_functionName text := 'FILTER';

        rec _type_zapp_statistics_%ROWTYPE;
        var_query text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

            BEGIN
                CREATE TEMPORARY TABLE tmp_tbl_zapp_statistics (
                    accountId bigint,
                    zappsCreated integer default(0),
                    zappsPublished integer default(0),
                    zappsSold integer default(0),
                    zappsRevenue integer default(0),
                    gadgetsCreated integer default(0),
                    gadgetsPublished integer default(0),
                    gadgetsSold integer default(0),
                    gadgetsRevenue integer default(0),
                    totalRevenue integer default(0),
                    convertedCreditBalance integer default(0)
                );
            EXCEPTION
                WHEN DUPLICATE_TABLE THEN
                    TRUNCATE TABLE tmp_tbl_zapp_statistics;
            END;

            INSERT INTO tmp_tbl_zapp_statistics(accountId, gadgetsCreated)
            SELECT accountId, COUNT(*)
            FROM Projects WHERE authorMemberId = var_credentialsId AND projectTypeId = 1 AND retired = 'f'
            GROUP BY accountId;

            INSERT INTO tmp_tbl_zapp_statistics(accountId, zappsCreated)
            SELECT accountId, COUNT(*)
            FROM Projects WHERE authorMemberId = var_credentialsId AND projectTypeId = 2 AND retired = 'f'
            GROUP BY accountId;

            INSERT INTO tmp_tbl_zapp_statistics(accountId, gadgetsPublished)
            SELECT accountId, COUNT(*)
            FROM Projects WHERE authorMemberId = var_credentialsId AND projectTypeId = 1 AND retired = 'f' AND published = 't'            
            GROUP BY accountId;

            INSERT INTO tmp_tbl_zapp_statistics(accountId, zappsPublished)
            SELECT accountId, COUNT(*)
            FROM Projects WHERE authorMemberId = var_credentialsId AND projectTypeId = 2 AND retired = 'f' AND published = 't'            
            GROUP BY accountId;

            INSERT INTO tmp_tbl_zapp_statistics(accountId, gadgetsSold)
            SELECT apn.accountId, COUNT(*)
            FROM Projects p, AccountProjectNumbers apn
            WHERE authorMemberId = var_credentialsId AND apn.projectId = p.projectId AND p.projectTypeId = 1
            GROUP BY apn.accountId;

            INSERT INTO tmp_tbl_zapp_statistics(accountId, zappsSold)
            SELECT apn.accountId, COUNT(*)
            FROM AccountProjectNumbers apn, Projects p
            WHERE authorMemberId = var_credentialsId AND apn.projectId = p.projectId AND p.projectTypeId = 2
            GROUP BY apn.accountId;

            INSERT INTO tmp_tbl_zapp_statistics(accountId, gadgetsRevenue)
            SELECT pa.accountId, SUM(pli.amountTotal)
            FROM Projects p, Payments pa, PaymentLineItems pli
            WHERE authorMemberId = var_credentialsId
            AND p.projectTypeId = 1
            AND p.accountId = pa.accountId
            AND pa.paymentId = pli.paymentId
            GROUP BY pa.accountId;

            INSERT INTO tmp_tbl_zapp_statistics(accountId, zappsRevenue)
            SELECT pa.accountId, SUM(pli.amountTotal)
            FROM Projects p, Payments pa, PaymentLineItems pli
            WHERE authorMemberId = var_credentialsId
            AND p.projectTypeId = 2
            AND p.accountId = pa.accountId
            AND pa.paymentId = pli.paymentId
            GROUP BY pa.accountId;

            FOR rec IN
                SELECT accountId,
                    SUM(zappsCreated), SUM(zappsPublished), SUM(zappsSold), SUM(zappsRevenue),
                    SUM(gadgetsCreated), SUM(gadgetsPublished), SUM(gadgetsSold), SUM(gadgetsRevenue),
                    SUM(zappsRevenue) + SUM(gadgetsRevenue),
                    SUM(convertedCreditBalance)
               FROM tmp_tbl_zapp_statistics GROUP BY accountId ORDER BY accountId
            LOOP
                RETURN NEXT rec;
            END LOOP;

        END IF;


    END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;

