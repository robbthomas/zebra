-------------------------------------------------------------------------------
-- r6_to_r7.sql
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (7);
-------------------------------------------------------------------------------

-- Following change added to r5_to_r6.sql AFTER Staging updated to r6 so we'll
-- repeat it here.
--ALTER TABLE Accounts ADD COLUMN convertMethod varchar(255); -- moved to r8_to_r9.sql


-- Add Accounts.merchantCustomerId and populate in fn_account_create()
ALTER TABLE Accounts ADD COLUMN merchantCustomerId text;

-- Create merchantCustomerId values for any existing accounts
CREATE OR REPLACE FUNCTION fn_accounts_prime_merchantCustomerIds ()
  RETURNS integer AS
$BODY$
    DECLARE
        index int; -- postgresql strings first char position=1
        var_uuid text;
        new_merchantCustomerId text;

        rec Accounts%ROWTYPE;
    BEGIN

        FOR rec IN
            SELECT * FROM Accounts
        LOOP
            -- generate an as yet unused random UUID-based but only 20 chars long merchantCustomerId
            -- for new entry but try only 12 times before bailing and failing!?
            index := 1;
            var_uuid := replace(cast(uuid_generate_v4() As text),'-','');

            LOOP
                new_merchantCustomerId := upper(substring(var_uuid from index for 20));
                IF NOT EXISTS (select merchantCustomerId from Accounts where merchantCustomerId = new_merchantCustomerId) THEN
                    EXIT;
                END IF;
                index := index + 1;
                IF index > 12 THEN -- only have 12 20-char substrings in 32-char uuid string
                    RETURN -1;
                END IF;
            END LOOP;

            UPDATE Accounts SET merchantCustomerId = new_merchantCustomerId WHERE accountId = rec.accountId;

        END LOOP;

        RETURN 1;

    End;
$BODY$
LANGUAGE 'plpgsql' VOLATILE COST 100;

SELECT * FROM fn_accounts_prime_merchantCustomerIds ();

DROP FUNCTION fn_accounts_prime_merchantCustomerIds ();

-- Add not null and unique index constraints to enforce uniqueness
ALTER TABLE Accounts ALTER COLUMN merchantCustomerId SET NOT NULL;
CREATE UNIQUE INDEX ix_Accounts_merchantCustomerId ON Accounts(merchantCustomerId);


-- Delete all AccountTypes except the Free account, now called Collector, and add
-- a new single paid account with new specifications. NOTE that earlier updates
-- have set admin and beta account types to types we will be deleting. Set accounttypes
-- for ALL these accounts to type=free for now and then upgrade to new type=creator.

UPDATE Accounts SET typeId = 1;

DELETE FROM AccountTypes WHERE accountTypeId > 1;

UPDATE AccountTypes SET tag = 'collector', name = 'Collector Account' WHERE accountTypeId = 1;

INSERT INTO AccountTypes(AccountTypeId, tag, name, price, displayOrder, description)
VALUES(2, 'creator', 'Creator Account', 899, 100, 'Vivamus CREATUS auctor leo vel dui. Aliquam erat volutpat. Phasellus nibh. Vestibulum ante ipsum primis in Aliquam erat volutpat. Phasellus nibh.');

UPDATE Accounts SET typeId = 2;


-- Change Payments.holdingDateTime default to be 10 days
ALTER TABLE Payments ALTER COLUMN holdingDateTime SET DEFAULT current_date + interval '10 days';


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
