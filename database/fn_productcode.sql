--------------------------------------------------------------------------------
-- fn_productcodes.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- ProductCodes functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='ProductCodes'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('ProductCodes','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ProductCodes','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ProductCodes','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ProductCodes','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('ProductCodes','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ProductCodes','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_productcode_create(text, text, text, text, text, integer, text, timestamp)

-- DROP FUNCTION fn_productcode_create(text, text, text, text, text, integer, text, timestamp);

CREATE OR REPLACE FUNCTION fn_productcode_create(in_credentials_email text, in_credentials_password text, in_tag text, in_name text, in_description text, in_displayOrder integer, in_retiredById text, in_retiredDateTime timestamp)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProductCodes';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_productCodesId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO ProductCodes(
            tag,
            name,
            description,
            displayOrder,
            retiredById,
            retiredDateTime
        )
        VALUES(
            in_tag,
            in_name,
            in_description,
            in_displayOrder,
            in_retiredById,
            in_retiredDateTime
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_productCodesId := currval('seq_ProductCodes_productCodesId');
        END IF;

        return new_productCodesId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_productcode_create(text, text, text, text, text, integer, text, timestamp) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_productcode_update(text, text, integer, text, text, text, integer, text, timestamp, text)

-- DROP FUNCTION fn_productcode_update(text, text, integer, text, text, text, integer, text, timestamp, text);

CREATE OR REPLACE FUNCTION fn_productcode_update(in_credentials_email text, in_credentials_password text, in_productCodesId integer, in_tag text, in_name text, in_description text, in_displayOrder integer, in_retiredById text, in_retiredDateTime timestamp, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProductCodes';
        var_functionName text := 'UPDATE';

        rec ProductCodes%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from ProductCodes where productCodesId = in_productCodesId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_tag' THEN
                rec.tag := in_tag;
            ELSEIF param_name = 'in_name' THEN
                rec.name := in_name;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_displayOrder' THEN
                rec.displayOrder := in_displayOrder;
            ELSEIF param_name = 'in_retiredById' THEN
                rec.retiredById := in_retiredById;
            ELSEIF param_name = 'in_retiredDateTime' THEN
                rec.retiredDateTime := in_retiredDateTime;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE ProductCodes SET
            tag = rec.tag,
            name = rec.name,
            description = rec.description,
            displayOrder = rec.displayOrder,
            retiredById = rec.retiredById,
            retiredDateTime = rec.retiredDateTime
        WHERE productCodesId = in_productCodesId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_productcode_update(text, text, integer, text, text, text, integer, text, timestamp, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_productcode_retire(text, text, integer, text)

-- DROP FUNCTION fn_productcode_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_productcode_retire(in_credentials_email text, in_credentials_password text, in_productCodesId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProductCodes';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE ProductCodes SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        WHERE productCodesId = in_productCodesId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_productcode_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_productcode_filter(text, text, integer, integer, text, text, text, text, integer, text, timestamp, text)

-- DROP FUNCTION fn_productcode_filter(text, text, integer, integer, text, text, text, text, integer, text, timestamp, text);

CREATE OR REPLACE FUNCTION fn_productcode_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_tag text, in_name text, in_description text, in_displayOrder integer, in_retiredById text, in_retiredDateTime timestamp, in_filter_list text)
  RETURNS SETOF ProductCodes AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProductCodes';
        var_functionName text := 'FILTER';

        rec ProductCodes%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM ProductCodes WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_productCodeId' THEN
            ELSEIF filter_name = 'in_tag' THEN
                var_query := var_query || ' AND tag ILIKE ''%' || in_tag || '%''';
            ELSEIF filter_name = 'in_name' THEN
                var_query := var_query || ' AND name ILIKE ''%' || in_name || '%''';
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_displayOrder' THEN
                var_query := var_query || ' AND displayOrder = ' || in_displayOrder;
            ELSEIF filter_name = 'in_retired' THEN
            ELSEIF filter_name = 'in_retiredById' THEN
                var_query := var_query || ' AND retiredById ILIKE ''%' || in_retiredById || '%''';
            ELSEIF filter_name = 'in_retiredDateTime' THEN
                var_query := var_query || ' AND retiredDateTime = ''' || in_retiredDateTime || '''';
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM ProductCodes;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_productcode_filter(text, text, integer, integer, text, text, text, text, integer, text, timestamp, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_productcode_find(text, text, integer)

-- DROP FUNCTION fn_productcode_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_productcode_find(in_credentials_email text, in_credentials_password text, in_productCodesId integer)
  RETURNS SETOF ProductCodes AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProductCodes';
        var_functionName text := 'FIND';

        rec ProductCodes%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM ProductCodes
                WHERE productCodesId = in_productCodesId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_productcode_find(text, text, integer) OWNER TO postgres;
