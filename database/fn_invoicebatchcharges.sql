-- fn_invoicebatchcharges.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
---------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Invoices' AND (functionName='BATCH_CHARGES_CREATE' OR functionName='BATCH_STATE_UPDATE');

select * from fn_TableFunctionRoleMap_insert('Invoices','BATCH_CHARGES_CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','BATCH_CHARGES_CREATE','ROLE_TYPE_COMMERCE_CRON');

select * from fn_TableFunctionRoleMap_insert('Invoices','BATCH_STATE_UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','BATCH_STATE_UPDATE','ROLE_TYPE_COMMERCE_CRON');


-- Function: fn_invoice_batch_charges_create(integer)

DROP TYPE IF EXISTS _type_invoice_batch_charge_create_ CASCADE;

CREATE TYPE _type_invoice_batch_charge_create_ As (
    accountId bigint,
    email text,
    firstName text,
    lastName text,
    currencyTypeId int,
    totalCharge bigint, -- type of sum(int) is defined as bigint
    transactionId bigint,
    transactionLabel text,
    includesSubscriptionCharge int,  -- whether or not to ignore the amount when printing email
    subscrCharge int
);

CREATE OR REPLACE FUNCTION fn_invoice_batch_charges_create(in_credentials_email text, in_credentials_password text, in_accountId integer)
  RETURNS SETOF _type_invoice_batch_charge_create_ AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Invoices';
        var_functionName text := 'BATCH_CHARGES_CREATE';

        invoice_state_pending int;
        var_accountId bigint;
        var_currencyTypeId int;
        var_newTransactionId bigint := -1;
        var_count int := -1;

        rec _type_invoice_batch_charge_create_%ROWTYPE;

    BEGIN
        var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN

            invoice_state_pending := invoiceStateId FROM InvoiceStates WHERE tag = 'invoice_state_pending';

            -- NOTE that temp table below is NOT the same as the function return type
           	DROP TABLE IF EXISTS tmp_tbl_create_invoice_batch_charge_transactions;
            CREATE TEMPORARY TABLE tmp_tbl_create_invoice_batch_charge_transactions (
                accountId bigint,
                email text,
                firstName text,
                lastName text,
                currencyTypeId int,
                invoiceId bigint,
                includesSubscriptionCharge int,
                subscriptionCharge int,
                totalCharge int,
                transactionId bigint, -- leave these as last 2 fields in tmp table,
                transactionLabel text -- else have to call out all preceding fields in INSERT below
            );

            -- get list of accounts in good standing with invoices pending and at or beyond any optional holding date
            WITH ungrouped (
                    accountId,
                    email,
                    firstName,
                    lastName,
                    currencyTypeId,
                    invoiceId,
                    invoiceLineItemId,
                    amountTotal,
                    productIsSubscription
            ) as (
                SELECT distinct Invoices.accountId, App_User.email, coalesce(App_User.firstName, 'Customer'), coalesce(App_User.lastName, 'Customer'),
                    Invoices.currencyTypeId, Invoices.invoiceId, InvoiceLineItems.invoiceLineItemId, InvoiceLineItems.amountTotal,
                    ProductCategories.tag in ('SUBSCRIPTION_INITIAL', 'SUBSCRIPTION_RECURRING', 'SUBSCRIPTION_UPGRADE')
                FROM Invoices
                  INNER JOIN InvoiceLineItems
                    ON InvoiceLineItems.invoiceId = Invoices.invoiceId
                  INNER JOIN Accounts
                    ON Invoices.accountId = Accounts.accountId
                    AND Accounts.goodStanding = 1
                  INNER JOIN AccountMemberRoles
                    ON Accounts.accountId = AccountMemberRoles.accountId
                    AND AccountMemberRoles.typeId = 1 -- Account Administrator
                  INNER JOIN App_User
                    ON AccountMemberRoles.memberId = App_User.id
                  LEFT JOIN ProductCodes
                    ON ProductCodes.productCodeId = InvoiceLineItems.productCodeId
                  LEFT JOIN  ProductCategories
                    ON ProductCodes.productCategoryId = ProductCategories.productCategoryId
                WHERE (in_accountId <= 0 OR Invoices.accountId = in_accountId)
                  AND Invoices.stateId = invoice_state_pending
                  AND Invoices.holdingDateTime <= current_date
            )
            INSERT INTO tmp_tbl_create_invoice_batch_charge_transactions
            SELECT accountid, email, firstname, lastName, currencyTypeId, invoiceId
              , max(CASE WHEN productIsSubscription THEN 1 ELSE 0 END) AS includesSubscriptionCharge
              , sum(CASE WHEN productIsSubscription THEN amountTotal ELSE 0 END) AS subscriptionCharge
              , sum(amountTotal) as totalCharge
              , null, null
            FROM ungrouped
            GROUP BY accountid, email, firstname, lastName, currencyTypeId, invoiceId;

            -- create new FinancialTransactions table entries for every distinct account+currency combination
            FOR rec IN
                SELECT DISTINCT accountId, '', '', '', currencyTypeId FROM tmp_tbl_create_invoice_batch_charge_transactions
            LOOP
                INSERT INTO FinancialTransactions(accountId, currencyTypeId)
                VALUES (rec.accountId, rec.currencyTypeId);

                var_newTransactionId := currval('seq_FinancialTransactions_financialTransactionId');

                UPDATE tmp_tbl_create_invoice_batch_charge_transactions
                SET transactionId = var_newTransactionId
                  , transactionLabel = (select label from FinancialTransactions where financialTransactionId = var_newTransactionId)
                WHERE accountId = rec.accountId
                AND currencyTypeId = rec.currencyTypeId;

            END LOOP;

            -- update invoices with new transaction Ids
            UPDATE Invoices
            SET transactionId = tmp.transactionId
            FROM tmp_tbl_create_invoice_batch_charge_transactions tmp
            WHERE Invoices.invoiceId = tmp.invoiceId
            AND Invoices.accountId = tmp.accountId;

            -- return aggregate list of charges to process ordered by accountId and currencyTypeId
            -- 'Subscription' should change to the product code for a subscription
            FOR rec IN
                SELECT accountId, email, firstName, lastName, currencyTypeId, sum(totalCharge) as totalCharge,
                       transactionId, transactionLabel, max(includesSubscriptionCharge) as includesSubscriptionCharge, sum(subscriptionCharge) as subscriptionCharge
                FROM tmp_tbl_create_invoice_batch_charge_transactions
                GROUP BY accountId, email, firstName, lastName, currencyTypeId, transactionId, transactionLabel
                ORDER BY accountId, currencyTypeId
            LOOP
                RETURN NEXT rec;
            END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_invoice_batch_charges_create(integer) OWNER TO postgres;



CREATE OR REPLACE FUNCTION fn_invoice_batch_state_update(in_credentials_email text, in_credentials_password text, in_transactionId integer, in_state text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Invoices';
        var_functionName text := 'BATCH_STATE_UPDATE';

        var_stateId int;
        var_count int = -1;

    BEGIN
        var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN

            var_stateId := invoiceStateId FROM InvoiceStates WHERE tag ILIKE in_state;

            UPDATE Invoices SET stateId = var_stateId WHERE transactionId = in_transactionId;

            GET DIAGNOSTICS var_count = Row_Count;

        END IF;

        RETURN var_count;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_invoice_batch_state_update(text, text, integer, text) OWNER TO postgres;
