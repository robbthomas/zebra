--------------------------------------------------------------------------------
-- fn_accountprojectnumbers_exists.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------

select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','EXISTS','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','EXISTS','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','EXISTS','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','EXISTS','ROLE_TYPE_VIEWER');


--------------------------------------------------------------------------------
-- Function: fn_accountprojectnumber_exists(text, text, integer, integer)

-- DROP FUNCTION fn_accountprojectnumber_exists(text, text, integer, integer);

CREATE OR REPLACE FUNCTION fn_accountprojectnumber_exists(
    in_credentials_email text, in_credentials_password text, in_accountId integer, in_publishId integer, in_retired integer)
  RETURNS SETOF AccountProjectNumbers AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountProjectNumbers';
        var_functionName text := 'EXISTS';

        rec AccountProjectNumbers%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT * FROM AccountProjectNumbers
                WHERE accountId = in_accountId AND publishId = in_publishId AND retired = in_retired
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_accountprojectnumber_exists(text, text, integer, integer) OWNER TO postgres;
