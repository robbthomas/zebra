--------------------------------------------------------------------------------
-- fn_projecttransfers.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- ProjectTransfers functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='ProjectTransfers'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ProjectTransfers','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_projecttransfer_create(text, text, integer, text, integer, text, text, integer, timestamp, text, timestamp, integer, timestamp)

-- DROP FUNCTION fn_projecttransfer_create(text, text, integer, text, integer, text, text, integer, timestamp, text, timestamp, integer, timestamp);

CREATE OR REPLACE FUNCTION fn_projecttransfer_create(in_credentials_email text, in_credentials_password text, in_senderAccountId integer, in_senderMemberId text, in_receiverAccountId integer, in_receiverMemberId text, in_projectId text, in_complete integer, in_completedDateTime timestamp, in_retiredById text, in_retiredDateTime timestamp, in_viewedByReceiver integer, in_viewedDate timestamp)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProjectTransfers';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_projectTransferId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO ProjectTransfers(
            senderAccountId,
            senderMemberId,
            receiverAccountId,
            receiverMemberId,
            projectId,
            complete,
            completedDateTime,
            retiredById,
            retiredDateTime,
            viewedByReceiver,
            viewedDate
        )
        VALUES(
            in_senderAccountId,
            in_senderMemberId,
            in_receiverAccountId,
            in_receiverMemberId,
            in_projectId,
            in_complete,
            in_completedDateTime,
            in_retiredById,
            in_retiredDateTime,
            in_viewedByReceiver,
            in_viewedDate
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_projectTransferId := currval('seq_ProjectTransfers_projectTransferId');
        END IF;

        return new_projectTransferId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_projecttransfer_create(text, text, integer, text, integer, text, text, integer, timestamp, text, timestamp, integer, timestamp) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_projecttransfer_update(text, text, integer, integer, text, integer, text, text, integer, timestamp, text, timestamp, integer, timestamp, text)

-- DROP FUNCTION fn_projecttransfer_update(text, text, integer, integer, text, integer, text, text, integer, timestamp, text, timestamp, integer, timestamp, text);

CREATE OR REPLACE FUNCTION fn_projecttransfer_update(in_credentials_email text, in_credentials_password text, in_projectTransferId integer, in_senderAccountId integer, in_senderMemberId text, in_receiverAccountId integer, in_receiverMemberId text, in_projectId text, in_complete integer, in_completedDateTime timestamp, in_retiredById text, in_retiredDateTime timestamp, in_viewedByReceiver integer, in_viewedDate timestamp, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProjectTransfers';
        var_functionName text := 'UPDATE';

        rec ProjectTransfers%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from ProjectTransfers where projectTransferId = in_projectTransferId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_senderAccountId' THEN
                rec.senderAccountId := in_senderAccountId;
            ELSEIF param_name = 'in_senderMemberId' THEN
                rec.senderMemberId := in_senderMemberId;
            ELSEIF param_name = 'in_receiverAccountId' THEN
                rec.receiverAccountId := in_receiverAccountId;
            ELSEIF param_name = 'in_receiverMemberId' THEN
                rec.receiverMemberId := in_receiverMemberId;
            ELSEIF param_name = 'in_projectId' THEN
                rec.projectId := in_projectId;
            ELSEIF param_name = 'in_complete' THEN
                rec.complete := in_complete;
            ELSEIF param_name = 'in_completedDateTime' THEN
                rec.completedDateTime := in_completedDateTime;
            ELSEIF param_name = 'in_retiredById' THEN
                rec.retiredById := in_retiredById;
            ELSEIF param_name = 'in_retiredDateTime' THEN
                rec.retiredDateTime := in_retiredDateTime;
            ELSEIF param_name = 'in_viewedByReceiver' THEN
                rec.viewedByReceiver := in_viewedByReceiver;
            ELSEIF param_name = 'in_viewedDate' THEN
                rec.viewedDate := in_viewedDate;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE ProjectTransfers SET
            senderAccountId = rec.senderAccountId,
            senderMemberId = rec.senderMemberId,
            receiverAccountId = rec.receiverAccountId,
            receiverMemberId = rec.receiverMemberId,
            projectId = rec.projectId,
            complete = rec.complete,
            completedDateTime = rec.completedDateTime,
            retiredById = rec.retiredById,
            retiredDateTime = rec.retiredDateTime,
            viewedByReceiver = rec.viewedByReceiver,
            viewedDate = rec.viewedDate
        WHERE projectTransferId = in_projectTransferId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_projecttransfer_update(text, text, integer, integer, text, integer, text, text, integer, timestamp, text, timestamp, integer, timestamp, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_projecttransfer_retire(text, text, integer, text)

-- DROP FUNCTION fn_projecttransfer_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_projecttransfer_retire(in_credentials_email text, in_credentials_password text, in_projectTransferId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProjectTransfers';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE ProjectTransfers SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        WHERE projectTransferId = in_projectTransferId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_projecttransfer_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_projecttransfer_filter(text, text, integer, integer, text, integer, text, integer, text, text, integer, timestamp, text, timestamp, integer, timestamp, text)

-- DROP FUNCTION fn_projecttransfer_filter(text, text, integer, integer, text, integer, text, integer, text, text, integer, timestamp, text, timestamp, integer, timestamp, text);

CREATE OR REPLACE FUNCTION fn_projecttransfer_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_senderAccountId integer, in_senderMemberId text, in_receiverAccountId integer, in_receiverMemberId text, in_projectId text, in_complete integer, in_completedDateTime timestamp, in_retiredById text, in_retiredDateTime timestamp, in_viewedByReceiver integer, in_viewedDate timestamp, in_filter_list text)
  RETURNS SETOF ProjectTransfers AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProjectTransfers';
        var_functionName text := 'FILTER';

        rec ProjectTransfers%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM ProjectTransfers WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_projectTransferId' THEN
            ELSEIF filter_name = 'in_senderAccountId' THEN
                var_query := var_query || ' AND senderAccountId = ' || in_senderAccountId;
            ELSEIF filter_name = 'in_senderMemberId' THEN
                var_query := var_query || ' AND senderMemberId ILIKE ''%' || in_senderMemberId || '%''';
            ELSEIF filter_name = 'in_receiverAccountId' THEN
                var_query := var_query || ' AND receiverAccountId = ' || in_receiverAccountId;
            ELSEIF filter_name = 'in_receiverMemberId' THEN
                var_query := var_query || ' AND receiverMemberId ILIKE ''%' || in_receiverMemberId || '%''';
            ELSEIF filter_name = 'in_createdDateTime' THEN
            ELSEIF filter_name = 'in_projectId' THEN
                var_query := var_query || ' AND projectId ILIKE ''%' || in_projectId || '%''';
            ELSEIF filter_name = 'in_complete' THEN
                var_query := var_query || ' AND complete = ' || in_complete;
            ELSEIF filter_name = 'in_completedDateTime' THEN
                var_query := var_query || ' AND completedDateTime = ''' || in_completedDateTime || '''';
            ELSEIF filter_name = 'in_retired' THEN
            ELSEIF filter_name = 'in_retiredById' THEN
                var_query := var_query || ' AND retiredById ILIKE ''%' || in_retiredById || '%''';
            ELSEIF filter_name = 'in_retiredDateTime' THEN
                var_query := var_query || ' AND retiredDateTime = ''' || in_retiredDateTime || '''';
            ELSEIF filter_name = 'in_viewedByReceiver' THEN
                var_query := var_query || ' AND viewedByReceiver = ' || in_viewedByReceiver;
            ELSEIF filter_name = 'in_viewedDate' THEN
                var_query := var_query || ' AND viewedDate = ''' || in_viewedDate || '''';
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM ProjectTransfers;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_projecttransfer_filter(text, text, integer, integer, text, integer, text, integer, text, text, integer, timestamp, text, timestamp, integer, timestamp, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_projecttransfer_find(text, text, integer)

-- DROP FUNCTION fn_projecttransfer_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_projecttransfer_find(in_credentials_email text, in_credentials_password text, in_projectTransferId integer)
  RETURNS SETOF ProjectTransfers AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ProjectTransfers';
        var_functionName text := 'FIND';

        rec ProjectTransfers%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM ProjectTransfers
                WHERE projectTransferId = in_projectTransferId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_projecttransfer_find(text, text, integer) OWNER TO postgres;
