-------------------------------------------------------------------------------
-- r7_to_r8.sql
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (8);
-------------------------------------------------------------------------------

-- Should not have more than ONE ROLE_TYPE_ACCOUNT_ADMINISTRATOR per account
-- because this is used to look up an Email address for eCommerce functions
-- which generate Emails to account owners. So delete ALL accountId=1 account
-- admins EXCEPT blackbeard.

DELETE FROM AccountMemberRoles WHERE typeId=1 AND accountId=1
    AND memberid <> (SELECT id FROM App_User WHERE displayName='blackbeard');


ALTER TABLE Published ADD COLUMN screenShotId text;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
