-------------------------------------------------------------------------------
-- r58_to_r59.sql
--
-- Add External User Id to GadgetState and PublishLaunchCounts
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (59);
-------------------------------------------------------------------------------

alter table gadget_state drop constraint gadget_state_app_user_id_key;
alter table gadget_state add column externalUserId varchar(255) default null;
alter table gadget_state add constraint gadget_state_user_key unique (app_user_id, root_gadget_id, externalUserId, author_mode);

alter table PublishLaunchCounts drop constraint fk_publishlaunchcounts_parentpublishid;
alter table PublishLaunchCounts drop column parentpublishid;
alter table PublishLaunchCounts rename column launchedbyaccountid to accountId;
alter table PublishLaunchCounts add column appUserId varchar(255) not null;
alter table PublishLaunchCounts add column externalUserId varchar(255) default null;
alter table PublishLaunchCounts add column accountProjectNumbersId varchar(255) default null;
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
