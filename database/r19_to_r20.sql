-------------------------------------------------------------------------------
-- r19_to_r20.sql
--
-- Clean up bad credit card rows. This is mainly so that ongoing testing can
-- continue on the QA server where earlier broken functionality allowed bad
-- data into the database.
--
-- Set emails for special app_user entries.
--
-- Clean up App_User entries with no displayName or no email or with
-- duplicate emails and add unique constraint to email column.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (20);
-------------------------------------------------------------------------------

UPDATE CreditCards
SET expirationYear = TRIM(BOTH FROM expirationYear), expirationMonth = TRIM(BOTH FROM expirationMonth);

DELETE FROM CreditCards
WHERE expirationYear IS NULL OR LENGTH(expirationYear) = 0
OR expirationMonth IS NULL OR LENGTH(expirationMonth) = 0;

DELETE FROM CreditCards
WHERE expirationYear !~ '^[0-9]+$' OR expirationMonth !~ '^[0-9]+$';


UPDATE App_User SET email = 'guest@zebrazapps.com' WHERE displayName ILIKE 'guest';
UPDATE App_User SET email = 'jvp@zebrazapps.com' WHERE displayName ILIKE 'jvp';
UPDATE App_User SET email = 'pswdAdmin@zebrazapps.com' WHERE displayName ILIKE 'pswdAdmin';
UPDATE App_User SET email = 'eCommCron@zebrazapps.com' WHERE displayName ILIKE 'eCommCron';


-------------------------------------------------------------------------------
-- Fix duplicate emails as part of unique email requirement
-------------------------------------------------------------------------------

-- Cannot dump all hopeless entries, with no displayName and no email, because
-- some referenced in other tables (e.g., Accounts.editedById) and would fail
-- delete because of foreign key constraints. So instead, we'll triple down the
-- already unique App_User.id across displayName and email.
UPDATE App_User SET displayName=id, email=id
WHERE (displayName IS NULL OR LENGTH(TRIM(BOTH FROM displayName)) = 0)
AND (email IS NULL OR LENGTH(TRIM(BOTH FROM email)) = 0);

-- Next fix remaining with null or blank emails
UPDATE App_User SET email = displayName || '@' || displayName || '.zebrazapps.com'
WHERE email IS NULL OR LENGTH(TRIM(BOTH FROM email)) = 0;

-- Next collect all duplicates
SELECT email, count(*) as cnt INTO TEMPORARY TABLE tmp_app_user_email_counts
FROM App_User GROUP BY email;

DELETE FROM tmp_app_user_email_counts WHERE cnt <= 1;

-- Update all duplicates with new email based on prefixing existing email with displayName
UPDATE App_User SET email = displayName || '.' || email
WHERE email IN (SELECT email FROM tmp_app_user_email_counts);

DROP TABLE tmp_app_user_email_counts;

-- Add unique constraint to App_User.email
CREATE UNIQUE INDEX ix_App_User_email ON App_User(email);


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

