-------------------------------------------------------------------------------
-- r91_to_r92.sql
--
-- Author:  Joe Bodell
-- Date:  05-21-2012
-- Notes:  Create the ProjectFeedbacks table, along with the sequence for autonumbering the primary key
--
-------------------------------------------------------------------------------

-- DO NOT USE A TRANSACTION BLOCK HERE -- THESE SCRIPTS ARE BATCHED INSIDE A TRANSACTION AND POSTGRES DOES NOT SUPPORT NESTED TRANSACTIONS
DROP SEQUENCE IF EXISTS seq_projectfeedbacks_projectfeedbackid;
CREATE SEQUENCE seq_projectfeedbacks_projectfeedbackid START 2000;


CREATE TABLE ProjectFeedbacks (
      projectFeedbackID INT4                     NOT NULL PRIMARY KEY DEFAULT nextval('seq_projectfeedbacks_projectfeedbackid')
    , projectID         VARCHAR(255)             NOT NULL REFERENCES projects( projectID )
    , feedbackTypeID    INT4                     NOT NULL REFERENCES feedbackTypes( feedbackTypeID )
    , memberID          VARCHAR(255)             NOT NULL REFERENCES app_user( id )
    , flagReason        TEXT                     NOT NULL
    , createdDateTime   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
    , updatedDateTime   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
    , retired           INT4                     NOT NULL DEFAULT 0
    , retiredByID       VARCHAR(255)                 NULL
    , retiredDateTime   TIMESTAMP WITH TIME ZONE     NULL
);


------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (92);
------------------------------------------------------

