-------------------------------------------------------------------------------
-- r53_to_r54.sql
--
-- Update share types
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (56);
-------------------------------------------------------------------------------

-- upgrade admin accounts to PRO users
update accounts set typeid = 3 where accountid in 
(
    select distinct accounts.accountid
 from app_user
 inner join accountmemberroles
   on app_user.id = accountmemberroles.memberid
 inner join accounts
   on accounts.accountid = accountmemberroles.accountid 
 left join systemadministrators
   on systemadministrators.memberId = app_user.id 
 where accounts.name = 'admin account'
    or systemadministrators.memberid is not null

);



-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;