create or replace function fn_tmp_insert_prodcode(in_categorytag varchar(16), in_tag varchar(16), in_desc varchar(64), in_discountType varchar(16), in_productTypeTag varchar(16), in_price integer, in_refund integer)
RETURNS integer AS
$BODY$
    DECLARE
        cnt int := 0;

-- ignore product type tag, do it programatically
    BEGIN
        insert into productcodes (productcategoryid, tag, description, discountType, productTypeId, price, refunddays, effdate, termdate ) values
            ((select productcategoryid from productcategories where tag = in_categorytag),
             in_tag, in_desc, in_discountType,
             (select productTypeId from ProductTypes where tag = 'COM'),
             in_price, in_refund, now(), date '2038-1-19'
             );
            
        insert into productcodes (productcategoryid, tag, description, discountType, productTypeId, price, refunddays, effdate, termdate ) values
            ((select productcategoryid from productcategories where tag = in_categorytag),
             in_tag, in_desc, in_discountType,
             (select productTypeId from ProductTypes where tag = 'EDU'),
             in_price, in_refund, now(), date '2038-1-19'
             );

        insert into productcodes (productcategoryid, tag, description, discountType, productTypeId, price, refunddays, effdate, termdate ) values
            ((select productcategoryid from productcategories where tag = in_categorytag),
             in_tag, in_desc, in_discountType,
             (select productTypeId from ProductTypes where tag = 'GOV'),
             in_price, in_refund, now(), date '2038-1-19'
             );
        RETURN cnt;
    END;
$BODY$
LANGUAGE 'plpgsql';

