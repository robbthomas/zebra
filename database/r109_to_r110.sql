-------------------------------------------------------------------------------
-- r109_to_r110.sql
--
-- Author:  Jesse Coyle
-- Notes:
--
-------------------------------------------------------------------------------

-- DEFUNCT DUE TO TEMPORARY CONFLICT BETWEEN 1204 and 1301
-- CONTENT FROM BOTH IS REPRODUCIBLE AND MOVED TO r111


------------------------------------------------------
-- Increment the database version - DO NOT remove this line
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (110);
------------------------------------------------------