-------------------------------------------------------------------------------
-- r53_to_r54.sql
--
-- Update share types
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (55);
-------------------------------------------------------------------------------
alter table guestlistinvitees add column retired int not null default 0;

\i fn_guestlist.sql
\i fn_guestlistinvitee.sql

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;