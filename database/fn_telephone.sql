--------------------------------------------------------------------------------
-- fn_telephones.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- Telephones functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Telephones'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('Telephones','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Telephones','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Telephones','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Telephones','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('Telephones','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Telephones','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_telephone_create(text, text, text, integer, text, text, text, text)

-- DROP FUNCTION fn_telephone_create(text, text, text, integer, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_telephone_create(in_credentials_email text, in_credentials_password text, in_type text, in_accountId integer, in_memberId text, in_label text, in_number text, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Telephones';
        var_functionName text := 'CREATE';

        var_editedById text;
        new_telephoneId int := -1;
        new_telephoneTypeId int;
        var_count int := -1;


    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- get type Id
        new_telephoneTypeId := addressTelephoneTypeId FROM AddressTelephoneTypes WHERE tag ILIKE in_type;

        -- set editing user
        var_editedById := COALESCE(in_editedById,'');
        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;


        insert into Telephones(
            typeId,
            accountId,
            memberId,
            label,
            number,
            editedById
        )
        values(
            new_telephoneTypeId,
            in_accountId,
            in_memberId,
            in_label,
            in_number,
            var_editedById
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_telephoneId := currval('seq_Telephones_telephoneId');
        END IF;

        return new_telephoneId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_telephone_create(text, text, text, integer, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_telephone_update(text, text, integer, text, integer, text, text, text, text, text)

-- DROP FUNCTION fn_telephone_update(text, text, integer, text, integer, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_telephone_update(in_credentials_email text, in_credentials_password text, in_telephoneId integer, in_type text, in_accountId integer, in_memberId text, in_label text, in_number text, in_editedById text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Telephones';
        var_functionName text := 'UPDATE';

        rec Telephones%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from Telephones where telephoneId = in_telephoneId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_type' THEN
                rec.typeId := addressTelephoneTypeId FROM AddressTelephoneTypes WHERE tag ILIKE in_type;
            ELSEIF param_name = 'in_accountId' THEN
                rec.accountId := in_accountId;
            ELSEIF param_name = 'in_memberId' THEN
                rec.memberId := in_memberId;
            ELSEIF param_name = 'in_label' THEN
                rec.label := in_label;
            ELSEIF param_name = 'in_number' THEN
                rec.number := in_number;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE Telephones SET
            typeId = rec.typeId,
            accountId = rec.accountId,
            memberId = rec.memberId,
            label = rec.label,
            number = rec.number,
            editedById = rec.editedById
        WHERE telephoneId = in_telephoneId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_telephone_update(text, text, integer, text, integer, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_telephone_retire(text, text, integer, text)

-- DROP FUNCTION fn_telephone_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_telephone_retire(in_credentials_email text, in_credentials_password text, in_telephoneId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Telephones';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE Telephones SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE telephoneId = in_telephoneId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_telephone_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_telephone_filter(text, text, integer, integer, text, text, integer, text, text, text, text)

-- DROP FUNCTION fn_telephone_filter(text, text, integer, integer, text, text, integer, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_telephone_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_type text, in_accountId integer, in_memberId text, in_label text, in_number text, in_filter_list text)
  RETURNS SETOF Telephones AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Telephones';
        var_functionName text := 'FILTER';

        rec Telephones%ROWTYPE;
        var_typeId int;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM Telephones WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_telephoneId' THEN
            ELSEIF filter_name = 'in_type' THEN
                var_typeId := addressTelephoneTypeId FROM AddressTelephoneTypes WHERE tag ILIKE in_type;
                var_query := var_query || ' AND typeId = ' || var_typeId;
            ELSEIF filter_name = 'in_accountId' THEN
                var_query := var_query || ' AND accountId = ' || in_accountId;
            ELSEIF filter_name = 'in_memberId' THEN
                var_query := var_query || ' AND memberId ILIKE ''%' || in_memberId || '%''';
            ELSEIF filter_name = 'in_label' THEN
                var_query := var_query || ' AND label ILIKE ''%' || in_label || '%''';
            ELSEIF filter_name = 'in_number' THEN
                var_query := var_query || ' AND number ILIKE ''%' || in_number || '%''';
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            ELSEIF filter_name = 'in_retired' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM Telephones;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;
        
            IF var_query is not null THEN
                FOR rec IN
                    EXECUTE var_query
                LOOP
                    RETURN NEXT rec;
                END LOOP;
            END IF;
        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_telephone_filter(text, text, integer, integer, text, text, integer, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_telephone_find(text, text, integer)

-- DROP FUNCTION fn_telephone_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_telephone_find(in_credentials_email text, in_credentials_password text, in_telephoneId integer)
  RETURNS SETOF Telephones AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Telephones';
        var_functionName text := 'FIND';

        rec Telephones%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM Telephones
                WHERE telephoneId = in_telephoneId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_telephone_find(text, text, integer) OWNER TO postgres;
