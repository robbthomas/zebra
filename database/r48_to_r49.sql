-------------------------------------------------------------------------------
-- r48_to_r49.sql
--
-- Create tables for tracking sharing of zapps, and email invites
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (49);
-------------------------------------------------------------------------------

alter table SharedProjectInvitee add column lastName text;
alter table SharedProjectInvitee add column lastName text;

\i fn_sharedprojectinvitee.sql
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

