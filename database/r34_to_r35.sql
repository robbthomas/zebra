-------------------------------------------------------------------------------
-- r31_to_r32.sql
--   add case-insensitive constraint on email and displayname
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (33);
-------------------------------------------------------------------------------

-- this sql will find all the non-unique ones
-- run it in all environments before checking in this script
--select au.id, au.displayname, au2.id as id_2, au2.displayname as displayname2 
--    from app_user au 
--    inner join app_user au2 
--        on lower(au.displayname) = lower(au2.displayname) 
--           and au.id <> au2.id;



create unique index ix_email_unique_lower on app_user (lower(email));
create unique index ix_displayname_unique_lower on app_user (lower(displayname));

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;


