-------------------------------------------------------------------------------
-- r37_to_r38.sql
--
-- updating plans and pricing information
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

UPDATE accounttypes SET name = 'Collector', description = 'FREE' WHERE tag = 'collector';

UPDATE accounttypes SET name = 'Creator', description = 'FREE, normally $8.99/month', price = 0 WHERE tag = 'creator';

-------------------------------------------------------------------------------
-- Update database version.
-------------------------------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES(38);

-------------------------------------------------------------------------------
-- End transaction wrapper for this update.
-------------------------------------------------------------------------------
COMMIT;
