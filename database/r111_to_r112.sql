-------------------------------------------------------------------------------
-- r111_to_r112.sql
--
-- Author:  Shannon Jackson
-- Notes:
--
-------------------------------------------------------------------------------

-- add new invoice state for canceled

INSERT INTO invoicestates (invoicestateid, tag, name, displayorder)
    SELECT 4, 'invoice_state_canceled', 'Invoice Canceled', 40
    WHERE NOT EXISTS (
        SELECT 'x'
        FROM invoicestates
        WHERE invoiceStateId = 4
        AND tag = 'invoice_state_canceled'
    );

-- clean up credit cards round two

update creditcards set retired = 1
where (customerpaymentprofileid is null
or trim(both ' ' from customerpaymentprofileid) = ''
or customerpaymentprofileid = 'NULL'
or customerpaymentprofileid = 'null'
or trim(both ' ' from customerprofileid) = ''
or customerprofileid is null
or customerprofileid = 'NULL'
or customerprofileid = 'null'
or trim(both ' ' from maskednumber) = ''
or maskednumber is null
or maskednumber = 'NULL'
or maskednumber = 'null')
and retired = 0;


------------------------------------------------------
-- Increment the database version - DO NOT remove this line
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (112);
------------------------------------------------------