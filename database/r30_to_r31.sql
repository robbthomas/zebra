-------------------------------------------------------------------------------
-- r30_to_r31.sql
--   reset expiration date for expired fake CCs
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (31);
-------------------------------------------------------------------------------

-- Add new column.
alter table published add column featured smallint default 0;



-- Save existing autohistory.
SELECT * INTO TEMPORARY TABLE temp_Published_Autohistory FROM Published_Autohistory;

-- Recreate autohistory for Published.
--DROP SEQUENCE seq_Published_AutoHistory CASCADE;
DROP TABLE Published_AutoHistory CASCADE;

--CREATE SEQUENCE seq_Published_AutoHistory;
CREATE TABLE Published_AutoHistory (
    LIKE Published,
    Published_AutoHistory_Id bigint DEFAULT nextval('seq_Published_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_Published_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO Published_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

-- Restore saved autohistory.
INSERT INTO Published_Autohistory
(
    publishId,accountId,projectId,firstPublishId,versionMajor,versionMinor,versionDot,versionBuildNumber,
    categoryId,name,description,thumbnailId,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,
    editedById,retired,retiredById,retiredDateTime,screenshotId,hideInStoreList,featured,published_autohistory_id,autohistorydate
)
SELECT
    publishId,accountId,projectId,firstPublishId,versionMajor,versionMinor,versionDot,versionBuildNumber,
    categoryId,name,description,thumbnailId,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,
    editedById,retired,retiredById,retiredDateTime,screenshotId,hideInStoreList,0,published_autohistory_id,autohistorydate
FROM temp_Published_Autohistory;

DROP TABLE temp_Published_Autohistory;

update published set featured = 1 where publishid in (8185,8187,8009,8039,8197,8192,8200,8195,8040,8098);
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;


