--------------------------------------------------------------------------------
-- fn_categorys.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- Categories functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Categories' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('Categories','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Categories','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Categories','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Categories','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Categories','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Categories','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Categories','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Categories','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Categories','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Categories','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Categories','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Categories','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Categories','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('Categories','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Categories','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Categories','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Categories','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_category_create(text, text, integer, text, text, integer, integer, integer, text)

-- DROP FUNCTION fn_category_create(text, text, integer, text, text, integer, integer, integer, text);

CREATE OR REPLACE FUNCTION fn_category_create(in_credentials_email text, in_credentials_password text, in_parentCategoryId integer, in_name text, in_description text, in_typeId integer, in_categoryOrder integer, in_categoryLevel integer, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Categories';
        var_functionName text := 'CREATE';

        new_categoryId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into Categories(
            parentCategoryId,
            name,
            description,
            typeId,
            categoryOrder,
            categoryLevel,
            editedById
        )
        values(
            in_parentCategoryId,
            in_name,
            in_description,
            in_typeId,
            in_categoryOrder,
            in_categoryLevel,
            in_editedById
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_categoryId := currval('seq_Categories_categoryId');
        END IF;

        return new_categoryId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_category_create(text, text, integer, text, text, integer, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_category_update(text, text, integer, integer, text, text, integer, integer, integer, text, text)

-- DROP FUNCTION fn_category_update(text, text, integer, integer, text, text, integer, integer, integer, text, text);

CREATE OR REPLACE FUNCTION fn_category_update(in_credentials_email text, in_credentials_password text, in_categoryId integer, in_parentCategoryId integer, in_name text, in_description text, in_typeId integer, in_categoryOrder integer, in_categoryLevel integer, in_editedById text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Categories';
        var_functionName text := 'UPDATE';

        rec Categories%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from Categories where categoryId = in_categoryId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_parentCategoryId' THEN
                rec.parentCategoryId := in_parentCategoryId;
            ELSEIF param_name = 'in_name' THEN
                rec.name := in_name;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_typeId' THEN
                rec.typeId := in_typeId;
            ELSEIF param_name = 'in_categoryOrder' THEN
                rec.categoryOrder := in_categoryOrder;
            ELSEIF param_name = 'in_categoryLevel' THEN
                rec.categoryLevel := in_categoryLevel;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE Categories SET
            parentCategoryId = rec.parentCategoryId,
            name = rec.name,
            description = rec.description,
            typeId = rec.typeId,
            categoryOrder = rec.categoryOrder,
            categoryLevel = rec.categoryLevel,
            editedById = rec.editedById
        WHERE categoryId = in_categoryId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_category_update(text, text, integer, integer, text, text, integer, integer, integer, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_category_retire(text, text, integer, text)

-- DROP FUNCTION fn_category_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_category_retire(in_credentials_email text, in_credentials_password text, in_categoryId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Categories';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE Categories SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE categoryId = in_categoryId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_category_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_category_filter(text, text, integer, integer, text, integer, text, text, integer, integer, integer, text)

-- DROP FUNCTION fn_category_filter(text, text, integer, integer, text, integer, text, text, integer, integer, integer, text);

CREATE OR REPLACE FUNCTION fn_category_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_parentCategoryId integer, in_name text, in_description text, in_typeId integer, in_categoryOrder integer, in_categoryLevel integer, in_filter_list text)
  RETURNS SETOF Categories AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Categories';
        var_functionName text := 'FILTER';

        rec Categories%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM Categories WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_categoryId' THEN
            ELSEIF filter_name = 'in_parentCategoryId' THEN
                var_query := var_query || ' AND parentCategoryId = ' || in_parentCategoryId;
            ELSEIF filter_name = 'in_name' THEN
                var_query := var_query || ' AND name ILIKE ''%' || in_name || '%''';
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_typeId' THEN
                var_query := var_query || ' AND typeId = ' || in_typeId;
            ELSEIF filter_name = 'in_categoryOrder' THEN
                var_query := var_query || ' AND categoryOrder = ' || in_categoryOrder;
            ELSEIF filter_name = 'in_categoryLevel' THEN
                var_query := var_query || ' AND categoryLevel = ' || in_categoryLevel;
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            ELSEIF filter_name = 'in_retired' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM Categories;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_category_filter(text, text, integer, integer, text, integer, text, text, integer, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_category_find(text, text, integer)

-- DROP FUNCTION fn_category_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_category_find(in_credentials_email text, in_credentials_password text, in_categoryId integer)
  RETURNS SETOF Categories AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Categories';
        var_functionName text := 'FIND';

        rec Categories%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM Categories
                WHERE categoryId = in_categoryId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_category_find(text, text, integer) OWNER TO postgres;
