-------------------------------------------------------------------------------
-- recreate publishedname column as varchar instead of text 
-- set some defaults for projects.urlName, where appropriate
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (74);

alter table projects drop column urlName;
alter table projects add column urlName varchar(256);

alter table projects add column tmp_publishedname varchar(256);
update projects set tmp_publishedname = substring(publishedname from 0 for 256);
alter table projects drop column publishedname;
alter table projects rename column tmp_publishedname to publishedname;


-- can't nest function calls in pgsql, so we have to make two passes.
update projects set urlName = (
    select regexp_replace(publishedName, E'\\s', '_', 'g'))
  where published = true and retired = false and nextpublishedversionid is null;

update projects set urlName = (
    select regexp_replace(urlName,  E'[^\\w\\-]', '', 'g'))
  where published = true and retired = false and nextpublishedversionid is null;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;