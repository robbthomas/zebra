
-- fn_registration.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for advanced registration tasks.
--
--------------------------------------------------------------------------------

DROP TYPE IF EXISTS _type_Registration_Create_ CASCADE;

CREATE TYPE _type_Registration_Create_ AS (
    appUserId text,
    accountId integer,
    accountTypeId integer,
    accountRoleTypeId integer
);

CREATE OR REPLACE FUNCTION fn_registration_create (in_accountType text, in_firstName text, in_lastName text, in_email text, in_password text, in_passwordSalt text)
  RETURNS SETOF _type_Registration_Create_ AS
$BODY$
    Declare
        new_appUserId text := '';
        new_appUserDisplayName text := in_email;
        new_appUserDescription text := 'Description for user ' || in_firstName || ' ' || in_lastName;

        new_accountId int := -1;
        new_accountTypeId int := -1;
        new_accountName text := 'Account ' || in_firstName || ' ' || in_lastName;
        new_accountDescription text := 'Description for Account ' || in_firstName || ' ' || in_lastName;

        new_accountRoleTypeId int := -1;

        nullEditedById text;
        nullCompanyId int;

        success int := -1;

        rec _type_Registration_Create_;

    BEGIN
        -- create new user
        new_appUserId := fn_appuser_create(new_appUserDisplayName, new_appUserDescription, in_email, in_firstName, in_lastName, in_password, in_passwordSalt, nullEditedById);

        IF length(new_appUserId) > 0 THEN
            -- create account for new user
            new_accountTypeId := accountTypeId from AccountTypes where tag ilike in_accountType;
            new_accountId := fn_account_create(nullCompanyId, new_accountTypeId, new_accountName, new_accountDescription, 0, new_appUserId);

            IF new_accountId > 0 THEN
                -- make new user account admin
                new_accountRoleTypeId := RoleTypeId from RoleTypes where tag ilike 'ROLE_TYPE_ACCOUNT_ADMINISTRATOR';

                IF new_accountRoleTypeId > 0 THEN
                    -- make new user account admin
                    --success := fn_accountmemberrole_create(new_accountId, new_appUserId, new_accountRoleTypeId);
                    success := fn_accountmemberrole_create(new_accountId, new_appUserId, 'ROLE_TYPE_ACCOUNT_ADMINISTRATOR');

                    IF success <= 0 THEN
                        -- something went wrong
                    END IF;

                 END IF;

            END IF;

        END IF;

        FOR rec IN
            SELECT new_appUserId, new_accountId, new_accountTypeId, new_accountRoleTypeId
        LOOP
            RETURN NEXT rec;
        END LOOP;
    End;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_registration_create(text, text, text, text, text, text) OWNER TO postgres;



-- PROTOTYPE filter function with both filter and sort order lists
-- Example psql query: select * from fn_appuser_filter(6,0,'','','alleni.com','','','in_email','firstName:a,lastName:a');
/*
CREATE OR REPLACE FUNCTION fn_appuser_filter(
    in_count integer, in_offset integer, in_order_list text, in_displayName text, in_description text, in_email text, in_firstName text, in_lastName text,
    in_filter_list text)
  RETURNS SETOF app_user AS
$BODY$
    DECLARE
        rec App_User%ROWTYPE;
        filter_name text;
        var_query text := 'SELECT * FROM App_User WHERE retired = ''f''';
        var_order text;
        var_count int := 1;
    Begin
        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_displayName' THEN
                var_query := var_query || ' AND displayName ILIKE ''%' || in_displayName || '%''';
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_email' THEN
                var_query := var_query || ' AND  email ILIKE ''%' || in_email || '%''';
            ELSEIF filter_name = 'in_firstName' THEN
                var_query := var_query || ' AND  firstName ILIKE ''%' || in_firstName || '%''';
            ELSEIF filter_name = 'in_lastName' THEN
                var_query := var_query || ' AND  lastName ILIKE ''%' || in_lastName || '%''';
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM App_User;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        RAISE NOTICE 'EXECUTE %', var_query;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_appuser_filter(integer, integer, text, text, text, text, text, text, text) OWNER TO postgres;
*/