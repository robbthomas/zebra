-- fn_subscription.sql
--
-- Allen Interactions Copyright 2011
--
---------------------------------------------------------------------
-- Function: fn_subscription_new(text, text, integer)

-- DROP FUNCTION fn_subscription_new(text, text, integer);

CREATE OR REPLACE FUNCTION fn_subscription_new(in_credentials_email text, in_credentials_password text, in_accountId integer)
  RETURNS integer AS
$BODY$
    Declare
        var_credentialsId text;

        new_invoiceId int := -1;
        var_count int := -1;
        var_newTermDueDateTime timestamp;
        var_terms text := 0;
        rtype ProductCodes%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- get product code information
        SELECT INTO rtype ProductCodes.* 
            FROM
                Accounts
                inner join AccountTypes on AccountTypes.accounttypeId = Accounts.typeId
                inner join ProductCodes on ProductCodes.productCodeId = AccountTypes.initialProductCodeId
           WHERE accountId = in_accountId;

        INSERT INTO Invoices(accountId, cachedTotal, currencyTypeId, description, createdDateTime)
        VALUES(in_accountId, rtype.price, 1, rtype.description, now());

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_invoiceId := currval('seq_Invoices_invoiceId');

            -- create invoice line item for the subscription
            INSERT INTO InvoiceLineItems(invoiceId, amountTotal, description, productcodeid)
            VALUES (new_invoiceId, rtype.price, rtype.description, rtype.productcodeid);

            -- set account termDueDateTime
            -- in 1301 we should move the 'terms' field to ProductCodes
            -- TODO: don't use the same field for display and for interval casting in the db
            var_terms := terms FROM AccountTypes, Accounts where accountId = in_accountId and typeId = accountTypeId;
            
            var_newTermDueDateTime := termDueDateTime FROM Accounts WHERE accountId = in_accountId;
            IF var_newTermDueDateTime > current_date THEN
                var_newTermDueDateTime := var_newTermDueDateTime + var_terms::interval;
            ELSE
                var_newTermDueDateTime := current_date + var_terms::interval;
            END IF;

            UPDATE Accounts SET goodStanding = 1, termDueDateTime = var_newTermDueDateTime WHERE accountId = in_accountId;

        END IF;

        return new_invoiceId;
    End;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_subscription_new(text, text, integer) OWNER TO postgres;


---------------------------------------------------------------------
-- Function: fn_subscriptions_renew(text, text, integer)

DROP TYPE IF EXISTS _type_subscriptions_renew_ CASCADE;

CREATE TYPE _type_subscriptions_renew_ AS (
    accountId bigint,              -- Accounts
    accountName text,
    termDueDateTime timestamp,
    newTermDueDateTime timestamp,
    goodStanding int,
    creditCardId bigint, 		-- CreditCards
    accountTypeName text,       -- AccountTypes
    accountTypeId int4,
    terms text,
    price int,
    currencyTypeId int,
    firstName text,             -- App_User, for use Emailing account holders
    lastName text,
    displayName text,
    email text,
    invoiceId bigint,
    productCodeId int4, -- not reflected in the DAO ,
    productCodeDescription text,
    numLearners int4
);


CREATE OR REPLACE FUNCTION fn_subscriptions_renew(in_credentials_email text, in_credentials_password text, withinHoldingDays integer)
  RETURNS SETOF _type_subscriptions_renew_ AS
$BODY$
    Declare
        var_credentialsId text;
        new_invoiceId int := -1;
        var_count int := -1;
        var_invoiceId int;
        var_invoiceLineItemId int;
        var_holdingDaysInterval text;

        rec _type_subscriptions_renew_%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

            var_holdingDaysInterval := CAST((COALESCE(withinHoldingDays,0) + 1) As text) || ' days';

                drop table if exists tmp_tbl_subscriptions_renew;
                CREATE TEMPORARY TABLE tmp_tbl_subscriptions_renew (
                    accountId bigint,
                    accountName text,
                    termDueDateTime timestamp,
                    newTermDueDateTime timestamp,
    				        goodstanding int,
    				        creditCardId bigint,
                    accountTypeName text,
                    accountTypeId int4,
                    terms text,
                    price int,
                    currencyTypeId int,
                    firstName text,
                    lastName text,
                    displayName text,
                    email text,
                    invoiceId bigint,
                    productCodeId int4,
                    productCodeDescription text,
                    numLearners int4
                );

            -- get list of accounts due for renewal within holding days
           INSERT INTO tmp_tbl_subscriptions_renew
           SELECT distinct acct.accountId
           , acct.name As accountName
           , termDueDateTime
           , termDueDateTime
           , acct.goodstanding
           , cc.creditcardid
           , atyp.name As accountTypeName
           , atyp.accountTypeId
           , atyp.terms
           , ProductCodes.price
           , currencyTypeId
           , au.firstName
           , au.lastName
           , au.displayName
           , au.email
           , 0
           , ProductCodes.productCodeId
           , ProductCodes.description
           , numlearners
            FROM Accounts acct
            	inner join AccountTypes atyp on acct.typeId = atyp.accountTypeId
            	inner join AccountMemberRoles amr on acct.accountId = amr.accountId
            	inner join RoleTypes rtyp on amr.typeId = rtyp.roleTypeId
            	inner join App_User au on amr.memberId = au.Id
                inner join ProductCodes on ProductCodes.productCodeId = atyp.recurringProductCodeId
            	left outer join CreditCards cc on (cc.accountid = acct.accountid AND cc.retired = 0)
            WHERE acct.goodStanding = 1
            AND acct.termDueDateTime < current_date + var_holdingDaysInterval::interval
            AND ProductCodes.price > 0 
            AND rtyp.tag = 'ROLE_TYPE_ACCOUNT_ADMINISTRATOR'
            AND acct.renewalNoticeSent = 0;

            -- for accounts which may have returned to goodStanding AFTER termDueDateTime passed,
            -- reset termDueDateTime to current date so that subscription term will reset to
            -- begin today
            --
            -- COMMENTING OUT this item -- if and how termDueDateTime gets reset when account
            -- good standing is restored is a business decision we can control in the functionality
            -- that restores good standing -- PMQ
            -- Putting this reset of termDueDateTime back. It should be unnecessary because these
            -- are reset on entering good credit card or upgrading from collector
            -- but this will catch anyone who somehow got missed.

            UPDATE tmp_tbl_subscriptions_renew
            SET termDueDateTime = current_date WHERE termDueDateTime < current_date;

            -- calculate subscription intervals and new due dates based on account types
            UPDATE  tmp_tbl_subscriptions_renew
            SET newTermDueDateTime = termDueDateTime + terms::interval;

            -- create new invoices and invoicelineitems for the subscription charges
            FOR rec IN
                SELECT * FROM tmp_tbl_subscriptions_renew
            LOOP
                INSERT INTO Invoices (
                      accountId
                    , currencyTypeId
                    , holdingDateTime
                    , description
                ) VALUES (
                      rec.accountId
                    , rec.currencyTypeId
                    , rec.termDueDateTime
                    , rec.productCodeDescription
                );

                var_invoiceId := currval('seq_Invoices_invoiceId');

                INSERT INTO InvoiceLineItems(
                      invoiceId
                    , amountTotal
                    , description
                    , productCodeId
                    , lineNumber
                ) VALUES (
                      var_invoiceId
                    , rec.price
                    , rec.productCodeDescription
                    , rec.productCodeId
                    , 1
                );
                
                -- id:328, 401-100-0  Free Learners
                -- id:331, 401-101-0  100 Learners
                -- id:334, 401-102-0  500 Learners
                -- id:337, 401-103-0 1000 Learners   
 
              
                    INSERT INTO InvoiceLineItems(
                          invoiceId
                        , amountTotal
                        , description
                        , productCodeId
                        , lineNumber
                    ) (select 
                          var_invoiceId
                        , pc.price
                        , pc.description
                        , pc.productCodeId
                        , 2
                        from ProductCodes pc
                        where pc.productCodeId = 
                        CASE    
                            WHEN rec.accountTypeId < 3 THEN NULL
                            WHEN rec.numLearners IS NULL OR rec.numLearners <= 25 THEN 328
                            WHEN rec.numLearners <= 100 THEN 331
                            WHEN rec.numLearners <= 500 THEN 334
                            WHEN rec.numLearners <= 1000 THEN 337
                        END
                    );

                UPDATE invoices
                SET cachedTotal = (
                  SELECT sum(amountTotal)
                  FROM invoicelineitems
                  WHERE invoicelineitems.invoiceid = invoices.invoiceid
                )
                WHERE invoices.invoiceid = var_invoiceId;
    

                UPDATE tmp_tbl_subscriptions_renew
                SET 
                      invoiceId = var_invoiceId
                WHERE accountId = rec.accountId;
            END LOOP;

            -- update accounts term due dates and renewalNoticeSent
            UPDATE Accounts SET termDueDateTime = newTermDueDateTime,
                    renewalNoticeSent = 1
            FROM tmp_tbl_subscriptions_renew
            WHERE Accounts.accountId = tmp_tbl_subscriptions_renew.accountId;
    
            -- return list of accounts renewed
            FOR rec IN
                SELECT * FROM tmp_tbl_subscriptions_renew
            LOOP
                RETURN NEXT rec;
            END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_subscriptions_renew(text, text, integer) OWNER TO postgres;

