-------------------------------------------------------------------------------
-- remove obsolete foreign keys to parent
--
-- replace them with keys to projects where necessary
-- 
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (71);

-- already have another key
alter TABLE "gadget_state" drop CONSTRAINT "fk342ad2de2bc8c320";
alter TABLE "parent_child" drop CONSTRAINT "fk52e8b9476409620d";
alter TABLE "parent_child" drop CONSTRAINT "fk52e8b9477b50127d";


-- drop bad one, make new one
alter TABLE "feedback" drop CONSTRAINT "fk_feedback_reprojectid";
alter TABLE "feedback" add CONSTRAINT "fk_feedback_reprojectid" FOREIGN KEY (reprojectid) REFERENCES projects(projectid);

-- we don't use this table yet, but we should.  Fix it.
alter TABLE "screenshots" drop CONSTRAINT "fk_screenshots_projectid";
alter TABLE "screenshots" add CONSTRAINT "fk_screenshots_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectId);


-- we don't really care, but drop it anyway. 
alter TABLE "gadget_ribbon" drop CONSTRAINT "fk4edfe02972616c5f";
alter TABLE "gadget_permission" drop CONSTRAINT "fk9ec39fa2b74c7a3d";
alter TABLE "published" drop CONSTRAINT "fk_published_projectid";
alter TABLE "user_gadget_permission" drop CONSTRAINT "fkad74382eb74c7a3d";
alter  TABLE "parent" drop CONSTRAINT "fkc4ab08aa6f939afd";
alter TABLE "parent" drop CONSTRAINT "fkc4ab08aaa2a357e9";
alter TABLE "gadget_component" drop CONSTRAINT "fkd3b53cca9e059080";
alter TABLE "gadget_wire" drop CONSTRAINT "fkd86624988d41e14e";

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;