-------------------------------------------------------------------------------
-- r100_to_r101.sql
--
-- Author:  Joe Bodell
-- Date:  11-28-2012
-- Notes:  Add the banner image id to App_User, then update the AppUser custom return type
--
-------------------------------------------------------------------------------

ALTER TABLE App_User add column bannerId int4 NULL;


--there was more here, but it is now a noop

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (101);
------------------------------------------------------