-------------------------------------------------------------------------------
-- r95_to_r96.sql
--
-- Author:  Joe Bodell
-- Date:  10-05-2012
-- Notes:  Change the definition of the fn_appuser_findbyId function
--
-------------------------------------------------------------------------------

--NOOP

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (96);
------------------------------------------------------