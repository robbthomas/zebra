--------------------------------------------------------------------------------
-- fn_invoicelineitems.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- InvoiceLineItems functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='InvoiceLineItems' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_invoicelineitem_create(text, text, integer, integer, integer, integer, text, integer)

DROP FUNCTION IF EXISTS fn_invoicelineitem_create(text, text, integer, integer, integer, integer, text, integer); -- drop publishId version

-- DROP FUNCTION fn_invoicelineitem_create(text, text, integer, integer, integer, integer, text, text); -- newer projectId version

CREATE OR REPLACE FUNCTION fn_invoicelineitem_create(in_credentials_email text, in_credentials_password text, in_invoiceId integer, in_lineNumber integer, in_quantity integer, in_amountTotal integer, in_description text, in_projectid text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'InvoiceLineItems';
        var_functionName text := 'CREATE';

        new_invoiceLineItemId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into InvoiceLineItems(
            invoiceId,
            lineNumber,
            quantity,
            amountTotal,
            description,
            projectId
        )
        values(
            in_invoiceId,
            in_lineNumber,
            in_quantity,
            in_amountTotal,
            in_description,
            in_projectId
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_invoiceLineItemId := currval('seq_InvoiceLineItems_invoiceLineItemId');
        END IF;

        return new_invoiceLineItemId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_invoicelineitem_create(text, text, integer, integer, integer, integer, text, integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoicelineitem_update(text, text, integer, integer, integer, integer, integer, text, integer, text)

DROP FUNCTION IF EXISTS fn_invoicelineitem_update(text, text, integer, integer, integer, integer, integer, text, integer, text); -- drop publishId version

-- DROP FUNCTION fn_invoicelineitem_update(text, text, integer, integer, integer, integer, integer, text, text, text); -- newer projectId version

CREATE OR REPLACE FUNCTION fn_invoicelineitem_update(in_credentials_email text, in_credentials_password text, in_invoiceLineItemId integer, in_invoiceId integer, in_lineNumber integer, in_quantity integer, in_amountTotal integer, in_description text, in_projectId text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'InvoiceLineItems';
        var_functionName text := 'UPDATE';

        rec InvoiceLineItems%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from InvoiceLineItems where invoiceLineItemId = in_invoiceLineItemId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_invoiceId' THEN
                rec.invoiceId := in_invoiceId;
            ELSEIF param_name = 'in_lineNumber' THEN
                rec.lineNumber := in_lineNumber;
            ELSEIF param_name = 'in_quantity' THEN
                rec.quantity := in_quantity;
            ELSEIF param_name = 'in_amountTotal' THEN
                rec.amountTotal := in_amountTotal;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_projectId' THEN
                rec.projectId := in_projectId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE InvoiceLineItems SET
            invoiceId = rec.invoiceId,
            lineNumber = rec.lineNumber,
            quantity = rec.quantity,
            amountTotal = rec.amountTotal,
            description = rec.description,
            projectId = rec.projectId
        WHERE invoiceLineItemId = in_invoiceLineItemId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_invoicelineitem_update(text, text, integer, integer, integer, integer, integer, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoicelineitem_retire(text, text, integer, text)

-- DROP FUNCTION fn_invoicelineitem_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_invoicelineitem_retire(in_credentials_email text, in_credentials_password text, in_invoiceLineItemId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'InvoiceLineItems';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE InvoiceLineItems SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE invoiceLineItemId = in_invoiceLineItemId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_invoicelineitem_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoicelineitem_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, integer, text)

DROP FUNCTION IF EXISTS fn_invoicelineitem_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, integer, text); -- drop publishId version

-- DROP FUNCTION fn_invoicelineitem_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, text, text); -- newer projectId version

CREATE OR REPLACE FUNCTION fn_invoicelineitem_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_invoiceId integer, in_lineNumber integer, in_quantity integer, in_amountTotal integer, in_description text, in_projectId text, in_filter_list text)
  RETURNS SETOF InvoiceLineItems AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'InvoiceLineItems';
        var_functionName text := 'FILTER';

        rec InvoiceLineItems%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        --var_query text := 'SELECT * FROM InvoiceLineItems WHERE retired = 0';
        var_query text := 'SELECT * FROM InvoiceLineItems WHERE 0 = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_invoiceLineItemId' THEN
            ELSEIF filter_name = 'in_invoiceId' THEN
                var_query := var_query || ' AND invoiceId = ' || in_invoiceId;
            ELSEIF filter_name = 'in_lineNumber' THEN
                var_query := var_query || ' AND lineNumber = ' || in_lineNumber;
            ELSEIF filter_name = 'in_quantity' THEN
                var_query := var_query || ' AND quantity = ' || in_quantity;
            ELSEIF filter_name = 'in_amountTotal' THEN
                var_query := var_query || ' AND amountTotal = ' || in_amountTotal;
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_projectId' THEN
                var_query := var_query || ' AND projectId = ''' || in_projectId || '''';
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM InvoiceLineItems;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_invoicelineitem_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoicelineitem_find(text, text, integer)

-- DROP FUNCTION fn_invoicelineitem_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_invoicelineitem_find(in_credentials_email text, in_credentials_password text, in_invoiceLineItemId integer)
  RETURNS SETOF InvoiceLineItems AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'InvoiceLineItems';
        var_functionName text := 'FIND';

        rec InvoiceLineItems%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM InvoiceLineItems
                WHERE invoiceLineItemId = in_invoiceLineItemId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_invoicelineitem_find(text, text, integer) OWNER TO postgres;
