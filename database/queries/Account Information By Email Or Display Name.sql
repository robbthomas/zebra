SELECT 
      app_user.id
    , app_user.date_created
    , app_user.retired
    , app_user.email
    , app_user.displayName
    , accounts.accountId
    , accounts.termDueDateTime
    , accounts.goodstanding
    , accountTypes.tag
FROM app_user 
    INNER JOIN accountMemberRoles amr
        ON app_user.id = amr.memberId
        INNER JOIN accounts
            ON amr.accountId = accounts.accountId
            INNER JOIN accountTypes
                ON accounts.typeId = accountTypes.accountTypeId
WHERE email = 'admin@admin.com'
OR displayName = 'jbAdmin'


select 
ProjectMemberAccess.*
, au.displayName as senderDisplayName
, au2.displayName as receiverDisplayName
, au.email as senderEmail
, au2.email as receiverEmail
, a.name as senderAccountName
, a2.name as receiverAccountName
, projects.price
, projects.projectname
, RetiredReasonTypes.name as retiredReasonCode
, publishImages.iconFileId 
from ProjectMemberAccess   
    inner join app_user au 
        on au.id = ProjectMemberAccess.senderMemberid   
    inner join accounts a 
        on a.accountid = ProjectMemberAccess.senderAccountId   
    inner join app_user au2 
        on au2.id = ProjectMemberAccess.receiverMemberId   
    inner join accounts a2 
        on a2.accountid = ProjectMemberAccess.receiverAccountId   
    inner join projects 
        on projects.projectid = ProjectMemberAccess.projectid 
        left outer join publishimages 
            on publishimages.projectid = projects.projectid  
            and iconFileId is not null   
    left outer join RetiredReasonTypes 
        on RetiredReasonTypes.retiredReasonTypeId = ProjectMemberAccess.retiredReasonTypeId   

where true 
and ProjectMemberAccess.receiverMemberId = '064a029aabdc47eeb87ddd69d7aa4096' 
and (ProjectMemberAccess.retiredreasontypeid is null or ProjectMemberAccess.retiredreasontypeid <> 6) 
and ProjectMemberAccess.hideFromReceiver = false





















SELECT * FROM accounts LIMIT 10

CREATE TABLE z_Accounts_history (
      ID SERIAL PRIMARY KEY
    , logDate TIMESTAMP
    , deleted BOOLEAN
    , accountId INT8
    , companyId INT4
    , typeId INT4
    , termduedatetime TIMESTAMP
    , goodstanding INT4
    , name TEXT
    , description TEXT
    , tokenbalance INT4
    , editedById TEXT
    , editedDateTime TIMESTAMP
    , retired INT4
    , retiredDateTime TIMESTAMP
    , showCompanyInfoForAll INT4
    , companySameAsPersonal INT4
    , paypallogin VARCHAR(255)
    , payaccountfirst INT4
    , convertamount INT4
    , merchantCustomerId TEXT
    , convertMethod VARCHAR(255)
    , contactPreference VARCHAR(255)
    , allowSubscriptionPayment INT4
    , maxInvitesPerProject INT4
    , maxInvitesPerAccount INT4
    , numLearners INT4
    , numMembers INT4
    , billingContactSameAsPersonal INT4
    , billingAddressSameAsCompany INT4
    , bannerId INT4
    , renewalNoticeSent INT4
);

