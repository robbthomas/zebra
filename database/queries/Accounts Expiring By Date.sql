SELECT firstname, lastname, accounts.accountId, accountTypes.tag, email, accounts.termduedatetime, count(amr.*)
FROM app_user
    INNER JOIN accountmemberRoles amr
        ON app_user.id = amr.memberId
        INNER JOIN accounts
            ON amr.accountId = accounts.accountid
            INNER JOIN accountTypes
                ON accounts.typeid = accountTypes.accountTypeId
WHERE accounts.termduedateTime > '2013-02-24' AND accounts.termDueDateTime < '2013-02-25' 
AND app_user.retired = FALSE
AND accounts.retired = 0
AND email NOT LIKE '%allenlt.com%'
AND email NOT LIKE '%alleni.com%'
AND email NOT LIKE '%zebrazapps.com%'
AND email NOT LIKE 'ShortStuf7%'
AND email NOT LIKE 'TomKCarroll%'
AND email NOT LIKE '%qualityprocess.com'
AND email NOT LIKE 'mbraaten%'
AND lastname <> 'Grannes'
AND lastname <> 'Korte'
AND firstname <> ''
AND firstname NOT LIKE 'javascript:%'
AND email NOT LIKE '%email.com'
AND NOT (firstname = 'Test' AND lastname = 'User')
GROUP BY firstname, lastname, accounts.accountId, accountTypes.tag, email,accounts.termduedatetime
ORDER BY lastname, firstname