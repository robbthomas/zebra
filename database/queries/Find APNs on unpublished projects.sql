SELECT apn.accountId, projects.projectTypeId, au.email, COUNT(*) 
FROM accountProjectNumbers apn
    left join AccountMemberRoles amr 
        on apn.accountId = amr.accountId 
        and apn.accountid <> 1
    left join RoleTypes rtyp 
        on amr.typeId = rtyp.roleTypeId 
        and rtyp.tag = 'ROLE_TYPE_ACCOUNT_ADMINISTRATOR'
    left join App_User au on amr.memberId = au.Id
    INNER JOIN projects
        ON apn.projectId = projects.projectId
WHERE apn.retired = 0
AND projects.publishedName IS NULL
GROUP BY apn.accountId, projects.projectTypeId, au.email