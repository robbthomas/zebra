SELECT 
      to_char(date_trunc('day', sourceProjects.createdDateTime), 'Dy MM/DD/YYYY') AS ProjectCreationDate
    , projectAccountType.tag AS AccountType
    , COUNT(projects.projectId)
FROM (
    SELECT 
          projectName
        , min(createdDateTime) AS createdDateTime
    FROM projects
        INNER JOIN app_user
            ON projects.authorMemberId = app_user.id
    WHERE 1=1
    AND app_user.email NOT LIKE '%alleni.com' AND app_user.email NOT LIKE '%allenlt.com'
    GROUP BY projectName
    HAVING min(createdDateTime) > '2013-01-01' AND min(createdDateTime) < '2013-02-01'
) AS sourceProjects
    INNER JOIN projects
        ON sourceProjects.projectName = projects.projectName
        INNER JOIN accountTypes projectAccountType
            ON projects.accountTypeId = projectAccountType.accountTypeId
WHERE sourceProjects.createdDateTime > '2013-01-01' AND sourceProjects.createdDateTime < '2013-02-01'
AND projects.nextVersionId IS NULL
GROUP BY date_trunc('day', sourceProjects.createdDateTime), projectAccountType.tag
ORDER BY projectAccountType.tag, date_trunc('day', sourceProjects.createdDateTime)


--Published Gadgets and Apps
SELECT 
      to_char(date_trunc('day', sourceProjects.createdDateTime), 'Dy MM/DD/YYYY') AS ProjectCreationDate
    , projectAccountType.tag AS AccountType
    , projectType.name
    , COUNT(projects.projectId)
FROM (
    SELECT 
          publishedName
        , min(createdDateTime) AS createdDateTime
    FROM projects
        INNER JOIN app_user
            ON projects.authorMemberId = app_user.id
    WHERE 1=1
    AND app_user.email NOT LIKE '%alleni.com' AND app_user.email NOT LIKE '%allenlt.com'
    GROUP BY publishedName
    HAVING min(createdDateTime) > '2013-01-01' AND min(createdDateTime) < '2013-02-01' 
) AS sourceProjects
    INNER JOIN projects
        ON sourceProjects.publishedName = projects.publishedName
        INNER JOIN accountTypes projectAccountType
            ON projects.accountTypeId = projectAccountType.accountTypeId
        INNER JOIN projectTypes AS projectType
            ON projects.projectTypeId = projectType.projectTypeId
WHERE sourceProjects.createdDateTime > '2013-01-01' AND sourceProjects.createdDateTime < '2013-02-01'
AND projects.nextVersionId IS NULL
GROUP BY date_trunc('day', sourceProjects.createdDateTime), projectAccountType.tag, projectType.name
ORDER BY projectAccountType.tag, date_trunc('day', sourceProjects.createdDateTime)