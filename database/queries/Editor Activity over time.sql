SELECT  
      to_char(date_trunc('day', app_user.date_created), 'Dy MM/DD/YYYY') AS SignupDate
    , app_user.firstname
    , app_user.lastname
    , app_user.email
    , COUNT(projectId) AS ActionCount
FROM app_user
    LEFT JOIN projects
        ON app_user.id = projects.authorMemberId
WHERE 1=1
 --and app_user.date_created >= '2011-10-31'
 --and app_user.date_created <= '2013-02-02'
 AND app_user.email NOT LIKE '%allenlt.com'
 AND app_user.email NOT LIKE '%alleni.com'
 AND app_user.email NOT LIKE '%alleninteractions.com'
 AND app_user.email NOT LIKE '%zebrazapps.com'
 AND app_user.email NOT LIKE '%@email.com'
 AND app_user.email NOT LIKE 'ShortStuf7%'
 AND app_user.email NOT LIKE '%qualityprocess.com'
 AND app_user.email NOT LIKE '%__CANCELED'
 AND app_user.email NOT IN (
      'gregdaigle@mac.com'
    , 'dgsobolewski@gmail.com'
    , 'mbraaten13@gmail.com'
    , 'TomKCarroll@gmail.com'
    , 'mike.braaten13@gmail.com'
    , 'erik.thomas@charter.net'
    , 'gran0439@UMN.EDU'
    , 'sjackson@jidev.com'
    , 'patrick@elearningeek.com'
    , 'joe.bodell@gmail.com'
)
GROUP BY app_user.date_created, app_user.email, app_user.firstname, app_user.lastname
ORDER BY COUNT(projectId) DESC


SELECT to_char(date_trunc('month', projects.createdDateTime), 'MM/YY') AS ProjectMonth, COUNT(*)
FROM projects
    INNER JOIN app_user ON projects.authorMemberId = app_user.id
WHERE 1=1
 AND app_user.email NOT LIKE '%allenlt.com'
 AND app_user.email NOT LIKE '%alleni.com'
 AND app_user.email NOT LIKE '%alleninteractions.com'
 AND app_user.email NOT LIKE '%zebrazapps.com'
 AND app_user.email NOT LIKE '%@email.com'
 AND app_user.email NOT LIKE 'ShortStuf7%'
 AND app_user.email NOT LIKE '%qualityprocess.com'
 AND app_user.email NOT LIKE '%__CANCELED'
 AND app_user.firstname <> 'template'
 AND app_user.email NOT IN (
      'gregdaigle@mac.com'
    , 'dgsobolewski@gmail.com'
    , 'mbraaten13@gmail.com'
    , 'TomKCarroll@gmail.com'
    , 'mike.braaten13@gmail.com'
    , 'erik.thomas@charter.net'
    , 'gran0439@UMN.EDU'
    , 'sjackson@jidev.com'
    , 'patrick@elearningeek.com'
    , 'joe.bodell@gmail.com'
)
GROUP BY date_trunc('month', projects.createdDateTime)
ORDER BY date_trunc('month', projects.createdDateTime)