--List
select distinct app_user.displayname, app_user.firstname, app_user.lastname, accounts.goodstanding, accounts.termduedateTime
, app_user.email, app_user.date_created, accounttypes.name as CurrentAccountType
from app_user
inner join accountmemberroles
  on accountmemberroles.memberid = app_user.id
inner join accounts
    LEFT JOIN creditcards
        ON accounts.accountId = creditcards.accountId
  on accounts.accountid = accountmemberroles.accountid
inner join accounttypes
  on accounttypes.accounttypeid = accounts.typeid
WHERE 1=1
 and app_user.date_created >= '2013-01-01'
 and app_user.date_created <= '2013-02-02'
 AND app_user.email NOT LIKE '%allenlt.com'
 AND app_user.email NOT LIKE '%alleni.com'
 AND app_user.email NOT LIKE '%zebrazapps.com'
order by accounttypes.name

--Summary by Date and Account Type
select date_trunc('day', app_user.date_created) AS createdDate, accounttypes.name as accountType, COUNT(*)
from app_user
inner join accountmemberroles
  on accountmemberroles.memberid = app_user.id
inner join accounts
    LEFT JOIN creditcards
        ON accounts.accountId = creditcards.accountId
  on accounts.accountid = accountmemberroles.accountid
inner join accounttypes
  on accounttypes.accounttypeid = accounts.typeid
WHERE 1=1
 and app_user.date_created >= '2013-01-01'
 and app_user.date_created <= '2013-02-02'
 AND app_user.email NOT LIKE '%allenlt.com'
 AND app_user.email NOT LIKE '%alleni.com'
 AND app_user.email NOT LIKE '%zebrazapps.com'
GROUP BY date_trunc('day', app_user.date_created), accountTypes.name
order by accounttypes.name, date_trunc('day', app_user.date_created)

--Summary by Account Type
select accounttypes.name as accountType, COUNT(*)
from app_user
inner join accountmemberroles
  on accountmemberroles.memberid = app_user.id
inner join accounts
    LEFT JOIN creditcards
        ON accounts.accountId = creditcards.accountId
  on accounts.accountid = accountmemberroles.accountid
inner join accounttypes
  on accounttypes.accounttypeid = accounts.typeid
WHERE 1=1
 and app_user.date_created >= '2013-01-01'
 and app_user.date_created <= '2013-02-01'
 AND app_user.email NOT LIKE '%allenlt.com'
 AND app_user.email NOT LIKE '%alleni.com'
 AND app_user.email NOT LIKE '%zebrazapps.com'
GROUP BY accountTypes.name
order by accounttypes.name