-------------------------------------------------------------------------------
-- r47_to_r48.sql
--
-- Create tables for tracking sharing of zapps, and email invites
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (48);
-------------------------------------------------------------------------------
alter table accounttypes add column maxInvitesPerProject int not null default 0;
alter table accounttypes add column maxInvitesPerAccount int not null default 0;
comment on column accounttypes.maxInvitesPerProject is 'default value for column of same name on table Accounts';
comment on column accounttypes.maxInvitesPerProject is 'default value for column of same name on table Accounts';

update accounttypes set maxInvitesPerProject = 100 where accounttypeid = 3;
update accounttypes set maxInvitesPerAccount = 100 where accounttypeid = 3;

alter table accounts add column maxInvitesPerProject int not null default 0;
alter table accounts add column maxInvitesPerAccount int not null default 0;

update accounttypes set maxInvitesPerProject = 100 where accountTypeId = 3;
update accounttypes set maxInvitesPerAccount = 100 where accountTypeId = 3;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

