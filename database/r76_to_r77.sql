-------------------------------------------------------------------------------
-- r76_to_r77.sql
--
-- Adding support for coupon codes
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

-- UPDATE THE COUPONTYPE TABLE

UPDATE CouponCodeTypes SET description = '2 FREE Months' WHERE description = '3 FREE Months';
INSERT INTO CouponCodeTypes(description) VALUES ('3 FREE Months');

    
    
-- TRUNCATE OLD COUPON CODES
TRUNCATE  CouponCodes;  

-- POPULATE THE COUPONCODE TABLE
INSERT INTO CouponCodes(code,couponCodeTypeId)
	SELECT code, 1 AS couponCodeTypeId
		FROM unnest(string_to_array('K9TRD,S7ZZV,TN7TM,B4WG8,DN4CV,VNVDQ,KJX2B,PWZZD,TL7FP,958RQ',',')) g(code);

INSERT INTO CouponCodes(code,couponCodeTypeId)
	SELECT code, 2 AS couponCodeTypeId
		FROM unnest(string_to_array('T63BD,QH5VF,9NBCJ,THX78,SNR78,JMD73,82VMV,PQL8H,J3HW7,3R4J7',',')) g(code);
		
INSERT INTO CouponCodes(code,couponCodeTypeId)
	SELECT code, 3 AS couponCodeTypeId
		FROM unnest(string_to_array('3PLL7,F23QW,9MLGD,H4DK6,XPQJF,8XC45,ZXL6N,X3HVB,PZD36,Z222V',',')) g(code);
		
-------------------------------------------------------------------------------
-- Update database version.
-------------------------------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES(77);

-------------------------------------------------------------------------------
-- End transaction wrapper for this update.
-------------------------------------------------------------------------------
COMMIT;
