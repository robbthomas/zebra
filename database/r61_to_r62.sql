-------------------------------------------------------------------------------
-- Add columns to projectmemberaccess for "hide from receiver" and "hide from sender"
-- 
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (62);

alter table ProjectMemberAccess
	add column hideFromReceiver boolean default false;
alter table ProjectMemberAccess
	add column hideFromSender boolean default false;

	

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;