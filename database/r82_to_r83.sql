-------------------------------------------------------------------------------
-- Add third project type for events
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES(83);

insert into projecttypes (projecttypeid, name, description, displayorder) values (3, 'Event', 'A single event page from an event flow', 30);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;