-------------------------------------------------------------------------------
-- r31_to_r32.sql
--   Change the account id of user 'guest'
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (32);
-------------------------------------------------------------------------------

delete from accountmemberroles where memberid = (select id from app_user where displayname = 'guest');

INSERT INTO Accounts (typeId, name, description, editedById, merchantcustomerid) 
        VALUES (1, 'guest account', 'guest account', (SELECT Id FROM App_User WHERE displayName ILIKE 'dhoyt'), 'nonrandomvalue');


INSERT INTO AccountMemberRoles (accountid, memberid, typeid, retired, retiredbyid, retireddatetime) 
        values ((select accountid from accounts where name = 'guest account'), (select id from app_user where displayname = 'guest'), 3, 0, null, null );


-- In some environments, guest belongs to account 1.  In others, guest belongs to its own account.  In those other cases, we will end up
-- with an orpan account (e.g. one with no associated members or roles).

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;


