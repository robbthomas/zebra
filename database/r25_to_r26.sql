-------------------------------------------------------------------------------
-- r25_to_r26.sql
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (26);
-------------------------------------------------------------------------------

-- DWH: bug in create code left this value null for a while
update publishimages set retired = false where retired is null;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
