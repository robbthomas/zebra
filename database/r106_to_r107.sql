-------------------------------------------------------------------------------
-- r105_to_r106.sql
--
-- Author:  David Hoyt
-- Notes:  add column denoting whether renewal notice has been sent
--
-------------------------------------------------------------------------------

alter table accounts
 add column renewalNoticeSent int4 default 0;

alter table creditcards 
    add column expirationNoticeSent int4 default 0;
------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (107);
------------------------------------------------------
         