-------------------------------------------------------------------------------
-- r4_to_r5.sql
--
-- Build database tables-functions-roles mapping table and function to add
-- new mappings.
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (5);
-------------------------------------------------------------------------------

CREATE SEQUENCE seq_ZappComments_zappCommentId START WITH 2000;

CREATE TABLE ZappComments (
    zappCommentId int NOT NULL PRIMARY KEY DEFAULT nextval('seq_zappComments_zappCommentId'),
    publishId bigint,
    memberId text,
    comment text,
    updatedDateTime timestamp DEFAULT now(),
    createdDateTime timestamp DEFAULT now(),

    retired int default(0),  -- 0=not retired
    retiredById text,  -- FK to App_User.Id (aka Members.memberId)
    retiredDateTime timestamp,

--    CONSTRAINT fk_zappComment_publishId FOREIGN KEY (publishId) REFERENCES Published(publishid),
    CONSTRAINT fk_zappComment_memberId FOREIGN KEY (memberId) REFERENCES App_User(Id),
    CONSTRAINT fk_zappComment_retiredById FOREIGN KEY (retiredById) REFERENCES App_User(Id)
);



CREATE SEQUENCE seq_ZappRatings_zappRatingId START WITH 2000;

CREATE TABLE ZappRatings (
    zappRatingId int NOT NULL PRIMARY KEY DEFAULT nextval('seq_ZappRatings_zappRatingId'),
    publishId bigint,
    memberId text,
    rating integer,
    updatedDateTime timestamp DEFAULT now(),
    createdDateTime timestamp DEFAULT now(),
    
    retired int default(0),  -- 0=not retired
    retiredById text,  -- FK to App_User.Id (aka Members.memberId)
    retiredDateTime timestamp,

--    CONSTRAINT fk_ZappRating_publishId FOREIGN KEY (publishId) REFERENCES Published(publishid),
    CONSTRAINT fk_ZappRating_memberId FOREIGN KEY (memberId) REFERENCES App_User(Id),
    CONSTRAINT fk_ZappRating_retiredById FOREIGN KEY (retiredById) REFERENCES App_User(Id)
);

CREATE SEQUENCE seq_ZappRatingCaches_zappRatingCacheId START WITH 2000;

CREATE TABLE ZappRatingCaches (
    zappRatingCacheId int NOT NULL PRIMARY KEY DEFAULT nextval('seq_ZappRatingCaches_zappRatingCacheId'),
    publishId bigint,
    numRatings integer,
    ratingTotal integer,
    updatedDateTime timestamp DEFAULT now(),
    createdDateTime timestamp DEFAULT now(),
    
    retired int default(0),  -- 0=not retired
    retiredById text,  -- FK to App_User.Id (aka Members.memberId)
    retiredDateTime timestamp,

--    CONSTRAINT fk_ZappRatingCache_publishId FOREIGN KEY (publishId) REFERENCES Published(publishid),
    CONSTRAINT fk_ZappRatingCache_retiredById FOREIGN KEY (retiredById) REFERENCES App_User(Id)
);


\i fn_zapprating.sql
\i fn_zappratingcache.sql
\i fn_zappcomment.sql
\i fn_updateratingandcache.sql;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
