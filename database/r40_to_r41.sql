-------------------------------------------------------------------------------
-- r40_to_r41.sql
--
-- updating plans and pricing information
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

UPDATE accounttypes SET name = 'Creator', description = '60-day No Obligation Free Trial', price = 899 WHERE tag = 'creator';

-------------------------------------------------------------------------------
-- Update database version.
-------------------------------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES(41);

-------------------------------------------------------------------------------
-- End transaction wrapper for this update.
-------------------------------------------------------------------------------
COMMIT;
