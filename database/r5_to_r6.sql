-------------------------------------------------------------------------------
-- r5_to_r6.sql
--
-- Build database tables-functions-roles mapping table and function to add
-- new mappings.
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (6);
-------------------------------------------------------------------------------

CREATE TABLE ContactPreferenceTypes (
    contactPreferenceTypeId int NOT NULL PRIMARY KEY,
    tag text,
    name text,
    displayOrder int,
    retired int DEFAULT 0,  -- 0=not retired, 1=retired
    retiredById text,  -- FK to App_User.Id (aka Members.memberId)
    retiredDateTime timestamp,

    CONSTRAINT fk_ContactPreferenceTypes_retiredById FOREIGN KEY (retiredById) REFERENCES App_User(Id)
);
INSERT INTO ContactPreferenceTypes(contactPreferenceTypeId,tag,name,displayOrder) VALUES (1,'phone','Phone',10);
INSERT INTO ContactPreferenceTypes(contactPreferenceTypeId,tag,name,displayOrder) VALUES (2,'email','EMail',20);



CREATE TABLE CreditCardTypes (
    creditCardTypeId int NOT NULL PRIMARY KEY,
    tag text,
    name text,
    displayOrder int,
    retired int DEFAULT 0,  -- 0=not retired, 1=retired
    retiredById text,  -- FK to App_User.Id (aka Members.memberId)
    retiredDateTime timestamp,

    CONSTRAINT fk_CreditCardTypes_retiredById FOREIGN KEY (retiredById) REFERENCES App_User(Id)
);
INSERT INTO CreditCardTypes(creditCardTypeId,tag,name,displayOrder) VALUES (1,'visa','Visa',10);
INSERT INTO CreditCardTypes(creditCardTypeId,tag,name,displayOrder) VALUES (2,'mastercard','MasterCard',20);
INSERT INTO CreditCardTypes(creditCardTypeId,tag,name,displayOrder) VALUES (3,'amex','American Express',30);
INSERT INTO CreditCardTypes(creditCardTypeId,tag,name,displayOrder) VALUES (4,'discover','Discover',40);
INSERT INTO CreditCardTypes(creditCardTypeId,tag,name,displayOrder) VALUES (5,'dinersclub','Diners Club',50);
INSERT INTO CreditCardTypes(creditCardTypeId,tag,name,displayOrder) VALUES (6,'jcb','JCB',60);

ALTER TABLE App_User ADD COLUMN website varchar(255);
ALTER TABLE App_User ADD COLUMN twitter varchar(255);

ALTER TABLE Addresses ADD COLUMN website varchar(255);
ALTER TABLE Addresses ADD COLUMN twitter varchar(255);
ALTER TABLE Addresses ADD COLUMN email varchar(255);
ALTER TABLE Addresses ADD COLUMN phone varchar(255);

ALTER TABLE Accounts ADD COLUMN showCompanyInfoForAll int;
ALTER TABLE Accounts ADD COLUMN companySameAsPersonal int;
ALTER TABLE Accounts ADD COLUMN billingAddressSameAsCompany int;
ALTER TABLE Accounts ADD COLUMN billingContactSameAsCompany int;
ALTER TABLE Accounts ADD COLUMN contactPreference varchar(255);
ALTER TABLE Accounts ADD COLUMN paypalLogin varchar(255);
--alter table Accounts add column convertMethod varchar(255); -- moved to r8_to_r9.sql
ALTER TABLE Accounts ADD COLUMN convertAmount int;


ALTER TABLE CreditCards ADD COLUMN creditCardType int;
ALTER TABLE CreditCards ADD COLUMN firstName varchar(255);
ALTER TABLE CreditCards ADD COLUMN lastName varchar(255);
ALTER TABLE CreditCards ADD COLUMN email varchar(255);
ALTER TABLE CreditCards ADD COLUMN phone varchar(255);
ALTER TABLE CreditCards ADD CONSTRAINT fk_CreditCards_cardType FOREIGN KEY (creditCardType) REFERENCES CreditCardTypes(creditCardTypeId);

-- add column to cache the Parent.content byte size
ALTER TABLE Parent ADD COLUMN gadgetSize integer;

-- add column to store transaction Id returned by CIM
ALTER TABLE FinancialTransactions ADD COLUMN cimTransactionId VARCHAR(255) DEFAULT '';

-- Recreate autohistory for FinancialTransactions. DO NOT save and restore any of the data in old table this time.
DROP TRIGGER IF EXISTS trg_FinancialTransactions_Update ON FinancialTransactions_AutoHistory CASCADE;
DROP SEQUENCE IF EXISTS seq_FinancialTransactions_AutoHistory CASCADE;
DROP TABLE IF EXISTS FinancialTransactions_AutoHistory CASCADE;

CREATE SEQUENCE seq_FinancialTransactions_AutoHistory;
CREATE TABLE FinancialTransactions_AutoHistory (
    LIKE FinancialTransactions,
    FinancialTransactions_AutoHistory_Id bigint DEFAULT nextval('seq_FinancialTransactions_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

-- create eCommerce Cron dedicated sys admin user matching credentials in commerce.cron
\i fn_appuser.sql -- reload in case all_functions.sql not yet loaded if re-up'ing from r0

SELECT * FROM fn_appuser_create('','','eCommCron','Dedicated eCommerce cron system administrator user','info@alleni.com','','','o4vBnE4Q11','salt9876','');

INSERT INTO SystemAdministrators(memberId, passwordHash, passwordSalt)
VALUES ((SELECT id FROM App_User WHERE displayName='eCommCron' LIMIT 1), encode(digest('o4vBnE4Q22' || 'salt8765','sha1'),'hex'), 'salt8765');

-- thumbnailId now points to a file in S3, not a record in Asset table
alter table published drop constraint fk_Published_thumbnailId;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
