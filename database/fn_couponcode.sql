--------------------------------------------------------------------------------
-- fn_couponcodes.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- CouponCodes functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='CouponCodes'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('CouponCodes','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CouponCodes','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CouponCodes','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('CouponCodes','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('CouponCodes','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('CouponCodes','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_couponcode_create(text, text, text, text)

-- DROP FUNCTION fn_couponcode_create(text, text, text, text);

CREATE OR REPLACE FUNCTION fn_couponcode_create(in_credentials_email text, in_credentials_password text, in_code text, in_couponCodeTypeId text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CouponCodes';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_couponCodeId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO CouponCodes(
            code,
            couponCodeTypeId
        )
        VALUES(
            in_code,
            in_couponCodeTypeId
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_couponCodeId := currval('seq_CouponCodes_couponCodeId');
        END IF;

        return new_couponCodeId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_couponcode_create(text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_couponcode_update(text, text, integer, text, text, text)

-- DROP FUNCTION fn_couponcode_update(text, text, integer, text, text, text);

CREATE OR REPLACE FUNCTION fn_couponcode_update(in_credentials_email text, in_credentials_password text, in_couponCodeId integer, in_code text, in_couponCodeTypeId text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CouponCodes';
        var_functionName text := 'UPDATE';

        rec CouponCodes%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from CouponCodes where couponCodeId = in_couponCodeId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_code' THEN
                rec.code := in_code;
            ELSEIF param_name = 'in_couponCodeTypeId' THEN
                rec.couponCodeTypeId := in_couponCodeTypeId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE CouponCodes SET
            code = rec.code,
            couponCodeTypeId = rec.couponCodeTypeId
        WHERE couponCodeId = in_couponCodeId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_couponcode_update(text, text, integer, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_couponcode_retire(text, text, integer, text)

-- DROP FUNCTION fn_couponcode_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_couponcode_retire(in_credentials_email text, in_credentials_password text, in_couponCodeId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CouponCodes';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE CouponCodes SET retiredDateTime = now()
        WHERE couponId = in_couponCodeId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_couponcode_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_couponcode_filter(text, text, integer, integer, text, text, text, text)

-- DROP FUNCTION fn_couponcode_filter(text, text, integer, integer, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_couponcode_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_code text, in_couponCodeTypeId text, in_filter_list text)
  RETURNS SETOF CouponCodes AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CouponCodes';
        var_functionName text := 'FILTER';

        rec CouponCodes%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM CouponCodes WHERE retiredDateTime is NULL';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);
		
        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_couponId' THEN
            ELSEIF filter_name = 'in_code' THEN
                var_query := var_query || ' AND code =''' || in_code || '''';
            ELSEIF filter_name = 'in_couponCodeTypeId' THEN
                var_query := var_query || ' AND couponCodeTypeId ILIKE ''%' || in_couponCodeTypeId || '%''';
            ELSEIF filter_name = 'in_retiredDateTime' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM CouponCodes;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;
        
		FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;
        END IF;
		
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_couponcode_filter(text, text, integer, integer, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_couponcode_find(text, text, integer)

-- DROP FUNCTION fn_couponcode_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_couponcode_find(in_credentials_email text, in_credentials_password text, in_couponCodeId integer)
  RETURNS SETOF CouponCodes AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CouponCodes';
        var_functionName text := 'FIND';

        rec CouponCodes%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM CouponCodes
                WHERE couponCodeId = in_couponCodeId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_couponcode_find(text, text, integer) OWNER TO postgres;