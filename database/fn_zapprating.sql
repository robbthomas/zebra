--------------------------------------------------------------------------------
-- fn_zappratings.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- ZappRatings functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='ZappRatings'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('ZappRatings','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ZappRatings','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ZappRatings','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ZappRatings','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('ZappRatings','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatings','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_zapprating_create(text, text, integer, text, integer)

-- DROP FUNCTION fn_zapprating_create(text, text, integer, text, integer);

CREATE OR REPLACE FUNCTION fn_zapprating_create(in_credentials_email text, in_credentials_password text, in_projectId text, in_memberId text, in_rating integer)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ZappRatings';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_zappRatingId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO ZappRatings(
            projectId,
            memberId,
            rating
        )
        VALUES(
            in_projectId,
            in_memberId,
            in_rating
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_zappRatingId := currval('seq_ZappRatings_zappRatingId');
        END IF;

        return new_zappRatingId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_zapprating_create(text, text, integer, text, integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zapprating_update(text, text, integer, integer, text, integer, text)

-- DROP FUNCTION fn_zapprating_update(text, text, integer, integer, text, integer, text);

CREATE OR REPLACE FUNCTION fn_zapprating_update(in_credentials_email text, in_credentials_password text, in_zappRatingId integer, in_projectId text, in_memberId text, in_rating integer, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ZappRatings';
        var_functionName text := 'UPDATE';

        rec ZappRatings%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from ZappRatings where zappRatingId = in_zappRatingId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_projectId' THEN
                rec.projectId := in_projectId;
            ELSEIF param_name = 'in_memberId' THEN
                rec.memberId := in_memberId;
            ELSEIF param_name = 'in_rating' THEN
                rec.rating := in_rating;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE ZappRatings SET
            projectId = rec.projectId,
            memberId = rec.memberId,
            rating = rec.rating,
            updatedDateTime = now()
        WHERE zappRatingId = in_zappRatingId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_zapprating_update(text, text, integer, integer, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zapprating_retire(text, text, integer, text)

-- DROP FUNCTION fn_zapprating_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_zapprating_retire(in_credentials_email text, in_credentials_password text, in_zappRatingId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ZappRatings';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE ZappRatings SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE zappRatingId = in_zappRatingId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_zapprating_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zapprating_filter(text, text, integer, integer, text, integer, text, integer, text)

-- DROP FUNCTION fn_zapprating_filter(text, text, integer, integer, text, integer, text, integer, text);

CREATE OR REPLACE FUNCTION fn_zapprating_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_projectId text, in_memberId text, in_rating integer, in_filter_list text)
  RETURNS SETOF ZappRatings AS
$BODY$
    DECLARE
--        var_credentialsId text;
        var_tableName text := 'ZappRatings';
        var_functionName text := 'FILTER';

        rec ZappRatings%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
--        var_query text := 'SELECT * FROM ZappRatings WHERE retired = 0';
        var_query text  :=              'SELECT ZappRatings.* from projects ';
    BEGIN
        var_query       := var_query || ' p1 inner join projects p2 on p2.publishedname = p1.publishedname';
        var_query       := var_query || ' inner join ZappRatings on ZappRatings.projectid = p2.projectid';
        var_query       := var_query || ' where ZappRatings.retired = 0';
--        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

--        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_Id' THEN
            ELSEIF filter_name = 'in_projectId' THEN
--                var_query := var_query || ' AND projectId = ''' || in_projectId || '''';
                var_query := var_query || ' AND p1.projectId = ''' || in_projectId || '''';
            ELSEIF filter_name = 'in_memberId' THEN
                var_query := var_query || ' AND ZappRatings.memberId ILIKE ''%' || in_memberId || '%''';
            ELSEIF filter_name = 'in_rating' THEN
                var_query := var_query || ' AND ZappRatings.rating = ' || in_rating;
            ELSEIF filter_name = 'in_createdDateTime' THEN
            ELSEIF filter_name = 'in_updatedDateTime' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM ZappRatings;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

--        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_zapprating_filter(text, text, integer, integer, text, integer, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zapprating_find(text, text, integer)

-- DROP FUNCTION fn_zapprating_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_zapprating_find(in_credentials_email text, in_credentials_password text, in_zappRatingId integer)
  RETURNS SETOF ZappRatings AS
$BODY$
    DECLARE
--        var_credentialsId text;
        var_tableName text := 'ZappRatings';
        var_functionName text := 'FIND';

        rec ZappRatings%ROWTYPE;

    BEGIN
--        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

--        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM ZappRatings
                WHERE zappRatingId = in_zappRatingId
            LOOP
                RETURN NEXT rec;
            END LOOP;
--        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_zapprating_find(text, text, integer) OWNER TO postgres;
