-------------------------------------------------------------------------------
-- r1_to_r2.sql
--
-- Changes to App_User (aka Members, task #622) and Parent (aka Projects,
-- task #625) that are expected to be made in the SpringSource Zephyr project.
--
-- NOTE that we use App_User.Id for username=dhoyt where an admin editedById
-- value is required (or should we just be plugging in null for all of these!?).
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (2);
-------------------------------------------------------------------------------

ALTER TABLE App_User ALTER COLUMN version SET DEFAULT 0; -- optimistic locking starts at 0 and increments with each update
ALTER TABLE App_User DROP COLUMN company;
ALTER TABLE App_User ALTER COLUMN date_created SET DEFAULT now();
--ALTER TABLE App_User RENAME COLUMN date_created TO editedDateTime;
--ALTER TABLE App_User ALTER COLUMN editedDateTime SET DEFAULT now();
ALTER TABLE App_User ADD COLUMN editedById varchar(255);
ALTER TABLE App_User ADD CONSTRAINT fk_App_User_editedById FOREIGN KEY (editedById) REFERENCES App_User(Id);
ALTER TABLE App_User RENAME COLUMN deleted TO retired;
ALTER TABLE App_User ALTER COLUMN retired SET DEFAULT '0'; -- false
ALTER TABLE App_User ADD COLUMN retiredById varchar(255);
ALTER TABLE App_User ADD CONSTRAINT fk_App_User_retiredById FOREIGN KEY (retiredById) REFERENCES App_User(Id);
ALTER TABLE App_User ADD COLUMN retiredDateTime timestamp;
ALTER TABLE App_User RENAME COLUMN email_show TO emailShow;
ALTER TABLE App_User ALTER COLUMN emailShow SET DEFAULT false; 
ALTER TABLE App_User ALTER COLUMN enabled SET DEFAULT true;
ALTER TABLE App_User ALTER COLUMN last_updated SET DEFAULT now();
ALTER TABLE App_User RENAME COLUMN passwd TO passwordHash;
ALTER TABLE App_User ADD COLUMN passwordSalt varchar(128);
ALTER TABLE App_User DROP COLUMN permissions;
ALTER TABLE App_User ADD COLUMN firstName varchar(128);
ALTER TABLE App_User ADD COLUMN lastName varchar(128);
UPDATE App_User SET firstName = substring(user_real_name from '^(\\S*)\\s'), lastName = substring(user_real_name from '\\s(\\S*)$');
ALTER TABLE App_User DROP COLUMN user_real_name;
ALTER TABLE App_User RENAME COLUMN username TO displayName;

ALTER TABLE Parent DROP COLUMN class;
ALTER TABLE Parent RENAME COLUMN gadget_ref_id TO gadgetRefId; -- rename should preserve existing FK to Parent.Id
ALTER TABLE Parent RENAME COLUMN initial_value TO initialValue;
--ALTER TABLE Parent ADD COLUMN categoryId integer;  -- replaced by Published.categoryId
--ALTER TABLE Parent ADD CONSTRAINT fk_Parent_categoryId FOREIGN KEY (categoryId) REFERENCES Categories(categoryId);
ALTER TABLE Parent RENAME COLUMN user_id TO ownerId; -- rename should preserve existing FK to App_User(Id)
ALTER TABLE Parent RENAME COLUMN initial_values TO initialValues;
ALTER TABLE Parent RENAME COLUMN next_version_id TO nextVersionId; -- rename should preserve existing FK to Parent.Id
ALTER TABLE Parent DROP COLUMN permissions;  -- wait until we have something to replace it with
ALTER TABLE Parent ALTER COLUMN price TYPE integer USING CAST(price As Integer);
ALTER TABLE Parent DROP COLUMN sharable;
ALTER TABLE Parent ADD COLUMN currencyTypeId integer;
ALTER TABLE Parent ADD CONSTRAINT fk_Parent_currencyTypeId FOREIGN KEY (currencyTypeId) REFERENCES CurrencyTypes(currencyTypeId);
ALTER TABLE Parent ADD COLUMN projectTypeId integer;
ALTER TABLE Parent ADD CONSTRAINT fk_Parent_projectTypeId FOREIGN KEY (projectTypeId) REFERENCES ProjectTypes(projectTypeId);

update parent set projectTypeId = 2 where type = 'project';
update parent set projectTypeId = 1 where type != 'project';
ALTER TABLE Parent DROP COLUMN type;

-- set up admin accounts
INSERT INTO Accounts (accountId, typeId, name, description, editedById) VALUES (1, 5, 'admin account', 'admin account', (SELECT Id FROM App_User WHERE displayName ILIKE 'dhoyt'));
INSERT INTO AccountMemberRoles SELECT 1, app_user.id, 1, 0, null, null FROM app_user WHERE is_admin = true; -- privs: ROLE_TYPE_ACCOUNT_ADMINISTRATOR
INSERT INTO AccountMemberRoles SELECT 1, app_user.id, 2, 0, null, null FROM app_user WHERE is_admin = true; -- privs: ROLE_TYPE_AUTHOR
INSERT INTO AccountMemberRoles SELECT 1, app_user.id, 3, 0, null, null FROM app_user WHERE is_admin = true; -- privs: ROLE_TYPE_VIEWER
INSERT INTO AccountMemberRoles SELECT 1, app_user.id, 4, 0, null, null FROM app_user WHERE is_admin = true; -- privs: ROLE_TYPE_SALES_CONTACT
INSERT INTO AccountMemberRoles SELECT 1, app_user.id, 5, 0, null, null FROM app_user WHERE is_admin = true; -- privs: ROLE_TYPE_SUPPORT_CONTACT
INSERT INTO AccountMemberRoles SELECT 1, app_user.id, 6, 0, null, null FROM app_user WHERE is_admin = true; -- privs: ROLE_TYPE_FEEDBACK

INSERT INTO SystemAdministrators SELECT app_user.id, encode(digest('a8irAX9g77'||'saltsalt','sha1'),'hex'), 'saltsalt' FROM app_user WHERE is_admin = true;


-- fk into Accounts from AppUser
ALTER TABLE App_User ADD COLUMN accountId int8; --NOT NULL; -- fk to Accounts.accountId
update app_user set accountId = 1;
ALTER TABLE App_User ADD CONSTRAINT fk_AppUser_accountId FOREIGN KEY (accountId) references Accounts(accountId);


-- set up non-admin accounts
INSERT INTO Accounts (accountId, typeId, name, description, editedById) VALUES (2, 1, 'beta account', 'beta account', (SELECT Id FROM App_User WHERE displayName ILIKE 'dhoyt'));
INSERT INTO AccountMemberRoles SELECT 2, app_user.id, 2, 0, null, null FROM app_user WHERE is_admin = false; -- privs: AUTHOR
INSERT INTO AccountMemberRoles SELECT 2, app_user.id, 3, 0, null, null FROM app_user WHERE is_admin = false; -- privs: VIEWER

ALTER TABLE App_User DROP COLUMN is_admin;

-- run query to populate categories table
INSERT INTO Categories (name, typeId, editedById) VALUES ('Navigation', 1, (SELECT Id FROM App_User WHERE displayName ILIKE 'dhoyt'));
INSERT INTO Categories (name, typeId, editedById) VALUES ('Buttons', 1, (SELECT Id FROM App_User WHERE displayName ILIKE 'dhoyt'));
INSERT INTO Categories (name, typeId, editedById) VALUES ('Questions', 1, (SELECT Id FROM App_User WHERE displayName ILIKE 'dhoyt'));
INSERT INTO Categories (name, typeId, editedById) VALUES ('Calculations', 1, (SELECT Id FROM App_User WHERE displayName ILIKE 'dhoyt'));
INSERT INTO Categories (name, typeId, editedById) VALUES ('Simulations', 1, (SELECT Id FROM App_User WHERE displayName ILIKE 'dhoyt'));
INSERT INTO Categories (name, typeId, editedById) VALUES ('Interactions', 1, (SELECT Id FROM App_User WHERE displayName ILIKE 'dhoyt'));


-- not necessary since this relation has been replaced by Published.categoryId, and there are no published gadgets
--UPDATE Parent p SET categoryId = (SELECT categoryId FROM Categories WHERE Categories.name = p.category);
ALTER TABLE Parent DROP COLUMN category;

-- description
-- no need to copy data, as there's no corresponding published rows
ALTER TABLE Parent DROP COLUMN description;

-- tags (no-one's using tags right now, so don't bother saving them)
ALTER TABLE Gadget_Tag add column publishedId integer;
ALTER TABLE Gadget_Tag ADD CONSTRAINT fk_Gadget_Tags_publishedId FOREIGN KEY (publishedId) references Published(publishId);
ALTER TABLE Gadget_Tag DROP CONSTRAINT fk6fafd27b74c7a3d;
ALTER TABLE Gadget_Tag DROP COLUMN gadget_id;

\i autohistory.sql

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
