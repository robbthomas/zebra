-------------------------------------------------------------------------------
-- r23_to_r24.sql
--
-- Feedback on comments (e.g. "Was this comment helpful: yes|no|flag")
-- Add columns to ZappComments table to cache the aggregrate response of "CommentsFeedbacks" table.
-- Need to add a routine to regenerate these caches from scratch.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (24);
-------------------------------------------------------------------------------

CREATE TABLE FeedbackTypes (
    feedbackTypeId int NOT NULL PRIMARY KEY,
    tag text,
    name text,
    displayOrder int
);
INSERT INTO FeedbackTypes(feedbackTypeId,tag,name,displayOrder) VALUES (1,'helpful','Helpful',10);
INSERT INTO FeedbackTypes(feedbackTypeId,tag,name,displayOrder) VALUES (2,'notHelpful','Not Helpful',20);
INSERT INTO FeedbackTypes(feedbackTypeId,tag,name,displayOrder) VALUES (3,'flag','Flag',30);

CREATE UNIQUE INDEX ix_FeedbackTypes_tag ON FeedbackTypes(tag);

CREATE SEQUENCE seq_commentFeedbacks_commentFeedbackId START WITH 2000;

CREATE TABLE CommentFeedbacks (
    commentFeedbackId int NOT NULL PRIMARY KEY DEFAULT nextval('seq_commentFeedbacks_commentFeedbackId'),
    commentId int,
    memberId text,
    feedback text,
    updatedDateTime timestamp DEFAULT now(),
    createdDateTime timestamp DEFAULT now(),

    retired int default(0),  -- 0=not retired
    retiredById text,  -- FK to App_User.Id (aka Members.memberId)
    retiredDateTime timestamp,

    CONSTRAINT fk_commentFeedback_commentId FOREIGN KEY (commentId) REFERENCES ZappComments(zappCommentId),
    CONSTRAINT fk_commentFeedback_memberId FOREIGN KEY (memberId) REFERENCES App_User(Id),
    CONSTRAINT fk_commentFeedback_feedback FOREIGN KEY (feedback) REFERENCES FeedbackTypes(tag),
    CONSTRAINT fk_commentFeedback_retiredById FOREIGN KEY (retiredById) REFERENCES App_User(Id)
);


DROP FUNCTION fn_zappcomment_create(in_credentials_email text, in_credentials_password text, in_publishId integer, in_memberId text, in_comment text);
DROP FUNCTION fn_zappcomment_update(in_credentials_email text, in_credentials_password text, in_zappCommentId integer, in_publishId integer, in_memberId text, in_comment text, in_param_list text);
DROP FUNCTION fn_zappcomment_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_publishId integer, in_memberId text, in_comment text, in_filter_list text);

ALTER TABLE ZappComments ADD COLUMN helpfulCount int DEFAULT 0;
ALTER TABLE ZappComments ADD COLUMN feedbackCount int DEFAULT 0;

\i fn_zappcomment.sql
\i fn_commentfeedback.sql;
\i fn_updatecommentfeedback.sql;


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

