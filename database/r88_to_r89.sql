-------------------------------------------------------------------------------
-- Add CreditCards column and table constraints.
--
-- Retire CreditCard entries missing required minimum information to be of use.
--
-- Populate CreditCards.firstName, lastName, email where it is missing
-- using account administrator information.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES(89);

UPDATE CreditCards SET firstName = '' WHERE firstName is null;
UPDATE CreditCards SET lastName = '' WHERE lastName is null;
UPDATE CreditCards SET email = '' WHERE email is null;

UPDATE CreditCards SET addressId = 3000; -- MIN(Addresses.addressId)

ALTER TABLE CreditCards ALTER COLUMN firstName SET NOT NULL;
ALTER TABLE CreditCards ALTER COLUMN firstName SET DEFAULT '';

ALTER TABLE CreditCards ALTER COLUMN lastName SET NOT NULL;
ALTER TABLE CreditCards ALTER COLUMN lastName SET DEFAULT '';

ALTER TABLE CreditCards ALTER COLUMN email SET NOT NULL;
ALTER TABLE CreditCards ALTER COLUMN email SET DEFAULT '';

ALTER TABLE CreditCards ADD CONSTRAINT fk_CreditCards_addressId
  FOREIGN KEY (addressId) REFERENCES Addresses(addressId);


UPDATE CreditCards SET retired = 1
WHERE retired = 0 AND (
  nameOnCreditCard is null OR LENGTH(TRIM(BOTH FROM nameOnCreditCard)) = 0 OR
  maskedNumber is null OR LENGTH(TRIM(BOTH FROM maskedNumber)) = 0 OR
  expirationYear is null OR LENGTH(TRIM(BOTH FROM expirationYear)) = 0 OR
  expirationMonth is null OR LENGTH(TRIM(BOTH FROM expirationMonth)) = 0 OR
  creditCardType is null);


UPDATE CreditCards
SET firstName = COALESCE(au.firstName,''), lastName = COALESCE(au.lastName,''), email = COALESCE(au.email,'')
FROM AccountMemberRoles amr, App_User au
WHERE CreditCards.retired = 0
AND (LENGTH(CreditCards.firstName) = 0 OR LENGTH(CreditCards.lastName) = 0 OR LENGTH(CreditCards.email) = 0)
AND CreditCards.accountId = amr.accountId
AND amr.accountId > 2000
AND amr.retired = 0
AND amr.typeId = 1 -- Account Administrator
AND amr.memberId = au.id;


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
