--------------------------------------------------------------------------------
-- fn_accounts.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- Accounts functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------
/*
delete from TableFunctionRoleMap WHERE tableName='Accounts' AND
    (functionName='SEARCHBYACCOUNT' OR functionName='SEARCHBYACCOUNT' OR functionName='SEARCHBYACCOUNT');
*/

--------------------------------------------------------------------------------
/*
DROP FUNCTION IF EXISTS fn_admin_search_by_account(
    text, text, -- credentials
    integer, integer, text, -- paging and sort order
    integer, text, timestamp, integer, integer, -- status
    text, text, integer, integer, text, text, integer, text, -- settings
    text) CASCADE; -- filter list
*/

DROP TYPE IF EXISTS _type_admin_search_by_account_ CASCADE;

CREATE TYPE _type_admin_search_by_account_ AS (
    accountId integer,      -- Accounts table
    companyId integer,
    typeId integer,
    termDueDateTime timestamp,
    goodStanding integer,
    name text,
    description text,
    tokenbalance integer,
    editedById text,
    editedDateTime timestamp,
    retired integer,
    retiredById text,
    retiredDateTime timestamp,
    showCompanyInfoForAll integer,
    companySameAsPersonal integer,
    billingAddressSameAsCompany integer,
    billingContactSameAsCompany integer,
    paypallogin text,
    convertAmount integer,
    merchantCustomerId text,
    convertMethod text,
    contactPreference text,
    companyName text,           -- Companies
    companyUrl text,
    accountTypeTag text,        -- AccountTypes
    accountTypeName text,
    accountTypeTerms text,
    accountLastBilled timestamp,
    accountMembersCount integer -- AccountMemberRoles.memberId aggregate
);


CREATE OR REPLACE FUNCTION fn_admin_search_by_account(
    in_credentials_email text, in_credentials_password text, -- credentials
    in_count integer, in_offset integer, in_order_list text, -- paging
    in_companyId integer, in_type text, in_termDueDateTime timestamp, in_goodStanding integer, in_retired integer,
    in_name text, in_description text, in_tokenBalance integer, in_showCompanyInfoForAll integer, in_contactPreference text, in_convertMethod text, in_convertAmount integer, in_paypalLogin text,
    in_filter_list text)
  RETURNS SETOF _type_admin_search_by_account_ AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Accounts';
        var_functionName text := 'SEARCHBYACCOUNT';

        rec _type_admin_search_by_account_%ROWTYPE;
        filter_name text;
        var_typeId int;
        var_count int := 1;
        var_order text;
        var_query text;
    BEGIN
        var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN

            var_query :=
                'SELECT a.*, at.tag AS accountTypeTag, at.name AS accountTypeName, at.terms AS accountTypeTerms, at.termDueDateTime AS accountLastBilled,' ||
                ' c.name as companyName, c.url AS companyUrl, 0 AS accountMembersCount' ||
                ' FROM Accounts a, AccountTypes at' ||
                '  LEFT JOIN Companies c ON (a.companyId = c.companyId)' ||
                ' WHERE a.typeId = at.accountTypeId';

            LOOP
                filter_name := split_part(in_filter_list, ',', var_count);
                filter_name := trim(both ' ' from filter_name);
                EXIT WHEN filter_name = '';

                IF filter_name = 'in_accountId' THEN
                ELSEIF filter_name = 'in_companyId' THEN
                    var_query := var_query || ' AND a.companyId = ' || in_companyId;
                ELSEIF filter_name = 'in_type' THEN
                    var_typeId := accountTypeId FROM AccountTypes WHERE tag ILIKE in_type;
                    var_query := var_query || ' AND a.typeId = ' || var_typeId;
                ELSEIF filter_name = 'in_termDueDateTime' THEN
                    var_query := var_query || ' AND a.termDueDateTime = ' || in_termDueDateTime;
                ELSEIF filter_name = 'in_retired' THEN
                    var_query := var_query || ' AND a.retired = ' || in_retired;
                ELSEIF filter_name = 'in_goodStanding' THEN
                    var_query := var_query || ' AND a.goodStanding = ' || in_goodStanding;
                ELSEIF filter_name = 'in_name' THEN
                    var_query := var_query || ' AND a.name ILIKE ''%' || in_name || '%''';
                ELSEIF filter_name = 'in_description' THEN
                    var_query := var_query || ' AND a.description ILIKE ''%' || in_description || '%''';
                ELSEIF filter_name = 'in_tokenBalance' THEN
                    var_query := var_query || ' AND a.tokenBalance = ' || in_tokenBalance;
                ELSEIF filter_name = 'in_showCompanyInfoForAll' THEN
                    var_query := var_query || ' AND a.showCompanyInfoForAll = ' || in_showCompanyInfoForAll;
                ELSEIF filter_name = 'in_contactPreference' THEN
                    var_query := var_query || ' AND a.contactPreference ILIKE ''%' || in_contactPreference || '%''';
                ELSEIF filter_name = 'in_convertMethod' THEN
                    var_query := var_query || ' AND a.convertMethod ILIKE ''%' || in_convertMethod || '%''';
                ELSEIF filter_name = 'in_convertAmount' THEN
                    var_query := var_query || ' AND a.convertAmount = ' || in_convertAmount;
                ELSEIF filter_name = 'in_paypalLogin' THEN
                    var_query := var_query || ' AND a.paypalLogin ILIKE ''%' || in_paypalLogin || '%''';
                ELSEIF filter_name = 'in_editedById' THEN
                ELSEIF filter_name = 'in_editedDateTime' THEN
                END IF;
                var_count := var_count + 1;
            END LOOP;

            var_query := var_query || ' GROUP BY amr.memberId';

            IF LENGTH(in_order_list) > 0 THEN
                var_order := replace(in_order_list,':a',' ASC');
                var_order := replace(var_order,':d',' DESC');
                var_query := var_query || ' ORDER BY ' || var_order;
            END IF;

            var_count := in_count;
            IF var_count <= 0 THEN
                SELECT count(*) INTO var_count FROM Accounts;
            END IF;
            var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

            BEGIN
                CREATE TEMPORARY TABLE tmp_tbl_admin_search_by_account (
                    LIKE _type_admin_search_by_account_
                );
            EXCEPTION
                WHEN DUPLICATE_TABLE THEN
                    TRUNCATE TABLE tmp_tbl_admin_search_by_account;
            END;

            var_query := 'INSERT INTO tmp_tbl_admin_search_by_account ' || var_query;
            EXECUTE var_query;

            -- Add in account member counts - note that any particular member may have
            -- multiple roles for a given account but should be counted only once per
            -- account member belongs to.
            BEGIN
                CREATE TEMPORARY TABLE tmp_tbl_account_members (
                    accountId integer,
                    memberId integer
                );
            EXCEPTION
                WHEN DUPLICATE_TABLE THEN
                    TRUNCATE TABLE tmp_tbl_account_members;
            END;

            INSERT INTO tmp_tbl_account_members
            SELECT DISTINCT accountId, memberId
            FROM AccountMemberRoles
            WHERE accountId IN (SELECT accountId FROM tmp_tbl_admin_search_by_account);

            BEGIN
                CREATE TEMPORARY TABLE tmp_tbl_account_member_counts (
                    accountId integer,
                    memberCount integer
                );
            EXCEPTION
                WHEN DUPLICATE_TABLE THEN
                    TRUNCATE TABLE tmp_tbl_account_member_counts;
            END;

            INSERT INTO tmp_tbl_account_member_counts
            SELECT accountId, COUNT(*) AS memberCounts
            FROM tmp_tbl_account_members
            GROUP BY memberId;

            UPDATE tmp_tbl_admin_search_by_account
            SET accountMembersCount = memberCount
            FROM tmp_tbl_account_member_counts
            WHERE tmp_tbl_account_member_counts.accountId = tmp_tbl_admin_search_by_account.accountId;

            FOR rec IN
                SELECT * FROM tmp_tbl_admin_search_by_account
            LOOP
                RETURN NEXT rec;
            END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
