-------------------------------------------------------------------------------
-- Add indexes as needed for columns added r59 to r75.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (76);

-- r60_to_r61
create index ix_AccountProjectNumbers_projectId on AccountProjectNumbers (projectId);
create index ix_Gadget_Tag_projectId on Gadget_Tag (projectId);
create index ix_InvoiceLineItems_projectId on InvoiceLineItems (projectId);
create index ix_PaymentLineItems_projectId on PaymentLineItems (projectId);
create index ix_PublishedPurchasesAcquisitions_projectId on PublishedPurchasesAcquisitions (projectId);
create index ix_PublishImages_projectId on PublishImages (projectId);
create index ix_PublishLaunchCounts_projectId on PublishLaunchCounts (projectId);
create index ix_SharedProjects_projectId on SharedProjects (projectId);
create index ix_ZappRatingCaches_projectId on ZappRatingCaches (projectId);

-- r62_to_r63
update ProductCodes set tag = 'SHOPP_UNPUBLISH' where tag = 'SHOP_UNPUBLISH';
create index ix_ProductCodePrices_productCodeId on ProductCodePrices (productCodeId);
create index ix_InvoiceLineItems_productCodeId on InvoiceLineItems (productCodeId);

-- r67_to_r68
create index ix_ZappComments_projectId on ZappRatingCaches (projectId);
create index ix_ZappRatings_projectId on ZappRatingCaches (projectId);

-- r69_to_r70
create index ix_Projects_nextPublishedVersionId on Projects (nextPublishedVersionId);
create index ix_Projects_accountTypeId on Projects (nextPublishedVersionId);


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
