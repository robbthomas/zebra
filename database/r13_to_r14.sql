-------------------------------------------------------------------------------
-- r13_to_r14.sql
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (14);
-------------------------------------------------------------------------------

-- Per David Hoyt, 5/24, App_User.accountId no longer being used by editor.
ALTER TABLE App_User DROP COLUMN accountId;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
