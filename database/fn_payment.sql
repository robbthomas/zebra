--------------------------------------------------------------------------------
-- fn_payments.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- Payments functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Payments' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('Payments','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Payments','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Payments','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Payments','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Payments','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('Payments','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Payments','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_payment_create(text, text, integer, integer, integer, text, timestamp, integer)

-- DROP FUNCTION fn_payment_create(text, text, integer, integer, integer, text, timestamp, integer);

CREATE OR REPLACE FUNCTION fn_payment_create(in_credentials_email text, in_credentials_password text, in_accountId integer, in_cachedTotal integer, in_currencyTypeId integer, in_state text, in_holdingDateTime timestamp, in_transactionId integer)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Payments';
        var_functionName text := 'CREATE';

        new_paymentId int := -1;
        var_count int := -1;
        var_stateId int;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_stateId := paymentStateId FROM PaymentStates WHERE tag ILIKE in_state;

        insert into Payments(
            accountId,
            cachedTotal,
            currencyTypeId,
            stateId,
            holdingDateTime,
            transactionId
        )
        values(
            in_accountId,
            in_cachedTotal,
            in_currencyTypeId,
            var_stateId,
            in_holdingDateTime,
            in_transactionId
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_paymentId := currval('seq_Payments_paymentId');
        END IF;

        return new_paymentId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_payment_create(text, text, integer, integer, integer, text, timestamp, integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_payment_update(text, text, integer, integer, integer, integer, text, timestamp, integer, text)

-- DROP FUNCTION fn_payment_update(text, text, integer, integer, integer, integer, text, timestamp, integer, text);

CREATE OR REPLACE FUNCTION fn_payment_update(in_credentials_email text, in_credentials_password text, in_paymentId integer, in_accountId integer, in_cachedTotal integer, in_currencyTypeId integer, in_state text, in_holdingDateTime timestamp, in_transactionId integer, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Payments';
        var_functionName text := 'UPDATE';

        rec Payments%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from Payments where paymentId = in_paymentId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_accountId' THEN
                rec.accountId := in_accountId;
            ELSEIF param_name = 'in_cachedTotal' THEN
                rec.cachedTotal := in_cachedTotal;
            ELSEIF param_name = 'in_currencyTypeId' THEN
                rec.currencyTypeId := in_currencyTypeId;
            ELSEIF param_name = 'in_state' THEN
                rec.stateId := paymentStateId FROM PaymentStates WHERE tag ILIKE in_state;
            ELSEIF param_name = 'in_holdingDateTime' THEN
                rec.holdingDateTime := in_holdingDateTime;
            ELSEIF param_name = 'in_transactionId' THEN
                rec.transactionId := in_transactionId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE Payments SET
            accountId = rec.accountId,
            cachedTotal = rec.cachedTotal,
            currencyTypeId = rec.currencyTypeId,
            stateId = rec.stateId,
            holdingDateTime = rec.holdingDateTime,
            transactionId = rec.transactionId
        WHERE paymentId = in_paymentId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_payment_update(text, text, integer, integer, integer, integer, text, timestamp, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_payment_filter(text, text, integer, integer, text, integer, integer, integer, text, timestamp, integer, text)

-- DROP FUNCTION fn_payment_filter(text, text, integer, integer, text, integer, integer, integer, text, timestamp, integer, text);

CREATE OR REPLACE FUNCTION fn_payment_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_accountId integer, in_cachedTotal integer, in_currencyTypeId integer, in_state text, in_holdingDateTime timestamp, in_transactionId integer, in_filter_list text)
  RETURNS SETOF Payments AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Payments';
        var_functionName text := 'FILTER';

        rec Payments%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_stateId int;
        var_order text;
        var_query text := 'SELECT * FROM Payments WHERE 0 = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_paymentId' THEN
            ELSEIF filter_name = 'in_accountId' THEN
                var_query := var_query || ' AND accountId = ' || in_accountId;
            ELSEIF filter_name = 'in_cachedTotal' THEN
                var_query := var_query || ' AND cachedTotal = ' || in_cachedTotal;
            ELSEIF filter_name = 'in_currencyTypeId' THEN
                var_query := var_query || ' AND currencyTypeId = ' || in_currencyTypeId;
            ELSEIF filter_name = 'in_state' THEN
                var_stateId := paymentStateId FROM PaymentStates WHERE tag ILIKE in_state;
                var_query := var_query || ' AND stateId = ' || var_stateId;
            ELSEIF filter_name = 'in_holdingDateTime' THEN
                var_query := var_query || ' AND holdingDateTime = ' || in_holdingDateTime;
            ELSEIF filter_name = 'in_transactionId' THEN
                var_query := var_query || ' AND transactionId = ' || in_transactionId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM Payments;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_payment_filter(text, text, integer, integer, text, integer, integer, integer, text, timestamp, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_payment_find(text, text, integer)

-- DROP FUNCTION fn_payment_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_payment_find(in_credentials_email text, in_credentials_password text, in_paymentId integer)
  RETURNS SETOF Payments AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Payments';
        var_functionName text := 'FIND';

        rec Payments%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM Payments
                WHERE paymentId = in_paymentId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_payment_find(text, text, integer) OWNER TO postgres;
