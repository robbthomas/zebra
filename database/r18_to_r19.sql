-------------------------------------------------------------------------------
-- r18_to_r19.sql
--
-- This update is "no-op", to test the automated db update job from Hudson.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (19);
-------------------------------------------------------------------------------

-- This update is "no-op", to test the automated db update job from Hudson.

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

