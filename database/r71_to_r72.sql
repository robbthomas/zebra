-------------------------------------------------------------------------------
-- remove obsolete foreign keys to parent
--
-- replace them with keys to projects where necessary
-- 
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (72);

alter table projects alter column version drop not null;
alter table gadget_tag alter column version drop not null;
alter table app_user alter column version drop not null;
alter table gadget_state alter column version drop not null;
alter table asset alter column version drop not null;
alter table app_role alter column version drop not null;


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;