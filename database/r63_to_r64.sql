-------------------------------------------------------------------------------
-- create productcodes table 
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (64);

select 'Will you still need me';
select 'Will you still feed me when I am', max(versionNumber) from databaseversion;

alter table projects add column width int not null default -1;
alter table projects add column height int not null default -1;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;