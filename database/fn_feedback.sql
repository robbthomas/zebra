--------------------------------------------------------------------------------
-- fn_feedbacks.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- Feedback functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Feedback' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('Feedback','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Feedback','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Feedback','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Feedback','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('Feedback','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Feedback','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_feedback_create(text, text, text, integer, text, text, text)

-- DROP FUNCTION fn_feedback_create(text, text, text, integer, text, text, text);

CREATE OR REPLACE FUNCTION fn_feedback_create(in_credentials_email text, in_credentials_password text, in_reMemberId text, in_reAccountId integer, in_reProjectId text, in_comments text, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Feedback';
        var_functionName text := 'CREATE';

        new_feedbackId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into Feedback(
            reMemberId,
            reAccountId,
            reProjectId,
            comments,
            editedById
        )
        values(
            in_reMemberId,
            in_reAccountId,
            in_reProjectId,
            in_comments,
            in_editedById
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_feedbackId := currval('seq_Feedback_feedbackId');
        END IF;

        return new_feedbackId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_feedback_create(text, text, text, integer, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_feedback_update(text, text, integer, text, integer, text, text, text, text)

-- DROP FUNCTION fn_feedback_update(text, text, integer, text, integer, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_feedback_update(in_credentials_email text, in_credentials_password text, in_feedbackId integer, in_reMemberId text, in_reAccountId integer, in_reProjectId text, in_comments text, in_editedById text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Feedback';
        var_functionName text := 'UPDATE';

        rec Feedback%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from Feedback where feedbackId = in_feedbackId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_reMemberId' THEN
                rec.reMemberId := in_reMemberId;
            ELSEIF param_name = 'in_reAccountId' THEN
                rec.reAccountId := in_reAccountId;
            ELSEIF param_name = 'in_reProjectId' THEN
                rec.reProjectId := in_reProjectId;
            ELSEIF param_name = 'in_comments' THEN
                rec.comments := in_comments;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE Feedback SET
            reMemberId = rec.reMemberId,
            reAccountId = rec.reAccountId,
            reProjectId = rec.reProjectId,
            comments = rec.comments,
            editedById = rec.editedById
        WHERE feedbackId = in_feedbackId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_feedback_update(text, text, integer, text, integer, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_feedback_retire(text, text, integer, text)

-- DROP FUNCTION fn_feedback_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_feedback_retire(in_credentials_email text, in_credentials_password text, in_feedbackId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Feedback';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE Feedback SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE feedbackId = in_feedbackId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_feedback_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_feedback_filter(text, text, integer, integer, text, text, integer, text, text, text)

-- DROP FUNCTION fn_feedback_filter(text, text, integer, integer, text, text, integer, text, text, text);

CREATE OR REPLACE FUNCTION fn_feedback_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_reMemberId text, in_reAccountId integer, in_reProjectId text, in_comments text, in_filter_list text)
  RETURNS SETOF Feedback AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Feedback';
        var_functionName text := 'FILTER';

        rec Feedback%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM Feedback WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_feedbackId' THEN
            ELSEIF filter_name = 'in_reMemberId' THEN
                var_query := var_query || ' AND reMemberId ILIKE ''%' || in_reMemberId || '%''';
            ELSEIF filter_name = 'in_reAccountId' THEN
                var_query := var_query || ' AND reAccountId = ' || in_reAccountId;
            ELSEIF filter_name = 'in_reProjectId' THEN
                var_query := var_query || ' AND reProjectId ILIKE ''%' || in_reProjectId || '%''';
            ELSEIF filter_name = 'in_comments' THEN
                var_query := var_query || ' AND comments ILIKE ''%' || in_comments || '%''';
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM Feedback;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_feedback_filter(text, text, integer, integer, text, text, integer, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_feedback_find(text, text, integer)

-- DROP FUNCTION fn_feedback_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_feedback_find(in_credentials_email text, in_credentials_password text, in_feedbackId integer)
  RETURNS SETOF Feedback AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Feedback';
        var_functionName text := 'FIND';

        rec Feedback%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM Feedback
                WHERE feedbackId = in_feedbackId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_feedback_find(text, text, integer) OWNER TO postgres;
