--------------------------------------------------------------------------------
-- fn_publish.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- Published functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Published'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('Published','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Published','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Published','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Published','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Published','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Published','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Published','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Published','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Published','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Published','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Published','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Published','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Published','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('Published','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Published','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Published','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Published','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_publish_create(text, text, integer, text, integer, integer, integer, integer, integer, integer, text, text, text, text, text, text, integer, integer, text)

-- DROP FUNCTION fn_publish_create(text, text, integer, text, integer, integer, integer, integer, integer, integer, text, text, text, text, text, text, integer, integer, text);

CREATE OR REPLACE FUNCTION fn_publish_create(in_credentials_email text, in_credentials_password text, in_accountId integer, in_projectId text, in_firstPublishId integer, in_versionMajor integer, in_versionMinor integer, in_versionDot integer, in_versionBuildNumber integer, in_categoryId integer, in_name text, in_description text, in_thumbnailId text, in_status text, in_publishType text, in_licenseType text, in_price integer, in_currencyTypeId integer, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Published';
        var_functionName text := 'CREATE';

        new_publishId int := -1;

        var_firstPublishId int;
        var_editedById text;

        var_count int := -1;
        var_statusId int;
        var_publishTypeId int;
        var_licenseTypeId int;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_editedById := COALESCE(in_editedById,'');
        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        IF in_firstPublishId > 0 THEN
            var_firstPublishId := in_firstPublishId;
        END IF;

        var_statusId := publishStatusId FROM PublishStatuses WHERE tag ILIKE in_status;
        var_publishTypeId := publishTypeId FROM PublishTypes WHERE tag ILIKE in_publishType;
        var_licenseTypeId := licenseTypeId FROM LicenseTypes WHERE tag ILIKE in_licenseType;

        insert into Published(
            accountId,
            projectId,
            firstPublishId,
            versionMajor,
            versionMinor,
            versionDot,
            versionBuildNumber,
            categoryId,
            name,
            description,
            thumbnailId,
            statusId,
            publishTypeId,
            licenseTypeId,
            price,
            currencyTypeId,
            editedById
        )
        values(
            in_accountId,
            in_projectId,
            var_firstPublishId,
            in_versionMajor,
            in_versionMinor,
            in_versionDot,
            in_versionBuildNumber,
            in_categoryId,
            in_name,
            in_description,
            in_thumbnailId,
            var_statusId,
            var_publishTypeId,
            var_licenseTypeId,
            in_price,
            in_currencyTypeId,
            var_editedById
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_publishId := currval('seq_Published_publishId');
        END IF;

        return new_publishId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_publish_create(text, text, integer, text, integer, integer, integer, integer, integer, integer, text, text, text, text, text, text, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_publish_update(text, text, integer, integer, text, integer, integer, integer, integer, integer, integer, text, text, text, text, text, text, integer, integer, text, text)

-- DROP FUNCTION fn_publish_update(text, text, integer, integer, text, integer, integer, integer, integer, integer, integer, text, text, text, text, text, text, integer, integer, text, text);

CREATE OR REPLACE FUNCTION fn_publish_update(in_credentials_email text, in_credentials_password text, in_publishId integer, in_accountId integer, in_projectId text, in_firstPublishId integer, in_versionMajor integer, in_versionMinor integer, in_versionDot integer, in_versionBuildNumber integer, in_categoryId integer, in_name text, in_description text, in_thumbnailId text, in_status text, in_publishType text, in_licenseType text, in_price integer, in_currencyTypeId integer, in_editedById text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Published';
        var_functionName text := 'UPDATE';

        rec Published%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from Published where publishId = in_publishId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_accountId' THEN
                rec.accountId := in_accountId;
            ELSEIF param_name = 'in_projectId' THEN
                rec.projectId := in_projectId;
            ELSEIF param_name = 'in_firstPublishId' THEN
                rec.firstPublishId := in_firstPublishId;
            ELSEIF param_name = 'in_versionMajor' THEN
                rec.versionMajor := in_versionMajor;
            ELSEIF param_name = 'in_versionMinor' THEN
                rec.versionMinor := in_versionMinor;
            ELSEIF param_name = 'in_versionDot' THEN
                rec.versionDot := in_versionDot;
            ELSEIF param_name = 'in_versionBuildNumber' THEN
                rec.versionBuildNumber := in_versionBuildNumber;
            ELSEIF param_name = 'in_categoryId' THEN
                rec.categoryId := in_categoryId;
            ELSEIF param_name = 'in_name' THEN
                rec.name := in_name;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_thumbnailId' THEN
                rec.thumbnailId := in_thumbnailId;
            ELSEIF param_name = 'in_status' THEN
                rec.statusId := publishStatusId FROM PublishStatuses WHERE tag ILIKE in_status;
            ELSEIF param_name = 'in_publishType' THEN
                rec.publishTypeId := publishTypeId FROM PublishTypes WHERE tag ILIKE in_publishType;
            ELSEIF param_name = 'in_licenseType' THEN
                rec.licenseTypeId := licenseTypeId FROM LicenseTypes WHERE tag ILIKE in_licenseType;
            ELSEIF param_name = 'in_price' THEN
                rec.price := in_price;
            ELSEIF param_name = 'in_currencyTypeId' THEN
                rec.currencyTypeId := in_currencyTypeId;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE Published SET
            accountId = rec.accountId,
            projectId = rec.projectId,
            firstPublishId = rec.firstPublishId,
            versionMajor = rec.versionMajor,
            versionMinor = rec.versionMinor,
            versionDot = rec.versionDot,
            versionBuildNumber = rec.versionBuildNumber,
            categoryId = rec.categoryId,
            name = rec.name,
            description = rec.description,
            thumbnailId = rec.thumbnailId,
            statusId = rec.statusId,
            publishTypeId = rec.publishTypeId,
            licenseTypeId = rec.licenseTypeId,
            price = rec.price,
            currencyTypeId = rec.currencyTypeId,
            editedById = rec.editedById
        WHERE publishId = in_publishId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_publish_update(text, text, integer, integer, text, integer, integer, integer, integer, integer, integer, text, text, text, text, text, text, integer, integer, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_publish_retire(text, text, integer, text)

-- DROP FUNCTION fn_publish_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_publish_retire(in_credentials_email text, in_credentials_password text, in_publishId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Published';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE Published SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE publishId = in_publishId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_publish_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_publish_filter(text, text, integer, integer, text, integer, text, integer, integer, integer, integer, integer, integer, text, text, text, text, text, text, integer, integer, text)

-- DROP FUNCTION fn_publish_filter(text, text, integer, integer, text, integer, text, integer, integer, integer, integer, integer, integer, text, text, text, text, text, text, integer, integer, text);

CREATE OR REPLACE FUNCTION fn_publish_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_accountId integer, in_projectId text, in_firstPublishId integer, in_versionMajor integer, in_versionMinor integer, in_versionDot integer, in_versionBuildNumber integer, in_categoryId integer, in_name text, in_description text, in_thumbnailId text, in_status text, in_publishType text, in_licenseType text, in_price integer, in_currencyTypeId integer, in_filter_list text)
  RETURNS SETOF Published AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Published';
        var_functionName text := 'FILTER';

        rec Published%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM Published WHERE retired = 0';
        var_statusId int;
        var_publishTypeId int;
        var_licenseTypeId int;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_publishId' THEN
            ELSEIF filter_name = 'in_accountId' THEN
                var_query := var_query || ' AND accountId = ' || in_accountId;
            ELSEIF filter_name = 'in_projectId' THEN
                var_query := var_query || ' AND projectId ILIKE ''%' || in_projectId || '%''';
            ELSEIF filter_name = 'in_firstPublishId' THEN
                var_query := var_query || ' AND firstPublishId = ' || in_firstPublishId;
            ELSEIF filter_name = 'in_versionMajor' THEN
                var_query := var_query || ' AND versionMajor = ' || in_versionMajor;
            ELSEIF filter_name = 'in_versionMinor' THEN
                var_query := var_query || ' AND versionMinor = ' || in_versionMinor;
            ELSEIF filter_name = 'in_versionDot' THEN
                var_query := var_query || ' AND versionDot = ' || in_versionDot;
            ELSEIF filter_name = 'in_versionBuildNumber' THEN
                var_query := var_query || ' AND versionBuildNumber = ' || in_versionBuildNumber;
            ELSEIF filter_name = 'in_categoryId' THEN
                var_query := var_query || ' AND categoryId = ' || in_categoryId;
            ELSEIF filter_name = 'in_name' THEN
                var_query := var_query || ' AND name ILIKE ''%' || in_name || '%''';
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_thumbnailId' THEN
                var_query := var_query || ' AND thumbnailId ILIKE ''%' || in_thumbnailId || '%''';
            ELSEIF filter_name = 'in_status' THEN
                var_statusId := publishStatusId FROM PublishStatuses WHERE tag ILIKE in_status;
                var_query := var_query || ' AND statusId = ' || var_statusId;
            ELSEIF filter_name = 'in_publishType' THEN
                var_publishTypeId := publishTypeId FROM PublishTypes WHERE tag ILIKE in_publishType;
                var_query := var_query || ' AND publishTypeId = ' || var_publishTypeId;
            ELSEIF filter_name = 'in_licenseType' THEN
                var_licenseTypeId := licenseTypeId FROM LicenseTypes WHERE tag ILIKE in_licenseType;
                var_query := var_query || ' AND licenseTypeId = ' || var_licenseTypeId;
            ELSEIF filter_name = 'in_price' THEN
                var_query := var_query || ' AND price = ' || in_price;
            ELSEIF filter_name = 'in_currencyTypeId' THEN
                var_query := var_query || ' AND currencyTypeId = ' || in_currencyTypeId;
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            ELSEIF filter_name = 'in_retired' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM Published;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_publish_filter(text, text, integer, integer, text, integer, text, integer, integer, integer, integer, integer, integer, text, text, text, text, text, text, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_publish_find(text, text, integer)

-- DROP FUNCTION fn_publish_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_publish_find(in_credentials_email text, in_credentials_password text, in_publishId integer)
  RETURNS SETOF Published AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Published';
        var_functionName text := 'FIND';

        rec Published%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM Published
                WHERE publishId = in_publishId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_publish_find(text, text, integer) OWNER TO postgres;
