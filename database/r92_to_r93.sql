-------------------------------------------------------------------------------
-- r92_to_r93.sql
--
-- Add sequence for automatically generating PaymentLineItems.lineNumber values.
-- Note that this sequence can be reset each time a new line item entry is made
-- when making them one at a time OR it can be adjusted after the fact, much as
-- it is below in the existing entries updates, after batch create of new entries.
--
-- Update lineNumber for existing PaymentLineItems entries.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES (93);

-- Create lineNumbers sequence

CREATE SEQUENCE seq_PaymentLineItems_lineNumber START 1;

ALTER TABLE PaymentLineItems 
  ALTER COLUMN lineNumber SET DEFAULT nextval('seq_PaymentLineItems_lineNumber');

-- Update existing entries

CREATE TEMPORARY SEQUENCE seq_tmpLineNumbers START 1;
CREATE TEMPORARY TABLE tmpLineNumbers (
  seq integer default nextval('seq_tmpLineNumbers'),
  paymentId bigint,
  paymentLineItemId bigint,
  minseq integer default 0
);

INSERT INTO tmpLineNumbers(paymentId,paymentLineItemId)
SELECT paymentId, paymentLineItemId
FROM PaymentLineItems
ORDER BY paymentId, paymentLineItemId;

SELECT paymentId, MIN(seq) AS minseq
INTO TEMPORARY TABLE tmpMinSeq
FROM tmpLineNumbers
GROUP BY paymentId;

UPDATE tmpLineNumbers
SET minseq = tmpMinSeq.minseq
FROM tmpMinSeq
WHERE tmpLineNumbers.paymentId = tmpMinSeq.paymentId;

UPDATE PaymentLineItems
SET lineNumber = seq - minseq + 1
FROM tmpLineNumbers
WHERE PaymentLineItems.paymentLineItemId = tmpLineNumbers.paymentLineItemId;

DROP TABLE tmpMinSeq;
DROP TABLE tmpLineNumbers;
DROP SEQUENCE seq_tmpLineNumbers CASCADE;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

