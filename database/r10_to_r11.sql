-------------------------------------------------------------------------------
-- r10_to_r11.sql
--
-- Redo Published Autohistory to include screenShotId column.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (11);
-------------------------------------------------------------------------------

-- reinforce not allowing an rX_to_ry to be applied more than once
CREATE UNIQUE INDEX ix_DatabaseVersion_versionNumber ON  DatabaseVersion(versionNumber);

-- Change Accounts.contactPrference type to varchar(255)
-- column inadvertently added as integer because r5_to_r6.sql applied
-- prematurely before Accounts table changes which add this column were
-- concluded
ALTER TABLE Accounts DROP COLUMN contactPreference;
ALTER TABLE Accounts ADD COLUMN contactPreference varchar(255);

-- Add a unique constraint on ContactPrefernceTypes(tag) to permit creating
-- FKs from tables which store contact preference.
CREATE UNIQUE INDEX ix_ContactPreferenceTypes_tag ON ContactPreferenceTypes(tag);

-- Add FK to newly redone column.
ALTER TABLE Accounts ADD CONSTRAINT fk_Accounts_contactPreference FOREIGN KEY (contactPreference) REFERENCES ContactPreferenceTypes(tag);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
