-------------------------------------------------------------------------------
-- r49_to_r50.sql
--
-- Create tables for tracking sharing of zapps, and email invites
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (50);
-------------------------------------------------------------------------------

alter table Published add column inviteOnly boolean not null default false;
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

