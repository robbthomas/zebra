-------------------------------------------------------------------------------
-- Foreign keys to published that were missed in r60_to_r61.sql
--
-- 
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (68);


alter table zappcomments add column projectid varchar(255);
update zappcomments set projectid = projects.projectid
    from projects
    where projects.dep_publishid = zappcomments.publishid;

alter TABLE "zappcomments" add CONSTRAINT "fk_zappcomments_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);


alter table zappratings add column projectid varchar(255);
update zappratings set projectid = projects.projectid
    from projects
    where projects.dep_publishid = zappratings.publishid;

alter TABLE "zappratings" add CONSTRAINT "fk_zappratings_projectid" FOREIGN KEY (projectid) REFERENCES projects(projectid);



-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;