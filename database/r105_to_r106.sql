-------------------------------------------------------------------------------
-- r105_to_r106.sql
--
-- Author:  David Hoyt
-- Notes:  retire all versions of projects that are already retired
--
-------------------------------------------------------------------------------

update projects
set retired = true
where projectId in
        (select p1.projectId from projects p1
            inner join projects p2
            on p2.retired = true
            and p1.projectName    = p2.projectName
            and p1.authorMemberId = p2.authorMemberId
         )
;
------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (106);
------------------------------------------------------
         