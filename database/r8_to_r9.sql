-------------------------------------------------------------------------------
-- r8_to_r9.sql
--
-- "Move" (repeat) items here which did not make builds they were intended for.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (9);
-------------------------------------------------------------------------------

-- Following change added to r5_to_r6.sql and again to r6_to_r7.sql but in
-- both cases AFTER Staging updated to r6 so we'll repeat it here and comment
-- out in earlier updates (so we don't break an r0->rN update at a later time).
ALTER TABLE Accounts ADD COLUMN convertMethod varchar(255);



-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
