--------------------------------------------------------------------------------
-- fn_invoices.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- Invoices functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Invoices' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('Invoices','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Invoices','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Invoices','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Invoices','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('Invoices','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Invoices','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_invoice_create(text, text, integer, text, integer, integer, text, timestamp, integer)

-- DROP FUNCTION fn_invoice_create(text, text, integer, text, integer, integer, text, timestamp, integer);

CREATE OR REPLACE FUNCTION fn_invoice_create(in_credentials_email text, in_credentials_password text, in_accountId integer, in_description text, in_cachedTotal integer, in_currencyTypeId integer, in_state text, in_holdingDateTime timestamp, in_transactionId integer)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Invoices';
        var_functionName text := 'CREATE';

        new_invoiceId int := -1;
        var_count int := -1;
        var_stateId int;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_stateId := invoiceStateId FROM InvoiceStates WHERE tag ILIKE in_state;

        insert into Invoices(
            accountId,
            description,
            cachedTotal,
            currencyTypeId,
            stateId,
            holdingDateTime --,
            --transactionId
        )
        values(
            in_accountId,
            in_description,
            in_cachedTotal,
            in_currencyTypeId,
            var_stateId,
            in_holdingDateTime --,
            --in_transactionId
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_invoiceId := currval('seq_Invoices_invoiceId');
        END IF;

        return new_invoiceId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_invoice_create(text, text, integer, text, integer, integer, text, timestamp, integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoice_update(text, text, integer, integer, text, integer, integer, text, timestamp, integer, text)

-- DROP FUNCTION fn_invoice_update(text, text, integer, integer, text, integer, integer, text, timestamp, integer, text);

CREATE OR REPLACE FUNCTION fn_invoice_update(in_credentials_email text, in_credentials_password text, in_invoiceId integer, in_accountId integer, in_description text, in_cachedTotal integer, in_currencyTypeId integer, in_state text, in_holdingDateTime timestamp, in_transactionId integer, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Invoices';
        var_functionName text := 'UPDATE';

        rec Invoices%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from Invoices where invoiceId = in_invoiceId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_accountId' THEN
                rec.accountId := in_accountId;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_cachedTotal' THEN
                rec.cachedTotal := in_cachedTotal;
            ELSEIF param_name = 'in_currencyTypeId' THEN
                rec.currencyTypeId := in_currencyTypeId;
            ELSEIF param_name = 'in_state' THEN
                rec.stateId := invoiceStateId FROM InvoiceStates WHERE tag ILIKE in_state;
            ELSEIF param_name = 'in_holdingDateTime' THEN
                rec.holdingDateTime := in_holdingDateTime;
            ELSEIF param_name = 'in_transactionId' THEN
                rec.transactionId := in_transactionId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE Invoices SET
            accountId = rec.accountId,
            description = rec.description,
            cachedTotal = rec.cachedTotal,
            currencyTypeId = rec.currencyTypeId,
            stateId = rec.stateId,
            holdingDateTime = rec.holdingDateTime,
            transactionId = rec.transactionId
        WHERE invoiceId = in_invoiceId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_invoice_update(text, text, integer, integer, text, integer, integer, text, timestamp, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoice_filter(text, text, integer, integer, text, integer, text, integer, integer, text, timestamp, integer, text)

-- DROP FUNCTION fn_invoice_filter(text, text, integer, integer, text, integer, text, integer, integer, text, timestamp, integer, text);

CREATE OR REPLACE FUNCTION fn_invoice_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_accountId integer, in_description text, in_cachedTotal integer, in_currencyTypeId integer, in_state text, in_holdingDateTime timestamp, in_transactionId integer, in_filter_list text)
  RETURNS SETOF Invoices AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Invoices';
        var_functionName text := 'FILTER';

        rec Invoices%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_stateId int;
        var_order text;
        var_query text := 'SELECT * FROM Invoices WHERE 0 = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_invoiceId' THEN
            ELSEIF filter_name = 'in_accountId' THEN
                var_query := var_query || ' AND accountId = ' || in_accountId;
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_cachedTotal' THEN
                var_query := var_query || ' AND cachedTotal = ' || in_cachedTotal;
            ELSEIF filter_name = 'in_currencyTypeId' THEN
                var_query := var_query || ' AND currencyTypeId = ' || in_currencyTypeId;
            ELSEIF filter_name = 'in_state' THEN
                var_stateId := invoiceStateId FROM InvoiceStates WHERE tag ILIKE in_state;
                var_query := var_query || ' AND stateId = ' || var_stateId;
            ELSEIF filter_name = 'in_holdingDateTime' THEN
                var_query := var_query || ' AND holdingDateTime <= ' || in_holdingDateTime;
            ELSEIF filter_name = 'in_transactionId' THEN
                var_query := var_query || ' AND transactionId = ' || in_transactionId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM Invoices;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_invoice_filter(text, text, integer, integer, text, integer, text, integer, integer, text, timestamp, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoice_find(text, text, integer)

-- DROP FUNCTION fn_invoice_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_invoice_find(in_credentials_email text, in_credentials_password text, in_invoiceId integer)
  RETURNS SETOF Invoices AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Invoices';
        var_functionName text := 'FIND';

        rec Invoices%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM Invoices
                WHERE invoiceId = in_invoiceId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_invoice_find(text, text, integer) OWNER TO postgres;










CREATE OR REPLACE FUNCTION fn_getPendingSubscriptionInvoices(in_credentials_email text, in_credentials_password text)
  RETURNS SETOF Invoices AS
$BODY$
    DECLARE
        var_credentialsId text;

        rec Invoices%ROWTYPE;

    BEGIN
        var_credentialsId := id FROM App_User WHERE (in_credentials_email ILIKE email OR in_credentials_email = displayName)
            AND passwordHash = encode(digest(in_credentials_password || COALESCE(passwordSalt,''),'sha1'),'hex');
        
        -- id:328, 401-100-0  Free Learners
        -- id:331, 401-101-0  100 Learners
        -- id:334, 401-102-0  500 Learners
        -- id:337, 401-103-0 1000 Learners   
        
        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT DISTINCT i.*
                FROM invoices i
                INNER JOIN accounts a on a.accountid = i.accountid
                INNER JOIN accountmemberroles amr on amr.accountid = a.accountid
                INNER JOIN invoicelineitems ili on ili.invoiceid = i.invoiceid
                INNER JOIN ProductCodes pc
                      on  pc.productCodeId = ili.productCodeId
                INNER JOIN ProductCategories pcat
                       on pc.productCategoryId = pcat.productCategoryId
                       and pcat.tag in ('SUBSCRIPTION_INITIAL', 'SUBSCRIPTION_RECURRING', 'SUBSCRIPTION_UPGRADE')
                WHERE amr.memberid = var_credentialsId
                AND i.stateid = 1
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_getPendingSubscriptionInvoices(text, text) OWNER TO postgres;


CREATE OR REPLACE FUNCTION fn_cancelPendingSubscriptionInvoices(in_credentials_email text, in_credentials_password text) 
RETURNS VOID AS
$BODY$
    DECLARE
        var_credentialsId text;
        rec Invoices%ROWTYPE; 

    BEGIN
        var_credentialsId := id FROM App_User WHERE (in_credentials_email ILIKE email OR in_credentials_email = displayName)
            AND passwordHash = encode(digest(in_credentials_password || COALESCE(passwordSalt,''),'sha1'),'hex');

        IF var_credentialsId IS NOT NULL THEN
            
            
            FOR rec IN SELECT * FROM fn_getPendingSubscriptionInvoices(in_credentials_email, in_credentials_password)
            LOOP
                UPDATE invoices
                SET stateid = 4
                WHERE invoiceid = rec.invoiceid;

                UPDATE accounts
                SET renewalnoticesent = 0
                  -- Note this term due date is supposed to set it back to what the date was
                  -- but if it was Jan 31, the addition will be Feb 28, and then this subtraction will be Jan 28
                  -- this will fix itself next time it is pushed by a month
                  -- this change attempts to fix a date pushed out by cron1, but will not term earlier than tonight
                  , termduedatetime = GREATEST(CURRENT_DATE + '1 days'::INTERVAL, termduedatetime - COALESCE(
                  (SELECT max(terms::INTERVAL)
                  FROM invoicelineitems
                  INNER JOIN productcodes
                      ON productcodes.productcodeid = invoicelineitems.productcodeid
                     AND productcodes.terms IS NOT NULL
                  WHERE invoicelineitems.invoiceid = rec.invoiceid)
                  , '0 months'::INTERVAL
                ))
                WHERE accountid = rec.accountid;
            END LOOP;
            
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_cancelPendingSubscriptionInvoices(text, text) OWNER TO postgres;
