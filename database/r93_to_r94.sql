-------------------------------------------------------------------------------
-- r93_to_r94.sql
-- Fix self-price data corrupted by bug in ProjectCreateServiceImpl
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES (94);

-- it might be more correct to add 25 in this case, but that would potentially
--   set the price below our 0.25 minimum. 
update projects set selfprice = 0 where selfprice < 0;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

