-------------------------------------------------------------------------------
-- 
-- add a column to projects
-- use the DB to set createdDateTime
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (73);

alter table projects alter column createdDateTime set default now();
alter table projects alter column createdDateTime drop not null;

alter table projects add column urlName varchar(256);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;