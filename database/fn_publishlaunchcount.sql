--------------------------------------------------------------------------------
-- fn_publishlaunchcounts.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- PublishLaunchCounts functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='PublishLaunchCounts'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('PublishLaunchCounts','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_publishlaunchcount_create(text, text, integer, integer, integer)

-- DROP FUNCTION fn_publishlaunchcount_create(text, text, integer, integer, integer);

CREATE OR REPLACE FUNCTION fn_publishlaunchcount_create(in_credentials_email text, in_credentials_password text, in_publishId integer, in_parentPublishId integer, in_launchedByAccountId integer)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'PublishLaunchCounts';
        var_functionName text := 'CREATE';

        new_publishLaunchCountId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into PublishLaunchCounts(
            publishId,
            parentPublishId,
            launchedByAccountId
        )
        values(
            in_publishId,
            in_parentPublishId,
            in_launchedByAccountId
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_publishLaunchCountId := currval('seq_PublishLaunchCounts_publishLaunchCountId');
        END IF;

        return new_publishLaunchCountId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_publishlaunchcount_create(text, text, integer, integer, integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_publishlaunchcount_update(text, text, integer, integer, integer, integer, text)

-- DROP FUNCTION fn_publishlaunchcount_update(text, text, integer, integer, integer, integer, text);

CREATE OR REPLACE FUNCTION fn_publishlaunchcount_update(in_credentials_email text, in_credentials_password text, in_publishLaunchCountId integer, in_publishId integer, in_parentPublishId integer, in_launchedByAccountId integer, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'PublishLaunchCounts';
        var_functionName text := 'UPDATE';

        rec PublishLaunchCounts%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from PublishLaunchCounts where publishLaunchCountId = in_publishLaunchCountId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_publishId' THEN
                rec.publishId := in_publishId;
            ELSEIF param_name = 'in_parentPublishId' THEN
                rec.parentPublishId := in_parentPublishId;
            ELSEIF param_name = 'in_launchedByAccountId' THEN
                rec.launchedByAccountId := in_launchedByAccountId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE PublishLaunchCounts SET
            publishId = rec.publishId,
            parentPublishId = rec.parentPublishId,
            launchedByAccountId = rec.launchedByAccountId
        WHERE publishLaunchCountId = in_publishLaunchCountId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_publishlaunchcount_update(text, text, integer, integer, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_publishlaunchcount_retire(text, text, integer, text)

-- DROP FUNCTION fn_publishlaunchcount_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_publishlaunchcount_retire(in_credentials_email text, in_credentials_password text, in_publishLaunchCountId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'PublishLaunchCounts';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE PublishLaunchCounts SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE publishLaunchCountId = in_publishLaunchCountId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_publishlaunchcount_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_publishlaunchcount_filter(text, text, integer, integer, text, integer, integer, integer, text)

-- DROP FUNCTION fn_publishlaunchcount_filter(text, text, integer, integer, text, integer, integer, integer, text);

CREATE OR REPLACE FUNCTION fn_publishlaunchcount_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_publishId integer, in_parentPublishId integer, in_launchedByAccountId integer, in_filter_list text)
  RETURNS SETOF PublishLaunchCounts AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'PublishLaunchCounts';
        var_functionName text := 'FILTER';

        rec PublishLaunchCounts%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM PublishLaunchCounts WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_publishLaunchCountId' THEN
            ELSEIF filter_name = 'in_publishId' THEN
                var_query := var_query || ' AND publishId = ' || in_publishId;
            ELSEIF filter_name = 'in_parentPublishId' THEN
                var_query := var_query || ' AND parentPublishId = ' || in_parentPublishId;
            ELSEIF filter_name = 'in_launchedByAccountId' THEN
                var_query := var_query || ' AND launchedByAccountId = ' || in_launchedByAccountId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM PublishLaunchCounts;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_publishlaunchcount_filter(text, text, integer, integer, text, integer, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_publishlaunchcount_find(text, text, integer)

-- DROP FUNCTION fn_publishlaunchcount_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_publishlaunchcount_find(in_credentials_email text, in_credentials_password text, in_publishLaunchCountId integer)
  RETURNS SETOF PublishLaunchCounts AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'PublishLaunchCounts';
        var_functionName text := 'FIND';

        rec PublishLaunchCounts%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM PublishLaunchCounts
                WHERE publishLaunchCountId = in_publishLaunchCountId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_publishlaunchcount_find(text, text, integer) OWNER TO postgres;
