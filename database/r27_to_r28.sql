-------------------------------------------------------------------------------
-- r27_to_r28.sql
--
-- Add category data
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (28);
-------------------------------------------------------------------------------

-- 1 = gadget, 2 = zapp
-- gadget categories
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Accessibility', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Animation', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Audio & Media', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('CCAF Sets', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Charting & Graphing', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Experiments', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Feedback & Messaging', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Graphics', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Interface & Design', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Logic & Branching', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Quiz & Test', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Utilities', 1, (select id from app_user where displayname = 'dhoyt'), now(), 0);

-- zapp categories
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Business', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Education', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Entertainment', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Experiments', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Fitness', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Friends & Family', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Fun & Games', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Health & Medical', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('How To & DIY', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Lifestyle', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Music & Media', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('News & Weather', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Productivity', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Social', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Sports', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Training', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);
insert into categories (name, typeid, editedbyid, editeddatetime, retired) values ('Zebra', 2, (select id from app_user where displayname = 'dhoyt'), now(), 0);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
