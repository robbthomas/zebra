-------------------------------------------------------------------------------
-- r112_to_r113.sql
--
-- Author:  Jesse Coyle
-- Notes:
--
-------------------------------------------------------------------------------

-- update product code prices to reflect what is listed in the site and approved by Marty

-- Account Learners 500
update productcodes
  set price = 10000 -- $100.00
  where productcodeid in (334,335,336);

-- Account Learners 1000
update productcodes
set price = 20000 -- $200.00
where productcodeid in (337,338,339);


------------------------------------------------------
-- Increment the database version - DO NOT remove this line
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (113);
------------------------------------------------------