-------------------------------------------------------------------------------
-- r0_to_r1.sql
--
-- TBD:
-- - Changes to eCommerce supporting schema.
-- - If keeping primary keys as text UUIDs, then switch additional new
--   tables primary keys to text UUIDs where there is and risk of hackers
--   fishing for key Ids
--   
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

-------------------------------------------------------------------------------

-- Load plpgsql scripting language for db functions.
-- CREATE LANGUAGE plpgsql;

-- Load contrib libs to support encryption and uuid gen. Paths below correspond
-- to default Ubuntu 10.0 Synaptic Package manager installation location in VM1142.
-- on mac:  /Library/PostgreSQL/8.4/share/postgresql/contrib/
--\i /usr/share/postgresql/8.4/contrib/pgcrypto.sql
\i /usr/share/postgresql/8.4/contrib/uuid-ossp.sql


CREATE SEQUENCE seq_databaseVersionId;

CREATE TABLE DatabaseVersion (
	databaseVersionId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_databaseVersionId'),
	versionNumber int NOT NULL,
	versionDateTime timestamp NOT NULL DEFAULT now()
);

-- may be redundant to have both a primary key and version number that must be unique
-- BUT may allow flexibility of applying version updates that do not overlap our of
-- sequence
CREATE UNIQUE INDEX ix_DatabaseVersion_versionNumber ON DatabaseVersion(versionNumber);



CREATE SEQUENCE seq_Companies_companyId START WITH 1000;

CREATE TABLE Companies (
	companyId int NOT NULL PRIMARY KEY DEFAULT nextval('seq_Companies_companyId'),
	name text,
	description text,
	companyUrl text,
	editedById text NOT NULL,  -- FK to App_User.Id (aka Members.memberId)
	editedDateTime timestamp DEFAULT now(),
	
	CONSTRAINT fk_Companies_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id)
);

-- Use App_User.Id for username=blackbeard to create Allen Interactions company
-- entry (or should we just be plugging in null for all of these!?)

INSERT INTO Companies(name, companyUrl, editedById, description)
VALUES ('Allen Interactions Inc.', 'http://www.alleninteractions.com', (SELECT Id FROM App_User WHERE username ILIKE 'dhoyt'), 'Custom design e-learning, blended learning and other technology-enabled solutions customized for specific performance improvement.');


CREATE TABLE CurrencyTypes (
	currencyTypeId int NOT NULL PRIMARY KEY,
        name text,
	currencyName text,
	currencyCode text,
	currencySymbol text,
	countryCode text,
	precision int default 2,  -- number of zeros after minor delimiter, used when normalizing to integer
	majorDelimiter text default(','),  -- delimiter for thousands, usually comma or period
	minorDelimiter text default('.')  -- delimiter for fractional component, usuallly comma or period
);

INSERT INTO CurrencyTypes(currencyTypeId, name, currencyName, currencyCode, currencySymbol, countryCode)
VALUES (1, 'US Dollar', 'Dollar', 'USD', '$', 'US');


CREATE TABLE AccountTypes (
	accountTypeId int NOT NULL PRIMARY KEY,
        tag text,
	name text,
	description text,
        terms text DEFAULT '1 month', -- uses postgresql interval syntax
        price int,
        currencyTypeId int DEFAULT(1),
	displayOrder int,
	editedById text,  -- FK to App_User.Id (aka Members.memberId)
	editedDateTime timestamp DEFAULT now(),
	retired int default(0),  -- 0=not retired
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_AccountTypes_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id),

        CONSTRAINT fk_AccountTypes_currencyTypeId FOREIGN KEY
        (currencyTypeId) REFERENCES CurrencyTypes(currencyTypeId),

	CONSTRAINT fk_AccountTypes_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
);

INSERT INTO AccountTypes(AccountTypeId, tag, name, price, displayOrder, description)
VALUES(1, 'free', 'Free Trial Account', 0, 10, 'Vivamus GRATIS auctor leo vel dui. Aliquam erat volutpat. Phasellus nibh. Vestibulum ante ipsum primis in Aliquam erat volutpat. Phasellus nibh.');

INSERT INTO AccountTypes(AccountTypeId, tag, name, price, displayOrder, description)
VALUES(2, 'basic', 'Basic Account', 499, 20, 'Vivamus BASIS auctor leo vel dui. Aliquam erat volutpat. Phasellus nibh. Vestibulum ante ipsum primis in Aliquam erat volutpat. Phasellus nibh.');

INSERT INTO AccountTypes(AccountTypeId, tag, name, price, displayOrder, description)
VALUES(3, 'standard', 'Standard Account', 999, 30, 'Vivamus VEXILLUM auctor leo vel dui. Aliquam erat volutpat. Phasellus nibh. Vestibulum ante ipsum primis in Aliquam erat volutpat. Phasellus nibh.');

INSERT INTO AccountTypes(AccountTypeId, tag, name, price, displayOrder, description)
VALUES(4, 'premium', 'Premium Account', 1999, 40, 'Vivamus PREMIUM auctor leo vel dui. Aliquam erat volutpat. Phasellus nibh. Vestibulum ante ipsum primis in Aliquam erat volutpat. Phasellus nibh.');

INSERT INTO AccountTypes(AccountTypeId, tag, name, price, displayOrder, description)
VALUES(5, 'professional', 'Professional Account', 2999, 50, 'Vivamus PROFESSIO auctor leo vel dui. Aliquam erat volutpat. Phasellus nibh. Vestibulum ante ipsum primis in Aliquam erat volutpat. Phasellus nibh.');



CREATE SEQUENCE seq_Accounts_accountId START WITH 2000;

CREATE TABLE Accounts (
	accountId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_Accounts_accountId'),
	companyId int, -- FK to Company table
	typeId int not null, -- FK to AccountType table (at this time, Free Account, Basic Account, Pro Account)
        termDueDateTime timestamp, -- mm/dd/yyyy subscription expires and needs to be renewed/invoiced
        goodStanding int not null DEFAULT(1), -- 0=false, 1=true
	name text,
	description text,
	tokenBalance int default(0),
	editedById text not null, -- FK to App_User.Id (aka Member.memberId)
	editedDateTime timestamp default now(),
	retired int default(0),  -- 0=not retired
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_Accounts_companyId FOREIGN KEY
	(companyId) REFERENCES Companies(companyId),
	
	CONSTRAINT fk_Accounts_accountTypeId FOREIGN KEY
	(typeId) REFERENCES AccountTypes(accountTypeId),
	
	CONSTRAINT fk_Accounts_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id),
	
	CONSTRAINT fk_Accounts_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
	
);

CREATE INDEX ix_Accounts_companyId ON Accounts(companyId);
CREATE INDEX ix_Accounts_typeId ON Accounts(typeId);



CREATE TABLE AddressTelephoneTypes (
	addressTelephoneTypeId int NOT NULL PRIMARY KEY,
        tag text,
	name text,
	description text,
	displayOrder int,
	retired int,  -- 0=not retired, 1=retired
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_AddressTelephoneTypes_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
		
);

INSERT INTO AddressTelephoneTypes(addressTelephoneTypeId,tag,name,displayOrder) VALUES (1,'primary','Primary',10);
INSERT INTO AddressTelephoneTypes(addressTelephoneTypeId,tag,name,displayOrder) VALUES (2,'mobile','Mobile',20);
INSERT INTO AddressTelephoneTypes(addressTelephoneTypeId,tag,name,displayOrder) VALUES (3,'sales','Sales',30);
INSERT INTO AddressTelephoneTypes(addressTelephoneTypeId,tag,name,displayOrder) VALUES (4,'billing','Billing',40);
INSERT INTO AddressTelephoneTypes(addressTelephoneTypeId,tag,name,displayOrder) VALUES (5,'shipping','Shipping',50);
INSERT INTO AddressTelephoneTypes(addressTelephoneTypeId,tag,name,displayOrder) VALUES (6,'technical','Technical',60);
INSERT INTO AddressTelephoneTypes(addressTelephoneTypeId,tag,name,displayOrder) VALUES (7,'personal','Personal',70);


CREATE SEQUENCE seq_Addresses_addressId START WITH 3000;

CREATE TABLE Addresses (
	addressId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_Addresses_addressId'),
	typeId int not null,  -- FK to AddressTypes
	accountId bigint not null,  -- FK to Accounts table (required?)
	name text,  -- allows associating optional name (as it appears on credit card or bank account, etc.)
	company text,  -- overrides Accounts.companyId join to Companies.name if not null
	address1 text,
	address2 text,
	city text,
	stateProvince text,
	zipPostalCode text,
	country text,
	editedById text not null,  -- FK to App_User.Id (aka Members.memberId)
	editedDateTime timestamp default now(),
	retired int not null default 0,
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_Addresses_typeId FOREIGN KEY
	(typeId) REFERENCES AddressTelephoneTypes(addressTelephoneTypeId),
	
	CONSTRAINT fk_Addresses_accountId FOREIGN KEY
	(accountId) REFERENCES Accounts(accountId),
	
	CONSTRAINT fk_Addresses_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id),
	
	CONSTRAINT fk_Addresses_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)

	);

CREATE INDEX ix_Addresses_typeId ON Addresses(typeId);
CREATE INDEX ix_Addresses_accountId ON Addresses(accountId);



CREATE SEQUENCE seq_Telephones_telephoneId START WITH 4000;

CREATE TABLE Telephones (
	telephoneId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_Telephones_telephoneId'),
	typeId int not null,  -- FK to AddressTelephoneTypes
	accountId bigint not null,  -- FK to Accounts table
	memberId text,  -- optional - FK to Members table (aka App_User)
	label text,  -- descriptive display label
	number text,  -- sequence of 0-9, *, # (and optional dashes, spaces, periods and other punctuation which is ignored?)
	editedById text not null,  -- FK to Members table
	editedDateTime timestamp,
	retired int default 0,
	retiredById text,  -- FK to Members table
	retiredDateTime timestamp,
	
	CONSTRAINT fk_Telephones_typeId FOREIGN KEY
	(typeId) REFERENCES AddressTelephoneTypes(addressTelephoneTypeId),
	
	CONSTRAINT fk_Telephones_accountId FOREIGN KEY
	(accountId) REFERENCES Accounts(accountId),
		
	CONSTRAINT fk_Telephones_memberId FOREIGN KEY
	(memberId) REFERENCES App_User(Id),
		
	CONSTRAINT fk_Telephones_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id),
		
	CONSTRAINT fk_Telephones_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
		
);

CREATE INDEX ix_Telephones_typeId ON Telephones(typeId);
CREATE INDEX ix_Telephones_accountId ON Telephones(accountId);
CREATE INDEX ix_Telephones_memberId ON Telephones(memberId);



CREATE TABLE RoleTypes (
	roleTypeId int NOT NULL PRIMARY KEY,
        tag text,
	name text,
	description text,
	displayOrder int,
	retired int default 0,  -- 0=not retired, 1=retired
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_RoleTypes_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
		
);

INSERT INTO RoleTypes(roleTypeId,tag,name,displayOrder) VALUES (1,'ROLE_TYPE_ACCOUNT_ADMINISTRATOR','Account Administrator', 10);
INSERT INTO RoleTypes(roleTypeId,tag,name,displayOrder) VALUES (2,'ROLE_TYPE_AUTHOR','Author', 20);
INSERT INTO RoleTypes(roleTypeId,tag,name,displayOrder) VALUES (3,'ROLE_TYPE_VIEWER','Viewer', 30);
INSERT INTO RoleTypes(roleTypeId,tag,name,displayOrder) VALUES (4,'ROLE_TYPE_SALES_CONTACT','Sales Contact', 40);
INSERT INTO RoleTypes(roleTypeId,tag,name,displayOrder) VALUES (5,'ROLE_TYPE_SUPPORT_CONTACT','Support Contact', 50);
INSERT INTO RoleTypes(roleTypeId,tag,name,displayOrder) VALUES (6,'ROLE_TYPE_FEEDBACK','Feedback', 60);
INSERT INTO RoleTypes(roleTypeId,tag,name,displayOrder) VALUES (1000,'ROLE_TYPE_SYSTEM_ADMINISTRATOR','System Administrator', 1000);
INSERT INTO RoleTypes(roleTypeId,tag,name,displayOrder) VALUES (2000,'ROLE_TYPE_COMMERCE_CRON','Commerce CRON', 2000);


CREATE TABLE AccountMemberRoles (
	accountId bigint not null,  -- FK to Accounts.accountId
	memberId text not null,  -- FK to App_User.Id (aka Members.memberId)
	typeId int CHECK (typeId < 100),  -- FK to RoleType.roleTypeId
	retired int default(0),  -- 0=not retired, 1=retired
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_AccountMemberRoles_accountId FOREIGN KEY
	(accountId) REFERENCES Accounts(accountId),
	
	CONSTRAINT fk_AccountMemberRoles_typeId FOREIGN KEY
	(typeId) REFERENCES RoleTypes(roleTypeId),
	
	CONSTRAINT fk_AccountMemberRoles_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
		
);

CREATE INDEX ix_AccountMemberRoles_accountId ON AccountMemberRoles(accountId);
CREATE INDEX ix_AccountMemberRoles_memberId ON AccountMemberRoles(memberId);
CREATE INDEX ix_AccountMemberRoles_typeId ON AccountMemberRoles(typeId);



CREATE TABLE ProjectMemberRoles (
	projectId text not null,  -- FK to Projects.projectId
	memberId text not null,  -- FK to App_User.Id (aka Members.memberId)
	typeId int not null default 3 CHECK (typeId < 100),  -- FK to RoleTypes.roleTypeId, 3=Viewer
	retired int default 0,  -- 0=not
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,

	CONSTRAINT fk_ProjectMemberRoles_typeId FOREIGN KEY
	(typeId) REFERENCES RoleTypes(roleTypeId),
	
	CONSTRAINT fk_ProjectMemberRoles_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
		
);

CREATE INDEX ix_ProjectMemberRoles_projectId ON ProjectMemberRoles(projectId);
CREATE INDEX ix_ProjectMemberRoles_memberId ON ProjectMemberRoles(memberId);
CREATE INDEX ix_ProjectMemberRoles_typeId ON ProjectMemberRoles(typeId);



CREATE SEQUENCE seq_Feedback_feedbackId START WITH 5000;

CREATE TABLE Feedback (
	feedbackId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_Feedback_feedbackId'),
	reMemberId text,  -- optional, member addressed by feedback, FK to App_User.Id (aka Members.memberId)
	reAccountId bigint,  -- optional, account addressed by feedback, FK to Accounts
	reProjectId text,  -- optional, project addressed by feedback, FK to Parent.Id (aka Projects.projectId)
	comments text not null,  -- required (might require a certain length or screen for language?)
	editedById text,  -- optional, member giving the feedback, FK to App_User.Id (aka Members.memberId), null for anonoymous
	editedDateTime timestamp default now(),
	
	CONSTRAINT fk_Feedback_reMemberId FOREIGN KEY
	(reMemberId) REFERENCES App_User(Id),
	
	CONSTRAINT fk_Feedback_reAccountId FOREIGN KEY
	(reAccountId) REFERENCES Accounts(accountId),
	
	CONSTRAINT fk_Feedback_reProjectId FOREIGN KEY
	(reProjectId) REFERENCES Parent(Id),
	
	CONSTRAINT fk_Feedback_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id)
	
	-- * Should require at least one of reMemberId, reAccountId, reProjectId to be non-null.
	-- * If reAccountId or reProjectId specified, should Email to RoleTypeId = Feedback folks
	--   for the corresponding account	
);

CREATE INDEX ix_Feedback_reMemberId ON Feedback(reMemberId);
CREATE INDEX ix_Feedback_reAccountId ON Feedback(reAccountId);
CREATE INDEX ix_Feedback_reProjectId ON Feedback(reProjectId);
CREATE INDEX ix_Feedback_editedById ON Feedback(editedById);



CREATE TABLE SystemAdministrators (
	memberId text not null,  -- FK to App_User.Id (aka Members.memberId) to retrieve Email, etc.???
	passwordHash text,  -- SHA-1 hash of the password, different from Members.password
	passwordSalt text, -- randomly generated salt
	
	CONSTRAINT fk_SystemAdministrators_memberId FOREIGN KEY
	(memberId) REFERENCES App_User(Id)
	
);

-- add special user to run commerce cron webservices



CREATE TABLE ProjectTypes (
	projectTypeId int NOT NULL PRIMARY KEY,
	name text,  -- currently only Gadget or App
	description text,
	displayOrder int
);

INSERT INTO ProjectTypes(projectTypeId,name,displayOrder) VALUES (1,'Gadget',10);
INSERT INTO ProjectTypes(projectTypeId,name,displayOrder) VALUES (2,'App',20);


CREATE TABLE RetiredReasonTypes (
	retiredReasonTypeId int NOT NULL PRIMARY KEY,
	name text,
	description text,
	displayOrder int
);

INSERT INTO RetiredReasonTypes(retiredReasonTypeId,name,displayOrder) VALUES (1,'Expired',10);
INSERT INTO RetiredReasonTypes(retiredReasonTypeId,name,displayOrder) VALUES (2,'Retired by Author',20);
INSERT INTO RetiredReasonTypes(retiredReasonTypeId,name,displayOrder) VALUES (3,'Retired by User',30);
INSERT INTO RetiredReasonTypes(retiredReasonTypeId,name,displayOrder) VALUES (4,'Retired by System',40);



CREATE SEQUENCE seq_Categories_categoryId START WITH 6000;

CREATE TABLE Categories (
	categoryId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_Categories_categoryId'),
	parentCategoryId int,  -- FK to Categories.categoryId to accomodate hierarchical organization
	name text,
	description text,
	typeId int not null,  -- FK to ProjectTypes.projectTypeId
	categoryOrder int,  -- depth first ordering of this category in hierarchical organization
	categoryLevel int,  -- level of this category in hierarchical organization
	editedById text not null,  -- FK to App_User.Id (aka Members.memberId)
	editedDateTime timestamp,
	retired int,  -- 0=not
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_Categories_typeId FOREIGN KEY
	(typeId) REFERENCES ProjectTypes(projectTypeId),
	
	CONSTRAINT fk_Categories_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id),
	
	CONSTRAINT fk_Categories_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
	
);

ALTER TABLE Categories ADD CONSTRAINT fk_Categories_parentCategoryId FOREIGN KEY
	(parentCategoryId) REFERENCES Categories(categoryId);

CREATE INDEX ix_Categories_parentCategoryId ON Categories(parentCategoryId);
CREATE INDEX ix_Categories_typeId ON Categories(typeId);
CREATE INDEX ix_Categories_categoryOrder ON Categories(categoryOrder);
CREATE INDEX ix_Categories_categoryLevel ON Categories(categoryLevel);



CREATE SEQUENCE seq_ScreenShots_screenShotId START WITH 7000;

CREATE TABLE ScreenShots (
	screenShotId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_ScreenShots_screenShotId'),
	projectId text not null,  -- FK to Projects.projectId
	orderNumber int,  -- display order number for this screen shot
	assetId text not null,  -- FK to Asset.Id
	description text,
	editedById text not null,  -- FK to App_User.Id (aka Members.memberId)
	editedDateTime timestamp default now(),
	retired int default 0,  -- 0=not
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_ScreenShots_projectId FOREIGN KEY
	(projectId) REFERENCES Parent(Id),
	
	CONSTRAINT fk_ScreenShots_assetId FOREIGN KEY
	(assetId) REFERENCES Asset(Id),
	
	CONSTRAINT fk_ScreenShots_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id),
	
	CONSTRAINT fk_ScreenShots_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
	
);

CREATE INDEX ix_ScreenShots_projectId ON ScreenShots(projectId);
CREATE INDEX ix_ScreenShots_assetId ON ScreenShots(assetId);



CREATE TABLE PublishStatuses (
	publishStatusId int NOT NULL PRIMARY KEY,
        tag text,
	name text,
	description text, -- for tooltips
	displayOrder int
);

INSERT INTO PublishStatuses(publishStatusId,tag,name,displayOrder) values (1,'publish_status_development','Development',10);
INSERT INTO PublishStatuses(publishStatusId,tag,name,displayOrder) values (2,'publish_status_alpha','Alpha',20);
INSERT INTO PublishStatuses(publishStatusId,tag,name,displayOrder) values (3,'publish_status_beta','Beta',30);
INSERT INTO PublishStatuses(publishStatusId,tag,name,displayOrder) values (4,'publish_status_release','Release',40);


CREATE TABLE PublishTypes (
	publishTypeId int NOT NULL PRIMARY KEY,
        tag text,
	name text,
	description text, -- for tooltips
	displayOrder int
);

INSERT INTO PublishTypes(publishTypeId,tag,name,displayOrder) VALUES (1,'publish_type_gadget','Gadget',10);
INSERT INTO PublishTypes(publishTypeId,tag,name,displayOrder) VALUES (2,'publish_type_app','App',20);


CREATE TABLE LicenseTypes (
	licenseTypeId int NOT NULL PRIMARY KEY,
        tag text,
	name text,
	description text,
	displayOrder int
);

INSERT INTO LicenseTypes(licenseTypeId,tag,name,displayOrder) VALUES (1,'license_type_gnu_open_source','GNU Open Source',10);
INSERT INTO LicenseTypes(licenseTypeId,tag,name,displayOrder) VALUES (2,'license_type_zebra_open_source','Zebra Open Source',20);
INSERT INTO LicenseTypes(licenseTypeId,tag,name,displayOrder) VALUES (3,'license_type_account_use_paid','Account Use Paid',30);
INSERT INTO LicenseTypes(licenseTypeId,tag,name,displayOrder) VALUES (4,'license_type_project_use_paid','Project Use Paid',40);


CREATE SEQUENCE seq_Published_publishId START WITH 8000;

CREATE TABLE Published (
	publishId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_Published_publishId'),
        accountId bigint not null,  -- FK to Accounts.accountId
        projectId text not null,  -- FK to Parent.Id (aka Projects table)
        firstPublishId bigint,  -- FK to Published.publishId referencing ORIGINAL version published, so entire lineage will share this
	versionMajor int not null default 0,
	versionMinor int not null default 0,
	versionDot int not null default 0,
	versionBuildNumber int not null default 0,
        categoryId bigint not null,  -- FK to Categories.categoryId
        name text,
        description text,
        thumbnailId text,  -- FK to Asset.id
	statusId int not null,  -- FK to PublishStatus.publishStatusId
	publishTypeId int not null,  -- FK to PublishTypes.publishTypeId (same as Projects.projectTypeId?)
	licenseTypeId int not null,  -- FK to LicenseTypes.licenseTypeId
	price int not null default 0,  -- copied from Projects.price, can override here
	currencyTypeId int not null,  -- FK to CurrencyTypes.currencyTypeId, copied from Projects, can override here
	editedById text not null,  -- FK to App_User.Id (aka Members.memberId)
	editedDateTime timestamp default now(),
	retired int default 0,  -- 0=not
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_Published_accountId FOREIGN KEY
	(accountId) REFERENCES Accounts(accountId),

	CONSTRAINT fk_Published_projectId FOREIGN KEY
	(projectId) REFERENCES Parent(id),

	CONSTRAINT fk_Published_categoryId FOREIGN KEY
	(categoryId) REFERENCES Categories(categoryId),

	CONSTRAINT fk_Published_thumbnailId FOREIGN KEY
	(thumbnailId) REFERENCES Asset(id),

	CONSTRAINT fk_Published_statusId FOREIGN KEY
	(statusId) REFERENCES PublishStatuses(publishStatusId),

	CONSTRAINT fk_Published_publishTypeId FOREIGN KEY
	(publishTypeId) REFERENCES PublishTypes(publishTypeId),
	
	CONSTRAINT fk_Published_licenseTypeId FOREIGN KEY
	(licenseTypeId) REFERENCES LicenseTypes(licenseTypeId),

	CONSTRAINT fk_Published_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id),
	
	CONSTRAINT fk_Published_currencyTypeId FOREIGN KEY
	(currencyTypeId) REFERENCES CurrencyTypes(currencyTypeId),
	
	CONSTRAINT fk_Published_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
	
);

-- FK to self can only be added AFTER the table has been created
ALTER TABLE Published ADD CONSTRAINT fk_Published_firstPublishId
    FOREIGN KEY (firstPublishId) REFERENCES Published(publishId);

CREATE INDEX ix_Published_accountId ON Published(accountId);
CREATE INDEX ix_Published_projectId ON Published(projectId);
CREATE INDEX ix_Published_categoryId ON Published(categoryId);
CREATE INDEX ix_Published_statusId ON Published(statusId);
CREATE INDEX ix_Published_firstPublishId ON Published(firstPublishId);
CREATE INDEX ix_Published_publishTypeId ON Published(publishTypeId);
CREATE INDEX ix_Published_licenseTypeId ON Published(licenseTypeId);
CREATE INDEX ix_Published_currencyTypeId ON Published(currencyTypeId);
CREATE INDEX ix_Published_editedById ON Published(editedById);


CREATE SEQUENCE seq_PublishLaunchCounts_publishLaunchCountId START WITH 17000;

CREATE TABLE PublishLaunchCounts (
    publishLaunchCountId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_PublishLaunchCounts_publishLaunchCountId'),
    publishId bigint NOT NULL, -- FK to Publish
    parentPublishId bigint,  -- FK to Publish, if publishId for this launch is as a nested gadget
    launchedByAccountId bigint NOT NULL,
    launchedDateTime timestamp DEFAULT now(),

    CONSTRAINT fk_PublishLaunchCounts_publishId FOREIGN KEY
    (publishId) REFERENCES Published(publishId),

    CONSTRAINT fk_PublishLaunchCounts_parentPublishId FOREIGN KEY
    (parentPublishId) REFERENCES Published(publishId),

    CONSTRAINT fk_PublishLaunchCounts_launchedByAccountId FOREIGN KEY
    (launchedByAccountId) REFERENCES Accounts(accountId)

);

CREATE INDEX ix_PublishLaunchCounts_publishId ON PublishLaunchCounts(publishId);
CREATE INDEX ix_PublishLaunchCounts_launchedByAccountId ON PublishLaunchCounts(launchedByAccountId);



CREATE SEQUENCE seq_CreditCards_creditCardId START WITH 9000;

CREATE TABLE CreditCards (
	creditCardId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_CreditCards_creditCardId'),
	accountId bigint not null,  -- FK to Accounts.accountId
        addressId bigint not null,  -- FK to Addresses.addressId for billing address used?
        nameOnCreditCard text, -- name as it appears on the card
	maskedNumber text,  -- as used by CIM
	expirationMonth text,  -- 01 to 12
	expirationYear text,  -- yyyy
        customerProfileId text, -- as returned by CIM
	customerPaymentProfileId text,  -- as returned by CIM
	editedById text not null,  -- FK to App_User.Id (aka Members.memberId)
	editedDateTime timestamp default now(),
	retired int default 0,  -- 0=not
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_CreditCards_accountId FOREIGN KEY
	(accountId) REFERENCES Accounts(accountId),
	
	CONSTRAINT fk_CreditCards_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id),
	
	CONSTRAINT fk_CreditCards_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)
	
);

CREATE INDEX ix_CreditCards_accountId ON CreditCards(accountId);



CREATE SEQUENCE seq_EChecks_eCheckId START WITH 10000;

CREATE TABLE EChecks (
	eCheckId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_EChecks_eCheckId'),
	accountId bigint not null,  -- FK to Accounts.accountId
	bankRoutingNumber text,
	bankAccountNumber text, -- we might mask this one?
	bankName text,
	bankAccountName text, -- name on the account
        customerProfileId text, -- as returned by CIM
	customerPaymentProfileId text,  -- as returned by CIM
	editedById text not null,  -- FK to App_User.Id (aka Members.memberId)
	editedDateTime timestamp default now(),
	retired int default 0,  -- 0=not
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	
	CONSTRAINT fk_EChecks_accountId FOREIGN KEY
	(accountId) REFERENCES Accounts(accountId),
	
	CONSTRAINT fk_EChecks_editedById FOREIGN KEY
	(editedById) REFERENCES App_User(Id),
	
	CONSTRAINT fk_EChecks_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id)	
);

CREATE INDEX ix_EChecks_accountId ON EChecks(accountId);


CREATE TABLE FinancialTransactionTypes (
	financialTransactionTypeId int NOT NULL PRIMARY KEY,
        tag text,
	name text,
	description text, -- for tooltips
	displayOrder int
);

INSERT INTO FinancialTransactionTypes(financialTransactionTypeId,tag,name,displayOrder) VALUES (1,'transaction_type_creditcard','Credit Card',10);
INSERT INTO FinancialTransactionTypes(financialTransactionTypeId,tag,name,displayOrder) VALUES (2,'transaction_type_echeck','eCheck',20);


CREATE TABLE FinancialTransactionStatus (
    FinancialTransactionStatusId int NOT NULL PRIMARY KEY,
    tag text,
    name text,
    description text,
    displayOrder int
);

-- Status values after Pending correspond to Authorize.net result codes.

INSERT INTO FinancialTransactionStatus(FinancialTransactionStatusId,tag,name,displayOrder,description)
    VALUES (10,'transaction_status_pending','Transaction Pending',10,'Transaction processing starting');
INSERT INTO FinancialTransactionStatus(FinancialTransactionStatusId,tag,name,displayOrder,description)
    VALUES (1,'transaction_status_approved','Transaction Approved',20,'Transaction has been approved');
INSERT INTO FinancialTransactionStatus(FinancialTransactionStatusId,tag,name,displayOrder,description)
    VALUES (2,'transaction_status_declined','Transaction Declined',30,'Transaction has been declined, see reason text for details');
INSERT INTO FinancialTransactionStatus(FinancialTransactionStatusId,tag,name,displayOrder,description)
    VALUES (3,'transaction_status_processing_error','Transaction Processing Error',40,'Transaction processing error, see reason text for details');
INSERT INTO FinancialTransactionStatus(FinancialTransactionStatusId,tag,name,displayOrder,description)
    VALUES (4,'transaction_status_held_for_review','Transaction Held for Review',50,'Transaction being held pending review, see reason text for details');
INSERT INTO FinancialTransactionStatus(FinancialTransactionStatusId,tag,name,displayOrder,description)
    VALUES (12,'transaction_status_declined_archived','Transaction Declined Archived',60,'Transaction was declined and then archived when account standing was restored');
INSERT INTO FinancialTransactionStatus(FinancialTransactionStatusId,tag,name,displayOrder,description)
    VALUES (13,'transaction_status_processing_error_archived','Transaction Processing Error Archived',60,'Transaction processing error was archived by a system administrator');
INSERT INTO FinancialTransactionStatus(FinancialTransactionStatusId,tag,name,displayOrder,description)
    VALUES (14,'transaction_status_held_for_review_archived','Transaction Held for Review Archived',60,'Transaction being held pending review was archived by a system administrator');



CREATE SEQUENCE seq_FinancialTransactions_financialTransactionId START 11000;

CREATE TABLE FinancialTransactions (
    financialTransactionId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_FinancialTransactions_financialTransactionId'),
    accountId int not null, -- FK to Accounts.accountId
    amount int not null DEFAULT(0), -- if positive then transaction was a charge, else transaction was a credit???
    perTransactionFee int not null DEFAULT(0), -- may not need this if separate Invoice generated against same transaction for per transaction fees???
    currencyTypeId int not null, -- FK to CurrencyTypes.currencyType
    label text DEFAULT uuid_generate_v1(), -- merchant (AI) generated transaction identifier
    typeId int,  -- FK to FinancialTransactionTypes, cc or echeck
    agentId bigint,  -- FK to CreditCards.creditCardId for cc or ECheck.eCheckId for eCheck used to make payment
    statusId int not null DEFAULT(10), -- FK to FinancialTransactionStatus, default to Pending
    statusReasonCode int, -- Authorize.net result reason code
    statusReasonText text, -- Authorize.net result reason text
    transactionDateTime timestamp DEFAULT now(),

    -- Details of Authorize.net return results and reason codes can be found in:
    -- http://www.authorize.net/support/ReportingGuide_XML.pdf

    CONSTRAINT fk_FinancialTransactions_accountId FOREIGN KEY
    (accountId) REFERENCES Accounts(accountId),

    CONSTRAINT fk_FinancialTransactions_currencyTypeId FOREIGN KEY
    (currencyTypeId) REFERENCES CurrencyTypes(currencyTypeId),

    CONSTRAINT fk_FinancialTransactions_typeId FOREIGN KEY
    (typeId) REFERENCES FinancialTransactionTypes(FinancialTransactionTypeId),

    CONSTRAINT fk_FinancialTransactions_statusId FOREIGN KEY
    (statusId) REFERENCES FinancialTransactionStatus(FinancialTransactionStatusId)
);

CREATE INDEX ix_FinancialTransactions_accountId ON FinancialTransactions(accountId);
CREATE INDEX ix_FinancialTransactions_currencyTypeId ON FinancialTransactions(currencyTypeId);
CREATE INDEX ix_FinancialTransactions_typeId ON FinancialTransactions(typeId);
CREATE INDEX ix_FinancialTransactions_agentId ON FinancialTransactions(agentId);
CREATE INDEX ix_FinancialTransactions_statusId ON FinancialTransactions(statusId);
CREATE INDEX ix_FinancialTransactions_statusReasonCode ON FinancialTransactions(statusReasonCode);


CREATE TABLE InvoiceStates (
    invoiceStateId int NOT NULL PRIMARY KEY,
    tag text,
    name text,
    description text,
    displayOrder int
);

INSERT INTO InvoiceStates(invoiceStateId,tag,name,displayOrder) VALUES(1,'invoice_state_pending','Invoice Pending',10);
INSERT INTO InvoiceStates(invoiceStateId,tag,name,displayOrder) VALUES(2,'invoice_state_charged','Invoice Charged',20);
INSERT INTO InvoiceStates(invoiceStateId,tag,name,displayOrder) VALUES(3,'invoice_state_credits_created','Invoice Credits Created',30);


CREATE SEQUENCE seq_Invoices_invoiceId START WITH 12000;

CREATE TABLE Invoices (
	invoiceId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_Invoices_invoiceId'),
	accountId bigint not null,  -- FK to Accounts.accountId
	description text,
	cachedTotal int DEFAULT(0),
	currencyTypeId int not null,  -- FK to CurrencyTypes.currencyTypeId
        stateId int not null DEFAULT(1), -- FK to InvoiceStates.invoiceStateId (Pending, Charged, Credit Created)
        createdDateTime timestamp DEFAULT now(),
	holdingDateTime timestamp not null default current_date, -- optional "holding" period if want to generate subscription invoices 1 week before processing
        transactionId bigint, -- FK to FinancialTransactions.financialTransactionId
	
	CONSTRAINT fk_Invoices_accountId FOREIGN KEY
	(accountId) REFERENCES Accounts(accountId),

	CONSTRAINT fk_Invoices_currencyTypeId FOREIGN KEY
	(currencyTypeId) REFERENCES CurrencyTypes(currencyTypeId),
	
	CONSTRAINT fk_Invoices_stateId FOREIGN KEY
	(stateId) REFERENCES InvoiceStates(InvoiceStateId),

	CONSTRAINT fk_Invoices_transactionId FOREIGN KEY
	(transactionId) REFERENCES FinancialTransactions(financialTransactionId)
);

CREATE INDEX ix_Invoices_accountId ON Invoices(accountId);
CREATE INDEX ix_Invoices_currencyTypeId ON Invoices(currencyTypeId);
CREATE INDEX ix_Invoices_stateId ON Invoices(stateId);
CREATE INDEX ix_Invoices_transactionId ON Invoices(transactionId);


CREATE SEQUENCE seq_InvoiceLineItems_invoiceLineItemId START WITH 13000;

CREATE TABLE InvoiceLineItems (
	invoiceLineItemId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_InvoiceLineItems_invoiceLineItemId'),
	invoiceId int not null,  -- FK to Invoices.invoiceId
	lineNumber int not null default(1),  -- display order in the invoice
        quantity int not null default(1), -- will likely always be 1 for gadget/app line items since would only charge once per gadget
	amountTotal int not null default(0),
	description text,
	publishId bigint,  -- FK to Projects.projectId (or null if something else)
	
	CONSTRAINT fk_InvoiceLineItems_invoiceId FOREIGN KEY
	(invoiceId) REFERENCES Invoices(invoiceId),
	
	CONSTRAINT fk_InvoiceLineItems_publishId FOREIGN KEY
	(publishId) REFERENCES Published(publishId)
);

CREATE INDEX ix_InvoiceLineItems_invoiceId ON InvoiceLineItems(invoiceId);
CREATE INDEX ix_InvoiceLineItems_publishId ON InvoiceLineItems(publishId);



CREATE TABLE PaymentStates (
    paymentStateId int NOT NULL PRIMARY KEY,
    tag text,
    name text,
    description text,
    displayOrder int
);

INSERT INTO PaymentStates(paymentStateId,tag,name,displayOrder) VALUES(1,'payment_state_pending','Payment Pending',10);
INSERT INTO PaymentStates(paymentStateId,tag,name,displayOrder) VALUES(2,'payment_state_credited','Payment Credited',20);

CREATE SEQUENCE seq_Payments_paymentId START WITH 14000;

CREATE TABLE Payments (
	paymentId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_Payments_paymentId'),
	accountId bigint not null,  -- FK to Invoices.invoiceId against which payment is being made
	cachedTotal int DEFAULT(0),
	currencyTypeId int not null,  -- FK to CurrencyTypes.currencyTypeId
        stateId int not null DEFAULT(1), -- FK to PaymentStates.paymentStateId (Pending, Credited)
        createdDateTime timestamp not null default now(),
	holdingDateTime timestamp not null default current_date + interval '1 month', -- reflect "holding" period
	transactionId bigint,  -- FK to FinancialTransactions.financialTransactionId
	
	CONSTRAINT fk_Payments_accountId FOREIGN KEY
	(accountId) REFERENCES Accounts(accountId),
	
	CONSTRAINT fk_Payments_currencyTypeId FOREIGN KEY
	(currencyTypeId) REFERENCES CurrencyTypes(currencyTypeId),
	
	CONSTRAINT fk_Payments_stateId FOREIGN KEY
	(stateId) REFERENCES PaymentStates(paymentStateId),

	CONSTRAINT fk_Payments_transactionId FOREIGN KEY
	(transactionId) REFERENCES FinancialTransactions(financialTransactionId)

);

CREATE INDEX ix_Payments_accountId ON Payments(accountId);
CREATE INDEX ix_Payments_currencyTypeId ON Payments(currencyTypeId);
CREATE INDEX ix_Payments_stateId ON Payments(stateId);
CREATE INDEX ix_Payments_transactionId ON Payments(transactionId);


CREATE SEQUENCE seq_PaymentLineItems_paymentLineItemId START WITH 16000;

CREATE TABLE PaymentLineItems (
	paymentLineItemId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_PaymentLineItems_paymentLineItemId'),
	paymentId bigint not null,  -- FK to Payments.paymentId
	lineNumber int,  -- display order and numbering
        quantity int not null default(1),
        amountTotal int not null default(0),
	description text,
	publishId bigint,  -- FK to Published.publishId (or null if something else)
	
	CONSTRAINT fk_PaymentLineItems_publishId FOREIGN KEY
	(publishId) REFERENCES Published(publishId)
);

CREATE INDEX ix_PaymentLineItems_paymentId ON PaymentLineItems(paymentId);
CREATE INDEX ix_PaymentLineItems_publishId ON PaymentLineItems(publishId);




CREATE TABLE AccountProjectNumbers (
	accountId bigint not null,  -- FK to Accounts.accountId for account which made the purchase/license
	publishId bigint not null,  -- FK to Published.publishId
	invoiceLineItemId bigint,  -- FK to InvoiceLineItems.invoiceLineItemId if this project was purchased
	retired int default 0,  -- 0=not
	retiredById text,  -- FK to App_User.Id (aka Members.memberId)
	retiredDateTime timestamp,
	retiredReasonId int,  -- FK to RetiredReasonTypes.retiredReasonTypeId
	
	CONSTRAINT fk_AccountProjectNumbers_accountId FOREIGN KEY
	(accountId) REFERENCES Accounts(accountId),
	
	CONSTRAINT fk_AccountProjectNumbers_publishId FOREIGN KEY
	(publishId) REFERENCES Published(publishId),
	
	CONSTRAINT fk_AccountProjectNumbers_invoiceLineItemId FOREIGN KEY
	(invoiceLineItemId) REFERENCES InvoiceLineItems(invoiceLineItemId),
	
	CONSTRAINT fk_AccountProjectNumbers_retiredById FOREIGN KEY
	(retiredById) REFERENCES App_User(Id),
	
	CONSTRAINT fk_AccountProjectNumbers_retiredReasonId FOREIGN KEY
	(retiredReasonId) REFERENCES RetiredReasonTypes(retiredReasonTypeId)
);

CREATE INDEX ix_AccountProjectNumbers_accountId ON AccountProjectNumbers(accountId);
CREATE INDEX ix_AccountProjectNumbers_publishId ON AccountProjectNumbers(publishId);
CREATE INDEX ix_AccountProjectNumbers_invoiceLineItemId ON AccountProjectNumbers(invoiceLineItemId);
CREATE INDEX ix_AccountProjectNumbers_retiredById ON AccountProjectNumbers(retiredById);
CREATE INDEX ix_AccountProjectNumbers_retiredReasonId ON AccountProjectNumbers(retiredReasonId);


-------------------------------------------------------------------------------
-- Update database version.
-------------------------------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES(1);

-------------------------------------------------------------------------------
-- End transaction wrapper for this update.
-------------------------------------------------------------------------------
COMMIT;
