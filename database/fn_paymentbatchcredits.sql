
-- fn_paymentbatchcredits.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
---------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Payments' 
    AND (functionName='CREDITS_CREATE' OR functionName='BATCH_TRANSACTIONS_CREATE' OR functionName='BATCH_STATE_UPDATE');

select * from fn_TableFunctionRoleMap_insert('Payments','CREDITS_CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','CREDITS_CREATE','ROLE_TYPE_COMMERCE_CRON');

select * from fn_TableFunctionRoleMap_insert('Payments','BATCH_TRANSACTIONS_CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','BATCH_TRANSACTIONS_CREATE','ROLE_TYPE_COMMERCE_CRON');

select * from fn_TableFunctionRoleMap_insert('Payments','BATCH_STATE_UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Payments','BATCH_STATE_UPDATE','ROLE_TYPE_COMMERCE_CRON');



-- Function: fn_payment_credits_create(integer)

DROP TYPE IF EXISTS _type_payment_batch_credit_create_ CASCADE;

CREATE TYPE _type_payment_batch_credit_create_ As (
    paymentId bigint,
    accountId bigint,
    currencyTypeId int,
    cachedTotal int
);

CREATE OR REPLACE FUNCTION fn_payment_credits_create(in_credentials_email text, in_credentials_password text, in_holdingDays integer)
  RETURNS SETOF _type_payment_batch_credit_create_ AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Payments';
        var_functionName text := 'CREDITS_CREATE';

        invoice_state_charged int;
        invoice_state_credits_created int;
        var_accountId bigint;
        var_currencyTypeId int;
        var_newPaymentId bigint := -1;
        var_count int := -1;
        var_holdingDaysInterval text;

        rec _type_payment_batch_credit_create_%ROWTYPE;

    BEGIN
        var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN

            invoice_state_charged := invoiceStateId FROM InvoiceStates WHERE tag = 'invoice_state_charged';
            invoice_state_credits_created := invoiceStateId FROM InvoiceStates WHERE tag = 'invoice_state_credits_created';

            var_holdingDaysInterval := CAST((COALESCE(in_holdingDays,0) + 1) As text) || ' days';

            -- NOTE that temp table below is NOT the same as the function return type
            BEGIN
                CREATE TEMPORARY TABLE tmp_tbl_create_payment_batch_credits (
                    currencyTypeId int,    -- Invoices
                    invoiceId bigint,      -- InvoiceLineItems
                    invoiceLineItemId bigint,
                    projectId varchar(255),
                    quantity int,
                    selfPrice int,
                    accountId bigint,       -- Projects
                    paymentId bigint,       -- will create FinancialTransactions, Payments, and PaymentLineItems entries
                    paymentLineItemId bigint
                );
            EXCEPTION
                WHEN DUPLICATE_TABLE THEN
                    TRUNCATE TABLE tmp_tbl_create_payment_batch_credits;
            END;

            -- get list of apps and gadgets from invoices which have been charged but not yet credited
            INSERT INTO tmp_tbl_create_payment_batch_credits
            SELECT 
                  inv.currencyTypeId
                , invli.invoiceId
                , invli.invoiceLineItemId
                , invli.projectId
                , invli.quantity
                , prj.selfPrice, prj.accountId
            FROM Invoices inv, InvoiceLineItems invli, Projects prj
            WHERE inv.stateId = invoice_state_charged
            AND inv.invoiceId = invli.invoiceId
            AND invli.projectId = prj.projectId;

            -- update with child gadgets of invoiced apps and gadgets per Parent_Child table
            INSERT INTO tmp_tbl_create_payment_batch_credits
            SELECT tmp.currencyTypeId, tmp.invoiceId, tmp.invoiceLineItemId, prj.projectId, tmp.quantity, prj.selfPrice, prj.accountId
            FROM tmp_tbl_create_payment_batch_credits tmp
            JOIN Parent_Child pc ON pc.parent_children_id = tmp.projectId 
            JOIN Projects prj ON (pc.child_id = prj.projectId AND prj.selfPrice IS NOT NULL);

            -- create new Payments table entries for every distinct account+currency combination
            FOR rec IN
                SELECT DISTINCT 0, accountId, currencyTypeId FROM tmp_tbl_create_payment_batch_credits
            LOOP
                 -- create Payments entry
                INSERT INTO Payments(accountId, currencyTypeId, holdingDateTime)
                SELECT rec.accountId, rec.currencyTypeId, current_date + var_holdingDaysInterval::interval;

                var_newPaymentId := currval('seq_Payments_paymentId');

                -- save into our temp table
                UPDATE tmp_tbl_create_payment_batch_credits
                SET paymentId = var_newPaymentId
                WHERE accountId = rec.accountId
                AND currencyTypeId = rec.currencyTypeId;

            END LOOP;

            -- reset PaymentLineItems.lineNumber sequence so values never exceed integer maxsize
            PERFORM setval('seq_PaymentLineItems_lineNumber', 1, false);

            -- create PaymentLineItems entries for every row in our temp table
            INSERT INTO PaymentLineItems(paymentId, quantity, amountTotal, projectId, description)
            SELECT tmp.paymentId, SUM(tmp.quantity), SUM(tmp.selfPrice), tmp.projectId, prj.publishedName || ' '|| prj.versionMajor || '.' || prj.versionMinor || '.' || prj.versionDot || '-' || prj.versionBuildNumber
            FROM tmp_tbl_create_payment_batch_credits tmp, Projects prj
            WHERE tmp.projectId = prj.projectId
            GROUP BY tmp.paymentId, tmp.projectId, prj.publishedName, prj.versionMajor, prj.versionMinor, prj.versionDot, prj.versionBuildNumber
            ORDER BY tmp.paymentId; -- need ORDER BY to ensure lineNumber sequence applied sequentially to related line items

            -- get min lineNumbers for each paymentId and use to adjust to begin with 1 for each paymentId
            BEGIN
                CREATE TEMPORARY TABLE tmp_tbl_min_line_numbers (
                    paymentId bigint,
                    minLineNumber integer
                );
            EXCEPTION
                WHEN DUPLICATE_TABLE THEN
                    TRUNCATE TABLE tmp_tbl_min_line_numbers;
            END;

            INSERT INTO tmp_tbl_min_line_numbers
            SELECT pli.paymentId, MIN(lineNumber)
            FROM PaymentLineItems pli, tmp_tbl_create_payment_batch_credits tmp
            WHERE pli.paymentId = tmp.paymentId
            GROUP BY pli.paymentId;

            UPDATE PaymentLineItems
            SET lineNumber = lineNumber - minLineNumber + 1
            FROM tmp_tbl_min_line_numbers tmp
            WHERE PaymentLineItems.paymentId = tmp.paymentId;

            -- update invoices status to credits created
            UPDATE Invoices
            SET stateId = invoice_state_credits_created
            WHERE invoiceId IN (SELECT invoiceId FROM tmp_tbl_create_payment_batch_credits);

            -- update payments cachedTotal
            UPDATE Payments
            SET cachedTotal = (SELECT sum(payli.amountTotal) FROM PaymentLineItems payli WHERE payli.paymentId = Payments.paymentId)
            WHERE Payments.paymentId in (SELECT paymentId FROM tmp_tbl_create_payment_batch_credits);

            -- return summary aggregate list of payments created ordered by accountId and currencyTypeId
            FOR rec IN
                SELECT pay.paymentId, pay.accountId, pay.currencyTypeId, pay.cachedTotal
                FROM tmp_tbl_create_payment_batch_credits tmp, Payments pay
                WHERE tmp.paymentId = pay.paymentId
                ORDER BY pay.accountId, pay.currencyTypeId, pay.paymentId
            LOOP
                RETURN NEXT rec;
            END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_payment_credits_create(integer) OWNER TO postgres;



---------------------------------------------------------------------
-- Function: fn_payment_batch_transactions_create(integer)

DROP TYPE IF EXISTS _type_payment_batch_transactions_create_ CASCADE;

CREATE TYPE _type_payment_batch_transactions_create_ As (
    accountId bigint,
    email text,
    currencyTypeId int,
    totalCredit int,
    transactionId bigint,
    transactionLabel text
);

CREATE OR REPLACE FUNCTION fn_payment_batch_transactions_create(in_credentials_email text, in_credentials_password text, in_accountId integer)
  RETURNS SETOF _type_payment_batch_transactions_create_ AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Payments';
        var_functionName text := 'BATCH_TRANSACTIONS_CREATE';

        payment_state_pending int;
        var_accountId bigint;
        var_currencyTypeId int;
        var_newTransactionId bigint := -1;
        var_count int := -1;

        rec _type_payment_batch_transactions_create_%ROWTYPE;

    BEGIN
        var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN

            payment_state_pending := paymentStateId FROM PaymentStates WHERE tag = 'payment_state_pending';

            -- NOTE that temp table below is NOT the same as the function return type
            BEGIN
                CREATE TEMPORARY TABLE tmp_tbl_create_payment_credit_transactions (
                    accountId bigint,
                    email text,
                    currencyTypeId int,
                    paymentId bigint,
                    cachedTotal int, -- not used since aggregate of all payment line items will be returned
                    transactionId bigint,
                    transactionLabel text
                );
            EXCEPTION
                WHEN DUPLICATE_TABLE THEN
                    TRUNCATE TABLE tmp_tbl_create_payment_credit_transactions;
            END;

            -- get list of accounts in good standing with payments pending and at or beyond the holding date
            INSERT INTO tmp_tbl_create_payment_credit_transactions
            SELECT Payments.accountId, App_User.email, currencyTypeId, paymentId, cachedTotal
            FROM Payments, Accounts, AccountMemberRoles, App_User
            WHERE (in_accountId <= 0 OR Payments.accountId = in_accountId)
            AND Payments.accountId = Accounts.accountId
            AND Accounts.goodStanding = 1
            AND Accounts.accountId = AccountMemberRoles.accountId
            AND AccountMemberRoles.typeId = 1 -- Account Administator
            AND AccountMemberRoles.memberId = App_User.id
            AND stateId = payment_state_pending
            AND holdingDateTime <= current_date;

            -- create new FinancialTransactions table entries for every distinct account+currency combination
            FOR rec IN
                SELECT DISTINCT accountId, '', currencyTypeId FROM tmp_tbl_create_payment_credit_transactions
            LOOP
                INSERT INTO FinancialTransactions(accountId, currencyTypeId)
                VALUES (rec.accountId, rec.currencyTypeId);

                var_newTransactionId := currval('seq_FinancialTransactions_financialTransactionId');

                UPDATE tmp_tbl_create_payment_credit_transactions
                SET transactionId = var_newTransactionId
                WHERE accountId = rec.accountId
                AND currencyTypeId = rec.currencyTypeId;

            END LOOP;

            -- update Payments with new transaction Ids and cached totals
            UPDATE Payments
            SET transactionId = tmp.transactionId, cachedTotal = (
            	select sum(amountTotal)
				from PaymentLineItems
				where PaymentLineItems.paymentId = Payments.paymentId
			)
            FROM tmp_tbl_create_payment_credit_transactions tmp
            WHERE Payments.paymentId = tmp.paymentId
            AND Payments.accountId = tmp.accountId;

            -- return aggregate list of credits to process ordered by accountId and currencyTypeId
            FOR rec IN
                SELECT tmp.accountId, tmp.email, tmp.currencyTypeId, SUM(amountTotal) As totalCredit, tmp.transactionId, label As transactionLabel
                FROM tmp_tbl_create_payment_credit_transactions tmp, Payments, PaymentLineItems, FinancialTransactions
                WHERE tmp.transactionId = Payments.transactionId
                AND Payments.transactionId = FinancialTransactionId
                AND Payments.paymentId = PaymentLineItems.paymentId
                GROUP BY tmp.accountId, tmp.email, tmp.currencyTypeId, tmp.transactionId, label
                ORDER BY tmp.accountId, tmp.currencyTypeId
            LOOP
                RETURN NEXT rec;
            END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_payment_batch_transactions_create(integer) OWNER TO postgres;



CREATE OR REPLACE FUNCTION fn_payment_batch_state_update(in_credentials_email text, in_credentials_password text, in_transactionId integer, in_state text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Payments';
        var_functionName text := 'BATCH_STATE_UPDATE';

        var_stateId int;
        var_count int = -1;

    BEGIN
        var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN

            var_stateId := invoiceStateId FROM InvoiceStates WHERE tag ILIKE in_state;

            UPDATE Invoices SET stateId = var_stateId WHERE transactionId = in_transactionId;

            GET DIAGNOSTICS var_count = Row_Count;

        END IF;

        RETURN var_count;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_payment_batch_state_update(text, text, integer, text) OWNER TO postgres;
