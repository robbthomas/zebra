-------------------------------------------------------------------------------
-- r3_to_r4.sql
--
-- Build database tables-functions-roles mapping table and function to add
-- new mappings.
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (4);
-------------------------------------------------------------------------------

CREATE TABLE TableFunctionRoleMap (
    tableName text,
    functionName text,
    roleTypeTag text,
    roleTypeId int,

    CONSTRAINT fk_TableFunctionRoleMap_roleTypeId FOREIGN KEY
    (roleTypeId) REFERENCES RoleTypes(roleTypeId)
);

CREATE UNIQUE INDEX ix_TableFunctionRoleMap_all ON TableFunctionRoleMap(tableName,functionName,roleTypeTag,roleTypeId);
CREATE INDEX ix_TableFunctionRoleMap_tableName ON TableFunctionRoleMap(tableName);
CREATE INDEX ix_TableFunctionRoleMap_functionName ON TableFunctionRoleMap(functionName);
CREATE INDEX ix_TableFunctionRoleMap_roleTypeTag ON TableFunctionRoleMap(roleTypeTag);
CREATE INDEX ix_TableFunctionRoleMap_roleTypeId ON TableFunctionRoleMap(roleTypeId);

CREATE OR REPLACE FUNCTION fn_TableFunctionRoleMap_insert(in_tableName text, in_functionName text, in_roleType text)
RETURNS integer AS
$BODY$
    DECLARE
        var_count int;

    BEGIN
        INSERT INTO TableFunctionRoleMap 
        VALUES (in_tableName,in_functionName,in_roleType,(SELECT roleTypeId FROM RoleTypes WHERE tag=in_roleType));
        
        GET DIAGNOSTICS var_count := row_count;

        RETURN var_count;
    END;
$BODY$
LANGUAGE 'plpgsql';

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
