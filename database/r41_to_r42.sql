-------------------------------------------------------------------------------
-- r41_to_r42.sql
--
-- Splits gadgets where the same gadget is used in more than one Published record
-- keeping in mind GadgetStates and preserves gadgetId of most recent Published record
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (42);
-------------------------------------------------------------------------------

create temporary table DuplicatePublished (
	publishId int8,
	projectId text,
	newProjectId text,
	gadgetStateId text,
	newGadgetStateId text,
	ok   	   boolean
);


-- find gadgets that have been published twice
insert into DuplicatePublished
select pub.publishid, p.id, null, gs.id, null, false
	from published pub
	inner join parent p on p.id = pub.projectId
	inner join published pub2 on p.id = pub2.projectid and pub.publishid <> pub2.publishid
	left join gadget_state gs on gs.root_gadget_id = p.id;


select * from DuplicatePublished;

-- mark the newest ones as "don't mess with it"
update DuplicatePublished set ok = true
	from (
		select max(publishid) as publishId, projectId from DuplicatePublished
			group by projectId
		) as GoodPublish
	where GoodPublish.publishId = DuplicatePublished.publishId;


select * from DuplicatePublished;


update DuplicatePublished set newProjectId = replace(cast(uuid_generate_v4() As text),'-','') where ok = false;
update DuplicatePublished set newGadgetStateId = replace(cast(uuid_generate_v4() As text),'-','') where ok = false and gadgetStateid is not null;

select * from DuplicatePublished;

-- create a table of gadgets cloned from the "not okay" list in DupPubGad.
-- give them new ids
select p.*, d.publishId into temporary table NewGadgets
from DuplicatePublished d
	inner join parent p on d.projectId = p.id and d.ok = false;

select id, nextVersionId, publishId from NewGadgets;

-- set up join from NewGadgets to original gadgets
update NewGadgets n set nextVersionId = d.projectId
	from  DuplicatePublished d where n.id = d.projectId and d.ok = true;

update NewGadgets n set id = d.newProjectId
	from  DuplicatePublished d where n.publishId = d.publishId;


select id, nextVersionId, publishId from NewGadgets;

--- do much of the same for gadget_state
select gs.*, d.publishId into temporary table NewGadgetState
from DuplicatePublished d
	inner join gadget_state gs on d.gadgetStateId = gs.id and d.ok = false;

select id, root_gadget_id, publishId from NewGadgetState;

update NewGadgetState n set id = d.newGadgetStateId,
		root_gadget_id = d.newProjectId
	from  DuplicatePublished d where n.publishid = d.publishId;


select id, root_gadget_id, publishId from NewGadgetState;



-- aaand, here we go!
alter table NewGadgets drop column publishId;
insert into parent (select * from NewGadgets);

alter table NewGadgetState drop column publishId;
insert into gadget_state (select * from NewGadgetState);

update Published p set projectId = d.newProjectId
	from DuplicatePublished d 
	where p.publishId = d.publishId 
		and d.ok = false ;



drop table NewGadgets;
drop table NewGadgetState;
drop table DuplicatePublished;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

