--------------------------------------------------------------------------------
-- fn_zappcomments.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- ZappComments functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='ZappComments'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('ZappComments','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ZappComments','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ZappComments','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ZappComments','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('ZappComments','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ZappComments','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_zappcomment_create(text, text, integer, text, text, integer, text)

-- DROP FUNCTION fn_zappcomment_create(text, text, integer, text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_zappcomment_create(in_credentials_email text, in_credentials_password text, in_projectId text, in_memberId text, in_comment text, in_helpfulCount integer, in_feedbackCount integer)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ZappComments';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_zappCommentId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO ZappComments(
            projectId,
            memberId,
            comment,
            helpfulCount,
            feedbackCount
        )
        VALUES(
            in_projectId,
            in_memberId,
            in_comment,
            in_helpfulCount,
            in_feedbackCount
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_zappCommentId := currval('seq_ZappComments_zappCommentId');
        END IF;

        return new_zappCommentId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_zappcomment_create(text, text, integer, text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zappcomment_update(text, text, integer, integer, text, text, integer, text, text)

-- DROP FUNCTION fn_zappcomment_update(text, text, integer, integer, text, text, integer, text, text);

CREATE OR REPLACE FUNCTION fn_zappcomment_update(in_credentials_email text, in_credentials_password text, in_zappCommentId integer, in_projectId text, in_memberId text, in_comment text, in_helpfulCount integer, in_feedbackCount integer, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ZappComments';
        var_functionName text := 'UPDATE';

        rec ZappComments%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from ZappComments where zappCommentId = in_zappCommentId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_projectId' THEN
                rec.projectId := in_projectId;
            ELSEIF param_name = 'in_memberId' THEN
                rec.memberId := in_memberId;
            ELSEIF param_name = 'in_comment' THEN
                rec.comment := in_comment;
            ELSEIF param_name = 'in_helpfulCount' THEN
                rec.helpfulCount := in_helpfulCount;
            ELSEIF param_name = 'in_feedbackCount' THEN
                rec.feedbackCount := in_feedbackCount;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE ZappComments SET
            projectId = rec.projectId,
            memberId = rec.memberId,
            comment = rec.comment,
            helpfulCount = rec.helpfulCount,
            feedbackCount = rec.feedbackCount,
            updatedDateTime = now()
        WHERE zappCommentId = in_zappCommentId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_zappcomment_update(text, text, integer, integer, text, text, integer, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zappcomment_retire(text, text, integer, text)

-- DROP FUNCTION fn_zappcomment_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_zappcomment_retire(in_credentials_email text, in_credentials_password text, in_zappCommentId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ZappComments';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE ZappComments SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE zappCommentId = in_zappCommentId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN in_zappCommentId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_zappcomment_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zappcomment_filter(text, text, integer, integer, text, integer, text, text, integer, text, text)

-- DROP FUNCTION fn_zappcomment_filter(text, text, integer, integer, text, integer, text, text, integer, text, text);

CREATE OR REPLACE FUNCTION fn_zappcomment_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_projectId text, in_memberId text, in_comment text, in_helpfulCount integer, in_feedbackCount integer, in_filter_list text)
  RETURNS SETOF ZappComments AS
$BODY$
    DECLARE
--        var_credentialsId text;
        var_tableName text := 'ZappComments';
        var_functionName text := 'FILTER';

        rec ZappComments%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
--         var_query text := 'SELECT * FROM ZappComments WHERE retired = 0';
        
        var_query text  :=              'SELECT zappcomments.* from projects ';
    BEGIN
        var_query       := var_query || ' p1 inner join projects p2 on p2.publishedname = p1.publishedname';
        var_query       := var_query || ' inner join zappcomments on zappcomments.projectid = p2.projectid';
        var_query       := var_query || ' where zappcomments.retired = 0';

--        var_credentialsId := id FROM App_User WHERE (in_credentials_email ILIKE email OR in_credentials_email = displayName)
--            AND passwordHash = encode(digest(in_credentials_password || COAL  ESCE(passwordSalt,''),'sha1'),'hex');

--        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_Id' THEN
            ELSEIF filter_name = 'in_projectId' THEN
--                        where p1.projectid = in_projectId;
                var_query := var_query || ' AND p1.projectId = ''' || in_projectId || '''';
            ELSEIF filter_name = 'in_memberId' THEN
                var_query := var_query || ' AND zappcomments.memberId ILIKE ''%' || in_memberId || '%''';
            ELSEIF filter_name = 'in_comment' THEN
                var_query := var_query || ' AND zappcomments.comment ILIKE ''%' || in_comment || '%''';
            ELSEIF filter_name = 'in_helpfulCount' THEN
                var_query := var_query || ' AND zappcomments.helpfulCount = ' || in_helpfulCount;
            ELSEIF filter_name = 'in_feedbackCount' THEN
                var_query := var_query || ' AND zappcomments.feedbackCount = ' || in_feedbackCount;
            ELSEIF filter_name = 'in_createdDateTime' THEN
            ELSEIF filter_name = 'in_updatedDateTime' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM ZappComments;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

--        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_zappcomment_filter(text, text, integer, integer, text, integer, text, text, integer, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zappcomment_find(text, text, integer)

-- DROP FUNCTION fn_zappcomment_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_zappcomment_find(in_credentials_email text, in_credentials_password text, in_zappCommentId integer)
  RETURNS SETOF ZappComments AS
$BODY$
    DECLARE
--        var_credentialsId text;
        var_tableName text := 'ZappComments';
        var_functionName text := 'FIND';

        rec ZappComments%ROWTYPE;

    BEGIN
--        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

--        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM ZappComments
                WHERE zappCommentId = in_zappCommentId
            LOOP
                RETURN NEXT rec;
            END LOOP;
--        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_zappcomment_find(text, text, integer) OWNER TO postgres;



--------------------------------------------------------------------------------
-- Function: fn_zappcomment_countCommentsForprojectId(text, text, integer)

-- DROP FUNCTION fn_zappcomment_find(text, text, integer);

-- MySQL has a nice way to integrate this call with a call that uses LIMIT.
-- http://stackoverflow.com/questions/156114/best-way-to-get-result-count-before-limit-was-applied-in-php-postgres

CREATE OR REPLACE FUNCTION fn_zappcomment_countCommentsForprojectId(in_credentials_email text, in_credentials_password text, in_projectId text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_commentsCount int;
--        var_credentialsId text;
    BEGIN


--    var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

--        IF var_credentialsId IS NULL THEN
--            RETURN -1;
--        END IF;

        var_commentsCount := count(*) FROM
            projects p1
            inner join projects p2 on p2.publishedname = p1.publishedname
            inner join zappcomments on zappcomments.projectid = p2.projectid
            where p1.projectid = in_projectId
            and zappcomments.retired = 0;
 
        return var_commentsCount;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
-- ALTER FUNCTION fn_zappcomment_find(text, text, integer) OWNER TO postgres;
