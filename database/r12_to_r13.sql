-------------------------------------------------------------------------------
-- r12_to_r13.sql
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (13);
-------------------------------------------------------------------------------

-- Create accounts for all Beta users -- move all users fron accountId=1
-- to their own accounts as account admins EXCEPT for blackbeard.

CREATE FUNCTION tmpfn_create_accounts_for_beta_users()
RETURNS integer AS
$BODY$
    DECLARE
        cnt int := 0;
        rec App_User%ROWTYPE;

        index int;
        var_uuid text;
        new_merchantCustomerId text;
        new_accountId int;
        var_count int := -1;

    BEGIN
        FOR rec IN 
            SELECT * FROM App_User WHERE accountId = 1 AND email NOT ILIKE '%@alleni.com'
        LOOP
            -- generate an as yet unused random UUID-based but only 20 chars long merchantCustomerId
            -- for new entry but try only 12 times before bailing and failing!?
            index := 1; -- postgresql strings first char position=1
            var_uuid := replace(cast(uuid_generate_v4() As text),'-','');
            LOOP
                new_merchantCustomerId := upper(substring(var_uuid from index for 20));
                IF NOT EXISTS (select merchantCustomerId from Accounts where merchantCustomerId = new_merchantCustomerId) THEN
                    EXIT;
                END IF;
                index := index + 1;
                IF index > 12 THEN -- only have 12 20-char substrings in 32-char uuid derived string
                    RETURN -1;
                END IF;
            END LOOP;

            -- create entries manually since we don't know the passwords to use in credentials function params
            -- when calling the db functions which normally do all this
            INSERT INTO Accounts(typeId, termDueDateTime, merchantCustomerId, editedById)
            VALUES (2, '9/1/2011', new_merchantCustomerId, rec.id);

            GET DIAGNOSTICS var_count = Row_Count;
            IF var_count = 1 THEN
                new_accountId := currval('seq_Accounts_accountId');
            ELSE
                RETURN -2;
            END IF;

            UPDATE App_User SET accountId = new_accountId WHERE id = rec.id;

            DELETE FROM AccountMemberRoles WHERE memberId = rec.id;

            INSERT INTO AccountMemberRoles(accountId, memberId, typeId)
            VALUES (new_accountId, rec.id, 1);

            UPDATE Published SET accountId = new_accountId
            FROM Parent
            WHERE accountId = 1
            AND Published.projectId = Parent.id
            AND Parent.ownerId = rec.id;

            UPDATE AccountProjectNumbers SET accountId = new_accountId
            WHERE publishId IN (SELECT publishId FROM Published WHERE accountId = new_accountId);

            cnt := cnt + 1;
        END LOOP;

        RETURN cnt;
    END;
$BODY$
LANGUAGE 'plpgsql';

SELECT * FROM tmpfn_create_accounts_for_beta_users();

DROP FUNCTION tmpfn_create_accounts_for_beta_users();


-- create password reset dedicated sys admin user
SELECT * FROM fn_appuser_create('','','pswdAdmin','Dedicated password reset webservice system administrator user','info@alleni.com','','','o5vBnE5Q22','salt9876','');

INSERT INTO SystemAdministrators(memberId, passwordHash, passwordSalt)
VALUES ((SELECT id FROM App_User WHERE displayName='pswdAdmin' LIMIT 1), encode(digest('o5vBnE5Q33' || 'salt8765','sha1'),'hex'), 'salt8765');


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
