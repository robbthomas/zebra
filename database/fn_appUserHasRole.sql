/*

    Name: fn_AppUserHasRole.sql
    Author: Joe Bodell
    Date: 11/07/2012
    Description: fn_AppUserHasRole() takes an appUserId and a roleTypeName, and returns a boolean indicating whether the user has that role. Pretty straightforward
    
*/


CREATE OR REPLACE FUNCTION fn_AppUserHasRole (
      in_appUserId VARCHAR(255)
    , in_roleTypeTag TEXT
) RETURNS INT4 AS $$ 
    DECLARE hasRole BOOLEAN;
    BEGIN

        SELECT COUNT(*) INTO hasRole
        FROM app_user
            INNER JOIN accountMemberRoles
                ON app_user.id = accountMemberRoles.memberId
                INNER JOIN roleTypes
                    ON accountMemberRoles.typeId = roleTypes.roleTypeId
        WHERE app_user.id = in_appUserId
        AND roleTypes.tag = in_roleTypeTag
        ;
        return hasRole;
    END
$$ LANGUAGE 'plpgsql' VOLATILE
COST 100;
