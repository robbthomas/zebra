-------------------------------------------------------------------------------
-- r31_to_r32.sql
--   add metadata column to Published.  Some extra work is done to regenerate
--   the autohistory table.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (33);
-------------------------------------------------------------------------------
-- Add new column.
ALTER TABLE Published ADD COLUMN Metadata varchar(1024);

-- Save existing autohistory.
SELECT * INTO TEMPORARY TABLE temp_Published_Autohistory FROM Published_Autohistory;

-- Recreate autohistory for Published.
DROP SEQUENCE seq_Published_AutoHistory CASCADE;
DROP TABLE Published_AutoHistory CASCADE;

CREATE SEQUENCE seq_Published_AutoHistory;
CREATE TABLE Published_AutoHistory (
    LIKE Published,
    Published_AutoHistory_Id bigint DEFAULT nextval('seq_Published_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_Published_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO Published_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

-- Restore saved autohistory.
INSERT INTO Published_Autohistory
(
    publishId,accountId,projectId,firstPublishId,versionMajor,versionMinor,versionDot,versionBuildNumber,
    categoryId,name,description,thumbnailId,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,
    editedById,retired,retiredById,retiredDateTime,screenshotId,hideInStoreList,autohistorydate
)
SELECT
    publishId,accountId,projectId,firstPublishId,versionMajor,versionMinor,versionDot,versionBuildNumber,
    categoryId,name,description,thumbnailId,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,
    editedById,retired,retiredById,retiredDateTime,screenshotId,'f',autohistorydate
FROM temp_Published_Autohistory;

DROP TABLE temp_Published_Autohistory;
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;


