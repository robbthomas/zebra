-------------------------------------------------------------------------------
-- r28_to_r29.sql
--
-- regenerate FinancialTransactions_AutoHistory table
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (29);
-------------------------------------------------------------------------------

DROP TABLE IF EXISTS FinancialTransactions_AutoHistory CASCADE;

CREATE TABLE FinancialTransactions_AutoHistory (
    LIKE FinancialTransactions,
    FinancialTransactions_AutoHistory_Id bigint DEFAULT nextval('seq_FinancialTransactions_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);




-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
