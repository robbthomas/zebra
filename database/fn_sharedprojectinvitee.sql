--------------------------------------------------------------------------------
-- fn_sharedprojectinvitees.sql
--
-- Allen Interactions Copyright 2011
-- 
--  DELETE all obsolete functions that refer to renamed table
--
--------------------------------------------------------------------------------
-- SharedProjectInvitees functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='SharedProjectInvitees'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');



--------------------------------------------------------------------------------
-- Function: fn_sharedprojectinvitee_create(text, text, integer, text, text, timestamp, integer, text, text, text, text)

DROP FUNCTION if exists fn_sharedprojectinvitee_create(text, text, integer, text, text, timestamp, integer, text, text, text, text);


--------------------------------------------------------------------------------
-- Function: fn_sharedprojectinvitee_update(text, text, integer, integer, text, text, timestamp, integer, text, text, text, text, text)

DROP FUNCTION if exists fn_sharedprojectinvitee_update(text, text, integer, integer, text, text, timestamp, integer, text, text, text, text, text);

--------------------------------------------------------------------------------
-- Function: fn_sharedprojectinvitee_retire(text, text, integer, text)

DROP FUNCTION if exists fn_sharedprojectinvitee_retire(text, text, integer, text);


--------------------------------------------------------------------------------
-- Function: fn_sharedprojectinvitee_filter(text, text, integer, integer, text, integer, text, text, timestamp, integer, text, text, text, text, text)

DROP FUNCTION if exists fn_sharedprojectinvitee_filter(text, text, integer, integer, text, integer, text, text, timestamp, integer, text, text, text, text, text);


--------------------------------------------------------------------------------
-- Function: fn_sharedprojectinvitee_find(text, text, integer)

DROP FUNCTION if exists fn_sharedprojectinvitee_find(text, text, integer);
