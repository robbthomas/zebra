import ProjectSource.{LookupProjectPublished, LookupProjectSource, PlainProjectSource}
import collection.mutable
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.Session
//import scala.collection._
import Common._
import Default1204._

object Upgrade1203Content {

  val hasValueChildren = Common.ValueFor("valueChildren")
  val hasProject = Common.ValueFor("project")

  def nextProject(session:Session) = {
    using(session) {
      join(Db1204.projectmigrationstaging, Db1204.projects)((pmt, p) =>
              where(pmt.migrationsuccess === None and pmt.migrationstarteddatetime === None)
              select(pmt, p)
              orderBy(pmt.id)
              on(pmt.projectid === p.projectid)
            ).page(0, 1).headOption
    }
  }

  def upgradeAll(session:Session, sessionSource:()=>Session) {
    var innerSession = sessionSource()
    while(true) {
      nextProject(session) match {
        case None => {
          println("All Done!")
          return
        }
        case Some((migrationTodo, project)) => {
          try {
            using(session) {
              update(Db1204.projectmigrationstaging)(pmt =>
                where(pmt.id === migrationTodo.id)
                set(pmt.migrationstarteddatetime := Some(new java.sql.Timestamp(new java.util.Date().getTime)))
              )
            }
            val (convertedProject, convertedState, convertedEvent) = upgrade(PlainProjectSource(project), innerSession, null, null, true)
            using(session) {
              update(Db1204.projectmigrationstaging)(pmt =>
                where(pmt.id === migrationTodo.id)
                set(
                  pmt.migrationsuccess := Some(true),
                  pmt.migrationcompletedatetime := Some(new java.sql.Timestamp(new java.util.Date().getTime)),
                  pmt.content := Some(convertedProject.content),
                  pmt.initialvalues := convertedProject.initialvalues,
                  pmt.currentvalues := convertedState.map(_.content),
                  pmt.eventprojectid := convertedEvent.map(_.projectid),
                  pmt.eventcontent := convertedEvent.map(_.content),
                  pmt.eventvalues := convertedEvent.flatMap(_.initialvalues),
                  pmt.width := Some(convertedProject.width),
                  pmt.height := Some(convertedProject.height)
                  )
              )
            }
          } catch {
            case t => {
              println("TRANSFER ERROR: " + t.toString)
              var t2 = t
              while(t2.getCause != null) {
                t2 = t2.getCause
                println("Caused by: " + t2.toString)
              }
              println(t2.getStackTraceString)
              using(session) {
                update(Db1204.projectmigrationstaging)(pmt =>
                  where(pmt.id === migrationTodo.id)
                  set(pmt.migrationsuccess := Some(false), pmt.migrationcompletedatetime := Some(new java.sql.Timestamp(new java.util.Date().getTime)))
                )
              }
              innerSession = sessionSource()
            }
          }
        }
      }
    }
  }

  def upgradeAndMigrate(sourceUser:String, projectName:String, source:Session, destUser:String, destination:Session) {use(List(source,destination)) {
    upgrade(LookupProjectSource(sourceUser, projectName), source, destUser, destination, false)
  }}
  def upgradeAndMigratePublished(publishedName:String, source:Session, destUser:String, destination:Session) {use(List(source,destination)) {
    upgrade(LookupProjectPublished(publishedName), source, destUser, destination, false)
  }}


  def upgrade (projectSource:ProjectSource.ProjectSource, source:Session, destUser:String, destination:Session, inPlace:Boolean):Tuple3[Projects1204, Option[GadgetState1204], Option[Projects1204]] = {

//    def fixId(objectId:String) = {
//      objectId.split(",").grouped(2).flatMap( _ match {
//        case Array(e1, e2) => Seq(e1, "pages", e2)
//        case Array(e) => Seq(e)
//      }).mkString(",")
//    }

    def fixId(objectId:List[Any]) = {
      objectId.grouped(2).flatMap( _ match {
        case List(e1, e2) => Seq(e1, "pages", e2)
        case List(e) => Seq(e)
        case List() => Seq()
        case _ => throw new Error("Impossible group length")
      }).toList
    }

    val hasMasterAndSlave = Common.ValuesFor("masterAnchor", "slaveAnchor")
    val hasIdAndModifier = Common.ValuesFor("id", "modifier")

    def gadgetWiring(wiring:List[Any]):List[Any] = wiring.flatMap( w => w match {
      case hasMasterAndSlave(master,slave) => (gadgetFixAnchor(master), gadgetFixAnchor(slave)) match {
        case (Some(fixedMaster), Some(fixedSlave)) => Some(Map("masterAnchor" -> fixedMaster, "slaveAnchor" -> fixedSlave))
      }
    })

    def gadgetFixAnchor(anchor:Any) = anchor match {
      case hasIdAndModifier(id:List[Any], mod:String) => Some(Map("id" -> fixId(id), "modifier" -> mod))
      case _ => throw new Error("Unable to resolve wire anchor: " + anchor)
    }

    def projectWiring(wiring:List[Any], eventSmartWireAnchors:List[SmartWireAnchor]):List[Any] = wiring.flatMap( w => w match {
          case hasMasterAndSlave(master,slave) => (projectFixAnchor(master), projectFixAnchor(slave)) match {
            case (Some(fixedMaster), Some(fixedSlave)) => Some(Map("masterAnchor" -> fixedMaster, "slaveAnchor" -> fixedSlave))
            case (Some(fixedMaster), None) => Some(Map("masterAnchor" -> fixedMaster, "slaveAnchor" -> matchingOuterAnchor(eventFixAnchor(slave).get, eventSmartWireAnchors)))
            case (None, Some(fixedSlave)) => Some(Map("masterAnchor" -> matchingOuterAnchor(eventFixAnchor(master).get, eventSmartWireAnchors), "slaveAnchor" -> fixedSlave))
            case (None, None) => None
          }
        })

    def projectFixAnchor(anchor:Any) = anchor match {
      case hasIdAndModifier(List("project"), _) => Some(anchor)
      case hasIdAndModifier(List("lms"), _) => Some(anchor)
      case hasIdAndModifier(_, _) => None
      case _ => throw new Error("Unable to resolve wire anchor: " + anchor)
    }

    def matchingInnerAnchor(anchor:Map[String,Any], eventSmartWireAnchors:List[SmartWireAnchor]) = Map(
      "id" -> List(),
      "modifier" -> eventSmartWireAnchors.find(_.dependentAnchor == (anchor.get("id").asList, anchor.get("modifier").asString)).get.innerKey
    )

    def matchingOuterAnchor(anchor:Map[String,Any], eventSmartWireAnchors:List[SmartWireAnchor]) = Map(
      "id" -> List("eventFlow", "pages", 0),
      "modifier" -> eventSmartWireAnchors.find(_.dependentAnchor == (anchor.get("id").asList, anchor.get("modifier").asString)).get.key
    )

    def eventWiring(wiring:List[Any], eventSmartWireAnchors:List[SmartWireAnchor]):List[Any] = wiring.flatMap( w => w match {
      case hasMasterAndSlave(master,slave) => (eventFixAnchor(master), eventFixAnchor(slave)) match {
        case (Some(fixedMaster), Some(fixedSlave)) => Some(Map("masterAnchor" -> fixedMaster, "slaveAnchor" -> fixedSlave))
        case (Some(fixedMaster), None) => Some(Map("masterAnchor" -> fixedMaster, "slaveAnchor" -> matchingInnerAnchor(fixedMaster, eventSmartWireAnchors)))
        case (None, Some(fixedSlave)) => Some(Map("masterAnchor" -> matchingInnerAnchor(fixedSlave, eventSmartWireAnchors), "slaveAnchor" -> fixedSlave))
        case (None, None) => None
      }
    })

    def eventFixAnchor(anchor:Any) = anchor match {
      case hasIdAndModifier(List("project"), _) => None
      case hasIdAndModifier(List("lms"), _) => None
      case hasIdAndModifier(id:List[Any], mod:String) => Some(Map("id" -> fixId(id), "modifier" -> mod))
      case _ => throw new Error("Unable to resolve wire anchor: " + anchor)
    }

    def makeEventSmartWireAnchors(oldContent:Map[String, Any], contentMap:Map[String, Map[String, Any]]) = oldContent.get("wiring").asList.flatMap(_ match {
      case hasMasterAndSlave(master:Map[String, Any],slave:Map[String, Any]) => (eventFixAnchor(master), eventFixAnchor(slave)) match {
        case (Some(_), Some(_)) => None // this wire will go entirely in the event
        case (Some(_), None) => Some(makeSmartWireAnchor(master, contentMap(master.get("id").asList.mkString(",")), master.getOrElse("modifier","").asInstanceOf[String], slave.getOrElse("modifier","").asInstanceOf[String]))
        case (None, Some(_)) => Some(makeSmartWireAnchor(slave, contentMap(slave.get("id").asList.mkString(",")), slave.getOrElse("modifier","").asInstanceOf[String], master.getOrElse("modifier","").asInstanceOf[String]))
        case (None, None) => None // this wire will go entirely outside the event
      }
    })

    case class SmartWireAnchor(dependentAnchor: (List[Any], String), key:String, innerKey:String, definition:Map[String, Any], values:Map[String, Any])

    case class AnchorForKey(key: String) {
      val hasKey = Common.ValueFor("key")
      val hasExternalId = Common.ValueFor("externalID")
      val hasPathNodeInfo = Common.ValueFor("pathNodeInfo")
      def unapply(map: Map[String,Any]): Option[Map[String,Any]] = map match {
        case hasKey(k) if(k == key) => Some(map)
        case hasExternalId(id) if("external-"+id == key) => Some(map)
        case hasPathNodeInfo(hasKey(k)) if(k == key) => Some(map)
        case _ => None
      }
    }

    def makeSmartWireAnchor(anchor:Map[String, Any], content:Map[String, Any], modifier:String, otherModifier:String) = {
      val internalId = Common.randomEditorUUID
      val externalId = Common.randomEditorUUID
      val hasCustomAnchors = Common.ValueFor("customAnchors")
      val matchingAnchor = AnchorForKey(modifier)
      val customType = content match {
        case hasCustomAnchors(anchors:List[Map[String, Any]]) => anchors.flatMap( _ match {
          case matchingAnchor(a) => a("kind") match {
            case "setter" => Some(("inlet", Map()))
            case "trigger" => Some(("outlet", Map()))
            case "calc" => Some(("property", Map("valueType" -> "string", "valueConstraints" -> null, "writable" -> false)))
            case "var" => Some(("property", Map("valueType" -> "string", "valueConstraints" -> null, "writable" -> true)))
            case "style" => Some(("property", Map("valueType" -> "string", "valueConstraints" -> null, "writable" -> true)))
            case "edge" => Some((a.get(modifier).asMap.getOrElse("type", "property"), a.get(modifier).asMap))
            case "pathNode" => Some(("property", Map("valueType" -> "position", "valueConstraints" -> null, "writable" -> true)))
            case _ => throw new Error("Unknown anchor type " + a)
          }
          case _ => None
        }).headOption
        case _ => None
      }

      val modifierType = customType.getOrElse(
        if(Set("sendValues", "score_nudgeUp", "score_nudgeDown", "restart", "quit").contains(otherModifier)) {
          ("outlet", Map())
        } else if(Set("sendSuccess", "sendFailure", "onRun").contains(otherModifier)) {
          ("inlet", Map())
        } else {
          ("property", Map("valueType" -> "string", "valueConstraints" -> null, "writable" -> true))
        }
      )

      val modifierMap = Map(
        "label" -> "Custom",
        "description" -> "",
        "category" -> "CUSTOM",
        "weight" -> 0,
        "type" -> modifierType._1
      ) ++ modifierType._2

      SmartWireAnchor(
        (fixId(anchor.get("id").asList), modifier),
        "external-" + externalId,
        "internal-" + internalId,
        Map(
          "kind" -> "edge",
          "internalID" -> internalId,
          "externalID" -> externalId,
          "label" -> "Custom",
          "labelChanged" -> false,
          "modifier" -> modifierMap,
          "wireboardSide" -> -1,
          "wireboardPosition" -> -1,
          "uid" -> Common.randomEditorUUID
        ),
        Map(
          "value" -> null,
          "valueNewerThanInside" -> false
        )
      )
    }

    /**
     * Class to allow converstion from Option[Any] to one of List[Any] or List[ Map[String,Any] ]
     * @param a value to be converted
     */
    class OptionAny(a:Option[Any]) {
      def asList:List[Any] = a match {
        case Some(l:List[Any]) => l
        case _ => List()
      }
      def asListOfMap:List[Map[String,Any]] = a match {
        case Some(l:List[Map[String,Any]]) => l
        case _ => List()
      }
      def asMap:Map[String,Any] = a match {
        case Some(m:Map[String,Any]) => m
        case _ => Map()
      }
      def asMapOfMap:Map[String,Map[String,Any]] = a match {
        case Some(m:Map[String,Map[String,Any]]) => m
//        case Some(m:Map[String,Any]) => m.map
        case _ => Map()
      }
      def asString:String = a match {
        case Some(s:String) => s
        case _ => ""
      }
    }

    implicit def optionAnyWrapper(a:Option[Any]):OptionAny = new OptionAny(a)
    
    def makeEventRow(oldProject:Projects1204, content:String, initialValues:Option[String], width:Int, height:Int) =
      new Projects1204(
        projectid = randomUUID,
        version = 0L,
        content = content,
        createddatetime = oldProject.createddatetime,
        retired = oldProject.retired,
        initialvalues = initialValues,
        editeddatetime = oldProject.editeddatetime,
        projectmetadata = None,
        projectname = "Event 1",
        nextversionid = None,
        authormemberid = oldProject.authormemberid,
        dep_gadgetrefid = None,
        initialvalue = None,
        dep_instanceid = None,
        projecttypeid = 3,
        gadgetsize = Some(content.getBytes("UTF-8").length),
        autosave = false,
        dep_publishid = None,
        published = false,
        accountid = oldProject.accountid,
        versionmajor = None,
        versionminor = None,
        versiondot = None,
        versionbuildnumber = None,
        categoryid = None,
        description = Some(""),
        statusid = None,
        licensetypeid = None,
        price = None,
        currencytypeid = None,
        hideinstorelist = Some(true),
        featured = Some(false),
        publishedmetadata = None,
        selfprice = None,
        inviteonly = false,
        guestlistid = None,
        dep_publishedretired = None,
        width = width,
        height = height,
        nextpublishedversionid = None,
        accounttypeid = oldProject.accounttypeid,
        urlname = None,
        publishedname = None)
    

    def makeStateContent(oldState:Map[String, Any], width:Int, height:Int, projectId:String, contentMaps:String => Map[String,Map[String, Any]], eventSmartWireAnchors:List[SmartWireAnchor]):Map[String, Any] = {
      Map(
        "assets" -> oldState.get("assets").asList,
        "gadgets" -> oldState.get("gadgets").asList,
        "initialValues" -> makeProjectValues(oldState.get("initialValues").asMap, width, height, projectId, contentMaps, eventSmartWireAnchors, false),
        "locked" -> false,
        "running" -> oldState.get("running").get, //TODO check world.running perhaps
        "scripts" -> null
      )
    }

    def fixAssetsList(assets:Option[Any]):List[Map[String, Any]] = assets.asList.flatMap(_ match {
      case m:Map[String, Any] => (m.get("type"), m.get("id"), m.get("assetID")) match {
        case (Some("AssetReference") | Some("Asset"), Some(id:String), _) =>
          Some(Map("type" -> "AssetReference", "id" -> id))
        case (Some("AssetReference") | Some("Asset"), _, Some(id:String)) =>
          Some(Map("type" -> "AssetReference", "id" -> id))
        case _ => None
      }
      case uuidRegex(id:String) =>
        Some(Map("type" -> "AssetReference", "id" -> id))
      case _ => None
    })

    def fixGadgetsList(gadgets:Option[Any]):List[Map[String, Any]] = gadgets.asList.flatMap(_ match {
      case m:Map[String, Any] => (m.get("type"), m.get("id")) match {
        case (Some("ProjectReference"), Some(id:String)) =>
          Some(Map("type" -> "ProjectReference", "id" -> id))
        case _ => None
      }
      case uuidRegex(id:String) =>
        Some(Map("type" -> "ProjectReference", "id" -> id))
      case _ => None
    })

    def makeProjectContent(oldContent:Map[String, Any], projectId:String, eventProjectId:String, eventSmartWireAnchors:List[SmartWireAnchor]):Map[String, Any] = {
      val eventProject = Map(
        "type" -> "ProjectReference",
        "id" -> eventProjectId
      )
      Map(
        "children" -> List[Any](), // empty world children
        "valueChildren" -> Map(
          "eventFlow" -> Map(
            "className" -> "EventFlow",
            "valueChildrenLists" -> Map(
              "masterPages" -> List(
                Map(
                  "className" -> "EventPage",
                  "inspectorVisible" -> List(
                    "moveWithTransition"
                  )
                )
              ),
              "pages" -> List(
                Map(
                  "className" -> "EventPage",
                  "gadget" -> eventProject,
                  "inspectorVisible" -> List(
                    "transitionSecs",
                    "resetOnEnter",
                    "transTweenType",
                    "eventTransition"
                  )
                )
              )
            ),
            "children" -> List[Any]() // empty backstage
          ),
          "project" -> (oldContent match {
            case hasValueChildren(hasProject(m:Map[String, Any])) => convertChild(m, projectId, List("project"))
            case _ => defaultProjectContent()
          })
        ),
        "wiring" -> projectWiring(oldContent.get("wiring").asList, eventSmartWireAnchors),
        "gadgets" -> "foo", // inherit from child
        "assets" -> fixAssetsList(oldContent.get("assets")), // inherit from child
        "gadgets" -> (eventProject :: fixGadgetsList(oldContent.get("gadgets")))
      )
    }


    def makeEventContent(oldContent:Map[String, Any], projectId:String, eventSmartWireAnchors:List[SmartWireAnchor]):Map[String, Any] = Map(
      "children" -> (for((child, index) <- oldContent.get("children").asListOfMap.zipWithIndex) yield convertChild(child, projectId, List(index))),
      "wiring" -> eventWiring(oldContent.get("wiring").asList, eventSmartWireAnchors),
      "gadgets" -> fixGadgetsList(oldContent.get("gadgets")),
      "assets" -> fixAssetsList(oldContent.get("assets")),
      "edgeAnchors" -> eventSmartWireAnchors.map(_.definition)
    )

    def makeEventValues(oldValues:Map[String, Any], width:Int, height:Int, projectId:String, contentMaps:String => Map[String,Map[String,Any]], eventSmartWireAnchors:List[SmartWireAnchor], initial:Boolean):Map[String, Any] = Map(
      "values" -> defaultEvent1Values(width, height, initial),
      "anchorValues" -> eventSmartWireAnchors.map(s => (s.key, s.values)).toMap,
      "children" -> (for((child, index) <- oldValues.get("children").asListOfMap.zipWithIndex) yield convertChildValues(contentMaps, child, projectId, List(index), initial))
    )

    def makeGadgetContent(oldContent:Map[String, Any], gadgetId:String):Map[String, Any] =
      (oldContent - "initialValues") ++ Map(
        "children" -> (for((child, index) <- oldContent.get("children").asListOfMap.zipWithIndex) yield convertChild(child, gadgetId, List(index))),
        "wiring" -> gadgetWiring(oldContent.get("wiring").asList),
        "gadgets" -> fixGadgetsList(oldContent.get("gadgets")),
        "assets" -> fixAssetsList(oldContent.get("assets")),
        "edgeAnchors" -> oldContent.get("edgeAnchors").asList
      )

    def makeGadgetValues(oldValues:Map[String, Any], gadgetId:String, contentMaps:String => Map[String,Map[String,Any]], initial:Boolean):Map[String, Any] =
      convertChildValues(contentMaps, oldValues, gadgetId, List(), initial)

    def makeProjectValues(oldValues:Map[String, Any], width:Int, height:Int, projectId:String, contentMaps:String => Map[String,Map[String,Any]], eventSmartWireAnchors:List[SmartWireAnchor], initial:Boolean):Map[String, Any] = Map(
      "autosaveMinutes" -> (oldValues.get("autosaveMinutes") match {
        case Some(minutes:Int) => minutes
        case _ => 20
      }),
      "values" -> defaultWorldValues(width, height),
      "children" -> List[Any](), // empty world children
      "valueChildren" -> Map(
        "eventFlow" -> Map(
          "values" -> defaultEventFlowValues(width, height, initial),
          "valueChildrenLists" -> Map[String, Any](
            "masterPages" -> List(
              defaultMasterPageValues(width, height)
            ),
            "pages" -> List(
              makeEventValues(oldValues, width, height, projectId, contentMaps, eventSmartWireAnchors, false)
            )
          ),
          "children" -> List[Any]() // empty backstage
        ),
        "project" -> (oldValues match {
          case hasValueChildren(hasProject(m:Map[String, Any])) => convertChildValues(contentMaps, m, projectId, List("project"), initial)
          case _ => defaultProjectValues(initial)
        })
      )
    )

     //TODO convert a child element recursively
    def convertChild(child:Map[String, Any], startGadgetId:String, startId:List[Any]):Map[String, Any] = {
      val hasClassName = ValueFor("className")
       val hasClassNameAndGadgetId = ValuesFor("className", "gadgetID")

      def reRoot(gadgetId:String, id:String, child:Map[String, Any]) = None // content won't descend past boundaries that need reRooting
      def mutateChild(gadgetId:String, id:String, child:Map[String, Any]) = (child match {
        case hasClassName("Project") => child.updated("className", "ProjectObject")
        case hasClassNameAndGadgetId("Composite", id) => (child - "gadgetID") + ("gadget" -> Map("type" -> "ProjectReference", "id" -> id))
        case hasClassName("Arena") => {
          val children = child.get("children").asList
          (child - "children") ++ Map(
            "children" -> List[Any](),
            "valueChildrenLists" -> Map[String, Any](
              "masterPages" -> List(
              ),
              "pages" -> children
            ),
            "children" -> List[Any]() // empty backstage
          )
        }
        case _ => child
      }) + ("customAnchors" -> getContentAnchors(child))

      buildObject(mutateChild, reRoot, child, startGadgetId, startId)
    }

    def getContentAnchors(child:Map[String, Any]):List[Any] = {
      val hasClassAndCustomAnchors = ValuesFor("className", "customAnchors")
      val hasKey = ValueFor("key")
      val variableFor = """var;(\w+);[\-A-Z0-9]+""".r

      def extraProps(a:Map[String, Any]) = a match {
        case hasKey(variableFor(variable)) => Map("variable" -> variable)
        case _ => Map()
      }
      child match {
        case hasClassAndCustomAnchors(_:String, anchors:List[Map[String, Any]]) =>
          anchors.map( a => a.filterKeys(Set("customModifier", "key", "kind", "uid", "pathNodeInfo")) ++ extraProps(a))
        case _ => List() //throw new Error("Content has no customAnchors")
      }
    }


    def fixValue(value:Any):Any =  {
      val hasTypeAndId = ValuesFor("type", "id")
      val hasTypeAndassetID = ValuesFor("type", "assetID")
      val hasFilterFunctionLengthListAndSource = ValuesFor("filterFunction", "length", "list", "source")
      val hasLengthAndSource = ValuesFor("length", "source")
      value match {
        case hasTypeAndId("AbstractObjectReference", id:List[Any]) => Map("type" -> "AbstractObjectReference", "id" -> fixId(id))
        case hasTypeAndassetID("Asset", id:String) => Map("type" -> "AssetReference", "id" -> id)
        case arraylist @ hasFilterFunctionLengthListAndSource(_, _, list @ hasLengthAndSource(_, source1:List[Any]), source2:List[Any]) => Some(arraylist).asMap + ("list" -> (Some(list).asMap + ("source" -> source1.map(fixValue)) )) + ("source" -> source2.map(fixValue))
        case list:List[Any] => list.map(fixValue)
        case _ => value
      }
    }

    def fixKeyedValue(key:String, value:Any):Any = fixValue(value) // for now ignore property names

    def fixValues(values: Map[String,Any]): Map[String, Any] = values map {case (k,v) => (k, fixKeyedValue(k, v))}


    //TODO convert a child element recursively
    def convertChildValues(contentMaps:String => Map[String,Map[String,Any]], child:Map[String, Any], startGadgetId:String, startId:List[Any], initial:Boolean):Map[String, Any] = {

      val hasClassName = ValueFor("className")
      val hasWiringAndInitialValues = ValuesFor("wiring", "initialValues")
      val hasClassNameAndGadgetID = ValuesFor("className", "gadgetID")

      def resumeVisible(values:Map[String, Any]) = {
        val hasHideOnRun = ValueFor("hideOnRun")
        val hasVisibility = ValueFor("visible")
        values match {
          case hasHideOnRun(true) => false
          case hasVisibility(false) => false
          case _ => true
        }
      }

      def resumeCloaked(values:Map[String, Any]) = {
        val hasCloakOnRun = ValueFor("cloakOnRun")
        val hasCloakedNow = ValueFor("cloakedNow")
        values match {
          case hasCloakOnRun(true) => true
          case hasCloakedNow(true) => true
          case _ => false
        }
      }

      def reRoot(gadgetId:String, id:String, child:Map[String, Any]) = {
        var content = contentMaps(gadgetId)(id)
        content match {
          case hasClassNameAndGadgetID("Composite", gadgetId:String) => Some(gadgetId)
          case _ => None
        }
      }

      def mutateChild(gadgetId:String, id:String, child:Map[String, Any]) = {

        var content = contentMaps(gadgetId)(id)
        (content match {
          case hasClassName("Arena") => {
            val children = child.get("children").asListOfMap
            val pageLayoutProperties = Set("layout","marginAll","marginLeft","marginTop","marginBottom","marginRight",
                        "alignObjsHorz","alignObjsVert","alignCellsHorz","alignCellsVert","hGap","vGap","rows","columns",
                        "insertIndicatorColor","insertIndicatorActive","insertIndicatorWeight","layoutTweenType","layoutTweenTime")
            val pageLayoutMap = child.get("values").asMap.filterKeys(pageLayoutProperties)
            val updatedPages = children.map(_.map({
              case ("values", values:Map[String,Any]) => ("values", values ++ pageLayoutMap)
              case other => other
            }))
            (child - "children") ++ Map(
              "children" -> List[Any](),
              "valueChildrenLists" -> Map[String, Any](
                "masterPages" -> List(
                ),
                "pages" -> updatedPages
              ),
              "children" -> List[Any]() // empty backstage
            )
          }
          case hasClassName(_) => child // no specific changes
          case hasWiringAndInitialValues(wiring:List[Any], _) if id == "" => child // we've got a gadget here
          case _ => throw new Error("Could not find content for gadgetId: "+gadgetId+" id [" + id + "]")
        }) ++ Map(
          "anchorValues" -> getValuesAnchors(content, child.get("anchorValues").asMap),
          "values" -> (child.get("values") match {
            case Some(values:Map[String, Any]) => fixValues(values) ++ Map(
              "resumeVisible" -> resumeVisible(values),
              "resumeCloaked" -> resumeCloaked(values)
            ) ++ (values.getOrElse("shortClassName", child.getOrElse("className", null)) match {
              case null => Map()
              case "Project" => Map("shortClassName" -> "ProjectObject")
              case other => Map("shortClassName" -> other)
            }) ++ ((values.get("controlsCloaked"), values.get("controlsVisible")) match {
              case (Some(hideOnRun:Boolean), Some(visible:Boolean)) => Map("resumeControlsVisible" -> (!hideOnRun && visible))
              case (_, Some(visible:Boolean)) => Map("resumeControlsVisible" -> (visible))
              case (Some(hideOnRun:Boolean), _) => Map("resumeControlsVisible" -> (!hideOnRun))
              case _ => Map()
            }) ++ (values.get("autorun") match {
              case Some(autorun:Boolean) => Map("resumeTicking" -> autorun)
              case _ => Map()
            })
            case _ => throw new Error("No values for child gadgetId: "+gadgetId+" id: " + id + " values: " + child)
          })
        )
      }

      buildObject(mutateChild, reRoot, child, startGadgetId, startId)
    }

    def anchorKey(anchor:Map[String, Any]) = {
      val hasKindAndPathNodeInfo = ValuesFor("kind", "pathNodeInfo")
      val hasKey = ValueFor("key")
      anchor match {
        case hasKindAndPathNodeInfo("pathNode", hasKey(key:String)) => key
        case hasKey(key:String) => key
        case _ => throw new Error("Unable to find anchor key in " + anchor)
      }
    }


    def getValuesAnchors(child:Map[String, Any], existingAnchors:Map[String, Any]) = {
      val hasClassAndCustomAnchors = ValuesFor("className", "customAnchors")
      child match {
        case hasClassAndCustomAnchors(_:String, anchors:List[Map[String, Any]]) =>
          anchors.map( a => {
            val key = anchorKey(a)
            val existing = existingAnchors.get(key).asMap.mapValues(fixValue)
            val badKeys = List("customModifier", "key", "kind", "uid", "pathNodeInfo")
            (key , (a -- badKeys).mapValues(fixValue) ++ existing)
          }).toMap
        case _ => Map()
      }
    }


    def getSize(oldState:Option[Any], oldContent:Option[Any], oldInitialValues:Option[Any]):(Int, Int) = {
      val hasInitialValues = ValueFor("initialValues")
      val hasValues = ValueFor("values")
      val hasSize = ValuesFor("width", "height")
      val hasScaled = ValuesFor("scaledWidth","scaledHeight")
      object valid {
        def unapply(a:Any):Option[Int] = a match {
          case d:Double if(d > 0 && d < Int.MaxValue) => Some(d.round.toInt)
          case d:Int if(d > 0) => Some(d)
          case d:BigInt if(d > 0 && d < Int.MaxValue) => Some(d.toInt)
          case _ => None
        }
      }
      Seq(
        oldState.collect({
          case hasInitialValues(hasValues(hasSize(valid(width), valid(height)))) => (width, height)
        }),
        oldContent.collect({
          case hasScaled(valid(width), valid(height)) => (width, height)
        }),
        oldInitialValues.collect({
          case hasValues(hasSize(valid(width), valid(height))) => (width, height)
        })
      ).flatten.headOption.getOrElse(800, 600)
    }

    def buildObject(mutateObject: (String, String, Map[String, Any]) => Map[String, Any], reRoot:(String, String, Map[String, Any]) => Option[String], oldObject:Map[String, Any], startGadgetId:String, startId:List[Any]):Map[String, Any] = {
      val hasChildren = ValueFor("children")
      val hasValueChildren = ValueFor("valueChildren")
      val hasValueChildrenLists = ValueFor("valueChildrenLists")

      def recurse(gadgetId:String, inverseId:List[Any], oldObject:Map[String, Any]):Map[String, Any] = {
        val stringId = inverseId.reverse.mkString(",")
        val (childGadgetId, childRootInverseId) = if(inverseId.isEmpty)
          (gadgetId, inverseId)
        else
          reRoot(gadgetId, stringId, oldObject) match {
            case Some(id) => (id, List())
            case _ => (gadgetId, inverseId)
          }

        val children:Map[String, Any] = oldObject match { // grab any direct children (ex objects on a page)
          case hasChildren(children:List[Map[String, Any]]) => {
            val list:List[Map[String, Any]] = for((child, index) <- children.zipWithIndex) yield recurse(childGadgetId, index :: childRootInverseId, child)
            Map("children" -> list)
          }
          case _ => Map()
        }
        val valueChildren:Map[String, Any] = oldObject match { // grab any children in properties (ex lms object)
          case hasValueChildren(valueChildren:Map[String, Map[String, Any]]) => {
            val map:Map[String, Map[String, Any]] = (for((key, child) <- valueChildren) yield (key, recurse(childGadgetId, key :: childRootInverseId, child)))
            Map("valueChildren" -> map)
          }
          case _ => Map()
        }
        val valueChildrenLists:Map[String, Any] = oldObject match { // grab any children in lists in properties (ex master pages)
          case hasValueChildrenLists(lists:Map[String, List[Map[String, Any]]]) => {
            val map:Map[String, List[Map[String, Any]]] = for((key, list) <- lists)
              yield (key, for((child, index) <- list.zipWithIndex)
                yield recurse(childGadgetId, index :: key :: childRootInverseId, child))
            Map("valueChildrenLists" -> map)
          }
          case _ => Map()
        }
        mutateObject(gadgetId, stringId, oldObject ++ children ++ valueChildren ++ valueChildrenLists)
      }

      recurse(startGadgetId, startId.reverse, oldObject)
    }

    def eachChild(oldContent:Map[String, Any])(action: (String, Map[String, Any])=> Unit) {
      val hasChildren = ValueFor("children")
      val hasValueChildren = ValueFor("valueChildren")
      val hasValueChildrenLists = ValueFor("valueChildrenLists")

      def stringId(inverseId:List[Any]):String = inverseId.reverse.mkString(",")

      val contents = new mutable.Stack[(List[Any],Map[String, Any])]()
      contents.push((List(),oldContent)) // bootstrap with the root object (world or gadget)

      while(contents.isEmpty == false) {
        val (inverseId, content) = contents.pop()
        content match { // grab any direct children (ex objects on a page)
          case hasChildren(children:List[Map[String, Any]]) =>
            for((child, index) <- children.zipWithIndex) {
              contents.push((index :: inverseId, child))
            }
          case _ =>
        }
        content match { // grab any children in properties (ex lms object)
          case hasValueChildren(valueChildren:Map[String, Map[String, Any]]) =>
            for((key, child) <- valueChildren) {
              contents.push((key :: inverseId, child))
            }
          case _ =>
        }
        content match { // grab any children in lists in properties (ex master pages)
          case hasValueChildrenLists(lists:Map[String, List[Map[String, Any]]]) =>
            for((key, list) <- lists) {
              val listId = key :: inverseId
              for((child, index) <- list.zipWithIndex) {
                contents.push((index :: listId, child))
              }
            }
          case _ =>
        }
        action(stringId(inverseId), content)
      }
    }

    def getContentMap(oldContent:Map[String, Any]):Map[String, Map[String, Any]] = {
      val result = new mutable.HashMap[String, Map[String, Any]]()

      eachChild(oldContent) {(id, content) => result.put(id, content)}
      result.toMap
    }

    def findAssets(project:Projects1204) = {
      val content = Common.parse(project.content)
      val values = getState(project) flatMap { s => Common.parse(s.content) }

      val libraryIds = Common.parseAssetsFromLibrary(content)
      val assetIds = Common.parseAssetsFromValues(content)
      println("Found content assets: "+libraryIds + "," +assetIds)
      val currentLibraryIds = Common.parseAssetsFromLibrary(values)
      val currentAssetIds = Common.parseAssetsFromValues(values)
      println("Found currentValue assets: "+currentAssetIds + "," + currentAssetIds)

      val allAssetIds = currentLibraryIds ++ currentAssetIds ++ libraryIds ++ assetIds

      println("Looking up assets: " + allAssetIds)

      val assets = if(!allAssetIds.isEmpty) {
        from(Db1204.asset)(a =>
          where(a.id in allAssetIds)
            select(a)
        ).toList
      } else {
        List[Asset1204]()
      }

      for(asset <- assets) {
        println("Found asset id: "+asset.id+" name: "+asset.name)
      }
      assets
    }

    def getState(convertProject:Projects1204) = {
      from(Db1204.gadget_state)(gs =>
        where(
          gs.root_project_id === convertProject.projectid
            and gs.app_user_id === convertProject.authormemberid
            and gs.author_mode === true

        )
        select(gs)
      ).headOption
    }

    def convertApp(convertProject:Projects1204, contentMaps:mutable.Map[String, Map[String,Map[String,Any]]]):Tuple3[Projects1204, Option[Projects1204], Option[GadgetState1204]] = {

      val state = getState(convertProject)

      state match {
        case Some(s) =>  println("Found state id: "+s.id)
        case None => println("No state found")
      }

      val parsedContent = Common.parse(convertProject.content)
      val parsedInitial = parsedContent.asMap.get("initialValues")
      val parsedCurrent = state flatMap { s => Common.parse(s.content) }


      val (oldWidth, oldHeight) = getSize(parsedCurrent, parsedContent, parsedInitial)
      val contentMap = getContentMap(parsedContent asMap)

      val eventSmartWireAnchors = makeEventSmartWireAnchors(parsedContent asMap, contentMap/*, parsedCurrent, parsedInitial*/)

      contentMaps.put(convertProject.projectid, contentMap)

      val eventContent = makeEventContent(parsedContent asMap, convertProject.projectid, eventSmartWireAnchors)
      val eventContentString = Common.render(eventContent).get
      val eventInitial = makeEventValues(parsedContent.asMap.get("initialValues").asMap, oldWidth, oldHeight, convertProject.projectid, contentMaps, eventSmartWireAnchors, true)

      val newEvent = makeEventRow(convertProject, eventContentString,  Common.render(eventInitial), oldWidth, oldHeight)


      val projectContent = makeProjectContent(parsedContent asMap, convertProject.projectid, newEvent.projectid, eventSmartWireAnchors)
      val projectInitial = makeProjectValues(parsedInitial.asMap, oldWidth, oldHeight, convertProject.projectid, contentMaps, eventSmartWireAnchors, true)
      val newProject = convertProject.copy(content = Common.render(projectContent).get, initialvalues = Common.render(projectInitial), width = oldWidth, height = oldHeight)


      val stateContent = parsedCurrent match {
        case Some(m:Map[String, Any]) => Some(makeStateContent(m, oldWidth, oldHeight, convertProject.projectid, contentMaps, eventSmartWireAnchors))
        case _ => None
      }
      val newState = state map { s => s.copy(content = Common.render(stateContent).get) }

      (newProject, Some(newEvent), newState)
    }

    def convert(convertProject:Projects1204, contentMaps:mutable.Map[String, Map[String,Map[String,Any]]], justChild:Boolean):Projects1204 = {

      val parsedContent = Common.parse(convertProject.content)
      val contentMap = getContentMap(parsedContent asMap)

      contentMaps.put(convertProject.projectid, contentMap)

      if(justChild) {
        convertProject
      } else {
        val parsedInitial = parsedContent.asMap.get("initialValues")
        val (oldWidth, oldHeight) = getSize(None, parsedContent, parsedInitial)

        val projectContent = makeGadgetContent(parsedContent asMap, convertProject.projectid)
        val projectInitial = makeGadgetValues(parsedInitial.asMap, convertProject.projectid, contentMaps, true)
        val newProject = convertProject.copy(content = Common.render(projectContent).get, initialvalues = Common.render(projectInitial), width = oldWidth, height = oldHeight)

        newProject
      }
    }

    using(source) {

      //org.squeryl.Session.currentSession.setLogger(println)
      val convertProject = projectSource.project


      val oldChildren = Migrate1204to1204.getChildren(convertProject)
      println("Found children: " + oldChildren.mapValues(_.projectname))
      val oldParentChildren = Migrate1204to1204.getParentChildren(oldChildren.keys)

      val oldConvertedChildren = new mutable.HashMap[String, Projects1204]
      val contentMaps = new mutable.HashMap[String, Map[String,Map[String, Any]]]
      for(gadgetId <- Migrate1204to1204.childrenFirst(oldParentChildren)) {
        val convertGadget = oldChildren(gadgetId)
        val convertedGadget = convert(convertGadget, contentMaps, inPlace)
        oldConvertedChildren.put(gadgetId, convertedGadget)
      }


      val (convertedProject, convertedEvent, convertedState) = if(convertProject.projecttypeid == 2) convertApp(convertProject, contentMaps) else (convert(convertProject, contentMaps, false), None, None)

      val createdParentChildren = convertedEvent match {
        case Some(event) =>
          ParentChild1204(convertedProject.projectid, event.projectid) ::
            oldParentChildren.filter(_.parent_children_id == convertProject.projectid).map(_.copy(parent_children_id = event.projectid))
        case _ => List()
      }

      if(inPlace) {
        modification("update project") {
          Db1204.projects.insert(convertedEvent)

          update(Db1204.projects)(p =>
            where(p.projectid === convertedProject.projectid)
            set(
              p.content := convertedProject.content,
              p.initialvalues := convertedProject.initialvalues,
              p.width := convertedProject.width,
              p.height := convertedProject.height
            )
          )

          if(createdParentChildren.nonEmpty) {
            Db1204.parent_child.insert(createdParentChildren)
          }

          for(state <- convertedState) {
            update(Db1204.gadget_state)(gs =>
              where(gs.id === state.id)
              set(gs.content := state.content)
            )
          }
        }
      } else {
        val event = convertedEvent.get // we better be creating a project and therefore an event
        val children = oldConvertedChildren.toMap + (event.projectid -> event) + (convertedProject.projectid -> convertedProject)

        val parentChildren = oldParentChildren ++ createdParentChildren

        val assets = findAssets(convertProject)
        using(destination) {
          Migrate1204to1204.migrateProjectFull(destUser, convertedProject, convertedState flatMap { s => Common.parse(s.content)}, assets, parentChildren, children, List())
        }
      }
      return (convertedProject, convertedState, convertedEvent)
    }


  }

}
