import collection.mutable
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.dsl.ast.{FunctionNode, ConstantExpressionNode}
import org.squeryl.dsl.{BinaryExpression, StringExpression}
import org.squeryl.internals.OutMapper
import org.squeryl.Session
import scala.{Some, None}

object Common {


  def nop:Boolean = false

  val uuidRegex = """^([0-9a-f]{32})$""".r
  val assetToStringRegex = """^\[Asset .* : ([0-9a-f]{32}) : .*\]$""".r

  def randomUUID = java.util.UUID.randomUUID().toString.replaceAll("-", "").toLowerCase
  def randomEditorUUID = """^(.{8})(.{4})(.{4})(.{4})(.{12})$""".r.unapplySeq(
    java.util.UUID.randomUUID().toString.replaceAll("-", "").toUpperCase
  ).get.reduce(_+"-"+_)

  def usedProjectsFromContent(p:Option[Any]):List[String] = {
    val projectIds = new mutable.HashSet[String]() // all found projects
    val unparsed = new mutable.Stack[Any]() // all json objects yet to search
    val hasTypeAndId = new ValuesFor("type", "id")
    def fromGadget(p:Option[Any]) = p match {
      case Some(hasTypeAndId("ProjectReference", id:String)) => projectIds.add(id)
      case _ =>
    }
    def fromChildren(p:Option[Any]) = p match {
      case Some(l:List[Any]) => unparsed.pushAll(l)
      case _ =>
    }
    def fromValueChildren(p:Option[Any]) = p match {
      case Some(m:Map[String, Any]) => unparsed.pushAll(m.values)
      case _ =>
    }
    def fromValueChildrenLists(p:Option[Any]) = p match {
      case Some(m:Map[String, List[Any]]) => unparsed.pushAll(m.values.flatten)
      case _ =>
    }
    p match {
      // seed with the root if we have one
      case Some(value) => unparsed.push(value)
      case _ => {}
    }
    while(!unparsed.isEmpty) {
      // take next value off the stack to parse through
      unparsed.pop() match {
        case m:Map[String, Any] => {
          fromGadget(m.get("gadget"))
          fromChildren(m.get("children"))
          fromValueChildren(m.get("valueChildren"))
          fromValueChildrenLists(m.get("valueChildrenLists"))
        }
        case _ => // ignore
      }

    }
    projectIds.toList
  }

  def parseAssetsFromLibrary(p:Option[Any]):List[String] = {
    p match {
      case Some(jsonMap:Map[String, Any]) => jsonMap.get("assets") match {
        case Some(jsonList:List[Any]) => jsonList.flatMap( item => item match {
          case jsonAssetMap:Map[String, Any] => jsonAssetMap.get("id") match {
            case Some(uuidRegex(idJson:String)) => Some(idJson) // found it in the id field in a map
            case _ => None // no id, or it isn't a string
          }
          case uuidRegex(idJson:String) => Some(idJson) // found it straight in the list
          case assetToStringRegex(idJson:String) => Some(idJson) // found it as Asset.toString
          case _ => None // not a map or string at this entry in the list
          })
        case _ => List[String]() // no map in the assets field
      }
      case _ => List[String]() // not a map
    }
  }

  def parseAssetsFromValues(p:Option[Any]):Set[String] = {
    val assetIds = new mutable.HashSet[String]() // all found assets
    val unparsed = new mutable.Stack[Any]() // all json objects yet to search
    p match {
      // seed with the root if we have one
      case Some(value) => unparsed.push(value)
      case _ => {}
    }
    while(!unparsed.isEmpty) {
      // take next value off the stack to parse through
      unparsed.pop() match {
        case m:Map[String, Any] => (m.get("type"), m.get("id"), m.get("assetID")) match {
          // found an asset or asset reference that has a proper id
          case (Some("AssetReference") | Some("Asset"), Some(id:String), _) => {
            assetIds.add(id)
          }
          // found an asset or asset reference that has a proper id
          case (Some("AssetReference") | Some("Asset"), _, Some(id:String)) => {
            assetIds.add(id)
          }
          case _ => for(value <- m.values) {
            // non special map, so process its values
            unparsed.push(value)
          }
        }
        case l:List[Any] => for(value <- l) {
            // non special List, so process its values
          unparsed.push(value)
        }
        case _ => // ignore other types such as number or string
      }

    }
    assetIds.toSet
  }

  def replaceIds(p:Option[Any], assetIds:Map[String, String], projectIds:Map[String, String], projectContent:Boolean):Option[Any] = {
    object State extends Enumeration {
      val Root, Assets, Gadgets, Generic = Value
    }

    def replace(s:State.Value)(p:Any):Any = s match {
      // at the root of the gadget content
      case State.Root => p match {
        case m:Map[String, Any] => {
          val mappedAssets:List[Any] = m.get("assets") match {
            // specially replace assets list
            case Some(l:List[Any]) => l.flatMap(elem => {
              try {
                Some(replace(State.Assets)(elem))
              } catch {
                case e:Error => {
                  println("ERROR: removing asset from Library: " + e.getMessage)
                  None
                }
              }
            }).toSet.toList
            case Some(null) => null
            case None => List()
          }
          val mappedGadgets:List[Any] = m.get("gadgets") match {
            // specially replace gadgets list
            case Some(l:List[Any]) => l.flatMap(elem => {
              try {
                Some(replace(State.Gadgets)(elem))
              } catch {
                case e:Error => {
                  println("ERROR: removing gadget from Library: " + e.getMessage)
                  None
                }
              }
            }).toSet.toList
            case Some(null) => null
            case None => List()
          }
          // replace everything other than assets list, then insert updated assets list
          val otherValues = ((m - "assets") - "gadgets")
          val replaced = otherValues.mapValues(replace(State.Generic))
          replaced.updated("assets", mappedAssets).updated("gadgets", mappedGadgets)
        }
        case _ => replace(State.Generic)(p) // Root that isn't a map... maybe we should error here?
      }
      // in the asset library list
      case State.Assets => p match {
        // found a raw id string not in a reference, so replace and then wrap it
        case assetToStringRegex(id:String) if(assetIds contains id) => { // found it as Asset.toString
          Map("type" -> "AssetReference", "id" -> assetIds(id))
        }
        case id:String if(assetIds contains id) => {
          Map("type" -> "AssetReference", "id" -> assetIds(id))
        }
        case id:String => throw new Error("Found unexpected assets id [" + id + "]")
        case _ => replace(State.Generic)(p)
      }
      // in the gadget library list
      case State.Gadgets => p match {
        // found a raw id string not in a reference, so replace and then wrap it
        case id:String if(projectIds contains id) => {
          Map("type" -> "ProjectReference", "id" -> projectIds(id))
        }
        case id:String => throw new Error("Found unexpected gadgets id [" + id + "]")
        case _ => replace(State.Generic)(p)
      }
      // some generic place where we will search for random references
      case State.Generic => p match {
        // recurse down JSON objects
        case m:Map[String, Any] => (m.get("type"), m.get("id"), m.get("assetID")) match {
          // found an asset or asset reference that has a proper id
          case (Some("AssetReference") | Some("Asset"), Some(id:String), _) if(assetIds contains id) => {
            // replace everything other than id, then insert updated id
            //(m - "id").mapValues(replace(State.Generic)).updated("id", assetIds(id))
            Map("type" -> "AssetReference", "id" -> assetIds(id))
          }
          // found an asset or asset reference that has a proper id
          case (Some("AssetReference") | Some("Asset"), _, Some(id:String)) if(assetIds contains id) => {
            // replace everything other than id, then insert updated id
            //(m - "id").mapValues(replace(State.Generic)).updated("id", assetIds(id))
            Map("type" -> "AssetReference", "id" -> assetIds(id))
          }
          case (typeValue @ (Some("AssetReference") | Some("Asset")), id, assetid) =>
            throw new Error("Found unexpected or malformed Asset type: " + typeValue + " id: [" + id + "] assetID: [" + assetid + "]" )
          // found an project reference that has a proper id
          case (Some("ProjectReference"), Some(id:String), _) if(projectIds contains id) => {
            // replace everything other than id, then insert updated id
            //(m - "id").mapValues(replace(State.Generic)).updated("id", projectIds(id))
            Map("type" -> "ProjectReference", "id" -> projectIds(id))
          }
          case (Some("ProjectReference"), id, _) =>
            throw new Error("Found unexpected or malformed ProjectReference  id: [" + id + "]" )
          case _ => m.mapValues(replace(State.Generic))
        }
        // recurse down JSON lists
        case l:List[Any] => l.map(replace(s))
        case _ => p // ignore other types such as number or string
      }
    }

    p match {
      // Garbage in => garbage out
      case None => None
      case Some(value) => {
        val startState = if(projectContent) State.Root else State.Generic
        val result = replace(startState)(value)
        Some(result)
      }
    }
  }

  def parse(json:String):Option[Any] = net.liftweb.json.parseOpt(json) map fromLiftJson
  def parse(json:Option[String]):Option[Any] = json flatMap net.liftweb.json.parseOpt map fromLiftJson
  def render(p:Option[Any]):Option[String] = p.map({v=>render(v, net.liftweb.json.compact)})
  def render(p:Any):Option[String] = Some(render(p, net.liftweb.json.compact))
  def renderPretty(p:Option[Any]):Option[String] = p.map({v=>render(v, net.liftweb.json.pretty)})
  def renderPretty(p:Any):Option[String] = Some(render(p, net.liftweb.json.pretty))
  def renderCode(p:Option[Any]):Option[String] = p.map(toCodeString(_, ""))
  def renderCode(p:Any):Option[String] = Some(toCodeString(p, ""))


  private def toCodeString(p:Any, indent:String=""):String = {
    val indent2 = indent+"  "
    p match {
      case m:Map[String, Any] if m.isEmpty => "Map()"
      case m:Map[String, Any] =>
        "Map(\n" +
        indent2 + m.toList.sortBy(_._1).map(toCodeString(_, indent2)).reduce(_+",\n"+indent2+_) + "\n" +
        indent + ")"
      case (key:String, value) => "\""+key+"\" -> " + toCodeString(value, indent)
      case List() => "List()"
      case l:List[Any] =>
        "List(\n" +
        indent2 + l.map(toCodeString(_, indent2)).reduce(_+",\n"+indent2+_) + "\n" +
        indent + ")"
      case s:String => '"' + s.replaceAllLiterally("\\", "\\\\").replaceAll("\r", "\\r").replaceAll("\n", "\\n").replaceAll("\t", "\\t") + '"'
      case null => "null"
      case true => "true"
      case false => "false"
      case x:BigInt => x.toString()
      case x:Int => x.toString
      case x:Double => x.toString
      case v => throw new Exception("Unknown value: " + v)
    }
  }

  private def render[A](p:Any, renderFunc: scala.text.Document=>String) = renderFunc(net.liftweb.json.render(toLiftJson(p)))


  def fromLiftJson(json:net.liftweb.json.JValue):Any = json match {
    case net.liftweb.json.JInt(x) => x
    case net.liftweb.json.JDouble(x) => x
    case net.liftweb.json.JString(s) => s
    case net.liftweb.json.JBool(x) => x
    case net.liftweb.json.JObject(fields) => fields.map({
      case net.liftweb.json.JField(k, v) => (k, fromLiftJson(v))
    }).toMap
    case net.liftweb.json.JArray(values) => values.map(fromLiftJson)
    case net.liftweb.json.JNull => null
    case v => throw new Exception("Unknown value: " + v)
  }

  def toLiftJson(value:Any):net.liftweb.json.JValue = value match {
    case x:BigInt => new net.liftweb.json.JInt(x)
    case x:Int => new net.liftweb.json.JInt(x)
    case x:Double => new net.liftweb.json.JDouble(x)
    case x:String => new net.liftweb.json.JString(x)
    case x:Boolean => new net.liftweb.json.JBool(x)
    case x:List[Any] => new net.liftweb.json.JArray(x map toLiftJson)
    case x:Map[String, Any] => new net.liftweb.json.JObject(x.toList.sortBy(_._1).map ({t => new net.liftweb.json.JField(t._1, toLiftJson(t._2))}) )
    case None | null => net.liftweb.json.JNull
    case v => throw new Exception("Unknown value: " + v)
  }

  // may be enough to do just first case says jesse
  def toUtilJson(p:Any):Any = p match {
    case m:Map[String, Any] => new util.parsing.json.JSONObject(m.mapValues(toUtilJson))
    case l:List[Any] => new util.parsing.json.JSONArray(l.map(toUtilJson))
    case _ => p
  }

// new ConstantExpressionNode[String]("sha1") with StringExpression[String]

class Digest(e: StringExpression[String], t: StringExpression[String], m:OutMapper[Array[Byte]])
  extends FunctionNode[Array[Byte]]("digest", Some(m), Seq(e, t)) with BinaryExpression[Array[Byte]]
def digest(e: StringExpression[String], t: StringExpression[String])(implicit m:OutMapper[Array[Byte]]) = new Digest(e,t,m)

class Encode(e: BinaryExpression[Array[Byte]], t: StringExpression[String], m:OutMapper[String])
  extends FunctionNode[String]("encode", Some(m), Seq(e, t)) with StringExpression[String]
def encode(e: BinaryExpression[Array[Byte]], t: StringExpression[String])(implicit m:OutMapper[String]) = new Encode(e,t,m)

//class Sha1Hex(e: StringExpression[String], m:OutMapper[String])
//  extends FunctionNode[]

//  def sha1hex(e: StringExpression[String])(implicit m:OutMapper[String]) =
//  new FunctionNode[String]("encode", Some(m), Seq(
//    new FunctionNode[String]("digest", Some(m), Seq(
//      e,
//      new ConstantExpressionNode[String]("sha1") with StringExpression[String]
//    )) with StringExpression[String],
//    new ConstantExpressionNode[String]("hex") with StringExpression[String]
//  )) with StringExpression[String]

  def sha1hex(s:String) = {
    val saltDigest:java.security.MessageDigest  = java.security.MessageDigest.getInstance("SHA-1")
    saltDigest.update(s.getBytes("UTF-8"))
    saltDigest.digest.map({b =>
      (if (( b & 0xff ) < 0x10 ) "0" else "" ) + Integer.toHexString(b & 0xff)
    }).mkString
  }

  def sha1hex(p:Projects1204) = Common.encode(Common.digest(p.content, "sha1"), "hex") || Common.encode(Common.digest(nvl(p.initialvalues, ""), "sha1"), "hex")

//  encode(digest('jg' || coalesce('',''), 'sha1'), 'hex')


  def modification(name:String)(action: => Unit) {
    if(nop) {
      println("<><><><><> Modification ["+name+"] prevented")
    } else {
      println("<><><><><> Modification ["+name+"] permitted")
      action
    }
  }

  def use(sessions:List[Session])(action: => Unit) {
    try {
      for(s <- sessions) {
        s.connection.setAutoCommit(false)
      }
      action
      for(s <- sessions) {
        s.connection.commit()
      }
    } catch {
      case e: Exception => {
        for(s <- sessions) {
          try {
            s.connection.rollback()
          } catch {
            case e:Exception => // ignore
          }
        }
        e match {
          case s:java.sql.SQLException => {
            var se = s
            while(se != null) {
              println(se.toString)
              se = se.getNextException
            }
          }
          case _ =>
        }
        throw new Exception("Rolling back transaction", e)
      }
    } finally {
      for(s <- sessions) {
        try {
          s.connection.close()
        } catch {
          case e:Exception => // ignore
        }
      }
    }
  }

  case class User(displayName:String, email:String, id:String, accountId:Long)

  def findUser(userName:String) = join(Db1204.app_user, Db1204.accountmemberroles, Db1204.accounts)((a, amr, acc) =>
          where(
            a.displayname === userName
            and a.retired === false
            and a.enabled === true
          )
          select(User(a.displayname, a.email, a.id, acc.accountid))
          on(a.id === amr.memberid, amr.accountid === acc.accountid)
        ).distinct.single

  case class ValueFor(key: String) {
    def unapply(map: Map[String,Any]): Option[Any] = map.get(key)
  }
  case class ValuesFor(keys: String*) {
    def unapplySeq(map: Map[String,Any]): Option[Seq[Any]] = {
      val res = keys.flatMap(map get _)
      if(res.length == keys.length) Some(res) else None
    }
  }
}