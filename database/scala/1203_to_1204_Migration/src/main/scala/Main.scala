import org.squeryl.adapters.PostgreSqlAdapter
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.Session
import scala.collection.mutable

object Main {


  def main(args: Array[String]) {

//    println(Common.render(Map(
//      "foo" -> List(1, 2, 3),
//      "bar" -> Map (
//        "baz" -> List(
//           Map(
//             "foo1" -> "ss",
//             "400" -> false
//           ),
//             Map(
//             (if(1==2)"foo1" else "foo2") -> "ss",
//             (if(1==2) ("400" -> false) else ())
//             )
//        )
//      )
//    )))
//
//    return

    Class.forName("org.postgresql.Driver")

    def session(port:Int, db:String="boreal", pass:String="postgres") = Session.create(
                  java.sql.DriverManager.getConnection("jdbc:postgresql://localhost:"+port+"/"+db, "postgres", pass),
                  new PostgreSqlAdapter)

    def sessionQA(port:Int) = session(port, "boreal-qa", "zebra12")

//    val userName = "jcoyle"
//    val projectName = "AssetTest"
//    val userName = "sbirth"
//    val projectName = "oval"

//    Search.singleObjectProjects(session(5003))
//    Search.singleObjectProjects(session(5432))
//      (Suzanne,23a9901073894ac999e7b4d4173241de,RCW Color Palette,Drawing)

//    Upgrade1203Content.upgrade(userName, projectName, session(5001), session(5432))

//    Migrate1204to1204.migrate("jcoyle", "AssetTest", sessionQA(5002), session(5432))


//    Upgrade1203Content.upgrade("ShortStuf7", "BSOD", session(5001), "jcoyle", session(5432))
//    Upgrade1203Content.upgrade("sbirth", "oval", session(5001), "jcoyle", session(5432))

//    Upgrade1203Content.upgrade("sbirth", "oval", session(5003), "jcoyle", sessionQA(5002))

//    Upgrade1203Content.upgrade("dsobolewski", "000001_jesse_all", session(5003), "jcoyle", session(5432))


val names = List(
//  ("dsobolewski", "transfer_tester", List("000001_jesse_all","000001_jesse_all_mod","000002_jesse_line","000002_jesse_line_mod","000003_jesse_path","000003_jesse_path_mod","000004_jesse_polygon","000004_jesse_polygon_mod","000005_jesse_rectangle","000005_jesse_rectangle_mod","000006_jesse_oval","000006_jesse_oval_mod","000007_jesse_","000007_jesse_mod","000008_jesse_button","000008_jesse_button_mod","000009_jesse_radio_button","000009_jesse_radio_button_mod","000010_jesse_check_box","000010_jesse_check_box_mod","000011_jesse_text_entry","000011_jesse_text_entry_mod","000012_jesse_sliders","000012_jesse_sliders_mod","000013_jesse_clock","000013_jesse_clock_mod","000014_jesse_audio","000014_jesse_audio_mod","000015_jesse_video","000015_jesse_video_mod","000016_jesse_map","000016_jesse_map_mod","000017_jesse_arena","000017_jesse_arena_mod","000018_jesse_calculator","000018_jesse_calculator_mod","000019_jesse_weblink","000019_jesse_weblink_mod","000020_jesse_truth_table","000020_jesse_truth_table_mod","000021_jesse_answer_judging_table","000021_jesse_answer_judging_table_mod","000022_jesse_state_table","000022_jesse_state_table_mod","000023_jesse_gadget","000023_jesse_gadget_mod","000024_jesse_asset","000024_jesse_asset_mod")),
//  ("ShortStuf7", "Transfer", List("CarControls_parking3", "Let's Make: Sandwiches", "Earn Your Stripes", /*"Anmatiion Suite x16",*/ "stocks game", "elf_workshop_nouveau_Final8", "DeltaGoo 2","photography_001","projectile motion","Rollercoaster 2","carsim_16x9_2","performance tree 024","color picker_example","Puzzle Maze 3D Game")),
//  ("Transfer", "Transfer", List("001Test","001Test-paused","001Test-resumed","001Test-running","564564654654","6454654654","8489749749849489456456","All-AnswerTable","All-Arena","All-Audio","All-Button","All-Calc","All-Checkbox","All-Clock","All-Gadget","All-InputText","All-Line","All-Maps","All-Oval","All-Path","All-Pentagon","All-Radio","All-Slider","All-Square","All-StateTable","all-Subribbons","All-Text","All-Text2","All-TruthTable","All-Video","All-Weblink","Ans. Table","arena tests","arenatests2","arenatests2whileru","Asset references","AutoRun","AutoRun-SaveWhileRunning","big test!","big test2!","Button-Pressed","button set","calc+table","Click Down","Cloak Hide","Clock","Collision","Custom IN/OUT","Deep Gadget","Drag and Drop","Gadget Custom Ribbons","Graphic Asset","Hide on Run","Hide on Run-IsRunning","huiuoiuoiu","JG Test Arena","Key Inputs","keypress tests","misc tests","nhjhkj","Object references","Object Variables","Project Message Center","Project Stage 500X250","Project Test","State Table","table object references","Tables","tests","Text Variable Ribbons","text zoom","Truth Table","zoomtests")),
//
//  ("template", "Transfer", List("gad-01_detector_fin","gad-02_gradient_fin","gad-02_gradient-Gad","gad-03_appleCol_fin","gad-03_appleCol-Gad","gad-04_sun_fin","gad-04_sun_Gad","gad-05_chart_fin","gad-05_chart_Gad","gad-06_energize_fin","gad-06_energize_Gad","gad-07_sort_fin","gad-07_sort_Gad","gad-08_problems_fin","gad-08_problems_Gad")),
////
//  ("mallen", "Transfer", List("User Gestures QA B08", "Anchor Points final", "User Gestures B08", "ICE 2012 Leaving ADDIE for SAM", "ASTD TCC 2011 03")),
//
//  ("Live01", "ACMigration01", List( "Coolness", "Drop Down Test", "Scorekeeper", "Circle Click", "Z#ZZ#Z", "Galaxy")),
//
//  ("rthomas", "rthomas", List("Test Wire Scope Migration")),
//
//    ("mallen", "sjackson", List("User Gestures QA B08")),
//    ("sjackson", "sjackson", List("00 - Arena Demo")),
//
//  ("scolehour", "scolehour", List("Barco V3","Sprinkler Winterization V10.1","Protractor","Resistor Color Coding","Chopper Head Angle V4","Marine Corp Demo V3","Utica Subrogation Interaction","HealthStream - Swap","Birthday Wishes - NYC","Multi Meter Course Final","Ear Anatomy and Exam","Bryers Product Slider","H2M Try-Angle G3-A5","G - Four Corner Display w/ Selection","Delta - Boarding Screen","Celebrity Wedding","projectile motion SAC","Model - Flip Card Example","Hot or Cold Game","Smile Face v4 - Thomas","Camera - Aperture","Paint Book - Extended V1"))

//    ("ISUCookie", "jcoyle", List("Slick Final Template"))
//    ("dsobolewski", "jcoyle", List("000006_jesse_oval_mod"))

//      ("callen", "callen", List("10","111","1234","15","222","4","465","5","55","7.16","AI Communications 5 Aug 2012 final","Allen Interactions","answer table","arena layout","ASTD DC SAM","aug 1st","autostart clock","awer","background arenas","been there","Build a Template","building a coach","button set","cant do this","changing setters","concept learning practice","concept learning practice","concept learning practice","copy parent","creating navigation","creator dump","ct","demo 22","devlearn","Drag & Drop and Collisions Teachlet - Second","Drag & Drop and Collisions Teachlet - Second(1)","edit path autosave","eventflow","event flow de","event flow demo","Event Flow Template","EventFlow Template 3","Experienced","Exploring Event Flow","FINAL_PathsTeachlet","gadget","hello","hide on run foreground master","hotel","hotspot feedback","Hot Spots Teachlet (Lon)","inflight multi part questions","inflight multi part questions new ajt","kjhk","left in small box","line crash","make a wire","master page","master pages","master page test","messing","Multiple Interactions","new ecent","newer project","new name","New Project","new project return","new projects","open project","Outs and Ins Teachlet Sketch","Outs and Ins Teachlet Sketch(1)","Outs and Ins Teachlet Sketch - Broken","Outs and Ins Teachlet v4.0","Outs and Ins Teachlet v4.0(1)","paging arena in master","paoeir","paste","pro demo","project","pro projct","qa test","random order exhaustive","Resources for Events","Ricoh","Robot fun","Robot fun(1)","role play","sales meeting","SAM2","SAM2(1)","Select Comfort V2","Sketch - Teachlet Hot Spot","Sketch - Teachlet Hot Spot(1)","Sketch - Teachlet Hot Spot(1)(2)","Sketch - Teachlet Hot Spot 3","Sketch - Teachlet Hot Spots cwa","Slider Gadget Interaction","Template 1","Template Example","testing arenas","testing paths","textbox","TinCan","tin can idea","triathlon","Untitled Project 1","Untitled Project 12","Untitled Project 14","Untitled Project 16","Untitled Project 22","Untitled Project 25","Untitled Project 5","Untitled Project60","Untitled Project 99","use gadget","video","WaxLRS","who are we","wiring simulation","yup","Zebra Primer","123","2 tests","987","ALPHA - Arenas Teachlet","BETA - Hot Spots Teachlet","BETA - Paths","BETA - Paths(1)","Closed Caption","ELB - People Layouts","Event Flow Teachlet - CWA","Event Flow Teachlet Prototype_1_help_2","Event Flow Teachlet Prototype_3","Event Flow Teachlet Prototype_3a","Flow view","geaeawegawe","Gizmo Template","Gizmo Template(1)","Mario","Master","Master Page Tutorial","Navigation bars CWA bg","Navigation bars CWA red","Navigation bars CWA warm","Navigation bars v5(1)","Navigation bars v5(1)(2)","neat slider","popup menu","Rollout","setter","State Table","Template Example(1)","text","text2","Text Input 2","text input gadget"))
//      ("dwh2m", "ddh2m", List("H2M V2","Trial_dw"))


//  ("ISUCookie", "scolehour", List("G - Graphic Seq 6G - To Gadget V11"))



//        ("Joe Ganci", "joeganci", List("Chapter2","DevLearn","hotel","Hotel 2","inflight multi part questions","Joe","linda","Marty","questions","Ricoh"))
//List((Joe Ganci,Ricoh), (Joe Ganci,questions), (Joe Ganci,linda), (Joe Ganci,Hotel 2), (Joe Ganci,hotel), (Joe Ganci,DevLearn), (Joe Ganci,Chapter2))
//    ("template", "template", List("001_Pager - Blue","001_Pager - Blue-IntGad","001_Pager - Blue-intgad2","001_Pager - Blue-intgad3","002_Pager - Blue","003_Pager - Blue","004_Pager - Red","005_Pager - Red","006_Pager - Red","007_Pager - Green","008_Pager - Green","009_Pager - Green","011_Zoom 5 - Blue","012_Zoom 5 - Blue","013_Zoom 5 - Blue","014_Zoom 5 - Red","015_Zoom 5 - Red","016_Zoom 5 - Red","017_Zoom 5 - Green","018_Zoom 5 - Green","019_Zoom 5 - Green","021_Zoom 7 - Blue","022_Zoom 7 - Blue","023_Zoom 7 - Blue","024_Zoom 7 - Red","025_Zoom 7 - Red","026_Zoom 7 - Red","027_Zoom 7 - Green","028_Zoom 7 - Green","029_Zoom 7 - Green","031_Topic Bar - Blue","032_Topic Bar - Blue","033_Topic Bar - Blue","034_Topic Bar - Red","035_Topic Bar - Red","036_Topic Bar - Red","037_Topic Bar - Green","038_Topic Bar - Green","039_Topic Bar - Green","041_Quads - Blue","042_Quads - Blue","043_Quads - Blue","044_Quads - Red","045_Quads - Red","046_Quads - Red","047_Quads - Green","048_Quads - Green","049_Quads - Green"))
      ("Gary", "gbrandenburg", List("QModel-gadgets-orig01","GadgetsToSave-02","GadgetsToSave-01","scroll bar test 01","Gradation Interaction 01","Event Flashing Test 01","inner glow- bug test 03","simulation test 01 save","Q01 Inst-Fdbk-works","ProTemplate-ZoomTest-01","Q-templates-g07-works","Q05-09-works","Q04-04-save-works","Q-templates-g06-works","PRO-004_Pager MOD1 - V5g6","PRO-004_Pager MOD1 - V5g5","PRO-004_Pager MOD1 - V5g3","PRO-001_Pager - V5g1","QLayout-A30","QLayout-A29","QLayout-A27","QLayout-A25-save","QLayout-A21","QLayout-A16-save","QLayout-A12-save","Question templates works","Q01 Inst-FBK-g01-works","QModel-g15-save-works","QModel-g18-works","Q03-14-save-works","Q03-16-works","Q04-10-works","Q05-08","PowerOnOff-01","ScrollBar Problems 01","QModel-g00","scrollbar test 2012-072"))
//      ("Foo", "Bar", List())

)

    val publishedNames = List(
//      ("Transfer", List("GOO", "inflight emergency complete", "CCAF 101", "The Outs and Ins of ZebraZapps"))

    )

//    for (namesForUser <- names; (sourceUser:String, destUser:String, projectNames) <- Some(namesForUser)) {
//      println("From "+sourceUser+" To "+destUser)
//      for ( projectName:String <- projectNames) {
//        println("    "+projectName)
//      }
//
//    }

    for (namesForUser <- names; (sourceUser:String, destUser:String, projectNames) <- Some(namesForUser)) {
      println("Published From "+sourceUser+" To "+destUser)
      for ( projectName:String <- projectNames) {
        println("    "+projectName)
      }

    }

    val results = new mutable.HashMap[Tuple2[String, String], Tuple2[mutable.Queue[String], mutable.Queue[String]]]

    for (namesForUser <- names; (sourceUser:String, destUser:String, projectNames) <- Some(namesForUser); projectName:String <- projectNames) {
      val key = (sourceUser, destUser)
      val (success, failure) = results.get(key) match {
        case Some(lists) => lists
        case None => {
          val lists = (new mutable.Queue[String](), new mutable.Queue[String]())
          results.put(key, lists)
          lists
        }
      }
      try {

        val pro = sessionQA(5004)
        val production = session(5005)
        println("ATTEMPTING TRANSFER: "+sourceUser+"->"+destUser+": ["+projectName+"]")
        Migrate1204to1204.migrateProject(sourceUser, projectName, pro, destUser, production)
        success += projectName
//        Upgrade1203Content.upgradeAndMigrate(sourceUser, projectName, pro, destUser, production)
      } catch {
        case t => {
          println("TRANSFER ERROR: " + t.toString)
          var t2 = t
          while(t2.getCause != null) {
            t2 = t2.getCause
            println("Caused by: " + t2.toString)
          }
          println(t2.getStackTraceString)
          failure += projectName
        }
      }
    }

//    for (namesForUser <- publishedNames; (sourceUser:String, destUser:String, projectNames) <- Some(namesForUser); projectName:String <- projectNames) {
//      val key = (sourceUser, destUser)
//      val (success, failure) = results.get(key) match {
//        case Some(lists) => lists
//        case None => {
//          val lists = (new mutable.Queue[String](), new mutable.Queue[String]())
//          results.put(key, lists)
//          lists
//        }
//      }
//      try {
//
//        val pro = sessionQA(5004)
//        val production = session(5005)
//        println("ATTEMPTING PUBLISHED TRANSFER: "+sourceUser+"->"+destUser+": ["+projectName+"]")
//        Migrate1204to1204.migratePublished(projectName, pro, destUser, production)
//        success += projectName
////        Upgrade1203Content.upgradeAndMigrate(sourceUser, projectName, pro, destUser, production)
//      } catch {
//        case t => {
//          println("TRANSFER ERROR: " + t.toString)
//          var t2 = t
//          while(t2.getCause != null) {
//            t2 = t2.getCause
//            println("Caused by: " + t2.toString)
//          }
//          println(t2.getStackTraceString)
//          failure += projectName
//        }
//      }
//    }

    println("Successful Transfers:")
    for((key, lists) <- results) {
      val (source, dest) = key
      val (success, _) = lists
      println((source, dest, success.toList))
    }
    println("Failed Transfers:")
    for((key, lists) <- results) {
      val (source, dest) = key
      val (_, failure) = lists
      println((source, dest, failure.toList))
    }

//    var failedPublished = List[String]()
//    for (namesForUser <- publishedNames; (destUser:String, publishedNames) <- Some(namesForUser); publishedName:String <- publishedNames) {
//      try {
//        val staging = session(5003)
//        val qa = sessionQA(5002)
//        println("ATTEMPTING TRANSFER: to "+destUser+": ["+publishedName+"]")
//        Upgrade1203Content.upgradeAndMigratePublished(publishedName, staging, destUser, qa)
//      } catch {
//        case t => {
//          println("TRANSFER ERROR: " + t.toString)
//          var t2 = t
//          while(t2.getCause != null) {
//            t2 = t2.getCause
//            println("Caused by: " + t2.toString)
//          }
//          println(t2.getStackTraceString)
//          failedPublished = publishedName :: failedPublished
//        }
//      }
//    }
//    println("Failed: " + failed)
//    println("Failed Published: " + failedPublished)

//    val userName = "jcoyle"
//    val projectName = "oval"
//    Migrate1204to1204.migrate(userName, projectName, session(5432), session(5432))


//    Upgrade1203Content.upgradeAll(session(5005), () => { session(5005)})

//    Upgrade1203Content.upgradeAll(session(5003), () => { session(5003)})
  }
}
