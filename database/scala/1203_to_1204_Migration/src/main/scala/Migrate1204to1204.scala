import ProjectSource.{LookupProjectSource, LookupProjectPublished}
import collection.mutable
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.{SessionFactory, Session}
import Common._

object Migrate1204to1204 {

  def migratePublished(publishedName:String, source:Session, destUser:String, destination:Session) = {
    migrate(LookupProjectPublished(publishedName), source, destUser, destination)
  }

  def migrateProject(sourceUser:String, projectName:String, source:Session, destUser:String, destination:Session) = {
    migrate(LookupProjectSource(sourceUser, projectName), source, destUser, destination)
  }

  def migrate(projectSource:ProjectSource.ProjectSource, source:Session, destUser:String, destination:Session) {use(List(source, destination)) {

    var convertProject:Projects1204 = null
    var state:Option[GadgetState1204] = None
    var children:Map[String, Projects1204] = null
    var parentChildren:List[ParentChild1204] = null
    var previewImages:List[PreviewImage1204] = null
    var publishImages:List[PublishImages] = null
    var assets:List[Asset1204] = null
    var parsedContent:Option[Any] = null
    var parsedInitial:Option[Any] = null
    var parsedCurrent:Option[Any] = null

    using(source) {

//      org.squeryl.Session.currentSession.setLogger(println)
      convertProject = projectSource.project

      state = from(Db1204.gadget_state)(gs =>
        where(
          gs.root_project_id === convertProject.projectid
          and gs.app_user_id === convertProject.authormemberid
          and gs.author_mode === true

        )
        select(gs)
      ).headOption

      state match {
        case Some(s) => println("Found state id: "+s.id)
        case _ => println("Didn't Find any state")
      }

      publishImages = from(Db1204.publishimages)(pi =>
        where(pi.projectid === Some(convertProject.projectid))
        select(pi)
      ).toList
      println("Found publishImages: "+publishImages.map(pi => List(pi.screenshotfileid, pi.iconfileid).flatten.head))

      children = getChildren(convertProject)
      parentChildren = getParentChildren(children.keys)

      previewImages = from(Db1204.previewimage)(pi =>
        where(pi.projectid in children.keys)
        select(pi)
      ).toList

      val previewAssetIds = previewImages.map(pi => pi.assetid)
      println("Found preview assets: "+previewAssetIds)

      parsedContent = Common.parse(convertProject.content)
//      println("-------------------------------------------")
//      println("parsedContent:\n"  + Common.renderPretty(parsedContent))
//      println("-------------------------------------------")
      val assetIds = Common.parseAssetsFromLibrary(parsedContent)
      println("Found library assets: "+assetIds)

      parsedInitial = Common.parse(convertProject.initialvalues.get)
//      println("-------------------------------------------")
//      println("parsedInitial:\n"  + Common.renderPretty(parsedInitial))
//      println("-------------------------------------------")

      // some older projects dont have initialValues
      // newer projects do not have initial values
      val initialAssetIds = Common.parseAssetsFromValues(parsedInitial)
      println("Found initialValue assets: "+initialAssetIds)

//      convertProject.initialvalues = jsontostring(parsedInitial)


      val (stateLibraryAssetIds, currentAssetIds) = state match {
        case Some(s) =>  {
          parsedCurrent = Common.parse(s.content)
//          println("-------------------------------------------")
//          println("parsedCurrent:\n"  + Common.renderPretty(parsedCurrent))
          (Common.parseAssetsFromLibrary(parsedCurrent), Common.parseAssetsFromValues(parsedCurrent))
        }
        case _ => {
          println("WARNING: failed to find state for project")
          parsedCurrent = parsedInitial
          (Set(), Set())
        }
      }
      println("Found currentValue assets: "+ stateLibraryAssetIds + ", " + currentAssetIds)

      val childrenAssetIds = children.values.filter(_ != convertProject).flatMap(child => {
        val childInitial = Common.parse(child.initialvalues)
        val childContent = Common.parse(child.content)
        val result = Common.parseAssetsFromLibrary(childContent) ++ Common.parseAssetsFromValues(childInitial)
        println("Found children assets projectid "+child.projectid+" assets "+result)
        result
      })

      val allAssetIds = initialAssetIds ++ stateLibraryAssetIds ++ currentAssetIds ++ assetIds ++ previewAssetIds ++ childrenAssetIds

      println("Looking up assets: " + allAssetIds)

      if(!allAssetIds.isEmpty) {
        assets = from(Db1204.asset)(a =>
          where(a.id in allAssetIds)
            select(a)
        ).toList

        for(asset <- assets) {
          println("Found asset id: "+asset.id+" fileid: "+asset.fileid+" name: "+asset.name)
        }

        val missingIds = allAssetIds.diff(assets.map(_.id).toSet)
        if(missingIds.nonEmpty) {
          throw new Error("ERROR missing asset ids: " + missingIds)
        }
      }else{
        assets = List[Asset1204]()
      }
    }

   // val hasInitialValues = Common.ValueFor("initialValues")
   // val hasValueChildren = Common.ValueFor("valueChildren")
  //  val hasEventFlow = Common.ValueFor("eventFlow")
  //  val hasProject = Common.ValueFor("project")
  //  val hasValueChildrenLists = Common.ValueFor("valueChildrenLists")
  //  val hasMasterPages = Common.ValueFor("masterPages")
  //  val hasPages = Common.ValueFor("pages")
  //  val hasValues = Common.ValueFor("values")

//    println("-------------------------------------------")
//    println("Project")
//    println(parsedCurrent match {
//      case Some(hasValueChildren(hasProject(project:Map[String, Any]))) => Common.renderCode(project).get
//      case _ => "ERROR NOT FOUND"
//    })
//    println("-------------------------------------------")
//    println("Master Page")
//    println(parsedCurrent match {
//      case Some(hasInitialValues(hasValueChildren(hasEventFlow(hasValueChildrenLists(hasMasterPages(List(masterPage:Map[String, Any]))))))) => Common.renderCode(masterPage).get
//      case _ => "ERROR NOT FOUND"
//    })
//    println("-------------------------------------------")
//    println("Project values")
//    println(parsedCurrent match {
//      case Some(hasInitialValues(hasValueChildren(hasProject(project:Map[String, Any])))) => Common.renderCode(project).get
//      case _ => "ERROR NOT FOUND"
//    })
//    println("-------------------------------------------")
//    println("EventFlow values")
//    println(parsedCurrent match {
//      case Some(hasInitialValues(hasValueChildren(hasEventFlow(hasValues(values:Map[String, Any]))))) => Common.renderCode(values).get
//      case _ => "ERROR NOT FOUND"
//    })
//    println("-------------------------------------------")
//    println("EventFlow Initial values")
//    println(parsedInitial match {
//      case Some(hasValueChildren(hasEventFlow(hasValues(values:Map[String, Any])))) => Common.renderCode(values).get
//      case _ => "ERROR NOT FOUND"
//    })
//    println("-------------------------------------------")
//    println("Event1 values")
//    println(parsedCurrent match {
//      case Some(hasInitialValues(hasValueChildren(hasEventFlow(hasValueChildrenLists(hasPages(List(hasValues(values:Map[String, Any])))))))) => Common.renderCode(values).get
//      case _ => "ERROR NOT FOUND"
//    })
//    println("-------------------------------------------")
//    println("Event1 Initial values")
//    println(parsedInitial match {
//      case Some(hasValueChildren(hasEventFlow(hasValueChildrenLists(hasPages(List(hasValues(values:Map[String, Any]))))))) => Common.renderCode(values).get
//      case _ => "ERROR NOT FOUND"
//    })
//    println("-------------------------------------------")
//    println("World values")
//    println(parsedCurrent match {
//      case Some(hasInitialValues(hasValues(values:Map[String, Any]))) => Common.renderCode(values).get
//      case _ => "ERROR NOT FOUND"
//    })
//    println("-------------------------------------------")

    using(destination) {
      migrateProjectFull(destUser, convertProject, parsedCurrent, assets, parentChildren, children, previewImages)
    }
  }}

  def migrateProjectFull(userName:String, convertProject:Projects1204, parsedCurrent:Option[Any], assets:List[Asset1204], parentChildren:List[ParentChild1204], projects:Map[String, Projects1204], previewImages:List[PreviewImage1204]) {
    val newUser = findUser(userName)

    //TODO lookup user account

    var undoSql = new mutable.MutableList[String]()

    val assetIdMap = migrateAssets(assets, newUser, undoSql)
    val projectIdMap = new mutable.HashMap[String, String]()

    val neededChildren = generateNeededProjects(newUser, assetIdMap, projectIdMap, childrenFirst(parentChildren), projects, undoSql)

    migrateProjects(convertProject, parsedCurrent, neededChildren, assetIdMap, projectIdMap, newUser, undoSql)

    migrateParentChildLinks(parentChildren, projectIdMap.toMap, undoSql)

    migratePreviewImages(previewImages, assetIdMap, projectIdMap.toMap, newUser, undoSql)

    println("Undo SQL:")
    for(s <- undoSql.reverse) {
      println(s)
    }
  }

  def getChildren(convertProject:Projects1204) = {


    val children = join(Db1204.parent_child, Db1204.projects)((pc, p) =>
      where(pc.parent_children_id === convertProject.projectid)
      select(p)
      on(pc.child_id === p.projectid)
    ).distinct.toList

    val origChildrenIds = children.map(_.projectid)

    val usedChildren = new mutable.HashSet[String]()
    usedChildren ++= usedProjectsFromContent(parse(convertProject.content))
    usedChildren ++= children.flatMap(child => usedProjectsFromContent(parse(child.content)))

    var extraChildren:List[Projects1204] = List()
    var extraChildrenIds:List[String] = List()
    var checkForExtraChildren = usedChildren.nonEmpty
    while(checkForExtraChildren) {
      val brokenChildren = join(Db1204.parent_child, Db1204.projects)((pc, p) =>
        where((pc.parent_children_id in (origChildrenIds ++ extraChildrenIds)) and (pc.child_id in (usedChildren)) and not (pc.child_id in (origChildrenIds ++ extraChildrenIds)))
        select(p)
        on(pc.child_id === p.projectid)
      ).distinct.toList

      extraChildren = brokenChildren ++ extraChildren
      usedChildren ++= brokenChildren.flatMap(child => usedProjectsFromContent(parse(child.content)))
      extraChildrenIds = brokenChildren.map(_.projectid) ++ extraChildrenIds
      checkForExtraChildren = brokenChildren.nonEmpty
    }



    val childrenMap = Map(convertProject.projectid -> convertProject) ++ (children ++ extraChildren).map(c => (c.projectid -> c))

    for(child <- children) {
      println("Found child id: "+child.projectid+" name: "+child.projectname)
    }
    for(child <- extraChildren) {
      println("Found extra child id: "+child.projectid+" name: "+child.projectname)
    }

    childrenMap
  }

  def getParentChildren(projectIds:Iterable[String]) = {
    val parentChildren = from(Db1204.parent_child)((pc) =>
      where((pc.parent_children_id in projectIds) and (pc.child_id in projectIds))
      select(pc)
    ).distinct.toList

    val parentsOf = parentChildren.groupBy(_.child_id).mapValues(_.map(_.parent_children_id).toSet)
    val additional = new mutable.HashSet[ParentChild1204]()

    for(childId <- childrenFirst(parentChildren) if parentsOf contains childId; parentId <- parentsOf(childId) if parentsOf contains parentId; grandParentId <- parentsOf(parentId)) {
      if(!(parentsOf(childId) contains grandParentId)) {
        additional.add(ParentChild1204(grandParentId, childId))
      }
    }

    for(pc <- parentChildren) {
      println("Found parent_child id: "+pc.parent_children_id+" child: "+pc.child_id)
    }
    for(pc <- additional) {
      println("Creating parent_child id: "+pc.parent_children_id+" child: "+pc.child_id)
    }
    parentChildren ++ additional
  }

  def replaceIds(project:Projects1204, newUser:User, assetIdMap:Map[String, String], projectIdMap:mutable.HashMap[String, String], undoSql: mutable.MutableList[String]) = {
    val parsedContent = Common.parse(project.content)
//        println("-------------------------------------------")
//        println("parsedContent: "+project.projectname+"\n"  + Common.renderPretty(parsedContent))
//        println("-------------------------------------------")
    val parsedInitial = Common.parse(project.initialvalues.get)
//        println("-------------------------------------------")
//        println("parsedInitial: "+project.projectname+"\n"  + Common.renderPretty(parsedInitial))
//        println("-------------------------------------------")

    val convertedContent = Common.replaceIds(parsedContent, assetIdMap, projectIdMap.toMap, projectContent = true)
    val convertedInitial = Common.replaceIds(parsedInitial, assetIdMap, projectIdMap.toMap, projectContent = false)

    val renderedContent = Common.render(convertedContent).get
    val renderedInitial = Common.render(convertedInitial).get

    val hash = Common.sha1hex(renderedContent) + Common.sha1hex(renderedInitial)

    val existingId = from(Db1204.projects)(p =>
      where(p.authormemberid === newUser.id
        and p.projectname === project.projectname
        and Common.sha1hex(p) === hash
      )
      select(p.projectid)
    ).headOption

    def projectType(p:Projects1204):String = p.projecttypeid match {
      case 1 => "gadget "
      case 2 => "project"
      case 3 => "event  "
      case _ => "unknown"
    }

    existingId match {
      case Some(id) => {
        println("Found existing project id: " + id + " from "  + project.projectid+ " " + projectType(project)+ " '" + project.projectname + "'")
        projectIdMap.put(project.projectid, id)
        None
      }
      case None => {
        val id = Common.randomUUID
        val newProject = project.copy(projectid = id, content = renderedContent, initialvalues = Some(renderedInitial), authormemberid = newUser.id, accountid = newUser.accountId, nextversionid = None)
        println("Inserting project id: " + newProject.projectid + " from "  + project.projectid+ " " + projectType(project)+ " '" + project.projectname + "'")
        undoSql += ("-- delete from projects where projectid = '" + newProject.projectid + "'; --  "+projectType(newProject)+" '" + newProject.projectname + "'")
//        println(Common.renderPretty(Common.parse(newProject.content)))
        projectIdMap.put(project.projectid, id)
        Some(newProject)
      }
    }
  }

  def childrenFirst(l:List[ParentChild1204]):List[String] = if(l == Nil) List() else {

    // gather all projects that have children
    val parents = l.map(_.parent_children_id).distinct

    // split list into elements that represent lowest level children
    // and all the others
    val (remaining, leaves) = l.partition(pc => parents.contains(pc.child_id))

    // return lowest children first followed by the rest reprocessed in the same way
    leaves.map(pc => pc.child_id).distinct ++ childrenFirst(remaining)
  }

  def generateNeededProjects(newUser:User, assetIdMap:Map[String, String], projectIdMap: mutable.HashMap[String, String], projectIds:List[String], projectMap:Map[String, Projects1204], undoSql: mutable.MutableList[String]) = {
    projectIds flatMap {childId =>
      replaceIds(projectMap(childId), newUser, assetIdMap, projectIdMap, undoSql)
    }
  }

  def migrateAssets(assets:List[Asset1204], newUser:User, undoSql: mutable.MutableList[String]) = {
    val existingAssetList = assets.flatMap({asset =>
      from(Db1204.asset)(a =>
        where(a.fileid === asset.fileid and a.user_id === newUser.id)
        select(a.fileid, a.id)
        orderBy((a.id eq asset.id).desc, a.date_created.asc)
      ).headOption
    })
    val existingAssets = existingAssetList.toMap

    val assetIdMap = assets.flatMap( asset => existingAssets.get(asset.fileid) match {
      case Some(id:String) if(asset.id == id) => {
        println("*** Mapping asset id: "+asset.id+" fileid: "+asset.fileid+" name: "+asset.name+" to existing asset id: "+id)
        None
      } // don't add to the map, it exists
      case Some(id:String) => {
        println("Mapping asset id: "+asset.id+" fileid: "+asset.fileid+" name: "+asset.name+" to existing asset id: "+id)
        Some((asset.id, id))
      } // add to the map with the found id
      case None => {
        val newId = Common.randomUUID
        println("Mapping asset id: "+asset.id+" fileid: "+asset.fileid+" name: "+asset.name+" to new asset id: "+newId)
        Some((asset.id, newId))
      } // add to the map with a new randomId
    }).toMap

    val neededAssets = assets
      .filter(asset => !(existingAssets.contains(asset.fileid)))
      .map(a => a.copy(id = assetIdMap(a.id), user_id = newUser.id))

    for(asset <- neededAssets) {
      println("Inserting asset id: "+asset.id+" fileid: "+asset.fileid+" name: "+asset.name)
      undoSql += ("-- delete from asset where id = '" + asset.id + "' -- '" + asset.name + "';")
    }
    if(neededAssets.nonEmpty) {
      modification("insert assets") {
        Db1204.asset.insert(neededAssets)
      }
      undoSql += ("delete from asset where id in (" + neededAssets.map(a => "'" + a.id + "'").reduce((a1, a2) => a1 + ", " + a2) + "); -- All assets")
    }

    assetIdMap
  }

  def migrateProjects(convertProject:Projects1204, parsedCurrent:Option[Any], neededChildren:List[Projects1204], assetIdMap:Map[String, String], projectIdMap:mutable.HashMap[String, String], newUser:User, undoSql: mutable.MutableList[String]) {
    val parsedContent = Common.parse(convertProject.content)
    val parsedInitial = Common.parse(convertProject.initialvalues)
    val convertedContent = Common.replaceIds(parsedContent, assetIdMap, projectIdMap.toMap, projectContent = true)
    val convertedInitial = Common.replaceIds(parsedInitial, assetIdMap, projectIdMap.toMap, projectContent = false)
    val renderedContent = Common.render(convertedContent).get
    val renderedInitial = Common.render(convertedInitial).get
    val projectId = Common.randomUUID

    val newProject = convertProject.copy(projectid = projectId, content = renderedContent, initialvalues = Some(renderedInitial)
      , nextversionid = None, authormemberid = newUser.id, accountid = newUser.accountId, published=false, featured = Some(false), guestlistid = None
    )
    projectIdMap.put(convertProject.projectid, newProject.projectid)
    val neededProjects = (newProject :: (neededChildren.map(p => p.copy(published = false, featured = Some(false), guestlistid = None)))).map(p => p.copy(nextversionid = Some(p.projectid), nextpublishedversionid = Some(p.projectid)))


    val prevVersions = from(Db1204.projects)(p =>
      where(p.authormemberid === newUser.id
        and p.projectname === newProject.projectname
        and p.projecttypeid === newProject.projecttypeid
        and p.nextversionid === None
        and p.retired === false
        and p.projectid <> newProject.projectid)
      select(p.projectid)
    ).toList

    val prevPublishedVersions = if(newProject.published) {
      from(Db1204.projects)(p =>
        where(p.urlname === newProject.urlname
          and p.urlname <> None
          and p.urlname <> Some("")
          and p.projecttypeid === newProject.projecttypeid
          and p.nextpublishedversionid === None
          and p.published === true
          and p.retired === false
          and p.projectid <> newProject.projectid)
        select(p.projectid)
      ).toList
    } else {
      Nil
    }

    println("Inserting Main project id: " + newProject.projectid + " name: " + newProject.projectname)
//    println(Common.renderPretty(Common.parse(newProject.content)))
    undoSql += ("-- delete from projects where projectid = '" + newProject.projectid + "'; -- main project '" + newProject.projectname + "'")
    undoSql += ("delete from projects where projectid in (" + neededProjects.map(p => "'" + p.projectid + "'").reduce((p1, p2) => p1 + ", " + p2) + "); -- all Projects")
    modification("Insert projects") {
      Db1204.projects.insert(neededProjects)
    }



    if(prevVersions.nonEmpty) {
      println("Setting nextversionid to " + newProject.projectid)
      undoSql += ("update projects set nextVersionId = null where projectid in (" + prevVersions.map(p => "'" + p + "'").reduce((p1, p2) => p1 + ", " + p2) + "); -- previous head versions")
      modification("Update projects nextversionid") {
        update(Db1204.projects)(p =>
          where(p.projectid in prevVersions)
          set(p.nextversionid := Some(newProject.projectid))
        )
      }
    }

    if(prevPublishedVersions.nonEmpty) {
      println("Setting nextpublishedversionid to " + newProject.projectid)
      undoSql += ("update projects set nextpublishedversionid = null where projectid in (" + prevPublishedVersions.map(p => "'" + p + "'").reduce((p1, p2) => p1 + ", " + p2) + "); -- previous head versions")
      modification("Update projects nextpublishedversionid") {
        update(Db1204.projects)(p =>
          where(p.projectid in prevPublishedVersions)
          set(p.nextpublishedversionid := Some(newProject.projectid))
        )
      }
    }

    println("Updating project nextversionid and nextpublishedversionid to null")
    undoSql += ("update projects set nextversionid = projectid, nextpublishedversionid = projectid where projectid in (" + neededProjects.map(p => "'" + p.projectid + "'").reduce((p1, p2) => p1 + ", " + p2) + "); -- all Projects")
    modification("Update nextversionid and nextpublishedversionid") {
      update(Db1204.projects)(p =>
        where(p.projectid in neededProjects.map(_.projectid))
        set(p.nextpublishedversionid := None, p.nextversionid := None)
      )
    }

    createPublishedExtras(newProject, newUser, undoSql)

    if(parsedCurrent.nonEmpty) {
      val convertedCurrent = Common.replaceIds(parsedCurrent, assetIdMap, projectIdMap.toMap, projectContent = true)
      val renderedCurrent = Common.render(convertedCurrent).get
      val stateId = Common.randomUUID
      val newState = GadgetState1204(id = stateId, version = Some(0L), app_user_id = newUser.id, author_mode = true, content = renderedCurrent, root_project_id = projectId, externaluserid = None)
      println("Inserting State id: " + newState.id)
      undoSql += ("delete from gadget_state where id = '" + newState.id + "'; -- main project author state")
      modification("Insert gadget_state") {
        Db1204.gadget_state.insert(newState)
      }
    }
  }

  def migrateParentChildLinks(parentChildren:List[ParentChild1204], projectIdMap:Map[String, String], undoSql: mutable.MutableList[String]) {
    val candidateLinks = parentChildren.map(_ match {
      case ParentChild1204(parent, child) => ParentChild1204(projectIdMap(parent), projectIdMap(child))
    })

    val existingLinks = from(Db1204.parent_child)(pi =>
      where(pi.parent_children_id in projectIdMap.keys)
      select(pi.parent_children_id, pi.child_id)
    ).toMap

    val neededLinks = candidateLinks.filter(_ match {
      case ParentChild1204(parent, child) => existingLinks.getOrElse(parent, "") != child
    })

    if(neededLinks.nonEmpty) {
      for(pc <- neededLinks) {
        println("Inserting parent_child parent: " + pc.parent_children_id + " child: " + pc.child_id)
        undoSql += ("-- delete from parent_child where parent_children_id = '" + pc.parent_children_id + "' and child_id = '" + pc.child_id + "';")
      }
      undoSql += ("delete from parent_child where parent_children_id in (" + neededLinks.map(pc => "'" + pc.parent_children_id + "'").distinct.mkString(",") + ");")
      modification("Insert parent_child links") {
        Db1204.parent_child.insert(neededLinks)
      }
    }
  }

  def migratePreviewImages(previewImages:List[PreviewImage1204], assetIdMap:Map[String, String], projectIdMap:Map[String, String], newUser:User, undoSql: mutable.MutableList[String]) {

    val existingPreviewImages = from(Db1204.previewimage)(pi =>
      where(pi.projectid in projectIdMap.values)
      select(pi.projectid, pi.assetid)
    ).toMap

    val neededPreviewImages = previewImages.flatMap(pi => {
      val project = projectIdMap.get(pi.projectid).get
      val asset = assetIdMap.get(pi.assetid).get

      existingPreviewImages.get(project) match {
        case None => Some(pi.copy(projectid = project, assetid = asset))
        case Some(a:String) if(a == asset) => None // it's already there
        case Some(a:String) => {
          println("WARNING: project "+project+" associated with asset "+a+" already instead of desired "+asset)
          None
        }
      }
    })
    if(neededPreviewImages.nonEmpty) {
      for(pi <- neededPreviewImages) {
        println("Inserting previewimage project: "+pi.projectid+" asset: "+pi.assetid)
        undoSql += ("-- delete from previewimage where projectid = '" + pi.projectid + "' and assetid = '" +pi.assetid + "';")
      }
      undoSql += ("delete from previewimage where projectid in (" + neededPreviewImages.map(pi => "'"+pi.projectid+"'").reduce((p1, p2) => p1 + ", " + p2) + "); -- event images")
      modification("Insert previewimage") {
        Db1204.previewimage.insert(neededPreviewImages)
      }
    }
  }

  def createPublishedExtras(newProject:Projects1204, newUser:User, undoSql: mutable.MutableList[String]) {
    if(newProject.published) {
      modification("Insert APN") {
        undoSql += ("delete from accountprojectnumbers where projectid = '" + newProject.projectid + "'")
        Db1204.accountprojectnumbers.insert(AccountProjectNumbers(accountid = newUser.accountId, projectid = Some(newProject.projectid)))
      }
    }
  }



//  def getProjects(userName:String) = join(Db1204.app_user,
//    Db1204.accountmemberroles,
//    Db1204.accounts,
//    Db1204.projects) ((au, amr, acc, p) =>
//    where(
//      au.email === userName
//        and au.retired === false
//        and au.enabled === true
//        and p.retired === false
//        and p.nextversionid <> null
//      //and p.projecttypeid < 3
//    )
//      select(p.projectname)
//      on(p.authormemberid === amr.memberid)
//  ).toList
}