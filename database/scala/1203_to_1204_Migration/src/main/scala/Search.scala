import net.liftweb.json._
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.Session
import scala.collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 11/7/12
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
object Search {

  def singleObjectProjects(source:Session) {

    def convert(json:JValue):Any = json match {
        case JInt(x) => x
        case JDouble(x) => x
        case JString(s) => s
        case JBool(x) => x
//        case j: JValue if (targetType == classOf[JValue]) => j
        case JObject(fields) => fields.map({case JField(k, v) => (k, convert(v))}).toMap
        case JArray(values) => values.map(convert)
        case JNull => null
        case JNothing => throw new Exception("Unknown value")
        case JField(k, x) => (k, x)
        case _ => throw new Exception("Unknown value")
      }

    def rep[A](seq: Seq[A]) = {
      def inner(proj: Seq[A]): Stream[A] = {
        if (proj.isEmpty)
          inner(seq)
        else
          Stream.cons(proj.first, inner(proj drop 1))
      }

      if (seq.isEmpty)
        Stream.empty
      else
        inner(seq)
    }

    def process(l:List[Tuple4[String, String, String, String]], f:(String, String, String, String) => Option[Tuple4[String, String, String, String]]) {

      println("start time")
      val start = System.currentTimeMillis
      var ovals = rep(l).take(5000) flatMap {
        case (displayname, projectid, projectname, content) => f(displayname, projectid, projectname, content)
      } toList
//      for( i <- (0 until 10)) {
//        ovals = l flatMap {
//          case (displayname, projectid, projectname, content) => f(displayname, projectid, projectname, content)
//        }
//      }
      val end = System.currentTimeMillis
      println("Time = " + ((end-start)/1000.0) + " seconds")

      println("start list " + ovals.length)
      for(p <- ovals) {
        println(p)
      }
//      println(ovals.length + ", " + ovals.headOption.getOrElse("ERROR"))
      println("end list")
    }

    class PartialThen[A, B](a:PartialFunction[A, B]) {
      def pThen[C](b:PartialFunction[B, C]):PartialFunction[A, C] = new PartialFunction[A, C] {
          def isDefinedAt(x: A): Boolean = a.isDefinedAt(x) && b.isDefinedAt(a(x))
          def apply(x: A): C = b(a(x))
        }
    }

    implicit def PartialThenWrapper[A,B](a:PartialFunction[A,B]):PartialThen[A, B] = new PartialThen(a)

//    def jerkins(displayname:String, projectid:String, projectname:String, content:String): Option[Tuple4[String, String, String, String]] = {
////      print("Checking " + projectname)
//      val c = com.codahale.jerkson.Json.parse[java.util.Map[String, Any]](content)
////      println(" parsed")
//      val children:PartialFunction[Any, Any] = {case m:java.util.Map[String, Any] => m.get("children")}
//      ( children pThen {
//        case l:java.util.List[Any] if (l.size == 1) => l.get(0)
//      } pThen {
//        case child:java.util.Map[String, Any] => child.get("className")
//      } pThen {
//        case name:String => name
//      } andThen {
//        (displayname, projectid, projectname, _)
//      }) lift c
//    }

    def liftweb(displayname:String, projectid:String, projectname:String, content:String): Option[Tuple4[String, String, String, String]] = {
//      print("Checking " + projectname)
      val c = net.liftweb.json.parse(content)
//      println(" parsed")
      val res = for {
        JField("children", JArray(children)) <- c.children
        if children.length == 1
        child @ JObject(_) <- children
        JField("className", JString(name)) <- child.children
      } yield (displayname, projectid, projectname, name)
      res.headOption
    }

    def liftwebMap(displayname:String, projectid:String, projectname:String, content:String): Option[Tuple4[String, String, String, String]] = {
//      print("Checking " + projectname)
      val c = convert(net.liftweb.json.parse(content))
      c match {
        case m:collection.Map[String, Any] => m.get("children") match {
          case Some((child:Map[String, Any]) :: Nil) => child.get("className") match {
            case Some(name:String) => Some((displayname, projectid, projectname, name))
            case _ => None
          }
          case _ => None
        }
        case _ => None
      }
    }

//    def liftwebMapFor(displayname:String, projectid:String, projectname:String, content:String): Option[Tuple4[String, String, String, String]] = {
////      print("Checking " + projectname)
//      val c = convert(net.liftweb.json.parse(content))
////      println(" parsed")
//      val res = for {
//        m:collection.Map[String, Any] <- List(c)
//        children:List[Any] <- m.get("children")
//        if children.length == 1
//        child:Map[String, Any] <- children.headOption
////        (child:Map[String, Any]) :: Nil <- m.get("children")
//        Some(name:String) <- child.get("className")
//      } yield (displayname, projectid, projectname, name)
//      res.headOption
//    }

    def liftwebMapOptionThen(displayname:String, projectid:String, projectname:String, content:String): Option[Tuple4[String, String, String, String]] = {
//      print("Checking " + projectname)

      class OptionThen(a:Option[Any]) {
        def andThen[C](b:PartialFunction[Any, C]):Option[C] = a match {
          case Some(x) => if(b isDefinedAt(x)) Some(b(x)) else None
          case _ => None
        }
      }

      implicit def OptionThenWrapper(a:Option[Any]):OptionThen = new OptionThen(a)

      val c = convert(net.liftweb.json.parse(content))
      val ret = Some(c) andThen {
        case m:collection.Map[String, Any] => m.get("children")
      } andThen {
        case Some((child:Map[String, Any]) :: Nil) => child.get("className")
      } andThen {
        case Some(name:String) => (displayname, projectid, projectname, name)
      }
      ret
    }

    def liftwebMapFlatMap(displayname:String, projectid:String, projectname:String, content:String): Option[Tuple4[String, String, String, String]] = {
//      print("Checking " + projectname)

      val c = convert(net.liftweb.json.parse(content))
      val ret = Some(c) flatMap {
        case m:collection.Map[String, Any] => m.get("children")
        case _ => None
      } flatMap {
        case (child:Map[String, Any]) :: Nil => child.get("className")
        case _ => None
      } flatMap {
        case name:String => Some((displayname, projectid, projectname, name))
        case _ => None
      }
      ret
    }

    def liftwebMapCollect(displayname:String, projectid:String, projectname:String, content:String): Option[Tuple4[String, String, String, String]] = {
//      print("Checking " + projectname)

      val c = convert(net.liftweb.json.parse(content))
      val ret = Some(c).collect({
        case m:collection.Map[String, Any] => m.get("children")
      }).flatten.collect({
        case (child:Map[String, Any]) :: Nil => child.get("className")
      }).flatten.collect({
        case name:String => Some((displayname, projectid, projectname, name))
      }).flatten
      ret.headOption
    }

    def liftwebMapUnapply(displayname:String, projectid:String, projectname:String, content:String): Option[Tuple4[String, String, String, String]] = {
//      print("Checking " + projectname)
      val hasChildren = Common.ValueFor("children")
      val hasClassName = Common.ValueFor("className")

      val c = convert(net.liftweb.json.parse(content))
      Some(c) collect {
        case hasChildren(List(hasClassName(name:String))) => (displayname, projectid, projectname, name)
      }
    }

    def setter(content:String) = {
//      print("Checking " + projectname)
      val hasChildren = Common.ValueFor("children")
      val hasCustomAnchors = Common.ValueFor("customAnchors")

      val c = Common.parse(content)
      c collect {
        case hasChildren(List(hasCustomAnchors(l:List[Map[String, Any]]))) => l.find(a => a.get("customModifier") == None && a.contains("inValue"))
      }
    }

//    def parseAssetsFromValues(p:Option[Any]):Set[String] = {
//      val assetIds = new mutable.HashSet[String]() // all found assets
//      val unparsed = new mutable.Stack[Any]() // all json objects yet to search
//      p match {
//        // seed with the root if we have one
//        case Some(value) => unparsed.push(value)
//        case _ => {}
//      }
//      while(!unparsed.isEmpty) {
//        // take next value off the stack to parse through
//        unparsed.pop() match {
//          case m:Map[String, Any] => m.get("type") match {
//            case Some("ProjectReference") => m.get("id") match {
//              // map has type:AssetReference or type:Asset, so check for id
//              case Some(Common.uuidRegex(id:String)) => {
//                assetIds.add("ProjectRef: id)
//              }
//              case _ => m.get("assetID") match {
//                // map doesnt have a valid id field, so check assetID
//                case Some(Common.uuidRegex(id:String)) => {
//                  assetIds.add(id)
//                }
//                case _ => // probably invalid asset, ignore
//              }
//            }
//            case _ => for(value <- m.values) {
//              // non special map, so process its values
//              unparsed.push(value)
//            }
//          }
//          case l:List[Any] => for(value <- l) {
//              // non special List, so process its values
//            unparsed.push(value)
//          }
//          case _ => // ignore other types such as number or string
//        }
//
//      }
//      assetIds.toSet
//    }

//    def liftwebMapUnapply(displayname:String, projectid:String, projectname:String, content:String): Option[Tuple4[String, String, String, String]] = {
////      print("Checking " + projectname)
//      case class ValueFor(key: String) {
//        def unapply(map: Map[String,Any]): Option[Any] = map.get(key)
//      }
//      val hasChildren = ValueFor("children")
//      val hasClassName = ValueFor("className")
//      val c = convert(net.liftweb.json.parse(content))
//      c match {
//        case hasChildren(children) => children match {
//          case (child :: Nil) => child match {
//            case hasClassName(name:String) => Some((displayname, projectid, projectname, name))
//            case _ => None
//          }
//          case _ => None
//        }
//        case _ => None
//      }
//    }


    using(source) {

    val names = for( i <- (1 until 22) ) yield i.toString.reverse.padTo(6, "0").reverse.mkString("") + "_jesse_XXX"

    val names2 = (names ++ names.map(_+"_mod")).toList

    println(names2)

    val projectList = join(Db1204.projects, Db1204.app_user)((p, a) =>
      where(
        p.nextversionid.isNull
          and (p.gadgetsize lt 10000)
//          and a.displayname === "dsobolewski"
//          and p.projectname === "Other Gadgets"
          and p.projecttypeid === 2
          and p.nextversionid === None
          and p.retired === false
//          and (p.projectname like "000%_jesse_%")
//          and p.projectid === "23a9901073894ac999e7b4d4173241de"
//          and p.projectid === "9427313647a849c882d5328971421b85"
      )
        select(a.displayname, p.projectname, p.content)
        orderBy(p.projectname)
      on(p.authormemberid === a.id)
    )

      for((user, name, content) <- projectList) {
        setter(content) match {
          case Some(m) => {
            println("User: " + user + " name: " + name + " anchor: " + m)
          }
          case _ =>
        }
//        println("------------ CONTENT -------------------")
//        println(Common.renderPretty(Common.parse(p.content)))
//        println("------------ INITIAL VALUES-------------")
//        println(Common.renderPretty(Common.parse(p.initialvalues)))
//        println("---------------- END -------------------")
      }


//      for(p <- projectList) {
//        p match {
//          case (displayname, projectid, projectname, content) => liftwebMapUnapply(displayname, projectid, projectname, content) match {
//            case Some(v) => {
//              println(v)
////              println(Common.renderPretty(Common.parse(content)))
//            }
//            case _ =>
//          }
//          case _ =>
//        }
//      }

//      process(projectList, oneAsset)

//      process(projectList, liftweb)
//      process(projectList, liftwebMapFlatMap)
//      process(projectList, liftwebMapCollect)
//      process(projectList, liftwebMapOptionThen)
//      process(projectList, liftwebMapUnapply)
////      process(projectList, liftwebMapFor)
//      process(projectList, liftwebMap)
//      process(projectList, liftweb)
//      process(projectList, jerkins)
  }
  }
}
