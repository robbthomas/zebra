import org.squeryl.PrimitiveTypeMode._
import org.squeryl.Session
import util.parsing.json.JSON

object Migrate1203to1204 {

  def convert(a:AppUser1203, id:String):AppUser1204 = new AppUser1204(id, a.version, a.date_created, a.retired, a.description, a.email, a.emailshow, a.enabled, a.last_updated, a.passwordhash, a.displayname,
  a.editedbyid, a.retiredbyid, a.retiredbydatetime, a.passwordsalt, a.firstname, a.lastname, a.website, a.twitter, a.nameformat)

  def convert(a:Parent1203, id:String, content:String):Projects1204 = new Projects1204() // TO BE IMPLEMENTED

  /**
   * Class to allow converstion from Option[Any] to one of List[Any] or List[ Map[String,Any] ]
   * @param a value to be converted
   */
  class OptionAny(a:Option[Any]) {
    def asList:List[Any] = a match {
      case Some(l:List[Any]) => l
      case _ => List()
    }
    def asListOfMap:List[Map[String,Any]] = a match {
      case Some(l:List[Map[String,Any]]) => l
      case _ => List()
    }
    def asMap:Map[String,Any] = a match {
      case Some(m:Map[String,Any]) => m
      case _ => Map()
    }
  }

  implicit def optionAnyWrapper(a:Option[Any]):OptionAny = new OptionAny(a)

  def makeProjectContent(oldContent:Map[String, Any], eventProjectId:String):Map[String, Any] = {
    def eventProject = Map(
                    "className" -> "EventPage",
                    "gadget" -> Map(
                      "type" -> "ProjectReference",
                      "id" -> eventProjectId
                    )
                  )
    Map(
      "children" -> List[Any](), // empty world children
      "valueChildren" -> (Map(
        "eventFlow" -> Map(
          "valueChildrenLists" -> Map(
            "masterPages" -> List(
              Map(
                "className" -> "EventPage"
              )
            ),
            "pages" -> List(
              eventProject
            )
          ),
          "children" -> List[Any]() // empty backstage
        )
      ) ++ (oldContent.get("project") match {
        case None => Map()
        case Some(m:Map[String, Any]) => Map("project" -> m)
      })),
      "wiring" -> projectWiring(oldContent.get("wiring").asList),
      "gadgets" -> (eventProject :: oldContent.get("gadgets").asList), // inherit from child
      "assets" -> oldContent.get("assets").asList // inherit from child
    )
  }

  //TODO populate with half wires from those to the ProjectMC
  def projectWiring(wiring:List[Any]):List[Any] = List()

  //TODO replace ProjectMC wires with half wires to the edge
  def eventWiring(wiring:List[Any]):List[Any] = wiring

  def makeEventContent(oldContent:Map[String, Any]):Map[String, Any] = Map(
    "children" -> oldContent.get("children").asListOfMap.map(convertChild),
    "wiring" -> eventWiring(oldContent.get("wiring").asList),
    "gadgets" -> oldContent.get("gadgets").asList,
    "assets" -> oldContent.get("assets").asList
  )

  def convertChild(child:Map[String, Any]):Map[String, Any] = child //TODO convert a child element recursively

  def migrate(userName:String, projectName:String, source:Session, destination:Session) {

    using(source) {
          //      org.squeryl.Session.currentSession.setLogger(println)

      val (email, userId) = from(Db1203.app_user)(a =>
        where(
          a.displayname === userName
          and a.retired === false
          and a.enabled === true
        )
        select((a.email, a.id))
      ).single

      println(email, userId)

      val projectList = from(Db1203.parent)(p =>
        where(
          p.nextversionid.isNull
          and p.ownerid === userId
          and p.projecttypeid === 2
        )
        select((p.id, p.name))
      )

      for(p <- projectList) {
        println(p)
      }

      val convertProject = from(Db1203.parent)(p =>
        where(
          p.nextversionid.isNull
          and p.ownerid === userId
          and p.projecttypeid === 2
          and p.name === projectName
        )
        select(p)
      ).single

      println("Found project id: "+convertProject.id)

      val state = from(Db1203.gadget_state)(gs =>
        where(
          gs.root_gadget_id === convertProject.id
          and gs.app_user_id === userId
          and gs.author_mode === true

        )
        select(gs)
      ).single

      println("Found state id: "+state.id)

      val parsedContent = JSON.parseFull(convertProject.content)
      println("parsedContent: " + parsedContent)
      val parsedCurrent = JSON.parseFull(state.content)

      val eventProjectId = ""

      val projectContent = makeProjectContent(parsedContent asMap, eventProjectId)
      println("projectContent: " + projectContent)
    }
  }


}