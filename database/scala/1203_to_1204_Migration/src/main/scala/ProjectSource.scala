import Common._
import org.squeryl.PrimitiveTypeMode._
import scala.Some
import scala.Some

/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 1/8/13
 * Time: 8:03 AM
 * To change this template use File | Settings | File Templates.
 */
object ProjectSource {
  abstract class ProjectSource {
    def project:Projects1204;
  }

  case class PlainProjectSource(p:Projects1204) extends ProjectSource {
    def project:Projects1204 = p
  }

  case class LookupProjectSource(sourceUser:String, projectName:String) extends ProjectSource {
    def project:Projects1204 = {
        val user = findUser(sourceUser)

        println("Found user: "+sourceUser+" email: "+user.email+" id: "+user.id)

        val convertProject = from(Db1204.projects)(p =>
          where(
            p.nextversionid.isNull
              and p.authormemberid === user.id
              and p.projecttypeid === 2
              and p.projectname === projectName
              and p.retired === false
          )
            select(p)
        ).single

        println("Found project: "+projectName+" id: "+convertProject.projectid)
        convertProject
      }
  }

  case class LookupProjectPublished(publishedName:String) extends ProjectSource {
    def project:Projects1204 = {

        val convertProject = from(Db1204.projects)(p =>
          where(
            p.nextpublishedversionid.isNull
              and p.projecttypeid === 2
              and p.publishedname === Some(publishedName)
              and p.published === true
              and p.retired === false
          )
            select(p)
        ).single

        println("Found project: "+publishedName+" id: "+convertProject.projectid)
        convertProject
      }
  }
}
