import java.sql.Timestamp
import org.squeryl.{KeyedEntity, Schema}
import scala.Some
import org.squeryl.annotations.Column


case class AppUser1204(
  id: String = "",
  version: Long = 0L,
  date_created: Timestamp = new Timestamp(0L),
  retired: Boolean = false,
  description: Option[String] = Some(""),
  email: String = "",
  emailshow: Boolean = false,
  enabled: Boolean = false,
  last_updated: Timestamp = new Timestamp(0L),
  passwordhash: String = "",
  displayname: String = "",
  editedbyid: Option[String] = Some(""),
  retiredbyid: Option[String] = Some(""),
  retiredbydatetime: Option[Timestamp] = Some(new Timestamp(0L)),
  passwordsalt: String = "",
  firstname: Option[String] = Some(""),
  lastname: Option[String] = Some(""),
  website: Option[String] = Some(""),
  twitter: Option[String] = Some(""),
  nameformat: String = ""
) {
  def this() = this("")
}

case class AccountMemberRoles1204(
  accountid:Long = 0L,
  memberid:String = "",
  typeid:Option[Int] = Some(0),
  retired:Option[Int] = Some(0),
  retiredbyid:Option[String] = Some(""),
  retireddatetime:Option[Timestamp] = Some(new Timestamp(0L))
) {
  def this() = this(0L)
}

class Accounts1204(
  val accountid:Long = 0L,
  val companyid:Option[Int] = Some(0),
  val typeid:Int = 0,
  val termduedatetime:Option[Timestamp] = Some(new Timestamp(0L)),
  val goodstanding:Int = 0,
  val name:Option[String] = Some(""),
  val description:Option[String] = Some(""),
  val tokenbalance:Option[Int] = Some(0),
  val editedbyid:String = "",
  val editeddatetime:Option[Timestamp] = Some(new Timestamp(0L)),
  val retired:Option[Int] = Some(0),
  val retiredbyid:Option[String] = Some(""),
  val retireddatetime:Option[Timestamp] = Some(new Timestamp(0L)),
  val showcompanyinfoforall:Option[Int] = Some(0),
  val companysameaspersonal:Option[Int] = Some(0),
  val paypallogin:Option[String] = Some(""),
  val payaccountfirst:Option[Int] = Some(0),
  val convertamount:Option[Int] = Some(0),
  val merchantcustomerid:String = "",
  val convertmethod:Option[String] = Some(""),
  val contactpreference:Option[String] = Some(""),
  val allowsubscriptionpayment:Int = 0,
  val maxinvitesperproject:Int = 0,
  val maxinvitesperaccount:Int = 0,
  val numlearners:Option[Int] = Some(0),
  val nummembers:Option[Int] = Some(0),
  val billingcontactsameaspersonal:Int = 0,
  val billingaddresssameascompany:Int = 0
) {
  def this() = this(0L)
}

class Projects1204(
  val projectid: String = "",
  val version: Long = 0L,
  val content: String = "",
  val createddatetime: Timestamp = new Timestamp(0L),
  val retired: Boolean = false,
  val initialvalues: Option[String] = Some(""),
  val editeddatetime: Timestamp = new Timestamp(0L),
  val projectmetadata: Option[String] = Some(""),
  val projectname: String = "",
  val nextversionid: Option[String] = Some(""),
  val authormemberid: String = "",
  val dep_gadgetrefid: Option[String] = Some(""),
  val initialvalue: Option[String] = Some(""),
  val dep_instanceid: Option[String] = Some(""),
  val projecttypeid: Int = 0,
  val gadgetsize: Option[Int] = Some(0),
  val autosave: Boolean = false,
  val dep_publishid: Option[Long] = Some(0L),
  val published: Boolean = false,
  val accountid: Long = 0L,
  val versionmajor: Option[Int] = Some(0),
  val versionminor: Option[Int] = Some(0),
  val versiondot: Option[Int] = Some(0),
  val versionbuildnumber: Option[Int] = Some(0),
  val categoryid: Option[Long] = Some(0L),
  val description: Option[String] = Some(""),
  val statusid: Option[Int] = Some(0),
  val licensetypeid: Option[Int] = Some(0),
  val price: Option[Int] = Some(0),
  val currencytypeid: Option[Int] = Some(0),
  val hideinstorelist: Option[Boolean] = Some(false),
  val featured: Option[Boolean] = Some(false),
  val publishedmetadata: Option[String] = Some(""),
  val selfprice: Option[Int] = Some(0),
  val inviteonly: Boolean = false,
  val guestlistid: Option[Long] = Some(0L),
  val dep_publishedretired: Option[Boolean] = Some(false),
  val width: Int = 0,
  val height: Int = 0,
  val nextpublishedversionid: Option[String] = Some(""),
  val accounttypeid: Option[Int] = Some(0),
  val urlname: Option[String] = Some(""),
  val publishedname: Option[String] = Some("")
) {
  def this() = this("") // force explicit default constructor
  def copy(projectid:String=this.projectid, content:String=this.content, initialvalues:Option[String]=this.initialvalues, nextversionid:Option[String]=this.nextversionid, authormemberid:String=this.authormemberid, published:Boolean=this.published, accountid:Long=this.accountid, hideinstorelist:Option[Boolean]=this.hideinstorelist, featured:Option[Boolean]=this.featured, guestlistid:Option[Long]=this.guestlistid, width:Int=this.width, height:Int=this.height, nextpublishedversionid:Option[String]=this.nextpublishedversionid) =
    new Projects1204(projectid, version, content, createddatetime, retired, initialvalues, editeddatetime, projectmetadata, projectname, nextversionid, authormemberid, dep_gadgetrefid, initialvalue, dep_instanceid, projecttypeid, gadgetsize, autosave, dep_publishid, published, accountid, versionmajor, versionminor, versiondot, versionbuildnumber, categoryid, description, statusid, licensetypeid, price, currencytypeid, hideinstorelist, featured, publishedmetadata, selfprice, inviteonly, guestlistid, dep_publishedretired, width, height, nextpublishedversionid, accounttypeid, urlname, publishedname)
}

case class GadgetState1204(
  id:String = "",
  version:Option[Long] = Some(0L),
  app_user_id:String = "",
  author_mode:Boolean = false,
  content:String = "",
  root_project_id:String = "",
  externaluserid:Option[String] = Some("")
) {
  def this() = this("") // force explicit default constructor
}

case class ParentChild1204(
  parent_children_id:String = "",
  child_id:String = ""
) {
  def this() = this("") // force explicit default constructor
}

case class Asset1204(
  id: String = "",
  version: Option[Long] = Some(0L),
  data_size_in_bytes: Long = 0L,
  date_created: Timestamp = new Timestamp(0L),
  deleted: Boolean = false,
  fileid: String = "",
  last_updated: Timestamp = new Timestamp(0L),
  mime_type: String = "",
  name: String = "",
  next_version_id: Option[String] = Some(""),
  permissions: String = "",
  user_id: String = ""
){
  def this() = this("") // force explicit default constructor
}

case class PreviewImage1204(
// temporarily cut out autoincremented id field, as squeryl requires they are named id. We aren't using it here anyway
  //previewimageid: Int = 0,
  projectid: String = "",
  assetid: String = ""
) {
  def this() = this("") // force explicit default constructor
}

case class ProjectMigrationStaging(
  id:Int = 0,
  projectid:String = "",
  parentlevel:Option[Int] = Some(0),
  migrationstarteddatetime:Option[Timestamp] = Some(new Timestamp(0L)),
  migrationcompletedatetime:Option[Timestamp] = Some(new Timestamp(0L)),
  migrationsuccess:Option[Boolean] = Some(false),
  content:Option[String] = Some(""),
  initialvalues:Option[String] = Some(""),
  currentvalues:Option[String] = Some(""),
  eventprojectid:Option[String] = Some(""),
  eventcontent:Option[String] = Some(""),
  eventvalues:Option[String] = Some(""),
  width:Option[Int] = Some(0),
  height:Option[Int] = Some(0)
) extends KeyedEntity[Int] {
  def this() = this(0)
}

case class AccountProjectNumbers(
  accountid:Long = 0,
  projectid:Option[String] = Some("")
) {
  def this() = this(0)
}

case class PublishImages (
  screenshotfileid:Option[String] = Some(""),
  iconfileid:Option[String] = Some(""),
  renderingcomplete:Option[Boolean] = Some(false),
  displayorder:Option[Int] = Some(0),
  retired:Option[Boolean] = Some(false),
  projectid:Option[String] = Some("")
) {
  def this() = this(Some(""))
}

object Db1204 extends Schema {
  val app_user = table[AppUser1204]("app_user")
  val accountmemberroles = table[AccountMemberRoles1204]("accountmemberroles")
  val accounts = table[Accounts1204]("accounts")
  val projects = table[Projects1204]("projects")
  val gadget_state = table[GadgetState1204]("gadget_state")
  val parent_child = table[ParentChild1204]("parent_child")
  val asset = table[Asset1204]("asset")
  val previewimage = table[PreviewImage1204]("previewimage")
  val projectmigrationstaging = table[ProjectMigrationStaging]("projectmigrationstaging")
  val accountprojectnumbers = table[AccountProjectNumbers]("accountprojectnumbers")
  val publishimages = table[PublishImages]("publishimages")
}