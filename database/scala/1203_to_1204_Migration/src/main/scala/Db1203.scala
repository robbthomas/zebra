import java.sql.Timestamp
import org.squeryl.Schema

class AppUser1203(
  val id: String,
  val version: Long,
  val date_created: Timestamp,
  val retired: Boolean,
  val description: Option[String],
  val email: String,
  val emailshow: Boolean,
  val enabled: Boolean,
  val last_updated: Timestamp,
  val passwordhash: String,
  val displayname: String,
  val editedbyid: Option[String],
  val retiredbyid: Option[String],
  val retiredbydatetime: Option[Timestamp],
  val passwordsalt: String,
  val firstname: Option[String],
  val lastname: Option[String],
  val website: Option[String],
  val twitter: Option[String],
  val nameformat: String
) {
  def this() = this("", 0L, new Timestamp(0L), false, Some(""), "", false, false, new Timestamp(0L), "", "", Some(""), Some(""), Some(new Timestamp(0L)), "", Some(""), Some(""), Some(""), Some(""), "")
}

class Parent1203(
  val id: String,
  val version: Long,
  val content: String,
  val date_created: Timestamp,
  val deleted: Boolean,
  val initialvalues: Option[String],
  val last_updated: Timestamp,
  val meta_data: String,
  val name: String,
  val nextversionid: Option[String],
  val price: Option[Int],
  val ownerid: String,
  val index: Option[Int],
  val gadgetrefid: Option[String],
  val initialvalue: Option[String],
  val instanceid: Option[String],
  val currencytypeid: Option[Int],
  val projecttypeid: Int,
  val gadgetsize: Int,
  val autosave: Option[Boolean]
) {
  def this() = this("", 0, "", new Timestamp(0), false, Some(""), new Timestamp(0), "", "", Some(""), Some(0), "", Some(0), Some(""), Some(""), Some(""), Some(0), 0, 0, Some(false))
}

class GadgetState1203(
  val id:String,
  val version:Long,
  val app_user_id:String,
  val author_mode:Boolean,
  val content:String,
  val root_gadget_id:String,
  val externaluserid:Option[String]
) {
  def this() = this("", 0L, "", false, "", "", Some(""))
}

object Db1203 extends Schema {
  val app_user = table[AppUser1203]("app_user")
  val parent = table[Parent1203]("parent")
  val gadget_state = table[GadgetState1203]("gadget_state")
}