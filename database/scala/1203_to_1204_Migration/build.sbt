name := "query"

version := "0.1"

scalaVersion := "2.9.2"

resolvers += Classpaths.typesafeResolver


libraryDependencies ++= Seq(
  "org.squeryl" %% "squeryl" % "0.9.5-2"
)
