-------------------------------------------------------------------------------
-- r98_to_r99.sql
--
-- Author:  Joe Bodell
-- Date:  10-10-2012
-- Notes:  Removes the tag columnb on the S3Bucket table -- it is unnecessary
--
-------------------------------------------------------------------------------

ALTER TABLE UploadableAssetS3Bucket DROP COLUMN tag;

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (99);
------------------------------------------------------