--------------------------------------------------------------------------------
-- fn_contactpreferencetypes.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- ContactPreferenceTypes functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='ContactPreferenceTypes';

select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ContactPreferenceTypes','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_contactpreferencetype_create(text, text, text, text, integer)

-- DROP FUNCTION fn_contactpreferencetype_create(text, text, text, text, integer);

CREATE OR REPLACE FUNCTION fn_contactpreferencetype_create(in_credentials_email text, in_credentials_password text, in_tag text, in_name text, in_displayOrder integer)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ContactPreferenceTypes';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_contactPreferenceTypeId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO ContactPreferenceTypes(
            tag,
            name,
            displayOrder
        )
        VALUES(
            in_tag,
            in_name,
            in_displayOrder
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_contactPreferenceTypeId := currval('seq_ContactPreferenceTypes_contactPreferenceTypeId');
        END IF;

        return new_contactPreferenceTypeId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_contactpreferencetype_create(text, text, text, text, integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_contactpreferencetype_update(text, text, integer, text, text, integer, text)

-- DROP FUNCTION fn_contactpreferencetype_update(text, text, integer, text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_contactpreferencetype_update(in_credentials_email text, in_credentials_password text, in_contactPreferenceTypeId integer, in_tag text, in_name text, in_displayOrder integer, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ContactPreferenceTypes';
        var_functionName text := 'UPDATE';

        rec ContactPreferenceTypes%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from ContactPreferenceTypes where contactPreferenceTypeId = in_contactPreferenceTypeId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_tag' THEN
                rec.tag := in_tag;
            ELSEIF param_name = 'in_name' THEN
                rec.name := in_name;
            ELSEIF param_name = 'in_displayOrder' THEN
                rec.displayOrder := in_displayOrder;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE ContactPreferenceTypes SET
            tag = rec.tag,
            name = rec.name,
            displayOrder = rec.displayOrder,
            updatedDateTime = now()
        WHERE contactPreferenceTypeId = in_contactPreferenceTypeId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_contactpreferencetype_update(text, text, integer, text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_contactpreferencetype_retire(text, text, integer, text)

-- DROP FUNCTION fn_contactpreferencetype_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_contactpreferencetype_retire(in_credentials_email text, in_credentials_password text, in_contactPreferenceTypeId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ContactPreferenceTypes';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE ContactPreferenceTypes SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        WHERE contactPreferenceTypeId = in_contactPreferenceTypeId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_contactpreferencetype_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_contactpreferencetype_filter(text, text, integer, integer, text, text, text, integer, text)

-- DROP FUNCTION fn_contactpreferencetype_filter(text, text, integer, integer, text, text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_contactpreferencetype_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_tag text, in_name text, in_displayOrder integer, in_filter_list text)
  RETURNS SETOF ContactPreferenceTypes AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ContactPreferenceTypes';
        var_functionName text := 'FILTER';

        rec ContactPreferenceTypes%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM ContactPreferenceTypes WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_contactPreferenceTypeId' THEN
            ELSEIF filter_name = 'in_tag' THEN
                var_query := var_query || ' AND tag ILIKE ''%' || in_tag || '%''';
            ELSEIF filter_name = 'in_name' THEN
                var_query := var_query || ' AND name ILIKE ''%' || in_name || '%''';
            ELSEIF filter_name = 'in_displayOrder' THEN
                var_query := var_query || ' AND displayOrder = ' || in_displayOrder;
            ELSEIF filter_name = 'in_createdDateTime' THEN
            ELSEIF filter_name = 'in_updatedDateTime' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM ContactPreferenceTypes;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_contactpreferencetype_filter(text, text, integer, integer, text, text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_contactpreferencetype_find(text, text, integer)

-- DROP FUNCTION fn_contactpreferencetype_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_contactpreferencetype_find(in_credentials_email text, in_credentials_password text, in_contactPreferenceTypeId integer)
  RETURNS SETOF ContactPreferenceTypes AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ContactPreferenceTypes';
        var_functionName text := 'FIND';

        rec ContactPreferenceTypes%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM ContactPreferenceTypes
                WHERE contactPreferenceTypeId = in_contactPreferenceTypeId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_contactpreferencetype_find(text, text, integer) OWNER TO postgres;
