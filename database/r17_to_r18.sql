-------------------------------------------------------------------------------
-- r17_to_r18.sql
--
-- Add Leads and LeadItemValue tables.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (18);
-------------------------------------------------------------------------------

CREATE SEQUENCE seq_Leads_leadId START WITH 20000;

CREATE TABLE Leads (
    leadId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_Leads_leadId'),
    leadSource text,
    leadDateTime timestamp DEFAULT now()
);

CREATE INDEX ix_Leads_leadSource ON Leads(leadSource);


CREATE SEQUENCE seq_LeadItemValue_leadItemValueId START WITH 21000;

CREATE TABLE LeadItemValue (
    leadItemValueId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_LeadItemValue_leadItemValueId'),
    leadId bigint NOT NULL,
    item text,
    value text,

    CONSTRAINT fk_LeadsItemValue_leadId FOREIGN KEY
    (leadId) REFERENCES Leads(leadId)
);

CREATE INDEX ix_LeadItemValue_item ON LeadItemValue(item);
CREATE INDEX ix_LeadItemValue_value ON LeadItemValue(value);


-- make sure new functions get loaded too - NOTE that this will be repeated in
-- all_functions.sql but is tossed in here to facilitate development
\i fn_leads.sql


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
