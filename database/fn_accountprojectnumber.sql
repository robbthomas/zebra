--------------------------------------------------------------------------------
-- fn_accountprojectnumbers.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- AccountProjectNumbers functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='AccountProjectNumbers' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('AccountProjectNumbers','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_accountprojectnumber_create(text, text, integer, integer, integer, integer)

DROP FUNCTION IF EXISTS fn_accountprojectnumber_create(text, text, integer, integer, integer, integer); -- drop publishId version

CREATE OR REPLACE FUNCTION fn_accountprojectnumber_create(in_credentials_email text, in_credentials_password text, in_accountId integer, in_projectId text, in_invoiceLineItemId integer, in_retiredReasonId integer)
  RETURNS text AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountProjectNumbers';
        var_functionName text := 'CREATE';

        new_accountProjectNumberId text := '';
        var_count int := -1;

    BEGIN
        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = in_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into AccountProjectNumbers(
            accountId,
            projectId,
            invoiceLineItemId
        )
        values(
            in_accountId,
            in_projectId,
            in_invoiceLineItemId
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_accountProjectNumberId := accountProjectNumberId FROM AccountProjectNumbers
                WHERE accountId = in_accountId AND projectId = in_projectId AND invoiceLineItemId = in_invoiceLineItemId;
        END IF;

        return new_accountProjectNumberId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_accountprojectnumber_create(text, text, integer, integer, integer, integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_accountprojectnumber_update(text, text, integer, integer, integer, integer, integer, text)

DROP FUNCTION IF EXISTS fn_accountprojectnumber_update(text, text, integer, integer, integer, integer, integer, text); -- drop publishId version

CREATE OR REPLACE FUNCTION fn_accountprojectnumber_update(in_credentials_email text, in_credentials_password text, in_accountProjectNumberId text, in_accountId integer, in_projectId text, in_invoiceLineItemId integer, in_retiredReasonId integer, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountProjectNumbers';
        var_functionName text := 'UPDATE';

        rec AccountProjectNumbers%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = in_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from AccountProjectNumbers where accountProjectNumberId = in_accountProjectNumberId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_accountId' THEN
                rec.accountId := in_accountId;
            ELSEIF param_name = 'in_projectId' THEN
                rec.projectId := in_projectId;
            ELSEIF param_name = 'in_invoiceLineItemId' THEN
                rec.invoiceLineItemId := in_invoiceLineItemId;
            ELSEIF param_name = 'in_retiredReasonId' THEN
                rec.retiredReasonId := in_retiredReasonId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE AccountProjectNumbers SET
            accountId = rec.accountId,
            projectId = rec.projectId,
            invoiceLineItemId = rec.invoiceLineItemId,
            retiredReasonId = rec.retiredReasonId
        WHERE accountProjectNumberId = in_accountProjectNumberId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_accountprojectnumber_update(text, text, integer, integer, integer, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_accountprojectnumber_retire(text, text, integer, text)

DROP FUNCTION IF EXISTS fn_accountprojectnumber_retire(text, text, integer, text); -- drop publishId version

CREATE OR REPLACE FUNCTION fn_accountprojectnumber_retire(in_credentials_email text, in_credentials_password text, in_accountProjectNumberId text, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountProjectNumbers';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = in_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE AccountProjectNumbers SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE accountProjectNumberId = in_accountProjectNumberId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_accountprojectnumber_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_accountprojectnumber_filter(text, text, integer, integer, text, integer, integer, integer, integer, text)

DROP FUNCTION IF EXISTS fn_accountprojectnumber_filter(text, text, integer, integer, text, integer, integer, integer, integer, text); -- drop publishId version

CREATE OR REPLACE FUNCTION fn_accountprojectnumber_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_accountId integer, in_projectId text, in_invoiceLineItemId integer, in_retiredReasonId integer, in_filter_list text)
  RETURNS SETOF AccountProjectNumbers AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountProjectNumbers';
        var_functionName text := 'FILTER';

        rec AccountProjectNumbers%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

            var_query := 'SELECT * FROM AccountProjectNumbers WHERE retired = 0' ||
                    ' AND accountId IN (SELECT amr.accountId FROM AccountMemberRoles amr, TableFunctionRoleMap tfr' ||
                    '   WHERE amr.memberId = ''' || var_credentialsId || '''' ||
                    '   AND amr.typeId = tfr.roleTypeId' ||
                    '   AND tfr.tableName = ''' || var_tableName || '''' ||
                    '   AND tfr.functionName = ''' || var_functionName || ''')';

            LOOP
                filter_name := split_part(in_filter_list, ',', var_count);
                filter_name := trim(both ' ' from filter_name);
                EXIT WHEN filter_name = '';

                IF filter_name = 'in_accountProjectNumberId' THEN
                ELSEIF filter_name = 'in_accountId' THEN
                    var_query := var_query || ' AND accountId = ' || in_accountId;
                ELSEIF filter_name = 'in_projectId' THEN
                    var_query := var_query || ' AND projectId = ''' || in_projectId || '''';
                ELSEIF filter_name = 'in_invoiceLineItemId' THEN
                    var_query := var_query || ' AND invoiceLineItemId = ' || in_invoiceLineItemId;
                ELSEIF filter_name = 'in_retired' THEN
                ELSEIF filter_name = 'in_retiredReasonId' THEN
                    var_query := var_query || ' AND retiredReasonId = ' || in_retiredReasonId;
                END IF;
                var_count := var_count + 1;
            END LOOP;

            IF LENGTH(in_order_list) > 0 THEN
                var_order := replace(in_order_list,':a',' ASC');
                var_order := replace(var_order,':d',' DESC');
                var_query := var_query || ' ORDER BY ' || var_order;
            END IF;

            var_count := in_count;
            IF var_count <= 0 THEN
                SELECT count(*) INTO var_count FROM AccountProjectNumbers;
            END IF;
            var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

            FOR rec IN
                EXECUTE var_query
            LOOP
                RETURN NEXT rec;
            END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_accountprojectnumber_filter(text, text, integer, integer, text, integer, integer, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_accountprojectnumber_find(text, text, integer)

DROP FUNCTION IF EXISTS fn_accountprojectnumber_find(text, text, integer); -- drop publishId version

CREATE OR REPLACE FUNCTION fn_accountprojectnumber_find(in_credentials_email text, in_credentials_password text, in_accountProjectNumberId text)
  RETURNS SETOF AccountProjectNumbers AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountProjectNumbers';
        var_functionName text := 'FIND';

        var_accountId int;
        rec AccountProjectNumbers%ROWTYPE;

    BEGIN
        var_accountId := accountId FROM AccountProjectNumbers WHERE accountProjectNumberId = in_accountProjectNumberId;

        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = var_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM AccountProjectNumbers
                WHERE accountProjectNumberId = in_accountProjectNumberId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_accountprojectnumber_find(text, text, integer) OWNER TO postgres;
