-------------------------------------------------------------------------------
-- create productcodes table 
-- create productcodes_prices table
-- alter invoice_lineitems to allow productcodes
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (63);


create table ProductCodes (
    ProductCodeId   int primary key not null,
    tag             text,
    name            text,
    description     text,
    displayorder    int,
    retired         int,
    retiredbyid     text,
    retireddatetime timestamp,
    retiredreasontypeid int references retiredreasontypes(retiredreasontypeid)
);

insert into ProductCodes(ProductCodeId, tag, name, description) values (100, 'ACCOUNT_COLLECTOR', 'Account Level Change', 'Change in account type for ZebraZapps.com');
insert into ProductCodes(ProductCodeId, tag, name, description) values (101, 'ACCOUNT_CREATOR', 'Account Level Change', 'Change in account type for ZebraZapps.com');
insert into ProductCodes(ProductCodeId, tag, name, description) values (102, 'ACCOUNT_PRO', 'Account Level Change', 'Change in account type for ZebraZapps.com');
insert into ProductCodes(ProductCodeId, tag, name, description) values (103, 'ACCOUNT_LEARNER', 'Account Learner Add/Remove', 'Add or remove LMS maximum number of learners for ZebraZapps.com account');
insert into ProductCodes(ProductCodeId, tag, name, description) values (104, 'ACCOUNT_MEMBER', 'Account Member Add/Remove', 'Add or remove maximum number of for from ZebraZapps.com account');
insert into ProductCodes(ProductCodeId, tag, name, description) values (105, 'SHOPP_PURCHASE', 'Shopp Purchase', 'Gadget or Zapp purchase from ZebraZapps.com Shopp');
insert into ProductCodes(ProductCodeId, tag, name, description) values (106, 'SHOPP_DISCARD', 'Remove Item From Collection', 'Delete a gadget or zapp from ZebraZapps.com collection');
insert into ProductCodes(ProductCodeId, tag, name, description) values (107, 'SHOPP_PUBLISH', 'Publish Zapp/Gadget', 'Publish a gadget or zapp to the ZebraZapps.com Shopp');
insert into ProductCodes(ProductCodeId, tag, name, description) values (108, 'SHOP_UNPUBLISH', 'Unpublish Zapp/Gadget', 'Remove a gadget or zapp from the ZebraZapps.com Shopp');
insert into ProductCodes(ProductCodeId, tag, name, description) values (109, 'SHOPP_DEPEND_GADGET', 'Purchase Dependent Gadgets', 'Purchase rights to a gadget embedded into a zapp purchased by this account');
insert into ProductCodes(ProductCodeId, tag, name, description) values (110, 'PROJECT_XFER', 'Accept Project Transfer', 'Accept the transfer of a project from one ZebraZapps.com account to another');


CREATE SEQUENCE seq_ProductCodePrices;

create table ProductCodePrices (
    productcodepriceid  bigint primary key not null DEFAULT nextval('seq_ProductCodePrices'),
    productcodeid       int references productcodes(productcodeid),
    price               int not null default 0,
    effectiveDate       date,
    termDate            date,
    currencyTypeId      int default 1 references currencytypes(currencytypeid)
);

insert into ProductCodePrices (productcodeid, price, effectivedate, termdate) values (103, 50, date '1970-01-01', date '2038-01-19');


alter table invoicelineitems add column productcodeid int references productcodes(productcodeid);
alter table invoicelineitems add column fromaccounttypeid int references accounttypes(accounttypeid);
alter table invoicelineitems add column toaccounttypeid int references accounttypes(accounttypeid);
alter table invoicelineitems add column numlearners int;
alter table invoicelineitems add column numaccountmembers int;

alter table accounts add column numlearners int;
alter table accounts add column nummembers int;
update accounts set numlearners = 25 where typeid = 3;

drop table if exists temp_invoicelineitems_Autohistory;

SELECT * INTO TEMPORARY TABLE temp_invoicelineitems_Autohistory FROM invoicelineitems_Autohistory;

-- Recreate autohistory for invoicelineitems.
DROP SEQUENCE seq_invoicelineitems_AutoHistory CASCADE;
DROP TABLE invoicelineitems_AutoHistory CASCADE;

CREATE SEQUENCE seq_invoicelineitems_AutoHistory;
CREATE TABLE invoicelineitems_AutoHistory (
    LIKE invoicelineitems,
    invoicelineitems_AutoHistory_Id bigint DEFAULT nextval('seq_invoicelineitems_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

insert into invoicelineitems_autohistory 
    (invoicelineitemid,
     invoiceid,
     linenumber,
     quantity,
     amounttotal,
     description,
     publishid,
     projectid,
     autohistorydate) 
    (
    select invoicelineitemid,
     invoiceid,
     linenumber,
     quantity,
     amounttotal,
     description,
     publishid,
     projectid,
     autohistorydate from temp_invoicelineitems_autohistory);
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;