-------------------------------------------------------------------------------
-- r43_to_r44.sql
--   add allowSubscriptionPayment column to Accounts.  
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

-- Add new column.
ALTER TABLE accounts ADD COLUMN allowSubscriptionPayment integer DEFAULT 0;
ALTER TABLE accounts ALTER COLUMN allowSubscriptionPayment SET NOT NULL;

-------------------------------------------------------------------------------
-- Update database version.
-------------------------------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES(44);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

