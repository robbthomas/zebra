-------------------------------------------------------------------------------
-- r97_to_r98.sql
--
-- Author:  Joe Bodell
-- Date:  10-10-2012
-- Notes:  Adds renderingComplete
--
-------------------------------------------------------------------------------

ALTER TABLE UploadableAsset ADD renderingComplete BOOLEAN NOT NULL DEFAULT FALSE;
--NOOP -- this is now handled in r101_to_r102.sql. ARGH.

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (98);
------------------------------------------------------