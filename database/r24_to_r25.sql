-------------------------------------------------------------------------------
-- r24_to_r25.sql
--
-- Add Accounts.convertMethod definitions and FK.
--
-- Add table index to PublishedPurchasedAcquisitions.cached...Count that
-- should have been added in r22_to_r23.sql.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (25);
-------------------------------------------------------------------------------

DROP TABLE IF EXISTS ConvertMethodTypes CASCADE;

CREATE TABLE ConvertMethodTypes (
    convertMethodTypeId text NOT NULL PRIMARY KEY,
    caption text NOT NULL,
    description text
);


INSERT INTO ConvertMethodTypes
VALUES ('convert_method_convert_all', 'Do not apply earnings towards account', 'Do not apply earnings towards account. Convert all earnings.');

INSERT INTO ConvertMethodTypes
VALUES ('convert_method_convert_less_account_fee', 'Always pay account fee first', 'Always pay account fee first. Then convert remaining earnings.');

INSERT INTO ConvertMethodTypes
VALUES ('convert_method_convert_specific_amount', 'Apply a specific amount from earning', 'Apply a specific amount from earning. Leave remaining earnings in account.');


-- Add FK to Accounts.convertMethod and set existing creator accounts to "convert all"

UPDATE Accounts SET convertMethod = 'convert_method_convert_all';

ALTER TABLE Accounts ALTER COLUMN convertMethod SET DEFAULT 'convert_method_convert_all';

ALTER TABLE Accounts ADD CONSTRAINT fk_Accounts_convertMethod
    FOREIGN KEY (convertMethod) REFERENCES ConvertMethodTypes(convertMethodTypeId);


-- Create index on PublishedPurchasesAcquisitions(cachedPurchaseAcquisitionCount)
-- to support future ranking according to MIN/MAX window and calculating according
-- to where they are in the spread.

CREATE INDEX ix_PublishedPurchasesAcquisitions_cachedPurchaseAcquisitionCount
    ON PublishedPurchasesAcquisitions(cachedPurchaseAcquisitionCount);


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

