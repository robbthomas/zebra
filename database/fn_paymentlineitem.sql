--------------------------------------------------------------------------------
-- fn_paymentlineitems.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- PaymentLineItems functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='PaymentLineItems'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('PaymentLineItems','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_paymentlineitem_create(text, text, integer, integer, integer, integer, text, integer)

DROP FUNCTION IF EXISTS fn_paymentlineitem_create(text, text, integer, integer, integer, integer, text, integer); -- drop publishId version

-- DROP FUNCTION IF EXISTS fn_paymentlineitem_create(text, text, integer, integer, integer, integer, text, text); -- newer projectId version

CREATE OR REPLACE FUNCTION fn_paymentlineitem_create(in_credentials_email text, in_credentials_password text, in_paymentId integer, in_lineNumber integer, in_quantity integer, in_amountTotal integer, in_description text, in_projectId text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'PaymentLineItems';
        var_functionName text := 'CREATE';

        new_paymentLineItemId int := -1;
        var_count int := -1;

        -- TBD: drop in_lineNumber from function args and instead set to MAX(lineNumber)+1 or 1
        -- based on SELECT MAX(lineNumber) FROM PaymentLineItems WHERE paymentId = in_paymentId
        var_lineNumber int;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_lineNumber := MAX(lineNumber) FROM PaymentLineItems WHERE paymentId = in_paymentId;
        IF var_lineNumber IS NULL THEN 
            var_lineNumber := 1; 
        END IF;

        insert into PaymentLineItems(
            paymentId,
            lineNumber,
            quantity,
            amountTotal,
            description,
            projectId
        )
        values(
            in_paymentId,
            var_lineNumber,
            in_quantity,
            in_amountTotal,
            in_description,
            in_projectId
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_paymentLineItemId := currval('seq_PaymentLineItems_paymentLineItemId');
        END IF;

        return new_paymentLineItemId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_paymentlineitem_create(text, text, integer, integer, integer, integer, text, integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_paymentlineitem_update(text, text, integer, integer, integer, integer, integer, text, integer, text)

DROP FUNCTION IF EXISTS fn_paymentlineitem_update(text, text, integer, integer, integer, integer, integer, text, integer, text); -- drop publishid version

-- DROP FUNCTION fn_paymentlineitem_update(text, text, integer, integer, integer, integer, integer, text, text, text); -- newer projectId version

CREATE OR REPLACE FUNCTION fn_paymentlineitem_update(in_credentials_email text, in_credentials_password text, in_paymentLineItemId integer, in_paymentId integer, in_lineNumber integer, in_quantity integer, in_amountTotal integer, in_description text, in_projectId text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'PaymentLineItems';
        var_functionName text := 'UPDATE';

        rec PaymentLineItems%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from PaymentLineItems where paymentLineItemId = in_paymentLineItemId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_paymentId' THEN
                rec.paymentId := in_paymentId;
            ELSEIF param_name = 'in_lineNumber' THEN
                rec.lineNumber := in_lineNumber;
            ELSEIF param_name = 'in_quantity' THEN
                rec.quantity := in_quantity;
            ELSEIF param_name = 'in_amountTotal' THEN
                rec.amountTotal := in_amountTotal;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_projectId' THEN
                rec.projectId := in_projectId;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE PaymentLineItems SET
            paymentId = rec.paymentId,
            lineNumber = rec.lineNumber,
            quantity = rec.quantity,
            amountTotal = rec.amountTotal,
            description = rec.description,
            projectId = rec.projectId
        WHERE paymentLineItemId = in_paymentLineItemId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_paymentlineitem_update(text, text, integer, integer, integer, integer, integer, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_paymentlineitem_retire(text, text, integer, text)

-- DROP FUNCTION fn_paymentlineitem_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_paymentlineitem_retire(in_credentials_email text, in_credentials_password text, in_paymentLineItemId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'PaymentLineItems';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE PaymentLineItems SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE paymentLineItemId = in_paymentLineItemId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_paymentlineitem_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_paymentlineitem_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, integer, text)

DROP FUNCTION IF EXISTS fn_paymentlineitem_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, integer, text); -- drop publishId version

-- DROP FUNCTION fn_paymentlineitem_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, text, text); -- new projectId version

CREATE OR REPLACE FUNCTION fn_paymentlineitem_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_paymentId integer, in_lineNumber integer, in_quantity integer, in_amountTotal integer, in_description text, in_projectId text, in_filter_list text)
  RETURNS SETOF PaymentLineItems AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'PaymentLineItems';
        var_functionName text := 'FILTER';

        rec PaymentLineItems%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        --var_query text := 'SELECT * FROM PaymentLineItems WHERE retired = 0';
        var_query text := 'SELECT * FROM PaymentLineItems WHERE 0 = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_paymentLineItemId' THEN
            ELSEIF filter_name = 'in_paymentId' THEN
                var_query := var_query || ' AND paymentId = ' || in_paymentId;
            ELSEIF filter_name = 'in_lineNumber' THEN
                var_query := var_query || ' AND lineNumber = ' || in_lineNumber;
            ELSEIF filter_name = 'in_quantity' THEN
                var_query := var_query || ' AND quantity = ' || in_quantity;
            ELSEIF filter_name = 'in_amountTotal' THEN
                var_query := var_query || ' AND amountTotal = ' || in_amountTotal;
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_projectId' THEN
                var_query := var_query || ' AND projectId = ''' || in_projectId || '''';
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM PaymentLineItems;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_paymentlineitem_filter(text, text, integer, integer, text, integer, integer, integer, integer, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_paymentlineitem_find(text, text, integer)

-- DROP FUNCTION fn_paymentlineitem_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_paymentlineitem_find(in_credentials_email text, in_credentials_password text, in_paymentLineItemId integer)
  RETURNS SETOF PaymentLineItems AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'PaymentLineItems';
        var_functionName text := 'FIND';

        rec PaymentLineItems%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM PaymentLineItems
                WHERE paymentLineItemId = in_paymentLineItemId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_paymentlineitem_find(text, text, integer) OWNER TO postgres;
