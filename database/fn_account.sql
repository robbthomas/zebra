--------------------------------------------------------------------------------
-- fn_accounts.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- Accounts functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Accounts' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('Accounts','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Accounts','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Accounts','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Accounts','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('Accounts','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Accounts','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_account_create(text, text, integer, text, timestamp, integer, text, text, integer, text)

-- DROP FUNCTION fn_account_create(text, text, integer, text, timestamp, integer, text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_account_create(in_credentials_email text, in_credentials_password text, in_companyId integer, in_type text, in_termDueDateTime timestamp, in_goodStanding integer, in_name text, in_description text, in_tokenBalance integer, in_showCompanyInfoForAll integer, in_contactPreference text, in_convertMethod text, in_convertAmount integer, in_paypalLogin text, in_allowSubscriptionPayment integer, in_renewalNoticeSent int, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Accounts';
        var_functionName text := 'CREATE';

        var_editedById text;
        new_accountId int := -1;
        new_accountTypeId integer;
        new_accountCompanyId integer; -- defaults to null

        index int;
        var_uuid text;
        new_merchantCustomerId text;
        new_accountRoleTypeId integer := -1;

        var_count int := -1;
        success int;

        var_price int;
        var_contactPreference text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- generate an as yet unused random UUID-based but only 20 chars long merchantCustomerId
        -- for new entry but try only 12 times before bailing and failing!?
        index := 1; -- postgresql strings first char position=1
        var_uuid := replace(cast(uuid_generate_v4() As text),'-','');
        LOOP
            new_merchantCustomerId := upper(substring(var_uuid from index for 20));
            IF NOT EXISTS (select merchantCustomerId from Accounts where merchantCustomerId = new_merchantCustomerId) THEN
                EXIT;
            END IF;
            index := index + 1;
            IF index > 12 THEN -- only have 12 20-char substrings in 32-char uuid derived string
                RETURN '';
            END IF;
        END LOOP;

        -- set editing user
        var_editedById := COALESCE(in_editedById,'');
        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        -- set contact preference if not empty
        IF LENGTH(in_contactPreference) > 0 THEN var_contactPreference = in_contactPreference; END IF;

        -- get type Id
        new_accountTypeId := accountTypeId FROM AccountTypes WHERE tag ILIKE in_type;

        -- allow null company Id
        IF in_companyId > 0 THEN new_accountCompanyId := in_companyId; END IF;

        insert into Accounts(
            companyId,
            typeId,
            termDueDateTime,
            goodStanding,
            name,
            description,
            tokenBalance,
            showCompanyInfoForAll,
            contactPreference,
            convertMethod,
            convertAmount,
            paypalLogin,
            editedById,
            merchantCustomerId,
            allowSubscriptionPayment,
            renewalNoticeSent
        )
        values(
            new_accountCompanyId,
            new_accountTypeId,
            in_termDueDateTime,
            in_goodStanding,
            in_name,
            in_description,
            in_tokenBalance,
            in_showCompanyInfoForAll,
            var_contactPreference,
            in_convertMethod,
            in_convertAmount,
            in_paypalLogin,
            var_editedById,
            new_merchantCustomerId,
            in_allowSubscriptionPayment,
            in_renewalNoticeSent
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_accountId := currval('seq_Accounts_accountId');
        END IF;

        -- if account create succeeds, make editedById user account administrator
        IF new_accountId > 0 THEN
            -- make new user account admin
            new_accountRoleTypeId := RoleTypeId from RoleTypes where tag ilike 'ROLE_TYPE_ACCOUNT_ADMINISTRATOR';

            IF new_accountRoleTypeId > 0 THEN
                -- make new user account admin
                success := fn_accountmemberrole_create(in_credentials_email, in_credentials_password, new_accountId, var_editedById, 'ROLE_TYPE_ACCOUNT_ADMINISTRATOR');

                -- ALSO give new user author and viewer roles to workaround problem on zephyr/editor side???
                --success := fn_accountmemberrole_create(in_credentials_email, in_credentials_password, new_accountId, var_editedById, 'ROLE_TYPE_AUTHOR');
                --success := fn_accountmemberrole_create(in_credentials_email, in_credentials_password, new_accountId, var_editedById, 'ROLE_TYPE_VIEWER');

                IF success <= 0 THEN
                    -- something went wrong
                END IF;

            END IF;

            -- if not a free account and termDueDateTime not set greater than now, generate the 1st subscription invoice
            var_price := price FROM AccountTypes WHERE accountTypeId = new_accountTypeId;
            IF var_price > 0 AND in_termDueDateTime <= now() THEN
                success := fn_subscription_new(in_credentials_email, in_credentials_password, new_accountId);

                IF success <= 0 THEN
                    -- something went wrong
                END IF;

            END IF;

        END IF;

        RETURN new_accountId;
    End;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_account_create(text, text, integer, text, timestamp, integer, text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_account_update(text, text, integer, integer, text, timestamp, integer, text, text, integer, text, text)

-- DROP FUNCTION fn_account_update(text, text, integer, integer, text, timestamp, integer, text, text, integer, text, text);

CREATE OR REPLACE FUNCTION fn_account_update(in_credentials_email text, in_credentials_password text, in_accountId integer, in_companyId integer, in_type text, in_termDueDateTime timestamp, in_goodStanding integer, in_name text, in_description text, in_tokenBalance integer, in_showCompanyInfoForAll integer, in_contactPreference text, in_convertMethod text, in_convertAmount integer, in_paypalLogin text, in_allowSubscriptionPayment integer, in_billingContactSameAsPersonal integer, in_billingAddressSameAsCompany integer, in_companySameAsPersonal integer, in_renewalNoticeSent int4, in_editedById text,  in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Accounts';
        var_functionName text := 'UPDATE';

        rec Accounts%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        -- custom changes to allow eCommCron to call this function
        var_credentialsId := id FROM App_User au
            LEFT JOIN AccountMemberRoles amr
              ON amr.accountId = in_accountId
              AND au.id = amr.memberId
            LEFT JOIN TableFunctionRoleMap tfr
              ON amr.typeId = tfr.roleTypeId
              AND tfr.tableName = var_tableName
              AND tfr.functionName = var_functionName
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND (tfr.roleTypeId IS NOT NULL OR au.displayname = 'eCommCron')
            LIMIT 1;

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from Accounts where accountId = in_accountId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_companyId' THEN
                rec.companyId := in_companyId;
            ELSEIF param_name = 'in_type' THEN
                rec.typeId := accountTypeId FROM AccountTypes WHERE tag ILIKE in_type;
            ELSEIF param_name = 'in_termDueDateTime' THEN
                rec.termDueDateTime := in_termDueDateTime;
            ELSEIF param_name = 'in_goodStanding' THEN
                rec.goodStanding := in_goodStanding;
            ELSEIF param_name = 'in_name' THEN
                rec.name := in_name;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_tokenBalance' THEN
                rec.tokenBalance := in_tokenBalance;
            ELSEIF param_name = 'in_showCompanyInfoForAll' THEN
                rec.showCompanyInfoForAll := in_showCompanyInfoForAll;
            ELSEIF param_name = 'in_contactPreference' THEN
                rec.contactPreference := in_contactPreference;
            ELSEIF param_name = 'in_convertMethod' THEN
                rec.convertMethod := in_convertMethod;
            ELSEIF param_name = 'in_convertAmount' THEN
                rec.convertAmount := in_convertAmount;
            ELSEIF param_name = 'in_paypalLogin' THEN
                rec.paypalLogin := in_paypalLogin;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            ELSEIF param_name = 'in_allowSubscriptionPayment' THEN
                rec.allowSubscriptionPayment := in_allowSubscriptionPayment;
            ELSEIF param_name = 'in_billingContactSameAsPersonal' THEN
            	rec.billingContactSameAsPersonal := in_billingContactSameAsPersonal;
            ELSEIF param_name = 'in_billingAddressSameAsCompany' THEN
            	rec.billingAddressSameAsCompany := in_billingAddressSameAsCompany;
           	ELSEIF param_name = 'in_companySameAsPersonal' THEN
           		rec.companySameAsPersonal := in_companySameAsPersonal;
            ELSEIF param_name = 'in_renewalNoticeSent' THEN
                rec.renewalNoticeSent := in_renewalNoticeSent;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE Accounts SET
            companyId = rec.companyId,
            typeId = rec.typeId,
            termDueDateTime = rec.termDueDateTime,
            goodStanding = rec.goodStanding,
            name = rec.name,
            description = rec.description,
            tokenBalance = rec.tokenBalance,
            showCompanyInfoForAll = rec.showCompanyInfoForAll,
            contactPreference = rec.contactPreference,
            convertMethod = rec.convertMethod,
            convertAmount = rec.convertAmount,
            paypalLogin = rec.paypalLogin,
            editedById = rec.editedById,
            allowSubscriptionPayment = rec.allowSubscriptionPayment,
            billingContactSameAsPersonal = rec.billingContactSameAsPersonal,
            billingAddressSameAsCompany = rec.billingAddressSameAsCompany,
            companySameAsPersonal = rec.companySameAsPersonal,
            renewalNoticeSent = rec.renewalNoticeSent
        WHERE accountId = in_accountId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_account_update(text, text, integer, integer, text, timestamp, integer, text, text, integer, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_account_retire(text, text, integer, text)

-- DROP FUNCTION fn_account_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_account_retire(in_credentials_email text, in_credentials_password text, in_accountId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Accounts';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = in_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE Accounts SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        FROM AccountMemberRoles amr, TableFunctionRoleMap tfr
        WHERE Accounts.accountId = in_accountId
        AND Accounts.accountId = amr.accountId
        AND amr.memberId = var_credentialsId
        AND amr.typeId = tfr.roleTypeId
        AND tfr.tableName = var_tableName
        AND tfr.functionName = var_functionName;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_account_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_account_filter(text, text, integer, integer, text, integer, text, timestamp, integer, text, text, integer, text)

-- DROP FUNCTION fn_account_filter(text, text, integer, integer, text, integer, text, timestamp, integer, text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_account_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_companyId integer, in_type text, in_termDueDateTime timestamp, in_goodStanding integer, in_name text, in_description text, in_tokenBalance integer, in_showCompanyInfoForAll integer, in_contactPreference text, in_convertMethod text, in_convertAmount integer, in_paypalLogin text, in_filter_list text)
  RETURNS SETOF Accounts AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Accounts';
        var_functionName text := 'FILTER';

        rec Accounts%ROWTYPE;
        filter_name text;
        var_typeId int;
        var_count int := 1;
        var_order text;
        var_query text;
    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

            var_query :=
                'SELECT * FROM Accounts WHERE retired = 0' ||
                ' AND accountId IN (SELECT amr.accountId FROM AccountMemberRoles amr, TableFunctionRoleMap tfr' ||
                '   WHERE amr.memberId = ''' || var_credentialsId || '''' ||
                '   AND amr.typeId = tfr.roleTypeId' ||
                '   AND tfr.tableName = ''' || var_tableName || '''' ||
                '   AND tfr.functionName = ''' || var_functionName || ''')';

            LOOP
                filter_name := split_part(in_filter_list, ',', var_count);
                filter_name := trim(both ' ' from filter_name);
                EXIT WHEN filter_name = '';

                IF filter_name = 'in_accountId' THEN
                ELSEIF filter_name = 'in_companyId' THEN
                    var_query := var_query || ' AND companyId = ' || in_companyId;
                ELSEIF filter_name = 'in_type' THEN
                    var_typeId := accountTypeId FROM AccountTypes WHERE tag ILIKE in_type;
                    var_query := var_query || ' AND typeId = ' || var_typeId;
                ELSEIF filter_name = 'in_termDueDateTime' THEN
                    var_query := var_query || ' AND termDueDateTime = ' || in_termDueDateTime;
                ELSEIF filter_name = 'in_goodStanding' THEN
                    var_query := var_query || ' AND goodStanding = ' || in_goodStanding;
                ELSEIF filter_name = 'in_name' THEN
                    var_query := var_query || ' AND name ILIKE ''%' || in_name || '%''';
                ELSEIF filter_name = 'in_description' THEN
                    var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
                ELSEIF filter_name = 'in_tokenBalance' THEN
                    var_query := var_query || ' AND tokenBalance = ' || in_tokenBalance;

                ELSEIF filter_name = 'in_showCompanyInfoForAll' THEN
                    var_query := var_query || ' AND showCompanyInfoForAll = ' || in_showCompanyInfoForAll;
                ELSEIF filter_name = 'in_contactPreference' THEN
                    var_query := var_query || ' AND contactPreference ILIKE ''%' || in_contactPreference || '%''';
                ELSEIF filter_name = 'in_convertMethod' THEN
                    var_query := var_query || ' AND convertMethod ILIKE ''%' || in_convertMethod || '%''';
                ELSEIF filter_name = 'in_convertAmount' THEN
                    var_query := var_query || ' AND convertAmount = ' || in_convertAmount;
                ELSEIF filter_name = 'in_paypalLogin' THEN
                    var_query := var_query || ' AND paypalLogin ILIKE ''%' || in_paypalLogin || '%''';



                ELSEIF filter_name = 'in_editedById' THEN
                ELSEIF filter_name = 'in_editedDateTime' THEN
                ELSEIF filter_name = 'in_retired' THEN
                END IF;
                var_count := var_count + 1;
            END LOOP;

            IF LENGTH(in_order_list) > 0 THEN
                var_order := replace(in_order_list,':a',' ASC');
                var_order := replace(var_order,':d',' DESC');
                var_query := var_query || ' ORDER BY ' || var_order;
            END IF;

            var_count := in_count;
            IF var_count <= 0 THEN
                SELECT count(*) INTO var_count FROM Accounts;
            END IF;
            var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

            FOR rec IN
                EXECUTE var_query
            LOOP
                RETURN NEXT rec;
            END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_account_filter(text, text, integer, integer, text, integer, text, timestamp, integer, text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_account_find(text, text, integer)

-- DROP FUNCTION fn_account_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_account_find(in_credentials_email text, in_credentials_password text, in_accountId integer)
  RETURNS SETOF Accounts AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Accounts';
        var_functionName text := 'FIND';

        rec Accounts%ROWTYPE;

    BEGIN
        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = in_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM Accounts
                WHERE accountId = in_accountId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_account_find(text, text, integer) OWNER TO postgres;
