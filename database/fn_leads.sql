
-- fn_lead.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
---------------------------------------------------------------------

CREATE OR REPLACE FUNCTION fn_lead_create(in_credentials_email text, in_credentials_password text, in_leadSource text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Leads';
        var_functionName text := 'CREATE';

        new_leadId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into Leads(leadSource) values(in_leadSource);

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_leadId := currval('seq_Leads_leadId');
        END IF;

        return new_leadId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;



CREATE OR REPLACE FUNCTION fn_leaditemvalue_create(in_credentials_email text, in_credentials_password text, in_leadId bigint, in_item text, in_value text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'LeadItemValue';
        var_functionName text := 'CREATE';

        new_leadItemValueId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into LeadItemValue(leadId, item, value) values(in_leadId, in_item, in_value);

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_leadItemValueId := currval('seq_LeadItemValue_leadItemValueId');
        END IF;

        return new_leadItemValueId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;



DROP TYPE IF EXISTS _type_leadsleaditemvalue_ CASCADE;

CREATE TYPE _type_leadsleaditemvalue_ As (
    leadSource text,    -- Leads table
    leadDateTime timestamp,
    leadItemValueId bigint,
    leadId bigint,
    item text,
    value text
);

CREATE OR REPLACE FUNCTION fn_leaditemvalue_filter(
    in_credentials_email text, in_credentials_password text,
    in_count integer, in_offset integer, in_order_list text,
    in_leadSource text, in_item text, in_value text, in_fromDate text, in_toDate text,
    in_filter_list text)
  RETURNS SETOF _type_leadsleaditemvalue_ AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'LeadItemValue';
        var_functionName text := 'FILTER';

        rec _type_leadsleaditemvalue_%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'INSERT INTO tmp_tbl_filtered_leadids SELECT DISTINCT Leads.leadId FROM Leads, LeadItemValue WHERE Leads.leadId = LeadItemValue.leadId';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_leadSource' THEN
                var_query := var_query || ' AND leadSource ILIKE ''%' || in_leadSource || '%''';
            ELSEIF filter_name = 'in_item' THEN
                var_query := var_query || ' AND item ILIKE ''%' || in_item || '%''';
            ELSEIF filter_name = 'in_value' THEN
                var_query := var_query || ' AND value ILIKE ''%' || in_value || '%''';
            ELSEIF filter_name = 'in_fromDate' THEN
                var_query := var_query || ' AND leadDateTime >= ''' || in_fromDate || '''';
            ELSEIF filter_name = 'in_toDate' THEN
                var_query := var_query || ' AND leadDateTime <= ''' || in_toDate || '''';
            END IF;
            var_count := var_count + 1;
        END LOOP;
        var_query := var_query || ' ORDER BY leadId';

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM Leads;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        BEGIN
            CREATE TEMPORARY TABLE tmp_tbl_filtered_leadids (
                leadId integer
            );
        EXCEPTION
            WHEN DUPLICATE_TABLE THEN
                TRUNCATE TABLE tmp_tbl_filtered_leadids;
        END;

        EXECUTE var_query;

        FOR rec IN
            SELECT leadSource, leadDateTime, LeadItemValue.* FROM tmp_tbl_filtered_leadids, Leads, LeadItemValue
            WHERE tmp_tbl_filtered_leadids.leadId = Leads.leadId
            AND Leads.leadId = LeadItemValue.leadId
            ORDER BY LeadItemValue.leadId, item, value
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
