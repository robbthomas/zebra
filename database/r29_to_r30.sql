-------------------------------------------------------------------------------
-- r28_to_r29.sql
--   reset expiration date for expired fake CCs
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (30);
-------------------------------------------------------------------------------
update creditcards set expirationyear = '1970' where expirationyear = ' ';
update creditcards set expirationmonth = '01' where expirationmonth = ' ';

update creditcards
    set expirationmonth = '09', expirationyear = '2011' 
    where (CAST (expirationyear as integer)) <= date_part('year', now())
        and (CAST (expirationmonth as integer)) <= date_part('month', now())
        and maskednumber in ('XXXXXXXXXXXX0000', 'XXXXXXXXXXX0002', 'XXXXXXXXXXXX0012', 
                             'XXXXXXXXX8888', 'XXXXXXXXXXXX0017', 'XXXXXXXXXX0006');
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
