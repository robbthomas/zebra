-------------------------------------------------------------------------------
-- r19_to_r20.sql
--
-- Add support for multiple image/thumbnail/caption per Zapp/Gadget
-- in an ordered list.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (21);
-------------------------------------------------------------------------------
CREATE SEQUENCE seq_PublishImages_publishImageId START WITH 23000;

CREATE TABLE PublishImages (
	publishImageId bigint NOT NULL PRIMARY KEY DEFAULT nextval('seq_PublishImages_publishImageId'),
		publishId bigint,
        caption text,
        label text,
        screenshotFileId text,
        iconFileId text,
		renderingComplete boolean,
		displayOrder integer,
		retired boolean,
		
	CONSTRAINT fk_PublishImages_publishId FOREIGN KEY
	(publishId) REFERENCES Published(publishId)

);


-- migrate the current icon data to the new schema
insert into PublishImages (publishId, iconFileId, retired, renderingComplete, displayOrder) 
	(select p.publishId, p.thumbnailId, false, true, 1 from Published p where p.thumbnailid is not null);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

