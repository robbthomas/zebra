-------------------------------------------------------------------------------
-- r39_to_r40.sql
--   add selfPRice column to Published.  Some extra work is done to regenerate
--   the autohistory table.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (40);
-------------------------------------------------------------------------------
-- Add new column.

ALTER TABLE Published ADD COLUMN selfPrice int4 default -1;

-- Save existing autohistory.
SELECT * INTO TEMPORARY TABLE temp_Published_Autohistory FROM Published_Autohistory;

-- Recreate autohistory for Published.
DROP SEQUENCE seq_Published_AutoHistory CASCADE;
DROP TABLE Published_AutoHistory CASCADE;

CREATE SEQUENCE seq_Published_AutoHistory;
CREATE TABLE Published_AutoHistory (
    LIKE Published,
    Published_AutoHistory_Id bigint DEFAULT nextval('seq_Published_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_Published_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO Published_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

-- Restore saved autohistory.
INSERT INTO Published_Autohistory
(
    publishId,accountId,projectId,firstPublishId,versionMajor,versionMinor,versionDot,versionBuildNumber,
    categoryId,name,description,thumbnailId,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,
    editedById,retired,retiredById,retiredDateTime,screenshotId,hideInStoreList,autohistorydate, metadata, selfPrice
)
SELECT
    publishId,accountId,projectId,firstPublishId,versionMajor,versionMinor,versionDot,versionBuildNumber,
    categoryId,name,description,thumbnailId,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,
    editedById,retired,retiredById,retiredDateTime,screenshotId,hideInStoreList,autohistorydate, metadata, -1
FROM temp_Published_Autohistory;

DROP TABLE temp_Published_Autohistory;


-- grab all new relations in one fell swoop
-- we could dump it straight back to the table
-- if we weren't using it in this query
-- it goes to a temp table so we can murder the existing one
-- before adding these back and so we can
-- keep track of which children have been
-- processed
create temporary table FlattenWorking (
  parent_children_id text,
  child_id text,
  workingState int
);

insert into FlattenWorking
select distinct parent_child.parent_children_id, parent.gadgetRefId, 0
from parent_child
inner join parent
on parent.id =  parent_child.child_id;

-- we've captured this whole table so we don't have to
-- individually remove but can cheaply truncate it
truncate table parent_child;

-- we've captured all this data so these rows are already
-- unnecessary and can be removed all at once
delete from parent
where gadgetRefId is not null;

-- Flattening the parent_child table requires looping
-- so create a function and immediately execute it
create function "temporary_flatten_parent_child"()
returns int as $BODY$
declare
 num_rows int;
 iterations int;
begin

  create temporary table FlattenWorking0to1 (
    parent_children_id text,
    child_id text
  );

  iterations := 0;

  loop

    -- generate a list of all projects that have not yet had their children listed here
    insert into FlattenWorking0to1
    select parent_children_id, child_id
    from FlattenWorking
    where FlattenWorking.workingState = 0;

    GET DIAGNOSTICS num_rows = ROW_COUNT;

    exit when num_rows = 0;

    iterations := iterations + 1;

    -- promote these to the next step
    update FlattenWorking
    set workingState = 1
    from FlattenWorking0to1
    where FlattenWorking0to1.parent_children_id = FlattenWorking.parent_children_id
      and FlattenWorking0to1.child_id = FlattenWorking.child_id;

    -- add any children
    insert into FlattenWorking
    select FlattenWorking0to1.parent_children_id, FlattenWorking.child_id, 0
    from FlattenWorking0to1
    inner join FlattenWorking
      on FlattenWorking.parent_children_id = FlattenWorking0to1.child_id
    where not exists (
      select w2.parent_children_id, w2.child_id
      from FlattenWorking w2
      where w2.parent_children_id = FlattenWorking0to1.parent_children_id
        and w2.child_id = FlattenWorking.child_id
    );

    truncate table FlattenWorking0to1;

  end loop;

  drop table FlattenWorking0to1;
  return iterations;
end;
$BODY$ language 'plpgsql';

select temporary_flatten_parent_child();

drop function temporary_flatten_parent_child();

-- replace the whole table
insert into parent_child(child_id, parent_children_id)
select child_id, parent_children_id
from FlattenWorking;

drop table FlattenWorking;

create temporary table SelfPriceWorking (
 publishId int8 unique,
 projectId text,
 editable bool,
 price int4,
 selfPrice int4,
 workingState int4
);

insert into SelfPriceWorking
select Published.publishId, Published.projectId, LicenseTypes.editable, Published.price, -1, 0
from Published
inner join LicenseTypes
  on LicenseTypes.licenseTypeId = Published.licenseTypeId;

-- Calculating self price requires looping
-- so create a function and immediately execute it
create function "temporary_fill_self_price"()
returns int as $BODY$
declare
 num_rows int;
 iterations int;
begin

  create temporary table SelfPriceWorking0to1 (
    projectId text
  );

  iterations := 0;

  loop

    -- generate a list of all projects that have not yet had selfPrice
    -- calculated and have no children that are uncalculated
    insert into SelfPriceWorking0to1
    select w1.projectId
    from SelfPriceWorking w1
    where w1.workingState = 0
      and not exists (
        select w2.projectId
        from parent_child
        inner join SelfPriceWorking w2
          on w2.projectId = parent_child.child_id
          and w2.selfPrice < 0
        where parent_child.parent_children_id = w1.projectId
      );

    GET DIAGNOSTICS num_rows = ROW_COUNT;

    exit when num_rows = 0;

    iterations := iterations + 1;

    -- promote these to the next step
    update SelfPriceWorking
    set workingState = 1
    from SelfPriceWorking0to1
    where SelfPriceWorking0to1.projectId = SelfPriceWorking.projectId;

    -- calculate this level's self price
    update SelfPriceWorking
    set selfPrice = price - coalesce((
      select sum(w2.selfPrice)
        from parent_child
        inner join SelfPriceWorking w2
          on w2.projectId = parent_child.child_id
          and w2.editable = false
        where parent_child.parent_children_id = SelfPriceWorking.projectId
    ),0)
    from SelfPriceWorking0to1
    where SelfPriceWorking0to1.projectId = SelfPriceWorking.projectId;

    truncate table SelfPriceWorking0to1;

  end loop;

  drop table SelfPriceWorking0to1;
  return iterations;
end;
$BODY$ language 'plpgsql';

select temporary_fill_self_price();

drop function temporary_fill_self_price();

update Published
set selfPrice = SelfPriceWorking.selfPrice
from SelfPriceWorking where SelfPriceWorking.publishId = Published.publishId;

drop table SelfPriceWorking;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

