-------------------------------------------------------------------------------
-- r94_to_r95.sql
--
-- Author:  Joe Bodell
-- Date:  07-03-2012
-- Notes:  Create tables to support refactoring of asset and publishImage tables, renormalized for maximal awesomeness
--
-------------------------------------------------------------------------------

CREATE TABLE UploadableAssetType (
      UploadableAssetTypeID SERIAL  PRIMARY KEY
    , Tag                   VARCHAR NOT NULL
    , Description           VARCHAR NOT NULL
);

INSERT INTO UploadableAssetType (Tag, Description) VALUES ('Icon','The icon to be displayed in app/gadget lists');
INSERT INTO UploadableAssetType (Tag, Description) VALUES ('Screenshot','Screenshots associated with a given project');
INSERT INTO UploadableAssetType (Tag, Description) VALUES ('Avatar','User-uploaded image which appears on author profile');
INSERT INTO UploadableAssetType (Tag, Description) VALUES ('Project Asset','An asset to be associated with a project -- font or other content');

CREATE TABLE UploadableAssetTypeAttribute (
      UploadableAssetTypeAttributeID SERIAL  PRIMARY KEY
    , Tag                            VARCHAR NOT NULL
    , Description                    VARCHAR NOT NULL
);

CREATE TABLE UploadableAssetTypeAttributeValue (
      UploadableAssetTypeAttributeValueID SERIAL  PRIMARY KEY
    , UploadableAssetTypeID               INT4    NOT NULL REFERENCES UploadableAssetType( UploadableAssetTypeID )
    , UploadableAssetTypeAttributeID      INT4    NOT NULL REFERENCES UploadableAssetTypeAttribute( UploadableAssetTypeAttributeID )
    , AttributeValue                      VARCHAR NOT NULL
);

CREATE TABLE MimeCategory (
      MimeCategoryID SERIAL PRIMARY KEY
    , Tag VARCHAR NOT NULL
);

INSERT INTO MimeCategory (Tag) VALUES ('IMAGE');
INSERT INTO MimeCategory (Tag) VALUES ('APPLICATION');
INSERT INTO MimeCategory (Tag) VALUES ('VIDEO');
INSERT INTO MimeCategory (Tag) VALUES ('DOCUMENT');
INSERT INTO MimeCategory (Tag) VALUES ('AUDIO');
INSERT INTO MimeCategory (Tag) VALUES ('FONT');

CREATE TABLE MimeType (
      MimeTypeId     SERIAL  PRIMARY KEY
    , MimeCategoryID INT4    NOT NULL REFERENCES MimeCategory( MimeCategoryID )
    , ISOName        VARCHAR NOT NULL
);

INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'image/jpeg' FROM MimeCategory WHERE MimeCategory.Tag = 'IMAGE';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'image/gif' FROM MimeCategory WHERE MimeCategory.Tag = 'IMAGE';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'image/png' FROM MimeCategory WHERE MimeCategory.Tag = 'IMAGE';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'image/svg+xml' FROM MimeCategory WHERE MimeCategory.Tag = 'IMAGE';

INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'application/x-shockwave-flash' FROM MimeCategory WHERE MimeCategory.Tag = 'APPLICATION';

INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'video/mpeg' FROM MimeCategory WHERE MimeCategory.Tag = 'VIDEO';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'video/x-flv' FROM MimeCategory WHERE MimeCategory.Tag = 'VIDEO';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'video/mp4' FROM MimeCategory WHERE MimeCategory.Tag = 'VIDEO';

INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'audio/mp4' FROM MimeCategory WHERE MimeCategory.Tag = 'AUDIO';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'audio/wav' FROM MimeCategory WHERE MimeCategory.Tag = 'AUDIO';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'audio/basic' FROM MimeCategory WHERE MimeCategory.Tag = 'AUDIO';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'audio/mpeg' FROM MimeCategory WHERE MimeCategory.Tag = 'AUDIO';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'audio/x-aiff' FROM MimeCategory WHERE MimeCategory.Tag = 'AUDIO';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'audio/x-wav' FROM MimeCategory WHERE MimeCategory.Tag = 'AUDIO';

INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'application/x-font' FROM MimeCategory WHERE MimeCategory.Tag = 'FONT';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'application/x-font-truetype' FROM MimeCategory WHERE MimeCategory.Tag = 'FONT';
INSERT INTO MimeType (MimeCategoryID, ISOName) SELECT MimeCategoryID, 'application/x-font-opentype' FROM MimeCategory WHERE MimeCategory.Tag = 'FONT';

CREATE TABLE UploadableAssetS3Bucket (
      UploadableAssetS3BucketID SERIAL  PRIMARY KEY
    , Tag                       VARCHAR NOT NULL
    , BucketPath                VARCHAR NOT NULL
);

CREATE TABLE UploadableAssetTypeSupportedMimeType (
      UploadableAssetTypeSupportedMimeTypeID SERIAL PRIMARY KEY
    , UploadableAssetTypeId                  INT4   NOT NULL REFERENCES UploadableAssetType( UploadableAssetTypeID )
    , MimeTypeId                             INT4   NOT NULL REFERENCES MimeType( MimeTypeId )
    , UploadableAssetS3BucketID              INT4   NOT NULL REFERENCES UploadableAssetS3Bucket( UploadableAssetS3BucketID )
    , CONSTRAINT ux_UATSMT_assetType_mimeType UNIQUE( UploadableAssetTypeID, MimeTypeID )
);

CREATE TABLE UploadableAsset (
      UploadableAssetID                      SERIAL       PRIMARY KEY
    , UploadableAssetTypeSupportedMimeTypeID INT4         NOT NULL REFERENCES UploadableAssetTypeSupportedMimeType( UploadableAssetTypeSupportedMimeTypeID )
    , FileSize                               INT8         NOT NULL
    , DateCreated                            DATE         NOT NULL
    , CreatedBy                              VARCHAR(255) NOT NULL REFERENCES App_User( id )
    , Deleted                                BOOL         NOT NULL DEFAULT false
    , FileId                                 VARCHAR      NOT NULL
    , Name                                   VARCHAR      NOT NULL
    , AltText                                VARCHAR          NULL
    , Caption                                VARCHAR          NULL
    , DisplayOrder                           INT4         NOT NULL
);

CREATE TABLE ProjectAsset (
      ProjectAssetID    SERIAL       PRIMARY KEY
    , ProjectID         VARCHAR(255) NOT NULL REFERENCES Projects( ProjectID )
    , UploadableAssetID INT4         NOT NULL REFERENCES UploadableAsset( UploadableAssetID )
);

ALTER TABLE App_User ADD AvatarID INT4 NULL REFERENCES UploadableAsset( UploadableAssetID );

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (95);
------------------------------------------------------