-------------------------------------------------------------------------------
-- r53_to_r54.sql
--
-- create index on guestlistinvitees
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (57);
-------------------------------------------------------------------------------

create index ix_guestlistinvitees_email on guestlistinvitees(email);


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;