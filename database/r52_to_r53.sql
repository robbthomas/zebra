-------------------------------------------------------------------------------
-- r50_to_r51.sql
--
-- Fix published_autohistory
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (53);
-------------------------------------------------------------------------------

-- Save existing autohistory.
SELECT * INTO TEMPORARY TABLE temp_Published_Autohistory FROM Published_Autohistory;

-- Recreate autohistory for Published.
DROP SEQUENCE seq_Published_AutoHistory CASCADE;
DROP TABLE Published_AutoHistory CASCADE;

CREATE SEQUENCE seq_Published_AutoHistory;

SELECT setval('seq_Published_Autohistory', (SELECT MAX(published_autohistory_id) FROM temp_published_autohistory)+1);    

CREATE TABLE Published_AutoHistory (
    LIKE Published,
    Published_AutoHistory_Id bigint DEFAULT nextval('seq_Published_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_Published_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO Published_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

-- Restore saved autohistory.
INSERT INTO Published_Autohistory
(
    publishId,accountId,projectId,editeddatetime,firstPublishId,versionMajor,versionMinor,versionDot,versionBuildNumber,
    categoryId,name,description,thumbnailId,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,
    editedById,retired,retiredById,retiredDateTime,screenshotId,hideInStoreList,inviteonly, guestlistid, published_autohistory_id,autohistorydate
)
SELECT
    publishId,accountId,projectId,coalesce(editeddatetime, now()),firstPublishId,versionMajor,versionMinor,versionDot,versionBuildNumber,
    categoryId,name,description,thumbnailId,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,
    editedById,retired,retiredById,retiredDateTime,screenshotId,'f', 'f', null, published_autohistory_id,autohistorydate
FROM temp_Published_Autohistory;

DROP TABLE temp_Published_Autohistory;
-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
