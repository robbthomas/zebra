--------------------------------------------------------------------------------
-- fn_screenshots.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- ScreenShots functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='ScreenShots'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('ScreenShots','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ScreenShots','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ScreenShots','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ScreenShots','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('ScreenShots','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ScreenShots','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_screenshot_create(text, text, text, integer, text, text, text)

-- DROP FUNCTION fn_screenshot_create(text, text, text, integer, text, text, text);

CREATE OR REPLACE FUNCTION fn_screenshot_create(in_credentials_email text, in_credentials_password text, in_projectId text, in_orderNumber integer, in_assetId text, in_description text, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ScreenShots';
        var_functionName text := 'CREATE';

        new_screenShotId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into ScreenShots(
            projectId,
            orderNumber,
            assetId,
            description,
            editedById
        )
        values(
            in_projectId,
            in_orderNumber,
            in_assetId,
            in_description,
            in_editedById
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_screenShotId := currval('seq_ScreenShots_screenShotId');
        END IF;

        return new_screenShotId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_screenshot_create(text, text, text, integer, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_screenshot_update(text, text, integer, text, integer, text, text, text, text)

-- DROP FUNCTION fn_screenshot_update(text, text, integer, text, integer, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_screenshot_update(in_credentials_email text, in_credentials_password text, in_screenShotId integer, in_projectId text, in_orderNumber integer, in_assetId text, in_description text, in_editedById text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ScreenShots';
        var_functionName text := 'UPDATE';

        rec ScreenShots%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from ScreenShots where screenShotId = in_screenShotId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_projectId' THEN
                rec.projectId := in_projectId;
            ELSEIF param_name = 'in_orderNumber' THEN
                rec.orderNumber := in_orderNumber;
            ELSEIF param_name = 'in_assetId' THEN
                rec.assetId := in_assetId;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE ScreenShots SET
            projectId = rec.projectId,
            orderNumber = rec.orderNumber,
            assetId = rec.assetId,
            description = rec.description,
            editedById = rec.editedById,
            editedDateTime = now()
        WHERE screenShotId = in_screenShotId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_screenshot_update(text, text, integer, text, integer, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_screenshot_retire(text, text, integer, text)

-- DROP FUNCTION fn_screenshot_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_screenshot_retire(in_credentials_email text, in_credentials_password text, in_screenShotId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ScreenShots';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE ScreenShots SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE screenShotId = in_screenShotId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_screenshot_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_screenshot_filter(text, text, integer, integer, text, text, integer, text, text, text)

-- DROP FUNCTION fn_screenshot_filter(text, text, integer, integer, text, text, integer, text, text, text);

CREATE OR REPLACE FUNCTION fn_screenshot_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_projectId text, in_orderNumber integer, in_assetId text, in_description text, in_filter_list text)
  RETURNS SETOF ScreenShots AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ScreenShots';
        var_functionName text := 'FILTER';

        rec ScreenShots%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM ScreenShots WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_screenShotId' THEN
            ELSEIF filter_name = 'in_projectId' THEN
                var_query := var_query || ' AND projectId ILIKE ''%' || in_projectId || '%''';
            ELSEIF filter_name = 'in_orderNumber' THEN
                var_query := var_query || ' AND orderNumber = ' || in_orderNumber;
            ELSEIF filter_name = 'in_assetId' THEN
                var_query := var_query || ' AND assetId ILIKE ''%' || in_assetId || '%''';
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            ELSEIF filter_name = 'in_retired' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM ScreenShots;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_screenshot_filter(text, text, integer, integer, text, text, integer, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_screenshot_find(text, text, integer)

-- DROP FUNCTION fn_screenshot_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_screenshot_find(in_credentials_email text, in_credentials_password text, in_screenShotId integer)
  RETURNS SETOF ScreenShots AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ScreenShots';
        var_functionName text := 'FIND';

        rec ScreenShots%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM ScreenShots
                WHERE screenShotId = in_screenShotId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_screenshot_find(text, text, integer) OWNER TO postgres;
