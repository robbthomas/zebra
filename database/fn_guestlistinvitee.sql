--------------------------------------------------------------------------------
-- fn_guestlistinvitees.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- GuestListInvitees functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='GuestListInvitees'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('GuestListInvitees','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_guestlistinvitee_create(text, text, integer, text, text, timestamp, integer, text, text, text)

-- DROP FUNCTION fn_guestlistinvitee_create(text, text, integer, text, text, timestamp, integer, text, text, text);

CREATE OR REPLACE FUNCTION fn_guestlistinvitee_create(in_credentials_email text, in_credentials_password text, in_guestListId integer, in_email text, in_retiredById text, in_retiredDateTime timestamp, in_retiredReasonTypeId integer, in_uuid text, in_firstName text, in_lastName text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'GuestListInvitees';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_guestListInviteeId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO GuestListInvitees(
            guestListId,
            email,
            retiredById,
            retiredDateTime,
            retiredReasonTypeId,
            uuid,
            firstName,
            lastName
        )
        VALUES(
            in_guestListId,
            in_email,
            null,
            in_retiredDateTime,
            null,
            in_uuid,
            in_firstName,
            in_lastName
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_guestListInviteeId := currval('guestlistinvitees_guestlistinviteeid_seq');
        END IF;

        return new_guestListInviteeId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_guestlistinvitee_create(text, text, integer, text, text, timestamp, integer, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_guestlistinvitee_update(text, text, integer, integer, text, text, timestamp, integer, text, text, text, text)

-- DROP FUNCTION fn_guestlistinvitee_update(text, text, integer, integer, text, text, timestamp, integer, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_guestlistinvitee_update(in_credentials_email text, in_credentials_password text, in_guestListInviteeId integer, in_guestListId integer, in_email text, in_retiredById text, in_retiredDateTime timestamp, in_retiredReasonTypeId integer, in_uuid text, in_firstName text, in_lastName text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'GuestListInvitees';
        var_functionName text := 'UPDATE';

        rec GuestListInvitees%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from GuestListInvitees where guestListInviteeId = in_guestListInviteeId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_guestListId' THEN
                rec.guestListId := in_guestListId;
            ELSEIF param_name = 'in_email' THEN
                rec.email := in_email;
            ELSEIF param_name = 'in_retiredById' THEN
                rec.retiredById := in_retiredById;
            ELSEIF param_name = 'in_retiredDateTime' THEN
                rec.retiredDateTime := in_retiredDateTime;
            ELSEIF param_name = 'in_retiredReasonTypeId' THEN
                rec.retiredReasonTypeId := in_retiredReasonTypeId;
            ELSEIF param_name = 'in_uuid' THEN
                rec.uuid := in_uuid;
            ELSEIF param_name = 'in_firstName' THEN
                rec.firstName := in_firstName;
            ELSEIF param_name = 'in_lastName' THEN
                rec.lastName := in_lastName;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE GuestListInvitees SET
            guestListId = rec.guestListId,
            email = rec.email,
            updatedDateTime = now(),
            retiredById = rec.retiredById,
            retiredDateTime = rec.retiredDateTime,
            retiredReasonTypeId = rec.retiredReasonTypeId,
            uuid = rec.uuid,
            firstName = rec.firstName,
            lastName = rec.lastName
        WHERE guestListInviteeId = in_guestListInviteeId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_guestlistinvitee_update(text, text, integer, integer, text, text, timestamp, integer, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_guestlistinvitee_retire(text, text, integer, text)

-- DROP FUNCTION fn_guestlistinvitee_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_guestlistinvitee_retire(in_credentials_email text, in_credentials_password text, in_guestListInviteeId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'GuestListInvitees';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE GuestListInvitees SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        WHERE guestListInviteeId = in_guestListInviteeId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_guestlistinvitee_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_guestlistinvitee_filter(text, text, integer, integer, text, integer, text, text, timestamp, integer, text, text, text, text)

-- DROP FUNCTION fn_guestlistinvitee_filter(text, text, integer, integer, text, integer, text, text, timestamp, integer, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_guestlistinvitee_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_guestListId integer, in_email text, in_retiredById text, in_retiredDateTime timestamp, in_retiredReasonTypeId integer, in_uuid text, in_firstName text, in_lastName text, in_filter_list text)
  RETURNS SETOF GuestListInvitees AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'GuestListInvitees';
        var_functionName text := 'FILTER';

        rec GuestListInvitees%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM GuestListInvitees WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_guestListInviteeId' THEN
            ELSEIF filter_name = 'in_guestListId' THEN
                var_query := var_query || ' AND guestListId = ' || in_guestListId;
            ELSEIF filter_name = 'in_email' THEN
                var_query := var_query || ' AND email ILIKE ''%' || in_email || '%''';
            ELSEIF filter_name = 'in_retired' THEN
            ELSEIF filter_name = 'in_updatedDateTime' THEN
            ELSEIF filter_name = 'in_retiredById' THEN
                var_query := var_query || ' AND retiredById ILIKE ''%' || in_retiredById || '%''';
            ELSEIF filter_name = 'in_retiredDateTime' THEN
                var_query := var_query || ' AND retiredDateTime = ''' || in_retiredDateTime || '''';
            ELSEIF filter_name = 'in_retiredReasonTypeId' THEN
                var_query := var_query || ' AND retiredReasonTypeId = ' || in_retiredReasonTypeId;
            ELSEIF filter_name = 'in_uuid' THEN
                var_query := var_query || ' AND uuid ILIKE ''%' || in_uuid || '%''';
            ELSEIF filter_name = 'in_firstName' THEN
                var_query := var_query || ' AND firstName ILIKE ''%' || in_firstName || '%''';
            ELSEIF filter_name = 'in_lastName' THEN
                var_query := var_query || ' AND lastName ILIKE ''%' || in_lastName || '%''';
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM GuestListInvitees;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_guestlistinvitee_filter(text, text, integer, integer, text, integer, text, text, timestamp, integer, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_guestlistinvitee_find(text, text, integer)

-- DROP FUNCTION fn_guestlistinvitee_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_guestlistinvitee_find(in_credentials_email text, in_credentials_password text, in_guestListInviteeId integer)
  RETURNS SETOF GuestListInvitees AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'GuestListInvitees';
        var_functionName text := 'FIND';

        rec GuestListInvitees%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM GuestListInvitees
                WHERE guestListInviteeId = in_guestListInviteeId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_guestlistinvitee_find(text, text, integer) OWNER TO postgres;
