--------------------------------------------------------------------------------
-- fn_zappratingcaches.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- ZappRatingCaches functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='ZappRatingCaches'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('ZappRatingCaches','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_zappratingcache_create(text, text, integer, integer, integer)

-- DROP FUNCTION fn_zappratingcache_create(text, text, integer, integer, integer);

CREATE OR REPLACE FUNCTION fn_zappratingcache_create(in_credentials_email text, in_credentials_password text, in_projectId text, in_numRatings integer, in_ratingTotal integer)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ZappRatingCaches';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_zappRatingCacheId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO ZappRatingCaches(
            projectId,
            numRatings,
            ratingTotal
        )
        VALUES(
            in_projectId,
            in_numRatings,
            in_ratingTotal
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_zappRatingCacheId := currval('seq_ZappRatingCaches_zappRatingCacheId');
        END IF;

        return new_zappRatingCacheId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_zappratingcache_create(text, text, integer, integer, integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zappratingcache_update(text, text, integer, integer, integer, integer, text)

-- DROP FUNCTION fn_zappratingcache_update(text, text, integer, integer, integer, integer, text);

CREATE OR REPLACE FUNCTION fn_zappratingcache_update(in_credentials_email text, in_credentials_password text, in_zappRatingCacheId integer, in_projectId text, in_numRatings integer, in_ratingTotal integer, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ZappRatingCaches';
        var_functionName text := 'UPDATE';

        rec ZappRatingCaches%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from ZappRatingCaches where zappRatingCacheId = in_zappRatingCacheId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_projectId' THEN
                rec.projectId := in_projectId;
            ELSEIF param_name = 'in_numRatings' THEN
                rec.numRatings := in_numRatings;
            ELSEIF param_name = 'in_ratingTotal' THEN
                rec.ratingTotal := in_ratingTotal;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE ZappRatingCaches SET
            projectId = rec.projectId,
            numRatings = rec.numRatings,
            ratingTotal = rec.ratingTotal,
            updatedDateTime = now()
        WHERE zappRatingCacheId = in_zappRatingCacheId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_zappratingcache_update(text, text, integer, integer, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zappratingcache_retire(text, text, integer, text)

-- DROP FUNCTION fn_zappratingcache_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_zappratingcache_retire(in_credentials_email text, in_credentials_password text, in_zappRatingCacheId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'ZappRatingCaches';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE ZappRatingCaches SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE zappRatingCacheId = in_zappRatingCacheId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_zappratingcache_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zappratingcache_filter(text, text, integer, integer, text, integer, integer, integer, text)
DROP TYPE if exists _ZappRatingCacheSearchResult_ CASCADE;
CREATE TYPE _ZappRatingCacheSearchResult_ AS (
    id integer,
    publishid bigint,
    numratings integer,
    ratingtotal integer,
    createddatetime timestamp,
    updateddatetime timestamp,
    projectid varchar(255)
);
    
DROP FUNCTION if exists fn_zappratingcache_filter(text, text, integer, integer, text, text, integer, integer, text);
CREATE OR REPLACE FUNCTION fn_zappratingcache_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_projectId text, in_numRatings integer, in_ratingTotal integer, in_filter_list text)
  RETURNS SETOF _ZappRatingCacheSearchResult_ AS
$BODY$
    DECLARE
--        var_credentialsId text;
        var_tableName text := 'ZappRatingCaches';
        var_functionName text := 'FILTER';
        var_count2      integer := 0;
        
        rec ZappRatingCaches%ROWTYPE;
        rec2  _ZappRatingCacheSearchResult_;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM ZappRatingCaches WHERE retired = 0';
        
        ret_id              integer     := 0;
        ret_publishid       bigint      := 0;
        ret_numratings      integer     := 0;
        ret_ratingtotal     integer     := 0;
        ret_createddatetime timestamp   := now();
        ret_updateddatetime timestamp   := now();
        ret_projectid       varchar(255):= in_projectId;
        
    BEGIN
--        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_Id' THEN
            ELSEIF filter_name = 'in_projectId' THEN
                var_query :=              'select zrc.*';
                var_query := var_query || ' from projects p1 inner join projects p2';
                var_query := var_query || '   on p2.publishedName = p1.publishedName';
                var_query := var_query || ' inner join ZappRatingCaches zrc';
                var_query := var_query || '   on zrc.projectId = p2.projectId';
                var_query := var_query || ' where p1.projectId = ''' || in_projectId || '''';
            ELSEIF filter_name = 'in_numRatings' THEN
                var_query := var_query || ' AND numRatings = ' || in_numRatings;
            ELSEIF filter_name = 'in_ratingTotal' THEN
                var_query := var_query || ' AND ratingTotal = ' || in_ratingTotal;
            ELSEIF filter_name = 'in_createdDateTime' THEN
            ELSEIF filter_name = 'in_updatedDateTime' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM ZappRatingCaches;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

--        RAISE exception 'executing sql: [%s] with projectid (%s)', var_query, in_projectId;
        FOR rec IN
            EXECUTE var_query
        LOOP
            -- populate ret_*
            var_count2          := var_count2 + 1;
            ret_id              := rec.zappRatingCacheId;
            ret_publishid       := rec.publishid;
            ret_numratings      := ret_numratings   + rec.numratings;
            ret_ratingtotal     := ret_ratingtotal  + rec.ratingtotal;
            ret_createddatetime := rec.createddatetime;
            ret_updateddatetime := rec.updateddatetime;
        END LOOP;
--        RAISE exception 'var_count2: [%],  ret_numratings: [%], ret_ratingtotal: [%]', var_count2, ret_numratings, ret_ratingtotal;
        for rec2 in
            SELECT 
                 ret_id as id
                , ret_publishid         as publishid
                , ret_numratings        as numratings
                , ret_ratingtotal       as ratingtotal
                , ret_createddatetime   as createddatetime
                , ret_updateddatetime   as updateddatetime
                , in_projectid          as projectid
        LOOP
            return next rec2;
        END LOOP;         
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_zappratingcache_filter(text, text, integer, integer, text, integer, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_zappratingcache_find(text, text, integer)

-- DROP FUNCTION fn_zappratingcache_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_zappratingcache_find(in_credentials_email text, in_credentials_password text, in_zappRatingCacheId integer)
  RETURNS SETOF ZappRatingCaches AS
$BODY$
    DECLARE
--        var_credentialsId text;
        var_tableName text := 'ZappRatingCaches';
        var_functionName text := 'FIND';

        rec ZappRatingCaches%ROWTYPE;

    BEGIN
--        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

--        IF var_credentialsId IS NOT NULL THEN
--RAISE EXCEPTION 'hoopda!';
            FOR rec IN
                SELECT *
                FROM ZappRatingCaches
                WHERE zappRatingCacheId = in_zappRatingCacheId
            LOOP
                RETURN NEXT rec;
            END LOOP;
--        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_zappratingcache_find(text, text, integer) OWNER TO postgres;
