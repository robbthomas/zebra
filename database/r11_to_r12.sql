-------------------------------------------------------------------------------
-- r11_to_r12.sql
--
-- Populate App_User.accountId from AccountMemberRoles as short-term fix
-- until Zephyr App_User join with AccountMemberRoles in and working.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (12);
-------------------------------------------------------------------------------

-- set empty accountIds based on AccountMemberRoles data
UPDATE App_User
SET accountId = AccountMemberRoles.accountId
FROM AccountMemberRoles
WHERE App_User.accountId IS NULL
AND App_User.id = AccountMemberRoles.memberId;

-- pick up stragglers not in AccountMemberRoles table at all
UPDATE App_User
SET accountId = 1
WHERE accountId IS NULL;


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
