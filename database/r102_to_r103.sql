-------------------------------------------------------------------------------
-- r102_to_r103.sql
--
-- Author:  Joe Bodell
-- Date:  11-29-2012
-- Notes:  Add the banner image id to Accounts and remove it from App_User
--
-------------------------------------------------------------------------------

ALTER TABLE Accounts ADD COLUMN bannerId int4 NULL;

ALTER TABLE App_User DROP COLUMN bannerId;

--there was more here, but it is now a noop

------------------------------------------------------
-- Increment the database version - DO NOT remove this line  
-- This should be the last thing that happens in this script.
------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES (103);
------------------------------------------------------