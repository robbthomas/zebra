-------------------------------------------------------------------------------
-- Fix CreditCards table entries columns expirationMonth, expirationYear,
-- maskedNumber, and nameOnCreditCard to replace text "null" with actual null.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES(87);

UPDATE CreditCards SET expirationMonth = null WHERE expirationMonth ilike 'null';
UPDATE CreditCards SET expirationYear = null WHERE expirationYear ilike 'null';
UPDATE CreditCards SET maskedNumber = null WHERE maskedNumber ilike 'null';
UPDATE CreditCards SET nameOnCreditCard = null WHERE nameOnCreditCard ilike 'null';

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
