-------------------------------------------------------------------------------
-- r53_to_r54.sql
--
-- Update share types
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (54);
-------------------------------------------------------------------------------
ALTER TABLE SharedProjects
	DROP CONSTRAINT fk_sharedprojects_sharetypes;
TRUNCATE TABLE ShareTypes CASCADE;
ALTER SEQUENCE ShareTypes_shareTypeId_seq RESTART WITH 1;
ALTER TABLE SharedProjects
	ADD CONSTRAINT fk_sharedprojects_sharetypes FOREIGN KEY (sharetypeid) REFERENCES sharetypes (sharetypeid) MATCH FULL;

-- insert some data for sharetypes
INSERT INTO sharetypes (tag, "name", displayorder) values ('SHARE_GUEST', 'Guest List', 1);
INSERT INTO sharetypes (tag, "name", displayorder) values ('SHARE_EMBED', 'HTML Embed', 2);
INSERT INTO sharetypes (tag, "name", displayorder) values ('SHARE_LMS', 'LMS', 3);
INSERT INTO sharetypes (tag, "name", displayorder) values ('SHARE_LINK', 'Link', 4);
INSERT INTO sharetypes (tag, "name", displayorder) values ('SHARE_EMAIL', 'Email', 5);
INSERT INTO sharetypes (tag, "name", displayorder) values ('SHARE_FACEBOOK', 'Facebook', 6);
INSERT INTO sharetypes (tag, "name", displayorder) values ('SHARE_LINKEDIN', 'LinkedIn', 7);
INSERT INTO sharetypes (tag, "name", displayorder) values ('SHARE_TWITTER', 'Twitter', 8);


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;