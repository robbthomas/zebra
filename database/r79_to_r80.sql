-------------------------------------------------------------------------------
-- Additional changes in support of r61 which added the projects table
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES(80);

alter table "accountprojectnumbers_autohistory" alter column publishid drop not null;

DROP TRIGGER IF EXISTS trg_Published_Insert ON Published;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
