--------------------------------------------------------------------------------
-- fn_addresss.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- Addresses functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='Addresses' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('Addresses','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Addresses','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Addresses','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('Addresses','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('Addresses','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('Addresses','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_address_create(text, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text)

-- DROP FUNCTION fn_address_create(text, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_address_create(in_credentials_email text, in_credentials_password text, in_type text, in_accountId integer, in_name text, in_company text, in_address1 text, in_address2 text, in_city text, in_stateProvince text, in_zipPostalCode text, in_country text, in_website text, in_twitter text, in_email text, in_phone text, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Addresses';
        var_functionName text := 'CREATE';

        var_editedById text;
        new_addressId int := -1;
        new_addressTypeId int;

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- set editing user
        var_editedById := COALESCE(in_editedById,'');
        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        -- get type Id
        new_addressTypeId := addressTelephoneTypeId FROM AddressTelephoneTypes WHERE tag ILIKE in_type;

        insert into Addresses(
            typeId,
            accountId,
            name,
            company,
            address1,
            address2,
            city,
            stateProvince,
            zipPostalCode,
            country,
            website,
            twitter,
            email,
            phone,
            editedById
        )
        values(
            new_addressTypeId,
            in_accountId,
            in_name,
            in_company,
            in_address1,
            in_address2,
            in_city,
            in_stateProvince,
            in_zipPostalCode,
            in_country,
            in_website,
            in_twitter,
            in_email,
            in_phone,
            var_editedById
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_addressId := currval('seq_Addresses_addressId');
        END IF;

        return new_addressId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_address_create(text, text, text, integer, text, text, text, text, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_address_update(text, text, integer, text, integer, text, text, text, text, text, text, text, text, text, text)

-- DROP FUNCTION fn_address_update(text, text, integer, text, integer, text, text, text, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_address_update(in_credentials_email text, in_credentials_password text, in_addressId integer, in_type text, in_accountId integer, in_name text, in_company text, in_address1 text, in_address2 text, in_city text, in_stateProvince text, in_zipPostalCode text, in_country text, in_website text, in_twitter text, in_email text, in_phone text, in_editedById text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Addresses';
        var_functionName text := 'UPDATE';

        rec Addresses%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from Addresses where addressId = in_addressId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_type' THEN
                rec.typeId := addressTelephoneTypeId FROM AddressTelephoneTypes WHERE tag ILIKE in_type;
            ELSEIF param_name = 'in_accountId' THEN
                rec.accountId := in_accountId;
            ELSEIF param_name = 'in_name' THEN
                rec.name := in_name;
            ELSEIF param_name = 'in_company' THEN
                rec.company := in_company;
            ELSEIF param_name = 'in_address1' THEN
                rec.address1 := in_address1;
            ELSEIF param_name = 'in_address2' THEN
                rec.address2 := in_address2;
            ELSEIF param_name = 'in_city' THEN
                rec.city := in_city;
            ELSEIF param_name = 'in_stateProvince' THEN
                rec.stateProvince := in_stateProvince;
            ELSEIF param_name = 'in_zipPostalCode' THEN
                rec.zipPostalCode := in_zipPostalCode;
            ELSEIF param_name = 'in_country' THEN
                rec.country := in_country;
            ELSEIF param_name = 'in_website' THEN
                rec.website := in_website;
            ELSEIF param_name = 'in_twitter' THEN
                rec.twitter := in_twitter;
            ELSEIF param_name = 'in_email' THEN
                rec.email := in_email;
            ELSEIF param_name = 'in_phone' THEN
                rec.phone := in_phone;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE Addresses SET
            typeId = coalesce(rec.typeId, typeId),
            accountId = rec.accountId,
            name = rec.name,
            company = rec.company,
            address1 = rec.address1,
            address2 = rec.address2,
            city = rec.city,
            stateProvince = rec.stateProvince,
            zipPostalCode = rec.zipPostalCode,
            country = rec.country,
            website = rec.website,
            twitter = rec.twitter,
            email = rec.email,
            phone = rec.phone,
            editedById = rec.editedById
        WHERE addressId = in_addressId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_address_update(text, text, integer, text, integer, text, text, text, text, text, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_address_retire(text, text, integer, text)

-- DROP FUNCTION fn_address_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_address_retire(in_credentials_email text, in_credentials_password text, in_addressId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Addresses';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE Addresses SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE addressId = in_addressId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_address_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_address_filter(text, text, integer, integer, text, text, integer, text, text, text, text, text, text, text, text, text)

-- DROP FUNCTION fn_address_filter(text, text, integer, integer, text, text, integer, text, text, text, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION fn_address_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_type text, in_accountId integer, in_name text, in_company text, in_address1 text, in_address2 text, in_city text, in_stateProvince text, in_zipPostalCode text, in_country text, in_website text, in_twitter text, in_email text, in_phone text, in_filter_list text)
  RETURNS SETOF Addresses AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Addresses';
        var_functionName text := 'FILTER';

        rec Addresses%ROWTYPE;
        filter_name text;
        var_typeId int;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM Addresses WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_addressId' THEN
            ELSEIF filter_name = 'in_type' THEN
                var_typeId := addressTelephoneTypeId FROM AddressTelephoneTypes WHERE tag ILIKE in_type;
                var_query := var_query || ' AND typeId = ' || var_typeId;
            ELSEIF filter_name = 'in_accountId' THEN
                var_query := var_query || ' AND accountId = ' || in_accountId;
            ELSEIF filter_name = 'in_name' THEN
                var_query := var_query || ' AND name ILIKE ''%' || in_name || '%''';
            ELSEIF filter_name = 'in_company' THEN
                var_query := var_query || ' AND company ILIKE ''%' || in_company || '%''';
            ELSEIF filter_name = 'in_address1' THEN
                var_query := var_query || ' AND address1 ILIKE ''%' || in_address1 || '%''';
            ELSEIF filter_name = 'in_address2' THEN
                var_query := var_query || ' AND address2 ILIKE ''%' || in_address2 || '%''';
            ELSEIF filter_name = 'in_city' THEN
                var_query := var_query || ' AND city ILIKE ''%' || in_city || '%''';
            ELSEIF filter_name = 'in_stateProvince' THEN
                var_query := var_query || ' AND stateProvince ILIKE ''%' || in_stateProvince || '%''';
            ELSEIF filter_name = 'in_zipPostalCode' THEN
                var_query := var_query || ' AND zipPostalCode ILIKE ''%' || in_zipPostalCode || '%''';
            ELSEIF filter_name = 'in_country' THEN
                var_query := var_query || ' AND country ILIKE ''%' || in_country || '%''';
            ELSEIF filter_name = 'in_website' THEN
                var_query := var_query || ' AND country ILIKE ''%' || in_website || '%''';
            ELSEIF filter_name = 'in_twitter' THEN
                var_query := var_query || ' AND country ILIKE ''%' || in_twitter || '%''';
            ELSEIF filter_name = 'in_email' THEN
                var_query := var_query || ' AND country ILIKE ''%' || in_email || '%''';
            ELSEIF filter_name = 'in_phone' THEN
                var_query := var_query || ' AND country ILIKE ''%' || in_phone || '%''';
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            ELSEIF filter_name = 'in_retired' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM Addresses;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_address_filter(text, text, integer, integer, text, text, integer, text, text, text, text, text, text, text, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_address_find(text, text, integer)

-- DROP FUNCTION fn_address_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_address_find(in_credentials_email text, in_credentials_password text, in_addressId integer)
  RETURNS SETOF Addresses AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'Addresses';
        var_functionName text := 'FIND';

        rec Addresses%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM Addresses
                WHERE addressId = in_addressId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_address_find(text, text, integer) OWNER TO postgres;
