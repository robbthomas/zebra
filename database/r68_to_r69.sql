-------------------------------------------------------------------------------
-- change not-nulls in publishlaunchcounts
--
-- 
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (69);

alter table publishlaunchcounts alter column publishid drop not null;
alter table publishlaunchcounts alter column projectid set not null;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;