/*

SELECT fn_project_list_count(
      2
    , false 
    , 9 
    , 0 
    , 4360 
    , 'createdDateTime desc'
    , NULL 
    , NULL 
    , NULL 
    , NULL 
    , 4360 
    , FALSE 
    , FALSE 
    , TRUE
    , NULL 
    , TRUE 
    , '064a029aabdc47eeb87ddd69d7aa4096'
    , NULL
);

SELECT * FROM fn_Project_List_Small_Count (
      2
    , true
    , 9
    , 4360
    , '064a029aabdc47eeb87ddd69d7aa4096'
    , NULL
    , NULL
    , NULL
    , NULL
)

*/

CREATE OR REPLACE FUNCTION fn_Project_List_Small_Count (
      in_projectTypeId     INT4
    , in_isPublished       BOOLEAN
    , in_limit             INT4
    , in_searcherAccountId INT8
    , in_searcherMemberId  TEXT
    , in_categoryId        BIGINT
    , in_genericSearch     TEXT
    , in_projectName       TEXT
    , in_projectIdList     TEXT
    , in_urlName           TEXT
) RETURNS BIGINT AS $$ 
    DECLARE resultCount BIGINT;
    BEGIN
        
        SELECT INTO resultCount COUNT(projects.projectId)
        FROM projects
            INNER JOIN app_user
                ON projects.authorMemberId = app_user.id
            LEFT OUTER JOIN accountProjectNumbers userApn
                ON projects.projectId = userApn.projectId
                AND userApn.retired = 0
                AND in_IsPublished = true
            LEFT OUTER JOIN publishImages icon
                ON projects.projectId = icon.projectId
                AND icon.iconFileId IS NOT NULL
                AND icon.retired = FALSE
            LEFT OUTER JOIN projectTypes 
                ON projects.projectTypeId = projectTypes.projectTypeId
            LEFT OUTER JOIN LicenseTypes
                ON projects.licenseTypeId = LicenseTypes.licenseTypeId
            LEFT OUTER JOIN PublishedPurchasesAcquisitions ppa 
                ON projects.projectId = ppa.projectId 
            LEFT OUTER JOIN ZappRatingCaches zrc 
                ON projects.projectId = zrc.projectId 
        WHERE 1=1 
         AND CASE 
            WHEN in_projectIdList IS NOT NULL THEN
                projects.projectId IN (in_projectIdList)
            ELSE 1=1
                AND projects.retired = false
                AND CASE WHEN in_urlName IS NOT NULL THEN UPPER(projects.urlName) = UPPER(in_urlName) ELSE TRUE END
                AND CASE 
                    WHEN in_isPublished THEN 
                        CASE 
                            WHEN in_searcherAccountId IS NOT NULL THEN userApn.accountId = in_searcherAccountId
                            ELSE projects.authorMemberId = in_searcherMemberId
                        END
                        AND projects.published = TRUE
                        AND projects.nextPublishedVersionId IS NULL
                    ELSE 
                        projects.authorMemberId = in_searcherMemberId 
                        AND projects.nextVersionID IS NULL
                END
                AND CASE WHEN in_projectTypeId IS NOT NULL THEN projects.projectTypeId = in_projectTypeId ELSE TRUE END
                AND CASE
                    WHEN in_categoryId IS NOT NULL THEN projects.categoryId = in_categoryId
                    ELSE TRUE
                END
                AND CASE 
                        WHEN in_genericSearch IS NOT NULL THEN 
                            UPPER(projects.publishedName) LIKE '%' || UPPER(in_genericSearch) || '%' 
                            OR UPPER(projects.description) LIKE '%' || UPPER(in_genericSearch) || '%'
                            OR UPPER(app_user.firstname) LIKE '%' || UPPER(in_genericSearch) || '%'
                            OR UPPER(app_user.lastname) LIKE '%' || UPPER(in_genericSearch) || '%'
                            OR UPPER(app_user.displayname) LIKE '%' || UPPER(in_genericSearch) || '%'
                            OR UPPER(app_user.firstName) || ' ' || UPPER(app_user.lastName) LIKE '%' || UPPER(in_genericSearch) || '%'
                        ELSE TRUE 
                    END
                AND CASE WHEN in_projectName IS NOT NULL THEN UPPER(projects.projectName) LIKE '%' || UPPER(in_projectName) || '%' ELSE TRUE END
        END -- end of in_projectIdList CASE statement. It's a biggie, because that parameter is a master override.

        ;
        RETURN resultCount;
    END
$$ LANGUAGE 'plpgsql' VOLATILE
COST 100;




CREATE OR REPLACE FUNCTION fn_project_List_Count (
      in_projectTypeId  INT4
    , in_featured BOOLEAN
    , in_limit INT4
    , in_offset INT4
    , in_searcherAccountId INT8
    , in_orderBy VARCHAR
    , in_categoryId INT4
    , in_authorMemberId TEXT
    , in_searchText TEXT
    , in_genericSearch TEXT
    , in_ownerAccountId INT4
    , in_showContent BOOLEAN
    , in_isPublished BOOLEAN
    , in_hideInStoreList BOOLEAN
    , in_parentProjectId VARCHAR(255)
    , in_isForLatest BOOLEAN
    , in_searcherMemberId TEXT
    , in_projectname VARCHAR(255)
    , in_urlName VARCHAR(256)
    , in_projectIdList TEXT
) RETURNS BIGINT
AS $$ 
    DECLARE resultCount BIGINT;
    BEGIN
        SELECT INTO resultCount COUNT(InnerProjects.projectId)
            FROM projects InnerProjects
                INNER JOIN app_user 
                    ON app_user.id = InnerProjects.authorMemberId 
                LEFT JOIN parent_child 
                    ON parent_child.child_id = InnerProjects.projectId
                    AND parent_child.parent_children_id = in_parentProjectId
                INNER JOIN accounts authorAccount 
                    LEFT OUTER JOIN companies AS authorCompany
                        ON authorAccount.companyId = authorCompany.companyId
                    ON authorAccount.accountId = InnerProjects.accountId 
                    INNER JOIN AccountTypes authorAccountType 
                        ON authorAccount.typeId = authorAccountType.accounttypeid 
                        AND authorAccountType.tag <> 'collector'
                    LEFT OUTER JOIN AccountProjectNumbers publisherApn 
                        ON publisherApn.accountId = authorAccount.accountId
                        AND publisherApn.projectId = InnerProjects.projectId
                        AND publisherApn.retired = 0
                LEFT OUTER JOIN accountTypes projectAccountType 
                    ON InnerProjects.accountTypeId = projectAccountType.accountTypeId 
                LEFT OUTER JOIN LicenseTypes
                    ON InnerProjects.licenseTypeId = LicenseTypes.licenseTypeId
                LEFT OUTER JOIN PublishedPurchasesAcquisitions ppa 
                    ON InnerProjects.projectId = ppa.projectId 
                LEFT OUTER JOIN AccountProjectNumbers userApn 
                    ON InnerProjects.projectId = userApn.projectId 
                    AND userApn.retired = 0
                    AND userApn.accountId = COALESCE(in_ownerAccountId, in_searcherAccountId)
            WHERE 1=1 
            AND CASE WHEN in_projectIdList IS NOT NULL THEN InnerProjects.projectId IN (in_projectIdList)
            ELSE (
                1=1
                AND CASE WHEN in_projectTypeId IS NOT NULL   THEN InnerProjects.projectTypeId     = in_projectTypeId   ELSE TRUE END
                AND CASE WHEN in_featured                    THEN InnerProjects.featured          = TRUE               ELSE TRUE END
                AND CASE WHEN in_isPublished                 THEN InnerProjects.published         = TRUE               ELSE TRUE END
                AND CASE WHEN in_categoryId IS NOT NULL      THEN InnerProjects.categoryId        = in_categoryId      ELSE TRUE END
                AND CASE WHEN in_authorMemberId IS NOT NULL  THEN InnerProjects.authorMemberId    = in_authorMemberId  ELSE TRUE END
                AND CASE WHEN in_parentProjectId IS NOT NULL THEN parent_child.parent_children_id IS NOT NULL ELSE TRUE END
                AND CASE WHEN in_hideInStoreList IS NOT NULL 
                          AND in_hideInStoreList = FALSE     THEN InnerProjects.hideInStoreList   = FALSE              ELSE TRUE END
                AND CASE WHEN in_ownerAccountId IS NOT NULL THEN userApn.accountProjectNumberId IS NOT NULL ELSE TRUE END
                AND CASE 
                    WHEN in_isForLatest THEN 
                        CASE
                            WHEN in_isPublished THEN InnerProjects.nextPublishedVersionId IS NULL
                            ELSE InnerProjects.nextVersionId IS NULL
                        END 
                        AND InnerProjects.retired = FALSE
                        AND (publisherApn.retired IS NULL OR publisherApn.retired = 0)
                        AND CASE
                            WHEN in_authorMemberId IS NOT NULL AND NOT in_authorMemberId = in_searcherMemberId THEN InnerProjects.hideInStoreList = false
                            ELSE TRUE
                        END
                    ELSE TRUE
                END
                AND CASE WHEN in_projectName IS NOT NULL THEN UPPER(innerProjects.projectName) = UPPER(in_projectName) ELSE TRUE END
                AND CASE WHEN in_urlName IS NOT NULL THEN UPPER(innerProjects.urlName) = UPPER(in_urlName) ELSE TRUE END
                AND CASE 
                        WHEN in_searchText IS NOT NULL AND in_searchText <> '' THEN 
                            UPPER(innerProjects.publishedName) = UPPER(in_searchText)
                            OR UPPER(innerProjects.urlName) = UPPER(in_searchText) 
                        ELSE TRUE 
                    END
                AND CASE 
                    WHEN in_genericSearch IS NOT NULL THEN 
                        UPPER(innerProjects.publishedName) LIKE '%' || UPPER(in_genericSearch) || '%' 
                        OR UPPER(innerProjects.description) LIKE '%' || UPPER(in_genericSearch) || '%'
                        OR UPPER(app_user.firstname) LIKE '%' || UPPER(in_genericSearch) || '%'
                        OR UPPER(app_user.lastname) LIKE '%' || UPPER(in_genericSearch) || '%'
                        OR UPPER(app_user.displayname) LIKE '%' || UPPER(in_genericSearch) || '%'
                        OR UPPER(app_user.firstName) || ' ' || UPPER(app_user.lastName) LIKE '%' || UPPER(in_genericSearch) || '%'
                    ELSE TRUE 
                END
            )
            END

;

        RETURN resultCount;
    END
$$ LANGUAGE plpgsql;









CREATE OR REPLACE FUNCTION fn_Project_List_My_Count (
      in_projectTypeId INT4
    , in_isPublished BOOLEAN
    , in_searcherAccountId INT8
    , in_authorMemberId VARCHAR(255)
    , in_categoryId INT4
    , in_genericSearch TEXT
) RETURNS BIGINT AS $$ 
    DECLARE resultCount BIGINT;
    BEGIN
        
        SELECT INTO resultCount COUNT(projects.projectId)
        FROM projects
            INNER JOIN app_user
                ON projects.authorMemberId = app_user.id
            INNER JOIN accounts authorAccount 
                LEFT OUTER JOIN companies AS authorCompany
                    ON authorAccount.companyId = authorCompany.companyId
                ON authorAccount.accountId = projects.accountId 
                INNER JOIN AccountTypes authorAccountType 
                    ON authorAccount.typeId = authorAccountType.accounttypeid 
            LEFT OUTER JOIN AccountProjectNumbers userApn 
                ON projects.projectId = userApn.projectId 
                AND userApn.retired = 0
                AND userApn.accountId = in_searcherAccountId
            LEFT OUTER JOIN PublishedPurchasesAcquisitions ppa 
                ON projects.projectId = ppa.projectId 
    WHERE 1=1
    AND projects.projectTypeId = in_projectTypeId
    AND CASE 
        WHEN in_isPublished THEN userApn.accountId = in_searcherAccountId
        ELSE projects.authorMemberId = in_authorMemberId 
             AND projects.nextVersionId IS NULL 
             AND projects.retired = FALSE
    END
    AND CASE WHEN in_categoryId IS NOT NULL      THEN projects.categoryId        = in_categoryId      ELSE TRUE END
    AND CASE 
        WHEN in_genericSearch IS NOT NULL THEN 
            UPPER(projects.publishedName) LIKE '%' || UPPER(in_genericSearch) || '%' 
            OR UPPER(projects.projectName) LIKE '%' || UPPER(in_genericSearch) || '%'
            OR UPPER(projects.description) LIKE '%' || UPPER(in_genericSearch) || '%'
            OR UPPER(app_user.firstname) LIKE '%' || UPPER(in_genericSearch) || '%'
            OR UPPER(app_user.lastname) LIKE '%' || UPPER(in_genericSearch) || '%'
            OR UPPER(app_user.displayname) LIKE '%' || UPPER(in_genericSearch) || '%'
            OR UPPER(app_user.firstName) || ' ' || UPPER(app_user.lastName) LIKE '%' || UPPER(in_genericSearch) || '%'
        ELSE TRUE 
    END
        ;
        RETURN resultCount;
    END
$$ LANGUAGE 'plpgsql' VOLATILE
COST 100;