DROP TYPE IF EXISTS commentFeedbackType CASCADE;

CREATE type commentFeedbackType AS (
    helpfulCount int,
    feedbackCount int,
    debug text
);

CREATE OR REPLACE FUNCTION fn_updatecommentfeedback(in_credentials_email text, in_credentials_password text, in_commentId int, in_memberId text, in_feedback text)
  RETURNS commentFeedbackType AS $$

    DECLARE
        myCurFeedback text;
	cft commentFeedbackType;
    BEGIN

        SELECT INTO myCurFeedback feedback FROM CommentFeedbacks WHERE commentId = in_commentId AND memberId = in_memberId;

        SELECT INTO cft helpfulCount, feedbackCount FROM ZappComments WHERE zappcommentid = in_commentId;


        IF myCurFeedback IS NULL THEN       -- I've never left feedback for this comment
            PERFORM fn_commentfeedback_create(in_credentials_email, in_credentials_password, in_commentId, in_memberId, in_feedback);

            cft.feedbackCount = cft.feedbackCount + 1;

            IF (in_feedback = 'helpful') THEN
                cft.helpfulCount = cft.helpfulCount + 1;
                UPDATE ZappComments SET helpfulCount = helpfulCount + 1, feedbackCount = feedbackCount + 1 WHERE zappcommentId = in_commentId;
            ELSE
                UPDATE ZappComments SET feedbackCount = feedbackCount + 1 WHERE zappcommentId = in_commentId;
            END IF;           
            
            cft.debug = 'new';            
        ELSE                            -- I'm updating my rating
            UPDATE CommentFeedbacks SET feedback = in_feedback WHERE commentId = in_commentId AND memberId = in_memberId;

            IF (myCurFeedback = 'helpful' AND in_feedback != 'helpful') THEN
                cft.helpfulCount = cft.helpfulCount - 1;
                UPDATE ZappComments SET helpfulCount = helpfulCount - 1 WHERE zappcommentId = in_commentId;
            END IF;
            IF (myCurFeedback != 'helpful' AND in_feedback = 'helpful') THEN
                cft.helpfulCount = cft.helpfulCount + 1;
                UPDATE ZappComments SET helpfulCount = helpfulCount + 1 WHERE zappcommentId = in_commentId;
            END IF;           

            cft.debug = 'updated';
        END IF;        

	return cft;

    END;

$$  LANGUAGE 'plpgsql' VOLATILE
  COST 100;


-- TEST THIS OUT
-- SELECT * FROM fn_updatecommentfeedback('bennew@qp.com', 'foo', 2021, 'e0e18fc27ff44661857331332cac2bf4', 'helpful');
-- SELECT * FROM fn_updatecommentfeedback('ben11@qp.com', 'foo', 2021, '1cad10743f6647a59e68dff23e7598d7', 'notHelpful');
-- SELECT * FROM fn_updatecommentfeedback('ben11@qp.com', 'foo', 2021, '1cad10743f6647a59e68dff23e7598d7', 'helpful');


