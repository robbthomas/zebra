
-- fn_creditcardexpires.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
---------------------------------------------------------------------
-- Function: fn_creditcard_expiring(integer)

DROP FUNCTION IF EXISTS fn_creditcard_expiring(integer);

CREATE OR REPLACE FUNCTION fn_creditcard_expiring(in_credentials_email text, in_credentials_password text, in_withinDays integer)
  RETURNS SETOF CreditCards AS
$BODY$
    DECLARE
        var_credentialsId text;

        rec CreditCards%ROWTYPE;
        var_withinDaysInterval text;
        var_withinDateTime timestamp;

    BEGIN
        var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            LIMIT 1;

        IF var_credentialsId IS NOT NULL THEN

            var_withinDaysInterval := CAST((COALESCE(in_withinDays,0) + 1) As text) || ' days';
            var_withinDateTime := current_date + var_withinDaysInterval::interval;

            FOR rec IN
                SELECT * FROM CreditCards
                WHERE retired = 0
		AND expirationYear IS NOT NULL AND LENGTH(TRIM(BOTH FROM expirationYear)) > 0
                AND expirationMonth IS NOT NULL AND LENGTH(TRIM(BOTH FROM expirationMonth)) > 0
                AND CAST(expirationYear As integer) <= DATE_PART('year', var_withinDateTime)
                AND CAST(expirationMonth As integer) <= DATE_PART('month', var_withinDateTime)
                ORDER BY expirationYear, expirationMonth
            LOOP
                RETURN NEXT rec;
            END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_creditcard_expiring(integer) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_creditcard_validforaccount(text, text, bigint)

DROP FUNCTION IF EXISTS fn_creditcard_validforaccount(text, text, integer);

CREATE OR REPLACE FUNCTION fn_creditcard_validforaccount(in_credentials_email text, in_credentials_password text, in_accountId bigint)
  RETURNS SETOF CreditCards AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'CreditCards';
        var_functionName text := 'FIND';

        rec CreditCards%ROWTYPE;

    BEGIN
        var_credentialsId := id FROM App_User au, AccountMemberRoles amr, TableFunctionRoleMap tfr
            WHERE au.id = fn_get_credentials_id(in_credentials_email, in_credentials_password)
            AND au.id = amr.memberId
            AND amr.accountId = in_accountId
            AND amr.typeId = tfr.roleTypeId
            AND tfr.tableName = var_tableName
            AND tfr.functionName = var_functionName
            LIMIT 1;

        RAISE NOTICE 'var_credentialsId=%',var_credentialsId;

        IF var_credentialsId IS NULL THEN
            -- check for sys admin login
            var_credentialsId := memberId FROM SystemAdministrators WHERE memberId = fn_get_credentials_id(in_credentials_email, in_credentials_password)
                LIMIT 1;
        END IF;

        RAISE NOTICE 'var_credentialsId=%',var_credentialsId;

        IF var_credentialsId IS NOT NULL THEN

            FOR rec IN
                SELECT * FROM CreditCards
                WHERE retired = 0
		AND accountId = in_accountId
		AND expirationYear IS NOT NULL AND LENGTH(TRIM(BOTH FROM expirationYear)) > 0
                AND expirationMonth IS NOT NULL AND LENGTH(TRIM(BOTH FROM expirationMonth)) > 0
                AND CAST(expirationYear As integer) >= DATE_PART('year', current_date)
                AND CAST(expirationMonth As integer) >= DATE_PART('month', current_date)
                ORDER BY expirationYear DESC, expirationMonth DESC
            LOOP
                RETURN NEXT rec;
            END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_creditcard_validforaccount(text, text, integer) OWNER TO postgres;


