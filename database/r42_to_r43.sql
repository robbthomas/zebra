-------------------------------------------------------------------------------
-- r42_to_r43.sql
--
-- updating termduedate for accounts with termduedate before 11-01-2011
--
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
-------------------------------------------------------------------------------
-- Update database version.
-------------------------------------------------------------------------------
INSERT INTO DatabaseVersion(versionNumber) VALUES(43);

select distinct termduedatetime, timestamp '2012-01-01' from accounts where termduedatetime < timestamp '2011-11-01';

-------------------------------------------------------------------------------
-- End transaction wrapper for this update.
-------------------------------------------------------------------------------
COMMIT;
