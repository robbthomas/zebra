-------------------------------------------------------------------------------
-- created_date fields formerly managed by a Spring pointcut/asset.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;

INSERT INTO DatabaseVersion(versionNumber) VALUES(82);


update projects set editeddatetime = createddatetime where createddatetime is not null 
    and date_part('minute', editeddatetime) = 0 
    and date_part('second', editeddatetime) = 0
    and date_part('second', editeddatetime) = 0
    and date_part('month', editeddatetime) = date_part('month', createddatetime)
    and date_part('day', editeddatetime) = date_part('day', createddatetime)
    and date_part('year', editeddatetime) = date_part('year', createddatetime)
;

alter table projects alter column editeddatetime set default now();

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;
