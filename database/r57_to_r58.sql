-------------------------------------------------------------------------------
-- r53_to_r54.sql
--
-- create index on guestlistinvitees
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (58);
-------------------------------------------------------------------------------

insert into categories (name, typeid, editedbyid, retired) values ('ZebraZapps Examples', 1, (select id from app_user where displayname = 'dhoyt'), 0);
insert into categories (name, typeid, editedbyid, retired) values ('ZebraZapps Examples', 2, (select id from app_user where displayname = 'dhoyt'), 0);


-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;