--------------------------------------------------------------------------------
-- fn_accounttypes.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- AccountTypes functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='AccountTypes' AND
    (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('AccountTypes','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('AccountTypes','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('AccountTypes','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('AccountTypes','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('AccountTypes','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('AccountTypes','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_accounttype_create(text, text, text, text, text, text, integer, integer, integer, text)

-- DROP FUNCTION fn_accounttype_create(text, text, text, text, text, text, integer, integer, integer, text);

CREATE OR REPLACE FUNCTION fn_accounttype_create(in_credentials_email text, in_credentials_password text, in_tag text, in_name text, in_description text, in_terms text, in_price integer, in_currencyTypeId integer, in_displayOrder integer, in_editedById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountTypes';
        var_functionName text := 'CREATE';

        new_accountTypeId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        insert into AccountTypes(
            tag,
            name,
            description,
            terms,
            price,
            currencyTypeId,
            displayOrder,
            editedById
        )
        values(
            in_tag,
            in_name,
            in_description,
            in_terms,
            in_price,
            in_currencyTypeId,
            in_displayOrder,
            in_editedById
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_accountTypeId := currval('seq_AccountTypes_accountTypeId');
        END IF;

        return new_accountTypeId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_accounttype_create(text, text, text, text, text, text, integer, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_accounttype_update(text, text, integer, text, text, text, text, integer, integer, integer, text, text)

-- DROP FUNCTION fn_accounttype_update(text, text, integer, text, text, text, text, integer, integer, integer, text, text);

CREATE OR REPLACE FUNCTION fn_accounttype_update(in_credentials_email text, in_credentials_password text, in_accountTypeId integer, in_tag text, in_name text, in_description text, in_terms text, in_price integer, in_currencyTypeId integer, in_displayOrder integer, in_editedById text, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountTypes';
        var_functionName text := 'UPDATE';

        rec AccountTypes%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from AccountTypes where accountTypeId = in_accountTypeId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

            IF param_name = 'in_tag' THEN
                rec.tag := in_tag;
            ELSEIF param_name = 'in_name' THEN
                rec.name := in_name;
            ELSEIF param_name = 'in_description' THEN
                rec.description := in_description;
            ELSEIF param_name = 'in_terms' THEN
                rec.terms := in_terms;
            ELSEIF param_name = 'in_price' THEN
                rec.price := in_price;
            ELSEIF param_name = 'in_currencyTypeId' THEN
                rec.currencyTypeId := in_currencyTypeId;
            ELSEIF param_name = 'in_displayOrder' THEN
                rec.displayOrder := in_displayOrder;
            ELSEIF param_name = 'in_editedById' THEN
                rec.editedById := in_editedById;
            END IF;
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE AccountTypes SET
            tag = rec.tag,
            name = rec.name,
            description = rec.description,
            terms = rec.terms,
            price = rec.price,
            currencyTypeId = rec.currencyTypeId,
            displayOrder = rec.displayOrder,
            editedById = rec.editedById
        WHERE accountTypeId = in_accountTypeId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_accounttype_update(text, text, integer, text, text, text, text, integer, integer, integer, text, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_accounttype_retire(text, text, integer, text)

-- DROP FUNCTION fn_accounttype_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_accounttype_retire(in_credentials_email text, in_credentials_password text, in_accountTypeId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountTypes';
        var_functionName text := 'RETIRE';

        var_count int := -1;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        UPDATE AccountTypes SET retired = 1, retiredById = in_retiredById, retiredDateTime = now()
        WHERE accountTypeId = in_accountTypeId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_accounttype_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_accounttype_filter(text, text, integer, integer, text, text, text, text, text, integer, integer, integer, text)

-- DROP FUNCTION fn_accounttype_filter(text, text, integer, integer, text, text, text, text, text, integer, integer, integer, text);

CREATE OR REPLACE FUNCTION fn_accounttype_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, in_tag text, in_name text, in_description text, in_terms text, in_price integer, in_currencyTypeId integer, in_displayOrder integer, in_filter_list text)
  RETURNS SETOF AccountTypes AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountTypes';
        var_functionName text := 'FILTER';

        rec AccountTypes%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM AccountTypes WHERE retired = 0';

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

            IF filter_name = 'in_accountTypeId' THEN
            ELSEIF filter_name = 'in_tag' THEN
                var_query := var_query || ' AND tag ILIKE ''%' || in_tag || '%''';
            ELSEIF filter_name = 'in_name' THEN
                var_query := var_query || ' AND name ILIKE ''%' || in_name || '%''';
            ELSEIF filter_name = 'in_description' THEN
                var_query := var_query || ' AND description ILIKE ''%' || in_description || '%''';
            ELSEIF filter_name = 'in_terms' THEN
                var_query := var_query || ' AND terms ILIKE ''%' || in_terms || '%''';
            ELSEIF filter_name = 'in_price' THEN
                var_query := var_query || ' AND price = ' || in_price;
            ELSEIF filter_name = 'in_currencyTypeId' THEN
                var_query := var_query || ' AND currencyTypeId = ' || in_currencyTypeId;
            ELSEIF filter_name = 'in_displayOrder' THEN
                var_query := var_query || ' AND displayOrder = ' || in_displayOrder;
            ELSEIF filter_name = 'in_editedById' THEN
            ELSEIF filter_name = 'in_editedDateTime' THEN
            ELSEIF filter_name = 'in_retired' THEN
            END IF;
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM AccountTypes;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_accounttype_filter(text, text, integer, integer, text, text, text, text, text, integer, integer, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_accounttype_find(text, text, integer)

-- DROP FUNCTION fn_accounttype_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_accounttype_find(in_credentials_email text, in_credentials_password text, in_accountTypeId integer)
  RETURNS SETOF AccountTypes AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'AccountTypes';
        var_functionName text := 'FIND';

        rec AccountTypes%ROWTYPE;

    BEGIN
        var_credentialsId := fn_get_credentials_id(in_credentials_email, in_credentials_password);

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM AccountTypes
                WHERE accountTypeId = in_accountTypeId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_accounttype_find(text, text, integer) OWNER TO postgres;
