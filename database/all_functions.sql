-- Add PL/SQL function files for new tables here.
--
-- function files should only contain PL/SQL functions.
-- They should NOT contain schema changes.
-- If new tables are added, the corresponding
-- function file needs to be added here.

-- first we load pgcrypto.sql, uuid-ossp.sql, and citext.sql modules
-- to support App_User passwords hashing, Id random UUID generation,
-- and split_part parsing of filter function params

BEGIN;

-- Mac:  /Library/PostgreSQL/8.4/share/postgresql/contrib/
--\i /usr/share/postgresql/8.4/contrib/pgcrypto.sql
\i /usr/share/postgresql/8.4/contrib/uuid-ossp.sql

\i fn_accountmemberrole.sql
\i fn_accountprojectnumber.sql
\i fn_account.sql
\i fn_accounttype.sql
\i fn_address.sql
\i fn_appuser.sql
\i fn_category.sql
\i fn_company.sql
\i fn_couponcodetype.sql
\i fn_couponcode.sql
\i fn_creditcard.sql
\i fn_echeck.sql
\i fn_feedback.sql
\i fn_financialtransaction.sql
\i fn_invoicelineitem.sql
\i fn_invoice.sql
\i fn_payment.sql
\i fn_paymentlineitem.sql
\i fn_projectmemberrole.sql
--\i fn_publish.sql
\i fn_roletype.sql
\i fn_screenshot.sql
\i fn_telephone.sql
\i fn_creditcardtype.sql
\i fn_projectfeedback.sql
--\i fn_sharedproject.sql
--\i fn_sharedprojectinvitee.sql
--\i fn_sharetype.sql

-- following functions sql NOT CRUD generated
\i fn_account_subscription.sql
\i fn_appuser_login.sql
\i fn_creditcardexpires.sql
\i fn_subscription.sql
\i fn_invoicebatchcharges.sql
\i fn_paymentbatchcredits.sql
\i fn_registration.sql
--\i fn_accountprojectnumber_exists.sql -- not being used
\i fn_publishedpurchasesacquisitions.sql
\i fn_zappstatistics.sql

\i fn_zappcomment.sql
\i fn_zapprating.sql
\i fn_zappratingcache.sql

\i fn_leads.sql

\i fn_updateratingandcache.sql

\i fn_project_List_Count.sql
\i fn_project_List.sql

COMMIT;
