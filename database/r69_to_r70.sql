-------------------------------------------------------------------------------
-- add new column to Projects table
--
-- 
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (70);

alter table projects add column nextpublishedversionid varchar(255) references projects(projectId);
alter table projects add column accountTypeId int references accountTypes(accountTypeId);

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;