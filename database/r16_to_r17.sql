-------------------------------------------------------------------------------
-- r16_to_r17.sql
--
-- Add Published.hideInStoreList and save/redo/restore Published_AutoHistory.
--
-- Add guest user to accountId=1.
--
-- RE-drop Zephyr project columns dropped or renamed in r1_to_r2 and subsequent
-- scripts to undo unintended readd by Grails. NOTE that this is done is a
-- separate transaction outside the r17 upgrade so that it will fail gracefully
-- if applied to dbs which were not altered by Grails.
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (17);
-------------------------------------------------------------------------------

-- Add new column.
ALTER TABLE Published ADD COLUMN hideInStoreList BOOLEAN NOT NULL DEFAULT 'f';

-- Save existing autohistory.
SELECT * INTO TEMPORARY TABLE temp_Published_Autohistory FROM Published_Autohistory;

-- Recreate autohistory for Published.
DROP SEQUENCE seq_Published_AutoHistory CASCADE;
DROP TABLE Published_AutoHistory CASCADE;

CREATE SEQUENCE seq_Published_AutoHistory;
CREATE TABLE Published_AutoHistory (
    LIKE Published,
    Published_AutoHistory_Id bigint DEFAULT nextval('seq_Published_AutoHistory') NOT NULL PRIMARY KEY,
    AutoHistoryDate timestamp with time zone DEFAULT now()
);

CREATE OR REPLACE FUNCTION fn_trg_Published_Update()
RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO Published_AutoHistory SELECT OLD.*;
    RETURN OLD;
END;
$BODY$
Language 'plpgsql';

-- Restore saved autohistory.
INSERT INTO Published_Autohistory
(
    publishId,accountId,projectId,firstPublishId,versionMajor,versionMinor,versionDot,versionBuildNumber,
    categoryId,name,description,thumbnailId,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,
    editedById,retired,retiredById,retiredDateTime,screenshotId,hideInStoreList,published_autohistory_id,autohistorydate
)
SELECT
    publishId,accountId,projectId,firstPublishId,versionMajor,versionMinor,versionDot,versionBuildNumber,
    categoryId,name,description,thumbnailId,statusId,publishTypeId,licenseTypeId,price,currencyTypeId,
    editedById,retired,retiredById,retiredDateTime,screenshotId,'f',published_autohistory_id,autohistorydate
FROM temp_Published_Autohistory;

DROP TABLE temp_Published_Autohistory;


-- Create user guest and add to accountId=1 as anonymous user.
CREATE FUNCTION tmpfn_create_guest_user_and_account()
RETURNS text AS
$BODY$
    DECLARE
        var_count int;
        guestUserId text;
        guestAccountId int;

    BEGIN
        guestUserId := id FROM App_User WHERE displayName = 'guest';
        IF guestUserId IS NOT NULL THEN
            RAISE NOTICE 'User ''guest'' already exists, id=%', guestUserId;

        ELSE
            guestUserId := fn_appuser_create('', '', 'guest', 'description', 'guest', 'firstname', 'lastname', 'none', '', null);

            IF guestUserId IS NULL THEN
                RAISE NOTICE 'Failed guest user create';
                ROLLBACK;
            ELSE
                RAISE NOTICE 'Created guest user, id=%', guestUserId;
            END IF;

        END IF;

        IF guestUserId IS NOT NULL THEN
            IF NOT EXISTS (SELECT * FROM AccountMemberRoles WHERE memberId = guestUserId) THEN
                INSERT INTO Accounts (typeId, editedById, merchantCustomerId)
                VALUES (1, guestUserId, 'guestguest');

                GET DIAGNOSTICS var_count = Row_Count;
                IF var_count = 1 THEN
                    guestAccountId := currval('seq_Accounts_accountId');
                END IF;

                IF guestAccountId > 0 THEN
                    RAISE NOTICE 'Created guest account, id=%', guestAccountId;

                    INSERT INTO AccountMemberRoles(accountId, memberId, typeId)
                    VALUES (1, (SELECT id FROM App_User WHERE displayName='guest' LIMIT 1), (SELECT roleTypeId FROM RoleTypes WHERE tag='ROLE_TYPE_VIEWER' LIMIT 1));

                END IF;

            END IF;

        END IF;

        RETURN guestUserId;
    END;
$BODY$
LANGUAGE 'plpgsql';

SELECT * FROM tmpfn_create_guest_user_and_account();

DROP FUNCTION tmpfn_create_guest_user_and_account();

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

-------------------------------------------------------------------------------
-- Drop columns readded by Grails in App_User, Parent, and Gadget_Tag. We do
-- this in its own transaction so it will not fail to appy the r17 upgrade
-- in databases that did not get reworked by Grails.
-------------------------------------------------------------------------------
BEGIN;

ALTER TABLE App_User DROP COLUMN company;
ALTER TABLE App_User DROP COLUMN deleted;
ALTER TABLE App_User DROP COLUMN email_show;
ALTER TABLE App_User DROP COLUMN passwd;
ALTER TABLE App_User DROP COLUMN permissions;
ALTER TABLE App_User DROP COLUMN user_real_name;
ALTER TABLE App_User DROP COLUMN username;
ALTER TABLE App_User DROP COLUMN is_admin;

ALTER TABLE Parent DROP COLUMN class;
ALTER TABLE Parent DROP COLUMN gadget_ref_id;
ALTER TABLE Parent DROP COLUMN initial_value;
ALTER TABLE Parent DROP COLUMN user_id;
ALTER TABLE Parent DROP COLUMN initial_values;
ALTER TABLE Parent DROP COLUMN next_version_id;
ALTER TABLE Parent DROP COLUMN permissions;
ALTER TABLE Parent DROP COLUMN sharable;
ALTER TABLE Parent DROP COLUMN type;
ALTER TABLE Parent DROP COLUMN category;
ALTER TABLE Parent DROP COLUMN description;

ALTER TABLE Gadget_Tag DROP COLUMN gadget_id;

COMMIT;

