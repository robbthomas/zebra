-------------------------------------------------------------------------------
-- r33_to_r34.sql
--   add autosave boolean field to parent table
--
-------------------------------------------------------------------------------
-- Begin transaction wrapper for this update.
BEGIN;
INSERT INTO DatabaseVersion(versionNumber) VALUES (34);
-------------------------------------------------------------------------------
-- Add new column.
ALTER TABLE Parent ADD COLUMN Autosave boolean;

-------------------------------------------------------------------------------
-- End transaction wrapper for this version
-------------------------------------------------------------------------------
COMMIT;

