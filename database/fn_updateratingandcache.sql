DROP TYPE IF EXISTS ratingInfo CASCADE;

CREATE type ratingInfo AS (
    numRatings int,
    ratingTotal int,
    debug text
);


-- signature has changed, so we need to delete it.
-- rather than try to figure out if/when everyone's run this, and then commenting out the drop
-- do a create/replace first, then drop it.
create or replace FUNCTION fn_updateratingandcache(in_credentials_email text, in_credentials_password text, in_projectId int, in_memberId text, in_rating int)
  RETURNS int AS $$

    BEGIN
    return 0;
    END;
$$  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
drop function fn_updateratingandcache(text, text, int, text, int);
  
  
CREATE OR REPLACE FUNCTION fn_updateratingandcache(in_credentials_email text, in_credentials_password text, in_projectId text, in_memberId text, in_rating int)
  RETURNS ratingInfo AS $$

    DECLARE
        myCurRating int;
	   res ratingInfo;
    BEGIN

        SELECT INTO myCurRating rating FROM ZappRatings WHERE projectId = in_projectId AND memberId = in_memberId;

        SELECT INTO res numRatings, ratingTotal FROM
            projects p1 inner join projects p2 on p2.publishedName = p1.publishedName
            inner join ZappRatingCaches on ZappRatingCaches.projectId = p2.projectId 
            WHERE p1.projectId = in_projectId;


        IF myCurRating IS NULL THEN       -- I've never rating this Zapp
            PERFORM fn_zapprating_create(in_credentials_email, in_credentials_password, in_projectId, in_memberId, in_rating);

            IF res.numRatings IS NULL THEN  -- Nobdy has rated this Zapp
                res.numRatings = 1;
                res.ratingTotal = in_rating;
                PERFORM fn_zappratingcache_create(in_credentials_email, in_credentials_password, in_projectId, res.numRatings, res.ratingTotal);
                res.debug = 'new rating for all';
            ELSE                            -- others have rated this zapp, but not me.
                res.numRatings = res.numRatings + 1;
                res.ratingTotal = res.ratingTotal + in_rating;
                UPDATE ZappRatingCaches SET ratingTotal = ratingTotal + in_rating, numRatings = numRatings + 1 WHERE projectId = in_projectId;
                res.debug = 'new rating for me';
            END IF;
            
        ELSE                            -- I'm updating my rating
            UPDATE ZappRatings SET rating = in_rating WHERE projectId = in_projectId AND memberId = in_memberId;

            res.ratingTotal = res.ratingTotal + in_rating - myCurRating;
            UPDATE ZappRatingCaches SET ratingTotal = ratingTotal + in_rating - myCurRating WHERE projectId = in_projectId;

            res.debug = 'updated rating';
        END IF;        

	return res;

    END;

$$  LANGUAGE 'plpgsql' VOLATILE
  COST 100;


-- TEST THIS OUT
-- SELECT * FROM fn_updateratingandcache('ben@qp.com', 'foo', 63, 'aa009fdfa3d54af9bc87972cd76ba7a1', 5);
-- SELECT * FROM fn_updateratingandcache('ben@qp.com', 'foo', 63, '8a8181da2c561a8a012c561aa5960001', 4);
-- SELECT * FROM fn_updateratingandcache('ben@qp.com', 'foo', 63, '8a8181da2c561a8a012c561aa5bb0002', 4);
-- SELECT * FROM fn_updateratingandcache('ben@qp.com', 'foo', 63, 'aa009fdfa3d54af9bc87972cd76ba7a1', 4);


