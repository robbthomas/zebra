package com.alleni.savana {
import com.alleni.author.definition.FunctionLibraryContent;
import com.alleni.taconite.factory.TaconiteFactory;

import flash.utils.getTimer;

public class Ast {
public static const STRING_TYPE:String = "string";
public static const MATH_TYPE:String = "math";
public static const GENERAL_TYPE:String = "general";
public static const BINARY_TYPE:String = "binary";
public static const UNARY_TYPE:String = "unary";
public static const TERNARY_TYPE:String = "ternary";
public static const MODIFIER_TYPE:String = "modifier";
public static const KEY_TYPE:String = "key";
public static const FUNCTION_CATEGORY:String = "function";
public static const VARIABLE_CATEGORY:String = "variable";
public static const CONSTANT_CATEGORY:String = "constant";
public static const OPERATOR_CATEGORY:String = "operator";
public static const KEYPRESS_CATEGORY:String = "keypress";
public static const CALCULATOR_CATEGORY:String = "calculator";
public static const TIPS_CATEGORY:String = "tips";

public static function boolCast(obj:*):* {
    return !(obj == null || obj == 0 || obj == "" || obj == "false" || obj === false);
}

private static var binaryOp:* = {
    "+":function(lhs:*, rhs:*):*{return Number(lhs) + Number(rhs)},
    "-":function(lhs:*, rhs:*):*{return Number(lhs) - Number(rhs)},
    "*":function(lhs:*, rhs:*):*{return Number(lhs) * Number(rhs)},
    "/":function(lhs:*, rhs:*):*{return Number(lhs) / Number(rhs)},
    "%":function(lhs:*, rhs:*):*{return Number(lhs) % Number(rhs)},
    "=":function(lhs:*, rhs:*):*{return lhs == rhs},
    "<":function(lhs:*, rhs:*):*{return lhs < rhs},
    ">":function(lhs:*, rhs:*):*{return lhs > rhs},
    "<=":function(lhs:*, rhs:*):*{return lhs <= rhs},
    ">=":function(lhs:*, rhs:*):*{return lhs >= rhs},
    "<>":function(lhs:*, rhs:*):*{return lhs != rhs},
    "&":function(lhs:*, rhs:*):*{return boolCast(lhs) && boolCast(rhs)},
    "|":function(lhs:*, rhs:*):*{return boolCast(lhs) || boolCast(rhs)},
    "^":function(lhs:*, rhs:*):*{return String(lhs) + String(rhs)},
    "**":function(lhs:*, rhs:*):*{return Math.pow(Number(lhs), Number(rhs))}
};

private static var binaryOpType:* = {
    "+":"number",
    "-":"number",
    "*":"number",
    "/":"number",
    "%":"number",
    "=":"boolean",
    "<":"boolean",
    ">":"boolean",
    "<=":"boolean",
    ">=":"boolean",
    "<>":"boolean",
    "&":"boolean",
    "|":"boolean",
    "^":"string",
    "**":"number"
};

private static var unaryOp:* = {
    "+":function(rhs:*):*{return + Number(rhs)},
    "-":function(rhs:*):*{return - Number(rhs)},
    "~":function(rhs:*):*{return ! boolCast(rhs)}
};

private static var unaryOpType:* = {
    "+":"number",
    "-":"number",
    "~":"boolean"
};

public static function defaultEnvironment():* {
	return {
		sin:function(x:*):* { return Math.sin(Number(x))},
		cos:function(x:*):* { return Math.cos(Number(x))},
		tan:function(x:*):* { return Math.tan(Number(x))},
		asin:function(x:*):* { return Math.asin(Number(x))},
		acos:function(x:*):* { return Math.acos(Number(x))},
		atan:function(x:*):* { return Math.atan(Number(x))},
		atan2:function(y:*, x:*):* { return Math.atan2(Number(y), Number(x))},
		abs:function(x:*):* { return Math.abs(Number(x))},
		random:function():* { return Math.random()},
		min:Math.min,
		max:Math.max,
		sqrt:function(x:*):* { return Math.sqrt(Number(x))},
		pow:function(x:*, y:*):* { return Math.pow(Number(x), Number(y))},
		log:function(x:*):* { return Math.log(Number(x))},
		exp:function(x:*):* { return Math.exp(Number(x))},
		pi:Math.PI,
		e:Math.E,
		infinity:Infinity,
		nan:Number.NaN,
		isnan:function(val:*):* { return isNaN(val)},
        substr:function(str:*, index:*=0, length:*=Number.MAX_VALUE):* { return String(str).substr(index, length)},
        length:function(collection:*):* { return collection.length},
        touppercase:function(str:*):* { return String(str).toUpperCase()},
        tolowercase:function(str:*):* { return String(str).toLowerCase()},
        map:function(collection:*, func:*):* {
        		var result:* = [];
        		for(var i:*=0; i<collection.length; i++) {
        			var args:* = [collection[i], i, collection];
        			result[i] = func.apply(null,args);
        		}
        		return result;
        	},
        reduce:function(collection:*, func:*, identity:*=undefined):* {
	        	var result:* = identity;
	        	var first:* = true;
	        	for(var i:*=0; i<collection.length; i++) {
	        		if(first && result == undefined) {
	        			result = collection[i];
	        		} else {
	        			var args:* = [result, collection[i], i, collection];
		        		result = func.apply(null,args);
	        		}
	        	}
	        	return result;
        	},
        tofixed:function(num:*, digits:*):* {
                var result:* = Number(num).toFixed(digits);
                if(result.charAt(result.length-1) == ".") {
                    result = result.substr(0, result.length-1);
                }
                return result;
            },
        toexponential:function(num:*, digits:*):* {
                return Number(num).toExponential(digits);
            },
        toprecision:function(num:*, digits:*):* {
                return Number(num).toPrecision(digits);
            },
        floor:function(num:*, digits:*=0):* {
                if(digits != 0) {
                    num = num * Math.pow(10, digits);
                }
                num = Math.floor(num);
                if(digits != 0) {
                    num = num / Math.pow(10, digits);
                }
                return num
            },
        ceil:function(num:*, digits:*=0):* {
                if(digits != 0) {
                    num = num * Math.pow(10, digits);
                }
                num = Math.ceil(num);
                if(digits != 0) {
                    num = num / Math.pow(10, digits);
                }
                return num
            },
        round:function(num:*, digits:*=0):* {
                if(digits != 0) {
                    num = num * Math.pow(10, digits);
                }
                num = Math.round(num);
                if(digits != 0) {
                    num = num / Math.pow(10, digits);
                }
                return num
            },
        charcount:function(str:*):* {
                return String(str).length;
            },
        empty:function(collection:*):* {
                return collection.length == 0;
            },
        contains:function(collection:*, item:*):* {
                return collection.indexOf(item) >= 0;
            },
        rgb:function(r:*, g:*, b:*):* {
                r = Math.max(0, Math.min(255, Number(r)));
                g = Math.max(0, Math.min(255, Number(g)));
                b = Math.max(0, Math.min(255, Number(b)));
                return (r << 16) + (g << 8) + b;
            },
        red:function(c:*):* {
                return Number(c) >> 16 & 0xFF;
            },
        green:function(c:*):* {
                return Number(c) >> 8 & 0xFF;
            },
        blue:function(c:*):* {
                return Number(c) & 0xFF;
            },
        tobase:function(num:*, base:*):* {
                return Number(num).toString(base);
            },
        frombase:function(num:*, base:*):* {
                return parseInt(String(num),base);
            },
        string:function(obj:*):* {
                return String(obj);
            },
        color:function(obj:*):* {
                return uint(obj) & 0xFFFFFF;
            },
        number:function(obj:*):* {
                return Number(obj);
            },
        list:function(obj:*):* {
                return obj as Array;
            },
        boolean:function(obj:*):* {
                return boolCast(obj);
            },
		sessiontime:getTimer().toString(),
		fulldate:getDayAsString(dateObj.day) + ", " + getMonthAsString(dateObj.month) + " " + String(dateObj.date) + ", " +  String(dateObj.fullYear),
		dayofmonth:dateObj.date,
		dayofweek:dateObj.day,
		monthofyear:dateObj.month,
		monthname:getMonthAsString(dateObj.month),
		dayname:getDayAsString(dateObj.day),
		year:dateObj.fullYear,
		username:getUserName()
		
	}
}

    private static function getUserName():String {
        try {
            return TaconiteFactory.getEnvironmentImplementation().username
        } catch(e:Error) {
        }
        return "UserName"
    }

public static function defaultTypeEnvironment():Object
{
	return {
		sin:"number",
		cos:"number",
		tan:"number",
		asin:"number",
		acos:"number",
		atan:"number",
		atan2:"number",
		abs:"number",
		random:"number",
		min:"number",
		max:"number",
		sqrt:"number",
		pow:"number",
		log:"number",
		exp:"number",
		pi:"number",
		e:"number",
		infinity:"number",
		nan:"number",
		isnan:"boolean",
        substr:"string",
        touppercase:"string",
        tolowercase:"string",
        length:"number",
        map:null,
        reduce:null,
        tofixed:"string",
        toexponential:"string",
        toprecision:"string",
        floor:"number",
        ceil:"number",
        round:"number",
        charcount:"number",
        empty:"boolean",
        contains:"boolean",
        rgb:"color",
        red:"number",
        green:"number",
        blue:"number",
        tobase:"string",
        frombase:"number",
        string:"string",
        color:"color",
        number:"number",
        list:"list",
        boolean:"boolean",
		sessiontime:"number",
		dayofmonth:"number",
		dayofweek:"number",
		monthofyear:"number",
		fulldate:"string",
		dayname:"string",
		monthname:"string",
		year:"number",
		username:"string"
	}
}

public static function defaultHelpEnvironment():Object
{
	return FunctionLibraryContent.getVariableFunctionDescriptions();
}

private static function envStack(env:*):* {
	if(env == null) {
		env = {}
	}
	return {
		parent: null,
		env: env
	}
}
		
private static function pushEnv(stack:*, env:*):* {
	return {
		parent: stack,
		env: env
	}
}

private static function lookupEnv(stack:*, name:*):* {
	for(var s:* = stack; s != null; s = s.parent) {
		if(s.env.hasOwnProperty(name)) {
			return s.env[name];
		}
	}
	throw new Error("unknown variable [" + name + "]");
}

private static function evalAst(ast:*, env:*):* {
    var i:*;
    var args:*;
    var object:*;

    if(ast.kind == "[number]") {
        return Number(ast.token.text);
    } else if(ast.kind == "[boolean]") {
        return ast.token.text != "false";
    } else if(ast.kind == "[string]") {
        return String(ast.token.text);
    } else if(ast.kind == "[identifier]") {
    	return lookupEnv(env, ast.token.text);
    } else if(ast.kind == "binary operator") {
        if(ast.rhs is Array) {
            var result:* = evalAst(ast.lhs, env);
            var comparison:* = ast.token[0].text.match(/[>=<]{1,2}/);
            var oldRhs:* = null;
            if(comparison) {
                oldRhs = result;
                result = true;
            }
            for(i=0; i<ast.token.length; i++) {
                if(comparison) {
                    var rhs:* = evalAst(ast.rhs[i], env);
                    result = result && binaryOp[ast.token[i].text](oldRhs, rhs);
                    oldRhs = rhs;
                } else {
                    result = binaryOp[ast.token[i].text](result, evalAst(ast.rhs[i], env));
                }
            }
            return result;
        }
        return binaryOp[ast.token.text](evalAst(ast.lhs, env), evalAst(ast.rhs, env));
    } else if(ast.kind == "unary operator") {
        return unaryOp[ast.token.text](evalAst(ast.rhs, env));
    } else if(ast.kind == "ternary operator") {
        if(evalAst(ast.condition, env)) {
            return evalAst(ast.trueExp, env);
        } else {
            return evalAst(ast.falseExp, env);
        }
    } else if(ast.kind == "function application") {
    	var func:* = evalAst(ast.func, env);

        args = [];
        for(i=0; i<ast.arguments.length; i++) {
            args[i] = evalAst(ast.arguments[i], env);
        }
        return func.apply(null, args);
    } else if(ast.kind == "array literal") {
        var arrayLit:* = [];
        for(i=0; i<ast.items.length; i++) {
            arrayLit[i] = evalAst(ast.items[i], env);
        }
        return arrayLit;
    } else if(ast.kind == "subscript access") {
        var collection:* = evalAst(ast.collection, env);
        var key:* = evalAst(ast.key, env);
        if(isNaN(key)) {
            throw new PositionalError(ast.key.token.position, "subscript index [" + key + "] is not a number");
        }
        return collection[key];
    } else if(ast.kind == "member access") {
       object = evalAst(ast.object, env);
       var member:* = ast.member.token.text;
       return object[member];
    } else if(ast.kind == "function literal") {
    	var params:* = [];
    	for(i=0; i<ast.parameters.length; i++) {
    	    params[i] = ast.parameters[i].text;
    	}
    	var body:* = ast.body;
    	var bodyEnv:* = env;
		return function():* {
			if(arguments.length < params.length) {
				throw new Error("wrong numer of arguments required " + params.length + " but found " + arguments.length);
			}
			var args:* = {};
			for(var i:*  =0; i<params.length; i++) {
				args[params[i]] = arguments[i];
			}
			return evalAst(body, pushEnv(bodyEnv, args));
		};
    } else if(ast.kind == "let scope") {
    	args = {};
    	for(i=0; i<ast.scope.length; i++) {
    		args[ast.scope[i].name.token.text] = evalAst(ast.scope[i].value, env);
    	}
    	return evalAst(ast.body, pushEnv(env, args));
    } else if(ast.kind == "object literal") {
    	object = {};
    	for(i=0; i<ast.values.length; i++) {
    		object[ast.values[i].name.token.text] = evalAst(ast.values[i].value, env);
    	}
    	return object;
    } else {
        throw new Error("Unknown ast kind " + ast.kind);
    }
}

public static function eval(ast:*, env:*):Object {
	return evalAst(ast, envStack(env));
}

private static function getTypeStack(ast:*, env:*):String {
    if(ast.kind == "[number]") {
        return "number"
    } else if(ast.kind == "[boolean]") {
        return "boolean"
    } else if(ast.kind == "[string]") {
        return "string"
    } else if(ast.kind == "[identifier]") {
    	return lookupEnv(env, ast.token.text);
    } else if(ast.kind == "binary operator") {

        if(ast.rhs is Array) {
            return binaryOpType[ast.token[0].text];
        }
        return binaryOpType[ast.token.text];
    } else if(ast.kind == "unary operator") {
        return unaryOpType[ast.token.text];
    } else if(ast.kind == "ternary operator") {
	    return getTypeStack(ast.trueExp, env);
    } else if(ast.kind == "function application") {
    	return getTypeStack(ast.func, env);
    } else if(ast.kind == "array literal") {
        return "list"
    } else if(ast.kind == "subscript access") {
        return null;
    } else if(ast.kind == "member access") {
        return null;
    } else if(ast.kind == "function literal") {
    	return getTypeStack(ast.body, pushEnv(env, args));
    } else if(ast.kind == "let scope") {
    	var args:* = {};
    	for(var i:*=0; i<ast.scope.length; i++) {
    		args[ast.scope[i].name.token.text] = getTypeStack(ast.scope[i].value, env);
    	}
    	return getTypeStack(ast.body, pushEnv(env, args));
    } else if(ast.kind == "object literal") {
    	return null;
    } else {
        throw new Error("Unknown ast kind " + ast.kind);
    }
}

private static function getVariablesStack(ast:*, env:*):Array {
    var i:*;
    var result:Array;
    if(ast.kind == "[number]") {
        return [];
    } else if(ast.kind == "[boolean]") {
        return [];
    } else if(ast.kind == "[string]") {
        return [];
    } else if(ast.kind == "[identifier]") {
	    try {
		    lookupEnv(env, ast.token.text);
	    } catch(e:Error) {
		    return [ast.token.text];
	    }
    	return [];
    } else if(ast.kind == "binary operator") {
        if(ast.rhs is Array) {
            result = getVariablesStack(ast.lhs, env);
            for(i=0; i<ast.token.length; i++) {
	            result = result.concat(getVariablesStack(ast.rhs[i], env));
            }
            return result;
        }
        return getVariablesStack(ast.lhs, env).concat(getVariablesStack(ast.rhs, env));
    } else if(ast.kind == "unary operator") {
        return getVariablesStack(ast.rhs, env);
    } else if(ast.kind == "ternary operator") {
        result = getVariablesStack(ast.condition, env);
	    result = result.concat(getVariablesStack(ast.trueExp, env));
	    result = result.concat(getVariablesStack(ast.falseExp, env));
        return result;
    } else if(ast.kind == "function application") {
        var args:Array = [];
        for(i=0; i<ast.arguments.length; i++) {
            args = args.concat(getVariablesStack(ast.arguments[i], env));
        }
        return args.concat(getVariablesStack(ast.func, env));
    } else if(ast.kind == "array literal") {
        var arrayLit:Array = [];
        for(i=0; i<ast.items.length; i++) {
            arrayLit = arrayLit.concat(getVariablesStack(ast.items[i], env));
        }
        return arrayLit;
    } else if(ast.kind == "subscript access") {
        var collection:Array = getVariablesStack(ast.collection, env);
        var key:* = getVariablesStack(ast.key, env);
	    return collection.concat(key);
    } else if(ast.kind == "member access") {
       return getVariablesStack(ast.object, env);
    } else if(ast.kind == "function literal") {
		var params:* = {};
		for(i=0; i<ast.parameters.length; i++) {
			params[ast.parameters[i].text] = null;
		}
	    return getVariablesStack(ast.body, pushEnv(env, params));
    } else if(ast.kind == "let scope") {
    	var letVars:* = [];
	    var letEnv:* = {};
    	for(i=0; i<ast.scope.length; i++) {
		    letVars = letVars.concat(getVariablesStack(ast.scope[i].value, env));
    		letEnv[ast.scope[i].name.token.text] = null;
    	}
    	return letVars.concat(getVariablesStack(ast.body, pushEnv(env, letEnv)));
    } else if(ast.kind == "object literal") {
    	var objVars:* = [];
    	for(i=0; i<ast.values.length; i++) {
		    objVars = objVars.concat(getVariablesStack(ast.values[i].value, env))
    	}
    	return objVars;
    } else {
        throw new Error("Unknown ast kind " + ast.kind);
    }
}

public static function printAst(ast:*):*{
    var result:*;
    var i:*;
    if(ast.kind == "[number]") {
        return "number: " + ast.token.text;
    } else if(ast.kind == "[boolean]") {
        return "boolean: " + ast.token.text;
    } else if(ast.kind == "[string]") {
        return "string: " + ast.token.text;
    } else if(ast.kind == "[identifier]") {
        return "identifier: " + ast.token.text;
    } else if(ast.kind == "binary operator") {
        if(ast.rhs is Array) {
            result = "binary operator chain " +
                "<ul>" +
                "<li>[0]" + "lhs: " + printAst(ast.lhs);
            for(i=0; i<ast.token.length; i++) {
                result += "<li>["+(i+1)+"]" + " operator: " + ast.token[i].text +
                    "<li>["+(i+1)+"]" + " rhs: " + printAst(ast.rhs[i]);
            }
            result += "</ul>";
            return result;
        }
        return "binary operator: " + ast.token.text +
            "<ul>" +
            "<li>" + "lhs: " + printAst(ast.lhs) +
            "<li>" + "rhs: " + printAst(ast.rhs) +
            "</ul>";
    } else if(ast.kind == "unary operator") {
        return "unary operator: " + ast.token.text +
            "<ul>" +
            "<li>" + "rhs: " + printAst(ast.rhs) +
            "</ul>";
    } else if(ast.kind == "ternary operator") {
        return "ternary operator " +
            "<ul>" +
            "<li>" + "condition: " + printAst(ast.condition) +
            "<li>" + "true expression: " + printAst(ast.trueExp) +
            "<li>" + "false expression: " + printAst(ast.falseExp) +
            "</ul>";
    } else if(ast.kind == "function application") {
        result = "function application: " + printAst(ast.func) + "<ul>";

        for(i=0; i<ast.arguments.length; i++) {
            result += "<li>[argument"+(i)+"]" + printAst(ast.arguments[i]);
        }
        result += "</ul>";
        return result;
    } else if(ast.kind == "array literal") {
        result = "array literal" + "<ul>";

        for(i=0; i<ast.items.length; i++) {
            result += "<li>[item"+(i)+"]" + printAst(ast.items[i]);
        }
        result += "</ul>";
        return result;
    } else if(ast.kind == "subscript access") {
    	return "subscript access " + "<ul>" +
    	"<li>" + "collection: " + printAst(ast.collection) +
    	"<li>" + "key: " + printAst(ast.key) +
    	"</ul>";
    } else if(ast.kind == "member access") {
    	return "member access " + "<ul>" +
    	"<li>" + "object: " + printAst(ast.object) +
    	"<li>" + "member: " + ast.member.token.text +
    	"</ul>";
    } else if(ast.kind == "function literal") {
    	result = "function literal" + "<ul>";

    	result += "parameters" + "<ul>";
    	for(i=0; i<ast.parameters.length; i++) {
    	    result += "<li>[item"+(i)+"]" + ast.parameters[i].text;
    	}
    	result += "</ul>";
    	result += "body: " + printAst(ast.body);
    	result += "</ul>";
    	return result;
    } else if(ast.kind == "let scope") {
    	result = "let scope" + "<ul>";

    	result += "scope" + "<ul>";
    	for(i=0; i<ast.scope.length; i++) {
    	    result += "<li>[item"+(i)+"]" + ast.scope[i].name.token.text + ": " + printAst(ast.scope[i].value);
    	}
    	result += "</ul>";
    	result += "body: " + printAst(ast.body);
    	result += "</ul>";
    	return result;
    } else if(ast.kind == "object literal") {
    	result = "object literal" + "<ul>";
    	for(i=0; i<ast.values.length; i++) {
    	    result += "<li>[item"+(i)+"]" + ast.values[i].name.token.text + ": " + printAst(ast.values[i].value);
    	}
    	result += "</ul>";
    	return result;
    } else {
        return "PRINTING_ERROR";
    }
}

private var _root:Object = null;

public function Ast(text:String) {
	_root = Parser.parse(Lexer.lex(text));
}

public function evaluate(env:*):Object {
	return evalAst(_root, envStack(env));
}

public function variables(env:*):Array {
	return getVariablesStack(_root, envStack(env));
}

public function type(env:*):String {
	return getTypeStack(_root, envStack(env));
}

//comments based on Wednesday, August 10, 2011
public static function get dateObj():Object{
	var _date:* = new Date();
	
	return {
			date:_date.date, //10
			time:_date.time,
			month:_date.month, //07
			day:_date.day, //3
			fullYear:_date.fullYear //2011
	}
}

public static function getMonthAsString(month:Number):String {
	var months:Array = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	return months[month];
}

public static function getDayAsString(day:Number):String {
	var days:Array = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
	return days[day];
}

}
}