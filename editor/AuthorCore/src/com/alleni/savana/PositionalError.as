package com.alleni.savana {
public class PositionalError extends Error {
	public var position:int;
	public function PositionalError(position:int, str:String) {
		super("Error at position " + position + " " + str);
		this.position = position;
	}
}
}
