package com.alleni.savana.symbols {
public class Terminal extends Symbol {
	public function Terminal(id:String) {
		super(id);
		nud = function(token:Object, tokens:Object):Object {
			return {
				value:{
					kind: id,
					token: token
				},
				tokens:tokens
			};
		}
    }
}
}
