
package com.alleni.savana.symbols {
import com.alleni.savana.PositionalError;

public class Symbol {

	private static var symbolTable:Object = {};

	private static function fillTable():void {

	}


	public static function lookup(token:Object):Symbol {
		var result:Symbol = null;
		if(token.kind == "number" || token.kind == "boolean" || token.kind == "string" || token.kind == "identifier") {
			result = symbolTable["[" + token.kind + "]"];
		} else {
			result = symbolTable[token.text];
		}
		if(result == null) {
			throw new PositionalError(token.position, "unknown symbol " + token.kind + " [" + token.text + "]")
		}
		return result;
	}

	/**
	 * Lookup name in the symbol table
	 */
	public var id:String;

	/**
	 * Left binding power
	 */
	public var lbp:int = 0;

	public function Symbol(id:String) {
		this.id = id;
		symbolTable[id] = this;
	}

	/**
	 * null denotation (literals, variables, and prefix operators)
	 * @param token token representing this symbol
	 * @param tokens unparsed token list
	 * @return AST branch for this symbol
	 */
	public var nud:* = function(token:Object, tokens:Object):Object {
		throw new PositionalError(token.position, token.kind + " [" + token.text + "] is not a literal, variable, or prefix operator");
	};
	public var led:* = function(lhs:*, token:*, tokens:*):Object {
		throw new PositionalError(token.position, token.kind + " [" + token.text + "] is not an infix or suffix operator");
	}
}
}
