package com.alleni.savana.symbols {
public class Operator extends Symbol {
	public function Operator(id:String, lbp:int, nud:Function, led:Function) {
		super(id)
		this.lbp = lbp;
        if(nud != null) {
		    this.nud = nud;
        }
        if(led != null) {
		    this.led = led;
        }
	}
}
}
