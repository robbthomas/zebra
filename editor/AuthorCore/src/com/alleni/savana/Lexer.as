package com.alleni.savana {
public class Lexer {

private static var sequenceRegex:Object = {
	8: /^[0-7]+$/,
	16: /^[0-9a-fA-F]+$/
};

private static function decodeSequence(exp:String, index:int, length:int, radix:int):String {
    if(index+length <= exp.length ) {
        var str:* = exp.substr(index, length);
        if(str.search(sequenceRegex[radix]) == 0) {
            var charCode:* = parseInt(str, radix);
            return String.fromCharCode(charCode);
        }
    }
    throw new PositionalError(index, "malformed character sequence");

}

public static function lex(exp:*):Object {
    var rslt:Object = {obj:null, next:null};
    var last:Object = rslt;
    var start:int = 0;
    var state:String = null;
    var state2:String = null;
    var stringTerminator:String = null;
    var currentString:String = null;
    for(var i:int=0; i<=exp.length; i++) {
        var c:String = i<exp.length?exp.charAt(i):"";
        switch(state) {
            case "number":
                if(c.search(/^\d$/) == 0) {
                    if(state2 == "exponent-sign") {
                        state2 = "exponent";
                    }
                    continue;
                } else if(c.search(/^\.$/) == 0) {
                    if(state2 == "decimal") {
                        throw new PositionalError(start, "second decimal found in number literal");
                    } else if(state2 == "exponent" || state2 == "exponent-sign") {
                        throw new PositionalError(start, "decimal not allowed in exponent");
                    } else {
                        state2 = "decimal";
                        continue;
                    }
                } else if(c.search(/^[eE]$/) == 0) {
                    if(state2 == "exponent" || state2 == "exponent-sign") {
                        throw new PositionalError(start, "second exponent in number literal");
                    } else {
                        state2 = "exponent-sign";
                        continue;
                    }
                } else if(c.search(/^[+-]$/) == 0) {
                    if(state2 == "exponent-sign") {
                        // fall through to exponent-sign error since
                        // we got two signs and no numbers
                        continue;
                    }
                    // fall through and let this be an operator
                }
                if(state2 == "exponent-sign") {
                    throw new PositionalError(i, "exponent value required but [" + c + "] found");
                }
                if(state2 == "exponent" && exp.charAt(i-1).search(/^\d$/) < 0) {
                    // we've gotten a sign but haven't seen a number yet
                    throw new PositionalError(i, "exponent value required but [" + c + "] found");
                }

				var num:* = exp.substring(start,i);
				if(num == ".") {
					last.obj = {kind:"operator", text:".", position:start, len:1}
				} else {
	                last.obj = {kind:"number", text:exp.substring(start,i), position:start, len:i-start}
                }
                last.next = {obj:null, next:null};
                last = last.next;
                start = i;
                state = null;
                state2 = null;
                break;
            case "identifier":
                if(c.search(/^\w$/) == 0) {
                    continue;
                } else {
                    var word:* = exp.substring(start,i).toLowerCase();
                    switch(word) {
                        case "and": last.obj = {kind:"operator", text:"&", position:start, len:3}; break;
                        case "or": last.obj = {kind:"operator", text:"|", position:start, len:2}; break;
                        case "not": last.obj = {kind:"operator", text:"~", position:start, len:3}; break;
                        case "true": last.obj = {kind:"boolean", text:"true", position:start, len:4}; break;
                        case "false": last.obj = {kind:"boolean", text:"false", position:start, len:5}; break;
                        case "function": last.obj = {kind:"operator", text:"function", position:start, len:8}; break;
                        case "let": last.obj = {kind:"operator", text:"let", position:start, len:3}; break;
                        case "mod": last.obj = {kind:"operator", text:"%", position:start, len:3}; break;
                        default: last.obj = {kind:"identifier", text:word, position:start, len:i-start}; break;
                    }
                    last.next = {obj:null, next:null};
                    last = last.next;
                    start = i;
                    state = null;
                }
                break;
            case "string":
                var escapedChar:*;
                if(state2 == "escape") {
                    if(c.search(/^[\'\"\\]$/) == 0) {
                        currentString += c;
                    } else if(c == "b") {
                        // backslash
                        currentString += '\b';
                    } else if(c == "f") {
                        // form feed
                        currentString += '\f';
                    } else if(c == "n") {
                        // new line
                        currentString += '\n';
                    } else if(c == "r") {
                        // carriage return
                        currentString += '\r';
                    } else if(c == "t") {
                        // horizontal tab
                        currentString += '\t';
                    } else if(c.search(/^\d$/) == 0) {
                        // octal sequence
                        escapedChar = decodeSequence(exp, i, 3, 8);
                        currentString += escapedChar;
                        i += 2;
                    } else if(c == "x") {
                        // hexadecimal sequence
                        escapedChar = decodeSequence(exp, i+1, 2, 16);
                        currentString += escapedChar;
                        i += 2;
                    } else if(c == "u") {
                        // unicode sequence
                        escapedChar = decodeSequence(exp, i+1, 4, 16);
                        currentString += escapedChar;
                        i += 4;
                    }
                    state2 = null;
                } else if(c.search(/^\\$/) == 0) {
                    state2 = "escape";
                } else if(c == stringTerminator) {
                    last.obj = {kind:"string", text:currentString, position:start, len:i-start};
                    last.next = {obj:null, next:null};
                    last = last.next;
                    start = i;
                    state = null;
                    stringTerminator = null;
                    currentString = null;
                } else {
                    currentString += c;
                }
                continue;
            default:
                break;
        }
        if(i == exp.length) {
            break;
        }
        // start a new token
        if(c.search(/^\s$/) == 0) {
            start = i+1; // ignore
        } else if(c.search(/^\d$/) == 0) {
            state = "number";
        } else if(c.search(/^[\'\"]$/) == 0) {
            state = "string";
            currentString = "";
            stringTerminator = c;
        } else if(c.search(/^\.$/) == 0) {
            state = "number";
            state2 = "decimal";
        } else if(c.search(/^[a-zA-Z]$/) == 0) {
            state = "identifier"
        } else if(c.search(/^[\<\>\=\*\/\%\+\-\&\|\~\?\:\~\^\(\)\,\[\]\{\}]$/) == 0) {
            var c2:* = (i+1)<exp.length?exp.charAt(i+1):"";
            if((c+c2).search(/^(\<\=|\>\=|\<\>|\*\*)$/) == 0) {
                last.obj = {kind:"operator", text:c+c2, position:start, len:2};
                last.next = {obj:null, next:null};
                last = last.next;
                start = i+2;
                i++;
            } else {
                last.obj = {kind:"operator", text:c, position:start, len:1};
                last.next = {obj:null, next:null};
                last = last.next;
                start = i+1;
            }
        } else if( c.search(/^[\u2264\u2265\u2260\u221e\u03c0]$/) == 0) {
	        if(c == "\u2264" /*"≤"*/) {
                last.obj = {kind:"operator", text:"<=", position:start, len:1}
	        } else if(c == "\u2265" /*"≥"*/) {
		        last.obj = {kind:"operator", text:">=", position:start, len:1}
	        } else if(c == "\u2260" /*"≠"*/) {
		        last.obj = {kind:"operator", text:"<>", position:start, len:1}
	        } else if(c == "\u221e" /*"∞"*/) {
		        last.obj = {kind:"identifier", text:"infinity", position:start, len:1}
	        } else if(c == "\u03c0" /*"π"*/) {
		        last.obj = {kind:"identifier", text:"pi", position:start, len:1}
	        }
			last.next = {obj:null, next:null};
			last = last.next;
			start = i+1;
        } else if(c == "#") {
            last.obj = {kind:"identifier", text:"#", position:start, len:1};
            last.next = {obj:null, next:null};
            last = last.next;
            start = i+1;
        } else {
            throw new PositionalError(start, "unknown symbol [" + c + "]");
        }
    }
    if(state == "string") {
        throw new PositionalError(start, "unterminated string");
    }
    last.obj = {kind:"eof", text:"[EOF]", position:exp.length, len:0};
    return rslt;
}


public static function printTokens(tokens:*):String {
    var result:* = "<ol>\n";
    for(var cur:Object=tokens; cur != null; cur = cur.next) {
        result += "  <li>{kind:"+cur.obj.kind+", text:"+cur.obj.text+", position:"+cur.obj.position+", len:"+cur.obj.len+"}\n";
    }
    return result;
}

}
}
