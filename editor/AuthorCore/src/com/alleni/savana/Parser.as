package com.alleni.savana {
import com.alleni.savana.symbols.Operator;
import com.alleni.savana.symbols.Symbol;
import com.alleni.savana.symbols.Terminal;


public class Parser {

private static var unary:* = function (token:*, tokens:*):* {
    var right:* = parseExpression(Symbol(this).lbp, tokens);
    return {
        value:{
            kind: "unary operator",
            token: token,
            rhs: right.value
        },
        tokens:right.tokens
    };
};

private static var binary:* = function (lhs:*, token:*, tokens:*):* {
    var right:* = parseExpression(Symbol(this).lbp, tokens);
    if(lhs.kind == "binary operator" && lhs.bp == Symbol(this).lbp) {
        lhs.token = [].concat(lhs.token, token);
        lhs.rhs = [].concat(lhs.rhs, right.value);
        return {value:lhs, tokens:right.tokens};
    } else {
        return {
            value:{
                kind: "binary operator",
                lhs: lhs,
                token: token,
                rhs: right.value,
                bp:Symbol(this).lbp
            },
            tokens:right.tokens
        };
    }
};

// right associative version
private static var binaryR:* = function (lhs:*, token:*, tokens:*):* {
    var right:* = parseExpression(Symbol(this).lbp-1, tokens);
    if(right.value.kind == "binary operator" && right.value.bp == Symbol(this).lbp) {
        right.value.lhs = [].concat(lhs, right.value.lhs);
        right.value.token = [].concat(token, right.value.token);
        return right;
    } else {
        return {
            value:{
                kind: "binary operator",
                lhs: lhs,
                token: token,
                rhs: right.value,
                bp:Symbol(this).lbp
            },
            tokens:right.tokens
        };
    }
};

private static var ternary:* = function (lhs:*, token:*, tokens:*):* {
    var middle:* = parseExpression(Symbol(this).lbp-1, tokens);
    tokens = middle.tokens;
    var token2:* = tokens.obj;
    if(tokens.obj.kind != "operator" || token2.text != ":") {
        throw new PositionalError(token2.position, "found [" + token2.kind + "] but required [:] to match ternary operator at position " + token.position);
    }
    tokens = tokens.next;
    var right:* = parseExpression(Symbol(this).lbp-1, tokens);
    return {
        value:{
            kind: "ternary operator",
            condition: lhs,
            trueExp: middle.value,
            falseExp: right.value,
            token: token
        },
        tokens:right.tokens
    }
};

private static var group:* = function (token:*, tokens:*):* {
    var middle:* = parseExpression(0, tokens);
    tokens = middle.tokens;
    var token2:* = tokens.obj;
    if(tokens.obj.kind != "operator" || token2.text != ")") {
        throw new PositionalError(token2.position, "found [" + token2.kind + "] but required [)] to match [(] at position " + token.position);
    }
    middle.tokens = tokens.next;
    return middle;
};

private static var application:* = function (lhs:*, token:*, tokens:*):* {
    if(lhs.kind != "[identifier]" && lhs.kind != "function literal" && lhs.kind != "member access" && lhs.kind != "subscript access") {
        throw new PositionalError(token.position, "found [" + lhs.kind + "] but required function name");
    }
    var args:* = [];
    var arg:*;
    if(tokens.obj.kind != "operator" || tokens.obj.text != ")") {
        arg = parseExpression(0, tokens);
        tokens = arg.tokens;
        args.push(arg.value);
    }
    while(tokens.obj.kind == "operator" && tokens.obj.text == ",") {
        tokens = tokens.next;
        arg = parseExpression(0, tokens);
        tokens = arg.tokens;
        args.push(arg.value);
    }
    if(tokens.obj.kind != "operator" || tokens.obj.text != ")") {
        throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required [)] to match [(] at position " + token.position);
    }
    tokens = tokens.next;

    return {
        value:{
            kind: "function application",
            func: lhs,
            arguments: args,
            token:token
        },
        tokens:tokens
    }
};


private static var arrayLiteral:* = function (token:*, tokens:*):* {
    var items:* = [];
	var item:*;
	if(tokens.obj.kind != "operator" || tokens.obj.text != "]") {
	    item = parseExpression(0, tokens);
	    tokens = item.tokens;
	    items.push(item.value);
	}
	while(tokens.obj.kind == "operator" && tokens.obj.text == ",") {
	    tokens = tokens.next;
	    item = parseExpression(0, tokens);
	    tokens = item.tokens;
	    items.push(item.value);
	}
	if(tokens.obj.kind != "operator" || tokens.obj.text != "]") {
	    throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required []] to match [[] at position " + token.position);
	}
	tokens = tokens.next;
	
	return {
	    value:{
	        kind: "array literal",
	        items: items,
	        token:token
	    },
	    tokens:tokens
	}
};

private static var subscriptAccess:* = function (lhs:*, token:*, tokens:*):* {
	var key:* = parseExpression(0, tokens);
    tokens = key.tokens;

	if(tokens.obj.kind != "operator" || tokens.obj.text != "]") {
	    throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required []] to match [[] at position " + token.position);
	}
	tokens = tokens.next;
	
	return {
	    value:{
	        kind: "subscript access",
	        collection: lhs,
	        key: key.value,
	        token:token
	    },
	    tokens:tokens
	}
};

private static var memberAccess:* = function (lhs:*, token:*, tokens:*):* {
	if(tokens.obj.kind != "identifier") {
	    throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required identifier at position " + token.position);
	}
	var key:* = Symbol.lookup(tokens.obj).nud(tokens.obj, tokens.next);
	tokens = key.tokens;
	
	return {
	    value:{
	        kind: "member access",
	        object: lhs,
	        member: key.value,
	        token:token
	    },
	    tokens:tokens
	}
};

private static var functionLiteral:* = function (token:*, tokens:*):* {
	
	if(tokens.obj.kind != "operator" || tokens.obj.text != "(") {
	    throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required [(] for function definition at " + token.position);
	}
	var parenPos:* = tokens.obj.position;
	tokens = tokens.next;
	
    var parameters:* = [];
	if(tokens.obj.kind != "operator" || tokens.obj.text != ")") {
		if(tokens.obj.kind != "identifier") {
			throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required [)] to match [(] at position " + parenPos);
		}
	    parameters.push(tokens.obj);
	    tokens = tokens.next;
	}
	while(tokens.obj.kind == "operator" && tokens.obj.text == ",") {
		tokens = tokens.next;
		if(tokens.obj.kind != "identifier") {
			throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required [)] to match [(] at position " + parenPos);
		}
		parameters.push(tokens.obj);
		tokens = tokens.next;
	}

	if(tokens.obj.kind != "operator" || tokens.obj.text != ")") {
	    throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required [)] to match [(] at position " + parenPos);
	}
	tokens = tokens.next;
	
	var body:* = parseExpression(0, tokens);
	tokens = body.tokens;
	
	return {
	    value:{
	        kind: "function literal",
	        parameters: parameters,
	        body: body.value,
	        token:token
	    },
	    tokens:tokens
	}
};

private static var letScope:* = function (token:*, tokens:*):* {
	
	if(tokens.obj.kind != "operator" || tokens.obj.text != "(") {
	    throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required [(] for function definition at " + token.position);
	}
	var parenPos:* = tokens.obj.position;
	tokens = tokens.next;
	
    var scope:* = [];
    var entry:*;
	if(tokens.obj.text != ")") {
		entry = parseNameValue(tokens);
	    scope.push(entry.value);
	    tokens = entry.tokens;
	}
	while(tokens.obj.kind == "operator" && tokens.obj.text == ",") {
		tokens = tokens.next;
		entry = parseNameValue(tokens);
		scope.push(entry.value);
		tokens = entry.tokens;
	}

	if(tokens.obj.kind != "operator" || tokens.obj.text != ")") {
	    throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required [)] to match [(] at position " + parenPos);
	}
	tokens = tokens.next;
	
	var body:* = parseExpression(0, tokens);
	tokens = body.tokens;
	
	return {
	    value:{
	        kind: "let scope",
	        scope: scope,
	        body: body.value,
	        token:token
	    },
	    tokens:tokens
	}
};

private static var objectLiteral:* = function (token:*, tokens:*):* {
    var values:* = [];
    var entry:*;
	if(tokens.obj.text != "}") {
		entry = parseNameValue(tokens);
	    values.push(entry.value);
	    tokens = entry.tokens;
	}
	while(tokens.obj.kind == "operator" && tokens.obj.text == ",") {
		tokens = tokens.next;
		entry = parseNameValue(tokens);
		values.push(entry.value);
		tokens = entry.tokens;
	}

	if(tokens.obj.kind != "operator" || tokens.obj.text != "}") {
	    throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but required [}] to match [{] at position " + token.obj.position);
	}
	tokens = tokens.next;
	
	return {
	    value:{
	        kind: "object literal",
	        values: values,
	        token:token
	    },
	    tokens:tokens
	}
};

private static function parseNameValue(tokens:*):* {
	if(tokens.obj.kind != "identifier") {
		throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but expected to find identifier");
	}
	var name:* = Symbol.lookup(tokens.obj).nud(tokens.obj, tokens.next);
	tokens = name.tokens;
	
	if(tokens.obj.kind != "operator" || tokens.obj.text != ":") {
		throw new PositionalError(tokens.obj.position, "found [" + tokens.obj.kind + "] but expected to find [:]");
	}
	tokens = tokens.next;
	var value:* = parseExpression(0, tokens);

	return {
	    value:{
	        kind: "name:value",
	        name: name.value,
	        value: value.value
	    },
	    tokens:value.tokens
	}
}

{ // static block

	// deliminators
	new Symbol(")");
	new Symbol("]");
	new Symbol("}");
	new Symbol(":");
	new Symbol(",");
	new Symbol("[EOF]");

	new Terminal("[number]");
	new Terminal("[boolean]");
	new Terminal("[string]");
	new Terminal("[identifier]");

	// operator definitions

	new Operator("let", 0, letScope, null);
	new Operator("function", 0, functionLiteral, null);

	// grouping scope array/member access - 16
	new Operator("(", 16, group, application);
	new Operator("[", 16, arrayLiteral, subscriptAccess);
	new Operator(".", 16, null, memberAccess);
	new Operator("{", 0, objectLiteral, null);

	// most unary - 15
	new Operator("~", 15, unary, null);

	// exponents and roots - 14
	new Operator("**", 14, null, binary);


	// multiplication division modulo - 13
	new Operator("*", 13, null, binary);
	new Operator("/", 13, null, binary);
	new Operator("%", 13, null, binary);

	// addition and subtraction - 12
	new Operator("+", 12, unary, binary);
	new Operator("-", 12, unary, binary);

	// bitwise shift - 11

	new Operator("^", 14, null, binary); // string concatenation

	// comparison less/greater - 10
	new Operator("<", 10, null, binary);
	new Operator(">", 10, null, binary);
	new Operator("<=", 10, null, binary);
	new Operator(">=", 10, null, binary);

	// comparison equal and not equal- 9
	new Operator("=", 9, null, binary);
	new Operator("<>", 9, null, binary);

	// bitwise and - 8

	// bitwise exclusive or - 7

	// bitwise inclusive or - 6

	// logical and - 5
	new Operator("&", 5, null, binary);

	// logical or - 4
	new Operator("|", 4, null, binary);

	// conditional - 3
	new Operator("?", 3, null, ternary);

	// assignment - 2

	// comma - 1
}

private static function parseExpression(rbp:*, tokens:*):* {
    // observe first token
    var t:* = tokens.obj;
    var s:* = Symbol.lookup(t);
    // consume it
    tokens = tokens.next;

    // execute it with no left operands
    var nud:* = s.nud(t, tokens);
    var left:* = nud.value;
    tokens = nud.tokens;

    // observe second token
    t = tokens.obj;
    s = Symbol.lookup(t);
    while (rbp < s.lbp) {
        // next token binds to this one from the left more strongly than we do from the right
        // so we will allow that token to consume it.
        tokens = tokens.next;

        // execute it with the left operand we have and the remaining tokens
        var led:* = s.led(left, t, tokens);
        // respect any consumption the led call did
        left = led.value;
        tokens = led.tokens;
        
        // observe next token
        t = tokens.obj;
        s = Symbol.lookup(t);
    }
    return {value:left, tokens:tokens};
}
public static function parse(tokens:Object):Object {
	return parseExpression(0, tokens).value;
}
}
}