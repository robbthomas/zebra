package com.alleni.author.view
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.World;
import com.alleni.author.view.feedback.ObjectFeedback;
import com.alleni.author.view.ui.WiringView;
	import com.alleni.taconite.controller.ITaconiteController;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.ModelRoot;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	import com.alleni.taconite.view.ViewInfo;

import flash.display.DisplayObject;

import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	
	
	public class WorldContainer extends ViewContext
	{
		public var viewsUnderMouse:Vector.<ObjectView> = new Vector.<ObjectView>();
		public var oldViewsUnderMouse:Vector.<ObjectView> = new Vector.<ObjectView>();
		
		private var _world:World;
		private var _worldView:WorldView;
        private var _pageEditingView:Sprite;
		private var _feedbackUIView:Sprite;
		private var _feedbackControlsView:Sprite;
		private var _feedbackHandlesView:Sprite;
		private var _feedbackGroupHandlesView:Sprite;
		private var _feedbackAnnotationView:Sprite;
		private var _feedbackWiringView:WiringView;
		private var _editingView:Sprite;
		
		private var _stageCurtain:Shape;
		private var _shadowHolder:Shape;
		
		private var _role:uint;
		private var _centerOffset:Point = new Point(0,0);
		private var _dummy:Boolean=false;

		
		public function WorldContainer(controller:ITaconiteController, worldRoot:ModelRoot, wiringRoot:ModelRoot, role:uint, dummy:Boolean=false)
		{
			super(new ViewInfo(), controller);
			_role = role;
			_dummy = dummy;

			if(_dummy)
				return;
			
			_worldView = new WorldView(this, worldRoot, role);
			_feedbackHandlesView = new Sprite();
			_feedbackControlsView = new Sprite();
			
			// Instantiate the views (note that the order is important here)
			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
                _pageEditingView = new Sprite();
                _feedbackUIView = new Sprite();
				_feedbackGroupHandlesView = new Sprite();
				_feedbackAnnotationView = new Sprite();
				_feedbackWiringView = new WiringView(this, wiringRoot, role);
				_editingView = new Sprite();
				
				_stageCurtain = new Shape();
				_shadowHolder = new Shape();
				_shadowHolder.filters = [new DropShadowFilter(2, 80, 0, 0.8, 12, 12, 0.7, 3)];
				
				_world = worldRoot.value as World;
				
				addChild(_shadowHolder);
				addChild(_worldView);
                addChild(_pageEditingView);
				addChild(_feedbackHandlesView);
				addChild(_feedbackGroupHandlesView);
				addChild(_feedbackAnnotationView);
				addChild(_feedbackControlsView);
                addChild(_feedbackUIView);
				addChild(_feedbackWiringView);
				addChild(_editingView);
				addChild(_stageCurtain);
			} else { // completely separate condition in case this needs different layers
				_world = worldRoot.value as World;
				
				addChild(_worldView);
				addChild(_feedbackHandlesView); // temporarily show handles in the Player
				addChild(_feedbackControlsView);
			}
		}

		public function initialize():void
		{
			if (_dummy)
				return;
			// Initialize the views
			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
				_feedbackWiringView.initialize();
				renderShadow();
			}

			_worldView.initialize();
		}
		
		private function renderShadow():void
		{
//			_shadowHolder.graphics.clear();
//			_shadowHolder.graphics.beginFill(0, 1);
//			_shadowHolder.graphics.drawRect(0, 0, _worldView.world.width, _worldView.world.height - 1);
//			_shadowHolder.graphics.endFill();
		}
		
		public function get centerOffset():Point
		{
			return _centerOffset;
		}

		public function set centerOffset(value:Point):void
		{
			_centerOffset = value;
			if(_dummy)
				return;
			scheduleReposition();
		}
		
		/*public function scrollToCenterOffset(value:Point):void
		{
			removeEventListener(Event.ENTER_FRAME, reposition);
			_centerOffset = value;
			var newPosition:Point = pointForReposition;
			Tweener.addTween(this, {x:newPosition.x, y:newPosition.y, time:0.3, transition:"easeInOutCubic"});
		}*/

		private function scheduleReposition():void
		{
			addEventListener(Event.ENTER_FRAME, reposition);
		}
		
		private function get pointForReposition():Point
		{
			return new Point(Math.round((parent.width - _world.width * this.scaleX)/2 + _centerOffset.x),
				Math.round((parent.height - _world.height * this.scaleY)/2 + _centerOffset.y));
		}
		
		public function reposition(event:Event = null):void
		{
			if (parent) {
				var newPosition:Point = pointForReposition;
				this.x = newPosition.x;
				this.y = newPosition.y;
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.WORLD_REPOSITIONED, {width:parent.width,  height:parent.height, x:newPosition.x,  y:newPosition.y, scaleX:this.scaleX, scaleY:this.scaleY}));
				
				if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor)
					renderShadow();

                stage.invalidate();
			}
			removeEventListener(Event.ENTER_FRAME, reposition);
		}
		
		public function get worldView():WorldView
		{
			return _worldView;
		}
		
        public function get feedbackUIView():Sprite
        {
            return _feedbackUIView;
        }

        public function get pageEditingView():Sprite
        {
            return _pageEditingView;
        }

		public function get feedbackControlsView():Sprite
		{
			return _feedbackControlsView;
		}
		
		public function get feedbackGroupHandlesView():Sprite
		{
			return _feedbackGroupHandlesView;
		}

		public function get feedbackHandlesView():Sprite
		{
			return _feedbackHandlesView;
		}
		
		public function get feedbackAnnotationView():Sprite
		{
			return _feedbackAnnotationView;
		}
		
		public function get feedbackWiringView():WiringView
		{
			return _feedbackWiringView;
		}

		public function get editingView():Sprite
		{
			return _editingView;
		}
		
		public function get stageCurtain():Shape
		{
			return _stageCurtain;
		}
		
		public function warnViewsToDelete():void
		{
			if (_worldView)
				_worldView.warnViewToDelete();
			if (_feedbackWiringView)
				_feedbackWiringView.warnViewToDelete();
		}
		
		public override function addEventListener(type:String, listener:Function, useCapture:Boolean=false, priority:int=0, useWeakReference:Boolean=false) : void
		{
			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
				Utilities.assert(EditorUI.instance != null);
				Utilities.assert(EditorUI.instance.leftBox != null);
				
				if (_role == ViewRoles.PRIMARY){
					EditorUI.instance.leftBox.addEventListener(type, listener, useCapture, priority, useWeakReference);
				} else {
					EditorUI.instance.rightBox.addEventListener(type, listener, useCapture, priority, useWeakReference);
				}
			} else super.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}

		public override function removeEventListener(type:String, listener:Function, useCapture:Boolean=false) : void
		{
			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
				if (_role == ViewRoles.PRIMARY){
					EditorUI.instance.leftBox.removeEventListener(type, listener, useCapture);
				} else {
					EditorUI.instance.rightBox.removeEventListener(type, listener, useCapture);
				}
			} else super.removeEventListener(type, listener, useCapture);
		}

		public function toggleRun(run:Boolean):void
		{
			if(_dummy)
				return;
			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
				if (run) {
                    _feedbackUIView.visible = false;
                    _feedbackGroupHandlesView.visible = false;
					_feedbackControlsView.visible = true;
					// currently handling feedback handles view specifically in ObjectFeedback
					//_feedbackHandlesView.visible = false;
					_feedbackAnnotationView.visible = false;
					_feedbackWiringView.visible = false;
				} else {
					if (EditorUI.instance.projectViewMode == EditorUI.PROJECT_VIEW_SPLIT) {
						_feedbackUIView.visible = false;
						_feedbackWiringView.visible = false;
					} else {
						_feedbackUIView.visible = true;
						_feedbackWiringView.visible = true;
					}
                    _feedbackGroupHandlesView.visible = true;
					_feedbackControlsView.visible = true;
					// currently handling feedback handles view specifically in ObjectFeedback
					//_feedbackHandlesView.visible = true;
					_feedbackAnnotationView.visible = true;
				}
			}
		}

		public function get viewUnderMouse():ObjectView
		{
			if (viewsUnderMouse.length > 0)
				return viewsUnderMouse[0];
			else
				return null;
		}
		
		public function get modelUnderMouse():TaconiteModel
		{
			if (viewsUnderMouse.length > 0)
				return viewsUnderMouse[0].model;
			else
				return null;
		}

		public function get role():uint
		{
			return _role;
		}
	}
}
