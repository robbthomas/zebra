package com.alleni.author.view
{
	import flash.display.Sprite;
	
	public class DashedRectView extends Sprite
	{
		private var _leftSide:DashedLineView = new DashedLineView();
		private var _bottomSide:DashedLineView = new DashedLineView();
		private var _rightSide:DashedLineView = new DashedLineView();
		private var _topSide:DashedLineView = new DashedLineView();
		
		public function DashedRectView()
		{
			super();
			addChild(_leftSide);	
			addChild(_bottomSide);	_bottomSide.rotation = 270;
			addChild(_rightSide);	_rightSide.rotation = 180;
			addChild(_topSide);		_topSide.rotation = 90
		}
		
		public function draw(ww:Number, hh:Number):void
		{
			_leftSide.x = 0;	_leftSide.y = 0;	_leftSide.draw(hh);
			_bottomSide.x = 0;	_bottomSide.y = hh;	_bottomSide.draw(ww);
			_rightSide.x = ww;	_rightSide.y = hh;	_rightSide.draw(hh);
			_topSide.x = ww;	_topSide.y = 0;		_topSide.draw(ww);
		}
	}
}