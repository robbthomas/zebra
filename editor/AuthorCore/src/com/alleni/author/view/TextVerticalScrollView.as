package com.alleni.author.view
{
	import com.alleni.author.model.ScrollModel;
	import com.alleni.taconite.dev.GraphicUtils;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.EventPhase;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class TextVerticalScrollView extends VerticalScrollView
	{
		private const BUTTON_ALPHA:Number = .2;
		private const LINE_ALPHA:Number = .2;
		public function TextVerticalScrollView(model:ScrollModel, scrollControllerWidth:Number, scrollControllerHeight:Number, trackBackgroundColor:uint=0, buttonBackgroundColor:uint=0, thumbAlpha:Number=1)
		{
			super(model, scrollControllerWidth, scrollControllerHeight, trackBackgroundColor, buttonBackgroundColor, thumbAlpha);
			_thumbColor = 0xF5F5F5;
			_thumbLineColor = 0xA5A5A5;
		}
		
		override protected function init():void //done
		{	
			_documentLength = _model.maxScrollPosition - _model.minScrollPosition;
			_buttonAreaWidth = 10;
			_buttonAreaHeight = 10;
			_scrollControllerWidth = 18; 
			_thumbWidth = 14
			_lineScrollSize = (_documentLength - _model.viewPortLength) / 20;
			nudgeAmount = 15;
			
		}
		
		
		override protected function makeButtons():void{}
		
	
		
	}

}