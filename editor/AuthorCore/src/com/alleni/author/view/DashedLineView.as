package com.alleni.author.view
{
	import flash.display.BitmapData;
	import flash.display.Shape;
	
	public class DashedLineView extends Shape
	{
		private static const LINE_THICK:Number = 1;
		private static const BLACK_LEN:Number = 3;
		private static const WHITE_LEN:Number = 3;
		private static const BLACK_COLOR:uint = 0x00000000;
		private static const WHITE_COLOR:uint = 0x00ffffff;
		
		private static var _stripe:BitmapData = null;
		
		
		public function DashedLineView()
		{
			super();
			
			if (!_stripe)
				_stripe = buildStripe();  // there is currently only one flavor of dashed line, so all share the stripe data
		}
		
		public function draw(length:Number):void
		{
			// be careful to have lineStyle null here
			graphics.clear();
			graphics.beginBitmapFill(_stripe, null, true);  // matrix=null, repeat=true
			graphics.drawRect(0,0,LINE_THICK,length);
			graphics.endFill();
		}
		
		private function buildStripe():BitmapData
		{
			var temp:Shape = new Shape();
			
			var bitmapData:BitmapData = new BitmapData(LINE_THICK, BLACK_LEN+WHITE_LEN, false, WHITE_COLOR); // transparent=false
			
			temp.graphics.lineStyle(LINE_THICK, BLACK_COLOR);
			temp.graphics.moveTo(0,0);
			temp.graphics.lineTo(0,BLACK_LEN);
			
			bitmapData.draw(temp);
			return bitmapData;
		}
	}
}
