package com.alleni.author.view	
{
	import com.alleni.author.event.ScrollUpdateEvent;
	import com.alleni.author.model.ScrollModel;
	
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventPhase;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	/*
	This class assumes that the beginning of the scrollable document is the model's minPosition and the end of the scrollabel document
	is the model's maxPosition.
	*/
	public class ScrollView extends Sprite
	{
		protected var _model:ScrollModel;
		protected var _documentLength:Number = 0;
		protected var _scrollControllerWidth:Number;
		protected var _scrollControllerHeight:Number;
		protected var _lineScrollSize:Number = 0;
		
		protected var _hMargin:Number = 10;
		protected var _vMargin:Number = 10;
		
		
		protected var _enabled:Boolean = true;
		private var _nudgeAmount:Number = 15;
		protected var _pageSize:Number = 0;
		protected var _thumbAlpha:Number = 1;
		protected var _thumbColor:uint = 0xA5A5A5;
		protected var _thumbOverColor:uint =  0x91ADC9;
		protected var _thumbLineColor:uint = 1;
		protected var _trackAlpha:Number = 1;
		protected var _trackForegroundColor:uint = 0xAFAFAF;
		protected var _trackBackgroundColor:uint = 0x000000;
		protected var _buttonBackgroundColor:uint = 0xA5A5A5;
		
		protected var _dotSize:Number = 1;
		protected var _upState:Sprite;
		protected var _overState:Sprite
		
		
		//props
		
		protected var _track:Sprite;
		
		protected var _scrollPosition:Number =  0;
		
		protected var _scrollPositionAbsolute:Number = 0;
		protected var _thumbStartPos:Point;
		protected var _thumbEndPos:Point;
		protected var _thumbWidth:Number = 0;
		protected var _thumbHeight:Number = 0;
		protected var _buttonAreaWidth:Number;
		protected var _buttonAreaHeight:Number;
		protected var _scrollBar:Sprite;
		protected var _docToViewPortRatio:Number = 1;
		protected var _dragging:Boolean = false;  // either buttons or thumb
		protected var _draggingThumb:Boolean = false;  // only thumb
		protected var _trackExtents:Rectangle;
		protected var _thumb:Sprite;
		
		//buttons
		protected var _lessButton:ScrollButton;  // left button / up button
		protected var _moreButton:ScrollButton;  // right button / down button
		protected var _buttonHolder:Sprite;
	

		public function ScrollView(model:ScrollModel, scrollControllerWidth:Number, scrollControllerHeight: Number)
		{
			_model = model;
			_model.addListener(this);
			_scrollControllerWidth = scrollControllerWidth;
			_scrollControllerHeight = scrollControllerHeight;
		}
		
		
	
		

		public function removeSpriteChildren(sprite:Sprite):void{}
		
		protected function init():void{}
		protected function makeButtons():void{}
		protected function makeScrollBar():void{}
		protected function makeTrack():void{}
		protected function makeTrackGraphic():void{}
		protected function makeThumb():void{}
		protected function addThumbGraphic(_thumbSprite:Sprite):void{}
		
		public function get scrollControllerHeight():Number
		{
			return _scrollControllerHeight;
		}
		
		public function set scrollControllerHeight(value:Number):void
		{
			_scrollControllerHeight = value;
		}
		
		public function get scrollControllerWidth():Number
		{
			return _scrollControllerWidth;
		}
		
		public function set scrollControllerWidth(value:Number):void
		{
			_scrollControllerWidth = value;
		}

		public function get lineScrollSize():Number
		{
			return _lineScrollSize;
		}

		public function set lineScrollSize(value:Number):void
		{
			_lineScrollSize = value;
		}
		
		public function get buttonAreaWidth():Number
		{
			return _buttonAreaWidth;
		}
		
		public function set buttonAreaWidth(value:Number):void
		{
			_buttonAreaWidth = value;
		}
		
		public function get documentLength():Number
		{	
			return _documentLength;
		}
		
		public function get model():ScrollModel
		{
			return _model;
		}
		
		public function get hMargin():Number
		{
			return _hMargin;
		}
		
		public function get vMargin():Number
		{
			return _vMargin;
		}
		
		public function handleModelUpdate():void
		{	
			_documentLength = _model.maxScrollPosition - _model.minScrollPosition;
			validate();
			//scrollPosition = _model.minScrollPosition;	
		}
		
		public function get trackForegroundColor():uint
		{
			return _trackForegroundColor;
		}
		
		public function set trackForegroundColor(value:uint):void
		{
			_trackForegroundColor = value;
			validate();
		}
		
		public function get trackAlpha():Number
		{
			return _trackAlpha;
		}
		
		public function set trackAlpha(value:Number):void
		{
			_trackAlpha = value;
			validate();
		}
		
		public function get thumbColor():uint
		{
			return _thumbColor;
		}
		
		public function set thumbColor(value:uint):void
		{
			_thumbColor = value;
			_moreButton = null;  // ensure the arrows change color also
			_lessButton = null;
			validate();
		}
		
		public function get thumbOverColor():uint
		{
			return _thumbOverColor;
		}
		
		public function set thumbOverColor(value:uint):void
		{
			_thumbOverColor = value;
			validate();
		}
		
		public function get thumbAlpha():Number
		{
			return _thumbAlpha;
		}
		
		public function set thumbAlpha(value:Number):void
		{
			_thumbAlpha = value;
			validate();
		}
		
		public function get thumbLineColor():uint
		{
			return _thumbLineColor;
		}
		
		public function set thumbLineColor(value:uint):void
		{
			_thumbLineColor = value;
			validate();
		}

		
		public function get trackBackgroundColor():uint
		{
			return _trackBackgroundColor;
		}
		
		public function set trackBackgroundColor(value:uint):void
		{
			_trackBackgroundColor = value;
			validate();
		}

		protected function handleNudgeForward(e:MouseEvent):void
		{
			scrollPosition += this._nudgeAmount;
			updateThumbPosition();
			dispatchScrollUpdateEvent();
		}
		
		protected function handleNudgeBackward(e:MouseEvent):void
		{
			scrollPosition -= this._nudgeAmount;
			updateThumbPosition();
			dispatchScrollUpdateEvent();
		}
		
		public function validate():void
		{	
			init();
			makeScrollBar();			
			makeTrack();			
			makeTrackGraphic();			
			makeThumb();
			makeButtons();
		}
		
		//mediator functions
		
		protected function handleMouseDown(event:MouseEvent):void
		{	
			_dragging = true;
			_draggingThumb = true;
			_thumb.startDrag(false, _trackExtents);
			_thumb.addEventListener(Event.ENTER_FRAME, onFrameHandler);
			stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			event.stopImmediatePropagation();
		}
		
		protected function handleMouseUp(event:MouseEvent):void
		{	
			_thumb.addEventListener(MouseEvent.MOUSE_OUT,handleMouseOut);
			_thumb.removeEventListener(MouseEvent.MOUSE_OVER,handleMouseOver);
			_thumb.getChildAt(0).alpha = 1; //upState
			_thumb.getChildAt(1).alpha = 0; //overState
			_thumb.stopDrag();
			_thumb.removeEventListener(Event.ENTER_FRAME, onFrameHandler)
			_dragging = false;
			_draggingThumb = false;
		}
		
		protected function handleMouseOver(event:MouseEvent):void
		{			
			_thumb.getChildAt(0).alpha = 0; //upState
			_thumb.getChildAt(1).alpha = 1; //overState
		}
		
		protected function handleMouseOut(event:MouseEvent):void
		{
			_thumb.addEventListener(MouseEvent.MOUSE_OVER,handleMouseOver);
			_thumb.getChildAt(0).alpha = 1; //upState
			_thumb.getChildAt(1).alpha = 0; //overState
		}
		
		protected function onFrameHandler(event:Event):void
		{
			calcScrollPosition();
			dispatchScrollUpdateEvent();
		}
		
		protected function calcScrollPosition():void
		{
			var _scrollPositionPercentage:Number  = (_thumb.x - _thumbStartPos.x - _dotSize) / _trackExtents.width;
			scrollPosition = _model.minScrollPosition + _scrollPositionPercentage * (_documentLength - _model.viewPortLength);
		}
		
		public function get scrollPosition():Number
		{
			return _scrollPosition;
		}
		
		private var _settingScrollPos:Boolean = false;
		
		public function set scrollPosition(value:Number):void
		{
			if (_settingScrollPos == false) {
				_settingScrollPos = true;
				var tempScrollPosition:Number = (value > 0) ? value : 0;  // filter out NaN and negative
				
				if (tempScrollPosition > _model.maxScrollPosition - _model.viewPortLength){
					
					tempScrollPosition = _model.maxScrollPosition - _model.viewPortLength;
				}
				
				if (tempScrollPosition < _model.minScrollPosition){
					
					tempScrollPosition = _model.minScrollPosition;
				}
				
				_scrollPosition = tempScrollPosition;
				updateThumbPosition();
				
				dispatchScrollUpdateEvent();
				_settingScrollPos = false;
			}
		}
		
		protected function dispatchScrollUpdateEvent():void
		{
			dispatchEvent(new ScrollUpdateEvent(ScrollUpdateEvent.SCROLL, {scrollPosition:_scrollPosition}));
		}
		
		
		protected function handleTrackClick(e:MouseEvent):void
		{
			if (e.eventPhase != EventPhase.AT_TARGET)
				return;
			
			if (e.localX < _thumb.x)
				_scrollPosition -= _lineScrollSize;	
			else if (e.localX > _thumb.x)
				_scrollPosition += _lineScrollSize;
			
			updateThumbPosition();
			dispatchScrollUpdateEvent();
		}
		
		protected function updateThumbPosition():void
		{
		}
		
		public function get nudgeAmount():Number
		{
			return _nudgeAmount;
		}

		public function set nudgeAmount(value:Number):void
		{
			_nudgeAmount = value;
		}
		
		public function get dragging():Boolean
		{
			return _dragging;
		}
		
		public function get draggingThumb():Boolean
		{
			return _draggingThumb;
		}
		
		
		// variable-speed scrolling stuff
		protected function buttonHolderCreated():void
		{
			this.useHandCursor = true;
			this.buttonMode = true;

			_scrollBar.addEventListener(MouseEvent.MOUSE_DOWN, buttonsMouseDown);
			_buttonHolder.addEventListener(MouseEvent.ROLL_OVER, buttonsRollover);
			_buttonHolder.addEventListener(MouseEvent.ROLL_OUT, buttonsRollout);
		}		
		
		private var _buttonsMouseDown:Boolean;
		private var _buttonsMouseGlobal:Point;
		private var _buttonsDeadZone:Boolean;  // true when mouse is between the two buttons
		
		private function buttonsMouseDown(event:MouseEvent):void
		{
			if (_buttonHolder.hitTestPoint(event.stageX, event.stageY)) {
				trace("model.maxScrollPosition="+model.maxScrollPosition, "portLength="+model.viewPortLength);
				_dragging = true;  // affects binding with wire to scrollposition, tested in ArenaView
				_buttonsMouseDown = true;
				_buttonsMouseGlobal = new Point(event.stageX, event.stageY);
				stage.addEventListener(Event.ENTER_FRAME, buttonsFrameHandler);
				stage.addEventListener(MouseEvent.MOUSE_MOVE, buttonsMouseMove);
				stage.addEventListener(MouseEvent.MOUSE_UP, buttonsMouseUp);
				event.stopImmediatePropagation();
				updateButtonsWhileMouseDown();
			}
		}
				
		private function buttonsMouseUp(event:MouseEvent):void
		{	
			if(!stage)
				return;
			
			_dragging = false;
			_buttonsMouseDown = false;
			_buttonsMouseGlobal = null;
			stage.removeEventListener(Event.ENTER_FRAME, buttonsFrameHandler)
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, buttonsMouseMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, buttonsMouseUp);
			
			var over:Boolean = _buttonHolder.hitTestPoint(event.stageX, event.stageY);
			var newState:String = over ? ScrollButton.OVER : ScrollButton.UP;
			_lessButton.state = newState;
			_moreButton.state = newState;
		}
		
		private function buttonsMouseMove(event:MouseEvent):void
		{	
			_buttonsMouseGlobal = new Point(event.stageX, event.stageY);
			updateButtonsWhileMouseDown();
		}
		
		private function updateButtonsWhileMouseDown():void
		{
			_buttonsDeadZone = true;
			var xx:Number = _buttonsMouseGlobal.x;
			var yy:Number = _buttonsMouseGlobal.y;
			if (_lessButton.hitTestPoint(xx, yy)) {
				_lessButton.state = ScrollButton.DOWN;
				_buttonsDeadZone = false;
			} else {
				_lessButton.state = ScrollButton.OVER;
			}
			if (_moreButton.hitTestPoint(xx, yy)) {
				_moreButton.state = ScrollButton.DOWN;
				_buttonsDeadZone = false;
			} else {
				_moreButton.state = ScrollButton.OVER;
			}
			
			// ensure the area beyond buttons is available for ultra-fast scrolling
			_buttonsDeadZone &&= Math.abs(offsetFromCenterOfButtons(_buttonsMouseGlobal)) < 4;
		}
		
		private function buttonsFrameHandler(event:Event):void
		{
			if(!stage)
				return;
			
			// enforce dead-zone between the two buttons
			if (_buttonsDeadZone == false) {
				var offset:Number = offsetFromCenterOfButtons(_buttonsMouseGlobal);
//				trace("offset="+offset);
				var delta:Number = offset * offset / 4;
				if (offset < 0)
					delta = -delta;
				scrollPosition += delta;
				dispatchScrollUpdateEvent();
			}
		}
		
		private function buttonsRollover(event:MouseEvent):void
		{
			if (_buttonsMouseDown == false) {
				_lessButton.state = ScrollButton.OVER;
				_moreButton.state = ScrollButton.OVER;
			}
		}
		
		private function buttonsRollout(event:MouseEvent):void
		{
			if (_buttonsMouseDown == false) {
				_lessButton.state = ScrollButton.UP;
				_moreButton.state = ScrollButton.UP;
			}
		}

		
		protected function offsetFromCenterOfButtons(global:Point):Number
		{
			return 0;
		}
	}
}