/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.view
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class ScrollButton extends Sprite
	{
		private var _upState:Sprite;
		private var _downState:Sprite;
		private var _overState:Sprite;
		private var _hitTestState:Sprite;  // probably not used
		
		public static const UP:String = "buttonUp";
		public static const OVER:String = "buttonOver";
		public static const DOWN:String = "buttonDown";
		
		public function ScrollButton(upState:Sprite, downState:Sprite, overState:Sprite, hitTestState:Sprite)
		{
			super();
			_upState = upState;
			_downState = downState;
			_overState = overState;
			addChild(_upState);
			addChild(_downState);
			addChild(_overState);
			_upState.visible = true;
			_overState.visible = false;
			_downState.visible = false;
			
			this.mouseChildren = false;
			this.useHandCursor = true;
			this.buttonMode = true;
		}
		
		public function set state(value:String):void
		{
			switch (value) {
				case UP:
					_upState.visible = true;
					_downState.visible = false;
					_overState.visible = false;
					break;
				case DOWN:
					_upState.visible = false;
					_downState.visible = true;
					_overState.visible = false;
					break;
				case OVER:
					_upState.visible = false;
					_downState.visible = false;
					_overState.visible = true;
					break;
			}
		}
	}
}