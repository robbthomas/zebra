package com.alleni.author.view
{
	import com.alleni.author.model.ScrollModel;
	import com.alleni.author.model.objects.AbstractTable;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class TruthTableScrollView extends HorizontalScrollView
	{
		public var table:AbstractTable;
		
		public function TruthTableScrollView(model:ScrollModel, scrollControllerWidth:Number, scrollControllerHeight: Number, table:AbstractTable)
		{
			super(model, scrollControllerWidth, scrollControllerHeight);
			
			_model = model;
			_scrollControllerWidth = scrollControllerWidth;
			_scrollControllerHeight = scrollControllerHeight;
			this.table = table;
			
			init();
			drawScrollBar();
		}
		
		override protected function init():void
		{	
			_documentLength = _model.maxScrollPosition - _model.minScrollPosition;
			_buttonAreaWidth = 34;
			_buttonAreaHeight = 18;
			_scrollControllerHeight = 18; 
			_thumbHeight = 14;

            _thumbColor = 0x7F7F7F;
            _thumbOverColor =  0x6B91BA;
            _thumbLineColor = 0;

            _lineScrollSize = 15;
			nudgeAmount = 15;
			_hMargin = 2;
		}
		
		override protected function computeButtonAreaX():Number
		{
			return 0;  // put buttons at left end of track
		}
		
		override protected function makeScrollBar():void
		{
			if(_scrollBar == null){
				_scrollBar = new Sprite();
				this.addChild(_scrollBar);
			}
			
			_scrollBar.graphics.clear();
			_scrollBar.graphics.beginFill(_trackBackgroundColor,1);
			_scrollBar.graphics.drawRect(0,0,_scrollControllerWidth, _scrollControllerHeight);
			_scrollBar.graphics.endFill();
			
			_scrollBar.x = _buttonAreaWidth;
			_scrollBar.y = _scrollControllerHeight - _scrollControllerHeight;
			
		}
		
		
		override protected function makeTrackGraphic():void
		{
            var startPos:Point = new Point(_hMargin, _scrollControllerHeight / 2);
            var endPos:Point = new Point(_scrollControllerWidth - _buttonAreaWidth, _scrollControllerHeight / 2);
            _thumbStartPos = startPos;
            _thumbEndPos = endPos;
            return;
		}

        override protected function makeButtons():void {
            return;
        }

		override protected function makeThumb():void
		{	
			var padding:Number = 3;
			
			if(_thumb == null){
				_thumb = new Sprite();
				_track.addChild(_thumb);
				_thumb.addEventListener(MouseEvent.ROLL_OVER,handleMouseOver);
				_thumb.addEventListener(MouseEvent.ROLL_OUT,handleMouseOut);
			}
			else{
				
				removeSpriteChildren(_thumb);
			}

			var trackLength:Number = Math.abs((_thumbEndPos.x - _dotSize) - (_thumbStartPos.x + _dotSize));
			
			_thumbHeight  = _scrollControllerHeight - 2 * padding ;

			_thumb.x = _thumbStartPos.x + _dotSize;
			_thumb.y = _thumbStartPos.y - _thumbHeight / 2;
			
			_thumbWidth = Math.max((trackLength / _documentLength) * trackLength, 20);
			_thumb.visible = true;
			
			_upState = addThumbUpState();
			_thumb.addChild(_upState);
			
			_overState = addThumbOverState();
			_thumb.addChild(_overState);
			
			_trackExtents = new Rectangle(_thumbStartPos.x + _dotSize, _thumb.y, trackLength - _thumbWidth, 0);	
		}
	}
}