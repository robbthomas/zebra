package com.alleni.author.view.text
{
	import flash.display.Stage;
	
	public interface IRichEditor
	{
		function get text():String;
		function set text(value:String):void;
		function get x():Number;
		function get y():Number;
		function set x(value:Number):void;
		function set y(value:Number):void;
		function get textHeight():Number;
		function get textWidth():Number;
		function set textHeight(value:Number):void;
		function set textWidth(value:Number):void;
		function set lineHeight(value:Number):void;
		function set editable(value:Boolean):void;
		function get editable():Boolean;
		function set formattable(value:Boolean):void;
		function get formattable():Boolean;
		function set color(value:uint):void;
		function get color():uint;
		function get textFormat():Object;
		function set textFormat(value:Object):void;
		function get selectionFormat():Object;
		function setPaddingParams(padding:Object):void;
		function getPaddingParams():Object;
		//function getParent():DisplayObjectContainer;
		function get textController():*;
		function setCompositionSize(width:Number, height:Number):void;
		function addUpdateListener(callBack:Function):void;
		function removeUpdateListener(callBack:Function):void;
		function addSelectionListener(callBack:Function):void;
		function removeSelectionListener(callBack:Function):void;
		function get containerHeight():Number;
		function get containerWidth():Number;
		function set explicitLineBreak(value:Boolean):void;
		function get explicitLineBreak():Boolean;
		function set textAlign(value:String):void;
		function importFormat(format:*):void;
		function set captureEnter(value:Boolean):void;
		function set rawText(value:String):void;
		function set visible(value:Boolean):void;
		function set verticalScrollPolicy(policy:String):void;
		function get selectionStart():int;
		function get selectionEnd():int;
		function select(start:int, end:int):void;
		function refreshSelection():void;
		function get stage():Stage;
	}
}