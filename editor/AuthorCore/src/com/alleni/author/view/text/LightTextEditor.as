package com.alleni.author.view.text
{
import caurina.transitions.Tweener;

import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.palettes.InspectorController;
	import com.alleni.author.model.ui.Application;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	
	import mx.core.UIComponent;

	public class LightTextEditor extends Sprite
	{
		
		private const XOFFSET:Number = 10;
		private var _editor:IRichEditor;
		private var _width:Number;
		private var _height:Number;
		private var _lineColor:uint = 0xFFFF00;
		private var _backgroundColor:uint = 0x000000;
		private var _lineThickness:uint = 2;
		private var _referenceObject:Sprite;
		private var _filter:DropShadowFilter = new DropShadowFilter(1, 90, 0x000000, 0.75, 6, 6, 2, BitmapFilterQuality.HIGH);
		private var _editingView:Sprite;
		
		public function LightTextEditor()
		{
			filters = [_filter];
			this.addEventListener(MouseEvent.MOUSE_DOWN, handleMouse);
		}
		
		public function set editingView(value:Sprite):void
		{
			_editingView = value;
		}

		public function setReferenceObject(value:Sprite):void
		{
			_referenceObject = value;
		}

		public function reposition():void{
			
			if(!_referenceObject || !_editingView)
				return;

                if (!_editingView.contains(this))
				    _editingView.addChild(this);
				
				
				//current assumption is that the _editingView parent is either presentation container or rightcontrol area
				var inspectorWidth:Number = Application.instance.inspectorVisible == true || Application.instance.ribbonLibraryVisible == true?EditorUI.instance.rightControlBar.width:0;
				var point:Point = _referenceObject.parent.localToGlobal(new Point(_referenceObject.x, _referenceObject.y));
				
				//if field is not in the inspector or ribbon library, adjust x position so that it doesn't run underneath the rightcontrol area
				if(_editingView === EditorUI.instance.presentationContainer.editingView || _editingView === EditorUI.instance.rightControlBar.editingView){
					var _appContainer:UIComponent = EditorUI.instance;
                    var isInspector:Boolean = _editingView === EditorUI.instance.rightControlBar.editingView;
		
					if (point.x > _appContainer.width - _editor.containerWidth - (isInspector? 0: inspectorWidth)) {
						point.x = _appContainer.width - _editor.containerWidth - (isInspector? 0: inspectorWidth);
					}
					if (point.y > _appContainer.height - _editor.containerHeight) {
						point.y = _appContainer.height - _editor.containerHeight
					}
                }
			
				
				var editorPoint:Point = _editingView.parent.globalToLocal(point);
				
				Tweener.addTween(this, {x: editorPoint.x - XOFFSET, time:0.2});
				y = editorPoint.y;
			
		}

		public function setEditor(value:IRichEditor):void
		{
			_editor = value;
			_editor.x = 0;
			addChild(Sprite(_editor));
			
		}

		public function setLineThickness(value:uint):void
		{
			_lineThickness = value;
		}

		public function setLineColor(value:uint):void
		{
			_lineColor = value;
		}
		
		public function setBackgroundColor(value:uint):void
		{
			_backgroundColor = value;
		}

		public function setSize(w:Number, h:Number):void
		{
			_width = w;
			_height = h;
			invalidate();
		}


		public function setCompositionSize(w:Number, h:Number):void
		{	
			_editor.setCompositionSize(w, h);  	
		}
		
		private function invalidate():void{
			draw();
		}
			
		protected function draw():void
		{
			with(graphics){
				clear();
				lineStyle(_lineThickness,_lineColor,1);
				beginFill(_backgroundColor, 1);
				drawRect(0, 0, _width, _height);
				graphics.endFill();
			}
		}
		
		/*eat up mousedown so that user doesn't inadvertently hide message center and leave this open */
		// NOTE: this editor is not used only for MC's, so be careful not to make assumptions that it is
		protected function handleMouse(event:Event):void{
			event.stopImmediatePropagation();
		}
		
		public function refreshSelection():void{
			if (_editor.stage)
				_editor.stage.addEventListener(Event.ENTER_FRAME, refreshSelectionListener);
		}
	
		
		private function refreshSelectionListener(event:Event):void
		{
			if (_editor.stage)
				_editor.stage.removeEventListener(Event.ENTER_FRAME, refreshSelectionListener);
			
			var start:int = _editor.selectionStart;
			var end:int = _editor.selectionEnd;
			_editor.select(-1, -1);
			_editor.select(start, end);
		}
		
	}
}