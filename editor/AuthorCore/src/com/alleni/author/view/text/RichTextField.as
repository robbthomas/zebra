/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

//to do transfer keyhandler from old controller to new controller

package com.alleni.author.view.text
{
    import com.alleni.author.application.ApplicationUI;
    import com.alleni.author.controller.objects.RichTextController;
    import com.alleni.author.controller.text.EditManager;
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.controller.ui.RichContainerController;
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.definition.text.ActionMap;
    import com.alleni.author.definition.text.Fonts;
    import com.alleni.author.definition.text.FormattedText;
    import com.alleni.author.model.objects.TextObject;
    import com.alleni.taconite.event.ApplicationEvent;
    import com.alleni.taconite.event.TextEditEvent;
    import com.alleni.taconite.factory.ISystemClipboard;
    import com.alleni.taconite.factory.TaconiteFactory;
    
    import flash.display.BlendMode;
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
import flash.geom.Rectangle;
    import flash.text.engine.CFFHinting;
    import flash.text.engine.ElementFormat;
    import flash.text.engine.FontLookup;
    import flash.text.engine.FontPosture;
    import flash.text.engine.FontWeight;
    import flash.text.engine.RenderingMode;
    import flash.text.engine.TextLine;
    
    import flashx.textLayout.compose.ISWFContext;
    import flashx.textLayout.conversion.ConversionType;
    import flashx.textLayout.conversion.ITextImporter;
    import flashx.textLayout.conversion.TextConverter;
    import flashx.textLayout.edit.IEditManager;
    import flashx.textLayout.edit.SelectionFormat;
    import flashx.textLayout.edit.TextScrap;
    import flashx.textLayout.elements.Configuration;
    import flashx.textLayout.elements.IFormatResolver;
    import flashx.textLayout.elements.ParagraphElement;
    import flashx.textLayout.elements.SpanElement;
    import flashx.textLayout.elements.TextFlow;
    import flashx.textLayout.elements.TextRange;
    import flashx.textLayout.events.SelectionEvent;
    import flashx.textLayout.events.UpdateCompleteEvent;
    import flashx.textLayout.formats.FormatValue;
    import flashx.textLayout.formats.ITextLayoutFormat;
    import flashx.textLayout.formats.LeadingModel;
    import flashx.textLayout.formats.LineBreak;
    import flashx.textLayout.formats.TextAlign;
    import flashx.textLayout.formats.TextDecoration;
    import flashx.textLayout.formats.TextLayoutFormat;
    import flashx.textLayout.formats.VerticalAlign;
    import flashx.undo.UndoManager;
    
    import mx.core.mx_internal;
    import mx.events.CloseEvent;
    
    import spark.layouts.HorizontalAlign;
	
    /**
     * View of a text box which uses the Text Layout Framework to display rich, editable text in a Sprite.
     */
    public class RichTextField extends Sprite implements IRichEditor
    {
		private static const FLAG_TEXT_SET:uint 			= 1 << 1;
		private static const FLAG_FORMAT_INVALID:uint 		= 1 << 2;
		private static const FLAG_TEXT_LINES_INVALID:uint 	= 1 << 3;
		private static const FLAG_GRAPHICS_INVALID:uint 	= 1 << 4;
		private static const FLAG_AUTOSIZE:uint				= 1 << 5;
		private static const FLAG_BORDER:uint				= 1 << 6;
		private static const FLAG_BACKGROUND:uint			= 1 << 7;
		private static const FLAG_EMBED_FONTS:uint			= 1 << 8;
		private static const FLAG_EDITING:uint				= 1 << 9;
		private static const FLAG_EDITABLE:uint				= 1 << 10;
		private static const FLAG_VALIDATE_IN_PROGRESS:uint = 1 << 11;
		
		private static const FLAG_FORMATTABLE:uint			= 1 << 12;
		private static const FLAG_INSPECTABLE:uint			= 1 << 13;
		private static const FLAG_SET_TEXTFLOW:uint		    = 1 << 14;
		
		private static const ALL_INVALIDATION_FLAGS:uint =
			FLAG_FORMAT_INVALID |
			FLAG_TEXT_LINES_INVALID |
			FLAG_GRAPHICS_INVALID;
		
		public var borderThickness:Number = 1;
		public var borderAlpha:Number = 1;
		public var borderColor:uint = 0xff0000;
		public var backgroundAlpha:Number = 0;
		public var backgroundColor:uint = 0xFFFFFF;
		
		public static const SELECTION_COLOR:uint = 0xCCCCCC;
		public static const SELECTION_ALPHA:Number = 0.7;
		//public static const SELECTION_BLEND_MODE:String = BlendMode.MULTIPLY;
		public static const SELECTION_BLEND_MODE:String = BlendMode.INVERT;
		
		private var _textFlow:TextFlow;
        private var _textContainer:Sprite;
    	private var _containerController:RichContainerController;
    	private var _span:SpanElement;
		
		private var _flags:uint = 0;
		private var _alwaysUseTextFlow:Boolean = true;
		
		protected var _textHeight:Number;
		protected var _textWidth:Number;
		
		private var _textController:RichTextController;
		private var _config:Configuration;
		private var _textObject:TextObject;
		
		private var _captureEnter:Boolean = false;
		private var _formatResolver:IFormatResolver;
		
		private var _defaultFontFamily:String = Fonts.DEFAULT_FAMILY;
		private var allowableVAlignmentValues:Array = [VerticalAlign.BOTTOM,VerticalAlign.MIDDLE,VerticalAlign.TOP];
		private var allowableHAlignmentValues:Array = [HorizontalAlign.CENTER,HorizontalAlign.LEFT,HorizontalAlign.RIGHT];
		
		private var _tempColor:uint = 0;
		
		               
		use namespace mx_internal;
		
		/**
		* @param width width of text field
		* @param height height of text field 
		* @param embedFonts true value causes fontlookup to look for embedded fonts versus device
		* @param textFlow  uses existing textflow that may have been generated from markup; else richtextfield creates its own textflow
		* @param configuration primarily affects the selection format -- new textflows may be created with a specific configuration
		* @param textObject allows richtextfield to set the editing mode on textObject
		* @param defaultFontFamily message center titles, for example, use the Zebra font as default; else, richtextfield uses the font family that is passed to it
		*/

        public function RichTextField(width:Number=100, height:Number=50, embedFonts:Boolean=true, textFlow:TextFlow = null, config:Configuration = null, textObject:TextObject = null, defaultFontFamily:String = Fonts.DEFAULT_FAMILY)
        {
			_config = config;
			_textObject = textObject;
			_defaultFontFamily = defaultFontFamily;
	
			// initialize edit flags
			setFlagToValue(FLAG_EDITING, false);
			setFlagToValue(FLAG_EDITABLE, true);
			setFlagToValue(FLAG_FORMATTABLE, true);
			setFlagToValue(FLAG_INSPECTABLE, true);  //flag to allow inspector to make changes to text
			setFlagToValue(FLAG_EMBED_FONTS, embedFonts);
	
			createTextFlow(textFlow);

			this.setPadding(10);
			this.setCompositionSize(width, height);
			this.focusRect = false;
			this.filters = null;
        }
		
		public function set textFlow(textFlow:TextFlow):void
		{
			setFlag(FLAG_SET_TEXTFLOW);
			createTextFlow(textFlow);
		}

		public function get textController():*
		{
			return _textController;
		}

		public function set textController(value:RichTextController):void
		{
			_textController = value;
		}
		
		
		/*
		TextObjects have their outermost properties defined by the ZebraDefault style.  
		They should never write to the outermost span.  This would stop the inspector from responding.
		*/
		
		public function makeOuterSpanReadOnly(value:Boolean):void{
			
			if(_textController)
				_textController.makeOuterSpanReadOnly(value);
		}
		
		
		public function set formatResolver(value:IFormatResolver):void
		{
			this._formatResolver = value;
			this.textFlow.formatResolver = value;
			setFlag(FLAG_TEXT_LINES_INVALID | FLAG_GRAPHICS_INVALID);
			invalidate();
		}

		// this is a kluge until richtextfield is replaced 			<-- with what?
		public function set alwaysUseTextFlow(value:Boolean):void
		{
			_alwaysUseTextFlow = value;
		}

		public function set embedFonts(value:Boolean):void
		{
			if (value == testFlag(FLAG_EMBED_FONTS))  
				return;
			
			
			setFlagToValue(FLAG_EMBED_FONTS, value);
			
			if (value) {
				_textFlow.fontLookup = FontLookup.EMBEDDED_CFF;
				_textFlow.cffHinting = CFFHinting.NONE;
				_textFlow.renderingMode = RenderingMode.CFF;
				_textFlow.flowComposer.swfContext = ISWFContext(ApplicationUI.instance.getFontContext(_defaultFontFamily, false, false, FontLookup.EMBEDDED_CFF));
			} else {
				_textFlow.fontLookup = FontLookup.DEVICE;
				_textFlow.cffHinting = CFFHinting.NONE;
				_textFlow.renderingMode = RenderingMode.NORMAL;
			}
			setFlag(FLAG_TEXT_LINES_INVALID | FLAG_GRAPHICS_INVALID);
			invalidate();
		}
		
		public function get embedFonts():Boolean
		{
			return testFlag(FLAG_EMBED_FONTS);
		}
		
		public function get textFlowSet():Boolean{
			
			return this.testFlag(FLAG_SET_TEXTFLOW); 
		}
		
		public function set textFlowSet(value:Boolean):void
		{	
			setFlagToValue(FLAG_SET_TEXTFLOW, value);
		}

	    public function canUndo():Boolean
		{
		    return IEditManager(_textFlow.interactionManager).undoManager.canUndo();
	    }

	    public function canRedo():Boolean
		{
		    return IEditManager(_textFlow.interactionManager).undoManager.canRedo();
	    }
		
		public function handleUndo(e:TextEditEvent=null):void
		{
			if (IEditManager(_textFlow.interactionManager).undoManager.canUndo())
				IEditManager(_textFlow.interactionManager).undo();
		}
			
		public function handleRedo(e:TextEditEvent=null):void
		{
			if (IEditManager(_textFlow.interactionManager).undoManager.canRedo())
				IEditManager(_textFlow.interactionManager).redo();
		}
				
		public function handleCut(e:Event):void
		{
			var cutScrap:TextScrap = IEditManager(_textFlow.interactionManager).cutTextScrap();
            if (cutScrap != null) {
                TaconiteFactory.getClipboardImplementation().setData(TaconiteFactory.getClipboardImplementation().TEXT_SCRAP, cutScrap);
            }
        }
		
		public function handleCopy(e:Event):void
		{
			var copyScrap:TextScrap = TextScrap.createTextScrap(new TextRange(_textFlow, IEditManager(_textFlow.interactionManager).absoluteStart, IEditManager(_textFlow.interactionManager).absoluteEnd));
			TaconiteFactory.getClipboardImplementation().setData(TaconiteFactory.getClipboardImplementation().TEXT_SCRAP, copyScrap);
		}
		
		public function handlePaste(e:Event):void
		{
			if (!e) { // need special handling because the system paste handling will ignore this without a valid PASTE event.
				var clipboard:ISystemClipboard = TaconiteFactory.getClipboardImplementation(e);
				var contents:TextScrap = clipboard.getData(clipboard.TEXT_SCRAP) as TextScrap;
				IEditManager(_textFlow.interactionManager).pasteTextScrap(contents);	
			}
			
			
			//MessageCenter titles, for example, shouldn't get color styling from pasted text scraps
			if(!formattable){
				_tempColor = this._span.format.color;
				this.addEventListener(Event.ENTER_FRAME, handleTextPaste);
			}	
		}
		
		//prevent textfields with formattable flag set to false from getting affected by pasted text
		private function handleTextPaste(event:Event):void{
			this.removeEventListener(Event.ENTER_FRAME, handleTextPaste);
			this._textController.clearAllStylesFromAllElements(_textFlow);
			this.color = _tempColor;
		}
		
		private function handleNameAlertSelection(event:CloseEvent):void
		{
		
		}
		
		
		
		public function handleSelectAll(e:TextEditEvent=null):void
		{
			_textFlow.interactionManager.selectAll();
			
			setFlag(FLAG_TEXT_LINES_INVALID);
			invalidate();
		}
		
		public function select(start:int, end:int):void
		{
			handleBeVeryInsistentAboutMaintainingFocus(null);
			
			setFlag(FLAG_TEXT_LINES_INVALID);
			invalidate();
			
			this.addEventListener(Event.RENDER, handleBeVeryInsistentAboutMaintainingFocus);
		}
		
		private function handleBeVeryInsistentAboutMaintainingFocus(e:Event):void
		{
			if(!_textFlow.interactionManager) {
				this.removeEventListener(Event.RENDER, handleBeVeryInsistentAboutMaintainingFocus);
				return;
			}
			
			_textFlow.interactionManager.setFocus();
		}
		
        public function set text(value:String):void
        {
			// could trigger error here.
			if (value == null || value == _span.text)   // ignore attempt to set text while editing: it causes blinking cursor to dissappear
				return;
			_span.text = value;
			
			setFlag(FLAG_TEXT_LINES_INVALID | FLAG_GRAPHICS_INVALID);
			invalidate();
        }
		
		/* this method is used to get a clean textflow and prevent problems caused by pasting into the textfield.  Intended to be used by LightEditableLabel as
		it switches from display mode to edit mode.  Ensures that the RichTextField cleanly receives its value from LightEditableLabel. */
		
		public function set rawText(value:String):void
		{	
			//if (value == null || value == _span.text)   
			if (value == null)   
				return;
			
			if (value == "")  //an empty string crashes ContainerController at "if (flowComposer.getLineAt(lineIdx).controller == this)"  lineIdx is -1
				value = " ";
			
			_span.text = value;
			var p:ParagraphElement = new ParagraphElement();
			p.addChild(_span);
			textFlow.replaceChildren(0,textFlow.numChildren,p);
			
			setFlag(FLAG_TEXT_LINES_INVALID);
			invalidate();
		}
		
        public function get text():String
        {
			//this is a kluge -- the text component gets bogged down by text with multiple paraagraphs
			
			if (_alwaysUseTextFlow){
				var temp:String = _textFlow.getText();
				return temp;
			}
			else{
				return _span.text;
			}
        }
		
		public function extractTextFromTextFlow():String
		{	
			return _textFlow.getText();
		}
		
		public function exportText():String
		{	
			return TextConverter.export(_textFlow, TextConverter.PLAIN_TEXT_FORMAT, ConversionType.STRING_TYPE).toString();
		}
		
		public function exportMarkup():String
		{ 
			return TextConverter.export(_textFlow, TextConverter.TEXT_LAYOUT_FORMAT, ConversionType.STRING_TYPE).toString();
		}
		
		public function importFormat(format:*):void
		{	
			if(format is FormattedText){
				this.createTextFlow((format as FormattedText).toTextFlow());
			}
		}
		
		
		private function createInteractionManager():void
		{	
			if(_textFlow == null)
				return;
			
			_textFlow.interactionManager = new EditManager(new UndoManager());
			_textFlow.interactionManager.selectAll();
		}
		
        public function set editing(value:Boolean):void
        {
			if (value == editing)
				return;

			if(!value) {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_CURSOR));
                this.dispatchEvent(new TextEditEvent(TextEditEvent.LOST_FOCUS,null));
            }

			// dont allow editing a non editable field
			if (!editable && value)
				return;

			setFlagToValue(FLAG_EDITING, value);
	    	
        	if (value) {
        		trace("RichTextField OPENING EDITING");
				createInteractionManager();
				
        	} else {
        		trace("RichTextField CLOSING EDITING");
				
				if(_textFlow.interactionManager) {
					_textFlow.interactionManager.selectRange(0,0);
					_textFlow.interactionManager = null;
				}
                if(scrollRect)
                    this.scrollRect = new Rectangle(0,0,scrollRect.width,scrollRect.height);

                // discard the hit-area created by ContainerController during editing
                _textContainer.graphics.clear();
        	}

			setFlag(FLAG_GRAPHICS_INVALID);
			invalidate();
			
			if(_textObject)
				_textObject.editing = value;
        }
		
        public function get editing():Boolean
        {
        	return testFlag(FLAG_EDITING);
        }
		
		public function set editable(value:Boolean):void
		{
			if (value == editable)
				return;
			setFlagToValue(FLAG_EDITABLE, value);
		}
		
		public function get editable():Boolean
		{
			return testFlag(FLAG_EDITABLE);
		}
		
		public function set formattable(value:Boolean):void
		{
			if (value == formattable)
				return;
			setFlagToValue(FLAG_FORMATTABLE, value);
		}
		
		public function get formattable():Boolean
		{
			return testFlag(FLAG_FORMATTABLE);
		}
		
		public function setStyle(action:String, value:String, applyToTopLevel:Boolean = true):void
		{
			_textController.applyStyleToAllElements(action, value,  _textFlow);
		}
		
		/* not currently used, but will be in the future */
		public function setParagraphStyle(action:String, value:String):void
		{
			_textController.applyStyleToAllParagraphs(action, value, _textFlow);
		}
		
		public function clearStyleFromAllElements(action:String):void{
			_textController.clearStyleFromAllElements(action,_textFlow);
		}
		
		public function clearAllStylesFromFirstElement():void{
			_textController.clearAllStylesFromFirstElement(_textFlow);
		}
		
		public function set size(value:uint):void
		{
			if(_textController.isSelected && testFlag(FLAG_INSPECTABLE) && testFlag(FLAG_FORMATTABLE)){
				_textController.perform(ActionMap.FONTSIZE_ACTION,value);
			}
			
			else{
				_textController.applyStyleToAllElements(ActionMap.FONTSIZE_ACTION, value, _textFlow); 
			}
			
		}
		
		
		public function set styleName(value:String):void
		{	
			if(_textController.isSelected && testFlag(FLAG_INSPECTABLE) && testFlag(FLAG_FORMATTABLE)){
				_textController.applyStyleName(value);
				setFlag(FLAG_TEXT_LINES_INVALID | FLAG_GRAPHICS_INVALID);
				invalidate();
				
			}
			else{
				_textController.applyStyleToAllElements(ActionMap.STYLENAME_ACTION, value, _textFlow);
			}	
		}
		
		public function set fontFamily(value:String):void
		{
			if(_textController.isSelected && testFlag(FLAG_INSPECTABLE) && testFlag(FLAG_FORMATTABLE)){
				_textController.perform(ActionMap.FONTNAME_ACTION,value);
			}
			else{
				_textController.applyStyleToAllElements(ActionMap.FONTNAME_ACTION, value, _textFlow);
			}
			
			//trace( "Font information: ", (this.getChildAt(0) as TextLine).dump());
			
//			if(this.numChildren>0 && this.getChildAt(0) as TextLine){
//				LogService.debug( "Font information: " + (this.getChildAt(0) as TextLine).dump());
//				LogService.debug( "Textflow information: " + this.exportMarkup());
//			}
		}
		
		public function set fontAlpha(value:Number):void
		{
			if(_textController.isSelected && testFlag(FLAG_INSPECTABLE) && testFlag(FLAG_FORMATTABLE)){
				_textController.perform(ActionMap.FONTALPHA_ACTION,value);
			}
			else{
				_textController.applyStyleToAllElements(ActionMap.FONTALPHA_ACTION, value, _textFlow);
			}
		}
		
		public function set italic(value:Boolean):void
		{
			var styleValue:String = value?FontPosture.ITALIC:FontPosture.NORMAL;
			
			if(_textController.isSelected && testFlag(FLAG_INSPECTABLE) && testFlag(FLAG_FORMATTABLE)){
				_textController.perform(ActionMap.ITALIC_ACTION,styleValue);
			}
			else{
				_textController.applyStyleToAllElements(ActionMap.ITALIC_ACTION, styleValue, _textFlow);
			}
		}
		
		public function set bold(value:Boolean):void
		{
			var styleValue:String = value?FontWeight.BOLD:FontWeight.NORMAL;
			
			if(_textController.isSelected && testFlag(FLAG_INSPECTABLE) && testFlag(FLAG_FORMATTABLE)){
				_textController.perform(ActionMap.BOLD_ACTION, styleValue);
			}
			else{
				_textController.applyStyleToAllElements(ActionMap.BOLD_ACTION, styleValue, _textFlow);
			}
		}
		
		public function set underline(value:Boolean):void
		{
			var styleValue:String = value?TextDecoration.UNDERLINE:TextDecoration.NONE;
			
			if(_textController.isSelected && testFlag(FLAG_INSPECTABLE) && testFlag(FLAG_FORMATTABLE)){
				_textController.perform(ActionMap.UNDERLINE_ACTION,styleValue);
			}
			else{
				_textController.applyStyleToAllElements(ActionMap.UNDERLINE_ACTION, styleValue,  _textFlow);
			}
		}
		
		public function set kerning(value:String):void
		{
			_textFlow.kerning = value;	
		}
		
		public function set color(value:uint):void
		{
			if(_textController.isSelected && testFlag(FLAG_INSPECTABLE) && testFlag(FLAG_FORMATTABLE)){
				_textController.perform(ActionMap.FOREGROUNDCOLOR_ACTION, value);
			}
			else{
				_textController.applyStyleToAllElements(ActionMap.FOREGROUNDCOLOR_ACTION, value,  _textFlow);
			}
		}
		
		public function set verticalAlign(value:String):void
		{
			if (isValid(value, allowableVAlignmentValues)){
				_textFlow.verticalAlign = value;
			}
			else{
				_textFlow.verticalAlign = VerticalAlign.TOP;
			}
		}
		
		public function set textAlign(value:String):void
		{
			if (isValid(value, allowableHAlignmentValues)){
				_textFlow.textAlign = value;
			}
			else{
				_textFlow.textAlign = HorizontalAlign.LEFT;
			}
		}
		
		public function get color():uint
		{
			return _textFlow.color;
		}
		
		public function set border(value:Boolean):void
		{
			if (value == border)
				return;
			setFlagToValue(FLAG_BORDER, value);
		}
		
		public function get border():Boolean
		{
			return testFlag(FLAG_BORDER);
		}
		
		public function set background(value:Boolean):void
		{
			if (value == background)
				return;
			setFlagToValue(FLAG_BACKGROUND, value);
		}
		
		public function get background():Boolean
		{
			return testFlag(FLAG_BACKGROUND);
		}
		
        public function set explicitLineBreak(value:Boolean):void
        {
			if (value)
				_textFlow.lineBreak = LineBreak.EXPLICIT;
			else
				_textFlow.lineBreak = LineBreak.TO_FIT;
        }
		
		public function get explicitLineBreak():Boolean{
			
			if(_textFlow.lineBreak == LineBreak.EXPLICIT){
				return true;
			}
			else{
				return false;
			}
		}

		public function get containerHeight():Number
		{
			return _containerController.getContentBounds().height;	
		}
		
		
		public function get containerWidth():Number
		{
			return _containerController.getContentBounds().width;
		}
		
		/*get ContainerHeight is just an estimate -- this force a recomposition, which returns an exact metric */
		
		public function calculateContainerHeight():Number{
			_textFlow.flowComposer.composeToPosition();
			var contentHeight:Number = _containerController.getContentBounds().height;	
			return contentHeight;
		}
		
		public function get verticalScrollPosition():Number{
			
			return _containerController.verticalScrollPosition;
		}
		
		public function set verticalScrollPosition(value:Number):void{
			
			_containerController.verticalScrollPosition = value;
		}
		
		public function set verticalScrollPolicy(policy:String):void{
			
			_containerController.verticalScrollPolicy = policy;
		}
	
		
		public function get numLines():int
		{
			return _textFlow.flowComposer.numLines;
		}
		
		public function set left(value:Boolean):void
		{
			if (value)
				_textFlow.textAlign = TextAlign.LEFT;
		}
        
        public function set right(value:Boolean):void
        {
        	if (value)
				_textFlow.textAlign = TextAlign.RIGHT;
        }
		
		public function set center(value:Boolean):void
		{
			if (value)
				_textFlow.textAlign = TextAlign.CENTER;
		}
		
        
        public function set justify(value:Boolean):void
        {
        	if (value)
				_textFlow.textAlign = TextAlign.JUSTIFY;
        }
		
		// BUG: when autoSize is on, the backspace key fails and sometimes gets crash:  The TextLine is INVALID and cannot be used...
		// this is apparently not related to vertical alignment (tried various values including default)
		// and not related to setCompositionSize (tried omitting calls to it)
//		public function set autoSize(value:Boolean):void
//		{
//			if (value == autoSize)
//				return;
//			setFlagToValue(FLAG_AUTOSIZE, value);
//		}
		
		public function get autoSize():Boolean
		{
			return testFlag(FLAG_AUTOSIZE);
		}
        
        public function set keyHandler(value:Function):void
        {
			_containerController.keyHandler = value;
        }
        
        public function set captureEnter(value:Boolean):void
        {
        	this._containerController.captureEnter = value;
			_captureEnter = value;
        }
        
        public function setPadding(left:Number, top:Number=-1, right:Number=-1, bottom:Number=-1):void
        {
			left = Math.max(0,Math.min(100,left));
			
        	_containerController.paddingLeft = left;
        	
        	if (top < 0) {
	            _containerController.paddingTop = 0;
	            _containerController.paddingRight = left;
	            _containerController.paddingBottom = left;
	        } else {
	            _containerController.paddingTop = top;
	            _containerController.paddingRight = right;
	            _containerController.paddingBottom = bottom;
	        }
			
			setFlag(FLAG_TEXT_LINES_INVALID | FLAG_GRAPHICS_INVALID);
			invalidate();
        }
        
        public function setCompositionSize(width:Number, height:Number):void
		{
            _containerController.setCompositionSize(width, height);
			
			setFlag(FLAG_TEXT_LINES_INVALID | FLAG_GRAPHICS_INVALID);
            invalidate();
        }
        
        public function setFocus():void
        {
	        if(_textFlow.interactionManager == null) {
		        trace("Null interaction manager");
				createInteractionManager();
				_textFlow.interactionManager.setFocus();
	        } else {
				_textFlow.interactionManager.setFocus();
	        }

            this.dispatchEvent(new TextEditEvent(TextEditEvent.GOT_FOCUS,null));
        }

		public function focused():Boolean
		{
	        if(_textFlow.interactionManager == null) {
		        trace("Null interaction manager");
		        return false;
	        } else {
				return _textFlow.interactionManager.focused;
	        }
		}
		
		public function removeAllFlowElements():void
		{
			var initFormat:ITextLayoutFormat = _textFlow.format;
			_textFlow.replaceChildren(0,_textFlow.numChildren);
			initialize(initFormat);
		}
		
		private function setupTextController(tf:TextFlow):void
		{
			if(textController == null){
				textController = new RichTextController(tf);
			}
			else{
				textController.setTextFlow(tf);
			}
		}
		
		private function setupContainerController():void
		{	
			// set up a container controller at the prescribed size
            if (_textContainer == null) {
                _textContainer = new Sprite();
                addChild(_textContainer);
            }
			
			if (_containerController == null){
				_containerController = new RichContainerController(_textContainer);
			}
			_textFlow.flowComposer.addController(_containerController);
			_textFlow.addEventListener(MouseEvent.MOUSE_OUT, handleMouseOut);
		}

        public function getTextBounds(container:DisplayObject):Rectangle
        {
            // bounds of text, not including background fill
            return _textContainer.getBounds(container);
        }
		
		private function removeContainerController():void
		{	
			_containerController.keyHandler = null;
			_textFlow.flowComposer.removeAllControllers();
			_textFlow.removeEventListener(MouseEvent.MOUSE_OUT, handleMouseOut);
			
		}
		
		private function initialize(format:ITextLayoutFormat = null):void
		{				
			// paragraph and span bare minimum to display text
			var p:ParagraphElement = new ParagraphElement();
			_span = new SpanElement();
			
			if (format)
				_span.format = format;
			
			if (testFlag(FLAG_EMBED_FONTS)) {
				_span.fontLookup = FontLookup.EMBEDDED_CFF;
				_span.cffHinting = CFFHinting.NONE;
				_span.renderingMode = RenderingMode.CFF;
			} else {
				_span.fontLookup = FontLookup.DEVICE;
				_span.cffHinting = CFFHinting.NONE;
				_span.renderingMode = RenderingMode.NORMAL;
			}
			
			p.addChild(_span);
			_span.backgroundAlpha = FormatValue.INHERIT;
			_span.backgroundColor = FormatValue.INHERIT;
			p.backgroundAlpha = FormatValue.INHERIT;
			p.backgroundColor = FormatValue.INHERIT;
			_textFlow.addChild(p);
		}
		
		private function invalidate():void
		{
			if (stage)
				stage.invalidate();
			
			addEventListener(Event.RENDER, renderHandler);
			addEventListener(Event.ENTER_FRAME, renderHandler);
		}
		
		private function validateNow():void
		{
			
			setFlag(FLAG_VALIDATE_IN_PROGRESS);
			
			if (testFlag(FLAG_GRAPHICS_INVALID)) {
				graphics.clear();
				
				// tolerate setCompositionSize(NaN,NaN) without crashing drawRect here
				if (_containerController.compositionWidth>0 && _containerController.compositionHeight>0) {
					if (border) 
						graphics.lineStyle(borderThickness, borderColor, borderAlpha);
					
					graphics.beginFill(backgroundColor, background?backgroundAlpha:0.001);  // alpha enough to get mouse clicks
					
					graphics.drawRect(0, 0, _containerController.compositionWidth-1, _containerController.compositionHeight-1);
					graphics.endFill();
				}
			}
			
			if (testFlag(FLAG_TEXT_LINES_INVALID)) {
				if (autoSize) {
					_textFlow.flowComposer.removeAllControllers();
					var containerController:RichContainerController = new RichContainerController(this, NaN, NaN);
					this._containerController = containerController;
					_textFlow.flowComposer.addController(containerController);
				}
				_textFlow.flowComposer.updateAllControllers();
			}
			
			clearFlag(ALL_INVALIDATION_FLAGS | FLAG_VALIDATE_IN_PROGRESS);
		}
		
		private function renderHandler(event:Event):void
		{
			validateNow();
			
			removeEventListener(Event.RENDER, renderHandler);
			removeEventListener(Event.ENTER_FRAME, renderHandler);
		}

		private function testFlag(mask:uint):Boolean
		{
			return (_flags & mask) != 0;
		}
		
		private function setFlag(mask:uint):void
		{
			_flags |= mask;
		}
		
		private function clearFlag(mask:uint):void
		{
			_flags &= ~mask;
		}
		
		private function setFlagToValue(mask:uint, value:Boolean):void
		{
			if (value)
				_flags |= mask;
			else
				_flags &= ~mask;
		}
		
		public function get textFlow():TextFlow
		{	
			return _textFlow;
		}
		
		public function get selectionStart():int
		{
			if (_textFlow.interactionManager)
				return _textFlow.interactionManager.absoluteStart;
			else
				return 0;
		}
		
		public function get selectionEnd():int
		{
			if (_textFlow.interactionManager)
				return _textFlow.interactionManager.absoluteEnd;
			else
				return 0;
		}

		override public function toString():String
		{
			return "RichTextField" + "/" + ((parent) ? parent.toString() : "");
		}
		
		//implementation of IRichText interface
		
		public function get textHeight():Number
		{
			return _textHeight;
		}
		
		public function get textWidth():Number
		{
			return _textWidth;
		}
		
		public function set textHeight(value:Number):void
		{
			_textHeight = value;
		}
		
		public function set textWidth(value:Number):void
		{
			_textWidth = value;
			setCompositionSize(_textWidth,_textHeight);
		}
		
		public function set lineHeight(value:Number):void
		{
			if (value != value) return;
			const percentage:int = Math.round(value * 100);
			if (percentage >= 0 && percentage <= 100)
				textFlow.lineHeight = percentage.toString() + "%";
			invalidate();
		}
		
		public function get textFormat():Object
		{
			return textFlow.hostFormat;
		}
		
		public function set textFormat(value:Object):void
		{	
			if(value as ElementFormat){	
				textFlow.format = convertFormat(value);
			}
		}
		
		private function convertFormat(value:Object):TextLayoutFormat
		{	
			if (value as ElementFormat) {
				var format:ElementFormat = value as ElementFormat;
				var textLayoutFormat:TextLayoutFormat = new TextLayoutFormat();
				textLayoutFormat.fontSize = format.fontSize;
				textLayoutFormat.fontWeight = format.fontDescription.fontWeight;
				textLayoutFormat.fontStyle = format.fontDescription.fontPosture;
				textLayoutFormat.fontFamily = format.fontDescription.fontName;
				textLayoutFormat.fontLookup = format.fontDescription.fontLookup;
				textLayoutFormat.alignmentBaseline = format.alignmentBaseline;
				textLayoutFormat.baselineShift = format.baselineShift;
				//textLayoutFormat.firstBaselineOffset = BaselineOffset.AUTO  Position of the first base line
				textLayoutFormat.leadingModel = LeadingModel.ROMAN_UP;
				textLayoutFormat.cffHinting = format.fontDescription.cffHinting;
				textLayoutFormat.dominantBaseline = format.dominantBaseline;
				textLayoutFormat.color = format.color;
				textLayoutFormat.breakOpportunity = format.breakOpportunity;
				textLayoutFormat.digitCase = format.digitCase;
				textLayoutFormat.digitWidth = format.digitWidth;
				textLayoutFormat.kerning = format.kerning;
				textLayoutFormat.trackingLeft = format.trackingLeft;
				textLayoutFormat.trackingRight = format.trackingRight;
				textLayoutFormat.typographicCase = format.typographicCase;
				textLayoutFormat.renderingMode = format.fontDescription.renderingMode;
				
				
				return textLayoutFormat;
			} else{
				return null;   //can't convert
			}
		}
		
		public function setPaddingParams(padding:Object):void
		{	
			var leftPadding:Number = padding.left;
			var topPadding:Number  = padding.top;
			var rightPadding:Number = padding.right;
			var bottomPadding:Number = padding.bottom;
			
			setPadding(leftPadding,topPadding,rightPadding,bottomPadding);
		}
		
		public function getPaddingParams():Object
		{	
			if(_containerController){
				return {left:_containerController.paddingLeft, top:_containerController.paddingTop, right:_containerController.paddingTop, bottom:_containerController.paddingTop};
			} else{
				return null;
			}
		}
		
		public function setColumnCount(value:uint):void
		{
			if(value < 1)
				return;
				
			_textFlow.columnCount = value;
			//trace(this.exportMarkup());
			setFlag(FLAG_TEXT_LINES_INVALID);
			invalidate();
		}
		
		public function setColumnGap(value:Number):void
		{	
			_textFlow.columnGap = value;
			setFlag(FLAG_TEXT_LINES_INVALID);
			invalidate();
		}
		
		private function deselectAll():void
		{	
			if(_textFlow && _textFlow.interactionManager)
				_textFlow.interactionManager.selectRange(0,0);
		}
		
		public function selectAll():void
		{	
			if(_textFlow && _textFlow.interactionManager)
				_textFlow.interactionManager.selectAll();
		}
		
		public function refreshSelection():void //redraws the selectionshapes
		{
			if(_textFlow && _textFlow.interactionManager)
				_textFlow.interactionManager.refreshSelection();
		}
		
		private function createTextFlow(textFlowFromMarkup:TextFlow):void
		{	
			if(textFlowFromMarkup != null){
				
				if(_textFlow) {//if existing textflow, clean it up
					_textFlow.replaceChildren(0,_textFlow.numChildren);
					removeContainerController();
				}
				
				_textFlow = textFlowFromMarkup;
				_textFlow.fontLookup = FontLookup.EMBEDDED_CFF;
				
				if(_formatResolver != null)
					_textFlow.formatResolver = _formatResolver;
	
				setFlag(FLAG_TEXT_LINES_INVALID);
				invalidate();
			} else {
				_textFlow = new TextFlow(_config);  //_config gets passed into or set in constructor
				_textFlow.fontSize = 14;
				_textFlow.styleName = "zebradefault";
				_textFlow.fontFamily = _defaultFontFamily;
				_textFlow.fontLookup = FontLookup.EMBEDDED_CFF;
				
				initialize();
			}
			_textFlow.flowComposer.swfContext = ISWFContext(ApplicationUI.instance.getFontContext(_defaultFontFamily, false, false, FontLookup.EMBEDDED_CFF));
			setupTextController(_textFlow);
			setupContainerController();
			_textFlow.flowComposer.updateAllControllers();
		}
		
		public static function createTextFlowFromMarkup(markup:String,config:Configuration = null):TextFlow
		{	
			if(markup == null || markup == "")
				return null;
			
			//Create a default configuration, if none is passed
			if(config == null){
				var config:Configuration = new Configuration();
				var selectionFormat:SelectionFormat = new SelectionFormat(SELECTION_COLOR, SELECTION_ALPHA, SELECTION_BLEND_MODE);
				config.focusedSelectionFormat = selectionFormat;
			}
			
			var importer:ITextImporter = TextConverter.getImporter(TextConverter.TEXT_LAYOUT_FORMAT,config);
			importer.throwOnError = false;
			var _tempFlow:TextFlow  = importer.importToFlow(markup);
			
			return _tempFlow;
		}
		
		private function handleMouseOut(event:Event):void
		{
			// stop propagation here as there are bugs in some Flex components which attempt to
			// cast event.target to things like IMenuBarItemRenderer upon MOUSE_OUT.
			event.stopPropagation();
		}
		
		public function addUpdateListener(callBack:Function):void
		{	
			_textFlow.addEventListener(UpdateCompleteEvent.UPDATE_COMPLETE, callBack);	
		}
		
		public function removeUpdateListener(callBack:Function):void
		{	
			_textFlow.removeEventListener(UpdateCompleteEvent.UPDATE_COMPLETE, callBack);
		}
		
		public function addSelectionListener(callBack:Function):void
		{	
			_textFlow.addEventListener(SelectionEvent.SELECTION_CHANGE, callBack);
			
		}
		
		public function removeSelectionListener(callBack:Function):void
		{	
			_textFlow.removeEventListener(SelectionEvent.SELECTION_CHANGE, callBack);
		}
		
		public function get selectionFormat():Object
		{	
			if(_textFlow && _textFlow.interactionManager)
				return _textFlow.interactionManager.getCommonCharacterFormat() as TextLayoutFormat;
			
			return null;
		}
		
		private function isValid(value:String, array:Array):Boolean{
			
			if(array.indexOf(value) > -1)
				return true;
			
			return false;
		}
		
		/**
		 * Hit-test the TextLine objects within the field. 
		 * @param stageX
		 * @param stageY
		 * @return true if the point is on the text.
		 * 
		 */
		public function hitTestText(stageX:Number, stageY:Number):Boolean
		{
			for (var n:int = numChildren-1; n >= 0; n--) {
				var txLine:TextLine = getChildAt(n) as TextLine;
				if (txLine && txLine.hitTestPoint(stageX, stageY)) {
					return true;
				}
			}
			return false;
		}
    }
}
