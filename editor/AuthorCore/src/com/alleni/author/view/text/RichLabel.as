package com.alleni.author.view.text
{
	import com.alleni.author.application.ApplicationUI;
	import com.alleni.author.definition.text.Fonts;
	
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.engine.FontLookup;
	
	import mx.core.FTETextField;
	import mx.core.IFlexModuleFactory;
	import mx.core.mx_internal;
	
	use namespace mx_internal;
	
	public class RichLabel extends FTETextField
	{
		private var _fmt:TextFormat;
		
		public function RichLabel(width:Number=50)
		{
			super();
			
			_fmt = new TextFormat();
			_fmt.color = 0xffffff;
			_fmt.size = 11;
			_fmt.font = Fonts.DEFAULT_FAMILY;
			
			this.width = width;
			this.fontContext = ApplicationUI.instance.getFontContext(Fonts.DEFAULT_FAMILY, false, false, FontLookup.EMBEDDED_CFF) as IFlexModuleFactory;
			this.embedFonts = true;
	    	this.defaultTextFormat = _fmt;
		}
        
        public function set size(size:int):void
        {
        	_fmt.size = size;
        	updateFormat();
        }
		
		public function set color(color:uint):void
		{
			_fmt.color = color;
			updateFormat();
        }
		
		public function set italic(value:Boolean):void
		{
			_fmt.italic = value;
			updateFormat();
		}
		
		public function set bold(value:Boolean):void
		{
			_fmt.bold = value;
			updateFormat();
		}
		
		public function set left(value:Boolean):void
		{
			if (value) {
				_fmt.align = TextFormatAlign.LEFT;
				updateFormat();
			}
		}
        
        public function set right(value:Boolean):void
        {
        	if (value) {
        		_fmt.align = TextFormatAlign.RIGHT;
        		updateFormat();
        	}
        }
        
        public function set center(value:Boolean):void
        {
        	if (value) {
        		_fmt.align = TextFormatAlign.CENTER;
        		updateFormat();
        	}
        }
		
		public function italicizePrefix(endIndex:int=-1, color:int=-1):void
		{
			//TODO
		}
		
		private function updateFormat():void
		{
			this.setTextFormat(_fmt);
		}
	}
}