package com.alleni.author.view.text
{
	import flash.text.engine.TextLine;
	
	import flashx.textLayout.formats.TextAlign;
	
	public class LightLabel extends LightTextBase
	{	
		private var _multiLine:Boolean = false;
		
		public function LightLabel(width:Number = 100, colorVal:uint = 0xFFFFFF, multiLine:Boolean = false, height:Number = 25)
		{
			super(width,height,colorVal);  //25 is a default height
			_multiLine = multiLine;
		}
		
		override protected function createLines():void 
		{
			if(_width < 0)
				return;
			
			var textLine:TextLine = _block.createTextLine(null, _width);
			
			var _y:Number = 0;
			while(textLine)
			{
				_holder.addChild(textLine);
				_y += textLine.height;
				
				if(textAlign == TextAlign.CENTER)
					textLine.x = (width - textLine.width)/2;
				
				if(textAlign == TextAlign.RIGHT)
					textLine.x = (width - textLine.width);
				
				textLine.y = _y;
				textLine = _block.createTextLine(textLine, _width);
				
				if(!_multiLine)
					break;
			}
		}
	}
}