package com.alleni.author.view.text
{
	public interface ILightEditableText extends ILightText
	{
		
		function get formattedText():*;
		function set formattedText(value:*):void;
		function get prompt():String;
		function set prompt(value:String):void;
		function set expandedWidth(value:Number):void;
		function get expandedWidth():Number;
		
	}
}