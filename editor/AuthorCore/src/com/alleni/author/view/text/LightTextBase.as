package com.alleni.author.view.text
{
	import com.alleni.author.definition.text.Fonts;

import flash.display.DisplayObjectContainer;

import flash.display.Sprite;
	import flash.events.Event;
import flash.geom.Rectangle;
import flash.text.engine.CFFHinting;
	import flash.text.engine.ContentElement;
	import flash.text.engine.ElementFormat;
	import flash.text.engine.FontDescription;
	import flash.text.engine.FontLookup;
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	import flash.text.engine.GroupElement;
	import flash.text.engine.Kerning;
	import flash.text.engine.RenderingMode;
	import flash.text.engine.TextBlock;
	import flash.text.engine.TextElement;
	import flash.text.engine.TextLine;
	
	import flashx.textLayout.formats.TextAlign;
	
	
	public class LightTextBase extends Sprite 
	{
		private static const FLAG_TEXT_SET:uint 			= 1 << 1;
		private static const FLAG_FORMAT_BOLD:uint			= 1 << 2;
		private static const FLAG_FORMAT_ITALIC:uint		= 1 << 3;
		private static const FLAG_FORMAT_INVALID:uint 		= 1 << 4;
		private static const FLAG_TEXT_LINES_INVALID:uint 	= 1 << 5;
		private static const FLAG_GRAPHICS_INVALID:uint 	= 1 << 6;
		
		private static const FLAG_VALIDATE_IN_PROGRESS:uint = 1 << 7;
		
		private static const ALL_INVALIDATION_FLAGS:uint =
			FLAG_TEXT_SET |
			FLAG_FORMAT_INVALID |
			FLAG_TEXT_LINES_INVALID |
			FLAG_GRAPHICS_INVALID;
		
		protected var _text:String = " ";
		protected var _width:Number;
		protected var _block:TextBlock;
        protected var _holder:Sprite;
		protected var _textHeight:Number;
		protected var _textWidth:Number;
		protected var _leading:Number = 1;
		
		private var _elements:Vector.<ContentElement>;
		
		private var _color:uint;
		private var _fontsize:uint;
		private var _flags:uint = 0;
		private var _specialFormatElement:ElementFormat;
		private var _explicitLineBreak:Boolean = true;
		private var _textAlign:String = TextAlign.LEFT;
		private var _expandable:Boolean = true;
		
		
		
		public function LightTextBase(textWidth:Number=100, textHeight:Number = 25, colorVal:uint = 0xFFFFFF)
		{
			_width = textWidth;
			_textWidth = textWidth;
			_textHeight = textHeight;
			_color = colorVal;
			
			drawBackground();
			initializeElements();
			_block = new TextBlock(new GroupElement(_elements));
            _holder = new Sprite();
            addChild(_holder);
			createLines();
			this.mouseChildren = false;
		}
		
		public function get textAlign():String
		{
			return _textAlign;
		}

		public function set textAlign(value:String):void
		{
			_textAlign = value;
		}

		protected function createTextElement(elementFormat:ElementFormat, text:String=null):TextElement
		{
			return new TextElement((text==null)?_text:text, elementFormat);
		}
		
		
		public function set text(value:String):void
		{
			// could trigger error here.
			if (value == null || value == text)
				return;
			
			_text = value;
			
			setFlag(FLAG_TEXT_SET | FLAG_TEXT_LINES_INVALID | FLAG_GRAPHICS_INVALID);
			invalidate();
		}
		
		public function get text():String
		{
			return _text;	
		}
		
		override public function get width():Number{
			return _width;
		}
		
		override public function set width(value:Number):void
		{
			_width = value;
			
			setFlag(FLAG_TEXT_LINES_INVALID | FLAG_GRAPHICS_INVALID);
			invalidate();
		}
		
		public function set color(value:uint):void
		{
			_color = value;
			
			setFlag(FLAG_GRAPHICS_INVALID);
			invalidate();
		}
		
		public function get color():uint
		{
			return _color;
		}
		
		public function set size(value:uint):void
		{
			_fontsize = value;
			
			setFlag(FLAG_TEXT_LINES_INVALID | FLAG_GRAPHICS_INVALID);
			invalidate();
		}
		
		public function get size():uint{
			return _fontsize;
		}
		
		public function set bold(value:Boolean):void
		{
			if (value == bold)
				return;
			
			setFlagToValue(FLAG_FORMAT_BOLD, value);
			
			setFlag(FLAG_FORMAT_INVALID);
			invalidate();
		}
		
		public function get bold():Boolean
		{
			return testFlag(FLAG_FORMAT_BOLD);
		}
		
		public function set italic(value:Boolean):void
		{
			if (value == italic)
				return;
			
			setFlagToValue(FLAG_FORMAT_ITALIC, value);
			
			setFlag(FLAG_FORMAT_INVALID);
			invalidate();
		}
		
		public function get italic():Boolean
		{
			return testFlag(FLAG_FORMAT_ITALIC);
		}
		
		public function set explicitLineBreak(value:Boolean):void
		{
			_explicitLineBreak = value;
		}
		
		public function get explicitLineBreak():Boolean
		{
			return _explicitLineBreak;
		}
		
		
		public function formatRange(beginIndex:int=-1, endIndex:int=-1, color:uint=0xffffff, bold:Boolean=false, italic:Boolean=false):void
		{
			var preText:String = _text.substring(0,beginIndex);
			var modifiedText:String = _text.substring(beginIndex, endIndex + 1);
			var postText:String = _text.substring(endIndex+1, _text.length);
			
			text="";
			
			var normalElementFormat:ElementFormat = cloneElementFormat;
			
			var modifiedElementFormat:ElementFormat = cloneElementFormat;
			var modifiedFontDescription:FontDescription = cloneFontDescription;
			
			modifiedFontDescription.fontWeight = bold ? FontWeight.BOLD : FontWeight.NORMAL;
			modifiedFontDescription.fontPosture = italic ? FontPosture.ITALIC : FontPosture.NORMAL;
			modifiedElementFormat.fontDescription = modifiedFontDescription;
			modifiedElementFormat.color = color;
			_specialFormatElement = modifiedElementFormat;
			
			
			var index:uint = 0;
			_elements = new Vector.<ContentElement>(); 
			_elements.push(createTextElement(normalElementFormat));
			
			if(preText.length > 0){
				_elements.push(createTextElement(normalElementFormat, preText));
			}
			
			if(modifiedText.length > 0){
				_elements.push(createTextElement(modifiedElementFormat, modifiedText));
			}
			
			if(postText.length > 0){
				_elements.push(createTextElement(normalElementFormat, postText));
			}
			var groupElement:GroupElement = new GroupElement(_elements);
			
			_block = new TextBlock(groupElement);
			
			clearLines();
			createLines();
		}
		
		
		private function initializeElements():void
		{
			var fontDescription:FontDescription = new FontDescription(Fonts.DEFAULT_FAMILY, FontWeight.NORMAL, FontPosture.NORMAL, FontLookup.EMBEDDED_CFF);
			fontDescription.renderingMode = RenderingMode.CFF;
			fontDescription.cffHinting = CFFHinting.HORIZONTAL_STEM;
			_elements = new Vector.<ContentElement>();
			
			var elementFormat:ElementFormat = new ElementFormat(fontDescription);
			elementFormat.color = _color;
			elementFormat.fontSize = _fontsize;
			_elements.push(createTextElement(elementFormat));
		}
		
		private function get cloneElementFormat():ElementFormat
		{
			return _elements[0].elementFormat.clone();
		}
		
		private function get cloneFontDescription():FontDescription
		{
			return _elements[0].elementFormat.fontDescription.clone();
		}
		
		private function updateElementFormat():void
		{
			var origFormat:ElementFormat;
			
			for each (var element:TextElement in _elements){
				
				if(element.elementFormat === this._specialFormatElement)
					continue;
				
				origFormat = element.elementFormat.clone();
				origFormat.color = _color;
				origFormat.fontSize = _fontsize;
				origFormat.kerning = Kerning.ON;
				origFormat.trackingRight = 0;
				origFormat.trackingLeft = 0;
				element.elementFormat = origFormat;
			}
		}
		
		private function updateFontDescription():void
		{
			var newFormat:ElementFormat = cloneElementFormat;
			var newFontDescription:FontDescription = cloneFontDescription;
			newFontDescription.fontWeight = bold ? FontWeight.BOLD : FontWeight.NORMAL;
			newFontDescription.fontPosture = italic ? FontPosture.ITALIC : FontPosture.NORMAL;
			newFormat.fontDescription = newFontDescription;
			for each (var element:TextElement in _elements){
				if(element.elementFormat === this._specialFormatElement)
					continue;
				
				element.elementFormat = newFormat;
			}
		}
		
		protected function createLines():void 
		{
			var textLine:TextLine = _block.createTextLine(null, _width);
			
			var _y:Number = 0;
			while(textLine)
			{
				_holder.addChild(textLine);
				_y += textLine.height;
				textLine.y = _y;
				textLine = _block.createTextLine(textLine, _width);
			}
		}
		
		private function clearLines():void
		{
			while(_holder.numChildren > 0) {
                _holder.removeChildAt(0);
            }
		}
		
		private function invalidate():void
		{
			if (stage)
				stage.invalidate();
			
			addEventListener(Event.RENDER, renderHandler);
			addEventListener(Event.ENTER_FRAME, renderHandler);
		}
		
		private function validateNow():void
		{	
			setFlag(FLAG_VALIDATE_IN_PROGRESS);
			
			// TODO: there may be a way to do this without casting the ContentElement
			//  and this only replaces the first element
			if (testFlag(FLAG_TEXT_SET))
				TextElement(_elements[0]).text = _text;
			if (testFlag(FLAG_FORMAT_INVALID)) updateFontDescription();
			if (testFlag(FLAG_TEXT_LINES_INVALID | FLAG_GRAPHICS_INVALID)) updateElementFormat();
			clearLines();
			createLines();
			
			clearFlag(ALL_INVALIDATION_FLAGS | FLAG_VALIDATE_IN_PROGRESS);
		}
		
		private function renderHandler(event:Event):void
		{
			validateNow();
			
			removeEventListener(Event.RENDER, renderHandler);
			removeEventListener(Event.ENTER_FRAME, renderHandler);
		}
		
		protected function testFlag(mask:uint):Boolean
		{
			return (_flags & mask) != 0;
		}
		
		protected function setFlag(mask:uint):void
		{
			_flags |= mask;
		}
		
		protected function clearFlag(mask:uint):void
		{
			_flags &= ~mask;
		}
		
		private function setFlagToValue(mask:uint, value:Boolean):void
		{
			if (value)
				_flags |= mask;
			else
				_flags &= ~mask;
		}
		
		public function get textHeight():Number
		{
			return _textHeight;
		}
		
		public function get textWidth():Number
		{
			return _textWidth;
		}
		
		public function get leading():Number{
			return _leading;
		}
		
		
		public function set textHeight(value:Number):void
		{
			_textHeight = value;
		}
		
		public function set textWidth(value:Number):void
		{
			_textWidth = value;
		}
		
		public function set leading(value:Number):void{
			_leading = value;
		}
		
		public function get textFormat():Object
		{
			return this.cloneElementFormat;
		}
		
		public function set textFormat(value:Object):void
		{	
			//to do: implement
		}
		
		public function setPaddingParams(padding:Object):void
		{	
			//implemented by children
		}
		
		public function getPaddingParams():Object
		{	
			//implement by children
			return null;
		}
		
		public function set edit(value:Boolean):void
		{
			//implemented by children
		}
		
		public function get edit():Boolean
		{
			return false;
		}
		
		private function drawBackground():void
		{	
			//needed for mousedown listener
            graphics.beginFill(0x00000,0);
            graphics.drawRect(0,0,_textWidth, _textHeight);
            graphics.endFill();
		}
		
		public function get expandable():Boolean
		{
			return _expandable;
		}
		
		public function set expandable(value:Boolean):void
		{
			_expandable = value;
		}

        public function getTextBounds(space:DisplayObjectContainer):Rectangle
        {
            validateNow();
            return _holder.getBounds(space)
        }
	}
}