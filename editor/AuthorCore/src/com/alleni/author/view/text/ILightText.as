package com.alleni.author.view.text
{
	import com.alleni.author.controller.text.LightTextMediator;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.text.engine.ElementFormat;
	
	public interface ILightText
	{
		function get text():String;
		function set text(value:String):void;
		function get x():Number;
		function get y():Number;
		function set x(value:Number):void;
		function set y(value:Number):void;
		function get textHeight():Number;
		function get textWidth():Number;
		function set textHeight(value:Number):void;
		function set textWidth(value:Number):void;
		function set editable(value:Boolean):void;
		function get editable():Boolean;
		function set color(value:uint):void;
		function get color():uint;
		function set bold(value:Boolean):void;
		function get bold():Boolean;
		function set italic(value:Boolean):void;
		function get italic():Boolean;
		function get textFormat():Object;
		function set textFormat(value:Object):void;
		function setPaddingParams(padding:Object):void;
		function getPaddingParams():Object;
		function set edit(value:Boolean):void;
		function get edit():Boolean;
		function get mediator():LightTextMediator;
		function get parent():DisplayObjectContainer;
		function setVisible(value:Boolean):void;
		function set explicitLineBreak(value:Boolean):void;
		function get explicitLineBreak():Boolean;
		function get textAlign():String;
		function set textAlign(value:String):void;
		function get expandable():Boolean;
		function set expandable(value:Boolean):void;
		function get leading():Number;
		function set leading(value:Number):void;
	}
}