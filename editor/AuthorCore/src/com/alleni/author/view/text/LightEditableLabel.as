package com.alleni.author.view.text
{
	import com.alleni.author.controller.text.LightTextMediator;
	import com.alleni.author.definition.text.FormattedText;
    import com.alleni.author.view.ui.RibbonValueField;
    import com.alleni.author.view.ui.RibbonView;

import flash.events.Event;
	import flash.text.engine.ElementFormat;
	import flash.text.engine.TextElement;
	import flash.text.engine.TextLine;
	
	import flashx.textLayout.formats.TextAlign;
	
	public class LightEditableLabel extends LightEditableTextBase implements ILightEditableText
	{	
		private var _multiLine:Boolean = false;
		private var _height:Number = 0;
		private var _edit:Boolean;
		private var _id:String;  //to do: remove after debugging
		public var propertyType:String;
		private var _formattedText:*;
		private var MARGIN:uint = 1;
		private const PROMPT_COLOR:uint = 0x7f7f7f;
		private var _originalColor:uint = 0xFFFFFF;
		private var _prompt:String = "";
		private var _expandedWidth:Number = -1;
		private var _tempValue:String = "";
		
		private var _padding:Object;
		
		public  var embedFonts:Boolean = true;
		
		public function LightEditableLabel(textWidth:Number = 100, textHeight:Number = 25, multiLine:Boolean = false, propertyType:String = null)
		{
			setPaddingParams({left:MARGIN, top:3, right:MARGIN, bottom:0}); //top must be 3
			super(textWidth,textHeight);
			_multiLine = multiLine;
			this.propertyType = propertyType;
			editable = true;
		}

		public function get prompt():String
		{
			return _prompt;
		}

		
		public function set prompt(value:String):void
		{
			_prompt = value;
			if(_prompt.length > 0)
				addEventListener(Event.ENTER_FRAME, setPrompt);
		}
		
		private function setPrompt(event:Event):void{
			removeEventListener(Event.ENTER_FRAME, setPrompt);
			super.color = PROMPT_COLOR;
			super.text = _prompt;
			//formatRange(0,_prompt.length - 1,PROMPT_COLOR,false, true);  //this should work, but doesn't
		}
		
		private function setText(event:Event):void{
			removeEventListener(Event.ENTER_FRAME, setText);
			super.text = _tempValue;
		}

		override protected function createTextElement(elementFormat:ElementFormat, text:String=null):TextElement
		{
			_mediator = new LightTextMediator(this);
			return new TextElement(_text, elementFormat,_mediator);
		}
		
		public function italicizePrefix(endIndex:int=-1, color:int=-1):void
		{
			formatRange(-1, endIndex, color, false, true);
		}
		
		override protected function createLines():void 
		{
			if(_width < 0)
				return;
			
			var textLine:TextLine = _block.createTextLine(null, _width);
	
			var _y:Number = 0;
			while(textLine)
			{
				_holder.addChild(textLine);
				_y += textLine.height * leading;
				
				var padding:Object = getPaddingParams();
				if(padding.hasOwnProperty("left"))
					textLine.x = padding.left;
				
				if(textAlign == TextAlign.CENTER)
					textLine.x = (width - textLine.width)/2;
				
				if(textAlign == TextAlign.RIGHT)
					textLine.x = (width - textLine.width);
				
				textLine.y = _y;
				textLine = _block.createTextLine(textLine, _width);
				
				if(!_multiLine)
					break;
			}
		}
		
		override public function setPaddingParams(padding:Object):void
		{
			_padding = padding;
		}
		
		override public function getPaddingParams():Object
		{
			return _padding;
		}
		
		override public function setVisible(value:Boolean):void
		{	
			this.visible = value;
			
			if(_mediator)
				_mediator.setEditorVisible(!value);
		}
		
		override public function set edit(value:Boolean):void
		{
			_edit = value;
			_mediator.setEditorVisible(value);
			this.visible = !value;
			
			if(value) {
				_mediator.removeListeners();
			} else {
				_mediator.addListeners();
			}
		}
		
		override public function get edit():Boolean
		{
			return _edit;
		}
		
		/*
		 Prompts use a different color than text;  The prompt sets color to prompt color and then calls the superclasses text setter.  This override set the color back for normal,
		non-prompt values.
		*/
		override public function set text(value:String):void
		{
			
			
			if(!isNaN(_originalColor))
		 		color = _originalColor;
			
			if(value == "" && prompt != ""){
			  	prompt = _prompt;
			}
			else{
				super.text = value;  //immediately set parent text, but also set on enter frame so that text displays properly
				_tempValue = value;
				addEventListener(Event.ENTER_FRAME, setText);
				
			}
		}
		
		public function get formattedText():*
		{
			return _formattedText;
		}
		
		public function set formattedText(value:*):void
		{
			_formattedText = value;
			
			if(value as FormattedText)
				text = (value as FormattedText).toString();
		}
		
		public function get firstLine():TextLine
		{	
			return _block.firstLine;
		}
		
		public function get lastLine():TextLine
		{	
			return _block.lastLine;
		}
		
		/*
		Save the original color in order to restore the color after setting a prompt for this field.
		*/
		
		override public function set color(value:uint):void
		{
			_originalColor = value;
			super.color = value;
		}
		
		//sets the minimum width of the expanded editor; if its -1, it is ignored
		public function set expandedWidth(value:Number):void{
			_expandedWidth = value;
		}
		
		public function get expandedWidth():Number{
			return _expandedWidth;
		}

        public function get associatedRibbonView():RibbonView {
            if (parent as RibbonValueField == null) {
                return null;
            } else {
                return (parent as RibbonValueField).parent as RibbonView;
            }
        }
	}
}