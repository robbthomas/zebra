package com.alleni.author.view.text
{
	import com.alleni.author.controller.text.LightTextMediator;
	
	public class LightEditableTextBase extends LightTextBase implements ILightText
	{
		private static const EDITABLE:uint 	= 1 << 8;
		
		protected var _mediator:LightTextMediator;
		
		public function LightEditableTextBase(textWidth:Number=100, textHeight:Number = 25, color:uint = 0xFFFFFF)
		{
			super(textWidth, textHeight, color);
		}
		
		public function get mediator():LightTextMediator
		{
			return _mediator;
		}
		
		public function setVisible(value:Boolean):void
		{	
			this.visible = value;	
		}
		
		public function set editable(value:Boolean):void
		{
			if(value){
				setFlag(EDITABLE);
			} else{
				clearFlag(EDITABLE);
			}
		}
		
		public function get editable():Boolean
		{
			return testFlag(EDITABLE);
		}
	}
}