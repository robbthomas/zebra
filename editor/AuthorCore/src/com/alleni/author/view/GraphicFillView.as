package com.alleni.author.view
{
	import com.alleni.author.model.GraphicFill;
	
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.InterpolationMethod;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	
	public class GraphicFillView extends Sprite
	{
		
		public function GraphicFillView()
		{
			super();
		}
		
		public function update(fill:GraphicFill, fillWidth:Number, fillHeight:Number, fillAlpha:Number=1.0, cornerRadius:Number=0, lineThick:Number=0, lineColor:Number=0, lineAlpha:Number=1, test:Boolean=false):void
		{
			graphics.clear();
			
			if (fill.type != GraphicFill.JELLYBEAN_FILL) {
				if (_jellyBean) {
					removeChild(_jellyBean);
					_jellyBean = null;
					_jellyBkgnd = null;
					_jellyShine = null;
				}
			}
			
			if (lineThick > 0)
				graphics.lineStyle(lineThick, lineColor, lineAlpha);
			
			switch (fill.type) {
				case GraphicFill.SOLID_FILL:
				case GraphicFill.GRADIENT_FILL:
					beginGraphicFill(graphics, fill, fillWidth, fillHeight, 0,0, fillAlpha, test);
					break;
				case GraphicFill.JELLYBEAN_FILL:
					if (test) {
						graphics.beginFill(fill.color1, 1);
						if (_jellyBean)
							_jellyBean.alpha = 0;
					} else {
						drawJellyBean(fillWidth, fillHeight, fill.color1, fill.color2, cornerRadius, fill.flipGlare, lineThick);
						_jellyBean.alpha = fillAlpha;
					}
					if (lineThick > 0){
						fillAlpha = Math.max(fillAlpha,.01);
						graphics.beginFill(lineColor,fillAlpha); // ensure no crack between frame & fill
					}
					break;  // drop thru to draw the frame, if any
	
			}
			if(cornerRadius < 0)
			{
				graphics.drawEllipse(0,0,fillWidth,fillHeight);
			}else{
			 graphics.drawRoundRect(0,0, fillWidth, fillHeight, cornerRadius);
			}
			
			graphics.endFill();
		}
		
		public static function beginGraphicFill(graphics:Graphics, fillModel:GraphicFill, fillWidth:Number, fillHeight:Number, tx:Number, ty:Number, fillAlpha:Number=1.0, test:Boolean=false):void
		{
			if (fillModel) {
				switch (fillModel.type) {
					case GraphicFill.SOLID_FILL:
						graphics.beginFill(fillModel.color, fillAlpha);
						break;
					case GraphicFill.GRADIENT_FILL:
					case GraphicFill.JELLYBEAN_FILL:
						if (test)
							graphics.beginFill(fillModel.convertToColor(), fillAlpha);
						else
							beginGradientFill(graphics, fillModel, fillWidth, fillHeight, tx,ty, fillAlpha);
						break;
				}
			}
		}
		
		
		private static function beginGradientFill(graphics:Graphics, fillModel:GraphicFill, fillWidth:Number, fillHeight:Number, tx:Number, ty:Number, fillAlpha:Number):void
		{
			var colorArray:Array = new Array();
			if (fillModel.type == GraphicFill.JELLYBEAN_FILL) {
				colorArray[0] = fillModel.color1;  // in case jellybean is applied to an object not using a GraphicFillView
				colorArray[1] = fillModel.color2;
			} else {
				for(var i:int; i < fillModel.colors.length; ++i)
					colorArray[i] = fillModel.colors[i];
			}

			var band:Number = 255 / (colorArray.length-1);  // 3 colors = bands 128 wide
			
			var ratios:Array = [];
			var alphas:Array = [];
			for (var n:int=0; n < colorArray.length; n++) {
				ratios[n] = n * band;
				alphas[n] = fillAlpha;
			}
			
			var degrees:Number = 0;
			if (fillModel.type == GraphicFill.JELLYBEAN_FILL) {
				degrees = GraphicFill.JELLYBEAN_GRADIENT_ANGLE;
			} else if (fillModel.linear && (fillModel.angle > 0 || fillModel.angle < 0)) {
				degrees = fillModel.angle;  // angle in radians
			} else {  // radial gradient
				tx += fillModel.xOffset * fillWidth / 200;  // 100% takes you from center to edge
				ty += fillModel.yOffset * fillHeight / 200;
			}
			
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(fillWidth, fillHeight, degrees * Math.PI/180, tx, ty);
						
			var gradientType:String = fillModel.linear ? GradientType.LINEAR : GradientType.RADIAL;
			graphics.beginGradientFill(gradientType, colorArray, alphas, ratios, matrix, SpreadMethod.PAD, InterpolationMethod.LINEAR_RGB);
		}
		
		
		// Jelly Bean fill
		// This code written by Aaron E Silvers but has been altered by Allen to resolve some issues
		// http://www.aaronsilvers.com/
		// used under Creative Commons with Attribution 3.0
		private var _jellyBean:Sprite = null;
		private var _jellyBkgnd:Shape = null;
		private var _jellyShine:Shape = null;
		
		private function drawJellyBean(www:Number, hhh:Number, color1:uint, color2:uint, radius:Number, flip:Boolean, lineThick:Number):void
		{
			// FEEL FREE TO CHANGE THESE VALUES, AS THE REST OF THE CODE WILL ADAPT
			var btnW:int = www - lineThick; 		// Button Width
			var btnH:int = hhh - lineThick; 		// Button Height
			var colors:Array = [color1, color2];	// The main gradient of your button
			var intX:int = lineThick/2;				// X-Position of your button's upper-left corner
			var intY:int = lineThick/2;				// Y-Position of your button's upper-left corner

            var isEllipse:Boolean = radius < 0;     // is this an ellipse before we take off the lineThickness?
			radius -= lineThick;
			if (radius < 0 && !isEllipse)
				radius = 0;

			// PLAY WITH THESE AT YOUR OWN PERIL
			var fillType:String = GradientType.LINEAR;
			var alphas:Array = [1, 1];
			var ratios:Array = [0, 245];
			
			// TRY TO LEAVE EVERYTHING ELSE ALONE...
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox( btnW, btnH, 90/180*Math.PI, 0, 0 );
			var spreadMethod:String = SpreadMethod.PAD;
			
			if (!_jellyBean) {
				_jellyBean = new Sprite();
				this.addChild( _jellyBean );
				_jellyBkgnd = new Shape();
				_jellyBean.addChild(_jellyBkgnd);
				_jellyShine = new Shape();
				_jellyBean.addChild(_jellyShine);
			}
			
			_jellyBean.x = intX;
			_jellyBean.y = intY;
			
			// BUTTON BACKGROUND
			_jellyBkgnd.graphics.clear();
			_jellyBkgnd.graphics.lineStyle(0, 0, 0.3, true, "none", "square", "round");
			_jellyBkgnd.graphics.beginGradientFill(fillType, colors, alphas, ratios, matrix, spreadMethod );
			
			if(radius < 0)
			{
				_jellyBkgnd.graphics.drawEllipse(0,0,btnW,btnH);
			}else{
				_jellyBkgnd.graphics.moveTo(radius, 0);
                _jellyBkgnd.graphics.drawRoundRect(0,0,btnW, btnH,radius);
			}
			
			_jellyBkgnd.graphics.endFill();
			
			
						
			// THE SHINY GLASSY EFFECT
			_jellyShine.graphics.clear();
			var shineRadius:Number = radius * 2 / 3;
			var shineW:Number = btnW - ( 0.0333 * btnW );
			var shineH:Number = btnH - ( 0.0667 * btnH );
			var shineFillType:String = GradientType.LINEAR;
			var shineColors:Array = [0xFFFFFF, 0xFFFFFF];
			var shineAlphas:Array = [0.7, 0];
			var shineRatios:Array = [0, 111];
            if (flip) {
                shineAlphas.reverse();
                shineRatios = [111,255];
            }

			var shineMatrix:Matrix = new Matrix();
			shineMatrix.createGradientBox( shineW, shineH, 90/180*Math.PI, 0, 0 );

            
			_jellyShine.rotation = 0;
			_jellyShine.alpha = (flip? 0.8 : 1.0);
			_jellyShine.graphics.lineStyle(0, 0xFFFFFF, 0);
			_jellyShine.graphics.beginGradientFill(shineFillType, shineColors, shineAlphas, shineRatios, shineMatrix, spreadMethod );
			
			if(radius < 0)
			{
				var eBtnW:Number = btnW*5/6;
				var eBtnH:Number = btnH*2/3;
				_jellyShine.x = (btnW/2)-(eBtnW/2);
				_jellyShine.y = btnH*.025;
				_jellyShine.graphics.drawEllipse(0,0,eBtnW,eBtnH);
			}else{
				var shineX:Number = ( 0.0333 * btnW ) / 2;
				var shineY:Number = ( 0.0667 * btnH ) / 2;
                _jellyShine.graphics.drawRoundRect(shineX,shineY,shineW,shineH,shineRadius);
			}

			_jellyShine.graphics.endFill();
		}
	}
}