/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: LineFeedback.as 5981 2010-09-09 02:52:48Z sbirth $  */

package com.alleni.author.view.feedback
{
    import com.alleni.author.view.ObjectView;
    import com.alleni.taconite.model.TaconiteModel;
    import com.alleni.taconite.view.ViewContext;


    /**
     * feedback controls for line objects
     * 
     */
    public class RectangleBoxFeedback extends ObjectBoxFeedback
    {
				
        public function RectangleBoxFeedback(context:ViewContext, objectView:ObjectView, model:TaconiteModel=null)
        {
            super(context, model, objectView);
        }
    }
}