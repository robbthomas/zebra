package com.alleni.author.view.feedback
{
	import com.alleni.author.controller.CompositeFeedbackMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.PublishIcons;
	import com.alleni.author.model.objects.Composite;
	import com.alleni.author.view.ObjectView;
import com.alleni.taconite.model.Notification;
import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
import flash.events.Event;
import flash.filters.DropShadowFilter;
	
	import mx.events.PropertyChangeEvent;
	
	public class CompositeFeedback extends ObjectBoxFeedback
	{
		private var _publishButtonContainer:Sprite;
		private var _publishedButton:DisplayObject;
		private var _unpublishedButton:DisplayObject;

        public var mediator:CompositeFeedbackMediator;
		
		public function CompositeFeedback(context:ViewContext, model:TaconiteModel, view:ObjectView)
		{
			super(context, model, view);
			
			_publishButtonContainer = new Sprite();
			_publishedButton = new SimpleButton(PublishIcons.feedbackCloudPublished, PublishIcons.feedbackCloudPublishedOver, PublishIcons.feedbackCloudPublishedDown, PublishIcons.feedbackCloudPublished);
			_unpublishedButton = new SimpleButton(PublishIcons.feedbackCloudUnpublished, PublishIcons.feedbackCloudUnpublishedOver, PublishIcons.feedbackCloudUnpublishedDown, PublishIcons.feedbackCloudUnpublished);
        }

        override public function warnViewToDelete():void {
            super.warnViewToDelete();
            mediator = null;
        }

        override protected function createChildren():void
		{
			super.createChildren();
			
			_publishButtonContainer.filters = [new DropShadowFilter(2, 80, 0, 0.9, 3, 3, 0.9, 2)];
			addChild(_publishButtonContainer);
			mediator = new CompositeFeedbackMediator();
            mediator.handleFeedbackEvents(_objectView, _publishButtonContainer);
		}
		
		override public function handleRunning(running:Boolean):void
		{
			super.handleRunning(running);
			_publishButtonContainer.visible = !running;
		}

        private function handleUpdate(event:Event):void{
            updateView();
        }

		override protected function updateView():void
		{
			super.updateView();
			
			var composite:Composite = model.value as Composite;
			if (!composite) return;
			
			_publishButtonContainer.x = _handles[0].x + PublishIcons.SIZE * Math.cos(composite.rotation*Math.PI/180);
			_publishButtonContainer.y = _handles[0].y + PublishIcons.SIZE * Math.sin(composite.rotation*Math.PI/180);
			
			if (composite.gadget && composite.gadget.published) {
				if (!_publishButtonContainer.contains(_publishedButton)) {
					while (_publishButtonContainer.numChildren > 0)
						_publishButtonContainer.removeChildAt(0);
					_publishButtonContainer.addChild(_publishedButton);
				}
			} else {
				if (!_publishButtonContainer.contains(_unpublishedButton)) {
					while (_publishButtonContainer.numChildren > 0)
						_publishButtonContainer.removeChildAt(0);
					_publishButtonContainer.addChild(_unpublishedButton);
				}
			}
		}
	}
}
