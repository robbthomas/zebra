/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 2/8/12
 * Time: 6:56 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.feedback {
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.geom.Point;
import flash.geom.Rectangle;

public class ArenaInsertionFeedback extends Sprite {

    private var container:DisplayObjectContainer;
    private var triangle1:Sprite = new Sprite();
    private var triangle2:Sprite = new Sprite();

    public var insertIndicatorColor:uint = 0x990000;
    public var insertIndicatorWeight:Number = 3;

    /**
     * Constructor
     * @param worldView
     *
     */
    public function ArenaInsertionFeedback(container:DisplayObjectContainer)
    {
        this.container = container;

        addChild(triangle1);
        addChild(triangle2);
    }

    /**
     * Clears out graphics and sets visibility to false.
     *
     */
    public function clear():void
    {
        graphics.clear();
        triangle1.graphics.clear();
        triangle2.graphics.clear();
        visible = false;
    }

    /**
     * Draws the insert graphic based on dimensions of currently
     * @param arena
     * @param bounds:Rectangle - worldView coordinates
     * @param selected
     *
     */
    public function drawInsertFeedback(pt1:Point, pt2:Point, rotate:Number, growHorz:Boolean):void
    {
        var weight:Number = insertIndicatorWeight;
        var tWeight:Number = 4 + (4*(weight/10));

        this.visible = true;

        graphics.clear();
        graphics.lineStyle(weight, insertIndicatorColor,1);

        if(growHorz)
        {
            /**
             * HORIZONTAL
             */
            graphics.moveTo(pt1.x,pt1.y);
            graphics.lineTo(pt2.x,pt2.y);

            triangle1.x = pt1.x;
            triangle1.y = pt1.y;
            triangle1.rotation = rotate;
            triangle1.graphics.beginFill(insertIndicatorColor,1);
            triangle1.graphics.lineTo(-tWeight,-tWeight);
            triangle1.graphics.lineTo(-tWeight, tWeight);
            triangle1.graphics.lineTo(0,0);

            triangle2.x = pt2.x;
            triangle2.y = pt2.y;
            triangle2.rotation = rotate+180;
            triangle2.graphics.beginFill(insertIndicatorColor,1);
            triangle2.graphics.lineTo(-tWeight,-tWeight);
            triangle2.graphics.lineTo(-tWeight, tWeight);
            triangle2.graphics.lineTo(0,0);

        }else{
            /**
             * VERTICAL
             */
            graphics.moveTo(pt1.x,pt1.y);
            graphics.lineTo(pt2.x,pt2.y);

            triangle1.x = pt1.x;
            triangle1.y = pt1.y;
            triangle1.rotation = rotate+90;
            triangle1.graphics.beginFill(insertIndicatorColor,1);
            triangle1.graphics.lineTo(-tWeight,-tWeight);
            triangle1.graphics.lineTo(-tWeight, tWeight);
            triangle1.graphics.lineTo(0,0);

            triangle2.x = pt2.x;
            triangle2.y = pt2.y;
            triangle2.rotation = rotate-90;
            triangle2.graphics.beginFill(insertIndicatorColor,1);
            triangle2.graphics.lineTo(-tWeight,-tWeight);
            triangle2.graphics.lineTo(-tWeight, tWeight);
            triangle2.graphics.lineTo(0,0);
        }

    }
}
}
