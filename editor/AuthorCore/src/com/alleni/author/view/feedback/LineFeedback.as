/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.feedback
{
    import com.alleni.author.view.ObjectView;
    import com.alleni.taconite.definition.HandleDescription;
    import com.alleni.taconite.definition.HandleRoles;
    import com.alleni.taconite.model.TaconiteModel;
    import com.alleni.taconite.view.SelectionHandle;
    import com.alleni.taconite.view.ViewContext;
    
    import flash.geom.Point;


    /**
     * feedback controls for line objects
     * 
     */
    public class LineFeedback extends ObjectFeedback
    {
        public function LineFeedback(context:ViewContext, objectView:ObjectView, model:TaconiteModel=null)
        {
            super(context, model, objectView);
        }
        
        override protected function createChildren():void
        {
            super.createChildren();
            
            // create the resize handles
            _handleDescriptions.push( new HandleDescription( HandleRoles.RESIZE_LEFT+HandleRoles.RESIZE_UP,
														ZERO ,
														ZERO ) ); 
														
            _handleDescriptions.push( new HandleDescription( HandleRoles.RESIZE_RIGHT+HandleRoles.RESIZE_DOWN,
														new Point(100,100) ,
														ZERO ) ); 
			
			
			_handleDescriptions.push( new HandleDescription( HandleRoles.ROTATE,
														new Point(100,100) , 
														new Point(20,0) ) );
            
			createHandles();
       	}

        override protected function updateView():void
        {
			if (parent == null)
				return;
			var anchor:Point = parent.globalToLocal(_objectView.localToGlobal(new Point(0, 0)));
			x = anchor.x;
			y = anchor.y;
			rotation = _objectView.computeNetRotation();
			_anchorHandle.x = 0;
			_anchorHandle.y = 0;
			
			var topLeft:Point = globalToLocal(_objectView.localToGlobal(new Point(object.left, object.top)));
			var botRight:Point = globalToLocal(_objectView.localToGlobal(new Point(object.right, object.bottom)));
			var www:Number = botRight.x - topLeft.x;
			var hhh:Number = botRight.y - topLeft.y;

           	for each ( var handle:SelectionHandle in _handles )
			{
				
				var factor:int = handle.descriptor.role == 16 && object.left > 0 ? -1: 1;
				handle.x = topLeft.x + handle.descriptor.percentageOffset.x/100 * www + (factor * handle.descriptor.offset.x);
				handle.y = topLeft.y + handle.descriptor.percentageOffset.y/100 * hhh + handle.descriptor.offset.y;
			}
        }
    }
}