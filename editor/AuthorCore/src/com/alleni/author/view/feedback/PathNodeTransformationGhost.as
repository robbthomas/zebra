package com.alleni.author.view.feedback
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.TransformationGhostDragMediator;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.LogicObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.editors.TransformationEditor;
	import com.alleni.taconite.definition.HandleRoles;
	import com.alleni.taconite.definition.Handles;
	import com.alleni.taconite.view.SelectionHandle;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.CapsStyle;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.display.Sprite;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class PathNodeTransformationGhost extends TransformationGhost {
		
		public function PathNodeTransformationGhost(transformationEditor:TransformationEditor, transformation:PositionAndAngle, target:AbstractObject=null) {
			super(transformationEditor,transformation,target);
		}
		
		override public function draw():void {
			
			rotationHandle.visible = false;
			
			graphics.clear();
			graphics.beginFill(0x000000, 0.25);
			graphics.lineStyle(1, 0xaaaaaa, .9, false, LineScaleMode.NONE);
			graphics.drawRect(-15, -15, 30, 30);
			graphics.endFill();
			graphics.beginFill(0xFFFFFF, 0.50);
			graphics.drawCircle(0,0, 5);
			graphics.endFill();
			drawCross(3, 0xFFFFFF);
		}
		
		override public function drawCross(thickness:Number, color:uint):void {
			
		}
		
		override public function set transformation(value:PositionAndAngle):void {
			
			var pt:Point = WorldContainer(Application.instance.viewContext).feedbackHandlesView.globalToLocal(
				target.getView(Application.instance.viewContext).parent.localToGlobal(new Point(value.x,value.y))
			);
			_transformation = value;
			this.x = pt.x;
			this.y = pt.y;
			this.rotation = _transformation.angle;
		}
		
		override public function get editor():TransformationEditor {
			return _transformationEditor;
		}
	}
}