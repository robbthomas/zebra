package com.alleni.author.view.feedback
{
	public interface IFeedback
	{
		function render():void;
		function get visible():Boolean;
	}
}