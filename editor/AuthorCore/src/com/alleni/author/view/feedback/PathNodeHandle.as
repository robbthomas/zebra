/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.view.feedback
{
	import com.alleni.author.controller.PathHandleDragMediator;
	import com.alleni.author.model.objects.PathNode;
	import com.alleni.author.model.objects.PathObject;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.objects.PathObjectView;
	import com.alleni.taconite.view.SelectionHandle;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.CapsStyle;
	import flash.display.LineScaleMode;

	public class PathNodeHandle extends SelectionHandle
	{
		private var _path:PathObject;
		private var _node:PathNode;
		private var _mediator:PathHandleDragMediator;
		
		private static const FILL_COLOR:uint = 0xffffff;
		private static const ROLL_COLOR:uint = 0x404040;
		private static const SELECTED_COLOR:uint = 0x56bfff;
		private static const FIRST_COLOR:uint = 0x00ff00;
		private static const LAST_COLOR:uint = 0xff0000;
		
		private static const LINE_COLOR:uint = 0x7f7f7f;

		
		public function PathNodeHandle(context:ViewContext, role:uint, pathView:PathObjectView, node:PathNode)
		{
			_path = pathView.model.value as PathObject;
			_node = node;
			super(context, role);
			_mediator = new PathHandleDragMediator(context);
			_mediator.handleViewEvents(this, pathView);
			this.doubleClickEnabled = true;
		}
		
		public function warnViewToDelete():void
		{
			_mediator.clearViewEvents();
		}
		
		override protected function updateGraphics():void
		{
			var index:int = nodeIndex;
			if (index < 0)
				return;  // rollout can cause update after deletion of node
			const L:Number = 4;  // triangle or circle
			
			graphics.clear();
			
			var color:uint;
			if (_node.selected)
				color = SELECTED_COLOR;
			else if (rolled)
				color = ROLL_COLOR;
			else if (index == 0)
				color = FIRST_COLOR;
			else if (index == _path.nodeCount-1)
				color = LAST_COLOR;
			else
				color = FILL_COLOR;
			
			graphics.lineStyle(1, LINE_COLOR, 0.4, false, LineScaleMode.NONE, CapsStyle.ROUND);
			graphics.beginFill(color, 1);

			if (_node.sharp) {
				graphics.moveTo(0,-L);
				graphics.lineTo(1.5*L,L);
				graphics.lineTo(-1.5*L,L);
				graphics.lineTo(0,-L);
			} else {
				graphics.drawCircle(0,0, L);
			}
			graphics.endFill();
		}
		
		public function update():void
		{
			updateGraphics();
		}
		
		public function get node():PathNode
		{
			return _node;
		}
		
		public function get nodeIndex():int
		{
			return _path.path.indexOfNode(_node);
		}
	}
}