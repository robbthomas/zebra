package com.alleni.author.view.feedback {
    import com.alleni.author.Navigation.EventPage;
    import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.TransformationGhostDragMediator;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.LogicObject;
	import com.alleni.author.model.ui.Application;
    import com.alleni.author.util.ViewUtils;
    import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.editors.TransformationEditor;
	import com.alleni.taconite.definition.HandleRoles;
	import com.alleni.taconite.definition.Handles;
    import com.alleni.taconite.dev.BitmapUtils;
    import com.alleni.taconite.view.SelectionHandle;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.CapsStyle;
import flash.display.DisplayObject;
import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.display.Sprite;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class TransformationGhost extends Sprite {
		protected var _transformationEditor:TransformationEditor;
		protected var _transformation:PositionAndAngle;
		protected var _bitmap:Bitmap;
		
		
		public var target:AbstractObject;
		public var rotationHandle:SelectionHandle;
	
		public function TransformationGhost(transformationEditor:TransformationEditor, transformation:PositionAndAngle, target:AbstractObject=null) {
			_transformationEditor = transformationEditor;
			this.target = target;
			this.transformation = transformation;
			
			rotationHandle = new SelectionHandle(Application.instance.viewContext, HandleRoles.ROTATE);
			addChild(rotationHandle);
			
			new TransformationGhostDragMediator(Application.instance.viewContext, this, false);
			new TransformationGhostDragMediator(Application.instance.viewContext, this, true);

			draw();
			
		}
		
		private function get bounds():Rectangle {
			if(target == null || target is LogicObject || target is EventPage) {
				return new Rectangle(-20, -20, 40, 40);
			}
			return new Rectangle(target.left, target.top, target.width, target.height);
		}
	
		public function draw():void {
			
			
			if (target && (target is LogicObject == false && target is EventPage == false)) {
				_bitmap = createBitmap(target);
				if (_bitmap) 
					addChild(_bitmap);
			}
			
			
			graphics.clear();
			graphics.beginFill(0x000000, 0.25);
	        graphics.lineStyle(1, 0xaaaaaa, .9, false, LineScaleMode.NONE);
	        graphics.drawRect(bounds.left, bounds.top, bounds.width, bounds.height);
			graphics.endFill();
	
			drawCross(3, 0xFFFFFF);
	
			rotationHandle.x = bounds.right + Handles.ROTATION_HANDLE_OFFSET;
			rotationHandle.y = bounds.bottom;
		}
	
		public function drawCross(thickness:Number, color:uint):void {
			graphics.lineStyle(thickness, color, 0.9, false, "normal", CapsStyle.NONE, JointStyle.MITER);
	
			graphics.moveTo(bounds.left+1, bounds.top + bounds.height/2);
			graphics.lineTo(bounds.right-1, bounds.top + bounds.height/2);
	
			graphics.moveTo(bounds.left + bounds.width/2, bounds.top + thickness*2);
			graphics.lineTo(bounds.left + bounds.width/2, bounds.bottom - 1);
	
			graphics.beginFill(color);
			graphics.lineStyle(undefined);
			graphics.moveTo(bounds.left + bounds.width/2, bounds.top + 1);
			graphics.lineTo(bounds.left + bounds.width/2 + thickness*2, bounds.top + thickness*2);
			graphics.lineTo(bounds.left + bounds.width/2 - thickness*2, bounds.top + thickness*2);
			graphics.lineTo(bounds.left + bounds.width/2, bounds.top + 1);
			graphics.endFill();
		}

		public function set transformation(value:PositionAndAngle):void {
			var pointLocation:DisplayObject;
            var ghostLocation:Point = new Point();
            var ghostRotation:Number = 0;

			if(target is EventPage){
                if(Application.instance.flowVisible) {
                    pointLocation = World(Application.instance.document.root.value).eventFlow.flowView.getModelView(target as EventPage);
                } else {
                    pointLocation = World(Application.instance.document.root.value).eventFlow.eventPagerView.getViewForModel(target as EventPage) as DisplayObject;
                }
            }else{
                pointLocation = target.parent.getView(Application.instance.viewContext);
            }

            if(value != null){
                ghostLocation = new Point(value.x,value.y);
                ghostRotation = value.angle;
            }

            var pt:Point = WorldContainer(Application.instance.viewContext).feedbackHandlesView.globalToLocal(pointLocation.localToGlobal(ghostLocation));

			this.x = pt.x;
			this.y = pt.y;
			this.rotation = ghostRotation;
		}
	
		public function get editor():TransformationEditor {
			return _transformationEditor;
		}
		
		private function createBitmap(obj:AbstractObject):Bitmap
		{
			var view:ObjectView = obj.getView(Application.instance.viewContext);
			if (view) {
                var bitmapData:BitmapData = ViewUtils.instance.takeObjectScreenshot(view)
                var bitmap:Bitmap = new Bitmap(bitmapData);
                bitmap.smoothing = true;
                bitmap.x = bounds.left;
                bitmap.y = bounds.top;
                bitmap.alpha = 0.4;
                var glow:GlowFilter = new GlowFilter(0, 1.0, 20,20, 3, 1);
                bitmap.filters = [glow];

                return bitmap;
			}
			return null;
		}
	}
}