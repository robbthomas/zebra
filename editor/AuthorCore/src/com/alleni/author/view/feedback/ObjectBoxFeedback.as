/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.feedback
{
	import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.definition.Handles;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.SelectionHandle;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.geom.Point;
    
	/**
	 * feedback controls for box objects: marquee, handles, and registration point.
	 * 
	 */
	public class ObjectBoxFeedback extends ObjectFeedback
	{
		private static const BORDER_ALPHA:Number = 0.3;
		protected var _object:AbstractObject;
        
		public function ObjectBoxFeedback(context:ViewContext, model:TaconiteModel, view:ObjectView)
		{
			super(context, model, view);
			_object = model.value as AbstractObject;
		}
        
        override protected function createChildren():void
        { 	
            super.createChildren();
            
            // A default handle description with standard resize and rotation
            _handleDescriptions = Handles.DEFAULT_DEFINITION;
      		
            createHandles();
       	}
		
		
		override protected function updateView():void
		{
			if (_anchorHandle) {
				var anchor:Point = globalToLocal(_objectView.localToGlobal(new Point(0, 0)));
				_anchorHandle.x = anchor.x;
				_anchorHandle.y = anchor.y;
			}
			
			var topLeft:Point = globalToLocal(_objectView.localToGlobal(new Point(_object.left, _object.top)));
			var topRight:Point = globalToLocal(_objectView.localToGlobal(new Point(_object.right, _object.top)));
			var botRight:Point = globalToLocal(_objectView.localToGlobal(new Point(_object.right, _object.bottom)));
			var botLeft:Point = globalToLocal(_objectView.localToGlobal(new Point(_object.left, _object.bottom)));
			
			// Draw a gray selection border around the object (may be skewed, if gadget contains rotated child, and gadget width & height scaled differently)
			graphics.clear();
            if (!Application.uiRunning) {
				graphics.lineStyle(1, 0xaaaaaa, BORDER_ALPHA);
				graphics.moveTo(topLeft.x, topLeft.y);
				graphics.lineTo(topRight.x, topRight.y);
				graphics.lineTo(botRight.x, botRight.y);
				graphics.lineTo(botLeft.x, botLeft.y);
				graphics.lineTo(topLeft.x, topLeft.y);
			}
			
			for each (var handle:SelectionHandle in _handles)
			{
				// setup interpolation between the endpoints of a side
				var xFrac:Number = handle.descriptor.percentageOffset.x / 100;
				var yFrac:Number = handle.descriptor.percentageOffset.y / 100;
				var p1:Point, p2:Point;
				var frac:Number;
				if (yFrac == 0) {  // top side
					p1 = topLeft;
					p2 = topRight;
					frac = xFrac;
				} else if (xFrac == 1) {  // right side
					p1 = topRight;
					p2 = botRight;
					frac = yFrac;
				} else if (yFrac == 1) {  // bottom side
					p1 = botLeft;
					p2 = botRight;
					frac = xFrac;
				} else {  // left side
					p1 = topLeft;
					p2 = botLeft;
					frac = yFrac;
				}
				var point:Point = new Point(p1.x + frac*(p2.x - p1.x), p1.y + frac*(p2.y - p1.y));
				
				if (handle.descriptor.offset.length > 0) {  // offset of rotation handle
					// obey rotation of object, but distance is global pixels
					var global:Point = localToGlobal(point);
					var dx:Number = handle.descriptor.offset.x;
					var dy:Number = handle.descriptor.offset.y;
					var radians:Number = _objectView.computeNetRotation() * Math.PI / 180;
					global.x += dy * Math.sin(radians) + dx * Math.cos(radians);
					global.y += dy * Math.cos(radians) + dx * Math.sin(radians);
					point = globalToLocal(global);
				}
				handle.x = point.x;
				handle.y = point.y;
			}
		}
	}
}
