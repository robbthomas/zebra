/**
 * Created with IntelliJ IDEA.
 * User: mfitch
 * Date: 10/3/12
 * Time: 3:50 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.feedback {

import flash.display.Graphics;

import flash.display.Sprite;
import flash.filters.DropShadowFilter;

public class NotAllowedIcon extends Sprite{
    public function NotAllowedIcon() {
        var shadow:DropShadowFilter = new DropShadowFilter(2, 80, 0, 0.8, 6, 6, 1, 3);
        var g:Graphics = this.graphics;
        g.beginFill(0xffffff);
        g.lineStyle(0,0,0);
        g.drawCircle(0,0,10);
        g.endFill();
        g.lineStyle(2,0xff0000);
        g.beginFill(0xffffff);
        g.drawCircle(0,0,6);
        g.endFill();
        g.moveTo(-3,-3);
        g.lineTo(3,3);
        this.filters = [shadow];
    }
}
}
