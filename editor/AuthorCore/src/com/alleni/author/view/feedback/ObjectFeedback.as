/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.feedback
{
	import com.alleni.author.controller.PointDragMediator;
	import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.definition.HandleDescription;
	import com.alleni.taconite.definition.HandleRoles;
import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.AnchorHandle;
	import com.alleni.taconite.view.SelectionHandle;
	import com.alleni.taconite.view.TaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;
	import flash.geom.Point;

	/**
	 * Abstract class for displaying feedback controls for objects in an editor in their own layer.
	 * 
	 */
	public class ObjectFeedback extends TaconiteView implements IFeedback
	{
		protected const ZERO:Point = new Point(0,0);
		
		protected var _objectView:ObjectView; // object view represented by this feedback
        protected var _handles:Vector.<SelectionHandle> = new Vector.<SelectionHandle>();
    	protected var _handleDescriptions:Vector.<HandleDescription> = new Vector.<HandleDescription>();
        protected var _anchorHandle:AnchorHandle;
//        protected var _pivotHandle:PivotHandle;
		protected var _rotation:Number;  // netRotation of this object, based on rotation of it and all ancestors

		public function ObjectFeedback(context:ViewContext, model:TaconiteModel, view:ObjectView)
		{
			_objectView = view;
			super(context, model);
		}

        override public function initialize():void
        {
            super.initialize();
        }
		
		public function handleRunning(running:Boolean):void
		{
			// note that VectorElementView.updateStatus() controls the visibility of the whole view, based on selection
			for (var i:int = 0 ; i < this.numChildren ; i++) {
				var handle:DisplayObject = this.getChildAt(i) as DisplayObject;
				if (!object.runtimeRotation || (object.runtimeRotation && !object.movable)
					|| !(handle as SelectionHandle && (handle as SelectionHandle).descriptor.role == HandleRoles.ROTATE))
					handle.visible = !running;
			}
			this.render();  // so ObjectBoxFeedback can show/hide the outline
		}
		
		/**
		 * The object that this view presents.
		 */
		public function get object():AbstractObject
		{
			return model.value as AbstractObject;
		}
		
        override protected function createChildren():void
        {
			if ("anchorPoint" in object.anchors) {
	            _anchorHandle = new AnchorHandle(context);
	            addChild(_anchorHandle);
				new PointDragMediator(context, _anchorHandle.descriptor.role).handleViewEvents( _objectView, _anchorHandle);
			}

//            _pivotHandle = new PivotHandle(context);
//           	addChild(_pivotHandle);
       	}

		protected function createHandles():void
		{
			for each ( var descriptor:HandleDescription in _handleDescriptions )
				createHandle(descriptor);
			handleRunning(Application.uiRunning);
		}
		
		private function createHandle( descriptor:HandleDescription ) : void
		{
			// todo: use cached handles for performance.
			var handle:SelectionHandle = new SelectionHandle(context,descriptor.role);
			handle.descriptor = descriptor;
			_handles.push(handle);
			addChild(handle);
			// anchor point as broken circle does not need to be on top.
			//swapChildrenAt(getChildIndex(handle), getChildIndex(_anchorHandle));
//			swapChildrenAt(getChildIndex(handle), getChildIndex(_pivotHandle));
         	new PointDragMediator(context, handle.descriptor.role).handleViewEvents(_objectView, handle);
		}
		
		public function get objectView():ObjectView
		{
			return _objectView;
		}
		
		public function render():void
		{
			updateView();
		}
		
		override public function toString():String
		{	
			var objStr:String = model.value.toString();
			// [ViewClass XY=  Obj= ...]
			return "[" + object.shortClassName + " XY="+x+","+y+ " Obj="+objStr + "]";
		}
	}
}
