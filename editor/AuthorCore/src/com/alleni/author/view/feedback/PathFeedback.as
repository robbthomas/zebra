package com.alleni.author.view.feedback
{
	import com.alleni.author.controller.PathHandleDragMediator;
	import com.alleni.author.controller.objects.PathFeedbackMediator;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.model.objects.Path;
	import com.alleni.author.model.objects.PathNode;
	import com.alleni.author.model.objects.PathNodeInfo;
	import com.alleni.author.model.objects.PathObject;
	import com.alleni.author.model.ui.PathNodeWireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.WorldView;
	import com.alleni.author.view.objects.PathObjectView;
	import com.alleni.taconite.definition.HandleDescription;
	import com.alleni.taconite.definition.HandleRoles;
	import com.alleni.taconite.view.SelectionHandle;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	
	public class PathFeedback extends ObjectFeedback {
	
		private var _view:PathObjectView;
		private var _mediator:PathFeedbackMediator;
		private var _worldView:WorldView;
		private var _flashGraphic:Shape;
		private var _hilitedNode:int = -1;  // anchor nodes currently hilited

		private static const ANCHOR_WIDTH:Number = 14;  // anchor symbol (gray square)

	
		public function PathFeedback(context:ViewContext, view:PathObjectView) {
			super(context, view.model, view);
			_view = view;
			_worldView = WorldContainer(context).worldView;
			_mediator = new PathFeedbackMediator(path, this);
		}
		
		override public function warnViewToDelete():void
		{
            for each (var handle:PathNodeHandle in _handles) {
                handle.warnViewToDelete();
            }
			_mediator.clearViewEvents();
			super.warnViewToDelete();
		}
	
		public function get path():PathObject {
			return model.value as PathObject;
		}
	
		override protected function createChildren():void {
			for(var i:int=0; i<path.nodeCount; i++) {
				addHandle(path.path.nodeForIndex(i));
			}
		}

		public function addHandle(node:PathNode):void
		{
			var handle:PathNodeHandle = new PathNodeHandle(context, ViewRoles.PRIMARY, _view, node);
			_handles.push(handle);
			addChild(handle);
			updateHandle(handle);  // immediate update to prevent blip at topleft
		}
		
		public function removeHandle(node:PathNode):void
		{
			var len:int = _handles.length;
			for (var n:int=0; n < len; n++) {
				var handle:PathNodeHandle = _handles[n] as PathNodeHandle;
				if (handle.node == node) {
					handle.warnViewToDelete();
					removeChild(handle);
					_handles.splice(n, 1);
					return;
				}
			}
		}
		
		public function initialDrawPath(global:Point):void
		{
			var local:Point = _view.curve.globalToLocal(global);
			trace("initialDraw: global="+global, "local="+local);
			_view.path.addPoint(0, local, false);
			updateView();
			
			var hdl:SelectionHandle = _handles[0];
			local = hdl.globalToLocal(global);
			var event:MouseEvent = new MouseEvent(MouseEvent.MOUSE_DOWN);
			event.localX = local.x;
			event.localY = local.y;
			hdl.dispatchEvent(event);  // mouseDown on the handle
		}
	
		override protected function updateView():void
		{
            if (stage == null) return;

			super.updateView();
			
			// node anchors, if any (gray rects)
			redrawAnchors();

			// path markers (triangles & circles)
			for (var i:int=0; i<_handles.length; i++) {
				var handle:PathNodeHandle = _handles[i] as PathNodeHandle;
				updateHandle(handle);
			}
		}
		
		private function updateHandle(handle:PathNodeHandle):void
		{
            var index:int = handle.nodeIndex;
            if (index >= 0) {  // during Undo of a path edit, an obsolete feedback may exist for a moment but will be replaced
                var pt:Point = path.path.getPoint(index);
                pt = this.globalToLocal(_view.curve.localToGlobal(pt));
                handle.x = pt.x;
                handle.y = pt.y;
                handle.update();
            }
		}
		
		public function hiliteAnchor(index:int, hilite:Boolean):void
		{
			if (hilite == false)
				index = -1;
			if (index != _hilitedNode) {
				_hilitedNode = index;
				redrawAnchors();
			}
		}
		
		private function redrawAnchors():void
		{
			graphics.clear();
			for (var n:int=0; n < _handles.length; n++) {
				var info:PathNodeInfo = path.path.getAnchorInfo(n);
				if (info) {
					var point:Point = path.path.getPoint(n);
					point = this.globalToLocal(_view.curve.localToGlobal(point));
					var hilite:Boolean = (n == _hilitedNode);
					drawAnchor(graphics, info.serial, point, hilite);
				}
			}
		}
		
		private function drawAnchor(g:Graphics, serial:int, point:Point, hilite:Boolean):void
		{
			var anchorColor:uint = hilite ? 0x0000ff : 0x000000;
			var anchorAlpha:Number = hilite ? 0.4 : 0.2;
			g.lineStyle(1, 0xffffff,0.4);
			g.beginFill(anchorColor, anchorAlpha);
			g.drawRect(point.x - ANCHOR_WIDTH, point.y - ANCHOR_WIDTH, 2*ANCHOR_WIDTH, 2*ANCHOR_WIDTH);
			g.endFill();
		}
		
		public function setupFlashGraphic(point:Point):Shape
		{
			if (_flashGraphic == null) {
				_flashGraphic = new Shape();
				var g:Graphics = _flashGraphic.graphics;
				g.lineStyle(1);
				oneCircle(g, 30);
				oneCircle(g, 35);
				oneCircle(g, 40);
				addChild(_flashGraphic);
			}
			var pt:Point = _worldView.globalToLocal(_view.curve.localToGlobal(point));
			_flashGraphic.x = pt.x;
			_flashGraphic.y = pt.y;
			return _flashGraphic;
		}
		
		private function oneCircle(g:Graphics, radius:Number):void
		{
			g.lineStyle(3, 0xffffff);
			g.drawCircle(0,0, radius);
			g.lineStyle(1);
			g.drawCircle(0,0, radius);
		}
		
		public function anchorUnderPoint(global:Point):int
		{
			for (var n:int=0; n < _handles.length; n++) {
				if (path.path.getAnchorInfo(n)) {
					var point:Point = path.path.getPoint(n);
					point = _view.curve.localToGlobal(point);
					if (Math.abs(point.x - global.x) <= ANCHOR_WIDTH && Math.abs(point.y - global.y) <= ANCHOR_WIDTH)
						return n;
				}
			}
			return -1;
		}
	}
}