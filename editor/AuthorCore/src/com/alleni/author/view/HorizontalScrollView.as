package com.alleni.author.view
{
	import com.alleni.author.model.ScrollModel;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class HorizontalScrollView extends ScrollView
	{
		public function HorizontalScrollView(model:ScrollModel, scrollControllerWidth:Number, scrollControllerHeight: Number)
		{
			super(model, scrollControllerWidth, scrollControllerHeight);
			
			_model = model;
			_scrollControllerWidth = scrollControllerWidth;
			_scrollControllerHeight = scrollControllerHeight;
			
			init()
			drawScrollBar();
		}
		
		override protected function init():void
		{	
			_documentLength = _model.maxScrollPosition - _model.minScrollPosition;
			_buttonAreaWidth = 34;
			_buttonAreaHeight = 18;
			_scrollControllerHeight = 18; 
			_thumbHeight = 14
			_lineScrollSize = (_documentLength - _model.viewPortLength) / 20;
			 nudgeAmount = 15;
		}
		
		override public function set scrollControllerWidth(value:Number):void
		{
			_scrollControllerWidth = value;
			validate();
		}
		
		override public function set scrollControllerHeight(value:Number):void  //done
		{
			_scrollControllerHeight = value;
			validate();
		}
		
		
		override protected function makeButtons():void
		{				
			// button overlay under both buttons, for variable speed scrolling
			if (_buttonHolder == null) {
				_buttonHolder = new Sprite();
				with (_buttonHolder.graphics) {
					beginFill(0xffffff, 0.01);
					drawRect(0, 0, _buttonAreaWidth, _scrollControllerHeight);
					endFill();
				}
				_scrollBar.addChild(_buttonHolder);
				buttonHolderCreated();
				_buttonHolder.useHandCursor = true;
			}
			_buttonHolder.x = computeButtonAreaX();

			
			if(_moreButton == null){	
				_moreButton = new ScrollButton(
					rotateGraphic(90, getUpStateGraphic()),
					rotateGraphic(90, getDownStateGraphic()),
					rotateGraphic(90, getOverStateGraphic()),
					getHitStateGraphic());
				_buttonHolder.addChild(_moreButton);
			}
			_moreButton.x = _buttonAreaWidth /2;
			_moreButton.y = this._thumbEndPos.y - _moreButton.height/2;
			
			
			if(_lessButton == null){
				_lessButton = new ScrollButton(
					rotateGraphic(-90, getUpStateGraphic()),
					rotateGraphic(-90, getDownStateGraphic()),
					rotateGraphic(-90, getOverStateGraphic()),
					getHitStateGraphic() );
				_buttonHolder.addChild(_lessButton);
			}
			_lessButton.x = 0;
			_lessButton.y = this._thumbEndPos.y - _lessButton.height/2;
		}
		
		protected function computeButtonAreaX():Number
		{
			var gap:uint = 1
			return _scrollControllerWidth - _buttonAreaWidth + gap;  // override by TruthTableScrollView
		}
		
		override protected function offsetFromCenterOfButtons(global:Point):Number
		{
			var local:Point = _buttonHolder.globalToLocal(global);
			return local.x - _buttonAreaWidth /2 +1;  // +1 to give even speed on either side of dead zone
		}
		
		override protected function makeScrollBar():void
		{	
			if(_scrollBar == null){
				_scrollBar = new Sprite();
				this.addChild(_scrollBar);
			}
			
			_scrollBar.graphics.clear();
			_scrollBar.graphics.beginFill(_trackBackgroundColor,1);
			_scrollBar.graphics.drawRect(0,0,_scrollControllerWidth, _scrollControllerHeight);
			_scrollBar.graphics.endFill();
			
			_scrollBar.x = 0;
			_scrollBar.y = 0;
			
		}
		
		override protected function makeTrack():void
		{			
			if(_scrollBar == null)
				return;
			
			if(_track == null){
				_track = new Sprite();
				_scrollBar.addChild(_track);
			}
			
			_track.graphics.clear();
			_track.graphics.beginFill(_trackBackgroundColor,1);
			_track.graphics.drawRect(0,0,_scrollControllerWidth, _scrollControllerHeight);
			_track.graphics.endFill();
			
			_track.x = 0;
			_track.y = 0;
			
			_track.addEventListener(MouseEvent.CLICK, handleTrackClick);
		}
		
		override protected function makeTrackGraphic():void
		{	
			if(_track == null)
				return;	
			
			_track.graphics.lineStyle(_dotSize,_trackForegroundColor);
			
			//first point
			var startPos:Point = new Point(_hMargin, _scrollControllerHeight / 2);
			_track.graphics.moveTo(startPos.x, startPos.y + 3);
			_track.graphics.lineTo(startPos.x, startPos.y - 3);
			
			//second point
			var endPos:Point = new Point(_scrollControllerWidth - _buttonAreaWidth, _scrollControllerHeight / 2);
			_track.graphics.moveTo(endPos.x, endPos.y + 3);
			_track.graphics.lineTo(endPos.x, endPos.y - 3);
			
			//Draw line
			
			_track.graphics.moveTo(startPos.x, startPos.y);
			_track.graphics.lineTo(endPos.x, endPos.y);
			
			
			_thumbStartPos = startPos;
			_thumbEndPos = endPos;
		}
		
		override protected function makeThumb():void
		{	
			var padding:Number = 3;
			
			if(_thumb == null){
				_thumb = new Sprite();
				_thumb.alpha = _thumbAlpha;
				_track.addChild(_thumb);
				_thumb.addEventListener(MouseEvent.ROLL_OVER,handleMouseOver);
				_thumb.addEventListener(MouseEvent.ROLL_OUT,handleMouseOut);
				_thumb.y = _thumbStartPos.y - _thumbHeight / 2;
			}
			else{
				
				this.removeSpriteChildren(_thumb);
			}
			
			var trackLength:Number = Math.abs(_thumbEndPos.x - _dotSize - _thumbStartPos.x);
			
			_thumbHeight  = _scrollControllerHeight - 2 * padding ;
			
			
			_thumbWidth = (trackLength / _documentLength) * trackLength;
			
			if (_thumbWidth > trackLength){
				_thumbWidth = trackLength;
			}
			else if(!(_thumbWidth > 15)){  // small or NaN
				_thumbWidth = 15;
			}
			
			_upState = addThumbUpState();
			_thumb.addChild(_upState);
			
			_overState = addThumbOverState();
			_thumb.addChild(_overState);
			
			_trackExtents = new Rectangle(_thumbStartPos.x + _dotSize, _thumb.y, trackLength - _thumbWidth, 0);
			
			updateThumbPosition();
		}
		
		protected function addThumbUpState():Sprite
		{	
			var _thumbSprite:Sprite = new Sprite();
			
			var roundedEdge:Number = 10;
			
			with(_thumbSprite.graphics){
				beginFill(_thumbColor,1);
				lineStyle(1, _thumbLineColor,1,true);
				drawRoundRect(0,0,_thumbWidth, _thumbHeight, roundedEdge, roundedEdge);
				endFill();
			}
			
			//make center line
			
			if(_thumbSprite.width > _thumbSprite.height){  //don't draw the line if the thumb is too small
				var centerLineLength:Number = _thumbHeight;
				
				var centerPoint:Point = new Point(_thumbWidth/2, _thumbHeight/2);
				
				with(_thumbSprite.graphics){
					
					lineStyle(1, _thumbLineColor);
					moveTo(centerPoint.x - centerLineLength/2, centerPoint.y);
					lineTo(centerPoint.x + centerLineLength/2, centerPoint.y);
				}
			}
			return _thumbSprite;
		}
		
		protected function addThumbOverState():Sprite
		{	
			var _thumbSprite:Sprite = new Sprite();
			
			var roundedEdge:Number = 10;
			
			with(_thumbSprite.graphics){
				beginFill(_thumbOverColor,1);
				lineStyle(1, 0x000000,1,true);
				drawRoundRect(0,0,_thumbWidth, _thumbHeight, roundedEdge, roundedEdge);
				endFill();
			}
			
			if(_thumbSprite.width > _thumbSprite.height){  //don't draw the line if the thumb is too small
				var centerLineLength:Number = _thumbHeight;
				
				var centerPoint:Point = new Point(_thumbWidth/2, _thumbHeight/2);
				
				with(_thumbSprite.graphics){
					lineStyle(1, 0x000000);
					moveTo(centerPoint.x - centerLineLength/2, centerPoint.y);
					lineTo(centerPoint.x + centerLineLength/2, centerPoint.y);
				}
			}			
			_thumbSprite.alpha = 0;
			return _thumbSprite;
		}
		
		override protected function addThumbGraphic(_thumbSprite:Sprite):void
		{	
			var roundedEdge:Number = 20;
			
			with(_thumbSprite.graphics){
				clear();
				beginFill(this._trackBackgroundColor,1);
				lineStyle(1, this._trackForegroundColor,1,true);
				drawRoundRect(0,0,_thumbWidth, _thumbHeight, roundedEdge, roundedEdge);
				endFill();
			}
			
			//make center line
			
			if(_thumbSprite.width > _thumbSprite.height){  //don't draw the line if the thumb is too small
				
				var centerLineLength:Number = _thumb.height;
				
				var centerPoint:Point = new Point(_thumb.width/2, _thumb.height/2);
				
				with(_thumbSprite.graphics){	
					lineStyle(1, this._thumbLineColor);
					moveTo(centerPoint.x - centerLineLength/2, centerPoint.y);
					lineTo(centerPoint.x + centerLineLength/2, centerPoint.y);
				}
			}
		}
				
		override protected function updateThumbPosition():void
		{
			if (!draggingThumb) {
				var frac:Number = (_scrollPosition - _model.minScrollPosition) / (_documentLength - _model.viewPortLength);
				if ((frac >= 0) == false) frac = 0;  // negative or NaN
				if (frac > 1) frac = 1;
				var newPos:Number =  frac * _trackExtents.width;
				_thumb.x = _thumbStartPos.x + newPos;
			}
		}
	
		
		override public function removeSpriteChildren(sprite:Sprite):void
		{
			while(sprite.numChildren > 0){
				
				sprite.removeChildAt(0);
			}
		}
		
		override public function set width(value:Number):void
		{
			// do nothing
		}
		
		public function drawScrollBar():void
		{	
			makeScrollBar();			
			makeTrack();			
			makeTrackGraphic();			
			makeThumb();
			makeButtons();
			addListeners();
		}
		
		protected function addListeners():void
		{	
			_thumb.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);	
		}
		
		protected function getUpStateGraphic():Sprite
		{	
			var upArrow:Sprite = new Sprite();	
			
			with(upArrow.graphics){
				beginFill(_thumbColor,1);
				drawCircle(8,8,8);
				
				endFill();
				
				beginFill(0x969696,1);
				moveTo(8,4);
				lineTo(8 - 3.857,12);
				lineTo(8 + 3.857,12);
				lineTo(8,4);
				endFill();
			}
			return upArrow;
		}
		
		protected function getOverStateGraphic():Sprite
		{
			var upArrow:Sprite = new Sprite();
			
			with(upArrow.graphics){
				beginFill(0x737373,1);
				drawCircle(8,8,8);
				
				endFill();
				
				beginFill(0xFFFFFF,1);
				moveTo(8,4);
				lineTo(8 - 3.857,12);
				lineTo(8 + 3.857,12);
				lineTo(8,4);
				endFill();
			}
			
			/* horiz
			beginFill(0x646464,1);
			moveTo(4,8);
			lineTo(12, 8 - 3.857);
			lineTo(12, 8 + 3.857);
			lineTo(4,8);
			endFill();
			*/
			return upArrow;
		}
		
		protected function getDownStateGraphic():Sprite
		{	
			var upArrow:Sprite = new Sprite();
			
			with(upArrow.graphics){
				beginFill(0xaaaaaa,1);
				drawCircle(8,8,8);
				
				endFill();
				
				beginFill(0x000000,1);
				moveTo(8,4);
				lineTo(8 - 3.857,12);
				lineTo(8 + 3.857,12);
				lineTo(8,4);
				endFill();
			}
			
			return upArrow;
		}
		
		protected function getHitStateGraphic():Sprite
		{
			var sprite:Sprite = new Sprite();
			
			sprite.graphics.beginFill(0x0000FF,1);
			sprite.graphics.drawCircle(8,8,8);
			sprite.graphics.endFill();
			
			return sprite;
		}
		
		protected function rotateGraphic(degrees:Number, sprite:Sprite):Sprite
		{
			var matrix:Matrix = sprite.transform.matrix;
			matrix.translate(-sprite.width/2, -sprite.height /2);
			matrix.rotate(degrees/180 * Math.PI);
			matrix.translate(sprite.width/2, sprite.height /2);
			sprite.transform.matrix = matrix;
			return sprite;
			
		}	
	}
}