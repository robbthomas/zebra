/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.view
{
import caurina.transitions.Tweener;

import com.alleni.author.Navigation.BackstageView;
import com.alleni.author.Navigation.EventPage;

import com.alleni.author.Navigation.IPageView;
import com.alleni.author.Navigation.Pager;

import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.Navigation.SmartObjectEditingMediator;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.CollisionMediator;
import com.alleni.author.controller.ComputeMessageCenterPosition;
import com.alleni.author.controller.ObjectDragMediator;
import com.alleni.author.controller.app.CursorController;
import com.alleni.author.controller.ui.AbstractObjectDragMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.DiagramMediator;
import com.alleni.author.controller.ui.DragDescription;
import com.alleni.author.controller.ui.MessageCenterMediator;
import com.alleni.author.controller.ui.WireController;
import com.alleni.author.controller.ui.palettes.ToolboxController;
import com.alleni.author.definition.DoubleClickAction;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.Objects;
import com.alleni.author.definition.Thumbnails;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.GraphicFill;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.LogicObject;
import com.alleni.author.model.objects.PathObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.util.ViewUtils;
import com.alleni.author.view.feedback.ObjectBoxFeedback;
import com.alleni.author.view.feedback.ObjectFeedback;
import com.alleni.author.view.objects.AuthorNoteView;
import com.alleni.author.view.objects.LogicObjectView;
import com.alleni.author.view.objects.VectorElementView;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.author.view.ui.ReplacementFeedback;
import com.alleni.taconite.dev.BitmapUtils;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.lang.Geometry;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.*;
import com.alleni.taconite.service.LogService;

import flash.display.Bitmap;

import flash.display.BitmapData;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Shape;
import flash.events.MouseEvent;
import flash.filters.BitmapFilter;
import flash.filters.BitmapFilterQuality;
import flash.filters.BlurFilter;
import flash.filters.ColorMatrixFilter;
import flash.filters.DropShadowFilter;
import flash.filters.GlowFilter;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.ui.MouseCursor;
import flash.utils.getTimer;

/**
	 * View of a AbstractObject value object in this application. ObjectViews may be dragged around if
	 * appropriate, and they move to track the object's position when that changes. 
	 */
	public class ObjectView extends VectorElementView
	{
		public static const CLOAKING_ALPHA:Number = 0.005;

		protected var _dragMediator:AbstractObjectDragMediator;
		protected var _role:uint = ViewRoles.PRIMARY;
		protected var _thumbnail:DisplayObject;
		protected var _messageCenterView:MessageCenterView;
        private var _coverForUnsafeView:Shape;

		private var _destroyed:Boolean = false;
		private var _authorNoteView:AuthorNoteView;
		private var _rollHilite:Boolean;
		private var _rolloverCopy:DisplayObject;
		private var _rolloverFilter:BitmapFilter;
		private var _reflectedView:DisplayObject;
        private var _preventMessageCenter:Boolean;

        private static var _utilities:Utilities;
        protected var _viewClassName:String;

        private static var _seed:int = 0;
        protected var _serial:int = (++_seed);


		/**
		 * Create a view of an object. 
		 * 
		 * @param context the ViewContext shared by all views in this document view
		 * @param model a TaconiteModel whose value object is the object to be displayed
		 * 
		 */
		public function ObjectView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model);
			_role = role;
            registerView();

            if (!_utilities)
                _utilities = new Utilities();
            _viewClassName = _utilities.shortClassName(this);
		}

        protected function registerView():void
        {
            object.register(this);
        }

        override public function createChildView(child:TaconiteModel):ITaconiteView
        {
            return TaconiteViewFactory.createForModel(context, child, ViewRoles.PRIMARY, Objects.DEFINITION);
        }

        public function set preventMessageCenter(value:Boolean):void
        {
            _preventMessageCenter = value;
        }

        public function get role():uint
        {
            return _role;
        }
		
		public function get stagePosition():Point
		{
			// return the anchorPoint location in stage coordinates
			updateViewPosition();
			var localAnchor:Point = new Point(0,0);
			return localToGlobal(localAnchor);
		}

		/**
		 * Initialize this view by adding a mediator for drag handling, if specified.
		 */
		override public function initialize():void
		{
			super.initialize();

			// if object is complete already, its probably loaded from a file
			// so we should immediately create the MC and thumbnail
			if (object.complete)
				handleObjectComplete();
			
			setDragMediator();

			if(_role==ViewRoles.PRIMARY) {
				recalcVisibility();
				// Update the view position.
				this.x = object.x;
				this.y = object.y;
				this.scaleX = object.scale/100;
				this.scaleY = object.scale/100;
				updateFilters();
				// TODO: remove this once deserialize sets blendMode only when blendMode is a proper value.
				setBlendMode();
			}
		}

		private function setBlendMode():void {
			if (object.blendMode == "") return;
			try {
				this.blendMode = object.blendMode;
			} catch (e:Error) {
				if (e.errorID != 2008) {
					throw e;
					return;
				} // else this is just an attempt to set an invalid blendMode
				LogService.info("Ignoring attempt to set blend mode to " + object.blendMode);
			}
		}
		
		protected function setDragMediator():void
		{
			if (_role==ViewRoles.PRIMARY)
				_dragMediator = new ObjectDragMediator(context).handleViewEvents(this, _role);
			if (_role==ViewRoles.DIAGRAM)
				_dragMediator = new DiagramMediator(context).handleViewEvents(this, _role);
			_dragMediator.inertial = object.inertial;
		}
		
		override protected function setSelectionMediator():void
		{
			// our ObjectDragMediator takes care of selection, and prevents the selection of descendants of locked gadgets
		}
		
		public function interceptMouseDown(e:MouseEvent):void
		{
			var objectDragMediator:ObjectDragMediator = _dragMediator as ObjectDragMediator;
			if (!objectDragMediator) return;
			
			objectDragMediator.mouseDownThisObject(e);
		}
		
		public function interceptObjMouseDown(e:MouseEvent):Boolean
		{
			var objectDragMediator:ObjectDragMediator = _dragMediator as ObjectDragMediator;
			if (!objectDragMediator) return false;
			
			return objectDragMediator.handleObjMouseDown(e);
		}
		
		public function interceptMouseMove(e:MouseEvent):void
		{
			var objectDragMediator:ObjectDragMediator = _dragMediator as ObjectDragMediator;
			if (!objectDragMediator) return;
			
			objectDragMediator.forwardToHandleMouseMove(e);
		}
		
		public function interceptSetupDragging(e:MouseEvent):void
		{
			var objectDragMediator:ObjectDragMediator = _dragMediator as ObjectDragMediator;
			if (!objectDragMediator) return;
			
			objectDragMediator.setupDragging(e);
		}
		
		public function interceptMouseOver(e:MouseEvent = null,view:ObjectView = null):void
		{
			var objectDragMediator:ObjectDragMediator = _dragMediator as ObjectDragMediator;
			if (!objectDragMediator) return;
			
			objectDragMediator.handleMouseOver(e,view);
		}

		public function interceptMouseOut(e:MouseEvent = null,view:ObjectView = null):void
		{
			var objectDragMediator:ObjectDragMediator = _dragMediator as ObjectDragMediator;
			if (!objectDragMediator) return;
			
			objectDragMediator.handleMouseOut(e,view);
		}
		
		public function preventDrag(prevent:Boolean=true):void
		{
			var objectDragMediator:ObjectDragMediator = _dragMediator as ObjectDragMediator;
			if (!objectDragMediator) return;
			
			objectDragMediator.preventDrag(prevent);
		}		

		override protected function updateView():void
		{
			ApplicationController.instance.requestUpdateView(this);
		}
		
		/**
		 * This is called during the application render event for all object views in the queue
		 * 
		 */
		public function render():void
		{
			// Let the super view do its thing (like clear Graphics).
			super.updateView();
			finishRender();
		}
		
		protected function finishRender():void
		{
			if (_role == ViewRoles.PRIMARY) {
				
				// Update the object view based on whether the object is enabled or disabled.
				recalcVisibility();
				if (object.enabled)
				{
					updateEnabledView();
				}else{
					updateDisabledView();
				}
				
				updateRollover();
				
				if (_reflectedView) {
					this.removeChild(_reflectedView);
					_reflectedView = null;
				}
				if (object.reflection) {
					if (this.width > 1 && this.height > 1) { // ensure view has dimensions
                        try {
                            _reflectedView = BitmapUtils.createReflectionBitmapData(this, 1.0, 0.5, 2.0, object.width, object.height, object.top, object.left);
                        } catch (e:Error) {
                            // we can't take a picture of certain views due to security, so won't get a reflection
                        }
                        if (!_reflectedView) return;
                        this.addChild(_reflectedView);
                        _reflectedView.x = object.left;
                        _reflectedView.y = object.top+object.height;
					}
				}
			}
		}
		
		
		/**
		 * Respond to a changed property value.  
		 * Return true if its completely dealt with and no updateView is needed. 
		 * @param property
		 * @param oldValue
		 * @param newValue
		 * @return 
		 * 
		 */
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{
			//trace("updateModelProperty prop="+property, "val="+newValue);
			switch(property) {
				// the following property changes do not require a redraw, so we will return true for all of them
				// they may however still require changes in properties of the view which may have been handled
				// in the redraw method. These changes will be handled here instead.
				case "inertial":
					_dragMediator.inertial = newValue as Boolean;
					return true;
				case "pendingReplacement":
				case "pendingData":
					updateReplacementFeedback();
					return true;
				case "x":
				case "y":
					return true;
				case "locationXYR":
					if (_role==ViewRoles.PRIMARY) {
						updateViewPosition();
					}
					return true;
				case "anchorPoint":
					if (object.isAnimationPath) {
						updateViewPosition();
						object.resnapAll();
					}
					break;
				case "xDiagram":
				case "yDiagram":
					if (_role==ViewRoles.DIAGRAM) {
						updateViewPosition();
					}
					return true;
					// there must be a statement here because if it is allowed to fall through
					// to the next case it will trigger an obscure language bug in AS3
					// resulting in "VerifyError: Error #1068: <C> and <C> cannot be reconciled."
					// where <C> is the name of this class.
				case "scale":
					this.scaleX = object.scale/100;
					this.scaleY = object.scale/100;
					return true;
				case "authorNotes":
					addAuthorNote();
					return true;
				case "xAuthorNote":
				case "yAuthorNote":
					return true;
				case "shadowLength":
				case "shadowRollOverLength":
				case "shadowAngle":
				case "shadowColor":
				case "shadowAlpha":
				case "shadowBlur":
				case "glowLength":
				case "glowColor":
				case "glowStrength":
				case "glowInner":
				case "colorSaturation":
				case "blur":
				case "transientPointerOver":
					updateFilters();
					return true;
				case "reflection":
					updateView();
					return true;
				case "blendMode":
					setBlendMode();
					return false;  // redraw is necessary for blending mode to show
				case "messageCenterRelative":
				case "xMessageCenter":
				case "yMessageCenter":
					return true;
				default:
					break;
			}
			if (_role == ViewRoles.DIAGRAM) {
				switch(property) {
					case "content":
					case "lineColor":
					case "lineThickness":
					case "lineAlpha":
					case "fillSpec":
					case "fillAlpha":
					case "width":
					case "height":
					case "thumbnailScale":
					
						updateThumbnail();
						break;
					default:
						break;
				}
			} else if (_role == ViewRoles.PRIMARY) {
				switch(property) {
					case "visible":
					case "cloakedNow":
					case "showAll":
						recalcVisibility();
						// force a redraw if cloaked and have a low fillAlpha (adjustFillAlpha() will adjust fill alpha)
                        updateView();
						return context.info.testView || !(object.cloakedNow && object.fillAlpha < 100);
					case "editing":
						if (newValue == true)
							ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_CURSOR));
						else
							super.updateStatus();
						break;
					case "messageCenterVisible":
						if (newValue == false)
                            _didWireOver = false;
						else if (object.messageCenter)
                            addMessageCenter();
						return true;
					case "messageCenter":
						if (newValue)
							addMessageCenter();
						else
							clearMessageCenter();
						return true;
					case "active":  
						if (_authorNoteView) {
							_authorNoteView.updatePosition();
						}
						if (newValue == false) {  // (because pager will delete this view)
							// the WorldMediator will also de-select this object, but needs help removing handles
							removeFeedback();
						}
						return true;
					case "forceHilite":
						forceHilite(newValue);
						break;
					default:
						break;
				}
			}
			
			// Add message center if initial object drawing has finished
			if (property=="complete") {
				if (oldValue!=newValue && newValue) {
					handleObjectComplete();
					return true;
				}
			}
			
			return false;
		}

		protected function updateReplacementFeedback():void
		{
			if (object.pendingReplacement) // put construction tape around the existing object
				ReplacementFeedback.instance.showPendingReplacement(this);
			else if (object.pendingData) // put warning line around the existing object
				ReplacementFeedback.instance.showPendingData(this);
			else {
				ReplacementFeedback.instance.clear();
				return;
			}
		}
		
		public function updateViewPosition():void
		{
			x = object.x;
			y = object.y;
			rotation = object.rotation;
		}
		
		protected function handleObjectComplete():void
		{
			// TODO: TEMP HACK. REMOVE ASAP.
			//if (!World(Application.instance.document.root.value).runningLocked) {
			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
				if (!Application.running && !object.childOfClosedComposite)
					addMessageCenter();
				
				if (_role == ViewRoles.DIAGRAM)
					updateThumbnail();
			}
		}
		
		protected function updateThumbnail():void
		{
			if (object.complete) {
				updateEnabledView();
				
				if (_thumbnail != null && this.contains(_thumbnail))
					this.removeChild(_thumbnail);
				
				if (object.width > 0 && object.height > 0) {
					_thumbnail = Thumbnails.generate(this, object.width, object.height, object.thumbnailScale);
					// position thumbnail to remain docked
					if(_thumbnail) {
						_thumbnail.x = 5;
						_thumbnail.y = Thumbnails.THUMB_NOMINAL_SIZE - _thumbnail.height;
						this.addChild(_thumbnail);
					}
				}
				graphics.clear();  // ensure the full-size graphic doesn't show in diagram
			}
		}
		
		override protected function updateStatus():void
		{
			// deselectIfNotVisible(); seems to be redundant with view.okToSelect which is already accounted for in SelectionMediator and which caused DEV-5225
			// which is more a change request than a bug, but commenting this out and adding a condition to MessageCenterView::okToSelect enables the desired functionality
			
			super.updateStatus();
			// PLAYERTODO
			if (selected && object.complete && !object.childOfClosedComposite && !Application.running) {
				addMessageCenter();
				ApplicationController.instance.handleMessageCenterBringToFront(_messageCenterView as MessageCenterView);
			}
		}

		private function addAuthorNote():void
		{
			const context:WorldContainer = context as WorldContainer;
			if (!context) return;
			
			if (!_authorNoteView && context.feedbackAnnotationView) {
				_authorNoteView = new AuthorNoteView(this.context, this.model, this._role);
				context.feedbackAnnotationView.addChildAt(_authorNoteView, 0);
				_authorNoteView.initialize();
			}
		}

		protected function clearAuthorNote():void
		{
			if (_authorNoteView != null) {
				_authorNoteView.warnViewToDelete();
				WorldContainer(context).feedbackAnnotationView.removeChild(_authorNoteView as DisplayObject);
				_authorNoteView = null;
			}
		}

		protected function addMessageCenter():void
		{
            Utilities.assert(!_destroyed);
            if (_preventMessageCenter){
                return;
            }
			
			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
				Utilities.assert(!_destroyed);
	
				if (!object.messageCenter){
					clearMessageCenter();
					return;
				}
				if (!object.complete)
					return;
				
				if (_messageCenterView == null && !("noMessageCenter" in object) && (!object.editing || ((object as SmartObject || object as PathObject) && object.editing) || _role==ViewRoles.DIAGRAM)) {
					if (object.messageCenterCache != null) {
						_messageCenterView = object.messageCenterCache;
                        _messageCenterView.objectView = this;
						_messageCenterView.initialize();
						object.messageCenterCache = null;
					} else {
						_messageCenterView = createMessageCenterView();
					}
					WorldContainer(context).feedbackUIView.addChild(_messageCenterView as DisplayObject);
					if(Application.instance.wireDragDescription == null && _messageCenterView.readyForWires){  // if not ready for wires, the MC will signal wireController when it becomes ready
                        ApplicationController.instance.wireController.showWiresForLevel();
                    }
				} else if (this is LogicObjectView && Application.instance.wireDragDescription == null) {
                    ApplicationController.instance.wireController.showWiresForLevel();   // drag-copy a table: should show the wires
                }
			}
		}

		
		protected function clearMessageCenter():void
		{
			// TODO: caching of message centers needs to happen on the controller level.
			if (_messageCenterView != null) {
				object.messageCenterCache = _messageCenterView;
				_messageCenterView.warnViewToDelete();
				WorldContainer(context).feedbackUIView.removeChild(_messageCenterView as DisplayObject);
				_messageCenterView = null;
			}
			ApplicationController.instance.wireController.showWiresForLevel();
		}

		
		public function updateDiagramPosition(point:Point):void
		{
			object.xDiagram = point.x;
			object.yDiagram = point.y;
			
			if (object.hasOwnProperty("noMessageCenter")) {
				// a gadget has been moved, possibly by moving its container...  update its wires
				ApplicationController.instance.wireController.requestRedrawForObject(object);
			}
		}
		
		
		private var _oldMCx:Number;
		private var _oldMCy:Number;
		private var _didWireOver:Boolean; // keep MC on screen until mouseUp  (messageCenterVisible=false will clear this)
		private var _wasWireOver:Boolean;  // previously in the wire-over state
		public static const MC_EDGE:Number = 10;  // how much MC to keep showing at edge of window
		
		public function updateMessageCenterPosition(area:Rectangle, mcHt:Number, wireOver:Boolean):Boolean
		{
            Utilities.assert(!_destroyed);
            var result:Boolean = false;  // return true if positioned it
			if (_messageCenterView) {
				if (_didWireOver && !Application.instance.wireDragDescription && !Application.instance.assetDragDescription) {
					_didWireOver = false;  // mouseUp has occurred
				} else if (wireOver) {
					_didWireOver = true;
				}
				var ignoreWindowBounds:Boolean;
                ignoreWindowBounds = (!_didWireOver && !this.object.selected && this.object is EventPage)
				// compute MC position, and constrain to window area
				var pos:Point = computeMessageCenterPosition();
                if (pos == null) {
                    return false;
                } else {
                    pos.x -= MessageCenterView.WIDTH/2;
                    pos.y += MessageCenterView.OBJECT_PADDING;
                }

                const mcWd:Number = MessageCenterView.WIDTH;
                var xOuttaBounds:Boolean = pos.x > area.right-mcWd-MC_EDGE || pos.x < area.left-MC_EDGE
                var yOuttaBounds:Boolean = pos.y < area.top+MC_EDGE || pos.y > area.bottom-mcHt-MC_EDGE;
                var openingOuttaBounds:Boolean = ((xOuttaBounds || yOuttaBounds) && _messageCenterView.isOpen && !_messageCenterView.draggedSinceOpen);
                if (ignoreWindowBounds && !openingOuttaBounds && !(this.model.value is EventPage))  {
                    if (pos.x < area.left-mcWd+MC_EDGE)	pos.x = area.left-mcWd+MC_EDGE;
                    if (pos.x > area.right-MC_EDGE)		pos.x = area.right-MC_EDGE;
                    if (pos.y < area.top-mcHt+MC_EDGE)		pos.y = area.top-mcHt+MC_EDGE;
                    if (pos.y > area.bottom-MC_EDGE)		pos.y = area.bottom-MC_EDGE;
//                    trace(".....................................................");
                } else if (_didWireOver) {
                    // force whole MC into window area
                    // if wire is being dragged over MC, use previous MC position, so collapsing category won't snap MC back down to bottom of screen
                    //
                    pos.x = _oldMCx;
                    pos.y = _oldMCy;

                    pos.y = Math.max(pos.y, area.top);
                    pos.y = Math.min(pos.y, area.bottom+MC_EDGE-mcHt);
                    pos.x = Math.max(pos.x, area.left);
                    pos.x = Math.min(pos.x, area.right-mcWd);

                    result = true;
//                    trace("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                } else if(!ignoreWindowBounds){
                    if (yOuttaBounds) {
                        pos.y = Math.max(pos.y, area.top);
                        pos.y = Math.min(pos.y, area.bottom+MC_EDGE-mcHt);
                    }
                    if (xOuttaBounds) {
                        pos.x = Math.max(pos.x, area.left);
                        pos.x = Math.min(pos.x, area.right-mcWd);
                    }

                    result = true;
//                    trace("##############################");
                }

				// position the MC if coordinates have changed:  and redraw connected wires
				if (pos.x != _oldMCx || pos.y != _oldMCy) { // detect change in position: to redraw wires
					if (_didWireOver || _wasWireOver) {
						beginMCtween(pos);
					} else {
						messageCenterView.x = pos.x;
						messageCenterView.y = pos.y;
                        result = true;
						ApplicationController.instance.wireController.requestRedrawForObject(object);
						ApplicationController.instance.authorController.repositionLightTextEditor();
					}
					_oldMCx = pos.x;
					_oldMCy = pos.y;
					_wasWireOver = _didWireOver;
				}
			}
            return result;
		}
				
		private function beginMCtween(destination:Point):void
		{
			Tweener.addTween(messageCenterView, {x:destination.x, time:0.5, transition:"easeOutExpo", onUpdate:updateMCtween});
			Tweener.addTween(messageCenterView, {y:destination.y, time:0.5, transition:"easeOutExpo", onUpdate:updateMCtween});
		}
				
		public function updateMCtween():void
		{
			ApplicationController.instance.wireController.requestRedrawForObject(object);
			ApplicationController.instance.authorController.repositionLightTextEditor();
			stage.invalidate();
		}
		
		public function computeMessageCenterPosition():Point
		{
			if (!object.messageCenterRepositioned && _messageCenterView) {  // docked
				return computeDockPosition();
			} else if (object.messageCenterRelative) {
				var point:Point = new Point(object.xMessageCenter, object.yMessageCenter);
				var worldView:WorldView = WorldContainer(context).worldView;
				return worldView.globalToLocal(parent.localToGlobal(point));
			} else {
				return new Point(object.xMessageCenter, object.yMessageCenter);
			}
		}
		
		
		protected function computeDockPosition():Point
		{
			var rotate:Number = computeNetRotation(); // sum of rotation of all containers plus this object
			var offset:Point = computeMessageCenterOffset(rotate);  // offset from topLeft of obj
			var local:Point = new Point();
			local.x = object.left + object.width/2 + offset.x;  // offset from anchor point
			local.y = object.top + object.height/2 + offset.y;
			var global:Point = this.localToGlobal(local);
			return WorldContainer(_context).feedbackUIView.globalToLocal(global);
		}

		
		/**
		 * Compute the offset from topLeft of object to topCenter of MC. 
		 * @param obj
		 * @param rotation = the net rotation of object plus all nested containers holding the object.
		 * @return 
		 * 
		 */
		protected function computeMessageCenterOffset(rotation:Number):Point
		{
			// this function is overridden by LineView
			
			// offset from topLeft of obj to the center of the MC, in obj local coordinates
			var offset:Point = ComputeMessageCenterPosition.computeMessageCenterPosition(object.width, object.height, rotation);
			return offset;
		}

		public function undockMessageCenter(point:Point):void
		{
			object.messageCenterRepositioned = true;

			if (object.parent as SmartObject) {
				object.messageCenterRelative = true;
				var worldView:WorldView = WorldContainer(context).worldView;
				point = worldView.localToGlobal(point);
				point = parent.globalToLocal(point);
				object.xMessageCenter = point.x;
				object.yMessageCenter = point.y;
			} else {
				object.messageCenterRelative = false;
				object.xMessageCenter = point.x;
				object.yMessageCenter = point.y;
			}
		}

		public function dockMessageCenter():void 
		{
			object.messageCenterRepositioned = false;
		}
		
		
		public function messageCenterNearDockPosition():Boolean
		{
			var dockPoint:Point = computeDockPosition();
			var mcPoint:Point = computeMessageCenterPosition();
			
			return (Geometry.distanceBetweenPoints(dockPoint.x, dockPoint.y, mcPoint.x, mcPoint.y) < MessageCenterMediator.SNAP_DISTANCE);
		}
		
		public function computeAuthorNotePosition():Point
		{
			var local:Point = new Point(object.left+object.xAuthorNote, object.top+object.yAuthorNote);
			return localToGlobal(local);
		}


		
        override public function warnViewToDelete():void
        {
            var title:String = object.title;
			super.warnViewToDelete();

			clearMessageCenter();
			clearAuthorNote();
			if (_dragMediator)
				_dragMediator.clearViewEvents();
			
			removeFeedback();  // undo needs this to be done here
			_destroyed = true;
        }

		override protected function addRollHighlight(e:MouseEvent) : void
		{
			if ( !(this as IPageView) && !object.childOfClosedComposite && !object.locked && Application.instance.currentTool == ToolboxController.ARROW) {
				super.addRollHighlight(e);
				
				var drag:DragDescription = Application.instance.wireDragDescription;
				if (drag == null)
					drag = Application.instance.assetDragDescription;
				if (drag != null && !selected && !(model.value as AbstractObject).wiringLocked && _role==ViewRoles.PRIMARY) {
					var prevModel:TaconiteModel = drag.shownMessageCenter;
					var velumMediator:SmartObjectEditingMediator = ApplicationController.instance.authorController.currentVelumMediator;
					if (prevModel != this.model && !(velumMediator != null && velumMediator.editingObject == model.value )) {
						drag.shownMessageCenter = model;
					}
				}
			}
		}
		
		protected function setRolloverCursor():void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.CHANGE_CURSOR_TO, {name:MouseCursor.HAND, type:CursorController.CURSOR_TYPE_STANDARD}));	
		}
		
		
		
		override protected function removeRollHighlight(e:MouseEvent) : void
		{
			super.removeRollHighlight(e);
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_CURSOR));
			
//			updateMouseOver(false);
		}
		
		private function updateMouseOver(over:Boolean):void
		{
			object.controller.updateMouseOver(model, over);
		}

		protected function recalcVisibility():void
		{
			visible = object.visible || object.cloakedNow || object.showAll;
			
			if (object.cloakedNow && !object.showAll) {
				this.alpha = CLOAKING_ALPHA;
			}
			else {
				this.alpha = object.alpha/100;
			}

            if (object.interactive == false) {
                this.alpha *= (object.disabledAlpha / 100);
            }

            deselectIfNotVisible();
		}
		
		private function deselectIfNotVisible():void
		{
			if((!object.visible || object.cloakedNow) && !object.showAll && selected){
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.DESELECT_OBJECT, model));
			}
		}
		
		protected function createMessageCenterView():MessageCenterView
		{
			return new MessageCenterView(context, model, _role, this);
		}
		
		override protected function createFeedbackView():ITaconiteView
		{
			Utilities.assert(!_destroyed);
			
			if (_role == ViewRoles.PRIMARY) return ObjectFeedbackFactory();
			else return null;
		}
		
		protected function ObjectFeedbackFactory():ITaconiteView
		{
			return new ObjectBoxFeedback(context, model, this);
		}

        /**
         * Very important function used for mouse testing both for clicks and rollovers but also for dropping into containers
         * It is currently recursive.
         * @param stagePoint Position of mouse in stage coordinates
         * @param subjectView View that is being dragged, null if no dragging
         * @param path // array of integer child indexes from the world to the subject view
         * @return null if no view was hit, subjectView if we got a non droppable hit denoting no parent change, otherwise the view that was clicked, or the view that should be dropped into
         */
		public function findViewUnderPoint(stagePoint:Point, subjectView:ObjectView = null, path:Array = null):ObjectView
		{
            // first do some immediate checks that let us know we wont pick this view or any child of it

			if (!visible && !object.cloakedNow && !(this is BackstageView))
				return null;  // invisible objects do not respond to rollover, clicks or anything
			if (object is LogicObject && visible==false)
				return null;  // Table views go vis=false in closed gadgets

            if(this == subjectView) {
                return null; // we don't want to pick the thing we are testing under
            }

			if (hitTestPoint(stagePoint.x, stagePoint.y, true)) {  // the rough hit-test of Flash Sprite, counts transparent areas as a hit
//              if(subjectView)trace("Hit test success for ", object, "path="+(path ? path.join(",") : "null"));

                // now that we know we are a candidate, first process any children since hitting one of those before
                // considering ourselves given that our drawing is in the back
                var children:Vector.<ObjectView> = getOnStageChildrenBelow(subjectView, path);
                for (var n:int=children.length-1; n >= 0; n--) {
                    var childView:ObjectView = children[n]
//                        if(subjectView)trace(" childView="+childView, "n="+n);
                    if (childView) {
                        var result:ObjectView = childView.findViewUnderPoint(stagePoint, subjectView, path);
//                            if(subjectView)trace("   childView="+childView, "findViewUnderPoint="+result);
                        if ((subjectView != null) && (result == subjectView)) {
                            break; // don't check children below this point
                        } else if(result != null) {
                            return result;  // found hit !
                        }
                    }
                }

				// missed all the children ... maybe the point is on this view
                // dragging an object will always hit an arena that is fully transparent in a point
                // clicking an arena will hit if it is opaque, but during authoring also hit even if not opaque
                const arena:Boolean = (object is Pager);
                const page:Boolean = (this is IPageView);
                const dragging:Boolean = subjectView != null;
                const authoring:Boolean = !Application.running;
                if(dragging) {
                    if (object.childOfClosedComposite && object.wireScopeContainer != subjectView.object.wireScopeContainer) {
                        return null;  // can't drag into an arena in a closed gadget (running or not)
                    }
                    if (object is Composite && !object.editing && subjectView.object.wireScopeContainer == object) {
                        return this;  // keep object inside the gadget it belongs to
                    }
                    if(accepting || subjectView.parentObjectView == this) {
                        return this; // we passed the bounds test and pick regardless if accepting
                    } else if(exactHitTest(stagePoint)) {
                        // return sentinel value to denote that we got a hit and should not keep searching
                        // but that it is not to be dropping into some parent
                        return subjectView;
                    }
                } else if(page) {
                    return null; // skip out to parent for arena pages
                } else if((arena && authoring) || exactHitTest(stagePoint)) {
                    return this; // found hit !
                }
			}
			return null; // missed this and children
		}

		
		public function exactHitTest(stagePoint:Point):Boolean
		{
			return CollisionMediator.viewIsOpaqueUnderPoint(this, stagePoint);
		}
		
		
		/**
		 * Decide whether this view would accept a child view, possibly dragged over it.
		 * @param subjectView
		 * @param global = the anchor point of the subjectView
		 * @return the accepting view, or null
		 * 
		 */
		public function acceptChild(subjectView:ObjectView, stagePoint:Point):ObjectView
		{
//			trace("ObjectView::acceptChild global="+stagePoint, "this="+object, "subjectView="+subjectView);

            if(!subjectView.object.acceptIntoOutOfArena) {
                return null;
            }

            var path:Array = null;
            if(object is AbstractContainer) {
                path = AbstractContainer(object).objectPathArray(subjectView.object);
            }
            var oldVisible:Boolean = subjectView.visible;
            subjectView.visible = false; // hide temporarily so it is invisible to our cursor
            var target:ObjectView = findViewUnderPoint(stagePoint, subjectView, path);
            if(target == subjectView) {
                target = null;
            }
            subjectView.visible = oldVisible;
//            trace("Found view ", target?target.object:'null');

            return target;
        }

        /**
         * Generates a list of object views suitable for mouse picking or dragging
         * intended to be used in findViewUnderPoint and overridden as necessary
         *
         * @param subjectView null if not dragging a view, otherwise
         * @param path null or the object index path of subjectView rooted at this object  (entries are REMOVED from this array)
         * @return list of all children on stage if subjectView is null, otherwise only children that are below
         */
        public function getOnStageChildrenBelow(subjectView:ObjectView=null, path:Array=null):Vector.<ObjectView> {
            var total:int = childViewContainer ? childViewContainer.numChildren : 0;
            if(subjectView != null && path != null && path.length > 0 && path[0] < total) {
                total = int(path.shift());
                if(path.length != 0) {
                    total += 1; // this is the index not for the object itself but for a parent so make sure it is included
                }
            }
            var result:Vector.<ObjectView> = new Vector.<ObjectView>(total);
            for(var i:int = 0; i < total; i++) {
                result[i] = childViewContainer.getChildAt(i) as ObjectView;
            }
            return result;
        }
		
		public function releaseChild(subjectView:ObjectView, newParent:ObjectView, stagePoint:Point):Boolean
		{
			return releasing;
		}
		
		public function get parentObjectView():ObjectView
		{
			var parentView:DisplayObjectContainer = this.parent;
			
			// skip over any container Sprites that are not ObjectView
			while (parentView && !(parentView is ObjectView))
				parentView = parentView.parent;
				
			return parentView as ObjectView;
		}
		
		
		protected function get accepting():Boolean
		{
			// overridden by ArenaPageView
			return ((model.value is AbstractContainer) && AbstractContainer(model.value).accepting);
		}
		
		public function get releasing():Boolean
		{
			// overridden by ArenaPageView
			return ((model.value is AbstractContainer) && AbstractContainer(model.value).releasing);
		}

		private function forceHilite(hilite:Boolean):void
		{
			_rolled = hilite;
			updateStatus();
		}
		
		public function get messageCenterView():MessageCenterView
		{
			return _messageCenterView as MessageCenterView;
		}

		
		//////////  EFFECTS  &  rollover glow

        override public function get useRolloverGlow():Boolean
        {
            return super.useRolloverGlow && !Application.uiRunning && !object.editingLocked && !object.childOfClosedMasterPage;
        }

		override protected function set rolloverGlow(value:Boolean):void
		{
			// PLAYERTODO
			if (Application.runningLocked)
				return;
			_rollHilite = value;
			updateFilters();
		}

		private function get rolloverFilter():BitmapFilter
		{
			if (!_rolloverFilter)
				_rolloverFilter = new DropShadowFilter(2, 45, context.info.selectionColors[0], 0.9, 6, 6, 1, 3, false, true);
			return _rolloverFilter;
		}
		
		private function updateRollover():void
		{
			// extra verbose method for manually bitmap caching the view to apply the rollover filter to that instead of the view itself.
			// this avoids Flash auto bmp cache which was exhibiting sub-pixel shifting and artifacts.
			// since the filter is set to knock out, only the filter is visible, not the bitmap picture of the view.
			
			if (_rolloverCopy) {
				this.removeChild(_rolloverCopy);
				_rolloverCopy = null;
			}
			if (_rollHilite && this.width > 0) {
                var data:BitmapData = ViewUtils.instance.takeObjectScreenshot(this);
				_rolloverCopy = new Bitmap(data);
                _rolloverCopy.x = object.left;
                _rolloverCopy.y = object.top;
				if (_rolloverCopy) {
					_rolloverCopy.filters = [rolloverFilter];
					this.addChild(_rolloverCopy);
				}
			}
		}

		/**
		 * the view is safe to cache if it does not contain cross-domain content such as external SWF's for playing videos or displaying maps. 
		 * @return 
		 * 
		 */
        public function set makeSafeForCaching(value:Boolean):void
        {
            // override in Pager, Map & Video views
            if (childViewContainer) {
                var count:int = childViewContainer.numChildren;
                for(var n:int = 0; n < count; n++) {
                    var child:ObjectView = childViewContainer.getChildAt(n) as ObjectView;
                    child.makeSafeForCaching = value;
                }
            }
        }

        protected function showCoverForUnsafeView():void
        {
            var cover:Shape = new Shape();
            with (cover.graphics) {
                beginFill(0xBBBBBB, 0.5);
                drawRect(0,0, object.width, object.height);
                endFill();
            }
            cover.x = object.left;
            cover.y = object.top;
            addChild(cover);
            _coverForUnsafeView = cover;
        }

        protected function hideCoverForUnsafeView():void
        {
            if (_coverForUnsafeView) {
                if (this.contains(_coverForUnsafeView)) {
                    this.removeChild(_coverForUnsafeView);
                }
                _coverForUnsafeView = null;
            }
        }

		protected function updateFilters():void
		{
			if (context.info.testView) {  // automated testing is active
				filters = null;
				cacheAsBitmap = false;
				return;
			}
			
			updateRollover();

			var filterArray:Array = [];
			
			if (object.colorSaturation != 100) {
				filterArray.push(createColorFilter(object));
			}
			
			if (object.blur > 0) {
				filterArray.push(createBlurFilter(object));
			}
			
			if (object.glowLength > 0) {
				const glowOpacity:Number = 0.8;
				var glow:GlowFilter = new GlowFilter(object.glowColor, glowOpacity, object.glowLength, object.glowLength, 
					object.glowStrength, 1, object.glowInner);  // quality=1
				filterArray.push(glow);
			}
			
			if (object.shadowLength > 0 || (object.shadowRollOverLength != 0 && object.transientPointerOver && (object.shadowLength+object.shadowRollOverLength > 0))) {
				var shadow:DropShadowFilter = new DropShadowFilter(object.shadowLength+((object.transientPointerOver)?object.shadowRollOverLength:0), object.shadowAngle, object.shadowColor, object.shadowAlpha/100, 
					object.shadowBlur, object.shadowBlur, 0.8, 3);  // strength=0.8, quality=3
				filterArray.push(shadow);
			}
							
			if (filterArray.length > 0)
				filters = filterArray;
			else
				filters = null;
		}

		private function createColorFilter(obj:AbstractObject):ColorMatrixFilter
		{
			var c:Number = obj.colorSaturation / 100;
			var offset:Number = 0;
			if (c < 0)  c = 0;
			if (c > 1.0) {
				offset = c - 1.0;  // saturation values greater than 100 give "supersaturation"
				c = 1.0;
				if (offset > 1)
					offset = 1;
				offset *= 255;  // ... increasing offset moves color to more intense (less white)... eventually to black
			}
			var m:Number = (1.0 - c)/3;  // monochrome coefficient
//			trace("colorSat filter: c="+c, "m="+m, "offset="+offset);
			
			var colorMatrix:Array = 
				[	m+c,	m,		m,		0,		-offset,
					m,		m+c,	m,		0,		-offset,
					m, 		m, 		m+c, 	0, 		-offset,
					0, 		0, 		0, 		1, 		0];
			
			return new ColorMatrixFilter(colorMatrix);
		}
		
		private function createBlurFilter(obj:AbstractObject):BlurFilter
		{
			var blur:Number = obj.blur;
			if (blur < 0)  blur = 0;
			if (blur > 100) blur = 100;
			blur = blur * 20 / 100;  // 20 seems to blur it to oblivion, so we'll consider that 100%
			
			return new BlurFilter(blur, blur, BitmapFilterQuality.HIGH);  // High quality approximates gaussian blur
		}
		
		override public function toString():String
		{	
			return "[" + _viewClassName + " \"" + object.title + "\" XY="+x+","+y + " #="+_serial + "]";
		}
		
				
		public function computeNetRotation():Number
		{
			var netRotation:Number = 0;
			var view:DisplayObjectContainer = this;
			while (view) {
				var rot:Number = view.rotation;
				if (rot > 0 || rot < 0)
					netRotation += view.rotation;
				view = view.parent;
			}
			return netRotation;
		}
		
		
		// doubleClickAction()
		// The double-click event will bubble, starting with the clicked object and going to ancestors, eventually reaching world.
		// This function chooses between:
		// IGNORE:  allow the event to bubble to ancestors
		// STOP:  kill the event
		// EDIT:  edit this object.
		
		private var _doubleClicked:int;  // time when last double-clicked this view
		private static const DBL_CLICK_WAIT:int = 300;  // wait after double-click text to open gadget, before double-click to edit text
		
		public function get doubleClickAction():int
		{
//			trace("doubleClickAction",object);
			const smart:SmartObject = object as SmartObject;
			if (object.editingLocked)
				return DoubleClickAction.IGNORE;
			
			if (Application.running && (isWired("clickStart") || isWired("clickRelease") || isWired("click")))
				return DoubleClickAction.STOP;
			
			// double-click child of a closed gadget
			var recentDbl:Boolean = _doubleClicked + DBL_CLICK_WAIT > getTimer();  // true if recently already double-clicked this view
			if ((object.childOfClosed && !smart) || recentDbl) {  // can't edit text in closed gadget, but can edit nested gadget
				_doubleClicked = getTimer();  // prevent editing text from a triple-click on a gadget child
				return DoubleClickAction.IGNORE;
			}
			
			return DoubleClickAction.EDIT;
		}
		
		
		private function isWired(key:String):Boolean
		{
			var wiring:WireController = ApplicationController.instance.wireController;
			return wiring.objectHasWires(object, Modifiers.instance.fetch(key));
		}
		
		/**
		 * Given a rectangle in local space, return a rectangle in the given coordinate space. 
		 * Will enlarge the rect as needed when "space" is rotated in reference to this view,
		 * to ensure the whole rect is covered by the result.
		 * @param rect
		 * @param space
		 * @return 
		 * 
		 */
		protected function rectToSpace(rect:Rectangle, space:DisplayObject):Rectangle
		{
			var topLeft:Point = space.globalToLocal(localToGlobal(rect.topLeft));
			var topRight:Point = space.globalToLocal(localToGlobal(new Point(rect.right,rect.top)));
			var botRight:Point = space.globalToLocal(localToGlobal(rect.bottomRight));
			var botLeft:Point = space.globalToLocal(localToGlobal(new Point(rect.left,rect.bottom)));
			var result:Rectangle = new Rectangle();
			result.left = Math.min(topLeft.x, topRight.x, botRight.x, botLeft.x);
			result.right = Math.max(topLeft.x, topRight.x, botRight.x, botLeft.x);
			result.top = Math.min(topLeft.y, topRight.y, botRight.y, botLeft.y);
			result.bottom = Math.max(topLeft.y, topRight.y, botRight.y, botLeft.y);
			return result;
		}
		
		override public function get okToSelect():Boolean
		{
			return object.okToSelect;
		}
		
		public function flashRedForPathSelection(show:Boolean):void
		{
			// PathObject and Line override this to hilite themselves when author is selecting an animation path
		}
		
		
		/*
		BEGIN SVG VIEW METHODS
		*/
		
		
		/**
		 * 
		 * @param bounds
		 * @return 
		 * 
		 */
		public function getSVG(localToTarget:Function):XML
		{
			return new XML();
		}
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function getSVGDef():XML
		{
			var def:XML;
			
			switch(object.fillSpec.type)
			{
				case GraphicFill.SOLID_FILL:
					def = <linearGradient id={object.uid}
					spreadMethod="pad"
					x1="0%" y1="0%" 
					x2="0%" y2="0%" />;
					def.appendChild(<stop offset="100%" stop-color={convertColorValueToHex(object.fillSpec.color)} stop-opacity={object.fillAlpha / 100} />)
					break;
				case GraphicFill.GRADIENT_FILL:
					def = encodeGradient(object.fillSpec.linear, object.fillSpec.colors, object.fillSpec.angle);
					break;
				case GraphicFill.JELLYBEAN_FILL:
					// a path object can be marked "jellybean" but needs to render as gradient
					var colors:Vector.<uint> = new Vector.<uint>();
					colors.push(object.fillSpec.color1);
					colors.push(object.fillSpec.color2);
					def = encodeGradient(true, colors, GraphicFill.JELLYBEAN_GRADIENT_ANGLE);
					break;
			}
			return def;
		}
		
		private function encodeGradient(linear:Boolean, colors:Vector.<uint>, angle:Number):XML
		{
			var def:XML;
			var offset:Number = 100 / colors.length;
			var colorCt:uint = 0;
			var band:Number = 100 / (colors.length-1);
			
			if (linear)
			{
				def = <linearGradient id={object.uid} 
				spreadMethod="pad"
				x1="0%" y1="0%" 
				x2="100%" y2="0%" />;
				
			}else{
				def = <radialGradient id={object.uid} 
				spreadMethod="pad" 
				x1="0%" y1="0%" 
				x2="100%" y2="0%" />;
			}
			for each(var c:uint in colors)
			{
				def.appendChild(<stop offset={(band*colorCt++)+'%'} stop-color={convertColorValueToHex(c)} stop-opacity={object.fillAlpha / 100} />)
			}
			// a path object can be marked "jellybean" but needs to render as gradient
			def.@gradientTransform = "rotate("+angle+")";

			return def;
		}
		
		/**
		 * 
		 * @param element
		 * 
		 */
		protected function setCommonSVGAttributes(element:XML, rotation:Number, svgAnchor:Point):void{
			if(object.fillAlpha)
			{
				element.@style = "fill: url(#"+object.uid+");";
			}else{
				element.@style = "fill:none;";
			}
			
			element.@transform = "rotate("+rotation+","+svgAnchor.x+","+svgAnchor.y+")";
			
			if(object.lineThickness)
			{
				element.@stroke = convertColorValueToHex(object.lineColor);
				element.@["stroke-width"] = object.lineThickness ;
				element.@["stroke-linecap"] = "round";
				element.@["stroke-opacity"] = object.lineAlpha / 100;
			}
			
            element.@opacity = object.alpha / 100;
		}
		
		/**
		 * 
		 * @param colorValue
		 * @return 
		 * 
		 */
		final public function convertColorValueToHex(colorValue:uint):String{
			
			var colorString:String;
			
			colorString = "000000" + colorValue.toString(16);
			colorString = colorString.substring(colorString.length - 6, colorString.length);
			return "#" + colorString;
		}

        public function handleRunningChanged():void {
            updateFilters(); // in case of automated test
            if(_feedback is ObjectFeedback) {
                ObjectFeedback(_feedback).handleRunning(Application.uiRunning);
            }
        }
	}
}
