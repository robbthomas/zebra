/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view
{
import com.alleni.author.Navigation.BackstageView;
import com.alleni.author.Navigation.FlowView;
import com.alleni.author.Navigation.EventFlowView;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.WorldMediator;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ITaconiteView;
import com.alleni.taconite.view.ViewContext;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;

/**
     * View for the World value object.
     */
    public class WorldView extends ObjectView
    {
    	private const HUD_WIDTH:int = 960;
    	private const HUD_HEIGHT:int = 600;
		private var _mediator:WorldMediator;

        public var projectMessageCenter:MessageCenterView;
        public var lmsMessageCenter:MessageCenterView;

        public function WorldView(context:ViewContext, model:TaconiteModel=null, role:uint=ViewRoles.PRIMARY)
        {
            super(context, model,role);
            _role = role;

            removeEventListener(MouseEvent.ROLL_OVER, addRollHighlight);
            removeEventListener(MouseEvent.ROLL_OUT, removeRollHighlight);
            preventMessageCenter = true;

            if (world.eventFlow) {
                createEventFlowView(world.eventFlow);
            }

			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
                addEventListener(Event.ENTER_FRAME, fixTopMessageCenters);
            }
        }

        private function fixTopMessageCenters(event:Event):void {
            if(WorldContainer(context).feedbackUIView == null) {
                return; // wait till we have a feedback layer
            }

            if(world.project && (projectMessageCenter == null || projectMessageCenter.model.value != world.project)) {
                if(projectMessageCenter) {
                    WorldContainer(context).feedbackUIView.removeChild(projectMessageCenter);
                    projectMessageCenter.warnViewToDelete();
                }
                projectMessageCenter = new MessageCenterView(context,  world.project.model, role);
                projectMessageCenter.titleEditable = false;
                WorldContainer(context).feedbackUIView.addChild(projectMessageCenter);
            } else if(world.project == null && projectMessageCenter != null) {
                WorldContainer(context).feedbackUIView.removeChild(projectMessageCenter);
                projectMessageCenter.warnViewToDelete();
                projectMessageCenter = null;
            }

            if(world.lms && (lmsMessageCenter == null || lmsMessageCenter.model.value != world.lms)) {
                if(lmsMessageCenter) {
                    WorldContainer(context).feedbackUIView.removeChild(lmsMessageCenter);
                    lmsMessageCenter.warnViewToDelete();
                }
                lmsMessageCenter = new MessageCenterView(context,  world.lms.model, role);
                lmsMessageCenter.titleEditable = false;
                WorldContainer(context).feedbackUIView.addChild(lmsMessageCenter);
            } else if(world.lms == null && lmsMessageCenter != null) {
                WorldContainer(context).feedbackUIView.removeChild(lmsMessageCenter);
                lmsMessageCenter.warnViewToDelete();
                lmsMessageCenter = null;
            }

            EditorUI.instance.topControlBar.updateMessageCenters();

            // remove this listener
            event.target.removeEventListener(event.type, arguments.callee);
        }

        private function createEventFlowView(flow:EventFlow):void
        {
            if (!flow.eventPagerView) {
                var backView:BackstageView = new BackstageView(context, flow.model, ViewRoles.PRIMARY);
                backView.initialize();
                addChild(backView);
                flow.backstageView = backView;

                var pagerView:EventFlowView = new EventFlowView(context, flow.model, ViewRoles.PRIMARY);
                pagerView.initialize();
                addChild(pagerView);
                pagerView.visible = false;  // initially hidden since flow is showing
                flow.eventPagerView = pagerView;

                if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
                    var flowView:FlowView = new FlowView(flow, context);
                    flowView.initialize();
                    addChild(flowView);
                    flow.flowView = flowView;
                }
            }
        }

        /**
         * The World of which this is a view.
         */
        public function get world():World
        {
            return model.value as World;
        }

        /**
         * Initialize this object by creating the appropriate mediator to handle events.
         */
        override public function initialize():void
        {
            super.initialize();
            
            // if this is a HUD view, draw a special mask. If not, we need a mediator for UI events within.
            if (_role == ViewRoles.HUD) {
            	var HUDMask:Sprite = new Sprite();
            	HUDMask.graphics.beginFill(0);
            	HUDMask.graphics.drawRect(0, 0, HUD_WIDTH+1, HUD_HEIGHT+1);
            	HUDMask.graphics.endFill();
			} else {
            	_mediator = new WorldMediator(context, _role).handleViewEvents(this);
			}
        }
		
		override protected function setDragMediator():void
		{
			// avoid having a drag mediator ... we don't want to drag the world, or select it
		}
		
		override protected function handleObjectComplete():void
		{
			// nothing the World needs specifically, but this override prevents the (very cool) World Message Center.
		}

        override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean {
            var handled:Boolean = super.updateModelProperty(property, oldValue, newValue);
            switch(property) {
                case "project":
                case "lms":
                    addEventListener(Event.ENTER_FRAME, fixTopMessageCenters);
                    return true;
                case "eventFlow":
                    createEventFlowView(newValue as EventFlow);
                    return true;
            }
            return handled;
        }

        /**
         * Update the view by drawing the backdrop for the world.
         */
        override public function render():void
        {
			this.x = 0; // always draw world at origin
			this.y = 0;

            if(_role != ViewRoles.PRIMARY) {
                return;
            }
//			graphics.clear();
//            graphics.beginFill(0xFFFFFF, 1);
//			graphics.drawRect(0, 0, world.width, world.height);
//            graphics.endFill();
        }
        
        public function dumpViews( title:String="" ):String
		{
			var buffer:String = "\nDump WorldView: role=" + _role;
			trace(buffer);
			buffer += Utilities.dumpDisplayObjectContainer(this, "WorldView");
			buffer += Utilities.dumpDisplayObjectContainer(WorldContainer(context).feedbackUIView, "feedbackUIView", 1);  // levels=1 shows only the MCs, not ribbons
			return buffer;
        }        

		override public function warnViewToDelete() : void
		{
			super.warnViewToDelete();
			_mediator.clearViewEvents();
            if(flowView) {
                flowView.warnViewToDelete();
            }
            if(world.eventFlow && world.eventFlow.eventPagerView) {
                world.eventFlow.eventPagerView.warnViewToDelete();
            }
		}
		
		/**
		 * Create a child view for some child model of our model. 
		 * @param child a child TaconiteModel
		 * @return the appropriate type of view for the value object belonging to that child.
		 */
		override public function createChildView(child:TaconiteModel):ITaconiteView
		{
            throw new Error("worldView:createChildView")
		}
		
		override protected function ObjectFeedbackFactory():ITaconiteView
		{
			return null;  // prevent handles on the world
		}

		override public function interceptMouseDown(e:MouseEvent):void
		{
			_mediator.handleMouseDown(e);
		}

        override public function getOnStageChildrenBelow(subjectView:ObjectView = null, path:Array = null):Vector.<ObjectView> {
            if(world.eventFlow == null){
                return new Vector.<ObjectView>();
            }
            if(subjectView) {
                var childName:String = path.shift(); // one of [project,lms,eventflow] and in this case must be eventflow as others cant be dragged
                if(path[0] is Number) {
                    // numbered index means direct child of eventFlow and therefore on the backstage
                    return new <ObjectView>[backstageView];
                }
            }
            if (Application.instance.flowVisible) {
                if(subjectView) {
                    // we are dragging but we didn't return out above so we must be on one of the pages
                    return new <ObjectView>[flowView];
                } else {
                    // not dragging so mouse over whatever we are able
                    return new <ObjectView>[backstageView, flowView];
                }
            } else {
                // backstage cant be visible here, so dont include it
                return new <ObjectView>[eventPagerView];
            }
        }

		override protected function get accepting():Boolean
		{
			return false;
		}

        override public function hitTestPoint(x:Number, y:Number, shapeFlag:Boolean=false):Boolean
        {
            return true;
        }

		override public function exactHitTest(stagePoint:Point):Boolean
		{
			return true;
		}

	    override protected function set rolloverGlow(value:Boolean):void
		{
		    // do nothing
	    }

        override public function handleRunningChanged():void {
            super.handleRunningChanged();
            backstageView.handleRunningChanged();
            eventPagerView.handleRunningChanged();
        }

        public function get flowView():FlowView
        {
            return world.eventFlow.flowView;
        }

        public function get backstageView():BackstageView
        {
            return world.eventFlow.backstageView;
        }

        public function get eventPagerView():EventFlowView
        {
            return world.eventFlow.eventPagerView;
        }

        override public function set makeSafeForCaching(value:Boolean):void
        {
            if (eventPagerView) {
                eventPagerView.makeSafeForCaching = value;
            }
        }
    }
}
