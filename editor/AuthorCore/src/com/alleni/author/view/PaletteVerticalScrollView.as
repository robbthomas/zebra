package com.alleni.author.view
{
	import com.alleni.author.model.ScrollModel;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	
	public class PaletteVerticalScrollView extends VerticalScrollView
	{

		public function PaletteVerticalScrollView(model:ScrollModel, scrollControllerWidth:Number, scrollControllerHeight: Number, trackBackgroundColor:uint = 0, buttonBackgroundColor:uint = 0)
		{
			super(model, scrollControllerWidth, scrollControllerHeight, trackBackgroundColor, buttonBackgroundColor);
			
			_model = model;
			_scrollControllerWidth = scrollControllerWidth;
			_scrollControllerHeight = scrollControllerHeight;
			
			init()
			drawScrollBar();
		}
		
		override protected function init():void //done
		{	
			_documentLength = _model.maxScrollPosition - _model.minScrollPosition;
			_buttonAreaWidth = 14;
			_buttonAreaHeight = 25;
			_scrollControllerWidth = 14; 
			_thumbWidth = 10
			_lineScrollSize = (_documentLength - _model.viewPortLength) / 20;
			nudgeAmount = 10;
		}
		
		override protected function get upStateGraphic():Sprite
		{	
			var upArrow:Sprite = new Sprite();
			
			with (upArrow.graphics) {
				beginFill(_buttonBackgroundColor,1);
				drawCircle(6,6,6);
				
				endFill();
				
				beginFill(0x969696,1);
				moveTo(6,3);
				lineTo(6 - 3,9);
				lineTo(6 + 3,9);
				lineTo(6,3);
				endFill();
			}
			return upArrow;
		}
		
		override protected function get overStateGraphic():Sprite
		{	
			var upArrow:Sprite = new Sprite();
			
			with (upArrow.graphics) {
				beginFill(0x737373,1);
				drawCircle(6,6,6);
				
				endFill();
				
				beginFill(0xFFFFFF,1);
				moveTo(6,3);
				lineTo(6 - 3,9);
				lineTo(6 + 3,9);
				lineTo(6,3);
				endFill();
			}
			return upArrow;
		}
		
		override protected function get downStateGraphic():Sprite
		{	
			var upArrow:Sprite = new Sprite();

			with (upArrow.graphics) {
				beginFill(0xFFFFFF,1);
				drawCircle(6,6,6);
				
				endFill();
				
				beginFill(0x000000,1);
				moveTo(6,2);
				lineTo(6 - 3,9);
				lineTo(6 + 3,9);
				lineTo(6,2);
				endFill();
			}
			return upArrow;
		}
		
		override protected function get hitStateGraphic():Sprite
		{	
			var shape:Sprite = new Sprite();

			shape.graphics.beginFill(0x0000FF,1);
			shape.graphics.drawCircle(6,6,6);
			shape.graphics.endFill();
			
			return shape;
		}
	}
}