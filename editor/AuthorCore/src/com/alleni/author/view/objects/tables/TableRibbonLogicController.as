/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.objects.tables
{
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.PortView;
	
	import flash.display.Sprite;
	
	public class TableRibbonLogicController extends TableRibbonController
	{
		public function TableRibbonLogicController(ribbonColumn:TableResizableContainer, expressionColumn:TableResizableContainer, truthPortContainer:Sprite, 
												   table:AbstractTable, index:int, anchor:WireAnchor, context:WorldContainer, role:int)
		{
			super(ribbonColumn, expressionColumn, truthPortContainer, table, index, anchor, context, role);
		}
		
		override protected function PortViewFactory(anchor:WireAnchor):PortView
		{
			return new PortView(anchor, _context, true, 0x313131);
		}
		
		override protected function createNameAndExpression():void
		{
			_ribbonName = new TableRibbonLogicName(_table, _index);
			_ribbonExpression = new TableRibbonLogicExpression(_context, _table, _index);
		}
		
		public function unhighlight():void{
			_ribbonExpression.fillColor = AbstractTableView.GRAY_LIGHT;
			_ribbonExpression.draw();
			
			_ribbonName.fillColor = AbstractTableView.GRAY_LIGHT;
			_ribbonName.draw();
		}
		
		public function highlight():void{
			_ribbonExpression.fillColor = 0xFFFFFF;
			_ribbonExpression.draw();
			
			_ribbonName.fillColor = 0xFFFFFF;
			_ribbonName.draw();
		}
	}
}