package com.alleni.author.view.objects
{
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.DoubleClickAction;
import com.alleni.author.model.objects.LogicObject;
import com.alleni.author.model.objects.LogicObject;
	import com.alleni.author.view.text.RichTextField;
import com.alleni.author.view.ui.MenuButton;
import com.alleni.author.view.ui.MessageCenterView;
	import com.alleni.author.view.ui.PortView;
import com.alleni.author.view.ui.RibbonView;
import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;

	public class LogicObjectView extends GadgetView
	{
		public var viewState:int;
		
		protected var _title:RichTextField;
		protected var _fillColor:uint = 0x000000;
		protected var _powerOffPortView:PortView;
		protected var _powerOnPortView:PortView;
		protected var _width:Number = 0;
		protected var _height:Number = 0;
		protected var deleting:Boolean = false;

		private var minimized:Sprite = null;
		
		public function LogicObjectView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);


			minimized = new Sprite();
			minimized.doubleClickEnabled = true;
			minimized.visible = false;
		}

		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
		{
			var handled:Boolean = super.updateModelProperty(property, oldValue, newValue);
			if(object.loading) {
				return handled;
			}
			switch(property) {
				case "loading":
					if(!object.loading) {
						if(LogicObject(model.value).minimized) {
                            minimize();
                        } else {
                            maximize();
                        }
					}
					return true;
			}
			return handled;
		}
		
		protected function createAndAddChildren():void
		{
			//Overridden by children
		}
		
		protected function addPowerPorts():Sprite{
			//Overridden by children
			return null;
		}
		
		protected function createInitialSections():void
		{
			//Overridden by children
		}
		
		
		override protected function updateStatus():void
		{
			render();
		}
		
		override public function render():void
		{
			super.render();
			
			graphics.clear();

			var borderWidth:Number = LogicObject(model.value).minimized?MessageCenterView.BORDER_WIDTH*5:MessageCenterView.BORDER_WIDTH;
			
			if(deleting){
				graphics.lineStyle(borderWidth, 0xFF0000, 1, true);
			}else if (selected) {
				graphics.lineStyle(borderWidth, MessageCenterView.SELECTED_BORDER_COLOR, 1, true);
			}else{
				graphics.lineStyle(borderWidth, MessageCenterView.BORDER_COLOR, 1, true);
			}
			
			var borderOffset:Number = borderWidth/2;
			graphics.beginFill(_fillColor);
			graphics.drawRoundRect(-borderOffset, -borderOffset, _width+borderWidth, _height+borderWidth, 15, 15);
			if(minimized.parent) {
				minimized.parent.removeChild(minimized);
			}
			if(model && LogicObject(model.value).minimized) {
				addChild(minimized);
				minimized.visible = true;
				minimized.graphics.clear();
				minimized.graphics.beginFill(0x000000, 0.01);
				minimized.graphics.drawRoundRect(-borderOffset, -borderOffset, _width+borderWidth, _height+borderWidth, 15, 15);
				return;
			} else {
				minimized.visible = false;
			}
		}

		protected function makeMinimizeButton():MenuButton {
			var size:Number = Math.min(PortView.WIDTH, RibbonView.HEIGHT);
			var sprite:Sprite = new Sprite();
			sprite.graphics.clear();
			sprite.graphics.lineStyle(0);
			sprite.graphics.beginFill(0x282828);
			sprite.graphics.drawEllipse(2, 2, size-4, size-4);
			sprite.graphics.endFill();
			sprite.graphics.lineStyle(2, 0xA5A5A5);
			sprite.graphics.moveTo(5, 5);
			sprite.graphics.lineTo(size-5, size-5);
			sprite.graphics.moveTo(5, size-5);
			sprite.graphics.lineTo(size-5, 5);
			var result:MenuButton = new MenuButton(function(s:Sprite):void{}, minimize, null, null, null, sprite);
			result.toggle = false;
			result.x = _width - PortView.WIDTH;
			result.y = 0;
			return result;
		}

		protected function minimize():void {
			scaleX = 0.2;
			scaleY = 0.2;
			LogicObject(model.value).minimized = true;
			updateView();
			ApplicationController.instance.wireController.requestRedrawForObject(LogicObject(model.value));
		}

		protected function maximize():void {
			scaleX = 1;
			scaleY = 1;
			LogicObject(model.value).minimized = false;
			updateView();
			ApplicationController.instance.wireController.requestRedrawForObject(LogicObject(model.value));
		}

		override public function get doubleClickAction():int {
			var result:int = super.doubleClickAction;
			if(result == DoubleClickAction.EDIT && LogicObject(model.value).minimized == true) {
				maximize();
				return DoubleClickAction.STOP;
			}
			return result;
		}
	}
}