package com.alleni.author.view.objects
{
import caurina.transitions.Tweener;
import caurina.transitions.properties.DisplayShortcuts;

import com.alleni.author.Navigation.SmartObjectVelumView;
import com.alleni.author.controller.objects.CompositeMediator;
import com.alleni.author.controller.objects.CompositeVelumMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.RectangleObject;
import com.alleni.author.view.DashedRectView;
import com.alleni.author.view.text.LightLabel;
import com.alleni.author.view.ui.WireboardView;
import com.alleni.taconite.view.ViewContext;

import flash.display.BlendMode;
import flash.display.Graphics;
import flash.display.Loader;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;

DisplayShortcuts.init();

public class CompositeVelumView extends SmartObjectVelumView
	{
		
		[Embed(source="/assets/buttons/gadgetPackageButton.swf", mimeType="application/octet-stream")]
		[Bindable]
		private static var closeButtonCls:Class;
		private var _closeButtonLoader:Loader;
		private var _packageCloseButton:MovieClip;
		private var _packageCloseButtonContainer:Sprite = new Sprite();
		private var _closeButtonMouseDown:Boolean;
		
		private var _mediator:CompositeVelumMediator;  // this is not a drag mediator
		
		private var _topText:LightLabel;
		private var _trail:CompositeTrailView;  // breadcrumb trail (little boxes)
		private var _composite:Composite;
		private var _bgColor:uint;
		private var _bgAlpha:Number;
		private var _blendMode:String;
		private var _actualVelum:Sprite = new Sprite();

        public var velumHolder:RectangleObject;

		public static const TOP_MARGIN:Number = 30;
		public static const SIDE_MARGIN:Number = 15;
		public static const PADDING:Number = 15;
		
		private static const TOP_LINE_Y:Number = 20;
		private static const SECOND_LINE_Y:Number = 30;

		
		public function CompositeVelumView(context:ViewContext, composite:Composite, velumHolder:RectangleObject, objMediator:CompositeMediator)
		{
			super(context, composite, objMediator);  // creates wireboards
            _context = context;
			
			_bgColor = 0xE1E1E1;
			_bgAlpha = 0.4;
			_blendMode = BlendMode.NORMAL;
			_holder.addChildAt(_actualVelum,0);  // behind wireboards
			
			_topText = new LightLabel();
			_topText.x = 5-SIDE_MARGIN - PADDING;
			_topText.y = -TOP_MARGIN - PADDING +3; 
			_topText.text = "Making Object";
			_topText.italic = true;
			_topText.color = 0x222222;
			_topText.hitArea = new Sprite();
			_holder.addChild(_topText);

			_packageCloseButtonContainer.graphics.beginFill(0xFFFFFF,0.01);
			_packageCloseButtonContainer.graphics.drawRect(0,0,20,20);
			_closeButtonLoader = new Loader();
			try {  // loader crashes in desktop version
			_closeButtonLoader.loadBytes(new closeButtonCls());
			_closeButtonLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,closeButtonLoaded);
			} catch (e:*) {}; 

			_composite = composite;

			_trail = new CompositeTrailView(context,_composite.model);
			_holder.addChild(_trail);
			_trail.x = 4 - SIDE_MARGIN-PADDING;
			_trail.y = TOP_LINE_Y - TOP_MARGIN-PADDING;

			_mediator = new CompositeVelumMediator(context, this, _composite.getView() as CompositeView, objMediator);
		}

        public function warnViewToDelete():void
        {
            if (_mediator)
                _mediator.clearViewEvents();
        }

		private function closeButtonLoaded(e:Event):void
		{
			_closeButtonLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE,closeButtonLoaded);
			_packageCloseButton = _closeButtonLoader.content as MovieClip;
			_packageCloseButton.gotoAndStop(1);
			
			_packageCloseButtonContainer.addChild(_packageCloseButton);
			_holder.addChild(_packageCloseButtonContainer);

			_packageCloseButtonContainer.addEventListener(MouseEvent.MOUSE_DOWN, handleCloseButtonMouseDown,false,0,true);
			_packageCloseButtonContainer.addEventListener(MouseEvent.MOUSE_UP, handleCloseButtonClick,false,0,true);
			_packageCloseButtonContainer.addEventListener(MouseEvent.MOUSE_OVER, handleCloseButtonMouseOver,false,0,true);
			_packageCloseButtonContainer.addEventListener(MouseEvent.MOUSE_OUT, handleCloseButtonMouseOut,false,0,true);

		}

		override public function drawVelum(_w:Number, _h:Number):void
		{
			if (_perimeterMode) {
				drawPerimeter(_w, _h);
				return;
			}
			
			var w:Number = _w + 2*(SIDE_MARGIN+PADDING);
			var h:Number = _h + 2*PADDING; // extra height to prevent child message centers hitting status text
			
			// make sure we have enough room for the wire ports on the breadboard
			h = Math.max(h, wireboards[0].minHeight, wireboards[1].minHeight);
			h += TOP_MARGIN;

			_actualVelum.x = -SIDE_MARGIN-PADDING;
			_actualVelum.y = -TOP_MARGIN-PADDING;

			var g:Graphics = _actualVelum.graphics;
			g.clear();
			g.beginFill(0xE1E1E1, 0.40/2);
			g.drawRoundRectComplex(0,0,w,h,10,10,0,0);
			g.endFill();
			_actualVelum.blendMode = _blendMode;
			
			// opaque fill under the breadcrumb trail
			g.lineStyle();
			g.beginFill(0xffffff, .9);
			g.drawRect(0, TOP_LINE_Y, w, SECOND_LINE_Y - TOP_LINE_Y);
			g.endFill();
			
			// outline & fill whole velum
			g.lineStyle(1,0xAFAFAF,1.0, true);
			g.beginFill(_bgColor, _bgAlpha);
			g.drawRoundRectComplex(0,0,w,h, 10,10,0,0);
			g.endFill();
			g.beginFill(_bgColor, .66/2);
			g.drawRoundRectComplex(0,0, w, SECOND_LINE_Y, 10,10,0,0);
			g.endFill();
			g.moveTo(0,TOP_LINE_Y);
			g.lineTo(w,TOP_LINE_Y);
			g.moveTo(0,SECOND_LINE_Y);
			g.lineTo(w, SECOND_LINE_Y);

			if (_packageCloseButtonContainer != null) {
				_packageCloseButtonContainer.x = w - 26 - SIDE_MARGIN - PADDING;
				_packageCloseButtonContainer.y = 1- TOP_MARGIN - PADDING;
			}
						
			_topText.width = Math.min(w, 1000);
			
			if ( _wireboards != null ) {
				if ( _wireboards.length > 1 ) {
					_wireboards[0].y = _actualVelum.y + SECOND_LINE_Y;
					_wireboards[1].y = _actualVelum.y + SECOND_LINE_Y;
					_wireboards[0].x = _actualVelum.x;
					_wireboards[1].x = _actualVelum.x + w - WireboardView.WIDTH;
					_wireboards[0].draw(h-SECOND_LINE_Y);
					_wireboards[1].draw(h-SECOND_LINE_Y);
				}
			}
		}
		
		private function drawPerimeter(ww:Number, hh:Number):void
		{
			if (_perimeterRect == null) {
				_perimeterRect = new DashedRectView();
				addChild(_perimeterRect);
			}
			_perimeterRect.draw(ww, hh);
		}
				
		public function set heading(value:String):void
		{
			_topText.text = value;
		}
		
		private function handleCloseButtonMouseOver(e:MouseEvent):void
		{ 
			if (!_closeButtonMouseDown)
				Tweener.addTween(_packageCloseButton,{_frame:11,time:0.5});
			else {
				Tweener.addTween(_packageCloseButton,{_frame:12,time:0.1});
				_closeButtonMouseDown = false;
			}
		}
		
		private function handleCloseButtonMouseOut(e:MouseEvent):void
		{ 
			if (!_closeButtonMouseDown) {
				Tweener.addTween(_packageCloseButton,{_frame:1,time:0.5});
			} else {
				Tweener.addTween(_packageCloseButton,{_frame:12,time:0.1});
				_packageCloseButton.gotoAndStop(12);
			}

		}
		
		private function handleCloseButtonMouseDown(e:MouseEvent):void
		{ 
			e.stopPropagation();
			_closeButtonMouseDown = true;
			Tweener.addTween(_packageCloseButton,{_frame:12,time:0.25});
			_packageCloseButton.gotoAndStop(12);
		}
		
		private function handleCloseButtonClick(e:MouseEvent):void
		{
			e.stopImmediatePropagation();
			ApplicationController.instance.authorController.closeEditingCurrentSmartObject();
		}

        public function get trail():CompositeTrailView
        {
            return _trail;
        }

	}
}
