package com.alleni.author.view.objects.tables
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.AbstractRibbonControl;
import com.alleni.author.view.ui.RibbonView;

import flash.events.MouseEvent;

public class TableRibbonExpression extends TableContainerChild
	{		
		protected var _editor:AbstractRibbonControl; // comparison editor
		
		protected var _table:AbstractTable;
		protected var _index:int;
		protected var _context:WorldContainer;

        private var mouseDownPosX:Number = -10;
        private var mouseDownPosY:Number = -10;
		
		public function TableRibbonExpression(context:WorldContainer, table:AbstractTable, index:int)
		{	
			super();
			
			_context = context;
			_table = table;
			_index = index;
            this.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void{
                if(_editor) {
//                    trace("Caught table ribbon expression MOUSE_DOWN");
//                    trace(mouseX, mouseY);
                    mouseDownPosX = mouseX;
                    mouseDownPosY = mouseY;
                }
            });
            this.addEventListener(MouseEvent.MOUSE_UP, function(event:MouseEvent):void{
                if(_editor) {
//                    trace("Caught table ribbon expression MOUSE_UP...");
//                    trace(mouseDownPosX, mouseX, mouseDownPosY, mouseY);
                    if(Math.abs(mouseDownPosX - mouseX) < 2 && Math.abs(mouseDownPosY - mouseY) < 2) {
//                        trace("... Starting editing");
                        _editor.openEditing();
                    } else {
//                        trace("... Not editing");
                    }

                }
            });
            this.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void{
                if(_editor) {
//                    trace("Caught table ribbon expression click... Starting editing");
                    _editor.openEditing();
                }
            });
		}
		
		protected function setValueCallback(value:Object):void
		{
			if (value == "true") 
				value = true;
			else if (value == "false") 
				value = false;
		}
		
		public function get myValueFromControl():Object
		{
			// overridden by subclasses
			return null;
		}
		
		protected function setValueComponent(prop:PropertyDescription, existing:Boolean):void
		{
			// overridden by subclasses
		}
		
		public function handleWireAdded(title:String, prop:PropertyDescription, existing:Boolean):void
		{
			_fillAlpha = 1;
			draw();

			if(_editor) {
				this.removeChild(_editor);
				_editor = null;
			}
			setValueComponent(prop, existing);
		}

		public function handleWireRemoved(existing:Boolean):void
		{
			_fillAlpha = 1;
			draw();

			if(_editor) {
				this.removeChild(_editor);
				_editor = null;
			}
			setValueComponent(new PropertyDescription("", "", "Property", "", "", 0, false, -1, Modifiers.BOOLEAN_TYPE), existing);
		}

		override public function set width(value:Number):void 
		{
			super.width = value;
			if(_editor){
				_editor.width = width;
			}
		}

		public function handleBecameAddition():void
		{
			_fillAlpha = 0;
			draw();

			if(_editor) {
				this.removeChild(_editor);
				_editor = null;
			}
		}

        public function updateExpression():void{
            _editor.setValue(myValueFromControl, RibbonView.DEFAULT); //This might be weird
        }
	}
}