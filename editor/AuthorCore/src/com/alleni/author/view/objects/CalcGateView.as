package com.alleni.author.view.objects
{
	import com.alleni.author.model.objects.CalcGate;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.BooleanSwitch;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	public class CalcGateView extends CalcView
	{
		private var _booleanSwitch:BooleanSwitch;
		
		public function CalcGateView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
		}
		
		/**
		 * The CalcGate object associated with this view's TaconiteModel.
		 */
		private function get calcGate():CalcGate
		{
			return calc as CalcGate;
		}
				
		override protected function updateEnabledView() : void
		{
			super.updateEnabledView();

			graphics.drawRoundRect(35, -20, 20, 20, 8,8);
		}
		
		protected function createAnchorModels() : void
		{
			// create the models only if necessary (they are permanent)
			if (calcGate.propertyRibbons.length == 0) {
//				createAnchor("calcValue");
//				createAnchor("calcValue2");
//				createAnchor("gateValue");
			}
		}
		
		override protected function createPorts() : void
		{
			var _rightPort:PortView;
			var _leftPort:PortView;
			var _topPort:PortView;
			
			var anchor:WireAnchor;
			
			for each (anchor in calcGate.propertyRibbons) {
				switch (anchor.hostProperty) {
					case "calcValue":
						// left port
						_leftPort = new PortView(anchor, context as WorldContainer);
						_leftPort.x = X_INLET_PORT;
						_leftPort.y = Y_INLET_PORT;
						addChild(_leftPort);
						break;
					case "calcValue2":
						// right port
						_rightPort = new PortView(anchor, context as WorldContainer);
						_rightPort.x = X_VALUE_PORT;
						_rightPort.y = Y_VALUE_PORT;
						addChild(_rightPort);
						break;
					case "gateValue":
						_topPort = new PortView(anchor, context as WorldContainer);
						_topPort.x = 35;
						_topPort.y = -20;
						addChild(_topPort);
						break;
				}
			}
		}
		
		override protected function addComponents() : void
		{	
	/*		_booleanSwitch = new BooleanSwitch(this);
			_booleanSwitch.width = 42;
			_booleanSwitch.firstPosition = 0;
			_booleanSwitch.x = 20;
			_booleanSwitch.y = 3;
			
			addChild(_booleanSwitch);*/
		}
		override protected function changeControl() : void
		{
			_booleanSwitch.setValue(calcGate.gateValue.toString(), RibbonView.INIT);
		}
		
		
		override public function set myValue(value:Object):void
		{
			trace("CalcGateView, myVale="+value);
			if (value=="false")
				CalcGate(model.value).gateValue = false;
			else
				CalcGate(model.value).gateValue = value;
		}
		
		/**
		 * This is how switch buttons set the control value 
		 * @param value
		 * 
		 */
		override public function set myValueFromControl(value:String):void{
			this.myValue = value;
		}
		
	}
}