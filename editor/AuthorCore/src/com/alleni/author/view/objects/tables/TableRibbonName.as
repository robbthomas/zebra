package com.alleni.author.view.objects.tables
{
	import com.adobe.utils.StringUtil;
	import com.alleni.author.controller.text.TextEditMediator;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.util.StringUtils;
	import com.alleni.author.view.text.RichTextField;
import com.alleni.author.view.ui.RibbonValueField;

import flash.events.MouseEvent;

public class TableRibbonName extends TableContainerChild
	{
		protected var _titleLabel:RibbonValueField;
		protected var _titleColor:uint = 0x000000;
		protected var _defaultText:String = "Attach property";
		protected var _table:AbstractTable;
		protected var _index:int;
		private var _textMargin:Number = 25;

        private var mouseDownPosX:Number = -10;
        private var mouseDownPosY:Number = -10;
		
		public function TableRibbonName(table:AbstractTable, index:int)
		{
			super();
			_table = table;
			_index = index;
            this.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void{
                if(_titleLabel) {
//                    trace("Caught table ribbon name MOUSE_DOWN");
//                    trace(mouseX, mouseY);
                    mouseDownPosX = mouseX;
                    mouseDownPosY = mouseY;
                }
            });
            this.addEventListener(MouseEvent.MOUSE_UP, function(event:MouseEvent):void{
                if(_titleLabel) {
//                    trace("Caught table ribbon name MOUSE_UP...");
//                    trace(mouseDownPosX, mouseX, mouseDownPosY, mouseY);
                    if(Math.abs(mouseDownPosX - mouseX) < 2 && Math.abs(mouseDownPosY - mouseY) < 2) {
//                        trace("... Starting editing");
                        _titleLabel.openEditing();
                    } else {
//                        trace("... Not editing");
                    }

                }
            });
		}

		public function set myValueFromControl(value:Object):void
		{
		}

		public function get myValueFromControl():Object
		{
			return _defaultText;
		}
		
		protected function createLabelText():RibbonValueField
		{
            var tx:RibbonValueField = new RibbonValueField(
                    function(val:String):void { myValueFromControl=FormattedText.convertToString(val);},
                    this.width, AbstractTableView.ROW_HEIGHT, false, false, false, Modifiers.STRING_TYPE);

            tx.setValue(myValueFromControl, Modifiers.STRING_TYPE);
            tx.textSize = 14;
			tx.color = _titleColor;
            tx.expandable = false;
			tx.x = 20;
			tx.y = 0;

			return tx;
		}
		
		public function handleWireAdded(title:String, prop:PropertyDescription, existing:Boolean):void
		{
			if(!_titleLabel) {
				_titleLabel = this.createLabelText();
				addChild(_titleLabel);
			}
			_defaultText = title + "\u00ae" + prop.label;
			updateName();
			_fillAlpha = 1;
			draw();
		}

		public function handleWireRemoved(existing:Boolean):void
		{
			if(!_titleLabel) {
				_titleLabel = this.createLabelText();
				addChild(_titleLabel);
			}
            updateName();
			_fillAlpha = 1;
			draw();
		}

		override public function set width(value:Number):void
		{
			super.width = value;
			updateName()
		}
		

		public function handleBecameAddition():void
		{
			if(_titleLabel != null) {
				if(_titleLabel.parent != null) {
					_titleLabel.parent.removeChild(_titleLabel)
				}
				_titleLabel = null;
			}
			_fillAlpha = 0;
			draw();
		}

        public function updateName():void {
            if(_titleLabel) {
				_titleLabel.setValue(truncate(parseTitle(myValueFromControl as String),width - _textMargin), Modifiers.STRING_TYPE);
				_titleLabel.width = width
                _titleLabel.height = AbstractTableView.ROW_HEIGHT;
            }
        }
		
		private function parseTitle(title:String):Array{
			
			var strings:Array = title.split("\u00ae");
			return strings;
		}
		
		private function truncate(strings:Array, cellWidth:Number):String{
			
			var truncatedString:String = "";
			var additionalSpace:Number = 0;
			
			
			if(strings.length == 1){
				strings[0] = StringUtils.truncate(strings[0], cellWidth);
				truncatedString = strings[0];
			}
			
			if(strings.length > 1){
				var tempString:String = strings[0];
				strings[0] = StringUtils.truncate(strings[0], cellWidth * .4);
				truncatedString += strings[0];
				
				//any unused space?
				if(truncatedString.length == tempString.length){
					additionalSpace = cellWidth * .4 - StringUtils.getStringLengthInPixels(truncatedString);
				}
				
			   strings[1] = StringUtils.truncate(strings[1], cellWidth * .6 + additionalSpace);
			   truncatedString += ":" + strings[1];
			}
			
			return truncatedString;
		}
		
		
	}
}