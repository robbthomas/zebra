/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.objects
{
	import com.alleni.author.model.objects.Clock;
	import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;

    /**
     * View of a Clock value object in the world. 
     */
    public class ClockView extends ObjectView
    {
    	private var _hands:Sprite;
    	
        public function ClockView(context:ViewContext, model:TaconiteModel, role:uint)
        {
            super(context, model, role);
			_hands = new Sprite();
			addChild(_hands);
        }
        
        /**
         * The Clock object associated with this view's TaconiteModel.
         */
        public function get clock():Clock
        {
            return model.value as Clock;
        }
        
        /**
         * Update this view by drawing the appropriate graphics.
         */
        override protected function updateEnabledView():void
        {
            super.updateEnabledView();

            rotation = clock.rotation;
            
			// draw outline
			setupCommonAttributes();
	    	graphics.drawEllipse(clock.left, clock.top, clock.width, clock.height);
			graphics.endFill();
			
	    	drawHands();
        }
        
	    private function drawHands():void
	    {	
	    	var radius:Number = (clock.width > clock.height?clock.height:clock.width) / 2;
			
			// draw hands
			_hands.x = clock.left + clock.width / 2;
			_hands.y = clock.top + clock.height / 2;
	    	_hands.graphics.clear();
	    	_hands.graphics.lineStyle(clock.handWeight, clock.handColor, clock.handAlpha / 100);
	    	_hands.graphics.lineTo(0, -radius);
			_hands.graphics.beginFill(clock.handColor, clock.handAlpha / 100);
			_hands.graphics.drawCircle(0, 0, clock.handWeight); // hand base circle
			_hands.rotation = clock.secondsElapsed / clock.duration * 360;
	    }
		
		override protected function updateThumbnail():void
		{
			if (object.complete) {
				super.updateThumbnail();
				_hands.graphics.clear();		
			}
		}
    }
}
