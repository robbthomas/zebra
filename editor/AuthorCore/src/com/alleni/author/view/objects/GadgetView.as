package com.alleni.author.view.objects
{
	import com.alleni.author.controller.ObjectDragMediator;
	import com.alleni.author.controller.ui.MessageCenterDiagramMediator;
	import com.alleni.author.definition.ViewRoles;
import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	public class GadgetView extends ObjectView
	{
		public function GadgetView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			recalcVisible();
		}
		
		override public function warnViewToDelete():void
		{
			super.warnViewToDelete();
		}
		
		override protected function setDragMediator() : void
		{
			if (_role == ViewRoles.PRIMARY) {
				_dragMediator = new ObjectDragMediator(context).handleViewEvents(this, _role);
			} else if (_role == ViewRoles.DIAGRAM) {
				// TODO: Gadgets act like MCs in the diagram window. However, the MC code for adding ribbons doesn't apply
				_dragMediator = new MessageCenterDiagramMediator(context, true).handleViewEvents(this, _role);
			}
		}
				
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
		{
			var handled:Boolean = super.updateModelProperty(property, oldValue, newValue);

			switch (property) {
				case "childOfClosedComposite":  // opening or closing Gadget editing, maybe our grandparent
					recalcVisible();
					return true;
			}
			return handled;
		}
		
		protected function recalcVisible():void
		{
			if (_role == ViewRoles.PRIMARY) {
                // Tables in closed EventPage should show, but not in closed gadget
				super.visible = object.visible && !object.childOfClosedComposite;
			}
		}
		
		override public function set visible(value:Boolean):void
		{
			// ensure that truth-table will not show inside of closed gadget,
			// even when setting initial values or doing view initialize()
			super.visible = value && !object.childOfClosedComposite;
		}

	}
}