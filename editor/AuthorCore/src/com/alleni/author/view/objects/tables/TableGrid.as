package com.alleni.author.view.objects.tables
{
import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.model.objects.StateTable;
	import com.alleni.author.view.WorldContainer;
	
	import mx.collections.IList;

	public class TableGrid extends TableSection
	{	
		protected var _table:AbstractTable;
		protected var _columns:int;
		protected var _rows:int;		
		protected var _rowArray:Array; // Stores an array of labels for each row
		protected var _type:Array = [];
		protected var _cellColor:uint = AbstractTableView.BLUE_DARK;
		protected var _textColor:uint = 0x000000;
		protected var _border:Boolean = true;

		public var tableView:AbstractTableView;
		
		
		public function TableGrid(table:AbstractTable, tableView:AbstractTableView, columns:int, rows:int, initialValues:IList, name:String = "generic", 
								  cellColor:int=-1, textColor:int=-1, border:Boolean=true)
		{
			super();
			
			sectionName = name;
			
			this.tableView = tableView;
			
			_table = table;
			_columns = columns;
			_rows = rows;
			_rowArray = [];
			
			if (cellColor > -1)
				_cellColor = cellColor;
			if (textColor > -1)
				_textColor = textColor;
			
			_border = border;
			
			setSize(_columns, _rows);
            trace("number of cells in container:",this.numChildren);
			if(initialValues != null && initialValues.length > 0) {
				for(var r:int=0; r<_rows; r++) {
					if(_columns < 0) {
                        if(initialValues.length > r){
						    setValue(r, 0, initialValues[r]);
                        }
					} else {
						for(var c:int=0; c<_columns; c++) {
							setValue(r, c, initialValues[r * _columns + c]);
						}
					}
				}
			}
		}

		public function setSize(columns:int, rows:int):void
		{
			_columns = columns;
			_rows = rows;

			var row:Array;
			var n:int;
			for (n=0; n<rows; n++) { 
				if (_rowArray.length < rows) { // if _rowArray doesn't have this many rows yet, add a new row
					row = [];
					_rowArray.push(row);
					_type.push(null);
				}
				growRow(n);
			}
			var cell:TableCell;
			while(_rowArray.length > _rows) {
				row = _rowArray.pop();
				_type.pop();
				for each(cell in row) {
					removeChild(cell);
				}
			}
			if(_rowArray.length > 0 && _columns >= 0) {
				while(_rowArray[0].length > _columns) {
					for each(row in _rowArray) {
						cell = row.pop();
						removeChild(cell);
					}
				}
			}
		}
		
		public function addRow():void
		{
			_table.fireBranches = false;
			_rows++;
			_rowArray.push([]);
			growRow(_rowArray.length-1);
			
			if(_table is StateTable || _table is AnswerTable || this is StateTableGrid){
				for each(var row:Array in _rowArray){
					(row[0] as TableCell).updateEvents();
				}
			}
		}
		
		public function addColumns(columns:int):void
		{
			_columns = columns;
			for (var n:int=0; n<rows; n++) {
				growRow(n);
			}
		}
		
		private function growRow(rowNumber:int):void
		{
			var row:Array = _rowArray[rowNumber]; // The array of embedded labels for this rowNumber
			var labelX:Number = 0; // starting x position of T's and F's
			var gap:int = 1; // add a 5px gap every 4 columns
			
			var n:int = 0;
			for (n=0; n<_columns || (_columns == -1 && n == 0); n++) {
				var existingCell:TableCell = row[n] as TableCell;
				// See if a cell was already created for this column in this row ...
				var cell:TableCell;
				if (existingCell==null) { // .. If not, create a new label and add it to the display.
					var shouldHighlight:Boolean = _table.column == 0 && _columns == -1 || _table.column-1 == n && _columns >= 0;
					if(rowNumber == tableView.currentRow){
						if(tableView.currentSection == sectionName){
							shouldHighlight = true;
						}
					}
					cell = createCell(rowNumber, (_columns==-1)?-1:n, shouldHighlight);
					if(_type[rowNumber]) {
						cell.setType(_type[rowNumber]);
					}
					row.push(cell);
					cell.x = labelX;
					cell.y = rowNumber*AbstractTableView.ROW_HEIGHT;
					addChild(cell);
				}		
				
				labelX += AbstractTableView.COLUMN_WIDTH;
			}
		}
		
		protected function createCell(row:int, column:int, highlighted:Boolean):TableCell
		{
			return new TableCell(_table, this, row, column, highlighted, _cellColor);
		}
		
		public function highlightColumn(column:int):void
		{
			if (!_rowArray) return;
			var n:int;
			var cell:TableCell;
			for (n = 0; n < _rowArray.length; n++) {
				cell = _rowArray[n][column] as TableCell;
				if (cell)
					cell.highlight();
			}
		}
		
		public function unhighlightColumn(column:int):void
		{
			if (!_rowArray || !tableView) return;
			var n:int;
			var cell:TableCell;
			for (n = 0; n < _rowArray.length; n++) {
				if(n != tableView.currentRow || sectionName != tableView.currentSection){
					cell = _rowArray[n][column] as TableCell;
					if (cell)
						cell.unhighlight();
				}
			}
		}
		
		public function highlightRow(row:int):void
		{
			if (!_rowArray) return;
			if (!_rowArray[row]) return;
			var i:int;
			var cell:TableCell;
			for(i = 0; i < (_rowArray[row] as Array).length; i++){
				cell = _rowArray[row][i] as TableCell;
				if (cell)
					cell.highlight();
			}
		}
		
		public function unhighlightRow(row:int):void
		{
			if (!_rowArray) return;
			if (!_rowArray[row]) return;
			var i:int;
			var cell:TableCell;
			for(i = 0; i < (_rowArray[row] as Array).length; i++){
				if(i != _table.column - 1 || columns == -1){
					cell = _rowArray[row][i] as TableCell;
					if (cell)
						cell.unhighlight();
				}
			}
		}
		
		public function setValue(row:int, column:int, value:Object):void 
		{
			if(row >= _rows || (column >= columns && _columns != -1)) {
				return;
			}
			TableCell(_rowArray[row][column]).updateValue(value);
		}
		
		public function getCell(row:int, column:int):TableCell 
		{
			return TableCell(_rowArray[row][column]);
		}
		
		public function get rows():int
		{
			return _rows;
		}
		public function get columns():int
		{
			return _columns;
		}

		public function setType(row:int, modifier:PropertyDescription):void
		{
			_type[row] = modifier;
			for each(var cell:TableCell in _rowArray[row]) {
				cell.setType(modifier, null, _textColor);
			}
            _table.updateForAssetChange();
		}
		
		public function focusCell(row:int, column:int):void
		{
			row %= _rows;
			column %= _columns;
			var cell:TableCell = _rowArray[row][column]  as TableCell;
			cell.focus();
		}

		public function handleRearranged(columns:int, rows:int, initialValues:IList, types:IList=null, constraints:IList=null, context:WorldContainer=null):void
		{
			setSize(columns, rows);
			var row:int;
			if(types != null && constraints != null) {
				for(row=0; row < rows; row++) {
					var prop:PropertyDescription = new PropertyDescription("", "", "", "", "", 0, false, -1, types[row], constraints[row]);
					setType(row, prop);
				}
			}
			if(initialValues != null && initialValues.length > 0) {
				for(row=0; row<_rows; row++) {
					if(_columns < 0) {
						setValue(row, 0, initialValues[row]);
					} else {
						for(var c:int=0; c<_columns; c++) {
							setValue(row, c, initialValues[row * _columns + c]);
						}
					}
				}
			}
		}
	}
}