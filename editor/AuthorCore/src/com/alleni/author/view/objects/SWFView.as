package com.alleni.author.view.objects
{
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.model.objects.SWFObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;

	public class SWFView extends ObjectView
	{
		private var _assetIcon:Sprite;
		private var _swf:Sprite;
		
		public function SWFView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			if (role == ViewRoles.PRIMARY) {
				_swf = new Sprite();
				_swf.width = object.width;
				_swf.height = object.height;
				
				addChild(_swf);
				updateEnabledView();
			}
		}
		
		public function get swfObject():SWFObject
		{
			return model.value as SWFObject;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{	
			var handledModelUpdate:Boolean = super.updateModelProperty(property,oldValue,newValue);	
			
			switch (property) {
				case "asset":
				case "complete":
				case "content":
					if (swfObject.complete && swfObject.asset) {
						updateAsset(); // Update the asset when it is set/reset and the object is complete
						updateStatus();
					}
					break;
				case "height":
					if(_swf)
						_swf.height = newValue as Number;
					break;
				case "width":
					if(_swf)
						_swf.width = newValue as Number;
					break;
			}
			return handledModelUpdate;
		}
		
		override protected function updateEnabledView():void
		{
			super.updateEnabledView();
			if (_swf && _role == ViewRoles.PRIMARY) {
				_swf.x = object.left;
				_swf.y = object.top;
			}
		}
		
		private function updateAsset():void
		{	
			if (_swf == null)
				return;

			while (_swf.numChildren > 0)
				_swf.removeChildAt(0);
			
			var mc:MovieClip = swfObject.content as MovieClip;
			mc.stop();
			_swf.addChild(mc);
			
			_swf.width = object.width;
			_swf.height = object.height;
			updateEnabledView();
		}

        override public function handleRunningChanged():void {
            super.handleRunningChanged();
            if(_assetIcon) {
			    _assetIcon.visible = !Application.running;
            }
        }
    }
}
