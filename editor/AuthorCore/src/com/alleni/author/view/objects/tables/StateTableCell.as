package com.alleni.author.view.objects.tables
{
	import com.alleni.author.controller.objects.IReplaceableReceiver;
	import com.alleni.author.controller.objects.TableHeaderDragMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.action.ModifyTableAction;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractTable;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.IReplaceable;
	
	import mx.collections.IList;
	
	public class StateTableCell extends TableCell implements IReplaceableReceiver
	{
		public function StateTableCell(table:AbstractTable, grid:TableGrid, row:int, column:int, highlighted:Boolean, cellColor:int=-1, border:Boolean=true)
		{
			super(table, grid, row, column, highlighted, cellColor, border);
			updateValue(_table.getStateValue(_row, _column));
		}
		
		override public function set value(val:Object):void
		{
            // called when enter text in cell or drop asset into cell, running or paused.
            // not called when table value changed by wiring
			if(Application.running){
                ApplicationController.instance.authorController.editWhileRunningStart();
            }
            var oldValue:Object = _table.getStateValue(_row, _column);
			_table.setStateValue(_row, _column, val, true);
			ApplicationController.currentActionTree.commit(ModifyTableAction.fromObject(_table, "allStateValues", _row, _column, oldValue));
            if(Application.uiRunning){
                ApplicationController.instance.authorController.editWhileRunningEnd();
            }
		}
		
		public function acceptsReplaceableDrop(newReplaceable:IReplaceable):Boolean
		{
			// same asset type or not wired
			return (_table.stateValueType[_row] == newReplaceable.propertyType || !IList(_table.anchors["currentStateValues"])[_row].wired)
				&& ((this.replaceable && this.replaceable.id != newReplaceable.id) || !this.replaceable); // prevent replacement with identical item
		}
				
		public function get replaceable():IReplaceable
		{
			return _table.getStateValue(_row, _column) as IReplaceable;
		}
		
		public function set replaceable(replaceable:IReplaceable):void
		{
			// dropping asset onto unwired row changes type to match asset
			var type:String = replaceable.propertyType;
			if (type != "" && type != _table.stateValueType[_row]) 
				_table.stateValueType[_row] = type;  // converts all values in row

			value = replaceable;
		}

        public function assetReplaceAll(oldAsset:IReplaceable, newAsset:IReplaceable):void
        {
            if (this.replaceable == oldAsset && acceptsReplaceableDrop(newAsset)) {
                this.value = newAsset;  // creates Undo action
            }
        }

		override protected function get dragSection():String
		{
			return TableHeaderDragMediator.VALUE_ROWS;
		}
	}
}
