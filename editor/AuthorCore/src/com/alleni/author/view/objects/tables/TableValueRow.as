package com.alleni.author.view.objects.tables
{
	
	import com.alleni.author.view.TruthTableScrollView;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.HorizontalScrollContainer;
	import com.alleni.author.view.ui.IconButton;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class TableValueRow extends Sprite
	{
		public static const WIDTH:Number = 123;
		public static const HEIGHT:Number = 16;
		public static const LABEL_START_X:Number = 33;
		public static const FILL_COLOR:uint = 0x9EC8ED;
        public static const MUTED_FILL_COLOR:uint = 0x000000;
		
		private static const LABEL_WIDTH:Number = 73;
		
		private var _container:HorizontalScrollContainer;
		private var _columns:int;
		private var _addButtonFunction:Function;
		private var _removeButtonFunction:Function;
		
		private var _rowSprite:Sprite;
		private var _labelBackground:Sprite;
		private var _text:RichTextField;
		private var _plusButton:IconButton;
		private var _minusButton:IconButton;
		
		public function TableValueRow(scrollController:TruthTableScrollView, columns:int, addButtonFunction:Function, removeButtonFunction:Function)
		{
			super();
						
			_columns = columns;
			
			_addButtonFunction = addButtonFunction;
			_removeButtonFunction = removeButtonFunction;
			
			_rowSprite = new Sprite();
			_container = new HorizontalScrollContainer(_rowSprite, scrollController, AbstractTableView.BLUE_LIGHT);
			_container.x = LABEL_START_X + WIDTH;
			addChild(_container);
			
			_labelBackground = new Sprite();
			_labelBackground.x = LABEL_START_X;
			_plusButton = TableButtons.plusButton;
			_plusButton.x = 0;
			_plusButton.y = 7;
			_plusButton.addEventListener(MouseEvent.CLICK, handlePlusButtonClick);
			
			_minusButton = TableButtons.minusButton;
			_minusButton.x = 15;
			_minusButton.y = 7;
			_minusButton.addEventListener(MouseEvent.CLICK, handleMinusButtonClick);
			
			addChild(_labelBackground);
			addChild(_plusButton);
			addChild(_minusButton);
			
			createLabel();
			createFillLabel();
			
			draw();
			updateRow();
		}
		
		private function handlePlusButtonClick(e:MouseEvent):void
		{
			_addButtonFunction.call(null, this.y+HEIGHT);
		}
		
		private function handleMinusButtonClick(e:MouseEvent):void
		{
			_removeButtonFunction.call(null, this);
		}
		
		private function createLabel():void
		{
			_text = new RichTextField(LABEL_WIDTH, HEIGHT);
			_text.x = LABEL_START_X + WIDTH - LABEL_WIDTH - 3;
			_text.right = true;
			_text.color = 0xFFFFFF;
			_text.text = "Values";
			addChild(_text);
		}
		
		private function createFillLabel():void
		{
			var fillLabel:RichTextField = new RichTextField(WIDTH-LABEL_WIDTH, HEIGHT);
			fillLabel.color = 0x929292;
			fillLabel.italic = true;
			fillLabel.x = LABEL_START_X + 4;
			fillLabel.text = "Fill-";
			
			addChild(fillLabel);
		}
		
		private function draw():void
		{
			graphics.clear();
			drawLabelBackground();
		}
		
		private function drawLabelBackground():void
		{
			_labelBackground.graphics.clear();
			_labelBackground.graphics.beginFill(0x3A3A3A, 0.7);
			_labelBackground.graphics.drawRoundRectComplex(0,0,WIDTH, HEIGHT, 15,0,15,0);
			_labelBackground.graphics.endFill();
			_labelBackground.graphics.beginFill(FILL_COLOR);
			_labelBackground.graphics.drawRoundRectComplex(50,0,LABEL_WIDTH, HEIGHT, 15,0,15,0);
		}
		
		private var _values:Array = [];
		private function updateRow():void
		{
			_rowSprite.graphics.clear();
			_rowSprite.graphics.lineStyle(2, 0xFFFFFF);
			
			var rowWidth:Number = 0;
			
			for (var n:int=0; n<_columns; n++) {
				var valueField:RichTextField = new RichTextField(AbstractTableView.COLUMN_WIDTH, AbstractTableView.ROW_HEIGHT);
				valueField.x = AbstractTableView.COLUMN_WIDTH*n;
				valueField.color = 0xFFFFFF;
				valueField.text = "1";
				_values.push("1");
				// Needs edit mediator
				valueField.background = true;
				valueField.backgroundColor = FILL_COLOR;
				rowWidth += AbstractTableView.COLUMN_WIDTH
				_rowSprite.addChild(valueField);
			}
			
			_rowSprite.graphics.clear();
			_rowSprite.graphics.beginFill(FILL_COLOR);
			_rowSprite.graphics.drawRect(0,0,rowWidth,HEIGHT);
		}
		
		override public function set width(value:Number):void
		{
			_container.width = value;
		}
	}
}