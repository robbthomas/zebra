package com.alleni.author.view.objects
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.controller.text.TextEditMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AuthorNoteMediator;
	import com.alleni.author.event.ScrollUpdateEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ScrollModel;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.TextVerticalScrollView;
	import com.alleni.author.view.VerticalScrollView;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.SelectableView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import flashx.textLayout.container.ScrollPolicy;
	
	public class AuthorNoteView extends SelectableView
	{
		
		private static const HEIGHT_CLOSED:Number	=	15;
		private static const WIDTH_CLOSED:Number	=	15;
		private static const HEIGHT_OPEN:Number		=   150;
		private static const WIDTH_OPEN:Number		=	170;
		
		private var _model:TaconiteModel;
		private var _role:uint;
		private var _mediator:AuthorNoteMediator;
		private var _text:RichTextField;
		private var _object:AbstractObject;
		private var _w:Number; // width of note
		private var _h:Number; // height of note
		private const NOTE_COLOR:uint = 0xffc000;
		
		private var _scrollBar:VerticalScrollView;
		private var _scrollModel:ScrollModel;
		
		
		public function AuthorNoteView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model);
			
			_model = model;
			_role = role;
			
			_w = 20;
			_h = 20;
			
			_object = _model.value as AbstractObject;
			
			_text = new RichTextField(WIDTH_OPEN - 30, HEIGHT_OPEN);
			_text.x = 15;
			_text.y = 15;
			_text.visible = false;
			_text.setCompositionSize(WIDTH_OPEN - 30,  HEIGHT_OPEN);
			_text.verticalScrollPolicy = ScrollPolicy.ON;
			_text.explicitLineBreak = false;
			_text.captureEnter = false;
			setUpScrollBar();
			
			
			var _keyPressFunction:Function = function(charCode:uint):uint{
				
				return charCode;
			};
			
			new TextEditMediator().handleTextEditEvents(_text, TextEditMediator.doubleClickToEdit,
				function(tx:RichTextField,val:String):void{_object.authorNotes=val; addEventListener(Event.ENTER_FRAME, updateScrollBar)}, null );
			
			this.addChild(_text);
			this.addEventListener(MouseEvent.CLICK, handleMouseClick);

			
			
		}
		
		/**
		 * Initialize this view by adding a mediator specific to author note views.
		 */	
		override public function initialize() : void
		{
			super.initialize();
			
			_mediator = new AuthorNoteMediator(context)
			_mediator.handleViewEvents(this, _role);
		}
		
		override protected function updateView() : void
		{
			super.updateView();
			
			updatePositionAndVisibility();
			
			_text.rawText = _object.authorNotes;
			redraw();
		}
		
		private function get computedVisibility():Boolean
		{
			if(_model.parent != null){
                if((_model.parent.value is EventFlow && Application.instance.flowVisible == false) || (!(_model.parent.value is EventFlow) && Application.instance.flowVisible == true)){
                    return false;
                }
                if((_model.parent.value is EventPage && (_model.parent.value as EventPage).editing == false)){
                    return false;
                }
            }
            if (_text.editing)
				return true;
			if (_object.active == false || (!_object.visible && !Application.instance.showAll))
				return false;
			return !(_object.authorNotes == "" || _object.authorNotes == " ");
		}
				
		override protected function updateStatus() : void
		{
			if (!selected)
				open = false;
		}
		
		private function handleMouseClick(e:MouseEvent):void
		{
			open = true;
		}
		
		private function handleRollOut(e:MouseEvent):void
		{
			
			//is mouse cursor still within bounds of the note
		 	var withinBounds:Boolean = isWithinBounds(e.stageX, e.stageY);
				
			if (!_text.editing && !withinBounds){
				open = false;
			this.removeEventListener(MouseEvent.ROLL_OUT, handleRollOut);
			}
		}
		
		private function isWithinBounds(cursorX:Number, cursorY:Number):Boolean{
			
			if((cursorX < x + width && cursorX > x) && (cursorY < y + height && cursorY > y))
				return true;
			
			return false;
		}
		
		private function set open(value:Boolean):void
		{
			_w = (value?WIDTH_OPEN:WIDTH_CLOSED);
			_h = (value?HEIGHT_OPEN + 60:HEIGHT_CLOSED);
			_text.visible = value;
			
			updateScrollBar(null);
			redraw();
			
			if(value){
				this.addEventListener(Event.ENTER_FRAME, prepareToHandleRollOut);
				this.removeEventListener(MouseEvent.CLICK, handleMouseClick);
			}
			else{
				this.addEventListener(MouseEvent.CLICK, handleMouseClick);
			}
		}
		
		private function prepareToHandleRollOut(event:Event):void{
			
			if(width== WIDTH_OPEN){
				this.removeEventListener(Event.ENTER_FRAME, prepareToHandleRollOut);
				this.addEventListener(MouseEvent.ROLL_OUT, handleRollOut);
			}
			
		}
		
		private function redraw():void
		{
			graphics.clear();
			
			graphics.beginFill(NOTE_COLOR, 1);
			
			graphics.moveTo(0,0);
			graphics.lineTo(_w,0);
			graphics.lineTo(_w,_h-(0.14*_h));
			graphics.curveTo(3*(_w/4),_h-(0.18*_h),_w/2,_h)
			graphics.curveTo(_w/4,_h+(0.14*_h),0,_h-(0.04*_h));
			graphics.lineTo(0,0);
			
			graphics.endFill();			
		}
		
		override public function warnViewToDelete() : void
		{
			this.removeEventListener(MouseEvent.ROLL_OVER, handleMouseClick);
			this.removeEventListener(MouseEvent.ROLL_OUT, handleRollOut);
			_mediator.clearViewEvents();
		}
		
		public function updatePositionAndVisibility(event:Event = null):void
		{
			visible = computedVisibility;
			updatePosition();
		}
		
		public function updatePosition():void
		{
			// this function called in the render loop
			if (visible && this.parent) {
				var objView:ObjectView = _object.getView(context) as ObjectView;
				if (objView) {
					var global:Point = objView.computeAuthorNotePosition();
					var worldPoint:Point = this.parent.globalToLocal(global);
					x = worldPoint.x;
					y = worldPoint.y;
				}
			}
		}
		
		private function setUpScrollBar():void{
			
			_scrollModel = new ScrollModel(HEIGHT_OPEN,0,_text.containerHeight);
			_scrollBar = new TextVerticalScrollView(_scrollModel,30,HEIGHT_OPEN);
			_scrollBar.x = WIDTH_OPEN - _scrollBar.width;
			_scrollBar.y = 0;
			_scrollBar.visible = false;  // for now
			_scrollBar.trackBackgroundColor = NOTE_COLOR;
			_scrollBar.addEventListener(ScrollUpdateEvent.SCROLL, handleScrollUpdate);
			
			
			addChild(_scrollBar);
			
		}
		
		private function updateScrollBar(event:Event):void{
			removeEventListener(Event.ENTER_FRAME, updateScrollBar)
		 	_scrollModel.maxScrollPosition = _text.containerHeight;
			
			//is the scrollbar needed and is the author note view open
			if (_scrollModel.maxScrollPosition > HEIGHT_OPEN  && _h > HEIGHT_CLOSED){
				_scrollBar.visible = true;
			}
			else{
				_scrollBar.visible = false;
			}
		}
		
		
		private function handleScrollUpdate(event:ScrollUpdateEvent):void{
			
			if (event && event.params && (event.params.hasOwnProperty("scrollPosition"))) {
				_text.verticalScrollPosition = event.params.scrollPosition;
			}
			
		}
		
	}
}