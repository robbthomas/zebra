package com.alleni.author.view.objects
{
import com.alleni.author.definition.Thumbnails;
import com.alleni.author.model.objects.Drawing;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.events.Event;
import flash.geom.Rectangle;

import org.svgweb.core.SVGViewer;
import org.svgweb.events.SVGEvent;

public class DrawingView extends ObjectView
	{
		[Bindable] [Transient] public var contentUpdated:int = 0;  // increment for each update.  ArenaController watches for this.
		
		public static const SVG_VIEWER_COMPLETE:String = "SVG_viewerComplete";
		private var _viewer:*;
		private var _svgViewOnStage:SVGViewer;  // the viewer is temporarily attached to stage for initial rendering
		private var _fullBitmap:BitmapData;
		private var _tinyBitmap:BitmapData;  // for the 5% scaling bug fix
		private var _bitmapWidth:Number;
		private var _bitmapHeight:Number;  // original bitmap dimensions
		
		
		public function DrawingView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
		}
		
		public function get drawing():Drawing
		{
			return model.value as Drawing;
		}
		
		override protected function updateEnabledView():void
		{
//			trace("updateEnabledView");
			if (_viewer == null && drawing.content != null)
				handleContentChange();
			
			super.updateEnabledView();

			if (_viewer != null) {
				_viewer.alpha = drawing.alpha/100;
				
				var w:Number;
				var h:Number;
				
				if(_viewer is Bitmap)
				{
					w = _bitmapWidth;
					h = _bitmapHeight;
				}else if (_viewer is SVGViewer)
				{
					w = drawing.width;
					h = drawing.height;
				}
				
				switch (drawing.resizeImage) {
					case "native":
					case "fill":
						w = drawing.width;
						h = drawing.height;
						break;
					case "shrink":
					case "enlarge":
						var horzStretch:Number = drawing.width / w;
						var vertStretch:Number = drawing.height / h;
						var stretch:Number = Math.min(horzStretch, vertStretch);  // bounds is "stretch" times bigger than image
						if (stretch > 1 && drawing.resizeImage == "shrink")
							stretch = 1;  // prevent enlargement of image
						w = w * stretch;
						h = h * stretch;
						break;
				}
				
				if (_viewer is Bitmap) {
					updateBitmap(w, h); // use tiny version when necessary
				}
				_viewer.width = w;
				_viewer.height = h;
				_viewer.x = drawing.left + (drawing.width - w) * drawing.anchorPoint.x;  // place the image within the bounds, according to anchor point
				_viewer.y = drawing.top + (drawing.height - h) * drawing.anchorPoint.y;
			}

			/*
			trace("drawing.content:", drawing.content);
			if (drawing.content is XML) {
				trace(XML(drawing.content).toXMLString());
			}

			trace("_viewer="+_viewer);
			if (_viewer is DisplayObjectContainer)
				Utilities.dumpDisplayObjectContainer(_viewer);
			if (_viewer is SVGViewer) {
				trace("viewer xml:");
				trace(_viewer.xml.toXMLString());
				trace("");
				trace("obj.contains(viewer) = "+this.contains(_viewer), "viewer.visible="+_viewer.visible);
			}
			*/
		}

		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
		{
			var result:Boolean = super.updateModelProperty(property,oldValue,newValue);

			switch(property)
			{
				case "content":
					handleContentChange();
					break;
				case "scale":
					if (_viewer is Bitmap) {
						return false;  // force redraw in case tiny bitmap is needed
					}
					break;
			}
			return result;
		}
		
		private function handleContentChange():void
		{
			if(drawing.content)
			{
				if(drawing.content is Bitmap)
				{
					if(_viewer is SVGViewer) 
					{
						removeChild(_viewer);
						_viewer = null;
					}
					if(_viewer == null || _viewer != drawing.content)
					{
						setupBitmap();
						contentUpdated++;
					}
				}else if(drawing.content is XML)
				{ 	
					if(_viewer is Bitmap) 
					{
						removeChild(_viewer);	
						_viewer = null;
					}
					if(_viewer == null || SVGViewer(_viewer).xml != XML(drawing.content))
					{
						if(_viewer && contains(_viewer))
							removeChild(_viewer);
						_viewer = null;
						if (_viewer == null && _svgViewOnStage == null)
							createSVGViewer(drawing.content as XML);  // creates viewer on stage; _viewer will be null until completion
					}
				}
			}
		}
		
		private function setupBitmap():void
		{
			// brand-new view or content changed
			_fullBitmap = (drawing.content as Bitmap).bitmapData;
			_bitmapWidth = _fullBitmap.width;
			_bitmapHeight = _fullBitmap.height;
			_tinyBitmap = null;
			if(_viewer && this.contains(_viewer))
				removeChild(_viewer);
			_viewer = new Bitmap();
            _viewer.smoothing = true;
			addChild(_viewer);
		}
		
		private function updateBitmap(w:Number, h:Number):void
		{
			const CRIT:Number = 0.05;  // critical scaling:  below this scale a flash bug causes bad rendering
			
			// compute the net scaling of this bitmap resulting from all containers
//			var scale:Number = Math.min(w / _fullBitmap.width, h / _fullBitmap.height);
//			var disp:DisplayObjectContainer = this;
//			while (disp) {
//				scale *= Math.min(disp.scaleX, disp.scaleY);
//				disp = disp.parent;
//			}
			
			// use the tiny bitmap if scaling less than CRIT
			var data:BitmapData = _fullBitmap;
//			if (scale < CRIT) {
//				if (_tinyBitmap == null) {
//					var www:int = _fullBitmap.width*CRIT +1;
//					var hhh:int = _fullBitmap.height*CRIT +1;
//					_tinyBitmap = new BitmapData(www, hhh, true, 0);
//					var matrix:Matrix = new Matrix();
//					matrix.scale(CRIT, CRIT);
//					_tinyBitmap.draw(_fullBitmap, matrix, null,null,null, true); // smoothing=true
//				}
//				data = _tinyBitmap;
//			}
			Bitmap(_viewer).bitmapData = data;
            _viewer.smoothing = true;
		}
		
		private function createSVGViewer(xml:XML):void
		{
			/*
				The SVGViewer is added directly to the stage and left there until rendering is complete.
				That is, until the viewer finishes creating all its sprites according to the xml data.
			
				The viewer is sensitive to being removed from the stage, and Zebra normally
				removes a view from the stage momentarily in the process of dropping into a container.
			*/
//			trace("createSVGViewer");
			var viewer:SVGViewer = new SVGViewer();
			viewer.xml = xml;
			viewer.svgRoot.addEventListener(SVGEvent.SVGLoad, handleSVGLoadComplete);
//			drawing.content = viewer.xml; // SVGWeb modifies xml so we set it back here
			viewer.height = drawing.height;
			viewer.width = drawing.width;
			viewer.visible = false;
			context.stage.addChild(viewer);
			_svgViewOnStage = viewer;
			
//			trace("drawing xml:");
//			trace(xml.toXMLString());
//			trace("");
//			trace("viewer xml:");
//			trace(viewer.xml.toXMLString());
//			trace("");
			
			/**
			 * TODO: some svgs contain a 'namedview' node that
			 * has @window-width & @window-height.  These svgs
			 * do not appear to behave correctly with svgweb. 
			 * The symptom is: when you import they properly size
			 * to their viewBox or @height/@width, but when 
			 * attempting to resize, the contents appear to scale
			 * to the 'namedview' dimensions.
			 */
		}
		
		private function handleSVGLoadComplete(e:SVGEvent):void
		{
//			trace("handleSVGLoadComplete");
			// give the SVGViewer one more chance to finish rendering, else it will fail to show the last object
            if (context.stage) {
                context.stage.addEventListener(Event.ENTER_FRAME, handleEnterAfterLoadComplete);
            }
		}
		
		private function handleEnterAfterLoadComplete(e:Event):void
		{
//			trace("handleEnterAfterLoadComplete");
			context.stage.removeEventListener(Event.ENTER_FRAME, handleEnterAfterLoadComplete);
			var viewer:SVGViewer = _svgViewOnStage;
			context.stage.removeChild(viewer);
			_svgViewOnStage = null;
			viewer.visible = true;
			addChild(viewer);
			_viewer = viewer;
			viewer.doubleClickEnabled = true;  // ensure double-click on graphic will edit gadget
			viewer.mouseChildren = false;  // necessary for doubleClick to work
			updateView();
			
			drawing.dispatchEvent(new Event(SVG_VIEWER_COMPLETE));
			contentUpdated++;
		}
		
		override public function warnViewToDelete():void
		{
			super.warnViewToDelete();
			
			if (_svgViewOnStage && context.stage && context.stage.contains(_svgViewOnStage)) 
				context.stage.removeChild(_svgViewOnStage);
			_svgViewOnStage = null;
		}
		
		
		override protected function updateThumbnail() : void
		{
			if (object.complete) {
				if (_thumbnail != null && this.contains(_thumbnail))
					this.removeChild(_thumbnail);
				
				if (drawing.content != null) {
					var displayObject:DisplayObject = drawing.content as DisplayObject;
					if (displayObject) {
						_thumbnail = Thumbnails.generate(displayObject, displayObject.width, displayObject.height, object.thumbnailScale);
						_thumbnail.x = 5;
						_thumbnail.y = Thumbnails.THUMB_NOMINAL_SIZE - _thumbnail.height;
						this.addChild(_thumbnail);
					}
				}
			}
		}

        override public function getBounds(space:DisplayObject):Rectangle
        {
            // ensure that Arena grid layout isn't messed up by the view having null size while its being rendered, such as when replacing an graphic with a new SVG (with replace-all)
            return rectToSpace(object.localBounds, space);
        }

	}
}
