package com.alleni.author.view.objects
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
	
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	public class ControlBar
	{
		
		private const BUTTONWIDTH:Number = 14;
		private const GAP:Number = 3;

		public static const PAUSED:uint = 1;
		public static const PLAYING:uint = 2;
		
		private var _pageText:RichTextField;
		private var _counterText:RichTextField;
		
		private var playButton:SimpleButton;
		private var pauseButton:SimpleButton;
		
		private var _tooltips:Dictionary = new Dictionary();

        public static const UP:String = "up";
        public static const OVER:String = "over";
        public static const NORMAL:String = "normal";

		
		public function ControlBar()
		{
		}
		
		public function makeArenaBar(width:Number):Sprite
		{
            var backgroundBar:Sprite = new Sprite();
            setArenaBarWidth(backgroundBar, width);
			var dragIcon:Sprite = this.drawDragIcon(0x969696);
			dragIcon.x = 4;
			dragIcon.y = 6;
			backgroundBar.addChild(dragIcon);
			return backgroundBar;
		}

        public function setArenaBarWidth(backGroundBar:Sprite, width:Number):void
        {
            drawBar(backGroundBar, width, 21,21,0x000000,2,0xC8C8C8);
        }

		public function makeNavControl(goToBeginningFunction:Function, previousFunction:Function, playFunction:Function, nextFunction:Function, goToEndFunction:Function,
				firstTip:String=null, prevTip:String=null, playTip:String=null, pauseTip:String=null, nextTip:String=null, lastTip:String=null):Sprite
		{
			var navControl:Sprite = new Sprite();
			var navBar:Sprite = createBar(75,14,14,0x484848);
		
			var firstButton:SimpleButton = new SimpleButton();
			firstButton.upState = this.drawEndButton(0x484848,0xC8C8C8);
			firstButton.overState = this.drawEndButton(0xAFAFAF,0x000000);
			firstButton.downState = this.drawEndButton(0xFFFFFF,0x000000);
			firstButton.hitTestState = this.drawEndButton(0xFFFFFF,0x000000);
			firstButton.addEventListener(MouseEvent.CLICK, function(event:Event):void{
				goToBeginningFunction.apply()});
			
			centerButton(navBar, firstButton);
			firstButton.x = GAP;
			
			navBar.addChild(firstButton);
			setupTooltip(firstButton, firstTip);
			
			var previousButton:SimpleButton = new SimpleButton();
			previousButton.upState = this.drawPreviousButton(0x484848,0xC8C8C8);
			previousButton.overState = this.drawPreviousButton(0xAFAFAF,0x000000);
			previousButton.downState = this.drawPreviousButton(0xFFFFFF,0x000000);
			previousButton.hitTestState = this.drawPreviousButton(0xFFFFFF,0x000000);
			previousButton.addEventListener(MouseEvent.CLICK, function(event:Event):void{
				previousFunction.apply()});
			
			centerButton(navBar, previousButton);
			previousButton.x = GAP + 1 * BUTTONWIDTH;
			
			navBar.addChild(previousButton);
			setupTooltip(previousButton, prevTip);

			
			playButton = new SimpleButton();
			playButton.upState = this.drawPlayButton(0x484848,0xC8C8C8);
			playButton.overState = this.drawPlayButton(0xAFAFAF,0x000000);
			playButton.downState = this.drawPlayButton(0xFFFFFF,0x000000);
			playButton.hitTestState = this.drawPlayButton(0xFFFFFF,0x000000);
			playButton.addEventListener(MouseEvent.CLICK, function(event:Event):void{
				playFunction.apply()});
			
			centerButton(navBar, playButton);
			playButton.x = GAP + 2 * BUTTONWIDTH;
			
			navBar.addChild(playButton);
			setupTooltip(playButton, playTip);


			pauseButton = new SimpleButton();
			pauseButton.visible = false;
			pauseButton.upState = this.drawPauseButton(0x484848,0xC8C8C8);
			pauseButton.overState = this.drawPauseButton(0xAFAFAF,0x000000);
			pauseButton.downState = this.drawPauseButton(0xFFFFFF,0x000000);
			pauseButton.hitTestState = this.drawPauseButton(0xFFFFFF,0x000000);
			pauseButton.addEventListener(MouseEvent.CLICK, function(event:Event):void{
				playFunction.apply()});
			
			centerButton(navBar, pauseButton);
			pauseButton.x = GAP + 2 * BUTTONWIDTH;
			
			navBar.addChild(pauseButton);
			setupTooltip(pauseButton, pauseTip);
			
			
			var nextButton:SimpleButton = new SimpleButton();
			nextButton.upState = this.drawNextButton(0x484848,0xC8C8C8);
			nextButton.overState = this.drawNextButton(0xAFAFAF,0x000000);
			nextButton.downState = this.drawNextButton(0xFFFFFF,0x000000);
			nextButton.hitTestState = this.drawNextButton(0xFFFFFF,0x000000);
			nextButton.addEventListener(MouseEvent.CLICK, function(event:Event):void{
				nextFunction.apply()});
			
			centerButton(navBar, nextButton);
			nextButton.x = GAP + 3 * BUTTONWIDTH;
			
			navBar.addChild(nextButton);
			setupTooltip(nextButton, nextTip);
			
			var lastButton:SimpleButton = new SimpleButton();
			lastButton.upState = this.drawEndButton(0x484848,0xC8C8C8);
			lastButton.overState = this.drawEndButton(0xAFAFAF,0x000000);
			lastButton.downState = this.drawEndButton(0xFFFFFF,0x000000);
			lastButton.hitTestState = this.drawEndButton(0xFFFFFF,0x000000);
			lastButton.addEventListener(MouseEvent.CLICK, function(event:Event):void{
				goToEndFunction.apply()});
			
			centerButton(navBar, lastButton);
			lastButton.x = GAP + 4 * BUTTONWIDTH;
			
			navBar.addChild(lastButton);
			setupTooltip(lastButton, lastTip);
			
			
			navControl.addChild(navBar);
			
			return navControl;
		}
		
		public function setupTooltip(button:InteractiveObject, tooltip:String):void
		{
			if (tooltip) {
				_tooltips[button] = tooltip;
				button.addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
				button.addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			}
		}
		
		private function rollOverListener(event:MouseEvent):void
		{
			if (event.target as InteractiveObject) {  // either SimpleButton or RichTextField
				var tempMicahZMI:InteractiveObject = event.target as InteractiveObject;
				var tooltip:String = _tooltips[tempMicahZMI];
				if (tooltip) {
					var global:Point = tempMicahZMI.localToGlobal(new Point(0, tempMicahZMI.height+5));
					ApplicationController.instance.displayToolTip(global,ControlAreaToolTip.DISPLAY_BELOW, 
						tooltip, ControlAreaToolTip.ARROW_HIDDEN);
				}
			}
		}
		
		private function rollOutListener(event:MouseEvent):void
		{
			ApplicationController.instance.hideToolTip();
		}
		
		
		public function makePageControl():Sprite
		{	
			return createBar(34,14,14,0x484848);
        }

        public function addPagingButton(pageBar:Sprite, drawFunc:Function,  actionFunc:Function, tooltip:String):void
        {
            const SPACING:Number = 2;
			var btn:SimpleButton = new SimpleButton();
			btn.upState = drawFunc(UP);
			btn.overState = drawFunc(OVER);
			btn.downState = drawFunc(NORMAL);
			btn.hitTestState = drawFunc(NORMAL);
			btn.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void{
				actionFunc(event.shiftKey, event.altKey)});

            if (pageBar.numChildren > 0) {
                var n:int = pageBar.numChildren-1;
                var previous:DisplayObject = pageBar.getChildAt(n);
                btn.x = previous.x + previous.width + SPACING;
            } else {
                btn.x = SPACING;
            }
			centerButton(pageBar, btn);
			pageBar.addChild(btn);
			setupTooltip(btn, tooltip);
		}
		
		public function makeCounterControl(numTip:String, countTip:String):Sprite
		{
			var counterControl:Sprite = new Sprite();
			var textBar:Sprite = createBar(28,14,0,0x000000);
			var separator:Sprite = this.drawSeparatorIcon(0x484848);
			separator.x = 20;
			textBar.addChild(separator);
			counterControl.addChild(textBar);
			
			//( xPos:Number, yPos:Number, textWidth:Number, textHeight:Number, rightAlign:Boolean=false )
			_pageText = createText(0,2,20,14,true);
			_pageText.x = 0;
			textBar.addChild(_pageText);
			setupTooltip(_pageText, numTip);
			
			_counterText = createText(26,2,20,14,true);
			_counterText.x = 26;
			textBar.addChild(_counterText);
			setupTooltip(_counterText, countTip);

			return counterControl;
			
		}
		
		public function set pageNumber(value:String):void
		{
			_pageText.text = value;
		}
		
		public function set pageCount(value:String):void
		{
			_counterText.text = value;
		}
		
        private function createBar(width:Number, height:Number, radius:Number, color:uint, lineWeight:Number = 0, lineColor:uint = 0x000000):Sprite
        {
            var bar:Sprite = new Sprite();
            drawBar(bar, width, height, radius, color, lineWeight, lineColor);
            return bar;
        }

        private function drawBar(bar:Sprite, width:Number, height:Number, radius:Number, color:uint, lineWeight:Number = 0, lineColor:uint = 0x000000):void
        {
            with (bar.graphics){
                clear();
                lineStyle(lineWeight, lineColor, 1.0, true);
                beginFill(color,1);
                drawRoundRect(0,0,width, height,radius, radius);
                endFill();
            }
        }

		public function drawChip(color:uint):Sprite
		{	
			var chip:Sprite = new Sprite();
			
			with(chip.graphics){
				beginFill(color);
				drawRoundRect(0,0,14,12,5,5);
				endFill();
			}
			return chip;
		}
		
		private function drawPlayButton(chipColor:uint, iconColor:uint):DisplayObject
		{	
			var chip:Sprite = drawChip(chipColor);
			var icon:Shape = new Shape();

			chip.addChild(icon);
			
			with (icon.graphics) {
				beginFill(iconColor);
				moveTo(0,0);
				lineTo(0,8);
				lineTo(6,4);
				lineTo(0,0);
				endFill();
			}
			centerIcon(chip, icon);
			return chip;
		}
		
		private function drawEndButton(chipColor:uint, iconColor:uint):Sprite
		{	
			var chip:Sprite = drawChip(chipColor);
			var icon:Sprite= new Sprite();
			
			chip.addChild(icon);
			
			with(icon.graphics){
				beginFill(iconColor);
				moveTo(0,0);
				lineTo(0,8);
				lineTo(2,8);
				lineTo(2,0);
				lineTo(0,0);
				endFill();
			}
			
			centerIcon(chip, icon);
			return chip;
		}
		
		private function drawPreviousButton(chipColor:uint, iconColor:uint):Sprite
		{	
			var chip:Sprite = drawChip(chipColor);
			var icon:Sprite= new Sprite();
			
			chip.addChild(icon);
			
			with(icon.graphics){
				beginFill(iconColor);
				moveTo(6,0);
				lineTo(0,4);
				lineTo(6,8);
				lineTo(6,6);
				lineTo(3,4);
				lineTo(6,2);
				lineTo(6,0);
				endFill();
			}
			
			centerIcon(chip, icon);
			return chip;
		}
		
		private function drawNextButton(chipColor:uint, iconColor:uint):Sprite
		{	
			var chip:Sprite = drawChip(chipColor);
			var icon:Sprite= new Sprite();
			
			chip.addChild(icon);
			
			with(icon.graphics){
				beginFill(iconColor);
				moveTo(0,0);
				lineTo(6,4);
				lineTo(0,8);
				lineTo(0,6);
				lineTo(3,4);
				lineTo(0,2);
				lineTo(0,0);
				endFill();
			}
			
			centerIcon(chip, icon);
			return chip;
		}

		private function drawDragIcon(iconColor:uint):Sprite
		{	
			var icon:Sprite= new Sprite();
			
			with (icon.graphics){
				lineStyle(1,iconColor);
				moveTo(3,2);
				lineTo(9,2);
				moveTo(3,4);
				lineTo(9,4);
				moveTo(3,6);
				lineTo(9,6);
			}
			
			return icon;
		}
		
		private function drawSeparatorIcon(iconColor:uint):Sprite
		{	
			var icon:Sprite= new Sprite();
			
			with (icon.graphics){
				lineStyle(1.5,iconColor);
				moveTo(3,1);
				lineTo(3,13);
				
			}
		
			return icon;
		}
		
		private function drawPauseButton(chipColor:uint, iconColor:uint):Sprite
		{	
			var chip:Sprite = drawChip(chipColor);
			var icon:Sprite= new Sprite();
			
			chip.addChild(icon);
			
			with (icon.graphics){
				beginFill(iconColor);
				moveTo(4,2);
				lineTo(4,10);
				lineTo(6,10);
				lineTo(6,2);
				lineTo(4,2);
				
				moveTo(8,2);
				lineTo(8,10);
				lineTo(10,10);
				lineTo(10,2);
				lineTo(8,2);
				
				endFill();
			}
			
			//centerIcon(chip, icon);
			return chip;
		}
		
		public function centerIcon(parent:DisplayObject, child:DisplayObject):void
		{	
			child.x = (parent.width - child.width) /2;
			child.y = (parent.height - child.height) /2;
		}
		
		private function centerButton(parent:DisplayObject, child:SimpleButton):void
		{	
			child.y = (parent.height - child.height) /2;
		}
		
		private function createText( xPos:Number, yPos:Number, textWidth:Number, textHeight:Number, rightAlign:Boolean=false ):RichTextField
		{	
			var tx:RichTextField = new RichTextField(textWidth-2,textHeight-2);
			tx.x = xPos + 1;
			tx.y = yPos;
			tx.color = 0xEEEEEE;  // white text
			tx.right = rightAlign;
			tx.setPadding(0,0,0,0);
			return tx;
		}
		
		public function enablePlayButton(state:Boolean):void
		{	
			playButton.upState = state?drawPlayButton(0x484848,0xC8C8C8):drawPlayButton(0x484848,0x7D7D7D);
			playButton.enabled = state;
		}
		
		public function setPlayButtonState(state:uint):void
		{	
			if (state == PAUSED) {
				playButton.visible = true;
				pauseButton.visible = false;
			} else {
				playButton.visible = false;
				pauseButton.visible = true;
			}
		}
	}
}