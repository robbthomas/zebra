package com.alleni.author.view.objects.tables
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
	
	import flash.display.CapsStyle;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class TableBranchingCell extends TableCell
	{
		private var _state:int = 3; // 0 = continue, 1 = pause continue, 2 = try again, 3 = exit, 4 = exit power down.
		private var _icon:Sprite;
		private var _powerSymbol:Sprite = new Sprite();
		private var _toolTipText:String;
		
		public function TableBranchingCell(table:AbstractTable, row:int, column:int, highlighted:Boolean)
		{
			super(table, null, row, column, highlighted);
			
			this.addEventListener(MouseEvent.CLICK, handleClick);
			this.addEventListener(MouseEvent.MOUSE_OVER, showToolTip);
			this.addEventListener(MouseEvent.MOUSE_OUT, hideToolTip);
			
			_icon = new Sprite();
			_icon.x = AbstractTableView.COLUMN_WIDTH/2;
			_icon.y = TableLayoutManager.BRANCHING_SECTION_HEIGHT/2;
			addChild(_icon);
			
			buildBranches();
		}
		
		protected function buildBranches():void{
			if(_icon.numChildren > 0){
				while(_icon.numChildren > 0){
					_icon.removeChildAt(0);
				}
			}

            _icon.graphics.clear();
			
			_icon.graphics.beginFill(AbstractTableView.GRAY_4D);
			_icon.graphics.drawRect(-(AbstractTableView.COLUMN_WIDTH/2), -(TableLayoutManager.BRANCHING_SECTION_HEIGHT/2), AbstractTableView.COLUMN_WIDTH, TableLayoutManager.BRANCHING_SECTION_HEIGHT);
			_icon.graphics.endFill();
			
			_icon.graphics.lineStyle(2, 0xBFBFBF, 1, false, "normal", CapsStyle.NONE);
			
			switch (_table.branches[_column]) {
				case 0:
					if(_table is AnswerTable){
						drawPause();
						_toolTipText = "Pause";
					}else{
						drawContinue();
						_toolTipText = "Continue";	
					}
					break;
				case 1:
					if(_table is AnswerTable){
						drawContinue();
						_toolTipText = "Continue";	
					}else{
						drawPause();
						_toolTipText = "Pause";
					}
					break;
				case 2:
					if(_table is AnswerTable){
						drawTryAgain();
						_toolTipText = "Try again";
					}else{
						drawReplay();
						_toolTipText = "Replay";
					}
					break;
				case 3:
					drawReset();
					_toolTipText = "Reset";
					break;
				case 4:
					drawExitPowerOff();
					_toolTipText = "Exit & power off";
					break;
			}
		}
		
		private function showToolTip(event:MouseEvent):void{
			var point:Point = new Point(AbstractTableView.COLUMN_WIDTH/2, TableLayoutManager.BRANCHING_SECTION_HEIGHT);
			ApplicationController.instance.displayToolTip(this.localToGlobal(point), ControlAreaToolTip.DISPLAY_BELOW, _toolTipText, ControlAreaToolTip.ARROW_HIDDEN);
		}
		
		private function hideToolTip(event:MouseEvent):void{
			ApplicationController.instance.hideToolTip();
		}
		
		override protected function addLabel():void
		{
			// no label needed	
		}
		
		private function handleClick(e:MouseEvent):void
		{
			changeState();
            hideToolTip(null);
            showToolTip(null);
		}

        public function updateIcon():void{
            buildBranches();
        }

		private function changeState():void
		{
			if(_icon.numChildren > 0){
				while(_icon.numChildren > 0){
					_icon.removeChildAt(0);
				}
			}
			_icon.graphics.clear();
			
			_icon.graphics.beginFill(AbstractTableView.GRAY_4D);
			_icon.graphics.drawRect(-(AbstractTableView.COLUMN_WIDTH/2), -(TableLayoutManager.BRANCHING_SECTION_HEIGHT/2), AbstractTableView.COLUMN_WIDTH, TableLayoutManager.BRANCHING_SECTION_HEIGHT - 1);
			_icon.graphics.endFill();
			
			_icon.graphics.lineStyle(2, 0xBFBFBF, 1, false, "normal", CapsStyle.NONE);
			
			switch (_table.branches[_column]) {
				case 0:
					if(_table is AnswerTable){
						drawContinue();
						_table.branches[_column] = 1;
						_toolTipText = "Continue";
					}else{
						drawPause();
						_table.branches[_column] = 1;
						_toolTipText = "Pause";
					}
                    _table.recordInitialValue("branches");
					break;
				case 1:
					if(_table is AnswerTable){
						drawTryAgain();
						_toolTipText = "Try again";
					}else{
						drawReplay();
						_toolTipText = "Replay";
					}
					_table.branches[_column] = 2;
                    _table.recordInitialValue("branches");
					break;
				case 2:
					drawReset();
					_table.branches[_column] = 3;
					_toolTipText = "Reset";
                    _table.recordInitialValue("branches");
					break;
				case 3:
                    if(_table is AnswerTable){
					    drawExitPowerOff();
					    _table.branches[_column] = 4;
					    _toolTipText = "Exit and power off";
                    }else{
                        drawContinue();
						_table.branches[_column] = 0;
						_toolTipText = "Continue";
                    }
                    _table.recordInitialValue("branches");
                    break;
				case 4:
					if(_table is AnswerTable){
						drawPause();
						_table.branches[_column] = 0;
						_toolTipText = "Pause";
					}
                    _table.recordInitialValue("branches");
					break;
			}
		}
		
		private const ICON_WIDTH:Number = AbstractTableView.COLUMN_WIDTH/3;
		
		private function drawReplay():void{
			_icon.graphics.lineStyle(1.5, 0xBFBFBF, 1, false, "normal", CapsStyle.NONE);
			
			_icon.graphics.moveTo(6.5, 3);
			_icon.graphics.lineTo(-8, 3);
			_icon.graphics.lineTo(-8, -3);
			_icon.graphics.lineTo(4, -3);
			
			_icon.addChild(drawArrow1());
			_icon.getChildAt(0).x = 4;
			_icon.getChildAt(0).y = -3;
		}
		
		private function drawReset():void{
			_icon.graphics.beginFill(0xBFBFBF);
			_icon.graphics.drawRect(-12, -3, 6, 6);
			
			_icon.graphics.moveTo(10, -3.5);
			_icon.graphics.lineTo(10, 3.5);
			_icon.graphics.moveTo(1, 0);
			_icon.graphics.lineTo(10, 0);
			
			_icon.addChild(drawArrow1());
			_icon.getChildAt(0).x = 1;
			_icon.getChildAt(0).rotation = 180;
		}
		
		private function drawTryAgain():void
		{
			_icon.graphics.moveTo(-9, -4);
			_icon.graphics.lineTo(-9, 4);
			_icon.graphics.moveTo(-13, 0);
			_icon.graphics.lineTo(-5, 0);
			
			_icon.graphics.moveTo(10, -3.5);
			_icon.graphics.lineTo(10, 3.5);
			_icon.graphics.moveTo(1, 0);
			_icon.graphics.lineTo(10, 0);
			
			_icon.addChild(drawArrow1());
			_icon.getChildAt(0).x = 1;
			_icon.getChildAt(0).rotation = 180;
		}
		
		private function drawExit():void
		{
			_icon.graphics.moveTo(AbstractTableView.COLUMN_WIDTH/2, 2);
			_icon.graphics.lineTo(AbstractTableView.COLUMN_WIDTH/2, ICON_WIDTH-2);
		}
		
		private function drawExitPowerOff():void
		{
			_icon.graphics.lineStyle(1.5, 0xBFBFBF, 1, false, "normal", CapsStyle.NONE);
			_icon.graphics.moveTo(-12, -4);
			_icon.graphics.lineTo(0, -4);
			_icon.graphics.moveTo(-6, -4);
			_icon.graphics.lineTo(-6, 3);
			
			_icon.graphics.drawCircle(8, 0, 4);
			_icon.graphics.moveTo(4, 0);
			_icon.graphics.lineTo(11, 0);
			
			_icon.addChild(drawArrow2());
			_icon.getChildAt(0).x = -6;
			_icon.getChildAt(0).y = 3;
		}
		
		private function drawPause():void{
			_icon.graphics.moveTo(-2 , -3.5);
			_icon.graphics.lineTo(-2, 3.5)
			_icon.graphics.moveTo(2, -3.5);
			_icon.graphics.lineTo(2, 3.5);
		}
		
		private function drawContinue():void
		{
			_icon.graphics.moveTo(-9,0);
			_icon.graphics.lineTo(5,0);
			_icon.addChild(drawArrow1());
			_icon.getChildAt(0).x = 4;
		}

		private function drawArrow1():Sprite{
			var _triangle:Sprite;
			_triangle = new Sprite();
			_triangle.graphics.clear();
			_triangle.graphics.beginFill(0xBFBFBF);
			_triangle.graphics.moveTo(-3.5, -3);
			_triangle.graphics.lineTo(3.5, 0);
			_triangle.graphics.lineTo(-3.5, 3);
			return _triangle;
		}
		
		private function drawArrow2():Sprite{
			var _triangle:Sprite;
			_triangle = new Sprite();
			_triangle.graphics.clear();
			_triangle.graphics.beginFill(0xBFBFBF);
			_triangle.graphics.moveTo(-4, -2.5);
			_triangle.graphics.lineTo(0, 2.5);
			_triangle.graphics.lineTo(4, -2.5);
			return _triangle;
		}
		
		private function drawArrow3():Sprite{
			var _triangle:Sprite;
			_triangle = new Sprite();
			_triangle.graphics.clear();
			_triangle.graphics.beginFill(0xBFBFBF);
			_triangle.graphics.moveTo(-5, -3);
			_triangle.graphics.lineTo(0, 3);
			_triangle.graphics.lineTo(5, -3);
			return _triangle;
		}
		
		override protected function get fillColor():uint
		{
			return 0;
		}
	}
}