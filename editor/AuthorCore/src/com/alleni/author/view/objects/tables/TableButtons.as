package com.alleni.author.view.objects.tables
{
	import com.alleni.author.view.ui.IconButton;
	
	import flash.display.CapsStyle;
	import flash.display.Sprite;

	public class TableButtons
	{
		private static const ICON_GRAY:uint = 0x6A6A6A;
		
		public function TableButtons()
		{
		}
		
		public static function get plusButton():IconButton
		{
			var plusButton:IconButton = new IconButton(getCircleBackground(7), plusIcon, ICON_GRAY, 0, 0, 0, 0xFFFFFF, 0xFFFFFF, 0);
			plusButton.positionIcon(false, -3.5, -3.5);
			
			return plusButton;
		}
		
		public static function get minusButton():IconButton
		{
			var icon:Sprite = new Sprite();
			icon.graphics.lineStyle(2,ICON_GRAY, 1.0, false, "normal", CapsStyle.NONE);
			icon.graphics.moveTo(0,3.5);
			icon.graphics.lineTo(7,3.5);
			
			var minusButton:IconButton = new IconButton(getCircleBackground(7), icon, ICON_GRAY, 0, 0, 0, 0xFFFFFF, 0xFFFFFF, 0);
			minusButton.positionIcon(false, -3.5, -3.5);
			
			return minusButton;	
		}
		
		public static function get collapseButton():IconButton
		{
			const w:Number = 3.0;
			const h:Number = 6;
				
			var icon:Sprite = new Sprite();
		//	icon.graphics.lineStyle(1,ICON_GRAY, 1.0, false, "normal", CapsStyle.NONE,JointStyle.MITER);
			icon.graphics.beginFill(ICON_GRAY);
			
			// left arrow
			icon.graphics.moveTo(w,0);
			icon.graphics.lineTo(0,h/2);
			icon.graphics.lineTo(w,h);
			icon.graphics.lineTo(w,0);
			
			// right arrow
			icon.graphics.moveTo(6,0);
			icon.graphics.lineTo(6+w,h/2);
			icon.graphics.lineTo(6,h);
			icon.graphics.lineTo(6,0);
			
			var collapseButton:IconButton = new IconButton(getCircleBackground(6), icon, ICON_GRAY, 0, 0, 0, 0xFFFFFF, 0xFFFFFF, 0);
			collapseButton.positionIcon(false, -w*3/2, -h/2);
			
			return collapseButton;	
		}
		
//		public static function get valueRowAddButton():IconButton
//		{
//			var plusButton:IconButton = new IconButton(getRoundRectangleBackground(18,12, TableValueRow.MUTED_FILL_COLOR), plusIcon, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, TableValueRow.MUTED_FILL_COLOR,
//															TableValueRow.MUTED_FILL_COLOR, TableValueRow.MUTED_FILL_COLOR);
//
//			return plusButton;
//		}
//
//		public static function get outletRowAddButton():IconButton
//		{
//			var plusButton:IconButton = new IconButton(getRoundRectangleBackground(18,12, TableOutletRow.MUTED_COLOR), plusIcon, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, TableOutletRow.MUTED_COLOR,
//				TableOutletRow.MUTED_COLOR, TableOutletRow.MUTED_COLOR);
//			plusButton.positionIcon(true);
//
//			return plusButton;
//		}
		
		public static function get locateButton():IconButton
		{
			var bg:Sprite = new Sprite();
			var icon:Sprite = new Sprite();
			
			bg.graphics.beginFill(0x000000,0);
			bg.graphics.drawCircle(0,0,5.5);
			
			icon.graphics.lineStyle(2, 0x7D7D7D);
			icon.graphics.drawCircle(0,0,5.5);
			icon.graphics.beginFill(0x7D7D7D);
			icon.graphics.drawCircle(0,0,2); // inner circle
			icon.graphics.endFill();
			
			var iconButton:IconButton = new IconButton(bg,icon,0x7D7D7D,0xB4B4B4,0xFFFFFF);
			
			return iconButton;	
		}
		
		public static function get resetButton():IconButton
		{
			var bg:Sprite = new Sprite();
			var icon:Sprite = new Sprite();
			
			bg.graphics.lineStyle(undefined);   // draw the outline: simple filled circle
			bg.graphics.beginFill(0xB9B9B9);
			bg.graphics.drawCircle(0,0,13/2);  // x,y,radius
			bg.graphics.endFill();
			
			const DIA:Number = 8.322; // diameter of the circle arrow line
			const RAD:Number = DIA/2; // radius 
			
			icon.graphics.lineStyle(1.5, 0,1.0,false,"normal",CapsStyle.NONE);  // design said 1.25 but it looks thin
			icon.graphics.moveTo(DIA, RAD);  // right end of circle line
			icon.graphics.curveTo(DIA,DIA, RAD,DIA);
			icon.graphics.curveTo(0,DIA, 0,RAD);
			icon.graphics.curveTo(0,0, RAD,0);
			
			// draw the triangle arrow
			const W_ARR:Number = 4//3.846;
			const H_ARR:Number = 5//3.149;
			
			icon.graphics.lineStyle(undefined);
			icon.graphics.beginFill(0);
			icon.graphics.moveTo(RAD+W_ARR,0);  // point of arrowhead (back of arrowhead is at end of line)
			icon.graphics.lineTo(RAD,-H_ARR/2); // top corner of arrowhead
			icon.graphics.lineTo(RAD,+H_ARR/2); // bottom corner of arrowhead
			icon.graphics.lineTo(RAD+W_ARR,0);  // back to the tip of the arrow
			icon.graphics.endFill();
			
			var iconButton:IconButton = new IconButton(bg,icon,0x000000,0x000000,0x000000,0xB9B9B9,0xDCDCDC,0xFFFFFF);
			iconButton.positionIcon(false,-4,-4);
			
			return iconButton;
		}
		
		
		private static function get plusIcon():Sprite
		{
			var icon:Sprite = new Sprite();
			icon.graphics.lineStyle(2, 0x7D7D7D, 1.0, false, "normal", CapsStyle.NONE);
			icon.graphics.moveTo(3.5,0);
			icon.graphics.lineTo(3.5,7.5);
			icon.graphics.moveTo(0,3.5);
			icon.graphics.lineTo(7,3.5);
			
			return icon;
		}
		
		private static function getCircleBackground(radius:Number):Sprite
		{
			var bg:Sprite = new Sprite();
			bg.graphics.beginFill(0xFFFFFF);
			bg.graphics.drawCircle(0,0,radius);
			
			return bg;
		}
		
		private static function getRoundRectangleBackground(w:Number, h:Number, color:uint):Sprite
		{
			var bg:Sprite = new Sprite();
			bg.graphics.beginFill(color);
			bg.graphics.drawRoundRect(0,0,w,h,h,h);
			
			return bg;
		}
	}
}