/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/5/11
 * Time: 9:40 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.objects {
import com.alleni.author.view.text.LightLabel;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

public class QueueConnectionView extends SimpleIconedView {

	public function QueueConnectionView(context:ViewContext, model:TaconiteModel, role:uint) {
		super(context, model, role);
		var label:LightLabel = new LightLabel(125);
		label.size = 40;
		label.bold = true;
		label.color = 0x000000;
		label.text = "Queue";
		label.y = -40;
		this.icon = label;
	}
}
}
