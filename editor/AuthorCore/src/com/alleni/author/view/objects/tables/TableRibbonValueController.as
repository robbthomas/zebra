/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.objects.tables
{
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.PortView;
	
	import flash.display.Sprite;
	
	public class TableRibbonValueController extends TableRibbonController
	{
		public function TableRibbonValueController(ribbonColumn:TableResizableContainer, expressionColumn:TableResizableContainer, truthPortContainer:Sprite, 
												   table:AbstractTable, index:int, anchor:WireAnchor, context:WorldContainer, role:int)
		{
			super(ribbonColumn, expressionColumn, truthPortContainer, table, index, anchor, context, role);

			var y:Number = index * AbstractTableView.ROW_HEIGHT;

			_rightPortView = new PortView(anchor, context, true);
			_rightPortView.angle = 0;
			_rightPortView.y = y;
		}
		
		override protected function PortViewFactory(anchor:WireAnchor):PortView
		{
			return new PortView(anchor, _context, true, 0x313131);
		}
		
		override protected function createNameAndExpression():void
		{
			_ribbonName = new TableRibbonValueName(_table, _index);
			_ribbonExpression = new TableRibbonValueExpression(_context, _table, _index);
		}

		override public function handleRearranged(index:int, anchor:WireAnchor, context:WorldContainer):void
		{
			var yPosition:Number = _index * AbstractTableView.ROW_HEIGHT;
			if(_rightPortView.parent != null) {
				_rightPortView.parent.removeChild(_rightPortView);
			}
			_rightPortView = new PortView(anchor, context, true);
			_rightPortView.angle = 0;
			_rightPortView.y = yPosition;
			super.handleRearranged(index, anchor, context);
		}

		override public function remove():void
		{
			super.remove();
			if(_rightPortView != null && _rightPortView.parent != null) {
				_rightPortView.parent.removeChild(_rightPortView);
			}
		}
		
		public function unhighlight():void{
			_ribbonExpression.fillColor = AbstractTableView.BLUE_LIGHT;
			_ribbonExpression.draw();
			
			_ribbonName.fillColor = AbstractTableView.BLUE_LIGHT;
			_ribbonName.draw();
		}
		
		public function highlight():void{
			_ribbonExpression.fillColor = 0xFFFFFF;
			_ribbonExpression.draw();
			
			_ribbonName.fillColor = 0xFFFFFF;
			_ribbonName.draw();
		}
	}
}