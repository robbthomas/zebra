package com.alleni.author.view.objects.tables
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.view.ui.RibbonValueField;
	
	import flash.display.CapsStyle;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import flashx.textLayout.formats.TextAlign;
	
	public class AnswerTableCell extends TableCell
	{
		private var _satisfied:Boolean = false;
		private const _satisfiedColor:uint = 0x5FE675;

		private var _overlay:Sprite;
		private var _iBeamOverlay:Sprite;
		private var _blankOverlay:Sprite;
		private var _markOverlay:Sprite;

        private var marks:Array = ["", AnswerTable.CHECK_MARK, AnswerTable.X_MARK];

        private function isMark():Boolean {
            return marks.indexOf(_value) >= 0;
        }

        private function nextMark():String {
            return marks[(marks.indexOf(_value)+1)%3];
        }
		
		public function AnswerTableCell(table:AnswerTable, grid:TableGrid, row:int, column:int, highlighted:Boolean)
		{
			super(table, grid, row, column, highlighted);
			_defaultColor = AbstractTableView.GRAY_LIGHT;
			
			this.addEventListener(MouseEvent.ROLL_OVER, handleRollOver);
			this.addEventListener(MouseEvent.ROLL_OUT, handleRollOut);

			draw();
		}
		
		private function handleRollOver(e:MouseEvent):void
		{
			showOverlay(true);
		}
		
		private function handleRollOut(e:MouseEvent):void
		{	
			showOverlay(false);
		}

		private function showOverlay(show:Boolean):void
		{
			if (_overlay == null) {
				_overlay = new Sprite();
				_iBeamOverlay = new Sprite();
				_iBeamOverlay.addEventListener(MouseEvent.MOUSE_DOWN, handleIBeamSpaceDown);

				_blankOverlay = new Sprite();
				_blankOverlay.addEventListener(MouseEvent.MOUSE_DOWN, handleBlankSpaceDown);
				
				_markOverlay = new Sprite();
				_markOverlay.addEventListener(MouseEvent.MOUSE_DOWN, handleCheckSpaceDown);
				
				_overlay.addChild(_iBeamOverlay);
				_overlay.addChild(_blankOverlay);
				_overlay.addChild(_markOverlay);
				addChild(_overlay);
				
				drawOverlay();
                draw();
			}

			Tweener.addTween(_overlay, {alpha:show?1:0, transition:"easeOutExpo", time:0.5});
		}
		
		private function handleCheckSpaceDown(e:MouseEvent):void
		{
			e.stopPropagation();
            var val:String = (_value == AnswerTable.CHECK_MARK)?AnswerTable.X_MARK:AnswerTable.CHECK_MARK;
            updateValue(val); // draw it
            value = val; // send it to the model
		}

		private function handleBlankSpaceDown(e:MouseEvent):void
		{
			e.stopPropagation();

            if(_value == AnswerTable.CHECK_MARK || _value == AnswerTable.X_MARK) {
                var val:String = "";
                updateValue(val); // draw it
                value = val; // send it to the model
            } else {
			    focus();
            }
		}
		
		private function handleIBeamSpaceDown(e:MouseEvent):void
		{
            if(_value == AnswerTable.CHECK_MARK || _value == AnswerTable.X_MARK) {
                updateValue(""); // clear the symbol from the editor
            }
			e.stopPropagation();
			focus();
		}
		
		private function drawOverlay():void
		{
			_iBeamOverlay.graphics.beginFill(0xFF0000, .01);
			_iBeamOverlay.graphics.drawRect(0,0,AbstractTableView.COLUMN_WIDTH*1/3,AbstractTableView.ROW_HEIGHT);
			_iBeamOverlay.graphics.endFill();
			var ibx:Number = 5.25;
			var iby:Number = AbstractTableView.ROW_HEIGHT/2;
			_iBeamOverlay.graphics.lineStyle(1,0x787878,1,false,"normal",CapsStyle.ROUND);
			_iBeamOverlay.graphics.moveTo(ibx-2, iby-4);
			_iBeamOverlay.graphics.lineTo(ibx, iby-3.5);
			_iBeamOverlay.graphics.lineTo(ibx+2, iby-4);
			_iBeamOverlay.graphics.moveTo(ibx, iby-3.5);
			_iBeamOverlay.graphics.lineTo(ibx, iby+3.5);
			_iBeamOverlay.graphics.moveTo(ibx-2, iby+4);
			_iBeamOverlay.graphics.lineTo(ibx, iby+3.5);
			_iBeamOverlay.graphics.lineTo(ibx+2, iby+4);
			_iBeamOverlay.graphics.moveTo(ibx-1.25, iby);
			_iBeamOverlay.graphics.lineTo(ibx+1.25, iby);
			_iBeamOverlay.graphics.endFill();

			_blankOverlay.graphics.beginFill(0x0000FF, .01);
			_blankOverlay.graphics.drawRect(0,0,AbstractTableView.COLUMN_WIDTH*1/3,AbstractTableView.ROW_HEIGHT);
			_blankOverlay.graphics.endFill();
			_blankOverlay.x = AbstractTableView.COLUMN_WIDTH*1/3;

			_markOverlay.x = AbstractTableView.COLUMN_WIDTH*2/3;
		}

        private function drawCheck():void {
            _markOverlay.graphics.clear();
			_markOverlay.graphics.beginFill(0x0000FF, .01);
			_markOverlay.graphics.drawRect(0,0,AbstractTableView.COLUMN_WIDTH*1/3,AbstractTableView.ROW_HEIGHT);
			_markOverlay.graphics.endFill();
			var cmx:Number = 5.25;
			var cmy:Number = AbstractTableView.ROW_HEIGHT/2;
			_markOverlay.graphics.beginFill(0x828282);
			_markOverlay.graphics.moveTo(cmx-1,cmy+1);
			_markOverlay.graphics.lineTo(cmx-2.5,cmy-1);
			_markOverlay.graphics.lineTo(cmx-4.5,cmy-1);
			_markOverlay.graphics.lineTo(cmx-1,cmy+4);
			_markOverlay.graphics.lineTo(cmx+5,cmy-4.5);
			_markOverlay.graphics.lineTo(cmx+3,cmy-4.5);
			_markOverlay.graphics.lineTo(cmx-1,cmy+1);
			_markOverlay.graphics.endFill();
        }

        private function drawX():void {
            _markOverlay.graphics.clear();
			_markOverlay.graphics.beginFill(0x0000FF, .01);
			_markOverlay.graphics.drawRect(0,0,AbstractTableView.COLUMN_WIDTH*1/3,AbstractTableView.ROW_HEIGHT);
			_markOverlay.graphics.endFill();
			var cmx:Number = 5.25;
			var cmy:Number = AbstractTableView.ROW_HEIGHT/2;
			_markOverlay.graphics.beginFill(0x828282);

            // top left corner
            _markOverlay.graphics.moveTo(cmx - 5.0, cmy - 4.5);

            // top edge
            _markOverlay.graphics.lineTo(cmx - 3.0, cmy - 4.5);
            _markOverlay.graphics.lineTo(cmx - 0.0, cmy - 1.0);
            _markOverlay.graphics.lineTo(cmx + 3.0, cmy - 4.5);

            // right edge
            _markOverlay.graphics.lineTo(cmx + 5.0, cmy - 4.5);
            _markOverlay.graphics.lineTo(cmx + 1.0, cmy - 0.0);
            _markOverlay.graphics.lineTo(cmx + 5.0, cmy + 4.5);

            // bottom edge
            _markOverlay.graphics.lineTo(cmx + 3.0, cmy + 4.5);
            _markOverlay.graphics.lineTo(cmx - 0.0, cmy + 1.0);
            _markOverlay.graphics.lineTo(cmx - 3.0, cmy + 4.5);

            // left edge
            _markOverlay.graphics.lineTo(cmx - 5.0, cmy + 4.5);
            _markOverlay.graphics.lineTo(cmx - 1.0, cmy - 0.0);
            _markOverlay.graphics.lineTo(cmx - 5.0, cmy - 4.5);

			_markOverlay.graphics.endFill();
        }
		
		override protected function addLabel():void
		{
			super.addLabel();
			RibbonValueField(_editor).textAlign = TextAlign.CENTER;
			RibbonValueField(_editor).expandable = false;
            RibbonValueField(_editor).bold = true;
            RibbonValueField(_editor).textSize = 12;
		}
		
		override protected function get fillColor():uint	
		{	
			var stateColor:uint = _highlighted?_highlightColor:AbstractTableView.GRAY_LIGHT;
			var color:uint = ((_value == "" || _satisfied)?_satisfiedColor:stateColor);
			return color;
		}
		
		public function set satisfied(value:Boolean):void
		{
			_satisfied = value;
			draw();
		}

        override protected function draw():void {
            super.draw();

			RibbonValueField(_editor).closeEditing(null);
            RibbonValueField(_editor).text = String(_value);

            if(_overlay) {
                if(_value == AnswerTable.CHECK_MARK) {
                    drawX();
                } else {
                    drawCheck();
                }
            }
        }

        override public function updateValue(value:Object):void
		{
            if(_value == value) {
                return;
            }
            _value = value;

			draw();
		}
		
		override public function set value(val:Object):void
		{
            _value = val;
			AnswerTable(_table).setLogicValue(_row, _column, val as String, true);
		}
	}
}