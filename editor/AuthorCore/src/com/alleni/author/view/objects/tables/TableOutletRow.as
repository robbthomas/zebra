package com.alleni.author.view.objects.tables
{
	import com.alleni.author.event.ScrollUpdateEvent;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.TruthTableScrollView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.HorizontalScrollContainer;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import mx.collections.IList;


	public class TableOutletRow extends HorizontalScrollContainer
	{
		public static const LABEL_WIDTH:Number = 37;
		public static const WIRE_BAY_WIDTH:Number = 14;
		public static const COLOR:uint = 0xF76B00;
        public static const MUTED_COLOR:uint = 0x000000;
		private static const PORT_X_OFFSET:Number = AbstractTableView.COLUMN_WIDTH/2 - PortView.WIDTH/2;

		private var _scrollX:Number = 0;
		private var _ports:Vector.<PortView> = new Vector.<PortView>();
		
		private var _model:TaconiteModel;
		private var _context:ViewContext;
		private var _columns:int;
		
		public function TableOutletRow(model:TaconiteModel, context:ViewContext, columns:int, scrollController:TruthTableScrollView)
		{
			super(null, scrollController, 0);
			height = RibbonView.HEIGHT;
			//width = scrollController.viewPortWidth;
			width = scrollController.model.viewPortLength;
			
			_model = model;
			_context = context;
			_columns = columns;

			updateRow();
		}
		
		private function updateRow():void
		{
			for (var n:int=0; n<_columns; n++) {
				// Add wire anchors
				createPort(n);
			}			
		}

		public function handleRearranged():void {
			for each(var port:PortView in _ports) {
				removeChild(port);
			}
			_ports = new Vector.<PortView>();
			_columns = IList(AbstractTable(_model.value).anchors["colPort"]).length;
			updateRow();
		}
		
		public function addPorts(totalColumns:int):void
		{
			for (var n:int=0; n<totalColumns; n++) {
				if (n>_columns-1) {
					createPort(n);
				}
			}
			_columns = totalColumns;
		}

		private function getX(col:int):Number {
			var result:Number =  col * AbstractTableView.COLUMN_WIDTH + PORT_X_OFFSET - _scrollX;
			if(result < -PortView.WIDTH/2) {
				result = -(4 + WIRE_BAY_WIDTH/2 + PortView.WIDTH/2);
			} else if (result > width - PortView.WIDTH/2) {
				result = width + (3 + WIRE_BAY_WIDTH/2 - PortView.WIDTH/2);
			}
			return result;
		}
		
		private function createPort(columnNumber:int):void
		{
			var anchor:WireAnchor = IList(AbstractTable(_model.value).anchors["colPort"])[columnNumber];
			var portView:PortView = new PortView(anchor, _context as WorldContainer, true);
			portView.x = getX(columnNumber);
			portView.angle = 90;
			addChild(portView);
			_ports.push(portView);
		}

		private function movePorts():void {
			for(var i:int = 0; i<_ports.length; i++) {
				_ports[i].x = getX(i);
			}
		}

		override public function set width(value:Number):void {
			super.width = value;
			movePorts();
		}

		override protected function handleScroll(event:ScrollUpdateEvent):void
		{
			_scrollX = event.params.scrollPosition;
			movePorts();
		}

		override public function get height():Number
		{
			return AbstractTableView.ROW_HEIGHT;
		}
	}
}