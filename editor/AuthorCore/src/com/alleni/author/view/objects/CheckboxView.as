/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.view.objects
{
	import com.alleni.author.model.objects.Checkbox;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	
	public class CheckboxView extends ToggleButtonView
	{
		
		public function CheckboxView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
		}
		
		/**
		 * Override to draw indicator for selected state.
		 *  
		 * @param sprite
		 * 					The sprite instance that this method draws into.
		 * 
		 */
		override protected function drawSelectVector(sprite:Sprite):void
		{
			var margin:Number = btn.width > btn.height ? btn.height * .20 : btn.width * .20;
			
			sprite.graphics.clear();
			sprite.graphics.lineStyle(Checkbox(btn).checkWeight, btn.checkColor, btn.checkOpacity/100);
			if(Checkbox(btn).checkMark) {
				sprite.graphics.moveTo(btn.left + margin, btn.top + btn.height*.6);
				sprite.graphics.lineTo(btn.left+btn.width*.35,btn.top+btn.height-margin);
				sprite.graphics.curveTo(btn.left+(btn.width*.4),btn.top+(btn.height*.4),btn.left+btn.width-margin, btn.top+btn.height*.15);
			} else {
				sprite.graphics.moveTo(btn.left + margin, btn.top + margin);
				sprite.graphics.lineTo(btn.right - margin, btn.bottom - margin);
				sprite.graphics.moveTo(btn.left + margin, btn.bottom - margin);
				sprite.graphics.lineTo(btn.right - margin, btn.top + margin);
			}
			
		}
	}
}