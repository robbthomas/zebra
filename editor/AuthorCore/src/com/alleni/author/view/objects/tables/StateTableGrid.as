package com.alleni.author.view.objects.tables
{
	import com.alleni.author.controller.objects.TableHeaderDragMediator;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.view.WorldContainer;
	
	import mx.collections.IList;

	public class StateTableGrid extends TableGrid
	{
		public function StateTableGrid(table:AbstractTable, tableView:AbstractTableView, columns:int, rows:int, initialValues:IList, 
									   cellColor:int=-1, textColor:int=-1, border:Boolean=true)
		{
			super(table, tableView, columns, rows, initialValues, TableHeaderDragMediator.VALUE_ROWS, cellColor, textColor, border);

            for(var row:int=0; row<rows; row++) {
                setType(row, new PropertyDescription("", "", "", "", "", 0, false, -1, table.stateValueType[row], table.stateValueTypeConstraints[row]))
            }
		}
		
		override protected function createCell(row:int, column:int, highlighted:Boolean):TableCell
		{
			return new StateTableCell(_table, this, row, column, highlighted, _cellColor, _border);
		}
	}
}