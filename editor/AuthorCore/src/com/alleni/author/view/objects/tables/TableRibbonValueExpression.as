package com.alleni.author.view.objects.tables
{
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.Modifiers;
    import com.alleni.author.definition.PropertyDescription;
    import com.alleni.author.definition.action.ModifyObjectProperty;
    import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.view.WorldContainer;
    import com.alleni.author.view.ui.RibbonValueField;

	public class TableRibbonValueExpression extends TableRibbonExpression
	{
		
		public function TableRibbonValueExpression(context:WorldContainer, table:AbstractTable, index:int)
		{
			super(context, table, index);
			
			_fillColor = AbstractTableView.BLUE_DARK;
		}
		
		override protected function setValueComponent(prop:PropertyDescription, existing:Boolean):void
		{
			var value:* = false;
			if(_table.stateValueExpressions && _table.stateValueExpressions.length > _index) {
				value = _table.stateValueExpressions[_index];
			} else {
				value = "";
			}

			if(!_editor) {
				_editor = new RibbonValueField(setValueCallback, this.width, AbstractTableView.ROW_HEIGHT, false, false, false, Modifiers.STRING_TYPE);
				RibbonValueField(_editor).color = 0x000000;
				this.addChild(_editor);
			}

			_editor.setValue(value, Modifiers.STRING_TYPE);
		}

		override public function get myValueFromControl():Object
		{
			return _table.stateValueExpressions[_index];
		}
		
		override protected function setValueCallback(value:Object):void
		{
            var oldValue:Object = _table.stateValueExpressions[_index];
            if(oldValue == value) {
                return;
            }
			_table.stateValueExpressions[_index] = value;
            _table.recordInitialValue("stateValueExpressions");
            ApplicationController.currentActionTree.commit(ModifyObjectProperty.fromObject(_table, "set table state value expression", "stateValueExpressions", _index, oldValue));
		}
		
		override public function handleWireAdded(title:String, prop:PropertyDescription, existing:Boolean):void
		{
//			if(prop.type == Modifiers.UNSIGNED_NUMBER_TYPE || prop.type == Modifiers.NUMBER_TYPE) {
//				_table.stateValueExpressions[_index] = "col - 1"
//			}
			if(_table.stateValueTypeConstraints[_index] != prop.constraints) {
				_table.stateValueTypeConstraints[_index] = prop.constraints;
			}
			
			if(_table.stateValueType[_index] != prop.type) {
				_table.stateValueType[_index] = prop.type;
			}
			
			super.handleWireAdded(title, prop, existing);
		}
	}
}