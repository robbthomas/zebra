/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.view.objects
{
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	public class PushButtonView extends ToggleButtonView
	{
		public function PushButtonView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
		}
		
		/**
		 * Override to position text for radio button.
		 *  
		 * @param text
		 * 				The RichTextField instance that this component uses.
		 * 
		 */
		override protected function positionText():void
		{
			// Sync the view text with the model text.
			textField.setCompositionSize(btn.width, btn.height-(btn.margin*2));
			textField.setPadding(btn.margin);
			// Calculate the label position, centered in the button 
			// (depends on a render of the text to produce a height value, setting "textHeight")
			textField.x = btn.left;
			textField.y = btn.top + btn.margin;
		}
	}
}