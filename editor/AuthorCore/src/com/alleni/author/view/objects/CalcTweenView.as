package com.alleni.author.view.objects
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.model.objects.CalcTween;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.controls.PopOutMenu;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class CalcTweenView extends CalcView
	{
		public static const HEIGHT:Number = 40;
		
		private static const X_TIME_FIELD:Number = 65;
		private static const Y_TIME_FIELD:Number = 20;
		
		private var _myHeight:Number = 40;
		
		private var _popOut:PopOutMenu;
		private var _tweenTime:RichTextField;
		
		private var _transitionTypes:Array = [
			{label:"none", data:"none"},						{label:"linear", data:"linear"},					{label:"easeInSine", data:"easeInSine"},	
            {label:"easeOutSine", data:"easeOutSine"},			{label:"easeInOutSine", data:"easeInOutSine"},			{label:"easeInQuad", data:"easeInQuad"},
			{label:"easeOutQuad", data:"easeOutQuad"},			{label:"easeInOutQuad", data:"easeInOutQuad"},		{label:"easeInCubic", data:"easeInCubic"},
			{label:"easeOutCubic", data:"easeOutCubic"},		{label:"easeInOutCubic", data:"easeInOutCubic"},	{label:"easeInQuart", data:"easeInQuart"},	
			{label:"easeOutQuart", data:"easeOutQuart"},		{label:"easeInOutQuart", data:"easeInOutQuart"},	{label:"easeInQuint", data:"easeInQuint"},	
			{label:"easeOutQuint", data:"easeOutQuint"},		{label:"easeInOutQuint", data:"easeInOutQuint"},	{label:"easeInExpo", data:"easeInExpo"},
			{label:"easeOutExpo", data:"easeOutExpo"},			{label:"easeInOutExpo", data:"easeInOutExpo"},		{label:"easeInCirc", data:"easeInCirc"},		
			{label:"easeOutCirc", data:"easeOutCirc"},			{label:"easeInOutCirc", data:"easeInOutCirc"},		{label:"easeInElastic", data:"easeInElastic"},	
			{label:"easeOutElastic", data:"easeOutElastic"},	{label:"easeInOutElastic", data:"easeInOutElastic"},{label:"easeInBack", data:"easeInBack"},	
			{label:"easeOutBack", data:"easeOutBack"},			{label:"easeInOutBack", data:"easeInOutBack"},		{label:"easeInBounce", data:"easeInBounce"},	
			{label:"easeOutBounce", data:"easeOutBounce"},		{label:"easeInOutBounce", data:"easeInOutBounce"},	
		]

		
		public function CalcTweenView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
		}

		/**
		 * The CalcTween object associated with this view's TaconiteModel.
		 */
		private function get calcTween():CalcTween
		{
			return calc as CalcTween;
		}
		
		override protected function get myHeight() : Number
		{
			return _myHeight;
		}
		
		override protected function addComponents() : void
		{
			super.addComponents();
			
			// Tweener components
			
			_popOut = new PopOutMenu(EditorUI.instance);
			_popOut.closedHeight = 15;
			_popOut.closedWidth = 55;
			_popOut.labelSize = 11;
			_popOut.popWidth = 105;
			_popOut.dataProvider = _transitionTypes;
			_popOut.defaultIndex = findMenuItem(_transitionTypes, calcTween.tweenType);
			_popOut.baseColor = 0x191919;
			_popOut.borderColor = 0x000000;
			_popOut.arrowScale = .75;
			_popOut.baseLabelOffsetX = -5;
			_popOut.baseLabelOffsetY = -3;
			_popOut.arrowOffsetX = 3;
			_popOut.baseLabelWidthAdjustment = -10;
			_popOut.direction = PopOutMenu.DIRECTION_BI;
			_popOut.drawComponent();
			_popOut.visible = true;
			_popOut.x = 5;
			_popOut.y = 20;
			
			_popOut.addEventListener(PopOutMenu.ITEM_CHANGED, itemChanged);
			
			_tweenTime = new RichTextField(20,20);
			_tweenTime.explicitLineBreak = true;
			_tweenTime.text = calcTween.tweenTime.toString();
			drawValueBackground(new Sprite(),20,15,X_TIME_FIELD,Y_TIME_FIELD);
			addChild(_tweenTime);
			_tweenTime.color = 0xFFFFFF;
			_tweenTime.x = X_TIME_FIELD;
			_tweenTime.y = Y_TIME_FIELD;
			new TextEditMediator().handleTextEditEvents(_tweenTime,null,  
					function(tx:RichTextField,val:String):void{tweenTime=Number(val)} );
			
			addChild(_popOut);
		}
		
		private function findMenuItem(dataArray:Array, matchData:String):int
		{
			for (var n:int=0; n < dataArray.length; n++) {
				var record:Object = dataArray[n];
				if (record.data == matchData)
					return n;
			}	
			return 0;
		}
		
		override protected function createPorts():void
		{
			_leftPort = new PortView(calc.anchors["calcInlet"], context as WorldContainer);
			_leftPort.angle = 180;
			addChild(_leftPort);
			
			_rightPort = new PortView(calc.anchors["calcTweenValue"], context as WorldContainer);
			_rightPort.x = 70;
			addChild(_rightPort);
		}
		
		private function itemChanged(event:Event):void{
			tweenType = _popOut.selectedItem.data as String;
		}
		
		public function set tweenTime(value:Number):void
		{
			calcTween.tweenTime = value;
		}
		
		public function set tweenType(value:String):void
		{
			calcTween.tweenType = value;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{
			var result:Boolean = super.updateModelProperty(property,oldValue,newValue);
			
			// handling property changes is needed when doing copy/paste of a tween object
			switch(property) {
				case "tweenTime":
					_tweenTime.text = newValue.toString();
					break;
				case "tweenType":
					_popOut.selectedIndex = findMenuItem(_transitionTypes, calcTween.tweenType);
					break;
			}
			return result;
		}
	}
}