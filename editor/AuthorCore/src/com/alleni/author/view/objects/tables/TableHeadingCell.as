package com.alleni.author.view.objects.tables
{
import com.alleni.author.controller.objects.TableHeaderDragMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.action.ModifyRibbonAction;
import com.alleni.author.model.objects.AbstractTable;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.text.RichLabel;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextFieldAutoSize;
	
	public class TableHeadingCell extends TableCell
	{
		private var _label:RichLabel;
		
		public function TableHeadingCell(table:AbstractTable, row:int, column:int, highlighted:Boolean, tableView:AbstractTableView=null)
		{
			super(table, null, row, column, highlighted, AbstractTableView.GRAY_4D, column<0?false:true);
			
			if(tableView && column >= 0) {
				// zero column cant be edited
				this.mouseChildren = false;
				buildRollover();
				// and is dragable
				new TableHeaderDragMediator(this, tableView, TableHeaderDragMediator.COLUMNS, column, handleClick);
			}
			
			_defaultColor = AbstractTableView.GRAY_4D;
		}
		
		override public function updateEvents():void{
			if(_table.numColumns > 1){
				addEventListener(MouseEvent.ROLL_OVER, handleRollOver);
				addEventListener(MouseEvent.ROLL_OUT, handleRollOut);
                if(this.contains(_inviteSprite)){
                    this.removeChild(_inviteSprite);
                }
				buildRollover();
			}
		}

        public function muteRollOver():void{
            removeEventListener(MouseEvent.ROLL_OVER, handleRollOver);
            removeEventListener(MouseEvent.ROLL_OUT, handleRollOut);
            if(this.contains(_inviteSprite)){
                this.removeChild(_inviteSprite);
            }
        }

		override protected function addLabel():void
		{
			_label = new RichLabel();
			_label.height = AbstractTableView.ROW_HEIGHT;
			_label.autoSize = TextFieldAutoSize.CENTER;
			_label.text = (_column+1).toString();
			_label.y = -2;
			_label.center = true;
			_label.color = 0xFFFFFF;
			_label.bold = true;
			
			_label.x = (AbstractTableView.COLUMN_WIDTH - _label.width)/2
			
			addChild(_label);
		}
		
		override public function updateValue(value:Object):void
		{
			if (_label)
				_label.text = value.toString();
			
			super.updateValue(value);
		}
		
		private function handleClick(e:MouseEvent):void
		{
			_table.fireBranches = false;
			if(Application.running){
                ApplicationController.instance.authorController.editWhileRunningStart();
            }
            var oldValue:int = _table.column;
			_table.column = _column+1;
			ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(_table, "activeColumn", oldValue));
            if(!Application.running){
                ApplicationController.instance.authorController.editWhileRunningEnd();
            }
		}
		
		private function handleRollOver(event:Event):void{
            if(_table.numColumns > 1){
			    addChild(_inviteSprite);
            }
		}
		
		private function handleRollOut(event:Event):void{
			if(contains(_inviteSprite)){
				removeChild(_inviteSprite);
			}
		}
		
		public override function highlight():void{
			_label.color = 0x000000;
			super.highlight();
		}
		
		public override function unhighlight():void{
			_label.color = 0xFFFFFF;
			super.unhighlight();
		}
		
		private function buildRollover():void{
			_inviteSprite = new Sprite();
			_inviteSprite.graphics.beginFill(0xFFFFFF);
			_inviteSprite.graphics.drawRect(0,0,width,AbstractTableView.COLUMN_HEADING_HEIGHT);
			
			if(_table.numColumns > 1){
				if(_column > 0){
					var leftArrow:Sprite = drawArrow();
					leftArrow.rotation = 180;
					leftArrow.y = AbstractTableView.COLUMN_HEADING_HEIGHT/2;
					leftArrow.x = _label.x - 3;
					_inviteSprite.addChild(leftArrow);
				}
				if(_column < _table.numColumns - 1){
					var rightArrow:Sprite = drawArrow();
					rightArrow.y = AbstractTableView.COLUMN_HEADING_HEIGHT/2;
					rightArrow.x = _label.x + _label.width + 2
					_inviteSprite.addChild(rightArrow);
				}
			}
			_inviteSprite.alpha = .5;
		}
		
		private function drawArrow():Sprite{
			var arrow:Sprite = new Sprite();
			arrow.graphics.beginFill(0x0000FF);
			arrow.graphics.moveTo(-1,-3);
			arrow.graphics.lineTo(3,0);
			arrow.graphics.lineTo(-1,3);
			arrow.graphics.endFill();
			return arrow;
		}
	}
}