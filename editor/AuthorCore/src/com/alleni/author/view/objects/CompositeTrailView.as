package com.alleni.author.view.objects
{
	import com.alleni.author.controller.objects.CompositeVelumMediator;
	import com.alleni.author.document.Document;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	
	public class CompositeTrailView extends Sprite
	{
		// display the breadcrumb trail
		
		private static const BOX_WIDTH:Number = 6;
		private static const BOX_HEIGHT:Number = 6;
		private static const BOX_INTERVAL:Number = 8;
		private static const SPACE_FOR_LINE:Number = 5;
		private static const BOX_Y:Number = 2;
		private static const LINE_HEIGHT:Number = 10;
		private static const LINE_COLOR:uint = 0xAFAFAF;
		private static const EDITING_COLOR:uint = 0x000000;
		private static const SELECTED_COLOR:uint = 0xFF0000;
		private static const ROLLED_COLOR:uint = 0x0099FF;
		private static const OTHER_COLOR:uint = 0xAAAAAA;
		
		private var _model:TaconiteModel;
		private var _displayedModels:Array;
		
		
		public function CompositeTrailView(context:ViewContext, model:TaconiteModel)
		{
			super();
			trace("compositeTrailView: model="+model);
			_model = model;
		}
		
		private function get document():Document
		{
			return Application.instance.document;
		}
		
		
		public function updateView(rolledObj:AbstractObject, models:Array):void
		{
			graphics.clear();
			var xx:Number = 0;
			_displayedModels = models;
			var childCount:int = models.length;
			for (var childNum:int=0; childNum < childCount; childNum++) {
				var child:TaconiteModel = models[childNum];
				drawBox(xx, child, rolledObj);
				
				if (childNum == 0) {  // vert line after the first tile (since it represents the gadget being edited)
					graphics.lineStyle(1,LINE_COLOR);
					var xLine:Number = xx+BOX_WIDTH + 3;
					graphics.moveTo(xLine, 0);
					graphics.lineTo(xLine, LINE_HEIGHT);
					xx += SPACE_FOR_LINE;
				}
				xx += BOX_INTERVAL;
			}
		}
		
		private function drawBox(xx:Number, objModel:TaconiteModel, rolledObj:AbstractObject):void
		{
			var color:uint = OTHER_COLOR;
			if (objModel == _model)
				color = EDITING_COLOR;
			else if (document.selection && document.selection.contains(objModel))
				color = SELECTED_COLOR;
			else if (objModel.value == rolledObj)
				color = ROLLED_COLOR;
			
			graphics.lineStyle();
			graphics.beginFill(color);
			graphics.drawRect(xx,BOX_Y,BOX_WIDTH,BOX_HEIGHT);
			graphics.endFill();
		}
		
		public function modelUnderX(xx:Number):TaconiteModel
		{
			var n:int;
			if (xx <= BOX_WIDTH)
				n = 0;  // the tile representing the gadget itself
			else
				n = (xx - SPACE_FOR_LINE) / BOX_INTERVAL;
			
			if (n >= 0 && n < _displayedModels.length)
				return _displayedModels[n];
			else
				return null;
		}
		
		public function get model():TaconiteModel
		{
			return _model;
		}
	}
}
