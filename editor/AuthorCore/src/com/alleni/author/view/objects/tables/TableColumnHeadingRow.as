package com.alleni.author.view.objects.tables
{
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.model.objects.StateTable;
	import com.alleni.author.model.objects.TruthTable;
	
	import flash.display.Sprite;
	
	public class TableColumnHeadingRow extends Sprite
	{		
		private var _columns:int;
		private var _table:AbstractTable;
		private var _columnNumbers:Array = [];
		private var _tableView:AbstractTableView;
			
		public function TableColumnHeadingRow(columns:int, table:AbstractTable, tableView:AbstractTableView)
		{
			super();
			
			_columns = columns;
			_table = table;
			_tableView = tableView;
			
			createRow();			
		}

		public function get columns():int 
		{
			return _columns;
		}

		public function set columns(value:int):void 
		{
			for (var n:int=_columns; n<value; n++) {
				var label:TableHeadingCell = new TableHeadingCell(_table,0,n,_table.column-1==n,_tableView);
				label.updateValue((n+1).toString());
				label.x = n*AbstractTableView.COLUMN_WIDTH;
				addChild(label);
				_columnNumbers.push(label);
			}
			_columns = value;
			
			while(_columnNumbers.length > _columns) {
				removeChild(_columnNumbers.pop());
			}
			
			if(_table is AnswerTable || _table is StateTable){
				for (var i:int=0; i<_columns; i++){
					var cell:Object = getChildAt(i);
					if(cell is TableHeadingCell){
						(cell as TableHeadingCell).updateEvents();
					}
				}
			}
		}

		private function createRow():void
		{			
			for (var n:int=0; n<_columns; n++) {
				var label:TableHeadingCell = new TableHeadingCell(_table,0,n,_table.column-1==n,_tableView);
				label.updateValue((n+1).toString());
				label.x = n*AbstractTableView.COLUMN_WIDTH;
				addChild(label);
				_columnNumbers.push(label);
			}
		}
		
		public function highlightColumn(value:int):void
		{
			if(_columnNumbers[value] != null){
                TableHeadingCell(_columnNumbers[value]).highlight();
            }
		}
		
		public function unhighlightColumn(value:int):void
		{
            if(_columnNumbers[value] != null){
                TableHeadingCell(_columnNumbers[value]).unhighlight();
            }
		}
	}
}