package com.alleni.author.view.objects
{
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	// an IconedView which cannot be resized, because the essence of the view IS the icon
	public class SimpleIconedView extends IconedView
	{
		public function SimpleIconedView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
		}
		
		// simple iconed views have no handles
		// icons should not be resized
		override protected function ObjectFeedbackFactory():ITaconiteView
		{
			return null;
		}
	}
}