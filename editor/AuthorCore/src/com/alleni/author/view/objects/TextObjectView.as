/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: TextView.as 4103 2010-03-31 21:21:18Z sbirth $  */

/*


Edit view
-------------------------

When text is being edited, the text source is used to update the text flow.  Upon completion of editing, source is updated by the textobjectmediator.  When source is wired to and changed
the source property change causes  an update to output, which is a parsed and formatted version of source.  An update to output invokes a method to parse the source and 
then to update the output property with the formatted, parsed output.  The change in the output property causes an update to the _text.textflow.  


Text with embedded variables
------------------------------------
Text with embedded variables are saved to source upon close edit.  When text is opened for edit, the source is used to restore the original text with variable names
rather than values. 


Wiring to Source Property
---------------
Wiring to source property will blow away the text flow, but the textflow is rebuilt with no internal markup styling.  The text object's properties affect the ZebraDefaultStyle in a
manner that is described below.

FormattedText Object
------------------------------

This object is responsible for converting string to textflow or visa versa.  It can also read from a textflow or produce one.


About the ZebraDefault Style
------------------------------

TextObject level property changes such as FontSize change the ZebraDefault Style.
For example, a change to font size at the text object level as opposed to the text selection markup level is passed to the StyleMediator, which stuffs the property value onto 
the text cssobject.  The styleValid flag is then set to false, which causes a call to the mediator style override function, which builds a new format resolver and assigns it to the textflow

The format resolver is the object that transforms css styles into property changes on the textflow  (i.e. CSS to TextLayoutFormat)


Styled versus Unstyled Text
-------------------------------

Styled text sends the FormattedText including {date} down the wire, which includes the CSS Object that defines the ZebraDefault Style
Unstyled text sends the evaluated, plain text Wednesday,April 10 down the wire, not including the styles information.


Flags
----------------------------

UPDATING_STYLES:  

When the style changes, the source gets updated with the CSS changes.  An update to source triggers an update to output.  
Normally, an update to output causes the textflow to be updated.  But this isn't necessary for a style change alone. The flag
set to true prevents the textflow from updating.  However, when text editing becomes false, the flag must be cleared otherwise the textflow isn't updated with the edited and parsed
text.

When text is reopened from a file UPDATING_STYLES should be cleared so that the textflow gets updated and any
embedded variables get properly displayed.

*/

package com.alleni.author.view.objects
{
	import com.alleni.author.controller.app.FontController;
import com.alleni.author.controller.objects.TextObjectController;
import com.alleni.author.controller.objects.TextObjectMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.definition.DoubleClickAction;
	import com.alleni.author.definition.Thumbnails;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.definition.text.ActionMap;
	import com.alleni.author.definition.text.CSSObject;
	import com.alleni.author.definition.text.Fonts;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.definition.text.StyleMediator;
	import com.alleni.author.event.ScrollUpdateEvent;
	import com.alleni.author.event.TextObjectEvent;
	import com.alleni.author.model.ScrollModel;
	import com.alleni.author.model.objects.TextObject;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.util.TextParser;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.TextVerticalScrollView;
	import com.alleni.author.view.VerticalScrollView;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
import flash.display.Graphics;
import flash.display.Shape;
import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	import flash.utils.Dictionary;
	
	import flashx.textLayout.edit.SelectionFormat;
	import flashx.textLayout.elements.Configuration;
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.events.TextLayoutEvent;
	import flashx.textLayout.events.UpdateCompleteEvent;
	import flashx.textLayout.formats.TextDecoration;
	import flashx.textLayout.formats.TextLayoutFormat;
	import flashx.textLayout.formats.VerticalAlign;
	
	/**
	 * View of a text box which uses the Text Layout Framework to display rich, editable text.
	 */
	public class TextObjectView extends ObjectView
	{
		private var _text:RichTextField;
		private var _mediator:TextObjectMediator;
		private var _textParser:TextParser;
		private var _flags:uint = 0;
		
		private static const TLF_MARKUP_INVALID:uint 		= 1 << 1;
		private static const TLF_PROPERTY_CHANGE:uint   	= 1 << 2;
		private static const UPDATING_ATTRIBUTES:uint 		= 1 << 3;
		private static const INITIAL_PARSE_REQUIRED:uint 	= 1 << 4;
		private static const UPDATING_STYLES:uint           = 1 << 5;

        private var _background:Shape;
        private var _layoutWidth:Number;
        private var _layoutHeight:Number;
		private var _origHeight:Number = -1;
		private var _styleMediator:StyleMediator;
		private var _scrollBar:VerticalScrollView;
		private var _scrollModel:ScrollModel;
		
		private var _CSS2Props:Dictionary = new Dictionary();
		private var _assetController:AssetController = AssetController.instance;
		
		
		public function TextObjectView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);

            // background is separate so bounds can be measured without including scrollbar
            _background = new Shape();
            addChild(_background);
			
			if (text as TextObject) { 
				//text that is first loaded should be parsed and evaluated so that values are shown, not embedded variables
				setFlag(INITIAL_PARSE_REQUIRED);
				ApplicationController.addEventListener(NotificationNamesApplication.SAVE_PROJECT, handleProjectSave);
				ApplicationController.addEventListener(NotificationNamesApplication.SAVE_AS_PROJECT, handleProjectSave);
					
				//set up a useful configuration for TextObjects that works well with effects
				var config:Configuration = new Configuration();
				var selectionFormat:SelectionFormat = new SelectionFormat(RichTextField.SELECTION_COLOR, RichTextField.SELECTION_ALPHA,BlendMode.MULTIPLY);
				config.focusedSelectionFormat = selectionFormat;
					
				//richtextfield holds the textflow logic
				_text = new RichTextField(text.width, text.height,true,null,config, text, Fonts.DEFAULT_SANS_FONT);  //no markup, create textflow from scratch
				_text.makeOuterSpanReadOnly(true);  //prevent writing properties to outermost span <tf><p><span></span></p></tf>
				createTextFlow();
				
				//used for evaluating embedded variables
				_textParser = new TextParser(text);
				mapCSS2Props();  //create a lookup to support conversion of css properties to text props
				ApplicationController.addEventListener(NotificationNamesApplication.FONTS_PROGRESS, handleFontsProgress);
				
			} else{
				_text = new RichTextField(text.width, text.height);  //for uses of text other than TextObject
				_text.alwaysUseTextFlow = false;
			}
			
			if (role == ViewRoles.PRIMARY) {
				_mediator = new TextObjectMediator();
				_mediator.handleViewEvents(this, _text);
				
				_text.textFlow.addEventListener(UpdateCompleteEvent.UPDATE_COMPLETE, handleTextFlowUpdate);
				_text.textFlow.addEventListener(TextLayoutEvent.SCROLL,updateScrollPosition);
			}
			
			addChild(_text);
			
			if(text.loading == false){
				init();
				_mediator.handleStyleSheetOverride();
				updateSource();
			}
		}
		
		override public function warnViewToDelete():void
		{
			super.warnViewToDelete();
			_mediator.clearViewEvents();
			_text.textFlow.removeEventListener(UpdateCompleteEvent.UPDATE_COMPLETE, handleTextFlowUpdate);
			_text.textFlow.removeEventListener(TextLayoutEvent.SCROLL,updateScrollPosition);
			_scrollBar.removeEventListener(ScrollUpdateEvent.SCROLL, handleScrollUpdate);
		}
		
		private function setUpScrollBar():void
		{
			_scrollModel = new ScrollModel(text.height,0,_text.containerHeight);
			_scrollBar = new TextVerticalScrollView(_scrollModel,30,text.height);
			_scrollBar.x = text.left + text.width;
			_scrollBar.y = text.top;
			_scrollBar.visible = false;  // for now
			_scrollBar.trackBackgroundColor = 0xEEEEEE;
			_scrollBar.addEventListener(ScrollUpdateEvent.SCROLL, handleScrollUpdate);
			
			addChild(_scrollBar);
			
		}
		
							
		
		/**
		 * The Text object associated with this view's TaconiteModel.
		 */
		public function get text():TextObject
		{
			return model.value as TextObject;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{
			var handledModelUpdate:Boolean = super.updateModelProperty(property, oldValue, newValue);
			
			if(text.loading) {
				return handledModelUpdate;  //if handledModelUpdate is true, then viewupdate isn't required, we've handled it
			}
			
			if (_role == ViewRoles.PRIMARY) {
//                trace("TextObjectView prop="+property, "val="+newValue);
				// Avoid application render mechanism when redrawing text
				// (because RichTextField will crash)
				switch (property) {
					case "loading":
						if (newValue == false) { // loading is done now
							init();
						}
						break;
					case "anchorPoint":
                        updateScrollBar();
                        break;
                    case "kerning":
					case "margin":
					case "lineThickness":
						updateTextAttributes();  // update now, instead of in application render handler
						setCompositionSize();
						return false;
					case "textAlignV":
						updateTextAttributes();  
						setCompositionSize();
						break;
					case "textAlignH":
						updateTextAttributes();  
						setCompositionSize();
						break;
					case "columnCount":
						if((newValue as int) < 1){
							text.columnCount = 1;
							return true;
						}
						_text.setColumnCount(newValue as uint);
						text.source.setColumnCount(newValue as uint);  //kluge -- should be able to do this on the styleMediator
						updateSource();
						updateTextAttributes();  
						setCompositionSize();
						break;
					
					case "columnGap":
						_text.setColumnGap(newValue as Number);
						text.source.setColumnGap(newValue as Number);  //kluge -- should be able to do this on the styleMediator
						updateSource();  //for initialvalues
						updateTextAttributes();  
						setCompositionSize();
						break;
					case "fontSize":
						trace("TextObjectView>updateModelProperty>fontSize");
						_mediator.styleMediator.fontSize = text.fontSize;
						text.stylesValid = false;
						setCompositionSize();
						break;
					case "fontAlpha":
						_mediator.styleMediator.textAlpha = text.fontAlpha/100;
						text.stylesValid = false;
						break;
					case "fontColor":
						_mediator.styleMediator.fontColor = text.fontColor;
						text.stylesValid = false;
						break;
					case "italic":
						_mediator.styleMediator.italic = text.italic;
						text.stylesValid = false;
						break;
					case "bold":
						_mediator.styleMediator.bold = text.bold;
						text.stylesValid = false;
						break;
					case "underline":
						_mediator.styleMediator.underline = text.underline;
						text.stylesValid = false;
						break;
					case "fontFamily":
						_mediator.styleMediator.fontFamily = text.fontFamily;
						text.stylesValid = false;
						break;
					case "height":
						if(Math.floor(oldValue as Number) == Math.floor(newValue as Number)){
							return true;  //don't do anything if there isn't a significant change in height
						}
						/*
						if(text.loading && _origHeight == -1){
							_origHeight = newValue as Number;
						}
						*/
					case "width":
                        if(newValue > 500000){
                            text.width = 500000;
                            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:75}));
                            break;
                        }
						updateTextAttributes();
						setCompositionSize();
						break;  // redraw the background
					case "inputVal":
						updateOutput();
						break;
					case "asset":
					case "complete":
					case "content":
						/* this should get restored when we restore assets
							if (text.complete && text.asset) {
								updateAsset();
								updateStatus();
							}
						*/
						break;
					case "valid":
						if(newValue == false){
								updateOutput();
						}
						break;
					
					case "stylesValid":
						if(newValue == false){
							setFlag(UPDATING_STYLES);
							_mediator.handleStyleSheetOverride();
							updateSource();
							text.stylesValid = true;
						}

						break;
					
					case "editing":
						if (newValue == false){        //if not editing
							trace("TextObjectView>ModelUpdate>handled editing set to false.");
							clearFlag(INITIAL_PARSE_REQUIRED);	
							clearFlag(UPDATING_STYLES);
							updateOutput();
							updateScrollBar();
						}
						else{ //editing
							trace("TextObjectView>ModelUpdate>handled editing set to true.");
							clearFlag(INITIAL_PARSE_REQUIRED);	
							updateTextFlowFromSource();
						}
						
						break;
					case "source":
						updateOutput();
						parseCSSObject(text.source.cssObject);
						this.handleTextFlowUpdate(null);  //call to update formatting from expanded text field
						break;
					
					case "output":
						if(!testFlag(UPDATING_STYLES)){
							createTextFlowFromOutput();
							text.plainText = text.output.toString(); 
						}
						clearFlag(UPDATING_STYLES);
						break;
					
					case "styleSpec":
						_mediator.handleStyleSheets(newValue as String);
						break;
					
					case "autoSize":
					case "vScrollPolicy":
						this.updateScrollBar();
						break;
					case "verticalScrollPosition":
						calculateScrollPosition();
						break;
					case "scrollThumbColor":
						if (_scrollBar){
							_scrollBar.thumbColor = newValue as uint;
						}
						break;
					
					case "scrollThumbOverColor":
						if (_scrollBar){
							_scrollBar.thumbOverColor = newValue as uint;
						}
						break;
					case "scrollThumbAlpha":
						if (_scrollBar){
							_scrollBar.thumbAlpha = text.scrollThumbAlpha/100;
						}
						break;
					case "scrollThumbLineColor":
						if (_scrollBar){
							_scrollBar.thumbLineColor = newValue as uint;
						}
						break;
					case "scrollTrackColor":
						if (_scrollBar){
							_scrollBar.trackBackgroundColor = newValue as uint;
						}
						break;
					case "scrollTrackAlpha":
						if (_scrollBar){
							_scrollBar.trackAlpha= text.scrollTrackAlpha /100;
						}
						break;
					case "scrollTrackLineColor":
						if (_scrollBar){
							_scrollBar.trackForegroundColor = newValue as uint;
						}
						break;
						
				}
				reviveBlink();
			}
			if (_role == ViewRoles.DIAGRAM) {
				switch(property) {
					case "text":
					case "height":
					case "fontColor":
					case "fontFamily":
					case "kerning":
					case "fontStyle":
						updateThumbnail();
						return true;
					default:
						break;
				}
			}
			return handledModelUpdate;
		}
		
		private function init():void
		{
			handleTextProperties();
			updateOutput();
			clearFlag(UPDATING_STYLES);
			createTextFlowFromOutput();
			updateTextAttributes(); 
			setCompositionSize();
			setUpScrollBar();
			updateScrollBar();
			updateScrollBarAppearance();
		}

        override protected function updateStatus():void {
            super.updateStatus();
        }
		
		private function updateTextFlowFromSource():void
		{
			createTextFlow();
			update();
		}
		
		private function updateTextFlowFromOutput():void
		{
			createTextFlowFromOutput();
			update();
		}
		
		private function update():void
		{
			updateTextAttributes();
			setCompositionSize();
		}
		
		/*
		This method updates source so that initial values picks up on the change to source.
		
		*/
		private function updateSource():void
		{
			if(text.source.toString() == "")  //don't update source with blank source -- once text closes editing, source will no longer be blank
				return;
			var formattedText:FormattedText = new FormattedText(text.source.toMarkUpAsString()); //for initialvalues
			formattedText.cssObject = text.css;
			text.source = formattedText;
		}
		
		private function updateOutput():void
		{
			var textFlow:TextFlow = text.source.toTextFlow();
			textFlow.styleName = "zebradefault";
			this._textParser.parseTextFlow(textFlow,true);
			
			text.output = FormattedText.fromTextFlow(textFlow);
            text.valid = true;
		}
		
		private function createConfiguration():Configuration
		{
			var config:Configuration = new Configuration();
			var selectionFormat:SelectionFormat = new SelectionFormat(RichTextField.SELECTION_COLOR, RichTextField.SELECTION_ALPHA, BlendMode.MULTIPLY);
			config.focusedSelectionFormat = selectionFormat;
			return config;
		}
		
		private function createTextFlowFromOutput():void
		{
			var config:Configuration = createConfiguration();
			 var textFlow:TextFlow = text.output.toTextFlow(config);
			if(_text.textFlow != null){
				_text.textFlow.removeEventListener(UpdateCompleteEvent.UPDATE_COMPLETE, handleTextFlowUpdate);
				_text.textFlow.removeEventListener(TextLayoutEvent.SCROLL,updateScrollPosition);
				
			}
			
			applyAlignment(textFlow);
			_text.textFlow = textFlow;
			_text.alwaysUseTextFlow = true;
			_text.textFlow.addEventListener(UpdateCompleteEvent.UPDATE_COMPLETE, handleTextFlowUpdate);
			_text.textFlow.addEventListener(TextLayoutEvent.SCROLL,updateScrollPosition);
			
			if(_mediator)
				_mediator.handleTextFlowChange();
		}
		
		private function createTextFlow():void
		{
			var config:Configuration = createConfiguration();
			var textFlow:TextFlow = text.source.toTextFlow(config);
			textFlow.styleName="zebradefault";
			
			 if(_text.textFlow != null){
				_text.textFlow.removeEventListener(UpdateCompleteEvent.UPDATE_COMPLETE, handleTextFlowUpdate);
				_text.textFlow.removeEventListener(TextLayoutEvent.SCROLL,updateScrollPosition);
			 }
			
			_text.textFlow = textFlow;
			_text.alwaysUseTextFlow = true;
			_text.textFlow.addEventListener(UpdateCompleteEvent.UPDATE_COMPLETE, handleTextFlowUpdate);
			_text.textFlow.addEventListener(TextLayoutEvent.SCROLL,updateScrollPosition);
			
			if(_mediator)
				_mediator.handleTextFlowChange();
		}
		
		public function get textField():RichTextField
		{
			return _text;
		}
		
	
		/**
		 * Update this view by drawing the appropriate graphics.
		 * Called in render event.
		 */
		override protected function updateEnabledView():void
		{
			super.updateEnabledView();
			_text.x = object.left;
			_text.y = object.top;
			
		}
		
		private function updateTextPadding():void
		{	
			var gap:Number = 2;  //provides separation of text from line
			
			var leftParam:Number = Math.max(text.lineThickness/2 + gap, text.margin);
			var topParam:Number = Math.max(text.lineThickness/2 + gap, text.margin);
			var rightParam:Number = Math.max(text.lineThickness/2 + gap , text.margin);
			var bottomParam:Number = Math.max(text.lineThickness/2 + gap, text.margin);
			
			_text.setPadding(leftParam,topParam,rightParam,bottomParam);
		}
		
		private function applyDefaultAttributes():void
		{
			if(!testFlag(UPDATING_ATTRIBUTES)){
				setFlag(UPDATING_ATTRIBUTES);
				_text.fontFamily = text.fontFamily;
				_text.size = text.fontSize;
				_text.fontAlpha = text.fontAlpha /100;
				_text.color = text.fontColor;
				_text.bold = text.bold;
				_text.italic = text.italic;
				_text.underline = text.underline;
				clearFlag(UPDATING_ATTRIBUTES);
			}
			
		}
		
		/* not quite working properly */
		
		private function createTextFlowInitialFormat():TextLayoutFormat
		{
			var initialFormat:TextLayoutFormat =  new TextLayoutFormat();
			initialFormat.fontSize = 14;
			initialFormat.fontFamily = text.fontFamily;
			initialFormat.fontSize= text.fontSize;
			initialFormat.textAlpha = text.fontAlpha /100;
			initialFormat.color = text.fontColor;
			
			return initialFormat;
		}
		
		private function handleTextProperties():void
		{
			with(_mediator.styleMediator){
				textAlpha = text.fontAlpha /100;
				fontColor = text.fontColor;
				fontFamily = text.fontFamily
				fontSize = text.fontSize
			    bold = text.bold;
				underline = text.underline;
				italic = text.italic;
			}
				
				text.stylesValid = true;
				_text.setColumnCount(text.columnCount);
				_text.setColumnGap(text.columnGap);
				
				rolloverGlow = false;
				getTextLevelFont(text.fontFamily);
				
		}
		private function getTextLevelFont(font:String):void
		{
			FontController.instance.requestByName(font);
		}
		
		private function updateTextAttributes():void
		{
			// not called in render event (updateEnabledView) since RichTextField would crash 
			if(!testFlag(UPDATING_ATTRIBUTES)){
				setFlag(UPDATING_ATTRIBUTES);
				
				_text.kerning = text.kerning;
				_text.background = false;  // (the background didn't consistently fill, when set in RTF.  Disappeared at start of editing)
				_text.border = false;
				
				clearFlag(UPDATING_ATTRIBUTES);
				updateTextPadding();
			}
		}
	
		private function setCompositionSize():void
		{
			if (_role == ViewRoles.PRIMARY) {
				_text.textAlign = text.textAlignH;
				_text.verticalAlign = text.textAlignV;
				_text.setCompositionSize(text.width, text.height);

				updateScrollBar();
				updateTextLength();  
			}
		}
				
		override protected function draw():void
		{
			if (object.fillAlpha == 0) {  // ensure the whole bounds of the text obj can get mouse clicks
				graphics.beginFill(0xFFFFFF, 0.01);
				graphics.drawRect(object.left, object.top, object.width, object.height);
				graphics.endFill();
			} else {
				graphics.drawRect(object.left, object.top, object.width, object.height);
			}
		}

        override public function get graphics():Graphics
        {
            return _background.graphics;
        }

		override protected function updateThumbnail() : void
		{
			if (object.complete) {
				updateEnabledView();
				_text.size = Math.round(14 * object.thumbnailScale);
				_text.setCompositionSize(text.width * object.thumbnailScale, Thumbnails.THUMB_NOMINAL_SIZE*object.thumbnailScale);
				_text.x = 5;
				// a little bit of tweaking to get the positioning right - can standardize at some point
				_text.y = Thumbnails.THUMB_NOMINAL_SIZE - 10 * object.thumbnailScale - 3;
			}
		}
		
		override public function exactHitTest(stagePoint:Point):Boolean
		{
			return hitTestPoint(stagePoint.x, stagePoint.y);  // click anywhere in bounds will drag the text (relies on fill)
		}

        override public function hitTestObject(obj:DisplayObject):Boolean
        {
            // compare other object's bounds with map bounds, (fix bug caused by text view going a bit further than right bounds of obj)
            var otherBounds:Rectangle = obj.getBounds(this); 
            var myBounds:Rectangle = new Rectangle(object.left, object.top, object.width, object.height);
            return otherBounds.intersects(myBounds);
        }

		
		private function reviveBlink():void
		{
			if (stage && _text && _text.editing) 
				stage.addEventListener(Event.ENTER_FRAME, reviveListener);
		}
		
		private function reviveListener(event:Event):void
		{
			if (stage)
				stage.removeEventListener(Event.ENTER_FRAME, reviveListener);
			
			// revive blinking text cursor by clearing and restoring the selection range
			var start:int = _text.selectionStart;
			var end:int = _text.selectionEnd;
			_text.select(-1, -1);
			_text.select(start, end);
		}
		
		protected function testFlag(mask:uint):Boolean
		{
			return (_flags & mask) != 0;
		}
		
		protected function setFlag(mask:uint):void
		{
			_flags |= mask;
		}
		
		protected function clearFlag(mask:uint):void
		{
			_flags &= ~mask;
		}
		
		private function updateScrollBar():void
		{
			if(!_scrollBar)
				return;

            var oldVis:Boolean = _scrollBar.visible;
			if(text.vScrollPolicy == "auto"){
				_scrollBar.visible = _text.containerHeight > text.height;
			}
			else if(text.vScrollPolicy == "on" && (text.autoSize == false || text.columnCount > 1)){
				_scrollBar.visible = true;
			}
			else{
				_scrollBar.visible = false;
			}
					
			_scrollBar.scrollControllerHeight = text.height;
			_scrollBar.x = text.left + text.width;
			_scrollBar.y = text.top;
			
			var documentLength:Number = _text.calculateContainerHeight();
			
			_scrollModel.maxScrollPosition = documentLength > -1?documentLength:_text.containerHeight;
			_scrollModel.viewPortLength = text.height;

            if (_scrollBar.visible != oldVis) {  // change affects arena layout
                trace("TEXT updateScroll="+_scrollBar.visible);
                checkLayoutSize();
            }
		}

        private function checkLayoutSize():void
        {
            // detect change in layout size, for benefit of Arena grid
            var bounds:Rectangle = getBounds(this.parent);
            if (bounds.width == 0 && !text.source.isEmpty) {
                return;  // view not ready to measure
            }
            if (bounds.width != _layoutWidth || bounds.height != _layoutHeight) {
                var hadLayoutWidth:Boolean = !isNaN(_layoutWidth);
                _layoutWidth = bounds.width;
                _layoutHeight = bounds.height;
                if (hadLayoutWidth) {
                    TextObjectController(text.controller).layoutSizeChange(text);
                }
            }
        }
		
		private function updateTextLength():void
		{
			text.textLength = Math.round(_text.calculateContainerHeight() * 10)/10;
		}
		
		private function updateScrollPosition(event:TextLayoutEvent):void
		{
			if(Math.abs(_text.verticalScrollPosition - _scrollBar.scrollPosition) > 10){
		    	_scrollBar.scrollPosition = _text.verticalScrollPosition;
			}
		}
		
		private function calculateScrollPosition():void
		{
			_text.verticalScrollPosition = (_scrollModel.maxScrollPosition  - text.height) * text.verticalScrollPosition/100;
		}
				
		
		private function handleScrollUpdate(event:ScrollUpdateEvent):void
		{
			if (event && event.params && (event.params.hasOwnProperty("scrollPosition"))) {
					_text.verticalScrollPosition = event.params.scrollPosition;
					text.verticalScrollPosition = (_text.verticalScrollPosition /(_scrollModel.maxScrollPosition - text.height)) * 100;
					
			}
			
		}
		
		
		
		private function handleTextFlowUpdate(event:UpdateCompleteEvent):void
		{
			if (text.autoSizeEffective){
				text.height = _text.containerHeight;  // auto height
			}
			
			if(!_text.editing && !Application.running){
				setFlag(TLF_MARKUP_INVALID);
			}

            checkLayoutSize();
		}
		
		private function updateScrollBarAppearance():void
		{
		   if(!_scrollBar)
			   return;
		   
			_scrollBar.thumbColor = text.scrollThumbColor;
			_scrollBar.thumbOverColor = text.scrollThumbOverColor;
			_scrollBar.thumbAlpha = text.scrollThumbAlpha/100;
			_scrollBar.thumbLineColor = text.scrollThumbLineColor;
			_scrollBar.trackBackgroundColor = text.scrollTrackColor;
			_scrollBar.trackAlpha= text.scrollTrackAlpha /100;
			_scrollBar.trackForegroundColor = text.scrollTrackLineColor;
		}
		
		private function handleProjectSave(event:ApplicationEvent = null):void
		{
			updateTLF();
			text.textLength = _text.calculateContainerHeight();
		}
		
		/*
		The TLFMarkup needs to be updated if not in editing or running modes, 
		but a property change is made by selecting the textobject and using inspector
		*/
		
		private function updateTLF():void
		{
			if(testFlag(TLF_MARKUP_INVALID) || testFlag(TLF_PROPERTY_CHANGE)){
				
				if(text.hasEmbeddedVariables){
					if(text.editing){  //currently editing text with embedded variables
						var newSource:FormattedText = new FormattedText(_text.exportMarkup());
							newSource.cssObject =  text.css;  //source needs to hold a definition of ZebraDefault, which comes from css
							text.source = newSource;
					}
					
					else{
						updateSource();  //make sure that the source is updated with the CSS styles
					}
				}
				
				//trace("TLF updating********************************************************");
				clearFlag(TLF_MARKUP_INVALID);
				clearFlag(TLF_PROPERTY_CHANGE);
			}
			
		}
		
		public function updateStyles():void
		{
			for(var i:int=0; i < text.styleOverrides.length; i++) {
				var style:String = text.styleOverrides.getItemAt(i) as String;
				if(text.hasOwnProperty(style)) {
					
					switch (style){
						
						case "fontAlpha":
							var value:String = String(text[style]/100);
							_text.setStyle(convertPropertyToAction(style),value);
							break;
						case "textAlignH":
							_text.textAlign = text[style];
							break;
						case "textAlignV":
							_text.verticalAlign = text[style];
							break;
						default:
						_text.setStyle(convertPropertyToAction(style),text[style]);
						
					} //switch
				} //if
			}//for		
		}
		
		/*
		map properties to ActionMap constants.
		*/
		private function convertPropertyToAction(property:String):String
		{
			var action:String = property;
			
			switch(property){
				
				case "fontSize":
					action = ActionMap.FONTSIZE_ACTION;
					break;
				case "bold":
					action = ActionMap.BOLD_ACTION;
					break;
				case "italic":
					action = ActionMap.ITALIC_ACTION;
					break;
				case "underline":
					action = ActionMap.UNDERLINE_ACTION;
					break;
				case "fontColor":
					action = ActionMap.FOREGROUNDCOLOR_ACTION;
					break;
				case "fontFamily":
					action = ActionMap.FONTNAME_ACTION;
					break;
				case "fontAlpha":
					action = ActionMap.FONTALPHA_ACTION;
					break;
				case "textAlignH":
					action = ActionMap.TEXT_ALIGN_ACTION;
					break;
				case "textAlignV":
					action = ActionMap.VERTICAL_ALIGN_ACTION;
					break;
				
			}
			return action;
		}
		
		
		/*apply alignment to targeted textflow*/
		
		private function applyAlignment(textFlow:TextFlow):void
		{
			if (text.textAlignH.length > 0)
				textFlow.textAlign = text.textAlignH;
			if (text.textAlignV.length > 0)
				textFlow.verticalAlign = text.textAlignV;
		}
		
		private function setAttribute(xml:XML, attribute:String, value:String):String
		{
			xml.@[attribute] = value;
			return xml.toString();
		}
		
		private function parseCSSObject(css:CSSObject):void
		{
			if(css != null){
			
				for each(var selector:Dictionary in css.selectors){
				
					for (var style:String in selector){
						var propName:String = _CSS2Props[style];
						var value:String = selector[style];
						
						switch(propName){
							
							case "fontFamily":
								text[propName] = value;
								break;
							
							case "fontSize":
								value = value.replace(/pt|px/i, "");
								var tempFontSize:Number = Number(value);  //fontsize must be > 0 or textflow crashes
								text[propName] = tempFontSize > 0? tempFontSize:1;  
								break;
							
							case "fontColor":
								value =  value.replace("#","0x");
								var color:uint = uint(value);
								text[propName]  = color;
								break;
							
							case "italic":
								text[propName] = value == FontPosture.ITALIC?true:false;
							break;
							
							case "bold":
								text[propName] = value == FontWeight.BOLD?true:false;
								break;
							
							case "underline":
								text[propName] = value == TextDecoration.UNDERLINE?true:false;
								break;
							
							case "fontAlpha":
								text[propName] = String(Number(value) * 100);
								break;
							
							default:
								break;
						}
						
					}
				}
			}
		}
		
		private function mapCSS2Props():void
		{
			_CSS2Props["font-family"] = "fontFamily";
			_CSS2Props["font-size"] = "fontSize";
			_CSS2Props["color"]    = "fontColor";
			_CSS2Props["font-style"] = "italic";
			_CSS2Props["font-weight"] = "bold";
			_CSS2Props["text-decoration"] = "underline";
			_CSS2Props["text-alpha"] = "fontAlpha";
			
		}
		
		
		
		override public function get doubleClickAction():int
		{
			// similar stuff in TextObjectController handles single-click while running
			if (!Application.uiRunning && text.sourceTextIsAccessible && !text.childOfClosedMasterPage && !context.info.testView) {
				if (!_text.editing)
					dispatchEvent(new TextObjectEvent(TextObjectEvent.OPEN_EDITING));
				return DoubleClickAction.STOP;
			} else {
				return super.doubleClickAction;
			}
		}
		
		private function handleFontsProgress(event:Event):void
		{
			text.valid = FontController.instance.requestedCount != 0;
		}
		
		override public function getBounds(containerObj:DisplayObject):Rectangle
		{
            // for a transparent text object, bounds is the text bounds
            if (_scrollBar && _scrollBar.visible) {
                return super.getBounds(containerObj); // includes scrollbar
            } else {
                return _background.getBounds(containerObj);
            }
		}

        override public function handleRunningChanged():void {
            super.handleRunningChanged();
            if (Application.running){
                updateTextAttributes(); // ensure switch to embedded font, for automated test
            }
        }
    }
}
