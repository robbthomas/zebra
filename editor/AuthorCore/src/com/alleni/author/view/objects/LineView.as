/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.objects
{
    import caurina.transitions.Tweener;
    
    import com.alleni.author.controller.ComputeMessageCenterPosition;
    import com.alleni.author.definition.Thumbnails;
    import com.alleni.author.definition.ViewRoles;
    import com.alleni.author.model.objects.Line;
    import com.alleni.author.view.ObjectView;
    import com.alleni.author.view.feedback.LineFeedback;
    import com.alleni.taconite.lang.Geometry;
    import com.alleni.taconite.lang.Trigonometry;
    import com.alleni.taconite.model.TaconiteModel;
    import com.alleni.taconite.view.ITaconiteView;
    import com.alleni.taconite.view.ViewContext;
    
    import flash.display.Sprite;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    /**
     * View of a Line. 
     */
    public class LineView extends ObjectView
    {
        public function LineView(context:ViewContext, model:TaconiteModel, role:uint)
        {
            super(context, model, role);
        }
        
        /**
         * The Line object associated with this view's TaconiteModel.
         */
        public function get line():Line
        {
            return model.value as Line;
        }
        
        /**
         * Update this view by drawing the appropriate graphics.
         */
        override protected function draw():void
        {
            graphics.moveTo(line.left, line.top);
            graphics.lineTo(line.right, line.bottom);
        }
		
		override protected function updateThumbnail():void
		{
			if (object.complete) {
				updateEnabledView();
				
				var objectWidth:Number = Math.abs(object.width);
				var objectHeight:Number = Math.abs(object.height);
				
				if (_thumbnail != null && this.contains(_thumbnail))
					this.removeChild(_thumbnail);
				
				if (objectWidth > 0 && objectHeight > 0) {
					_thumbnail = Thumbnails.generate(this, objectWidth, objectHeight, object.thumbnailScale);
					_thumbnail.x = 5;
					_thumbnail.y = Thumbnails.THUMB_NOMINAL_SIZE - _thumbnail.height;
					this.addChild(_thumbnail);
				}
				graphics.clear();
			}
		}
        
        /**
         * Create a feedback view that will be superimposed on this view in a transparent layer.
         */        
        override protected function ObjectFeedbackFactory():ITaconiteView
        {
			return new LineFeedback(context, this, model);
        }
		
		
		/**
		 * Compute the offset from topLeft of line object to topCenter of Message Center.
		 * Accounts for rotation of line and rotation of containers 
		 * @param obj = the line object
		 * @param netRotation = the net rotation of all nested containers plus the line rotation.
		 * @return  = offset in terms of the parent's coordinate space.
		 * 
		 */
		override protected function computeMessageCenterOffset(netRotation:Number):Point
		{
			// concept:
			// we pretend the line is a thin rectangle rotated at "netRotation" plus the angle of the drawn line "lineAngle"
			// this allows using the standard computeMessageCenterPosition() which handles rectangles.
			// But the "lineAngle" must be subtracted from the resulting offset, below
			
			// the angle of the line from topLeft to bottomRight (in degrees)
			var lineAngle:Number = Math.atan2(object.width,object.height)*180/Math.PI - 90;  // horz line is 0, straight down is +90
			var netAngle:Number = netRotation - lineAngle;
			
			// length of line
			var lineLen:Number = Geometry.distanceBetweenPoints(0,0,object.width,object.height);
			
			// offset from topLeft of obj to topCenter of MC, in local coordinates of the object
			var offset:Point = ComputeMessageCenterPosition.computeMessageCenterPosition(lineLen,1,netAngle);  // width=lineLen, height=1
			
			// local offset, subtracting the impact of "lineAngle"
			var result:Point = Trigonometry.transformOffset(object.width, object.height, null, - lineAngle,offset);	
//			trace("LineView: lineAngle="+lineAngle, "netAngle="+netAngle, "lineLen="+lineLen, "result="+result);
			return result;
		}
		
		private var _red:Sprite = null;
		
		override public function flashRedForPathSelection(show:Boolean):void
		{
			if (show) {
				if (_red == null) {
					_red = new Sprite();
					with (_red.graphics) {
						lineStyle(3,0x990000,1);  // similar code in ToggleButtonMediator, LineView, PathObjectView
						moveTo(line.left, line.top);
						lineTo(line.right, line.bottom);
					}
					addChild(_red);
					if (!visible || alpha < 0.5)
						this.graphics.clear();  // prevent suddenly showing hidden thick line
					this.visible = true;  // temporary to ensure the red will show
					this.alpha = 1;
					_red.alpha = 1;
				}
			} else if (_red) {
				removeChild(_red);
				_red = null;
				updateView();  // restore normal visibility
			}
		}


		/**
		 *  @inheritDoc
		 * 
		 */
		override public function getSVG(localToTarget:Function):XML
		{
			var result:XML = <line />;
			var anchor:Point = localToTarget(this, new Point(0,0));
			var obj:Line = object as Line;
			var topLeft:Point = localToTarget(this,new Point(obj.left,obj.top));
			var bottomRight:Point = localToTarget(this,new Point(obj.right,obj.bottom));
			setCommonSVGAttributes(result, obj.rotation, anchor);
			result.@x1 = anchor.x + obj.left;
			result.@y1 = anchor.y + obj.top;
			result.@x2 = anchor.x + obj.right;
			result.@y2 = anchor.y + obj.bottom; 
			return result;
		}
    }
}
