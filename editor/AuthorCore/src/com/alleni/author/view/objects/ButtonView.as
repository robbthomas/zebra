/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.view.objects
{
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	public class ButtonView extends ToggleButtonView
	{
		public function ButtonView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
		}
		
		/**
		 * Override to position text for radio button.
		 *  
		 * @param text
		 * 				The RichTextField instance that this component uses.
		 * 
		 */
		override protected function positionText():void
		{
			// Sync the view text with the model text.
			textField.setCompositionSize(btn.width, NaN);
			// Calculate the label position, centered in the button 
			// (depends on a render of the text to produce a height value, setting "textHeight")
			textField.x = btn.left;
			textField.y = btn.top + btn.height/2 - btn.labelHeight/2;
		}
	}
}