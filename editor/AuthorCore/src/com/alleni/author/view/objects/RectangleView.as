/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id:  $  */


package com.alleni.author.view.objects
{
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.model.objects.RectangleObject;
	import com.alleni.author.view.GraphicFillView;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.feedback.RectangleBoxFeedback;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class RectangleView extends ObjectView
	{
		private var _fillView:GraphicFillView; 
		
		
		public function RectangleView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			_fillView = new GraphicFillView();
			_fillView.mouseEnabled = false;
			_fillView.mouseChildren = false;
			addChild(_fillView);
		}
		
		override protected function ObjectFeedbackFactory():ITaconiteView
		{
			return new RectangleBoxFeedback(context, this, model);
		}		
		
		override protected function updateEnabledView():void
		{
			super.updateEnabledView();
			var obj:RectangleObject = object as RectangleObject;
			_fillView.update(obj.fillSpec, obj.width, obj.height, adjustedFillAlpha(), obj.cornerRadius,
				obj.lineThickness, obj.lineColor, obj.lineAlpha/100, context.info.testView);
			_fillView.x = object.left;
			_fillView.y = object.top;
		}
		
		
		/**
		 *  @inheritDoc
		 * 
		 */
		override public function getSVG(localToTarget:Function):XML
		{
			var result:XML = <rect  />; 
			var anchor:Point = localToTarget(this, new Point(0,0));
			var obj:RectangleObject = object as RectangleObject;
			setCommonSVGAttributes(result, obj.rotation, anchor);
			result.@x = anchor.x+obj.left;
			result.@y = anchor.y+obj.top;
			result.@width = obj.width;
			result.@height = obj.height;
			result.@rx = obj.cornerRadius; 
			result.@ry = obj.cornerRadius;
			
			return result;
		}
		
	}
}