/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.view.objects
{
	import com.alleni.author.model.GraphicFill;
import com.alleni.author.model.objects.RadioButton;
import com.alleni.author.view.GraphicFillView;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	public class RadioButtonView extends ToggleButtonView
	{
		public function RadioButtonView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
		}
		
		
		/**
		 * Override to draw indicator for selected state.
		 *  
		 * @param sprite
		 * 					The sprite instance that this method draws into.
		 * 
		 */
		override protected function drawSelectVector(sprite:Sprite):void
		{
			var shine:Sprite = new Sprite();
			var vectorW:Number = btn.width*.5;
			var vectorH:Number = btn.height*.5;
			var vectorX:Number = btn.left + (btn.width/2)-(vectorW/2);
			var vectorY:Number = btn.top + (btn.height/2)-(vectorH/2);
			
			var shineW:Number = vectorW*5/6;
			var shineH:Number = vectorH*2/3;
			var shineX:Number = vectorX+((vectorW/2)-(shineW/2));
			var shineY:Number = vectorY+(vectorH*.025);
			
			var shineFillType:String = GradientType.LINEAR; 
			var shineColors:Array = [0xFFFFFF, 0xFFFFFF];
			var shineAlphas:Array = [0.7, 0];
			var shineRatios:Array = [0, 255];
			var shineMatrix:Matrix = new Matrix();
			var spreadMethod:String = SpreadMethod.PAD;
			
			
			shineMatrix.createGradientBox( shineW, shineH, 90/180*Math.PI, 0, 0 );
			
			shine.graphics.lineStyle(0, 0xFFFFFF, 0);
			shine.graphics.beginGradientFill(shineFillType, shineColors, shineAlphas, shineRatios, shineMatrix, spreadMethod );
			
			shine.graphics.drawEllipse(0,0,shineW,shineH);
			shine.alpha = (model.value as RadioButton).checkOpacity * .01;
			
			
			
			sprite.graphics.beginFill(btn.checkColor,btn.checkOpacity/100);
			sprite.graphics.drawEllipse(vectorX,vectorY,vectorW,vectorH);
			sprite.graphics.endFill();
			
			sprite.addChild(shine);
			
			shine.x = shineX;
			shine.y = shineY;
		}
	}
}