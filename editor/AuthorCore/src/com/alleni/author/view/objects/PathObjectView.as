package com.alleni.author.view.objects
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.controller.objects.PathEditingMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.WireController;
	import com.alleni.author.model.objects.PathNode;
	import com.alleni.author.model.objects.PathObject;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.PathNodeWireAnchor;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.GraphicFillView;
	import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.lang.Geometry;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.LineScaleMode;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	
	
	public class PathObjectView extends ObjectView
	{
		private var _curve:Sprite;
		private var _mediator:PathEditingMediator;
		
		public function PathObjectView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			_curve = new Sprite();
			_curve.doubleClickEnabled = true;
			addChild(_curve);
			placeCurve();
			_mediator = new PathEditingMediator(this);
		}
		
		public function get path():PathObject
		{
			return model.value as PathObject;
		}
		
		public function get curve():Sprite
		{
			return _curve;
		}
		
		override public function warnViewToDelete():void
		{
			_mediator.clearViewEvents();
			super.warnViewToDelete();
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{
			// scale must be applied to view in the render function, because the PathObject.set width is setting scale
			// changing the view scale immediately would cause jitter when dragging resize handles
			var result:Boolean = super.updateModelProperty(property, oldValue, newValue);
			switch (property) {
				case "locationXYR":
					path.handlePathMoved();
					break;
				case "width":
				case "height":
				case "viewBox":
				case "anchorPoint":
				case "scale":
					placeCurve();
					path.handlePathMoved();
					break;
				case "path":
					placeCurve();
					_mediator.pathReplaced();
					break;
				case "lineColor":
				case "lineThickness":
				case "lineAlpha":
				case "fillSpec":
				case "fillAlpha":
                case "cloakedNow":
					path.requestRedraw();
					break;
				case "complete":
					if (newValue == true && _mediator)
						_mediator.objCompleted();
					break;
				case "bitmapFill":
					_mediator.bitmapFillChanged(oldValue as Asset, newValue as Asset);
					break;
			}
			return result;
		}
		
		override public function updateViewPosition():void
		{
			super.updateViewPosition();
			placeCurve();
		}
		
		private function placeCurve():void
		{
			_curve.x = path.left;
			_curve.y = path.top;
		}
		
		override public function render():void
		{
			if (path.prepRedraw()) {
				
				var g:Graphics = _curve.graphics;
				g.clear();
				
				// fill separate from line, so there won't be a bogus line from end to start
				if (path.fillAlpha > 0 || path.bitmapFill && path.bitmapFill.content as Bitmap) {

					if (path.bitmapFill && path.bitmapFill.content as Bitmap) {
						var bitmap:Bitmap = path.bitmapFill.content as Bitmap;
						g.beginBitmapFill(bitmap.bitmapData,null,true,true);  // repeat=true, smooth=true
					} else if (path.fillAlpha > 0) {
						var fillAlpha:Number = adjustedFillAlpha();
						GraphicFillView.beginGraphicFill(g, path.fillSpec, path.width, path.height, 0, 0,
								fillAlpha, _context.info.testView);
					}
	
					path.path.drawPath(g);
					g.endFill();
				}
				
				if (path.lineThickness > 0) {
					g.lineStyle(path.lineThickness, path.lineColor, path.lineAlpha/100);
					path.path.drawPath(g);
				}
				
				path.finishRedraw();
			}
			
			finishRender();
		}
		
		public function recalcBounds():void
		{
			_mediator.recalcBounds();
		}		

		override public function hitTestObject(obj:DisplayObject):Boolean
		{
            var otherBounds:Rectangle = obj.getBounds(this); 
            var myBounds:Rectangle = new Rectangle(object.left, object.top, object.width, object.height);
            return otherBounds.intersects(myBounds);
		}
		
		override protected function set rolloverGlow(value:Boolean):void
		{
			super.rolloverGlow = (value && !object.editing);
		}
		
		private var _red:Sprite = null;
		
		override public function flashRedForPathSelection(show:Boolean):void
		{
			if (show) {
				if (_red == null) {
					_red = new Sprite();
					_red.graphics.lineStyle(3,0x990000,1);  // similar code in ToggleButtonMediator, LineView, PathObjectView
					path.path.drawPath(_red.graphics);
					_red.x = path.left;
					_red.y = path.top;
					addChild(_red);
					if (!visible || alpha < 0.5)
						_curve.graphics.clear();  // prevent suddenly showing hidden polygon fill
					this.visible = true;  // temporary to ensure the red will show
					this.alpha = 1;
					_red.alpha = 1;
				}
			} else if (_red) {
				removeChild(_red);
				_red = null;
				path.requestRedraw();  // restore normal visibility
			}
		}
		
		override public function getSVG(localToTarget:Function):XML
		{
			var result:XML = <path />;
			var anchor:Point = localToTarget(this, new Point(0,0));
			var obj:PathObject = object as PathObject;
			setCommonSVGAttributes(result, 0, anchor);  // rotation=0 since points were transformed with rotation
			var func:Function = function(point:Point):Point { return localToTarget(_curve, point);};
			result.@['d'] = obj.path.getSVGData(func) + ((obj.fillAlpha > 0) ? " Z" : "");
			return result;
		}
	}
}