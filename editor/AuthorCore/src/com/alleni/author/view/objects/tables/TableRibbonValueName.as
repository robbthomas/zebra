package com.alleni.author.view.objects.tables
{
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.action.ModifyObjectProperty;
import com.alleni.author.model.objects.AbstractTable;

	public class TableRibbonValueName extends TableRibbonName
	{
		public function TableRibbonValueName(table:AbstractTable, index:int) 
		{
			super(table, index);
			this._fillColor = AbstractTableView.BLUE_DARK;
		}

		override public function set myValueFromControl(val:Object):void 
		{
            var oldValue:String = _table.stateValueName[_index];
            if(oldValue == val as String) {
                return;
            }
			_table.stateValueName[_index] = val as String;
            _table.recordInitialValue("stateValueName");
            ApplicationController.currentActionTree.commit(ModifyObjectProperty.fromObject(_table, "set table state value name", "stateValueName", _index, oldValue));
		}

		override public function get myValueFromControl():Object 
		{
			var result:Object;
			if (_table.stateValueName.length > _index)
				result = _table.stateValueName[_index];
			if (result == null || result == "") {
				result = super.myValueFromControl;
			}
			return result;
		}
	}
}