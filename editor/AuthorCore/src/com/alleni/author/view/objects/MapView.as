package com.alleni.author.view.objects
{
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.model.objects.MapObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.definition.RegistrationPoints;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	import com.google.maps.LatLng;
	import com.google.maps.Map;
	import com.google.maps.MapEvent;
import com.google.maps.MapMoveEvent;
import com.google.maps.MapType;
	import com.google.maps.interfaces.IMapType;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.system.Security;
	import flash.text.TextField;
	
	public class MapView extends ObjectView
	{
		private var _map:Map;
        private var _safeMode:Boolean;
	
		private var _currentLat:Number = -93;
		private var _currentLong:Number = 45;
		private var _currentZoom:Number = 10;
		
		private var registeredMarkers:Array = new Array();
		private var _mapBounds:Sprite = new Sprite();
		private var _tempTextField:TextField;
		
		public function MapView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			if(role == ViewRoles.PRIMARY && !mapObject.loading) {
				init();
			}
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{	
			var handledModelUpdate:Boolean = super.updateModelProperty(property,oldValue,newValue);	
			
			switch (property) {
				case "loading":
					if(newValue == false){
						init();
					}
				case "anchorPoint":
					setOffset();
					break;
				case "complete":
				case "height":
				case "width":
					resizeMap();
					break;
			}
			
			return handledModelUpdate;
		}
		
		private function init():void{
			buildMap();
			enableMap(false);
			setOffset();
			resizeMap();
			addChildAt(_mapBounds,0);

            _map.addEventListener(MapMoveEvent.MOVE_STEP, checkMapUpdates);
		}
		
		private function setOffset():void
		{	
			if (!_map){
				return;
			}
			
			
			var offset:Point = RegistrationPoints.registrationOffset(mapObject.width, mapObject.height, RegistrationPoints.TOP_LEFT,mapObject.anchorPoint,0);
			_map.x = -offset.x
			_map.y = -offset.y;	
		}
		
		private function setMapBounds():void{
			
			with(_mapBounds.graphics){
				clear();
				drawRect(object.left,object.top, mapObject.width, mapObject.height);
			}
		}
		
		private function enableMap(value:Boolean):void
		{
			if(value){
				this.mouseChildren = true;
			}
			else{
				this.mouseChildren = false;
			}
		}

        private function checkMapUpdates(event:MapMoveEvent):void{
            var latLong:LatLng = event.latLng;
            mapObject.latitude = latLong.lat();
            mapObject.longitude = latLong.lng();
        }
		
		override protected function updateEnabledView():void
		{
			super.updateEnabledView();
		}
		
		public function get mapObject():MapObject
		{
			return model.value as MapObject;
		}
		
		override protected function updateThumbnail():void
		{
		
		}
		
		public function buildMap():void
		{
			Security.allowInsecureDomain("maps.googleapis.com");
			_map = new Map();
			// currently, we are not using a sensor (such as a GPS locator) to determine the user's location. 
			// later, for mobile devices and browser geolocation, we can add support for this.
			// that may well coincide with premier api membership.
			_map.sensor = "false";
			_map.addEventListener(MapEvent.MAP_READY,onMapReady);
			
			//pick a key based on the server -- keys are specific to the hostname
			var environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			
			switch(environment.serverName.toUpperCase()){
				
				case "INT.ALLENI.COM":
					_map.key = "ABQIAAAAhLBhHfSdyRYlDInIJyZNcBRvQqnaRMEBKXyNR_AYw2gxlnKvNhQGWkyy18dvXDR3I2IdTameUTrZIw";
					break;
				case "ZEBRAQA.ALLENI.COM":  //TODO:We have applied with google to swap this key for one for pro.zebrazapps.com
					_map.key = "ABQIAAAAhLBhHfSdyRYlDInIJyZNcBR-JEf7D8PQu4Anvmn9dgZ8zHIMUBTM360U0UTPb4-15Qjmc8LKffyXaQ";
					break;
				case "UAT.ALLENI.COM":
					_map.key = "ABQIAAAAhLBhHfSdyRYlDInIJyZNcBR7r8J2zwUUOp8-uNe_mG1QSp4KpRS1h91seQafVhIcI2yBi85d7hKDiw";
					break;
				case "JOINZEBRA.COM":
					_map.key = "ABQIAAAAhLBhHfSdyRYlDInIJyZNcBQ-JJWF10jM739XaH65Ylnsk96YZRSupRu7E7N0SZ-G4hUG876EQiGTkg";
					break;
				case "ZEBRABETA.COM":
					_map.key = "ABQIAAAAhLBhHfSdyRYlDInIJyZNcBShMy0jtHidBGPHmuUU53KBbmIWzBSB317nO4_-DA1rLCk-lMpy7klTYQ";
					break;
				case "WWW.ZEBRABETA.COM":
					_map.key = "ABQIAAAAhLBhHfSdyRYlDInIJyZNcBR6LaS50R3HKsYIYdTVYuhfitkGLBSa4yawdkrx3TwLCYFm7XSYhxhIIQ";
					break;
				case "QA.ZEBRABETA.COM":
					_map.key = "ABQIAAAAhLBhHfSdyRYlDInIJyZNcBQhK3GVXFl-ytMXBCW8kpWRDRlozBSU0kCMmfiVCeG4ywc5VXNCZ9HU-A";
					break;
				case "STAGING.ZEBRABETA.COM":
					_map.key = "ABQIAAAAhLBhHfSdyRYlDInIJyZNcBTvMe-kL-h7XSBnj6UqpoEE1qPtTRQUkjWorjO6ZxzGcmsYogiHFmR4HQ";
					break;
				case "ZEBR.AS":
					_map.key = "ABQIAAAAhLBhHfSdyRYlDInIJyZNcBRFtXU_5YQiUutN3M-ul_gkIAIziBTu-lVz11CN7IFDHwBBvb8y9SNa0w";
					break;
				case "DEMO.ZEBRAZAPPS.COM":
					_map.key = "ABQIAAAAEEA1aP7uBgiw0w9-FbFU8RQscB82YIf6zBujzcwJPDttyHxS6BQpW8Wh37PwjaxH4SttyQo2u-4yAA";
					break;
				case "INTEGRATION.ZEBRAZAPPS.COM":
					_map.key = "ABQIAAAAEEA1aP7uBgiw0w9-FbFU8RTm_AO22ovuRk2Gy7PEDV-D293j8xSdIcIDPYnTZPY0dbvpqDFfS32E5Q";
					break;
				case "STAGING.ZEBRAZAPPS.COM":
					_map.key = "ABQIAAAAEEA1aP7uBgiw0w9-FbFU8RTx9Ji9VeFwYefBuYBlkIQdNFEJUBRp8Ynv0YJPJZftirl4OOjKDkfvqQ";
					break;
				case "QA.ZEBRAZAPPS.COM": // QA is secure. this key supports only HTTPS origin.
					_map.key = "ABQIAAAAEEA1aP7uBgiw0w9-FbFU8RT67jnHow5vm3YrkdgB5rXfOHVB3BQVmSpA3Wp0HC3PazyT92jnA2S0Uw";					
					break;
                case "QA2.ZEBRAZAPPS.COM": // QA2 is secure. this key supports only HTTPS origin.
					_map.key = "ABQIAAAAbYN_-Oy9IeNwNPmEa811YRTLnxTZDX6NsTJU7Agz_aqG2VhgxxQr5_py8TUxbxRXgwQP3r1tyGdIKw";
					break;
				case "PREVIEW.ZEBRAZAPPS.COM": // Preview is secure. this key supports only HTTPS origin.
					_map.key = "ABQIAAAAEEA1aP7uBgiw0w9-FbFU8RQu2Ttu5DQZomECZYfuGO_iwiQMnBSaokU422tggCLDGkyJQTbGJbgBtA";
					break;
				case "SECURE.ZEBRAZAPPS.COM": // Preview is secure. this key supports only HTTPS origin.
					_map.key = "ABQIAAAAEEA1aP7uBgiw0w9-FbFU8RSZz6U1RVtYiFPA3_Ob3DXWK99CAxRJmKKZaqFemyastlQX8YMU1AOwZA";
					break;
                case "WWW.ZEBRAZAPPS.COM": // This is an HTTPS key
					_map.key = "ABQIAAAALM-ImskL2oWWfSIzX9W38xRzyL-I6dD6NFliQ5Q51Diu2dAHHhQMna1JO52qWih4fYxUn6i4vrCYBQ";
                    break;
                case "ZEBRAZAPPS.COM":  // This is an HTTPS key
					_map.key = "ABQIAAAALM-ImskL2oWWfSIzX9W38xSjF3u_5cg2H5NDSfhR7Ok24NfEpRTuhk_Q3dq2U9evAFTwGiGTmHpd9A";
                    break;
				default:
				   _map.key = "ABQIAAAAhLBhHfSdyRYlDInIJyZNcBR6LaS50R3HKsYIYdTVYuhfitkGLBSa4yawdkrx3TwLCYFm7XSYhxhIIQ";
				   break;
			}
			  
			mapObject.map = _map;
			
			mapObject.anchorPoint = mapObject.anchorPoint;  //trigger recalc
			addChild(_map);
			updateEnabledView();
		}

		public function resizeMap():void
		{
			if (!_map){
				return;
			}
			
			_map.setSize(new Point(mapObject.width, mapObject.height));
			setOffset();
			setMapBounds();
		}
		
		private function onMapReady(event:MapEvent):void
		{	
			if (!_map){
				return;
			}
			_map.setCenter(new LatLng(mapObject.latitude,mapObject.longitude), mapObject.zoom, MapType.NORMAL_MAP_TYPE);
			setOffset();
			resizeMap();
			setType();
			//_map.addEventListener(MapMoveEvent.MOVE_END, onMapMoveEnd);
		}
		
		
		//not implemented yet
		private function onMapMoveEnd(event:MapEvent):void{
			
			var center:LatLng = _map.getCenter();
			mapObject.latitude = center.lat();
			mapObject.longitude = center.lng();
			
		}
		
		override public function exactHitTest(stagePoint:Point):Boolean
		{
			return hitTestPoint(stagePoint.x, stagePoint.y, true);
		}
		
		
		override public function hitTestObject(obj:DisplayObject):Boolean
		{
			// compare other object's bounds with map bounds,
			
			var otherBounds:Rectangle = obj.getBounds(this);  // BUG: when map is rotated, this rotates the marquee bounds, makes it larger
			//can't use 'this' bounds, because they are larger than the _map object
			var myBounds:Rectangle = new Rectangle(_map.x, _map.y, _map.width, _map.height);
			return otherBounds.intersects(myBounds);
		}
		
		override public function getBounds(space:DisplayObject):Rectangle
		{
			
			var bounds:Rectangle = _mapBounds.getBounds(space);
			if (bounds.width > 0)
				return bounds;
			else  
				return rectToSpace(object.localBounds, space);
		}
		

        override public function set makeSafeForCaching(makeSafe:Boolean):void
        {
            if (makeSafe) {
                if (!_safeMode) {
                    this.removeChild(_map);
                    showCoverForUnsafeView();
                    _safeMode = true;
                }
            } else if (_safeMode) {
                this.addChild(_map);
                hideCoverForUnsafeView();
                _safeMode = false;
            }
        }
		
		private function setType():void{
			if (!_map){
				return;
			}
			
			try{
			_map.setMapType(setMapType(mapObject));
			}
			catch (error:Error){
				//do something here
			}
			
		}
		
		public static function setMapType(mapObj:MapObject):IMapType
		{
			switch(mapObj.mapType) {		
				case 0:
					return MapType.NORMAL_MAP_TYPE;
				case 1:
					return MapType.SATELLITE_MAP_TYPE;
				case 2:
					return MapType.HYBRID_MAP_TYPE;
				case 3:
					return MapType.PHYSICAL_MAP_TYPE;
					
				default:
					return MapType.NORMAL_MAP_TYPE;
			}
		}

        override public function handleRunningChanged():void {
            super.handleRunningChanged();
            enableMap(Application.running);
        }
    }
}
