/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/20/11
 * Time: 10:55 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.objects {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.DistributionConnection;
import com.alleni.author.model.objects.MetricsConnection;
import com.alleni.author.model.objects.StatsConnection;
import com.alleni.author.model.objects.TopConnection;
import com.alleni.author.view.text.LightLabel;
import com.alleni.author.view.ui.MenuButton;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.Sprite;

public class ConnectionView extends SimpleIconedView {
	public function ConnectionView(context:ViewContext, model:TaconiteModel, role:uint) {
		super(context, model, role);
		if(object is MetricsConnection) {
			var container:Sprite = new Sprite();
			container.addChild(makeLabel(40, "Metrics", -75, -30, 150));
			container.addChild(new MenuButton(nop, makeObject, ["DistributionConnection"], null, null, makeLabel(10, "Dist", -75, -40, 125)));
			container.addChild(new MenuButton(nop, makeObject, ["TopConnection"], null, null, makeLabel(10, "Top", -35, -40, 125)));
			container.addChild(new MenuButton(nop, makeObject, ["StatsConnection"], null, null, makeLabel(10, "Stats", 0, -40, 125)));
			this.icon = container;
		} else if(object is DistributionConnection) {
			this.icon = makeLabel(40, "Distribution", -150, -30, 300);
		} else if(object is TopConnection) {
			this.icon = makeLabel(40, "Top", -75, -30, 150);
		} else if(object is StatsConnection) {
			this.icon = makeLabel(40, "Stats", -75, -30, 150);
		} else {
			this.icon = makeLabel(40, "Error", -75, -30, 150);
		}
	}

	private function nop(btn:Sprite):void {}

	private function makeObject(name:String):void {
		var obj:AbstractObject = ApplicationController.instance.authorController.addObjectForName(name, object.x + 100, object.y, 0, 0);
		obj["key"] = object["key"];
		obj.controller.objectCompleted(obj);
	}

	private function makeLabel(size:Number, text:String, x:Number, y:Number, width:Number):LightLabel {
		var label:LightLabel = new LightLabel(width);
		label.size = size;
		label.bold = true;
		label.color = 0x000000;
		label.text = text;
		label.x = x;
		label.y = y;
		return label;
	}
}
}
