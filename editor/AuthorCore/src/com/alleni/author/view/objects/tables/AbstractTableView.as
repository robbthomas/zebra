package com.alleni.author.view.objects.tables
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.objects.TableHeaderDragMediator;
	import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.TableStatusMessages;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.event.AnchorEvent;
import com.alleni.author.event.ScrollUpdateEvent;
	import com.alleni.author.event.WireEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ScrollModel;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.model.objects.InputText;
	import com.alleni.author.model.objects.StateTable;
	import com.alleni.author.model.objects.TruthTable;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.InspectorValues;
import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.TruthTableScrollView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.objects.AnswerTableView;
	import com.alleni.author.view.objects.ControlBar;
	import com.alleni.author.view.objects.LogicObjectView;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.HorizontalScrollContainer;
	import com.alleni.author.view.ui.IconButton;
	import com.alleni.author.view.ui.MenuButton;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.controls.ColorPicker;
	import com.alleni.author.view.ui.controls.IColorPickerParent;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.IBitmapDrawable;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
import flash.utils.Dictionary;

import mx.collections.IList;
	
	
	public class AbstractTableView extends LogicObjectView implements IColorPickerParent
	{
		// Colors used throughout the table UI
		public static const BLUE_DARK:uint 	= 0x9EC8ED;
		public static const BLUE_LIGHT:uint = 0xBCDDFF;
		public static const GRAY_DARK:uint	= 0xCDCDCD;
		public static const GRAY_LIGHT:uint = 0xE1E1E1;
		public static const GRAY_4D:uint = 0x4D4D4D;
		
		// TODO: some of these positioning constants are redundant with those in TableLayoutManager. They should be removed from here.
		public static const ROW_HEIGHT:Number = 15;
		public static const COLUMN_HEADING_HEIGHT:Number = ROW_HEIGHT;
		public static const COLUMN_WIDTH:Number = 35;
		
		//TOP_MARGIN is in the layout manager
		public static const LEFT_MARGIN:Number = 18;
		public static const RIGHT_MARGIN:Number = 15;
		public static const BOTTOM_MARGIN:Number = 18;
		public static const DEFAULT_COLUMN_WIDTH:Number = 130;
		
		private static const SCROLL_BAR_OFFSET:Number = -33;  //offset 
		private static const SPACER_COLUMN:Number = 3;
		private static const SCROLL_BAR_HEIGHT:Number = 16;
		private static const PORT_START_Y:Number = TableLayoutManager.TOP_MARGIN //+ ROW_HEIGHT;
			
		protected static const INITIAL_INLET_X:Number = 40.0;
		
		protected var _tableGrid:TableGrid;
		protected var _scrollController:TruthTableScrollView; 
		protected var _scrollModel:ScrollModel;
		
		private var _topBar:Sprite;
		
		protected var _layout:TableLayoutManager;
		
		public var currentRow:int = -1;
		public var currentSection:String;
		
		private var _logicRowMinusButton:IconButton;
		private var _logicRowButtonContainer:TableSection;
		private var _valueRowMinusButton:IconButton;
		private var _valueRowButtonContainer:TableSection;
		protected var _statusLabel:LightLabel;
		private var _statusMessageContainer:TableResizableContainer;
		private var _conditionBranchContainer:TableResizableContainer; //This is the area of the branching row below the "condition" heading
		private var _nowBranchContainer:TableResizableContainer; //This is the area of the branching row below the "Now" heading
		
		// Main Containers
		protected var _logicRibbonNameContainer:TableResizableContainer;
		protected var _logicRibbonExpressionContainer:TableResizableContainer;
		protected var _valueRibbonNameContainer:TableResizableContainer;
		protected var _valueRibbonExpressionContainer:TableResizableContainer;
		
		private var _gridContainer:HorizontalScrollContainer;
		private var _columnHeadingRowSection:HorizontalScrollContainer;
		protected var _branchingRow:TableBranchingRow;
		
		private var _ribbonResizeBar:TableColumnResizeBar;
		private var _expressionResizeBar:TableColumnResizeBar;
		private var _gridResizeBar:TableColumnResizeBar;
		private var _ribbonColumnHeading:TableColumnHeading;
		private var _expressionColumnHeading:TableColumnHeading
		private var _tableColumnHeading:TableColumnHeadingRow;
		private var _zeroColumnHeading:TableSection;
		
		protected var _inletContainer:TableSection;
		
		protected var _zeroColumnLogicSection:TruthTableGrid;
		protected var _zeroColumnValueSection:TableGrid;
		protected var _zeroColumnTweenSection:TableTweenGrid;
		protected var _truthPortContainer:TableSection;
		protected var _rightValuePortContainer:TableSection;
		
		protected var _valueRows:Array = [];
		private var _ribbons:Array = []; 
		private var _outletRow:TableOutletRow;
		private var _expandedWidth:Number;
		private var _ribbonColumnWidth:Number = 0;
		private var _gridColumnWidth:Number = 0;
		private var _expressionColumnWidth:Number = 0;
		
		protected var _valueGridContainer:HorizontalScrollContainer; //Contains the Scroll Rect code
		protected var _valueGrid:TableGrid;
		private var _columnPort:PortView;
		private var _colorPicker:ColorPicker;
		
		private var _shadow:DropShadowFilter;
		private var _selectedShadow:DropShadowFilter;
		private var _draggingAreaMuteSprite:Sprite;
		private var _valueColumnAreaMuteSprite:Sprite;
		private var _logicColumnAreaMuteSprite:Sprite;

        private var _isSetup:Boolean = false;
		
		public function AbstractTableView(viewContext:ViewContext, model:TaconiteModel, role:uint)
		{
			super(viewContext, model, role);
			
			/*if(this is StateTableView){
				currentSection = TableHeaderDragMediator.VALUE_ROWS;
			}*/
			
			_context = viewContext;
			_role = role;
			viewState = 0;
			if(!table.loading) {
				setupTable()
			}
		}

        protected function shouldIgnoreChanges(property:String=null):Boolean {
			if(table.rearranging || table.loading) {
				return true;
			}
            if(!_isSetup) {
                if("loading" == property || null == property) {
                    // only run setup responding to loading
                    setupTable();
                    testForScrollBarVisible();
                } else {
                    // we will get the loading signal later, till then ignore
                    return true;
                }
            }
            return false;
        }
		
		private function setupTable():void
		{
			createAndAddChildren();
			var i:uint

			_expandedWidth = _width;
			
			model.addEventListener(WireEvent.COMPLETE, handleWireComplete);
			model.addEventListener(WireEvent.DETACH, handleWireDetach);
			
			addInlets();
            for(i = 0; i<table.guidePositions.length; i++){
                if(table.guidePositions[i] >= 0){
                    _layout.setGuidePosition(i, table.guidePositions[i]);
                }
            }
            _isSetup = true;
		}
		
		private function handleMoveScroll(event:MouseEvent):void
		{
			trace("Move Scroll")
		}

		protected function handleRearranged():void {
			currentColumn = -1;
			var i:int=0;
			var anchors:IList;
			_tableColumnHeading.columns = table.numColumns;
			if(_valueGrid) {
				_valueGrid.handleRearranged(table.numColumns, table.currentStateValues.length, table.allStateValues, table.stateValueType, table.stateValueTypeConstraints, context as WorldContainer);
				_zeroColumnValueSection.handleRearranged(-1, table.currentStateValues.length, table.currentStateValues, table.stateValueType, table.stateValueTypeConstraints, context as WorldContainer);
			}
			if(_valueRows) {
				anchors = table.anchors["currentStateValues"];
				while(_valueRows.length > anchors.length) {
					TableRibbonController(_valueRows.pop()).remove();
				}
				while(_valueRows.length < anchors.length) {
					_valueRows.push(ValueRibbonControllerFactory(_valueRibbonNameContainer, _valueRibbonExpressionContainer, _rightValuePortContainer,
								table, _valueRows.length, IList(table.anchors["currentStateValues"])[_valueRows.length], _context as WorldContainer, role));
				}
				for(i=0; i<_valueRows.length; i++) {
					TableRibbonController(_valueRows[i]).handleRearranged(i, anchors[i], context as WorldContainer);
				}
			}
			if(_valueGridContainer) {
				_valueGridContainer.height = table.currentStateValues.length*ROW_HEIGHT;
			}
			rearrangeTableGrid();
			if(_gridContainer) {
				_gridContainer.height = tableGrid.rows*ROW_HEIGHT;
			}
			if(_zeroColumnLogicSection) {
				_zeroColumnLogicSection.handleRearranged(-1, table.port.length, table.truthPort);
			}
			if(_ribbons && _logicRibbonNameContainer && _logicRibbonExpressionContainer && _truthPortContainer) {
				anchors = table.anchors["port"];
				while(_ribbons.length > anchors.length) {
					TableRibbonController(_ribbons.pop()).remove();
				}
				while(_ribbons.length < anchors.length) {
					_ribbons.push(new TableRibbonLogicController(_logicRibbonNameContainer, _logicRibbonExpressionContainer, _truthPortContainer,
								table, _ribbons.length, IList(table.anchors["port"])[_ribbons.length], _context as WorldContainer, role));
				}
				for(i=0; i<_ribbons.length; i++) {
					TableRibbonController(_ribbons[i]).handleRearranged(i, anchors[i], context as WorldContainer);
				}
			}
			if(_branchingRow){
				_branchingRow.handleRearranged();
			}
			if(_truthPortContainer) {
				while(_truthPortContainer.numChildren > 0) {
					_truthPortContainer.removeChildAt(0);
				}
				for(i=0; i<IList(table.truthPort).length; i++) {
					var anchor:WireAnchor = IList(table.anchors["truthPort"])[i];
					var port:PortView = new PortView(anchor, context as WorldContainer, true);
					_truthPortContainer.addChild(port);
					port.y = i*ROW_HEIGHT;
				}
			}
			if(_tableTweenGrid) {
				_tableTweenGrid.handleRearranged(table.numColumns, 3, table.allTweenValues);22
			}
			
			if(_outletRow){
				_outletRow.handleRearranged();
			}
			
			currentColumn = table.column-1;
			_layout.calculateVerticalLayout();
			_layout.expandGrid(8);
			_layout.pack();

			_scrollModel.setMinMaxScrollPosition(0, COLUMN_WIDTH * table.numColumns);
			this.addEventListener(Event.ENTER_FRAME, testForScrollBarVisible);
		}

		protected function rearrangeTableGrid():void
		{
			if(_tableGrid) {
				_tableGrid.handleRearranged(table.numColumns, table.port.length, null);
			}
		}
		
		protected function handleWireComplete(event:WireEvent, anchor:String=null):void
		{
			event.stopImmediatePropagation();
			var wire:Wire = event.wire;
			var isMaster:Boolean = wire.masterAnchor.hostObject == table;
			var isSlave:Boolean = wire.slaveAnchor.hostObject == table;
			var other:Wire;
			var doPort:Boolean = anchor == null || anchor == "port";
			var doState:Boolean = anchor == null || anchor == "currentStateValues";
			var prop:PropertyDescription;
			var tableEnd:PropertyDescription;
			var column:Number;
			var i:int;
            var anchorObj:WireAnchor;
			if(doPort && isMaster && wire.masterAnchor.hostProperty == "port") {
				prop = wire.slaveAnchor.modifierDescription as PropertyDescription;
				if(wire.slaveAnchor.hostProperty == "text" && wire.slaveAnchor.hostObject is InputText && InputText(wire.slaveAnchor.hostObject).numbersOnly) {
					prop = new PropertyDescription(prop.key, prop.id, prop.label, prop.description, prop.category, prop.weight, prop.indexed, prop.autoAddUpTo, Modifiers.NUMBER_TYPE, null, prop.writeable);
				}
				_ribbons[wire.masterAnchor.hostPropertyIndex].handleWireAdded(wire.slaveAnchor.hostObject.title, prop, event.existing);
                for(var i=0; i<IList(table.anchors["port"]).length; i++) {
                    anchorObj = IList(table.anchors["port"])[i];
                    if(anchorObj.isAdditionAnchor && anchorObj.getViews(context as WorldContainer).length > 0){
                        anchorObj.getViews(context as WorldContainer)[0].updateWireEnd();
                    }
                }
			}
			
			if(doPort && isSlave && wire.slaveAnchor.hostProperty == "port") {
				prop = wire.masterAnchor.modifierDescription as PropertyDescription;
				if(wire.masterAnchor.hostProperty == "text" && wire.masterAnchor.hostObject is InputText && InputText(wire.masterAnchor.hostObject).numbersOnly) {
					prop = new PropertyDescription(prop.key, prop.id, prop.label, prop.description, prop.category, prop.weight, prop.indexed, prop.autoAddUpTo, Modifiers.NUMBER_TYPE, null, prop.writeable);
				}
				_ribbons[wire.slaveAnchor.hostPropertyIndex].handleWireAdded(wire.masterAnchor.hostObject.title, prop, event.existing);
                for(var i=0; i<IList(table.anchors["port"]).length; i++) {
                    var anchorObj:WireAnchor = IList(table.anchors["port"])[i];
                    if(anchorObj.isAdditionAnchor && anchorObj.getViews(context as WorldContainer).length > 0){
                        anchorObj.getViews(context as WorldContainer)[0].updateWireEnd();
                    }
                }
			}
			
			if(doState && isMaster && wire.masterAnchor.hostProperty == "currentStateValues") {
				prop = wire.slaveAnchor.modifierDescription as PropertyDescription;
				tableEnd = wire.masterAnchor.modifierDescription as PropertyDescription;
				if(wire.slaveAnchor.hostProperty == "text" && wire.slaveAnchor.hostObject is InputText && InputText(wire.slaveAnchor.hostObject).numbersOnly) {
					tableEnd = new PropertyDescription(tableEnd.key, tableEnd.id, tableEnd.label, tableEnd.description, tableEnd.category, tableEnd.weight, tableEnd.indexed, tableEnd.autoAddUpTo, Modifiers.NUMBER_TYPE, null, true);
				} else {
					tableEnd = new PropertyDescription(tableEnd.key, tableEnd.id, tableEnd.label, tableEnd.description, tableEnd.category, tableEnd.weight, tableEnd.indexed, tableEnd.autoAddUpTo, prop.type, prop.constraints, true);
				}
				wire.masterAnchor.modifierDescription = tableEnd;
				_valueGrid.setType(wire.masterAnchor.hostPropertyIndex, prop);
				_valueRows[wire.masterAnchor.hostPropertyIndex].handleWireAdded(wire.slaveAnchor.hostObject.title, prop, event.existing);
				table.stateValueType[wire.masterAnchor.hostPropertyIndex] = prop.type;
                for(var i=0; i<IList(table.anchors["currentStateValues"]).length; i++) {
                    var anchorObj:WireAnchor = IList(table.anchors["currentStateValues"])[i];
                    if(anchorObj.isAdditionAnchor && anchorObj.getViews(context as WorldContainer).length > 0){
                        anchorObj.getViews(context as WorldContainer)[0].updateWireEnd();
                    }
                }
			}
			
			if(doState && isSlave && wire.slaveAnchor.hostProperty == "currentStateValues") {
				// this code will not execute unless wiring to another table, since wiring code forces table to be master
				prop = wire.masterAnchor.modifierDescription as PropertyDescription;
				tableEnd = wire.slaveAnchor.modifierDescription as PropertyDescription;
				if(wire.masterAnchor.hostProperty == "text" && wire.masterAnchor.hostObject is InputText && InputText(wire.masterAnchor.hostObject).numbersOnly) {
					tableEnd = new PropertyDescription(tableEnd.key, tableEnd.id, tableEnd.label, tableEnd.description, tableEnd.category, tableEnd.weight, tableEnd.indexed, tableEnd.autoAddUpTo, Modifiers.NUMBER_TYPE, null, true);
				} else {
					tableEnd = new PropertyDescription(tableEnd.key, tableEnd.id, tableEnd.label, tableEnd.description, tableEnd.category, tableEnd.weight, tableEnd.indexed, tableEnd.autoAddUpTo, prop.type, prop.constraints, true);
				}
				wire.slaveAnchor.modifierDescription = tableEnd;
				_valueRows[wire.slaveAnchor.hostPropertyIndex].handleWireAdded(wire.masterAnchor.hostObject.title, prop, event.existing);
				table.stateValueType[wire.slaveAnchor.hostPropertyIndex] = prop.type;
				_valueGrid.setType(wire.slaveAnchor.hostPropertyIndex, prop);
                for(var i=0; i<IList(table.anchors["currentStateValues"]).length; i++) {
                    var anchorObj:WireAnchor = IList(table.anchors["currentStateValues"])[i];
                    if(anchorObj.isAdditionAnchor && anchorObj.getViews(context as WorldContainer).length > 0){
                        anchorObj.getViews(context as WorldContainer)[0].updateWireEnd();
                    }
                }
			}
		}
		
		protected function handleWireDetach(event:WireEvent):void
		{
			event.stopImmediatePropagation();
			var anchor:WireAnchor = event.oldAnchor;
			
			if(anchor == null) {
				return;
			}
			
			if(anchor.modifierDescription.key == "currentStateValues") {
				_valueRows[anchor.hostPropertyIndex].handleWireRemoved(false);
			} else if(anchor.modifierDescription.key == "port") {
				_ribbons[anchor.hostPropertyIndex].handleWireRemoved(false);
			}

            if(table.anchors[anchor.modifierDescription.key] is IList){
                for(var i:int=0; i<IList(table.anchors[anchor.modifierDescription.key]).length; i++) {
                    anchor = IList(table.anchors[anchor.modifierDescription.key])[i];
                    if(anchor.isAdditionAnchor && anchor.getViews(context as WorldContainer).length > 0){
                        anchor.getViews(context as WorldContainer)[0].updateWireEnd();
                    }
                }
            }
		}

        override public function get useRolloverGlow():Boolean
        {
            return false;
        }

		protected function get containerShadow():DropShadowFilter
		{
			if (selected) {
				if (_selectedShadow == null) {
					_selectedShadow = new DropShadowFilter(2,45,0,0.6,6,6,0.8,5);
				}
				return _selectedShadow;
			}
			else {
				if (_shadow == null) {
					_shadow = new DropShadowFilter(1,45,0,0.3,3,3,0.8,5);
				}
				return _shadow;
			}
		}
		
		override protected function createAndAddChildren():void
		{
			// instantiate layout manager
			_layout = new TableLayoutManager(this);
			
			// Add the expand/collapse button at the top of the table
			var collapseButton:IconButton = TableButtons.collapseButton;
			collapseButton.x = 15;
			collapseButton.y = 9;
			collapseButton.addEventListener(MouseEvent.CLICK, collapseButtonClick);
			//addChild(collapseButton);

			var minimizeButton:MenuButton = makeMinimizeButton();
			var minimizeSection:TableSection = new TableSection();
			minimizeSection.addChild(minimizeButton);
			addTableSection(minimizeSection, TableLayoutManager.H_PORT_LEFT, TableLayoutManager.H_TABLE_RIGHT, TableLayoutManager.V_TABLE_TOP);
			
			// Add the title at the top of the table
			_title = new RichTextField();
			_title.text = table.title;
			_title.color = 0xFFFFFF;
			_title.x = 16;
			_title.y = 5;
			addChild(_title);
			
			// create and configure scroll controller
			_scrollModel = new ScrollModel(COLUMN_WIDTH*2,0,COLUMN_WIDTH*tableGrid.columns);
			_scrollController = new TruthTableScrollView(_scrollModel, COLUMN_WIDTH*2,SCROLL_BAR_HEIGHT, table);
			_scrollController.addEventListener(ScrollUpdateEvent.SCROLL, handleScroll);
			_scrollController.visible = false;
			_scrollController.x = SCROLL_BAR_OFFSET - _scrollController.buttonAreaWidth;
			
			// Add scroll bar section
			var scrollTableSection:TableSection = new TableSection();
			scrollTableSection.addChild(_scrollController);
			var scrollLocation:int;
			addTableSection(scrollTableSection, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.H_GUIDE_RULE_3_LEFT, TableLayoutManager.V_SCROLL_TOP);
			
			if (table.showHeading) {
				// Add column heading section
				_ribbonColumnHeading = new TableColumnHeading((table is StateTable)?(table.poweredOnRun?"Read-Write":"Read Only"):(table.poweredOnRun?"Power On":"Power Off"), 21);
				addTableSection(_ribbonColumnHeading, TableLayoutManager.H_TABLE_LEFT, TableLayoutManager.H_GUIDE_RULE_1_LEFT, TableLayoutManager.V_COL_HEADING_TOP);
				var expressionHeadString:String = "Condition";
                if(this is StateTableView){
                    expressionHeadString = "Auto-Fill";
                }
                _expressionColumnHeading = new TableColumnHeading(expressionHeadString, 3);
				addTableSection(_expressionColumnHeading, TableLayoutManager.H_EXPRESSION_LEFT, TableLayoutManager.H_GUIDE_RULE_2_LEFT, TableLayoutManager.V_COL_HEADING_TOP);
				
				// add + and - buttons (if applicable)
				var btnSection:TableSection = new TableSection();
				addTableSection(btnSection, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.H_GUIDE_RULE_3_LEFT, TableLayoutManager.V_TABLE_TOP);
				var last:Sprite;
				for (var n:int=columnHeadingIcons.length-1; n>-1; n--) {
					var sprite:Sprite = Sprite(columnHeadingIcons[n]);
					btnSection.addChild(sprite);
					sprite.x = (last ? last.x : _width + 25) - sprite.width - 4;
					sprite.y = 10;
					last = sprite;
				}
				
				this is TruthTableView?null:createStatusSection();
				this is TruthTableView?null:createConditionBranchSection();
				this is TruthTableView?null:createNowBranchSection();
				this is TruthTableView?null:showBranchingRow();
				
				_zeroColumnHeading = new TableSection();
				var zeroColumnHeadingCell:TableHeadingCell = new TableHeadingCell(table, 0, -1, false);
				zeroColumnHeadingCell.updateValue("Now");
				new TableHeaderDragMediator(zeroColumnHeadingCell, this, TableHeaderDragMediator.COLUMNS, -1);
				_zeroColumnHeading.addChild(zeroColumnHeadingCell);
				addTableSection(_zeroColumnHeading, TableLayoutManager.H_COL_0_LEFT, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.V_COL_HEADING_TOP);
				
				// Add column heading row section
				_tableColumnHeading = new TableColumnHeadingRow(table.numColumns, table, this);
				var _columnHeadingRowSection:HorizontalScrollContainer = new HorizontalScrollContainer(_tableColumnHeading, _scrollController, GRAY_4D);
				_columnHeadingRowSection.height -= 2;
				addTableSection(_columnHeadingRowSection, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.H_GUIDE_RULE_3_LEFT, TableLayoutManager.V_COL_HEADING_TOP);
				
				// Add column port section
				var anchor:WireAnchor = table.anchors["activeColumn"];
				_columnPort = new PortView(anchor, context as WorldContainer, true);
				_columnPort.angle = 90;
				var columnPortSection:TableSection = new TableSection();
				columnPortSection.addChild(_columnPort);
				columnPortSection.graphics.beginFill(GRAY_4D);
				var fillerHeight:Number = AbstractTableView.ROW_HEIGHT;
				if(!(table is TruthTable)){
					fillerHeight = fillerHeight*2 + 2;
				}
				columnPortSection.graphics.drawRect(0,0,20,fillerHeight);
				columnPortSection.graphics.endFill();
				columnPortSection.graphics.lineStyle(2);
				columnPortSection.graphics.moveTo(0,AbstractTableView.ROW_HEIGHT);
				columnPortSection.graphics.lineTo(20, AbstractTableView.ROW_HEIGHT);
				columnPortSection.graphics.lineStyle();
				addTableSection(columnPortSection, TableLayoutManager.H_PORT_LEFT, TableLayoutManager.H_TABLE_RIGHT, TableLayoutManager.V_COL_HEADING_TOP);
				
				// Add PowerButton
				var powerPorts:Sprite = addPowerPorts();
				powerPorts.x = 4;
				powerPorts.y = 4;
				addChild(powerPorts);
			}
			
			// Add right side value port view container
			_rightValuePortContainer = new TableSection();
			_tableTweenLabels = new TableTweenLabels();
						
			createInitialSections();
			
			// Add initial port views
			createInitialPortViews();
			if(table.showValueRows) {
				showValueRows(true);
			}

			if(table.showControls) {
				showControls(true);
			}
			
			for each(var wire:Wire in ApplicationController.instance.wireController.getWires(table)) {
				handleWireComplete(new WireEvent(WireEvent.COMPLETE, wire, true));
			}
			if(table.showTweens) {
				showTweens(true);
			}
			if(table.showOuts) {
				showOuts(true);
			}
			
			// Add resize bars
			_ribbonResizeBar = new TableColumnResizeBar(_context, this, _layout, 0);
			_expressionResizeBar = new TableColumnResizeBar(_context, this, _layout, 1);
			_gridResizeBar = new TableColumnResizeBar(_context, this, _layout, 2);
			
			currentColumn = table.column - 1;
			
			// Update layout
			_layout.pack();
		}
		
		override protected function addPowerPorts():Sprite{
			var powerPorts:Sprite = new Sprite();
			
			powerPorts.graphics.beginFill(0x9A9A9A);
			powerPorts.graphics.drawRoundRect(0,0,11,30,10,10);
			powerPorts.graphics.endFill();
			
			_powerOffPortView = new PortView(table.anchors["powerOff"], context as WorldContainer, true);
			_powerOffPortView.x = -3.75;
			_powerOffPortView.y = 0;
			_powerOffPortView.angle = 180;
			_powerOffPortView.powerColor = 0xBFBFBF;
			powerPorts.addChild(_powerOffPortView);
			
			_powerOnPortView = new PortView(table.anchors["powerOn"], context as WorldContainer, true);
			_powerOnPortView.x = -3.75;
			_powerOnPortView.y = 17;
			_powerOnPortView.angle = 180;
			_powerOnPortView.powerColor = 0xFFE600;
			powerPorts.addChild(_powerOnPortView);
			
			return powerPorts;
		}
		
		protected function addTableSection(tableSection:TableSection, leftGuide:int, rightGuide:int, topGuide:int):void
		{
			tableSection.leftGuide = leftGuide;
			tableSection.rightGuide = rightGuide;
			tableSection.topGuide = topGuide;
			
			addChild(tableSection);
			_layout.sections.push(tableSection);
		}
		
		
		protected function removeTableSection(tableSection:TableSection):void 
		{
			removeChild(tableSection);
			_layout.sections.splice(_layout.sections.indexOf(tableSection), 1);
		}
		
		protected function createInitialPortViews():void
		{	
			// For logic section
			if (_logicRibbonNameContainer != null) {
				var anchor:WireAnchor;
				var i:int;
				for(i=0; i<IList(table.anchors["port"]).length; i++) {
					
					anchor = IList(table.anchors["port"])[i];
					var logicRibbon:TableRibbonLogicController = new TableRibbonLogicController(_logicRibbonNameContainer, _logicRibbonExpressionContainer, _truthPortContainer,
						table, i, anchor, _context as WorldContainer, role);
					if(anchor.isAdditionAnchor == false && ApplicationController.instance.wireController.getWiresForAnchor(anchor).length == 0) {
						logicRibbon.handleWireRemoved(true);
					}
					_ribbons.push(logicRibbon);
				}
				
				for(i=0; i<IList(table.truthPort).length; i++) {
					anchor = IList(table.anchors["truthPort"])[i];
					var port:PortView = new PortView(anchor, context as WorldContainer, true);
					_truthPortContainer.addChild(port);
					port.y = i*ROW_HEIGHT;
				}
			}
			
			// For values section
			for(i=0; i<IList(table.anchors["currentStateValues"]).length; i++) {
				anchor = IList(table.anchors["currentStateValues"])[i];

				var valueRibbon:TableRibbonValueController = ValueRibbonControllerFactory(_valueRibbonNameContainer, _valueRibbonExpressionContainer, _rightValuePortContainer,
					table, i, anchor, _context as WorldContainer, role);
				if(anchor.isAdditionAnchor == false && ApplicationController.instance.wireController.getWiresForAnchor(anchor).length == 0) {
					valueRibbon.handleWireRemoved(true);
				}
				_valueRows.push(valueRibbon);
			}
		}
		
		protected function ValueRibbonControllerFactory(ribbonColumn:TableResizableContainer, expressionColumn:TableResizableContainer, truthPortContainer:Sprite, 
															table:AbstractTable, index:int, anchor:WireAnchor, context:WorldContainer, role:int):TableRibbonValueController
		{
			return new TableRibbonValueController(ribbonColumn, expressionColumn, truthPortContainer, table, index, anchor, context, role);
		}
		
		protected function get columnHeadingIcons():Array
		{
			// to be overridden by subclasses...
			var locateButton:IconButton = TableButtons.locateButton;
			locateButton.addEventListener(MouseEvent.CLICK, handleLocateClick);
			
			return [locateButton];	
		}
		
		protected function handleLocateClick(e:MouseEvent):void{
			this.addEventListener(Event.ENTER_FRAME, scrollToCurrentColumn);
		}
		
		protected function handleMinusButtonClick(e:MouseEvent):void
		{
			if(table.numColumns > 1 && table.column > 0){
				table.fireBranches = false;
				table.removeColumn(table.column - 1);
				if(table.column ==0){
					table.column = 1;
				}
				table.rearranging = false;
				handleRearranged();
				if(table.numColumns <= 8){
					_layout.setGuidePosition(2, ((table.numColumns + 1) * COLUMN_WIDTH) + _expressionResizeBar.x + _expressionResizeBar.width);
				}
                table.updateForAssetChange();
			}
		}
		
		protected function handlePlusButtonClick(e:MouseEvent):void
		{
			table.numColumns++;
            table.recordInitialValue("numColumns");
			if(table is StateTable){
				table.column = table.numColumns;
                table.recordInitialValue("column");
			}
            table.updateForAssetChange();
		}
		
		private function collapseButtonClick(e:MouseEvent):void
		{
			
			if (viewState<2)
				viewState++
			else
				viewState = 0;
			
			updateViewState();
		}
		
		private function updateViewState():void
		{
			/*	Delayed until post-November - LML
			
			switch (viewState) {
			case 0: // Expanded
			trace(">>EXPANDED=guide1="+_guide1X, "guide2="+_guide2X, "guide3="+_guide3X);
			_layout.setGuidePosition(0,_guide1X, true);
			_layout.setGuidePosition(1,_guide2X, true);
			_layout.setGuidePosition(2,_guide3X, true); 
			//	setCollapseStateVisibility(true);
			break;
			case 1: // Only ribbons showing
			trace(">>Ribbons only");
			trace("guide1="+_guide1X, "guide2="+_guide2X, "guide3="+_guide3X);
			captureContainerWidths();
			trace("	>>>>after capture=guide1="+_guide1X, "guide2="+_guide2X, "guide3="+_guide3X);
			_layout.setGuidePosition(2,_guide2X+COLUMN_WIDTH, true); 
			break;
			case 2: // Collapsed
			trace(">>COLLAPSED");
			//	setCollapseStateVisibility(true);
			_layout.setGuidePosition(0,LEFT_MARGIN, true);
			_layout.setGuidePosition(1,LEFT_MARGIN+3, true);
			_layout.setGuidePosition(2,LEFT_MARGIN+COLUMN_WIDTH, true); 
			break;
			}*/
		}
		
		private function setCollapseStateVisibility(value:Boolean):void
		{
			_title.visible = value;
			
			/*	if (_outletRow) _outletRow.visible = value;
			for each (var valueRow:TableValueRow in _valueRows) {
			valueRow.visible = value;
			}*/
		}
		
		/*	private var _guide1X:Number = 0;
		private var _guide2X:Number = 0;
		private var _guide3X:Number = 0;
		private function captureContainerWidths():void
		{
		_guide1X = _layout.getGuidePosition(0);
		_guide2X = _layout.getGuidePosition(1);
		_guide3X = _layout.getGuidePosition(2);
		}*/
		
		private function handleScroll(e:ScrollUpdateEvent):void
		{
			ApplicationController.instance.wireController.requestRedrawForObject(model.value as AbstractObject);
		}
		
		override public function set height(value:Number):void
		{
			_height = value;
			updateView();
		}
		
		override public function set width(value:Number):void
		{
			_width = value;
			updateView();
		}
		
		override public function get width():Number
		{
			return _width;	
		}
		
		
		protected function createGrid():void
		{
			_tableGrid = new TableGrid(table,this,table.numColumns,table.port.length, null, TableHeaderDragMediator.LOGIC_ROWS);
		}
		
		protected function get tableGrid():TableGrid
		{
			if (_tableGrid == null)
				createGrid();
			
			return _tableGrid;
		}
		
		override protected function ObjectFeedbackFactory():ITaconiteView
		{
			return null;
		}
		
		override protected function updateThumbnail():void
		{
			// prevent thumbnails on truthtables
		}
		
		public function buildColorPickerIfNeeded():void 
		{
			if(!_colorPicker){
				_colorPicker = new ColorPicker(EditorUI.instance);
				_colorPicker.draw();
				addChild(_colorPicker);
			}
			_colorPicker.x = this.mouseX - ((_colorPicker.width/2) + 25);
		}
		
		public function get colorPicker():ColorPicker 
		{
			return _colorPicker;
		}
		
		override public function warnViewToDelete():void
		{
			//	_mediator.clearViewEvents();
			clearAuthorNote();
		}
		
		override protected function updateArrayProperty(property:String, modelEventKind:String, array:IList, index:int):Boolean
		{
			var handled:Boolean = super.updateArrayProperty(property, modelEventKind, array, index);
			var anchorIndex:int = index;
			if (index == IList(table[property]).length-1) {
				// adding at the end position, so actually insert after the current end
				// to preserve newly created wires
				anchorIndex++;
			}
            if(shouldIgnoreChanges(property)) {
				return handled;
			}
			switch(modelEventKind) {
				case ModelEvent.ADD_ARRAY_ITEM:
					switch(property) {
						case "port":
							if(_ribbons[anchorIndex-1].wired == false) {
								_ribbons[anchorIndex-1].handleWireRemoved(false);
							}
                            if(IList(table.anchors["port"]).length > anchorIndex){
						    	_ribbons.splice(anchorIndex,0, new TableRibbonLogicController(_logicRibbonNameContainer, _logicRibbonExpressionContainer, _truthPortContainer,
							    	table, anchorIndex, IList(table.anchors["port"])[anchorIndex], _context as WorldContainer, role));
                            }
							//	_ribbons[anchorIndex].y = anchorIndex*ROW_HEIGHT + TOP_MARGIN;
							// add outlet ports as necessary
							tableGrid.addRow();
							_gridContainer.height = tableGrid.rows*ROW_HEIGHT;
							_layout.calculateVerticalLayout();
							if(_logicRowMinusButton != null){
								_logicRowMinusButton.visible = table.port.length > 1 && currentRow > -1 && table is AnswerTable && currentSection == TableHeaderDragMediator.LOGIC_ROWS;
							}
							ApplicationController.instance.wireController.requestRedrawForObject(table);
							return false;
						case "truthPort":
							if(_truthPortContainer) {
								var port:PortView = new PortView(table.anchors["truthPort"][index], context as WorldContainer, true);
								_truthPortContainer.addChild(port);
								port.y = index*ROW_HEIGHT;
								_zeroColumnLogicSection.addRow();
							}
							return false;
						case "currentStateValues":
							if(_valueRows[anchorIndex-1].wired == false) {
								_valueRows[anchorIndex-1].handleWireRemoved(false);
							}
							_valueRows.splice(anchorIndex,0, ValueRibbonControllerFactory(_valueRibbonNameContainer, _valueRibbonExpressionContainer, _rightValuePortContainer,
								table, anchorIndex, IList(table.anchors["currentStateValues"])[anchorIndex], _context as WorldContainer, role));
							
							_valueGrid.addRow();
							_zeroColumnValueSection.addRow();
							_valueGridContainer.height = table.currentStateValues.length*ROW_HEIGHT;
							_layout.calculateVerticalLayout();
							_valueRowMinusButton.visible = table.currentStateValues.length > 1 && currentRow > -1 && currentSection == TableHeaderDragMediator.VALUE_ROWS;
							ApplicationController.instance.wireController.requestRedrawForObject(table);
							return false;
						case "currentTweenValues":
							// TODO
							return false;
							
					}
					break;
				case ModelEvent.CHANGE_ARRAY_ITEM:
					var row:int = index / (table.numColumns);
					var column:int = index % (table.numColumns);
					switch(property) {
						case "allStateValues":
							if(_valueGrid) {
								_valueGrid.setValue(row, column, table.getStateValue(row, column));
							}
							break;
						case "truthPort":
							if(_zeroColumnLogicSection) {
								_zeroColumnLogicSection.setValue(index, 0, table.truthPort[index]);
							}
							break;
						case "currentStateValues":
							if(_zeroColumnValueSection) {
								_zeroColumnValueSection.setValue(index, 0, table.currentStateValues[index]);
							}
							break;
						case "currentTweenValues":
							if(_zeroColumnTweenSection) {
								_zeroColumnTweenSection.setValue(index, 0, table.currentTweenValues[index]);
							}
							break;
						case "allTweenValues":
							if(_tableTweenGrid) {
								_tableTweenGrid.setValue(row, column, table.getTweenValue(row, column));
							}
							break;
						case "stateValueType":
							if(_valueGrid) {
								_valueGrid.setType(index, new PropertyDescription("", "", "", "", "", 0, false, -1, table.stateValueType[index], table.stateValueTypeConstraints[index]));
							}
							if (_zeroColumnValueSection) {
								_zeroColumnValueSection.setType(index, new PropertyDescription("", "", "", "", "", 0, false, -1, table.stateValueType[index], table.stateValueTypeConstraints[index]));
							}
                            break;
						case "logicRowName":
							if(_ribbons) {
								_ribbons[index].updateName();
							}
							break;
						case "stateValueName":
							if(_valueRows) {
								_valueRows[index].updateName();
							}
							break;
                        case "portValue":
                            if(_ribbons){
                                _ribbons[index].updateExpression();
                            }
                            break;
                        /*case "logicRowName": // removing punt to explore if this caused table issues
                            if(_ribbons && index in _ribbons) {
                                _ribbons[index].updateName();
                            } else {
								LogService.error("AbstractTableView: Failure to updateName for _ribbons["+index+"]");
							}
							break;
                        case "stateValueName":
                            if(_valueRows && index in _valueRows) {
                                _valueRows[index].updateName();
							} else {
								LogService.error("AbstractTableView: Failure to updateName for _valueRows["+index+"]");
							}
							break;*/
					}
			}
			/*(_model.value as AbstractTable).fireBranches = true;*/
			return handled;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean 
		{
			//trace("AbstractTableView::property="+property);
			var handled:Boolean = super.updateModelProperty(property, oldValue, newValue);
			if(shouldIgnoreChanges(property.toString())) {
				return handled;
			}
			switch(property) {
				case "rearranging":
					if(table.rearranging == false) {
						handleRearranged();
					}
					break;
				case "column":
					currentColumn = table.column - 1;
					//	changeScrollPosition();
					return true;
				case "showTweens":
					showTweens(newValue as Boolean);
					return true;
				case "showOuts":
					showOuts(newValue as Boolean);
					return true;
				case "showValueRows":
					if(!(this is StateTableView)){
                        if(newValue is Boolean && newValue == false){
                            for each (var valueAnchor:WireAnchor in (this.model.value as AbstractTable).anchors["currentStateValues"]) {
                                if(valueAnchor.wired == true){
                                    this.addEventListener(Event.ENTER_FRAME, resetValueRowButton);
                                    return true;
                                }
                            }
                        }
                        showValueRows(newValue as Boolean);
					}
					return true;
				case "showControls":
					showControls(newValue);
					return true;
				case "numColumns":
					tableGrid.addColumns(table.numColumns);
					_tableColumnHeading.columns = table.numColumns;
					if(_valueGrid) {
						_valueGrid.addColumns(table.numColumns);
					}
					if (_outletRow) {
						_outletRow.addPorts(table.numColumns);
					}
					if (_tableTweenGrid) {
						_tableTweenGrid.addColumns(table.numColumns);
					}
					if (_branchingRow){
                        _branchingRow.handleRearranged();
                    }
					_scrollModel.setMinMaxScrollPosition(0, COLUMN_WIDTH * table.numColumns);
					addEventListener(Event.ENTER_FRAME, testForScrollBarVisible);
					addEventListener(Event.ENTER_FRAME, jumpToCurrentColumn);
					_layout.expandGrid(8);
//					if (_branchingRow)
//						_branchingRow.addColumn(table.numColumns);
					return true;
				case "ticking":
					_controls.setPlayButtonState(Boolean(newValue)?ControlBar.PLAYING:ControlBar.PAUSED);
					return true;
				case "poweredOnRun":
					if(_ribbonColumnHeading){
						_ribbonColumnHeading.labelField = (table is StateTable)?(table.poweredOnRun?"Read-Write":"Read Only"):(table.poweredOnRun?"Power On":"Power Off");
					}
					if(table.poweredOnRun){
						_powerOnPortView.powerColor = 0xFFE600;
						_powerOffPortView.powerColor = 0xBFBFBF;
					}else{
						_powerOffPortView.powerColor = 0xFFE600;
						_powerOnPortView.powerColor = 0xBFBFBF;
					}
					break;
			}
			return handled;
		}

        private function resetValueRowButton(event:Event):void{
            this.removeEventListener(Event.ENTER_FRAME, resetValueRowButton);
            (this.model.value as AbstractTable).showValueRows = true;
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:151}));
        }

		public function testForScrollBarVisible(event:Event = null):void{
			this.removeEventListener(Event.ENTER_FRAME, testForScrollBarVisible);
			if(_scrollController.documentLength > _scrollController.model.viewPortLength){
				_scrollController.visible = true;
			}else{
				_scrollController.visible = false;
			}
		}
		
		//SHOW COLUMN
		private function scrollToLastColumn(event:Event):void{
			removeEventListener(Event.ENTER_FRAME, scrollToLastColumn);
			var newScrollPosition:Number = ((table.numColumns * COLUMN_WIDTH) - (COLUMN_WIDTH/2)) - (_scrollController.model.viewPortLength / 2);
			Tweener.addTween(_scrollController, {scrollPosition:newScrollPosition, time:0.75});
		}
		
		private function scrollToCurrentColumn(event:Event):void{
			removeEventListener(Event.ENTER_FRAME, scrollToCurrentColumn);
			var newScrollPosition:Number = ((table.column * COLUMN_WIDTH) - (COLUMN_WIDTH/2)) - (_scrollController.model.viewPortLength / 2);
			Tweener.addTween(_scrollController, {scrollPosition:newScrollPosition, time:0.75});
		}
		
		private function jumpToCurrentColumn(event:Event):void{
			removeEventListener(Event.ENTER_FRAME, jumpToCurrentColumn);
			var newScrollPosition:Number = ((table.column * COLUMN_WIDTH) - (COLUMN_WIDTH/2)) - (_scrollController.model.viewPortLength / 2);
			_scrollController.scrollPosition = newScrollPosition;
		}
		
		public function updateScrollLocation(adjustment:Number):void{
			_scrollController.scrollPosition -= adjustment;
		}
		
		private var _currentCell:TableCell;
		
		private function changeScrollPosition():void
		{
			//	Tweener.addTween(_scrollController, {scrollPosition:(table.column-1)*COLUMN_WIDTH, time:0.75, transition:"easeOutElastic"});
			//	_scrollController.scrollPosition = ((table.column-1)*COLUMN_WIDTH)/((table.numColumns-1)*COLUMN_WIDTH);
		}
		
		private function updateContainerHeights():void
		{
			_logicRibbonNameContainer.height += ROW_HEIGHT;
			_logicRibbonExpressionContainer.height += ROW_HEIGHT;
		}
		
		private var _currentColumn:int;
		
		protected function set currentColumn(value:int):void
		{
			// unhighlight old column
			if(_currentColumn >= 0) {
				tableGrid.unhighlightColumn(_currentColumn);
				_valueGrid.unhighlightColumn(_currentColumn);
				if (table.showHeading)
					_tableColumnHeading.unhighlightColumn(_currentColumn);
			} else if (_zeroColumnLogicSection) {
				_zeroColumnLogicSection.unhighlightColumn(0);
				_zeroColumnValueSection.unhighlightColumn(0);
			}
			
			// highglight new column
			if(value >= 0) {
				tableGrid.highlightColumn(value);
				_valueGrid.highlightColumn(value);
				if (table.showHeading)
					_tableColumnHeading.highlightColumn(value);
			} else if (_zeroColumnLogicSection) {
				_zeroColumnLogicSection.highlightColumn(0);
				_zeroColumnValueSection.highlightColumn(0);
			}
			
			_currentColumn = value;
		}
		
		public function setCurrentRow(row:int, section:String):void{
			//unhilight old row
			if(currentRow > -1){
				switch(currentSection){
					case TableHeaderDragMediator.LOGIC_ROWS:
						if(currentRow > tableGrid.rows - 1){
							break;
						}
						tableGrid.unhighlightRow(currentRow);
						_zeroColumnLogicSection.unhighlightRow(currentRow);
						(_ribbons[currentRow] as TableRibbonLogicController).unhighlight();
						break;
					case TableHeaderDragMediator.VALUE_ROWS:
						if(currentRow > _valueGrid.rows - 1){
							break;
						}
						_valueGrid.unhighlightRow(currentRow);
						_zeroColumnValueSection.unhighlightRow(currentRow);
						(_valueRows[currentRow] as TableRibbonValueController).unhighlight();
						break;
				}
			}
			//higlight new row
			if((row != currentRow || section != currentSection) && row > -1){
				switch(section){
					case TableHeaderDragMediator.LOGIC_ROWS:
						tableGrid.highlightRow(row);
						_zeroColumnLogicSection.highlightRow(row);
						(_ribbons[row] as TableRibbonLogicController).highlight();
						break;
					case TableHeaderDragMediator.VALUE_ROWS:
						_valueGrid.highlightRow(row);
						_zeroColumnValueSection.highlightRow(row);
						(_valueRows[row] as TableRibbonValueController).highlight();
						break;
				}
				currentRow = row;
				
			}else{
				if(section == currentSection){
					currentRow = -1
				}
			}
			
			currentSection = section;

			if(_logicRowMinusButton != null){
				_logicRowMinusButton.visible = table.port.length > 1 && currentRow > -1 && table is AnswerTable && currentSection == TableHeaderDragMediator.LOGIC_ROWS;
			}
			
			if(_valueRowMinusButton != null){
				_valueRowMinusButton.visible = table.currentStateValues.length > 1 && currentRow > -1 && currentSection == TableHeaderDragMediator.VALUE_ROWS;
			}
		} 
		
		private var _controls:ControlBar;
		private var _controlsSprite:Sprite;
		public function showControls(show:Boolean):void
		{
			if (_controls == null) {
				_controls = new ControlBar();
				_controlsSprite = _controls.makeNavControl(goToBegninning, goToPrevious, play, goToNext, goToEnd);
				_controlsSprite.x = DEFAULT_COLUMN_WIDTH + LEFT_MARGIN;
				_controlsSprite.y = 4;
				addChild(_controlsSprite);
			}	
			
			_controlsSprite.visible = show;
		}

        public function updateControlsPosition(newPos:Number):void{
            if(this is StateTableView){
                _controlsSprite.x = newPos;
            }
        }
		
		private function goToBegninning():void
		{
			table.column = 0;
		}
		
		private function goToPrevious():void
		{
			table.column--;
		}
		
		private function play():void
		{
			var stateTable:StateTable = table as StateTable;
			if(stateTable) {
				stateTable.ticking = !stateTable.ticking;
                stateTable.fireBranches = stateTable.ticking;
				if(stateTable.ticking) {
					stateTable.column++;
				}
			}
		}
		
		private function goToNext():void
		{
			table.column++;
		}
		
		private function goToEnd():void
		{
			table.column = table.numColumns;
		}
		
		public function showLogicRows():void
		{
			
		}
		
		protected function showBranchingRow():void
		{
			_branchingRow = new TableBranchingRow(table, _scrollController);
			_branchingRow.addColumn(table.numColumns);
			addTableSection(_branchingRow, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.H_GUIDE_RULE_3_LEFT, TableLayoutManager.V_BRANCHING_TOP);
			
			_layout.pack();
			_layout.calculateVerticalLayout();
		}
		
		public function showValueRows(show:Boolean):void
		{
			if (_valueGrid == null)
				createValueSection();
			
			var valueLocation:int;
			if (show && _valueRibbonNameContainer.parent != this) {
				if(this is StateTableView){
					valueLocation = TableLayoutManager.V_LOGIC_TOP;
				}else{
					valueLocation = TableLayoutManager.V_VALUE_TOP;
				}
				addTableSection(_valueRibbonNameContainer, TableLayoutManager.H_TABLE_LEFT, TableLayoutManager.H_GUIDE_RULE_1_LEFT, valueLocation);
				addTableSection(_valueRibbonExpressionContainer, TableLayoutManager.H_EXPRESSION_LEFT, TableLayoutManager.H_GUIDE_RULE_2_LEFT, valueLocation);
				addTableSection(_valueGridContainer, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.H_GUIDE_RULE_3_LEFT, valueLocation);
				addTableSection(_rightValuePortContainer, TableLayoutManager.H_PORT_LEFT, TableLayoutManager.H_TABLE_RIGHT, valueLocation);
				addTableSection(_zeroColumnValueSection, TableLayoutManager.H_COL_0_LEFT, TableLayoutManager.H_GRID_LEFT, valueLocation);
				
				_layout.pack();
				_layout.calculateVerticalLayout();
			} else if(!show && _valueRibbonNameContainer.parent == this) {
				removeTableSection(_valueRibbonNameContainer);
				removeTableSection(_valueRibbonExpressionContainer);
				removeTableSection(_valueGridContainer);
				removeTableSection(_rightValuePortContainer);
				removeTableSection(_zeroColumnValueSection);
				_layout.calculateVerticalLayout();
			}
		}
		
		private var _tableTweenLabels:TableTweenLabels;
		private var _tableTweenGrid:TableTweenGrid;
		private var _tweenGridScrollContainer:HorizontalScrollContainer; //Contains the Scroll Rect code
		public function showTweens(show:Boolean):void
		{
			if (show && _tableTweenLabels.parent != this) {
				addTableSection(_tableTweenLabels, TableLayoutManager.H_RIBBON_LEFT, TableLayoutManager.H_GUIDE_RULE_2_LEFT, TableLayoutManager.V_TWEEN_TOP);
				
				// add tween grid
				_tableTweenGrid = new TableTweenGrid(table, this, table.numColumns, 3, table.allTweenValues);
				_tweenGridScrollContainer = new HorizontalScrollContainer(_tableTweenGrid, _scrollController, 0x000000);
				_tweenGridScrollContainer.height = ROW_HEIGHT * 3;
				addTableSection(_tweenGridScrollContainer, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.H_GUIDE_RULE_3_LEFT, TableLayoutManager.V_TWEEN_TOP);
//				_zeroColumnTweenSection = new TableTweenGrid(table, -1, 3, table.currentTweenValues);
//				addTableSection(_zeroColumnTweenSection, TableLayoutManager.H_COL_0_LEFT, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.V_TWEEN_TOP);
				
				// TODO: This works, but why call both (i.e., can pack() updateVertical layout?)
				_layout.pack();
				_layout.calculateVerticalLayout();
			} 
			else if(!show && _tableTweenLabels.parent == this) {
				removeTableSection(_tableTweenLabels);
				removeTableSection(_tweenGridScrollContainer);
//				removeTableSection(_zeroColumnTweenSection);
				_layout.calculateVerticalLayout();
			}
			
		}
		
		public function get table():AbstractTable
		{
			return model.value as AbstractTable;
		}
		
		private var rightWireBaySection:TableSection = new TableSection();
		public function showOuts(show:Boolean):void
		{
			if (_outletRow == null)
				createOutletRow();
			
			if (show && _outletRow.parent != this) {
				addTableSection(_outletRow, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.H_GUIDE_RULE_3_LEFT, TableLayoutManager.V_OUTLET_TOP);
				addTableSection(rightWireBaySection, TableLayoutManager.H_PORT_LEFT, TableLayoutManager.H_TABLE_RIGHT, TableLayoutManager.V_OUTLET_TOP);
				
				// TODO: This works, but why call both (i.e., can pack() updateVertical layout?)
				_layout.pack();
				_layout.calculateVerticalLayout();
			} else if(!show && _outletRow.parent == this) {
				removeTableSection(_outletRow);
				removeTableSection(rightWireBaySection);
				_layout.calculateVerticalLayout();
			}
		}
		
		private function get wireBay():Sprite
		{
			var sprite:Sprite = new Sprite();
			sprite.graphics.lineStyle(2, TableOutletRow.COLOR);
			sprite.graphics.drawRect(0,0,TableOutletRow.WIRE_BAY_WIDTH,3/4*_outletRow.height);
			
			return sprite;
		}
		
		// Logic, value, tween, outs, branching
		protected function createNowBranchSection():void{
			var containerBackground:Sprite = new Sprite();
			containerBackground.graphics.beginFill(AbstractTableView.GRAY_4D);
			containerBackground.graphics.drawRect(0, 0, COLUMN_WIDTH, TableLayoutManager.BRANCHING_SECTION_HEIGHT);
			containerBackground.graphics.endFill();
			
			_nowBranchContainer = new TableResizableContainer(COLUMN_WIDTH, TableLayoutManager.BRANCHING_SECTION_HEIGHT);
			_nowBranchContainer.addChild(containerBackground);
			addTableSection(_nowBranchContainer, TableLayoutManager.H_COL_0_LEFT, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.V_BRANCHING_TOP);
		}
		
		protected function createConditionBranchSection():void{
			var containerBackground:Sprite = new Sprite();
			containerBackground.graphics.beginFill(AbstractTableView.GRAY_4D);
			containerBackground.graphics.drawRect(0, 0, (_layout.horizontalGuidePosition[TableLayoutManager.H_GUIDE_RULE_2_LEFT] - _layout.horizontalGuidePosition[TableLayoutManager.H_GUIDE_RULE_1_LEFT]), TableLayoutManager.BRANCHING_SECTION_HEIGHT);
			containerBackground.graphics.endFill();
			
			_conditionBranchContainer = new TableResizableContainer(DEFAULT_COLUMN_WIDTH, TableLayoutManager.BRANCHING_SECTION_HEIGHT);
			_conditionBranchContainer.addChild(containerBackground);
			addTableSection(_conditionBranchContainer, TableLayoutManager.H_EXPRESSION_LEFT, TableLayoutManager.H_GUIDE_RULE_2_LEFT, TableLayoutManager.V_BRANCHING_TOP);
		}
		
		protected function createStatusSection():void{
			var containerBackground:Sprite = new Sprite();
			containerBackground.graphics.beginFill(AbstractTableView.GRAY_4D);
			containerBackground.graphics.drawRect(0, 0, _layout.horizontalGuidePosition[TableLayoutManager.H_GUIDE_RULE_1_LEFT], TableLayoutManager.BRANCHING_SECTION_HEIGHT);
			containerBackground.graphics.endFill();
			
			if(this is AnswerTableView){
				_statusLabel.color = 0xFFFFFF;
				_statusLabel.italic = true;
				_statusLabel.x = _layout.horizontalGuidePosition[TableLayoutManager.H_RIBBON_LEFT];
				_statusLabel.text = TableStatusMessages.MESSAGES[(model.value as AbstractTable).statusMessage];
				
				var statusPort:PortView = new PortView(table.anchors["statusMessage"], context as WorldContainer, true);
				statusPort.x = 0;
				statusPort.y = 2;
				statusPort.width = 20;
				statusPort.angle = 180;
			}
			
			_statusMessageContainer = new TableResizableContainer(DEFAULT_COLUMN_WIDTH, TableLayoutManager.BRANCHING_SECTION_HEIGHT);
			_statusMessageContainer.addChild(containerBackground);
			if(this is AnswerTableView){
				_statusMessageContainer.addChild(statusPort);
				_statusMessageContainer.addChild(_statusLabel);
			}
			addTableSection(_statusMessageContainer, TableLayoutManager.H_TABLE_LEFT, TableLayoutManager.H_GUIDE_RULE_1_LEFT, TableLayoutManager.V_BRANCHING_TOP);
		}
		
		protected function createLogicSection():void
		{	
			// Add grid section
			_gridContainer = new HorizontalScrollContainer(tableGrid, _scrollController, GRAY_LIGHT);
			addTableSection(_gridContainer, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.H_GUIDE_RULE_3_LEFT, 4);
			_gridContainer.bottomGuide = TableLayoutManager.V_SCROLL_TOP;
			
			_zeroColumnLogicSection = new TruthTableGrid(table, this, -1, table.port.length, table.truthPort);
			addTableSection(_zeroColumnLogicSection, TableLayoutManager.H_COL_0_LEFT, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.V_LOGIC_TOP);
			
			// Add ribbon name section
			_logicRibbonNameContainer = new TableResizableContainer(DEFAULT_COLUMN_WIDTH,TableLayoutManager.TOP_MARGIN+ROW_HEIGHT*2);
			addTableSection(_logicRibbonNameContainer, TableLayoutManager.H_TABLE_LEFT, TableLayoutManager.H_GUIDE_RULE_1_LEFT, TableLayoutManager.V_LOGIC_TOP);
			
			// Add ribbon expression section
			_logicRibbonExpressionContainer = new TableResizableContainer(DEFAULT_COLUMN_WIDTH,TableLayoutManager.TOP_MARGIN+ROW_HEIGHT*2);
			addTableSection(_logicRibbonExpressionContainer, TableLayoutManager.H_EXPRESSION_LEFT, TableLayoutManager.H_GUIDE_RULE_2_LEFT, TableLayoutManager.V_LOGIC_TOP);
			
			// Add right side port view container
			_truthPortContainer = new TableSection();
			addTableSection(_truthPortContainer, TableLayoutManager.H_PORT_LEFT, TableLayoutManager.H_TABLE_RIGHT, TableLayoutManager.V_LOGIC_TOP);
			
			// Logic Row Minus Button and Section
			if(this is AnswerTableView){     //  && currentRow > 0
				_logicRowMinusButton = TableButtons.minusButton;
				_logicRowMinusButton.scaleX = _logicRowMinusButton.scaleY = .75;
				_logicRowMinusButton.x = 23;
				_logicRowMinusButton.y = _logicRowMinusButton.height/2 + 3;
				_logicRowMinusButton.addEventListener(MouseEvent.CLICK, handleRemoveLogicRowClick);
				if(table.port.length <= 1){
					_logicRowMinusButton.visible = false;
				}
				_logicRowButtonContainer = new TableSection();
				_logicRowButtonContainer.addChild(_logicRowMinusButton);
				addTableSection(_logicRowButtonContainer, TableLayoutManager.H_TABLE_LEFT, TableLayoutManager.H_GRID_LEFT, TableLayoutManager.V_SCROLL_TOP);
			}
		}
		
		protected function createValueSection():void
		{
			if(table.currentStateValues.length < 1) {
				table.currentStateValues.addItem("");
			}
			// value bars
			_valueRibbonNameContainer = new TableResizableContainer(DEFAULT_COLUMN_WIDTH,TableLayoutManager.TOP_MARGIN+ROW_HEIGHT*2);
			_valueRibbonExpressionContainer = new TableResizableContainer(DEFAULT_COLUMN_WIDTH,TableLayoutManager.TOP_MARGIN+ROW_HEIGHT*2);
			
			_zeroColumnValueSection = ZeroColumnGridFactory(table, this, -1, table.currentStateValues.length, table.currentStateValues);
			
			// add value grid section
			_valueGrid = new StateTableGrid(table, this, table.numColumns,table.currentStateValues.length, table.allStateValues);
			_valueGridContainer = new HorizontalScrollContainer(_valueGrid, _scrollController, BLUE_LIGHT);
			_valueGridContainer.height = table.currentStateValues.length*ROW_HEIGHT;
			
			// Value Row Minus Button and Section
			_valueRowMinusButton = TableButtons.minusButton;
			_valueRowMinusButton.scaleX = _valueRowMinusButton.scaleY = .75;
			_valueRowMinusButton.x = 23;
			_valueRowMinusButton.y = _valueRowMinusButton.height/2 + 3;
			_valueRowMinusButton.addEventListener(MouseEvent.CLICK, handleRemoveValueRowClick);
			if(table.port.length <= 1){
				_valueRowMinusButton.visible = false;
			}
			_valueRowButtonContainer = new TableSection();
			_valueRowButtonContainer.addChild(_valueRowMinusButton);
			addTableSection(_valueRowButtonContainer, TableLayoutManager.H_TABLE_LEFT, TableLayoutManager.H_GRID_LEFT, (this is StateTableView)?TableLayoutManager.V_SCROLL_TOP:TableLayoutManager.V_TWEEN_TOP);
		}
		
		private function handleRemoveLogicRowClick(event:MouseEvent):void{
			table.fireBranches = false;
			if (table as AnswerTable && currentRow >= 0)
				(table as AnswerTable).removeLogicRow(currentRow);
			table.rearranging = false;
			setCurrentRow(-1, currentSection);
		}
		
		private function handleRemoveValueRowClick(event:MouseEvent):void{
			if (currentRow >= 0) {
				table.fireBranches = false;
				table.removeValueRow(currentRow);
				table.rearranging = false;
				setCurrentRow(-1, currentSection);
			}
		}
		
		protected function ZeroColumnGridFactory(table:AbstractTable, tableView:AbstractTableView, columns:int, rows:int, initialValues:IList):TableGrid
		{
			return new StateTableGrid(table, tableView, columns, rows, initialValues);
		}
		
		protected function createTweenSection():void
		{
			
		}
		
		private function getColumnLocation(index:int, grid:TableGrid):Point
		{
			return this.globalToLocal(grid.localToGlobal(new Point(COLUMN_WIDTH * index,0)));
		}
		
		public function getDragableLocation(index:int, section:String):Point
		{
			var location:Point;
			
			switch(section){
				case TableHeaderDragMediator.COLUMNS:
					var grid:TableGrid;
					if(table is StateTable){
						grid = _valueGrid
					}else{
						grid = _tableGrid;
					}
					location = new Point(getColumnLocation(index, grid).x, TableLayoutManager.TOP_MARGIN);
					break;
				case TableHeaderDragMediator.LOGIC_ROWS:
					location = new Point(0, _gridContainer.y + (index * ROW_HEIGHT));
					break;
				case TableHeaderDragMediator.VALUE_ROWS:
					location = new Point(0, _valueGridContainer.y + (index * ROW_HEIGHT));
					break;
			}
			
			return location;
		}
		
		public function makeDragSnapShot(index:int, section:String):Sprite{
			//Hide scrollbar if showing
			var oldScrollContainerVisible:Boolean = _scrollController.visible;
			if(_scrollController.visible){
				!_scrollController.visible;
			}
			
			//Take snapshot of row or column
			var snapShot:Sprite = new Sprite();
			var snapShotBitmap:Bitmap;
			var shotRectangle:Rectangle;
			var container:IBitmapDrawable;
			
			switch(section){
				case TableHeaderDragMediator.COLUMNS:
					var grid:TableGrid;
					if(table is StateTable){
						grid = _valueGrid
					}else{
						grid = _tableGrid;
					}
					shotRectangle = new Rectangle(getColumnLocation(index, grid).x, TableLayoutManager.TOP_MARGIN, COLUMN_WIDTH, _height - TableLayoutManager.TOP_MARGIN - 4);
					break;
				case TableHeaderDragMediator.LOGIC_ROWS:
					shotRectangle = new Rectangle(0, _gridContainer.y + (index * ROW_HEIGHT), _width, ROW_HEIGHT);
					break;
				case TableHeaderDragMediator.VALUE_ROWS:
					shotRectangle = new Rectangle(0, _valueGridContainer.y + (index * ROW_HEIGHT), _width, ROW_HEIGHT);
					break;
			}
			
			var sectionBmpData:BitmapData = new BitmapData(shotRectangle.width, shotRectangle.height);
			var translate:Matrix = new Matrix();
			translate.tx = -shotRectangle.x;
			translate.ty = -shotRectangle.y;
			sectionBmpData.draw(this, translate);
			snapShotBitmap = new Bitmap(sectionBmpData);
			snapShotBitmap.y = snapShotBitmap.y + ROW_HEIGHT;
			snapShot.addChild(snapShotBitmap);
			
			EditorUI.instance.rawChildren.addChild(snapShot);


			// Restore scroll bar to previous state.
			_scrollController.visible = oldScrollContainerVisible;
			return snapShot;
		}

        public function muteHeadingRollovers():void{
            var i:uint;
            for(i=0; i<_tableColumnHeading.numChildren; i++){
                if(_tableColumnHeading.getChildAt(i) != null && _tableColumnHeading.getChildAt(i) is TableHeadingCell){
                    (_tableColumnHeading.getChildAt(i) as TableHeadingCell).muteRollOver();
                }
            }
        }

        public function restoreHeadingRollovers():void{
            var i:uint;
            for(i=0; i<_tableColumnHeading.numChildren; i++){
                if(_tableColumnHeading.getChildAt(i) != null && _tableColumnHeading.getChildAt(i) is TableHeadingCell){
                    (_tableColumnHeading.getChildAt(i) as TableHeadingCell).updateEvents();
                }
            }
        }

		public function muteDragArea(index:int, section:String):void{
			_draggingAreaMuteSprite = new Sprite();
			_draggingAreaMuteSprite.graphics.clear();
			_draggingAreaMuteSprite.graphics.beginFill(0x000000, .5);
			
			switch(section){
				case TableHeaderDragMediator.COLUMNS:
					_logicColumnAreaMuteSprite = new Sprite();
					_logicColumnAreaMuteSprite.graphics.clear();
					_logicColumnAreaMuteSprite.graphics.beginFill(0x000000, .5);
					_logicColumnAreaMuteSprite.graphics.drawRect(0, 0, COLUMN_WIDTH, (model.value as AbstractTable).port.length * ROW_HEIGHT);
					_logicColumnAreaMuteSprite.y = 0;
					_logicColumnAreaMuteSprite.x = COLUMN_WIDTH * index;
					_logicColumnAreaMuteSprite.graphics.endFill();
					_tableGrid.addChild(_logicColumnAreaMuteSprite);
					
					_valueColumnAreaMuteSprite = new Sprite();
					_valueColumnAreaMuteSprite.graphics.clear();
					_valueColumnAreaMuteSprite.graphics.beginFill(0x000000, .5);
					_valueColumnAreaMuteSprite.graphics.drawRect(0, 0, COLUMN_WIDTH, _valueRows.length * ROW_HEIGHT);
					_valueColumnAreaMuteSprite.y = 0;
					_valueColumnAreaMuteSprite.x = COLUMN_WIDTH * index;
					_valueColumnAreaMuteSprite.graphics.endFill();
					_valueGrid.addChild(_valueColumnAreaMuteSprite);
					
					_draggingAreaMuteSprite.graphics.drawRect(0,0, COLUMN_WIDTH, ROW_HEIGHT);
					_draggingAreaMuteSprite.y = 0;
					_draggingAreaMuteSprite.x = COLUMN_WIDTH * index;
					_draggingAreaMuteSprite.graphics.endFill();
					_tableColumnHeading.addChild(_draggingAreaMuteSprite);
					
					break;
				case TableHeaderDragMediator.LOGIC_ROWS:
					_draggingAreaMuteSprite.graphics.drawRect(0,0, this.width, ROW_HEIGHT);
					_draggingAreaMuteSprite.y = _gridContainer.y + (index * ROW_HEIGHT);
					_draggingAreaMuteSprite.graphics.endFill();
					addChild(_draggingAreaMuteSprite);
					break;
				case TableHeaderDragMediator.VALUE_ROWS:
					_draggingAreaMuteSprite.graphics.drawRect(0,0, this.width, ROW_HEIGHT);
					_draggingAreaMuteSprite.y = _valueGridContainer.y + (index * ROW_HEIGHT);
					_draggingAreaMuteSprite.graphics.endFill();
					addChild(_draggingAreaMuteSprite);
					break;
			}
		}
		
		public function unmuteDragArea():void{
			if(_draggingAreaMuteSprite != null){
				if(_draggingAreaMuteSprite.parent){
					_draggingAreaMuteSprite.parent.removeChild(_draggingAreaMuteSprite);
				}
			}
			if(_logicColumnAreaMuteSprite != null){
				if(_tableGrid.contains(_logicColumnAreaMuteSprite)){
					_tableGrid.removeChild(_logicColumnAreaMuteSprite);
				}
			}
			if(_valueColumnAreaMuteSprite != null){
				if(_valueGrid.contains(_valueColumnAreaMuteSprite)){
					_valueGrid.removeChild(_valueColumnAreaMuteSprite);
				}
			}
		}
			
		protected function createOutletRow():void
		{
			// add outlets
			_outletRow = new TableOutletRow(model, context, tableGrid.columns, _scrollController);
			
			// add wire bays
			var left:Sprite = wireBay;
			left.x = -(TableOutletRow.WIRE_BAY_WIDTH + 4);
			left.y = (1/4*_outletRow.height)/2;
			_outletRow.addChild(left);
			
			rightWireBaySection = new TableSection();
			var right:Sprite = wireBay;
			right.y = (1/4*_outletRow.height)/2;
			rightWireBaySection.addChild(right);
		}
		
		protected function addInlets():void
		{
			_inletContainer = new TableSection();
			addTableSection(_inletContainer, TableLayoutManager.H_TABLE_LEFT, TableLayoutManager.H_GUIDE_RULE_3_LEFT, TableLayoutManager.V_OUTLET_TOP);
			
			var inletPortX:Number = INITIAL_INLET_X;
			
			var ports:Array = (table is AnswerTable) ? ["judgeNow", "reset", "resume"] : ((table is StateTable) ? ["first", "previous", "start", "resume", "stop", "pause", "togglePause", "next", "last"] : []);
			for each (var str:String in ports) {
				createInlet(str, inletPortX);
				inletPortX += 20;
			}
			
			_layout.pack();
			_layout.calculateVerticalLayout();
		}

		protected function createInlet(property:String, inletPortX:Number, unselectedColor:int=-1):void
		{
			var portView:PortView = new PortView(table.anchors[property], context as WorldContainer, true, unselectedColor);
			portView.addEventListener(PortView.WIRE_CHANGED, handleWireChanged);
			portView.x = inletPortX;
			portView.y = 3;
			portView.angle = 90;
			_inletContainer.addChild(portView);
		}
		
		private function handleWireChanged(event:Event):void
		{
			if((event.currentTarget as PortView).anchor.modifierDescription.key == "judgeNow"){
				table.continuallyJudging = !(event.currentTarget as PortView).anchor.wired;
			}
		}
		
		public function getCellUnderMouse():TableCell
		{			
			var valRow:int = Math.floor((_valueGrid.mouseY)/ROW_HEIGHT);
			if (valRow >= 0 && valRow < _valueGrid.rows) {
				var col:int = Math.floor((_valueGrid.mouseX)/COLUMN_WIDTH);
				if (col >= 0 || col < table.numColumns) {
					return _valueGrid.getCell(valRow, col);
				}
			}
			return null;
		}
		
		public function getGridMousePosition(section:String):int
		{
			switch(section) {
				case TableHeaderDragMediator.COLUMNS:
					var grid:Sprite = (_tableGrid==null||_tableGrid.parent==null?_valueGrid:tableGrid);
					return Math.floor((grid.mouseX + COLUMN_WIDTH/2)/COLUMN_WIDTH);
				case TableHeaderDragMediator.LOGIC_ROWS:
					return Math.floor((tableGrid.mouseY + ROW_HEIGHT/2)/ROW_HEIGHT);
				case TableHeaderDragMediator.VALUE_ROWS:
					return Math.floor((_valueGrid.mouseY + ROW_HEIGHT/2)/ROW_HEIGHT);
			}
			return -1;
		}
		
		public function get nowColumnRightEdge():Number
		{
			return _zeroColumnHeading.x + _zeroColumnHeading.width;
		}
		
		public function setTitle(title:String=null):void
		{
			if(title == null) {
				_title.text = table.title;
			} else {
				_title.text = title;
			}
			_title.setCompositionSize(NaN, NaN);
		}

		private var _insertHighlight:Sprite;

		public function drawInsertHighlight(section:String, index:int):void
		{
			if(deleting) {
				deleting = false;
				render();
			}
			if(_insertHighlight == null) {
				_insertHighlight = new Sprite();
			}
			if(_insertHighlight.parent == null) {
				this.addChild(_insertHighlight);
			}
			_insertHighlight.graphics.clear();
			_insertHighlight.graphics.lineStyle(3, 0x0000FF);
			var p1:Point;
			switch(section) {
				case TableHeaderDragMediator.COLUMNS:
					var grid:Sprite = (_tableGrid==null||_tableGrid.parent==null?_valueGrid:tableGrid);
					p1 = globalToLocal(grid.localToGlobal(new Point(index*COLUMN_WIDTH, 0)));
					if(p1.x > _zeroColumnHeading.x + COLUMN_WIDTH  && p1.x < grid.parent.x + grid.parent.width){
						_insertHighlight.graphics.moveTo(p1.x, 0);
						_insertHighlight.graphics.lineTo(p1.x, _height);
					}
					break;
				case TableHeaderDragMediator.LOGIC_ROWS:
					p1 = globalToLocal(tableGrid.localToGlobal(new Point(0, index*ROW_HEIGHT)));
					_insertHighlight.graphics.moveTo(0, p1.y);
					_insertHighlight.graphics.lineTo(_width, p1.y);
					break;
				case TableHeaderDragMediator.VALUE_ROWS:
					p1 = globalToLocal(_valueGrid.localToGlobal(new Point(0, index*ROW_HEIGHT)));
					_insertHighlight.graphics.moveTo(0, p1.y);
					_insertHighlight.graphics.lineTo(_width, p1.y);
					break;
			}
		}

		public function drawDeleteHighlight():void
		{
			if(_insertHighlight != null) {
				if(_insertHighlight.parent != null) {
					_insertHighlight.parent.removeChild(_insertHighlight);
				}
				_insertHighlight = null;
			}
			deleting = true;
			render();
		}

		public function hideInsertHighlight():void
		{
			if(deleting) {
				deleting = false;
				render();
			}
			if(_insertHighlight != null) {
				if(_insertHighlight.parent != null) {
					_insertHighlight.parent.removeChild(_insertHighlight);
				}
				_insertHighlight = null;
			}
		}
		
		public function get statusLabel():LightLabel{
			return _statusLabel;
		}
		public function set statusLabel(value:LightLabel):void{
			_statusLabel = value;
		}
		
		override public function getBounds(targetCoordinateSpace:DisplayObject):Rectangle
		{
			return rectToSpace(new Rectangle(0,0,_width,_height), targetCoordinateSpace);
		}
		
		override public function get height():Number{
			return _layout.height;
		}

        public function get branchingRow():TableBranchingRow{
            return _branchingRow;
        }

        public function assetReplaceAll(oldAsset:IReplaceable, newAsset:IReplaceable):void
        {
            for (var row:int = 0; row < _valueGrid.rows; row++) {
                for (var col:int = 0; col < _valueGrid.columns; col++) {
                    var cell:StateTableCell = _valueGrid.getCell(row, col) as StateTableCell;
                    if (cell) {
                        cell.assetReplaceAll(oldAsset, newAsset);
                    }
                }
            }
        }


	}
}
