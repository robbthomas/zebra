package com.alleni.author.view.objects
{
	import com.alleni.author.controller.objects.media.MediaViewControlsMediator;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	public class AudioView extends SimpleIconedView
	{
		private var _controlsMediator:MediaViewControlsMediator;
		
		function AudioView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{	
			var handledModelUpdate:Boolean = super.updateModelProperty(property,oldValue,newValue);
			
			switch (property) {
				case "buffering":
					// show some buffering feedback if it suits you
					return true;
					break;
			}//switch
			
			return handledModelUpdate;
		}
		
		override protected function addIcon():void
		{
			super.addIcon();
			_controlsMediator = new MediaViewControlsMediator().handleViewControls(this);
		}
		
		override public function updateViewPosition():void
		{
			super.updateViewPosition();
			if (_controlsMediator)
				_controlsMediator.updatePosition();
		}
		
		override public function warnViewToDelete():void
		{
			super.warnViewToDelete();
			_controlsMediator.warnBeforeDelete();
		}
	}
}