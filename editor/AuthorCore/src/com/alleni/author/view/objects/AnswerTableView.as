package com.alleni.author.view.objects
{
	import com.alleni.author.definition.TableStatusMessages;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.objects.tables.AbstractTableView;
	import com.alleni.author.view.objects.tables.AnswerTableGrid;
	import com.alleni.author.view.objects.tables.TableBranchingRow;
	import com.alleni.author.view.objects.tables.TableButtons;
	import com.alleni.author.view.objects.tables.TableLayoutManager;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.IconButton;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.taconite.definition.HandleRoles;
	import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.events.MouseEvent;
	
	import mx.collections.IList;

	public class AnswerTableView extends AbstractTableView
	{
		public function AnswerTableView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			_statusLabel = new LightLabel();
			super(context, model, role);
		}
		
		override protected function createGrid():void
		{
			_tableGrid = new AnswerTableGrid(table,this,table.numColumns,table.port.length, AnswerTable(table).allLogicValues);
            for(var row:uint=0; row < _tableGrid.rows; row++) {
                for(var column:uint=0; column<_tableGrid.columns; column++) {
                    AnswerTableGrid(_tableGrid).satisfyCell(AnswerTable(table).getLogicFulfilled(row, column), row, column);
                }
            }
		}

		override protected function rearrangeTableGrid():void {
			if(_tableGrid) {
				_tableGrid.handleRearranged(table.numColumns, table.port.length, AnswerTable(table).allLogicValues);
                var row:int;
                for(row=0;row<_tableGrid.rows;row++){
                    var column:int;
                    for(column = 0;column<_tableGrid.columns;column++){
                        AnswerTableGrid(_tableGrid).satisfyCell(AnswerTable(table).getLogicFulfilled(row, column), row, column);
                    }
                }
			}
		}

		override protected function createInitialSections():void
		{
			createLogicSection();
			createValueSection();
		}
		
		override protected function get columnHeadingIcons():Array
		{
			var plusButton:IconButton = TableButtons.plusButton;
			plusButton.addEventListener(MouseEvent.CLICK, handlePlusButtonClick);
			
			var minusButton:IconButton = TableButtons.minusButton;
			minusButton.addEventListener(MouseEvent.CLICK, handleMinusButtonClick);
			
			var locateButton:IconButton = TableButtons.locateButton;
			locateButton.addEventListener(MouseEvent.CLICK, handleLocateClick);
			
			return [plusButton, minusButton, locateButton];
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean 
		{
			var table:AnswerTable = model.value as AnswerTable;
			var handled:Boolean = super.updateModelProperty(property, oldValue, newValue);
            if(shouldIgnoreChanges(property.toString())) {
                return handled;
            }
			switch (property) {
				case "statusMessage":
					_statusLabel.text = TableStatusMessages.MESSAGES[(table as AnswerTable).statusMessage];
					break;
				case "poweredOnRun":
					table.statusMessage = newValue?(table.continuallyJudging?1:2):0;
					break;
				case "continuallyJudging":
					table.statusMessage = newValue?1:2;
					break;
			}
			return handled;
		}

		override protected function updateArrayProperty(property:String, modelEventKind:String, array:IList, index:int):Boolean
		{
			var handled:Boolean = super.updateArrayProperty(property, modelEventKind, array, index);
            if(shouldIgnoreChanges(property)) {
				return handled;
			}
			switch(modelEventKind) {
				case ModelEvent.CHANGE_ARRAY_ITEM:
					var row:int = index / (table.numColumns);
					var column:int = index % (table.numColumns);
					if(property == "allLogicFulfilled") {
						AnswerTableGrid(_tableGrid).satisfyCell(AnswerTable(table).getLogicFulfilled(row, column), row, column);
					}
			}

			return handled;
		}
	}
}