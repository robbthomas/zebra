package com.alleni.author.view.objects.tables
{
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.view.text.RichLabel;

	public class TruthTableCell extends TableCell
	{
		private var _label:RichLabel;
		
		public function TruthTableCell(table:AbstractTable, grid:TableGrid, row:int, column:int, highlighted:Boolean)
		{			
			super(table, grid, row, column, highlighted);
			_defaultColor = AbstractTableView.GRAY_LIGHT;
			_highlightColor = 0xFFFFFF;
			draw();
		}
		
		override protected function addLabel():void
		{
			_label = new RichLabel(AbstractTableView.COLUMN_WIDTH);
			_label.height = AbstractTableView.ROW_HEIGHT;
			_label.y = -2;
			addChild(_label);

			if(_column >= 0) {
				var x:int = 1 << _row; // = 2^row
				var t:Boolean = _column % (x*2) >= x;

				_label.text = t?"T":"F";
			} else {
				_label.text = "T";
			}
			
			_label.center = true;
			_label.color = 0x000000;
			
			_label.bold = t;
		}
		
		override public function updateValue(value:Object):void
		{
			var t:Boolean = (value == true || value == "T"|| (value != null && value != false && value != "F"));
			_label.text = t?"T":"F";
		}
	}
}