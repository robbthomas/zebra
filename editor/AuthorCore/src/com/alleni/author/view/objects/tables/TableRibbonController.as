package com.alleni.author.view.objects.tables
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.PortView;
	
	import flash.display.Sprite;

	public class TableRibbonController
	{
		
		protected var _rightPortView:PortView;
		protected var _context:WorldContainer;
		protected var _table:AbstractTable;
		protected var _index:int;
		
		protected var _ribbonName:TableRibbonName;
		protected var _ribbonExpression:TableRibbonExpression;
		protected var _truthPortContainer:Sprite;
		protected var _portView:PortView;

		public function TableRibbonController(nameColumn:TableResizableContainer, expressionColumn:TableResizableContainer, truthPortContainer:Sprite, table:AbstractTable, index:int,
												anchor:WireAnchor, context:WorldContainer, role:int)
		{
			super();
			
			_context = context;
			_table = table;
			_index = index;
			_truthPortContainer = truthPortContainer;
			
			_portView = PortViewFactory(anchor);
			_portView.angle = 180;
			
			initialize(nameColumn, expressionColumn);
		}
		
		protected function PortViewFactory(anchor:WireAnchor):PortView
		{
			return new PortView(anchor, _context, true);
		}
		
		protected function initialize(nameColumn:TableResizableContainer, expressionColumn:TableResizableContainer):void
		{
			var y:Number = _index * AbstractTableView.ROW_HEIGHT;
			
			createNameAndExpression();
			
			_ribbonName.addChild(_portView);
			_ribbonName.y = y;
			nameColumn.addChild(_ribbonName);
			_ribbonName.width = nameColumn.width;
			
			_ribbonExpression.y = y;
			expressionColumn.addChild(_ribbonExpression);
			_ribbonExpression.width = expressionColumn.width;
		}
		
		protected function createNameAndExpression():void
		{
			// overridden by subclasses
		}
		
		public function handleWireAdded(title:String, prop:PropertyDescription, existing:Boolean):void
		{
			_portView.draw();
			if(_rightPortView && !_truthPortContainer.contains(_rightPortView)) {
				_truthPortContainer.addChild(_rightPortView);
			}
			_ribbonExpression.handleWireAdded(title, prop, existing);
			_ribbonName.handleWireAdded(title, prop, existing);
		}
		
		public function handleWireRemoved(existing:Boolean):void
		{
			_portView.draw();
			if (_portView.anchor.isAdditionAnchor)
				return;
			if(_rightPortView) {
				if(!_truthPortContainer.contains(_rightPortView)) {
					_truthPortContainer.addChild(_rightPortView);
				}
				_rightPortView.draw();
			}
			_ribbonExpression.handleWireRemoved(existing);
			_ribbonName.handleWireRemoved(existing);
		}

		public function handleBecameAddition():void
		{
			_portView.draw();
			if(_rightPortView != null && _rightPortView.parent != null) {
				_rightPortView.parent.removeChild(_rightPortView);
			}
			_ribbonExpression.handleBecameAddition();
			_ribbonName.handleBecameAddition();
		}

		public function get wired():Boolean
		{
			return _portView.anchor.wired;
		}

		public function handleRearranged(index:int, anchor:WireAnchor, context:WorldContainer):void
		{
			_ribbonName.removeChild(_portView);
			_portView = new PortView(anchor, context, true);
			_portView.angle = 180;
			_ribbonName.addChild(_portView);
			if(anchor.isAdditionAnchor) {
				handleBecameAddition();
			} else if(anchor.wired) {
				var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
				var other:WireAnchor;
				if(wires.length > 0) {
					if(Wire(wires[0]).masterAnchor == anchor) {
						other = Wire(wires[0]).slaveAnchor;
					} else {
						other = Wire(wires[0]).masterAnchor;
					}
				}
				if(other) {
					handleWireAdded(other.hostObject.title, other.modifierDescription as PropertyDescription, true);
				} else {
					handleWireRemoved(true);
				}
			} else {
				handleWireRemoved(true);
			}
		}

        public function updateName():void {
            _ribbonName.updateName();
        }

        public function updateExpression():void{
            _ribbonExpression.updateExpression();
        }

		public function remove():void
		{
			if(_ribbonName != null && _ribbonName.parent != null) {
				_ribbonName.parent.removeChild(_ribbonName);
			}
			if(_ribbonExpression != null && _ribbonExpression.parent != null) {
				_ribbonExpression.parent.removeChild(_ribbonExpression);
			}
			if(_portView != null && _portView.parent != null) {
				_portView.parent.removeChild(_portView);
			}
		}
	}
}