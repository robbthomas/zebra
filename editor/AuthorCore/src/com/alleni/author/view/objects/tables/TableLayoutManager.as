package com.alleni.author.view.objects.tables
{
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.model.objects.Calculator;
	import com.alleni.author.model.objects.StateTable;
	import com.alleni.author.model.objects.TruthTable;

	public class TableLayoutManager
	{
		// Section constants
		// vertical
		public static const V_TABLE_TOP:int			= 0;
		public static const V_TIMELINE_TOP:int 		= 1;
		public static const V_COL_HEADING_TOP:int	= 2;
		public static const V_BRANCHING_TOP:int		= 3;
		public static const V_LOGIC_TOP:int			= 4;
		public static const V_SCROLL_TOP:int		= 5;
		public static const V_VALUE_TOP:int			= 6;
		public static const V_EXPRESSIONS_TOP:int	= 7;
		public static const V_TWEEN_TOP:int			= 8;
		public static const V_OUTLET_TOP:int		= 9;
		public static const V_TABLE_BOTTOM:int		= 10;
		
		// horizontal
		public static const H_TABLE_LEFT:int		= 0;
		public static const H_RIBBON_LEFT:int 		= 1;
		public static const H_VAR_LEFT:int 			= 2;
		public static const H_GUIDE_RULE_1_LEFT:int	= 3;
		public static const H_EXPRESSION_LEFT:int	= 4;
		public static const H_GUIDE_RULE_2_LEFT:int	= 5;
		public static const H_COL_0_LEFT:int		= 6;
		public static const H_GRID_LEFT:int			= 7;
		public static const H_GUIDE_RULE_3_LEFT:int	= 8;
		public static const H_PORT_LEFT:int			= 9;
		public static const H_TABLE_RIGHT:int		= 10;		
		
		public static const BRANCHING_SECTION_HEIGHT:Number = 18;
		
		private static const ROW_HEIGHT:Number = 15;
		private static const OUTLET_ROW_HEIGHT:Number = 20;
		private static const COLUMN_HEADING_HEIGHT:Number = ROW_HEIGHT;
		private static const COLUMN_WIDTH:Number = 35;
		private static const TWEEN_SECTION_HEIGHT:Number = ROW_HEIGHT*3+2;
		public static const TIMELINE_HEIGHT:Number = ROW_HEIGHT*2;
		public static const TOP_MARGIN:Number = 20;
		private static const SCROLL_BAR_HEIGHT:Number = 20;
		private static const GUIDE_RULE_THICKNESS:Number = 4;
		private var _verticalGuidePositions:Vector.<Number> = new Vector.<Number>(11);
		private var _horizontalGuidePositions:Vector.<Number> = new Vector.<Number>(11);

		public var sections:Vector.<TableSection> = new Vector.<TableSection>();

		private var _view:AbstractTableView;
		
		
		public function TableLayoutManager(view:AbstractTableView)
		{
			_view = view;
			calculateVerticalLayout();
			setupHorizontalLayout();
		}

		public function pack():void
		{
			var i:int;
			var length:int = sections.length;
			for(i = 0 ; i < length; i++) {
				if(sections[i].topGuide > 0) sections[i].y = _verticalGuidePositions[sections[i].topGuide];
				if(sections[i].bottomGuide > 0) sections[i].height = _verticalGuidePositions[sections[i].bottomGuide] - sections[i].y;
				if(sections[i].leftGuide > 0) sections[i].x = _horizontalGuidePositions[sections[i].leftGuide];
				if(sections[i].rightGuide > 0) sections[i].width = _horizontalGuidePositions[sections[i].rightGuide] - sections[i].x;
			}
			_view.height = _verticalGuidePositions[V_TABLE_BOTTOM];
			_view.width = _horizontalGuidePositions[H_TABLE_RIGHT];
		}
		
		public function calculateVerticalLayout():void
		{
			_verticalGuidePositions[V_TABLE_TOP] = 0; // top of table
			_verticalGuidePositions[V_TIMELINE_TOP] = TOP_MARGIN; // top of timeline
			_verticalGuidePositions[V_COL_HEADING_TOP] = _verticalGuidePositions[V_TIMELINE_TOP] + (false?TIMELINE_HEIGHT:0); // top of column heading
			_verticalGuidePositions[V_BRANCHING_TOP] = (_verticalGuidePositions[V_COL_HEADING_TOP] + (_view.table.showHeading?COLUMN_HEADING_HEIGHT:0)) + 2; // top of branching
			_verticalGuidePositions[V_LOGIC_TOP] = _verticalGuidePositions[V_BRANCHING_TOP]
				+ ((_view.table is AnswerTable || _view.table is StateTable) && _view.table.showBranchingRow?BRANCHING_SECTION_HEIGHT:0); // top of logic section
			_verticalGuidePositions[V_SCROLL_TOP] = _verticalGuidePositions[V_LOGIC_TOP]
				+ (_view.table is StateTable?_view.table.currentStateValues.length*ROW_HEIGHT:_view.table.port.length*ROW_HEIGHT); // top of scroll bar
			_verticalGuidePositions[V_VALUE_TOP] = _verticalGuidePositions[V_SCROLL_TOP] + (_view.table.scrollGrid?SCROLL_BAR_HEIGHT:0); // top of value section
			_verticalGuidePositions[V_EXPRESSIONS_TOP] = _verticalGuidePositions[V_VALUE_TOP]
				+ ("expressionValues" in _view.table?ROW_HEIGHT+2:0)
				+ (_view.table is StateTable?3:(_view.table.currentStateValues.length-(_view.table.showValueRows ? 0 : 1))*ROW_HEIGHT);
			_verticalGuidePositions[V_TWEEN_TOP] = _verticalGuidePositions[V_EXPRESSIONS_TOP]
				+ ("expressionValues" in _view.table?((_view.table["expressionValues"].length) * ROW_HEIGHT):0); // top of tween section, allowing for calculator expressions
			_verticalGuidePositions[V_OUTLET_TOP] = _verticalGuidePositions[V_TWEEN_TOP] + (_view.table.showTweens?TWEEN_SECTION_HEIGHT:0); // top of outlet row
			var TTneedsSpace:Boolean = _view.table.showOuts || (_view.table.showValueRows && !_view.table.showTweens);
			// TT has no inlets so sometimes needs this next region to be height zero to prevent a row that would be empty with no inlets
			_verticalGuidePositions[V_TABLE_BOTTOM] = _verticalGuidePositions[V_OUTLET_TOP] + ((!(_view.table as TruthTable) || TTneedsSpace) && OUTLET_ROW_HEIGHT); // bottom of table
			
			var i:int;
			var length:int = sections.length;
			for(i = 0; i < length; i++) {
				if(sections[i].topGuide > 0) sections[i].y = _verticalGuidePositions[sections[i].topGuide];
				if(sections[i].bottomGuide > 0) sections[i].height = _verticalGuidePositions[sections[i].bottomGuide] - sections[i].y;
			}
			_view.height = _verticalGuidePositions[V_TABLE_BOTTOM];
		}
		
		private function setupHorizontalLayout():void
		{	
			_horizontalGuidePositions[H_TABLE_LEFT] = 0; // Left edge of table
			_horizontalGuidePositions[H_RIBBON_LEFT] = 20; // Left edge of Ribbon Text
			_horizontalGuidePositions[H_VAR_LEFT] = _horizontalGuidePositions[H_RIBBON_LEFT] + 25;
			_horizontalGuidePositions[H_GUIDE_RULE_1_LEFT] = _horizontalGuidePositions[H_RIBBON_LEFT] + 50 + 80; // left edge of first guide rule
			_horizontalGuidePositions[H_EXPRESSION_LEFT] = _horizontalGuidePositions[H_GUIDE_RULE_1_LEFT] + GUIDE_RULE_THICKNESS; // left edge of expression section
			_horizontalGuidePositions[H_GUIDE_RULE_2_LEFT] = _horizontalGuidePositions[H_EXPRESSION_LEFT] + ((_view.table as Calculator)?20:80); // left edge second guide rule
			_horizontalGuidePositions[H_COL_0_LEFT] = _horizontalGuidePositions[H_GUIDE_RULE_2_LEFT] + GUIDE_RULE_THICKNESS; // left edge of column zero
			_horizontalGuidePositions[H_GRID_LEFT] = _horizontalGuidePositions[H_COL_0_LEFT] + AbstractTableView.COLUMN_WIDTH; // left edge of Grid section
			_horizontalGuidePositions[H_GUIDE_RULE_3_LEFT] = _horizontalGuidePositions[H_GRID_LEFT] + _view.table.numColumns * COLUMN_WIDTH; // left edge third guide rule
			_horizontalGuidePositions[H_PORT_LEFT] = _horizontalGuidePositions[H_GUIDE_RULE_3_LEFT] + GUIDE_RULE_THICKNESS; // left edge of right ports
			_horizontalGuidePositions[H_TABLE_RIGHT] = _horizontalGuidePositions[H_PORT_LEFT] + 20; // right edge of table
		}

		public function expandGrid(maxColumns:int):Boolean
		{
			var desiredWidth:Number = Math.min(_view.table.numColumns, maxColumns) * COLUMN_WIDTH;
			var actualWidth:Number = _horizontalGuidePositions[H_GUIDE_RULE_3_LEFT] - _horizontalGuidePositions[H_GRID_LEFT];
			if(desiredWidth > actualWidth) {
				setGuidePosition(2, _horizontalGuidePositions[H_GRID_LEFT] + desiredWidth);
				return true;
			}
			return false;
		}
		
		public function contractGrid(maxColumns:int):Boolean
		{
			var desiredWidth:Number = Math.min(_view.table.numColumns, maxColumns) * COLUMN_WIDTH;
			var actualWidth:Number = _horizontalGuidePositions[H_GUIDE_RULE_3_LEFT] - _horizontalGuidePositions[H_GRID_LEFT];
			if(desiredWidth < actualWidth) {
				setGuidePosition(2, _horizontalGuidePositions[H_GRID_LEFT] + desiredWidth);
				return true;
			}
			return false;
		}

		public function setGuidePosition(guideIndex:int, xPos:Number):void
		{
            _view.table.guidePositions[guideIndex] = xPos;
			switch(guideIndex) {
				case 0:
					xPos = Math.max(xPos, _horizontalGuidePositions[H_RIBBON_LEFT] + 40);
					xPos = Math.min(xPos, _horizontalGuidePositions[H_GUIDE_RULE_2_LEFT] - 42);
					_horizontalGuidePositions[H_GUIDE_RULE_1_LEFT] = xPos; // left edge of first guide rule
					_horizontalGuidePositions[H_EXPRESSION_LEFT] = _horizontalGuidePositions[H_GUIDE_RULE_1_LEFT] + GUIDE_RULE_THICKNESS; // left edge of expression section
					if(_view is StateTableView){
                        _view.updateControlsPosition(xPos);
                    }
                    break;
				case 1:
					xPos = Math.max(xPos, _horizontalGuidePositions[H_EXPRESSION_LEFT] + 42);
					if (_view.table as StateTable) {
						xPos = Math.max(xPos, 210); // make room for inlets
					}
					xPos = Math.min(xPos, _horizontalGuidePositions[H_GUIDE_RULE_3_LEFT] - COLUMN_WIDTH*2);
					_horizontalGuidePositions[H_GUIDE_RULE_2_LEFT] = xPos; // left edge second guide rule
					_horizontalGuidePositions[H_COL_0_LEFT] = _horizontalGuidePositions[H_GUIDE_RULE_2_LEFT] + GUIDE_RULE_THICKNESS// left edge of column zero
					_horizontalGuidePositions[H_GRID_LEFT] = _horizontalGuidePositions[H_COL_0_LEFT] + AbstractTableView.COLUMN_WIDTH; ; // left edge of Grid section
					break;
				case 2:
					xPos = Math.max(xPos, _horizontalGuidePositions[H_COL_0_LEFT] + COLUMN_WIDTH*2);
					_horizontalGuidePositions[H_GUIDE_RULE_3_LEFT] = xPos; // left edge third guide rule
					_horizontalGuidePositions[H_PORT_LEFT] = _horizontalGuidePositions[H_GUIDE_RULE_3_LEFT] + GUIDE_RULE_THICKNESS; // left edge of right ports
					_horizontalGuidePositions[H_TABLE_RIGHT] = _horizontalGuidePositions[H_PORT_LEFT] + 20; // right edge of table
					break;
				default:
					throw new Error("Invalid guide index");
			}
			
			var i:int;
			var length:int = sections.length;
			for(i = 0; i < length; i++) {
				if(sections[i].leftGuide > 0) sections[i].x = _horizontalGuidePositions[sections[i].leftGuide];
				if(sections[i].rightGuide > 0) sections[i].width = _horizontalGuidePositions[sections[i].rightGuide] - sections[i].x;
			}
			_view.width = _horizontalGuidePositions[H_TABLE_RIGHT];
		}

		public function get viewState():int 
		{
			return _view.table.viewState;
		}

		public function set viewState(value:int):void 
		{
			_view.table.viewState = value;
		}

		public function get height():int 
		{
			return _verticalGuidePositions[V_TABLE_BOTTOM];
		}

		public function get width():int 
		{
			return _horizontalGuidePositions[H_TABLE_RIGHT];
		}
		
		public function get horizontalGuidePosition():Vector.<Number>{
			return _horizontalGuidePositions;
		}
	}
}
