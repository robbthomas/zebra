package com.alleni.author.view.objects
{
import com.alleni.author.Navigation.EmptyPageView;
import com.alleni.author.Navigation.IPage;
import com.alleni.author.Navigation.IPageView;
import com.alleni.author.controller.objects.ArenaPageController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Objects;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ITaconiteView;
import com.alleni.taconite.view.TaconiteViewFactory;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;

public class ArenaPageView extends ObjectView implements IPageView
	{		
		private var _arenaView:ArenaView;
        private var _blankPage:EmptyPageView;
        private var _childContainer:Sprite;


		public function ArenaPageView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);

            _childContainer = new Sprite();
            addChild(_childContainer);
            preventMessageCenter = true;
		}

        override public function get childViewContainer():DisplayObjectContainer
        {
            return _childContainer;
        }

        public function get arenaView():ArenaView
        {
            if (_arenaView == null) {
                _arenaView = arena.getView(context) as ArenaView;
            }
            return _arenaView;
        }
		
		public function get arena():Arena
		{
			return model.parent.value as Arena;
		}
		
		private function get arenaPage():ArenaPage
		{
			return model.value as ArenaPage;
		}

        private function get isMasterPage():Boolean
        {
            return model.parent && (arena.masterPages.getItemIndex(arenaPage) >= 0);
        }

        public function get objView():ObjectView
        {
            return this;
        }

        public function get page():IPage
        {
            return arenaPage;
        }

		override protected function setDragMediator():void
		{
			// prevent dragging the page itself, by not having a drag mediator
		}

		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
		{
            var page:ArenaPage = arenaPage;
			var result:Boolean = super.updateModelProperty(property,oldValue,newValue);
            switch (property) {
                case "active":
                    if (page.hadChildren==false)
                        return false;  // update the "Page x" label on blank page
                    break;
            }
			return result;
		}

		override protected function draw():void
		{
			super.draw();
		}
		
        override protected function updateEnabledView():void
        {
            super.updateEnabledView();
            updateGrayPage();
		}

        override public function interceptMouseDown(e:MouseEvent):void
        {
            // when click thru transparent object into empty arena, ensure the click goes to the arena
			if (_blankPage && arenaView) {
                arenaView.interceptMouseDown(e);
            }
        }

		

		override protected function updateThumbnail():void
		{
			// Arena pages have no MC or thumbnail
		}

		
		override protected function get accepting():Boolean
		{
			if (!arena.accepting) {
				return false;
            } else {
				return arena.visible || Application.instance.showAll;
            }
		}
		
		override public function get releasing():Boolean
		{
			return arena.releasing;
		}

        override public function hitTestPoint(x:Number, y:Number, shapeFlag:Boolean = false):Boolean {
            var p:ObjectView = parentObjectView;
            if(p) {
                return p.hitTestPoint(x,  y, shapeFlag); // hit test against the arena if we have one
			} else {
                return true; // we have no parent, so assume hit if we are even bothering asking
			}
        }

        override public function exactHitTest(stagePoint:Point):Boolean {
            var p:ObjectView = parentObjectView;
            if(p) {
                return p.exactHitTest(stagePoint); // hit test against the arena if we have one
			} else {
                return true; // we have no parent, so assume hit if we are even bothering asking
			}
        }

        override public function handleRunningChanged():void {
            super.handleRunningChanged();
            if (arenaPage.hadChildren==false) {
                updateView();
            }
            for(var i:int = 0; i < _childContainer.numChildren; i++) {
                var child:ObjectView = _childContainer.getChildAt(i) as ObjectView;
                if(child) {
                    child.handleRunningChanged();
                }
            }
        }

        private function get needGrayPage():Boolean
        {
            var page:ArenaPage = arenaPage;
            return page.objects.length == 0 && !page.hadChildren && !Application.running && !isMasterPage;
        }

        private function updateGrayPage():void
        {
            if (needGrayPage) {
                if (!_blankPage) {
                    _blankPage = new EmptyPageView();
                    addChildAt(_blankPage, 0);
                }
                _blankPage.x = 0;
                _blankPage.y = 0;
                _blankPage.width = arena.width;
                _blankPage.height = arena.height;
                _blankPage.labelText = "Page " + arenaPage.pageNumber.toString();
                _blankPage.doubleClickEnabled = true;
                _blankPage.addEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
            } else if (_blankPage) {
                _blankPage.removeEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
                removeChild(_blankPage);
                _blankPage = null;
            }
        }

        private function handleDoubleClick(e:MouseEvent):void
        {
            // enable selecting a locked, empty arena by double-clicking it
            context.controller.selectSingleModel(model);
        }

        override public function getBounds(space:DisplayObject):Rectangle
        {
            // children bounds may extend beyond arena bounds, but are clipped so we ignore them
            return rectToSpace(object.localBounds, space);
        }

        // disable features of ObjectView
        override protected function createFeedbackView():ITaconiteView { return null}; // no selection-handles for pages


    }
}
