package com.alleni.author.view.objects.tables
{
	import com.alleni.author.view.text.LightLabel;
	
	public class TableColumnHeading extends TableSection
	{
		
		private var _width:Number;
		private var _labelField:LightLabel;
		
		public function TableColumnHeading(label:String, indent:Number=0)
		{
			super();
			
			_width = AbstractTableView.DEFAULT_COLUMN_WIDTH*2;
			
			_labelField = new LightLabel(_width, 0xFFFFFF);
			_labelField.text = label;
			_labelField.italic = true
			_labelField.x = indent;
			
			addChild(_labelField);
			
			draw();
		}
			
		private function draw():void
		{
			graphics.clear();
			graphics.beginFill(AbstractTableView.GRAY_4D);
			graphics.drawRect(0,0,_width,AbstractTableView.ROW_HEIGHT);
			
			// Bottom line
			graphics.lineStyle(2,0x000000);
			graphics.moveTo(0,AbstractTableView.ROW_HEIGHT);
			graphics.lineTo(_width,AbstractTableView.ROW_HEIGHT);
		}
		
		
		override public function set width(value:Number):void
		{
			_width = value;
			draw();
		}
		
		public function set labelField(value:String):void{
			_labelField.text = value;
		}
	}
}