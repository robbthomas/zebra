package com.alleni.author.view.objects
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.event.WireEvent;
	import com.alleni.author.model.objects.Calculator;
	import com.alleni.author.model.objects.InputText;
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.MessageCenterView;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.collections.IList;

	public class CalculatorView extends LogicObjectView
	{

		private var barContainer:Sprite = null;
		private var barContainerMask:Sprite = null;
		private var titleBar:Object = null;
		private var inputRows:Vector.<InputRow> = new Vector.<InputRow>;
		private var middleBar:Object = null;
		private var outputRows:Vector.<OutputRow> = new Vector.<OutputRow>;
		private var footerBar:Object = null;
        private var rowAdjustment:int = 0

		public function get calculator():Calculator {
			return model.value as Calculator;
		}

		public function CalculatorView(viewContext:ViewContext, model:TaconiteModel, role:uint)
		{
			super(viewContext, model, role);
			_width = 205;
			if(!calculator.loading) {
				setup();
			}
		}

		private var nextLine:int;
		private function nextLineListener(event:Event):void {
			removeEventListener(Event.ENTER_FRAME, nextLineListener);
			LightEditableLabel(outputRows[nextLine].expression).mediator.openEditing();
		}

		public function goToNextLine(row:int):void {
			while(row >= outputRows.length) {
				calculator.calcOutputExpressions.addItem("Enter Expression");
			}
			nextLine = row;
			addEventListener(Event.ENTER_FRAME, nextLineListener);
		}

		private function minimizedListener(event:Event):void {
			maximize();
		}

		private function setup():void {
			model.addEventListener(WireEvent.COMPLETE, handleWireComplete);
			model.addEventListener(WireEvent.DETACH, handleWireDetach);

			barContainer = new Sprite();
			addChild(barContainer);
			barContainerMask = new Sprite();
			addChild(barContainerMask);
			barContainer.mask = barContainerMask;

			createTitleBar();
			createMiddleBar();
			createFooterBar();
			var anchor:WireAnchor;
			var i:int
			for(i=0; i<IList(calculator.anchors["calcInputValues"]).length; i++) {
				anchor = IList(calculator.anchors["calcInputValues"])[i];
				var input:InputRow = createInputRow(i);
				if(anchor.isAdditionAnchor == false) {
					var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
					if(wires.length == 0) {
						input.wireRemoved();
					} else {
						input.wireAdded();
					}
				}
			}
			for(i=0; i<IList(calculator.anchors["calcOutputValues"]).length; i++) {
				anchor = IList(calculator.anchors["calcOutputValues"])[i];
				createOutputRow(i);
			}
		}
		
		public override function getBounds(targetCoordinateSpace:DisplayObject):Rectangle{
			var objPoint:Point = new Point(x, y);
			objPoint = targetCoordinateSpace.globalToLocal(this.parent.localToGlobal(objPoint));
			return new Rectangle(objPoint.x, objPoint.y, width, height);
		}
		
		public function createInputRow(row:int):InputRow {

			var input:InputRow;
            var updateText:Function = function(e:Event):void{
                inputRows[row].valueChanged();
            };
            if(row < PropertyDescription((calculator.anchors["calcInputValues"][0] as WireAnchor).modifierDescription).autoAddUpTo){
                var input:InputRow = new InputRow(calculator, this, row);
                input.x = 0;
                input.y = RibbonView.HEIGHT * (row + 1) + 5;
                inputRows.push(input);
                barContainer.addChild(input);
            }else{
                rowAdjustment = 1;
            }

            middleBar.bg.y = (RibbonView.HEIGHT * (inputRows.length + rowAdjustment) + 5);
            for(var i:int=0; i < outputRows.length; i++) {
                outputRows[i].y = (RibbonView.HEIGHT * (inputRows.length + rowAdjustment + 1 + i) + 5);
            }
            footerBar.bg.y = (RibbonView.HEIGHT * (inputRows.length + rowAdjustment + 1 + outputRows.length) + 5);
            return input;
		}

		private function createOutputRow(row:int):OutputRow {
			var numberOfInputRows = inputRows.length;
            var output:OutputRow = new OutputRow(calculator, this, row);
			output.x = 0;
			output.y = RibbonView.HEIGHT * (numberOfInputRows + 1 + row) + 5;
			outputRows.push(output);
			barContainer.addChild(output);
			// Remove the frame event, it is a hack
			var updateText:Function = function(e:Event):void{
				outputRows[row].valueChanged();
			};
			this.addEventListener(Event.EXIT_FRAME, updateText);
			footerBar.bg.y = RibbonView.HEIGHT * (numberOfInputRows + 1 + outputRows.length) + 5;
			return output;
		}

		private function makeJudgeNowPort():PortView
		{
			var result:PortView = new PortView(calculator.anchors["judgeNow"], context as WorldContainer, true);
			result.x = RibbonView.HEIGHT;
			result.y = PortView.WIDTH;
			result.scaleX = -1;
			result.angle = 180;
			result.rotation = 90;
			return result;
		}

		private function makeTitleLabel():RichTextField {
			var result:RichTextField = new RichTextField();
			result.text = calculator.title;
			result.color = 0xFFFFFF;
			result.x = 30;
			result.y = 4;
			return result;
		}

		private function createTitleBar():void {
			var bg:Sprite = new Sprite();
			var bar:Object = {
				bg: bg,
				judgeNowPort: makeJudgeNowPort(),
				title: makeTitleLabel(),
				minimizeBtn: makeMinimizeButton()
			};
			bg.addChild(bar.judgeNowPort);
			bg.addChild(bar.title);
			bg.addChild(bar.minimizeBtn);
			bg.x = 0;
			bg.y = 0;
			bg.graphics.clear();
			bg.graphics.beginFill(0x282828);
			bg.graphics.drawRect(0, 0, _width, RibbonView.HEIGHT + 5);
			barContainer.addChild(bg);
			titleBar = bar;
		}

		private function makeExpressionLabel():LightLabel {
			var result:LightLabel = new LightLabel(_width - 6);
			result.text = "Enter Expressions Below";
			result.x = PortView.WIDTH * 2 + 3;
			result.y = 0;
			return result;
		}

		private function createMiddleBar():void {
			var bg:Sprite = new Sprite();
			var bar:Object = {
				bg: bg,
				label: makeExpressionLabel()
			};
			bg.addChild(bar.label);
			bg.x = 0;
			bg.y = RibbonView.HEIGHT * inputRows.length;
			bg.graphics.clear();
			bg.graphics.beginFill(0x0064FF);
			bg.graphics.drawRect(0, 0, _width, RibbonView.HEIGHT);
			barContainer.addChild(bg);
			middleBar = bar;
		}

		private function createFooterBar():void {
			var bg:Sprite = new Sprite();
			var bar:Object = {
				bg: bg
			};
			bg.x = 0;
			bg.y = RibbonView.HEIGHT * (inputRows.length + 1 + outputRows.length) + 5;
			bg.graphics.clear();
			bg.graphics.beginFill(0xFFFFFF);
			bg.graphics.drawRect(0, 0, _width, 3);
			barContainer.addChild(bg);
			footerBar = bar;
		}

		private function adjustheight():void {
			_height = RibbonView.HEIGHT * (inputRows.length + rowAdjustment + 1 + outputRows.length) + 5 + 3;
		}


		override public function render():void {
			adjustheight();
			var borderWidth:Number = calculator.minimized?MessageCenterView.BORDER_WIDTH*5:MessageCenterView.BORDER_WIDTH;
			var borderOffset:Number = borderWidth/2;
			if(barContainerMask != null){
				barContainerMask.graphics.clear();
				barContainerMask.graphics.beginFill(0xFF0000, 0.2);
				barContainerMask.graphics.drawRoundRect(borderOffset, borderOffset, _width-borderWidth, _height-borderWidth, 10, 10);
				barContainerMask.graphics.endFill();
			}
			super.render();
		}

		override protected function updateArrayProperty(property:String, modelEventKind:String, array:IList, index:int):Boolean
		{
			var handled:Boolean = super.updateArrayProperty(property, modelEventKind, array, index);
			var anchorIndex:int = index;
			var calculator:Calculator = this.calculator;

			if (index == IList(calculator[property]).length-1) {
				// adding at the end position, so actually insert after the current end
				// to preserve newly created wires
				anchorIndex++;
			}

			if(calculator.loading || barContainer == null)
				return handled;

			switch(modelEventKind) {
				case ModelEvent.ADD_ARRAY_ITEM:
					switch(property) {
						case "calcInputValues":
							if(inputRows[anchorIndex-1].port.anchor.wired == false) {
								inputRows[anchorIndex-1].wireRemoved();
							}
							createInputRow(anchorIndex);
							
							ApplicationController.instance.wireController.requestRedrawForObject(calculator);
							return false;
						case "calcOutputExpressions":
							createOutputRow(index);
							
							return false;
					}
					break;
				case ModelEvent.CHANGE_ARRAY_ITEM:
					switch(property) {
						case "calcInputValues":
							inputRows[index].valueChanged();
							var updateText:Function = function(e:Event):void{
								inputRows[index].valueChanged(); 
								(e.currentTarget as EventDispatcher).removeEventListener(Event.EXIT_FRAME, updateText);
							};
							this.addEventListener(Event.EXIT_FRAME, updateText);
							return true;
						case "calcOutputExpressions":
							outputRows[index].expressionChanged();
							var updateText:Function = function(e:Event):void{
								outputRows[index].expressionChanged();
								(e.currentTarget as EventDispatcher).removeEventListener(Event.EXIT_FRAME, updateText);
							};
							this.addEventListener(Event.EXIT_FRAME, updateText);
							return true;
						case "calcOutputValues":
							outputRows[index].valueChanged();
							var updateText:Function = function(e:Event):void{
								outputRows[index].valueChanged(); 
									(e.currentTarget as EventDispatcher).removeEventListener(Event.EXIT_FRAME, updateText)
							};
							this.addEventListener(Event.EXIT_FRAME, updateText);
							return true;
						case "calcInputTypes":
							inputRows[index].typeChanged();
							return true;
						case "calcOutputTypes":
							outputRows[index].typeChanged();
							return true;
						case "calcInputDefaultNames":
						case "calcInputNames":
							if(index < inputRows.length) {
								inputRows[index].nameChanged();
							}
							return true;
					}
					break;
			}
			return handled;
		}

		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean 
		{
			var handled:Boolean = super.updateModelProperty(property, oldValue, newValue);
			if(calculator.loading) {
				return handled;
			}
			switch(property) {
				case "loading":
					if(!calculator.loading) {
						setup();
					}
					return true;
			}
			return handled;
		}
		
		protected function handleWireComplete(event:WireEvent):void
		{
			event.stopImmediatePropagation();
			var wire:Wire = event.wire;

			if(wire.masterAnchor == titleBar.judgeNowPort.anchor || wire.slaveAnchor == titleBar.judgeNowPort.anchor) {
				calculator.continuallyJudging = false;
				return;
			}

			var isMaster:Boolean = wire.masterAnchor.hostObject == calculator && wire.masterAnchor.hostProperty == "calcInputValues";
			var isSlave:Boolean = wire.slaveAnchor.hostObject == calculator && wire.slaveAnchor.hostProperty == "calcInputValues";

			if(!isMaster && !isSlave) {
				return;
			}

			var myAnchor:WireAnchor;
			var otherAnchor:WireAnchor;

			if(isMaster) {
				myAnchor = wire.masterAnchor;
				otherAnchor = wire.slaveAnchor;
			} else {
				myAnchor = wire.slaveAnchor;
				otherAnchor = wire.masterAnchor;
			}

            for(var i:int=0; i<IList(calculator.anchors["calcInputValues"]).length; i++) {
                var anchorObj:WireAnchor = IList(calculator.anchors["calcInputValues"])[i];
                if(anchorObj.isAdditionAnchor){
					var portViews:Vector.<PortView> = anchorObj.getViews(context as WorldContainer);
					if (portViews.length > 0)
						portViews[0].updateWireEnd();
                }
            }

			var title:String = otherAnchor.hostObject.title;
			var prop:PropertyDescription = otherAnchor.modifierDescription as PropertyDescription;

			var row:int = myAnchor.hostPropertyIndex;
			if(otherAnchor.hostProperty == "text" && otherAnchor.hostObject is InputText && InputText(otherAnchor.hostObject).numbersOnly) {
				calculator.calcInputConstraints[row] = null;
				calculator.calcInputTypes[row] = Modifiers.NUMBER_TYPE;
			} else {
				calculator.calcInputConstraints[row] = prop.constraints;
				calculator.calcInputTypes[row] = prop.type;
			}
			calculator.calcInputDefaultNames[row] = title.substr(0, 7) + "..." + ":" + prop.label;
			inputRows[row].wireAdded();
		}

		protected function handleWireDetach(event:WireEvent):void
		{
			event.stopImmediatePropagation();
			var anchor:WireAnchor = event.oldAnchor;

			if(anchor == null) {
				return;
			}

			if(anchor.modifierDescription.key == "judgeNow") {
				calculator.continuallyJudging = true;
			} else if(anchor.modifierDescription.key == "calcInputValues") {
				if(anchor.isAdditionAnchor == false) {
					var defaultName:String = anchor.hostPropertyIndex == 0? "Attach Property..." : "Local Variable";
					calculator.calcInputDefaultNames[anchor.hostPropertyIndex] = defaultName;
					inputRows[anchor.hostPropertyIndex].wireRemoved();
				}

                for(var i:int=0; i<IList(calculator.anchors[anchor.modifierDescription.key]).length; i++) {
                    anchor = IList(calculator.anchors[anchor.modifierDescription.key])[i];
                    if(anchor.isAdditionAnchor && anchor.getViews(context as WorldContainer).length > 0){
                        anchor.getViews(context as WorldContainer)[0].updateWireEnd();
                    }
                }
			}
		}

		override public function get width():Number {
			return _width;
		}

		override public function get height():Number {
			return ((inputRows.length + outputRows.length + 2) * RibbonView.HEIGHT); 
//			return _height;
		}

		override public function set width(value:Number):void {
			// nop
		}

		override public function set height(value:Number):void {
			// nop
		}
	}
}

import com.alleni.author.controller.text.TextEditMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.action.ModifyObjectProperty;
import com.alleni.author.definition.text.FormattedText;
import com.alleni.author.model.objects.Calculator;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.objects.CalculatorView;
import com.alleni.author.view.text.LightEditableLabel;
import com.alleni.author.view.text.LightLabel;
import com.alleni.author.view.ui.AbstractRibbonControl;
import com.alleni.author.view.ui.AlignSwitch;
import com.alleni.author.view.ui.BooleanSwitch;
import com.alleni.author.view.ui.PortView;
import com.alleni.author.view.ui.RibbonValueField;
import com.alleni.author.view.ui.RibbonView;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import flashx.textLayout.formats.TextAlign;

import mx.collections.IList;


class InputRow extends Sprite {
	public var row: int;
	public var letterChip: Sprite;
	public var letter: LightLabel;
	public var port: PortView;
	public var inputName: LightEditableLabel;
	public var editor: AbstractRibbonControl;

	private var calculator:Calculator;
	private var view:CalculatorView;

	public function InputRow(calculator:Calculator, view:CalculatorView, row:int) {
		this.calculator = calculator;
		this.view = view;
		this.row = row;
		port = makeInputPort();
		this.addChild(port);
		this.x = 0;
		this.y = RibbonView.HEIGHT * (row+1);
		var input:InputRow = this;
//		this.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void {
//			if(inputName && input == event.target || inputName.contains(event.target as DisplayObject)) {
//				inputName.mediator.openEditing();
//			}
//		});
	}

	public function typeChanged():void {
		if(editor && editor.parent) {
			editor.parent.removeChild(editor);
		}
		editor = makeInputEditor();
		this.addChild(editor);
	}

	public function nameChanged():void {
		if(inputName) {
			inputName.text = calculator.getInputName(row);
		}
	}

	public function valueChanged():void {
		if(editor) {
			editor.setValue(calculator.calcInputValues[row], calculator.calcInputTypes[row]);
		}
	}

	private function editorCallback(value:Object):void {
        var oldValue:Object = calculator.calcInputValues[row];
        if(oldValue === value) {
            return;
        }
		calculator.calcInputValues[row] = value;
        ApplicationController.currentActionTree.commit(ModifyObjectProperty.fromObject(calculator, "set calculator input value", "calcInputValues", row, oldValue));
	}

	private function nameCallback(value:String):void {
		inputName.edit = false;
		value = new FormattedText(value).toString();
		if(value == calculator.calcInputDefaultNames) {
			value = "";
		}
        var oldValue:String = calculator.calcInputNames[row];
        if(oldValue == value) {
            return;
        }
		calculator.calcInputNames[row] = value;
        ApplicationController.currentActionTree.commit(ModifyObjectProperty.fromObject(calculator, "set calculator input name", "calcInputNames", row, oldValue));
	}

	private function makeInputPort():PortView {
		var result:PortView = new PortView(IList(calculator.anchors["calcInputValues"]).getItemAt(row) as WireAnchor, view.context as WorldContainer);
		result.x = 0;
		result.y = 0;
		result.angle = 180;
		return result;
	}

	private function makeInputName():LightEditableLabel {
		var result:LightEditableLabel = new LightEditableLabel(view.width - RibbonView.VALUE_WIDTH - PortView.WIDTH*3 - 6, RibbonView.HEIGHT-3);
		result.mediator.setCallBacks({
				okEditFunc: TextEditMediator.singleClickToEdit,
				updateFunc:null,
				keyPressFunc:TextEditMediator.closeOnEnter,
				closeFunc: nameCallback,
				hittestFunc:null
			});
		result.size = 11;
		result.textAlign = TextAlign.LEFT;
		result.expandable = false;
		result.color = 0xFFFFFF;
		result.setPaddingParams({left:0, top:3, right:0, bottom:0});
		result.explicitLineBreak = true;
		result.x = PortView.WIDTH * 2 + 3;
		result.y = 0;
		return result;
	}

	private function makeInputEditor():AbstractRibbonControl {
		var result:AbstractRibbonControl = AbstractRibbonControl.createControl(calculator.getInputDescription(row), editorCallback, calculator, calculator.calcInputValues[row], RibbonView.VALUE_WIDTH, RibbonView.HEIGHT);
		result.x = view.width - PortView.WIDTH - RibbonView.VALUE_WIDTH;
		result.y = 0;
		if (result is AlignSwitch) {
			result.x += -58;
			result.y += 2;
		}
		else if (result is BooleanSwitch) {
			result.x += -74;
			result.y += 2;
		}
		return result;
	}

	private function makeLetterChip():Sprite {
		var result:Sprite = new Sprite();
		var size:Number = Math.min(PortView.WIDTH, RibbonView.HEIGHT);
		result.graphics.clear();
		result.graphics.lineStyle(undefined);
		result.graphics.beginFill(0xA5A5A5);
		result.graphics.drawRoundRect(1, 1, PortView.WIDTH-2, RibbonView.HEIGHT-2, 4, 4);
		result.graphics.endFill();
		result.x = PortView.WIDTH;
		result.y = 0;
		return result;
	}

	private function makeLetter():LightLabel {
		var result:LightLabel = new LightLabel(PortView.WIDTH);
		result.color = 0x000000;
		result.textAlign = TextAlign.CENTER;
		result.text = String.fromCharCode("A".charCodeAt() + row);
		result.x = PortView.WIDTH;
		result.y = 0;
		return result;
	}

	public function wireAdded():void {
		port.draw();

		if(letterChip == null) {
			letterChip = makeLetterChip();
			this.addChild(letterChip);
			letter = makeLetter();
			this.addChild(letter);
		}

		if(inputName == null) {
			inputName = makeInputName();
			this.addChild(inputName);
		}
		LightEditableLabel(inputName).text = calculator.getInputName(row);

		if(editor != null) {
			if(editor.parent != null) {
				editor.parent.removeChild(editor);
			}
		}
		editor = makeInputEditor();
		this.addChild(editor);

		this.graphics.clear();
		this.graphics.beginFill(0x333333);
		this.graphics.drawRect(0, 0, view.width, RibbonView.HEIGHT);
		this.graphics.endFill();
	}

	public function wireRemoved():void {
		port.draw();

		if(letterChip == null) {
			letterChip = makeLetterChip();
			this.addChild(letterChip);
			letter = makeLetter();
			this.addChild(letter);
		}

		if(inputName == null) {
			inputName = makeInputName();
			this.addChild(inputName);
		}
		LightEditableLabel(inputName).text = calculator.getInputName(row);

		if(editor != null) {
			if(editor.parent != null) {
				editor.parent.removeChild(editor);
			}
		}
		editor = makeInputEditor();
		this.addChild(editor);

		this.graphics.clear();
		this.graphics.beginFill(0x282828);
		this.graphics.drawRect(0, 0, view.width + 20, RibbonView.HEIGHT);
		this.graphics.endFill();
	}

	public function becameAddition():void {
		port.draw();

		if(inputName != null) {
			if(inputName.parent != null) {
				inputName.parent.removeChild(inputName);
			}
			inputName = null;
		}

		if(editor != null) {
			if(editor.parent != null) {
				editor.parent.removeChild(editor);
			}
		}
		editor = null;

		this.graphics.clear();
	}
}

class OutputRow extends Sprite {
	public var row: int;
	public var port: PortView;
	public var expression: LightEditableLabel;
	public var editor: AbstractRibbonControl;
	public var overLine: Sprite;

	private var calculator:Calculator;
	private var view:CalculatorView;

	public function OutputRow(calculator:Calculator, view:CalculatorView, row:int) {
		this.calculator = calculator;
		this.view = view;
		this.row = row;
		port = makeOutputPort();
		expression = makeOutputExpression();
		editor = makeOutputEditor();
        overLine = makeOverline();

		this.addChild(port);
		this.addChild(expression);
		this.addChild(editor);
        this.addChild(overLine);
		var output:OutputRow = this;
		this.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void {
			if(output == event.target || expression.contains(event.target as DisplayObject)) {
				expression.mediator.openEditing();
			}
		});

		this.graphics.clear();
		this.graphics.beginFill(0xFFFFFF);
		this.graphics.drawRect(0, 0, view.width, RibbonView.HEIGHT);
	}

	public function typeChanged():void {
		if(editor && editor.parent) {
			editor.parent.removeChild(editor);
		}
		editor = makeOutputEditor();
		this.addChild(editor);
	}

	public function valueChanged():void {
		editor.setValue(calculator.calcOutputValues[row], calculator.calcOutputTypes[row]);
	}

	public function expressionChanged():void {
        var exp:String = calculator.calcOutputExpressions[row];
        overLine.graphics.clear();
        if(/^\s*[\+\-\*\/]/.test(exp) == false) {
            overLine.graphics.lineStyle(2);
            overLine.graphics.moveTo(0, 0);
            overLine.graphics.lineTo(view.width,  0);
        }
		expression.text = exp;
	}

	private function makeOutputPort():PortView {
		var result:PortView = new PortView(IList(calculator.anchors["calcOutputValues"]).getItemAt(row) as WireAnchor, view.context as WorldContainer);
		result.x =  view.width - PortView.WIDTH;
		result.y = 0;
		return result;
	}

    private function makeOverline():Sprite {
        var result:Sprite = new Sprite();
        result.x = 0;
        result.y = 0;
        return result;
    }

	public function expressionKeyPressFunc(charCode:uint):int {
		if (charCode == Keyboard.ENTER) {
			view.goToNextLine(row+1);
			return TextEditMediator.REQ_CLOSE;
		} else {
			return TextEditMediator.REQ_NULL;
		}
	}

	private function expressionCallback(value:String):void {
		expression.edit = false;
		value = new FormattedText(value).toString();
        var oldValue:String = calculator.calcOutputExpressions[row];
        if(oldValue == value) {
            return;
        }
		calculator.calcOutputExpressions[row] = value;
        ApplicationController.currentActionTree.commit(ModifyObjectProperty.fromObject(calculator, "set calculator expression", "calcOutputExpressions", row, oldValue));
	}

	private function makeOutputExpression():LightEditableLabel {
		var result:LightEditableLabel = new LightEditableLabel(view.width - RibbonView.VALUE_WIDTH - PortView.WIDTH-6, RibbonView.HEIGHT-3);
		result.mediator.setCallBacks({
				okEditFunc: TextEditMediator.singleClickToEdit,
				updateFunc:null,
				keyPressFunc: expressionKeyPressFunc,
				closeFunc: expressionCallback,
				hittestFunc:null
			});
		result.size = 11;
		result.textAlign = TextAlign.RIGHT;
		result.expandable = false;
		result.color = 0x000000;
		result.setPaddingParams({left:0, top:3, right:0, bottom:0});
		result.explicitLineBreak = true;
		result.x = 3;
		result.y = 0;
        if(calculator.calcOutputExpressions.length == 0 || calculator.calcOutputExpressions[row] == ""){
            result.text = "Enter Expression...";
        }else{
		    result.text = calculator.calcOutputExpressions[row];
        }
		return result;
	}

	private function makeOutputEditor():AbstractRibbonControl {
		var result:AbstractRibbonControl = AbstractRibbonControl.createControl(calculator.getOutputDescription(row), function(value:Object):void{}, calculator, calculator.calcOutputValues[row], RibbonView.VALUE_WIDTH, RibbonView.HEIGHT);
		if(result is RibbonValueField) {
			RibbonValueField(result).valueBackgroundColor = 0xE6E6E6;
			RibbonValueField(result).color = 0x000000;
			RibbonValueField(result).textAlign = TextAlign.LEFT;
		}
		result.x = view.width - PortView.WIDTH - RibbonView.VALUE_WIDTH;
		result.y = 0;
		if (result is AlignSwitch) {
			result.x += -58;
			result.y += 2;
		}
		else if (result is BooleanSwitch) {
			result.x += -74;
			result.y += 2;
		}
		return result;
	}
}
