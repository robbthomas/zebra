package com.alleni.author.view.objects.tables
{
	import com.alleni.author.model.objects.AbstractTable;
	
	import mx.collections.IList;

	public class TableTweenGrid extends TableGrid
	{
		public function TableTweenGrid(table:AbstractTable, tableView:AbstractTableView, columns:int, rows:int, initialValues:IList)
		{
			super(table, tableView, columns, rows, initialValues);
		}


        override public function addColumns(columns:int):void {
            super.addColumns(columns);

            if(_table.column > 0){
                for(var i:uint; i < _rows; i++){
                    this.getCell(i, (_table.numColumns - 1)).value = _table.allTweenValues[i*_table.numColumns-1 + _table.column]
                }
            }
        }

        override protected function createCell(row:int, column:int, highlighted:Boolean):TableCell
		{
			return new TableTweenCell(_table, this, row, column, highlighted);
		}
	}
}