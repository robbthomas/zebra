/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.view.objects
{
	import com.alleni.author.controller.app.FontController;
	import com.alleni.author.controller.objects.ToggleButtonMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.DoubleClickAction;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.objects.ToggleButton;
	import com.alleni.author.model.objects.ToggleButtonGroup;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.model.ui.Asset;
	import com.alleni.author.util.Bitmap9Slice;
	import com.alleni.author.view.GraphicFillView;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
    import flash.display.BlendMode;
    import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	/**
	 *
	 * @author Shannon Jackson
	 * 
	 */
	public class ToggleButtonView extends ObjectView
	{
		public var textField:RichTextField;
		public var currentDisplayedGraphic:Sprite = new Sprite();
		
		protected var _fillSpec:GraphicFill;
		protected var _fillSpecDown:GraphicFill;
		
		private var _currentBitmap:Bitmap9Slice = new Bitmap9Slice(null,"auto",true);
		private var _fillView:GraphicFillView = new GraphicFillView();
		private var _optionalSelectVector:Sprite = new Sprite();
		private var _mediator:ToggleButtonMediator;
        private var _clickMask:Sprite = new Sprite();

		/**
		 * Constructor
		 *  
		 * @param context
		 * @param model
		 * @param role
		 * 
		 */
		public function ToggleButtonView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			addChild(_clickMask);
			textField = new RichTextField(NaN,NaN);
			textField.formattable = false;
			textField.doubleClickEnabled = true;
			textField.setPadding(0);
			currentDisplayedGraphic.doubleClickEnabled = true;
			_optionalSelectVector.doubleClickEnabled = true;
			_fillView.mouseEnabled = false;
			_fillView.mouseChildren = false;
			
			_mediator = new ToggleButtonMediator(this);
			
			addChild(currentDisplayedGraphic);
			addChild(_optionalSelectVector);
			addChild(textField);
			currentDisplayedGraphic.addChild(_fillView);
			
			ApplicationController.addEventListener(NotificationNamesApplication.FONTS_PROGRESS, handleFontsProgress);

			if(!btn.loading){
				init();
			}
		}
		
		private function init():void
		{
            updateView();
			textField.rawText = currentLabel;
            positionText();
            _mediator.monitorToggleButtonGroup(btn.toggleButtonGroup);
			
			updateLabel();

			var font:String = btn.fontFamily;
			FontController.instance.requestByName(font);
            textField.fontFamily = btn.fontFamily;

		}

        public function adjustButtonSizeForNewAsset():void
        {
            updateEnabledView();  // immediately adjust the btn.width & height, so Undo can grab it
        }

		/**
		 * Responsible for setting appropriate bitmapData to
		 * the bitmap that is shown on the stage.  It also
		 * recalculates the scale9Grid based off of the 
		 * dimensions of the current image.
		 * 
		 * @param bitmapData
		 * 
		 */
		private function applyBitmapData(data:BitmapData):void
		{
			// verify data is not null
			if(data) {
				_currentBitmap.bitmapData = data;
				
				if(_currentBitmap.width && _currentBitmap.height) {
					// create the scale9Grid rectangle that determines where the 
					// bitmap is sliced. The first version of this is a 1px x 1px 
					// at the center of the bitmap.  Latter versions could allow 
					// the user to define the rectangle in an enhanced ribbon.
					_currentBitmap.scale9Grid = drawScale9Grid(data);
					// handle assets of different sizes				
					if(btn.draggingHandle) {
						btn.widthScale = btn.width/data.width;
						btn.heightScale = btn.height/data.height;
					}
					if(!btn.buttonFit) {
						if(!btn.draggingHandle) {
							btn.x += (btn.width-(data.width*btn.widthScale))/2;
							btn.y += (btn.height-(data.height*btn.heightScale))/2;
							btn.width = data.width*btn.widthScale;
							btn.height = data.height*btn.heightScale;
							positionText();
						}
					}
				}
			}
		}
		
		/**
		 * Overrideable method that defines the 9-slicing
		 * Rectangle.  
		 * 
		 * NOTE: returning null stops 9 slicing.
		 *  
		 * @return 
		 * 
		 */
		protected function drawScale9Grid(data:BitmapData):Rectangle
		{
			var rect:Rectangle;
			if(btn.bitmap9Slicing) {
				rect = new Rectangle(data.width/2,data.height/2,1,1);
			}else{
				rect = new Rectangle(0,0,data.width,data.height);
			}
			return rect;
		}
		
		/**
		 * Getter for the model.
		 *  
		 * @return 
		 * 			Toggle button model.
		 * 
		 */
		public function get btn():ToggleButton
		{
			return model.value as ToggleButton;
		}
		
		final override public function get doubleClickAction():int
		{
			if (Application.running)
				return DoubleClickAction.STOP;
			else
				return DoubleClickAction.EDIT;
		}
		
		final override protected function updateDisabledView():void
		{
			updateEnabledView();
		}
		
		/**
		 * Update this object's enabled view
		 */
		final override protected function updateEnabledView():void
		{
			super.updateEnabledView();

			if(btn.interactive) {
				if(btn.checked) {
					if(btn.over) {
						if(btn.down) {
							renderView(btn.toggleButtonCheckedDown, renderVectorSelectedDownView);
						}else{
							renderView(btn.toggleButtonCheckedOver, renderVectorSelectedOverView);
						}
					}else{
						renderView(btn.toggleButtonCheckedUp,renderVectorSelectedUpView);
					}
				}else{
					if(btn.over) {
						if(btn.down) {
							renderView(btn.toggleButtonNormalDown, renderVectorDownView);
						}else{
							renderView(btn.toggleButtonNormalOver, renderVectorOverView);
						}
					}else{
						renderView(btn.toggleButtonNormalUp, renderVectorUpView);
					}
				}
			}else{
				if(btn.checked) {
					renderView(btn.toggleButtonCheckedDisabled, renderVectorSelectedDisabledView);
				}else{
					renderView(btn.toggleButtonNormalDisabled, renderVectorDisabledView);
				}
			}
			
			rotation = btn.rotation;
			
			if(btn.isGraphicMode) {
				if(currentDisplayedGraphic.contains(_optionalSelectVector)) currentDisplayedGraphic.removeChild(_optionalSelectVector);	
				if(currentDisplayedGraphic.contains(_fillView)) currentDisplayedGraphic.removeChild(_fillView);	
				if(!currentDisplayedGraphic.contains(_currentBitmap)) currentDisplayedGraphic.addChild(_currentBitmap);
				
				currentDisplayedGraphic.width = _currentBitmap.width = btn.width;
				currentDisplayedGraphic.height = _currentBitmap.height = btn.height;
				currentDisplayedGraphic.x = btn.left;
				currentDisplayedGraphic.y = btn.top;
			}else{
				if(currentDisplayedGraphic.contains(_currentBitmap)) currentDisplayedGraphic.removeChild(_currentBitmap);
				if(!currentDisplayedGraphic.contains(_fillView)) currentDisplayedGraphic.addChild(_fillView);
				if(!currentDisplayedGraphic.contains(_optionalSelectVector)) currentDisplayedGraphic.addChild(_optionalSelectVector);
				
				// flip the jellybean glare when button is down
				_fillSpec.flipGlare = btn.down;
				_fillSpecDown.flipGlare = btn.down;
				// update the fill-view child object
				updateFillView(((btn.checked )?_fillSpecDown:_fillSpec));
				currentDisplayedGraphic.width = btn.width;
				currentDisplayedGraphic.height = btn.height;
				// clean up sprite
				_optionalSelectVector.graphics.clear();
				while(_optionalSelectVector.numChildren) {
					_optionalSelectVector.removeChildAt(0);
				}
				// draw new selector if needed
				if(btn.checked) drawSelectVector(_optionalSelectVector);
				// position the fillView
				_fillView.x = btn.left;
				_fillView.y = btn.top;
				
			}
			
			textField.color = btn.fontColor;
			textField.kerning = btn.kerning;
			textField.italic = btn.italic;
			textField.bold = btn.bold;
			textField.underline = btn.underline;
			textField.textAlign = btn.textAlignH;
			textField.verticalAlign = btn.textAlignV;
			textField.size = btn.fontSize;
			textField.fontAlpha = btn.fontAlpha/100;
			
			positionText();
			
			// switch off our protection for first render and the "fit" feature
			//btn.assetsArrivedDuringLoad = false;
		}
		
		protected function updateFillView(newFillSpec:GraphicFill):void
		{
			_fillView.update(newFillSpec, btn.width, btn.height, btn.fillAlpha/100, btn.cornerRadius, 
				btn.lineThickness, btn.lineColor, btn.lineAlpha/100, context.info.testView);
		}

		/**
		 * Overrideable method for drawing an optional vector "selected" indicator when not in bitmapMode.
		 *  
		 * @param text
		 * 
		 */
		protected function drawSelectVector(sprite:Sprite):void
		{
			// to be overridden
		}
		
		/**
		 * Overrideable method for positioning the text field.
		 *  
		 * @param text
		 * 
		 */
		protected function positionText():void
		{
			//textField.setCompositionSize(btn.width, btn.height);
			textField.setCompositionSize(btn.textWidth,NaN);

			if(btn.labelPosition.x == 0 && btn.labelPosition.y == .5) {				// left position
				textField.x = btn.left - textField.containerWidth - btn.labelPadding;
				textField.y = btn.top + ((btn.height-textField.containerHeight+(btn.margin))/2);
			} else if (btn.labelPosition.x == .5 && btn.labelPosition.y == 0){		// top position
				textField.x = btn.left + ((btn.width-textField.containerWidth)/2);
				textField.y = btn.top - (textField.containerHeight+btn.margin) - btn.labelPadding;
			} else if (btn.labelPosition.x == 1 && btn.labelPosition.y == .5){		// right position
				textField.x = btn.right + btn.labelPadding;
				textField.y = btn.top + ((btn.height-textField.containerHeight+(btn.margin))/2);;
			} else if (btn.labelPosition.x == .5 && btn.labelPosition.y == 1){		// bottom position
				textField.x = btn.left + ((btn.width-textField.containerWidth)/2);
				textField.y = btn.bottom + btn.margin + btn.labelPadding;
			}
			textField.setPadding(btn.margin);

            _clickMask.graphics.clear();
            _clickMask.graphics.beginFill(0xffffff,0.01);
            _clickMask.blendMode = BlendMode.DARKEN;  // prevent this rectangle from showing up in any way shape or form.
            _clickMask.graphics.drawRect(textField.x, textField.y,textField.getTextBounds(this).width,textField.getTextBounds(this).height);
		}
		
		override public function warnViewToDelete():void
		{
			super.warnViewToDelete();
			_mediator.warnViewToDelete();
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{
			var handledModelUpdate:Boolean = super.updateModelProperty(property, oldValue, newValue);
			
			if(btn.loading) {
				return handledModelUpdate;  //if handledModelUpdate is true, then viewupdate isn't required, we've handled it
			}
			
			switch (property) {
				case "loading":
					if(newValue == false){
						init();
					}
					break;
				case "toggleButtonNormalUp":
				case "toggleButtonNormalDown":
				case "toggleButtonNormalOver":
				case "toggleButtonNormalDisabled":
				case "toggleButtonCheckedUp":
				case "toggleButtonCheckedDown":
				case "toggleButtonCheckedOver":
				case "toggleButtonCheckedDisabled":
					_mediator.acceptNewAsset(newValue as Asset, oldValue as Asset);
					break;
				case "toggleButtonGroup":
					if(oldValue){
						_mediator.unmonitorToggleButtonGroup(oldValue as ToggleButtonGroup);
					}
					if(newValue){
						_mediator.monitorToggleButtonGroup(newValue as ToggleButtonGroup);
					}
					break;
				case "showingGroupIndicator":
					if(newValue) _mediator.showGroupIndicator();
					break;
				case "isGraphicMode":
				case "buttonFit":
					updateView();
					break;
				case "label":
				case "labelDown":
					textField.rawText = currentLabel;
					break;
				case "textHeight":
					positionText();
					break;
				case "down":
				case "margin":
				case "labelPadding":
					if(!btn.editingText) {
						textField.rawText = currentLabel;	
					}
					positionText();
					break;
				case "momentary":
					if(newValue) btn.checked = false;
					break;
				case "checked":
					if(!newValue && btn.toggleButtonGroupValue == btn.toggleButtonValue) {
						btn.toggleButtonGroupValue = btn.toggleButtonGroupNullValue;
					}
					if(!btn.editingText) {
						textField.rawText = currentLabel;	
					}
					positionText();
					break;
				case "maintainAspect":
					if(newValue && btn.isGraphicMode) {
						btn.width = _currentBitmap.bitmapData.width;
						btn.height = _currentBitmap.bitmapData.height;
					}
					break;
				case "assetsLoaded":
					updateView();
					break;
				case "fontFamily":
					textField.fontFamily = newValue as String;
					break;
				default: 
					// nothing to do
					break;
			}
			return super.updateModelProperty(property,oldValue,newValue);
		}

		private function get currentLabel():String
		{
			return (btn.checked && btn.labelDown != '') ? btn.labelDown : btn.label;
		}
		
		/**
		 * Responsible for determining whether to render
		 * bitmap, svg, or native graphics.
		 * @param asset
		 * @param override
		 * 
		 */
		private function renderView(asset:Asset, overrideFunc:Function):void
		{
			var bitmapData:BitmapData;
			if(btn.isGraphicMode){
				if(asset) {
					var bitmap:Bitmap = asset.content as Bitmap;
					if(bitmap) {
						bitmapData = bitmap.bitmapData;
						applyBitmapData(bitmapData);
					} else _mediator.informOfNeedForContent(asset);
				}
			}else{
				_fillSpec = btn.fillSpec;
				_fillSpecDown = btn.downColor;
				overrideFunc();
			}
		}

		/**
		 * Overrideable method allowing descendants to define
		 * there own GraphicFill for the up state. 
		 * @return 
		 * 
		 */
		protected function renderVectorUpView():void
		{
			// to be overridden
		}
		
		/**
		 * Overrideable method allowing descendants to define
		 * there own GraphicFill for the down state. 
		 * @return 
		 * 
		 */
		protected function renderVectorDownView():void
		{
			// to be overridden
		}
		
		/**
		 * Overrideable method allowing descendants to define
		 * there own GraphicFill for the over state. 
		 * @return 
		 *          
		 */
		protected function renderVectorOverView():void
		{
			// to be overridden
		}
		
		/**
		 * Overrideable method allowing descendants to define
		 * there own GraphicFill for the disabled state. 
		 * @return 
		 * 
		 */
		protected function renderVectorDisabledView():void
		{
			// to be overridden
		}
		
		/**
		 * Overrideable method allowing descendants to define
		 * there own GraphicFill for the selected/up state. 
		 * @return 
		 * 
		 */
		protected function renderVectorSelectedUpView():void
		{
			// to be overridden
		}
		
		/**
		 * Overrideable method allowing descendants to define
		 * there own GraphicFill for the selected/over state. 
		 * @return 
		 * 
		 */
		protected function renderVectorSelectedOverView():void
		{
			// to be overridden
		}
				
		/**
		 * Overrideable method allowing descendants to define
		 * there own GraphicFill for the selected/down state. 
		 * @return 
		 * 
		 */
		protected function renderVectorSelectedDownView():void
		{
			// to be overridden
		}
		
		/**
		 * Overrideable method allowing descendants to define
		 * there own GraphicFill for the selected/disabled state. 
		 * @return 
		 * 
		 */
		protected function renderVectorSelectedDisabledView():void
		{
			// to be overridden
		}
		
		private function updateLabel():void
		{
			var textHeight:Number = textField.containerHeight;
			if (textField.numLines <= 1)  // single-line label
				textHeight *= 0.66; // factor to omit the descenders, so a capital letter will be vertically centered
			btn.labelHeight = textHeight; 
		}
		
		override public function hitTestPoint(x:Number, y:Number, shapeFlag:Boolean=false):Boolean
		{
			if(Application.running)
				return super.hitTestPoint(x,y,shapeFlag) || textField.hitTestText(x,y);
			else
				return super.hitTestPoint(x,y,shapeFlag) && !textField.hitTestText(x,y);
		}

		override public function exactHitTest(stagePoint:Point):Boolean
		{
			if(Application.running)
				return super.exactHitTest(stagePoint) || textField.hitTestText(x,y);
			else
				return super.exactHitTest(stagePoint);
		}
		
		private function handleFontsProgress(event:Event):void
		{
			if(FontController.instance.requestedCount == 0)
				textField.fontFamily = btn.fontFamily;
		}
	}
}
