package com.alleni.author.view.objects.tables
{
	import com.alleni.author.view.text.LightLabel;

	public class TableTweenLabels extends TableSection
	{
		private static const X_OFFSET:Number = 35;
		
		private var _type:LightLabel;
		private var _transition:LightLabel;
		private var _delay:LightLabel;
		
		private var _rowHeight:Number = AbstractTableView.ROW_HEIGHT;

		public function TableTweenLabels()
		{
			super();
			
			setupLabels();
		}
		
		private function setupLabels():void
		{
			_type = new LightLabel();
			formatLabel(_type, "Trans- tween", 0);
			
			_transition = new LightLabel();
			formatLabel(_transition, "Trans- secs", _rowHeight);
			
			_delay = new LightLabel();
			formatLabel(_delay, "Hold- secs", _rowHeight*2);
			
			addChild(_type);
			addChild(_transition);
			addChild(_delay);
		}
		
		private function formatLabel(label:LightLabel, text:String, yPos:Number):void
		{
			label.text = text;
			label.color = 0xFFFFFF;
			label.x = X_OFFSET + 10;
			label.y = yPos;
		}
		
		private function draw():void
		{
			graphics.clear();
			graphics.lineStyle(1, 0x646464);
			
			// line0
			graphics.moveTo(X_OFFSET, _rowHeight);
			graphics.lineTo(width, _rowHeight);
			
			// line1
			graphics.moveTo(X_OFFSET, _rowHeight*2);
			graphics.lineTo(width, _rowHeight*2);
			
			// line2
			graphics.moveTo(X_OFFSET, _rowHeight*3);
			graphics.lineTo(width, _rowHeight*3);
		}
		
		override public function set width(value:Number):void
		{
			super.width = value;
			draw();
		}
	}
}