/**
 * © Copyright 2011 Allen Learning Technologies LLC.  All rights reserved.
 *
 * @author Allen Skunkworks
 * @version $Id$
 */
package com.alleni.author.view.objects
{
import com.alleni.author.Navigation.IPageView;
import com.alleni.author.Navigation.PagerView;
import com.alleni.author.controller.TransitionManager;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.event.ScrollUpdateEvent;
import com.alleni.author.model.ScrollModel;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.ui.Application;
import com.alleni.author.util.ViewUtils;
import com.alleni.author.view.HorizontalScrollView;
import com.alleni.author.view.VerticalScrollView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.WorldView;
import com.alleni.taconite.lang.TaconiteTimer;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Rectangle;

import mx.core.ScrollPolicy;

public class ArenaView extends PagerView
	{
        private var _dragHilite:Boolean;  // true while the clicked dragging object is inside this arena

		private var vScrollModel:ScrollModel;
		private var vScroller:VerticalScrollView;
		private var hScrollModel:ScrollModel;
		private var hScroller:HorizontalScrollView;
		private var scrollersSetUp:Boolean = false;


		public function ArenaView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);

            trace("ArenaView created:", this);
//            trace("ArenaView created: pager="+_pager, "controlVis="+_pager.controlsVisible, "resumeControlsVisible="+arena.resumeControlsVisible);
		}

		private function setUpScrollers():void
		{
			if(!scrollersSetUp && !arena.loading)
			{
				// setup scrollers
				vScrollModel = new ScrollModel(arena.height, 0, arena.height);
				vScroller = new VerticalScrollView(vScrollModel,0,arena.height);
				hScrollModel = new ScrollModel(arena.width, 0, arena.width);
				hScroller = new HorizontalScrollView(hScrollModel,0,arena.width);
				vScroller.addEventListener(ScrollUpdateEvent.SCROLL, handleScrollVert);
				hScroller.addEventListener(ScrollUpdateEvent.SCROLL, handleScrollHorz);

				vScroller.thumbColor = arena.scrollThumbColor;
				hScroller.thumbColor = arena.scrollThumbColor;
				vScroller.thumbOverColor = arena.scrollThumbOverColor;
				hScroller.thumbOverColor = arena.scrollThumbOverColor;
				vScroller.thumbAlpha = arena.scrollThumbAlpha;
				hScroller.thumbAlpha = arena.scrollThumbAlpha;
				vScroller.thumbLineColor = arena.scrollThumbLineColor;
				hScroller.thumbLineColor = arena.scrollThumbLineColor;
				vScroller.trackBackgroundColor = arena.scrollTrackColor;
				hScroller.trackBackgroundColor = arena.scrollTrackColor;
				vScroller.trackAlpha= arena.scrollTrackAlpha;
				hScroller.trackAlpha= arena.scrollTrackAlpha;
				vScroller.trackForegroundColor = arena.scrollTrackLineColor;
				hScroller.trackForegroundColor = arena.scrollTrackLineColor;

				scrollersSetUp = true;
			}
		}

		override public function warnViewToDelete():void
		{
			super.warnViewToDelete();

			vScroller.removeEventListener(ScrollUpdateEvent.SCROLL, handleScrollVert);
			hScroller.removeEventListener(ScrollUpdateEvent.SCROLL, handleScrollHorz);
		}

		private function get arena():Arena
		{
			return model.value as Arena;
		}

        override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
		{
            var result:Boolean = super.updateModelProperty(property,oldValue,newValue);
//            trace("ArenaView prop="+property, "val="+newValue, this, "loading="+object.loading, object.uid);
//            trace("ArenaView prop="+property, "val="+newValue, "pager="+_pager, "controlVis="+_pager.controlsVisible, "resumeControlsVisible="+arena.resumeControlsVisible);

        	switch (property) {
        		case "pageNumber":
					if (!arena.ticking) {
						ApplicationController.instance.authorController.log("arena page="+newValue + "  " + object.logStr);
					}
        			return true;  // handled the update
				case "active":
					if(newValue) {
						updateScrollers();
					}
					return true;
				case "contentHeight":
				case "contentWidth":
				case "vScroll":
				case "vScrollAuto":
				case "hScroll":
				case "hScrollAuto":
				case "layout":
                    handleSizeChange();
					break;
				case "pageRate":
					if(_controlBar)
						_controlBar.enablePlayButton(newValue as int > 0);
					break;
				case "ticking":
					if (_controlBar) {
						if (newValue)
							_controlBar.setPlayButtonState(ControlBar.PLAYING);
						else
							_controlBar.setPlayButtonState(ControlBar.PAUSED);
					}
					break;
				case "vertScrollPosition":
					positionContent();
					if (vScroller && !vScroller.dragging)  // ignore input from wires while user moves scroll thumb
						vScroller.scrollPosition = newValue as Number;
					break;
				case "horzScrollPosition":
					positionContent();
					if (hScroller && !hScroller.dragging)
						hScroller.scrollPosition = newValue as Number;
					break;
				case "vertScrollPolicy":
				case "horzScrollPolicy":
					updateScrollers();
					break;
				case "scrollThumbColor":
					if (vScroller) {
						vScroller.thumbColor = newValue as uint;
						hScroller.thumbColor = newValue as uint;
					}
					break;
				case "scrollThumbOverColor":
					if (vScroller){
						vScroller.thumbOverColor = newValue as uint;
						hScroller.thumbOverColor = newValue as uint;
					}
					break;
				case "scrollThumbAlpha":
					if (vScroller){
						vScroller.thumbAlpha = (newValue as uint)/100;
						hScroller.thumbAlpha = (newValue as uint)/100;
					}
					break;
				case "scrollThumbLineColor":
					if (vScroller){
						vScroller.thumbLineColor = newValue as uint;
						hScroller.thumbLineColor = newValue as uint;
					}
					break;
				case "scrollTrackColor":
					if (vScroller){
						vScroller.trackBackgroundColor = newValue as uint;
						hScroller.trackBackgroundColor = newValue as uint;
					}
					break;
				case "scrollTrackAlpha":
					if (vScroller){
						vScroller.trackAlpha= (newValue as uint)/100;
						hScroller.trackAlpha= (newValue as uint)/100;
					}
					break;
				case "scrollTrackLineColor":
					if (vScroller){
						vScroller.trackForegroundColor = newValue as uint;
						hScroller.trackForegroundColor = newValue as uint;
					}
					break;
       		}
        	return result;
        }

        override protected function afterLoad():void
        {
            super.afterLoad();
            setUpScrollers();
            updateScrollers();
            positionContent();
        }

        override protected function handleSizeChange():void
        {
            super.handleSizeChange(); // updates masks
            calculateClippedDimensions();
            positionContent();  // must follow calculateClippedDimensions
            updateScrollers();
        }

		private function updateScrollers():void
		{
			if(!arena.transientDrawingScrollers && !arena.loading)
			{
				var emptyPage:Boolean = arena.currentPage && arena.currentPage.objects.length == 0;
				var halfThick:Number = arena.lineThickness/2;
				setUpScrollers();
				arena.transientDrawingScrollers = true;
				arena.transientVScrollActive = (arena.vertScrollPolicy == ScrollPolicy.ON) || (arena.vertScrollPolicy == ScrollPolicy.AUTO && arena.contentHeight > spaceHeight);
				arena.transientHScrollActive = (arena.horzScrollPolicy == ScrollPolicy.ON) || (arena.horzScrollPolicy == ScrollPolicy.AUTO && arena.contentWidth > spaceWidth);
				// control vScroller visibility and positioning
				if(arena.transientVScrollActive)
				{
					addChild(vScroller);

					vScroller.x = arena.right - vScroller.width - halfThick;
					vScroller.y = arena.top + halfThick;
					vScroller.model.viewPortLength = spaceHeight;
					vScroller.model.maxScrollPosition = emptyPage ? 0 : arena.contentHeight;
					vScroller.scrollControllerHeight = spaceHeight - halfThick;
				}else{
					if(contains(vScroller))
					{
						removeChild(vScroller);
					}
				}
				// control hScroller visibility and positioning
				if(arena.transientHScrollActive)
				{
					addChild(hScroller);

					hScroller.x = arena.left + halfThick;
					hScroller.y = arena.bottom - hScroller.height - halfThick;
					hScroller.model.viewPortLength = spaceWidth;
					hScroller.model.maxScrollPosition = emptyPage ? 0 : arena.contentWidth;
					hScroller.scrollControllerWidth = spaceWidth - halfThick;
				}else{
					if(contains(hScroller))
					{
						removeChild(hScroller);
					}
				}
				if (emptyPage) {
					ApplicationController.instance.requestUpdateView(arena.currentPage.object.getView());  // update the blank-page, accounting for width of scrollbar
				}
			}
			arena.transientDrawingScrollers = false;
		}

		private function calculateClippedDimensions():void
		{
			arena.clippedWidth = (arena.contentWidth > spaceWidth) ? arena.contentWidth - spaceWidth : 0;
			arena.clippedHeight = (arena.contentHeight > spaceHeight) ? arena.contentHeight - spaceHeight : 0;
		}

		override protected function positionContent():void
		{
            // not calling super
            var xx:Number = arena.left - (arena.clippedWidth * (arena.horzScrollPosition/100));
			var yy:Number = arena.top - (arena.clippedHeight * (arena.vertScrollPosition/100));
            _background.x = xx;
            _background.y = yy;
            _pages.x = xx;
            _pages.y = yy;
		}

		private function get spaceWidth():Number
		{
			// width of open space, after deducting for scrollbar
			// we only care about lineThickness on the scrollbar side, since it moves the scrollbar
			// on the other sides, contents are simply masked to prevent overstriking the frame
			if (arena.transientVScrollActive && vScroller)
				return arena.width - vScroller.width - arena.lineThickness/2;
			else
				return arena.width;
		}

		private function get spaceHeight():Number
		{
			// height of open space, after deducting for scrollbar
			if (arena.transientHScrollActive && hScroller)
				return arena.height - hScroller.height - arena.lineThickness/2;
			else
				return arena.height;
		}

		private function handleScrollHorz(event:ScrollUpdateEvent):void
		{
			var clipWidth:Number = arena.contentWidth - spaceWidth;
			arena.horzScrollPosition = (clipWidth > 0) ? ((hScroller.scrollPosition / clipWidth)*100) : 0;
		}

		private function handleScrollVert(event:ScrollUpdateEvent):void
		{
			var clipHeight:Number = arena.contentHeight - spaceHeight;
			arena.vertScrollPosition = (clipHeight > 0) ? ((vScroller.scrollPosition / clipHeight)*100) : 0;
		}


		override protected function updateThumbnail():void {
			// Arena has no thumbnail:
			// it would overlap location of paging controls
			// it would require updating when any descendant changes
			// moving the arena thumbnail would cause all child thumbs to move also
		}

		/**
		 * Drag of other objects has begun.  Starting now, the rollover glow is tied to whether the
		 * clicked object being dragged is inside this arena.
		 * @param inside true if the frontmost object being dragged is inside this arena.
		 */
		public function set dragHilite(hilite:Boolean):void
		{
			_dragHilite = hilite;

			rolloverGlow = hilite;
		}
		public function get dragHilite():Boolean
		{
			return _dragHilite;
		}


		/**
		 * Set the rollover glow.
		 * While dragging other objects, we force the value we want...
		 * even tho the caller may be ObjectView responding to a rollout event.
		 * @param glow true to put on the glow.
		 *
		 */
		override protected function set rolloverGlow(value:Boolean) : void
		{
            // avoid a flash bug where rollover causes objects to shift, after a transition has occurred
            // and prevent flickering of rollover dropshadow while dragging a child object
			if ((arena.currentPage && arena.currentArenaPage.transientDraggingChild))
				return;

			if (Application.uiRunning) {
				super.rolloverGlow = _dragHilite;
			} else {
				super.rolloverGlow = value;
			}
		}

		override public function get filters():Array
		{
			// fix the VER-288 bug where dragging arena children was leaving cruft, by applying the filters to the
			// arena frame sprite instead of the arena view itself.
			return _frame.filters;
		}

		override public function set filters(value:Array):void
		{
			_frame.filters = value;
		}

		override public function hitTestObject(obj:DisplayObject):Boolean
		{
			// compare other object's bounds with the arena model bounds,
			// .... so that a huge object inside the arena will not match
			// the marquee outside the arena, causing arena to select
			var otherBounds:Rectangle = obj.getBounds(this);
			// BUG: when arena is rotated, this rotates the marquee bounds, makes it larger
			var myBounds:Rectangle = getBounds(this);
			return otherBounds.intersects(myBounds);
		}

        override public function handleRunningChanged():void {
            super.handleRunningChanged();
            if(Application.uiRunning)
            {
                dragHilite = false;
            }
            if(_controlBar){

                _controlBar.enablePlayButton(Application.running);

                if(arena.ticking) {
                    _controlBar.setPlayButtonState(ControlBar.PLAYING);
                } else {
                    _controlBar.setPlayButtonState(ControlBar.PAUSED);
                }

            }
        }

        override protected function beginTransitionEffect(pageView:IPageView, oldPageView:IPageView, transition:String, transSecs:Number, transTweenType:String, onComplete:Function):void
        {
            var lastShot:Sprite = getCaptureImage();  // capture current page
            immediatePageChange(pageView, true);
            var nextShot:Sprite = getCaptureImage();  // the next page
            var rect:Rectangle = new Rectangle(pageView.objView.x, pageView.objView.y, _pager.width, _pager.height);
            _transitions = new TransitionManager(lastShot, nextShot, rect, transition, transSecs, transTweenType);
            _pages.addChild(_transitions);
            _transitions.executeTransition();
            _transitionOnCompleteFunc = onComplete;
            _transitions.addEventListener(Event.COMPLETE, transitionCompleteListener);
        }

        private function getCaptureImage():Sprite
        {
            var result:Sprite;
            var fillColor:uint = arena.fillSpec.convertToColor();
            borderAlpha = 0;
            var rect:Rectangle = new Rectangle(object.left, object.top, object.width, object.height);  // local view coordinates
            if (arena.fillAlpha >= 100) {
                // capture whole Arena including background fill (helps prevent the one-pixel shift)
                result = ViewUtils.captureArenaBitmap(this, rect, _pages, fillColor);  // fillColor provided to fill the edge where bitmap width is rounded up to whole pixels
            } else {
                var worldView:WorldView = WorldContainer(context).worldView;
                rect = rectToSpace(rect, worldView);
                result = ViewUtils.captureArenaBitmap(worldView, rect, _pages, fillColor);  // section of worldView, to support transparent arena
            }
            borderAlpha = 1;
            return result;
        }

        private var _transitionOnCompleteFunc:Function;  // allows for removeTransition to be called early to terminate the transition before it completes

        private function transitionCompleteListener(e:Event):void
        {
            _transitionOnCompleteFunc();
            _transitionOnCompleteFunc = null;
        }

        override protected function removeTransition():void
        {
            _transitions.removeEventListener(Event.COMPLETE, transitionCompleteListener);
            _transitionOnCompleteFunc = null;
            super.removeTransition();
        }

        override protected function get maximumTransitionSeconds():Number
        {
            if (arena.ticking && arena.pageRate > 0) {
                var durationSecs:Number = 1 / arena.pageRate;
                var pageEndTime:Number = arena.startTime + durationSecs * 1000;
                var remainingSecs:Number = (pageEndTime - TaconiteTimer.instance.milliseconds) / 1000;
                if (remainingSecs <= 0) {
                    return 0;
                } else {
                    return remainingSecs;
                }
            }

            return super.maximumTransitionSeconds;
        }


        private function assert(condition:*):void
        {
            if (condition==null || condition==false)
                throw new Error("ArenaView");
        }


    }
}
