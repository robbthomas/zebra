package com.alleni.author.view.objects.tables
{
	import com.alleni.author.controller.objects.TableHeaderDragMediator;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.IconButton;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.events.MouseEvent;
	
	public class StateTableView extends AbstractTableView
	{	
		public function StateTableView(viewContext:ViewContext, model:TaconiteModel, role:uint)
		{
			_statusLabel = new LightLabel();
			super(viewContext, model, role);
		}
		
		override protected function createInitialSections():void
		{
			createValueSection();
		}
		
		override protected function get columnHeadingIcons():Array
		{
			var plusButton:IconButton = TableButtons.plusButton;
			plusButton.addEventListener(MouseEvent.CLICK, handlePlusButtonClick);
			
			var minusButton:IconButton = TableButtons.minusButton;
			minusButton.addEventListener(MouseEvent.CLICK, handleMinusButtonClick);
			
			var locateButton:IconButton = TableButtons.locateButton;
			locateButton.addEventListener(MouseEvent.CLICK, handleLocateClick);
			
			return [plusButton, minusButton, locateButton];
		}
	}
}