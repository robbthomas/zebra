package com.alleni.author.view.objects.tables
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.action.ModifyTableAction;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.AbstractRibbonControl;
	import com.alleni.author.view.ui.controls.PopOutMenu;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class TableTweenCell extends TableCell
	{	
		public function TableTweenCell(table:AbstractTable, grid:TableGrid, row:int, column:int, highlighted:Boolean)
		{	
			super(table, grid, row, column, highlighted);
			switch(row){
				case 0:
					setType(Modifiers.instance.fetch("tweenType") as PropertyDescription, "linear", 0xFFFFFF);
					break;
				case 1:
					setType(Modifiers.instance.fetch("tweenTime") as PropertyDescription, 0, 0xFFFFFF);
					break;
				case 2:
					setType(Modifiers.instance.fetch("tweenHold") as PropertyDescription, 0, 0xFFFFFF);
					break;
			}
		}
		
		override public function set value(val:Object):void
		{
			var oldValue:Object = _table.getTweenValue(_row, _column);
			_table.setTweenValue(_row, _column, val as String, true);
			ApplicationController.currentActionTree.commit(ModifyTableAction.fromObject(_table, "allTweenValues", _row, _column, oldValue));
		}
		
		override protected function get fillColor():uint
		{
			return 0;
		}
	}
}