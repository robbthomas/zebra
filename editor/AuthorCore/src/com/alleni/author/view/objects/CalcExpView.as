package com.alleni.author.view.objects
{
	import com.alleni.author.controller.objects.CalcExpController;
	import com.alleni.author.controller.objects.GadgetMediator;
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.model.objects.CalcExp;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	
	import mx.collections.IList;
	
	public class CalcExpView extends GadgetView
	{
		public static const TEXT_WIDTH:Number = 100;
		public static const PAD:Number = 3;
		public static const WIDTH:Number = TEXT_WIDTH+ RibbonView.HEIGHT*2 + PAD*2;
		
		private static const X_TEXT_FIELD:Number = 23;
		private static const Y_TEXT_FIELD:Number = 3;
		
		private var _inputPorts:Array = new Array();//PortView
		private var _outputPort:PortView;
		private var _inputNameBackground:Array = new Array();//Sprite
		private var _expressionBackground:Sprite;
		
		// Components
		private var _inputNameFields:Array = new Array();//RichTextField
		private var _expressionField:RichTextField;
		
		// event mediators
		private var _mediator:GadgetMediator;
		
		public function CalcExpView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			_mediator = new GadgetMediator(context);
			_mediator.handleViewEvents(this, role);
			
			addComponents();  // must be done before updateView
		}
		
		override public function initialize():void
		{
			super.initialize();  // calls createDragMediator, then does updateView
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean {
			
			var dealtWith:Boolean = super.updateModelProperty(property,oldValue,newValue);
			
			switch (property) {
				case "expression":
					_expressionField.text = calc.expression;
					return true;
				case "expressionValid":
					// color will update
					return false;
				case "toDiagram":
					updatePosition();
					return true;
			}
			return dealtWith;
		}
		
		/**
		 * Handle an array change by some incremental adjustment and return true,
		 * otherwise return false to reinitialize the view. 
		 */
		override protected function updateArrayProperty(property:String, modelEventKind:String, array:IList, index:int):Boolean
		{
			var field:RichTextField;
			var handled:Boolean = super.updateArrayProperty(property, modelEventKind, array, index);
			var anchorIndex:int = index;
			if(index == IList(calc[property]).length-1) {
				// adding at the end position, so actually insert after the current end
				// to preserve newly created wires
				anchorIndex++;
			}
			switch(modelEventKind) {
				case ModelEvent.ADD_ARRAY_ITEM:
					switch(property) {
						case "input":
							_inputPorts.splice(anchorIndex,0,new PortView(IList(calc.anchors["input"])[anchorIndex], context as WorldContainer));
							addChild(_inputPorts[anchorIndex]);
							ApplicationController.instance.wireController.requestRedrawForObject(calc);
							break;
						case "inputName":
							addInput(index);
							_inputNameFields[index].text = calc.inputName[index];
							break;
					}
					break;
				case ModelEvent.REMOVE_ARRAY_ITEM:
					switch(property) {
						case "input":
							removeChild(_inputPorts[index]);
							_inputPorts.splice(index,1);
							ApplicationController.instance.wireController.requestRedrawForObject(calc);
							break;
						case "inputName":
							removeChild(_inputNameBackground[index]);
							removeChild(_inputNameFields[index]);
							_inputNameBackground.splice(index,1);
							_inputNameFields.splice(index,1);
							break;
					}
					break;
				case ModelEvent.CHANGE_ARRAY_ITEM:
					switch(property) {
						case "input":
							return true;
						case "inputName":
							_inputNameFields[index].text = calc.inputName[index];
							return true;
					}
					break;
			}
			return handled;
		}
		
		override public function render() : void
		{
			graphics.clear();
			var h:Number = RibbonView.HEIGHT*(calc.input.length+1) + PAD*2;
			
			graphics.lineStyle(1, 0xBFBFBF, 0.75);
			var gradColors:Array;
			if (selected) {
				gradColors = [calc.expressionValid?0xBFFFBF:0xFFBFBF, 0x4D4D4D];  // top & bottom gradient, from msgCtr view
			} else {
				gradColors = [calc.expressionValid?0x195919:0x591919, 0x191919];  // top & bottom gradient, from msgCtr view
			}
			var gradAlphas:Array = [1,1];
			var gradRatios:Array = [0,255];
			var gradMatrix:Matrix = new Matrix();
			
			gradMatrix.createGradientBox(WIDTH, h, Math.PI/2);
			
			graphics.beginGradientFill(GradientType.LINEAR,gradColors,gradAlphas,gradRatios,gradMatrix);
			
			graphics.drawRoundRect(0,0,WIDTH,h,10,10);
			
			var index:Number;
			var anchor:WireAnchor;
			
			for(index=0; index< IList(calc.anchors["input"]).length-1; index++) {
				anchor = calc.anchors["input"][index];
                //TODO replace with location code
				//CalcExpController.instance.getAnchorLocationAndAngle(calc,anchor.modifierDescription,index).apply(anchor);
//				PortView(_inputPorts[index]).x = anchor.x;
//				PortView(_inputPorts[index]).y = anchor.y;
				if(index < _inputNameFields.length) {
					_inputNameFields[index].text = calc.inputName[index];
					_inputNameFields.x = 3 + RibbonView.HEIGHT;
					_inputNameFields.y = 3 + RibbonView.HEIGHT*index;
					_inputNameBackground.x = 3 + RibbonView.HEIGHT;
					_inputNameBackground.y = 3 + RibbonView.HEIGHT*index;
				}
			}
			
			anchor = calc.anchors["result"];
            //TODO replace with location code
			//CalcExpController.instance.getAnchorLocationAndAngle(calc,anchor.modifierDescription,index).apply(anchor);
//			_outputPort.x = anchor.x;
//			_outputPort.y = anchor.y;
			_expressionField.x = PAD + RibbonView.HEIGHT;
			_expressionField.y = PAD + RibbonView.HEIGHT*calc.input.length;
			_expressionBackground.x = PAD + RibbonView.HEIGHT;
			_expressionBackground.y = PAD + RibbonView.HEIGHT*calc.input.length;
			
			updatePosition();
			
			if (_role == ViewRoles.PRIMARY)
				this.visible = !Application.running;  // hide on Run
		}
		
		private function updatePosition():void
		{
			if (_dragMediator)
				_dragMediator.updateViewPosition();
		}
		
		override protected function updateStatus():void
		{
			updateView();  // selection is indicated by color change .. no handles
		}
		
		protected function addInput(n:Number):void
		{
			_inputNameBackground[n] = new Sprite();
			drawValueBackground(_inputNameBackground[n],100,RibbonView.HEIGHT,3 + RibbonView.HEIGHT,3 + RibbonView.HEIGHT*n);
			
			_inputNameFields[n] = new RichTextField(100,RibbonView.HEIGHT);
			_inputNameFields[n].explicitLineBreak = true;
			_inputNameFields[n].x = 3 + RibbonView.HEIGHT;
			_inputNameFields[n].y = 3 + RibbonView.HEIGHT*n;
			_inputNameFields[n].color = 0xFFFFFF;
			_inputNameFields[n].visible = true;
			addChild(_inputNameFields[n]);
			new TextEditMediator().handleTextEditEvents(_inputNameFields[n],null, handleUpdateNameField);
		}
		
		private function handleUpdateNameField(tx:RichTextField, val:String):void
		{
			var n:int = _inputNameFields.indexOf(tx);
			if (n != -1) {
				calc.inputName[n] = val;
			}
		}
		
		protected function addComponents():void
		{
			// add initial input port that adds more ports
			for(var i:Number = 0; i< IList(calc.anchors["input"]).length; i++) {
				_inputPorts[i] = new PortView(IList(calc.anchors["input"])[i], context as WorldContainer);
				_inputPorts[i].angle = 180;
				addChild(_inputPorts[i]);
				
				if(i < calc.inputName.length) {
					addInput(i);
					_inputNameFields[i].text = calc.inputName[i];
				}
			}
			
			_expressionBackground = new Sprite();
			drawValueBackground(_expressionBackground,100,RibbonView.HEIGHT,3 + RibbonView.HEIGHT,3 + RibbonView.HEIGHT*calc.input.length);
			
			_expressionField = new RichTextField(100,RibbonView.HEIGHT);
			_expressionField.explicitLineBreak = true;
			_expressionField.x = 3 + RibbonView.HEIGHT;
			_expressionField.y = 3 + RibbonView.HEIGHT*calc.input.length;
			_expressionField.color = 0xFFFFFF;
			_expressionField.text = calc.expression;
			addChild(_expressionField);
			new TextEditMediator().handleTextEditEvents(_expressionField,null,  
					function(tx:RichTextField,val:String):void{calc.expression=val} );

            //TODO replace with location code
			//CalcExpController.instance.getAnchorLocationAndAngle(calc,calc.modifiers["result"],0).apply(calc.anchors["result"]);
			_outputPort = new PortView(calc.anchors["result"], context as WorldContainer);
			_outputPort.angle = 0;
			addChild(_outputPort);
		}
		
		protected function drawValueBackground(sprite:Sprite, w:Number, h:Number, xx:Number, yy:Number):void
		{
			sprite.graphics.lineStyle(1);
			sprite.graphics.beginFill(0x191919, 0.8);
			sprite.graphics.drawRect(0,0,w,h);
			var shadow:DropShadowFilter = new DropShadowFilter(2,0);
			shadow.inner = true;
			sprite.filters = [shadow];
			sprite.x = xx;
			sprite.y = yy;
			
			addChild(sprite);
		}
		
		/**
		 * The Calc object associated with this view's TaconiteModel.
		 */
		public function get calc():CalcExp
		{
			return model.value as CalcExp;
		}
		
		
		
		/**
		 * Create a feedback view that will be superimposed on this view in a transparent layer.
		 */        
		override protected function ObjectFeedbackFactory():ITaconiteView
		{
			return null;
		}
		
		
		override protected function updateThumbnail():void
		{
			// prevent thumbnails on Calc
		}
		
	}
}
