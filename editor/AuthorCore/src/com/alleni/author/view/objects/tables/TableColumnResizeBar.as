package com.alleni.author.view.objects.tables
{
	import caurina.transitions.Tweener;
	
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.CapsStyle;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class TableColumnResizeBar extends TableSection
	{
		public static const WIDTH:Number = 4;
		
		private static const COLOR:uint = 0xFFFFFF;//0x6A6A6A;
		
		private var _viewContext:ViewContext;
		private var _view:AbstractTableView;
		private var _layout:TableLayoutManager;
		private var _guideIndex:int;
		private var _bar:Sprite;
		
		public function TableColumnResizeBar(viewContext:ViewContext, view:AbstractTableView, layout:TableLayoutManager, guideIndex:int)
		{
			super();
			
			_viewContext = viewContext;
			_view = view;
			_layout = layout;
			_guideIndex = guideIndex;

			switch(guideIndex) {
				case 0:
					topGuide = TableLayoutManager.V_TIMELINE_TOP;
					leftGuide = TableLayoutManager.H_GUIDE_RULE_1_LEFT;
					rightGuide = TableLayoutManager.H_EXPRESSION_LEFT;
					bottomGuide = TableLayoutManager.V_TWEEN_TOP;
					break;
				case 1:
					topGuide = TableLayoutManager.V_TIMELINE_TOP;
					leftGuide = TableLayoutManager.H_GUIDE_RULE_2_LEFT;
					rightGuide = TableLayoutManager.H_COL_0_LEFT;
					bottomGuide = TableLayoutManager.V_TABLE_BOTTOM;
					break;
				case 2:
					topGuide = TableLayoutManager.V_TIMELINE_TOP;
					leftGuide = TableLayoutManager.H_GUIDE_RULE_3_LEFT;
					rightGuide = TableLayoutManager.H_PORT_LEFT;
					bottomGuide = TableLayoutManager.V_TABLE_BOTTOM;
					break;
				default:
					throw new Error("Unknown guide index");
			}
			_layout.sections.push(this);
			_view.addChild(this);
			
			_bar = new Sprite();
			
			_bar.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			_bar.addEventListener(MouseEvent.ROLL_OVER, handleRollOver);
			_bar.addEventListener(MouseEvent.ROLL_OUT, handleRollOut);
			
			_bar.alpha = 0.01;
			
			addChild(_bar);
			
			draw();
		}
		
		private function draw():void
		{
			_bar.graphics.clear();
			_bar.graphics.lineStyle(WIDTH,COLOR,1.0,false,"normal",CapsStyle.NONE);
			_bar.graphics.moveTo(2, 0);
			_bar.graphics.lineTo(2, height);
		}
		
		private function handleMouseDown(e:MouseEvent):void
		{
			_bar.removeEventListener(MouseEvent.ROLL_OUT, handleRollOut); // Prevent rollout while dragging the resize bar

			_viewContext.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_viewContext.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			e.stopPropagation();
		}
		
		private function handleMouseMove(e:MouseEvent):void
		{
			_layout.setGuidePosition(_guideIndex, _view.globalToLocal(new Point(e.stageX, e.stageY)).x);
			_view.addEventListener(Event.ENTER_FRAME, _view.testForScrollBarVisible);
		}
		
		private function handleMouseUp(e:MouseEvent):void
		{
			_bar.addEventListener(MouseEvent.ROLL_OUT, handleRollOut);
			
			_viewContext.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_viewContext.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
						
			Tweener.addTween(_bar, {alpha:0.01, time:0.75, transition:"easeOutQuad"});
			
			_view.removeEventListener(Event.ENTER_FRAME, _view.testForScrollBarVisible);
		}
		
		private function handleRollOver(e:MouseEvent):void
		{
			Tweener.addTween(_bar, {alpha:1, time:0.15, transition:"easeOutQuad"});	
		}
		
		private function handleRollOut(e:MouseEvent):void
		{
			Tweener.addTween(_bar, {alpha:0.01, time:0.75, transition:"easeOutQuad"});
		}


	override public function set height(value:Number):void 
	{
		super.height = value;
		draw();
	}
}
}