package com.alleni.author.view.objects.tables
{
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	public class TruthTableView extends AbstractTableView
	{
		public function TruthTableView(viewContext:ViewContext, model:TaconiteModel, role:uint)
		{
			super(viewContext, model, role);
		}
		
		override protected function createGrid():void
		{
			_tableGrid = new TruthTableGrid(table,this,table.numColumns,table.port.length);
		}
		
		override protected function createInitialSections():void
		{
			createLogicSection();
			createValueSection();
		}
	}
}