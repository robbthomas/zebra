package com.alleni.author.view.objects
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.GotoURL;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.objects.tables.AbstractTableView;
	import com.alleni.author.view.objects.tables.TableButtons;
	import com.alleni.author.view.objects.tables.TableLayoutManager;
	import com.alleni.author.view.objects.tables.TableResizableContainer;
	import com.alleni.author.view.objects.tables.TableRibbonValueController;
	import com.alleni.author.view.objects.tables.TableSection;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.AbstractRibbonControl;
	import com.alleni.author.view.ui.BooleanSwitch;
	import com.alleni.author.view.ui.IconButton;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mx.collections.IList;
	
	public class GotoURLView extends LogicObjectView
	{
		private var _urlLabel:LightLabel;
		private var _queryLabel:LightLabel;
		private var _navigateLabel:LightLabel;
		private var _sendDataLabel:LightLabel;
		private var _newWindowLabel:LightLabel;
		
		private var _urlPortView:PortView;
		private var _queryPortView:PortView;
		private var _navigatePortView:PortView;
		private var _sendDataPortView:PortView;
		private var _newWindowPortView:PortView;
		
		private var _urlRibbonControl:AbstractRibbonControl;
		private var _queryRibbonControl:AbstractRibbonControl;
		private var _newWindowRibbonControl:AbstractRibbonControl;
		
		private var _toggleDotsBg:Sprite = new Sprite();
		private var _navigateToggle:Sprite;
		private var _sendDataToggle:Sprite;
		
		private var _gotoURLInlet:PortView;
		
		public function GotoURLView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			_fillColor = 0x272727;
			_context = context;
			_role = role;
			viewState = 0;
			init();
			if(!table.loading){
				setupTable();
			}
			_height = 140;
			_width = 208;
		}
		
		private function init():void{
			_urlRibbonControl =  AbstractRibbonControl.createControl(table.modifiers["theURL"], setValueURLCallback, null,
				(table as GotoURL).theURL, 100, AbstractTableView.ROW_HEIGHT);
			_queryRibbonControl =  AbstractRibbonControl.createControl(table.modifiers["queryString"], setValueQueryCallback, null,
				(table as GotoURL).queryString, 100, AbstractTableView.ROW_HEIGHT);
			_newWindowRibbonControl =  AbstractRibbonControl.createControl(table.modifiers["newWindow"], setValueWinCallback, null,
				(table as GotoURL).newWindow, 100, AbstractTableView.ROW_HEIGHT);
		}
		
		private function setupTable():void
		{
			createAndAddChildren();
			/*
			_model.addEventListener(WireEvent.COMPLETE, handleWireComplete);
			_model.addEventListener(WireEvent.DETACH, handleWireDetach);*/
		}
		
		override protected function createAndAddChildren():void
		{	
			// Add the expand/collapse button at the top of the table
			var collapseButton:IconButton = TableButtons.collapseButton;
			collapseButton.x = 15;
			collapseButton.y = 9;
			//collapseButton.addEventListener(MouseEvent.CLICK, collapseButtonClick);
			//addChild(collapseButton);
			
			// Add PowerButton
			var powerPorts:Sprite = addPowerPorts();
			powerPorts.x = 4;
			powerPorts.y = 4 + powerPorts.width;
			powerPorts.rotation = -90;
			addChild(powerPorts);
			
			// Add the title at the top of the table
			_title = new RichTextField();
			_title.text = table.title;
			_title.color = 0xFFFFFF;
			_title.x = 30;
			_title.y = 4;
			addChild(_title);
			
			addChild(getLine(20));
			
			var ribbonLocationTop:Number = 21;
			
			// Add Property Labels
			_urlLabel = new LightLabel()
			_urlLabel.text = "URL";
			_urlLabel.color = 0xFFFFFF;
			_urlLabel.x = 20;
			_urlLabel.y = ribbonLocationTop;
			addChild(_urlLabel);
			
			_queryLabel = new LightLabel();
			_queryLabel.text = "Query string";
			_queryLabel.color = 0xFFFFFF;
			_queryLabel.x = 20;
			_queryLabel.y = AbstractTableView.ROW_HEIGHT + _urlLabel.y + 3;
			addChild(_queryLabel);

			_navigateLabel = new LightLabel();
			_navigateLabel.text = "Jump";
			_navigateLabel.color = 0xFFFFFF;
			_navigateLabel.x = 20;
			_navigateLabel.y = AbstractTableView.ROW_HEIGHT + _queryLabel.y + 3;
			addChild(_navigateLabel);
			
			_sendDataLabel = new LightLabel();
			_sendDataLabel.text = "Send query";
			_sendDataLabel.color = 0xFFFFFF;
			_sendDataLabel.x = 20;
			_sendDataLabel.y = AbstractTableView.ROW_HEIGHT + _navigateLabel.y + 3;
			addChild(_sendDataLabel);
			
			_newWindowLabel = new LightLabel();
			_newWindowLabel.text = "New window";
			_newWindowLabel.color = 0xFFFFFF;
			_newWindowLabel.x = 20;
			_newWindowLabel.y = AbstractTableView.ROW_HEIGHT + _sendDataLabel.y + 5;
			addChild(_newWindowLabel);
			
			//Add property portViews
			_urlPortView = new PortView(table.anchors["theURL"], context as WorldContainer, true);
			_urlPortView.angle = 180;
			_urlPortView.y = _urlLabel.y;
			addChild(_urlPortView);
			
			_queryPortView = new PortView(table.anchors["queryString"], context as WorldContainer, true);
			_queryPortView.angle = 180;
			_queryPortView.y = _queryLabel.y;
			addChild(_queryPortView);
			
			_navigatePortView = new PortView(table.anchors["navigate"], context as WorldContainer, true);
			_navigatePortView.angle = 180;
			_navigatePortView.y = _navigateLabel.y;
			addChild(_navigatePortView);
			
			_sendDataPortView = new PortView(table.anchors["postData"], context as WorldContainer, true);
			_sendDataPortView.angle = 180;
			_sendDataPortView.y = _sendDataLabel.y;
			addChild(_sendDataPortView);
			
			_newWindowPortView = new PortView(table.anchors["newWindow"], context as WorldContainer, true);
			_newWindowPortView.angle = 180;
			_newWindowPortView.y = _newWindowLabel.y;
			addChild(_newWindowPortView);
			
			// Add value containers
			_urlRibbonControl.x = 100;
			_urlRibbonControl.y = _urlLabel.y + 2;
			_urlRibbonControl.setValue((model.value as GotoURL).theURL, Modifiers.STRING_TYPE);
			addChild(_urlRibbonControl);
			
			_queryRibbonControl.x = 100;
			_queryRibbonControl.y = _queryLabel.y + 2;
			_queryRibbonControl.setValue((model.value as GotoURL).queryString, Modifiers.STRING_TYPE);
			addChild(_queryRibbonControl);
			
			_toggleDotsBg.graphics.beginFill(0x8D8D8D);
			_toggleDotsBg.graphics.drawRoundRect(0,0,11,30,10,10);
			_toggleDotsBg.graphics.endFill();
			_toggleDotsBg.x = 100;
			_toggleDotsBg.y = _navigateLabel.y + 3;
			addChild(_toggleDotsBg);
			
			_navigateToggle = getToggleSprite();
			_navigateToggle.x = 8.5;
			_navigateToggle.y = 9.5;
			_toggleDotsBg.addChild(_navigateToggle);
			
			_sendDataToggle = getToggleSprite(AbstractTableView.GRAY_4D);
			_sendDataToggle.x = 8.5;
			_sendDataToggle.y = 25.5;
			_toggleDotsBg.addChild(_sendDataToggle);
			
			
			_newWindowRibbonControl.x = 24; //100 - 76
			_newWindowRibbonControl.y = _newWindowLabel.y + 3;
			(_newWindowRibbonControl as BooleanSwitch).setValue((model.value as GotoURL).newWindow, RibbonView.INIT);
			addChild(_newWindowRibbonControl);
			
			if((model.value as GotoURL).jump){
				handleNavigateClick(null);
			}else{
				handleSendDataClick(null);
			}
			
			// Add row lines
			addChild(getLine(AbstractTableView.ROW_HEIGHT + _queryLabel.y + 3));
			addChild(getLine(AbstractTableView.ROW_HEIGHT + _urlLabel.y + 3))
			//No line between "Jump" and "Send Data"
			addChild(getLine(AbstractTableView.ROW_HEIGHT + _sendDataLabel.y + 3));
			addChild(getLine(AbstractTableView.ROW_HEIGHT + _newWindowLabel.y + 3));
			
			// Hot spots
			var navigateClick:Sprite = new Sprite();
			navigateClick.graphics.beginFill(0xFF0000, .01);
			navigateClick.graphics.drawRect(0, 0, 184, AbstractTableView.ROW_HEIGHT + 1);
			navigateClick.graphics.endFill();
			navigateClick.x = _navigateLabel.x;
			navigateClick.y = _navigatePortView.y + 2;
			navigateClick.addEventListener(MouseEvent.CLICK, handleNavigateClick);
			addChild(navigateClick);
			
			var sendDataClick:Sprite = new Sprite();
			sendDataClick.graphics.beginFill(0xFF0000, .01);
			sendDataClick.graphics.drawRect(0, 0, 184, AbstractTableView.ROW_HEIGHT + 1);
			sendDataClick.graphics.endFill();
			sendDataClick.x = _sendDataLabel.x;
			sendDataClick.y = _sendDataPortView.y + 1;
			sendDataClick.addEventListener(MouseEvent.CLICK, handleSendDataClick);
			addChild(sendDataClick);
			
			// Add Go Inlet
			_gotoURLInlet = new PortView(table.anchors["gotoURL"], context as WorldContainer, true);
			_gotoURLInlet.y = 120;
			_gotoURLInlet.x = 20;
			_gotoURLInlet.angle = 90;
			addChild(_gotoURLInlet);
		}
		
		protected function handleNavigateClick(event:MouseEvent):void{
			_navigateToggle = getToggleSprite();
			_navigateToggle.x = 8.5;
			_navigateToggle.y = 9.5;
			_toggleDotsBg.addChild(_navigateToggle);
			(table as GotoURL).jump = true;
			
			_sendDataToggle = getToggleSprite(AbstractTableView.GRAY_4D);
			_sendDataToggle.x = 8.5;
			_sendDataToggle.y = 25.5;
			_toggleDotsBg.addChild(_sendDataToggle);
			
			_newWindowRibbonControl.alpha = 1;
			_newWindowLabel.alpha = 1;
		}
		
		protected function handleSendDataClick(event:MouseEvent):void{
			_navigateToggle = getToggleSprite(AbstractTableView.GRAY_4D);
			_navigateToggle.x = 8.5;
			_navigateToggle.y = 9.5;
			_toggleDotsBg.addChild(_navigateToggle);
			(table as GotoURL).jump = false;
			
			_sendDataToggle = getToggleSprite();
			_sendDataToggle.x = 8.5;
			_sendDataToggle.y = 25.5;
			_toggleDotsBg.addChild(_sendDataToggle);
			
			_newWindowRibbonControl.alpha = .4;
			_newWindowLabel.alpha = .4;
		}
		
		protected function setValueURLCallback(value:Object):void
		{
			(table as GotoURL).theURL = Modifiers.convertValue(Modifiers.STRING_TYPE, value) as String;
		}
		
		protected function setValueQueryCallback(value:Object):void
		{
			(table as GotoURL).queryString = Modifiers.convertValue(Modifiers.STRING_TYPE, value) as String;
		}
		
		protected function setValueWinCallback(value:Object):void
		{
			(table as GotoURL).newWindow = Modifiers.convertValue(Modifiers.BOOLEAN_TYPE, value) as Boolean;
		}
		
		private function getLine(yPos:Number):Sprite{
			var line:Sprite = new Sprite();
			line.graphics.lineStyle(1,0);
			line.graphics.moveTo(4,0);
			line.graphics.lineTo(204,0);
			line.y = yPos;
			return line;
		}
		
		private function getToggleSprite(color:uint = 0xFFFFFF):Sprite{
			var toggle:Sprite = new Sprite();
			toggle.graphics.beginFill(color);
			toggle.graphics.drawCircle(-3, -3, 3);
			return toggle;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean 
		{
			var handled:Boolean = super.updateModelProperty(property, oldValue, newValue);
			if(table.loading) {
				return handled;
			}
			switch(property) {
				case "poweredOnRun":
					if(table.poweredOnRun){
						_powerOnPortView.powerColor = 0xFFE600;
						_powerOffPortView.powerColor = 0xBFBFBF;
					}else{
						_powerOffPortView.powerColor = 0xFFE600;
						_powerOnPortView.powerColor = 0xBFBFBF;
					}
					break;
				case "jump":
					if(newValue){
						handleNavigateClick(null);
					}else{
						handleSendDataClick(null);
					}
					break;
				case "theURL":
					_urlRibbonControl.setValue(newValue, Modifiers.STRING_TYPE);
					break;
				case "queryString":
					_queryRibbonControl.setValue(newValue, Modifiers.STRING_TYPE);
					break;
				case "newWindow":
					_newWindowRibbonControl.setValue(newValue, Modifiers.BOOLEAN_TYPE);
					break;
				case "loading":
					setupTable();
					break;
			}
			return handled;
		}
		
		override protected function addPowerPorts():Sprite{
			var powerPorts:Sprite = new Sprite();
			
			powerPorts.graphics.beginFill(0x9A9A9A);
			powerPorts.graphics.drawRoundRect(0,0,11,30,10,10);
			powerPorts.graphics.endFill();
			
			_powerOffPortView = new PortView(table.anchors["powerOff"], context as WorldContainer, true, -1, 90);
			_powerOffPortView.x = -3.75;
			_powerOffPortView.y = 0;
			_powerOffPortView.angle = -90;
			_powerOffPortView.powerColor = 0xBFBFBF;
			powerPorts.addChild(_powerOffPortView);
			
			_powerOnPortView = new PortView(table.anchors["powerOn"], context as WorldContainer, true, -1, 90);
			_powerOnPortView.x = -3;
			_powerOnPortView.y = 17;
			_powerOnPortView.angle = -90;
			_powerOnPortView.powerColor = 0xFFE600;
			powerPorts.addChild(_powerOnPortView);
			
			return powerPorts;
		}
		
		public function get table():GotoURL{
			return model.value as GotoURL;
		}
	}
}
