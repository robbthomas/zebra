package com.alleni.author.view.objects
{
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.view.GraphicFillView;
	import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.GradientType;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class OvalView extends ObjectView
	{
		private var _shine:Sprite;
		
		public function OvalView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
		}
		
		
		override protected function setupFill():void
		{
			// force oval jellybean to have radial gradient fill, not linear
			if (object.fillSpec.type == GraphicFill.JELLYBEAN_FILL) {
				var fill:GraphicFill = object.fillSpec.clone();
				fill.linear = false;
				GraphicFillView.beginGraphicFill(graphics, fill, object.width, object.height, object.left, object.top,
					adjustedFillAlpha(), context.info.testView);
			} else {
				super.setupFill();
			}
		}
				
		override protected function draw():void
		{
			graphics.drawEllipse(object.left, object.top, object.width, object.height);
		}
		
		override public function render():void
		{
			// draw the base ellipse with gradient
			super.render();
			
			// update the shine, if any
			if (object.fillSpec.type == GraphicFill.JELLYBEAN_FILL) {
				if (!_shine) {
					_shine = new Sprite();
					addChild(_shine);
				}
				updateShine(object.fillSpec);
			} else {
				if (_shine) {
					removeChild(_shine);
					_shine = null;
				}
			}

            var b:Rectangle = this.getBounds(_context);
//            trace("Oval:render: ", this, "bounds="+b);
		}
		
		private function updateShine(fill:GraphicFill):void
		{
			_shine.graphics.clear();
			
			const SHINE_DY:Number = 0.05;  // location of top of shine as fraction of height
			const SHINE_X_FACTOR:Number = 0.75;  // width of shine as fraction of obj width
			const SHINE_Y_FACTOR:Number = 0.60;  // height of shine as fraction of obj height
			
			var dy:Number = object.height * SHINE_DY;
			var shineW:Number = object.width * SHINE_X_FACTOR;
			var shineH:Number = object.height * SHINE_Y_FACTOR;
			
			var shineDegrees:Number = (fill.flipGlare) ? -90 : 90;
			var shineColors:Array = [0xFFFFFF, 0xFFFFFF];
			var shineAlphas:Array = [0.95, 0];
			var shineRatios:Array = [0, 255];
			var shineMatrix:Matrix = new Matrix();
			shineMatrix.createGradientBox(shineW, shineH, shineDegrees * Math.PI/180, 0, 0);
			_shine.graphics.beginGradientFill(GradientType.LINEAR, shineColors, shineAlphas, shineRatios, shineMatrix, SpreadMethod.PAD );
			_shine.graphics.drawEllipse(0, 0, shineW, shineH);
			_shine.graphics.endFill();
			_shine.x = object.left + (object.width - shineW) / 2;
			_shine.y = object.top + dy;

		}
		
		/**
		 *  @inheritDoc
		 * 
		 */
		override public function getSVG(localToTarget:Function):XML
		{
			var result:XML = <ellipse  />;
			
			var cx:Number = object.left + (object.width/2);
			var cy:Number = object.top + (object.height/2);
			var center:Point = localToTarget(this, new Point(cx, cy));
			result.@cx = center.x;
			result.@cy = center.y;
			
			result.@rx = object.width/2;
			result.@ry = object.height/2;

			setCommonSVGAttributes(result, object.rotation, center);  // rotate about center point
			
			return result;
		}
	}
}