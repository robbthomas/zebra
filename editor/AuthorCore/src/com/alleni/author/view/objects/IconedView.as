package com.alleni.author.view.objects
{
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.Thumbnails;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.definition.application.ObjectIcons;
	import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	
	public class IconedView extends ObjectView
	{
		protected var _icon:DisplayObject;
		
		function IconedView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			// if we are to contain an asset, being an iconed type, we need
			// the default asset type to get its icon
			var assetTypeToContain:int = AssetType.getTypeForName(object.shortClassName);
			if (assetTypeToContain > -1)
				this.icon = com.alleni.author.definition.application.ObjectIcons.getByType(assetTypeToContain);
			this.mouseChildren = false;
		}
		
		// the value we get is the art 
		public function set icon(value:DisplayObject):void
		{
			if (_icon != null && this.contains(_icon))
				removeChild(_icon);
			
			if (value != null && _role == ViewRoles.PRIMARY) {
				_icon = value;
				_icon.x = object.left;
				_icon.y = object.top;
				addIcon();
			}
		}
		
		// separate method so views can override if they wish to add the icon to a different container
		protected function addIcon():void
		{
			this.addChild(_icon);
		}
		
		override protected function updateStatus():void
		{
			super.updateStatus();
			if (selected)
				_icon.filters = [new GlowFilter(0xFFFF80, 1.0, 3.0, 3.0, 2, 3, true)];
			else
				_icon.filters = [];
		}
	
		override protected function updateThumbnail() : void
		{
			if (object.complete) {
				if (_thumbnail != null && this.contains(_thumbnail))
					this.removeChild(_thumbnail);
				
				if (_icon != null) {
					_thumbnail = Thumbnails.generate(_icon, _icon.width, _icon.height, object.thumbnailScale);
					this.addChild(_thumbnail);
				}
			}
		}
		
		override public function exactHitTest(stagePoint:Point):Boolean
		{
			return hitTestPoint(stagePoint.x, stagePoint.y, true);;
		}
	}
}
