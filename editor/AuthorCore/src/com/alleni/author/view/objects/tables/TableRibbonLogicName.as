/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.objects.tables
{
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.action.ModifyObjectProperty;
import com.alleni.author.model.objects.AbstractTable;

	public class TableRibbonLogicName extends TableRibbonName
	{
		public function TableRibbonLogicName(table:AbstractTable, index:int)
		{
			super(table, index);
		}


		override public function set myValueFromControl(val:Object):void 
		{
            var oldValue:String = _table.logicRowName[_index];
            if(oldValue == val as String) {
                return;
            }
			_table.logicRowName[_index] = val as String;
            _table.recordInitialValue("logicRowName");
            ApplicationController.currentActionTree.commit(ModifyObjectProperty.fromObject(_table, "set table logic name", "logicRowName", _index, oldValue));
		}
		
		override public function get myValueFromControl():Object 
		{
			var result:Object = _table.logicRowName[_index];
			if(result == null || result == "") {
				result = super.myValueFromControl;
			}
			return result;
		}
	}
}