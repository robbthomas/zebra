package com.alleni.author.view.objects.tables
{
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.view.TruthTableScrollView;
	import com.alleni.author.view.ui.HorizontalScrollContainer;
	
	import flash.display.Sprite;
	
	public class TableBranchingRow extends HorizontalScrollContainer
	{
		private var _table:AbstractTable;
		private var _container:Sprite;
		
		public function TableBranchingRow(table:AbstractTable, scrollController:TruthTableScrollView)
		{
			_container = new Sprite();
			
			super(_container, scrollController, AbstractTableView.GRAY_4D);
			
			_table = table;
			
			this.height = TableLayoutManager.BRANCHING_SECTION_HEIGHT;
		}
		
		private var _cells:Array = [];
		
		public function addColumn(columns:int):void
		{
			for (var n:int=0; n<columns; n++) {
				if(_table.branches.length == n){
					_table.branches.addItem(_table.branchingDefault);
				}
				var cell:TableBranchingCell = TableBranchingCell(_cells[n]);
				if (cell == null) {
					_cells.push(addNewCell(n));
				}
			}
			
		}
		
		public function handleRearranged():void{
			clearBranches();
			addColumn(_table.numColumns);
		}

        public function refreshBranching():void{
            for (var n:int=0; n<_table.numColumns; n++) {
				var cell:TableBranchingCell = TableBranchingCell(_cells[n]);
				if (cell != null) {
					cell.updateIcon();
				}
			}
        }

		private function clearBranches():void{
			for each(var cell:TableBranchingCell in _cells){
				_container.removeChild(cell);
			}
			_cells = [];
		}
		
		private function addNewCell(column:int):TableBranchingCell
		{
			var cell:TableBranchingCell = new TableBranchingCell(_table, 0, column, column+1==_table.column);
			cell.x = column*AbstractTableView.COLUMN_WIDTH;
			_container.addChild(cell);
			
			return cell;
		}
	}
}