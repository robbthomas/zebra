package com.alleni.author.view.objects.tables
{
	import com.alleni.author.controller.objects.TableHeaderDragMediator;
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ui.AbstractRibbonControl;
	import com.alleni.author.view.ui.AbstractSwitch;
	import com.alleni.author.view.ui.BooleanSwitch;
import com.alleni.author.view.ui.RibbonValueField;
import com.alleni.author.view.ui.RibbonValueField;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.author.view.ui.editors.TransformationEditor;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import flashx.textLayout.formats.TextAlign;

	public class TableCell extends Sprite
	{			
		protected const CELL_TEXT_COLOR:uint = 0x000000;
		
		protected var _table:AbstractTable;
		protected var _grid:TableGrid;
		protected var _row:int;
		protected var _column:int;
		protected var _highlighted:Boolean;
		protected var _color:uint = fillColor;
		protected var _editor:AbstractRibbonControl;

		protected var _defaultColor:uint = AbstractTableView.GRAY_4D;
		protected var _border:Boolean = true;
		protected var _highlightColor:uint = 0xFFFFFF;
		protected var _value:Object = "";
		protected var _inviteSprite:Sprite;
		
		private var _editorMediator:TextEditMediator;
		
		public function TableCell(table:AbstractTable, grid:TableGrid, row:int, column:int, highlighted:Boolean, cellColor:int=-1, border:Boolean=true)
		{
			super();
			
			_table = table;
			_grid = grid;
			_row = row;
			_column = column;
			
			if (cellColor > -1)
				_defaultColor = cellColor;

			_border = border;
			
			if(grid != null && grid.columns < 0) {
				// zero column cant be edited
				this.mouseChildren = false;

				// and is dragable
				new TableHeaderDragMediator(this, grid.tableView, dragSection, row, selectRow);
				buildRollover()
			}

			_highlighted = highlighted;
			
			addLabel();
			draw();
		}
		
		public function selectRow(event:Event):void
		{
			_grid.tableView.setCurrentRow(_row, dragSection);
		}
		
		public function updateEvents():void
		{
			if(_grid.rows > 1 && _grid.columns == -1){
				addEventListener(MouseEvent.ROLL_OVER, handleRollOver);
				addEventListener(MouseEvent.ROLL_OUT, handleRollOut);
				buildRollover();
			}
		}
		
		private function setValueCallback(val:Object):void
		{
			if(_editor as BooleanSwitch) {
				Modifiers.convertValue(Modifiers.BOOLEAN_TYPE, val);
			}
			value = val;
		}
		
		public function set value(val:Object):void
		{
			// Overridden by subclasses.
		}

		public function updateValue(value:Object):void 
		{
			_value = value;
			
			if (_editor == null)
				return;
			
			if (_editor as TransformationEditor) {
				var container:AbstractObject = _table.parent;
				if (container) {
					TransformationEditor(_editor).parentView = container.getView(Application.instance.viewContext);
					TransformationEditor(_editor).transformation = _value as PositionAndAngle;
				}
			}
			else {
				_editor.setValue(_value, RibbonView.DEFAULT);	
			}
		}

		protected function addLabel():void
		{
			if (_editor == null) {
				_editor = new RibbonValueField(setValueCallback, AbstractTableView.COLUMN_WIDTH, AbstractTableView.ROW_HEIGHT, false);
				RibbonValueField(_editor).color = 0x000000;
				addChild(_editor);
			}
		}
		
		protected function draw():void
		{
			graphics.clear();
			if(this is TableHeadingCell || this is TableBranchingCell){
				graphics.lineStyle(1, 0x000000);
			}else{
				graphics.lineStyle(1, 0x646464, _border?1:0);
			}
			graphics.beginFill(fillColor);
			graphics.drawRect(0,0,AbstractTableView.COLUMN_WIDTH, AbstractTableView.ROW_HEIGHT);
			graphics.endFill();
		}
		
		protected function get fillColor():uint
		{
			return _highlighted?_highlightColor:_defaultColor;
		}
		
		public function highlight():void
		{
			_highlighted = true;
			draw();
		}
		
		public function unhighlight():void
		{
			_highlighted = false;
			draw();
		}
		
		private function handleRollOver(event:Event):void
		{
			addChild(_inviteSprite);
		}
		
		private function handleRollOut(event:Event):void
		{
			removeChild(_inviteSprite);
		}
		
		private function buildRollover():void
		{
			_inviteSprite = new Sprite();
			_inviteSprite.graphics.beginFill(0xFFFFFF);
			_inviteSprite.graphics.drawRect(0,0,AbstractTableView.COLUMN_WIDTH - 2,AbstractTableView.COLUMN_HEADING_HEIGHT);
			
			if(_row > 0){
				var leftArrow:Sprite = drawArrow();
				leftArrow.rotation = -90;
				leftArrow.y = AbstractTableView.COLUMN_HEADING_HEIGHT/2;
				leftArrow.x = (leftArrow.width/2) + 5;
				_inviteSprite.addChild(leftArrow);
			}
			if(_row < _grid.rows - 1){
				var rightArrow:Sprite = drawArrow();
				rightArrow.rotation = 90;
				rightArrow.y = AbstractTableView.COLUMN_HEADING_HEIGHT/2;
				rightArrow.x = AbstractTableView.COLUMN_WIDTH - (rightArrow.width/2) - 7;
				_inviteSprite.addChild(rightArrow);
			}
			_inviteSprite.alpha = .5;
		}
		
		private function drawArrow():Sprite
		{
			var arrow:Sprite = new Sprite();
			arrow.graphics.beginFill(0x0000FF);
			arrow.graphics.moveTo(-1,-3);
			arrow.graphics.lineTo(3,0);
			arrow.graphics.lineTo(-1,3);
			arrow.graphics.endFill();
			return arrow;
		}

		public function setType(prop:PropertyDescription, value:*=null, textColor:uint = CELL_TEXT_COLOR):void
		{
			if(value != null) {
				_value = value;
			}
			_value = Modifiers.convertValue(prop.type, _value);
			
			if(_editor) {
				removeChild(_editor);
				_editor = null;
				_editorMediator = null;
			}
			
			_editor = AbstractRibbonControl.createControl(prop, setValueCallback, _table, _value, AbstractTableView.COLUMN_WIDTH, AbstractTableView.ROW_HEIGHT, false, textColor);
			updateValue(_value);
			addChild(_editor);
			
			if (_editor is AbstractSwitch) {
				_editor.x = -70;//PROPERTIES_WIDTH;
				_editor.y = 2;
			}else if(_editor is RibbonValueField){
				//(_editor as RibbonValueField).textAlign = TextAlign.CENTER;
				_editor.x = 0;
                if(this is TableTweenCell){
                    (_editor as RibbonValueField).expandable = false;
                }
			}else{
				_editor.x = 0;
			}
		}
		
		private function traceWidth(event:Event):void{
			trace(_editor.width);
		}
		
		public function focus():void
		{
			if (_editor is RibbonValueField) {
				RibbonValueField(_editor).openEditing();
			}
		}
		
		private function closeOnEnter(charCode:uint):int
		{	
			switch (charCode) {
				case Keyboard.ENTER:
					_grid.focusCell(_row+1, _column);;
					break;
				case Keyboard.RIGHT:
					_grid.focusCell(_row, _column+1);
					break;	
			}
			
			return TextEditMediator.REQ_NULL;
		}

		protected function get dragSection():String
		{
			return TableHeaderDragMediator.LOGIC_ROWS;
		}
	}
}