/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.objects.tables
{
	import com.alleni.author.application.ApplicationUI;
	import com.alleni.author.controller.text.TextEditMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.action.ModifyObjectProperty;
import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.AbstractRibbonControl;
	import com.alleni.author.view.ui.AlignSwitch;
	import com.alleni.author.view.ui.BooleanSwitch;
	import com.alleni.author.view.ui.RibbonColorPicker;
	import com.alleni.author.view.ui.RibbonDropDownMenu;
	import com.alleni.author.view.ui.RibbonFillPicker;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.author.view.ui.editors.TransformationEditor;

	public class TableRibbonLogicExpression extends TableRibbonExpression
	{
		public function TableRibbonLogicExpression(context:WorldContainer, table:AbstractTable, index:int)
		{
			super(context, table, index);
		}
		
		override protected function setValueComponent(prop:PropertyDescription, existing:Boolean):void
		{
            prop = new PropertyDescription("", "", "", "", "", 0, false, -1, prop.type,  prop.constraints, true);
            if(prop.type == Modifiers.LIST_TYPE) {
                prop.overrideType(Modifiers.OBJECT_TYPE);
            }
			var value:* = _table.portValue[_index];
			if(prop.type == Modifiers.BOOLEAN_TYPE && value == ""){
				value = "true";
			}
			_editor = AbstractRibbonControl.createControl(prop, setValueCallback, _table, value, width, AbstractTableView.ROW_HEIGHT, false, 0x000000);
			_editor.setValue(value, RibbonView.DEFAULT);
			addChild(_editor);
			
			if (_editor is AlignSwitch) {
				_editor.x = -58;//PROPERTIES_WIDTH;
				_editor.y = 2;
			}
			else if (_editor is BooleanSwitch) {
				_editor.x = -74;//PROPERTIES_WIDTH;
				_editor.y = 2;
			}
			else {
				_editor.x = 0;	
			}
		}
		
		override public function handleWireAdded(title:String, prop:PropertyDescription, existing:Boolean):void
		{
			if(!existing) {
				var val:* = _table.port[_index];
				if(val === false) {
					val = true;
				}
				if(Modifiers.isNumericType(prop.type)) {
					val = "# = " + val;
				} else if(prop.type == Modifiers.BOOLEAN_TYPE) {
                    val = true;
                }
				_table.portValue[_index] = val;
			}

			super.handleWireAdded(title, prop, existing);
		}

        override public function handleWireRemoved(existing:Boolean):void {
            super.handleWireRemoved(existing);
            if(!existing) {
                _table.port[_index] = "";
            }
        }

        override public function get myValueFromControl():Object
		{
			return _table.portValue[_index];
		}
		
		override protected function setValueCallback(value:Object):void
		{
			super.setValueCallback(value);

            var oldValue:Object = _table.portValue[_index];
            if(oldValue === value) {
                return;
            }
			_table.portValue[_index] = value;
            _table.recordInitialValue("portValue");
            ApplicationController.currentActionTree.commit(ModifyObjectProperty.fromObject(_table, "set table logic expression", "portValue", _index, oldValue));
		}
	}
}