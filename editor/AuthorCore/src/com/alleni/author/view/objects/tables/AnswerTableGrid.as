package com.alleni.author.view.objects.tables
{
	import com.alleni.author.controller.objects.TableHeaderDragMediator;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.view.objects.tables.AbstractTableView;
	
	import mx.collections.IList;

	public class AnswerTableGrid extends TableGrid
	{
		public function AnswerTableGrid(table:AbstractTable, tableView:AbstractTableView, columns:int, rows:int, initialValues:IList)
		{
			super(table, tableView, columns, rows, initialValues, TableHeaderDragMediator.LOGIC_ROWS);
		}
		
		override protected function createCell(row:int, column:int, highlighted:Boolean):TableCell
		{
			return new AnswerTableCell(_table as AnswerTable, this, row, column, highlighted);
		}
		
		public function satisfyCell(satisfy:Boolean, row:int, column:int):void
		{
			try{
				(_rowArray[row][column] as AnswerTableCell).satisfied = satisfy;
			}catch(e:Error){
				
			}
		}
	}
}