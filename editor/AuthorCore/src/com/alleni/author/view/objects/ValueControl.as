package com.alleni.author.view.objects
{
	public class ValueControl
	{
		private var _o:Object;
		private var _property:String;
		private var _index:int;
		
		public function ValueControl(o:Object=null, property:String=null, index:int=-1)
		{
			_o = o;
			_property = property;
			_index = index;
		}
	
		public function get myValueFromControl():Object
		{
			if (_o && _property) {
				if (_index < 0) {
					return _o[_property];
				} else {
					return _o[_property][_index];
				}
			} else {
				return null;
			}
		}
	
		public function set myValueFromControl(val:Object):void
		{	
			if(_o && _property) {
				if(val == "true") val = true;
				else if (val == "false") val = false;
				if (_index < 0) {
					_o[_property] = val;
				} else {
					_o[_property][_index] = val;
				}
			}
		}
	}
}