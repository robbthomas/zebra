package com.alleni.author.view.objects
{
import com.alleni.author.Navigation.SmartObjectView;
import com.alleni.author.controller.objects.CompositeMediator;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.view.feedback.CompositeFeedback;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ITaconiteView;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.geom.Point;
import flash.geom.Rectangle;

public class CompositeView extends SmartObjectView
	{
		public var velumView:CompositeVelumView;
		
		private var _mediator:CompositeMediator;  // not a DragMediator ... its mainly for editing
		private var _composite:Composite;


		public function CompositeView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			_composite = object as Composite;
			_mediator = new CompositeMediator(context, this);
		}
		
        public function openPublishingDialog():void{
            if(_feedback != null){
                (_feedback as CompositeFeedback).mediator.prepPublishNameData(1);
            }
        }

		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
		{
			//trace("updateModelProperty prop="+property, "val="+newValue,  "scaleX="+this.scaleX);

			var result:Boolean = super.updateModelProperty(property,oldValue,newValue);
			
			switch (property) {
				case "editing":
				case "draggingChild":
					if (_composite.editing || _composite.draggingChild) {
						super.rolloverGlow = false;   // prevent ghosts by disabling composite rollover glow while editing
					}
					break;
			}
			return result;
		}

		override public function warnViewToDelete():void
		{
			super.warnViewToDelete();
			if (_mediator)
				_mediator.clearViewEvents();
		}

        override public function hitTestPoint(x:Number, y:Number, shapeFlag:Boolean = false):Boolean {
            if(velumView) {
                return velumView.hitTestPoint(x,  y, shapeFlag);
            } else {
                return super.hitTestPoint(x, y, shapeFlag);
            }
        }

        override public function exactHitTest(stagePoint:Point):Boolean {
            if(velumView) {
                return velumView.hitTestPoint(stagePoint.x, stagePoint.y);
            } else {
                return super.exactHitTest(stagePoint);
            }
        }

        override public function hitTestObject(other:DisplayObject):Boolean
		{
			if (object.editing)
				return false;  // while editing gadget, marquee on world should not select it
			
			var bounds:Rectangle = getBounds(other);
			var validBounds:Boolean = (bounds.width > 0 && bounds.height > 0);
			if (!validBounds) {
				return other.hitTestPoint(stagePosition.x, stagePosition.y);
			}
			if (!bounds.intersects(other.getBounds(other))) // ensure that children (clipped) will not register hit outside the clipping area
				return false;
			
			var i:int;
			var length:int = childViewContainer.numChildren;
			for (i = 0; i < length; i++) {
				var objView:DisplayObject = childViewContainer.getChildAt(i);
				if (objView.hitTestObject(other))
					return true;
			}
			return false;
		}
				
		public function hideVelum():void
		{
			_mediator.deleteVelumView();
		}
		
		override protected function updateStatus():void
		{
			if (!object.editing) {
				super.updateStatus();
			} else {
				if (_feedback) {
					(_feedback as DisplayObject).visible = false;
				}
				return;
			}
		}
		
		override public function computeMessageCenterPosition():Point
		{
			// when velum is showing & MC is docked...  place the MC below the velum
			if (velumView && !object.messageCenterRepositioned && messageCenterView) {  
				var uiLayer:DisplayObjectContainer = messageCenterView.parent;
				var bounds:Rectangle = velumView.getBounds(uiLayer);  // get the velum bounds in context of UI layer
				var result:Point = new Point(bounds.left+bounds.width/2, bounds.bottom);
				return result;
			} else {
				return super.computeMessageCenterPosition();
			}
		}
		
		override protected function ObjectFeedbackFactory():ITaconiteView
		{
			return new CompositeFeedback(context, model, this);
		}

		override public function openEditing():void
		{
			_mediator.openEditingThis();
		}

		override protected function set rolloverGlow(value:Boolean):void
		{
			// prevent ghosts by disabling composite rollover glow while editing, or while dragging child with XYR wired to edge
			if (!_composite.editing && !_composite.draggingChild)
				super.rolloverGlow = value;  
		}

	}
}
