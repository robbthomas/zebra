/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 */
package com.alleni.author.view.objects
{
	import com.alleni.author.controller.objects.SliderMediator;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.objects.Slider;
	import com.alleni.author.view.GraphicFillView;
	import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.TaconiteGrip;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Bitmap;
	import flash.display.PixelSnapping;
	import flash.filters.GlowFilter;
	
	import mx.events.PropertyChangeEvent;

	/**
	 * View of a Slider. 
	 */
	public class SliderView extends ObjectView
	{
	  	private var _knob:TaconiteGrip;
		private var _knobBitmap:Bitmap;
		private var _mediator:SliderMediator;

	  	private static const KNOB_FRAC:Number = 0.3;  // fraction of width
	  	private static const KNOB_MAX:Number = 50;
	  	public static const INDENT_FRAC:Number = 0.1; // fraction of height
	  	private static const TRACK_WIDTH:Number = 4;

	  	private var _scaleBottom:Number = 0;
	  	private var _scaleTop:Number = 1;

		public function SliderView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			// knob is present even in diagram view, for thumbnail
			_knob = new TaconiteGrip();
			addChild(_knob);
			
			// this mediator is for moving the knob (not for dragging the object)
			if (role == ViewRoles.PRIMARY) {
				_mediator = new SliderMediator(context);
				_mediator.handleViewEvents(this,_knob,role);
			}
		}
		
		override public function warnViewToDelete():void
		{
			super.warnViewToDelete();
			
			if (_mediator)
				_mediator.clearViewEvents();
			
			if (slider.sliderKnobGraphic)
				slider.sliderKnobGraphic.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, knobAssetListener);
		}

		/**
		 * The Slider model associated with this view.
		 */
		public function get slider():Slider
		{
			return model.value as Slider;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{
			switch (property)
			{
				case "sliderValue":
					updateKnobPosition();
					if (_role == ViewRoles.DIAGRAM)
						updateThumbnail();
					return true;
					
				case "sliderKnobGraphic":
					if (slider.sliderKnobGraphic)
						slider.sliderKnobGraphic.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, knobAssetListener);
					return false;
			}
			return super.updateModelProperty(property,oldValue,newValue);
		}
		
		
		private function knobAssetListener(event:PropertyChangeEvent):void
		{
			if (event.property == "content")
				updateView();
		}
		
		private function updateKnobPosition():void
		{
			_knob.x = slider.left + slider.width/2 - _knob.width / 2;
			_knob.y = _mediator.getKnobY(slider) - _knob.height / 2;  // controller gives ctr of knob
		}


		/**
		 * Update this view by drawing the appropriate graphics.
		 */
		override protected function updateEnabledView():void
		{
			super.updateEnabledView();
			if (object.width <= 0.0 ||  object.height <= 0.0) {
				return;
			}
			
			// Transparent background to catch mouse events for dragging
			graphics.lineStyle(0,0,0);
			graphics.beginFill(0, 0.001);
			graphics.drawRect(slider.left,slider.top,slider.width,slider.height);
			graphics.endFill();
			
			rotation = slider.rotation;
			
			var trackLeft:Number = slider.left + slider.width/2-TRACK_WIDTH/2;
			var trackTop:Number = slider.top + INDENT_FRAC*slider.height;
			var trackHeight:Number = ((1-INDENT_FRAC)*slider.height)-(INDENT_FRAC*slider.height);
			var trackFill:GraphicFill = slider.fillSpec;
			GraphicFillView.beginGraphicFill(graphics, trackFill, TRACK_WIDTH, trackHeight, trackLeft,trackTop, slider.fillAlpha/100, context.info.testView);
			graphics.lineStyle(slider.lineThickness, slider.lineColor, slider.lineAlpha/100);
			graphics.drawRoundRect(trackLeft, trackTop, TRACK_WIDTH, trackHeight, 2, 2);
			graphics.endFill();

			// knob is a separate sprite
			if (slider.sliderKnobGraphic && slider.sliderKnobGraphic.content as Bitmap) {
				_knob.graphics.clear();
				_knob.filters = null;
				if (_knobBitmap)
					_knob.removeChild(_knobBitmap);	
				_knobBitmap = new Bitmap((slider.sliderKnobGraphic.content as Bitmap).bitmapData);
				_knobBitmap.smoothing = true;
				_knobBitmap.alpha = slider.knobAlpha/100;
				_knob.addChild(_knobBitmap);
			} else {
				if (_knobBitmap) {
					_knob.removeChild(_knobBitmap);
					_knobBitmap = null;
				}

				var knobWidth:int = KNOB_MAX;
				if (slider.width*KNOB_FRAC < KNOB_MAX)
					knobWidth = slider.width*KNOB_FRAC;
	
				var knobFill:GraphicFill = slider.knobFill;
		 		_knob.graphics.clear();
				_knob.graphics.lineStyle(1,0,1);
				GraphicFillView.beginGraphicFill(_knob.graphics, knobFill, knobWidth, knobWidth, 0,0, slider.knobAlpha/100, context.info.testView);
				_knob.graphics.drawEllipse(0,0,knobWidth,knobWidth);
				_knob.graphics.endFill();
			
				// Outer Glow
				if (context.info.testView)
					_knob.filters = null;
				else
					_knob.filters = [new GlowFilter(0)];
			}
			
			updateKnobPosition();
		}
		
		override protected function updateThumbnail():void
		{
			if (object.complete) {
				super.updateThumbnail();
				
				_knob.graphics.clear();
				this.graphics.clear();
				this.x = object.xDiagram;
				this.y = object.yDiagram;
			}
		}

	}
}
