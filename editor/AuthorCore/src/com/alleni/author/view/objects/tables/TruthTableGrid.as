package com.alleni.author.view.objects.tables
{
	import com.alleni.author.controller.objects.TableHeaderDragMediator;
	import com.alleni.author.model.objects.AbstractTable;

import mx.collections.IList;

public class TruthTableGrid extends TableGrid
	{	
		
		public function TruthTableGrid(table:AbstractTable, tableView:AbstractTableView, columns:int, rows:int, initialValues:IList = null)
		{
			super(table, tableView, columns, rows, initialValues, TableHeaderDragMediator.LOGIC_ROWS);
		}
		

		override protected function createCell(row:int, column:int, highlighted:Boolean):TableCell
		{
			return new TruthTableCell(_table, this, row, column, highlighted);
		}
	}
}