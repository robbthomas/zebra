package com.alleni.author.view.objects.tables
{
	import flash.display.Sprite;
	
	public class TableSection extends Sprite
	{
		public var topGuide:int = -1;
		public var bottomGuide:int = -1;
		public var leftGuide:int = -1;
		public var rightGuide:int = -1;
		
		public var sectionName:String = "generic";
	
		private var _width:Number = 0;
		private var _height:Number = 0;
	
		public function TableSection()
		{
			super();
		}
	
		override public function get width():Number
		{
			return _width;
		}
	
		override public function set width(value:Number):void
		{
			_width = value;
		}
	
		override public function get height():Number
		{
			return _height;
		}
	
		override public function set height(value:Number):void
		{
			_height = value;
		}
	}
}