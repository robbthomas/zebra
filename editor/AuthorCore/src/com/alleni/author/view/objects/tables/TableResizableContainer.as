package com.alleni.author.view.objects.tables
{
	import com.alleni.author.view.ui.PortView;
	
	
	public class TableResizableContainer extends TableSection
	{
		protected var _width:Number;
		protected var _height:Number;

		
		/**
		 * Container to hold the resizable columns of truth tables and answer tables 
		 * @param viewContext
		 * @param width
		 * @param height
		 * 
		 */
		public function TableResizableContainer(width:Number, height:Number)
		{
			super();
						
			_width = width;
			_height = height;
		}

		override public function set width(value:Number):void
		{
			_width = value;
			
			for (var n:int=0; n<this.numChildren; n++) {
				if(!(this.getChildAt(n) is PortView)){
					this.getChildAt(n).width = value;
				}
			}
		}
		
		override public function get width():Number
		{
			return _width;
		}
		
		override public function set height(value:Number):void
		{
			_height = value;
		}
	}
}