package com.alleni.author.view.objects
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.GraphicFillView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.SelectableView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;

	public class VectorElementView extends SelectableView
	{
		public static var HIT_AREA_THICKNESS:Number = 5;
		public static var HIT_AREA_COLOR:uint = 0xFFFFFF;
		public static var HIT_AREA_ALPHA:Number = 0.01;
		
		public function VectorElementView(context:ViewContext, model:TaconiteModel)
		{
            super(context, model);
		}
		
		/**
		 * The object that this view presents.
		 */
		public function get object():AbstractObject
		{
			return model.value as AbstractObject;
		}
		
		/**
		 * Draws a small semi-transparent area along all lines so that the object is more easily selectable
		 * by clicking on those lines.
		 */
		protected function drawHitArea():void
		{
			if(object.lineThickness < HIT_AREA_THICKNESS && object.lineAlpha > 0 && object.lineThickness > 0) {
				this.rotation = object.rotation;
				graphics.lineStyle(HIT_AREA_THICKNESS, HIT_AREA_COLOR, HIT_AREA_ALPHA);
				draw();
			}
		}
		
		protected function setupCommonAttributes():void
		{
			this.rotation = object.rotation;
			
			if(object.fillAlpha > 0) setupFill();
			if (object.lineThickness > 0) this.graphics.lineStyle(object.lineThickness, object.lineColor, object.lineAlpha/100, false);
		}
		
		protected function setupFill():void
		{
			GraphicFillView.beginGraphicFill(graphics, object.fillSpec, object.width, object.height, object.left, object.top,
					adjustedFillAlpha(), context.info.testView);
		}
		
		protected function adjustedFillAlpha():Number
		{
			// returns a fraction 0..1
			// when object is cloaked, the whole sprite will be at alpha=0.01
			// so in that case we keep the fill-alpha high so a collision will still register
			var fillAlpha:Number = object.fillAlpha/100;
			if (object.cloakedNow && Application.running)
				fillAlpha = 1;  // 1.0 since obj.alpha = 0.01 and we want hit-test to work in middle of object
			return fillAlpha;
		}
		
		/**
		 * Update this object's disabled view.  This default implementation relies on the behavior of
		 * updateView to set the appropriate alpha on the Sprite  Subclasses of ObjectView can override
		 * this function to provide a more appropriate behavior.
		 */
		protected function updateDisabledView():void
		{
			updateEnabledView();
		}
		
		override protected function updateStatus():void
		{
			if (useSelectionTransform)
				transform.colorTransform = getColorTransform();
			
			// exception to temporarily show handles in the Player when object.runtimeRotation is true.
			if (!Application.runningLocked || object.runtimeRotation) { // only handle feedback in editor
				rolloverGlow = useRolloverGlow;
			
				// Manage creation/destruction of a specialized feedback view on a different layer.
				if (selected && _feedback == null) {
					_feedback = createFeedbackView();
					if (_feedback != null) {
						WorldContainer(context).feedbackHandlesView.addChild(_feedback as DisplayObject);
						_feedback.initialize();
					}
				}
				if (_feedback)
					(_feedback as DisplayObject).visible = selected;
			}
		}
		
		protected function removeFeedback():void
		{
			if (_feedback != null) {
                _feedback.warnViewToDelete();
				WorldContainer(context).feedbackHandlesView.removeChild(_feedback as DisplayObject);
				_feedback = null;
			}
		}
		
		
		/**
		 * Update this object's enabled view
		 */
		protected function updateEnabledView():void
		{
			// to be overridden by subclasses
			super.updateView();
			drawHitArea();
			setupCommonAttributes();
			draw();
			this.graphics.endFill();
			drawFrame();
		}
		
		/**
		 * object views can override this to draw their own frame 
		 * 
		 */
		protected function drawFrame():void
		{
			if (object.frameThickness > 0) {
				this.graphics.lineStyle(object.frameThickness, object.frameColor, object.frameAlpha, false);
				graphics.drawRect(0, 0, object.width, object.height);
			}
		}
		
		protected function draw():void
		{
			// override
		}
	}
}
