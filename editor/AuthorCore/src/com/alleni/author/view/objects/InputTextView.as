package com.alleni.author.view.objects
{
    import com.alleni.author.controller.objects.InputTextController;
    import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.DoubleClickAction;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.Thumbnails;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.model.objects.InputText;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.text.RichTextField;
    import com.alleni.taconite.event.TextEditEvent;
    import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	/**
	 * View of a text box which uses the Text Layout Framework to display rich, editable text.
	 */
	public class InputTextView extends ObjectView
	{
		private static const PROMPT_ALPHA:Number = 0.5;

		private var _text:RichTextField;
		private var _textEditor:TextEditMediator;
		
		
		public function InputTextView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			// the text itself
			_text = new RichTextField(text.width, text.height);
			_text.formattable = false;
			
			if (text.label != "")
				_text.text = text.label;  // copy text content from obj to rich text field
						
			if (role == ViewRoles.PRIMARY) {
				_textEditor = new TextEditMediator();
				_textEditor.handleTextEditEvents(_text, okEditFunc, handleTextFlowUpdate, keyPressed, closeEditFunc);
			}
			
			addChild(_text);
		}
		
		
		
		private function okEditFunc(event:MouseEvent):int
		{
			var action:int = DoubleClickAction.IGNORE;
			
			if (Application.running)  // single-click when running, double when paused
				action = DoubleClickAction.EDIT;
			else
				action = TextEditMediator.doubleClickToEdit(event);
			
			if (action == DoubleClickAction.EDIT) {
				stage.addEventListener(TextEvent.TEXT_INPUT, textInputListener, true);  // useCapture
			}

            if (text.interactive == false && Application.running)
                action = DoubleClickAction.IGNORE;
			
			return action;
		}
		
		private function closeEditFunc(text:String):void
		{
			stage.removeEventListener(TextEvent.TEXT_INPUT, textInputListener, true);  // useCapture
			flushBuffer();
		}
		
		
		private function textInputListener(event:TextEvent):void
		{
			var char:String = event.text;
			var isNumeric:Boolean = ((char>="0" && char<="9") || char=="." || char=="-");
			var rangeSelected:Boolean = (_text.selectionEnd > _text.selectionStart);
			if ((_text.text.length < text.maxCharCount || rangeSelected) && (isNumeric || !text.numbersOnly))
				return;
			
			event.stopImmediatePropagation();
		}
				
		
		/**
		 * The Text object associated with this view's TaconiteModel.
		 */
		public function get text():InputText
		{
			return model.value as InputText;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{
			super.updateModelProperty(property, oldValue, newValue);
			
			if (_role == ViewRoles.PRIMARY) {
				
				// Avoid application render mechanism when redrawing text
				// (because RichTextField will crash)
				switch (property) {
					case "loading":
						if (newValue == true)
							break;
						else
							; // drop thru to update text when loading is complete
					case "text":
						if (!_text.editing && _text.text != text.label) {
							_text.removeAllFlowElements();
							checkBufferForPromptFade(text.label);
							_text.text = text.label;
						}
						this.render();  // update now, instead of in application render handler
						//trace("text.text", text.text);
						if (_text.editing /* && Application.running */ && _text.text.length >= text.maxCharCount)
							ApplicationController.instance.wireController.sendTrigger(object, Modifiers.instance.fetch("hitMaxChars"));
						return true;
					case "height":
						this.render();
						return true;
					
				}
			}
			if (_role == ViewRoles.DIAGRAM) {
				switch(property) {
					case "text":
						if (_text.text != text.label)
							_text.text = text.label;
					case "height":
					case "fontColor":
					case "fontFamily":
					case "kerning":
					case "fontStyle":
						updateThumbnail();
						return true;
					default:
						break;
				}
			}
			return false;
		}
		
		
		/**
		 * Update this view by drawing the appropriate graphics.
		 */
		override protected function updateEnabledView():void
		{
			super.updateEnabledView();

			// sync text formatting
			_text.color = text.fontColor;
			_text.fontFamily = text.fontFamily;
			_text.kerning = text.kerning;
			_text.italic = text.italic;
			_text.bold = text.bold;
			_text.underline = text.underline;
			_text.fontAlpha = text.fontAlpha/100;
			
			var gap:Number = 5;  //provides separation of text from line
			
			var leftParam:Number = Math.max(text.lineThickness/2 + gap, text.margin);
			var topParam:Number = Math.max(text.lineThickness/2 + gap, text.margin);
			var rightParam:Number = Math.max(text.lineThickness/2 + gap , text.margin);
			var bottomParam:Number = Math.max(text.lineThickness/2 + gap, text.margin);
			
			_text.setPadding(leftParam,topParam,rightParam,bottomParam);
			
			
			_text.background = false;  // draw background ourselves, to ignore current size of text, and to allow gradients
			_text.border = false;
			
			if (_role == ViewRoles.PRIMARY) {
				_text.textAlign = text.textAlignH;
				_text.verticalAlign = text.textAlignV;
				_text.size = text.fontSize;
				
				_text.setCompositionSize(text.width, text.height);
				
				_text.x = object.left;
				_text.y = object.top;
			}
		}

        override protected function recalcVisibility():void {
            super.recalcVisibility();

            if (!text.interactive && !_textEditor.editing) {
                this.alpha = (text.disabledAlpha/100)*(text.alpha/100);
            } else {
                this.alpha = text.alpha/100;
            }
        }

		override protected function draw():void
		{
			graphics.drawRect(object.left, object.top, object.width, object.height);
		}

        override protected function initializeView():void {
            super.initializeView();
            _text.addEventListener(TextEditEvent.GOT_FOCUS, handleFocusChange);
            _text.addEventListener(TextEditEvent.LOST_FOCUS, handleFocusChange)
        }
        
        private function handleFocusChange(e:TextEditEvent):void {
            switch (e.type) {
                case TextEditEvent.GOT_FOCUS:
                    if (!text.interactive) this.alpha = text.alpha/100;
                    InputTextController.instance.textFocusChanged(model.value as InputText,true);
					validateBufferForPrompt();
                    break;
                case TextEditEvent.LOST_FOCUS:
                    if (!text.interactive) this.alpha = (text.disabledAlpha/100)*(text.alpha/100);
                    InputTextController.instance.textFocusChanged(model.value as InputText,false);
                    validateBufferForPrompt();
                    break;
            }
        }

        override public function warnViewToDelete():void {
            super.warnViewToDelete();
            _text.removeEventListener(TextEditEvent.GOT_FOCUS,handleFocusChange);
            _text.removeEventListener(TextEditEvent.LOST_FOCUS,handleFocusChange);
        }
		
		override protected function updateThumbnail() : void
		{
			if (object.complete) {
				updateEnabledView();
				_text.size = Math.round(14 * object.thumbnailScale);
				_text.setCompositionSize(text.width * object.thumbnailScale, Thumbnails.THUMB_NOMINAL_SIZE*object.thumbnailScale);
				_text.x = 5;
				// a little bit of tweaking to get the positioning right - can standardize at some point
				_text.y = Thumbnails.THUMB_NOMINAL_SIZE - 10 * object.thumbnailScale - 3;
			}
		}

		
		private function handleTextFlowUpdate(tx:RichTextField, value:String):void{
			if(_text.editing){
				if (text.submit == InputText.CONTINUOUSLY){
					text.label = value;
				}
			}
		}
	
		private function flushBuffer():void{
			text.label = _text.text;
		}
		
		override public function exactHitTest(stagePoint:Point):Boolean
		{
			return hitTestPoint(stagePoint.x, stagePoint.y);  // click anywhere in bounds will drag the text (relies on fill)
		}

		
		override public function interceptObjMouseDown(e:MouseEvent):Boolean
		{
			if (Application.running && _textEditor && text.interactive) {
				setTextFocus(true);  // click anywhere in the input object bounds to open editing
				return false;
			}
			return super.interceptObjMouseDown(e);
		}

		private function set fadeText(inValue:Boolean):void {
			if (inValue)
				_text.alpha = PROMPT_ALPHA;
			else
				_text.alpha = 1.0;
		}

		private function checkBufferForPromptFade(buffer:String):void {
			fadeText = (buffer == text.promptText);
		}

		private function validateBufferForPrompt():void {
			if (_text.text == text.promptText) {
				// prepare the field for entry
				_text.text = "";
				fadeText = false;
			} else if (_text.text.length == 0) {
				// restore the prompt if no text was entered or if the field was cleared
				_text.rawText = text.promptText;
				fadeText = true;
			} else { // do nothing
			}
		}

        public function setTextFocus(value:Boolean):void {
            if (_textEditor) {
                if (value) {
                    _textEditor.openEditing();
                    _textEditor.selectAll(true); // this is for consistent behavior.
                } else {
                    _textEditor.closeEditing(true);
                }
            }
        }

		public function keyPressed(charCode:uint):int
		{
			if (charCode == Keyboard.ENTER && text.submit == InputText.ON_ENTER ){
					this.flushBuffer();
                    ApplicationController.instance.wireController.sendTrigger(model.value, Modifiers.instance.fetch("entryMade"));
                    return TextEditMediator.REQ_CLOSE;
				}
		
			else if (charCode == Keyboard.TAB && text.submit == InputText.ON_TAB ){
					this.flushBuffer();
                    ApplicationController.instance.wireController.sendTrigger(model.value, Modifiers.instance.fetch("entryMade"));
					return TextEditMediator.REQ_CLOSE;
			}
			else if(charCode == Keyboard.ENTER){
					return TextEditMediator.REQ_CLOSE;   // why???
			}
			else {
                if (text.submit == InputText.CONTINUOUSLY)
                    ApplicationController.instance.wireController.sendTrigger(model.value, Modifiers.instance.fetch("entryMade"));
				return TextEditMediator.REQ_NULL;
            }
		}
		
		
	}
}
