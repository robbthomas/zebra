package com.alleni.author.view.objects
{
	import com.alleni.author.controller.objects.GadgetMediator;
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.DiagramMediator;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.model.objects.CalcSwitch;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	
	import mx.collections.IList;

	public class CalcSwitchView extends GadgetView
	{
		public static const TEXT_WIDTH:Number = 100;
		public static const PAD:Number = 3;
		public static const WIDTH:Number = TEXT_WIDTH+ RibbonView.HEIGHT*2 + PAD*2;
		
		private var _outputPorts:Array = new Array();//PortView
		private var _outputDefaultPort:PortView;
		private var _inputPort:PortView;
		private var _conditionBackground:Array = new Array();//Sprite
		
		private var _conditionFields:Array = new Array();//RichTextField
		private var _defaultTextField:RichTextField;
		
		// event mediators
		private var _mediator:GadgetMediator;
		private var _diagramMediator:DiagramMediator;
		
		public function CalcSwitchView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			if (role == ViewRoles.DIAGRAM) {
				_diagramMediator = new DiagramMediator(context);
				_diagramMediator.handleViewEvents(this, role);
			}
			
			_mediator = new GadgetMediator(context);
			_mediator.handleViewEvents(this, role);
			
			createPorts();  // needs the _mediator
			addComponents();  // must be done before updateView
			width = WIDTH;
		}
		
		override protected function initializeView() : void {
			for(var i:int=0; i<calc.condition.length; i++) {
				_conditionFields[i].text = calc.condition[i];
			}
		}
		
		/**
		 * Handle an array change by some incremental adjustment and return true,
		 * otherwise return false to reinitialize the view. 
		 */
		override protected function updateArrayProperty(property:String, modelEventKind:String, array:IList, index:int):Boolean
		{
			var field:RichTextField;
			var handled:Boolean = super.updateArrayProperty(property, modelEventKind, array, index);
			var anchorIndex:int = index;
			if(index == IList(calc[property]).length-1) {
				// adding at the end position, so actually insert after the current end
				// to preserve newly created wires
				anchorIndex++;
			}
			switch(modelEventKind) {
				case ModelEvent.ADD_ARRAY_ITEM:
					switch(property) {
						case "output":
							_outputPorts.splice(anchorIndex,0,new PortView(IList(calc.anchors["output"])[anchorIndex], context as WorldContainer));
							addChild(_outputPorts[anchorIndex]);
							ApplicationController.instance.wireController.requestRedrawForObject(calc);
							break;
						case "condition":
							addCondition(index, calc.condition[index]);
							_conditionFields[index].text = calc.condition[index];
							break;
					}
					break;
				case ModelEvent.REMOVE_ARRAY_ITEM:
					switch(property) {
						case "output":
							removeChild(_outputPorts[index]);
							_outputPorts.splice(index,1);
							ApplicationController.instance.wireController.requestRedrawForObject(calc);
							break;
						case "condition":
							removeChild(_conditionBackground[index]);
							removeChild(_conditionFields[index]);
							_conditionFields.splice(index,1);
							_conditionFields.splice(index,1);
							break;
					}
					break;
				case ModelEvent.CHANGE_ARRAY_ITEM:
					switch(property) {
						case "output":
							return true;
						case "condition":
							_conditionFields[index].text = calc.condition[index];
							return true;
					}
					break;
			}
			return handled;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean {
			
			var dealtWith:Boolean = super.updateModelProperty(property,oldValue,newValue);
			
			switch (property) {
				case "toDiagram":
					updatePosition();
					return true;
			}
			return dealtWith;
		}
		
		protected function createPorts():void
		{
			_outputPorts = new Array();
			for(var i:Number=0; i<IList(calc.anchors["output"]).length; i++) {
				_outputPorts[i] = new PortView(IList(calc.anchors["output"])[i], context as WorldContainer);
				addChild(_outputPorts[i]);
			}
			
			_outputDefaultPort = new PortView(calc.anchors["outputDefault"], context as WorldContainer);
			addChild(_outputDefaultPort);
			_inputPort = new PortView(calc.anchors["value"], context as WorldContainer);
			addChild(_inputPort);		
		}
		
		override public function render() : void
		{
			// skip super.updateView()  ... the graphics.clear is all we need from it
			graphics.clear();
			
			updateEnabledView();  // don't bother about a disabled view
			updatePosition();
		}
		
		private function updatePosition():void
		{
			if (_dragMediator)
				_dragMediator.updateViewPosition();
		}
		
		override protected function updateStatus():void
		{
			updateView();  // selection is indicated by color change .. no handles
		}
		
		override protected function updateEnabledView():void
		{
			graphics.clear();
			
			var height:Number = (IList(calc.anchors["output"]).length + 1)*RibbonView.HEIGHT;
			
			graphics.lineStyle(1, 0xBFBFBF, 0.75);
			var gradColors:Array;
			if (selected) {
				gradColors = [0xBFBFBF, 0x4D4D4D];  // top & bottom gradient, from msgCtr view
			} else {
				gradColors = [0x191919, 0x191919];  // top & bottom gradient, from msgCtr view
			}
			var gradAlphas:Array = [1,1];
			var gradRatios:Array = [0,255];
			var gradMatrix:Matrix = new Matrix();
			
			gradMatrix.createGradientBox(WIDTH, height, Math.PI/2);
			
			graphics.beginGradientFill(GradientType.LINEAR,gradColors,gradAlphas,gradRatios,gradMatrix);
			
			graphics.drawRoundRect(0,0,WIDTH,height,10,10);
		}
		
		protected function addCondition(n:Number, value:String):void
		{
			_conditionBackground[n] = new Sprite();
			drawValueBackground(_conditionBackground[n],100,RibbonView.HEIGHT,3 + RibbonView.HEIGHT,3 + RibbonView.HEIGHT*n);
			
			_conditionFields[n] = new RichTextField(100,RibbonView.HEIGHT);
			_conditionFields[n].explicitLineBreak = true;
			_conditionFields[n].x = 3 + RibbonView.HEIGHT;
			_conditionFields[n].y = 3 + RibbonView.HEIGHT*n;
			_conditionFields[n].color = 0xFFFFFF;
			_conditionFields[n].visible = true;
			_conditionFields[n].text = value;
			addChild(_conditionFields[n]);
			new TextEditMediator().handleTextEditEvents(_conditionFields[n],null,  handleConditionUpdate);
		}
		
		private function handleConditionUpdate(tx:RichTextField, val:String):void
		{
			var n:int = _conditionFields.indexOf(tx);
			if (n != -1) {
				calc.condition[n] = val;
			}
		}
		
		protected function addComponents():void
		{
			for(var i:Number=0; i<calc.condition.length; i++) {
				addCondition(i, calc.condition[i]);
			}
			
			_defaultTextField = new RichTextField(100,RibbonView.HEIGHT);
			_defaultTextField.text = "Default";
			_defaultTextField.editable = false;
			_defaultTextField.explicitLineBreak = true;
			_defaultTextField.x = 3 + RibbonView.HEIGHT;
			_defaultTextField.y = 3 + RibbonView.HEIGHT*(IList(calc.anchors["output"]).length);
			_defaultTextField.color = 0xFFFFFF;
			addChild(_defaultTextField);
		}
		
		protected function drawValueBackground(sprite:Sprite, w:Number, h:Number, xx:Number, yy:Number):void
		{
			sprite.graphics.lineStyle(1);
			sprite.graphics.beginFill(0x191919, 0.8);
			sprite.graphics.drawRect(0,0,w,h);
			var shadow:DropShadowFilter = new DropShadowFilter(2,0);
			shadow.inner = true;
			sprite.filters = [shadow];
			sprite.x = xx;
			sprite.y = yy;
			
			addChild(sprite);
		}
		
		/**
		 * The Calc object associated with this view's TaconiteModel.
		 */
		public function get calc():CalcSwitch
		{
			return model.value as CalcSwitch;
		}
		
		
		
		/**
		 * Create a feedback view that will be superimposed on this view in a transparent layer.
		 */        
		override protected function ObjectFeedbackFactory():ITaconiteView
		{
			return null;
		}
		
		
		override protected function updateThumbnail():void
		{
			// prevent thumbnails on Calc
		}
	}
}
