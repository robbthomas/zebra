package com.alleni.author.view.objects
{
import com.alleni.author.Navigation.Pager;
import com.alleni.author.Navigation.PagerController;
import com.alleni.author.Navigation.PagerView;
import com.alleni.author.Navigation.SmartObjectView;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.objects.ArenaController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.WorldContainer;
import com.alleni.taconite.event.ModelUpdateEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;

public class ArenaControlBar extends Sprite
	{		
		private static const VERT_SPACING:Number = 8;

		private var _pager:Pager;
        private var _pagerView:PagerView;
		private var _model:TaconiteModel;
		private var _role:uint;
        private var _context:ViewContext;
        private var _everPositioned:Boolean;
        private var _navigatorVersion:Boolean;
		private var _clickX:Number;
		private var _clickY:Number;
		private var _origPosition:Point;
						
		public static const WIDTH:Number = 180;
		private static const HEIGHT:Number = 20;

			
		private var _controlBar:ControlBar = new ControlBar();
        private var _backgroundBar:Sprite;
        private var _plusMinusBar:Sprite;

		/**
		 * Constructor 
		 * @param model = the pager that this control-bar is attached to.
		 * 
		 */
		public function ArenaControlBar( model:TaconiteModel, role:uint, context:ViewContext, pagerView:PagerView, navigatorVersion:Boolean = false)
		{
			super();
			
			_model = model;
			_pager = model.value as Pager;
            _pagerView = pagerView;
			_role = role;
			_context = context;
            _navigatorVersion = navigatorVersion;
	
			_backgroundBar = _controlBar.makeArenaBar(0);
			var navControl:Sprite = _controlBar.makeNavControl(startAction,prevAction,playAction,nextAction,endAction,
					"First page", "Previous page", "Play", "Pause", "Next page", "Last page");
            navControl.x = 16;
            navControl.y = 3;
            _backgroundBar.addChild(navControl);

            _plusMinusBar = _controlBar.makePageControl();
            _controlBar.addPagingButton(_plusMinusBar, drawDeleteButtonState, minusAction, "Delete this page");
            _controlBar.addPagingButton(_plusMinusBar, drawAddButtonState, plusAction, "Add new page:\n(Shift to insert before this page)\n(Option | Alt to duplicate page)");
            _plusMinusBar.x = 145;
            _plusMinusBar.y = 3;
            _plusMinusBar.visible = !(Application.uiRunning || _navigatorVersion);
            _backgroundBar.addChild(_plusMinusBar);
            showPlusMinus = !Application.uiRunning;   // redraws background to correct width

			var textControl:Sprite = _controlBar.makeCounterControl("Current page number", "Number of pages");
			textControl.y = 3;
			textControl.x = 95;
			_backgroundBar.addChild(textControl);
			
			addChild(_backgroundBar);
			
			updatePageNumText();
					
			_controlBar.enablePlayButton(false);

			// make the whole bar draggable  (in diagram its draggable with MC)
			if (_role != ViewRoles.DIAGRAM && !_navigatorVersion)
				this.addEventListener(MouseEvent.MOUSE_DOWN,grabListener);

			// listen for property changes
			model.addEventListener(ModelUpdateEvent.MODEL_UPDATE, propertyChangeListener, false, 4);
            if (_pagerView) {
                this.visible = false;
            }
            this.visible = false;  // keep hidden until after positioned
		}

        private function get computedBarWidth():Number
        {
            return (showPlusMinus) ? 184 : 150;  // when paused leave room for +/- buttons
        }

		public function destroy():void
		{	
			this.removeEventListener(MouseEvent.MOUSE_DOWN,grabListener);
			_model.removeEventListener(ModelUpdateEvent.MODEL_UPDATE, propertyChangeListener);
		}

		private function grabListener( e:MouseEvent ):void
		{	
			var arenaView:ArenaView = _pagerView as ArenaView;
            e.stopImmediatePropagation();  // ensure click doesn't de-select objects
			if (e.type==MouseEvent.MOUSE_DOWN && arenaView && this.mouseX < 15) {
				_clickX = e.stageX;
				_clickY = e.stageY;
                _origPosition = arenaView.computeControlBarPosition();
				stage.addEventListener(MouseEvent.MOUSE_MOVE,grabMoveListener);
				stage.addEventListener(MouseEvent.MOUSE_UP,grabUpListener);
            }
		}
		
		private function grabMoveListener( e:MouseEvent ):void
		{
			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			// proposed new controls location, based on mouse movement
			var mouse:Point = worldContainer.globalToLocal(new Point(e.stageX,e.stageY));
			var click:Point = worldContainer.globalToLocal(new Point(_clickX,_clickY));
			var position:Point = new Point(_origPosition.x + (mouse.x-click.x),  _origPosition.y + (mouse.y-click.y));

			var arenaView:ArenaView = _pagerView as ArenaView;
			if (arenaView) {
				arenaView.undockControlBar(position);
				if (arenaView.controlBarNearDockPosition())
					arenaView.dockControlBar();
				
				stage.invalidate();  // trigger the render in ApplicationController, so position will update
			}
		}
		
		private function grabUpListener( e:MouseEvent ):void
		{
			this.stopDrag();
			if (stage) {
				stage.removeEventListener(MouseEvent.MOUSE_MOVE,grabMoveListener);
				stage.removeEventListener(MouseEvent.MOUSE_UP,grabUpListener);
			}

            var arenaView:ArenaView = _pagerView as ArenaView;
            var pos:Point = arenaView.computeControlBarPosition();
            if (!Application.running && !_pager.childOfClosed && this.mouseX < 15 && Point.distance(pos, _origPosition) < 1) {
                ApplicationController.instance.authorController.selectSingleModel(_pager.model);
            }
		}
				
		private function propertyChangeListener(e:ModelUpdateEvent):void
		{
			if (e.source == _model) {
				
				switch (e.property) {
					case "pageNumber":
					case "pageCount":
						this.updatePageNumText();
						break;
				}
			}
		}
		
		public function updatePosition(force:Boolean=false):void
		{
			// called by render handler
            if (_pagerView && (_pagerView.stage || force)) {   // "force" needed because when addedToStage event is received, view.stage is still false
                var position:Point = _pagerView.computeControlBarPosition();
                x = position.x - WIDTH/2;
                y = position.y - HEIGHT - VERT_SPACING;
                var firstTime:Boolean = !_everPositioned;
                _everPositioned = true;
                if (firstTime) {
                    _pagerView.recalcControls();  // recalc visibility
                }
            }
		}

        override public function set visible(value:Boolean):void
        {
            super.visible = value && (_everPositioned || _navigatorVersion);  // ensure it doesn't appear at topLeft of screen
        }
		
		private function setPageNumber( number:int ):void
		{	
			if (number < 1)  number = 1;
			if (number > _pager.pageCount)  number = _pager.pageCount;
			
			if (number != _pager.pageNumber) {
				_pager.pageNumber = number;
			}
		}
		
		
		private function updatePageNumText():void
		{
			_controlBar.pageNumber = _pager.pageNumber.toString();
			_controlBar.pageCount = _pager.pageCount.toString();
		}
		
		
		private function startAction():void
		{
			trace("startButton");
			setPageNumber(1);
		}
		
		private function prevAction():void
		{
			trace("prevButton");
            setPageNumber(_pager.pageNumber-1);
		}
		
		private function nextAction():void
		{
			trace("nextAction");
            setPageNumber(_pager.pageNumber+1);
		}
		
		private function endAction():void
		{
			trace("endAction");
			setPageNumber(_pager.pageCount);
		}
		
		private function plusAction(shiftKey:Boolean, altKey:Boolean):void
		{
			var after:Boolean = !shiftKey;  // shift means insert before current page
			trace("plusAction duplicate="+altKey, "after="+after);
            controller.addPageBy(_pager, _pager.currentPage, after, altKey);  // duplicate=altKey
		}

        private function get controller():PagerController
        {
            return _pager.controller as PagerController;
        }
		
        private function minusAction(shiftKey:Boolean, altKey:Boolean):void
        {
            trace("minusAction");
            if (_pager.pageCount > 1) {
                controller.deletePage(_pager, _pager.currentPage);
            }
        }

		private function playAction():void
		{
            var arena:Arena = _pager as Arena;
            if (arena) {
                if(arena.ticking){
                    ArenaController.instance.stopClock(arena);
                    _controlBar.setPlayButtonState(ControlBar.PAUSED);
                }
                else{
                    ArenaController.instance.startClock(arena);
                    _controlBar.setPlayButtonState(ControlBar.PLAYING);
                }
            }
		}


        private function editCurrentPage(e:Event):void
        {
            _context.stage.removeEventListener(Event.ENTER_FRAME, editCurrentPage);
            var view:SmartObjectView = _pager.currentPage.object.getView() as SmartObjectView;
            if (view) {
                view.openEditing();
            }
        }

        private function get nowEditing():Boolean
        {
            return _pager.currentPage.object.editing;
        }


        private function drawAddButtonState(state:String):Sprite
        {
            switch (state) {
                case ControlBar.UP:    return drawAddButton(0x484848,0xC8C8C8);
                case ControlBar.OVER:  return drawAddButton(0xAFAFAF,0x000000);
                default:    return drawAddButton(0xFFFFFF,0x000000);
            }
        }

        private function drawAddButton(chipColor:uint, iconColor:uint):Sprite
        {
            var chip:Sprite = _controlBar.drawChip(chipColor);
            var icon:Sprite= new Sprite();

            chip.addChild(icon);

            with (icon.graphics){
                lineStyle(1.5,iconColor);
                moveTo(4,0);
                lineTo(4,8);
                moveTo(0,4);
                lineTo(8,4);
            }

            _controlBar.centerIcon(chip, icon);
            return chip;
        }

        private function drawDeleteButtonState(state:String):Sprite
        {
            switch (state) {
                case ControlBar.UP:    return drawDeleteButton(0x484848,0xC8C8C8);
                case ControlBar.OVER:  return drawDeleteButton(0xAFAFAF,0x000000);
                default:    return drawDeleteButton(0xFFFFFF,0x000000);
            }
        }

        private function drawDeleteButton(chipColor:uint, iconColor:uint):Sprite
        {
            var chip:Sprite = _controlBar.drawChip(chipColor);
            var icon:Sprite= new Sprite();

            chip.addChild(icon);

            with (icon.graphics){
                lineStyle(1.5,iconColor);
                moveTo(3,6);
                lineTo(11,6);
                endFill();
            }

            //centerIcon(chip, icon);
            return chip;
        }

        public function enablePlayButton(state:Boolean):void
        {
            _controlBar.enablePlayButton(state);
        }

        public function setPlayButtonState(state:uint):void
        {
            _controlBar.setPlayButtonState(state);
        }

        public function get showPlusMinus():Boolean
        {
            return _plusMinusBar.visible;
        }

        public function set showPlusMinus(value:Boolean):void
        {
            _plusMinusBar.visible = value;
            _controlBar.setArenaBarWidth(_backgroundBar, computedBarWidth);
        }

        private function recalcVisibility():void
        {
            var view:ArenaView = _pagerView as ArenaView;
            if (view) {
                view.recalcControls();
            }
        }

        public static function updateAllControlbarVisibility():void
        {
            // use render loop to avoid duplicate work
            ApplicationController.instance.requestLayoutAction(updateVisNow);
        }

        private static function updateVisNow():void
        {
            // controlBars for _pager objects outside the vellum must hide during gadget editing
            if (EditorUI.instance) {
                var container:DisplayObjectContainer = feedbackControlsView;
                var k:int = container.numChildren;
                for (var n:int=0; n < k; n++) {
                    var bar:ArenaControlBar = container.getChildAt(n) as ArenaControlBar;
                    if (bar && !bar._navigatorVersion) {
                        bar.recalcVisibility();
                    }
                }
            }
        }

        public function get pagerView():PagerView
        {
            return _pagerView;
        }

        private static function get feedbackControlsView():Sprite
        {
            return WorldContainer(Application.instance.viewContext).feedbackControlsView;
        }

	}
}
