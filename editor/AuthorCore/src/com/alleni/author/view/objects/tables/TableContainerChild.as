package com.alleni.author.view.objects.tables
{
	import flash.display.CapsStyle;
	import flash.display.Sprite;
	
	public class TableContainerChild extends Sprite
	{
		protected var _fillColor:uint = AbstractTableView.GRAY_DARK;
		protected var _fillAlpha:uint = 0;
		private var _width:Number = 0;
		
		public function TableContainerChild()
		{
			super();
		}
		
		public function draw():void
		{
			graphics.clear();
			if(_fillAlpha <= 0) {
				return;
			}
			graphics.beginFill(_fillColor, _fillAlpha);
			graphics.drawRect(0, 1, width, AbstractTableView.ROW_HEIGHT-1);
			
			// Bottom line
			graphics.lineStyle(1,0x646464, _fillAlpha,false,"normal",CapsStyle.NONE);
			graphics.moveTo(0,AbstractTableView.ROW_HEIGHT);
			graphics.lineTo(width,AbstractTableView.ROW_HEIGHT);
		}
		
		override public function set width(value:Number):void
		{
			_width = value;
			draw();
		}
		
		override public function get width():Number
		{
			return _width;
		}
		
		public function get fillColor():uint{
			return _fillColor;
		}
		public function set fillColor(value:uint):void{
			_fillColor = value;
		}
	}
}