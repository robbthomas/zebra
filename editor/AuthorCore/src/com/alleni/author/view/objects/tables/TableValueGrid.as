package com.alleni.author.view.objects.tables
{
	import com.alleni.author.model.objects.TruthTable;
	
	import mx.collections.IList;

	public class TableValueGrid extends TableGrid
	{
		public function TableValueGrid(table:TruthTable, tableView:AbstractTableView, columns:int, rows:int, initialValues:IList)
		{
			super(table, tableView, columns, rows, initialValues);
		}
	}
}