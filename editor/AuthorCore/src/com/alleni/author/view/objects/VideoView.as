package com.alleni.author.view.objects
{
	import com.alleni.author.controller.objects.media.MediaScrubberMediator;
	import com.alleni.author.controller.objects.media.MediaViewControlsMediator;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.definition.application.AssetIcons;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.VideoObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class VideoView extends IconedView
	{
		private static const BACKGROUND_ALPHA:Number = 0.75;
		
		private var _background:Shape;
		private var _clickEater:Sprite;
		private var _bufferingFeedback:LightLabel;
		private var _scalingFactor:Number;
		private var _controlsMediator:MediaViewControlsMediator;
		private var _scrubberMediator:MediaScrubberMediator;
		private var _safeMode:Boolean;
		
		public function VideoView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			if (role == ViewRoles.PRIMARY) {				
				_background = new Shape();
				_clickEater = new Sprite();
				_clickEater.doubleClickEnabled = true;
				this.addChild(_background);
				// ensure the icon is on top
				if (getChildIndex(_icon) > -1)
					this.swapChildrenAt(this.numChildren-1, getChildIndex(_icon));
				_bufferingFeedback = new LightLabel();
				_bufferingFeedback.visible = false;
				_bufferingFeedback.size = 18;
				_bufferingFeedback.bold = true;
				_bufferingFeedback.color = 0xffffff;
				_bufferingFeedback.alpha = 0.5;
				_bufferingFeedback.text = "buffering...";
				this.addChild(videoObject.contentViewer);
				this.addChild(_bufferingFeedback);
				this.addChild(_clickEater);
				
				_scrubberMediator = new MediaScrubberMediator().handleScrubber(this);
				_controlsMediator = new MediaViewControlsMediator().handleViewControls(this);
				drawBackground();
			}
		}
		
		public function get videoObject():VideoObject
		{
			return model.value as VideoObject;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{	
			var handledModelUpdate:Boolean = super.updateModelProperty(property,oldValue,newValue);	
			var numValue:Number;

			switch (property) {
				case "buffering":
					if (_scrubberMediator.scrubber.width != object.width)
						_scrubberMediator.scrubber.width = object.width;
					_bufferingFeedback.visible = Boolean(newValue);
					return true;
					break;
				case "width":
				case "height":
				case "maintainAspect":
				case "sourceDimensions":
					updateDimensions();
					break;
				case "anchorPoint":
					updatePosition();
				case "scaleX":
					// does this need to be handled here? prior to refactor this had been in the init viewer
					break;
				case "scaleY":
					// does this need to be handled here? prior to refactor this had been in the init viewer
					break;
			}//switch
			
			return handledModelUpdate;
		}
		
		override public function updateViewPosition():void
		{
			super.updateViewPosition();
			if (_controlsMediator)
				_controlsMediator.updatePosition();
		}

		private function updatePosition():void
		{
			var newCenter:Point = new Point(object.left + videoObject.width/2, object.top + videoObject.height/2)
			_background.x = newCenter.x;
			_background.y = newCenter.y;
			_clickEater.x = newCenter.x;
			_clickEater.y = newCenter.y;
			
			_icon.x = newCenter.x - _icon.width/2;
			_icon.y = newCenter.y - _icon.height/2;
			
			if (_controlsMediator)
				_controlsMediator.updatePosition();
		}
		
		override protected function updateEnabledView():void
		{
			super.updateEnabledView();
			if (_role == ViewRoles.PRIMARY) {
				if (videoObject.contentViewer.width > 0 && videoObject.maintainAspect) {
					videoObject.contentViewer.x = object.left + (object.width - videoObject.sourceDimensions.x * _scalingFactor) / 2;
					videoObject.contentViewer.y = object.top + (object.height - videoObject.sourceDimensions.y * _scalingFactor) / 2;
				} else {
					videoObject.contentViewer.x = object.left;
					videoObject.contentViewer.y = object.top;
				}
				_bufferingFeedback.x = object.left;
				_bufferingFeedback.y = object.top;
				_scrubberMediator.scrubber.x = object.left;
				_scrubberMediator.scrubber.y = object.top + object.height - 15; // canned scrubber height for draft UI
			}
		}
		
		private function updateDimensions():void
		{
			var naughtyVideoScaleY:Number = 1; // 1 is a well behaved scale
			var naughtyVideoScaleX:Number = 1; // 1 is a well behaved scale
			if (videoObject.contentViewer.numChildren > 0) {
				var videoPlayer:DisplayObject = videoObject.contentViewer.getChildAt(0);
				
				if (videoPlayer.width != videoObject.sourceDimensions.x) // scale will be other than 1 due to naughtiness such as ads overlaid in the player.
					naughtyVideoScaleX = videoObject.sourceDimensions.x / videoPlayer.width; // this only affects the height, but could also be done for width.
				
				if (videoPlayer.height != videoObject.sourceDimensions.y) // scale will be other than 1 due to naughtiness such as ads overlaid in the player.
					naughtyVideoScaleY = videoObject.sourceDimensions.y / videoPlayer.height;
			}
			if (videoObject.maintainAspect) {
				if (object.width / object.height > videoObject.sourceDimensions.x / videoObject.sourceDimensions.y)
					_scalingFactor = object.height / videoObject.sourceDimensions.y;
				else
					_scalingFactor = object.width / videoObject.sourceDimensions.x;
				videoObject.contentViewer.width = videoObject.sourceDimensions.x * _scalingFactor/naughtyVideoScaleX;
				videoObject.contentViewer.height = videoObject.sourceDimensions.y * _scalingFactor/naughtyVideoScaleY;
			} else {
				videoObject.contentViewer.width = object.width/naughtyVideoScaleX;
				videoObject.contentViewer.height = object.height/naughtyVideoScaleY;
			}
			_background.width = object.width;
			_background.height = object.height;
			_clickEater.width = object.width;
			_clickEater.height = object.height;
			_scrubberMediator.scrubber.width = object.width;
			updatePosition();
		}
	
		private function drawBackground():void
		{
			with (_background.graphics) {
				clear();
				beginFill(AssetIcons.ICONCHIP_FILL_COLOR_1, BACKGROUND_ALPHA);
				drawRect(object.left, object.top, videoObject.width, videoObject.height);
				endFill();
			}
			with (_clickEater.graphics) {
				clear();
				beginFill(0xff0000, 0.01);
				drawRect(object.left, object.top, videoObject.width, videoObject.height);
				endFill();
			}
			_scrubberMediator.scrubber.width = object.width;
		}
		
		override protected function addIcon():void
		{
			_icon.x = object.left + videoObject.width/2 - _icon.width/2;
			_icon.y = object.top + videoObject.height/2 - _icon.height/2;
			this.addChild(_icon);
			_icon.visible = !Application.running;
		}
		
        override public function set makeSafeForCaching(makeSafe:Boolean):void
        {
            if (makeSafe) {
                if (!_safeMode) {
                    this.removeChild(videoObject.contentViewer);   // allow video background with icon to show instead
                    if (_icon) {
                        _icon.visible = true;
                    }
                    _safeMode = true;
                }
            } else if (_safeMode) {
                this.addChild(videoObject.contentViewer);
                if (_icon) {
                    _icon.visible = !Application.running;
                }
                _safeMode = false;
            }
        }
		
		override public function warnViewToDelete():void
		{
			super.warnViewToDelete();
			_scrubberMediator.warnBeforeDelete();
			_controlsMediator.warnBeforeDelete();
		}

        override public function handleRunningChanged():void {
            super.handleRunningChanged();
			if (_icon) {
                _icon.visible = !Application.running;
            }
        }

    }
}
