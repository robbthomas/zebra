package com.alleni.author.view.objects
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.objects.GadgetMediator;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.objects.Calc;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.AlignSwitch;
	import com.alleni.author.view.ui.BooleanSwitch;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.RibbonColorPicker;
	import com.alleni.author.view.ui.RibbonDropDownMenu;
	import com.alleni.author.view.ui.RibbonFillPicker;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.author.view.ui.controls.ColorPicker;
	import com.alleni.author.view.ui.controls.IColorPickerParent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	
	public class CalcView extends GadgetView implements IColorPickerParent
	{
		public static const WIDTH:Number = 90;
		public static const HEIGHT:Number = 22;
		public static const X_INLET_PORT:Number = 0;
		public static const Y_INLET_PORT:Number = 0;
		public static const X_VALUE_PORT:Number = 70;
		public static const Y_VALUE_PORT:Number = 0;
		
		private static const X_TEXT_FIELD:Number = 23;
		private static const Y_TEXT_FIELD:Number = 3;
		
		protected var _leftPort:PortView;
		protected var _rightPort:PortView;
		private var _valueBackground:Sprite;

		private var _myHeight:Number = 22;
		
		// Components
		private var _textField:RichTextField;
		private var _colorPickerView:RibbonColorPicker;
		private var _fillPickerView:RibbonFillPicker;
		private var _booleanSwitch:BooleanSwitch;
		private var _alignSwitch:AlignSwitch;
		private var _dropDownMenu:RibbonDropDownMenu;
		private var _componentList:Array;
		
		// event mediators
		protected var _mediator:GadgetMediator;
		
		//COLOR PICKER
		private var _colorPicker:ColorPicker;
		
		public function CalcView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			
			_mediator = new GadgetMediator(context);
			_mediator.handleViewEvents(this, role);
			
			createPorts();  // needs the _mediator
			addComponents();  // must be done before updateView
			changeControl();
		}
		
		override public function initialize():void
		{
			super.initialize();  // calls createDragMediator, then does updateView
		}
		
		
		protected function get myHeight():Number
		{
			return _myHeight;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean {
			
			var dealtWith:Boolean = super.updateModelProperty(property,oldValue,newValue);
			
			switch (property) {
				case "toDiagram":
					updatePosition();
					return true;
				case "type":
				case "controlValue":
					changeControl();
					return true;
			}
			return dealtWith;
		}
		
		protected function createPorts():void
		{
			_leftPort = new PortView(calc.anchors["calcInlet"], context as WorldContainer);
			addChild(_leftPort);
			
			_rightPort = new PortView(calc.anchors["calcValue"], context as WorldContainer);
			addChild(_rightPort);
			_rightPort.x = 70;
		}
		
		override public function render() : void
		{
			// skip super.updateView()  ... the graphics.clear is all we need from it
			graphics.clear();
			
			updateEnabledView();  // don't bother about a disabled view
			updatePosition();
			
			if (_role == ViewRoles.PRIMARY)
				this.visible = !Application.running && !calc.childOfClosed;  // hide on Run or when parent composite is closed
		}
		
		private function updatePosition():void
		{
			if (_dragMediator)
				_dragMediator.updateViewPosition();
		}
		
		override protected function updateStatus():void
		{
			updateView();  // selection is indicated by color change .. no handles
		}
		
		override protected function updateEnabledView():void
		{
			graphics.clear();
			
			graphics.lineStyle(1, 0xBFBFBF, 0.75);
			if (selected) {
				var gradColors:Array = [0xBFBFBF, 0x4D4D4D];  // top & bottom gradient, from msgCtr view
				var gradAlphas:Array = [1,1];
				var gradRatios:Array = [0,255];
				var gradMatrix:Matrix = new Matrix();
				
				gradMatrix.createGradientBox(WIDTH, myHeight, Math.PI/2);
				
				graphics.beginGradientFill(GradientType.LINEAR,gradColors,gradAlphas,gradRatios,gradMatrix);
			} else {
				graphics.beginFill(0x191919);
			}
			graphics.drawRoundRect(0,0,WIDTH,myHeight,10,10);
		}
		
		protected function changeControl():void
		{
			if (calc.type) {
				switch (calc.type) {
					
					case Modifiers.BOOLEAN_TYPE:
						activateComponent(_booleanSwitch);
						_booleanSwitch.setValue(calc.controlValue.toString(), RibbonView.INIT);
						break;
					
					case Modifiers.COLOR_TYPE:				
						activateComponent(_colorPickerView);
						_colorPickerView.selectedColor = new uint(calc.controlValue);
						break;
					
					case Modifiers.FILL_TYPE:
						activateComponent(_fillPickerView);
						if (calc.controlValue is GraphicFill)
							_fillPickerView.setFillSpec(calc.controlValue as GraphicFill);
						break;
					
					case Modifiers.HORIZONTAL_ALIGN_TYPE: 	
						activateComponent(_alignSwitch);
						_alignSwitch.setValue(calc.controlValue.toString(), RibbonView.INIT);
						break;
					
					case Modifiers.ENUMERATED_TYPE:			
						activateComponent(_dropDownMenu);
						_dropDownMenu.setValue(calc.controlValue.toString(), RibbonView.INIT);			
						_dropDownMenu.args = calc.constraints as Array;
						break;
					
					default:								
						activateComponent(_textField);
						_textField.text = calc.controlValue.toString();
				}
			}	
		}
		
		// TODO: We should be adding components as needed.
		protected function addComponents():void {
			
		/*	_valueBackground = new Sprite();
			_textField = new RichTextField(42,20);
			_textField.text = "INPUT";
			_textField.explicitLineBreak = true;
			drawValueBackground(_valueBackground,42,15,X_TEXT_FIELD,Y_TEXT_FIELD);
			addChild(_textField);
			_textField.x = X_TEXT_FIELD;
			_textField.y = Y_TEXT_FIELD
			_textField.color = 0xFFFFFF;
			new TextEditMediator().handleTextEditEvents(_textField,null,  function(tx:RichTextField,val:String):void{myValue=val} );
			
			_colorPickerView = new RibbonColorPicker(context, this);
			_colorPickerView.x = 20;
			_colorPickerView.y = 3;
			addChild(_colorPickerView);
			
			_fillPickerView = new RibbonFillPicker(ApplicationUI.instance.rawChildren, null, fillValueCallback, null, calc);
			_fillPickerView.x = 20;
			_fillPickerView.y = 3;
			_fillPickerView.width = 40;
			addChild(_fillPickerView);
			
			_booleanSwitch = new BooleanSwitch(this);
			_booleanSwitch.width = 42;
			_booleanSwitch.firstPosition = 0;
			_booleanSwitch.x = 20;
			_booleanSwitch.y = 3;
			
			addChild(_booleanSwitch);
			
			_alignSwitch = new AlignSwitch(this,false);  // vert=false
			_alignSwitch.width = 42;
			_alignSwitch.firstPosition = 13;
			_alignSwitch.x = 20;
			_alignSwitch.y = 3;
			addChild(_alignSwitch);
			
			_dropDownMenu = new RibbonDropDownMenu(context, this, []);
			_dropDownMenu.x = 20;
			_dropDownMenu.y = 3;
			addChild(_dropDownMenu);
			
			// set visibility to false until needed
			_valueBackground.visible = false;
			_textField.visible = false;
			_colorPickerView.visible = false;
			_fillPickerView.visible = false;
			_booleanSwitch.visible = false;
			_alignSwitch.visible = false;
			_dropDownMenu.visible = false;
			
			_componentList = [_textField, _colorPickerView, _fillPickerView, _booleanSwitch, _alignSwitch, _dropDownMenu];*/
		}
		
		protected function drawValueBackground(sprite:Sprite, w:Number, h:Number, xx:Number, yy:Number):void
		{
			sprite.graphics.lineStyle(1);
			sprite.graphics.beginFill(0x191919, 0.8);
			sprite.graphics.drawRect(0,0,w,h);
			var shadow:DropShadowFilter = new DropShadowFilter(2,0);
			shadow.inner = true;
			sprite.filters = [shadow];
			sprite.x = xx;
			sprite.y = yy;
			
			addChild(sprite);
		}
		
		public function buildColorPickerIfNeeded():void{
			if(!_colorPicker){
				_colorPicker = new ColorPicker(EditorUI.instance);
				_colorPicker.draw();
				_colorPicker.x = -25;
				addChild(_colorPicker);
			}else{
				trace("Color Picker Present");
			}
		}
		
		/**
		 * The Calc object associated with this view's TaconiteModel.
		 */
		public function get calc():Calc
		{
			return model.value as Calc;
		}
		
		/**
		 * This is how the RichTextField sets the controlValue 
		 * @param value
		 * 
		 */
		public function set myValue(value:Object):void
		{
			if (value=="false")
				Calc(model.value).controlValue = false;
			else
				Calc(model.value).controlValue = value;
		}
		
		/**
		 * This is how switch buttons set the control value 
		 * @param value
		 * 
		 */
		public function set myValueFromControl(value:String):void{
			this.myValue = value;
		}
		
		
		private function activateComponent(component:Object):void
		{
			component.visible = true;
			for each (var obj:Object in _componentList) {
				if (obj==component)
					component.visible = true;
				else
					obj.visible = false;
			}
			
			_valueBackground.visible = _textField.visible;
		}
		
		/**
		 * Create a feedback view that will be superimposed on this view in a transparent layer.
		 */        
		override protected function ObjectFeedbackFactory():ITaconiteView
		{
			return null;
		}
		
		
		override protected function updateThumbnail():void
		{
			// prevent thumbnails on Calc
		}
		
		public function get colorPicker():ColorPicker{
			return _colorPicker
		}
		
		
		private function fillValueCallback(fillSpec:GraphicFill):void
		{
			calc.controlValue = fillSpec;
		}
	}
}
