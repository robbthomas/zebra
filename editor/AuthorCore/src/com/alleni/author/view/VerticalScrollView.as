package com.alleni.author.view	
{
	import com.alleni.author.model.ScrollModel;
	
	import flash.display.Sprite;
	import flash.events.EventPhase;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class VerticalScrollView extends ScrollView
	{
		public function VerticalScrollView(model:ScrollModel, scrollControllerWidth:Number, scrollControllerHeight: Number, trackBackgroundColor:uint = 0, buttonBackgroundColor:uint = 0, thumbAlpha:Number = 1)
		{
			super(model, scrollControllerWidth, scrollControllerHeight);
			
			_model = model;
			_scrollControllerWidth = scrollControllerWidth;
			_scrollControllerHeight = scrollControllerHeight;
			_trackBackgroundColor = trackBackgroundColor;
			_buttonBackgroundColor = buttonBackgroundColor;
			_thumbAlpha = thumbAlpha;
			
			init()
			drawScrollBar();
		}
		
		override protected function init():void //done
		{	
			_documentLength = _model.maxScrollPosition - _model.minScrollPosition;
			_buttonAreaWidth = 16;
			_buttonAreaHeight = 34;
			_scrollControllerWidth = 18; 
			_thumbWidth = 14
			_lineScrollSize = (_documentLength - _model.viewPortLength) / 20;
			nudgeAmount = 15;
		}
		
		override public function set scrollControllerWidth(value:Number):void  //done
		{
			_scrollControllerWidth = value;
			validate();
		}
		
		override public function set scrollControllerHeight(value:Number):void  //done
		{
			_scrollControllerHeight = value;
			validate();
		}
		
		override protected function makeButtons():void
		{
			var gap:uint = 1
				
			// button overlay under both buttons, for variable speed scrolling
			if (_buttonHolder == null) {
				_buttonHolder = new Sprite();
				with (_buttonHolder.graphics) {
					beginFill(0xffffff, 0.01);
					drawRect(0, 0, _buttonAreaWidth, _buttonAreaHeight);
					endFill();
				}
				_scrollBar.addChild(_buttonHolder);
				buttonHolderCreated();
			}
			_buttonHolder.y = _scrollControllerHeight - _buttonAreaHeight + gap;

			
			if (_lessButton == null) {
				_lessButton = new ScrollButton(upStateGraphic, downStateGraphic, overStateGraphic, hitStateGraphic);
				_buttonHolder.addChild(_lessButton);
			}
			_lessButton.x = this._thumbEndPos.x - _lessButton.width/2;
			_lessButton.y = 0;
			
			if (_moreButton == null) {
				_moreButton = new ScrollButton(
					rotateGraphic(180, upStateGraphic),
					rotateGraphic(180, downStateGraphic),
					rotateGraphic(180, overStateGraphic),
					hitStateGraphic);
				_buttonHolder.addChild(_moreButton);
			}
			_moreButton.x = this._thumbEndPos.x - _lessButton.width/2;
			_moreButton.y = _buttonAreaHeight /2;
		}
		
		override protected function offsetFromCenterOfButtons(global:Point):Number
		{
			var local:Point = _buttonHolder.globalToLocal(global);
			return local.y - _buttonAreaHeight /2 +1;  // +1 to give even speed on either side of dead zone
		}
		
		protected function rotateGraphic(degrees:Number, sprite:Sprite):Sprite
		{
			var matrix:Matrix = sprite.transform.matrix;
			matrix.translate(-sprite.width/2, -sprite.height /2);
			matrix.rotate(degrees/180 * Math.PI);
			matrix.translate(sprite.width/2, sprite.height /2);
			sprite.transform.matrix = matrix;
			return sprite;
			
		}	

		override protected function makeScrollBar():void
		{
			if (!_scrollBar) {
				_scrollBar = new Sprite();
				this.addChild(_scrollBar);
			}
			
			_scrollBar.graphics.clear();
			_scrollBar.graphics.beginFill(_trackBackgroundColor,_trackAlpha);
			_scrollBar.graphics.drawRect(0,0,_scrollControllerWidth, _scrollControllerHeight);
			_scrollBar.graphics.endFill();
			
			_scrollBar.x = 0;
			_scrollBar.y = 0;	
		}
		
		override protected function makeTrack():void
		{			
			if (!_scrollBar)
				return;
			
			if (!_track) {
				_track = new Sprite();
				_scrollBar.addChild(_track);
			}
			
			_track.graphics.clear();
			_track.graphics.beginFill(_trackBackgroundColor,_trackAlpha);
			_track.graphics.drawRect(0,0,_scrollControllerWidth, _scrollControllerHeight);
			_track.graphics.endFill();
			
			_track.x = 0;
			_track.y = 0;
			
			_track.addEventListener(MouseEvent.CLICK, handleTrackClick);
		}
		
		override protected function makeTrackGraphic():void  //done
		{
			if (!_track)
				return;	
			
			_track.graphics.lineStyle(_dotSize,_trackForegroundColor,_trackAlpha);
			
			//first point
			var startPos:Point = new Point(_scrollControllerWidth / 2, _vMargin);
			_track.graphics.moveTo(startPos.x - 3, startPos.y);
			_track.graphics.lineTo(startPos.x + 3, startPos.y);
			
			//second point
			var endPos:Point = new Point(_scrollControllerWidth / 2, (_scrollControllerHeight - this._buttonAreaHeight ));
			
			_track.graphics.moveTo(endPos.x - 3, endPos.y);
			_track.graphics.lineTo(endPos.x + 3, endPos.y);
			
			//Draw line
			
			_track.graphics.moveTo(startPos.x, startPos.y);
			_track.graphics.lineTo(endPos.x, endPos.y);
			
			_thumbStartPos = startPos;
			_thumbEndPos = endPos;
		}
		
		override protected function makeThumb():void
		{	
			var padding:Number = 3;
			
			if (!_thumb) {
				_thumb = new Sprite();
				_thumb.alpha = _thumbAlpha;
				_track.addChild(_thumb);
				_thumb.addEventListener(MouseEvent.ROLL_OVER,handleMouseOver);
				_thumb.addEventListener(MouseEvent.ROLL_OUT,handleMouseOut);
				_thumb.x = _thumbStartPos.x - _thumbWidth / 2;
			} else {
				this.removeSpriteChildren(_thumb);
			}
			
			var trackLength:Number = Math.abs(_thumbEndPos.y - _dotSize - _thumbStartPos.y);
			
			_thumbWidth  = _scrollControllerWidth - 2 * padding ;			
			_thumbHeight = (trackLength / _documentLength) * trackLength;
		
			if (_thumbHeight > trackLength){
				_thumbHeight = trackLength;
			}
			else if(!(_thumbHeight > 15)){  // small or NaN
				_thumbHeight = 15;
			}
			
			_upState = addThumbUpState();
			_thumb.addChild(_upState);
			
			_overState = addThumbOverState();
			_thumb.addChild(_overState);
			
			_trackExtents = new Rectangle(_thumb.x, _thumbStartPos.y + _dotSize, 0, trackLength - _thumbHeight);
			updateThumbPosition();
		}
		
		protected function addThumbUpState():Sprite
		{
			var thumbSprite:Sprite = new Sprite();
			
			const radius:Number = 10;
			
			if (_thumbWidth == 0 || _thumbHeight == 0) return thumbSprite;
			
			with (thumbSprite.graphics) {
				beginFill(_thumbColor, _thumbAlpha);
				lineStyle(1, _thumbLineColor, 1, true);
				drawRoundRect(0, 0, _thumbWidth, _thumbHeight, radius, radius);
				endFill();
			}
			
			//make center line
			
			if(thumbSprite.height > thumbSprite.width){  //don't draw the line if the thumb is too small
				var centerLineLength:Number = _thumbWidth;
				
				var centerPoint:Point = new Point(_thumbWidth/2, _thumbHeight/2);
				
				with (thumbSprite.graphics) {
					lineStyle(1, _trackForegroundColor);
					moveTo(centerPoint.x, centerPoint.y - centerLineLength/2);
					lineTo(centerPoint.x, centerPoint.y + centerLineLength/2);
				}
			}
			return thumbSprite;
		}
		
		protected function addThumbOverState():Sprite
		{
			var thumbSprite:Sprite = new Sprite();
			
			var roundedEdge:Number = 10;
			
			with (thumbSprite.graphics) {
				beginFill(_thumbOverColor,1);
				lineStyle(1, 0x000000,1,true);
				drawRoundRect(0,0,_thumbWidth, _thumbHeight, roundedEdge, roundedEdge);
				endFill();
			}
			
			//make center line
			
			if (thumbSprite.height > thumbSprite.width) {  //don't draw the line if the thumb is too small
				var centerLineLength:Number = _thumbWidth;
				
				var centerPoint:Point = new Point(_thumbWidth/2, _thumb.height/2);
				
				with(thumbSprite.graphics){
					lineStyle(1, 0x000000);
					moveTo(centerPoint.x, centerPoint.y - centerLineLength/2);
					lineTo(centerPoint.x, centerPoint.y + centerLineLength/2);
				}
			}
			
			thumbSprite.alpha = 0;
			return thumbSprite;
		}
				
		override protected function updateThumbPosition():void
		{
			if (!draggingThumb) {
				var frac:Number = (_scrollPosition - _model.minScrollPosition) / (_documentLength - _model.viewPortLength);
				if ((frac >= 0) == false) frac = 0;  // negative or NaN
				if (frac > 1) frac = 1;
				var newPos:Number =  frac * _trackExtents.height;
				_thumb.y = _thumbStartPos.y + newPos;
			}
		}
		
		
		override public function removeSpriteChildren(sprite:Sprite):void
		{
			while(sprite.numChildren > 0)
				sprite.removeChildAt(0);
		}
		
		override public function set width(value:Number):void
		{
			// do nothing
		}
		
		public function drawScrollBar():void
		{	
			makeScrollBar();			
			makeTrack();			
			makeTrackGraphic();			
			makeThumb();
			makeButtons();
			addListeners();
		}
		
		protected function addListeners():void
		{	
			_thumb.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);	
		}
		
		protected function get upStateGraphic():Sprite
		{	
			var upArrow:Sprite = new Sprite();	
			
			with (upArrow.graphics) {
				beginFill(_thumbColor,1);
				drawCircle(8,8,8);
				
				endFill();
				
				beginFill(0x969696,1);
				moveTo(8,4);
				lineTo(8 - 3.857,12);
				lineTo(8 + 3.857,12);
				lineTo(8,4);
				endFill();
			}
			return upArrow;
		}
		
		protected function get overStateGraphic():Sprite
		{
			var upArrow:Sprite = new Sprite();
			
			with (upArrow.graphics) {
				beginFill(0x737373,1);
				drawCircle(8,8,8);
				
				endFill();
				
				beginFill(0xFFFFFF,1);
				moveTo(8,4);
				lineTo(8 - 3.857,12);
				lineTo(8 + 3.857,12);
				lineTo(8,4);
				endFill();
			}
			return upArrow;
		}
		
		protected function get downStateGraphic():Sprite
		{	
			var upArrow:Sprite = new Sprite();
			
			with (upArrow.graphics) {
				beginFill(0xaaaaaa,1);
				drawCircle(8,8,8);
				
				endFill();
				
				beginFill(0x000000,1);
				moveTo(8,4);
				lineTo(8 - 3.857,12);
				lineTo(8 + 3.857,12);
				lineTo(8,4);
				endFill();
			}
			return upArrow;
		}
		
		protected function get hitStateGraphic():Sprite
		{
			var shape:Sprite = new Sprite();
			
			shape.graphics.beginFill(0x0000FF,1);
			shape.graphics.drawCircle(8,8,8);
			shape.graphics.endFill();
			
			return shape;
		}
		
		override protected function calcScrollPosition():void
		{
			var _scrollPositionPercentage:Number  = (_thumb.y - _thumbStartPos.y - _dotSize) / _trackExtents.height; 
			scrollPosition = _model.minScrollPosition + _scrollPositionPercentage * (_documentLength - _model.viewPortLength);
		}
		
		override protected function handleTrackClick(e:MouseEvent):void
		{
			if (e.eventPhase != EventPhase.AT_TARGET)
				return;
			
			if (e.localY < _thumb.y)
				_scrollPosition -= _lineScrollSize;
			else if (e.localY > _thumb.y)
				_scrollPosition += _lineScrollSize;
			
			updateThumbPosition();
			dispatchScrollUpdateEvent();
		}
	}
}