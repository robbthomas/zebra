package com.alleni.author.view.ui
{
	import com.alleni.author.view.ui.controls.Alert;

	public class WarningWindow extends Alert
	{	
		public static function showError(message:String, title:String, volatile:Boolean=false):void
		{
			if (volatile && _alert) { // urgent enough to kill other visilble alerts
				_alert.titleText.text = title;
				_alert.bodyText.text = message;
			} else {
				okLabel = "OK"
				show(message, title, Alert.FIRST);
			}
		}
	}
}
