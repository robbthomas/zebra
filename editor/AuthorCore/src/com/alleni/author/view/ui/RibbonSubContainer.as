package com.alleni.author.view.ui
{
	import assets.icons.inspector.*;
	
	import caurina.transitions.Tweener;
	
	import com.alleni.author.controller.ui.DragDescription;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.application.NotificationNamesEditor;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.ForwardingWireAnchor;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;

	public class

    RibbonSubContainer extends Sprite
	{
		public static const TIME:Number = 0.25;
		public static const TRANSITION:String = "easeOutQuad";
		
		protected var _maxHeight:Number = 0.05;
		protected var _minHeight:Number = 0;
		protected var _header:RibbonContainerHeading;
		protected var _label:String;
		protected var _color:uint = 0;
		
		private var _mainContainer:Sprite;
		private var _mask:Sprite;
		private var _zeroMask:Sprite;
		private var _open:Boolean = false;
		private var _showHeader:Boolean = false;
		private var _messageCenterButton:MenuButton;
		private var _propertyInspectorButton:MenuButton;
		private var _messageCenterVisible:Boolean = false;
		private var _propertyInspectorVisible:Boolean = false;
		private var _w:Number = MessageCenterView.WIDTH;
		
		private var _messageCenter:MessageCenterView;
		private var _ribbons:IList;
		protected var _ribbonViews:Vector.<RibbonView>;
		private var _cachedRibbonViews:Dictionary = new Dictionary;
		private var _alwaysShowingRibbons:Vector.<RibbonView>;
		private var _childContainers:Vector.<RibbonChildContainer>;

		private var _tweenCallback:Function;
		private var _viewContext:ViewContext;
		private var _role:uint;
		private var _timer:Timer;
		private var _type:String;
		private var _perceivedHeight:Number;
		private var _wasOpen:Boolean = false;

        private var _mcButton:mcWhite = new mcWhite();
        private var _inspectorButton:inspectorWhite = new inspectorWhite();

		private var _inspector:Boolean = false;
        private var _tweenTime:Number = NaN;
		
		public function RibbonSubContainer(tweenCallback:Function, viewContext:ViewContext, role:uint, label:String, color:uint, type:String, messageCenterView:MessageCenterView, inspector:Boolean, cachedRibbonViews:Dictionary= null,wasOpen:Boolean=false, tweentime:Number = NaN)
		{
			super();
			
			this.y = RibbonContainer.PAD;

            _tweenTime = tweentime;
			_inspector = inspector;
//			_wasOpen = true;
			
			if (cachedRibbonViews != null)
				_cachedRibbonViews = cachedRibbonViews;
			_ribbonViews = new Vector.<RibbonView>();
			_alwaysShowingRibbons = new Vector.<RibbonView>();
			_childContainers = new Vector.<RibbonChildContainer>();
			
			_label = label;
			_color = color;
			_type = type;
			_tweenCallback = tweenCallback; // RibbonContainer, updateContainerPositions()
			_viewContext = viewContext;
			_role = role;
			_messageCenter = messageCenterView;
            _wasOpen = wasOpen;
			
			setupContainerHeading();
			
			_mainContainer = new Sprite();
			_mainContainer.y = 0;//HEADER_HEIGHT;
			_mask = new Sprite();
			_zeroMask = new Sprite();
			_mask.y = 0; //HEADER_HEIGHT;
			_mainContainer.mask = _mask;
            addChild(_zeroMask);
			addChild(_mask);
			addChild(_mainContainer);
						
			_timer = new Timer(800, 1);
			_timer.addEventListener(TimerEvent.TIMER, handleTimerComplete);

			_perceivedHeight = 0;
			_header.openSubcontainer(false);
			
			if (libraryButtonsShowing) {
				_messageCenterButton = new MenuButton(drawMessageCenterBtn,setMessageCenterVisible, [true], setMessageCenterVisible, [false],null, new Point(2,1));
				_messageCenterButton.borderColor = 0x666666;
				_messageCenterButton.borderDownColor = 0xCCCCCC;
				_messageCenterButton.toggle = true;
				_messageCenterButton.selected = false;
				addChild(_messageCenterButton);
				
				_propertyInspectorButton = new MenuButton(drawPropertyInspectorBtn,setPropertyInspectorVisible, [true], setPropertyInspectorVisible, [false],null, new Point(2,1));
				_propertyInspectorButton.x = _messageCenterButton.width-2;
				_propertyInspectorButton.borderColor = 0x666666;
				_propertyInspectorButton.borderDownColor = 0xCCCCCC;
				_propertyInspectorButton.toggle = true;
				_propertyInspectorButton.selected = false;

		        addChild(_propertyInspectorButton);

                refreshToggleButtons();
			}

//			if (_label == Modifiers.SETTERS || label == Modifiers.TRIGGERS || label == Modifiers.COLLIDE || label == Modifiers.DRAG_N_DROP) {
//				_open = true;
//				_showHeader = true; // TODO: This is a temporary fix to make sure containers added after the initial creation of the MC (i.e., the setter container) doesn't hide the header by default.
//			}

			draw();
        }

		public function refreshToggleButtons():void {
			if (_messageCenterButton) {
				_messageCenterButton.selected = messageCenterVisible;
                _messageCenterButton.visible = libraryButtonsShowing;
            }
			if (_propertyInspectorButton) {
				_propertyInspectorButton.selected = propertyInspectorVisible;
                _propertyInspectorButton.visible = propertyInspectorButtonShowing;
            }

		}

        public function get libraryButtonsShowing():Boolean {
            return Application.instance.ribbonLibraryVisible && _inspector;
        }

        public function get propertyInspectorButtonShowing():Boolean {
            return libraryButtonsShowing && _type == MessageCenterView.PROPERTIES;
        }

        public function get inspector():Boolean {
            return _inspector;
        }

        public function drawMessageCenterBtn(sprite:Sprite):void {
            sprite.addChild(_mcButton);
            _mcButton.x = 5;
            _mcButton.y = 2;
        }

        public function drawPropertyInspectorBtn(sprite:Sprite):void {
            sprite.addChild(_inspectorButton);
            _inspectorButton.x = 5;
            _inspectorButton.y = 2;
        }
		
		public function get propertyInspectorVisible():Boolean {
			var allChecked:Boolean = true;
			var someChecked:Boolean = false;
			for each(var wireAnchor:WireAnchor in this.ribbons) {
				allChecked = allChecked && wireAnchor.propertyInspectorVisible;
				someChecked = someChecked || wireAnchor.propertyInspectorVisible;
			}
			//			if (!someChecked)
			//				_messageCenterVisible = false;
			_propertyInspectorVisible = someChecked;
			return _propertyInspectorVisible;			
		}
		public function setPropertyInspectorVisible(visible:Boolean):void {
			var hasWiredAnchor:Boolean = false;
			for each (var anchor:WireAnchor in this.ribbons) {
				var wiredReferences:Boolean = (anchor as ForwardingWireAnchor) != null && (anchor as ForwardingWireAnchor).hasWiredReferences();
				hasWiredAnchor = hasWiredAnchor || anchor.wired || wiredReferences;
				var ribbonView:RibbonView = _cachedRibbonViews[anchor.modifierDescription.key];
				if (ribbonView != null) {
					ribbonView.setPropertyInspectorVisible(visible || anchor.wired || wiredReferences);
				} else {
					anchor.propertyInspectorVisible = visible || anchor.wired || wiredReferences;
				}
			}
			_propertyInspectorButton.selected = visible;
		}
		
		
		public function get messageCenterVisible():Boolean {
			var allChecked:Boolean = true;
			var someChecked:Boolean = false;
			for each(var wireAnchor:WireAnchor in this.ribbons) {
				allChecked = allChecked && wireAnchor.messageCenterVisible;
				someChecked = someChecked || wireAnchor.messageCenterVisible;
			}
			//			if (!someChecked)
			//				_messageCenterVisible = false;
			_messageCenterVisible = someChecked;
			return _messageCenterVisible;			
		}
		
		public function setMessageCenterVisible(visible:Boolean):void {
			var hasWiredAnchor:Boolean = false;
			for each (var anchor:WireAnchor in this.ribbons) {
				var wiredReferences:Boolean = (anchor as ForwardingWireAnchor) != null && (anchor as ForwardingWireAnchor).hasWiredReferences();
				hasWiredAnchor = hasWiredAnchor || anchor.wired || wiredReferences;
				var ribbonView:RibbonView = _cachedRibbonViews[anchor.modifierDescription.key];
				if (ribbonView != null) {
					ribbonView.setMessageCenterVisible(visible || anchor.wired || wiredReferences);
				} else {
					anchor.messageCenterVisible = visible || anchor.wired || wiredReferences;
				}
			}
		_messageCenterButton.selected = visible;
		}
		
		private function handleTimerComplete(e:TimerEvent):void
		{
			var drag:DragDescription = Application.instance.wireDragDescription;
			if (!drag)
				drag = Application.instance.assetDragDescription;
			if (drag && drag.containerShouldOpen(_type)) {
				if (_open) {
					close();
					_wasOpen = false;
				}
				else {
					open();
					_wasOpen = true;
				}
			}
		}
		
		protected function get headerHeight():Number
		{
			return 12 + (_header? _header.buttonPadding : 0);
		}
		
		private function draw():void
		{
			graphics.clear();
			
			_mask.graphics.clear();
			_mask.graphics.beginFill(0x330000, 0.1);
			// Note: we must draw the mask with a height greater than 0 to be able to set the height of it later.
			_mask.graphics.drawRect(0, 0, _w, _maxHeight);
		}
		
		protected function setupContainerHeading():void
		{
			// Create header
			_header = new RibbonContainerHeading(_color, _label);
			_header.x = (_inspector? 15: 0);
			_header.visible = _open;
			addChild(_header);
			
			_header.addEventListener(MouseEvent.CLICK, handleHeadingClick, false, 0, true);
			_header.addEventListener(MouseEvent.ROLL_OVER, handleHeadingRollOver, false, 0, true);
			_header.addEventListener(MouseEvent.ROLL_OUT, handleHeadingRollOut, false, 0, true);
		}
		
		
		protected function handleHeadingClick(e:MouseEvent):void
		{
			if (_open) {
				close();
				_wasOpen = false;
			} else {
				open();
				_wasOpen = true;
			}
		}
		
		protected function handleHeadingRollOver(e:MouseEvent):void 
		{
			_timer.reset();
			_timer.start();
		}
		
		protected function handleHeadingRollOut(e:MouseEvent):void 
		{
			_timer.stop();		
		}
		
		protected function open(closeOthers:Boolean = true):void
		{
			if(_messageCenter!=null && closeOthers) {
				_messageCenter.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.CLOSE_UNFOCUSED_RIBBON_CONTAINERS,this));
			}

            if (_messageCenter!=null) {
                messageCenter.nowOpening.add(this);
            }

			_open = true;
			if (_minHeight > _maxHeight)
				_maxHeight = _minHeight;
			addAllRibbonViews();
			_header.openSubcontainer(true);

			openChildContainers();
			if(_messageCenter!=null) {
				_messageCenter.addEventListener(NotificationNamesEditor.CLOSE_UNFOCUSED_RIBBON_CONTAINERS,closeIfNotParent);
			}
		}
		
		private function highlightShowingRibbons(value:Boolean):void
		{
			for each (var ribbon:RibbonView in alwaysShowingRibbons) {
				ribbon.highlight(value);
			}
		}
		
		// TODO: close should not rely on tween
		public function closeOnLoad():void
		{
			close();
		}
		
		private function closeIfNotParent(e:ApplicationEvent):void {
			if (this.contains(e.data as DisplayObject)) {
				return;
			} else {
				if(_messageCenter!=null) {
					_messageCenter.removeEventListener(NotificationNamesEditor.CLOSE_UNFOCUSED_RIBBON_CONTAINERS,closeIfNotParent);
				}
				close();
			}
		}
		
		protected function  close(tween:Boolean=true):void
		{
			removeNotShowingRibbonViews();
			
			_header.openSubcontainer(false);
			_open = false;
		}
		
		// Called externally from the message center level
		public function openAll(closeOthers:Boolean = true):void
		{
            showHeader = _ribbons.length > 0;

			if (_wasOpen /*|| _label == Modifiers.SETTERS || label == Modifiers.TRIGGERS || label == Modifiers.COLLIDE || label == Modifiers.DRAG_N_DROP*/) { // ALways opening setter category will ensure that dynamically added setters show properly
				open(closeOthers);
			}
		}

        public function openThis(closeOthers:Boolean = true):void
        {
            showHeader = _ribbons.length > 0;

            open(closeOthers);
        }

		
		public function closeAll():void
		{
			showHeader = false;	 // Set this flag so we can hide headers when tween is complete
			close();
		}
		
		public function headerShowing(visible:Boolean = true):void {
            _mainContainer.mask = _mask;
		}
		
		private function handlePostOpen():void
		{
            if (_messageCenter) {
                _messageCenter.nowOpening.remove(this);
            }

			handleOpenUpdate();
			highlightShowingRibbons(false);
		}
		
		public function removeRibbon(ribbon:WireAnchor, childAnchor:WireAnchor):void
		{
			//TODO remove _ribbons array as redundant
			var i:int = _ribbons.getItemIndex(ribbon);
			if (i < 0) {
				if (alwaysShowingRibbons.length == 0) {
					showHeader = false;
				}
				return;
			}

//			if (ribbon.wired) {
//				for each (var wire:Wire in ApplicationController.instance.wireController.getWiresForAnchor(ribbon)) {
//					ApplicationController.instance.wireController.deleteWire(wire);
//				}
//			}
			if(childAnchor == null && ribbon.hostObject != null && ribbon.hostObject.anchors[ribbon.modifierDescription.key] == null ) {
				_ribbons.removeItemAt(i);
			}
			var k:int = _ribbonViews.length;
			for (i=0; i < k; i++) {
				var view:RibbonView = _ribbonViews[i];
				if (view.model.value == ribbon) {
					if(childAnchor == null && (ribbon.childAnchors != null) || (!ribbon.alwaysShow && !_open)) {
						removeRibbonView(view);
					} else if(childAnchor != null && view.childContainer.isOpen == false) {
						view.childContainer.removeRibbon(childAnchor, null);
					}
					return;
				}
			}
			if (alwaysShowingRibbons.length == 0) {
				showHeader = false;
			}
			return;

		}			
		
		public function get ribbonViews():Vector.<RibbonView> {
			return _ribbonViews;
		}
		
		public function addRibbon(ribbon:WireAnchor, display:Boolean, childAnchor:WireAnchor):void
		{
			if (_ribbons == null)
				_ribbons = new ArrayCollection();

            if (_ribbons.getItemIndex(ribbon) != -1) {
                if(_open || ribbon.alwaysShow) {
                    addRibbonView(ribbon, childAnchor);
                }
                return;
            }
			_ribbons.addItem(ribbon);

			if(_open || ribbon.alwaysShow) {
				addRibbonView(ribbon, childAnchor);
			}
			if (parent as RibbonContainer && (parent as RibbonContainer).open)
				showHeader = true;

			if(display) {
				_wasOpen = true;
				openAll(false);
			}
			
			refreshToggleButtons()
		}

		public function removeNotShowingRibbonViews():void {
			for each(var ribbonView:RibbonView in _ribbonViews.slice()) {
				if(ribbonView.ribbon.alwaysShow == false) {
					removeRibbonView(ribbonView);
				}
			}
		}

		public function removeRibbonView(view:RibbonView):void {
			var i:int = _ribbonViews.indexOf(view);
			if(i >= 0) {
                if (!_inspector) // views in the inspector are cached.  Don't remove their listeners.
				    view.warnViewToDelete();
                view.parentSubcontainer; // make sure the current subcontainer is set in case someone wants to interact with it when the subcontainer is closed.
				_mainContainer.removeChild(view);
				var idx:int = _childContainers.indexOf(view.childContainer);
				if(idx >= 0) {
					_childContainers.splice(idx, 1);
				}
				maneuverRibbonViewInOrder(view, false);
			}
		}

		public function addRibbonView(anchor:WireAnchor, childAnchor:WireAnchor):void {
			if(!_ribbonViews.every(function(view:RibbonView, index:int,  vector:Vector.<RibbonView>):Boolean {return view.ribbon.modifierDescription.key != anchor.modifierDescription.key})) {
				return;
			}
			var ribbonView:RibbonView = null;
			if(_inspector) {
				ribbonView = _cachedRibbonViews[anchor.modifierDescription.key] as RibbonView;
                var ribbonLibraryVisible:Boolean = Application.instance.ribbonLibraryVisible;
			}
			if (ribbonView == null) {
				ribbonView = new RibbonView(_viewContext, anchor.model, _role, this, _messageCenter != null);
				ribbonView.initialize();
				if(_inspector) {
					_cachedRibbonViews[anchor.modifierDescription.key] = ribbonView;
				}
			} else {
                ribbonView.showVisibilityButtons(ribbonLibraryVisible);
            }
			_mainContainer.addChild(ribbonView);
			maneuverRibbonViewInOrder(ribbonView, true);  // inserts view into _ribbonViews array
			if(childAnchor) {
				ribbonView.childContainer.addRibbonView(childAnchor, null);
			}
		}

		public function addAllRibbonViews():void {
			for each(var anchor:WireAnchor in _ribbons) {
				addRibbonView(anchor, null);
			}
		}

        public function reorderRibbonView(view:RibbonView):void {
            maneuverRibbonViewInOrder(view,  false);
            maneuverRibbonViewInOrder(view, true);
        }

        private function compareAnchors(lhs:WireAnchor, rhs:WireAnchor):int {
            if(lhs.modifierDescription.weight < rhs.modifierDescription.weight) {
                return -1;
            } else if(lhs.modifierDescription.weight > rhs.modifierDescription.weight) {
                return 1;
            } else {
                var l:String = lhs.label?lhs.label:lhs.modifierDescription.label;
                var r:String = rhs.label?rhs.label:rhs.modifierDescription.label;
                return l.localeCompare(r);
            }
        }
		
		private function maneuverRibbonViewInOrder(view:RibbonView, inserting:Boolean):void
		{
//			trace("view="+view.ribbon.modifierDescription.label);
			var y:int = 0;
			var maneuvered:Boolean = false;
			if(_ribbonViews.length != 0) {
				var len:int = _ribbonViews.length;
				for (var n:int; n < len; n++) {
					var viewN:RibbonView = _ribbonViews[n] as RibbonView;
					var anchorN:WireAnchor = viewN.model.value as WireAnchor;
					if(!maneuvered) {
						if (inserting && compareAnchors(view.ribbon, anchorN) < 1) {
							_ribbonViews.splice(n, 0, view);
							viewN = view;
							maneuvered = true;
							len++;
						} else if(!inserting && viewN == view) {
							_ribbonViews.splice(n, 1);
							len--;
							n--;
							maneuvered = true;
							continue;
						}
					}

					viewN.y = y;
					viewN.visible = true;
					y += viewN.height;
				}
			}
			if(inserting && !maneuvered) {
				_ribbonViews.push(view);
				view.y = y;
				view.visible = true;
				y += view.height;
			}
			_maxHeight = y;
			_minHeight = _maxHeight;
			tweenMask();
		}
		
		private function tweenMask():void{
			Tweener.addTween(_mask, {height:_maxHeight, time:tweenTime, transition:TRANSITION, onComplete:handlePostOpen, onUpdate:handleOpenUpdate});
		}
		
		private function handleOpenUpdate():void
		{
			updatePerceivedHeight();
			updateAnchors();
		}
		
		public function updateMaxHeight(ribbonView:RibbonView):void
		{
			adjustRibbonPositions();
			_mask.height = _maxHeight + ribbonView.height //- 15;
			updatePerceivedHeight();
		}
		
		public function handleWireRemoved():void
		{
			if (alwaysShowingRibbons.length == 0 && _open == false) {
				close();
				if (parent as RibbonContainer) {
					RibbonContainer(parent).handleWireRemoved();
				}
			}
		}
		
		public function adjustRibbonPositions():void
		{
			var yyy:Number = 0;
			var n:int;
			var k:int = _ribbonViews.length;
			for (n=0; n < k; n++) {
				var ribbonView:RibbonView = _ribbonViews[n];
				ribbonView.y = yyy;
				if (this._open || ribbonView.ribbon.alwaysShow) {
					yyy += ribbonView.height;
					ribbonView.visible = true;
				}
				ribbonView.adjustChildRibbons();
			}
		}

        public function handleSubContainerCallback():void
        {
            var newHeight:Number = 0;
            var n:int;
            var k:int = _ribbonViews.length;
            for (n = 0; n < k; n++) {
                var ribbonView:RibbonView = _ribbonViews[n];
                newHeight += ribbonView.height;
                // update the y values of ribbons below childContainer
                if (n > 0) {
                    var lastView:RibbonView = _ribbonViews[n-1];
                    ribbonView.y = lastView.height + lastView.y;
                } else {
                    ribbonView.parent.y = (_showHeader?RibbonContainerHeading.TAB_HEIGHT:0);
                }
            }

            // update the height of this container
            _mask.height = newHeight;
			_maxHeight = newHeight;
            updatePerceivedHeight();
			tweenMask();
        }

		protected function updatePerceivedHeight():void
		{
			_perceivedHeight = _mask.y + _mask.height;
            if (_perceivedHeight < 2) {
                _perceivedHeight = 0;
            }
			doCallback();
		}
		
		protected function doCallback():void
		{
			_tweenCallback.call();
		}
		
		public function get perceivedHeight():Number
		{
			return _perceivedHeight;
		}
		
		protected function set showHeader(value:Boolean):void
		{
			_showHeader = value;

//            headerShowing(value);
			var yPosition:Number = value ? headerHeight : 0;
			_mask.y = yPosition;
			_mainContainer.y = yPosition;
			_header.visible = value;
			updatePerceivedHeight();
		}
		
		public function set ribbons(value:IList):void
		{
			_ribbons = value;
		}
		
		public function get ribbons():IList
		{
			return _ribbons;
		}
		
		public function get label():String
		{
			return _label;
		}
		
		public function compareTo(other:RibbonSubContainer):Number 
		{
			// TODO: type these properly. int or number?
			var w:Number = Modifiers.instance.categoryWeight(_label);
			var ow:Number = Modifiers.instance.categoryWeight(other._label);
			if (w == ow)
				return _label.localeCompare(other._label);
			else if (w < ow)
				return -1;
			return 1;
		}
		
		/**
		 * Update the yContainer values of the wire anchors, so wires will draw accurately
		 * to the port locations. 
		 * 
		 */
		public function updateAnchors():void 
		{
			for each (var ribbonView:RibbonView in _ribbonViews) {
				ribbonView.updateAnchors(_showHeader);
			}
		}
		
		public function get alwaysShowingRibbons():Vector.<RibbonView>
		{
			_minHeight = 0;
			_alwaysShowingRibbons.splice(0, _alwaysShowingRibbons.length);
			for each (var ribbonView:RibbonView in _ribbonViews) {
				var anchor:WireAnchor = WireAnchor(ribbonView.model.value);
				if (ribbonView.ribbon.alwaysShow || anchor.wired) {
					_alwaysShowingRibbons.push(ribbonView);
					_minHeight += ribbonView.height;
				}
			}
			return _alwaysShowingRibbons;
		}
		
		public function get isOpen():Boolean
		{
			return _open;
		}
		
		// for debugging
		private function printRibbonViews():void
		{
			trace("RibbonSubContainer::ribbons for "+_label+":");

			for each (var rv:RibbonView in _ribbonViews) {
				trace("		"+rv.ribbon.modifierDescription.label);
			}
		}
		
		
		/** The following code handles child ribbon containers **/
		
		
		public function addSubContainer(ribbon:RibbonView):RibbonSubContainer
		{
			var label:String = ribbon.ribbon.modifierDescription.label;
			var sc:RibbonChildContainer = new RibbonChildContainer(this, ribbon, _viewContext, _role, label, 0x646464, "inlets");
			_childContainers.push(sc);
			
			return sc;
		}
		
		private function openChildContainers():void
		{
			for each (var container:RibbonChildContainer in _childContainers) {
				container.openAll();
			}
		}
		
		private function closeChildContainers():void
		{
			for each (var container:RibbonChildContainer in _childContainers) {
				container.closeAll();
			}
		}
		
		override public function get height():Number
		{
			return perceivedHeight;
		}
		
		public function get messageCenter():MessageCenterView {
			return _messageCenter;
		}

		public function makeItRight():void {
			for each (var container:RibbonChildContainer in _childContainers) {
				container.makeItRight();
			}
			if(this._open) {
				showHeader = false;
				close();
				showHeader = true;
				open();
			} else {
				showHeader = true;
				open();
				showHeader = false;
				close();
			}
		}
		
		public function ribbonUnderPoint(stagePoint:Point):RibbonView
		{
			var ribbon:RibbonView = null;
            var n:int;
			for (n = _ribbonViews.length-1; n >= 0; n--) {
				var testView:RibbonView = _ribbonViews[n];
				if (testView.visible) {
					var anchor:WireAnchor = testView.model.value as WireAnchor;
					// skip setters, since their hit-test would obscure the children
					if (anchor.childAnchors.length == 0 && testView.hitTestPoint(stagePoint.x, stagePoint.y)) {  
						return testView;
					}
				}
			}
			
			var len:int = _childContainers.length;
			for (n = 0; n < len; n++) {
				var container:RibbonChildContainer = _childContainers[n];
				testView = container.ribbonUnderPoint(stagePoint);
				if (testView)
					return testView;
			}
			return null;
		}
		
        public function get parentContainer():RibbonContainer {
            return (parent as RibbonContainer);
        }

        private function get tweenTime():Number {
            if (parentContainer && parentContainer.tweenTime) {
                return  parentContainer.tweenTime;
            } else if (!isNaN(_tweenTime)) {
                return _tweenTime;
            } else {
                return TIME;
            }
        }

		public function get category():String
		{
			var result:String = null;
			var anchor:WireAnchor;
			if(_ribbons.length) 
			{
				anchor = _ribbons[0];
				// all the ribbon in the subcontainer are same category, so just look at first one
				result = anchor.modifierDescription.category;
			}
			return result;
		}
	}
}
