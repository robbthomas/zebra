package com.alleni.author.view.ui.editors
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.feedback.TransformationGhost;
	import com.alleni.author.view.ui.AbstractRibbonControl;
	import com.alleni.author.view.ui.controls.TransformationEditorPanel;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class TransformationEditor extends AbstractRibbonControl
	{
		private const OUTLINE_COLOR:uint = 0x646464;
		private const BACKGROUND_COLOR:uint = 0x000000;
		private const CROSS_COLOR:uint = 0xFFFFFF;
		private const CROSS_SIZE:int = 7;
	
		private var _chipSize:Object = {width:35, height:12};
		private var _valueHandler:Object;
		private var _value:PositionAndAngle;
		private var _locationCross:Sprite = new Sprite();
	
		private var _ghost:TransformationGhost = null;
		private var _ghostType:Class = TransformationGhost;
		private var _parentView:ObjectView = null;
		private var _object:AbstractObject;
		
		private var _feedBackView:Sprite;
		private var _propertyDescription:PropertyDescription;

		private var _token:*;
	
		public function TransformationEditor(setValueCallback:Function, object:AbstractObject, w:Number=35, h:Number=12, propertyDescription:PropertyDescription=null)
		{
			super(setValueCallback);
			_feedBackView = WorldContainer(Application.instance.viewContext).feedbackHandlesView;
			_propertyDescription = propertyDescription;
			_object = object;
			
			_chipSize.width = w;
			_chipSize.height = h - 1;
			
			draw();
			drawCross();
			transformation = new PositionAndAngle(50, 50, 0);
	
			this.addEventListener(MouseEvent.CLICK, mouseClickListener);
		}
		
		override public function setValue(value:Object, valueType:String):void{
			_parentView = (Application.instance.viewContext as WorldContainer).worldView as ObjectView;
			_value = value as PositionAndAngle;
			transformation = _value;
		}
		
		public function transformationFromGhost(value:PositionAndAngle):void 
		{
			transformation = value;
			valueChanged(_value);
		}
	
		private function transformationFromPanel(value:PositionAndAngle):void 
		{
			transformation = value;
			valueChanged(_value);
		}
	
		private function panelClosed(cancel:Boolean):void
		{
			_ghost.parent.removeChild(_ghost);
			_ghost = null;
			if(_token != null) {
				if(cancel) {
					ApplicationController.currentActionTree.cancelGroup(_token);
				} else {
					ApplicationController.currentActionTree.closeGroup(_token);
				}
				_token = null;
			}
		}
		
		public function set ghostClass(type:Class):void
		{
			_ghostType = type;
		}
	
		private function mouseClickListener(e:MouseEvent):void 
		{
			if(_ghost == null) {
			
				_ghost = new _ghostType(this, _value,  _object);
				_feedBackView.addChild(_ghost);
				_token = ApplicationController.currentActionTree.openGroup("Pick position");
				var pt:Point = EditorUI.instance.globalToLocal(localToGlobal(new Point(_chipSize.width/2, _chipSize.height)));
				TransformationEditorPanel.showPanel(EditorUI.instance, _value, transformationFromPanel, panelClosed, pt, _ghost);
			}
		}
	
		private function drawCross():void
		{
			_locationCross.graphics.lineStyle(1, CROSS_COLOR);
			_locationCross.graphics.moveTo(-CROSS_SIZE/2, 0);
			_locationCross.graphics.lineTo(CROSS_SIZE/2, 0);
	
			_locationCross.graphics.lineStyle(1, CROSS_COLOR);
			_locationCross.graphics.moveTo(0, -CROSS_SIZE/2);
			_locationCross.graphics.lineTo(0, CROSS_SIZE/2);
			addChild(_locationCross);
		}
	
		private function draw():void
		{
			// draw background
			graphics.clear();
			graphics.lineStyle(1, OUTLINE_COLOR);
			graphics.beginFill(BACKGROUND_COLOR);
			graphics.drawRect(0,0,_chipSize.width,_chipSize.height);
			graphics.endFill();
	
			// draw middle guidelines
			graphics.lineStyle(1, OUTLINE_COLOR);
			graphics.moveTo(0, _chipSize.height/2);
			graphics.lineTo(_chipSize.width, _chipSize.height/2);
	
			graphics.lineStyle(1, OUTLINE_COLOR);
			graphics.moveTo(_chipSize.width/2, 0);
			graphics.lineTo(_chipSize.width/2, _chipSize.height);
	
			this.scrollRect = new Rectangle(0, 0, _chipSize.width, _chipSize.height);
		}
			
		override public function set height(value:Number) : void
		{
			_chipSize.height = value;
			if(_parentView) {
				_locationCross.x = _value.x / _parentView.object.width * _chipSize.width;
			} else {
				_locationCross.x = -100;
			}
			draw();
		}
	
		override public function set width(value:Number) : void
		{
			_chipSize.width = value;
			if(_parentView) {
				_locationCross.y = _value.y / _parentView.object.height * _chipSize.height;
			} else {
				_locationCross.y = -100;
			}
			draw();
		}
	
		public function set parentView(value:ObjectView):void 
		{
			if(_parentView == value) {
				return;
			}
			_parentView = value;
			if(_ghost) {
				if(_ghost.parent) {
					_ghost.parent.removeChild(_ghost);
				} if(_parentView) {
					_feedBackView.addChild(_ghost);
				}
			}
			if(_parentView) {
				_locationCross.x = _value.x / _parentView.object.width * _chipSize.width;
				_locationCross.y = _value.y / _parentView.object.height * _chipSize.height;
			} else {
				_locationCross.x = -100;
				_locationCross.y = -100;
			}
		}
	
		public function set transformation(value:PositionAndAngle):void
		{
			if (value == null)
				return;
			
			_value = value;
			if(_parentView) {
				_locationCross.x = _value.x / _parentView.object.width * _chipSize.width;
				_locationCross.y = _value.y / _parentView.object.height * _chipSize.height;
			} else {
				_locationCross.x = -100;
				_locationCross.y = -100;
			}
			_locationCross.rotation = _value.angle;
			if(_ghost) {

				_ghost.transformation = _value;
				
				TransformationEditorPanel.instance.transformation = _value;
			}
		}
	}
}