package com.alleni.author.view.ui.controls
{
	import com.alleni.author.view.text.LightLabel;
	
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLRequest;
	
	public class MenuItem extends Sprite
	{
		//CONSTANTS
		public static const TYPE_SEPERATOR:String = "Seperator";
		public static const TYPE_LABEL_ONLY:String = "Label Only";
		public static const TYPE_ICON_ONLY:String = "Icon Only";
		public static const TYPE_COMBO:String = "Icon and Label";
		
		public static const RESPONSE_SELECTABLE:String = "Response Selectable";
		public static const RESPONSE_ACTION:String = "Response Action";
		
		public static const HOTKEY_MODIFER_SHIFT:String = "SHIFT+";


		//Item Properties
		public var type:String;
		public var icon:Object;
		public var hotKey:String;
		public var menuWidth:Number = 75;
		public var labelSize:Number;
		public var checkMarkScale:Number = 0.5;
		public var data:Object;
		public var response:String = RESPONSE_SELECTABLE;
		public var isCustomAnchorPosition:Boolean = false;
		
		private var _label:String = " ";
		private var _labelStartX:Number = 11;
		private var _labelStartY:Number = 0;
		private var _iconStartX:Number = 11;
		
		//Item State
		private var _itemHilite:Boolean = false;
		private var _selected:Boolean = false;
		// toggle for multiple select such as a window menu wherein check marks indicate enabled properties
		//  not currently utilized by PopOutMenu.
		private var _toggled:Boolean = false;
		
		//Menu Item Heights
		private var _currentHeight:Number = 20;		
		private var _iconTypeHeight:Number = 24;
		private var _seperatorTypeHeight:Number = 10;
		private var _labelTypeHeight:Number = 20;
		
		//Menu Item Colors
		private var _itemBackground:int = 0xCCCCCC;
		private var _itemHiliteBackground:int = 0x505050;
		private var _textColor:int = 0x111111;
		private var _textHiliteColor:int = 0xFFFFFF;
		
		//Item View Objects
		private var _labelText:LightLabel = new LightLabel();
		private var _hotKeyLabel:LightLabel = new LightLabel();
		private var _checkMark:CheckMark;
		
		public function MenuItem()
		{
			super();
		}
		
		public function buildMenuItem():void
		{
			graphics.beginFill(_itemBackground);
			switch (this.type){
				case TYPE_LABEL_ONLY:
					_currentHeight = _labelTypeHeight;
					graphics.drawRect(1,0,(this.menuWidth - 2), _currentHeight);
					height = _labelTypeHeight;
					_labelStartX = 15;
					buildLabelItem();
					break;
				case TYPE_COMBO:
					_currentHeight = _iconTypeHeight;
					graphics.drawRect(1,0,(this.menuWidth - 2), _currentHeight);
					_iconStartX = 20;
					buildComboItem();
					break;
			}
		}
		
		//BUILD OUT ITEMS
		private function buildLabelItem():void
		{	
			_labelText.text = _label;
			_labelText.size = this.labelSize;
			_labelText.color = _textColor;
			_labelText.width = this.menuWidth - 11;
			_labelText.x = _labelStartX;
			_labelText.y = _labelStartY;
			_labelText.mouseEnabled = false;
			addChild(_labelText);
			
			if (this.hotKey)
				buildHotKey();
		}
		
		private function buildComboItem():void
		{
			var itemIcon:DisplayObject;
			if (this.icon as String) {
				itemIcon = new Loader();
				Loader(itemIcon).load(new URLRequest(this.icon as String));
				Loader(itemIcon).contentLoaderInfo.addEventListener(Event.COMPLETE, function(event:Event):void {
					updateIconPosition(itemIcon);
				});
			} else if (this.icon as Class) {
				itemIcon = new this.icon();
			} else if (this.icon as DisplayObject) {
				itemIcon = this.icon as DisplayObject;
			} else {
				// nothing
			}
			
			if (itemIcon) {
				addChild(itemIcon);
				updateIconPosition(itemIcon);
			}
			_labelStartY = 2;
			_labelStartX = _iconStartX + 33;
			buildLabelItem();
		}
		
		private function updateIconPosition(icon:DisplayObject):void
		{
			icon.x = _iconStartX + (PopOutMenu.ICON_SIZE - icon.width) / 2;
			icon.y = (_labelTypeHeight - icon.height) / 2 
				// TODO: remove the following necessary compromise for the sake of a balanced UI.
				+ this.labelSize - 12; // an unfortunate piece of code required until this and PopOutMenu are adjusted a bit to become simpler and more flexible
		}
		
		private function buildHotKey():void
		{
			_hotKeyLabel.text = this.hotKey;
			_hotKeyLabel.size = this.labelSize;
			_hotKeyLabel.color = _textColor;
			_hotKeyLabel.width = 70;
			_hotKeyLabel.x = width - (_hotKeyLabel.width);
			_hotKeyLabel.y = _labelStartY;
			_hotKeyLabel.mouseEnabled = false;
			addChild(_hotKeyLabel);
		}
		
		//CONTROLS
		public function hiliteToggle():void
		{
			_itemHilite = !_itemHilite;
			graphics.clear();
			if (_itemHilite) {
				graphics.beginFill(_itemHiliteBackground);
				graphics.drawRect(1,0, (this.menuWidth - 2), _currentHeight);
				_labelText.color = _textHiliteColor;
				_hotKeyLabel.color = _textHiliteColor;
				if (_toggled) {
					_checkMark = new CheckMark();
					_checkMark.x = 5;
					_checkMark.y = 5;
					_checkMark.scaleX = this.checkMarkScale;
					_checkMark.scaleY = _checkMark.scaleX;
					this.addChild(_checkMark)
				} else {
					if (_checkMark && this.contains(_checkMark)) {
						this.removeChild(_checkMark);
						_checkMark = null;
					}
				}
			} else {
				graphics.beginFill(_itemBackground);
				graphics.drawRect(1,0, (this.menuWidth - 2), _labelTypeHeight);
				_labelText.color = _textColor;
				_hotKeyLabel.color = _textColor;
				if (_checkMark && this.contains(_checkMark)) {
					this.removeChild(_checkMark);
					_checkMark = null;
				}
			}
			graphics.endFill();
		}
		
		//GETTERS and SETTERS
		public function get label():String
		{
			return _label;
		}
		
		public function set label(value:String):void
		{
			_label = value;
			_labelText.text = _label;
		}
		
		public function get selected():Boolean
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void
		{
			if (this.response == RESPONSE_SELECTABLE) {
				_selected = value;
				if ((_selected && !_itemHilite) || (!_selected && _itemHilite))
					hiliteToggle();
			} else
				_selected = false;
		}
	}
}