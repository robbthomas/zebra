/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.ui
{
	import assets.icons.inspector.*;
	
	import caurina.transitions.Tweener;

import com.alleni.author.Navigation.SmartObject;

import com.alleni.author.Navigation.SmartObjectEditingMediator;

import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.objects.CompositeVelumMediator;
	import com.alleni.author.controller.objects.IEditingMediator;
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.RibbonDragMediator;
	import com.alleni.author.controller.ui.RibbonMediator;
	import com.alleni.author.definition.DoubleClickAction;
	import com.alleni.author.definition.InletDescription;
	import com.alleni.author.definition.InspectorPresets;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.definition.action.ModifyRibbonAction;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.definition.text.TextSelection;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.World;
import com.alleni.author.model.objects.TextObject;
import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.ExternalSmartWireAnchor;
	import com.alleni.author.model.ui.ForwardingWireAnchor;
import com.alleni.author.model.ui.InternalSmartWireAnchor;
import com.alleni.author.model.ui.MetaWireAnchor;
import com.alleni.author.model.ui.NumberValueControlConstraint;
import com.alleni.author.model.ui.PathNodeWireAnchor;
	import com.alleni.author.model.ui.SetterWireAnchor;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.feedback.PathNodeTransformationGhost;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
	import com.alleni.author.view.ui.editors.RibbonAssetEditor;
	import com.alleni.author.view.ui.editors.TransformationEditor;
	import com.alleni.taconite.event.ModelUpdateEvent;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.SelectableView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;

import flashx.textLayout.container.ScrollPolicy;

public class RibbonView extends SelectableView
	{
		public static const HEIGHT:Number = 15;
		public static const VALUE_WIDTH:Number = 38;
        public static const LABEL_TEXT_WIDTH:Number = 110;
        public static const LABEL_WIDTH:Number = 80;
		public static const CUSTOM_LABEL_WIDTH:Number = 75;
		public static const LABEL_COLOR:uint = 0xFFFFFF;
		public static const GAP:Number = 6;
		
		// Types of value settings
		public static const DEFAULT:String = "default";
		public static const INIT:String = "init";
		public static const CURRENT:String = "current";
		
		// formatting
		private static const GROUP_LABEL_COLOR:uint = LABEL_COLOR;
		public static const VALUE_X:Number = 96;
		public static const VALUE_Y:Number = 0;
		private static const LABEL_X:Number = 15;
		private static const BACKGROUND_COLOR:uint = 0x636060//0x383838;//0xFCD116;//0x636060;
		private static const BACKGROUND_ALPHA:Number = 0.8;

        private var _mcButton:mcWhite = new mcWhite();
        private var _inspectorButton:inspectorWhite = new inspectorWhite();

		private var _label:LightLabel;
		private var _editableLabel:LightEditableLabel;
        private var _labelRect:Rectangle = new Rectangle(0,0,LABEL_WIDTH,HEIGHT);

		private var _parentSubcontainer:RibbonSubContainer;
		
		// Ribbon Controls
		private var _valueControl:AbstractRibbonControl;
        private var _valueControlBackground:Sprite;
		private var _ribbonSlider:RibbonSlider;
		
		public var _backgroundAlpha:Number = 0.01; // Public so Tweener can tween it.
		private var _leftPort:PortView;
		private var _rightPort:PortView;
		private var _role:uint;
		private var _showPorts:Boolean;		
		private var _keyPressFunction:Function;
		private var _subcontainer:RibbonSubContainer;

		private var _h:Number = HEIGHT; // height of ribbon;
		
		private var _messageCenterButton:MenuButton = null;
		private var _propertyInspectorButton:MenuButton = null;
		private var _ribbonDragMediator:RibbonDragMediator;

		
		public function RibbonView(context:ViewContext, model:TaconiteModel, role:uint, container:RibbonSubContainer=null, showPorts:Boolean=true)
		{
			super(context, model);
			
			_context = context;
			_role = role;
			_parentSubcontainer = container;
			_showPorts = showPorts;

			this.alpha = 1;

			_keyPressFunction = function(charCode:uint):uint{
				return TextEditMediator.closeOnEnter(charCode);
			};

            _valueControlBackground = new Sprite();
            _valueControlBackground.graphics.beginFill(0x191919);
            _valueControlBackground.graphics.lineStyle(1,0x000000);
            _valueControlBackground.graphics.drawRect(-3,0,VALUE_WIDTH+3,HEIGHT);
            _valueControlBackground.graphics.endFill();
            _valueControlBackground.x = VALUE_X + ribbonLibraryBtnPadding;
            _valueControlBackground.y = VALUE_Y;

			if (ribbon as ExternalSmartWireAnchor) {
				_editableLabel = new LightEditableLabel(RibbonView.CUSTOM_LABEL_WIDTH, RibbonView.HEIGHT);
                _editableLabel.explicitLineBreak = true;
                _editableLabel.scrollRect = _labelRect;
                _editableLabel.expandable = false;
				var callBacks:Object = {
					okEditFunc:
					function(e:MouseEvent):int{
						return ribbon.hostObject.editing?DoubleClickAction.EDIT:DoubleClickAction.IGNORE;
					},
					updateFunc:null,
					keyPressFunc:_keyPressFunction,
					closeFunc:
					function(val:String):void{
						ribbon.label=_editableLabel.text=FormattedText.convertToString(val);
						_editableLabel.edit = false;
						ribbon.labelChanged = true;
					},
					hittestFunc:null
				};
				setUpParams(_editableLabel, callBacks);
				_editableLabel.embedFonts = true;
				_editableLabel.explicitLineBreak = true;
				if (ribbon.label != null) {
					_editableLabel.text = ribbon.label;
				} else {
					_editableLabel.text = ribbon.modifierDescription.label;
				}

				addChild(_editableLabel);
			} else {
				// Instantiate children
				_label = new LightLabel(LABEL_TEXT_WIDTH-(ribbon.childAnchors.length>0? 16 : 0), LABEL_COLOR, false, 15); // TODO: Labels should be smarter about their widths
				_label.text = ribbon.modifierDescription.label;
                _label.explicitLineBreak = true;
                if(ribbon.childAnchors.length > 0)
                    _labelRect.width -= 16;
                _label.scrollRect = _labelRect;
				// Format text
				if (ribbon.modifierDescription as PropertyDescription && PropertyDescription(ribbon.modifierDescription).type == Modifiers.FILL_TYPE)
					updateFillRibbonLabel();
				addChild(_label);
			}
			formatLabel();

			if (_showPorts) {
				_leftPort = new PortView(model.value as WireAnchor, _context as WorldContainer);
				_leftPort.angle = 180;
				_rightPort = new PortView(model.value as WireAnchor, _context as WorldContainer);

				// Position ports
				_leftPort.x = 0;
				_leftPort.y = 0;
				_rightPort.x = MessageCenterView.WIDTH - PortView.WIDTH + 1;
				_rightPort.y = 0;

				addChild(_leftPort);
				addChild(_rightPort);
			} else if (Application.instance.ribbonLibraryVisible) {
                addVisibilityButtons();
            }

			// Set height
			height = 0;


			if (ribbon.modifierDescription as InletDescription) {
				
				setKeyPressEditor();
				updateInletValue();
			}

			this.model.addEventListener(ModelUpdateEvent.MODEL_UPDATE, handleModelUpdate, false, 4);

			//trace("childAnchors="+ribbon.childAnchors.length);
			if (ribbon.childAnchors.length > 0) {
				addChildRibbons();
			}

			// Set the value component
			if (ribbon.modifierDescription as PropertyDescription) {
				setValueComponent();
				updateValue();
			}

            if(ribbon.host is WireAnchor == false && ribbon.host != null) {
                _ribbonDragMediator = new RibbonDragMediator(this,_ribbonSlider);
            } else {
                _ribbonDragMediator = null;
            }

			// Draw
			drawBackground();
		}

    private function addVisibilityButtons():void {
        _messageCenterButton = new MenuButton(drawMessageCenterBtn, setMessageCenterVisible, [true], setMessageCenterVisible, [false],null,new Point(2,1));
        _messageCenterButton.borderColor = 0x666666;
        _messageCenterButton.borderDownColor = 0xCCCCCC;
        _messageCenterButton.toggle = true;
        _messageCenterButton.selected = ribbon.messageCenterVisible;
        addChild(_messageCenterButton);

        _propertyInspectorButton = new MenuButton(drawPropertyInspectorBtn, setPropertyInspectorVisible, [true], setPropertyInspectorVisible, [false],null,new Point(2,1));
        _propertyInspectorButton.x = _messageCenterButton.width-2;
        _propertyInspectorButton.borderColor = 0x666666;
        _propertyInspectorButton.borderDownColor = 0xCCCCCC;
        _propertyInspectorButton.toggle = true;
        _propertyInspectorButton.selected = ribbon.propertyInspectorVisible;
        addChild(_propertyInspectorButton);

        showVisibilityButtons(true);
    }

		public function showVisibilityButtons(value:Boolean):void {
			if (_messageCenterButton && _propertyInspectorButton) {
				_messageCenterButton.visible = value;
				_propertyInspectorButton.visible = value && parentSubcontainer.propertyInspectorButtonShowing;
			} else if (parentSubcontainer.inspector && Application.instance.ribbonLibraryVisible) {
                addVisibilityButtons();
            }
            if (_messageCenterButton)
                _messageCenterButton.selected = ribbon.messageCenterVisible;
            if(_propertyInspectorButton)
                _propertyInspectorButton.selected = ribbon.propertyInspectorVisible;
            if (parentSubcontainer)
                parentSubcontainer.refreshToggleButtons();
		}
		
		public function drawMessageCenterBtn(sprite:Sprite):void {
			sprite.addChild(_mcButton);
            _mcButton.x = 5;
            _mcButton.y = 2;
		}

		public function drawPropertyInspectorBtn(sprite:Sprite):void {
            sprite.addChild(_inspectorButton);
            _inspectorButton.x = 5;
            _inspectorButton.y = 2;
        }

		public function setMessageCenterVisible(visible:Boolean):void {
			ribbon.messageCenterVisible = visible;
			if (_messageCenterButton)
				_messageCenterButton.selected = visible;
			if (parentSubcontainer != null) {
				parentSubcontainer.refreshToggleButtons();
			}
		}
		
		public function setPropertyInspectorVisible(visible:Boolean):void {
			ribbon.propertyInspectorVisible = visible;
			if (_propertyInspectorButton)
				_propertyInspectorButton.selected = visible;
			if (parentSubcontainer != null) {
				parentSubcontainer.refreshToggleButtons();
			}
		}
		
		public function get parentSubcontainer():RibbonSubContainer {
            if (parent)
	           _parentSubcontainer = parent.parent as RibbonSubContainer;
			return _parentSubcontainer;
		}
				
		public function get ribbon():WireAnchor
		{
			return model.value as WireAnchor;	
		}
		
		public function set value(val:Object):void
		{
			// set value with Undo support
			myValueFromControl = val;
		}

		
		private function setValueCallback(value:Object):void
		{
			myValueFromControl = value;
		}
		
		private function set myValueFromControl(value:Object):void
		{
			var oldValue:Object = ribbon.value;
			var fontSelectionChange:Boolean = false;
			var numObjects:int = 0;
			var preset:Boolean = false;

			if (ribbon as ForwardingWireAnchor) {
				oldValue = new Dictionary();
				var objs:Vector.<ITaconiteObject> = ForwardingWireAnchor(ribbon).selectedObjects;
				if (objs.length < 1) {
					return;
				} else if (objs.length == 1 && objs[0] is InspectorPresets) {
					preset = true;
				} else if (objs.length == 1 && objs[0] is TextSelection) {
                    fontSelectionChange = true;
                } else {
					for each (var obj:ITaconiteObject in objs) {
                        var ao:AbstractObject = obj as AbstractObject;
                        var composite:SmartObject = obj as SmartObject;
                        if(ao != null) {
                            if(ribbon.path in ao.anchors) {
                                var anchor:WireAnchor = ao.anchors[ribbon.path];
                                oldValue[obj] = anchor.value;
                            } else if (ao.hasOwnProperty(ribbon.hostProperty)) {
                               oldValue[obj] = obj[ribbon.hostProperty];
                            } else if (composite != null) {
                                oldValue[obj] = composite.externalAnchorForKey(ribbon.hostProperty).value;
                            }
                        } else {
                            oldValue[obj] = obj[ribbon.hostProperty];
                        }
						numObjects++;
					}
				}
			}
			
			if(PropertyDescription(ribbon.modifierDescription).type == Modifiers.FORMATTED_TEXT_TYPE){
				this.myValue = value;
			}
			else{
				//this.myValue = value;
				if(value as FormattedText){
					this.myValue = (value as FormattedText).toString();
				}
				else{
					this.myValue = value;
				}
			}
			if (fontSelectionChange) {
				// these changes are handled by the editor's undo history.
			} else if (preset) {
				// for now preset changes are not in the action tree
			} else if (oldValue as Dictionary) {
				var token:* = ApplicationController.currentActionTree.openGroup("Set " + ribbon.modifierDescription.label + " on " + numObjects);
				for (var key:Object in oldValue) {
					var ao:AbstractObject = key as AbstractObject;
					ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(ao, ribbon.path, oldValue[key]));
					logEdit(ao, oldValue[key]);
				}
				ApplicationController.currentActionTree.closeGroup(token);
			} else {
				ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(ribbon.hostObject, ribbon.path, oldValue));
				logEdit(ribbon.hostObject, oldValue);
			}
			
			// do side-effects when changing property ribbon in MC or Inspector, but NOT when editing a Setter ribbon
			if (ribbon.hostObject is AbstractObject && !(ribbon.host is WireAnchor)) {
				sideEffectsOfEdit(ribbon.hostObject, PropertyDescription(ribbon.modifierDescription).key, value);
			}
		}
		
		private function sideEffectsOfEdit(obj:AbstractObject, key:String, value:*):void
		{
			trace("SIDE EFFECT obj="+obj, "key="+key);
			switch (key) {
				case "lineColor":
					// change line thickness automatically so the selected line color can be seen
					if (obj.lineThickness == 0) {
                        for each(var abObj:TaconiteModel in ApplicationController.instance.authorController.cleanSelection){
                            if((abObj.value as AbstractObject).hasOwnProperty("lineThickness")){
                                (abObj.value as AbstractObject).lineThickness = 3;
                                ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject((abObj.value as AbstractObject), "lineThickness", 0));
                            }
                        }
					}
					break;
				case "fillSpec":
					// change fillAlpha automatically so the selected color can be seen
					if (obj.fillAlpha == 0) {
                        for each(var abObj:TaconiteModel in ApplicationController.instance.authorController.cleanSelection){
                            if((abObj.value as AbstractObject).hasOwnProperty("fillAlpha")){
                                (abObj.value as AbstractObject).fillAlpha = 100;
                                ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject((abObj.value as AbstractObject), "fillAlpha", 0));
                            }
                        }
					}
					break;
				case "visible":
					if (value == false) {
                        for each(var abObj:TaconiteModel in ApplicationController.instance.authorController.cleanSelection){
                            (abObj.value as AbstractObject).showAll = (Application.instance.document.root.value as World).showAll;
                        }
					}
					break;
                case "vScrollPolicy":
                    if (value != ScrollPolicy.OFF && obj is TextObject) {
                        for each(var abObj:TaconiteModel in ApplicationController.instance.authorController.cleanSelection){
                            if((abObj.value as AbstractObject) is TextObject && (abObj.value as AbstractObject).hasOwnProperty("autoSize")){
                                TextObject((abObj.value as AbstractObject)).autoSize = false;
                            }
                        }
                    }
                    break;
			}
		}
		
		private function logEdit(obj:AbstractObject, oldValue:*):void
		{
			ApplicationController.instance.authorController.log("ribbon value="+ribbon.value + " old="+oldValue + " ribbon="+ribbon.logStr);
		}
		
		public function set myValue(value:Object):void
		{
			if (ribbon.hostObject == null || (ribbon.modifierDescription is PropertyDescription) || ribbon as ExternalSmartWireAnchor) {
//				trace("RibbonView::Setting ",ribbon.hostProperty," on ",ribbon.hostObject," to ",value);
				
				/* 	Don't let constrained values go beyond their constraints (including prohibiting 
					negative values for ribbons of UNSIGNED_NUMBER_TYPE)	*/
				if (PropertyDescription(ribbon.modifierDescription).type == Modifiers.UNSIGNED_NUMBER_TYPE) {
					
					var constraints:NumberValueControlConstraint = PropertyDescription(ribbon.modifierDescription).constraints as NumberValueControlConstraint;
					var numValue:Number = Number(value);
					
					if (constraints) {
						if (numValue < constraints.valueMin)
							ribbon.value = constraints.valueMin;
						else if (numValue > constraints.valueMax)
							ribbon.value = constraints.valueMax;
						else
							ribbon.value = value;
					} else if (numValue < 0) {
						ribbon.value = 0;
					} else {
						ribbon.value = value;
					}
				} 
				// Convert "false" string to boolean
				else if (value=="false"){
					ribbon.value = false;
				} else if (value as String && PropertyDescription(ribbon.modifierDescription).type == Modifiers.FILL_TYPE){
					ribbon.value = new GraphicFill(value as String);
				} else { 
					ribbon.value = value;
				}
			}
		}
		
		/**
		 * The order of these is important. Since our model is not the object model, but an 'atomic' model,
		 * we need to agressively capture the mouse events in order to prevent the message center from 
		 * disappearing upon click. This code makes ribbon controls work.
		 * 
		 */
		override public function initialize():void
		{
			new RibbonMediator(context).handleViewEvents(this);
			super.initialize();
		}
		
		private function handleModelUpdate(e:ModelUpdateEvent):void
		{
			if (e.source != model)
				return;  // ignore events bubbling from elsewhere
			
			if (e.property == "label") {
				if (ribbon as ExternalSmartWireAnchor) {
					_editableLabel.text = e.newValue.toString();
				} else {
					_label.text = e.newValue.toString();
				}
				formatLabel();			
			}
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{
			switch (property) {
				case "yContainer":
				//	trace("yContainer="+newValue);
					return true; // A view update is unnecessary when this property is being set.
				case "value":
					if (this.visible && ribbon.modifierDescription as PropertyDescription)
						updateValue();
					return true;
				case "messageCenterVisible":
					if(_messageCenterButton != null) {
						_messageCenterButton.selected = Boolean(newValue);
					}
					return true;
				case "propertyInspectorVisible":
					if(_propertyInspectorButton != null) {
						_propertyInspectorButton.selected = Boolean(newValue);
					}
					return true;
				case "wired":
					if (newValue == false && newValue != oldValue)
						parentSubcontainer.handleWireRemoved();
					return true;
				case "highlight":
					highlight(newValue);
					return true;
				case "childAnchors":
					parentSubcontainer.updateMaxHeight(this);
					return true;
				case "type":
				case "modifierDescription":  // author dragged edge-wire to a different property
					if (ribbon.modifierDescription is PropertyDescription)
						setValueComponent();
					return true;
				case "label":
					reposition();
					return true;
				default:
					return super.updateModelProperty(property, oldValue, newValue);	
			}
		}

        private function reposition():void {
            _parentSubcontainer.reorderRibbonView(this);
        }

		private var valueCache:Object = null;

		private function updateValue():void
		{
			if (ribbon.modifierDescription as PropertyDescription == false || _valueControl == null) {
				return;
			}

			var val:Object = ribbon.value;
			
			if (val != null && val != valueCache) {
				if (_valueControl as TransformationEditor && ribbon.hostObject!=null && ribbon.hostObject.model.parent) {
					var container:AbstractObject = ribbon.hostObject.model.parent.value as AbstractObject;
					TransformationEditor(_valueControl).parentView = container.getView(Application.instance.viewContext);
					TransformationEditor(_valueControl).transformation = val as PositionAndAngle;
				} else if (_valueControl as RibbonFillPicker) {
					_valueControl.setValue(val, DEFAULT);
					updateFillRibbonLabel();
				} else {
					_valueControl.setValue(val, DEFAULT);
				}
				
				if (_ribbonSlider) {
					_ribbonSlider.setValue(val, DEFAULT);
				}
			} else  if (val == null && _valueControl as RibbonAssetEditor) {
				_valueControl.setValue(null,null);
			}
			
		}
		
		private function updateInletValue():void
		{
			if (ribbon.modifierDescription as InletDescription == false || _valueControl == null) {
				return;
			}
			var val:String = ribbon.keyPress;
			_valueControl.setValue(val, DEFAULT);
		}
		
		public function handleWireDrop(left:Boolean):PortView 
		{
			if (_showPorts)
				return (left?_leftPort:_rightPort);

			return null;
		}
		
		public function updateAnchors(header:Boolean):void 
		{
			if (updateWiresForThisView) {		
				if (ribbon.alwaysShow) {
					ApplicationController.instance.wireController.requestRedrawForObject(ribbon.hostObject as AbstractObject);

				//	trace("MC.y="+parent.parent.parent.parent.y, "container.y="+parent.parent.parent.y, "subcontainer.y="+parent.parent.y, "ribbon.y="+this.y, "yContainer="+ribbon.yContainer);
				}
			}
		}
				
		private function get updateWiresForThisView():Boolean
		{
			var windowArrangement:int = Application.instance.windowArrangement;
			return ((_role == ViewRoles.PRIMARY && windowArrangement < 2) ||
						(_role == ViewRoles.DIAGRAM && windowArrangement > 1));
		}
		
		private function setTransformationGhost():void
		{
			if(_valueControl is TransformationEditor && 
				(ribbon is PathNodeWireAnchor ||  
					ribbon is MetaWireAnchor &&
					MetaWireAnchor(ribbon).parent is SetterWireAnchor &&
					SetterWireAnchor(MetaWireAnchor(ribbon).parent).prototypeAnchor is PathNodeWireAnchor))
			{
				TransformationEditor(_valueControl).ghostClass = PathNodeTransformationGhost;
			}
		}
					
		private function setValueComponent():void 
		{
			if(_valueControl) {
				if(_valueControl.parent) {
					_valueControl.parent.removeChild(_valueControl);
				}
			}
			_valueControl = AbstractRibbonControl.createControl(PropertyDescription(ribbon.modifierDescription), setValueCallback, ribbon.hostObject, ribbon.value, VALUE_WIDTH, HEIGHT, true, 0xffffff, _showPorts);
			
			_valueControl.x = ((_valueControl as AbstractSwitch) ? PortView.WIDTH : VALUE_X) + ribbonLibraryBtnPadding;
			_valueControl.y = VALUE_Y;
			
			if(_valueControl as TransformationEditor || _valueControl as RibbonFillPicker || _valueControl as RibbonColorPicker){
				_valueControl.y = 0;
			}
			
			
			/**
			 * If ribbon is PathNodeWireAnchor then
			 * set the ghost Class to be used on the
			 * TransformationEditor
			 */
			setTransformationGhost();
            addChild(_valueControlBackground);
			addChild(_valueControl);
			


			if(_ribbonSlider) {
				if(_ribbonSlider.parent) {
					_ribbonSlider.parent.removeChild(_ribbonSlider);
				}
			}

			_ribbonSlider = AbstractRibbonControl.createRibbonSlider(PropertyDescription(ribbon.modifierDescription), setValueCallback, _valueControl);
			
			if (_ribbonSlider) {
                _ribbonSlider.ribbonView = this;
				_ribbonSlider.visible = false;
			}
		}
		
		private function fillPickerCallback(newValue:GraphicFill):void
		{
			myValueFromControl = newValue.toString();
			updateFillRibbonLabel();
		}
		
		public function get ribbonLibraryBtnPadding():int {
			if (_showPorts) {
				return 0;
			} else {
				return 15;
			}
		}
		
		private function updateFillRibbonLabel():void
		{

			// Disable changing for now
//			// Fill- solid   --or--  Fill- gradient, etc
//			if (ribbon.value && ribbon.value as GraphicFill && _label && !(ribbon.host as WireAnchor)) {
//				// get the first word so we can preserve it, in case its slider "Knob- solid color"
//				var words:Array = _label.text.split("-");
//				var fill:GraphicFill = ribbon.value as GraphicFill;
//
//				//the above code wasn't working, so temporarily put in this line instead
//				var fillType:String = GraphicFill.typeLabel(fill.type) as String;
//				_label.text = fillType.substr(0,1).toUpperCase() + fillType.substr(1) + " ";
//			}
		}
		
		// This should be a generic call for controls that pop out of MCs - LML
		private function fillPickerOpenPanelCallback():void
		{
			showTooltip(false);  // fill panel is opening -- hide our tooltip so it doesn't cover the panel
		}
		
		private function drawBackground():void
		{
			// Background of entire ribbon
			graphics.clear();
			graphics.beginFill(BACKGROUND_COLOR, _backgroundAlpha);
			graphics.lineStyle(0, 0x220000, 0.1);
			graphics.drawRect(MessageCenterView.BORDER_WIDTH,0,MessageCenterView.WIDTH_NO_BORDER, HEIGHT);
			graphics.endFill();
		}
		
		private function formatLabel():void
		{
			if (ribbon as ExternalSmartWireAnchor) {
				_editableLabel.color = LABEL_COLOR;
				_editableLabel.x = LABEL_X + ribbonLibraryBtnPadding + childRibbonIndentation;
				//_editableLabel.y = 2;
				_editableLabel.size = 11;
				
			} else {
				_label.color = LABEL_COLOR;
				_label.x = LABEL_X + ribbonLibraryBtnPadding + childRibbonIndentation;
				_label.size = 11;
				
				// First part of label in italics
				var endIndex:int = _label.text.search("-");
				if (endIndex != -1)
					_label.formatRange(0,endIndex,GROUP_LABEL_COLOR,false, true);
			}
		}

        private function get childRibbonIndentation():Number {
            return (this.parentSubcontainer is RibbonChildContainer ? 5 : 0)
        }
		
		/**
		 * Set height of child components (prevents unwanted scaling)
		 * 
		 */
		override public function set height(value:Number) : void
		{	
			// Quick fix. Ribbon height will no longer be set when containers open and close.
			visible = value==0?false:true;
		}
		
		override public function set visible(value:Boolean) : void
		{
			if(visible == value) {
				return;
			}
			super.visible = value;
			
			updateValue();
			
			if (_valueControl && _valueControl as RibbonFillPicker && !value)
				(_valueControl as RibbonFillPicker).closeEditing(null);  // close the fill panel so its not orphaned by closing the MC
		}

		override protected function addRollHighlight(e:MouseEvent) : void
		{
            if (Application.instance.draggingSlider) {
                return;
            }
            if (e.relatedObject == _ribbonSlider || e.relatedObject == this || (_ribbonSlider && e.relatedObject&& _ribbonSlider.contains(e.relatedObject)) || (e.relatedObject && this.contains(e.relatedObject)))
                return;
			this.rolled = true;
		}
		
		override protected function removeRollHighlight(e:MouseEvent) : void
		{
            if (e.relatedObject == _ribbonSlider || e.relatedObject == this || (_ribbonSlider && e.relatedObject&& _ribbonSlider.contains(e.relatedObject)) || (e.relatedObject && this.contains(e.relatedObject)))
                return;
			this.rolled = false

		}
		
		public function roll(value:Boolean,tooltip:Boolean = true):void
		{
			if (value) {
                if(ribbon.hostObject != null){
                    if(!ribbon.hostObject.wiringLocked){
                        highlight(true);
                        hiliteCompositePort(true);
                    }
                }
                if (mouseY < 15 && tooltip)
					showTooltip(true);
				fadeInComponent();
			} else {
				highlight(false);
                if (tooltip) showTooltip(false);
			    hiliteCompositePort(false);
				fadeOutComponent();
			}
		}

        public function set rolled(value:Boolean):void {
            roll(value);
            this._rolled = value;
        }
        public function get rolled():Boolean{
            return this._rolled;
        }

		public function highlight(value:Boolean):void
		{
			if(!_context.info.userActionInProgress){
				Tweener.addTween(this, {_backgroundAlpha:value?BACKGROUND_ALPHA:0.01, time:0.15, transition:"easeOutQuad", onUpdate:drawBackground});
			}else{
				if(Application.instance.wireDragDescription != null){
					Tweener.addTween(this, {_backgroundAlpha:value?BACKGROUND_ALPHA:0.01, time:0.15, transition:"easeOutQuad", onUpdate:drawBackground});
				}	
			}
		}
		
		private function addChildRibbons():void
		{			
			_subcontainer = parentSubcontainer.addSubContainer(this);
			addChild(_subcontainer);
            _subcontainer.y = 0;
			for each (var anchor:WireAnchor in ribbon.childAnchors) {
				//trace("AddChildRibbons::anchor="+anchor.modifierDescription.label, "weight="+anchor.modifierDescription.weight, "value="+anchor.value);
				_subcontainer.addRibbon(anchor, false, null);
			}
            _parentSubcontainer.updateMaxHeight(this);
			_label.visible = false;
		}
		
		public function adjustChildRibbons():void
		{
			if (_subcontainer != null) {
				_subcontainer.adjustRibbonPositions();
			}
		}
		
		private function hiliteCompositePort(hilite:Boolean):void
		{
			var ed:SmartObjectEditingMediator = ApplicationController.instance.authorController.currentVelumMediator;
			if (ed) {
				ed.hilitePort(ribbon, hilite);
			}
		}
		
		public function showTooltip(show:Boolean):void
		{
			if(_context.info.userActionInProgress){
				if(Application.instance.wireDragDescription == null){
					return
				}
			}
			var toolTipLocation:Point;
			var toolTipText:String;
			if (show) {
				toolTipLocation = new Point(VALUE_X, this.height+10);
                if((model.value as WireAnchor).hostObject != null && (model.value as WireAnchor).hostObject.toolTipOverrides[(model.value as WireAnchor).path] != null){
                    toolTipText = (model.value as WireAnchor).hostObject.toolTipOverrides[(model.value as WireAnchor).path];
                } else {
                    toolTipText = (ribbon.modifierDescription.description!="" ? ribbon.modifierDescription.description : ribbon.modifierDescription.label);
                }
				if (ribbon as ExternalSmartWireAnchor) {
                    toolTipText = (ribbon as ExternalSmartWireAnchor).labelWithEditCue;
                } else if (ribbon as InternalSmartWireAnchor) {
                    toolTipText = ribbon.label;
                }
				ApplicationController.instance.displayToolTip(this.localToGlobal(toolTipLocation),ControlAreaToolTip.DISPLAY_BELOW, toolTipText, ControlAreaToolTip.ARROW_HIDDEN);
			} else {
				ApplicationController.instance.hideToolTip();
			}
		}
		
		private function fadeInComponent():void 
		{
			if (Application.instance.wireDragDescription || Application.instance.draggingSlider) // don't show component if dragging a wire or a ribbon slider
				return;

			if (_ribbonSlider) {
                // make sure the ribbon slider isn't in there yet
                if (parentSubcontainer.parent.contains(_ribbonSlider)){
                    parentSubcontainer.parent.removeChild(_ribbonSlider);
                }
//                var p:DisplayObjectContainer = this.parent ;
//                p.removeChild(this);
//                p.addChild(this);
//                p = parentSubcontainer.parent;
//                p.removeChild(parentSubcontainer);
//                p.addChild(parentSubcontainer);
                var localSliderCoordinate:Point = new Point();
                localSliderCoordinate.x = PortView.WIDTH-2 + ribbonLibraryBtnPadding; // Slider sits above the ribbon label
                localSliderCoordinate.y = VALUE_Y;

                var sliderCoordinate:Point = parentSubcontainer.parent.globalToLocal(this.localToGlobal(localSliderCoordinate));
                _ribbonSlider.x = sliderCoordinate.x;
                _ribbonSlider.y = sliderCoordinate.y;
                if (!parentSubcontainer.parent.contains(_ribbonSlider)) {
                    parentSubcontainer.parent.addChild(_ribbonSlider);
                }
                _ribbonSlider.addEventListener(MouseEvent.MOUSE_OUT,removeRollHighlight);
//                _ribbonSlider.addEventListener(MouseEvent.MOUSE_OVER,addRollHighlight);

                var hostComposite:SmartObject = this.ribbon.hostObject as SmartObject;
                var hostCompositeEditing:Boolean = hostComposite && hostComposite.editing;

                if (!hostCompositeEditing) {
                    _ribbonSlider.alpha = 0;
                    _ribbonSlider.visible = true;
                    Tweener.addTween(_ribbonSlider, {alpha:1, transition:"easeOutQuad", time:0.5});
                }

				if (_ribbonSlider) {
					_ribbonSlider.reset();	
				}
			}
		}

		public function fadeOutComponent():void
		{
			if (_ribbonSlider) {
				_ribbonSlider.visible = false;
                _ribbonSlider.removeEventListener(MouseEvent.MOUSE_OUT,removeRollHighlight);
			}
		}
		
		override public function get height():Number
		{
			var value:Number = HEIGHT;
			
			if (_subcontainer/* && _subcontainer.isOpen*/) {
			//	value +=ribbon.childAnchors.length*HEIGHT
				value = _subcontainer.perceivedHeight;
			} else {
				value = HEIGHT;
			}
			
			return value;
		}
		
		override public function toString():String
		{
			return "RibbonView" + "/" + ((ribbon) ? ribbon.hostProperty : "");
		}
		
		override protected function updateView():void
		{
			// do not clear graphics on updateView (redrawBackground does this)
		}
		
		private function setUpParams(label:LightEditableLabel, callBacks:Object):void
		{
			label.mediator.setCallBacks(callBacks);
			//label.setPaddingParams({left:0, top:3, right:0, bottom:0});
		}


		public function makeDragSnapShot():Sprite {
			var bmp:BitmapData = new BitmapData(MessageCenterView.WIDTH, HEIGHT);
			bmp.draw(this);
			var result:Sprite = new Sprite();
			result.addChild(new Bitmap(bmp));
			result.alpha = 0.7;
			result.mouseEnabled = false;
			EditorUI.instance.rawChildren.addChild(result);
			return result;
		}

		public function get childContainer():RibbonChildContainer {
			return _subcontainer as RibbonChildContainer;
		}
	
		

	private function setKeyPressEditor():void{
		if(ribbon.childAnchors.length != 0 || ribbon as ForwardingWireAnchor) {
			return;
		}
		
		_valueControl = AbstractRibbonControl.createControl(Modifiers.instance.fetch("keyPress") as PropertyDescription, handleCallBack, ribbon.hostObject, ribbon.value, VALUE_WIDTH, HEIGHT);
		
		//if(ribbon.keyPress == "")
			//(_valueControl as RibbonValueField).prompt = "key..";
		
		_valueControl.x = VALUE_X;
		_valueControl.y = VALUE_Y;
		addChild(_valueControl);
	}
	
	private function handleCallBack(value:Object):void{
		ribbon.keyPress = value as String;
	}
	
		
	}
}