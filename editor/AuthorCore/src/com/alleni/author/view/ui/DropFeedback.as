package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ui.EditorUI;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	import flash.geom.Rectangle;
	
	public class DropFeedback extends Sprite
	{
		public static const COLOR:uint = 0xfbff24;
		
		private static var _feedback:Shape;
		private static var _targetHost:IDropHost;
		private static var _shadow:DropShadowFilter;
		
		public function DropFeedback()
		{
			super();
		}
		
		public static function showPendingDrop(view:DisplayObject):void
		{
			show(view);
		}
		
		private static function get shadow():DropShadowFilter
		{
			if (!_shadow)
				_shadow = new DropShadowFilter(1, 80, 0, 0.6, 3, 3, 0.8, 3);
			return _shadow;
		}
		
		private static function show(view:DisplayObject):void
		{
			if (!view || !view.stage) {
				clear();
				return;
			}
			
			if (_feedback && _targetHost == view)
				return;
			
			clear(_feedback);
			
			_targetHost = view as IDropHost;
			
			if (!_targetHost) // only draw feedback for views which serve as hosts
				return;
			
			var container:DisplayObjectContainer = view.stage;
			
			var bounds:Rectangle = view.getBounds(container);
			
			_feedback = new Shape();
			_feedback.graphics.lineStyle(5, COLOR, 0.5, true);
			
			var drawFunction:Function = _targetHost.feedbackGraphicsDrawFor(_feedback.graphics);
			if (drawFunction == null)
				_feedback.graphics.drawRoundRect(0, 0, bounds.width, bounds.height, 10, 10);
			else
				drawFunction();
			
			_feedback.filters = [shadow];
			_feedback.alpha = 0;
			_feedback.x = bounds.x;
			_feedback.y = bounds.y;
			
			container.addChild(_feedback);
			Tweener.addTween(_feedback, {alpha:1, time:0.5, transition:EditorUI.ANIMATION_IN_ALGORITHM});
		}
		
		public static function clear(oldFeedback:DisplayObject=null):void
		{
			_targetHost = null;
			if (!oldFeedback)
				oldFeedback = _feedback;
			
			if (oldFeedback) {
				Tweener.removeTweens(oldFeedback);
				Tweener.addTween(_feedback, {alpha:0, time:0.5, transition:EditorUI.ANIMATION_IN_ALGORITHM,
					onComplete:function():void {
						if (!oldFeedback || !oldFeedback.parent) return;
						
						if (oldFeedback.parent.getChildIndex(oldFeedback) > -1)
							oldFeedback.parent.removeChild(oldFeedback);
						oldFeedback = null;
					}});
			}
		}
	}
}