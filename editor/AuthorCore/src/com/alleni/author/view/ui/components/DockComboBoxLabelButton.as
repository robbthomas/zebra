package com.alleni.author.view.ui.components
{
	import flash.display.DisplayObjectContainer;
	import flash.filters.DropShadowFilter;
	
	public class DockComboBoxLabelButton extends PushButton
	{
		public function DockComboBoxLabelButton(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, label:String="", defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, label, defaultHandler);
		}
		
		/**
		 * Draws the face of the button, color based on state.
		 */
		override protected function drawFace():void
		{
			_face.graphics.clear();
			if(_down)
			{
				_face.graphics.beginFill(0x00aa00, 0);
			}
			else
			{
				_face.graphics.beginFill(0x0000aa, 0);
			}
			_face.graphics.drawRect(0, 0, _width - 2, _height - 2);
			_face.graphics.endFill();
		}
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			super.draw();
			_back.graphics.clear();
			_back.graphics.beginFill(0xaa0000, 0);
			_back.graphics.drawRect(0, 0, _width, _height);
			_back.graphics.endFill();
			
			_label.x = -2;
			
			this.centerLabel = false;
		}
		
		/**
		 * DropShadowFilter factory method, used in many of the components.
		 * @param dist The distance of the shadow.
		 * @param knockout Whether or not to create a knocked out shadow.
		 */
		override protected function getShadow(dist:Number, knockout:Boolean = false):DropShadowFilter
		{
			return new DropShadowFilter(dist, 80, 0x00ff00, 0.5, dist, dist, .3, 3, knockout);
		}
	}
}