/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 1/16/12
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.ui.components {
import flash.display.DisplayObjectContainer;
import flash.events.MouseEvent;

public class ZappingPanelSearchComboBox extends SearchComboBox{
        public function ZappingPanelSearchComboBox(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, defaultLabel:String="", items:Array=null, labelPrefix:String="")
		{
			super(parent, xpos, ypos, defaultLabel, items, labelPrefix);
		}

		public override function draw():void
		{
			super.draw();
            this.graphics.beginFill(0xFFFFFF);
            this.graphics.lineStyle(1, 0xCCCCCC);
            this.graphics.drawRect(-3, -4, this.width + 4, 16);
            this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}

        private function onMouseDown(event:MouseEvent):void{
            textField.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
        }
    }
}
