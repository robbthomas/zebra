package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.definition.application.PublishIcons;
	import com.alleni.author.view.text.RichLabel;
	
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
import flash.utils.Timer;

public class
OverlayMessage extends Sprite
	{
		public static const DEFAULT_SKIN:String = "defaultSkin";
		public static const CLOUD_SKIN:String = "cloudSkin";
		
		public static const HEIGHT:Number 	= 80;
		public static const WIDTH:Number 	= 425;
		
		private static const HIGHLIGHT_COLOR:uint = 0x97C7FF;
		private var _mainMessageField:RichLabel;
		private var _subMessageField:RichLabel;
		private var _skin:String = DEFAULT_SKIN;
		private var _background:Sprite;
        private var _pane:Sprite = new Sprite();
        private var _auxDisplay:DisplayObject = null;
        private var _delay:Timer = new Timer(1000, 1);

		public function OverlayMessage()
		{
			super();
			initialize();
		}

		private function initialize():void
		{
            this.addChild(_pane);
			alpha = 0;

			_background = new Sprite();
			_mainMessageField = new RichLabel(WIDTH);
			_subMessageField = new RichLabel(WIDTH);

			_pane.addChild(_background);
			_pane.addChild(_mainMessageField);
			_pane.addChild(_subMessageField);

			formatText();

			draw();
		}

		private function draw(auxDisplay:DisplayObject = null):void
		{
            if (_auxDisplay as DisplayObject && this.contains(_auxDisplay)) removeChild(_auxDisplay);
            _auxDisplay = auxDisplay;
            if (_auxDisplay) addChildAt(_auxDisplay,0);

			while (_background.numChildren > 0)
				_background.removeChildAt(0);

			_background.graphics.clear();
			var mainShadow:DropShadowFilter = new DropShadowFilter(2, 80, 0, 0.8, 6, 6, 1, 3);
			var innerGlow:GlowFilter = new GlowFilter(0x000000, 0.8, 3, 3, 2, 3, true);
			var textShadow:DropShadowFilter = new DropShadowFilter(1, 80, 0, 1.0, 2, 2, 5, 3);
			var mask:Shape = new Shape();

			switch(_skin) {
				case CLOUD_SKIN:
					innerGlow.color = 0x000000;
					textShadow.color = 0xffffff;
					_mainMessageField.color = 0x000000;
					_subMessageField.color = 0x444444;
					_background.graphics.beginFill(0xffffff);
					_background.graphics.drawRect(0,0, WIDTH, HEIGHT);

					var matrix:Matrix = new Matrix();
					matrix.createGradientBox(WIDTH, HEIGHT, (Math.PI/1.6), 0, 0);
					_background.graphics.beginGradientFill(GradientType.LINEAR, [HIGHLIGHT_COLOR, HIGHLIGHT_COLOR], [0, 1], [0x00, 0xff], matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
					_background.graphics.drawRect(0, 0, WIDTH, HEIGHT);

					var backgroundDecoration:DisplayObject = PublishIcons.dialogDecoration;
					backgroundDecoration.x = -17;
					backgroundDecoration.y = HEIGHT - backgroundDecoration.height + 30;
					_background.addChild(backgroundDecoration);
					break;
				case DEFAULT_SKIN:
				default:
					_background.graphics.beginFill(0x000000);
					_background.graphics.drawRect(0,0, WIDTH, HEIGHT);

					innerGlow.color = 0xffffff;
					textShadow.color = 0x000000;
					_mainMessageField.color = 0xFFFFFF;
					_subMessageField.color = 0x9E9E9E;
					break;
			}
			mask.graphics.lineStyle(0, 0, 0, true);
			mask.graphics.beginFill(0, 1);
			mask.graphics.drawRoundRect(0,0, WIDTH, HEIGHT, 25, 25);
			mask.graphics.endFill();
			_background.graphics.endFill();
			_background.addChild(mask);
			_background.mask = mask;
			_background.filters = [innerGlow];
			_pane.filters = [mainShadow];
			_mainMessageField.filters = [textShadow];
			_subMessageField.filters = [textShadow];
		}

		public function show(mainMessage:String, subMessage:String="", skin:String=DEFAULT_SKIN,auxDisplay:DisplayObject = null):void
		{
			_skin = skin;
			draw(auxDisplay);
			
			_mainMessageField.text = mainMessage;
			_subMessageField.text = subMessage;
			if (!visible) {
				alpha = 0;
				visible = true;
			}
            // one second delay before attaching mouse move listener that
            // removes the messaging - otherwise, any mouse movement causes
            // it to disappear far too fast.
            stage.addEventListener(MouseEvent.MOUSE_DOWN, fadePanel, true);

            Tweener.addTween(this, {alpha:0.85, time:0.25, transition:"easeInQuad", onComplete: function():void{
                _delay.reset();
                _delay.addEventListener(TimerEvent.TIMER_COMPLETE, fadePanel);
                _delay.start();
            }
            });
		}

        private function fadePanel(e:Event):void {
            alpha = 0.85;
            Tweener.addTween(this, {alpha:0, time:1, transition:"easeInQuad",onComplete: function():void{visible = false;}});
            _delay.removeEventListener(TimerEvent.TIMER_COMPLETE, fadePanel);
            stage.removeEventListener(MouseEvent.MOUSE_DOWN, fadePanel);
        }

        public function set paneX(x:Number):void {
            _pane.x = x;
        }
        public function set paneY(y:Number):void {
            _pane.y = y;
        }

        private function formatText():void
		{
			_mainMessageField.y = 15;
			_mainMessageField.size = 20;
			_mainMessageField.center = true;

			_subMessageField.y = 50;
			_subMessageField.size = 14;
			_subMessageField.center = true;
		}
	}
}