package com.alleni.author.view.ui
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.WireEditMediator;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.InletDescription;
	import com.alleni.author.definition.OutletDescription;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Wire;
    import com.alleni.author.view.WorldContainer;
	import com.alleni.taconite.lang.Geometry;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.TaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;

	public class WireView extends TaconiteView
	{
		private static const INLET_COLOR:uint = 0xff3232;		// red
		private static const OUTLET_COLOR:uint = 0xf7931e;  	// orange

        private static const PROPERTY_COLOR:uint = 0x0084ff;	// blue
        private static const ILLEGAL_COLOR:uint = 0xb2b2b2;	// grey

        private static const PROPERTY_END_RADIUS:Number = 3.5;
		private static const TRIANGLE_WIDTH:Number = 7;			// Inlet and outlet triangle width
		private static const TRIANGLE_HEIGHT:Number = 8;		// Inlet and outlet triangle height
		
		private static var _applicationController:ApplicationController;
		private static var _application:Application;
		
		private static var _verticalFollow:Number = 0.1;
		private static var _hFarCurviness:Number = 0.3;
		private static var _hCloseCurviness:Number = 0.3;
		private static var _vCurviness:Number = 0.2;
		private static var _numberOfSteps:int = 50;
		private static var _spreadPower:Number = 0.5;
		private static var _radialSpacing:Number = 2;
		
		private var _masterEnd:Sprite;
		private var _slaveEnd:Sprite;
		private var _colorGrad:Vector.<Number>;
		
		private var _c1:Point;
		private var _c2:Point;
		private var _role:uint;
		private var _rolled:Boolean;
		private var _wire:Wire;
		
		public function WireView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model);
			_applicationController = ApplicationController.instance; // keep this locally, no danger of changing. static lookup is expensive.
			_application = Application.instance;
			_role = role;
			updateVisibility();
			_wire = model.value as Wire;
			_wire.register(this);
			
			_masterEnd = new Sprite();
			_slaveEnd = new Sprite();
			
			_c1 = new Point();
			_c2 = new Point();
			
			// Sprites for the ends
			addChild(_masterEnd);
			addChild(_slaveEnd);
			
			// Set up mediator for editing wires
			new WireEditMediator(context).handleWireViewEvents(this, role);
			
			// Add roll highlighting listeners
			this.addEventListener(MouseEvent.MOUSE_OVER, handleRollOver, false, 0, true);
			this.addEventListener(MouseEvent.MOUSE_OUT, handleRollOut, false, 0, true);
            this.addEventListener(MouseEvent.MOUSE_DOWN, handleRollOut, false, 0, true);
		}
		
		public function get wire():Wire
		{
			return model.value as Wire;
		}
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{
			switch (property) {
				case "masterAnchor":
				case "slaveAnchor":
					if (newValue == _applicationController.wireController.ghostAnchor)
						clearArrows();
					break;
                case "visible":
					updateVisibility();
					return true;  // no need for updateView
			}
			return true;  // let the normal update take place
		}

		public function render():void {
			graphics.clear();
			if (updateWiresForThisView && wire.masterAnchor != null && wire.slaveAnchor != null) {
				var angle2:Number;
				if(isNaN(wire.angle2) == false && wire.attached) {
					angle2 = wire.angle2;
				} else if(Math.abs(wire.x2-wire.x) > 50) {
					angle2 = wire.x2-wire.x > 0 ? 180 : 0
				} else {
					// when we are above and below but close in the x direction
					// use only C curves and dont try to use s curves that dont
					// look good this close
					angle2 = wire.angle * (wire.y>wire.y2? 1 : -1);
				}
				drawWire(wire.angle, 1, angle2, 1);

				// think about visibility of wire
				updateVisibility();
			}
		}
		
		override protected function updateView() : void
		{
            render();
			return;
		}
		
		private function updateVisibility():void
		{
            visible = wire.visible;
		}
		
		private function get updateWiresForThisView():Boolean
		{
			if (_applicationController.diagramWindowVisible) {
				return _role == ViewRoles.DIAGRAM;
			} else {
				return _role == ViewRoles.PRIMARY && !Application.running;
			}
		}
		
		
		private function drawWire( angle1:Number, spread1:int, angle2:Number, spread2:int):void
		{
			// Shadow filter
			var shadow:DropShadowFilter = new DropShadowFilter(2,45,0,0.5,3,3,0.8,5);
			shadow.distance = 1;
			shadow.angle = 25;
			shadow.alpha = 0.5;
			//this.filters = [shadow];
			
			var wx1:Number = wire.x;
			var wx2:Number = wire.x2;
			var wy1:Number = wire.y;
			var wy2:Number = wire.y2;
			
//			// Add a radial spacing to the ends if unattached.
			if (!wire.attached) {
				wx2 += _radialSpacing*Math.cos(angle2*Math.PI/180);
				wy2 += _radialSpacing*Math.sin(angle2*Math.PI/180);
			}
			
			updateControlPoints(angle1, spread1, angle2, spread2); // get the control points

			// calculate the bezier curve
			var bezierPoints:Vector.<Point> = new Vector.<Point>;
			var i:int=0;
			var n:int = _numberOfSteps; // number of steps
			for (i; i<=n; i++) {
				var t:Number = i/n;
				
				// coefficients for the Bezier function
				var a:Number = Math.pow((1-t),3);
				var b:Number = 3*t*Math.pow((1-t),2);
				var c:Number = 3*Math.pow(t,2)*(1-t);
				var d:Number = Math.pow(t,3);
				
				// the points
				bezierPoints.push(new Point(	wx1 * a + _c1.x * b + _c2.x * c + wx2 * d,
												wy1 * a + _c1.y * b + _c2.y * c + wy2 * d));
			}
			
			// draw the interpolation
			graphics.lineStyle(_rolled || _wire.illegal?4:2, wireFromColor, 1);
            var lockedWire:Boolean = (_wire.slaveAnchor.hostObject && _wire.slaveAnchor.hostObject.wiringLocked) || (_wire.masterAnchor.hostObject && _wire.masterAnchor.hostObject.wiringLocked);
            if (lockedWire)
                graphics.lineStyle(1, wireFromColor, 1);

			var color:Number = wireFromColor;
			if (_colorGrad == null) {
				_colorGrad = calculateGradient(wireFromColor,wireToColor,0.6);
			} else if (_colorGrad[0] != wireFromColor || _colorGrad[_colorGrad.length-1] != wireToColor) {
				_colorGrad = calculateGradient(wireFromColor,wireToColor,0.6);
			}

			i = 0;
			var count:int = bezierPoints.length-1;
			for (i; i < count; i++) {
				color = _colorGrad[i];
                if (lockedWire)
                    graphics.lineStyle(1, wireFromColor, 1);
                else
    				graphics.lineStyle(_rolled || _wire.illegal?4:2, color);
				graphics.moveTo(bezierPoints[i].x, bezierPoints[i].y);
				graphics.lineTo(bezierPoints[i+1].x, bezierPoints[i+1].y);
			}
		}
				
		private function updateControlPoints(angle1:int, spread1:int, angle2:int, spread2:int):void
		{
            // The following line is to disable spread for the time being (it is not implemented correctly below and causes skew in the wires).
                spread1 = spread2 = 0;

			var rad1:Number = angle1*Math.PI/180;
			var rad2:Number = angle2*Math.PI/180;
			
			var dx:Number = wire.x2 - wire.x;
			var dy:Number = wire.y2 - wire.y;

			var short:Boolean = false;
			
			var dist:Number = Math.sqrt(dx*dx + dy*dy);
			if(dist < 100) {
				short = true;
				// wire is very short and the following calculated control points will make the wire
				// not extend beyond the anchor points, making it hard or impossible to click
				// force it to extend out by faking the distance between end points
				if(dist == 0) {
					dx = 0;
					dy = 100;
				} else {
					dx = dx*100/dist;
					dy = dy*100/dist;
				}
			}
			
			// calculate the handle displacement
			var params:Array = [0.3,0.3,0.2,0.1] // these paramaters need to get moved out of here (where?)
			var handleVector:Array = [	Math.max(_hFarCurviness*Math.abs(dx), _hCloseCurviness*Math.abs(dx) + _vCurviness*Math.abs(dy)),
										_verticalFollow*dy];
			
			// control points
			_c1.x = wire.x + Math.cos(rad1) * handleVector[0];
			_c1.y = wire.y + handleVector[1] * Math.pow(spread1,_spreadPower) + Math.sin(rad1)*handleVector[0];  // multiply by number of connections raised to the spread power
			_c2.x = wire.x2 + Math.cos(rad2) * handleVector[0];
			_c2.y = wire.y2 + handleVector[1] * Math.pow(spread2,_spreadPower) + Math.sin(rad2)*handleVector[0];  // multiply by number of connections raised to the spread power
			if(short) {
				_c1.x = wire.x + Point.distance(_c1, new Point(wire.x, wire.y)) * Math.cos(rad1);
				_c2.x = wire.x2 + Point.distance(_c2, new Point(wire.x2, wire.y2)) * Math.cos(rad2);
				_c1.y = wire.y + Point.distance(_c1, new Point(wire.x, wire.y)) * Math.sin(rad1);
				_c2.y = wire.y2 + Point.distance(_c2, new Point(wire.x2, wire.y2)) * Math.sin(rad2);
			}
		}
		
		private function get wireFromColor():uint
		{
            if(_wire.illegal) return ILLEGAL_COLOR;
			var modifier:IModifier = wire.masterAnchor.modifierDescription;
			
			if (modifier as PropertyDescription)
				return PROPERTY_COLOR;
			else if (modifier as OutletDescription)
				return OUTLET_COLOR;
			else if (modifier as InletDescription)
				return INLET_COLOR;
			else
				return 0;
		}
		
		private function get wireToColor():uint
		{
            if(_wire.illegal) return ILLEGAL_COLOR;
            var modifier:IModifier = wire.slaveAnchor.modifierDescription;
			if (modifier == null) {
				modifier = wire.masterAnchor.modifierDescription;
			}
			if (modifier as PropertyDescription)
				return PROPERTY_COLOR;
			else if (modifier as OutletDescription)
				return OUTLET_COLOR;
			else if (modifier as InletDescription)
				return INLET_COLOR;
			else
				return 0;
		}
		
		private function calculateGradient(oldColor:uint,newColor:uint,squishFactor:Number = 0):Vector.<Number>
		{
			var grad:Vector.<Number> = new Vector.<Number>();
			var steps:Number = _numberOfSteps;
			var gradSteps:Number = steps-steps*squishFactor;
			var gradStart:Number = steps*squishFactor/2;
			var gradStop:Number = gradStart + gradSteps;
			var iGrad:Number = 0;

			for (var i:int = 0; i < _numberOfSteps; i++)
			{
				if (oldColor != newColor) {
					if (i < gradStart) {
						grad.push(oldColor);
					} else if ( i > gradStop ) {
						grad.push(newColor);
					} else {
						var oldRed:Number = (oldColor & 16711680) >> 16;
						var newRed:Number = (newColor & 16711680) >> 16;
						var oldGreen:Number = (oldColor & 65280) >> 8;
						var newGreen:Number = (newColor & 65280) >> 8;
						var oldBlue:Number = (oldColor & 255);
						var newBlue:Number = (newColor & 255);
						grad.push(uint(oldRed+iGrad*(newRed-oldRed)/gradSteps << 16 | oldGreen+iGrad*(newGreen-oldGreen)/gradSteps << 8 | oldBlue+iGrad*(newBlue-oldBlue)/gradSteps));
						iGrad++;
					}
				} else {
					grad.push(oldColor);
				}
			}
			return grad;
		}
		
		public function clearArrows():void 
		{
			_slaveEnd.graphics.clear();
			_masterEnd.graphics.clear();
		}
		
		private function highlightRibbons(value:Boolean):void
		{
			wire.masterAnchor.highlight = value;
			wire.slaveAnchor.highlight = value;
		}
		
		private function handleRollOver(e:MouseEvent):void
		{
            if(_application.wireDragDescription || _wire.illegal) {
                return;
            }

			const HALF:Number = PortView.WIDTH/2;
			var worldContainer:WorldContainer = _application.viewContext as WorldContainer;
			var click:Point = new Point(worldContainer.mouseX, worldContainer.mouseY);
			
			var masterEnd:Boolean = Geometry.distanceBetweenPoints(click.x,click.y,_wire.x,_wire.y) < HALF;
			var slaveEnd:Boolean = Geometry.distanceBetweenPoints(click.x,click.y,_wire.x2,_wire.y2) < HALF;
			
			if (!masterEnd && !slaveEnd) {
				_rolled = true;
				highlightRibbons(true);
				updateView();
			}else{
				if(masterEnd){
					wire.masterAnchor.highlight = true;
				}else if(slaveEnd){
					wire.slaveAnchor.highlight = true;
				}
				this.addEventListener(MouseEvent.MOUSE_MOVE, handleRollOver);	
			}
		}
		
		private function handleRollOut(e:MouseEvent):void
		{
			this.removeEventListener(MouseEvent.MOUSE_MOVE, handleRollOver);
            _rolled = false;
			updateView();
			
			highlightRibbons(false);
		}
	}
}
