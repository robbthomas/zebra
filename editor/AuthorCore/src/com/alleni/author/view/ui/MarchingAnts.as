package com.alleni.author.view.ui
{
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Timer;

	// thanks to Mario Klingermann for the original script to inspire this, which is ages old! --PJK
	public class MarchingAnts extends Shape
	{
		private static const COPY_POINT:Point = new Point(1,0);
		
		private static var _patternMap:BitmapData;
		private static var _copyRect:Rectangle;
		private static var _timer:Timer;
		
		private static var _patternLength:Number;
		
		public function MarchingAnts(w:Number, h:Number)
		{
			super();
			
			if (!_patternMap)
				setPattern([0xaa990000,0x00000000,0x33990000,0x00000000], [4,2,4,2]); // red by default, easy to change or extend to client code, though
			draw(this, new Rectangle(0, 0, w, h), 100, 2);
		}

		private function setPattern(colors:Array=null, pattern:Array=null):void
		{
			if (!colors)
				colors = [0xff000000, 0xffffffff];

			if (!pattern || pattern.length != colors.length)
				pattern = [2, 2];

			initBitmap(colors, pattern);
		}

		private function draw(canvas:Shape, rect:Rectangle, interval:Number, steps:Number):void
		{
			paint(canvas, rect);
			
			if (!interval) interval = 32;
			if (interval != 0 && interval < 10) interval = 10;
			
			if (!steps) steps = 1;
			steps = ((steps % _patternLength) + _patternLength) % _patternLength;
			
			reset();
			if (interval != 0) {
				_timer = new Timer(interval);
				_timer.addEventListener(TimerEvent.TIMER, function(e:Event):void {update(steps)});
				_timer.start();
			}
		}

		private function reset():void
		{
			if (_timer) {
				_timer.stop();
				_timer = null;
			}
		}
		
		private function paint(canvas:Shape, r:Rectangle):void
		{
			var rect:Rectangle = new Rectangle( r.width < 0 ? r.x + r.width : r.x, r.height < 0 ? r.y + r.height : r.y, Math.abs(r.width), Math.abs(r.height));
			
			canvas.graphics.lineStyle();
			
			canvas.graphics.moveTo( rect.left, rect.top );
			canvas.graphics.beginBitmapFill( _patternMap, new Matrix( 1,0,0,1,rect.left % _patternLength ,0) );
			canvas.graphics.lineTo (rect.left + rect.width, rect.top  );
			canvas.graphics.lineTo (rect.left + rect.width, rect.top + 1 );
			canvas.graphics.lineTo (rect.left, rect.top + 1 );
			canvas.graphics.lineTo (rect.left, rect.top );
			canvas.graphics.endFill();
			
			canvas.graphics.moveTo( rect.left + rect.width, rect.top + 1 );
			canvas.graphics.beginBitmapFill( _patternMap, new Matrix( 0,1,1,0,0,1 + rect.top % _patternLength + _patternLength - rect.width % _patternLength) );
			canvas.graphics.lineTo (rect.left  + rect.width, rect.top + rect.height );
			canvas.graphics.lineTo (rect.left  + rect.width - 1, rect.top + rect.height );
			canvas.graphics.lineTo (rect.left  + rect.width- 1, rect.top + 1);
			canvas.graphics.lineTo (rect.left  + rect.width, rect.top + 1);
			canvas.graphics.endFill();
			
			canvas.graphics.moveTo( rect.left, rect.top + rect.height - 1 );
			canvas.graphics.beginBitmapFill( _patternMap, new Matrix( -1,0,0,1,  rect.left + rect.width - 1 -  ( _patternLength - ( rect.width + rect.height - 1) % _patternLength),0) );
			canvas.graphics.lineTo (rect.left + rect.width - 1, rect.top + rect.height -1 );
			canvas.graphics.lineTo (rect.left + rect.width - 1, rect.top + rect.height );
			canvas.graphics.lineTo (rect.left, rect.top + rect.height );
			canvas.graphics.lineTo (rect.left, rect.top + rect.height - 1 );
			canvas.graphics.endFill();
			
			canvas.graphics.moveTo( rect.left, rect.top + 1 );
			canvas.graphics.beginBitmapFill( _patternMap, new Matrix( 0,-1,1,0,0, 1 + rect.top % _patternLength  ) );
			canvas.graphics.lineTo (rect.left + 1,  rect.top + 1 );
			canvas.graphics.lineTo (rect.left + 1, rect.top + rect.height - 1 );
			canvas.graphics.lineTo (rect.left, rect.top + rect.height - 1 );
			canvas.graphics.lineTo (rect.left, rect.top + 1);
			canvas.graphics.endFill();
		}
		
		private function update(steps:Number):void
		{
			do {
				var p:Number = _patternMap.getPixel32(_patternMap.width - 1, 0);
				_patternMap.copyPixels(_patternMap, _copyRect, COPY_POINT);
				_patternMap.setPixel32(0, 0, p);
			} while (--steps > 0);
		}
		
		private function initBitmap(colors:Array, pattern:Array):void
		{
			_patternLength = 0;
			
			var i:int;
			for (i = 0; i < pattern.length; i++)
				_patternLength += pattern[i];
			
			if (_patternMap)
				_patternMap.dispose();
			_patternMap = new BitmapData(_patternLength, 1, true, 0);
			
			var x:Number = 0;
			for (i = 0 ; i < pattern.length; i++ )
				for (var j:int = 0 ; j <  pattern[i]; j++ )
					_patternMap.setPixel32(x++ , 0, colors[i]);
			
			_copyRect = new Rectangle(0, 0, _patternLength - 1, 1);
		}	
	}
}