package com.alleni.author.view.ui.palettes
{
	import assets.icons.dock.magglassTransparent;
	
	import caurina.transitions.Tweener;
	
	import com.alleni.author.controller.ui.DockControlsMediator;
	import com.alleni.author.controller.ui.palettes.DockController;
	import com.alleni.author.controller.ui.palettes.DockMediator;
	import com.alleni.author.definition.application.DockIcons;
	import com.alleni.author.model.objects.IListable;
	import com.alleni.author.model.ui.Dock;
	import com.alleni.author.util.cache.LRUCache;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.components.DockComboBox;
	import com.alleni.author.view.ui.components.DotPaging;
	import com.alleni.author.view.ui.components.HBox;
	import com.alleni.author.view.ui.components.Style;
	import com.alleni.author.view.ui.components.VBox;
	import com.alleni.author.view.ui.controls.LoadingSpinner;
	import com.alleni.taconite.model.ITaconiteCollection;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Rectangle;
	
	import mx.events.PropertyChangeEvent;
	
	public class DockView extends Sprite
	{
		private static const ITEM_SPACING_X:Number 		= DockController.ITEM_SPACING_X;
		private static const ITEM_SPACING_Y:Number 		= DockController.ITEM_SPACING_Y;
		private static const CONTAINER_PADDING:Number 	= DockController.CONTAINER_PADDING;
		private static const CONTROLS_MARGIN:Number 	= 5;
		public static const TEXT_COLOR:uint 			= Style.LABEL_TEXT;
		
		private var _width:int;
		private var _height:int;
		
		private var _model:Dock;
		private var _progressIndicator:DisplayObject;
		private var _rowContainer:VBox;
		private var _categoryMenu:DockComboBox;
		private var _viewMenu:DockComboBox;
		private var _dotPaging:DotPaging;
		private var _countLabel:LightLabel;
		private var _itemCache:LRUCache;
		[Bindable] private var _visibleItemCount:int = 0;
		
		public function DockView(width:int, height:int)
		{
			super();
			_width = width;
			_height = height;
			
			_itemCache = new LRUCache(50, 200);
			
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(event:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			Style.setStyle(Style.LIGHT);
			
			var textShadow:DropShadowFilter = new DropShadowFilter(1,80,0xffffff,0.8,2,2,2,1);
			var controlShadow:DropShadowFilter = new DropShadowFilter(1, 80, 0, 0.5, 2, 2, 0.5, 3);
			var fieldShadow:DropShadowFilter = new DropShadowFilter(1, 80, 0, 0.8, 2, 2, 0.5, 3, true);
			
			var frame:DockFrameView = new DockFrameView();
			frame.frameWidth = _width;
			frame.frameHeight = _height;
			frame.draw();
			addChild(frame);
			
			var dockBox:VBox = new VBox(this, CONTAINER_PADDING/2, CONTAINER_PADDING/4, CONTROLS_MARGIN);

			var controlsBox:HBox = new HBox(dockBox, CONTAINER_PADDING/2, 0, 38);
			
			_viewMenu = new DockComboBox(controlsBox, 0, 0, Dock.VIEWS[0].label, Dock.VIEWS, "");
			_viewMenu.filters = [textShadow];
			_viewMenu.selectedIndex = model.selectedView;
			_viewMenu.labelSize = 12;
			_categoryMenu = new DockComboBox(controlsBox, 0, 0, Dock.CATEGORIES_DEFAULT[0].label, [], "Category:");
			_categoryMenu.filters = [textShadow];
			_categoryMenu.width = 190;
			_categoryMenu.selectedIndex = model.selectedCategoryId;
			_categoryMenu.labelSize = 12;
			refreshCategoryMenu();
			var sortMenu:DockComboBox = new DockComboBox(controlsBox, 0, 0, Dock.SORT_OPTIONS[0].label, Dock.SORT_OPTIONS, "Sort By:");
			sortMenu.filters = [textShadow];
			sortMenu.width = 110;
			sortMenu.selectedIndex = model.selectedSort;
			sortMenu.labelSize = 12;
			
			var searchBox:HBox = new HBox(controlsBox, 0, 0, 0);
			
			var searchField:Sprite = new Sprite;
			searchField.graphics.lineStyle(1, 0, 0.1, true);
			searchField.graphics.beginFill(0xffffff, 0.85);
			searchField.graphics.drawRoundRect(-5, 0, 215, 16, 16, 16);
			searchField.graphics.endFill();
			searchField.filters = [fieldShadow];
			
			var searchLabel:LightEditableLabel = new LightEditableLabel(205, 16);
			searchLabel.y = 2;
			searchLabel.expandable = false;
			searchLabel.explicitLineBreak = true;
			searchLabel.color = TEXT_COLOR;
			searchLabel.editable = true;
			searchLabel.bold = false;
			searchLabel.embedFonts = true;
			searchLabel.size = 12;
			searchLabel.setPaddingParams({left:2, top:3, right:0, bottom:0});
			
			searchField.addChild(searchLabel);
			searchBox.addChild(searchField);
			
			const magGlass:DisplayObject = new magglassTransparent();
			magGlass.filters = [textShadow];
			magGlass.alpha = 0.7;
			searchBox.addChild(magGlass);
			searchBox.alignment = HBox.MIDDLE;
			
			controlsBox.alignment = HBox.MIDDLE;
			
			_rowContainer = new VBox(dockBox, 0, 0, ITEM_SPACING_Y/5);

			_dotPaging = new DotPaging(this);
			_dotPaging.y = _height - CONTROLS_MARGIN*4;
			_dotPaging.filters = [controlShadow, fieldShadow];
			
			_countLabel = new LightLabel(100, TEXT_COLOR, false, 18);
			_countLabel.x = CONTROLS_MARGIN*5; // manual position and hard coded as this is changing every other day.
			_countLabel.y = _height - CONTROLS_MARGIN*4 - 3;
			_countLabel.bold = true;
			_countLabel.size = 12;
			_countLabel.filters = [textShadow];
			this.addChild(_countLabel);
			
			this.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);
			model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);
			
			new DockMediator().handleSelectionEvents(this, model, _itemCache);
			new DockControlsMediator().handleControlEvents(searchLabel, _viewMenu, _categoryMenu, sortMenu, _dotPaging);
		}
		
		private function handlePropertyChange(event:PropertyChangeEvent):void
		{
			switch(event.property) {
				case "pageList":
					refreshCategoryMenu();
					pageList = model.pageList;
					break;
				case "selectedView":
					refreshViewMenu();
					break;
				case "listInProgress":
					showProgress = model.listInProgress;
					if (model.listInProgress)
						_countLabel.text = "...";
					else
						refreshNavigationCountLabel();
					break;
				case "_visibleItemCount":
					refreshNavigationCountLabel();
					break;
				default:
					break;
			}
		}
		
		private function refreshCategoryMenu():void
		{
			if (!_categoryMenu || !model.categories) return;
			
			var currentView:Object = Dock.VIEWS[model.selectedView];
			if (!currentView || !("projectTypeId" in currentView)) return;
			
			var categories:Array = [];
			var category:Object;
			var i:int;
			var length:int = model.categories.length;
			for (i = 0; i < length; i++) {
				category = model.categories[i];
				if ("projectTypeId" in category && category.projectTypeId == currentView.projectTypeId)
					categories.push(category);
			}
			categories.unshift(Dock.CATEGORIES_DEFAULT[0]);
			
			Style.setStyle(Style.LIGHT);
			_categoryMenu.numVisibleItems = categories.length;
			_categoryMenu.items = categories;
		}
		
		private function refreshViewMenu():void
		{
			if (!_viewMenu) return;
			_viewMenu.selectedIndex = model.selectedView;
		}
		
		private function refreshNavigationCountLabel():void
		{
			if (model.pageList && model.totalLength > 0) {
				if (model.totalLength > model.pageSize)
					_dotPaging.length = Math.ceil(model.totalLength/model.pageSize);
				else _dotPaging.length = 0;
			} else _dotPaging.length = 0;
			_dotPaging.x = (_width - _dotPaging.width)/2;
			_dotPaging.selectedIndex = model.pageIndex;
			
			if (_visibleItemCount > 0) {
				_countLabel.text = (model.pageSize * model.pageIndex + 1) + "-" + (model.pageSize * model.pageIndex + _visibleItemCount) + " of " + model.totalLength;
				return;
			}
			_countLabel.text = model.totalLength + " items";
		}
		
		private function get model():Dock
		{
			if (!_model)
				_model = Dock.instance;
			return _model;
		}
		
		private function set pageList(value:ITaconiteCollection):void
		{
			if (_rowContainer.numChildren != 0) {
				resetList(value);
				return;
			}
			
			const length:int = value.children.length;
			const oneItemSizeX:Number = DockIcons.SIZE_SMALL + ITEM_SPACING_X;
			const oneItemSizeY:Number = DockIcons.SIZE_SMALL + ITEM_SPACING_Y;
			const itemsPerRow:int = (_width - CONTAINER_PADDING * 2) / oneItemSizeX;
			const rowCount:int = Math.ceil(length/itemsPerRow);
			const maxRows:int = Math.round((_height - CONTAINER_PADDING * 1.5 + ITEM_SPACING_Y) / oneItemSizeY);
			
			var id:String;
			var itemView:DockItemView;
			var itemIndex:int = 0;
			var visibleItemCount:int = 0;
			var itemContainer:HBox = new HBox(_rowContainer, 0, 0, ITEM_SPACING_X-3);
			for each (var item:IListable in value.children) {
				if ("publishedId" in item)
					id = item["publishedId"];
				else id = item.id;
				
				if (itemIndex > itemsPerRow && _rowContainer.numChildren < maxRows) {
					itemContainer = new HBox(_rowContainer, 0, 0, ITEM_SPACING_X-3);
					itemIndex = 0;
				}
				
				if (itemIndex > itemsPerRow && _rowContainer.numChildren == maxRows)
					break;
				else
					visibleItemCount++;

				itemView = _itemCache.getValue(id) as DockItemView;
				if (!itemView) {
					itemView = new DockItemView(item);
					itemView.scrollRect = new Rectangle(-26, -26, 54, 70);
					itemContainer.addChild(itemView);
					_itemCache.setValue(id, itemView);
				} else {
					itemView.refreshItem(item);
					itemContainer.addChild(itemView);
				}
				itemIndex++;
			}
			_visibleItemCount = visibleItemCount;
			
			_rowContainer.visible = true;
			_rowContainer.alpha = 0;
			Tweener.addTween(_rowContainer, {alpha:1, time:0.15, transition:"easeInOutQuart"});
		}
		
		private function set showProgress(value:Boolean):void
		{
			if (value) {
				if (_progressIndicator)
					return;
				_progressIndicator = new LoadingSpinner(50, true);
				_progressIndicator.alpha = 0.5;
				addChild(_progressIndicator);
				_progressIndicator.x = (_width - _progressIndicator.width)/2;
				_progressIndicator.y = (_height - _progressIndicator.height)/2;
			} else {
				if (_progressIndicator) {
					if (this.contains(_progressIndicator))
						this.removeChild(_progressIndicator);
					_progressIndicator = null;
				}
			}
		}
		
		private function resetList(newList:ITaconiteCollection):void
		{
			Tweener.addTween(_rowContainer, {alpha:0, time:0.15, transition:"easeOutQuart",
				onComplete:function():void {
					_rowContainer.visible = false;
					while (_rowContainer.numChildren > 0) {
						var itemContainer:DisplayObjectContainer = _rowContainer.getChildAt(0) as DisplayObjectContainer;
						
						if (itemContainer) {
							while (itemContainer.numChildren > 0){
								var itemView:DockItemView = itemContainer.getChildAt(0) as DockItemView;
                                if(itemView){
									itemView.clearToolTip();
                                }
								itemContainer.removeChildAt(0);
                            }
						}	
						_rowContainer.removeChildAt(0);
					}
					pageList = newList;
				}});
		}
	}
}