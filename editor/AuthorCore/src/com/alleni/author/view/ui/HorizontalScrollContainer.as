package com.alleni.author.view.ui
{
	import com.alleni.author.event.ScrollUpdateEvent;
	import com.alleni.author.view.TruthTableScrollView;
	import com.alleni.author.view.objects.tables.AbstractTableView;
	import com.alleni.author.view.objects.tables.TableSection;
	
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;

	public class HorizontalScrollContainer extends TableSection
	{
		protected var _displayObject:DisplayObject;
		
		
		private var _scrollController:TruthTableScrollView;
		private var _actualWidth:Number;
		private var _leftShadow:Sprite;
		private var _rightShadow:Sprite;
		private var _bgColor:uint = 0x000000;
			
		public function HorizontalScrollContainer(displayObject:DisplayObject, scrollController:TruthTableScrollView, bgColor:uint)
		{
			super();
			
			_leftShadow = new Sprite();
			_rightShadow = new Sprite();
			
			_scrollController = scrollController;
			_scrollController.addEventListener(ScrollUpdateEvent.SCROLL, handleScroll);
			
			_bgColor = bgColor;
			
			_actualWidth = _scrollController.documentLength;

			_displayObject = displayObject;
			if(_displayObject) {
				_displayObject.scrollRect = new Rectangle(0, 0, width, height);
			
				height = _displayObject.height;  
				width = _scrollController.model.viewPortLength;
			}
			
			createAndAddChildren();
			
			addChild(_leftShadow);
			addChild(_rightShadow);
		}
		
		private function draw():void
		{
			_rightShadow.x = width - AbstractTableView.COLUMN_WIDTH/2;
			drawShadow(_leftShadow, true);
			drawShadow(_rightShadow, false);
			
			graphics.clear();
			graphics.beginFill(_bgColor);
			graphics.drawRect(0,0,width,height);
		}
		
		private function drawShadow(sprite:Sprite, left:Boolean):void
		{
			sprite.graphics.clear();
			sprite.mouseEnabled = false;
			var gradColors:Array = []; 
			var gradAlphas:Array = [];
			
			if (left) {
				gradColors = [0x000000, 0x000000];
				gradAlphas = [0.4,0];
			}
			else {
				gradColors = [0x000000, 0x000000];
				gradAlphas = [0, 0.4];
			}
			
			var gradRatios:Array = [0,255];
			var gradMatrix:Matrix = new Matrix();
			
			gradMatrix.createGradientBox(AbstractTableView.COLUMN_WIDTH/2, height, 0);
			
			sprite.graphics.beginGradientFill(GradientType.LINEAR,gradColors,gradAlphas,gradRatios,gradMatrix);
			sprite.graphics.drawRect(0,0, AbstractTableView.COLUMN_WIDTH/2, height);
		}
		
		private function createAndAddChildren():void
		{                
			if(_displayObject) {
				addChild(_displayObject);
				_displayObject.cacheAsBitmap = true;
			}
		}

		protected function handleScroll(event:ScrollUpdateEvent):void
		{
			if(_displayObject) {
				_displayObject.scrollRect = new Rectangle(event.params.scrollPosition, 0, width, height);
			}
		}
		
		override public function set width(value:Number):void
		{
			super.width = value;
			updateScrollController();
			draw();

			if(_displayObject) {
				_displayObject.scrollRect = new Rectangle(_displayObject.scrollRect.x, 0, width, height);
			}
		}
		
		override public function set height(value:Number):void
		{
			super.height = value;
			draw();

			if(_displayObject) {
				_displayObject.scrollRect = new Rectangle(_displayObject.scrollRect.x, 0, width, height);
			}
		}
		
		private function updateScrollController():void
		{
			
			var scrollPos:Number = _scrollController.scrollPosition;
			
			_scrollController.model.viewPortLength = width;
			_scrollController.scrollControllerWidth = width + _scrollController.buttonAreaWidth + _scrollController.hMargin;
			_scrollController.validate();
			_scrollController.scrollPosition = scrollPos;
			
		}
	}
}