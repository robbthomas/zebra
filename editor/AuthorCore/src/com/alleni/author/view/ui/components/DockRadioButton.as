package com.alleni.author.view.ui.components
{
	import assets.appui.componentSkins.radioEmpty;
	import assets.appui.componentSkins.radioOn;
	
	import flash.display.DisplayObjectContainer;
	
	public class DockRadioButton extends RadioButton
	{
		public function DockRadioButton(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, label:String="", checked:Boolean=false, defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, label, checked, defaultHandler, radioEmpty, radioOn);
		}
	}
}