package com.alleni.author.view.ui
{
	import flash.display.Sprite;

	public class AlignSwitch extends AbstractSwitch
	{
		// dimensions for both Horz & Vert
		private static const lineThickness:Number = 1.6;
		private static const lineLength:Number = 7;		
		
		// horz buttons
		private static const lineY:Number = 2;
		private static const leftX:Number = 3;
		private static const centerX:Number = 5.5;
		private static const rightX:Number = 8;
		
		// vert buttons
		private static const lineX:Number = 2;
		private static const topY:Number = 3;
		private static const middleY:Number = 5.5;
		private static const bottomY:Number = 8;
		
		private var _vertical:Boolean;
				
		public function AlignSwitch(setValueCallback:Function, vertical:Boolean)
		{
			_vertical = vertical;
			
			// this button is used for both horz & vert alignment
			// its for when Arena alignment has not been set, so an empty button is shown
			var blankAlign:SwitchButton = new SwitchButton(valueChanged, drawBlankAlign, "");

			// create array of buttons
			var buttons:Array;
			if (vertical) {
				var topAlign:SwitchButton = new SwitchButton(valueChanged, drawTopAlign, "top");
				var middleAlign:SwitchButton = new SwitchButton(valueChanged, drawMiddleAlign, "middle");
				var bottomAlign:SwitchButton = new SwitchButton(valueChanged, drawBottomAlign, "bottom");
				
				buttons = [topAlign, middleAlign, bottomAlign, blankAlign];
			} else {
				var leftAlign:SwitchButton = new SwitchButton(valueChanged, drawLeftAlign, "left");
				var centerAlign:SwitchButton = new SwitchButton(valueChanged, drawCenterAlign, "center");
				var rightAlign:SwitchButton = new SwitchButton(valueChanged, drawRightAlign, "right");
				
				buttons = [leftAlign, centerAlign, rightAlign, blankAlign];
			}
			
			super(setValueCallback, buttons);
		}
		
		override protected function get offset():Number
		{
			return 16;
		}
		
		private function drawLeftAlign(letter:Sprite, color:uint):void {

			letter.graphics.lineStyle(lineThickness,color);
			letter.graphics.moveTo(leftX,lineY);
			letter.graphics.lineTo(leftX,lineY+lineLength);			
		}
		
		private function drawCenterAlign(letter:Sprite, color:uint):void {

			letter.graphics.lineStyle(lineThickness,color);
			letter.graphics.moveTo(centerX,lineY);
			letter.graphics.lineTo(centerX,lineY+lineLength);		
		}
		
		private function drawRightAlign(letter:Sprite, color:uint):void {
			
			letter.graphics.lineStyle(lineThickness,color);
			letter.graphics.moveTo(rightX,lineY);
			letter.graphics.lineTo(rightX,lineY+lineLength);			
		}
		
		private function drawTopAlign(letter:Sprite, color:uint):void {
			
			letter.graphics.lineStyle(lineThickness,color);
			letter.graphics.moveTo(lineX,topY);
			letter.graphics.lineTo(lineX+lineLength,topY);			
		}
		
		private function drawMiddleAlign(letter:Sprite, color:uint):void {
			
			letter.graphics.lineStyle(lineThickness,color);
			letter.graphics.moveTo(lineX,middleY);
			letter.graphics.lineTo(lineX+lineLength,middleY);			
		}
		
		private function drawBottomAlign(letter:Sprite, color:uint):void {
			
			letter.graphics.lineStyle(lineThickness,color);
			letter.graphics.moveTo(lineX,bottomY);
			letter.graphics.lineTo(lineX+lineLength,bottomY);			
		}

		
		private function drawBlankAlign(letter:Sprite, color:uint):void {
		}
		
		override protected function buttonVisible(buttonValue:Object, currentValue:Object, valueType:String, rollover:Boolean):Boolean {
			
			if (buttonValue == "" && rollover)
				return false;  // never show the blank button during rollover (user will click reset button to get blank align)
			
			return super.buttonVisible(buttonValue,currentValue,valueType,rollover);
		}
		
		override protected function buttonColumn(buttonValue:Object, currentValue:Object, valueType:String, rollover:Boolean):int {
			
			if (buttonValue == "reset")
				return super.buttonColumn(buttonValue,currentValue,valueType,rollover);
			
			if (rollover) {
				switch (buttonValue) {  // the alignment buttons are at fixed positions, unlike the boolean buttons
					case "left":	return -1;
					case "center":	return 0;
					case "right":	return 1;
						
					case "top":	return -1;
					case "middle":	return 0;
					case "bottom":	return 1;
				}
			} else {
				if (buttonValue == currentValue)  // could be left, center, right or blank button
					return 0; // put at main column
			}
			
			// other cases: this button should not be showing ... so put it out-of-bounds, so I can see it for debug
			return -2;
		}
		
		
		override protected function doReset():void 
		{	
			// change to blank alignment value
			// tbd: this should be done by setting to default value in the AbstractSwitch, 
			//   but at this time we can't get the default value.  Waiting for defaults in xml file.
			setValue("", RibbonView.DEFAULT);
			super.doReset();
		}
	}
}