package com.alleni.author.view.ui.controls
{
	import com.alleni.author.controller.ui.GadgetScreenshotMediator;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.components.HBox;
	import com.alleni.author.view.ui.components.PushButton;
	import com.alleni.author.view.ui.components.Style;
	import com.alleni.author.view.ui.components.VBox;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class ScreenshotButtonContainer extends HBox
	{
	    private var _mediator:GadgetScreenshotMediator;
	    private var _gadget:Project;
	
	    private var _snapGadgetBtn:PushButton;
	    private var _snapScreenBtn:PushButton;
	    private var _loadGraphicBtn:PushButton;
	    private var _deleteGraphicBtn:PushButton;
	    private var _cancelBtn:PushButton;
	    private var _okBtn:PushButton;
	
	    private var _verticalLine:Sprite;
	
	    private var _snapPanel:HBox;
	    private var _utilPanel:HBox;
	    private var _LeftSideButtonsPanel:VBox;
	
	    private var _generalControlsPanel:HBox;
	    private var _rightSideButtonsPanel:VBox;
	
	    public function ScreenshotButtonContainer(parent:DisplayObjectContainer = null, xpos:Number = 0, ypos:Number =  0, spacing:Number = 10, gadget:Project = null, mediator:GadgetScreenshotMediator = null)
		{
	        super(parent, xpos, ypos, spacing);
	
	        _mediator = mediator;
	        _gadget = gadget;
	
	        _LeftSideButtonsPanel = new VBox(this);
	        _snapPanel = new HBox(_LeftSideButtonsPanel);
	        _utilPanel = new HBox(_LeftSideButtonsPanel);
	
	        _rightSideButtonsPanel = new VBox(this);
	        _generalControlsPanel = new HBox(_rightSideButtonsPanel);
	
	        _snapGadgetBtn = new PushButton();
			_snapGadgetBtn.defaultStyle = Style.DARK;
	        _snapScreenBtn = new PushButton(_snapPanel);
			_snapScreenBtn.defaultStyle = Style.DARK;
	        _loadGraphicBtn = new PushButton(_utilPanel);
			_loadGraphicBtn.defaultStyle = Style.DARK;
	        _deleteGraphicBtn = new PushButton(_utilPanel);
			_deleteGraphicBtn.defaultStyle = Style.DARK;
	        _cancelBtn = new PushButton(_generalControlsPanel);
			_cancelBtn.defaultStyle = Style.DARK;
	        _okBtn = new PushButton(_generalControlsPanel);
			_okBtn.defaultStyle = Style.DARK;
	
	        //Adding Panels to Container
	        addChild(_LeftSideButtonsPanel);
	        _LeftSideButtonsPanel.addChild(_snapPanel);
	        _LeftSideButtonsPanel.addChild(_utilPanel);
	
	        //Build panel line
	        _verticalLine = new Sprite();
	        _verticalLine.graphics.lineStyle(1, 0xFFFFFF);
	        _verticalLine.graphics.lineTo(0,50);
	        addChild(_verticalLine);
	
	        addChild(_rightSideButtonsPanel);
	        //Build panel label
	        var labelString:String = _mediator.isZapp?"App":"Gadget"
	        var title:LightLabel = new LightLabel(200, 0xFFFFFF, false, 22);
	        title.text = labelString + " Snapshots"
	        title.size = 14;
	        title.x = 20;
	        _rightSideButtonsPanel.addChild(title);
	        //add General controls
	        _rightSideButtonsPanel.addChild(_generalControlsPanel);
	
	        //Adding Buttons to panels
	        _snapGadgetBtn.label = "Snap Gadget";
	        _snapGadgetBtn.width = 90;
	        _snapGadgetBtn.height = 22;
	        _snapGadgetBtn.addEventListener(MouseEvent.CLICK, handleSnapGadget);
	        _snapGadgetBtn.addEventListener(MouseEvent.MOUSE_OVER, showGadgetFrame);
	        _snapGadgetBtn.addEventListener(MouseEvent.MOUSE_OUT, hideGadgetFrame);
	        if(!(_mediator.isZapp)){
	            _snapPanel.addChild(_snapGadgetBtn);
	        }
	
	        _snapScreenBtn.label = "Snap Stage";
	        _snapScreenBtn.width = 90;
	        _snapScreenBtn.height = 22;
	        _snapScreenBtn.addEventListener(MouseEvent.CLICK, handleSnapScreen);
	        _snapScreenBtn.addEventListener(MouseEvent.MOUSE_OVER, showStageFrame);
	        _snapScreenBtn.addEventListener(MouseEvent.MOUSE_OUT, hideStageFrame);
	        _snapPanel.addChild(_snapScreenBtn);
	
	        _loadGraphicBtn.label = "Load Graphic";
	        _loadGraphicBtn.width = 90;
	        _loadGraphicBtn.height = 22;
	        _loadGraphicBtn.addEventListener(MouseEvent.CLICK, handleLoadGraphic);
	        _utilPanel.addChild(_loadGraphicBtn);
	
	        _deleteGraphicBtn.label = "Delete";
	        _deleteGraphicBtn.width = 90;
	        _deleteGraphicBtn.height = 22;
	        _deleteGraphicBtn.addEventListener(MouseEvent.CLICK, handleDeleteGraphic);
	        _utilPanel.addChild(_deleteGraphicBtn);
	
	        _cancelBtn.label = "Cancel";
	        _cancelBtn.addEventListener(MouseEvent.CLICK, handleCancel);
	        _generalControlsPanel.addChild(_cancelBtn);
	
	        _okBtn.label = "OK";
	        _okBtn.addEventListener(MouseEvent.CLICK, handleOK);
	        _generalControlsPanel.addChild(_okBtn);
	    }
	
	    private function handleLoadGraphic(event:MouseEvent):void {
	        _mediator.showImportDialog(_gadget);
	    }
	
	    private function handleSnapScreen(event:MouseEvent):void {
	        _mediator.captureStage();
	    }
	
	    private function handleSnapGadget(event:MouseEvent):void {
	        _mediator.storeGadgetScreenshot();
	    }
	
	    private function handleDeleteGraphic(event:MouseEvent):void {
	        _mediator.deleteScreenshot();
	    }
	
	    private function handleCancel(e:MouseEvent):void {
	        _mediator.cancelScreenShotBar();
	    }
	
	    private function handleOK(e:MouseEvent):void {
	        _mediator.prepForPublishing();
	    }
	
	    private function showGadgetFrame(event:MouseEvent):void {
	        _mediator.displayGadgetFrame();
	    }
	
	    private function hideGadgetFrame(event:MouseEvent):void {
	        _mediator.hideFrame();
	    }
	
	    private function showStageFrame(event:MouseEvent):void {
	        _mediator.displayStageFrame();
	    }
	
	    private function hideStageFrame(event:MouseEvent):void {
	        _mediator.hideFrame();
	    }
	}
}
