package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;
	import caurina.transitions.properties.ColorShortcuts;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class IconButton extends Sprite
	{
		// Constants
		private static const ANIMATION:String = "easeOutExpo";
		private static const TIME:Number = 0.25;
				
		// Display
		private var _background:Sprite;
		private var _icon:Sprite;
		
		private var _iconOutColor:uint;
		private var _iconOverColor:uint;
		private var _iconDownColor:uint;
		
		private var _backgroundOutColor:uint;
		private var _backgroundOverColor:uint;
		private var _backgroundDownColor:uint;
		private var _backgroundAlpha:Number;
		
		public function IconButton(background:Sprite, icon:Sprite, iconOutColor:uint=0x7D7D7D, iconOverColor:uint=0xB4B4B4, iconDownColor:uint=0xFFFFFF,
										backgroundOutColor:uint=0x0099FF, backgroundOverColor:uint=0x0065C3, backgroundDownColor:uint=0x1A305C,
										backgroundAlpha:Number=1)
		{
			super();
			
			_background = background;
			_background.alpha = backgroundAlpha;
			_icon = icon;
			
			addChild(_background);
			addChild(_icon);
			positionIcon(true);
			
			_iconOutColor = iconOutColor;
			_iconOverColor = iconOverColor;
			_iconDownColor = iconDownColor;
			_backgroundOutColor = backgroundOutColor;
			_backgroundOverColor = backgroundOverColor;
			_backgroundDownColor = backgroundDownColor;
			_backgroundAlpha = backgroundAlpha;
			
			// Mouse event listeners
			this.addEventListener(MouseEvent.ROLL_OVER, handleRollOver);
			this.addEventListener(MouseEvent.ROLL_OUT, handleRollOut);
			this.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			this.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			
			// Init Tweener ColorShortcuts (required for color tweening)
			ColorShortcuts.init();
		}
		
		public function positionIcon(center:Boolean, xx:Number=0, yy:Number=0):void
		{
			if (center) {
				_icon.x = _background.width/2 - _icon.width/2;
				_icon.y = _background.height/2 - _icon.height/2;
			}
			else {
				_icon.x = xx;
				_icon.y = yy;
			}
		}
		
		private function handleRollOver(e:MouseEvent):void
		{
			Tweener.addTween(_icon, {_color:_iconOverColor, transition:ANIMATION, time:TIME});
			Tweener.addTween(_background, {_color:_backgroundOverColor, transition:ANIMATION, time:TIME});
			if (_backgroundAlpha == 0)
				Tweener.addTween(_background, {alpha:1, transition:ANIMATION, time:TIME});
		}
		
		private function handleRollOut(e:MouseEvent):void
		{
			Tweener.addTween(_icon, {_color:_iconOutColor, transition:ANIMATION, time:TIME});
			Tweener.addTween(_background, {_color:_backgroundOutColor, transition:ANIMATION, time:TIME});
			if (_backgroundAlpha == 0)
				Tweener.addTween(_background, {alpha:0, transition:ANIMATION, time:TIME});
		}
		
		private function handleMouseDown(e:MouseEvent):void
		{
			Tweener.addTween(_icon, {_color:_iconDownColor, transition:ANIMATION, time:TIME});
			Tweener.addTween(_background, {_color:_backgroundDownColor, transition:ANIMATION, time:TIME});
		}
		
		private function handleMouseUp(e:MouseEvent):void
		{
			Tweener.addTween(_icon, {_color:_iconOverColor, transition:ANIMATION, time:TIME});
			Tweener.addTween(_background, {_color:_backgroundOverColor, transition:ANIMATION, time:TIME});
		}

        public function get iconOutColor():uint
        {
            return _iconOutColor;
        }

        public function set iconOutColor(value:uint):void
        {
            _iconOutColor = value;
            Tweener.addTween(_icon, {_color:value, transition:ANIMATION, time:TIME});
        }
	}
}