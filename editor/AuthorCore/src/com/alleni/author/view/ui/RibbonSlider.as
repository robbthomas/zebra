package com.alleni.author.view.ui
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.NumberValueControlConstraint;

import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Point;

	public class RibbonSlider extends AbstractRibbonControl
	{
		public static const WIDTH:Number = 70; // the width of the ribbon title field
		private static const HEIGHT:Number = RibbonView.HEIGHT; // The height, not including the top knob
		
		private var _startingValue:Number; // The beginning value when this control appears
		private var _radix:int;
        protected var _sliderUpdateHappened:Boolean = false;
		
		private var startingValueMarker:Sprite;
		protected var startingKnob:Sprite;
		protected var selectedKnob:Sprite;
        protected var dragStrip:Sprite;
		
		private var startingValue:Number;
		private var currentValue:Number;
		private var dragging:Boolean;
        public var ribbonView:RibbonView;

        private var _constraint:NumberValueControlConstraint;

		private var _token:*;
		
		
		public function RibbonSlider(setValueCallback:Function, constraint:NumberValueControlConstraint)
		{
			super(setValueCallback);
			
			_constraint = constraint;
			
			startingValueMarker = new Sprite();
			startingKnob = new Sprite();
			selectedKnob = new Sprite();
            dragStrip = new Sprite();
            addChild(dragStrip);
			addChild(startingValueMarker);
			addChild(selectedKnob);
			addChild(startingKnob);

			startingKnob.addEventListener(MouseEvent.MOUSE_OVER, mouseOverStartingKnob);
			
			selectedKnob.addEventListener(MouseEvent.MOUSE_OUT, mouseOutSelectedKnob);
			selectedKnob.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownSelectedKnob);
			
			drawInitial();
		}
		
		protected function mouseOverStartingKnob(e:MouseEvent):void 
		{
			selectedKnob.alpha = 1;
			startingKnob.visible = false;
			showStartingValue(true);
		}
		
		protected function mouseOutSelectedKnob(e:MouseEvent):void 
		{
			if (!dragging) {
				startingKnob.visible = true;
				selectedKnob.alpha = 0;
				showStartingValue(false);
			}		
		}
		
		private function visibleFalse():void 
		{
			startingKnob.visible = false;
		}
		
		private function mouseDownSelectedKnob(e:MouseEvent):void 
		{
            /* TODO: This is here because we have a problem with the red arrow not hiding properly in very rare occasions.
            We need to deal with it in the future, but it is no longer crashing */
            if(ribbonView == null){
                e.stopPropagation();
                e.stopImmediatePropagation();
                return;
            }
			if(ribbonView.stage == null){
                e.stopPropagation();
			    e.stopImmediatePropagation();
                ribbonView = null;
                return;
            }
            // ***********************

            _token = ApplicationController.currentActionTree.openGroup("Pick color slider");
			dragging = true;
			Application.instance.draggingSlider = true;
			ribbonView.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveKnob);
			ribbonView.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpKnob);
			e.stopPropagation();
			e.stopImmediatePropagation();
            drawDragStrip();
		}
		
		private function mouseMoveKnob(e:MouseEvent):void
		{
			var xValue:Number = globalToLocal(new Point(e.stageX, e.stageY)).x;
			var value:Number = convertPixelsToValue(xValue);
			var valRounded:Number = Math.round(value);
			
			// Modify the value of the property the slider controls
			setRibbonValue(numToString(valRounded), true);
				
			if (value==_startingValue) {
				startingValueMarker.scaleX = 1.0;
				startingValueMarker.scaleY = 1.0;
			}
			else {
				startingValueMarker.scaleX = 0.6;
				startingValueMarker.scaleY = 0.6;
			}
			if (Math.abs(e.localX) > ribbonView.width*2 // beyond range to left
				|| e.stageX >= ribbonView.stage.stageWidth) { // beyond range, off stage (cannot use mouse_leave as this does not apply when button is held)
				mouseUpKnob(e);
			}
		}
		
		protected function setRibbonValue(value:String, fromSlide:Boolean = false):void
		{
			var valueNumber:Number = stringToNum(value);
			var tempMax:Number;
            var tempMin:Number;
			//trace("RibbonSlider:setRibbonvalue value="+value, "valueNumber="+valueNumber);
			/*
			If the value comes from the slider we constrain to the control constraint,
				if it comes in from another source we use the value constraint.
			 */
            if(fromSlide){
                tempMax = _constraint.controlMax;
                tempMin = _constraint.controlMin;
            }else{
                tempMax = _constraint.valueMax;
                tempMin = _constraint.valueMin;
            }

            if (valueNumber > tempMax){
                valueChanged(numToString(tempMax));
            }else if(valueNumber < tempMin) {
                valueChanged(numToString(tempMin));
            }else{
                valueChanged(numToString(valueNumber));
            }

            setValue(value, null);
		}
		
		private function mouseUpKnob(e:MouseEvent):void 
		{
			dragging = false;
			Application.instance.draggingSlider = false;
			ribbonView.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveKnob);
			ribbonView.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpKnob);
			if(_token != null) {
				ApplicationController.currentActionTree.closeGroup(_token);
				_token = null;
			}
            dragStrip.graphics.clear();
            var targetDisplayObject:DisplayObject = e.target as DisplayObject;
            if (ribbonView && targetDisplayObject && !ribbonView.contains(targetDisplayObject) && !this.contains(targetDisplayObject)) {
               ribbonView.rolled = false;
            }
            _sliderUpdateHappened = true;
		}

        private function drawDragStrip():void
		{
            dragStrip.graphics.lineStyle(1,0xeeeeee);
            dragStrip.graphics.moveTo(-this.x+3,0);
            dragStrip.graphics.lineTo(MessageCenterView.WIDTH-this.x-2,0);
        }
		
		private function drawInitial():void 
		{
			drawStartingKnob();
			drawSelectedKnob();
			drawStartingValueMarker();
		//	selectedKnob.alpha = 0;
		}
		
		private function drawStartingKnob():void 
		{
            drawKnob(startingKnob);
		}
		
		private function drawSelectedKnob():void 
		{
            drawKnob(selectedKnob);
		}

        private function drawKnob(knob:Sprite):void
		{
            var w:int = 15;
            var h:int = 9;
            knob.graphics.clear();
            knob.graphics.beginFill(0x0000ff,0.01);
            knob.graphics.moveTo(-w*2,1);
            knob.graphics.lineTo(-w/2,-h);
            knob.graphics.lineTo(w/2,-h);
            knob.graphics.lineTo(w*2,1);
            knob.graphics.lineTo(-w*2,1);
            knob.graphics.endFill();

//            knob.graphics.lineStyle(1,0xdddddd);
            knob.graphics.beginFill(0x555555,0.1);
            knob.graphics.drawRect(-w/2,-h,w,h+6);
            knob.graphics.endFill();

            knob.graphics.lineStyle(1,0xdddddd);
            knob.graphics.beginFill(0xff001b);
            knob.graphics.moveTo(0,4);
            knob.graphics.lineTo(-w/2+2,-h+4);
            knob.graphics.lineTo(w/2-2,-h+4);
            knob.graphics.lineTo(0,4);
            knob.graphics.endFill();

//            knob.graphics.lineStyle(2,0xffffff);
//            knob.graphics.moveTo(0,-3);
//            knob.graphics.lineTo(0,RibbonView.HEIGHT/3-2);
        }
		
		private function drawStartingValueMarker():void 
		{
			startingValueMarker.graphics.clear();
			
			startingValueMarker.graphics.beginFill(0,0);
			startingValueMarker.graphics.drawCircle(0,0,6);
			startingValueMarker.graphics.endFill();
			
			var triWidth:Number = 10;
			var triHeight:Number = 6.55;
			var startXPos:Number = 0-triWidth/2;
			var startYPos:Number = 1-triHeight/2;
			startingValueMarker.graphics.beginFill(0xffffff);
			startingValueMarker.graphics.moveTo(startXPos+triWidth/2, startYPos);
			startingValueMarker.graphics.lineTo(startXPos+triWidth, startYPos+triHeight);
			startingValueMarker.graphics.lineTo(startXPos, startYPos+triHeight);
			startingValueMarker.graphics.lineTo(startXPos+triWidth/2, startYPos);
			startingValueMarker.graphics.endFill();
			
			startingValueMarker.filters = [new GlowFilter(0x000000, 1)];
			startingValueMarker.y = RibbonView.HEIGHT-5;
			startingValueMarker.alpha = 0;
			
		// Glow filter
            selectedKnob.filters = [new GlowFilter(0x000000, 0.8)];
            startingKnob.filters = [new GlowFilter(0x000000, 0.6)];
		}
		
		protected function convertValueToPixels(value:Number):Number 
		{
			var pixels:Number;
			var newValue:Number = value;

			var factor:Number = (_constraint.controlMax-_constraint.controlMin)/WIDTH;

            if (newValue > _constraint.controlMax)
				newValue = _constraint.controlMax;
			if (newValue < _constraint.controlMin)
				newValue = _constraint.controlMin;

            pixels = (newValue-_constraint.controlMin)/factor;


			return Math.round(pixels);
		}
		
		protected function convertPixelsToValue(pixels:Number):Number 
		{
			var value:Number;
			var newPixels:Number = pixels;

			var factor:Number = (_constraint.controlMax-_constraint.controlMin)/WIDTH;

            if (newPixels > _constraint.controlMax)
				newPixels = _constraint.controlMax;
			if (newPixels < _constraint.controlMin)
				newPixels = _constraint.controlMin;

            value = pixels*factor + _constraint.controlMin;
			
			return Math.round(value);			
		}
		
		public function showStartingValue(show:Boolean):void 
		{
			startingValueMarker.x = selectedKnob.x;
			startingValueMarker.alpha = show?1:0;
			
			_startingValue = this.convertPixelsToValue(selectedKnob.x);
		}
		
		public function reset():void 
		{
			if (!dragging) {
				startingValueMarker.scaleX = 1;
				startingValueMarker.scaleY = 1;
				drawStartingValueMarker();
				selectedKnob.alpha = 0;
				startingKnob.alpha = 1;
                startingKnob.visible = true;
			}
		}
		
		override public function set visible(value:Boolean) : void 
		{
			if (!(!value && dragging)) // Don't fade out this control if we are dragging
				super.visible = value;
		}
		
		/**
		 * Modifies the appearance of this control when a value is set 
		 * @param value
		 * @param valueType
		 * 
		 */
		override public function setValue(value:Object, valueType:String):void
		{
			var valNum:Number = anythingToNum(value);
//			trace("Ribbon Slider, setting value="+value, "valNum="+valNum);
			var valRounded:Number = Math.round(valNum);

			if (valRounded > _constraint.valueMax){
				valRounded = _constraint.valueMax;
            }
			if (valNum < _constraint.valueMin){
				valRounded = _constraint.valueMin;
            }

			selectedKnob.x = convertValueToPixels(valRounded);
			startingKnob.x = selectedKnob.x;
		}
		
		protected function numToString(value:Number):String
		{
			return value.toString();  // decimal
		}
		
		protected function stringToNum(value:String):Number
		{
			if (value.substr(0,1) == "#")
				return Number("0x" + value.substr(1));
			else
				return Number(value);  // decimal
		}
		
		protected function anythingToNum(value:*):Number
		{
			if (value is String)
				return stringToNum(value as String);
			else
				return value as Number;
		}
	}
}