package com.alleni.author.view.ui.controls
{
	import com.alleni.author.model.ui.Application;

import flash.display.DisplayObject;
import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;

	public class Button extends Sprite
	{
		public static const CLICK:String = "buttonclick";
		
		public var id:String;
		public var toolTip:String;
		
		protected var _toggle:Boolean;
		protected var _selected:Boolean;
		
		protected var _skin_unsel_down_source:String;
		protected var _skin_unsel_roll_source:String;
		protected var _skin_unsel_up_source:String;
		
		protected var _skin_sel_down_source:String;
		protected var _skin_sel_roll_source:String;
		protected var _skin_sel_up_source:String;
		
		protected var _skin_unsel_down:DisplayObject;
		protected var _skin_unsel_roll:DisplayObject;
		protected var _skin_unsel_up:DisplayObject;
		
		protected var _skin_sel_down:DisplayObject;
		protected var _skin_sel_roll:DisplayObject;
		protected var _skin_sel_up:DisplayObject;
		
		protected var _assetsToLoad:int;
		protected var _assetsLoaded:int = 0;
        protected var _clickArea:Sprite;
        public var centerAssets:Boolean = false;
		
		public function Button()
		{
            _clickArea = new Sprite();
            this.addEventListener("child added",drawClickArea);
			super();
		}
		
		public function buildButton(toggle:Boolean, selected:Boolean = false, active:Boolean = true):void
		{
			_toggle = toggle;
			_selected = selected;
			if(_skin_unsel_down_source){
				loadAssets();
			}

			if (active) {
				addEventListener(MouseEvent.CLICK, buttonClicked);
				addEventListener(MouseEvent.MOUSE_DOWN, buttonMouseDown);
			}
			
			addEventListener(MouseEvent.ROLL_OVER, buttonRollover);
			addEventListener(MouseEvent.ROLL_OUT, buttonRollout);

			if(selected){
				addChild(_skin_sel_up);
                center(_skin_sel_up);
			}else{
				addChild(_skin_unsel_up);
                center(_skin_unsel_up);
			}
		}
		
		private function buttonClicked(event:MouseEvent):void
		{
			dispatchEvent(new Event(CLICK));
		}
		
		private function buttonRollover(event:MouseEvent):void
		{
			if(Application.instance.viewContext && !Application.instance.viewContext.info.userActionInProgress){
				if(selected){
					removeChildAt(0);
					addChildAt(_skin_sel_roll,0);
                    center(_skin_sel_roll);
				}else{
					removeChildAt(0);
					addChildAt(_skin_unsel_roll,0);
                    center(_skin_unsel_roll);
				}
			}
		}
		
		private function buttonRollout(event:MouseEvent):void
		{
			if(selected){
				removeChildAt(0);
				addChildAt(_skin_sel_up,0);
                center(_skin_unsel_up);
			}else{
				removeChildAt(0);
				addChildAt(_skin_unsel_up,0);
                center(_skin_unsel_up);
			}
		}
		
		private function buttonMouseDown(event:MouseEvent):void
		{
			if(selected){
				removeChildAt(0);
				addChildAt(_skin_sel_down,0);
                center(_skin_sel_down);
			}else{
				removeChildAt(0);
				addChildAt(_skin_unsel_down,0);
                center(_skin_unsel_down);
			}
		}
		
		private function updateButtonState():void
		{
			if(!_selected){
				removeChildAt(0);
				addChildAt(_skin_unsel_up,0);
                center(_skin_unsel_up);
			}else{
				removeChildAt(0);
				addChildAt(_skin_sel_up,0);
                center(_skin_sel_up);
			}
		}

        public function center(obj:DisplayObject):void {
            if (centerAssets)
                obj.x = -obj.width/2;
            else
                obj.x = 0;
        }
		
		protected function loadAssets():void
		{
			_skin_unsel_down = new Loader();
			(_skin_unsel_down as Loader).contentLoaderInfo.addEventListener(Event.COMPLETE, assetLoaded);
			(_skin_unsel_down as Loader).load(new URLRequest(_skin_unsel_down_source));
			
			_skin_unsel_roll = new Loader();
			(_skin_unsel_roll as Loader).load(new URLRequest(_skin_unsel_roll_source));
			(_skin_unsel_roll as Loader).contentLoaderInfo.addEventListener(Event.COMPLETE, assetLoaded);
			
			_skin_unsel_up = new Loader();
			(_skin_unsel_up as Loader).load(new URLRequest(_skin_unsel_up_source));
			(_skin_unsel_up as Loader).contentLoaderInfo.addEventListener(Event.COMPLETE, assetLoaded);
			_assetsToLoad = 3;
			
			if(_toggle){
				_skin_sel_down = new Loader();
				(_skin_sel_down as Loader).load(new URLRequest(_skin_sel_down_source));
				(_skin_sel_down as Loader).contentLoaderInfo.addEventListener(Event.COMPLETE, assetLoaded);
				
				_skin_sel_roll = new Loader();
				(_skin_sel_roll as Loader).load(new URLRequest(_skin_sel_roll_source));
				(_skin_sel_roll as Loader).contentLoaderInfo.addEventListener(Event.COMPLETE, assetLoaded);
				
				_skin_sel_up = new Loader();
				(_skin_sel_up as Loader).load(new URLRequest(_skin_sel_up_source));
				(_skin_sel_up as Loader).contentLoaderInfo.addEventListener(Event.COMPLETE, assetLoaded);
				
				_assetsToLoad = 6;
			}
		}
		
		protected function assetLoaded(event:Event):void
		{
			_assetsLoaded++;
			if(_assetsLoaded == _assetsToLoad){
				// assets are loaded!
                drawClickArea();
			}
		}
		
		//GETTERS and SETTERS
		public function get selected():Boolean
		{
			return _selected;
		} 
		
		public function set selected(value:Boolean):void
		{
			_selected = value;
			updateButtonState();
		}

		//ASSETS
		public function set skin_unsel_down(value:DisplayObject):void{
			_skin_unsel_down = value;
		}
		public function set skin_unsel_roll(value:DisplayObject):void{
			_skin_unsel_roll = value
		}
		public function set skin_unsel_up(value:DisplayObject):void{
			_skin_unsel_up = value;
		}
		
		public function set skin_sel_down(value:DisplayObject):void{
			_skin_sel_down = value;
		}
		public function set skin_sel_roll(value:DisplayObject):void{
			_skin_sel_roll = value;
		}
		public function set skin_sel_up(value:DisplayObject):void{
			_skin_sel_up = value;
		}
		
		//ASSET SOURCES
		public function set skin_unsel_down_source(value:String):void{
			_skin_unsel_down_source = value;
		}
		public function set skin_unsel_roll_source(value:String):void{
			_skin_unsel_roll_source = value;
		}
		public function set skin_unsel_up_source(value:String):void{
			_skin_unsel_up_source = value;
		}
		
		public function set skin_sel_down_source(value:String):void{
			_skin_sel_down_source = value;
		}
		public function set skin_sel_roll_source(value:String):void{
			_skin_sel_roll_source = value;
		}
		public function set skin_sel_up_source(value:String):void{
			_skin_sel_up_source = value;
		}

        private function drawClickArea(e:Event = null):void {
            _clickArea.graphics.clear();
            _clickArea.graphics.beginFill(0xff0000,0.02);
            _clickArea.graphics.drawRect(0,0,this.width,this.height);
            _clickArea.graphics.endFill();

            if (!this.contains(_clickArea))
                this.addChild(_clickArea);
            
                center(_clickArea);
        }

        override public function addChildAt(child:DisplayObject,i:int):DisplayObject {
            var r:DisplayObject = super.addChildAt(child,i);
            this.dispatchEvent(new Event("child added"));
            return r;
        }
	}
}