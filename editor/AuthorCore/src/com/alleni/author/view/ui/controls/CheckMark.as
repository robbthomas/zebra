package com.alleni.author.view.ui.controls
{
	import flash.display.Shape;
	
	public class CheckMark extends Shape
	{	
		public function CheckMark()
		{
			super();
			graphics.lineStyle(1,0xBEBEBE);
			graphics.moveTo(0,10);
			graphics.lineTo(5,15);
			graphics.lineTo(11,0);
		}
	}
}