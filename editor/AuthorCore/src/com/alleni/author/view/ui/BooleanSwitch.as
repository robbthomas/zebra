package com.alleni.author.view.ui
{
import com.alleni.author.definition.Modifiers;

import flash.display.Sprite;

	public class BooleanSwitch extends AbstractSwitch
	{
		private static const LETTER_THICKNESS:Number = 1.6;	

		private	var trueButton:SwitchButton;
		private	var falseButton:SwitchButton;
			
		public function BooleanSwitch(setValueCallback:Function, showReset:Boolean=true, firstPosition:Number=76, defaultColor:uint=0xFFFFFF)
		{
			var options:Object = {letterInactive:0xAFAFAF,fadeBackground:true};
			trueButton = new SwitchButton(valueChanged, drawTrueButton, "true", options, defaultColor);
			falseButton = new SwitchButton(valueChanged, drawFalseButton, "false", options, defaultColor);
		
			var buttons:Array = [trueButton, falseButton];
			
			super(setValueCallback, buttons, showReset, firstPosition);
		}

		override protected function valueChanged(value:Object):void {
			value = Modifiers.convertValue(Modifiers.BOOLEAN_TYPE, value);
			super.valueChanged(value);
		}

		private function drawTrueButton(letter:Sprite, color:uint):void
		{
			// T dimensions
			var tW:Number = 5;
			var tH:Number = 5.5;
			var tX:Number = 3;
			var tY:Number = 3;
			letter.graphics.lineStyle(LETTER_THICKNESS,color);
			letter.graphics.moveTo(tX,tY);
			letter.graphics.lineTo(tX+tW,tY);
			letter.graphics.moveTo(tX+(tW/2),tY);
			letter.graphics.lineTo(tX+(tW/2),tY+tH);
		}
		
		private function drawFalseButton(letter:Sprite, color:uint):void 
		{
			// F dimensions
			var fW:Number = 3;
			var fH:Number = 5.5;		
			var fX:Number = 4.5;
			var fY:Number = 3;
		
			letter.graphics.lineStyle(LETTER_THICKNESS, color);
			letter.graphics.moveTo(fX, fY);
			letter.graphics.lineTo(fX+fW,fY);
			letter.graphics.moveTo(fX,fY+3);
			letter.graphics.lineTo(fX+fW,fY+3);
			letter.graphics.moveTo(fX, fY);
			letter.graphics.lineTo(fX, fY+fH);				
		}
	}
}