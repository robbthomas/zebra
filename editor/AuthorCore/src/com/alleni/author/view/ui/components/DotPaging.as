package com.alleni.author.view.ui.components
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class DotPaging extends Component
	{
		private var _length:int = 0;
		private var _hBox:HBox;
		private var _selectedIndex:int = 0;
		
		public function DotPaging(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, length:int=0)
		{
			super(parent, xpos, ypos);
			this.length = length;
		}
		
		override protected function addChildren():void
		{
			super.addChildren();
			
			_hBox = new HBox(this);
		}
		
		private function DotFactory(index:int, selected:Boolean):DisplayObject
		{
			var item:DotPagingItem = new DotPagingItem(index, selected);
			item.addEventListener(MouseEvent.MOUSE_DOWN, handleItemMouseDown);
			return item;
		}
		
		private function handleItemMouseDown(event:Event):void
		{
			var item:DotPagingItem = event.target as DotPagingItem;
			
			if (!item) return;
			
			selectedIndex = item.index;
		}
		
		public function get selectedIndex():int
		{
			return _selectedIndex;
		}
		
		public function set selectedIndex(value:int):void
		{
			var i:int = 0;
			var length:int = _hBox.numChildren;
			for (i=0 ; i < length ; i++) {
				var item:DotPagingItem = _hBox.getChildAt(i) as DotPagingItem;
				if (!item) continue;
				if (item.index == value)
					item.selected = true;
				else
					item.selected = false;
			}
			
			_selectedIndex = value;
		}
		
		public function set length(value:int):void
		{
			_length = value;
			
			while (_hBox.numChildren > 0)
				_hBox.removeChildAt(0);
			
			var i:int;
			var selected:Boolean = false;
			for (i = 0; i < _length; i++) {
				if (_selectedIndex == i)
					selected = true;
				else selected = false;
				_hBox.addChild(DotFactory(i, selected));
			}
		}
		
		override public function get width():Number
		{
			return _hBox.width;
		}
	}
}