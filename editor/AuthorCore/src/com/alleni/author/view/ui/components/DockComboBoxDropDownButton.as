package com.alleni.author.view.ui.components
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ui.EditorUI;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.filters.DropShadowFilter;
	
	public class DockComboBoxDropDownButton extends PushButton
	{
		private var _arrow:Shape;
		
		
		public function DockComboBoxDropDownButton(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, label:String="", defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, label, defaultHandler);
			this.toggle = true;
		}
		
		/**
		 * Draws the face of the button, color based on state.
		 */
		override protected function drawFace():void
		{
			_face.graphics.clear();
			if(_down)
			{
				_face.graphics.beginFill(0x00aa00, 0);
			}
			else
			{
				_face.graphics.beginFill(0x0000aa, 0);
			}
			_face.graphics.drawRect(0, 0, _width - 2, _height - 2);
			_face.graphics.endFill();
		}
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			_labelText = "";
			super.draw();
			
			_back.graphics.clear();
			_back.graphics.lineStyle();
			_back.graphics.beginFill(0xaa0000, 0);
			_back.graphics.drawRect(0, 0, _width, _height);
			_back.graphics.endFill();

			if (!this.contains(arrow)) {
				this.addChild(arrow);
				arrow.x = _height/2;
				arrow.y = _height/2-1;
			}
			
			const ROTATION:Number = this.selected?90:0;
			if (arrow.rotation != ROTATION)
				Tweener.addTween(arrow, {rotation:ROTATION, time:0.2, transition:EditorUI.ANIMATION_IN_ALGORITHM });
		}
		
		private function get arrow():DisplayObject
		{
			const ARROW_WIDTH:Number = 8.0;
			const ARROW_HEIGHT:Number = 8.0;
			
			if (!_arrow) 
				_arrow = new Shape();
			else
				_arrow.graphics.clear();
			_arrow.graphics.beginFill(_labelColor, 0.4);
			_arrow.graphics.moveTo(-ARROW_WIDTH/2, -ARROW_HEIGHT/2);
			_arrow.graphics.lineTo(ARROW_WIDTH/2, 0);
			_arrow.graphics.lineTo(-ARROW_WIDTH/2, ARROW_HEIGHT/2);
			_arrow.graphics.lineTo(-ARROW_WIDTH/2, -ARROW_HEIGHT/2);
			
			return _arrow;
		}
		
		override public function set selected(value:Boolean):void
		{
			super.selected = value;
			draw();
		}
		
		/**
		 * DropShadowFilter factory method, used in many of the components.
		 * @param dist The distance of the shadow.
		 * @param knockout Whether or not to create a knocked out shadow.
		 */
		override protected function getShadow(dist:Number, knockout:Boolean = false):DropShadowFilter
		{
			return new DropShadowFilter(dist, 80, 0x000000, 0.5, dist, dist, .3, 3, knockout);
		}
	}
}