package com.alleni.author.view.ui.components
{
	import com.alleni.author.view.text.RichTextField;
	
	import flash.display.BlendMode;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import flashx.textLayout.edit.SelectionFormat;
	import flashx.textLayout.elements.Configuration;
	
	public class SearchComboLabel extends PushButton
	{
		internal var _textField:RichTextField;
		
		public function SearchComboLabel(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, label:String="", defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, label, defaultHandler);
		}
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren():void
		{
			_back = new Sprite();
			_face = new Sprite();
			
			var config:Configuration = new Configuration();
			var selectionFormat:SelectionFormat = new SelectionFormat(RichTextField.SELECTION_COLOR, RichTextField.SELECTION_ALPHA, BlendMode.MULTIPLY, _labelColor);
			config.inactiveSelectionFormat = selectionFormat;
			config.focusedSelectionFormat = selectionFormat;
			
			_textField = new RichTextField(this.width, 30, true, null, config);
			_textField.explicitLineBreak = true;
			_textField.editable = true;
			_textField.size = 14;
			_textField.setPaddingParams({left:0, top:0, right:0, bottom:0});
			addChild(_textField);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseGoDown);
			addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
		}
		
		/**
		 * Draws the face of the button, color based on state.
		 */
		override protected function drawFace():void
		{
			if (defaultStyle != null)
				Style.setStyle(defaultStyle);
		}
		
		
		///////////////////////////////////
		// public methods
		///////////////////////////////////
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			drawFace();
			_textField.italic = !_active;
			if (!_textField.editing)
				_textField.rawText = _labelText;
			_textField.textWidth = this.width;
		}
		
		
		///////////////////////////////////
		// getter/setters
		///////////////////////////////////
		
		public function get textField():RichTextField
		{
			return _textField;	
		}
		
		override public function set labelSize(value:int):void
		{
			_textField.size = value;
		}
		
		override public function set labelMultiline(value:Boolean):void
		{
		}
		
		override public function set labelColor(value:uint):void
		{
			_labelColor = value;
			_textField.color = value;
		}
		
		override public function set labelBold(value:Boolean):void
		{
			_textField.bold = value;
		}
	}
}
