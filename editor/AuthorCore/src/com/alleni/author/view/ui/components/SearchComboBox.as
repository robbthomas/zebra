package com.alleni.author.view.ui.components
{
	import com.alleni.author.view.text.RichTextField;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	
	public class SearchComboBox extends ComboBox
	{
		public function SearchComboBox(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, defaultLabel:String="", items:Array=null, labelPrefix:String="")
		{
			super(parent, xpos, ypos, defaultLabel, items, labelPrefix, SearchComboLabel);
		}
		
		public override function draw():void
		{
			super.draw();
			_dropDownButton.visible = false;
		}
		
		public function get textField():RichTextField
		{
			var searchComboLabel:SearchComboLabel = _labelButton as SearchComboLabel;
			if (searchComboLabel){
                return searchComboLabel.textField;
            }
			return null;
		}
		
		public function set showOptions(value:Boolean):void
		{
			onDropDown(null);	
		}
		
		public function get listShowing():Boolean
		{
			return !_stage || _stage.contains(_list);
		}
		
		override public function set labelSize(value:int):void
		{
			super.labelSize = value;
			listItemSize = value;
		}
		
		override protected function LabelButtonFactory(label:String):PushButton
		{
			if (_labelButtonClass != null)
				return new _labelButtonClass(this, 0, 0, label, handleLabelButtonClick);
			return ButtonFactory(label);
		}
		
		private function handleLabelButtonClick(e:Event):void
		{
		}
	}
}