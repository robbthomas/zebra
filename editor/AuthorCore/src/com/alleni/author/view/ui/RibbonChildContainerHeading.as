package com.alleni.author.view.ui
{
import com.alleni.author.view.ui.RibbonView;

import flash.display.Sprite;

import flash.geom.Rectangle;

public class RibbonChildContainerHeading extends RibbonContainerHeading
	{

		public function RibbonChildContainerHeading(color:uint, label:String, enableInspectorBehavior:Boolean=false)
		{
			super(color, label, enableInspectorBehavior);
		}
		
		override protected function drawButton():void 
		{
			// The button sprite is an invisible circle with an arrow sprite child (this ensures rotation around the center point)
			_button.graphics.clear();
			_button.graphics.drawCircle(0,0,5);
			
			_arrow.graphics.clear();
			_arrow.graphics.beginFill(0xFFFFFF);
			_arrow.graphics.moveTo(0,0);
			_arrow.graphics.lineTo(BUTTON_HEIGHT, 0);
			_arrow.graphics.lineTo(BUTTON_HEIGHT/2, BUTTON_WIDTH);
			_arrow.graphics.lineTo(0,0);
			_arrow.y = -_arrow.width/2;
			_arrow.x = -_arrow.width/2;
			_button.addChild(_arrow);
		}
		
		override protected function defineLayout():void{
			super.defineLayout();
			_label.x = _label.x - 14;
			_button.x = _button.x - 11

            _clickarea.x = _button.x - 3;
            _clickarea.y = _button.y;
		}
		
		override protected function drawBackground(bkColor:uint):void 
		{
            // no background necessary
			graphics.clear();
		}
	}
}