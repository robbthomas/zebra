/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 6/24/11
 * Time: 8:44 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.ui.controls {
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.GadgetScreenshotMediator;
	import com.alleni.author.definition.application.PublishIcons;
	import com.alleni.author.model.gadgets.GadgetScreenshot;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.service.ThumbnailLoadingPipeline;
	import com.alleni.author.service.gadgets.GadgetScreenshotPreview;
	import com.alleni.author.view.ui.components.HBox;
import com.alleni.author.view.ui.components.VBox;
import com.alleni.taconite.dev.BitmapUtils;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	
	public class ScreenshotThumbnailConatiner extends VBox {
	    protected const BACKGROUND:uint = 0xBFBFBF;
	    protected const UNSELECTED_FRAME_COLOR:uint = 0xBBBBBB;
	    protected const SELECTED_FRAME_COLOR:uint = 0xFFFFFF;
	    protected const TOTAL_NUMBER_THUMBNAILS:int = 8;
	
	    protected const FROM_SERVER:String = "From Server";
	    protected const FROM_LOCAL:String = "From Local";
	
	
	    protected var _thumbnailDictionary:Dictionary;
	
	    protected var _gadget:Project;
	    protected var _mediator:GadgetScreenshotMediator;
	    protected var _activeScreenshot:int = -1;
	    protected var _thumbBorder:Sprite = new Sprite();
	    protected var _previewPanel:GadgetScreenshotPreview;
	    protected var _size:Number;
	    protected var _isZapp:Boolean
        protected var _topContainer:HBox;
        protected var _bottomContainer:HBox;
	
	    public function ScreenshotThumbnailConatiner(parent:DisplayObjectContainer = null, xpos:Number = 0, ypos:Number =  0, spacing:Number = 5,
													 gadget:Project = null, mediator:GadgetScreenshotMediator = null, publishing:Boolean = true,
													 isZapp:Boolean = false, size:Number = 70):void
		{	
	//        TODO: Spell name correctly
	        super(parent, xpos, ypos, spacing);
	        _thumbnailDictionary = new Dictionary();
	        _gadget = gadget;
	        _mediator = mediator;
	        _isZapp = isZapp;
	        _size = size;
	        if(_gadget != null && _gadget.screenshots != null){
	            for each(var screenshot:GadgetScreenshot in _gadget.screenshots.screenShots){
	                _thumbnailDictionary[screenshot.visibleIndex] = buildThumbnail(screenshot);
	                addThumbListeners(screenshot, FROM_SERVER);
	            }
	        }
            _topContainer = new HBox();
            _bottomContainer = new HBox();
            _bottomContainer.x = 21;
	        //If not publishing no empties.
	        if(publishing || _size == 70){;
	            var i:int;
	            for(i=0; i<TOTAL_NUMBER_THUMBNAILS; i++){
	                if(!_thumbnailDictionary[i]){
	                    _thumbnailDictionary[i] = buildShotFrame(_size, _size);
	                    addFrameListeners(_thumbnailDictionary[i]);
	                    if(_activeScreenshot == -1){
	                        activeScreenshot = i;
	                    }
	                }
	            }
	        }
            if(_mediator != null) {
                updateThumbnails();
            }

            addChild(_topContainer);

	        renderContainer();
	        selectContainer(findNextEmptyContainer(_activeScreenshot));
	    }
	
	    public function updateThumbnails(event:ApplicationEvent = null):void{
	        var mediator:GadgetScreenshotMediator;
	        if(event != null){
	            mediator = event.data as GadgetScreenshotMediator;
	        }else{
	            mediator = _mediator;
	        }

	        if(mediator.pendingUploads == null){
	            return;
	        }

	        var i:uint;
	        allThumbs: for(i = 0; i<TOTAL_NUMBER_THUMBNAILS; i++){
	            if(i in mediator.pendingUploads){
	                _thumbnailDictionary[i] = buildThumbnailFromLocal((mediator.pendingUploads[i] as GadgetScreenshot).imageBitmapData);
                    addThumbListeners(mediator.pendingUploads[i] as GadgetScreenshot, FROM_LOCAL);
	            }else{
	                if(_gadget.screenshots != null){
	                    for each(var screenshot:GadgetScreenshot in _gadget.screenshots.screenShots){
                            if(screenshot.visibleIndex != i) {
                                // keep looking for the item at this index
                                continue;
                            } else if(screenshot.visibleIndex in mediator.pendingUploads) {
	                            // This item was replaced nothing needs to be done at this time
                                // even if it was deleted
                                // we've already built a thumbnail for this spot above
                                continue allThumbs;
	                        } else if(screenshot.screenShotId in mediator.pendingDeletes) {
                                // this is deleted without an add, so fall down to default
                                break;
	                        } else {
                                // this item is still just hanging out so skip it
                                //TODO load the server screenshot
                                // so that this method can be a catch all update at any time
                                // and no other method need be used
                                continue allThumbs;
                            }
	                    }
	                }
                    _thumbnailDictionary[i] = buildShotFrame(_size, _size);
                    addFrameListeners(_thumbnailDictionary[i]);
	            }
	
	        }
	        renderContainer();
	    }
	
	    protected function addThumbListeners(screenshot:GadgetScreenshot, location:String):void{
	        addFrameListeners(_thumbnailDictionary[screenshot.visibleIndex] as Sprite);
	    }
	
	    protected function addFrameListeners(frame:Sprite):void{
	       frame.addEventListener(MouseEvent.CLICK, findContainer, false, 0, true);
	    }
	
	    protected function screenshotClickListener(index:int, location:String):Function {
	        return function(e:Event):void{
	                    if(_previewPanel != null){
	                        if(EditorUI.instance.rawChildren.contains(_previewPanel)){
	                            EditorUI.instance.rawChildren.removeChild(_previewPanel);
	                        }
	                        _previewPanel = null;
	                    }
	                    preview(index, location)
	                }
	    }
	
	    public function updateThumb(image:BitmapData):void{
	        var location:int = _activeScreenshot;
	        _thumbnailDictionary[location] = buildThumbnailFromLocal(image);
	        (_thumbnailDictionary[location] as DisplayObject).addEventListener(MouseEvent.CLICK, function(e:Event):void{
	                    if(location == _activeScreenshot){
	                        preview(location, FROM_LOCAL);
	                    }else{
	                        selectContainer(location);
	                    }
	                });
	        selectContainer(findNextEmptyContainer(location));
	        renderContainer();
	        _gadget.dispatchEvent(new ApplicationEvent(ScreenshotMiniThumbnailContainer.UPDATE_SCREENSHOTS, _mediator));
	    }
	
	    public function removeThumbnail(index:int):void{
	        _thumbnailDictionary[index] = buildShotFrame(70, 70);
            addFrameListeners(_thumbnailDictionary[index]);
	        selectContainer(index);
	        renderContainer();
	        _gadget.dispatchEvent(new ApplicationEvent(ScreenshotMiniThumbnailContainer.UPDATE_SCREENSHOTS, _mediator));
	    }
	
	    public function selectContainer(index:int):void{
	        if(index == -1){
	            return;
	        }
	        activeScreenshot = index;
	        highlightThumbnail(_activeScreenshot);
	    }
	
	    public function findNextEmptyContainer(index:int):int{
	        if(index == -1) return -1;
	
	        var i:int;
	        for(i = index; i<TOTAL_NUMBER_THUMBNAILS; i++){
	            if((_thumbnailDictionary[i] as Sprite).numChildren == 1){
	                return i;
	            }
	        }
	        for(i = 0;i<index;i++){
	            if((_thumbnailDictionary[i] as Sprite).numChildren == 1){
	                return i;
	            }
	        }
	        return 0;
	    }
	
	    public function renderContainer():void{
	        while(_topContainer.numChildren > 0){
	            _topContainer.removeChildAt(0);
	        }

            while(_bottomContainer.numChildren > 0){
	            _bottomContainer.removeChildAt(0);
	        }

	        var i:int;
	        for(i = 0; i<TOTAL_NUMBER_THUMBNAILS; i++){
	            if(i in _thumbnailDictionary){
                    if(_size == 70 || i < 4 || _isZapp){
	                    _topContainer.addChild(_thumbnailDictionary[i]);
                        if(this.contains(_bottomContainer)){
                            this.removeChild(_bottomContainer);
                        }
                    } else {
                        addChild(_bottomContainer);
                        _bottomContainer.addChild(_thumbnailDictionary[i]);
                    }
	            }
	        }
	    }
	
	    protected function findContainer(event:MouseEvent = null):void{
	        var i:int;
	        for(i = 0; i<TOTAL_NUMBER_THUMBNAILS; i++){
	            if(_thumbnailDictionary[i] == event.currentTarget){
	                selectContainer(i);
	            }
	        }
	    }
	
	    public function buildThumbnailFromLocal(imageData:BitmapData):Sprite{
	        var image:Bitmap = new Bitmap(imageData);
	        image = BitmapUtils.bilinearDownscale(image, 70, 70);
	        var thumbnail:Sprite = buildShotFrame(image.width, image.height, false);
	        image.x = (thumbnail.width-1 - image.width)/2;
	        image.y = (thumbnail.height-1 - image.height)/2;
	        thumbnail.addChild(image);
	        return thumbnail;
	    }
	
	    public function buildThumbnail(screenshot:GadgetScreenshot):Sprite{
	        var thumbnail:Sprite = new Sprite();
	        ThumbnailLoadingPipeline.push(screenshot.getImageUrl("70"), function(event:Event):void {
									var loaderInfo:LoaderInfo = LoaderInfo(event.target);
									thumbnail.addChild(buildThumbnailFromLocal((loaderInfo.loader.content as Bitmap).bitmapData))
	                                renderContainer();
								});
	        return thumbnail;
	    }
	
	    public function buildShotFrame(width:Number, height:Number, addCamera:Boolean = true):Sprite{
	        var shotFrame:Sprite = new Sprite();
	        shotFrame.graphics.beginFill(BACKGROUND);
	        shotFrame.graphics.lineStyle(1, UNSELECTED_FRAME_COLOR);
	        shotFrame.graphics.drawRect(-1, -1, width+2, height+2);
	        shotFrame.graphics.endFill();
	        var icon:DisplayObject = PublishIcons.screenshotIconUp;
	        icon.x = (width - icon.width)/2;
	        icon.y = (height - icon.height)/2;
	        icon.visible = addCamera;
	        shotFrame.addChild(icon);
	        return shotFrame;
	    }
	
	    protected function highlightThumbnail(index:int):void{
	        var i:int;
	        for(i = 0; i<TOTAL_NUMBER_THUMBNAILS; i++){
	            if((_thumbnailDictionary[i] as Sprite).contains(_thumbBorder)){
	                (_thumbnailDictionary[i] as Sprite).removeChild(_thumbBorder);
	            }
	        }
	        _thumbBorder.graphics.clear();
	        _thumbBorder.graphics.lineStyle(2, UNSELECTED_FRAME_COLOR);
	        _thumbBorder.graphics.drawRect(0, 0, (_thumbnailDictionary[index] as Sprite).width, (_thumbnailDictionary[index] as Sprite).height);
	        _thumbBorder.graphics.beginFill(0x000000, .01);
	        _thumbBorder.graphics.lineStyle(2, SELECTED_FRAME_COLOR);
	        _thumbBorder.graphics.drawRect(-1, -1, (_thumbnailDictionary[index] as Sprite).width+1, (_thumbnailDictionary[index] as Sprite).height+1);
	        _thumbBorder.graphics.endFill();
	        if((_thumbnailDictionary[index] as Sprite).numChildren > 1){
	            var location:String;
	            if(index in _mediator.pendingUploads){
	                location = FROM_LOCAL;
	            }else{
	                location = FROM_SERVER;
	            }
	        }
	        (_thumbnailDictionary[index] as Sprite).addChild(_thumbBorder);
	    }
	
	    public function preview(index:Number, location:String):void{
	        var sizedBitmap:Sprite = new Sprite();
	        if(location == FROM_SERVER){
	            for each(var screenshot:GadgetScreenshot in _gadget.screenshots.screenShots){
	                if(screenshot.visibleIndex == index){
	                    ThumbnailLoadingPipeline.push(screenshot.getImageUrl("450"), function(event:Event):void {
	                                var loaderInfo:LoaderInfo = LoaderInfo(event.target);
	                                sizedBitmap.addChild(loaderInfo.loader.content as Bitmap);
	                                _previewPanel.centerImage();
	                            });
	                }
	            }
	        }else if(location == FROM_LOCAL){
	            sizedBitmap.addChild(BitmapUtils.bilinearDownscale(new Bitmap((GadgetScreenshotMediator.instance.pendingUploads[index] as GadgetScreenshot).imageBitmapData as BitmapData), 450, 450));
	        }else{
	            return;
	        }
	
	        _previewPanel = new GadgetScreenshotPreview(GadgetScreenshotPreview.GAGDET_SCREENSHOTS, (_isZapp?"App":"Gadget") + " Snapshots", sizedBitmap, this);
	        _previewPanel.x = (Application.instance.width - 460)/2;
	        _previewPanel.y = (Application.instance.height - _previewPanel.height)/2;
	        ApplicationController.instance.captureMouseClicks(true, killPreviewPanel);
	        EditorUI.instance.rawChildren.addChild(_previewPanel);
	    }
	
	    public function killPreviewPanel(event:Event = null):void{
	        if(_previewPanel != null){
	            if(EditorUI.instance.rawChildren.contains(_previewPanel)){
	                EditorUI.instance.rawChildren.removeChild(_previewPanel);
	                _previewPanel = null;
	                ApplicationController.instance.captureMouseClicks(false);
	            }
	        }
	    }
	
	    //Getters and Setters
	    public function get activeScreenshot():int {
	        return _activeScreenshot;
	    }
	
	    public function set activeScreenshot(value:int):void {
	        _activeScreenshot = value;
	    }
	}
}
