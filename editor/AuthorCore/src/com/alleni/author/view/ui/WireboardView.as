package com.alleni.author.view.ui
{
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.WireController;
	import com.alleni.author.controller.ui.WireboardMediator;
	import com.alleni.author.definition.InletDescription;
	import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
	import com.alleni.author.model.ui.InternalSmartWireAnchor;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
	import com.alleni.taconite.lang.TaconiteTimer;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	import mx.containers.ApplicationControlBar;
	
	public class WireboardView extends Sprite
	{	
		public static const LEFT:int = 0;
		public static const RIGHT:int = 1;
		
		public static const WIDTH:int = 15;
		public static const TOP_MARGIN:int = 10;
		public static const PROPERTY:String = "property";
		public static const INLET:String = "inlet";
		public static const OUTLET:String = "outlet";
		
		private static const Y_FIRST_ITEM:Number = 8;
		private static const Y_ITEM_SPACING:Number = 16;
		
		public static const INLET_WEIGHT:Number = 100;
		public static const PROPERTY_WEIGHT:Number = 200;
		public static const OUTLET_WEIGHT:Number = 300;
		public static const ANGLE_WEIGHT:Number = 50;  // angle in radians is added to this
		public static const UNWIRED_PORT_WEIGHT:Number = 98; 
		public static const BUTTON_WEIGHT:Number = 99;  // button at end of each category
		
		public static const ROLL_HILITE_ALPHA:Number = 0.3; 
		
		
		public var other:WireboardView;
		public var side:int;
		
		private var _smart:SmartObject;
		private var _ports:Vector.<IWireboardItem> = new Vector.<IWireboardItem>(0);
		private var _portsContainer:Sprite;
		private var _rollHilite:Sprite;
		private var _context:ViewContext;
		private var _numPegs:int;
		private var _mediator:WireboardMediator;
		
		public function WireboardView(smart:SmartObject, context:ViewContext, side:int)
		{
			super();
			_context = context;
			_smart = smart;
			this.side = side;
			
			_portsContainer = new Sprite();
			addChild(_portsContainer);
						
			_mediator = new WireboardMediator(context, this, smart);

            if(smart is Composite) {
			    createButton(INLET);
            }
			createButton(PROPERTY);
			createButton(OUTLET);
		}
		
		public function createPort(anchor:InternalSmartWireAnchor):PortView
		{
			for each (var item:IWireboardItem in _ports) {
				if (item is PortView && PortView(item).anchor == anchor)
					return PortView(item);
			}

			var port:WireboardPortView = new WireboardPortView(anchor, _context as WorldContainer, this);
			anchor.register(port);
			anchor.portSide = this.side;
			port.angle = this.side*179 + 1;
			port.x = WIDTH / 2;
			_ports.push(port);
			_portsContainer.addChild(port);
			return port;
		}
		
		private function createButton(buttonType:String):void
		{
			var button:WireboardButton = new WireboardButton(buttonType, _context, this);
			button.x = WIDTH / 2;
			_portsContainer.addChild(button);
			_ports.push(button);
			_mediator.handleEventsForButton(button);
		}
		
		public function draw(hh:int):void
		{
			graphics.clear();
			graphics.beginFill(0xd7d7d7,0.5);
			graphics.lineStyle(0,0,0);
			graphics.drawRect(1,1,WIDTH-1,hh-1);
			graphics.endFill();
			
			_numPegs = (hh - Y_FIRST_ITEM + Y_ITEM_SPACING/2) / Y_ITEM_SPACING;
			for (var n:int=0; n < _numPegs; n++) {
				graphics.beginFill(0x484848);
				graphics.lineStyle(0,0,0);
				graphics.drawCircle(WIDTH/2, Y_FIRST_ITEM + n * Y_ITEM_SPACING, 1.5);
			}
		}
			
		
		public function recalc():void
		{
			// add ports as needed (new or moved from other side)  (mediator set anchor.portSide)
			var anchor:InternalSmartWireAnchor;
			for each (anchor in _smart.innerAnchors) {
				if (anchor.portSide == this.side && findPort(anchor) == null)
					createPort(anchor);
			}

			// remove ports that have moved to the other side, or have been deleted
			var n:int;
			var item:IWireboardItem;
			for (n=_ports.length-1; n >= 0; n--) {
				item = _ports[n];
				if (item as WireboardPortView) {
					var port:WireboardPortView = WireboardPortView(item);
					anchor = port.anchor as InternalSmartWireAnchor;
					if (anchor.portSide != this.side || !(anchor.modifierDescription.key in _smart.innerAnchors)) {
						_portsContainer.removeChild(port as DisplayObject);
						_ports.splice(n, 1);
					}
				}
			}
			
			// compute weights for vertical sorting
			for (n=0; n < _ports.length; n++) {
				item = _ports[n];
				item.weight = computeWeight(item);
			}
			
			_ports.sort( function(a:IWireboardItem, b:IWireboardItem):Number { return (a.weight - b.weight)*10000000; } );
			
			var yy:Number = Y_FIRST_ITEM;
			for (n=0; n < _ports.length; n++) {
				item = _ports[n];
				item.y = yy;
				yy += Y_ITEM_SPACING;
//				trace("  n="+n, "side="+side, "weight="+item.weight, "type="+item.type, "item="+item);
			}
			ApplicationController.instance.wireController.requestRedrawForObject(_smart);
		}
		
		private function computeWeight(item:IWireboardItem):Number
		{
			var weight:Number = 0;
			
			switch (item.type) {
				case PROPERTY:
					weight = PROPERTY_WEIGHT;
					break;
				case INLET:
					weight = INLET_WEIGHT;
					break;
				case OUTLET:
					weight = OUTLET_WEIGHT;
					break;
			}
			
			if (item as WireboardPortView) {
				// to minimize wire crossings...
				// get angle from top of this wireboard to the average of this anchor's wire ends
				var base:Point = this.localToGlobal(new Point(0,0));  // top of wireboard
				var angle:Number = WireboardPortView(item).averageWireAngle(base);
				if (angle == UNWIRED_PORT_WEIGHT)
					weight += UNWIRED_PORT_WEIGHT;
				else
					weight += ANGLE_WEIGHT + ((side==LEFT) ? angle : -angle);
			} else {
				weight += BUTTON_WEIGHT;
			}
			return weight;
		}
		
		private function findPort(anchor:WireAnchor):PortView
		{
			for (var n:int=0; n < _ports.length; n++) {
				var item:IWireboardItem = _ports[n];
				if (item as PortView && PortView(item).anchor == anchor)
					return PortView(item);
			}
			return null;
		}
	
		public function hilitePoint(local:Point):void
		{
			var top:Number = Y_FIRST_ITEM - Y_ITEM_SPACING/2;  // top of the top cell
			var index:int = (local.y - top) / Y_ITEM_SPACING;
			hilitePort(index, true);
		}
		
		public function pointToAnchor(local:Point):InternalSmartWireAnchor
		{
			var top:Number = Y_FIRST_ITEM - Y_ITEM_SPACING/2;  // top of the top cell
			var index:int = (local.y - top) / Y_ITEM_SPACING;
			if (index >= 0 && index < _ports.length) {
				var item:IWireboardItem = _ports[index];
				if (item as PortView) {
					return PortView(item).anchor as InternalSmartWireAnchor;
				}
			}
			return null;
		}
		
		public function hiliteAnchor(anchor:ExternalSmartWireAnchor, hilite:Boolean):void
		{
			for (var n:int=_ports.length-1; n >= 0; n--) {
				var item:IWireboardItem = _ports[n];
				if (item as PortView) {
					var itemAnchor:InternalSmartWireAnchor = PortView(item).anchor as InternalSmartWireAnchor;
					if (itemAnchor == anchor.other)
						hilitePort(n, hilite);
				}
			}
		}
			
		public function hilitePort(index:int, hilite:Boolean):void
		{
			if (!hilite || index < 0 || index >= _numPegs || isButtonItem(index)) {
				removeHilite();
			} else {
				if (_rollHilite == null)
					createRollHilite();
				var top:Number = Y_FIRST_ITEM - Y_ITEM_SPACING/2;  // top of the top cell
				_rollHilite.y = top + index * Y_ITEM_SPACING;
				_rollHilite.visible = true;
			}
			
			if (hilite && index >= 0 && index < _ports.length)
				showItemTooltip(_ports[index]);
			else
				ApplicationController.instance.hideToolTip();
		}
		
		public function removeHilite():void
		{
			if (_rollHilite)
				_rollHilite.visible = false;
			ApplicationController.instance.hideToolTip();
		}
		
		private function createRollHilite():void
		{
			_rollHilite = new Sprite();
			addChild(_rollHilite);
			_rollHilite.mouseEnabled = false;
			_rollHilite.x = 0;
			var g:Graphics = _rollHilite.graphics;
			g.beginFill(0x000000, ROLL_HILITE_ALPHA);
			g.drawRect(0,0, WIDTH, Y_ITEM_SPACING);
			g.endFill();
		}
		
		private function showItemTooltip(item:IWireboardItem):void
		{
			var text:String = "";
			if (item is PortView && PortView(item).anchor.label!=null) {
				var intAnchor:InternalSmartWireAnchor = PortView(item).anchor as InternalSmartWireAnchor;
				var extAnchor:ExternalSmartWireAnchor = intAnchor.other;
                if (extAnchor == null)
                    return;
				text = intAnchor.label;
			} else if (item is WireboardButton) {
				text = "Click and drag to create a wire";
			} else {
				return;
			}
			var global:Point = DisplayObject(item).localToGlobal(new Point(0,0));
			var direction:String;
			if (side == LEFT) {
				direction = ControlAreaToolTip.DISPLAY_LEFT;
				global.x -= 24;
				global.y += 26;
			} else {
				direction = ControlAreaToolTip.DISPLAY_RIGHT;
				global.x += 22;
				global.y -= 12;
			}
			ApplicationController.instance.displayToolTip(global, direction, text); 
		}
		
		private function isButtonItem(n:int):Boolean
		{
			if (n > 0 && n < _ports.length) {
				return (_ports[n] is WireboardButton);
			}
			return false;
		}
		
		public function get minHeight():Number
		{
			return _ports.length * Y_ITEM_SPACING;
		}
		
		public function get object():SmartObject
		{
			return _smart;
		}
		
		public function convertToCustomAnchor(incomplete:InternalSmartWireAnchor, fromAnchor:WireAnchor):PortView
		{
			return _mediator.convertToCustomAnchor(incomplete, fromAnchor);
		}
	}
}