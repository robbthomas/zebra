package com.alleni.author.view.ui
{
	public class DynamicDropDownMenu extends RibbonDropDownMenu
	{
		public function DynamicDropDownMenu(setValueCallback:Function, dataProvider:Array, isScaleable:Boolean = true)
		{
			super(setValueCallback, dataProvider, isScaleable);
		}

        // step 1
        protected function initializeDataProvider():void{

            _dataProvider = [];
        }

        // step 2
        protected function addData(data:Object, label:String, sort:String = null):void{
            _dataProvider.push({data:data,label:label,sort:sort});
        }

        // step 3
        protected function setDataProvider():void{

            _dataProvider = _dataProvider.sort(orderBy);
        }

        private function orderBy(a:*, b:*):int
		{
            var label1:String;
            var label2:String;

            if(a.sort) {
                label1 = a.sort;
            } else {
                label1 = a.label;
            }

            if(b.sort) {
                label2 = b.sort;
            } else {
                label2 = b.label;
            }

            if (label1 < label2)
			{ 
				return -1; 
			} 
			else if (label1 > label2) 
			{ 
				return 1; 
			} 
			else 
			{ 
				return 0; 
			} 
		} 
	}
}