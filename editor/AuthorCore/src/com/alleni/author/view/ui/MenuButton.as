/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.ui
{
	import caurina.transitions.*;
	import caurina.transitions.properties.ColorShortcuts;
	
	import com.alleni.author.view.text.RichLabel;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.BevelFilter;
	import flash.filters.DropShadowFilter;
import flash.geom.Point;
import flash.geom.Rectangle;

public class MenuButton extends Sprite
	{

	// Properties
		private var _toggle:Boolean;
		private var _selected:Boolean;
		private var _text:String;
		
	// Components
		private var _label:RichLabel;
        private var _clickArea:Sprite;
        private var _clickMargin:Point;
		private var _button:Sprite;
		private var _buttonBorder:Sprite;
		private var _buttonShadow:Sprite;
		
	// Display
		private var _labelColor:uint;
		private var _labelOverColor:uint;
		private var _selectedLabelColor:uint;
		private var _selectedLabelOverColor:uint;	
		private var _borderColor:uint;
		private var _borderOverColor:uint;
		private var _borderDownColor:uint;
		private var _selectedBorderColor:uint;
		private var _selectedBorderOverColor:uint;
		private var _color:uint;
		private var _overColor:uint;
		private var _selectedColor:uint;
		private var _selectedOverColor:uint;
		private var _fontSize:Number = 8;
	
	// Constants
		private static const ANIMATION:String = "easeOutExpo";
		private static const TIME:Number = 0.25;
		
	// Methods and arguments
		private var _drawFunction:Function;
		private var _toggleOnFunction:Function;
		private var _toggleOnArgs:Array;
		private var _buttonGraphic:Sprite;
		private var _toggleOffFunction:Function;
		private var _toggleOffArgs:Array;
		
		/**
		 * Creates an instance of MenuButton.
		 *  
		 * @param drawFunction:	The function used to draw the button shape in defineButton(). The function you pass must handle a sprite object argument.
		 * 						For example, you could pass the following function to draw a circular button:
		 * 							drawInletButton(btn:Sprite):void{btn.graphics.drawCircle(0,0,5)}
		 * 						The function should not include any call to graphics.beginFill() or graphics.lineStyle(). That is handled by setting
		 * 						the color and borderColor properties.
		 * @param toggleOnFunction: The function called when clicking the button (or when toggling the button on when toggle=true).
		 * @param toggleOnArgs: The arguments passed to the toggleOnFunction.
		 * @param toggleOffFunction: The function called when toggling the button off (not used when toggle=false)
		 * @param toggleOffArgs: The arguments passed to the toggleOffFunction.
		 * @param buttonGraphic: A sprite placed on the button (typically instead of a label, as in the arena control bar)
		 * 
		 */
		public function MenuButton(drawFunction:Function, toggleOnFunction:Function, toggleOnArgs:Array=null,
										toggleOffFunction:Function=null, toggleOffArgs:Array=null, buttonGraphic:Sprite=null,clickMargin:Point = null)
		{
			super();
			
			_drawFunction = drawFunction;
			_toggleOnFunction = toggleOnFunction;
			_toggleOnArgs = toggleOnArgs;
			_toggleOffFunction = toggleOffFunction;
			_toggleOffArgs = toggleOffArgs;
			_buttonGraphic = buttonGraphic;

			
		// Instantiate and add children
            _clickArea = new Sprite();
			_clickMargin = clickMargin;
            _button = new Sprite();
			_buttonBorder = new Sprite();
			_buttonShadow = new Sprite();
			_label = new RichLabel();
            addChild(_clickArea);
			addChild(_button);
			addChild(_buttonShadow);
			addChild(_buttonBorder);
			addChild(_label);
			
		// Event listeners
			this.addEventListener(MouseEvent.CLICK, mouseClickListener);
			this.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener);
			this.addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
			this.addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
		
		// Init Tweener ColorShortcuts (required for color tweening)
			ColorShortcuts.init();
		
		// Set defaults
			_text = "";
			_labelColor = 0xFFFFFF;
			_color = 0x000000;
			_borderColor = 0x000000;
			_borderOverColor = 0xFFFFFF;
			_borderDownColor = 0xB0B0B0;
			_buttonShadow.alpha = 0;

		// Draw Button
			draw();
		}
		
		private function draw():void {
			defineButton(_button);
			defineButton(_buttonShadow);
			defineButton(_buttonBorder, true);
			addFilters();
			formatLabel();
            var clickArea:Rectangle = _buttonBorder.getRect(this);
            if (_clickMargin != null) {
                clickArea.x -= _clickMargin.x;
                clickArea.y -= _clickMargin.y;
                clickArea.width += _clickMargin.x*2;
                clickArea.height += _clickMargin.y*2;
            }
            _clickArea.graphics.clear();
            _clickArea.graphics.beginFill(0xffffff,0.01);
            _clickArea.graphics.drawRect(clickArea.x,clickArea.y,clickArea.width,clickArea.height);
            _clickArea.graphics.endFill();
		}

		 
		/**
		 * Draws the button using the drawFunction received in the constructor.
		 * 
		 * @param btn:		The sprite object that is being defined.
		 * @param border:	When true, draw a border, not a filled-in shape.
		 * 
		 */
		 
		private var _btn:Sprite;
		
		protected function defineButton(btn:Sprite, border:Boolean=false):void {
			
			btn.graphics.clear();
			
			if (border) {
				btn.graphics.lineStyle(1);
				_drawFunction(btn);
			}
			else {
				btn.graphics.beginFill(_color);
				_drawFunction(btn);
			}
		}
		
		private function addFilters():void {
			
			var bevel:BevelFilter = new BevelFilter();
			bevel.highlightAlpha = 0.7;
			bevel.shadowAlpha = 0.5;
			bevel.blurX = 2;
			bevel.blurY = 2;
			bevel.distance = 2;
			bevel.quality = 5;
			
			var dsfTop:DropShadowFilter = new DropShadowFilter();
			dsfTop.inner = true;
			dsfTop.color = 0x000000;
			dsfTop.angle = 270;
			dsfTop.distance = 2;
			dsfTop.quality = 5;
			
			var dsfBottom:DropShadowFilter = new DropShadowFilter();
			dsfBottom.inner = true;
			dsfBottom.color = 0x000000;
			dsfBottom.angle = 90;
			dsfBottom.distance = 2;
			dsfBottom.quality = 5;
			
			_button.filters = [bevel];
			_buttonShadow.filters = [dsfTop, dsfBottom];
		}

		protected function formatLabel():void {
			
			_label.text = _text;
			_label.width = _button.width;
			_label.height = _button.height;
			_label.y = this.height/2 - _label.height/2 //-1;
			_label.x = -1;

			_label.color = _labelColor;
			_label.size = _fontSize;
			_label.center = true;
			
			if (_buttonGraphic) {
				this.addChild(_buttonGraphic);
			}
					
		}
		
		
	// Event handlers
	
		private function mouseClickListener( e:MouseEvent = null ):void 
		{
			
			if (_toggle) {
				
				_selected = !_selected;
				
				if (_selected) {
					fireFunction(_toggleOnFunction, _toggleOnArgs);
				}
				else {
					fireFunction(_toggleOffFunction, _toggleOffArgs);
				}
			}
			else
				fireFunction(_toggleOnFunction, _toggleOnArgs);
		}
		
		
		/**
		 * Executes the toggleOn or toggleOff function specified in the constructor.
		 *  
		 * @param func:	Function to be executed.
		 * @param args:	Array of arguments used by func.
		 * 
		 */
		private function fireFunction(func:Function, args:Array):void 
		{
			if (args)
				func.apply(this, args);
			else
				func.apply();
		}

		private function mouseDownListener( e:MouseEvent = null ):void 
		{
			addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
			Tweener.addTween(this._buttonShadow, {alpha:0.5, transition:ANIMATION, time:TIME});
			Tweener.addTween(_label, {alpha:0.7, transition:ANIMATION, time:TIME});
			Tweener.addTween(this._buttonBorder, {_color:_borderDownColor, transition:ANIMATION, time:0.5});
		}

		private function mouseUpListener( e:MouseEvent = null ):void {

			removeEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
			Tweener.addTween(_label, {alpha:1, transition:ANIMATION, time:TIME});
			if (!_toggle) {
				Tweener.addTween(this._buttonShadow, {alpha:0, transition:ANIMATION, time:TIME});
				return;
			}
			
			if (_selected)
				Tweener.addTween(this._buttonShadow, {alpha:0, transition:ANIMATION, time:TIME});
			else
				Tweener.addTween(this._buttonShadow, {alpha:1, transition:ANIMATION, time:TIME});
			
		}
		
		private function rollOverListener( e:MouseEvent = null ):void {

				Tweener.addTween(this._buttonBorder, {_color:_borderOverColor, transition:ANIMATION, time:0.5});
		}
		
		private function rollOutListener( e:MouseEvent = null ):void {
			
				Tweener.addTween(this._buttonBorder, {_color:_borderColor, transition:ANIMATION, time:0.5});
				if (selected)
					Tweener.addTween(this._buttonBorder, {_color:_borderDownColor, transition:ANIMATION, time:0.5});
				else
					Tweener.addTween(this._buttonBorder, {_color:_borderColor, transition:ANIMATION, time:0.5});
		}
		
		
	// Child getters and setters

		public function set labelText(value:String):void {
			_text = value;
			formatLabel();
		}
		
		public function set labelFontSize(value:Number):void
		{
			_fontSize = value;
			formatLabel();
		}
		
		public function set labelBold(value:Boolean):void
		{
			_label.bold = value;
		}

        public function get labelOpacity():Number {
            return _label.alpha;
        }

        public function set labelOpacity(value:Number):void {
            _label.alpha = value;
        }

		public function set toggle(value:Boolean):void {
			_toggle = value;
		}
		
		public function get selected():Boolean {
			return _selected;
		}

		public function set selected(value:Boolean):void {
			_selected = !value;
			mouseUpListener();
			if(_toggle)
				_selected = !_selected;
			rollOutListener();
		}
		
		public function deselect():void {
			_selected = true;
			mouseUpListener();
			if(_toggle)
				_selected = !_selected;
			rollOutListener();
		}
		
	// Color getters and setters
			
		public function set labelColor(value:uint):void {
			_labelColor = value;
			formatLabel();
		}
		
		public function set labelOverColor(value:uint):void {
			_labelOverColor = value;
		}
		
		public function set selectedLabelColor(value:uint):void {
			_selectedLabelColor = value;
		}
		
		public function set selectedLabelOverColor(value:uint):void {
			_selectedLabelOverColor = value;
		}
		
		public function set borderColor(value:uint):void {
			_borderColor = value;
		}
		
		public function set borderDownColor(value:uint):void {
			 _borderDownColor = value;
		}
		
		public function get borderColor():uint {
			return _borderColor;
		}
		
		public function set borderOverColor(value:uint):void {
			 _borderOverColor = value;
			 draw();
		}
		
		public function set selectedBorderColor(value:uint):void {
			 _selectedBorderColor = value;
		}
		
		public function set selectedBorderOverColor(value:uint):void {
			 _selectedBorderOverColor = value;
		}
		
		public function set color(value:uint):void {
			 _color = value;
			 draw();
		}
		
		public function get color():uint {
			return _color;
		}
		
		public function set overColor(value:uint):void {
			 _overColor = value;
		}
		
		public function set selectedColor(value:uint):void {
			 _selectedColor = value;
		}
		
		public function set selectedOverColor(value:uint):void {
			 _selectedOverColor = value;
		}
	}
}