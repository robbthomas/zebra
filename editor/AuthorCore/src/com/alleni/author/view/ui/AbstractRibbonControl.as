package com.alleni.author.view.ui
{
	import com.alleni.author.application.ApplicationUI;
    import com.alleni.author.controller.AuthorController;
    import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
import com.alleni.author.model.ui.NumberValueControlConstraint;
import com.alleni.author.view.ui.editors.RibbonAssetEditor;
	import com.alleni.author.view.ui.editors.TransformationEditor;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;

	public class AbstractRibbonControl extends Sprite
	{
		protected var _setValueCallback:Function;
		protected var _width:Number;
		protected var _height:Number;
        private var _writable:Boolean = true;
		
				
		public function AbstractRibbonControl(setValueCallback:Function, width:Number=-1, height:Number=-1)
		{
			_setValueCallback = setValueCallback;
			_width = width;
			_height = height;
			fixScrollRect();
		}

		protected function valueChanged(value:Object):void
		{
			if(_setValueCallback != null) {
				_setValueCallback(value);
			}
		}

		protected function fixScrollRect():void
		{
			if(_width >= 0 && _height >= 0) {
				this.scrollRect = new Rectangle(0, 0, _width, _height);
			}
		}

		override public function get width():Number
		{
			return _width;
		}

		override public function set width(value:Number):void
		{
			_width = value;
			fixScrollRect();
		}

		override public function get height():Number
		{
			return _height;
		}

		override public function set height(value:Number):void
		{
			_height = value;
			fixScrollRect();
		}

		public function setValue(value:Object, valueType:String):void
		{
			// overridden by subclasses
		}

		public function openEditing():void
		{

		}

		public function closeEditing(e:Event):void
		{

		}

        public function get writable():Boolean {
            return _writable;
        }

        public function set writable(value:Boolean):void {
            _writable = value;
            this.mouseEnabled = _writable;
            this.mouseChildren = _writable;
        }

        public static function createControl(propertyDescription:PropertyDescription, setValueCallback:Function, hostObject:AbstractObject,
											 	currentValue:Object, width:Number, height:Number, background:Boolean=true, textColor:uint = 0xFFFFFF, isScaleable:Boolean = true):AbstractRibbonControl
		{
			var valueControl:AbstractRibbonControl;
			switch (propertyDescription.type) {
				case Modifiers.COLOR_TYPE:
					valueControl = new RibbonColorPicker(setValueCallback, width, height);
					break;
				case Modifiers.BOOLEAN_TYPE:
					valueControl = new BooleanSwitch(setValueCallback, true, 76, textColor);
					valueControl.x = PortView.WIDTH;
					valueControl.setValue(currentValue, Modifiers.BOOLEAN_TYPE);
					break;
				case Modifiers.HORIZONTAL_ALIGN_TYPE:
					valueControl = new AlignSwitch(setValueCallback, false); 
					break;
				case Modifiers.VERTICAL_ALIGN_TYPE:
					valueControl = new AlignSwitch(setValueCallback,true); 
					break;
				case Modifiers.UNSIGNED_NUMBER_TYPE:
				case Modifiers.NUMBER_TYPE:
				case Modifiers.INT_TYPE:
					valueControl = new RibbonValueField(setValueCallback, width, height, background, propertyDescription.readOnly);
					(valueControl as RibbonValueField).color = textColor;
					break;
				case Modifiers.ENUMERATED_TYPE:
				case Modifiers.POINT_TYPE:
					valueControl = new RibbonDropDownMenu(setValueCallback, (propertyDescription.constraints as Array), isScaleable);
					break;
				case Modifiers.FILL_TYPE:
					var fillSpec:GraphicFill = (currentValue as GraphicFill) ? currentValue as GraphicFill : new GraphicFill();
					valueControl = new RibbonFillPicker(ApplicationUI.instance, fillSpec, setValueCallback, null, hostObject, width, height);
					break;
				case Modifiers.POSITION_TYPE:
					valueControl = new TransformationEditor(setValueCallback, hostObject, width, height, propertyDescription);
					break;
				case Modifiers.OBJECT_TYPE:
					valueControl = new ObjectReferenceField(setValueCallback, width, height, background, propertyDescription.readOnly, hostObject, propertyDescription);
					(valueControl as RibbonValueField).color = textColor;
					break;
                case Modifiers.EVENTPAGE_TYPE:
                    valueControl = new PagerDropDownMenu(AuthorController.instance.world.eventFlow,setValueCallback,[]);
                    break;
                case Modifiers.FORMATTED_TEXT_TYPE:
					/*PBR-4 Restore read-only after text markup is reinstated */
					//valueControl = new RibbonValueField(setValueCallback, width, height, background, propertyDescription.readOnly, true);
					valueControl = new RibbonValueField(setValueCallback, width, height, background, false, false, propertyDescription.type);
					(valueControl as RibbonValueField).color = textColor;
					break;
				case Modifiers.LIST_TYPE:
					valueControl = new RibbonValueField(setValueCallback, width, height, background, true, false);
					(valueControl as RibbonValueField).color = textColor;
					break;
				case Modifiers.STYLE_TYPE:
					var label:String = currentValue as String;
					valueControl = new StylePicker(ApplicationUI.instance, label, setValueCallback, null, hostObject, width, height);
					break;
				case Modifiers.KEY_TYPE:
					valueControl = new RibbonValueField(setValueCallback, width, height,true);
					(valueControl as RibbonValueField).valueBackgroundColor = 0x333333;
					(valueControl as RibbonValueField).prompt = "key..";
					break;
				case Modifiers.IMAGE_ASSET_TYPE:
				case Modifiers.AUDIO_ASSET_TYPE:
				case Modifiers.VIDEO_ASSET_TYPE:
				case Modifiers.TEXT_ASSET_TYPE:
				case Modifiers.FONT_ASSET_TYPE:
					valueControl = new RibbonAssetEditor(setValueCallback, width, height, hostObject, propertyDescription);
					break;
				case Modifiers.FONT_ENUMERATED_TYPE:  
					valueControl = new FontDropDownMenu(AssetType.FONT,setValueCallback, (propertyDescription.constraints as Array), isScaleable);
					break;
				default:
					valueControl = new RibbonValueField(setValueCallback, width, height, background, propertyDescription.readOnly,false,propertyDescription.type);
					(valueControl as RibbonValueField).color = textColor;
					break;
			}
			valueControl.writable = propertyDescription.writeable;
			return valueControl;
		}
		
		public static function createRibbonSlider(propertyDescription:PropertyDescription, setValueCallback:Function, valueControl:AbstractRibbonControl):RibbonSlider
		{
            if(propertyDescription.writeable == false) {
                return null;
            }
			var ribbonSlider:RibbonSlider;
			switch (propertyDescription.type) {
				case Modifiers.COLOR_TYPE:
					ribbonSlider = new RibbonColorSlider(setValueCallback, 0, 26561608, RibbonColorPicker(valueControl));
					break;
				case Modifiers.UNSIGNED_NUMBER_TYPE:
				case Modifiers.NUMBER_TYPE:
					if (propertyDescription.constraints && propertyDescription.constraints is NumberValueControlConstraint
                            && (propertyDescription.constraints as NumberValueControlConstraint).controlType == NumberValueControlConstraint.SLIDER){
                        ribbonSlider = new RibbonSlider(setValueCallback, propertyDescription.constraints as NumberValueControlConstraint);
                    }
					break;
				case Modifiers.FILL_TYPE:
					ribbonSlider = new RibbonFillSlider(setValueCallback, 0, 26561608, valueControl as RibbonFillPicker);
					break;
			}
			
			return ribbonSlider;
		}
	}
}