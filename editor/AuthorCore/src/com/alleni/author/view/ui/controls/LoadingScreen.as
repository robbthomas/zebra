package com.alleni.author.view.ui.controls
{
	import com.alleni.author.definition.application.PlayerIcons;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.components.HBox;
	import com.alleni.author.view.ui.components.VBox;
	
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	public class LoadingScreen extends Sprite
	{
		public static const TYPE_SPINNER:String 			= "spinner";
		public static const TYPE_BAR:String 				= "bar";
		public static const TYPE_PANEL:String 				= "panel";
		public static const THEME_COLOR:uint 				= 0x56BFFF;
		
		private static const ASPECT_WIDTH:Number 			= 350.0;
		private static const ASPECT_HEIGHT:Number 			= 144.0;
		private static const FADE_COLOR:uint 				= 0x7F7F7F;
		private static const PROGRESS_BAR_HEIGHT:Number 	= 10.0;
		
		
		private static var _isEditor:Boolean;
		private static var _containerBox:VBox;
		private static var _poweredByLabel:LightLabel;
		private static var _logoBox:HBox;
		private static var _labelText:LightLabel;
		private static var _detailText:LightLabel;
		private static var _initialDetailText:String = "Loading application...";
		
		protected static var _progressFraction:Number = 0;
		private static var _fadeModifier:int = 0;

		private static var _progressBarHolder:Sprite;
		private static var _progressBar:Shape;
		private var _color:uint;
		private var _type:String;
		
		public function LoadingScreen(bootstrapLoadingEvent:Event=null, 
									  validInvocation:Boolean=false, 
									  type:String=TYPE_PANEL,
									  color:uint=THEME_COLOR)
		{
			super();

			//_isGeneric = ExternalInterface.call("getDomainCapabilities") == true;
			_type = type; // generic loading screen is un-branded. we may save this for upper-tier accounts.
			_color = color;
			initialize(bootstrapLoadingEvent, validInvocation);
		}
		
		public function set isEditor(value:Boolean):void
		{
			if (!_logoBox) return;
			while (_logoBox.numChildren > 0)
				_logoBox.removeChildAt(0);
			
			if (value)
				_logoBox.addChild(PlayerIcons.zebraZappsEditor);
			else
				_logoBox.addChild(PlayerIcons.zebraZappsPlayer);
		}
		
		override public function set width(value:Number):void
		{
			this.x = (value - ASPECT_WIDTH) / 2;
		}
		
		override public function set height(value:Number):void
		{
			this.y = (value - ASPECT_HEIGHT) / 3;
		}
		
		public function set overrideDetailText(value:String):void
		{
			if (_detailText)
				detailText = value;
			else
				_initialDetailText = value;
		}

		public function set progressFraction(value:Number):void
		{
			if (value > _progressFraction)
				_progressFraction = value;
		}
		
		protected function initSpinner():void {
			var progressSpinner:LoadingSpinner = new LoadingSpinner(250, true, _color);
			progressSpinner.blendMode = "overlay";
			progressSpinner.alpha = 0.7;
			progressSpinner.x = (ASPECT_WIDTH - progressSpinner.width)/2;
			progressSpinner.y = (ASPECT_HEIGHT - progressSpinner.height)/2;
			progressSpinner.filters = [new GlowFilter(0x000000, 0.8, 3, 3, 1.2, 3)];
			this.addChild(progressSpinner);
		}
		
		protected function initProgressBar():void {
			var gradientBackground:Shape = new Shape();
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [FADE_COLOR, 0x000000];
			var alphas:Array = [1.0, 1.0];
			var ratios:Array = [0x00, 0xff];
			var matrix:Matrix = new Matrix();
			
			if (!_progressBarHolder) {
				_progressBarHolder = new Sprite();
				_progressBarHolder.graphics.lineStyle(1, 0xff0000, 0, true);
				_progressBarHolder.graphics.beginFill(_color); 
				_progressBarHolder.graphics.drawRect(0, 0, ASPECT_WIDTH-3, PROGRESS_BAR_HEIGHT);
				fillType = GradientType.LINEAR;
				colors = [0xffffff, _color];
				alphas = [0.3, 0.025];
				ratios = [0x00, 0xff];
				matrix.createGradientBox(ASPECT_WIDTH, PROGRESS_BAR_HEIGHT, (Math.PI/2), 0, 0);
				_progressBarHolder.graphics.beginGradientFill(fillType, colors, alphas, ratios, matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
				_progressBarHolder.graphics.drawRect(0, 0, ASPECT_WIDTH, PROGRESS_BAR_HEIGHT/2);	
				_progressBarHolder.x = 2;
				_progressBarHolder.y = ASPECT_HEIGHT/2 - 1;
				_progressBarHolder.filters = [new DropShadowFilter(1, 80, 0, 0.3, 2, 2, 0.8, 3, true)];
				
				_progressBar = new Shape();
				_progressBarHolder.addChild(_progressBar);
				_progressBar.filters = [new DropShadowFilter(1, 0, 0, 0.8, 2, 2, 0.8, 3), new GlowFilter(0, 0.9, 3, 3, 1.2, 3, true)];
			}
			this.addChild(_progressBarHolder);
		}
		
		protected function initPanel():void {
			var gradientBackground:Shape = new Shape();
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [FADE_COLOR, 0x000000];
			var alphas:Array = [1.0, 1.0];
			var ratios:Array = [0x00, 0xff];
			var matrix:Matrix = new Matrix();
			
			graphics.beginFill(0xffffff);
			graphics.lineStyle(3, 0xBFBFBF, 1.0, true);
			graphics.drawRoundRect(0, 0, ASPECT_WIDTH, ASPECT_HEIGHT, 15.0, 15.0);
			graphics.endFill();
			matrix.createGradientBox(ASPECT_WIDTH, ASPECT_HEIGHT/2, (Math.PI/2), 0, ASPECT_HEIGHT/2);
			gradientBackground.graphics.beginGradientFill(fillType, colors, alphas, ratios, matrix);
			gradientBackground.graphics.lineStyle(3, 0x00dd00, 0, true);
			gradientBackground.graphics.drawRoundRect(2, 1, ASPECT_WIDTH-3, ASPECT_HEIGHT-2, 15.0, 15.0);
			gradientBackground.graphics.endFill();
			gradientBackground.filters = [new DropShadowFilter(12, 90, 0, 0.8, 4, 4, 0.8, 3, true)];
			
			gradientBackground.y = ASPECT_HEIGHT/2;
			gradientBackground.scrollRect = new Rectangle(0, ASPECT_HEIGHT/2, ASPECT_WIDTH, ASPECT_HEIGHT/2);
			this.addChild(gradientBackground);
			
			if (!_containerBox) {
				visible = false;
				var textShadow:DropShadowFilter = new DropShadowFilter(1,80,0,1,1,1,0.5,3);
				
				_containerBox = new VBox(this, 15, 15, 50);
				
				_logoBox = new HBox(_containerBox);
				//_logoBox.addChild(PlayerIcons.zebraZappsPlayer);  // zebra + "powered by" + zebrazapps
				
				_detailText = new LightLabel(ASPECT_WIDTH, 0xFFFFFF, false, 20);
				_detailText.size = 18;
                _detailText.width = 330;
				_detailText.text = _initialDetailText;
				_detailText.filters = [textShadow];
				_detailText.x = _containerBox.x;
				_detailText.y = ASPECT_HEIGHT * 0.75 - _detailText.height/2;
			} else {
				addChild(_containerBox);
			}
			this.addChild(_detailText);
		}
		 
		protected function initialize(bootstrapLoadingEvent:Event=null, validInvocation:Boolean=false):void
		{
			this.mouseChildren = this.mouseEnabled = false;
			switch(_type) {
				case TYPE_SPINNER:
					initSpinner();
					break;
				
				case TYPE_PANEL:
					initPanel();
					
				case TYPE_BAR:
					initProgressBar();
					break;
			}
			this.filters = [new DropShadowFilter(2, 80, 0, 0.9, 12, 12, 1.0, 3)];
			waitForStage();
		}
		
		protected function waitForStage():void
		{
			if (stage) handleAddedToStage(null);
			else this.addEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
		}
		
		private function handleResize(event:Event):void
		{
			if (!stage) return;
			this.width = stage.stageWidth;
			this.height = stage.stageHeight;
		}
		
		protected function addListeners():void
		{
			this.addEventListener(Event.ENTER_FRAME, handleEnterFrame);
			this.addEventListener(Event.REMOVED_FROM_STAGE, handleRemovedFromStage);
			if (stage)
				stage.addEventListener(Event.RESIZE, handleResize);
		}
		
		protected function removeListeners():void
		{
			this.removeEventListener(Event.ENTER_FRAME, handleEnterFrame);
			this.removeEventListener(Event.REMOVED_FROM_STAGE, handleRemovedFromStage);
			if (stage)
				stage.removeEventListener(Event.RESIZE, handleResize);
		}
		
		protected function handleAddedToStage(e:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
			handleResize(null);
			addListeners();
		}
		
		protected function handleRemovedFromStage(e:Event):void
		{
			removeListeners();
		}
		
		private function handleEnterFrame(event:Event):void
		{
			visible = true;
			if(_type!=TYPE_SPINNER) {
				_progressBar.graphics.clear();
				_progressBar.graphics.lineStyle(1, 0xff0000, 0, true);
				_progressBar.graphics.beginFill(0x000000); 
				_progressBar.graphics.drawRect(0, 0, ASPECT_WIDTH-3, PROGRESS_BAR_HEIGHT);
				
				var fillType:String = GradientType.LINEAR;
				var colors:Array = [0x000000, FADE_COLOR];
				var alphas:Array = [1, 0.5];
				var ratios:Array = [0x00, 0xff];
				var matrix:Matrix = new Matrix();
				
				var i:int;
				var fadeWidth:int = ASPECT_WIDTH-3;
				for (i = 0; i < ASPECT_WIDTH-3 ; i += fadeWidth) {
					matrix.createGradientBox(fadeWidth/8, PROGRESS_BAR_HEIGHT, 0, _fadeModifier, 0);
					_progressBar.graphics.beginGradientFill(fillType, colors, alphas, ratios, matrix, SpreadMethod.REFLECT, InterpolationMethod.RGB);
					_progressBar.graphics.drawRect(i, 0, (i+fadeWidth), PROGRESS_BAR_HEIGHT);
				}
				
				colors = [0xffffff, _color];
				alphas = [0.3, 0.025];
				matrix.createGradientBox(ASPECT_WIDTH, PROGRESS_BAR_HEIGHT, (Math.PI/2), 0, 0);
				_progressBar.graphics.beginGradientFill(fillType, colors, alphas, ratios, matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
				_progressBar.graphics.drawRect(0, 0, ASPECT_WIDTH, PROGRESS_BAR_HEIGHT/2);			
				
				_progressBar.graphics.endFill();
				_progressBar.scrollRect = new Rectangle(0, 0, (ASPECT_WIDTH-3) * _progressFraction, PROGRESS_BAR_HEIGHT + 1);
				
				if (_fadeModifier < fadeWidth)
					_fadeModifier++;
				else _fadeModifier = 0;
			}
		}

		protected function set detailText(value:String):void
		{
			if (!_detailText) return;
			_detailText.text = value;
		}
	}
}