package com.alleni.author.view.ui.controlArea
{
	import com.alleni.author.controller.ui.controlArea.LeftControlAreaMediator;
import com.alleni.author.view.ui.IconButton;
import com.alleni.author.view.ui.controls.Button;

    import assets.appui.controlArea.fxg.*

import flash.display.DisplayObject;


import flash.display.Sprite;
	
	public class LeftControlArea extends Sprite
	{
		public static const CONTENT_CONTROL_AREA_OPEN:String = "Content Control Area Open";
		public static const CONTENT_CONTROL_AREA_CLOSE:String = "Content Control Area Close";
		
		public var panelWidth:Number;
		public var panelHeight:Number;
		
		public var libraryButton:Button;
		public var variableLibraryButton:Button;
		public var dockButton:Button;
		public var toolBoxButton:Button;
		public var qaButton:Button;


		
		public function initialize():void
		{
            dockButton = new Button();
            dockButton.centerAssets = true;
            dockButton.id = "dockButton";
            dockButton.skin_unsel_down = new clSize2Cyan54EBBE();
            dockButton.skin_unsel_roll = new clSize2Blue6B91BA();
            dockButton.skin_unsel_up = new clSize1Blue6B91BA();
            dockButton.skin_sel_down = new clSize2Blue6B91BA();
            dockButton.skin_sel_roll = new clSize2Cyan54EBBE();
            dockButton.skin_sel_up = new clSize1Cyan54EBBE();
            dockButton.y = 30;
            dockButton.x = panelWidth/2;
            dockButton.buildButton(true);
            addChild(dockButton);
            dockButton.buttonMode = true;

			toolBoxButton = new Button();
            toolBoxButton.centerAssets = true;
			toolBoxButton.id = "toolBoxButton";
            toolBoxButton.skin_unsel_down = new tbSize2Cyan54EBBE();
            toolBoxButton.skin_unsel_roll = new tbSize2Blue6B91BA();
            toolBoxButton.skin_unsel_up =   new tbSize1Blue6B91BA();
            toolBoxButton.skin_sel_down =   new tbSize2Blue6B91BA();
            toolBoxButton.skin_sel_roll =   new tbSize2Cyan54EBBE();
            toolBoxButton.skin_sel_up =     new tbSize1Cyan54EBBE();
			toolBoxButton.buildButton(true, true);
			toolBoxButton.y = dockButton.y + 50;
            toolBoxButton.x = panelWidth/2;
			toolBoxButton.buttonMode = true;
			toolBoxButton.toolTip = "Toolbox";
			addChild(toolBoxButton);
			dockButton.buildButton(true);
			dockButton.toolTip = "Cloud";

			libraryButton = new Button();
            libraryButton.centerAssets = true;
			libraryButton.id = "libraryButton";
            libraryButton.skin_unsel_down = new alSize2Cyan54EBBE();
            libraryButton.skin_unsel_roll = new alSize2Blue6B91BA();
            libraryButton.skin_unsel_up =   new alSize1Blue6B91BA();
            libraryButton.skin_sel_down =   new alSize2Blue6B91BA();
            libraryButton.skin_sel_roll =   new alSize2Cyan54EBBE();
            libraryButton.skin_sel_up =     new alSize1Cyan54EBBE();
			libraryButton.buildButton(true);
			libraryButton.y = toolBoxButton.y + 50;
            libraryButton.x = panelWidth/2;
			libraryButton.buttonMode = true;
			libraryButton.toolTip = "Resource Library";
			addChild(libraryButton);

			variableLibraryButton = new Button();
            variableLibraryButton.centerAssets = true;
			variableLibraryButton.id = "variableLibraryButton";
            variableLibraryButton.skin_unsel_down = new viSize2Cyan54EBBE();
            variableLibraryButton.skin_unsel_roll = new viSize2Blue6B91BA();
            variableLibraryButton.skin_unsel_up =   new viSize1Blue6B91BA();
            variableLibraryButton.skin_sel_down =   new viSize2Blue6B91BA();
            variableLibraryButton.skin_sel_roll =   new viSize2Cyan54EBBE();
            variableLibraryButton.skin_sel_up =     new viSize1Cyan54EBBE();
			variableLibraryButton.buildButton(true);
			variableLibraryButton.y = libraryButton.y + 50;
			variableLibraryButton.buttonMode = true;
			variableLibraryButton.toolTip = "Variables & Functions";
            variableLibraryButton.x = panelWidth/2;
			addChild(variableLibraryButton);

//			qaButton = new Button();
//			qaButton.id = "qaButton";
//			qaButton.skin_unsel_down_source = "assets/appui/controlArea/qa-unsel-down.png";
//			qaButton.skin_unsel_roll_source = "assets/appui/controlArea/qa-unsel-roll.png";
//			qaButton.skin_unsel_up_source = "assets/appui/controlArea/qa-unsel-up.png";
//			qaButton.skin_sel_down_source = "assets/appui/controlArea/qa-sel-down.png";
//			qaButton.skin_sel_roll_source = "assets/appui/controlArea/qa-sel-roll.png";
//			qaButton.skin_sel_up_source = "assets/appui/controlArea/qa-sel-up.png"
//			qaButton.buildButton(true);
//			qaButton.y = variableLibraryButton.y + 50;
//			qaButton.buttonMode = true;
//			qaButton.toolTip = "Quality Assurance";
//			addChild(qaButton);
			
			new LeftControlAreaMediator().handleViewEvents(this);
		}
		
		public function handleMeasure():void
		{
			// nothing necessary at this time, could use this to space objects out or something based on dimensions
		}
	}
}
