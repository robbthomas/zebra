/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 1/16/12
 * Time: 4:39 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.ui.components {
import flash.display.DisplayObjectContainer;

public class ZappingPanelComboBox extends ComboBox {
    public function ZappingPanelComboBox(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, defaultLabel:String="", items:Array=null, labelPrefix:String="")
    {
        super(parent, xpos, ypos, defaultLabel, items, labelPrefix, DockComboBoxLabelButton, DockComboBoxDropDownButton);
    }

    override public function draw():void
    {
        super.draw();
        _dropDownButton.x = -_dropDownButton.width;
        this.graphics.beginFill(0xFFFFFF);
        this.graphics.lineStyle(1, 0xCCCCCC);
        this.graphics.drawRect(-3, 2, this.width, 16);
    }
}
}
