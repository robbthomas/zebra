package com.alleni.author.view.ui.palettes
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	public class DockDetailsView extends PopoverView
	{
		private var _targetProperty:String;
		
		public function DockDetailsView(targetView:DisplayObject, targetProperty:String, allowedDirections:int=-1, minimumTargetWidth:Number=0)
		{
            super(targetView, allowedDirections, minimumTargetWidth);
			_targetProperty = targetProperty;
		}
		
		override protected function initialize(event:Event=null):void
		{
			super.initialize(event);
			addPanel(new DockDetailsFormView(_targetProperty, this));
		}
	}
}