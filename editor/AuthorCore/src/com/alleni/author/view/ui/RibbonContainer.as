package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;

import com.alleni.author.controller.AuthorController;

import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.WireController;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.lang.TaconiteTimer;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.CapsStyle;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import mx.collections.IList;

	public class RibbonContainer extends Sprite
	{
		public static const PAD:Number = 3;

		private var _ribbons:IList;
		private var _tweenCallback:Function;
		private var _viewContext:ViewContext;
		private var _role:uint;
		private var _messageCenter:MessageCenterView;
		
		private var _type:String;
		private var _subcontainers:Vector.<RibbonSubContainer>;
		private var _cachedRibbonViews:Dictionary = new Dictionary;
		private var _h:Number = 500;
		private var _perceivedHeight:Number;
		private var _topBorder:Sprite = new Sprite();
		private var _open:Boolean = false;
		private var _inspector:Boolean;
        private var _tweenTime:Number;
		private static var ribbonQueue:Array = [];

		private static function pushRibbonQueue(container:RibbonContainer, anchor:WireAnchor, display:Boolean):void {
			ribbonQueue.push({container:container,  anchor:anchor,  display:display });
			TaconiteTimer.instance.registerListener(construct);
		}

        public function clearRibbonQueue():void{
            var container:RibbonContainer = this;
            ribbonQueue = ribbonQueue.filter(function(item:*, index:int, array:Array):Boolean{
               return item.container != container;
            });
        }

		private static function construct(e:Event):void {
			var len:Number = ribbonQueue.length;
			if(len == 0) {
				TaconiteTimer.instance.unregisterListener(construct);
			} else {
				var num:int = Math.min(10, len);
				for(var i:int = 0; i<num; i++) {
					var entry:Object = ribbonQueue[i];
					RibbonContainer(entry.container).addRibbon(entry.anchor as WireAnchor, entry.display, null);
				}
				ribbonQueue = ribbonQueue.slice(num);
				//ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, ["Loading Ribbons", (len-num) + " ribbons left"]));
			}
		}

        public static function initializeQueue():void {
            ribbonQueue = [];
        }

		public function RibbonContainer(ribbons:IList, type:String, tweenCallback:Function, viewContext:ViewContext, role:uint, messageCenterView:MessageCenterView, inspector:Boolean=false, ribbonViewsCache:Dictionary = null, openInitially:Boolean = false, useQueue:Boolean = false, tweenTime:Number = NaN)
		{
			super();

            if (!isNaN(tweenTime)) {
                _tweenTime = tweenTime;
            }
			_inspector = inspector;
			
			_ribbons = ribbons;
			if (ribbonViewsCache != null)
				_cachedRibbonViews = ribbonViewsCache;
			_type = type;
			_tweenCallback = tweenCallback; // MCView, updateContainerPositions()
			_viewContext = viewContext;
			_role = role;
			_messageCenter = messageCenterView;

			_subcontainers = new Vector.<RibbonSubContainer>();

			for each(var ribbon:WireAnchor in ribbons) {
                if (useQueue) {
                    pushRibbonQueue(this,ribbon,openInitially)
                } else {
		    		addRibbon(ribbon, openInitially, null);
                }
			}
			var showTopBorder:Boolean = false;
			for each (var subcontainer:RibbonSubContainer in _subcontainers) {
				subcontainer.adjustRibbonPositions();  // set each ribbonView.y based on order in view list
				if (subcontainer.alwaysShowingRibbons.length>0) {
					showTopBorder = true;
				}
			}

			updateContainerPositions();
			
			_topBorder.graphics.lineStyle(PAD, getColorForType(_type),1,false,"normal",CapsStyle.NONE);
			_topBorder.graphics.moveTo(MessageCenterView.BORDER_WIDTH, 0)
			_topBorder.graphics.lineTo(MessageCenterView.WIDTH-MessageCenterView.BORDER_WIDTH, 0);
			_topBorder.visible = showTopBorder;
			addChild(_topBorder);
		}

        public function get tweenTime():Number {
            return _tweenTime;
        }

        public function set tweenTime(t:Number):void {
            _tweenTime = t;
        }

        public function get anchors():Vector.<WireAnchor> {
            var v:Vector.<WireAnchor> = new Vector.<WireAnchor>();
            for each (var sc:RibbonSubContainer in _subcontainers) {
                for each(var w:WireAnchor in sc.ribbons) {
                    v.push(w);
                }
            }
            return v;
        }

		public function updateRibbonPositions():void
		{
			for each (var subcontainer:RibbonSubContainer in _subcontainers) {
				subcontainer.adjustRibbonPositions();  // set each ribbonView.y based on order in view list
			}
		}
		
		private function getColorForType(type:String):uint
		{
			switch (type) {
				case MessageCenterView.PROPERTIES:
					return 0x0084FF;
				case MessageCenterView.INLETS:
					return 0xFF3333;
				case MessageCenterView.OUTLETS:
					return 0xEB7200;
				default:
					return 0;
			}
		}

		private function updateContainerPositions():void
		{
			var n:int;
			var k:int = _subcontainers.length;
			var ht:Number = 0;
			if (k > 0)
				ht = _subcontainers[0].perceivedHeight;
			for (n=1; n < k; n++) {
				var previousContainer:RibbonSubContainer = _subcontainers[n-1];
				var container:RibbonSubContainer = _subcontainers[n];
				container.y = previousContainer.y + previousContainer.perceivedHeight;
				ht += container.perceivedHeight;
				container.adjustRibbonPositions();
			}
			
			_perceivedHeight = ht + (this.ribbonsShowing ? PAD : 0);
			_tweenCallback.apply();
		}
		
		public function get open():Boolean
		{
			return _open;
		}
		
		public function get ribbonsShowing():Boolean
		{
			for each (var subcontainer:RibbonSubContainer in _subcontainers) {
				if (subcontainer.ribbonViews.length > 0)
					return true;
			}
			return false;
		}

		public function get perceivedHeight():Number
		{
			return _perceivedHeight;
		}

		public function addRibbon(ribbon:WireAnchor, display:Boolean, childAnchor:WireAnchor):void
		{
			var sc:RibbonSubContainer;
			for each (sc in _subcontainers) {
				if (sc.label == ribbon.modifierDescription.category) {
					sc.addRibbon(ribbon, display, childAnchor);
                    _topBorder.visible = this.ribbonsShowing;
                    _topBorder.alpha = (this.ribbonsShowing? 1 : 0);
					return;
				}
			}
			
			sc = new RibbonSubContainer(updateContainerPositions, _viewContext, _role, ribbon.modifierDescription.category, getColorForType(_type), _type, _messageCenter, _inspector,_cachedRibbonViews,display,tweenTime);
			sc.addRibbon(ribbon, display, childAnchor);
			
			if(_subcontainers == null) {
				_subcontainers = Vector.<RibbonSubContainer>([sc]);
				addChild(sc);
                _topBorder.visible = this.ribbonsShowing;
                _topBorder.alpha = (this.ribbonsShowing? 1 : 0);
				return;
			}
			
			var n:int;
			var k:int = _subcontainers.length;
            var added:Boolean = false;
			for(n=0; n < k; n++) {
				if(sc.compareTo(_subcontainers[n]) < 0) {
					_subcontainers.splice(n, 0, sc);
					added = true;
                    break;
				}
			}

            if(!added) {
			    _subcontainers.push(sc);
            }

			addChild(sc);
			if(display) {
				openCategory(ribbon.modifierDescription.category,false)
			}
			
			_topBorder.visible = this.ribbonsShowing;
			_topBorder.alpha = (this.ribbonsShowing? 1 : 0);
		}

		public function removeRibbon(ribbon:WireAnchor, childRibbon:WireAnchor):void
		{
			for each (var sc:RibbonSubContainer in _subcontainers) {
				if (sc.label == ribbon.modifierDescription.category && !ribbon.alwaysShow) {
					sc.removeRibbon(ribbon, childRibbon);
                    _topBorder.visible = this.ribbonsShowing;
                    _topBorder.alpha = (this.ribbonsShowing? 1 : 0);
					return;
				}
			}
		}

		public function updateAnchors():void
		{	
			for each (var subcontainer:RibbonSubContainer in _subcontainers) {
				subcontainer.updateAnchors();
			}
		}
		
		public function openAll(closeOthers:Boolean=true):void
		{
//			_messageCenter.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.CLOSE_UNFOCUSED_RIBBON_CONTAINERS,this));

			_open = true;
			_topBorder.visible = true;
			Tweener.addTween(_topBorder, {alpha:1, time:0.5, transition:"easeOutExpo"});
			for each (var subcontainer:RibbonSubContainer in _subcontainers) {
				subcontainer.openAll(closeOthers);
			}
			
//			_messageCenter.addEventListener(NotificationNamesEditor.CLOSE_UNFOCUSED_RIBBON_CONTAINERS,closeIfNotParent);
		}
		
		public function openCategory(category:String, closeOthers:Boolean = true):void
		{
			_open = true;
			_topBorder.visible = true;
			_topBorder.alpha = 1;
			for each (var subcontainer:RibbonSubContainer in _subcontainers) {
				if (subcontainer.category == category) {
					subcontainer.addAllRibbonViews();
					subcontainer.openThis(closeOthers);
				}
			}
		}

        public function arrayOfOpenCategories(containerName:String):Vector.<RibbonContainerInfo>
        {
            var result:Vector.<RibbonContainerInfo> = new <RibbonContainerInfo>[];
            for each (var subcontainer:RibbonSubContainer in _subcontainers) {
                if (subcontainer.isOpen) {
                    result.push(new RibbonContainerInfo(containerName, subcontainer.category))
                }
            }
            return result;
        }

		
//		private function closeIfNotParent(e:ApplicationEvent):void {
//			if (this.contains(e.data as DisplayObject)) {
//				return;
//			} else {
//				_messageCenter.removeEventListener(NotificationNamesEditor.CLOSE_UNFOCUSED_RIBBON_CONTAINERS,closeIfNotParent);
//				closeAll();
//				_messageCenter.unpressAllMenuButtonsExceptFor(this);
//			}
//		}

		// TODO: this should close the container without tweening
		public function closeOnLoad():void
		{
			closeAll(true);
		}
		
		public function closeAll(loading:Boolean=false):void
		{
            if (_messageCenter && !_messageCenter.nowOpening.isEmpty && Application.instance.wireDragDescription) {
                _messageCenter.closeDeferred = true;
                return;
            }
			_open = false;

            _messageCenter.resetDraggedSinceOpened();
			
			var showTopBorder:Boolean = false;
			
			for each (var subcontainer:RibbonSubContainer in _subcontainers) {
				if (loading)
					subcontainer.closeOnLoad();
				else
					subcontainer.closeAll();
				
				if (subcontainer.alwaysShowingRibbons.length>0)
					showTopBorder = true;
			}
			
			if (!showTopBorder)
				Tweener.addTween(_topBorder, {alpha:0, time:0.5, transition:"easeOutExpo", onComplete:handleCloseComplete(false)});
			else
				Tweener.addTween(_topBorder, {alpha:1, time:0.5, transition:"easeOutExpo", onComplete:handleCloseComplete(true)});
		}
		
		private function handleCloseComplete(showTopBorder:Boolean = false):void
		{
			_topBorder.visible = showTopBorder;
		}
		
		public function handleWireRemoved():void
		{
			var showTopBorder:Boolean = false;
			
			for each (var subcontainer:RibbonSubContainer in _subcontainers) {
				if (subcontainer.alwaysShowingRibbons.length > 0)
					showTopBorder = true;
			}
			_topBorder.visible = showTopBorder;
		}
		
		public function ribbonUnderPoint(stagePoint:Point):RibbonView
		{
			for each (var subcontainer:RibbonSubContainer in _subcontainers) {
				var ribbon:RibbonView = subcontainer.ribbonUnderPoint(stagePoint);
				if (ribbon)
					return ribbon;
			}
			return null;
		}
	}
}
