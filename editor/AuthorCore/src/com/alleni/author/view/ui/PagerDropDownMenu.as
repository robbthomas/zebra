package com.alleni.author.view.ui
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.IPage;
import com.alleni.author.Navigation.Pager;
import com.alleni.author.model.objects.ArenaPage;

import flash.events.MouseEvent;

public class PagerDropDownMenu extends DynamicDropDownMenu
{
    private var _pager:Pager;

    public function PagerDropDownMenu(pager:Pager, setValueCallback:Function, dataProvider:Array, isScaleable:Boolean = true)
    {
        _doBeforeOpen = refreshMenu;
        super(setValueCallback, dataProvider, isScaleable);
        _pager = pager;
        refreshMenu();
    }

    protected function refreshListener(event:MouseEvent):void
    {
        refreshMenu();
    }

    private function refreshMenu(drawNeeded:Boolean = false):void
    {
        initializeDataProvider();
        var page:IPage;
        for each(page in _pager.pages){
            var label:String;
            if (page is ArenaPage)
                label = String((page as ArenaPage).pageNumber);
            else if (page is EventPage)
                label = (page as EventPage).title;
            addData(page,label,String(page.pageNumber));  //data added to this dynamic data provider
        }

        setDataProvider();
        draw();
    }
}
}