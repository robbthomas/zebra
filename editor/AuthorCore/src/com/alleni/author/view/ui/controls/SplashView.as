/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.ui.controls
{
	import com.alleni.author.view.text.LightLabel;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
    
	/**
	 * Application splash screen to give the user feedback as we bring up the tools
	 * 
	 */
	public class SplashView extends Sprite
	{
		public static var LABEL_DEFAULT:String = "Welcome!";
		
		[Embed(source="/assets/images/splash.png",compression="true",quality="90")]
		private static var SplashTile:Class;
		private static var _splashTile:DisplayObject;
		
		private var _label:LightLabel;
		private var _body:LightLabel;
		
		private var _statusBar:Shape;
		
		public function SplashView()
		{
			super();
			initialize();
		}
		
		private function initialize():void
		{
			_splashTile = new SplashTile();
			_label = new LightLabel();
			_body = new LightLabel();

			_splashTile.filters = [new DropShadowFilter(3,90,0,1,8,8,1,3)];
			this.addChild(_splashTile);
			
			_label.x = 35;
			_label.y = 125;
			_label.width = 350;
			_label.color = 0xffffff;
			_label.size = 20;
			var ds:DropShadowFilter = new DropShadowFilter(1,45,0,0.8,2,2,2,1);
			_label.filters = [ds];
			this.addChild(_label);
			_label.text = LABEL_DEFAULT;
			
			_body.x = 35;
			_body.y = 150;
			_body.width = 350;
			_body.color = 0xdddddd;
			_body.size = 14;
			_body.filters = [ds];
			this.addChild(_body);
			
			// some canned values as this is temporary pending a real design
			var statusBarBackground:Sprite = new Sprite();
			statusBarBackground.x = 50;
			statusBarBackground.y = 200;
			statusBarBackground.graphics.lineStyle(1, 0xffffff, 0.7, true);
			statusBarBackground.graphics.beginFill(0x333333, 0.9);
			statusBarBackground.graphics.drawRoundRect(0, 0, 300, 10, 2, 2);
			statusBarBackground.graphics.endFill();
			statusBarBackground.filters = [new GlowFilter(0xffffff, 0.5, 1.0, 1.0, 2, 2), new DropShadowFilter(1, 45, 0, 0.8, 2, 2, 0.8, 3, true)];
			statusBarBackground.alpha = 0.5;
			
			_statusBar = new Shape();
			_statusBar.graphics.lineStyle(1, 0xffffff, 0.7, true);
			_statusBar.graphics.beginFill(0x666666, 0.9);
			_statusBar.graphics.drawRect(1, 1, statusBarBackground.width-2, statusBarBackground.height-2);
			_statusBar.graphics.endFill();
			_statusBar.scaleX = 0;
			
			statusBarBackground.addChild(_statusBar);
			this.addChild(statusBarBackground);
		}
		
		public function set progressFraction(value:Number):void
		{
			_statusBar.scaleX = value;
		}
		
		public function set labelText(value:String):void
		{
			_label.text = value;
			_label.width = 350;
			_label.color = 0xffffff;
			_label.size = 20;
		}
		
		public function set bodyText(value:String):void
		{
			_body.text = value;
		}
	}
}