package com.alleni.author.view.ui.editors
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.RibbonAssetMediator;
	import com.alleni.author.controller.ui.TooltipMediator;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.feedback.TransformationGhost;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.AbstractRibbonControl;
	import com.alleni.author.view.ui.controls.TransformationEditorPanel;
	import com.alleni.taconite.dev.BitmapUtils;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.events.PropertyChangeEvent;
	
	public class RibbonAssetEditor extends AbstractRibbonControl
	{
		private const OUTLINE_COLOR:uint = 0x646464;
		private const BACKGROUND_COLOR:uint = 0x000000;
		
		private var _chipSize:Object = {width:35, height:12};
		private var _valueHandler:Object;
		private var _asset:Asset;
		private var _thumb:Bitmap;
		private var _prop:PropertyDescription;
		private var _label:LightLabel;
		
		private var _object:AbstractObject;
		
		private var _token:*;
		
		public function RibbonAssetEditor(setValueCallback:Function, w:Number=35, h:Number=12, object:AbstractObject=null, prop:PropertyDescription=null)
		{
			super(setValueCallback);
			
			_object = object;
			_prop = prop;
			
			_chipSize.width = w;
			_chipSize.height = h - 1;
			
			draw();
			
			mouseEnabled = true;
			new RibbonAssetMediator().handleViewEvents(this, null);
			//putting this back in, unable to reproduce original bug DEV- 3256
			var tipper:TooltipMediator = new TooltipMediator();
			tipper.handleTooltip(this,"", new Point(0, 18));
			tipper.textCallback = tooltipCallback;
		}
		
		private function tooltipCallback(view:*):String
		{
			if (_asset == null)
				return "";
			else
				return _asset.name;
		}
		
		override public function setValue(value:Object, valueType:String):void
		{
			if (_asset) {
				_asset.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, assetPropertyListener);
				_asset = null;
			}
			if (value is Asset) {
				_asset = value as Asset;
				_asset.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, assetPropertyListener);
			}
			updateThumb();
		}
		
		public function get asset():Asset
		{
			return _asset;
		}
		
		public function set asset(value:Asset):void
		{
//			_object[_prop.key] = value;
			valueChanged(value);
		}
		
		private function assetPropertyListener(event:PropertyChangeEvent):void
		{
			if (event.property == "content")
				updateThumb();
		}
		
		private function updateThumb():void
		{
			if (_thumb) {
				removeChild(_thumb);
				_thumb = null;
			}
			
			if (_asset && _asset.content is Bitmap) {
				var bitmap:Bitmap = Bitmap(_asset.content);
				var data:BitmapData = bitmap.bitmapData;
				var output:BitmapData = new BitmapData(_chipSize.width, _chipSize.height, true);
				var matrix:Matrix = new Matrix();
				matrix.scale(output.width/data.width, output.height/data.height);
				output.draw(bitmap, matrix, null, null, null, true);
				_thumb = new Bitmap(output,PixelSnapping.AUTO,true);
				addChild(_thumb);
				removeLabel();
			} else {
				updateLabel();
			}
		}
		
		private function updateLabel():void
		{
			if (!_label) {
				_label = new LightLabel(_chipSize.width);
				addChild(_label);
			}
			
			var name:String = "";
			if (_asset) {  
				switch (_prop.type) {
					case Modifiers.IMAGE_ASSET_TYPE:	name = "";			break;  // for image, no label
					case Modifiers.AUDIO_ASSET_TYPE:	name = "Audio";		break;
					case Modifiers.VIDEO_ASSET_TYPE:	name = "Video";		break;
					case Modifiers.TEXT_ASSET_TYPE:		name = "Text";		break;
					case Modifiers.FONT_ASSET_TYPE:		name = "Font";		break;
				}
			}
			_label.text = name;
		}
		
		private function removeLabel():void
		{
			if (_label) {
				removeChild(_label);
				_label = null;
			}
		}

		
		private function get bounds():Rectangle {
			if(_object == null) {
				return new Rectangle(-20, -20, 40, 40);
			}
			return new Rectangle(_object.left, _object.top, _object.width, _object.height);
		}
		
		
		private function panelClosed(cancel:Boolean):void
		{
			if(_token != null) {
				if(cancel) {
					ApplicationController.currentActionTree.cancelGroup(_token);
				} else {
					ApplicationController.currentActionTree.closeGroup(_token);
				}
				_token = null;
			}
		}
		
		private function mouseClickListener(e:MouseEvent):void 
		{
			e.stopImmediatePropagation();
		}
		
		
		private function draw():void
		{
			// draw background
			graphics.clear();
			graphics.lineStyle(1, OUTLINE_COLOR);
			graphics.beginFill(BACKGROUND_COLOR);
			graphics.drawRect(0,0,_chipSize.width,_chipSize.height);
			graphics.endFill();
			
			this.scrollRect = new Rectangle(0, 0, _chipSize.width, _chipSize.height);
		}
		
		override public function set height(value:Number) : void
		{
			_chipSize.height = value;
			draw();
		}
		
		override public function set width(value:Number) : void
		{
			_chipSize.width = value;
			draw();
		}
		
		
	}
}