package com.alleni.author.view.ui.components
{
	import flash.display.DisplayObjectContainer;
	
	public class DockComboBox extends ComboBox
	{
		public function DockComboBox(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, defaultLabel:String="", items:Array=null, labelPrefix:String="")
		{
			super(parent, xpos, ypos, defaultLabel, items, labelPrefix, DockComboBoxLabelButton, DockComboBoxDropDownButton);
		}
		
		override public function draw():void
		{
			super.draw();
			_dropDownButton.x = -_height;
		}
	}
}