package com.alleni.author.view.ui.palettes
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.definition.application.PublishIcons;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	
	public class PopoverView extends Sprite
	{
		protected var _width:Number = 300;
		protected var _height:Number = 260;
		
		protected static const FILL_COLOR:uint = 0xFFFFFF;
		protected static const HIGHLIGHT_COLOR:uint = 0x97C7FF;
		protected static const CORNER_RADIUS:Number = 25;
		protected static const ARROW_MARGIN:Number = 20;
		
		public static const POINT_NONE:int 			= 0;
		public static const POINT_LEFT:int 			= 1 << 1;
		public static const POINT_RIGHT:int 		= 1 << 2;
		public static const POINT_UP:int 			= 1 << 3;
		public static const POINT_DOWN:int 			= 1 << 4;
		
		private var _pointDirection:uint 			= POINT_NONE;
		private var _allowedDirections:uint 		= POINT_LEFT	// allow all by default
			|POINT_RIGHT
			|POINT_UP
			|POINT_DOWN;
		
		private static const ANIMATION_DURATION:Number = 0.15;
		private static const ANIMATION_TRANSITION:String = "easeInOutQuart";
		
		private static const REPOSITION_THRESHOLD:Number = ANIMATION_DURATION * 1000;
		private var _repositionTimestamp:Number = 0;
		
		protected var _background:Sprite;
		protected var _backgroundMask:Sprite;
		protected var _targetView:DisplayObject;
		protected var _pointer:Shape;
		protected var _minimumTargetWidth:Number = 0;
		protected var _backgroundDecoration:DisplayObject;
		protected var _panelResizeHandler:Function;

		public function PopoverView(targetView:DisplayObject, allowedDirections:int=-1, minimumTargetWidth:Number=0)
		{
			super();
			_targetView = targetView;
			if (allowedDirections >= 0)
				_allowedDirections = allowedDirections;
			_minimumTargetWidth = minimumTargetWidth;
			
			this.alpha = 0;
			
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		protected function initialize(event:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			draw();
		}
		
		protected function draw():void
		{
			if (!_background) {
				_background = new Sprite();
				addChild(_background);
				
				_background.alpha = 0.9;
				_background.filters = [new GlowFilter(0x000000, 0.8, 3, 3, 2, 3, true)];
				filters = [new DropShadowFilter(2, 80, 0, 0.7, 15, 15, 0.9, 5)];
			} else _background.graphics.clear();
			
			// fill top black portion which also fills arrow when pointing up
			_background.graphics.beginFill(0x000000, 0.9);
			_background.graphics.drawRect(-ARROW_MARGIN, -ARROW_MARGIN, _width+ARROW_MARGIN*2, ARROW_MARGIN*2.5);
			_background.graphics.beginFill(FILL_COLOR);
			_background.graphics.drawRect(-ARROW_MARGIN, ARROW_MARGIN*1.5, _width+ARROW_MARGIN*2, _height+ARROW_MARGIN*2);
			
			var matrix:Matrix;
			
			// bottom gradient pointing from bottom left corner toward top right
			matrix = new Matrix();
			matrix.createGradientBox(_width+ARROW_MARGIN*2, 100, (Math.PI/1.6), 0, _height-100);
			_background.graphics.beginGradientFill(GradientType.LINEAR, [HIGHLIGHT_COLOR, HIGHLIGHT_COLOR], [0, 1], [0x00, 0xff], matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
			_background.graphics.drawRect(-ARROW_MARGIN, _height-200, _width+ARROW_MARGIN*2, _height);
			_background.graphics.endFill();
			
			// top gradient effect
			matrix = new Matrix();
			matrix.createGradientBox(_width+ARROW_MARGIN*2, ARROW_MARGIN*2.5, (Math.PI/2), 0, -ARROW_MARGIN);
			_background.graphics.beginGradientFill(GradientType.LINEAR, [0xffffff, 0x000000], [0.7,0], [0x00, 0xff], matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
			_background.graphics.drawRect(-ARROW_MARGIN, -ARROW_MARGIN, _width+ARROW_MARGIN*2, ARROW_MARGIN*2.5);
			_background.graphics.endFill();
			
			if (!_backgroundMask) {
				_backgroundMask = new Sprite();
				addChild(_backgroundMask);
				_backgroundDecoration = PublishIcons.dialogDecoration;
				_background.addChild(_backgroundDecoration);
				_background.mask = _backgroundMask;
			} else _backgroundMask.graphics.clear();
			
			_backgroundDecoration.x = -17;
			_backgroundDecoration.y = _height - _backgroundDecoration.height + 17;
			_backgroundMask.graphics.beginFill(0);
			_backgroundMask.graphics.lineStyle(1, 0, 0, true);
			_backgroundMask.graphics.drawRoundRect(0,0, _width, _height, CORNER_RADIUS, CORNER_RADIUS);
			_backgroundMask.graphics.endFill();

			if (!_pointer) {
				_pointer = new Shape();
				_backgroundMask.addChild(_pointer);
			}
			
			this.target = _targetView;
			this.visible = true;
		}
		
		protected function addPanel(child:DisplayObject):DisplayObject
		{
			// we only track resize for one child at a time, so we need to clean up any former listeners before we add one			
			var i:int = 0;
			var length:int = this.numChildren;
			
			addChild(child);
			if (child != _background && child != _backgroundMask) {
				_panelResizeHandler = handleResize(child);
				this.addEventListener(Event.ENTER_FRAME, _panelResizeHandler);
			}
			return child;
		}
		
		protected function handleResize(child:DisplayObject):Function
		{
			return function(event:Event):void
			{
				const goalHeight:Number = child.getBounds(child).height + 12;
				if (_height == goalHeight) {
					child.parent.removeEventListener(Event.ENTER_FRAME, _panelResizeHandler);
					child.addEventListener(Event.RESIZE, _panelResizeHandler);
				} else {
					_height = goalHeight;
					draw();
				}
			}
		}
		
		override public function removeChild(child:DisplayObject):DisplayObject
		{
			if (child != _background && child != _backgroundMask)
				child.removeEventListener(Event.RESIZE, _panelResizeHandler);
			return super.removeChild(child);
		}
			
		private function handleMouseDown(event:Event):void
		{
			event.stopImmediatePropagation();
		}
		
		override public function set visible(value:Boolean):void
		{
			if (value) {
				super.visible = true;
				this.mouseChildren = true;
				this.mouseEnabled = true;
				Tweener.addTween(this, {alpha:1, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION});
			} else {
				if (alpha == 0) {
					super.visible = false;
					this.mouseChildren = false;
					this.mouseEnabled = false;
				} else {
					Tweener.addTween(this, {alpha:0, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION,
						onComplete:function():void {
							visible = false;
							dispatchEvent(new Event(Event.CLOSE));
							if (parent)
								parent.removeChild(this);
						}});
				}
			}
		}
		
		public function resetDirection():void
		{
			Tweener.removeTweens(this, "x", "y");
			Tweener.removeTweens(_pointer);
			_pointDirection = POINT_NONE;
		}
		
		public function get target():DisplayObject
		{
			return _targetView;
		}
		
		protected function getTargetBounds(targetCoordinateSpace:DisplayObject=null):Rectangle
		{
			if (!targetCoordinateSpace) targetCoordinateSpace = this.parent;
			var bounds:Rectangle = _targetView.getBounds(targetCoordinateSpace);
			if (_targetView.scrollRect) {
				bounds.x += _targetView.scrollRect.x;
				bounds.y += _targetView.scrollRect.y;
				bounds.width = _targetView.scrollRect.width;
				bounds.height = _targetView.scrollRect.height;
			}
			return bounds;
		}
		
		public function set target(value:DisplayObject):void
		{
			if (!value || !stage)
				return;

            if (visible) {
                this.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			} else {
				visible = true;
                this.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			}
            _targetView = value;

			var bounds:Rectangle = getTargetBounds();
			var stageBounds:Rectangle = getTargetBounds(stage);
			stageBounds.x += bounds.width/2;
			const scaleAdjustment:Number = _background.scaleY;

			var pointDirection:uint = POINT_NONE;
			var destinationX:Number = 0;
			var destinationY:Number = 0;
			var referenceDimension:Number = 0;

			if (stageBounds.y < (_height+ARROW_MARGIN)
					&& isAllowed(POINT_UP)) { // position below
				pointDirection = POINT_UP;
				
				referenceDimension = _width * (stageBounds.x / stage.stageWidth);
				
				if (referenceDimension < ARROW_MARGIN)
					referenceDimension = ARROW_MARGIN;
				else if (referenceDimension > _width - ARROW_MARGIN)
					referenceDimension = _width - ARROW_MARGIN;
				else {} // maintain current
				
				destinationX = bounds.x + bounds.width/2			// basic positioning
					- referenceDimension;	// adjusting for fractional positioning across the stage
				destinationY = bounds.y + bounds.height + ARROW_MARGIN;
			} else if ((_height+ARROW_MARGIN) < stageBounds.y
					&& isAllowed(POINT_DOWN)
					|| !isAllowed(POINT_UP|POINT_LEFT|POINT_RIGHT) && isAllowed(POINT_DOWN)) { // position above
				pointDirection = POINT_DOWN;
				
				referenceDimension = _width * (stageBounds.x / stage.stageWidth);
				
				if (referenceDimension < ARROW_MARGIN)
					referenceDimension = ARROW_MARGIN;
				else if (referenceDimension > _width - ARROW_MARGIN)
					referenceDimension = _width - ARROW_MARGIN;
				else {} // maintain current
				
				destinationX = bounds.x + bounds.width/2	// basic positioning
					- referenceDimension;	// adjusting for fractional positioning across the stage
				destinationY = bounds.y - _height - ARROW_MARGIN;
			} else { // middle ground, position to the side
				destinationX = bounds.x;

				referenceDimension = bounds.height/2 / scaleAdjustment;
				
				if (referenceDimension > _height - ARROW_MARGIN)
					referenceDimension = _height - ARROW_MARGIN;
				else if (referenceDimension < ARROW_MARGIN)
					referenceDimension = ARROW_MARGIN;
				else {} // maintain current
				
				var bufferWidth:Number = 0;
				if (bounds.width < _minimumTargetWidth)
					bufferWidth = (_minimumTargetWidth - bounds.width)/2;
				
				var edgeX:Number;
				var xLimit:Number;
				
				if (stageBounds.x > stage.stageWidth/2
						&& isAllowed(POINT_RIGHT)
						|| !isAllowed(POINT_LEFT) && isAllowed(POINT_RIGHT)) {
					destinationX -= (_width + bufferWidth + ARROW_MARGIN); // position to the left
					
					// make sure we are in view
					edgeX = stageBounds.x - bounds.width/2 - (_width + bufferWidth + ARROW_MARGIN);
					xLimit = 0;
					if (edgeX < xLimit)
						destinationX += (xLimit - edgeX);
					
					pointDirection = POINT_RIGHT;
				} else if (isAllowed(POINT_LEFT)
						|| !isAllowed(POINT_RIGHT) && isAllowed(POINT_LEFT)) {
					
					destinationX += (bounds.width + bufferWidth + ARROW_MARGIN); // position to the right
					
					// make sure we are in view
					edgeX = stageBounds.x + bounds.width/2 + (_width + bufferWidth + ARROW_MARGIN);
					xLimit = stage.stageWidth;
					if (edgeX > xLimit)
						destinationX -= (edgeX - xLimit);
					
					pointDirection = POINT_LEFT;
				} else {// no pointer 
				}
				destinationY = bounds.y;
			}
			
			Tweener.removeTweens(_pointer);
			if (_pointDirection != pointDirection) {
				Tweener.addTween(_pointer, {alpha:0, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION});
			}
			drawPointer(pointDirection, referenceDimension);
			
			if (this.x != destinationX || this.y != destinationY) {
				if ((getTimer() - _repositionTimestamp) > REPOSITION_THRESHOLD || _pointDirection != pointDirection) {
					Tweener.removeTweens(this, "x", "y");
					Tweener.addTween(this, {x:destinationX, y:destinationY, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION});
				} else {
					if (!Tweener.isTweening(this) || this.alpha < 1) {
						this.x = destinationX;
						this.y = destinationY;
					}
				}
			}
			_repositionTimestamp = getTimer();
			_pointDirection = pointDirection;
		}
		
		private function drawPointer(pointDirection:uint, referenceDimension:Number):void
		{
			_pointer.graphics.clear();
			_pointer.graphics.beginFill(0);
			_pointer.graphics.moveTo(0, 0);

			switch (pointDirection) {
				case POINT_LEFT:
				case POINT_RIGHT:
					_pointer.x = (pointDirection == POINT_RIGHT)?_width:0;
					_pointer.y = referenceDimension - ARROW_MARGIN/2;
					_pointer.graphics.lineTo((pointDirection == POINT_RIGHT)?ARROW_MARGIN:-ARROW_MARGIN, ARROW_MARGIN/2);
					_pointer.graphics.lineTo(0, ARROW_MARGIN);
					_pointer.graphics.lineTo(0, 0);
					break;
				case POINT_UP:
				case POINT_DOWN:
					var pointerX:Number = 0;
					var pointerY:Number = 0;
					pointerX = referenceDimension - ARROW_MARGIN/2;					// slide the arrow along the top or bottom relative to the container position
					pointerY = (pointDirection == POINT_DOWN)?_height:0;
					_pointer.graphics.lineTo(ARROW_MARGIN/2, 
						(pointDirection == POINT_DOWN)?ARROW_MARGIN:-ARROW_MARGIN);	// draw the point either up or down
					_pointer.graphics.lineTo(ARROW_MARGIN, 0);
					_pointer.graphics.lineTo(0, 0);
					if (_pointDirection == pointDirection && 
							(getTimer() - _repositionTimestamp) > REPOSITION_THRESHOLD) {
						Tweener.removeTweens(_pointer, "x", "y");
						Tweener.addTween(_pointer, {x:pointerX, y:pointerY, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION});
					} else {
						_pointer.x = pointerX;
						_pointer.y = pointerY;
					}
					break;
				default:
					// none - no pointer
					break;
			}
			_pointer.graphics.endFill();
			
			if (_pointDirection != pointDirection && pointDirection != POINT_NONE)
				Tweener.addTween(_pointer, {alpha:1, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION});
		}
		
		private function isAllowed(mask:uint):Boolean
		{
			return (_allowedDirections & mask) != 0;
		}
		
		private function set pointDirection(mask:uint):void
		{
			_pointDirection = 0;
			_pointDirection |= mask;
		}
	}
}
