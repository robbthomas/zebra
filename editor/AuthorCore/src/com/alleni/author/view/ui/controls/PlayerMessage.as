package com.alleni.author.view.ui.controls
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.components.HBox;
	import com.alleni.author.view.ui.components.VBox;
	
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.utils.Timer;
	
	public class PlayerMessage extends Sprite
	{
		private static const CLOSE_DELAY:int = 1000; // 2 seconds
		private static const ICON_DIMENSION:Number = 30.0;

		protected static const PADDING:Number = 10.0;
		protected static const WIDTH:Number 	= 350.0;
		protected static const HEIGHT:Number 	= 180.0;
		private static const FILL_COLOR:uint = 0x000000;
		private static const HIGHLIGHT_COLOR:uint = 0x97C7FF;
		private static const CORNER_RADIUS:Number = 25.0;
		
		private static const ANIMATION_DURATION:Number = 0.5;
		private static const ANIMATION_TRANSITION:String = "easeInOutQuart";
		
		private var _background:Sprite;		
		private var _backgroundMask:Sprite;
		private var _backgroundDecoration:DisplayObject;
		private var _headerLabel:LightLabel;
		private var _headerText:String;
		protected var _bodyLabel:LightLabel;
		private var _bodyText:String;
		private var _icon:DisplayObject;
		
		protected var _buttonShadow:DropShadowFilter;
		protected var _container:VBox;
		protected var _headerContainer:HBox;
		
		public function PlayerMessage()
		{
			super();
			
			this.alpha = 0;
			_icon = null;
			
			if (this.stage) initialize();
			else this.addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(e:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			_buttonShadow = new DropShadowFilter(1,80,0,0.8,2,2,1.5,3);

			draw();
		}
		
		protected function draw():void
		{
			if (!_background) {
				_background = new Sprite();
				addChild(_background);
				
				_background.alpha = 0.85;
				_background.filters = [new GlowFilter(0xffffff, 0.8, 3, 3, 2, 3, true)];
				filters = [new DropShadowFilter(2, 80, 0, 0.7, 15, 15, 0.9, 5)];
			} else _background.graphics.clear();
			
			_background.graphics.beginFill(FILL_COLOR);
			_background.graphics.drawRect(0, 0, WIDTH, HEIGHT);
			
			var matrix:Matrix;
			
			// top gradient effect
			matrix = new Matrix();
			matrix.createGradientBox(WIDTH, 30, (Math.PI/2));
			_background.graphics.beginGradientFill(GradientType.LINEAR, [0xffffff, 0x000000], [0.7,0], [0x00, 0xff], matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
			_background.graphics.drawRect(0, 0, WIDTH, 30);
			_background.graphics.endFill();
			
			if (!_backgroundMask) {
				_backgroundMask = new Sprite();
				addChild(_backgroundMask);
				/*_backgroundDecoration = PublishIcons.dialogDecoration;
				_background.addChild(_backgroundDecoration);*/
				_background.mask = _backgroundMask;
			} else _backgroundMask.graphics.clear();
			
			/*_backgroundDecoration.x = -17;
			_backgroundDecoration.y = HEIGHT - _backgroundDecoration.height + 17;*/
			_backgroundMask.graphics.beginFill(0);
			_backgroundMask.graphics.lineStyle(1, 0, 0, true);
			_backgroundMask.graphics.drawRoundRect(0,0, WIDTH, height, CORNER_RADIUS, CORNER_RADIUS);
			_backgroundMask.graphics.endFill();
			
			_container = new VBox(this, PADDING, PADDING*2);
			_container.filters = [_buttonShadow];
			_headerContainer = new HBox(_container);
			updateHeaderContainer();
			
			_bodyLabel = new LightLabel(WIDTH - PADDING*2, 0xdddddd, true);
			_bodyLabel.size = 14;
			_bodyLabel.bold = false;
			_bodyLabel.text = _bodyText;
			_container.addChild(_bodyLabel);
			
			handleMeasure();
			this.visible = true;
		}
		
		public function set headerText(value:String):void
		{
			_headerText = value;
			if (_headerLabel) _headerLabel.text = _headerText;
		}
		
		public function set bodyText(value:String):void
		{
			_bodyText = value;
			if (_bodyLabel) _bodyLabel.text = _bodyText;
		}

		public function set headerIcon(value:DisplayObject):void
		{
			_icon = value;
			updateHeaderContainer();
		}

		private function updateHeaderContainer():void
		{
			if (_headerContainer == null) return;
			while (_headerContainer.numChildren > 0) {
				_headerContainer.removeChildAt(0);
			}

			var headerLabelWidth = WIDTH - PADDING*2;
			if (_icon != null) {
				_icon.width = ICON_DIMENSION;
				_icon.height = ICON_DIMENSION;
				_headerContainer.addChild(_icon);
				headerLabelWidth = WIDTH - PADDING*3 - ICON_DIMENSION;
			}
			_headerLabel = new LightLabel(headerLabelWidth, 0xffffff, true);
			_headerLabel.size = 18;
			_headerLabel.bold = true;
			_headerLabel.text = _headerText;
			_headerContainer.addChild(_headerLabel);

			_headerContainer.draw();
			_container.draw();
		}
		
		public function handleMeasure():void
		{
			if (!stage) return;
			this.x = (stage.stageWidth - WIDTH) / 2;
			this.y = (stage.stageHeight - HEIGHT) / 3;
		}
		
		public function dismissAfterDelay(delay:Number=-1):void
		{
			if (delay < 0) delay = CLOSE_DELAY;
			var timer:Timer = new Timer(delay, 1);
			timer.addEventListener(TimerEvent.TIMER, handleTimerComplete);
			timer.start();
		}
		
		private function handleTimerComplete(e:Event):void
		{
			e.target.removeEventListener(e.type, handleTimerComplete);
			this.visible = false;	
		}
		
		override public function set visible(value:Boolean):void
		{
			if (super.visible && value && this.alpha == 1) return;
			if (value) {
				super.visible = true;
				this.mouseChildren = true;
				this.mouseEnabled = true;
				Tweener.addTween(this, {alpha:1, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION});
			} else {
				if (alpha == 0) {
					super.visible = false;
					this.mouseChildren = false;
					this.mouseEnabled = false;
					if (this.parent)
						this.parent.removeChild(this);
				} else {
					Tweener.addTween(this, {alpha:0, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION,
						onComplete:function():void {
							visible = false;
						}});
				}
			}
		}
	}
}