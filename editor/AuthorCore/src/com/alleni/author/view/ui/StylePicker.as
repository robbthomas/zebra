package com.alleni.author.view.ui
{
	import com.alleni.author.controller.app.CursorController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.CursorNames;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.GraphicFillView;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.controls.GraphicFillPanel;
	import com.alleni.author.view.ui.controls.StylePanel;
	import com.alleni.taconite.model.Notification;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.core.UIComponent;
	import mx.events.PropertyChangeEvent;
	
	public class StylePicker extends AbstractRibbonControl
	{
		private var _appContainer:UIComponent;
		
		
		private var _outlineColor:uint = 0x646400;
		
		private var _valueCallback:Function;  // function called when fill changes, to give new value to client code
		private var _openPanelCallback:Function;  // when panel opens, we call this function to allow ribbon to hide its tooltip
		private var _hostObject:AbstractObject;
		
		private var _w:Number;
		private var _h:Number;
		private var _label:LightLabel;
		
		
		public function StylePicker(appContainer:UIComponent, initialValue:String, valueCallback:Function, openPanelCallback:Function, obj:AbstractObject,  w:Number=35, h:Number=12)
		{
			super(valueCallback);
			
			_appContainer = appContainer;
			_valueCallback = valueCallback;
			_openPanelCallback = openPanelCallback;
			_hostObject = obj;
			_w = w;
			_h = h;
			
			//draw();
			drawLabel();
			setLabel(initialValue);
			
			this.addEventListener(MouseEvent.CLICK, mouseClickListener);
			this.addEventListener(MouseEvent.MOUSE_OVER, mouseOverListener);
			this.addEventListener(MouseEvent.MOUSE_OUT, mouseOutListener);
		}
		
		
		override public function setValue(value:Object, valueType:String):void {
			
		}
		
		public function draw():void
		{	
			graphics.clear();
			graphics.beginFill(0x000000,1);
			graphics.lineStyle(1, _outlineColor);
			graphics.drawRect(0,0,_w,_h);
			graphics.endFill();
		}
		
		public function drawLabel():void{
			
			if(_label == null){
				_label = new LightLabel(_w,0xFFFFFF,false,_h);
				addChild(_label);
			}
			
			_label.y = 2;
			_label.x = 1;
		}
		
		public function setLabel(value:String):void{
			_label.text = "Select";
		}
		
		
		// Highligh/unhighlight the color chip
		private function mouseOverListener(e:MouseEvent):void
		{
			//			_outlineColor = 0xFFFFFF;
			draw();
		}
		
		private function mouseOutListener(e:MouseEvent):void
		{			
			/*_outlineColor = 0x000000;*/
			draw();	
		}
		
		
		private function mouseClickListener(e:MouseEvent):void
		{	
			openEditing();
		}
		
		private function handleUpdateStyle(event:Event):void
		{
			
		}
		
		
		override public function openEditing():void {
			
		var local:Point = new Point(width/2, height/2);
		var global:Point = localToGlobal(local);
			
		StylePanel.showPanel(_appContainer, _valueCallback, global, this);
		
		if (_openPanelCallback != null)
			_openPanelCallback();  // let the ribbon view disable tooltips, so they don't cover our panel
			
		}
		
		override public function closeEditing(e:Event):void
		{
			valueChanged(StylePanel.getStyles());
		}
		
		
		override public function set height(value:Number) : void
		{
			
			draw();
		}
		
		override public function set width(value:Number) : void
		{
			draw();
		}
		
		
		
		private function firstSelectedObj():AbstractObject
		{
			return null;
		}
	}
}