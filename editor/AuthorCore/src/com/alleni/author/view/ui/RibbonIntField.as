package com.alleni.author.view.ui
{
import com.alleni.author.definition.Modifiers;

public class RibbonIntField extends RibbonValueField
	{
		public function RibbonIntField(setValueCallback:Function, width:Number, height:Number, background:Boolean=true, readOnly:Boolean=false, formatted:Boolean=false, propertyType:String=null)
		{
			super(setValueCallback, width, height, background, readOnly, formatted, propertyType);
		}
		
		final override public function setValue(value:Object, valueType:String):void
		{
			_valueField.text = Modifiers.convertValue(Modifiers.INT_TYPE, value).toString();
		}
	}
	
	
	
}