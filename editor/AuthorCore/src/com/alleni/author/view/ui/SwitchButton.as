package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	public class SwitchButton extends Sprite
	{
		
		private static const BG_HIGHLIGHT:uint = 0xAFAFAF;
		private static const BG_ACTIVE:uint = 0xFFFFFF;
		private static const BG_CURRENT:uint = 0x0099FF;
		private static const LETTER_HIGHLIGHT:uint = 0x000000;
		private static const LETTER_ACTIVE:uint = 0x000000;
		private static const LETTER_CURRENT:uint = 0xFFFFFF;
		
		private var letterInactive:uint = 0xFFFFFF; // needs to be a variable for boolean switches, which have a slightly different design
		private var fadeBackground:Boolean=false; // Also required for boolean switches, which fade out the button background
		
		private var _drawFunction:Function;
		
		private var _displayedAsInitial:Boolean; // Button is displayed as initial value (white background, black text);
		private var _displayedAsCurrent:Boolean; // Button is displayed as current value (blue background, white text);
		private var _displayedAsDefault:Boolean; // Button is displayed as default value (gray letter, doesn't fade out);
		
		private var _value:String; // The value that this button represents
		private var _resetButton:Boolean; // This is the reset button for the switch 
		
		private var _mouseOver:Boolean; // True when the mouse is over the button
		
		private var _background:Sprite;
		private var _letter:Sprite;
		
		private var letterColor:uint;
		private var letterOverColor:uint;
		private var bgColor:uint;
		private var bgAlpha:Number;
		private var bgOverAlpha:Number;
		
		private var _setValueCallback:Function;
		
		public function SwitchButton(setValueCallback:Function, drawFunction:Function, value:String, args:Object=null, defaultColor:uint = 0xFFFFFF)
		{
			super();
			
			_setValueCallback = setValueCallback;
			
			_drawFunction = drawFunction;
			_value = value;
			
			// accept optional arguments
			if (args) {
				if (args.hasOwnProperty("resetButton"))		_resetButton = args.resetButton;
				if (args.hasOwnProperty("fadeBackground"))	fadeBackground = args.fadeBackground;
				if (args.hasOwnProperty("letterInactive"))	letterInactive = args.letterInactive;
			}
			
			_background = new Sprite();
			_letter = new Sprite();
			
			addChild(_background);
			addChild(_letter);
			
		// Default colors:  we begin in the non-mouseOver state
			letterInactive = defaultColor;
			letterColor = letterInactive;
			letterOverColor = LETTER_HIGHLIGHT;
			bgColor = BG_HIGHLIGHT;
			bgAlpha = (fadeBackground?0:1);
			bgOverAlpha = 1; 	
			
		// Event listeners
			// I'm not sure why putting listeners on "this" doesn't work.
			_background.addEventListener(MouseEvent.MOUSE_OVER, mouseOverListener);
			_background.addEventListener(MouseEvent.MOUSE_OUT, mouseOutListener);
//			_background.addEventListener(MouseEvent.CLICK, mouseClickListener);
			_letter.addEventListener(MouseEvent.MOUSE_OVER, mouseOverListener);
			_letter.addEventListener(MouseEvent.MOUSE_OUT, mouseOutListener);
//			_letter.addEventListener(MouseEvent.CLICK, mouseClickListener);
			
			if (!_resetButton) // _restButton's click listener is registered in AbstractSwitch
				this.addEventListener(MouseEvent.CLICK, mouseClickListener);	
			
			drawButton();
		}
		
		private function mouseOverListener(e:MouseEvent):void {

			_mouseOver = true;
			drawButton();
		}
		
		private function mouseOutListener(e:MouseEvent):void {
			
			_mouseOver = false;
			drawButton();
		}
		
		// This will need to be replaced by a mediator, since it will change the model
		private function mouseClickListener(e:MouseEvent):void {

			drawButton();
			
		// Functionality to be stored in mediator
			// model.value = this.value
			// The update should fire AbstractSwitch's setValue, specifying an INITIAL value update
			var newVal:Object = this.value;
			if (newVal == "reset")
				newVal = "";  // tbd:  should be using default value when Reset is clicked
			
			_setValueCallback(newVal);
		}
		
		public function drawButton():void {
			const dimension:Number = 11.0;

			_background.graphics.clear();
			
			_background.graphics.beginFill(0xff0000, 0.0);
			_background.graphics.drawRect(-1, -1, dimension+2, dimension+2);

			_background.graphics.lineStyle(0,0,0);
			_background.graphics.beginFill(bgColor, (_mouseOver?bgOverAlpha:bgAlpha));

			var corner:Number = (_resetButton?11:5); // Corner radius for reset buttons is 11 (to make circle outline)
			_background.graphics.drawRoundRect(0,0,dimension,dimension,corner,corner);
			
			_letter.graphics.clear();
			_drawFunction(_letter, (_mouseOver?letterOverColor:letterColor));
		}
		
		public function get value():String {
			return _value;
		}
		
		public function set displayedAsInitial(value:Boolean):void {
			
			_displayedAsInitial = value;
			
			if (value)
				applyInitialValueColors();
			else
				applyDefaultValueColors();
		}
		
		public function set displayedAsCurrent(value:Boolean):void {
		
			_displayedAsCurrent = value;
			
			if (value)
				applyCurrentValueColors();
			else
				applyDefaultValueColors();	
		}
		
		public function applyDefaultValueColors():void {
			
			letterColor = letterInactive;
			letterOverColor = LETTER_HIGHLIGHT;
			bgColor = BG_HIGHLIGHT;
			bgAlpha = (fadeBackground?0:1);
			bgOverAlpha = 1;
			drawButton();
		}
		
		private function applyCurrentValueColors():void {
			
			letterColor = LETTER_CURRENT;
			letterOverColor = LETTER_HIGHLIGHT;
			bgColor = BG_CURRENT;
			bgAlpha = 1;
			bgOverAlpha = 1;
			drawButton();		
		}
		
		private function applyInitialValueColors():void {
			
			letterColor = LETTER_ACTIVE;
			letterOverColor = LETTER_HIGHLIGHT;
			bgColor = BG_ACTIVE;
			bgAlpha = 1;
			bgOverAlpha = 1;
			drawButton();		
		}
		
		public function fade(fadeIn:Boolean):void {
			if (fadeIn)
				Tweener.addTween(this, {alpha:1, transition:"easeOutQuad", time:0.25});
			else
				Tweener.addTween(this, {alpha:0, transition:"easeInQuad", time:0.25});
		}
	}
}