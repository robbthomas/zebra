package com.alleni.author.view.ui.controls
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.view.GraphicFillView;
	import com.alleni.author.view.objects.tables.TableButtons;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.IconButton;
	import com.alleni.author.view.ui.MenuButton;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.display.CapsStyle;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	
	import mx.core.UIComponent;
	
	
	public class GraphicFillPanel extends UIComponent implements IColorPickerParent
	{
		public static var instance:GraphicFillPanel;
		private var _appContainer:UIComponent;
						
		private var _jellybeanAllowed:Boolean;
		private var _gradientAllowed:Boolean;
		
		private var _fillModel:GraphicFill = new GraphicFill();
		
		private var _editColorFieldName:String;
		private var _editColorChip:Sprite;
		private var _editColorIndex:int;
		private var _colorChipWidth:Number;
		
		private var _colorPicker:ColorPicker;
		
		private var _fillView:GraphicFillView;
		private var _gradientArrow:Sprite;
		private var _arrow:Sprite = new Sprite();  // arrow on top edge that points at the ribbon being edited
		private var _menu:PopOutMenu;

		

		private var _callback:Function;
		
		private var _gradientControls:Sprite;
		private var _linearButton:IconButton;
		private var _radialButton:IconButton;
		private var _plusButton:IconButton;
		private var _minusButton:IconButton;
		private var _swapButton:IconButton;

		private var _gradientChips:Array = [];
		private var _gradientAngleText:RichTextField;
		
		private var _jellyColorChip1:Sprite;
		private var _jellyColorChip2:Sprite;
		
		private var _linearToggleGraphic:Sprite;
		private var _radialToggleGraphic:Sprite;
		
		// dimensions of whole panel
		private static const WIDTH:Number = 231;
		private static const HEIGHT:Number = 91;
		private static const HEIGHT_GRADIENT:Number = 120;
		private static const CORNER_RADIUS:Number = 8;
		private static const BACKGROUND_COLOR:uint = 0x000000;
		private static const BACKGROUND_EDGE_COLOR:Number = 0x828282;
		private static const ARROW_X:Number = 50;  // arrow on top edge that points at the ribbon being edited
		
		private static const DIVIDER_LINE_COLOR:Number = 0x585858;
		private static const DIVIDER_LINE_Y:Number = 60;
		private static const DIVIDER_LINE2_Y:Number = 89;
		
		// locations of controls
		private static const PLUS_X:Number = 200;
		private static const PLUS_Y:Number = 26;
		private static const MINUS_X:Number = 216;
		private static const MINUS_Y:Number = 26;
		private static const SWAP_X:Number = 14;
		private static const SWAP_Y:Number = 18;
		private static const LINEAR_X:Number = 17;
		private static const LINEAR_Y:Number = 96;
		private static const RADIAL_X:Number = 42;
		private static const RADIAL_Y:Number = 96;
		private static const TOGGLE_ICON_WIDTH:Number = 15;  // the Linear and Radial icon buttons are this size
		private static const TOGGLE_ICON_HEIGHT:Number = 12;
		private static const BUTTON_ICON_COLOR:uint = 0x969696;
		private static const X_MENU:Number = 2;
		private static const Y_MENU:Number = 67;
		private static const MENU_WIDTH:Number = 60;
		private static const MENU_HEIGHT:Number = 16;
		private static const FILL_FRAME_X:Number = 40;
		private static const FILL_FRAME_Y:Number = 9;
		private static const FILL_FRAME_WIDTH:Number = 149;
		private static const FILL_FRAME_HEIGHT:Number = 34;
		private static const FILL_FRAME_COLOR:uint = 0xc8c8c8;
		private static const X_FILL:Number = FILL_FRAME_X+2;
		private static const Y_FILL:Number = FILL_FRAME_Y+2;
		private static const FILL_WIDTH:Number = 145;
		private static const FILL_HEIGHT:Number = 30;
		private static const OK_X:Number = 178;
		private static const OK_Y:Number = 64;
		private static const OK_WIDTH:Number = 44;
		private static const OK_HEIGHT:Number = 20;
		private static const CANCEL_X:Number = 126;
		private static const CANCEL_Y:Number = 64;
		private static const CANCEL_WIDTH:Number = 44;
		private static const CANCEL_HEIGHT:Number = 20;
		private static const COLOR_CHIP_HEIGHT:Number = 10;
		private static const COLOR_CHIP_WIDTH:Number = 25;  // for fixed-width chips (not in gradient mode)
		private static const GRADIENT_CHIPS_X:Number = X_FILL;
		private static const GRADIENT_CHIPS_Y:Number = FILL_FRAME_Y + FILL_FRAME_HEIGHT;
		private static const GRADIENT_LABEL_X:Number = 22;
		private static const GRADIENT_LABEL_Y:Number = 60;
		private static const GRADIENT_ANGLE_X:Number = 50;
		private static const GRADIENT_ANGLE_Y:Number = 60;
		private static const GRADIENT_ANGLE_WIDTH:Number = 35;
		private static const GRADIENT_ANGLE_HEIGHT:Number = 22;
		private static const JELLY_COLOR1_X:Number = X_FILL+26;
		private static const JELLY_COLOR1_Y:Number = GRADIENT_CHIPS_Y;
		private static const JELLY_COLOR2_X:Number = X_FILL+98;
		private static const JELLY_COLOR2_Y:Number = GRADIENT_CHIPS_Y;

		private var _token:*;

		
		
		public function GraphicFillPanel(appContainer:UIComponent)
		{
			Utilities.assert(instance == null);
			instance = this;
			super();
			_appContainer = appContainer;
			// TODO: find a way to position in the ApplicationController
			EditorUI.instance.addPopOver(this);
									
			_fillView = new GraphicFillView();
			_fillView.x = X_FILL;
			_fillView.y = Y_FILL;
			addChild(_fillView);
			
			_gradientArrow = new Sprite();
			drawGradientArrow(_gradientArrow.graphics, 0x000000, 4);  // black, thick=4
			drawGradientArrow(_gradientArrow.graphics, 0xffffff, 2);  // white, thick=2
			_fillView.addChild(_gradientArrow);
			_gradientArrow.x = FILL_WIDTH/2;
			_gradientArrow.y = FILL_HEIGHT/2;
			
			createButton(OK_X,OK_Y, drawOkButton, okAction, "OK");
			createButton(CANCEL_X,CANCEL_Y, drawCancelButton, cancelAction, "Cancel");
			
			drawArrow();
			addChild(_arrow);
			
			// dropshadow on whole table
			var dropShadow:DropShadowFilter = new DropShadowFilter();
			dropShadow.blurX = 15;
			dropShadow.blurY = 15;
			dropShadow.angle = 45;
			dropShadow.alpha = 0.90;
			dropShadow.distance = 6;
			this.filters = [dropShadow];
		}
		
		public static function showPanel(appContainer:UIComponent, initialValue:GraphicFill, valueCallback:Function, global:Point, obj:AbstractObject = null):void
		{
			if (!instance)
				new GraphicFillPanel(appContainer);
			else
				instance.visible = true;

			instance._token = ApplicationController.currentActionTree.openGroup("Pick Fill");
			
			instance._callback = valueCallback;
			
			//this needs to be revised -- just a quick fix
			instance._gradientAllowed = obj != null;
			instance._jellybeanAllowed = obj != null;
		
			// place the tip of the arrow at "global" which is the center of the color chip in the ribbon
			instance.x = global.x - ARROW_X - 10;
			instance.y = global.y + 10;
			
			instance.finishShowPanel(initialValue);
		}
		
		private function finishShowPanel(initialValue:GraphicFill):void
		{
			// force panel to be within screen bounds
			_arrow.visible = true;
			if (x > _appContainer.width - WIDTH) {
				x = _appContainer.width - WIDTH;
				_arrow.visible = false;  // suppress the arrow if we are forcing the panel location
			}
			if (y > _appContainer.height - HEIGHT - ColorPicker.HEIGHT - ColorPicker.ARROW_HEIGHT) {  // leave room below for the color picker
				y = _appContainer.height - HEIGHT - ColorPicker.HEIGHT - ColorPicker.ARROW_HEIGHT;
				_arrow.visible = false;  // suppress the arrow if we are forcing the panel location
			}
			
			_fillModel = initialValue.clone();
			setupPanelMode();
			
			_menu = createMenu(X_MENU,Y_MENU, MENU_WIDTH, MENU_HEIGHT, _fillModel.type);
			addChild(_menu);
			
			ApplicationController.instance.captureMouseClicks(true);
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
		}
		
		public static function closePanel():void
		{
			if (instance)
				instance.closeThisPanel();
		}
		
		
		private function closeThisPanel():void
		{
			closeEditColor();
			this.visible = false;
			_callback = null;
			_menu = null;
			ApplicationController.instance.captureMouseClicks(false);
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
			if(_token != null) {
				ApplicationController.currentActionTree.closeGroup(_token);
				_token = null;
			}
		}
		

		private function updatePanelView():void
		{
			// call this function after changing the model
			trace("updatePanelView   fillSpec="+_fillModel.toString());
			_fillView.update(_fillModel, FILL_WIDTH, FILL_HEIGHT);
			_gradientArrow.visible = (_fillModel.type == GraphicFill.GRADIENT_FILL && _fillModel.linear);
			_gradientArrow.rotation = _fillModel.angle;

			_callback(_fillModel.clone());
			
			if (_linearToggleGraphic) {
				_linearToggleGraphic.visible = (_fillModel.linear);
				_radialToggleGraphic.visible = (!_fillModel.linear);
			}
			
			// keep the popup menu in front of all graphics, except the color picker
			var colorPickerActive:Boolean = (_colorPicker!=null && _colorPicker.visible);
			if (_menu && !colorPickerActive)
				setChildIndex(_menu, numChildren-1);
		}

		
		private function drawBackground(graphics:Graphics, fillType:int):void
		{
			// background of whole panel
			graphics.clear();
			var hhh:Number = (fillType == GraphicFill.GRADIENT_FILL) ? HEIGHT_GRADIENT : HEIGHT;  // taller if gradient type
			graphics.lineStyle(1, BACKGROUND_EDGE_COLOR);
			graphics.beginFill(BACKGROUND_COLOR);  
			graphics.moveTo(0, 0);
			graphics.lineTo(WIDTH, 0);
			graphics.lineTo(WIDTH, hhh-CORNER_RADIUS);
			graphics.curveTo(WIDTH, hhh, WIDTH-CORNER_RADIUS, hhh);
			graphics.lineTo(CORNER_RADIUS, hhh);
			graphics.curveTo(0, hhh, 0, hhh-CORNER_RADIUS);
			graphics.lineTo(0, 0);
			graphics.endFill();
			
			// horz divider lines
			graphics.moveTo(-1,DIVIDER_LINE_Y);
			graphics.lineTo(WIDTH-1,DIVIDER_LINE_Y);
			
			if (fillType == GraphicFill.GRADIENT_FILL) {
				graphics.moveTo(-1,DIVIDER_LINE2_Y);
				graphics.lineTo(WIDTH-1,DIVIDER_LINE2_Y);
			}
			
			// fill view
			graphics.lineStyle();  // no outline on frame
			graphics.beginFill(FILL_FRAME_COLOR);
			graphics.drawRect(FILL_FRAME_X, FILL_FRAME_Y, FILL_FRAME_WIDTH, FILL_FRAME_HEIGHT);
			graphics.endFill();
		}
		
		private function drawArrow():void
		{
			// arrow on top edge that points at the ribbon being edited
			_arrow.graphics.beginFill(0x000000,.9);
			_arrow.graphics.moveTo(0,11);
			_arrow.graphics.lineTo(10,0);
			_arrow.graphics.lineTo(19,11);
			_arrow.graphics.endFill();
			_arrow.graphics.lineStyle(1,0x828282);
			_arrow.graphics.moveTo(0,11);
			_arrow.graphics.lineTo(10,0);
			_arrow.graphics.lineTo(19,11);
			_arrow.x = ARROW_X;
			_arrow.y = -10;
		}

		
		private function createButton(xx:Number, yy:Number, drawButton:Function, action:Function, label:String):MenuButton
		{
			var btn:MenuButton = new MenuButton(drawButton,action);
			btn.toggle = false;
			btn.color = 0x00303030;
			btn.x = xx;
			btn.y = yy;
			btn.labelText = label;
			btn.labelFontSize = 11;
			btn.labelColor = 0xffffff;
			addChild(btn);
			return btn;
		}
		
		private function drawOkButton( btn:Sprite ):void
		{	
			const R:Number = 5; //rounding
			
			// fill the button with gray, round the left side corners
			btn.graphics.lineStyle(0,0,0, true);  // zero alpha, to eliminate line
			btn.graphics.drawRoundRect(0,0,OK_WIDTH,OK_HEIGHT,R);
		}
		
		private function drawCancelButton( btn:Sprite ):void
		{	
			const R:Number = 5; //rounding
			
			// fill the button with gray, round the left side corners
			btn.graphics.lineStyle(0,0,0, true);  // zero alpha, to eliminate line
			btn.graphics.drawRoundRect(0,0,CANCEL_WIDTH,CANCEL_HEIGHT,R);
		}
		
		private function okAction():void
		{
			closeThisPanel();
		}
		
		private function cancelAction():void
		{
			if(_token) {
				// undo the fill change, and the automatic change to fillAlpha
				ApplicationController.currentActionTree.closeGroup(_token);
				ApplicationController.currentActionTree.undo();
				_token = null;
			}
			closeThisPanel();
		}

		
		public static function get isActive():Boolean
		{
			if (instance)
				return instance.visible;  // true if the fill panel is active (so MC tooltips should not show)
			else
				return false;
		}
		
		private function changeFillType(fillType:int):void
		{
			var oldType:int = _fillModel.type;
			_fillModel.type = fillType;
			
			// when switching from a solid color, make a nice gradient
			if (oldType == GraphicFill.SOLID_FILL)
				_fillModel.makeNiceGradient();
		
			setupPanelMode();
			
			updatePanelView();
		}
		
		
		public function setupPanelMode():void
		{
			drawBackground(this.graphics, _fillModel.type);

			takedownSolidFill();
			takedownGradientFill();
			takedownJellybeanFill();
			
			switch (_fillModel.type) {
				case GraphicFill.SOLID_FILL:
					setupSolidFill();
					break;
				case GraphicFill.GRADIENT_FILL:
					setupGradientFill();
					break;
				case GraphicFill.JELLYBEAN_FILL:
					setupJellybeanFill();
					break;
			}
			
			updatePanelView();
		}
		
// **** code for each type of fill:  solid, gradient, jellybean
		
		private function setupSolidFill():void
		{
			_fillView.addEventListener(MouseEvent.MOUSE_DOWN, solidColorMouseDownListener);
		}
		
		private function takedownSolidFill():void
		{
			_fillView.removeEventListener(MouseEvent.MOUSE_DOWN, solidColorMouseDownListener);
		}
		
		private function solidColorMouseDownListener(event:MouseEvent):void
		{
			editColor("color", -1, null);
		}
		
		
		private function setupGradientFill():void
		{
			
			setupGradientColorChips();
			
			_gradientControls = new Sprite(); // put controls on here so they can be dimmed during color-pick
			addChild(_gradientControls);

			setupGradientControls(_gradientControls);
						
			_fillView.addEventListener(MouseEvent.MOUSE_DOWN, gradientMouseDownListener);
			_fillView.useHandCursor = true;
			_fillView.buttonMode = true;
			_fillView.mouseEnabled = true;
			_fillView.mouseChildren = false;
		}
		
		
		private function takedownGradientFill():void
		{
			takedownGradientColorChips();
			
			if (_gradientControls) {
				removeChild(_gradientControls);
				_gradientControls = null;
				_plusButton.removeEventListener(MouseEvent.CLICK, plusButtonListener);
				_minusButton.removeEventListener(MouseEvent.CLICK, minusButtonListener);
				_swapButton.removeEventListener(MouseEvent.CLICK, swapButtonListener);
				_linearButton.removeEventListener(MouseEvent.CLICK, linearButtonListener);
				_radialButton.removeEventListener(MouseEvent.CLICK, radialButtonListener);
			}
			_fillView.removeEventListener(MouseEvent.MOUSE_DOWN, gradientMouseDownListener);
			_fillView.useHandCursor = false;
		}

		
		private function setupGradientControls(holder:Sprite):void
		{
			_plusButton = TableButtons.plusButton;
			_plusButton.x = PLUS_X;
			_plusButton.y = PLUS_Y;
			holder.addChild(_plusButton);
			_plusButton.addEventListener(MouseEvent.CLICK, plusButtonListener);
			
			_minusButton = TableButtons.minusButton;
			_minusButton.x = MINUS_X;
			_minusButton.y = MINUS_Y;
			holder.addChild(_minusButton);
			_minusButton.addEventListener(MouseEvent.CLICK, minusButtonListener);
			
			_swapButton = swapButton;
			_swapButton.x = SWAP_X;
			_swapButton.y = SWAP_Y;
			holder.addChild(_swapButton);
			_swapButton.addEventListener(MouseEvent.CLICK, swapButtonListener);
			
			_linearToggleGraphic = getRectangleBackground(TOGGLE_ICON_WIDTH,TOGGLE_ICON_HEIGHT, 0xffffff);
			_linearButton = createLinearButton(_linearToggleGraphic);
			_linearButton.x = LINEAR_X;
			_linearButton.y = LINEAR_Y;
			holder.addChild(_linearButton);
			_linearButton.addEventListener(MouseEvent.CLICK, linearButtonListener);
			
			_radialToggleGraphic = getRectangleBackground(TOGGLE_ICON_WIDTH,TOGGLE_ICON_HEIGHT, 0xffffff);
			_radialButton = createRadialButton(_radialToggleGraphic);
			_radialButton.x = RADIAL_X;
			_radialButton.y = RADIAL_Y;
			holder.addChild(_radialButton);
			_radialButton.addEventListener(MouseEvent.CLICK, radialButtonListener);
		}
		
		private function linearButtonListener(event:MouseEvent):void
		{
			trace("linearButtonListener");
			
			_fillModel.linear = true;
			updatePanelView();
		}
		
		private function radialButtonListener(event:MouseEvent):void
		{
			trace("radialButtonListener");
			_fillModel.linear = false;
			updatePanelView();
		}
		
		private function plusButtonListener(event:MouseEvent):void
		{
			var count:int = _fillModel.colors.length;
			if (count >= GraphicFill.MAX_COLORS)
				return; 
			var color:uint = _fillModel.colors[count-1];  // grab the last color
			_fillModel.colors.push(color);
			
			takedownGradientColorChips();
			setupGradientColorChips();
			
			updatePanelView();
			trace("color count="+_fillModel.colors.length);
		}
		
		private function minusButtonListener(event:MouseEvent):void
		{
			var count:int = _fillModel.colors.length;
			if (count <= 2)
				return;  // cannot have less than 2 colors in a gradient
			_fillModel.colors.splice(count-1,1);
			
			takedownGradientColorChips();
			setupGradientColorChips();
			
			updatePanelView();
		}
		
		private function swapButtonListener(event:MouseEvent):void
		{
			var lower:int = 0;
			var upper:int = _fillModel.colors.length -1;
			while (lower < upper) {
				var temp:uint = _fillModel.colors[lower];
				_fillModel.colors[lower] = _fillModel.colors[upper];
				_fillModel.colors[upper] = temp;
				
				++lower;  --upper;
			}
			
			takedownGradientColorChips();
			setupGradientColorChips();
			
			updatePanelView();
		}

		
		private function setupGradientColorChips():void
		{			
			_fillModel = _fillModel.clone();  // ensure each one is independent

			var count:int = _fillModel.colors.length;
			var xx:Number = GRADIENT_CHIPS_X;
			var spacing:Number = FILL_WIDTH / count;
			for (var n:int=0; n < count; n++) {
				var chip:Sprite = new Sprite();
				_gradientChips[n] = chip;
				addChild(chip);
				chip.addEventListener(MouseEvent.MOUSE_DOWN, gradientChipListener);
				chip.x = xx;
				chip.y = GRADIENT_CHIPS_Y;
				var color:uint = _fillModel.colors[n] as uint;
				drawColorChip(chip,color,spacing+1);  // the +1 prevents an occasional extra gap due to rounding err
				xx += spacing;
			}
			_colorChipWidth = spacing+1;
		}
		
		private function takedownGradientColorChips():void
		{
			for (var n:int=0; n < _gradientChips.length; n++) {
				var chip:Sprite = _gradientChips[n];
				removeChild(chip);
				chip.removeEventListener(MouseEvent.MOUSE_DOWN, gradientChipListener);
			}
			_gradientChips = [];
		}

		
		private function gradientChipListener(event:MouseEvent):void
		{
			trace("gradientChipListener: target="+event.target);
			var index:int = _gradientChips.indexOf(event.target);
			if (index != -1) {
				editColor("colors", index, event.target as Sprite);
			}
		}
		
		
		private function gradientAngleListener(event:Event):void
		{
			var value:Number = Number(_gradientAngleText.text);
			_fillModel.angle = value;
			trace("gradientAngleListener: angle="+value);
			
			updatePanelView();
		}
		
		
		// dragging in a gradient to adjust the angle of linear gradient, or offset of radial gradient
		private var _gradientDragStart:Point;
		private var _gradientDragStartOffset:Point;
		private var _gradientDragStartAngle:Number;
		private var _gradientDragStartMouseAngle:Number;
		
		private function gradientMouseDownListener(event:MouseEvent):void
		{
			stage.addEventListener(MouseEvent.MOUSE_MOVE, gradientMouseMoveListener);
			stage.addEventListener(MouseEvent.MOUSE_UP, gradientMouseUpListener);
			_gradientDragStart = new Point(event.stageX, event.stageY);
			_gradientDragStartOffset = new Point(_fillModel.xOffset, _fillModel.yOffset);
			_gradientDragStartAngle = _fillModel.angle;
			_gradientDragStartMouseAngle = computeMouseAngle(event.stageX, event.stageY);
		}
		
		private function gradientMouseMoveListener(event:MouseEvent):void
		{
			if (_fillModel.linear) {
				var degrees:Number = computeMouseAngle(event.stageX, event.stageY);
				degrees += _gradientDragStartAngle - _gradientDragStartMouseAngle;
				if (event.shiftKey)
					degrees = Math.round(degrees / 45) * 45;
				adjustLinearGradientAngle(degrees);
			} else {
				var dx:Number = event.stageX - _gradientDragStart.x;
				var dy:Number = event.stageY - _gradientDragStart.y;
				adjustRadialGradientOffset(dx,dy);
			}
		}
		
		private function computeMouseAngle(stageX:Number, stageY:Number):Number
		{
			// angle in degrees, from center of fill-view to stageX,stageY
			var center:Point = new Point(FILL_WIDTH/2, FILL_HEIGHT/2);
			var global:Point = _fillView.localToGlobal(center);
			var dx:Number = stageX - global.x;  // distance from mouse to center of fill view
			var dy:Number = stageY - global.y;
			return Math.atan2(dy, dx) * 180 / Math.PI;
		}
		
		private function adjustLinearGradientAngle(angle:Number):void
		{
			_fillModel.angle = angle;
			trace("adjustLinearGradientAngle angle="+_fillModel.angle);
			_gradientArrow.rotation = _fillModel.angle;
			updatePanelView();
		}
		
		private function adjustRadialGradientOffset(dx:Number, dy:Number):void
		{
			// dx,dy is from mouse-down to current move point
			var xoff:Number = _gradientDragStartOffset.x + dx * 200 / FILL_WIDTH;
			var yoff:Number = _gradientDragStartOffset.y + dy * 200 / FILL_HEIGHT;
			
			if (xoff < -100)	xoff = -100;
			if (xoff > 100)		xoff = 100;
			if (yoff < -100)	yoff = -100;
			if (yoff > 100)		yoff = 100;
			
			_fillModel.xOffset = xoff;
			_fillModel.yOffset = yoff;
			trace("adjustRadialGradient: xoff="+xoff, "yoff="+yoff);
			
			updatePanelView();
		}
		
		private function gradientMouseUpListener(event:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, gradientMouseMoveListener);
			stage.removeEventListener(MouseEvent.MOUSE_UP, gradientMouseUpListener);
		}
		
		private function drawGradientArrow(graphics:Graphics, color:uint, thick:Number):void
		{
			graphics.lineStyle(thick, color);
			graphics.moveTo(-15,0);
			graphics.lineTo(15, 0);
			graphics.lineTo(10, -5);
			graphics.moveTo(15, 0);
			graphics.lineTo(10, 5);
		}
				
		
		private function setupJellybeanFill():void
		{
			var chip:Sprite = new Sprite();
			_jellyColorChip1 = chip;
			addChild(chip);
			chip.addEventListener(MouseEvent.MOUSE_DOWN, jellyColorChipListener);
			chip.x = JELLY_COLOR1_X;
			chip.y = JELLY_COLOR1_Y;
			
			var color:uint = _fillModel.color1;
			drawColorChip(chip,color, COLOR_CHIP_WIDTH);

			chip = new Sprite();
			_jellyColorChip2 = chip;
			addChild(chip);
			chip.addEventListener(MouseEvent.MOUSE_DOWN, jellyColorChipListener);
			chip.x = JELLY_COLOR2_X;
			chip.y = JELLY_COLOR2_Y;
			
			color = _fillModel.color2;
			drawColorChip(chip,color, COLOR_CHIP_WIDTH);
			_colorChipWidth = COLOR_CHIP_WIDTH;
		}
		
		private function takedownJellybeanFill():void
		{
			if (_jellyColorChip1) {
				removeChild(_jellyColorChip1);
				_jellyColorChip1.removeEventListener(MouseEvent.MOUSE_DOWN, jellyColorChipListener);
				_jellyColorChip1 = null;
				removeChild(_jellyColorChip2);
				_jellyColorChip2.removeEventListener(MouseEvent.MOUSE_DOWN, jellyColorChipListener);
				_jellyColorChip2 = null;
			}
		}
		
		
		private function jellyColorChipListener(event:MouseEvent):void
		{
			trace("jellyColorChipListener: target="+event.target);
			if (event.target == _jellyColorChip1) {
				editColor("color1", -1, _jellyColorChip1);
			} else if (event.target == _jellyColorChip2) {
				editColor("color2", -1, _jellyColorChip2);
			}
		}

		
		
//  ****  editColor() and friends: can edit any color in the GraphicFill model
		
		private function editColor(fieldName:String, index:int, colorChip:Sprite):void
		{
			_editColorFieldName = fieldName;
			_editColorIndex = index;
			_editColorChip = colorChip;
			var targetX:Number;
			var targetY:Number;
			if (colorChip) {
				targetY = colorChip.y + COLOR_CHIP_HEIGHT/2;
				targetX = colorChip.x + colorChip.width/2;
			} else {
				targetY = Y_FILL + FILL_HEIGHT/2;  // for solid color there is no chip, so picker points to the fill view
				targetX = X_FILL + FILL_WIDTH/2;
			}
			
			showColorPicker(targetX, targetY);
			
			if (_editColorIndex == -1)
				_colorPicker.currentColor = _fillModel[_editColorFieldName];
			else
				_colorPicker.currentColor = _fillModel[_editColorFieldName][_editColorIndex];

			_colorPicker.addEventListener(ColorPicker.UPDATE_COLOR, updateColorListener);
			_appContainer.mouseChildren = false;
			_appContainer.addEventListener(MouseEvent.MOUSE_MOVE, colorPickerMoveListener);
			
			if (_gradientControls)
				_gradientControls.alpha = 0.3;  // dim the controls because they don't work while color picker is active (since it grabs colors for anywhere)
		}
		
		private function closeEditColor():void
		{
			if (_colorPicker) {
				_colorPicker.removeEventListener(ColorPicker.UPDATE_COLOR, updateColorListener);
				_colorPicker.visible = false;
			}
			_appContainer.mouseChildren = true;
			_appContainer.removeEventListener(MouseEvent.MOUSE_MOVE, colorPickerMoveListener);
			_editColorFieldName = null;
			_editColorChip = null;
			if (_gradientControls)
				_gradientControls.alpha = 1.0;  // restore the controls
		}
		
		private function colorPickerMoveListener(event:MouseEvent):void
		{
			_colorPicker.chooseColor(event);
			
			updateEditColor(_colorPicker.mouseMoveColor);
		}
		
		
		private function updateColorListener(event:Event):void
		{
			trace("GraphicFillPanel:updateColorListener color="+_colorPicker.newColor);
			
			updateEditColor(_colorPicker.newColor);
			
			closeEditColor();
			
			// bootstrap an ok action to prevent another click when they've selected a color already
			if (_fillModel.type == GraphicFill.SOLID_FILL)
				okAction();
		}
		
		
		private function showColorPicker(targetX:Number, targetY:Number):void
		{
			buildColorPickerIfNeeded();  // adds the picker as a child of this object

			_colorPicker.x = 1;
			_colorPicker.y = targetY + 11;
			_colorPicker.arrowX = targetX;
			_colorPicker.visible = true;
			setChildIndex(_colorPicker, numChildren-1);  // bring color picker to front
		}
		
		public function buildColorPickerIfNeeded():void
		{
			if(!_colorPicker){
				_colorPicker = new ColorPicker(_appContainer);
				_colorPicker.draw();
				addChild(_colorPicker);
			}else{
				trace("Color Picker Present");
			}
		}
		
		public function get colorPicker():ColorPicker
		{
			return _colorPicker;
		}
		
		
		private function updateEditColor(color:uint):void
		{
			trace("updateEditColor color="+color);
			if (_editColorIndex == -1)
				_fillModel[_editColorFieldName] = color;
			else
				_fillModel[_editColorFieldName][_editColorIndex] = color;
			
			if (_editColorChip) {
				drawColorChip(_editColorChip,color, _colorChipWidth);
			}
			
			updatePanelView();
		}
		
		
		
		//  ****  popup menu of fill types
		
		
		private function createMenu(xLoc:Number, yLoc:Number, www:Number, hhh:Number, value:int):PopOutMenu
		{
			var menuDataProvider:Array = [
				{label:"Solid color", data:GraphicFill.SOLID_FILL}
				
			];
			
			if(_gradientAllowed){
				
				menuDataProvider.push( {label:"Gradient", data:GraphicFill.GRADIENT_FILL} );
			}
			
			if (_jellybeanAllowed) {
				menuDataProvider.push( {label:"Jellybean", data:GraphicFill.JELLYBEAN_FILL} );
			}
			
			// find the current value in the array
			var currentIndex:int = 0;
			for each (var thing:Object in menuDataProvider) {
				if (thing.data == value) {
					currentIndex = menuDataProvider.indexOf(thing);
					break;
				}
			}
			
			var menu:PopOutMenu;
			menu = new PopOutMenu(_appContainer);
			menu.closedHeight = hhh;
			menu.closedWidth = www;
			menu.labelSize = 11;
			menu.popWidth = 105;
			menu.dataProvider = menuDataProvider;
			menu.defaultIndex = currentIndex;
			menu.baseColor = 0x000000;  // 0x191919;
			menu.borderColor = 0x000000;
			menu.arrowScale = .75;
			menu.baseLabelOffsetX = -5;
			menu.baseLabelOffsetY = -3;
			menu.arrowOffsetX = 3;
			menu.baseLabelWidthAdjustment = -10;
			menu.direction = PopOutMenu.DIRECTION_BI;
			menu.drawComponent();
			menu.visible = true;
			menu.x = xLoc;
			menu.y = yLoc;
			menu.addEventListener(PopOutMenu.ITEM_CHANGED, fillTypeChangeListener);
			return menu;
		}
		
		private function fillTypeChangeListener(event:Event):void{
			if (event.target is PopOutMenu) {
				var menu:PopOutMenu = event.target as PopOutMenu;
				var fillType:int = menu.selectedItem.data as int;
				changeFillType(fillType);
			}
		}
		
// **** buttons
		
		private static const ICON_GRAY:uint = 0x6A6A6A;
		
		
		private function get swapButton():IconButton
		{
			var btn:IconButton = new IconButton(getRoundRectangleBackground(20, 18, BACKGROUND_COLOR), swapIcon, ICON_GRAY, 0, 0, 0, 0xFFFFFF, 0xFFFFFF, 0);
			btn.positionIcon(false, 3, 5);
			return btn;
		}
		
		private function get swapIcon():Sprite
		{
			var icon:Sprite = new Sprite();
			icon.graphics.lineStyle(1, BUTTON_ICON_COLOR, 1.0, false, "normal", CapsStyle.NONE);
			const w:Number = 13; // width of icon
			const h:Number = 8;  // spacing between the two arrow lines
			const a:Number = 3; // arrowhead size
			icon.graphics.moveTo(0, 0);
			icon.graphics.lineTo(w, 0);
			icon.graphics.beginFill(BUTTON_ICON_COLOR);
			icon.graphics.lineTo(w-a, 0+a);
			icon.graphics.lineTo(w-a, 0-a);
			icon.graphics.lineTo(w, 0);
			icon.graphics.endFill();
			
			icon.graphics.moveTo(w, h);
			icon.graphics.lineTo(0, h);
			icon.graphics.beginFill(BUTTON_ICON_COLOR);
			icon.graphics.lineTo(0+a, h-a);
			icon.graphics.lineTo(0+a, h+a);
			icon.graphics.lineTo(0, h);
			icon.graphics.endFill();
			
			return icon;
		}
		
		private function createLinearButton(toggleGraphic:Sprite):IconButton
		{
			// the toggleGraphic is passed in so the client code can later update it, for toggle action
			var btn:IconButton = new IconButton(getRectangleBackground(18, 15, BACKGROUND_COLOR), linearIcon, ICON_GRAY, 0, 0, 0, 0xFFFFFF, 0xFFFFFF, 0);
			btn.positionIcon(false, 1, 1);
			btn.addChildAt(toggleGraphic,1);  // between background and icon
			toggleGraphic.x = 1;
			toggleGraphic.y = 1;
			return btn;
		}
		
		private function get linearIcon():Sprite
		{
			var icon:Sprite = new Sprite();
			icon.graphics.lineStyle(1, BUTTON_ICON_COLOR, 1.0, false, "normal", CapsStyle.NONE);
			const w:Number = TOGGLE_ICON_WIDTH; 
			const h:Number = TOGGLE_ICON_HEIGHT;
			
			icon.graphics.drawRect(0,0,w,h);
			
			for (var n:int=1; n < 5; n++) {
				var xx:Number = n * 3.0;  // spacing of bars
				icon.graphics.moveTo(xx, 0);
				icon.graphics.lineTo(xx, h);
			}
			return icon;
		}
		
		private function createRadialButton(toggleGraphic:Sprite):IconButton
		{
			// the toggleGraphic is passed in so the client code can later update it, for toggle action
			var btn:IconButton = new IconButton(getRectangleBackground(18, 15, BACKGROUND_COLOR), radialIcon, ICON_GRAY, 0, 0, 0, 0xFFFFFF, 0xFFFFFF, 0);
			btn.positionIcon(false, 1, 1);
			btn.addChildAt(toggleGraphic,1);  // between background and icon
			toggleGraphic.x = 1;
			toggleGraphic.y = 1;
			return btn;
		}
		
		private function get radialIcon():Sprite
		{
			var icon:Sprite = new Sprite();
			icon.graphics.lineStyle(1, BUTTON_ICON_COLOR, 1.0, false, "normal", CapsStyle.NONE);
			const w:Number = TOGGLE_ICON_WIDTH; 
			const h:Number = TOGGLE_ICON_HEIGHT;
			
			icon.graphics.drawRect(0,0,w,h);
			
			var xx:Number = w/2;
			var yy:Number = h/2;
			icon.graphics.drawCircle(xx,yy,1);
			icon.graphics.drawCircle(xx,yy,3);
			icon.graphics.drawCircle(xx,yy,5);
			
			return icon;
		}
		
		
		private static function getRectangleBackground(w:Number, h:Number, color:uint):Sprite
		{
			var bg:Sprite = new Sprite();
			bg.graphics.beginFill(color);
			bg.graphics.drawRect(0,0,w,h);
			
			return bg;
		}
		
		private static function getRoundRectangleBackground(w:Number, h:Number, color:uint):Sprite
		{
			var bg:Sprite = new Sprite();
			bg.graphics.beginFill(color);
			bg.graphics.drawRoundRect(0,0,w,h, h/2,h/2);
			
			return bg;
		}

				
		private function drawColorChip(chip:Sprite, color:uint, www:Number):void
		{
			trace("drawColorChip color="+color.toString(16), "width="+www, "x="+chip.x, "y="+chip.y);
			chip.graphics.clear();
			chip.graphics.beginFill(color);
			chip.graphics.lineStyle(1, 0);
			chip.graphics.drawRect(0,0, www, COLOR_CHIP_HEIGHT);
			chip.graphics.endFill();
		}
	}
}