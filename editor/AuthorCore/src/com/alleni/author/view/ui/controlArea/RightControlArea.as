package com.alleni.author.view.ui.controlArea
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.controlArea.RightControlAreaMediator;
	import com.alleni.author.view.ui.controls.Button;
    import assets.appui.controlArea.fxg.*
	
	import flash.display.Sprite;
	
	public class RightControlArea extends Sprite
	{
		public static const RIGHT_CONTROL_AREA_OPEN:String = "Content Control Area Open";
		public static const RIGHT_CONTROL_AREA_CLOSE:String = "Content Control Area Close";
		
		public var panelWidth:Number;
		public var panelHeight:Number;
		public var inspectorButton:Button;
		public var ribbonLibraryButton:Button;
		public var dockButton:Button;

		
		public function initialize():void
		{
            dockButton = new Button();
            dockButton.id = "dockButton";
            dockButton.centerAssets = true;
            dockButton.skin_unsel_down = new clSize2Cyan54EBBE();
            dockButton.skin_unsel_roll = new clSize2Blue6B91BA();
            dockButton.skin_unsel_up =   new clSize1Blue6B91BA();
            dockButton.skin_sel_down =   new clSize2Blue6B91BA();
            dockButton.skin_sel_roll =   new clSize2Cyan54EBBE();
            dockButton.skin_sel_up =     new clSize1Cyan54EBBE();
            dockButton.buildButton(true);
            dockButton.y = 30;
            dockButton.x = EditorUI.RIGHT_PALETTE_WIDTH + panelWidth/2;
            dockButton.buttonMode = true;
            dockButton.toolTip = "Cloud";
            addChild(dockButton);

			inspectorButton = new Button();
			inspectorButton.id = "inspectorButton";
            inspectorButton.centerAssets = true;
            inspectorButton.skin_unsel_down = new inSize2Cyan54EBBE();
            inspectorButton.skin_unsel_roll = new inSize2Blue6B91BA();
            inspectorButton.skin_unsel_up =   new inSize1Blue6B91BA();
            inspectorButton.skin_sel_down =   new inSize2Blue6B91BA();
            inspectorButton.skin_sel_roll =   new inSize2Cyan54EBBE();
            inspectorButton.skin_sel_up =     new inSize1Cyan54EBBE();
			inspectorButton.buildButton(true);
			inspectorButton.y = dockButton.y + 50;
			inspectorButton.x = EditorUI.RIGHT_PALETTE_WIDTH + panelWidth/2;
			inspectorButton.buttonMode = true;
			inspectorButton.toolTip = "Property Inspector";
			addChild(inspectorButton);

            ribbonLibraryButton = new Button();
            ribbonLibraryButton.id = "ribbonLibraryButton";
            ribbonLibraryButton.centerAssets = true;
            ribbonLibraryButton.skin_unsel_down = new rlSize2Cyan54EBBE();
            ribbonLibraryButton.skin_unsel_roll = new rlSize2Blue6B91BA();
            ribbonLibraryButton.skin_unsel_up =   new rlSize1Blue6B91BA();
            ribbonLibraryButton.skin_sel_down =   new rlSize2Blue6B91BA();
            ribbonLibraryButton.skin_sel_roll =   new rlSize2Cyan54EBBE();
            ribbonLibraryButton.skin_sel_up =     new rlSize1Cyan54EBBE();
            ribbonLibraryButton.buildButton(true);
            ribbonLibraryButton.y = inspectorButton.y + 50;
            ribbonLibraryButton.x = EditorUI.RIGHT_PALETTE_WIDTH + panelWidth/2;
            ribbonLibraryButton.buttonMode = true;
            ribbonLibraryButton.toolTip = "Ribbon Library";
            addChild(ribbonLibraryButton);


			new RightControlAreaMediator().handleViewEvents(this);
		}
		
				
		public function handleMeasure():void
		{
			// nothing necessary at this time, could use this to space objects out or something based on dimensions
		}
		
	}
}
