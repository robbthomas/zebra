package com.alleni.author.view.ui.controls
{
	import com.alleni.author.view.ui.components.PushButton;
	import com.alleni.author.view.ui.components.Style;
	
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.events.Event;

	public class ClipboardCopyMessage extends PlayerMessage
	{
		private static const SAVED_MESSAGE:String = "Saved to Clipboard!";
		
		private var _button:PushButton;
		private var _buffer:String;
		
		public function ClipboardCopyMessage()
		{
			super();
			_buffer = "";
		}
		
		public function set buffer(value:String):void
		{
			_buffer = value;
			if (_button) _button.label = _buffer;
		}
		
		override public function set visible(value:Boolean):void
		{
			super.visible = value;
			if (value && _button) _button.label = _buffer;
		}
		
		override protected function draw():void
		{
			super.draw();
			
			Style.setStyle(Style.DARK);
			_button = new PushButton(_container, 0, 0, _buffer, handleClick);
			_button.width = WIDTH - PADDING*2;
			_button.height = 80;
			_button.alpha = 0.9;
			_button.labelSize = 12;
			_button.labelMultiline = true;
			_button.filters = [_buttonShadow];
			
			handleMeasure();
			this.visible = true;
		}
		
		private function handleClick(e:Event):void
		{
			Clipboard.generalClipboard.setData(ClipboardFormats.TEXT_FORMAT, _buffer);
			var button:PushButton = e.target as PushButton;
			if (!button) return;
			button.label = SAVED_MESSAGE;
			
			dismissAfterDelay();
		}
	}
}