package com.alleni.author.view.ui.palettes
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.controller.ui.GadgetController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.ScrollUpdateEvent;
	import com.alleni.author.model.ScrollModel;
	import com.alleni.author.model.objects.IListable;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.Library;
	import com.alleni.author.util.cache.LRUCache;
	import com.alleni.author.view.PaletteVerticalScrollView;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichLabel;
	import com.alleni.author.view.ui.MenuButton;
	import com.alleni.author.view.ui.controls.PopOutMenu;
	import com.alleni.savana.Ast;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.CapsStyle;
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;
	import mx.collections.Sort;
	import mx.collections.SortField;

	public class VariableLibraryView extends Sprite
	{
		private static const BACKGROUND_COLOR:uint = 0x000000;
		private static const BACKGROUND_ALPHA:Number = 1;
		private static const BORDER_COLOR:uint = 0x585858;
		private static const BORDER_THICKNESS:Number = 1;
		private static const MARGIN:Number = 5;
		
		private static const RIBBON_HEIGHT:uint = 20;
		private static const TOP_MENU_HEIGHT:uint = 25;
		private static const BOTTOM_PADDNG_HEIGHT:uint = 5;
		private static const BOTTOM_MENU_AREA:uint = 12 * RIBBON_HEIGHT;
		
		private static const NUMBER_OF_TOP_MENUS:uint = 3;	
		private static const SORT_TYPE_DESCENDING:Boolean = true;
		private static const SORT_TYPE_ASCENDING:Boolean = false;
		private var _sortType:Boolean = SORT_TYPE_ASCENDING;  //for when ui allows changing sort type
		
		private static const OVERALL_WIDTH:Number 			= 175;
		private static const VARIABLE_COLUMN_WIDTH:Number      = 135;
		private static const COUNT_COLUMN_WIDTH:Number       = 25;
		private static const SCROLL_COLUMN_WIDTH:Number      = 15;
		
		private static const MINIMUM_PANEL_HEIGHT:Number = 75;
		
		private var _model:Library;
		
		private var _filteredItems:ArrayCollection;
		private var _projectNameLabel:LightLabel;
		private var _resultText:RichLabel;
		private var _typeFilterMenu:PopOutMenu;
		private var _categoryFilterMenu:PopOutMenu;
	
		private var _deletePanel:Sprite;
		private var _spacesNeeded:uint = 0;
		private var _itemArea:Sprite;
		private var _itemBorder:Shape;
		
		private var _scrollNeeded:Boolean = false;
		private var _scrollbar:PaletteVerticalScrollView;
		private var _scrollModel:ScrollModel;
		private var _scrollPosition:Number = 0;
		
		private var _bottomPanelScrollNeeded:Boolean = false;
		private var _bottomPanelScrollbar:PaletteVerticalScrollView;
		private var _bottomPanelScrollModel:ScrollModel;
		private var _bottomPanelScrollPosition:Number = 0;
		
		private var _itemCache:LRUCache;
		
		private var _descriptionField:LightEditableLabel;
		private var _descriptionPanel:Sprite
		private var _previousSelected:VariableItemView;
		
		private var _topPanel:Sprite = new Sprite();
		private var _topPanelHeight:Number = 0;
		
		private var _bottomPanel:Sprite = new Sprite();
		private var _bottomPanelHeight:Number = 0;
		
		private var _divider:Sprite = new Sprite();

		
		public function VariableLibraryView()
		{
			super();
			
			_filteredItems = new ArrayCollection();
			_itemCache = new LRUCache(300, 500);
			
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(event:Event=null):void{
			
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			var shadow:DropShadowFilter = new DropShadowFilter(1,80,0,0.8,2,2,2,1);
			
			initProjectLabel(shadow); 
			initMenu();
			
			
			var _initialheight:Number = (EditorUI.instance.paletteHeight - ((TOP_MENU_HEIGHT * NUMBER_OF_TOP_MENUS)  + BOTTOM_PADDNG_HEIGHT + _divider.height)) /2;
			_topPanelHeight = _initialheight;
			_bottomPanelHeight = _initialheight;
			
			dimensionPanel(_topPanel, OVERALL_WIDTH,_initialheight,0xFF0000);
			dimensionPanel(_bottomPanel, OVERALL_WIDTH,_initialheight,0x0000FF);
			dimensionPanel(_divider, OVERALL_WIDTH,8,BACKGROUND_COLOR);
			
			initBottomPanel();
			initDivider();
			
			_topPanel.x = 0;
			_topPanel.y = TOP_MENU_HEIGHT * NUMBER_OF_TOP_MENUS + 1;
			
			_divider.x = 0;
			_divider.y = _topPanel.y + _topPanel.height;
			
			_bottomPanel.x = 0;
			_bottomPanel.y = _divider.y + _divider.height;
			
			addChild(_topPanel);
			addChild(_bottomPanel);
			addChild(_divider);
			
			_divider.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener);
			
			renderTopPanel();
			renderBottomPanel();
			
			//filter();
			filterCategories();
			filterType();



		}
		
		private function initProjectLabel(shadow:DropShadowFilter):void{
			_projectNameLabel = new LightLabel();
			_projectNameLabel.color = 0xFFFFFF;
			_projectNameLabel.y = 5;
			_projectNameLabel.x = 15;
			_projectNameLabel.width = 150;
			_projectNameLabel.text = Application.instance.document.name;
			_projectNameLabel.filters = [shadow];
			addChild(_projectNameLabel);
		}
		
		private function initMenu():void{
			
			_categoryFilterMenu = new PopOutMenu(EditorUI.instance);
			_categoryFilterMenu.closedHeight = 15;
			_categoryFilterMenu.closedWidth = 150;
			_categoryFilterMenu.popWidth = 150;
			_categoryFilterMenu.baseType = PopOutMenu.BASE_TYPE_BOTH;
			_categoryFilterMenu.itemsType = PopOutMenu.ITEMS_TYPE_BOTH;
			_categoryFilterMenu.dataProvider = categoryData;
			_categoryFilterMenu.direction = PopOutMenu.DIRECTION_DOWN;
			_categoryFilterMenu.baseLabelOffsetX = 18;
			_categoryFilterMenu.baseLabelOffsetY = 1;
			_categoryFilterMenu.checkMarkScale = .5;
			_categoryFilterMenu.defaultIndex = 0;
			_categoryFilterMenu.drawComponent();
			_categoryFilterMenu.x = 3;
			_categoryFilterMenu.y = 26;
			_categoryFilterMenu.addEventListener(PopOutMenu.ITEM_CHANGED, handleCategoryFilterChange);
			
			
			_typeFilterMenu = new PopOutMenu(EditorUI.instance);
			_typeFilterMenu.closedHeight = 23;
			_typeFilterMenu.closedWidth = 150;
			_typeFilterMenu.popWidth = 150;
			_typeFilterMenu.baseType = PopOutMenu.BASE_TYPE_BOTH;
			_typeFilterMenu.itemsType = PopOutMenu.ITEMS_TYPE_BOTH;
			_typeFilterMenu.dataProvider = typeData;
			_typeFilterMenu.direction = PopOutMenu.DIRECTION_DOWN;
			_typeFilterMenu.baseLabelOffsetX = 18;
			_typeFilterMenu.baseLabelOffsetY = 1;
			_typeFilterMenu.checkMarkScale = .5;
			_typeFilterMenu.defaultIndex = 0;
			_typeFilterMenu.drawComponent();
			_typeFilterMenu.x = 3;
			_typeFilterMenu.y = 49;
			_typeFilterMenu.addEventListener(PopOutMenu.ITEM_CHANGED, handleTypeFilterChange);
			
			parent.addChild(_typeFilterMenu);
			parent.addChild(_categoryFilterMenu);  //must be added after type menu
			
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_NAME_CHANGED, handleProjectNameChanged);
			
			
		}
		
		
		private function initBottomPanel():void{
			_descriptionField = new LightEditableLabel(VARIABLE_COLUMN_WIDTH, BOTTOM_MENU_AREA, true);
			_descriptionField.explicitLineBreak = false;
			_descriptionField.editable = false;
			
			_descriptionField.embedFonts = true;
			_descriptionField.color = 0xFFFFFF;
			_descriptionField.size = 12;
			_descriptionField.bold = false;
			_descriptionField.expandable = false;
			_descriptionField.setPaddingParams({left:10, top:20, right:10, bottom:0});
			_descriptionField.text = "Description";

			_descriptionField.y = 5;
			
			_bottomPanel.addChild(_descriptionField);
		}
		
		private function initDivider():void{
			
			var xPos:Number = (OVERALL_WIDTH - 20)/2;  
			var yPos:Number = _divider.height / 2;
			with(_divider.graphics){
                lineStyle(2,0x575757,1,false,"normal",CapsStyle.ROUND);
                moveTo(xPos,yPos);
                lineTo(xPos+15,yPos);
                lineStyle(BORDER_THICKNESS, BORDER_COLOR,1, true);
                moveTo(0,0);
                lineTo(OVERALL_WIDTH,0);
                moveTo(0,_divider.height);
                lineTo(OVERALL_WIDTH,_divider.height);
			}
		}
		
		
		private function handleCategoryFilterChange(event:Event):void
		{
			filterCategories()
			_scrollPosition = 0;
		}
		
		private function handleTypeFilterChange(event:Event):void
		{
			filterType()
			_scrollPosition = 0;
		}
		
		private function get model():Library
		{
			if (!_model)
				_model = Library.instance;
			return _model;
		}
		
		private function handleProjectNameChanged(event:ApplicationEvent = null):void
		{
			_projectNameLabel.text = Application.instance.document.name;
		}
		
		private function get paletteHeight():uint
		{
			return (RIBBON_HEIGHT * _spacesNeeded) + (TOP_MENU_HEIGHT * NUMBER_OF_TOP_MENUS) + BOTTOM_MENU_AREA + BOTTOM_PADDNG_HEIGHT;
		}
		
		private function updateList():void
		{	
			if (_scrollbar) {
				_scrollPosition = _scrollbar.scrollPosition;
				_scrollbar.removeEventListener(ScrollUpdateEvent.SCROLL, handleScroll);
				if(contains(_scrollbar)){
					removeChild(_scrollbar);
				}
				
				_scrollbar = null;
				_scrollModel = null;
			}
			
			if (_bottomPanelScrollbar) {
				_bottomPanelScrollPosition = _bottomPanelScrollbar.scrollPosition;
				_bottomPanelScrollbar.removeEventListener(ScrollUpdateEvent.SCROLL, handleBottomPanelScroll);
				if(contains(_bottomPanelScrollbar)){
					removeChild(_bottomPanelScrollbar);
				}
				
				_bottomPanelScrollbar = null;
				_bottomPanelScrollModel = null;
			}
			
			
			while (_topPanel.numChildren > 0)
				_topPanel.removeChildAt(0);
			
			var i:int = 0;
			var length:int = _filteredItems.length;	
			for (i = 0; i < length; i++) {
				var itemView:VariableItemView = new VariableItemView(_filteredItems[i].item, _scrollNeeded);  //always display with _scrollNeeded=true
				itemView.addEventListener(MouseEvent.CLICK, handleItemClick);
				itemView.y = (RIBBON_HEIGHT * i);
				_topPanel.addChild(itemView);
			}
			
			renderTopPanel();
			renderBottomPanel();
		}
		
				
		private function renderTopPanel():void
		{
			var scrollRect:Rectangle;
			var availableSpace:Number = _topPanelHeight;
			var possibleAssetSpaces:uint = availableSpace>0?Math.floor(availableSpace/RIBBON_HEIGHT):0;
			var borderGraphicsHeight:Number = Math.max(_topPanelHeight, _filteredItems.length * RIBBON_HEIGHT);
			var gradientShadow:DropShadowFilter = new DropShadowFilter(1, 90, 0, 0.8, 3, 3, 1, 3, true);
			
			trace("border height", borderGraphicsHeight);
			
			if (_filteredItems.length < 7)
				_spacesNeeded = 7;
			else
				_spacesNeeded = _filteredItems.length;
			
			if (_filteredItems.length > possibleAssetSpaces) {
				_spacesNeeded = possibleAssetSpaces;
				_scrollNeeded = true;
			} else
				_scrollNeeded = false;
			
			
			with (this.graphics){
				clear();
				
				makeGradient(this.graphics,175,TOP_MENU_HEIGHT);
				lineStyle(BORDER_THICKNESS, BORDER_COLOR,1, true);
				drawRoundRectComplex(0,0,175,(TOP_MENU_HEIGHT),0,10,0,0);
				endFill();
				
				beginFill(BACKGROUND_COLOR, BACKGROUND_ALPHA);
				lineStyle(BORDER_THICKNESS, BORDER_COLOR,1, true);
				drawRect(0,TOP_MENU_HEIGHT,175,(TOP_MENU_HEIGHT * (NUMBER_OF_TOP_MENUS - 1)));
				endFill();
				
				
				//left vertical line
				moveTo(0,TOP_MENU_HEIGHT * NUMBER_OF_TOP_MENUS);
				lineTo(0,EditorUI.instance.paletteHeight  - BOTTOM_PADDNG_HEIGHT);
				
				//bottom horizontal line
				moveTo(0,EditorUI.instance.paletteHeight  - BOTTOM_PADDNG_HEIGHT);
				lineTo(OVERALL_WIDTH, EditorUI.instance.paletteHeight  - BOTTOM_PADDNG_HEIGHT);
				
			}
//			this.filters = [gradientShadow];
			
			
			
			with (_topPanel.graphics){
				clear();
				beginFill(BACKGROUND_COLOR, BACKGROUND_ALPHA);
				lineStyle(BORDER_THICKNESS, BORDER_COLOR,1, true);
				endFill();
				
				//draw right vertical line (left of scrollarea)
				moveTo(BORDER_THICKNESS + VARIABLE_COLUMN_WIDTH + COUNT_COLUMN_WIDTH,0);
				lineTo(BORDER_THICKNESS + VARIABLE_COLUMN_WIDTH + COUNT_COLUMN_WIDTH, borderGraphicsHeight);
				
				//draw right vertical line (right of scrollarea)
				moveTo(OVERALL_WIDTH - BORDER_THICKNESS,0);
				lineTo(OVERALL_WIDTH - BORDER_THICKNESS, borderGraphicsHeight);
			}
			
			
		
			
			if (_scrollNeeded) {
				scrollRect = new Rectangle(0,0,OVERALL_WIDTH, RIBBON_HEIGHT * possibleAssetSpaces);
				_topPanel.scrollRect = scrollRect;
				
				if(_scrollModel && _scrollbar && contains(_scrollbar)){
					_scrollModel.viewPortLength = scrollRect.height;
					_scrollModel.setMinMaxScrollPosition(0, _filteredItems.length * RIBBON_HEIGHT);
					_scrollbar.scrollControllerHeight = RIBBON_HEIGHT * _spacesNeeded;
				}
				
				else{
					_scrollModel = new ScrollModel(scrollRect.height, 0, RIBBON_HEIGHT * _filteredItems.length);
					_scrollbar = new PaletteVerticalScrollView(_scrollModel, 13, RIBBON_HEIGHT * _spacesNeeded);
					 addChild(_scrollbar);
					_scrollbar.x = BORDER_THICKNESS + VARIABLE_COLUMN_WIDTH + COUNT_COLUMN_WIDTH;
					_scrollbar.y = _topPanel.y;
					_scrollbar.addEventListener(ScrollUpdateEvent.SCROLL, handleScroll);
					_scrollbar.scrollPosition = _scrollPosition;
				
				}
				
				_scrollbar.visible = true;
			}
			else{
				
				scrollRect = new Rectangle(0,0,OVERALL_WIDTH, _topPanelHeight);
				_topPanel.scrollRect = scrollRect;
				
				if(_scrollbar){
					_scrollbar.visible = false;
					_scrollbar.removeEventListener(ScrollUpdateEvent.SCROLL, handleScroll);
					if(contains(_scrollbar))
						removeChild(_scrollbar);
					}
				}
		}
		
		private function renderBottomPanel():void
		{
			var scrollRect:Rectangle;
			_bottomPanelScrollNeeded = _descriptionField.lastLine.y + 25 > _bottomPanelHeight?true:false;	
			
			with (_bottomPanel.graphics){
				clear();
				beginFill(BACKGROUND_COLOR, BACKGROUND_ALPHA);
				lineStyle(BORDER_THICKNESS, BORDER_COLOR,1,true);
				
				//vertical line (left of scroll area)
				//moveTo(BORDER_THICKNESS + VARIABLE_COLUMN_WIDTH + COUNT_COLUMN_WIDTH,0);
				//lineTo(BORDER_THICKNESS + VARIABLE_COLUMN_WIDTH + COUNT_COLUMN_WIDTH,_bottomPanelHeight - BOTTOM_PADDNG_HEIGHT);
				endFill();
				
				//vertical line (right of scroll area)
				moveTo(OVERALL_WIDTH - BORDER_THICKNESS,0);
				lineTo(OVERALL_WIDTH - BORDER_THICKNESS,_bottomPanelHeight);
				endFill();
			}
			
			
			if (_bottomPanelScrollNeeded) {
				var _bottomPanelScrollRect:Rectangle = new Rectangle(0,0,OVERALL_WIDTH, _bottomPanelHeight);
				_bottomPanel.scrollRect = _bottomPanelScrollRect;
				
				if(_bottomPanelScrollModel && _bottomPanelScrollbar && contains(_bottomPanelScrollbar)){
					_bottomPanelScrollModel.viewPortLength = _bottomPanelScrollRect.height;
					_bottomPanelScrollModel.setMinMaxScrollPosition(0, _descriptionField.lastLine.y + 25);
					_bottomPanelScrollbar.scrollControllerHeight = _bottomPanelHeight;
					_bottomPanelScrollbar.y = _bottomPanel.y;
				}
					
				else{
					_bottomPanelScrollModel = new ScrollModel(_bottomPanelScrollRect.height, 0,  _descriptionField.lastLine.y + 25);
					_bottomPanelScrollbar = new PaletteVerticalScrollView(_bottomPanelScrollModel, 13, _bottomPanelHeight);
					 addChild(_bottomPanelScrollbar);
					_bottomPanelScrollbar.x = BORDER_THICKNESS + VARIABLE_COLUMN_WIDTH + COUNT_COLUMN_WIDTH;
					_bottomPanelScrollbar.y = _bottomPanel.y;
					_bottomPanelScrollbar.addEventListener(ScrollUpdateEvent.SCROLL, handleBottomPanelScroll);
					_bottomPanelScrollbar.scrollPosition = _bottomPanelScrollPosition;
					
				}
				
				_bottomPanelScrollbar.visible = true;
			}
			else{
				scrollRect = new Rectangle(0,0,OVERALL_WIDTH, _bottomPanelHeight);
				_bottomPanel.scrollRect = scrollRect;
				
				if(_bottomPanelScrollbar){
					_bottomPanelScrollbar.visible = false;
					_bottomPanelScrollbar.removeEventListener(ScrollUpdateEvent.SCROLL, handleBottomPanelScroll);
					if(contains(_bottomPanelScrollbar))
						removeChild(_bottomPanelScrollbar);
				}
			}
		}
		
		private function handleScroll(event:ScrollUpdateEvent):void
		{
			if (_scrollbar) {
				var rect:Rectangle = _topPanel.scrollRect;
				
				if (event && event.params && (event.params.hasOwnProperty("scrollPosition"))) {
					rect.y = event.params.scrollPosition;  //now displaying infinity
					_topPanel.scrollRect = rect;
				}
			}
		}
		
		
		private function handleBottomPanelScroll(event:ScrollUpdateEvent):void
		{
			if (_bottomPanelScrollbar) {
				var rect:Rectangle = _bottomPanel.scrollRect;
				
				if (event && event.params && (event.params.hasOwnProperty("scrollPosition"))) {
					rect.y = event.params.scrollPosition; 
					_bottomPanel.scrollRect = rect;
				}
			}
		}


		private function filterCategories(sortField:SortField = null):void{
			_scrollPosition = 0;
			
			if (!sortField){
				sortField = new SortField();
				sortField.name = "name";
				sortField.descending = SORT_TYPE_ASCENDING;
			}
			
			_descriptionField.text = "";
			var category:String = _categoryFilterMenu.selectedItem.data as String;
			var type:String = "";
			
			switch(_categoryFilterMenu.selectedItem.data){
				
				case Ast.CALCULATOR_CATEGORY:
					makeTypeData(new Array( {label:"General", data:Ast.GENERAL_TYPE,icon:CategoryIcons.getIconFor(Ast.GENERAL_TYPE)}));
                    _categoryFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.CALCULATOR_CATEGORY, false));
					_typeFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.CALCULATOR_CATEGORY, false));
                    break;
                case Ast.TIPS_CATEGORY:
					makeTypeData(new Array( {label:"General", data:Ast.GENERAL_TYPE,icon:CategoryIcons.getIconFor(Ast.GENERAL_TYPE)}));
					_categoryFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.TIPS_CATEGORY, false));
					_typeFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.GENERAL_TYPE, false));
					break;
				
				case Ast.FUNCTION_CATEGORY:
					makeTypeData(new Array({label:"Math", data:Ast.MATH_TYPE, icon:CategoryIcons.getIconFor(Ast.MATH_TYPE)}, {label:"Character", data:Ast.STRING_TYPE,icon:CategoryIcons.getIconFor(Ast.STRING_TYPE)}, {label:"General", data:Ast.GENERAL_TYPE, icon:CategoryIcons.getIconFor(Ast.GENERAL_TYPE)}));
					_categoryFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.FUNCTION_CATEGORY, false));
					_typeFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.MATH_TYPE, false));
					break;
				case Ast.CONSTANT_CATEGORY:
					makeTypeData(new Array({label:"Math", data:Ast.MATH_TYPE, icon:CategoryIcons.getIconFor(Ast.MATH_TYPE)}));
					_categoryFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.CONSTANT_CATEGORY, false));
					_typeFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.MATH_TYPE, false));
					break;
				case Ast.VARIABLE_CATEGORY:
					makeTypeData(new Array({label:"General", data:Ast.GENERAL_TYPE,icon:CategoryIcons.getIconFor(Ast.GENERAL_TYPE)}));
					_categoryFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.VARIABLE_CATEGORY, false));
					_typeFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.GENERAL_TYPE, false));
					break;
				case Ast.OPERATOR_CATEGORY:
					makeTypeData(new Array({label:"General", data:Ast.GENERAL_TYPE,icon:CategoryIcons.getIconFor(Ast.GENERAL_TYPE)}));
					_categoryFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.OPERATOR_CATEGORY, false));
					_typeFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.GENERAL_TYPE, false));
					break;
				case Ast.KEYPRESS_CATEGORY:
					makeTypeData(new Array({label:"Keys", data:Ast.KEY_TYPE,icon:CategoryIcons.getIconFor(Ast.KEY_TYPE)},{label:"Modifiers", data:Ast.MODIFIER_TYPE,icon:CategoryIcons.getIconFor(Ast.MODIFIER_TYPE)}));
					_categoryFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.KEYPRESS_CATEGORY, false));
					_typeFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.KEYPRESS_CATEGORY, false));
					break;
			}
			
			addFilteredItems(sortField);
		}
		
		private function filterType(sortField:SortField = null):void{
			_scrollPosition = 0;
			
			if (!sortField){
				sortField = new SortField();
				sortField.name = "name";
				sortField.descending = SORT_TYPE_ASCENDING;
			}
			
			_descriptionField.text = "";
			var category:String = "";
			var type:String = "";
			
			
			switch(_typeFilterMenu.selectedItem.data){
				
				case "math":
					type = Ast.MATH_TYPE;
					_typeFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.MATH_TYPE, false));
					break;
				
				case "string":
					type = Ast.STRING_TYPE;
					_typeFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.STRING_TYPE, false));
					break;
				default:
					type = Ast.GENERAL_TYPE;
					_typeFilterMenu.setBaseIcon(CategoryIcons.getIconFor(Ast.GENERAL_TYPE, false));
					break;
			}
			
			addFilteredItems(sortField);
		}
		
		
		private function addFilteredItems(sortField:SortField = null):void{
			
			///add category and type
			var category:String  = _categoryFilterMenu.selectedItem.data as String;
			var type:String  = _typeFilterMenu.selectedItem.data as String;
			
			var sort:Sort = new Sort();
			sort.fields = [sortField];
			_filteredItems.removeAll();
			
			for (var property:String in Ast.defaultHelpEnvironment()){
				
				var obj:Object = Ast.defaultHelpEnvironment()[property];
				
				if(obj.category == category && obj.type == type){
					var listItem:IListable  = new HelpItem(property, obj.hasOwnProperty("label")?obj.label:property);
					_filteredItems.addItem({item:listItem, name:property});
				}
			} 
			//	_typeFilterMenu.updateSelectedItemByData(_typeFilterMenu.selectedItem.data);
			
			_filteredItems.sort = sort;
			_filteredItems.refresh();
			
			updateList();
		}
		
		
		private function get categoryData():Array
		{
			var newArray:Array = [];
		
			newArray.push({label:"Calculator", 			data:Ast.CALCULATOR_CATEGORY, icon:CategoryIcons.getIconFor(Ast.KEYPRESS_CATEGORY)});
            newArray.push({label:"System Functions", 	data:Ast.FUNCTION_CATEGORY,	icon:CategoryIcons.getIconFor(Ast.FUNCTION_CATEGORY)});
			newArray.push({label:"System Variables", 	data:Ast.VARIABLE_CATEGORY,	icon:CategoryIcons.getIconFor(Ast.VARIABLE_CATEGORY)});
			newArray.push({label:"System Constants", 	data:Ast.CONSTANT_CATEGORY,	icon:CategoryIcons.getIconFor(Ast.CONSTANT_CATEGORY)});
			newArray.push({label:"System Operators", 	data:Ast.OPERATOR_CATEGORY,	icon:CategoryIcons.getIconFor(Ast.OPERATOR_CATEGORY)});
			newArray.push({label:"Key Constants",		data:Ast.KEYPRESS_CATEGORY,	icon:CategoryIcons.getIconFor(Ast.KEYPRESS_CATEGORY)});
			newArray.push({label:"Tips", 				data:Ast.TIPS_CATEGORY,	icon:CategoryIcons.getIconFor(Ast.TIPS_CATEGORY)});
			
			return newArray;
		}
		
		private function get typeData():Array
		{
			var newArray:Array = [];
			newArray.push({label:"Math", 	data:"math",	icon:CategoryIcons.getIconFor(Ast.MATH_TYPE)});
			newArray.push({label:"Character",data:"string",	icon:CategoryIcons.getIconFor(Ast.STRING_TYPE)});
			newArray.push({label:"General", data:"general",	icon:CategoryIcons.getIconFor(Ast.GENERAL_TYPE)});
			return newArray;
		}
		
		private function makeTypeData(choices:Array):void{
			
			var newArray:Array = [];
			for each(var choice:Object in choices){
				newArray.push({label:choice.label, data:choice.data, icon:choice.icon});
			}
			
			_typeFilterMenu.updateDataProvider(newArray);
		}

		private function handleItemClick(event:Event):void{
			
			if(event.currentTarget as VariableItemView){
				_descriptionField.text = "";
				var property:String  = HelpItem((event.currentTarget as VariableItemView).item).property;
				
				if(_previousSelected)
					_previousSelected.selected = false;
				
				(event.currentTarget as VariableItemView).scrollNeeded = _scrollNeeded;
				(event.currentTarget as VariableItemView).selected = true;
				_previousSelected = (event.currentTarget as VariableItemView);
				
				var object:Object = Ast.defaultHelpEnvironment()[property];
				if(object.hasOwnProperty("description") && object.description != ""){
					_descriptionField.text = object.description;
				} else{
                    _descriptionField.text = "Description not available";
                }
				
				addEventListener(Event.ENTER_FRAME, redrawBottomPanel);

			}
		}
		
		private function redrawBottomPanel(event:Event):void{
			removeEventListener(Event.ENTER_FRAME, redrawBottomPanel);
			renderBottomPanel();
		}
		
		private function dimensionPanel(panel:Sprite, width:Number, height:Number, color:uint, alpha:Number = .03):void{
			
			with(panel.graphics){
				clear();
				beginFill(color,alpha);
				drawRect(0,0,width,height);
				endFill();
			}
			
		}
		
		private function mouseDownListener(event:MouseEvent):void{
			
			
			var minimumPanelHeight:Number = Math.max(MINIMUM_PANEL_HEIGHT, _descriptionField.lastLine.y + 25);  //to do: eliminate this
			minimumPanelHeight = MINIMUM_PANEL_HEIGHT; 
			var _startY:Number = TOP_MENU_HEIGHT * NUMBER_OF_TOP_MENUS + MINIMUM_PANEL_HEIGHT;
			var _endY:Number = (EditorUI.instance.paletteHeight - BOTTOM_PADDNG_HEIGHT -  minimumPanelHeight) - _startY;
			var _dragConstraint:Rectangle = new Rectangle(0,_startY,0,_endY);
			_divider.removeEventListener(MouseEvent.MOUSE_DOWN,mouseDownListener);
			stage.addEventListener(MouseEvent.MOUSE_UP,mouseUpListener);
			_divider.startDrag(true,_dragConstraint);
			addEventListener(Event.ENTER_FRAME, frameEventListener);
		}
		
		private function mouseUpListener(event:MouseEvent):void{
			_divider.stopDrag();
			stage.removeEventListener(MouseEvent.MOUSE_UP,mouseUpListener);
			_divider.addEventListener(MouseEvent.MOUSE_DOWN,mouseDownListener);
			removeEventListener(Event.ENTER_FRAME, frameEventListener);
		}
		
		
		private function frameEventListener(event:Event):void{
			
			resizeTopPanel();
			resizeBottomPanel();
			renderTopPanel();
			renderBottomPanel();
		}
		
		private function resizeTopPanel():void{
			
			_topPanel.y = TOP_MENU_HEIGHT * NUMBER_OF_TOP_MENUS + 1;
			_topPanelHeight = _divider.y - _topPanel.y;
			dimensionPanel(_topPanel,_topPanel.width,_topPanelHeight,0xFF0000);
		}
		
		private function resizeBottomPanel():void{
			
			_bottomPanel.y = _divider.y + _divider.height;
			_bottomPanelHeight = EditorUI.instance.paletteHeight - BOTTOM_PADDNG_HEIGHT - _bottomPanel.y; 
			dimensionPanel(_bottomPanel,_bottomPanel.width,_bottomPanelHeight,0x0000FF);
		}
		
		private function makeGradient(g:Graphics, w:Number = OVERALL_WIDTH, h:Number = RIBBON_HEIGHT):void{
			
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [0x46638A, 0x000000];
			var alphas:Array = [1, 1];
			var ratios:Array = [0x00, 0xFF];
			var matr:Matrix = new Matrix();
			matr.createGradientBox(w, h, Math.PI/2);
			var spreadMethod:String = SpreadMethod.PAD;
			g.beginGradientFill(fillType, colors, alphas, ratios, matr);  
		}
		
		public function setVisible(value:Boolean):void{
			this.visible = value;
			
			if(_typeFilterMenu)
				_typeFilterMenu.visible = value;
			
			if(_categoryFilterMenu)
				_categoryFilterMenu.visible = value;
			
		}
		
		
	}
}

import com.alleni.author.model.objects.IListable;
import com.alleni.taconite.model.TaconiteModel;

class HelpItem implements IListable{
	private var _property:String = "";
	private var _name:String = "";
	
	public function HelpItem(property:String, name:String){
        _property = property;
		_name = name;
	}
	
	public function get name():String{
		return _name;
	}
	public function set name(value:String):void{
		_name = value;
	}
    public function get property():String {
        return _property;
    }
	public function get id():String{return "0";}
	public function get type():int{return 0;}
	public function get content():Object{return null;}
	public function get count():uint{return 0;}
	public function set count(value:uint):void{}
	public function get model():TaconiteModel{return null;}
}

import assets.icons.object.*;

import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.*;
import com.alleni.taconite.document.ObjectSelection;
import com.alleni.savana.Ast;
import assets.icons.variableLibrary.*;


import flash.display.DisplayObject;

class CategoryIcons
{
		static public function getIconFor(categoryOrType:String = null, isDark:Boolean = true):DisplayObject
		{
				if(isDark){
					switch (categoryOrType) {
						case Ast.TIPS_CATEGORY:
							return new TipsBlack();
							break;
						case Ast.FUNCTION_CATEGORY:
							return new FunctionsBlack();
							break;
						case Ast.CONSTANT_CATEGORY:
							return new ConstantsBlack();
							break;
						case Ast.VARIABLE_CATEGORY:
							return new VariablesBlack();
							break;
						case Ast.OPERATOR_CATEGORY:
							return new OperatorsBlack();
							break;
						case Ast.KEYPRESS_CATEGORY:
							return null;
							break;
                        case Ast.CALCULATOR_CATEGORY:
                            return null;
						case Ast.KEY_TYPE:
							return null;
							break;
						case Ast.MODIFIER_TYPE:
							return null;
							break;
						case Ast.MATH_TYPE:
							return new MathBlack();
							break;
						case Ast.STRING_TYPE:
							return new CharacterBlack();
							break;
						case Ast.GENERAL_TYPE:
							return new GeneralBlack();
							break;
						
						
					}
				}
					else{
						
						switch (categoryOrType) {
							case Ast.TIPS_CATEGORY:
								return new TipsWhite();
								break;
							case Ast.FUNCTION_CATEGORY:
								return new FunctionsWhite();
								break;
							case Ast.CONSTANT_CATEGORY:
								return new ConstantsWhite();
								break;
							case Ast.VARIABLE_CATEGORY:
								return new VariablesWhite();
								break;
							case Ast.OPERATOR_CATEGORY:
								return new OperatorsWhite();
								break;
							case Ast.KEYPRESS_CATEGORY:
								return null;
								break;
							case Ast.KEY_TYPE:
								return null;
								break;
							case Ast.MODIFIER_TYPE:
								return null;
								break;
							case Ast.MATH_TYPE:
								return new MathWhite();
								break;
							case Ast.STRING_TYPE:
								return new CharacterWhite();
								break;
							case Ast.GENERAL_TYPE:
								return new GeneralWhite();
								break;
						}//switch
						
					}//else
			
			return null;
		}//function
}//class
	
	