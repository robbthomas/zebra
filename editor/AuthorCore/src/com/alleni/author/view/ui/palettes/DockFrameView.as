package com.alleni.author.view.ui.palettes
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	
	public class DockFrameView extends Sprite
	{
		// client code should set these variables, then call draw() to redraw at new size
		public var frameWidth:Number = 800;
		public var frameHeight:Number = 100;
		public var minHeight:Number = 100;
		public var maxHeight:Number = 500;
		
		/*
		Tables are used to provide random-looking bumps as needed to reach the width and height of the cloud.
		The shape is drawn in a clockwise direction by a series of curveTo commands.
		After the shape is drawn and endFill() is done, then the stub lines are drawn as needed at the vertexes.
		
		The top and bottom bumps are stretched in order to fill the space with bumps from the table.
		The sides are not stretched:  a single variable-length bump is drawn at the bottom to take up the slack.
		*/
		
		private static const MIN_WIDTH:Number = 450;
		private static const HEADROOM:Number = 25;  // height of upper area, for dock controls
		private static const BOTTOM_SPREAD:Number = 20;  // extra width of bottom, causes sides to slope outward
		private static const MIN_BUMP:Number = 40;  // minimum bump size when adding optional bumps
		
		public static const X_OFFSET:Number = BOTTOM_SPREAD+12;  // offset from outside leftmost point, to the X position of the view
		public static const Y_OFFSET:Number = 15;
		
		private static const LINE_COLOR:uint = 0x98aabe;
		private static const FILL_COLOR:uint = 0xffffff;
		private static const DEFAULT_ALPHA:Number = 0.9;
		
		private static const GRAB_COLOR:uint = 0x9ca8be;
		
		private static const GLOW_COLOR:uint = 0xa5c9ff;
		private static const GLOW_ALPHA:Number = 0.75;
		private static const GLOW_LEN:Number = 30;
		private static const GLOW_STRENGTH:Number = 0.9;
		
		private static const SHADOW_LEN:Number = 2.0;
		private static const SHADOW_ANGLE:Number = 80.0;
		private static const SHADOW_COLOR:Number = 0x000000;
		private static const SHADOW_ALPHA:Number = 0.8;
		private static const SHADOW_BLUR_X:Number = 10.0;
		private static const SHADOW_BLUR_Y:Number = 10.0;
		private static const SHADOW_STRENGTH:Number = 0.7;
		
		private static const STUB_LEN:Number = 8;  // stubs are lines at end of curves
		
		private static const STUBNONE:uint = 0;
		private static const STUBSTART:uint = 1;  // these bits can be combined:  STUBSTART | STUBEND
		private static const STUBEND:uint = 2;
		
		// indexes values for internal arrays of bump data
		private static const DELTA:int = 0;		// dx or dy, depending on which way the bumps are going
		private static const CONTROL_X:int = 1;  // control-point to stretch the curve (offsets from center of line)
		private static const CONTROL_Y:int = 2;
		private static const STUB:int = 3;  // code such as STUBSTART, indicating line to be drawn from vertex of curve
		
		
		
		private var _stubLines:Array;
		private var _grab:Sprite;
		private var _clickY:Number; // location of click in stage coordinates
		private var _clickHeight:Number;
		
		
		public function DockFrameView()
		{
			super();
			
			/* not currently using the grabber to resize this view.
			_grab = new Sprite();
			drawGrab(_grab.graphics);
			addChild(_grab);
			_grab.addEventListener(MouseEvent.MOUSE_DOWN, handleGrabMouseDown);*/
		}
		
		public function draw():void
		{
			// this used to adjust www and hhh to be something less than frameWidth/frameHeight. This is not really useful any longer as
			// we want the containing view to take care of the dimensions, with direct control over what those dimensions should be
			// down to the pixel.
			var www:Number = frameWidth;
			var hhh:Number = frameHeight;
			
			if (hhh < minHeight)
				hhh = minHeight;
			
			if (www < MIN_WIDTH)
				www = MIN_WIDTH;
			
			var beginning:Point = new Point(77,0);
			var start:Point = beginning.clone();
			_stubLines = [];
			
			graphics.clear();
			graphics.lineStyle(1,LINE_COLOR);
			graphics.beginFill(FILL_COLOR);
			graphics.moveTo(start.x, start.y);
			
			drawTopShape(start,www);
			drawRightSide(start,www,hhh);
			drawBottomShape(start,www);
			drawLeftSide(start,www,hhh,beginning);
			
			graphics.endFill();

			drawStubLines();
			
			// position the grab bars at bottom center of cloud
			/*_grab.x = www/2;
			_grab.y = hhh + 5;*/
			
			var innerGlow:GlowFilter = new GlowFilter(GLOW_COLOR, GLOW_ALPHA, GLOW_LEN, GLOW_LEN, GLOW_STRENGTH, 2, true); 
			
			var dropShadow:DropShadowFilter = new DropShadowFilter(SHADOW_LEN, SHADOW_ANGLE, SHADOW_COLOR, SHADOW_ALPHA,
				SHADOW_BLUR_X, SHADOW_BLUR_Y, SHADOW_STRENGTH, 3);
			
			filters = [innerGlow, dropShadow];
			
			this.alpha = DEFAULT_ALPHA;
		}

		private function drawTopShape(start:Point, www:Number):void
		{
			var data:Array = [
				[73, -4, -20, STUBEND],  // bump dx, controlX, controlY, stubEnd  (controlX &Y are relative to center of bump)
				[58, -4, -20, STUBEND],
				[86, -4, -25, STUBEND],
				[55, -4, -25, STUBEND],
				[107, -4, -27, STUBNONE],
				[87, -4, -25, STUBSTART],
				[66, -4, -23, STUBSTART],
				[76, -4, -20, STUBSTART],
				[45, -4, -22, STUBSTART],
				[66, -4, -20, STUBSTART],
			];
			
			drawHorzBumpSpread(data, start, www-50);
		}		
		
		private function drawBottomShape(start:Point, www:Number):void
		{
			var rightData:Array = [
				[-50, 0, 10, STUBEND],  // dx, controlX, controlY, stubEnd
				[-80, 0, 19, STUBEND],
				[-84, 0, 16, STUBEND],
				[-97, 0, 13, STUBEND],  
				[-111, 0, 18, STUBEND],
				[-135, 0, 16, STUBEND],
				[-60, 0, 14, STUBEND],
			];
			var leftData:Array = [
				[-95, 0, 15, STUBSTART],
				[-53, 0, 10, STUBSTART],
				[-94, 0, 16, STUBSTART],
				[-76, 0, 13, STUBSTART],  
				[-119, 0, 18, STUBSTART],
				[-125, 0, 16, STUBSTART],
				[-77, 0, 14, STUBSTART],
			];
			
			// position the center bump, which goes around the grab handle
			const CENTER_WIDTH:Number = 138;
			var centerStartX:Number = www/2 + CENTER_WIDTH/2;
			
			// since we are going clockwise, the right side is first
			drawHorzBumpSpread(rightData, start, centerStartX);
			
			// center bump
			drawHorzBump(start, -CENTER_WIDTH, 0, 20, STUBNONE);
			
			// left half of bottom
			drawHorzBumpSpread(leftData, start, 30);
		}
		
		private function drawHorzBumpSpread(data:Array, start:Point, destX:Number):void
		{
			// decide how many bumps we are going to use
			var requestedDeltaX:Number = destX - start.x;
			var bump:Array, n:int;
			var dx:Number;
			var bumpsToUse:int = 0;
			var deltaX:Number = 0;  // total of all bumps
			var testX:Number = start.x;
			for (n=0; n < data.length; n++) {
				bump = data[n];
				dx = bump[DELTA];
				
				// do we have enough bumps to fill the space?
				if (n > 0) {
					if (requestedDeltaX > 0 && deltaX+MIN_BUMP > requestedDeltaX)  // quit if we are close to the dest
						break;
					if (requestedDeltaX < 0 && deltaX-MIN_BUMP < requestedDeltaX) 
						break;
				}
				testX += dx;
				deltaX += dx; 
				++bumpsToUse;
			}
			
			// scale the bumps to fill the space
			var bumpScale:Number = requestedDeltaX / deltaX;
			
			// actually draw the bumps
			for (n=0; n < bumpsToUse; n++) {
				bump = data[n];
				dx = bump[DELTA] * bumpScale;
				drawHorzBump(start, dx, bump[CONTROL_X], bump[CONTROL_Y], bump[STUB]);
			}
		}
		
		private function drawHorzBump(start:Point, bumpWidth:Number, controlX:Number, controlY:Number, stubLoc:uint):void
		{
			drawBump(start, start.x + bumpWidth/2 + controlX, start.y + controlY, start.x + bumpWidth, start.y, stubLoc);
		}
		
		private function drawRightSide(start:Point, www:Number, hhh:Number):void
		{
			var data:Array = [
				[44, 20, 0],  // dy, controlX, controlY
				[88, 20, 0],
				[66, 25, 0],
				[77, 30, 0], 
				[88, 27, 0],
				[66, 25, 0],
				[55, 23, 0],
				[66, 25, 0],
				[77, 30, 0], 
				[88, 27, 0],
				[66, 25, 0],
				[55, 23, 0],
				[66, 25, 0],
				[77, 30, 0], 
				[88, 27, 0],
				[66, 25, 0],
				[55, 23, 0],
			];
			
			
			// top right corner
			drawSideBump(start, 50, HEADROOM, 22, -25, STUBSTART);
			
			// the nose is the big curve in the bottom right corner
			var noseX:Number = www + BOTTOM_SPREAD;  // flare out the bottom so the side will slope outward
			var noseY:Number = hhh - 39;
			
			// draw variable number of bumps
			drawRightSideBumpsDownward(data, start, noseX, noseY);
			
			// extensible side bump: from extensible part to the nose
			drawSideBump(start, noseX - start.x, noseY - start.y, 22, -15, STUBSTART);
			
			// bottom right corner
			drawSideBump(start, -64, 38, 50, 35, STUBSTART|STUBEND);
		}
		
		private function drawRightSideBumpsDownward(data:Array, start:Point, destX:Number, destY:Number):void
		{
			// slope of line from the start to the dest
			var slope:Number = (destX - start.x) / (destY - start.y);  // inverse slope, so vertical would be zero
			//			trace("drawBumpsFromPoint slope="+slope);
			
			for each (var bump:Array in data) {
				var dy:Number = bump[DELTA];
				var dx:Number = dy * slope;
				//				trace("   n="+data.indexOf(bump), "len="+len, "dx="+dx, "dy="+dy);
				
				if (start.y+dy+MIN_BUMP > destY)  // quit if we are close to the destination Y
					break;
				
				drawSideBump(start, dx, dy, bump[CONTROL_X],bump[CONTROL_Y], STUBSTART);
			}
		}
		
		private function drawLeftSide(start:Point, www:Number, hhh:Number, end:Point):void
		{
			// these bumps are in top-down order
			var data:Array = [
				[-44, -20, 0],  // dy, stretchX, stretchY
				[-66, -20, 0],
				[-88, -25, 0],
				[-77, -30, 0],
				[-66, -27, 0],
				[-88, -25, 0],
				[-55, -23, 0],
				[-88, -25, 0],
				[-77, -30, 0],
				[-66, -27, 0],
				[-88, -25, 0],
				[-55, -23, 0],
				[-88, -25, 0],
				[-77, -30, 0],
				[-66, -27, 0],
				[-88, -25, 0],
				[-55, -23, 0],
			];
			
			// lower left corner
			drawSideBump(start, -BOTTOM_SPREAD - start.x, -44, -60, 40, STUBSTART|STUBEND);
			
			// the nose is the big curve in the upper left corner (and we are traveling upward here)
			var noseX:Number = -4; 
			var noseY:Number = 25;
			
			// this function draws extra bumps ... AND the extensible bump at the bottom left
			drawLeftSideBumpsUpward(data, start, noseX,noseY, -23, -10);
			
			// upper left corner:  this ends at the beginning of whole shape, to complete it
			// the dx & dy of this bump are determined by the total dx,dy of all the other bumps
			drawSideBump(start, end.x - start.x, end.y - start.y, -25, -20, STUBEND);
		}
		
		/**
		 * Draw variable number of bumps, reaching to destX, destY.  (the line from start to destX,Y is sloping a little)
		 * Includes drawing a single variable-length bump before the ones from the table, to take up the slack.
		 * @param data
		 * @param start
		 * @param destX
		 * @param destY
		 * @param exStretchX stretching of the single variable-length bump.
		 * @param exStretchY
		 * 
		 */
		private function drawLeftSideBumpsUpward(data:Array, start:Point, destX:Number, destY:Number, exStretchX:Number, exStretchY:Number):void
		{
			// slope of line from the start to the dest
			var beginningX:Number = start.x;
			var slope:Number = (destX - start.x) / (destY - start.y);  // inverse slope, so vertical is zero
			//			trace("drawBumpsToPoint: slope="+slope);
			
			// decide how many bumps we are going to use
			// scan from top down, so that new bumps will appear to be added at the bottom
			// accumulate total width & height of the extra bumps, in order to keep top end from shifting
			var bump:Array, n:int;
			var dx:Number, dy:Number;
			var bumpsToUse:int = 0;
			var deltaX:Number = 0, deltaY:Number = 0;  // total of all bumps
			var testY:Number = start.y;
			for (n=0; n < data.length; n++) {
				bump = data[n];
				
				dy = bump[DELTA];
				dx = dy * slope;
				//				trace("   n="+n, "len="+len, "dx="+dx, "dy="+dy);
				
				if (testY + dy - MIN_BUMP < destY)  // quit if we are close to the destY
					break;
				testY += dy;
				++bumpsToUse;
				deltaX += dx; 
				deltaY += dy;
			}
			
			// extensible bump:  from starting position to start of extra bumps
			dy = destY - start.y - deltaY;  // negative number, reduced by height of extra bumps
			dx = destX - start.x - deltaX;
			drawSideBump(start, dx, dy, exStretchX, exStretchY, STUBEND);
			
			// draw bumps starting at bottom, since we are circling the shape in clockwise direction
			for (n=bumpsToUse-1; n >= 0; n--) {
				bump = data[n];
				dy = bump[DELTA];
				dx = dy * slope;
				//				trace("   n="+n, "len="+len, "dx="+dx, "dy="+dy);
				drawSideBump(start, dx, dy, bump[CONTROL_X],bump[CONTROL_Y], STUBEND);
			}
		}
		
		private function drawSideBump(start:Point, dx:Number, dy:Number, controlDX:Number, controlDY:Number, stubLoc:uint):void
		{
			var controlX:Number = start.x + dx/2 + controlDX;
			var controlY:Number = start.y + dy/2 + controlDY;
			drawBump(start, controlX, controlY, start.x + dx, start.y + dy, stubLoc);
		}
		
		private function drawBump(start:Point, controlX:Number, controlY:Number, endX:Number, endY:Number, stubLoc:uint):void
		{
			graphics.curveTo(controlX, controlY, endX, endY);
			//trace("start.x = "+start.x);
			
			// record location of stub line
			var stub:Object;
			var slope:Number;
			var dx:Number;
			var dy:Number;
			if (stubLoc & STUBEND) {
				slope = (endY - controlY) / (endX - controlX);
				dx = Math.sqrt(STUB_LEN*STUB_LEN / (slope*slope +1));
				if (endX < controlX)
					dx = -dx;
				dy = dx * slope;
				stub = {startX:endX, startY:endY, endX:endX+dx, endY:endY+dy};
				_stubLines.push(stub);
			}
			if (stubLoc & STUBSTART) {
				slope = (start.y - controlY) / (start.x - controlX);
				dx = Math.sqrt(STUB_LEN*STUB_LEN / (slope*slope +1));
				if (start.x < controlX)
					dx = -dx;
				dy = dx * slope;
				stub = {startX:start.x, startY:start.y, endX:start.x+dx, endY:start.y+dy};
				_stubLines.push(stub);
			}
			
			// advance to start of next bump
			start.x = endX;
			start.y = endY;
		}
		
		private function drawStubLines():void
		{
			//			graphics.lineStyle(3,0xff);  // debug: show the stubs in thick blue
			for each (var stub:Object in _stubLines) {
				var startX:Number = stub.startX;
				var startY:Number = stub.startY;
				var endX:Number = stub.endX;
				var endY:Number = stub.endY;
				graphics.moveTo(startX,startY);
				graphics.lineTo(endX,endY);
			}
		}
		
		private function drawGrab(graphics:Graphics):void
		{
			// the graphic is horizontally centered around x=0
			// vertically starts at y=0 and goes downward
			
			graphics.beginFill(0xff,0);  // filled invisible rect to catch mouse clicks
			graphics.drawRect(-15,0,30,25);
			graphics.endFill();
			graphics.lineStyle(1,GRAB_COLOR);
			graphics.moveTo(-15, 0);
			graphics.lineTo(15, 0);
			
			graphics.moveTo(-10, 5);
			graphics.lineTo(10, 5);
			
			graphics.moveTo(-5, 10);
			graphics.lineTo(5, 10);
		}
		
		private function handleGrabMouseDown(event:MouseEvent):void
		{
			_clickY = event.stageY;
			_clickHeight = frameHeight;
			stage.addEventListener(MouseEvent.MOUSE_MOVE, handleGrabMouseMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, handleGrabMouseUp);
		}
		
		private function handleGrabMouseMove(event:MouseEvent):void
		{
			var hhh:Number = _clickHeight + event.stageY - _clickY;
			if (hhh >= minHeight && hhh <= maxHeight) {  // don't store a height less than min, else next click will work funny
				frameHeight = hhh;
				draw();
			}
		}
		
		private function handleGrabMouseUp(event:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleGrabMouseMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, handleGrabMouseUp);
		}
	}
}