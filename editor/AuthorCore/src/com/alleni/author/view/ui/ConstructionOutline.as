package com.alleni.author.view.ui
{
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.Sprite;
	
	public class ConstructionOutline extends Sprite
	{
		[Embed(source="/assets/images/stripe.png",compression="true",quality="90")]
		private static var Stripes:Class;
		
		public function ConstructionOutline(w:Number, h:Number, thickness:Number=8)
		{
			super();
			
			var adjustment:Number = 1 + thickness/2;
			w += 2*adjustment;
			h += 2*adjustment;
			
			var stripesAsset:Bitmap = Bitmap(new Stripes());
			
			var mask:Shape = new Shape();
			mask.graphics.lineStyle(thickness, 0xff00ff00, 1);
			mask.graphics.drawRect(0,0,w,h);
			mask.graphics.endFill();
			mask.cacheAsBitmap = true;
			
			var tape:Shape = new Shape();
			tape.graphics.lineStyle(1, 0, 0.5);
			tape.graphics.beginBitmapFill(stripesAsset.bitmapData);
			tape.graphics.drawRect(0,0,w,h);
			tape.graphics.endFill();
			tape.cacheAsBitmap = true;
			tape.mask = mask;
			addChild(tape);
			addChild(mask);
			
			mask.x = -adjustment;
			mask.y = -adjustment;
			tape.x = -adjustment;
			tape.y = -adjustment;
		}
	}
}