package com.alleni.author.view.ui
{
	import com.alleni.author.controller.app.CursorController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.CursorNames;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.GraphicFillView;
	import com.alleni.author.view.ui.controls.GraphicFillPanel;
	import com.alleni.taconite.model.Notification;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.core.UIComponent;
	import mx.events.PropertyChangeEvent;

	public class RibbonFillPicker extends AbstractRibbonControl
	{
		private var _appContainer:UIComponent;
		private var _chipSize:Object = {width:35, height:12};   // RibbonView sets picker.width
		
		private var _outlineColor:uint = 0x646464;
		
		private var _valueCallback:Function;  // function called when fill changes, to give new value to client code
		private var _openPanelCallback:Function;  // when panel opens, we call this function to allow ribbon to hide its tooltip
		
		private var _fillView:GraphicFillView;
		
		public var fillSpec:GraphicFill;
		
		private var _hostObject:AbstractObject;
				
		
		public function RibbonFillPicker(appContainer:UIComponent, initialValue:GraphicFill, valueCallback:Function, openPanelCallback:Function, obj:AbstractObject,  w:Number=35, h:Number=12)
		{
			super(valueCallback);
			
			_appContainer = appContainer;
			_valueCallback = valueCallback;
			_openPanelCallback = openPanelCallback;
			fillSpec = (initialValue==null) ? new GraphicFill() : initialValue;
			_hostObject = obj;
			
			_chipSize.width = w;
			_chipSize.height = h - 1;
			
			_fillView = new GraphicFillView();
			addChild(_fillView);

			draw();
			
			this.addEventListener(MouseEvent.CLICK, mouseClickListener);
			this.addEventListener(MouseEvent.MOUSE_OVER, mouseOverListener);
			this.addEventListener(MouseEvent.MOUSE_OUT, mouseOutListener);
			
			ApplicationController.instance.addEventListener(NotificationNamesApplication.OPEN_PROJECT, closeEditing);
			ApplicationController.instance.addEventListener(NotificationNamesApplication.SAVE_AS_PROJECT, closeEditing);
			ApplicationController.instance.addEventListener(NotificationNamesApplication.SAVE_PROJECT, closeEditing);
			ApplicationController.instance.addEventListener(NotificationNamesApplication.CREATE_NEW_PROJECT, closeEditing);
		}


		override public function setValue(value:Object, valueType:String):void {
			var fill:GraphicFill = value as GraphicFill;
			if(fill == null) {
				fill = new GraphicFill();
			}
			setFillSpec(fill);
		}

		public function draw():void
		{	
			graphics.clear();
			graphics.lineStyle(1, _outlineColor);
			graphics.drawRect(0,0,_chipSize.width,_chipSize.height);
			
			_fillView.x = 1;
			_fillView.y = 1;
			_fillView.update(fillSpec, _chipSize.width-2, _chipSize.height-2);
		}
		
		
		// Highligh/unhighlight the color chip
		private function mouseOverListener(e:MouseEvent):void
		{
//			_outlineColor = 0xFFFFFF;
			draw();
		}
		
		private function mouseOutListener(e:MouseEvent):void
		{			
			/*_outlineColor = 0x000000;*/
			draw();	
		}
		
		/**
		 * Adds a Flex ColorPicker component to the feedback layer, positioning it under the ribbon parent
		 * of this RibbonFillPicker object. 
		 * @param e
		 * 
		 */
		private function mouseClickListener(e:MouseEvent):void
		{	
			openEditing();
		}
		
		private function valueCallback(updatedFillSpec:GraphicFill):void
		{
			// panel settings have changed, so we have a new fill
			fillSpec = updatedFillSpec;
			draw();
			_valueCallback(fillSpec);
		}

		override public function openEditing():void {
			var local:Point = new Point(width/2, height/2);
			var global:Point = localToGlobal(local);
			var obj:AbstractObject = _hostObject ? _hostObject : firstSelectedObj();
			GraphicFillPanel.showPanel(_appContainer, fillSpec, valueCallback, global,obj);

			if (_openPanelCallback != null)
				_openPanelCallback();  // let the ribbon view disable tooltips, so they don't cover our panel
		}

		override public function closeEditing(e:Event):void
		{
			// called when the ribbonView.visible is set false, when properties container is closed
			// ... to prevent leaving orphan panel
			GraphicFillPanel.closePanel();
		}
		
		
		override public function set height(value:Number) : void
		{
			//_chipSize.height = value;
			draw();
		}
		
		override public function set width(value:Number) : void
		{
			//_chipSize.width = value;
			draw();
		}
		
		
		public function setFillSpec(value:GraphicFill):void
		{
			// called by the RibbonFillSlider while sliding the color knob in the ribbon
			// also helps inspector ribbon to see changes made in MC ribbon
			fillSpec = value;
			draw();
		}
		
		private function firstSelectedObj():AbstractObject
		{
			if (Application.instance.document.selection && !Application.instance.document.selection.empty) {
				var model:TaconiteModel = Application.instance.document.selection.selectedModels[0];
				return model.value as AbstractObject;
			}
			return null;
		}
	}
}