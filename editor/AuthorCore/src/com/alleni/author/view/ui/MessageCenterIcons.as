/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.view.ui
{
	import assets.icons.messageCenter.*;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
		
	public class MessageCenterIcons
	{
		private static const SIZE:int = 13;
		

		public static function lockPosition(locked:Boolean):DisplayObject
		{
			if (locked)
				return new lockPositionWhite();
			else
				return withClickRegion(new xyrWhiteTrans());
		}
		
		public static function lockWires(locked:Boolean):DisplayObject
		{
			if (locked)
				return new lockWiresWhite();
			else
				return withClickRegion(new wiresWhiteTrans());
		}
		
		private static function withClickRegion(icon:DisplayObject):DisplayObject
		{
			var sprite:Sprite = new Sprite();
			sprite.graphics.beginFill(0, 0);
			sprite.graphics.drawRect(0, 0, SIZE, SIZE);
			sprite.graphics.endFill();
			sprite.addChild(icon);
			icon.x = (sprite.width - icon.width)/2;
			icon.y = (sprite.height - icon.height)/2;
			return sprite;
		}
	}
}