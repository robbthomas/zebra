/**
 * PopupMenu.as
 * Keith Peters
 * version 0.9.9
 * 
 * A button that exposes a list of choices and displays the chosen item. 
 * 
 * Copyright (c) 2011 Keith Peters
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.alleni.author.view.ui.components
{
import flash.display.DisplayObjectContainer;
import flash.display.Stage;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;

[Event(name="select", type="flash.events.Event")]

	public class PopupMenu extends Component
	{
        private var _actionCallback:Function;

		protected var _items:Array;
		protected var _list:List;
		protected var _numVisibleItems:int = 8;
		protected var _stage:Stage;
		
		
		public function PopupMenu(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, items:Array = null)
		{
			_items = items;

			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			super(parent, xpos, ypos);
		}
		
		/**
		 * Initilizes the component.
		 */
		protected override function init():void
		{
			super.init();
			setSize(100, 20);
		}

        public function requestOpenMenu(actionCallback:Function = null):void
        {
            if (this.defaultStyle != null) {
                _list.defaultStyle = this.defaultStyle;
            }
            _list.width = 200;
            _list.listItemSize = 12;

            _stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
            _stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
            this.visible = true;
            _actionCallback = actionCallback;

            _list.selectedIndex = 0;  // assuming the mouse is over the first item, select it now
        }

        private function mouseMove(event:MouseEvent):void
        {
            var global:Point = new Point(event.stageX, event.stageY);
            var local:Point = _list.globalToLocal(global);
            var index:int = local.y / _list.listItemHeight;
            if (index >= 0 && index < _list.items.length) {
                _list.selectedIndex = index;
            }
        }

        private function mouseUp(event:MouseEvent):void
        {
            _stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
            _stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
            this.visible = false;

            if (_actionCallback != null && _list.hitTestPoint(event.stageX, event.stageY)) {
                var item:Object = _list.selectedItem;
                if (item != null) {
                    _actionCallback(item.id);
                }
            }
        }

		/**
		 * Creates and adds the child display objects of this component.
		 */
		protected override function addChildren():void
		{
			super.addChildren();

			_list = new List(null, 0, 0, _items);
			_list.autoHideScrollBar = true;
            _list.width = 200;
            addChild(_list);
            _list.alpha = 1;
		}


		/**
		 * Removes the list from the stage.
		 */
		protected function removeList():void
		{
		}
		

		
		///////////////////////////////////
		// public methods
		///////////////////////////////////
		
		public override function draw():void
		{
			super.draw();
			if (_items.length > 0) _numVisibleItems = _items.length;
			_list.setSize(_width, _numVisibleItems * _list.listItemHeight);
		}
		
		
		/**
		 * Adds an item to the list.
		 * @param item The item to add. Can be a string or an object containing a string property named label.
		 */
		public function addItem(item:Object):void
		{
			_list.addItem(item);
		}
		
		/**
		 * Adds an item to the list at the specified index.
		 * @param item The item to add. Can be a string or an object containing a string property named label.
		 * @param index The index at which to add the item.
		 */
		public function addItemAt(item:Object, index:int):void
		{
			_list.addItemAt(item, index);
		}
		
		/**
		 * Removes the referenced item from the list.
		 * @param item The item to remove. If a string, must match the item containing that string. If an object, must be a reference to the exact same object.
		 */
		public function removeItem(item:Object):void
		{
			_list.removeItem(item);
		}
		
		/**
		 * Removes the item from the list at the specified index
		 * @param index The index of the item to remove.
		 */
		public function removeItemAt(index:int):void
		{
			_list.removeItemAt(index);
		}
		
		/**
		 * Removes all items from the list.
		 */
		public function removeAll():void
		{
			_list.removeAll();
		}
	
		
		
		
		///////////////////////////////////
		// event handlers
		///////////////////////////////////
		

		/**
		 * Called when the component is added to the stage.
		 */
		protected function onAddedToStage(event:Event):void
		{
			_stage = stage;
		}
		

		///////////////////////////////////
		// getter/setters
		///////////////////////////////////
		

		public function set listItemSize(value:int):void
		{
			_list.listItemSize = value;
		}
		
		/**
		 * Sets / gets the index of the selected list item.
		 */
		public function set selectedIndex(value:int):void
		{
			_list.selectedIndex = value;
		}
		public function get selectedIndex():int
		{
			return _list.selectedIndex;
		}
		
		/**
		 * Sets / gets the item in the list, if it exists.
		 */
		public function set selectedItem(item:Object):void
		{
			_list.selectedItem = item;
		}
		public function get selectedItem():Object
		{
			return _list.selectedItem;
		}
		
		/**
		 * Sets/gets the default background color of list items.
		 */
		public function set defaultColor(value:uint):void
		{
			_list.defaultColor = value;
		}
		public function get defaultColor():uint
		{
			return _list.defaultColor;
		}
		
		/**
		 * Sets/gets the selected background color of list items.
		 */
		public function set selectedColor(value:uint):void
		{
			_list.selectedColor = value;
		}
		public function get selectedColor():uint
		{
			return _list.selectedColor;
		}
		
		/**
		 * Sets/gets the rollover background color of list items.
		 */
		public function set rolloverColor(value:uint):void
		{
			_list.rolloverColor = value;
		}
		public function get rolloverColor():uint
		{
			return _list.rolloverColor;
		}
		
		/**
		 * Sets the height of each list item.
		 */
		public function set listItemHeight(value:Number):void
		{
			_list.listItemHeight = value;
			invalidate();
		}
		public function get listItemHeight():Number
		{
			return _list.listItemHeight;
		}
		
		public function set listWidth(value:Number):void
		{
			_list.width = value;
			invalidate();
		}


		/**
		 * Sets / gets the number of visible items in the drop down list. i.e. the height of the list.
		 */
		public function set numVisibleItems(value:int):void
		{
			_numVisibleItems = value;
			invalidate();
		}
		public function get numVisibleItems():int
		{
			return _numVisibleItems;
		}

		/**
		 * Sets / gets the list of items to be shown.
		 */
		public function set items(value:Array):void
		{
			_list.items = value;
			invalidate();
		}
		public function get items():Array
		{
			return _list.items;
		}
		
		/**
		 * Sets / gets the class used to render list items. Must extend ListItem.
		 */
		public function set listItemClass(value:Class):void
		{
			_list.listItemClass = value;
		}
		public function get listItemClass():Class
		{
			return _list.listItemClass;
		}
		
		
		/**
		 * Sets / gets the color for alternate rows if alternateRows is set to true.
		 */
		public function set alternateColor(value:uint):void
		{
			_list.alternateColor = value;
		}
		public function get alternateColor():uint
		{
			return _list.alternateColor;
		}
		
		/**
		 * Sets / gets whether or not every other row will be colored with the alternate color.
		 */
		public function set alternateRows(value:Boolean):void
		{
			_list.alternateRows = value;
		}
		public function get alternateRows():Boolean
		{
			return _list.alternateRows;
		}

        /**
         * Sets / gets whether the scrollbar will auto hide when there is nothing to scroll.
         */
        public function set autoHideScrollBar(value:Boolean):void
        {
            _list.autoHideScrollBar = value;
            invalidate();
        }
        public function get autoHideScrollBar():Boolean
        {
            return _list.autoHideScrollBar;
        }
		

		override public function set enabled(value:Boolean):void
		{
			super.enabled = value;
			
			alpha = 1.0;
		}
	}
}