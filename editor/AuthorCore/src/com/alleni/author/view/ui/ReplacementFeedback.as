package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.feedback.IFeedback;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.geom.Rectangle;

	public class ReplacementFeedback implements IFeedback
	{
		private static var _instance:ReplacementFeedback;
		
		private static var _replacementFeedback:DisplayObject;
		private static var _view:DisplayObject;
		private static var _bounds:Rectangle;
		private static var _container:DisplayObjectContainer;
		
		public function ReplacementFeedback()
		{
			Utilities.assert(!_instance);
		}
		
		public static function get instance():ReplacementFeedback
		{
			if (!_instance)
				_instance = new ReplacementFeedback();
			return _instance;
		}
		
		public function render():void
		{
			if (!visible) return;
			var bounds:Rectangle = _bounds;
			if (bounds) {
				_replacementFeedback.x = bounds.x;
				_replacementFeedback.y = bounds.y;
			}
		}
		
		public function get visible():Boolean
		{
			return (_replacementFeedback != null);
		}
		
		public function showPendingReplacement(view:DisplayObject, bounds:Rectangle=null, container:DisplayObjectContainer=null):void
		{
			show(view, bounds, container, ConstructionOutline, 24);
		}
		
		public function showPendingData(view:DisplayObject, bounds:Rectangle=null, container:DisplayObjectContainer=null):void
		{
//			show(view, bounds, container, MarchingAnts);
		}
		
		public function showPendingAsset(view:DisplayObject, bounds:Rectangle=null, container:DisplayObjectContainer=null):void
		{
			show(view, bounds, container, ConstructionOutline);
		}
		
		private function show(view:DisplayObject, bounds:Rectangle, container:DisplayObjectContainer, feedbackClass:Class, thickness:int=-1):void
		{
			if (_replacementFeedback)
				return;
			
			_view = view;
			_container = container;
			if (!_container)
				_container = feedbackContainer;
			
			if (bounds == null)
				bounds = view.getBounds(_container);
			_bounds = bounds;
			
			if (thickness < 0)
				_replacementFeedback = new feedbackClass(bounds.width, bounds.height);
			else
				_replacementFeedback = new feedbackClass(bounds.width, bounds.height, thickness);
			render();
			_replacementFeedback.alpha = 0;
			_container.addChild(_replacementFeedback);
			Tweener.addTween(_replacementFeedback, {alpha:1, time:0.5, transition:EditorUI.ANIMATION_IN_ALGORITHM});
		}
		
		private function get feedbackContainer():DisplayObjectContainer
		{
			return EditorUI.instance.presentationContainer.editingView;
		}
		
		public function clear():void
		{
			if (_replacementFeedback) {
				Tweener.removeTweens(_replacementFeedback);
				Tweener.addTween(_replacementFeedback, {alpha:0, time:0.5, transition:EditorUI.ANIMATION_IN_ALGORITHM,
					onComplete:function():void {
						if (_replacementFeedback.parent.getChildIndex(_replacementFeedback) > -1)
							_replacementFeedback.parent.removeChild(_replacementFeedback);
						_replacementFeedback = null;
						_bounds = null;
						_view = null;
						_container = null;
					}});
			}
		}
	}
}