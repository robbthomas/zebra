package com.alleni.author.view.ui.palettes
{
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.application.AssetIcons;
	import com.alleni.author.model.objects.IListable;
    import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.service.ThumbnailLoadingPipeline;
	import com.alleni.taconite.dev.BitmapUtils;
	import com.alleni.taconite.dev.GraphicUtils;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;

	import org.svgweb.core.SVGViewer;

	// TODO: cache thumbnail per item then size as needed per container constraint requesting
	public class ItemThumbnail extends Sprite
	{
		private static var _loadingPipeline:ThumbnailLoadingPipeline;
		private var _thumbnail:DisplayObject;
		private var _item:IListable;
		private var _sizeConstraint:Number;
		private var _xOffset:Number;
		private var _yOffset:Number;
		private var _scaleUp:Boolean = false;
		
		public function ItemThumbnail(item:IListable, sizeConstraint:Number, xOffset:Number=0, yOffset:Number=0, scaleUp:Boolean=false)
		{
			super();
			_item = item;
			_sizeConstraint = sizeConstraint;
			_xOffset = xOffset;
			_yOffset = yOffset;
			_scaleUp = scaleUp;
		}
		
		public function render(refresh:Boolean=false):DisplayObject
		{
			if (refresh) {
				if (this.contains(_thumbnail))
					this.removeChild(_thumbnail);
				_thumbnail = null;
			}
			if (!_thumbnail) {
				_thumbnail = AssetIcons.getByType(_item.type);
				_thumbnail.x = (_sizeConstraint - _thumbnail.width) / 2 + _xOffset;
				_thumbnail.y = (_sizeConstraint - _thumbnail.height) / 2 + _yOffset;

				switch (_item.type) {
					case AssetType.GADGET:
						var gadget:Project = _item as Project;
						if (!gadget) break;
						
						if (gadget.icon && gadget.icon.smallURL) {
							ThumbnailLoadingPipeline.push(gadget.icon.smallURL, function(event:Event):void {
								var loaderInfo:LoaderInfo = LoaderInfo(event.target);
								thumbnail = loaderInfo.loader;
							});
						}
						break;
					case AssetType.BITMAP:
						ThumbnailLoadingPipeline.pushAsset(_item.id, _item.name, function(event:Event):void {
							var loaderInfo:LoaderInfo = LoaderInfo(event.target);
							thumbnail = loaderInfo.loader;
						});
						break;
					case AssetType.SVG:
						var svgViewer:SVGViewer;
						if (_item.content is XML)
						{
							svgViewer = new SVGViewer();
							svgViewer.xml = _item.content as XML;
							thumbnail = svgViewer;
						}else{
                            AssetController.instance.loadContent(_item as Asset, function(asset:Asset):void {
                                if(asset.content is XML){
                                    //Intentionally not recursive so we only try to load the thumbnail once if we don't already have it.
                                    svgViewer = new SVGViewer();
                                    svgViewer.xml = asset.content as XML;
                                    thumbnail = svgViewer;
                                } else {
                                    LogService.error("SVG Asset has null content for asset with fileId = "+asset.fileID);
                                }
                            });
                        }
						break;
					default:
						//LogService.debug("ItemThumbnail::render asked to generate an icon for currently unsupported type " + _item.type);
						break;
				}
				
				this.addChild(_thumbnail);
			}
			return this;
		}
		
		private function set thumbnail(value:DisplayObject):void
		{
			if (this.contains(_thumbnail))
				this.removeChild(_thumbnail);
			if (value as Loader)
				_thumbnail = BitmapUtils.bilinearDownscale((value as Loader).content as Bitmap, _sizeConstraint);
			else if (value)
				_thumbnail = value;
			else {
				LogService.error("ItemThumbnail unexpected thumbnail value:" + value);
				return;
			}
			this.addChild(_thumbnail);
			_thumbnail.addEventListener(Event.ENTER_FRAME, updateIconScale);
		}
		
		private function updateIconScale(e:Event):void
		{
			if (_thumbnail.width > 0 && _thumbnail.height > 0)
			{
				_thumbnail.removeEventListener(Event.ENTER_FRAME, updateIconScale);
			}else{
				return;
			}
			
			if (_scaleUp || (_thumbnail.width > _sizeConstraint || _thumbnail.height > _sizeConstraint)) {
				var scale:Number = GraphicUtils.scaleForAspect(_thumbnail.width, _thumbnail.height, _sizeConstraint);
				
				_thumbnail.scaleX = _thumbnail.scaleY = scale;
				if (_thumbnail.mask)
					_thumbnail.mask.scaleX = _thumbnail.mask.scaleY = scale;
			}
			
			var thumbnailX:Number = (_sizeConstraint - _thumbnail.width) / 2 + _xOffset;
			var thumbnailY:Number = (_sizeConstraint - _thumbnail.height) / 2 + _yOffset;
			
			if (_thumbnail.mask) {
				_thumbnail.mask.x = thumbnailX;
				_thumbnail.mask.y = thumbnailY;
			}
			_thumbnail.x = thumbnailX;
			_thumbnail.y = thumbnailY;
		}
	}
}