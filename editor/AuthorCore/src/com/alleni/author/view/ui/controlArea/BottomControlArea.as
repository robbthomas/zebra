package com.alleni.author.view.ui.controlArea
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.controlArea.BottomControlAreaMediator;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ui.controls.PopOutMenu;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class BottomControlArea extends Sprite
	{
		public var panelWidth:Number;
		public var panelHeight:Number;
		
		[Bindable]
		private var _zoomPopOutMenu:PopOutMenu;
		
		
		public function initialize():void
		{
			_zoomPopOutMenu = new PopOutMenu(EditorUI.instance);
			_zoomPopOutMenu.closedHeight = 25;
			_zoomPopOutMenu.closedWidth = 55;
			_zoomPopOutMenu.popWidth = 55;
			_zoomPopOutMenu.dataProvider = [{label:"25%", data:.25}, {label:"50%", data:.5}, {label:"75%", data:.75},
											{label:"100%", data:1}, {label:"150%", data:1.5}, {label:"200%", data:2}, 
											{label:"300%", data:3}, {label:"400%", data:4}];
			_zoomPopOutMenu.defaultIndex = 3;
			_zoomPopOutMenu.baseLabelOffsetX = -2;
			_zoomPopOutMenu.baseLabelOffsetY = 1;
			_zoomPopOutMenu.arrowOffsetX = 3;
			_zoomPopOutMenu.drawComponent();
			_zoomPopOutMenu.x = 5;
			_zoomPopOutMenu.y = 1;
			addChild(_zoomPopOutMenu);

			_zoomPopOutMenu.addEventListener(MouseEvent.MOUSE_OVER, function(event:Event):void{
				ApplicationController.instance.displayToolTip(_zoomPopOutMenu.localToGlobal(new Point(0,0)), ControlAreaToolTip.DISPLAY_ABOVE, "Zoom percent");});
			_zoomPopOutMenu.addEventListener(MouseEvent.MOUSE_OUT, function(event:Event):void{ApplicationController.instance.hideToolTip();});
			_zoomPopOutMenu.addEventListener(PopOutMenu.POP_OUT_OPEN, function(event:Event):void{ApplicationController.instance.hideToolTip();});
			
			ApplicationController.addEventListener(NotificationNamesApplication.CREATE_NEW_PROJECT, resetPopUp);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_COMPLETE, resetPopUp);
			
			new BottomControlAreaMediator().handleViewEvents(this);
		}
				
		public function scaleChanged(event:Event):void
		{
			var newZoom:Number = Application.instance.zoomLevel;
			
			_zoomPopOutMenu.baseLabelOverrideString = "" + Math.floor(newZoom * 100) + "%";
			_zoomPopOutMenu.updateSelectedItemByData(newZoom);
		}
		
		private function resetPopUp(event:Event):void{
			_zoomPopOutMenu.updateSelectedItemByData(1);
		}
		
		public function handleMeasure():void
		{
			// nothing necessary at this time, could use this to space objects out or something based on dimensions
		}
		
		public function get zoomPopOutMenu():PopOutMenu
		{
			return _zoomPopOutMenu;
		}
	}
}
