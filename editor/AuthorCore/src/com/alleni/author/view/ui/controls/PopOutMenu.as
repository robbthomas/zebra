package com.alleni.author.view.ui.controls
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.definition.application.AssetIcons;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.util.CustomPoint;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.RibbonDropDownMenu;

	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	import mx.core.UIComponent;

	public class PopOutMenu extends Sprite
	{
		//Events
		//TODO: Need to build out a true event class
		public static const ITEM_CHANGED:String = "Item Changed";
		public static const POP_OUT_OPEN:String = "Pop Out Open";
		public static const POP_OUT_CLOSED:String = "Pop Out Closed";
		
		//Static Properties
		public static const DIRECTION_UP:String = "Direction Up";
		public static const DIRECTION_DOWN:String = "Direction Down";
		public static const DIRECTION_BI:String = "Direction Bi";
		public static const DIRECTION_AUTO:String = "Direction Auto";
		
		public static const ITEMS_TYPE_LABEL_ONLY:String = "Label Only";
		public static const ITEMS_TYPE_ICON_ONLY:String = "Icon Only";
		public static const ITEMS_TYPE_BOTH:String = "Icon and Label";
		public static const ITEMS_TYPE_BOTH_HOTKEY:String = "Icon, Label and Hotkey";
		 
		public static const BASE_TYPE_DEFAULT_TITLE:String = "Default Title";
		public static const BASE_TYPE_LABEL_ONLY:String = "Base Label Only";
		public static const BASE_TYPE_ICON_ONLY:String = "Base Icon Only";
		public static const BASE_TYPE_BOTH:String = "Base Icon and Label";
		
		public static const LABEL_SIZE_SMALL:Number 	= 12.0;
		public static const LABEL_SIZE_MEDIUM:Number 	= 14.0;
		public static const LABEL_SIZE_LARGE:Number 	= 16.0;
		
		public static const SEPARATOR:String = "**Build Separator**";
		
		public static const ICON_SIZE:Number 	= 15.0;
		private static const ICON_X:Number 		= 14.0;
		private static const ICON_Y:Number 		= 4.0;
		
		//Config Data Objects
        public var isScalable:Boolean = false;
		public var direction:String;
		public var dataProvider:Array;
		public var itemHeight:Number = 20;
		public var itemsType:String = ITEMS_TYPE_LABEL_ONLY;
		public var baseType:String;
		public var labelSize:Number = LABEL_SIZE_SMALL;
		public var baseLabelOverrideString:String = "";
		public var baseLabelOffsetX:Number;
		public var baseLabelOffsetY:Number;
		public var popOutOffset:Number = 0;
		public var biHeightAdjust:Number = 80;
        public var doBeforeOpen:Function;
        private var _moveUp:Boolean;
		private var _defaultIndex:int;
		private var _selectedItem:MenuItem;

		//View Objects
		private var _baseBackground:Sprite;
		private var _baseObject:Sprite;
		private var _baseLabel:LightLabel;
		private var _baseIcon:DisplayObject;
		private var _popOutObject:DisplayObject;
		private var _items:Sprite;
		private var _popOutMask:Sprite;
		private var _popOutItemsMask:Sprite;
		private var _bottomButton:Sprite;
		private var _topButton:Sprite;
		
		//Dimensions
		public var checkMarkScale:Number = .5;
		public var popWidth:int = 50;
		
		private var _popHeight:int = 16;
		private var _backgroundStartY:int;
		private var _maskStartY:int;
		private var _closedHeight:int = 25;
		private var _closedWidth:int = 50;

		//Theme
        private var _baseColor:int = 0x000000;
        private var _baseAlpha:Number = 1;
		private var _arrowColor:int = 0x555555;
		private var _baseTextColor:int = 0xFFFFFF;
		private var _borderColor:int;
		private var _arrowScale:Number = 1;
		private var _baseLabelWidthAdjustment:Number = 0;
		private var _arrowOffsetX:Number = 0;
		private var _parentLocationY:Number;
		private var _defaultTitle:String = "Items";
		private var _appContainer:UIComponent;
		
		
		public function PopOutMenu(appContainer:UIComponent, dataProvider:Array = null, direction:String = DIRECTION_UP, defaultIndex:int = 0)
		{
			super();
			_appContainer = appContainer;
			this.direction = direction;
			this.dataProvider = dataProvider;
        }
		
		public function get defaultTitle():String
		{
			return _defaultTitle;
		}

		private var _alpha:Number;
		private var _enabled:Boolean = true;
		public function set enabled(value:Boolean):void {
			if (!_enabled && value) {
				this.alpha = _alpha;
			} else {
				_alpha = alpha;
				alpha = 0.5;
				contractMenu();
			}
			_enabled = value;
		}

		public function set defaultTitle(value:String):void
		{
			_defaultTitle = value;
		}

		
		public function drawComponent():void
		{
            if (_popOutObject != null && this.contains(_popOutObject)) {
                this.removeChild(_popOutObject);
            }
            if (_popOutObject != null && this.contains(_items)) {
                this.removeChild(_items);
            }

			//Build items before background so we have an accurate height
			_items = menuItems;
			
			//POP OUT LOCATION SETTINGS
			switch(this.direction){
				case DIRECTION_UP:
					_backgroundStartY = 0;
					_maskStartY = -(_popHeight);
					break;
				case DIRECTION_DOWN:
					_backgroundStartY = _closedHeight - _popHeight;
					_maskStartY = _closedHeight;
					break;
				case DIRECTION_BI:
					var myLocation:Point = new Point(x, y);
					break;
			}
			
			_items.y = _backgroundStartY + 8;
			
			_baseObject = base;
			_baseObject.buttonMode = true;
			_baseObject.addEventListener(MouseEvent.CLICK, expandMenu, false, 0, true);

			_popOutObject = background;
			_popOutObject.y = _backgroundStartY;
			_popOutObject.x = popOutOffset;
			_popOutObject.addEventListener(MouseEvent.CLICK, contractMenu, false, 0, true);
			
			_popOutMask = new Sprite();
			_popOutMask.cacheAsBitmap = true;
			_popOutMask.graphics.beginFill(0x0000FF);
			_popOutMask.graphics.drawRect(0,0,this.popWidth, _popHeight*zoomLevel);
			_popOutMask.y = _maskStartY;
			_popOutMask.x = popOutOffset;
			
			_popOutItemsMask = new Sprite();
			_popOutItemsMask.graphics.beginFill(0x0000FF);
			_popOutItemsMask.graphics.drawRect(0,0,this.popWidth, _popHeight*zoomLevel);
			_popOutItemsMask.y = _maskStartY;
			_popOutItemsMask.x = popOutOffset;
			
			addChild(_baseObject);
			addChild(_popOutObject);
			
			if (this.direction == DIRECTION_BI){
				_items.visible = false;
				_popOutObject.visible = false;
			}else{
				addChild(_popOutMask);
				_popOutObject.mask = _popOutMask;
				
				addChild(_popOutItemsMask);
				_items.visible = false;
				_popOutObject.visible = false;
				_items.mask = _popOutItemsMask;
			}
			
			//Place items
			addChild(_items);
		}
		
		//PUBLIC CONTROLS
		public function updateSelectedItemByData(data:Object):void
		{
			var clearedAllItems:Boolean = true;
            if (items){
				if(data is CustomPoint){
                    handleCustomPointDataUpdate(data as CustomPoint);
                }else{
                    var i:int;
                    for(i=0;i<items.numChildren;i++){
                        var tempMenuItem:MenuItem = (items.getChildAt(i) as MenuItem);
                        tempMenuItem.selected = false;
                        if (data == tempMenuItem.data){
                            tempMenuItem.selected = true;
                            _selectedItem = tempMenuItem;
                            clearedAllItems = false;
//                            break;
                        }else if(data is Point && !tempMenuItem.isCustomAnchorPosition && data.x == tempMenuItem.data.x && data.y == tempMenuItem.data.y){
                            tempMenuItem.selected = true;
                            _selectedItem = tempMenuItem;
                            clearedAllItems = false;
//                            break;
                        }else if(data == 99){
                            tempMenuItem.selected = false;
//                            break;
                        }else{
                            tempMenuItem.selected = false;
                            if(_selectedItem == tempMenuItem){
                                _selectedItem = null;
                            }
                        }
                    }
                }
                switch(this.baseType){
                    case BASE_TYPE_ICON_ONLY:
                        buildBaseIcon();
                        break;
                    case BASE_TYPE_DEFAULT_TITLE:
                    case BASE_TYPE_BOTH:
                        buildBaseIcon();
                    case BASE_TYPE_LABEL_ONLY:
                    default:
                        buildBaseLabel();
                }
			}
		}

        public function handleCustomPointDataUpdate(data:CustomPoint):void{
            var i:int;
            for(i=0;i<items.numChildren;i++){
                var tempMenuItem:MenuItem = (items.getChildAt(i) as MenuItem);
                tempMenuItem.selected = false;
                if(tempMenuItem.isCustomAnchorPosition){
                    tempMenuItem.selected = true;
                    _selectedItem = tempMenuItem;
                }else{
                    tempMenuItem.selected = false;
                }
            }
        }

		public function updateSelectedItemByLabel(label:String):void
		{
			_selectedItem = null;
			if (items){
				var i:int;
				for(i=0;i<items.numChildren;i++){
					var tempMenuItem:MenuItem = (items.getChildAt(i) as MenuItem);
					tempMenuItem.selected = false;
					if (label == tempMenuItem.label){
						tempMenuItem.selected = true;
						_selectedItem = tempMenuItem;
					}else{
						tempMenuItem.selected = false;
					}
				}
				switch(this.baseType){
					case BASE_TYPE_ICON_ONLY:
						buildBaseIcon();
						break;
					case BASE_TYPE_DEFAULT_TITLE:
					case BASE_TYPE_BOTH:
						buildBaseIcon();
					case BASE_TYPE_LABEL_ONLY:
					default:
						buildBaseLabel();
				}
			}
		}
		
		public function updateDataProvider(newDataProvider:Array):void
		{
			this.dataProvider = newDataProvider;
			tearDownUI();
			drawComponent();
		}
		
        private function clickedAppContainer(e:MouseEvent):void {
            var target:DisplayObject = e.target as DisplayObject;
            if (target != null && this.contains(target) && this != target) {
                return;
            } else {
                contractMenu(e);
                return;
            }
        }

        private function setupClickOutsideOfMenu():void {
            // this is a hack because for some reason the zoom menu is created before EditorUI.instance exists
            if (_appContainer == null)
                _appContainer = EditorUI.instance;

            if (_appContainer != null) {
                _appContainer.addEventListener(MouseEvent.MOUSE_UP, clickedAppContainer, false, 0, true);
            }
        }

		//CONTROLS FOR MENU
		private function expandMenu(event:MouseEvent = null):void
		{
			if (!_enabled) return;
            if (doBeforeOpen) {
                doBeforeOpen.call();
            }
            setupClickOutsideOfMenu();
            drawComponent();
			dispatchEvent(new Event(POP_OUT_OPEN));
			_items.visible = true;
			_popOutObject.visible = true;

			Application.instance.viewContext.info.userActionInProgress = true;
			switch (this.direction){
				case DIRECTION_UP:
					Tweener.addTween(_popOutObject, {y:(-1 * _popHeight), time:.2, transition:"easeOutCubic", onUpdate:moveMenuItems, onComplete:menuOpened});
					break;
				case DIRECTION_DOWN:
					Tweener.addTween(_popOutObject, {y:(_closedHeight), time:.2, transition:"easeOutCubic", onUpdate:moveMenuItems, onComplete:menuOpened});
					break;
				case DIRECTION_BI:
					var location:Point = new Point(0, 40);
					location = globalToLocal(location);
					_popOutMask.y = location.y;
					_popOutMask.height = (_appContainer.height - biHeightAdjust)/zoomLevel;
					addChild(_popOutMask);
    					_popOutObject.mask = _popOutMask;
					
					_popOutItemsMask.y = location.y;
					_popOutItemsMask.height = (_appContainer.height - biHeightAdjust)/zoomLevel;
					addChild(_popOutItemsMask);
					_items.mask = _popOutItemsMask;
					
					_popOutObject.visible = true;
					var currentIndex:int = selectedIndex;
					var popLocationY:Number = -((currentIndex + 1) * itemHeight);
					_popOutObject.y = popLocationY - 8;
					_items.y = popLocationY;
					menuOpened();
					break;
			}

			_baseObject.removeEventListener(MouseEvent.CLICK, expandMenu);
			_baseObject.addEventListener(MouseEvent.CLICK, contractMenu, false, 0, true);
			
			if(!(this.parent is RibbonDropDownMenu)){
			/*}else{*/
				event.stopPropagation();
			}
		}

		public function get selectedIndex():int
		{
			var i:int
			for (i=0;i<_items.numChildren; i++) {
				if((_items.getChildAt(i) as MenuItem).selected == true){
					return i;
				}
			}
			return 0;
		}
		
		public function set selectedIndex(value:int):void
		{
			selectItem(_items.getChildAt(value) as MenuItem);
		}
		
		private function contractMenu(event:Event = null):void
		{
            _appContainer.removeEventListener(MouseEvent.MOUSE_UP, clickedAppContainer);
            dispatchEvent(new Event(POP_OUT_CLOSED));
			if (Application.instance.viewContext) Application.instance.viewContext.info.userActionInProgress = false;
			if (this.direction != DIRECTION_BI){
				Tweener.addTween(_popOutObject, {y:_backgroundStartY, time:.2, transition:"easeInCubic", onUpdate:moveMenuItems, onComplete:menuClosed});
			} else {
				if (!_items || !_popOutObject) return;
				_items.visible = false;
				_popOutObject.visible = false;
				if(_topButton)
					if (this.contains(_topButton))
						removeChild(_topButton);

				if (_bottomButton)
					if (this.contains(_bottomButton))
						removeChild(_bottomButton);
			}
			if (_baseObject == null) return;
			_baseObject.addEventListener(MouseEvent.CLICK, expandMenu, false, 0, true);
			_baseObject.removeEventListener(MouseEvent.CLICK, contractMenu);
		}
		
		private function menuClosed():void
		{
			_popOutObject.visible = false;
			_items.visible = false;
			dispatchEvent(new Event(POP_OUT_CLOSED));
		}
		
		private function menuOpened():void
		{
			dispatchEvent(new Event(POP_OUT_OPEN));
			testForButton();
		}
		
		private function testForButton():void
		{
			if (this.direction == DIRECTION_BI) {
				var location:Point = new Point(0, 40);
				location = globalToLocal(location);
				
				if (_items.y < location.y){
					if (!_topButton){
						_topButton = buildTopButton();
						_topButton.addEventListener(MouseEvent.ROLL_OVER, animatePopOut, false, 0, true);
						_topButton.addEventListener(MouseEvent.ROLL_OUT, stopPopOutAnimation, false, 0, true);
						_topButton.addEventListener(MouseEvent.MOUSE_UP, captureMouse, false, 0, true);
						_topButton.addEventListener(MouseEvent.CLICK, captureMouse, false, 0, true);
					}
					_topButton.y = _popOutMask.y - 8;
					addChild(_topButton);
				}else{
					if(_topButton){
						if(contains(_topButton)){
                            stopPopOutAnimation();
							removeChild(_topButton);
							if(_items.y != _popOutMask.y){
								_popOutItemsMask.y = _popOutMask.y = _items.y;
							}
						}
					}	
				}
				
				var itemsLocation:Point = new Point(_items.x, _items.y);
				itemsLocation = localToGlobal(itemsLocation);
				if ((itemsLocation.y/zoomLevel + _items.height) > (_appContainer.height/zoomLevel - 48)){
					if (!_bottomButton){
						_bottomButton = buildBottomButton();
						_bottomButton.addEventListener(MouseEvent.ROLL_OVER, animatePopOut, false, 0, true);
						_bottomButton.addEventListener(MouseEvent.ROLL_OUT, stopPopOutAnimation, false, 0, true);
						_bottomButton.addEventListener(MouseEvent.MOUSE_UP, captureMouse, false, 0, true);
						_bottomButton.addEventListener(MouseEvent.CLICK, captureMouse, false, 0, true);
					}
					_bottomButton.y = _popOutMask.y + _popOutMask.height - this.itemHeight + 8;
					addChild(_bottomButton);
				}else{
					if(_bottomButton){
						if(contains(_bottomButton)){
                            stopPopOutAnimation();
							removeChild(_bottomButton);
						}
					}
				}
			}
		}
		
		private function captureMouse(event:MouseEvent):void{
			event.stopPropagation();
		}
		
		private function hideButtons(event:MouseEvent):void
		{
			if (_topButton)
				if (contains(_topButton))
					removeChild(_topButton);
			if (_bottomButton)
				if (this.contains(_bottomButton))
					removeChild(_bottomButton);
		}
		
		private function animatePopOut(event:MouseEvent):void
		{
			if (event.currentTarget == _topButton)
				_moveUp = false;
			else
				_moveUp = true;

			addEventListener(Event.ENTER_FRAME, movePopOut, false, 0, true);
		}
		
		private function movePopOut(event:Event):void
		{
			var moveBy:Number;
			if (_moveUp)
				moveBy = -this.itemHeight;
			else
				moveBy = this.itemHeight;
			_popOutObject.y = _popOutObject.y + moveBy;
			moveMenuItems();
			
			testForButton();
		}
		
		private function stopPopOutAnimation(event:MouseEvent = null):void
		{
			var moveBy:Number;
			if (_moveUp)
				moveBy = -8;
			else
				moveBy = 8;
			_popOutObject.y = _popOutObject.y + moveBy;
//			moveMenuItems();
			removeEventListener(Event.ENTER_FRAME, movePopOut);
		}
		
		private function buildTopButton():Sprite
		{
			var tempBackground:Sprite = new Sprite();
			tempBackground.graphics.lineStyle(1,0x828282, 1, true);
			tempBackground.graphics.beginFill(0xCCCCCC);
			tempBackground.graphics.drawRoundRectComplex(0,0,this.popWidth, this.itemHeight,0,8,0,0);
			tempBackground.graphics.endFill();
			
			var arrow:Shape = new Shape();
			arrow.graphics.beginFill(_arrowColor);
			arrow.graphics.moveTo(5,0);
			arrow.graphics.lineTo(10,10);
			arrow.graphics.lineTo(0,10);
			arrow.graphics.lineTo(5,0);
			arrow.graphics.endFill();
			arrow.x = (this.popWidth - arrow.width)/2;
			arrow.y = (this.itemHeight - arrow.height)/2;
			tempBackground.addChild(arrow);
			
			return tempBackground;
		}
		
		private function buildBottomButton():Sprite
		{
			var location:Point = new Point(0, 40);
			location = globalToLocal(location);
			
			var background:Sprite = new Sprite();
			background.graphics.lineStyle(1,0x828282, 1, true);
			background.graphics.beginFill(0xCCCCCC);
			background.graphics.drawRoundRectComplex(0,0,this.popWidth, this.itemHeight,0,0,0,8);
			background.graphics.endFill();
			
			var arrow:Shape = new Shape();
			arrow.graphics.beginFill(_arrowColor);
			arrow.graphics.moveTo(0,0);
			arrow.graphics.lineTo(10,0);
			arrow.graphics.lineTo(5,10);
			arrow.graphics.lineTo(0,0);
			arrow.graphics.endFill();
			arrow.x = (this.popWidth - arrow.width)/2;
			arrow.y = (this.itemHeight - arrow.height)/2;
			background.addChild(arrow);
			
			return background;
		}
		
		private function respondToClick(event:MouseEvent):void
		{
			var menuItem:MenuItem = event.currentTarget as MenuItem;
			switch(menuItem.response){
				case MenuItem.RESPONSE_ACTION:
					callAction(menuItem);
					break;
				case MenuItem.RESPONSE_SELECTABLE:
				default:
					selectItem(menuItem);
					break;
			}
		}
		
		private function callAction(clickedItem:MenuItem):void
		{
			dispatchEvent(new Event(clickedItem.data as String));
			clickedItem.selected = false;
			contractMenu();
		}
		
		private function selectItem(clickedItem:MenuItem):void
		{
			_selectedItem = clickedItem;
			
			//Reset Menu Items
			var i:int;
			for (i=0;i<this.dataProvider.length;i++)
				if (this.dataProvider[i].response != MenuItem.RESPONSE_ACTION)
					(_items.getChildAt(i) as MenuItem).selected = false;
			
			//Update Menu Items with the selected on hilited
			_selectedItem.selected = true;
			switch(this.baseType){
				case BASE_TYPE_ICON_ONLY:
					buildBaseIcon();
					break;
				case BASE_TYPE_DEFAULT_TITLE:  //**
				case BASE_TYPE_BOTH:
					buildBaseIcon();
				case BASE_TYPE_LABEL_ONLY:
				default:
					buildBaseLabel();
					break;
			}
			dispatchEvent(new Event(ITEM_CHANGED));
			contractMenu();
		}
		
		private function moveMenuItems():void
		{
			_items.y = _popOutObject.y + 8;
		}
		
		private function toggleHilite(event:MouseEvent):void
		{
			if (! (event.target as MenuItem).selected)
				(event.target as MenuItem).hiliteToggle();
		}
		
		//POP OUT OBJECT BUILDERS
		private function get background():DisplayObject
		{
			var ur:int;
			var lr:int;
			
			switch(this.direction){
				case DIRECTION_UP:
					ur = 8;
					lr = 0;
					break;
				case DIRECTION_DOWN:
					ur = 0;
					lr = 8;
					break;
				default:
					ur = 8;
					lr = 8;
					break;
			}
			
			var background:Shape = new Shape();
			background.graphics.lineStyle(1, 0x828282, 1, true);
			background.graphics.beginFill(0xCCCCCC);
			background.graphics.drawRoundRectComplex(0,0,this.popWidth, _popHeight-1,0,ur,0,lr);
			background.graphics.endFill();
			background.filters = [new DropShadowFilter(2.0, 45, 0x000000, 0.8, 2.0, 2.0, 0.8, 2)];
			return background;
		}
		
		private function get menuItems():Sprite
		{
			var items:Sprite = new Sprite();
			var i:int;
			var separatorCount:int = 0;
            var selectedItemSet:Boolean = false;
            var willSetToDefault:MenuItem = null;

			for (i=0;i<this.dataProvider.length;i++) {
				if (dataProvider[i].label != SEPARATOR) {
					var item:MenuItem = new MenuItem();
					item.x = popOutOffset;
					item.type = this.itemsType;
					item.y = (this.itemHeight * i)-(separatorCount * 12);
					item.label = this.dataProvider[i].label;
					item.isCustomAnchorPosition = this.dataProvider[i].isCustomAnchorPosition;
					item.labelSize = this.labelSize;
					item.data = this.dataProvider[i].data;
					item.icon = this.dataProvider[i].icon;

					if (this.dataProvider[i].response)
						item.response = this.dataProvider[i].response;
					else
						item.response = MenuItem.RESPONSE_SELECTABLE;
					item.hotKey = this.dataProvider[i].hotKey;
					item.menuWidth = this.popWidth;
					item.checkMarkScale = this.checkMarkScale;
					item.buildMenuItem();
                    if (_selectedItem != null 
                    		&& ((_selectedItem.data != null && _selectedItem.data == item.data)
                    		|| (_selectedItem.label != null && _selectedItem.label == item.label))) {
                        item.selected = true;
                        _selectedItem = item;
                        selectedItemSet = true;
                        willSetToDefault = null;
                    } else if (i == _defaultIndex && !selectedItemSet){
                        willSetToDefault = item;
					}
					item.addEventListener(MouseEvent.MOUSE_UP, respondToClick, false, 0, true);
					item.addEventListener(MouseEvent.ROLL_OVER, toggleHilite, false, 0, true);
					item.addEventListener(MouseEvent.ROLL_OUT, toggleHilite, false, 0, true);
					items.addChild(item);
				} else {
					var lineLocation:Number = (this.itemHeight * i)-(separatorCount * 10) + 5;
					items.graphics.lineStyle(1,0x7d7d7d);
					items.graphics.moveTo(popOutOffset,lineLocation);
					items.graphics.lineTo(this.popWidth, lineLocation);
					separatorCount++;
				}
			}

            if (willSetToDefault != null) {
                willSetToDefault.selected = true;
                _selectedItem = willSetToDefault;
            }
			_popHeight = ((this.itemHeight * this.dataProvider.length) - (separatorCount * 10))+ 16;

			return items;
		}

		//BASE OBJECT BUILDERS
		private function drawArrow(openDirection:String):DisplayObject
		{
			var arrow:Shape = new Shape();
			switch(openDirection){
				case DIRECTION_UP:
					arrow.graphics.beginFill(_arrowColor);
					arrow.graphics.moveTo(3,0);
					arrow.graphics.lineTo(6,7);
					arrow.graphics.lineTo(0,7);
					arrow.graphics.lineTo(3,0);
					arrow.graphics.endFill();
					break;
				case DIRECTION_DOWN:
					arrow.graphics.beginFill(_arrowColor);
					arrow.graphics.moveTo(0,0);
					arrow.graphics.lineTo(6,0);
					arrow.graphics.lineTo(3,7);
					arrow.graphics.lineTo(0,0);
					arrow.graphics.endFill();
					break;
				default:
					arrow.graphics.beginFill(_arrowColor);
					arrow.graphics.moveTo(0,0);
					arrow.graphics.lineTo(7,3);
					arrow.graphics.lineTo(0,6);
					arrow.graphics.lineTo(0,0);
					arrow.graphics.endFill();
					break;
			}
			arrow.scaleX = arrow.scaleY = _arrowScale;
			return arrow;
		}
		
		private function get base():Sprite
		{
            if (_baseBackground != null && this.contains(_baseBackground)) {
                removeChild(_baseBackground);
            }
			_baseBackground = new Sprite();
			_baseBackground.graphics.lineStyle(1,_baseColor,_baseAlpha);
			_baseBackground.graphics.beginFill(_baseColor,_baseAlpha);
			_baseBackground.graphics.drawRect(0,0,_closedWidth, _closedHeight-1);
			_baseBackground.graphics.endFill();
			addChild(_baseBackground);
			
			var arrow:DisplayObject = drawArrow(this.direction);
			arrow.y = (_closedHeight/2) - (arrow.height/2);
			arrow.x = _arrowOffsetX;
			_baseBackground.addChild(arrow);
			
			switch (this.baseType) {
				case BASE_TYPE_ICON_ONLY:
					buildBaseIcon();
					break;
				case BASE_TYPE_BOTH:
					buildBaseIcon();
				case BASE_TYPE_LABEL_ONLY:
				default:
					buildBaseLabel();
			}
			return _baseBackground;
		}
		
		private function buildBaseIcon():void
		{
			if (_baseIcon) {
				if (_baseBackground.contains(_baseIcon))
					_baseBackground.removeChild(_baseIcon);
//				if (_baseIcon as Loader)
//					(_baseIcon as Loader).unload();     // removed since it may cause URL-not-found crash
				_baseIcon = null;
			}
			if (_selectedItem) {
				if (_selectedItem.icon as String) {
					_baseIcon = new Loader();
					Loader(_baseIcon).load(new URLRequest(_selectedItem.icon as String));
					Loader(_baseIcon).contentLoaderInfo.addEventListener(Event.COMPLETE, function(event:Event):void {
						updateIconPosition();
					});
				} else if (_selectedItem.icon as Class) {
					_baseIcon = new _selectedItem.icon();
				} else if (_selectedItem.icon as DisplayObject) {
					_baseIcon = _selectedItem.icon as DisplayObject;
				} else {
					// nothing
				}
			} else if (_selectedItem && _selectedItem.icon as DisplayObject)
				_baseIcon = AssetIcons.getByType(_selectedItem.data as int);
			if (_baseIcon) {
				updateIconPosition();
				_baseBackground.addChild(_baseIcon);
			}
		}
		
		public function setBaseIcon(icon:DisplayObject):void{
			if (_baseIcon) {
				if (_baseBackground.contains(_baseIcon))
					_baseBackground.removeChild(_baseIcon);
//				if (_baseIcon as Loader)
//					(_baseIcon as Loader).unload();      // removed since it may cause URL-not-found crash
				_baseIcon = null;
			}
			
			if (icon) {
				_baseIcon = icon;
				updateIconPosition();
				_baseBackground.addChild(icon);
			}
		}
		
		private function updateIconPosition():void
		{
			_baseIcon.x = ICON_X + (ICON_SIZE - _baseIcon.width) / 2;
			_baseIcon.y = ICON_Y + (ICON_SIZE - _baseIcon.height) / 2;
		}
		
		private function buildBaseLabel():void
		{
			if (_baseLabel) {
				if (_baseBackground.contains(_baseLabel))
					_baseBackground.removeChild(_baseLabel);
				_baseLabel = null;
			}
			
			_baseLabel = new LightLabel();
			if (_selectedItem)
				_baseLabel.text = _selectedItem.label;
			else if (baseLabelOverrideString == "")
				_baseLabel.text = this.baseType == BASE_TYPE_DEFAULT_TITLE?_defaultTitle:"";
			else
				_baseLabel.text = baseLabelOverrideString;
			
			_baseLabel.color = _baseTextColor;
			_baseLabel.size = this.labelSize;
			_baseLabel.width = _closedWidth - (18 + _baseLabelWidthAdjustment);
			_baseLabel.x = 14 + this.baseLabelOffsetX;
			_baseLabel.y = 3 + this.baseLabelOffsetY;
			_baseLabel.scrollRect = new Rectangle(0, 0, _baseLabel.width, itemHeight - 4);
			_baseBackground.addChild(_baseLabel);
		}
		
		public function resetBaseLabel():void
		{	
			_baseLabel.text = _defaultTitle;
			_baseLabel.color = _baseTextColor;
			_baseLabel.size = this.labelSize;
			_baseLabel.width = _closedWidth - (18 + _baseLabelWidthAdjustment);
			_baseLabel.x = 14 + this.baseLabelOffsetX;
			_baseLabel.y = 3 + this.baseLabelOffsetY;
			_selectedItem = null;
			
			this.buildBaseIcon();
		}
		
		public function tearDownUI():void
		{
			while (numChildren > 0)
				removeChildAt(0);
			
			for each (var item:MenuItem in _items) {
				item.removeEventListener(MouseEvent.MOUSE_UP, respondToClick);
				item.removeEventListener(MouseEvent.ROLL_OVER, toggleHilite);
				item.removeEventListener(MouseEvent.ROLL_OUT, toggleHilite);
			}
			_items = null;
			
			_baseObject.removeEventListener(MouseEvent.MOUSE_DOWN, expandMenu);
			_baseObject.removeEventListener(MouseEvent.MOUSE_UP, contractMenu);
			_baseObject = null;
			
			_popOutObject.removeEventListener(MouseEvent.MOUSE_UP, contractMenu);
			_popOutObject = null;
		}
		
		public function addObjectToDataProvider(value:Object):void
		{
			this.dataProvider.push(value);
		}
		
		//GETTERS and SETTERS
		public function set parentLocationY(value:Number):void
		{
			_parentLocationY = value;
		}
		
		public function set defaultIndex(value:int):void
		{
			_defaultIndex = value;
		}
		
		public function get selectedItem():MenuItem
		{
			return _selectedItem;
		}
		
		public function get baseLabel():LightLabel
		{
			return _baseLabel;
		}
		
		public function get items():Sprite
		{
			return _items;
		}
		
		//THEME SETTINGS
		public function set closedHeight(value:int):void
		{
			_closedHeight = value;
		}
		
		public function set closedWidth(value:int):void
		{
			_closedWidth = value;
		}
		
        public function set baseColor(value:int):void
        {
            _baseColor = value;
        }

        public function set baseAlpha(value:Number):void
        {
            _baseAlpha = value;
        }

		public function set arrowColor(value:int):void
		{
			_arrowColor = value;
		}
		
		public function set baseTextColor(value:int):void
		{
			_baseTextColor = value;
		}
		
		public function set borderColor(value:int):void
		{
			_borderColor = value;
		}
		
		public function set arrowScale(value:Number):void
		{
			_arrowScale = value;
		}
		
		public function set baseLabelWidthAdjustment(value:Number):void
		{
			_baseLabelWidthAdjustment = value;
		}
		
		public function set arrowOffsetX(value:Number):void
		{
			_arrowOffsetX = value;
		}

        private function get zoomLevel():Number {
            if(isScalable) {
                return Application.instance.zoomLevel;
            } else {
                return 1;
            }
        }
	}
}