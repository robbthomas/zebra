package com.alleni.author.view.ui.controls
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ApplicationUI;
	import com.alleni.author.controller.test.TestPanelController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.definition.PlayerIconTips;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.definition.application.PlayerIcons;
	import com.alleni.author.document.Document;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.service.environment.ShortURLOperation;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.components.HBox;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.graphics.GlassBar;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.Dictionary;

	import flashx.textLayout.formats.TextAlign;

	public class PlayerControls extends Sprite
	{
		public static const CONTROL_BAR_HEIGHT:Number = 30.0;
		public static const WATERMARK_WIDTH:Number = 26.0;
		
		private static const ICON_PADDING:Number = 20.0;
		private static const ICON_SPACING:Number = 10.0;
		
		public static const ANIMATION_DURATION:Number = 0.25;
		public static const ANIMATION_TRANSITION:String = "easeInOutQuart";
		
		private var _bar:GlassBar;
		private var _overlay:Sprite;
		private var _barName:SimpleButton;
		private var _buttonContainer:HBox;
		private var _logoButton:SimpleButton;
		private var _watermarkButton:SimpleButton;
		private var _watermarkPosition:String;
		private var _audioButtonContainer:DisplayObjectContainer;
		private var _audioOnButton:SimpleButton;
		private var _audioOffButton:SimpleButton;
		private var _embedButton:SimpleButton;
		private var _shareButton:SimpleButton;
		private var _popupButton:SimpleButton;
		private var _fullScreenButtonContainer:DisplayObjectContainer;
		private var _fullScreenButton:SimpleButton;
        private var _hoverMessageContainer:DisplayObjectContainer;
        private var _hoverMessage:LightLabel;
        private var _regScreenButton:SimpleButton;
		private var _playerMessage:PlayerMessage;
		private var _clipboardControl:ClipboardCopyMessage;
		private var _dialogEmbedIcon:DisplayObject;
		private var _dialogLinkIcon:DisplayObject;
		private var _embedPermission:Boolean = false;
        private var _iconMessageLookup:Dictionary = new Dictionary();
		
		public function PlayerControls()
		{
			_overlay = new Sprite();
			_overlay.graphics.beginFill(0xff0000, 0);
			_overlay.graphics.drawRect(0, 0, 1, 1);
			_overlay.graphics.endFill();
			_overlay.mouseChildren = _overlay.mouseEnabled = false;
			
			_bar = new GlassBar();
			_logoButton = new SimpleButton(PlayerIcons.logoIcon, PlayerIcons.logoIconOver, PlayerIcons.logoIconDown, PlayerIcons.hitArea);
			_barName = new SimpleButton(PlayerIcons.barNameIcon);

			_dialogEmbedIcon = PlayerIcons.dialogEmbedWhiteIcon;
			_dialogLinkIcon = PlayerIcons.dialogLinkWhiteIcon;

			var watermark:DisplayObject = PlayerIcons.logoIcon;
			var watermarkScale:Number = WATERMARK_WIDTH/watermark.width;
			watermark.scaleX = watermark.scaleY = watermarkScale;
			watermark.filters = [new GlowFilter(0, 1.0, 2.0, 2.0, 1.5, 3)];
			watermark.alpha = 0.5;
			var hitArea:DisplayObject = PlayerIcons.hitArea;
			watermarkScale = 50/hitArea.width;
			hitArea.scaleX = hitArea.scaleY = watermarkScale;
			_watermarkButton = new SimpleButton(watermark, watermark, watermark, hitArea);

            _hoverMessageContainer = new Sprite();
            _hoverMessage = new LightLabel(400, 0xffffff, false, 15);
            _hoverMessage.textAlign = TextAlign.LEFT;
            _hoverMessage.setPaddingParams({left:0, top:5, bottom:0, right:0});
            _hoverMessage.text = "";

			_audioButtonContainer = new Sprite();
			_audioOnButton = new SimpleButton(PlayerIcons.audioOnIcon, PlayerIcons.audioOnIconOver, PlayerIcons.audioOnIconDown, PlayerIcons.hitArea);
			_audioOffButton = new SimpleButton(PlayerIcons.audioOffIcon, PlayerIcons.audioOffIconOver, PlayerIcons.audioOffIconDown, PlayerIcons.hitArea);
			
			_embedButton = new SimpleButton(PlayerIcons.embedIcon, PlayerIcons.embedIconOver, PlayerIcons.embedIconDown, PlayerIcons.hitArea);
			_shareButton = new SimpleButton(PlayerIcons.shareIcon, PlayerIcons.shareIconOver, PlayerIcons.shareIconDown, PlayerIcons.hitArea);
			_popupButton = new SimpleButton(PlayerIcons.popupIcon, PlayerIcons.popupIconOver, PlayerIcons.popupIconDown, PlayerIcons.hitArea);
			
			_fullScreenButtonContainer = new Sprite();
			_fullScreenButton = new SimpleButton(PlayerIcons.fullScreenIcon, PlayerIcons.fullScreenIconOver, PlayerIcons.fullScreenIconDown, PlayerIcons.hitArea);
			_regScreenButton = new SimpleButton(PlayerIcons.regScreenIcon, PlayerIcons.regScreenIconOver, PlayerIcons.regScreenIconDown, PlayerIcons.hitArea);

			if (this.stage) initialize();
			else this.addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(e:Event = null):void
		{
			if (e) e.target.removeEventListener(Event.ADDED_TO_STAGE, initialize);
			this.alpha = 0;
			super.visible = false;
			
			var mainShadow:DropShadowFilter = new DropShadowFilter(0.0, 270, 0, 0.5, 10.0, 10.0, 0.9, 3);
			var buttonShadow:DropShadowFilter = new DropShadowFilter(1,80,0,0.6,2,2,2,0.5);
			
			this.addChild(_watermarkButton);
			_watermarkButton.addEventListener(MouseEvent.MOUSE_OVER, handleWatermarkMouseOver);
			
			_bar.filters = [mainShadow];
			_bar.height = CONTROL_BAR_HEIGHT;
			_bar.mouseChildren = _bar.mouseEnabled = false;
			_bar.visible = false;
			_bar.alpha = 0;
			this.addChild(_bar);
			
			//var logoContainer:HBox = new HBox(_bar, 0, 0, ICON_SPACING);
			var logoContainer:HBox = new HBox(_bar, 0, 0, ICON_SPACING*2);
			logoContainer.alignment = HBox.MIDDLE;
			logoContainer.addChild(_logoButton);
			//logoContainer.addChild(_barName);
			_bar.addChild(_barName);
			_barName.y = (_bar.height - _barName.height)/2;
			_barName.visible = false;
			logoContainer.x = ICON_PADDING;
			logoContainer.y = (this.height - logoContainer.height)/2 - 1;
			logoContainer.filters = [buttonShadow];
			
			_logoButton.addEventListener(MouseEvent.MOUSE_OVER, handleLogoMouseOver);
			_logoButton.addEventListener(MouseEvent.MOUSE_OUT, handleLogoMouseOut);
			_logoButton.addEventListener(MouseEvent.CLICK, handleLogoClick);

			//_buttonContainer = new HBox(_bar, 0, 0, ICON_SPACING);
			_buttonContainer = new HBox(logoContainer, 0, 0, ICON_SPACING);
			_buttonContainer.alignment = HBox.MIDDLE;

			_audioButtonContainer.addChild(_audioOnButton);
			_buttonContainer.addChild(_audioButtonContainer);
			_audioButtonContainer.addEventListener(MouseEvent.CLICK, handleAudio);
            setupIconHover(_audioOnButton,PlayerIconTips.AUDIO);

			embedPermission = _embedPermission;
			
			_buttonContainer.addChild(_shareButton);
			_shareButton.addEventListener(MouseEvent.CLICK, handleShare);
            setupIconHover(_shareButton,PlayerIconTips.LINK);

            const hasDomainCapabilities:Boolean = TaconiteFactory.getEnvironmentImplementation().hasDomainCapabilities;
			if (hasDomainCapabilities) {
				_buttonContainer.addChild(_popupButton);
				_popupButton.addEventListener(MouseEvent.CLICK, handlePopup);
                setupIconHover(_popupButton,PlayerIconTips.POPUP_PLAYER);
            } else {
				_popupButton.mouseEnabled = false;
			}
			
			_buttonContainer.filters = [buttonShadow];
			
			// only show full screen control if capability exists
			if (this.stage.allowsFullScreen // whether or not this is allowed
				&& (this.stage.stageWidth < stage.fullScreenWidth
					|| this.stage.stageHeight < stage.fullScreenHeight)) { // whether or not this is useful :)
				_buttonContainer.addChild(_fullScreenButtonContainer);
				fullScreen = false;
				_fullScreenButtonContainer.addEventListener(MouseEvent.CLICK, handleToggleFullScreen);
            }

            _hoverMessageContainer.addChild(_hoverMessage);
            _buttonContainer.addChild(_hoverMessageContainer);

            _playerMessage = new PlayerMessage();
			ApplicationController.addEventListener(NotificationNamesApplication.NOTIFY_NO_EMBED_PERMISSION, handleNoEmbedPermission);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOAD_FAILED, handleLoadFailed); 
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOADING, handleProjectLoading); 
			handleMeasure();
		}

        private function handleIconMouseIn(e:MouseEvent):void {
            if(e.target && _iconMessageLookup[e.target])
                _hoverMessage.text = _iconMessageLookup[e.target];
        }

        private function setupIconHover(icon:DisplayObject,message:String):void {
            _iconMessageLookup[icon] = message;
            icon.addEventListener(MouseEvent.MOUSE_OVER, handleIconMouseIn);
            icon.addEventListener(MouseEvent.MOUSE_OUT, handleIconMouseOut);
        }

        private function handleIconMouseOut(e:MouseEvent = null):void {
            _hoverMessage.text = "";
        }
		
		private function handleNoEmbedPermission(e:Event):void
		{
			showPlayerMessage("Embed",
				"Permission to embed is no longer available.");
		}
		
		private function handleLoadFailed(e:Event):void
		{
			showPlayerMessage("Oops! Sorry to say...",
				"This App is no longer available for viewing. It may have been unpublished, " +
				"or published as a different item. You may want to check with the original author.");
		}
		
		private function showPlayerMessage(headerText:String, bodyText:String, delay:int = 5000):void
		{
			if (!stage || TestPanelController.testRunning) return;
			stage.addChild(_playerMessage);
			_playerMessage.headerText = headerText;
			_playerMessage.bodyText = bodyText;
			if (!_playerMessage.visible) _playerMessage.visible = true;
			_playerMessage.dismissAfterDelay(delay);
		}
		
		private function handleProjectLoading(e:Event):void
		{
			if (!_playerMessage || stage == null || !stage.contains(_playerMessage)) return;
			_playerMessage.visible = false;
		}
		
		private function handleToggleFullScreen(e:Event):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_FULL_SCREEN));
            handleIconMouseOut();
		}
		
		private function handleAudio(e:Event):void
		{
            handleIconMouseOut();
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_AUDIO));
			Application.instance.audioEnabled = !Application.instance.audioEnabled;
			while (_audioButtonContainer.numChildren > 0)
				_audioButtonContainer.removeChildAt(0);
			
			if (Application.instance.audioEnabled)
				_audioButtonContainer.addChild(_audioOnButton);
			else
				_audioButtonContainer.addChild(_audioOffButton);
		}
		
		private function handleEmbed(e:Event):void
		{
            handleIconMouseOut();
            const document:Document = Application.instance.document;
			if (!document || !document.project) return;
			const gadget:Project = document.project;
			if (!gadget.apnUUID) return; // requires a valid purchase ID
			const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			
			const embedURI:String = environment.getEmbedURIForPurchaseId(gadget.apnUUID);
			if (!embedURI) return;

			var width:int = gadget.width>0?gadget.width:400;
			var height:int = gadget.height>0?gadget.height:250;
			var titleText:String = "Click to copy code to embed this app in other sites.";

			showClipboardControl(titleText, _dialogEmbedIcon,
				"<iframe title='"+GadgetDescription.cleanName(gadget, true)+"' class='zapp' \n" +
				"type='text/html' width='" + width + "px' height='" + height + "px' \n" +
				"src='" + embedURI + "' \n" + 
				"frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>");
		}

		private function handleShare(e:Event):void
		{
            handleIconMouseOut();
            const document:Document = Application.instance.document;
			if (!document || !document.project) return;
			const gadget:Project = document.project;
			if (!gadget.published) return; // requires a valid purchase ID
			
			const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			const detailsURI:String = environment.getDetailsURIForPublishedId(gadget.projectId);
			if (!detailsURI) return;
			
			var operation:ShortURLOperation = new ShortURLOperation(detailsURI);
			operation.addEventListener(Event.COMPLETE, handleShortURLComplete);
			operation.execute();
		}
		
		private function handleShortURLComplete(e:Event):void
		{
			var operation:ShortURLOperation = e.target as ShortURLOperation;
			if (!operation) return;
			var titleText:String = "Click to copy a direct link to this app's Detail Page.";
			showClipboardControl(titleText, _dialogLinkIcon, operation.url);
		}
		
		private function showClipboardControl(titleText:String, titleIcon:DisplayObject, buffer:String):void
		{
			if (!buffer) return;
			clipboardControl.headerText = titleText;
			clipboardControl.headerIcon = titleIcon;
			clipboardControl.buffer = buffer;
			if (!clipboardControl.stage) stage.addChild(clipboardControl);
			clipboardControl.visible = true;
			handleMeasure();
		}
		
		private function get clipboardControl():ClipboardCopyMessage
		{
			if (!_clipboardControl) {
				_clipboardControl = new ClipboardCopyMessage();
			}
			return _clipboardControl;
		}
		
		private function handlePopup(e:Event):void
		{
            handleIconMouseOut();
            const document:Document = Application.instance.document;
			if (!document || !document.project) return;
			const gadget:Project = document.project;
			TaconiteFactory.getWindowImplementation().requestNewWindowAndInvokeId(gadget.apnUUID, {width:gadget.width, height:gadget.height});
		}
		
		private function handleWatermarkMouseOver(e:Event):void
		{
			var stageHeight:Number = stage.stageHeight;
			if (this.parent.scrollRect)
				stageHeight = this.parent.scrollRect.height;
			
			if (_overlay && !this.contains(_overlay))
				this.addChild(_overlay);
			_bar.visible = true;
			Tweener.addTween(_watermarkButton, {alpha:0, y:(stageHeight - 10 - _watermarkButton.height*2), time:0.3, transition:"easeOutQuart",
				onComplete:function():void {
					_watermarkButton.visible = false;
					_watermarkButton.mouseEnabled = false;
				}});
			Tweener.addTween(_bar, {alpha:0.8, y:(stageHeight - PlayerControls.CONTROL_BAR_HEIGHT), time:0.3, transition:"easeOutQuart",
				onComplete:function():void {
					_overlay.addEventListener(MouseEvent.MOUSE_OVER, handleOverlayMouseOver);
					_overlay.mouseChildren = _overlay.mouseEnabled = true;
					_bar.mouseChildren = _bar.mouseEnabled = true;
				}});
		}
		
		private function handleOverlayMouseOver(e:MouseEvent):void
		{
			if (e  && ((e.stageX == 0
				     && e.stageY == 0
					 && e.localX == 0
					 && e.localY == 0)
					 || _bar.hitTestPoint(e.stageX, e.stageY))) {
				return; // ignore inadvertent origin events which occur in some browsers, and erroneous mouse over from control bar
			}
			if (!_watermarkButton || !stage) return;
				
			var stageHeight:Number = stage.stageHeight;
			if (this.parent.scrollRect)
				stageHeight = this.parent.scrollRect.height;
			
			_bar.mouseChildren = _bar.mouseEnabled = false;
			_overlay.mouseChildren = _overlay.mouseEnabled = false;
			_overlay.removeEventListener(MouseEvent.MOUSE_OVER, handleOverlayMouseOver);
			if (_overlay && this.contains(_overlay))
				this.removeChild(_overlay);
			showWatermarkIfNotAlreadyShowing();
			
			Tweener.addTween(_bar, {alpha:0, y:(stageHeight - PlayerControls.CONTROL_BAR_HEIGHT + _bar.height), time:0.5, transition:"easeOutQuart",
				onComplete:function():void {
					_bar.visible = false;
				}});
		}
		
		private function showWatermarkIfNotAlreadyShowing():void
		{
			if (_watermarkButton.visible) return;
			var stageHeight:Number = stage.stageHeight;
			if (this.parent.scrollRect)
				stageHeight = this.parent.scrollRect.height;
			_watermarkButton.visible = true;
			Tweener.addTween(_watermarkButton, {alpha:1, y:(stageHeight - 10 - _watermarkButton.height), time:0.5, transition:"easeOutQuart",
				onComplete:function():void {
					_watermarkButton.mouseEnabled = true;
				}});
		}
		
		private function handleLogoMouseOver(e:Event):void
		{
			_barName.alpha = 0;
			_barName.visible = true;
			Tweener.addTween(_barName,{alpha:1, time:0.5, transition:"easeOutQuart"});
		}
		
		private function handleLogoMouseOut(e:Event):void
		{
			Tweener.addTween(_barName,{alpha:0, time:0.5, transition:"easeOutQuart",
				onComplete:function():void {
					_barName.visible = false;
				}});
		}
		
		private function handleLogoClick(e:Event):void
		{
            handleIconMouseOut();
            const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			// POSSIBLE TODO: trigger browser to display zebrazapps site at environment level: web implementation triggers in-browser. desktop triggers in web view.
			navigateToURL(new URLRequest("http://" + environment.serverName), "_blank");
		}
		
		private function set embedPermission(value:Boolean):void
		{
			_embedPermission = value;
			if (!_embedButton) return;
			_embedButton.mouseEnabled = value;
			_embedButton.visible = value;
			if (value) {
				if (!_buttonContainer.contains(_embedButton))
					_buttonContainer.addChildAt(_embedButton, 1);
				_embedButton.addEventListener(MouseEvent.CLICK, handleEmbed);
                setupIconHover(_embedButton,PlayerIconTips.EMBED);
                _embedButton.mouseEnabled = true;
			} else {
				if (_buttonContainer.contains(_embedButton))
					_buttonContainer.removeChild(_embedButton);
				_embedButton.removeEventListener(MouseEvent.CLICK, handleEmbed);
				_embedButton.mouseEnabled = false;
			}
			_buttonContainer.draw();
			handleMeasure();
		}
			
		public function set fullScreen(value:Boolean):void
		{
			handleOverlayMouseOver(null);

			while (_fullScreenButtonContainer.numChildren)
				_fullScreenButtonContainer.removeChildAt(0);
			
			// kill the old buttons because they aren't behaving. specifically, they never get a mouse out so they remain lit following toggle.
			_fullScreenButton = null;
			_regScreenButton = null;
			_fullScreenButton = new SimpleButton(PlayerIcons.fullScreenIcon, PlayerIcons.fullScreenIconOver, PlayerIcons.fullScreenIconDown, PlayerIcons.hitArea);
			_regScreenButton = new SimpleButton(PlayerIcons.regScreenIcon, PlayerIcons.regScreenIconOver, PlayerIcons.regScreenIconDown, PlayerIcons.hitArea);
			if (value)
				_fullScreenButtonContainer.addChild(_regScreenButton);
			else
				_fullScreenButtonContainer.addChild(_fullScreenButton);

			var fullScreenMessage:String = PlayerIconTips.FULL_SCREEN;

			if (!ApplicationUI.allowsFullScreenInteractive) {
				fullScreenMessage = PlayerIconTips.FULL_SCREEN_KEYBOARD_INACTIVE;
			}

            setupIconHover(_fullScreenButton, fullScreenMessage);
            setupIconHover(_regScreenButton, PlayerIconTips.NORMAL_SCREEN);
        }
		
		public function handleMeasure():void
		{
			if (!stage) return;
			
			var stageWidth:Number = stage.stageWidth;			
			var stageHeight:Number = stage.stageHeight;
			if (this.parent.scrollRect) {
				stageWidth = this.parent.scrollRect.width;
				stageHeight = this.parent.scrollRect.height;
			}
			
			_overlay.width = stageWidth;
			_overlay.height = stageHeight - 60;
			
			switch (_watermarkPosition) {
				case "right":
					_watermarkButton.x = stageWidth - WATERMARK_WIDTH - ICON_SPACING;
					break;
				case "left":
				default:
					_watermarkButton.x = ICON_SPACING;
					break;
			}
			_watermarkButton.y = stageHeight - 10 - _watermarkButton.height;
			
			_bar.width = stageWidth;
			_bar.y = stageHeight - PlayerControls.CONTROL_BAR_HEIGHT + _bar.height;
			
			//_buttonContainer.x = stageWidth - _buttonContainer.width - ICON_PADDING; // right justify buttons
			_barName.x = stageWidth - _barName.width - ICON_PADDING; // right justify url
			showWatermarkIfNotAlreadyShowing();
			
			if (_playerMessage)
				_playerMessage.handleMeasure();
			if (_clipboardControl)
				_clipboardControl.handleMeasure();
		}
			
		override public function set visible(value:Boolean):void
		{
			if (value) {
				const document:Document = Application.instance.document;
				if (!document || !document.project) return;
				const gadget:Project = document.project;
				const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
				
				embedPermission = gadget.permissions.canEmbed;
				
				_watermarkPosition = gadget.watermarkPosition;
				if (!environment.invokedAsEditor) { // in the Player, metadata gets precedence. in the Editor we want to show a preview
					if(gadget.watermarkPosition) {
                        _watermarkPosition = gadget.watermarkPosition;
                    }
				}
				super.visible = true;
				this.mouseChildren = true;
				this.mouseEnabled = true;
				
				handleMeasure();
				Tweener.addTween(this, {alpha:1, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION});
			} else {
				if (alpha == 0) {
					super.visible = false;
					this.mouseChildren = false;
					this.mouseEnabled = false;
				} else {
					Tweener.addTween(this, {alpha:0, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION,
						onComplete:function():void {
							visible = false;
						}});
				}
			}
		}
	}
}
