package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	public class AbstractSwitch extends AbstractRibbonControl
	{	
		private static const BTN_SPACING:Number = 13;

		protected var _firstPosition:Number;
		private var _buttons:Array;
		private var _resetButton:SwitchButton;
		private var _rollover:Boolean = false;  // true while mouse is over the switch
		
		private var _value:Object; // The value being displayed
		private var _valueType:String; // The value type this switch is showing (Default, Init, Current[different from init])
		
		private var _w:Number;
		private var _h:Number;
		
		private var _showReset:Boolean;
		
		public function AbstractSwitch(setValueCallback:Function, buttons:Array, showReset:Boolean=true, firstPosition:Number=76)
		{
			super(setValueCallback);
			_firstPosition = firstPosition;
			_buttons = buttons;
			_showReset = showReset;
			
			if (_showReset)
				_resetButton = new SwitchButton(valueChanged, drawResetButton, "reset", {resetButton:true});

			addButtons();
			
		// Switch mouse events
			this.addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
			
		// Draw a background
			drawBackground();
		}
		
		protected function drawBackground():void
		{
//			var w:Number = RibbonView.VALUE_WIDTH + offset;
//			var h:Number = RibbonView.HEIGHT;
//
//			graphics.clear();
//			graphics.beginFill(0x111111,0.5);
//			graphics.drawRect(_firstPosition-offset,0,w,h);
		}
		
		protected function get offset():Number
		{
			return 0;
		}
		
		public function set firstPosition(value:Number):void
		{
			_firstPosition = value;
			positionButtons();
		}
		
	/*	override public function set width(value:Number) : void
		{
			drawBackground(value, _h);
		}*/
		

		public function addButtons():void {
			
			var btn:SwitchButton;
			for each (btn in _buttons) {
				btn.addEventListener(MouseEvent.CLICK, btnClickListener); // To be replaced by a switch mediator
				addChild(btn);
			}
			
			// Add reset button
			if (_showReset)
				addChild(_resetButton);
			
			positionButtons();
		}

		
	// To be replaced by a switch mediator	
		private function btnClickListener(e:MouseEvent):void 
		{
			var btn:SwitchButton = e.currentTarget as SwitchButton;
			btn.drawButton();
			setValue(btn.value, RibbonView.INIT);
		}

		private function rollOverListener(e:MouseEvent):void 
		{
			if (!_positionInProgress) {
				_rollover = true;
				positionButtons();

				this.addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
				this.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);
				if (_showReset)
					_resetButton.addEventListener(MouseEvent.CLICK, mouseClickReset);
			}
		}
		
		private function rollOutListener(e:MouseEvent):void 
		{
			if (!_positionInProgress) {
				_rollover = false;
				positionButtons();

				this.removeEventListener(MouseEvent.ROLL_OUT, rollOutListener);
                if(this.stage != null){
				    this.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);
                }
                if (_showReset)
                    _resetButton.removeEventListener(MouseEvent.CLICK, mouseClickReset);
			}
		}

		private function mouseMoveListener(e:MouseEvent):void
		{
			// in case we rolled out during an animation, this will catch that case and close the button
			if (Math.abs(e.localX - width/2) > 3*RibbonView.HEIGHT 
					|| Math.abs(e.localY - height/2) > 3*RibbonView.HEIGHT) {
				rollOutListener(e);
			}
		}
		
		/**
		 * Decide whether a button should be visible, considering current value and rollover state.
		 * This function is overridden by AlignSwitch. 
		 * @param buttonValue
		 * @param currentValue
		 * @param valueType
		 * @param rollover
		 * @return 
		 * 
		 */
		protected function buttonVisible(buttonValue:Object, currentValue:Object, valueType:String, rollover:Boolean):Boolean 
		{	
			// the reset button only shows when there is an initial value
			if (buttonValue == "reset")
				return (rollover);  // && valueType==RibbonView.INIT);   // tbd: accurate knowledge of existence of init
			
			// during rollover show all buttons, else just show the button for the current value
			if (rollover)
				return true;
			else
				return (buttonValue == currentValue);
		}
		
		/**
		 * Compute an integer column number for the specified button value.
		 * Column zero is the main column.
		 * Legal values are -1 (left of main), 0 (main) and 1 (right of main) and 2 (for reset button) 
		 * @param buttonValue
		 * @param currentValue
		 * @param valueType
		 * @param rollover
		 * @return 
		 * 
		 */
		protected function buttonColumn(buttonValue:Object, currentValue:Object, valueType:String, rollover:Boolean):int 
		{	
			if (buttonValue == "reset")
				return 2;  // the reset button is always at +2
			
			// this should be overridden for other than the boolean switch
			if (buttonValue == currentValue)
				return 0;  // the current value is shown
			else
				return 1;  // for boolean, the other value is at +1
		}
		
		
		/**
		 * Position the buttons in the switch and show/hide them with tweening.
		 * This function is used for initial placement and for rollin, rollout.
		 */
		private function positionButtons():void {

			// set position and visibility of each button in the switch
			for each (var btn:SwitchButton in _buttons) {
				positionBtn(btn);
			}
			
			// the reset button
			if (_showReset)
				positionBtn(_resetButton);
		}
	
		private function positionBtn(btn:SwitchButton):void 
		{	
			var vis:Boolean = buttonVisible(btn.value, _value, _valueType, _rollover);
			var col:int = buttonColumn(btn.value, _value, _valueType, _rollover);
			
			btn.fade(vis);  // show/hide this button
			tweenButton(btn, _firstPosition + col*BTN_SPACING);  // position button to specified column
		}
		
		private var _positionInProgress:Boolean = false;
		private function tweenButton(btn:SwitchButton, destX:Number):void 
		{	
			_positionInProgress = true;
			Tweener.addTween(btn, {x:destX, time:0.25, transition:"easeOutExpo",
                onComplete:function():void {
                	_positionInProgress = false;
                }});
		}
				
		protected function drawResetButton(letter:Sprite, color:uint):void 
		{	
		// Reset button dimensions
			var nW:Number = 5;
			var nH:Number = 5;
			var nX:Number = 5.5;
			var nY:Number = 5.5;
			var nR:Number = 5.5;
				
			letter.graphics.lineStyle(1.6, color);
			letter.graphics.moveTo(nX-2.5,nY-2.5);
			letter.graphics.lineTo(nX+nW-2.5,nY+nH-2.5);
			letter.graphics.moveTo(nX+nW-2.5,nY-2.5);
			letter.graphics.lineTo(nX-2.5,nY+nH-2.5);	
		}
		
		private function mouseClickReset(e:MouseEvent):void 
		{
			doReset();
			valueChanged(_value);
		}
		
		/**
		 * Overridden by align switch. 
		 * 
		 */
		protected function doReset():void 
		{	
			// Doesn't affect model. Just changes display to DEFAULT valueType
			// However, depending on setValue gets the valueType argument, this may need to affect the model
			displayAsDefaultValue(_value, true);
			_valueType = RibbonView.DEFAULT;
		}
		
		// Fired on model update. We need to know if the update is setting an initial value, a current value (different from the init)
		// or a default value.
		override public function setValue(value:Object, valueType:String):void 
		{
			var val:String = String(value);
			
			if (val != "reset") {
				_value = val;	
			}
			
			_valueType = valueType;
			
			displayAsInitialValue(val);
			
			switch (_valueType) {
				case RibbonView.DEFAULT:	displayAsDefaultValue(val);	break; // Update switch showing current value (no init)
				case RibbonView.INIT:	 	displayAsInitialValue(val);	break; // Update switch showing initial value (same as current value)
				case RibbonView.CURRENT:	displayAsCurrentValue(val);	break; // Update switch showing current value (conflicts with initial value)
			}
		}
		
		private function displayAsDefaultValue(value:Object, show:Boolean=false):void 
		{	
			positionButtons();
			
			var btn:SwitchButton;
			
			for each (btn in _buttons) {
				btn.applyDefaultValueColors();
			}	
		}
		
		private function displayAsInitialValue(value:Object):void 
		{
			positionButtons();
			
			var btn:SwitchButton;
			
			for each (btn in _buttons) {
				if (btn.value == value) {
					btn.displayedAsInitial = true;
				}
				else {
					btn.displayedAsInitial = false;
				}
			}
		}
		
		private function displayAsCurrentValue(value:Object):void 
		{	
			positionButtons();
						
			var btn:SwitchButton;
			
			for each (btn in _buttons) {
				if (btn.value == value) {
					btn.displayedAsCurrent = true;
				}
				else {
					btn.displayedAsCurrent = false;
				}
			}
		}
		
	/*	override public function set x(value:Number):void
		{
			super.x = value;
			trace("SETTING SWITCH X="+value);
		}*/
	}
}