package com.alleni.author.view.ui.controls
{
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.view.feedback.TransformationGhost;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.MenuButton;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	
	import mx.core.IVisualElement;
	import mx.core.IVisualElementContainer;
	import mx.core.UIComponent;
	
	
	public class TransformationEditorPanel extends UIComponent
	{
		public static var instance:TransformationEditorPanel;
		private var _appContainer:IVisualElementContainer;
		
		private var _originalTransformation:PositionAndAngle;
		private var _transformation:PositionAndAngle;

		private var _ghost:TransformationGhost;
		
		private var _arrow:Sprite = new Sprite();  // arrow on top edge that points at the ribbon being edited
		private var _callback:Function;
		private var _closeCallback:Function;
		
		
		private var _xText:RichTextField;
		private var _yText:RichTextField;
		private var _rotationText:RichTextField;
		
		private var _mouseXOffset:Number = 0;
		private var _mouseYOffset:Number = 0;
				
		// dimensions of whole panel
		private static const WIDTH:Number = 255;
		private static const HEIGHT:Number = 198;
		private static const CORNER_RADIUS:Number = 8;
		private static const BACKGROUND_COLOR:uint = 0x000000;
		private static const BACKGROUND_EDGE_COLOR:Number = 0x828282;
		private static const ARROW_X:Number = 50;  // arrow on top edge that points at the ribbon being edited
		
		private static const DIVIDER_LINE_COLOR:Number = 0x585858;
		private static const DIVIDER_LINE_Y:Number = 58;
		private static const DIVIDER_LINE2_Y:Number = 166;
		
		// locations of controls
		private static const OK_X:Number = 202;
		private static const OK_Y:Number = 172;
		private static const OK_WIDTH:Number = 44;
		private static const OK_HEIGHT:Number = 20;
		private static const CANCEL_X:Number = 150;
		private static const CANCEL_Y:Number = 172;
		private static const CANCEL_WIDTH:Number = 44;
		private static const CANCEL_HEIGHT:Number = 20;
		
		
		
		public function TransformationEditorPanel(appContainer:IVisualElementContainer)
		{
			Utilities.assert(instance == null);
			instance = this;
			super();
			_appContainer = appContainer;
			_appContainer.addElement(this);
			
			
			setupView();
						
			createButton(OK_X,OK_Y, drawOkButton, okAction, "OK");
			createButton(CANCEL_X,CANCEL_Y, drawCancelButton, cancelAction, "Cancel");
			
			drawArrow();
			addChild(_arrow);
			
			// dropshadow on whole table
			var dropShadow:DropShadowFilter = new DropShadowFilter();
			dropShadow.blurX = 15;
			dropShadow.blurY = 15;
			dropShadow.angle = 45;
			dropShadow.alpha = 0.90;
			dropShadow.distance = 6;
			this.filters = [dropShadow];
			
			ApplicationController.instance.addEventListener(NotificationNamesApplication.OPEN_PROJECT, closePanel);
			ApplicationController.instance.addEventListener(NotificationNamesApplication.SAVE_AS_PROJECT, closePanel);
			ApplicationController.instance.addEventListener(NotificationNamesApplication.SAVE_PROJECT, closePanel);
			ApplicationController.instance.addEventListener(NotificationNamesApplication.CREATE_NEW_PROJECT, closePanel);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			
		}
		private function onMouseDown(e:MouseEvent):void
		{
			if(hitTestPoint(e.stageX,e.stageY,true))
			{
				_mouseXOffset = e.stageX-x;
				_mouseYOffset = e.stageY-y;
				addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
				stage.addEventListener(MouseEvent.MOUSE_MOVE,handleMouseMove);
			}
			
		}
		private function onMouseUp(e:MouseEvent):void
		{
			removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			stage.removeEventListener(MouseEvent.MOUSE_MOVE,handleMouseMove);
		}
		
		private function handleMouseMove(e:MouseEvent):void
		{
			x = e.stageX - _mouseXOffset;
			y = e.stageY - _mouseYOffset;
		}

		
		public static function showPanel(appContainer:IVisualElementContainer, initialValue:PositionAndAngle, valueCallback:Function, closeCallback:Function, global:Point, ghost:TransformationGhost):void
		{
			closePanel();
			
			if (!instance)
				new TransformationEditorPanel(appContainer);
			else
				instance.visible = true;
			
			instance._callback = valueCallback;
			instance._closeCallback = closeCallback;
			instance._ghost = ghost;
			
			// place the tip of the arrow at "global" which is the center of the color chip in the ribbon
			instance.x = global.x - ARROW_X - 10;
			instance.y = global.y + 10;
			
			instance.finishShowPanel(initialValue);
		}
		
		private function finishShowPanel(initialValue:PositionAndAngle):void
		{
			if(initialValue == null){
                initialValue = new PositionAndAngle();
            }

			// force panel to be within screen bounds
			_arrow.visible = true;
			var containerElement:IVisualElement = _appContainer as IVisualElement;
			if (x > containerElement.width - WIDTH) {
				x = containerElement.width - WIDTH;
				_arrow.visible = false;  // suppress the arrow if we are forcing the panel location
			}
			if (y > containerElement.height - HEIGHT - ColorPicker.HEIGHT - ColorPicker.ARROW_HEIGHT) {  // leave room below for the color picker
				y = containerElement.height - HEIGHT - ColorPicker.HEIGHT - ColorPicker.ARROW_HEIGHT;
				_arrow.visible = false;  // suppress the arrow if we are forcing the panel location
			}
			
			_originalTransformation = initialValue.clone();  // needed for Cancel button
			_transformation = initialValue.clone();
			updateView();
			ApplicationController.instance.captureMouseClicks(true, backgroundClickListener);
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
		}
		
		public static function closePanel(event:ApplicationEvent = null):void
		{
			if (instance)
				instance.closeThisPanel(true);
		}
		
		
		private function closeThisPanel(cancel:Boolean=true):void
		{
			if (_closeCallback != null) {
				_closeCallback(cancel);
				_closeCallback = null;
			}
			this.visible = false;
			_callback = null;
			ApplicationController.instance.captureMouseClicks(false, backgroundClickListener);
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
		}
		
		public function set transformation(value:PositionAndAngle):void
		{
			instance._transformation = value.clone();
			instance.updateView();
		}
						
		private function setupView():void
		{
			var label:LightLabel
			
			label= createLabel(44,15, "Drag ghost ...");
			addChild(label);
			label = createLabel(22, 72, "Location-X(H)");
			addChild(label);
			label = createLabel(22, 106, "Location-Y(V)");
			addChild(label);
			label = createLabel(22, 140, "Rotate degrees");
			addChild(label);
			
			_xText = createTextField(132, 72, 70, 15, xEditListener);
			addChild(_xText);
			_yText = createTextField(132, 106, 70, 15, yEditListener);
			addChild(_yText);
			_rotationText = createTextField(132, 140, 70, 15, rotationEditListener);
			addChild(_rotationText);
			_rotationText.visible = true;
		}
		
		
		private function xEditListener(value:String):void
		{
			_transformation.x = Number(FormattedText.convertToString(value));
			doCallback();
		}
		
		private function yEditListener(value:String):void
		{
			_transformation.y = Number(FormattedText.convertToString(value));
			doCallback();
		}
		
		private function rotationEditListener(value:String):void
		{
			_transformation.angle = Number(FormattedText.convertToString(value));
			doCallback();
		}
		
		private function doCallback():void
		{
			if (_callback != null)
				_callback(_transformation);
		}
		
		
		private function updateView():void
		{
			drawBackground(this.graphics);
			
			if (_xText) {
				_xText.rawText = formatNumber(_transformation.x);
				_yText.rawText = formatNumber(_transformation.y);
				_rotationText.rawText = formatNumber(_transformation.angle); // _transformation.angle.toString();
			}
		}
		
		
		private function drawBackground(graphics:Graphics):void
		{
			// background of whole panel
			graphics.clear();
			var hhh:Number = HEIGHT; 
			graphics.lineStyle(1, BACKGROUND_EDGE_COLOR);
			graphics.beginFill(BACKGROUND_COLOR);  
			graphics.moveTo(0, 0);
			graphics.lineTo(WIDTH, 0);
			graphics.lineTo(WIDTH, hhh-CORNER_RADIUS);
			graphics.curveTo(WIDTH, hhh, WIDTH-CORNER_RADIUS, hhh);
			graphics.lineTo(CORNER_RADIUS, hhh);
			graphics.curveTo(0, hhh, 0, hhh-CORNER_RADIUS);
			graphics.lineTo(0, 0);
			graphics.endFill();
			
			// horz divider lines
			graphics.moveTo(-1,DIVIDER_LINE_Y);
			graphics.lineTo(WIDTH-1,DIVIDER_LINE_Y);
			
			graphics.moveTo(-1,DIVIDER_LINE2_Y);
			graphics.lineTo(WIDTH-1,DIVIDER_LINE2_Y);
		}
		
		private function drawArrow():void
		{
			// arrow on top edge that points at the ribbon being edited
			_arrow.graphics.beginFill(0x000000,.9);
			_arrow.graphics.moveTo(0,11);
			_arrow.graphics.lineTo(10,0);
			_arrow.graphics.lineTo(19,11);
			_arrow.graphics.endFill();
			_arrow.graphics.lineStyle(1,0x828282);
			_arrow.graphics.moveTo(0,11);
			_arrow.graphics.lineTo(10,0);
			_arrow.graphics.lineTo(19,11);
			_arrow.x = ARROW_X;
			_arrow.y = -10;
		}
		
		
		private function createButton(xx:Number, yy:Number, drawButton:Function, action:Function, label:String):MenuButton
		{
			var btn:MenuButton = new MenuButton(drawButton,action);
			btn.toggle = false;
			btn.color = 0x00303030;
			btn.x = xx;
			btn.y = yy;
			btn.labelText = label;
			btn.labelFontSize = 11;
			btn.labelColor = 0xffffff;
			addChild(btn);
			return btn;
		}
		
		private function drawOkButton( btn:Sprite ):void
		{	
			const R:Number = 5; //rounding
			
			// fill the button with gray, round the left side corners
			btn.graphics.lineStyle(0,0,0);  // zero alpha, to eliminate line
			btn.graphics.drawRoundRect(0,0,OK_WIDTH,OK_HEIGHT,R);
		}
		
		private function drawCancelButton( btn:Sprite ):void
		{	
			const R:Number = 5; //rounding
			
			// fill the button with gray, round the left side corners
			btn.graphics.lineStyle(0,0,0);  // zero alpha, to eliminate line
			btn.graphics.drawRoundRect(0,0,CANCEL_WIDTH,CANCEL_HEIGHT,R);
		}
		
		private function okAction():void
		{
			closeThisPanel(false);
		}
		
		private function cancelAction():void
		{
			_transformation = _originalTransformation;
			doCallback();
			updateView();
			closeThisPanel(true);
		}
		
		
		public static function get isActive():Boolean
		{
			if (instance)
				return instance.visible;  // true if the fill panel is active (so MC tooltips should not show)
			else
				return false;
		}
		
		
		// **** various utilities
		
		
		private function createLabel(xxx:Number, yyy:Number, text:String):LightLabel
		{
			var label:LightLabel;
			label = new LightLabel();
			label.x = xxx;
			label.y = yyy;
			label.text = text;
			label.color = 0xffffff;
			return label;
		}
		
		
		private function createTextField(xxx:Number, yyy:Number, www:Number, hhh:Number, onCloseEditing:Function):RichTextField
		{
			var field:RichTextField = new RichTextField(www,hhh);
			field.embedFonts = true;
			field.x = xxx;
			field.y = yyy;
			field.text = "TEXT";
			field.color = 0xffffff;
			var mediator:TextEditMediator = new TextEditMediator();
			mediator.handleTextEditEvents(field, null, null, TextEditMediator.closeOnEnter, onCloseEditing,null,false);
			return field;
		}
		
		
		private function formatNumber(value:Number):String
		{
			var text:String = value.toString();
			if(isNaN(value))
			{
				return '';
			}
			else if (text.indexOf(".") != -1) {  // has decimal point
				// limit to one decimal place
				return text.replace(/(\.[0-9])[0-9]*/,"$1");
			} else {
				return text;
			}
		}

		private function backgroundClickListener(event:MouseEvent):void
		{
			// application shields the entire window so you can't interact
			// with anything accept the _ghost.
			if (_ghost.rotationHandle.hitTestPoint(event.stageX, event.stageY))
				_ghost.rotationHandle.dispatchEvent(event);
			else if (_ghost.hitTestPoint(event.stageX, event.stageY,true))
				_ghost.dispatchEvent(event);
		}

	}
}