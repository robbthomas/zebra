package com.alleni.author.view.ui.components
{
	import assets.appui.componentSkins.checkEmpty;
	import assets.appui.componentSkins.checkOnly;
	
	import flash.display.DisplayObjectContainer;
	
	public class DockCheckBox extends CheckBox
	{
		public function DockCheckBox(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, label:String="", defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, label, defaultHandler, checkEmpty, checkOnly);
		}
	}
}