package com.alleni.author.view.ui
{
import assets.appui.controlArea.fxg.viSize1Black;

import com.alleni.author.Navigation.EventFlowView;
import com.alleni.author.Navigation.EventPageView;
import com.alleni.author.Navigation.IPageView;
import com.alleni.author.Navigation.Pager;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.PathObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.persistence.GadgetEncoder;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldView;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class ObjectReferenceField extends RibbonValueField
	{
		private var _value:AbstractObject;
		private var _host:AbstractObject;
		private var _property:PropertyDescription;
		private var _hilitedView:ObjectView = null;
		
		public function ObjectReferenceField(setValueCallback:Function, width:Number, height:Number, background:Boolean=true, readOnly:Boolean=false, hostObject:AbstractObject=null, prop:PropertyDescription=null)
		{
			super(setValueCallback, width, height, background, readOnly);
			_host = hostObject;
			_property = prop;
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener, true);
		}
		
		private function mouseDownListener(event:MouseEvent):void
		{
			event.stopImmediatePropagation();  // prevent editing the title text in this ribbon
			openEditing();
		}

		private function get worldView():WorldView
		{
			return EditorUI.instance.presentationContainer.worldView;	
		}
		
		override public function openEditing():void
		{
			ApplicationController.instance.captureMouseClicks(true, backgroundClickListener);
            if(Application.running){
                AuthorController.instance.editWhileRunningStart();
            }
			if(_property.key == "snapTo") {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:735}));
			} else if(_property.key == "currentPage") {
                //TODO replace with better message
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:734}));
			} else {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:734}));
			}
			
			if (_property.key == "snapTo") {
				worldView.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener, false, 1, true);
			}
		}

		private function backgroundClickListener(event:MouseEvent):void
		{
			if (_host == null || _host.wireScopeContainer == null) {  // ribbon in Inspector, or the object has been deleted
				cleanup();
				return;  
			}
			
			var global:Point = new Point(event.stageX, event.stageY);
			var worldView:WorldView = EditorUI.instance.presentationContainer.worldView;
			var view:ObjectView;
			if (_property.key == "snapTo") {
				view = findPathObject(global, _host, worldView);
            } else if(_property.key == "currentPage") {
                view = findPageObject(global, _host, worldView);   // for Project MC, current Event ribbon
            } else {
                view = worldView.findViewUnderPoint(global);
                if (view is EventPageView || view is EventFlowView) {
                    view = null;  // enable clearing Collision ribbon by clicking the stage
                }
            }
			if (view && view != worldView) {
				var obj:AbstractObject = view.object;
				while (obj.childOfClosed)
					obj = obj.parent;
				if (obj.wireScopeContainer != _host.wireScopeContainer) {
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:732}));
					trace("not scope");
				} else {
					cleanup();
					valueChangedByClick(obj);
				}
			} else {
				cleanup();
				valueChangedByClick(null);
			}
            if(Application.uiRunning){
                AuthorController.instance.editWhileRunningEnd();
            }
		}
		
		private function valueChangedByClick(value:AbstractObject):void 
		{
			trace("ObjectReferenceField valueChangedByPanel value="+value);
			valueChanged(value);  // tell the model
			setValue(value, Modifiers.OBJECT_TYPE);  // tell the ribbon
			
			if (_value) {
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:737, inserts:[_value.title]}));
			} else {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:736}));
			}
		}

		
		override public function setValue(value:Object, valueType:String):void
		{
			trace("ObjectReferenceField setValue value="+value);
			// put a string into ribbon value
			_value = value as AbstractObject;
			if (value as AbstractObject)
				super.setValue(AbstractObject(value).title, valueType);
			else
				super.setValue("", valueType);
		}

        private function findPageObject(global:Point, host:AbstractObject, worldView:WorldView):ObjectView
        {
            var v:ObjectView = worldView.findViewUnderPoint(global);
            while(v != null) {
                // only pick v if it is one of the pages of the host object
                if(v is IPageView && IPageView(v).object.parent == host) {
                    return v;
                }
                v = v.parentObjectView;
            }
            return null;
        }
		
		private function findPathObject(global:Point, host:AbstractObject, worldView:WorldView):ObjectView
		{
			const MAX_DIST:Number = 15;
			
			var nearestPath:ObjectView = null;
			var nearestDist:Number = 999999999;
			var objects:Array = GadgetEncoder.collectChildren(host.wireScopeContainer, null, true, false);
			for each (var obj:AbstractObject in objects) {
				if (obj != host && obj.parent && obj.isAnimationPath) {
					var view:ObjectView = obj.getView();
					if (view) {
						var local:Point = view.globalToLocal(global);
						var frac:Number = obj.closestPointOnPath(local);
						var near:Point = obj.pathPointForFraction(frac);
						var nearGlobal:Point = view.localToGlobal(near);
						var dist:Number = Point.distance(global, nearGlobal);
						if (dist <= MAX_DIST && dist < nearestDist) {
							nearestDist = dist;
							nearestPath = view;
						}
					}
				}
			}
			if (nearestPath == null) {   // allow for click on the fill of a polygon
				var v:ObjectView = worldView.findViewUnderPoint(global);
				if (v && v.object != _host && v.object.isAnimationPath && v.object.wireScopeContainer == _host.wireScopeContainer)
					return v;
			}
			return nearestPath;
		}
		
		private function mouseMoveListener(event:MouseEvent):void
		{
			// when rollover a path, flash it red
			if (_host.parent) {
				var global:Point = new Point(event.stageX, event.stageY);
				var worldView:WorldView = EditorUI.instance.presentationContainer.worldView;
				var view:ObjectView = findPathObject(global, _host, worldView);
				
				if (_hilitedView && _hilitedView != view) {
					_hilitedView.flashRedForPathSelection(false);  // remove old hilite
					_hilitedView = null;
				}
				if (view) {
					view.flashRedForPathSelection(true);
					_hilitedView = view;
				}
			}
		}
		
		private function cleanup():void
		{
			ApplicationController.instance.captureMouseClicks(false, backgroundClickListener);
			worldView.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener, false);
			if (_hilitedView) {
				_hilitedView.flashRedForPathSelection(false);  // remove old hilite
				_hilitedView = null;
			}
		}
	}
}
