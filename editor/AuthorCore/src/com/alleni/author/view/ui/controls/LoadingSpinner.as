package com.alleni.author.view.ui.controls
{
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	
	/**
	 * fancy spinny lines loading feedback guy. the kind you use when you don't have any real information to relay, 
	 * like all modern OS's and websites use :) 
	 * 
	 * @author pkrekelberg
	 * 
	 */
	public class LoadingSpinner extends Sprite
	{
		private static const TICK_COLOR:uint = 0x6b91ba; // Zebra theme color
		
		private var _fadeTimer:Timer;
		private var _isPlaying:Boolean;
		
		private var _numTicks:int = 12;
		private var _size:Number = 30;
		private var _speed:int = 1000;
		private var _fadeSpeed:int = 600;
		private var _tickColor:uint;
		
		public var autoPlay:Boolean = true;
		
		public function LoadingSpinner(size:Number, autoPlay:Boolean, tickColor:int=-1)
		{
			super();
			_size = size;
			if (tickColor > -1) _tickColor = tickColor;
			
			initialize();
			
			if (autoPlay)
				play();
		}
		
		private function get tickColor():uint
		{
			if (_tickColor) return _tickColor;
			return TICK_COLOR;			
		}

		private function initialize():void
		{
			// Find out whether it's playing so we can restart it later if we need to
			var wasPlaying:Boolean = _isPlaying;
			
			// stop the spinning
			stop();
			
			// Remove all children
			for (var i:int = numChildren - 1; i >= 0; i--)
				removeChildAt(i);
			
			// Re-create the children
			var radius:Number = _size / 2;
			var angle:Number = 2 * Math.PI / _numTicks; // The angle between each tick
			var tickWidth:Number = _size / 12;
			var tickColor:uint = tickColor;
			
			var currentAngle:Number = 0;
			for (var j:int = 0; j < _numTicks; j++) {
				
				var xStart:Number = radius + Math.sin(currentAngle) * ((_numTicks + 2) * tickWidth / 2 / Math.PI);
				var yStart:Number = radius - Math.cos(currentAngle) * ((_numTicks + 2) * tickWidth / 2 / Math.PI);
				var xEnd:Number = radius + Math.sin(currentAngle) * (radius - tickWidth);
				var yEnd:Number = radius - Math.cos(currentAngle) * (radius - tickWidth);
				
				var t:LoadingSpinnerTick = new LoadingSpinnerTick(xStart, yStart, xEnd, yEnd, tickWidth, tickColor);
				t.alpha = 0.1;
				
				this.addChild(t);
				
				currentAngle += angle;
			}
			
			this.width = _size;
			this.height = _size;
			
			// Start the spinning again if it was playing when this function was called.
			if (wasPlaying)
				play();
		}
		
		/**
		 * Begin the circular fading of the ticks.
		 */
		public function play():void
		{
			if (!_isPlaying) {
				_fadeTimer = new Timer(_speed / _numTicks, 0);
				_fadeTimer.addEventListener(TimerEvent.TIMER, function (e:TimerEvent):void {
					var tickNum:int = int(_fadeTimer.currentCount % _numTicks);
					
					if (numChildren > tickNum) {
						var tick:LoadingSpinnerTick = getChildAt(tickNum) as LoadingSpinnerTick;
						tick.fade(_fadeSpeed != 1 ? _fadeSpeed : _speed * 6 / 10);
					}
				});
				_fadeTimer.start();
				_isPlaying = true;
			}
		}
		
		/**
		 * Stop the spinning.
		 */
		public function stop():void
		{
			if (_fadeTimer != null && _fadeTimer.running) {
				_isPlaying = false;
				_fadeTimer.stop();
			}
		}
	}
}