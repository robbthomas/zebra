package com.alleni.author.view.ui
{
	import flash.display.Graphics;

	public interface IDropHost
	{
		function feedbackGraphicsDrawFor(graphics:Graphics):Function;
	}
}