package com.alleni.author.view.ui
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Asset;
	
	import flash.events.Event;
	
	import mx.events.CollectionEvent;

	public class FontDropDownMenu extends DynamicDropDownMenu
	{
		private var _assetType:int;

		public function FontDropDownMenu(assetType:int,setValueCallback:Function, dataProvider:Array, isScaleable:Boolean = true)
		{
			super(setValueCallback, dataProvider, isScaleable);
			_assetType = assetType;

			AssetController.instance.model.assets.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleCollectionChange);
			ApplicationController.addEventListener(NotificationNamesApplication.FONTS_PROGRESS, handleCollectionChange);
			refreshMenu();
		}
		
		private function handleCollectionChange(event:Event):void
		{	
			refreshMenu();
		}
		
		private function refreshMenu(drawNeeded:Boolean = false):void
		{
            initializeDataProvider();

            var dataAdded:Boolean = false;
			var asset:Asset;
			for each(asset in AssetController.instance.model.children){
				if(asset.type == _assetType){
					var label:String = asset.name.replace(/\.\w+$/,"");  //remove extension
					addData(asset.name,label);  //data added to this dynamic data provider
                }
			}
			
			dataAdded = false;
			var fontObj:Object;
			for each(fontObj in ApplicationController.getSystemFonts()){
				var fontLabel:String = fontObj.label.replace(/\.\w+$/,"");  //remove extension
				addData(fontObj.data,fontLabel.replace(/_/gi," "));  //data added to this dynamic data provider
			}
			
            setDataProvider();
            draw();
		}
	}
}