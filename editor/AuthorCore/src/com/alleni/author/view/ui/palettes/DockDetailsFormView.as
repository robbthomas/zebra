package com.alleni.author.view.ui.palettes
{
	import com.alleni.author.controller.ui.GadgetPublishingMediator;
	import com.alleni.author.definition.Modifiers;
    import com.alleni.author.definition.application.DockIcons;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Dock;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichLabel;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.components.CheckBox;
    import com.alleni.author.view.ui.components.ComboBox;
    import com.alleni.author.view.ui.components.DockCheckBox;
	import com.alleni.author.view.ui.components.DockComboBox;
	import com.alleni.author.view.ui.components.DockRadioButton;
	import com.alleni.author.view.ui.components.HBox;
	import com.alleni.author.view.ui.components.PushButton;
	import com.alleni.author.view.ui.components.SearchComboBox;
	import com.alleni.author.view.ui.components.Style;
	import com.alleni.author.view.ui.components.VBox;
    import com.alleni.author.view.ui.controls.RatingContainer;
	
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Rectangle;
	
	import flashx.textLayout.edit.SelectionFormat;
	import flashx.textLayout.elements.Configuration;
	
	import mx.events.PropertyChangeEvent;
	
	public class DockDetailsFormView extends Sprite
	{
		protected static const FIELD_SPACING:Number = 3;
		protected static const PADDING:Number = 10;
		protected static const FORM_HEAD_SIZE:Number = 14;
		protected static const FORM_TITLE_SIZE:Number = 18;
		protected static const FORM_TEXT_SIZE:Number = 12;
		
		protected var _mediator:GadgetPublishingMediator;
		
		protected var _textColor:uint = 0x000000;
		protected var _existingButton:InteractiveObject;
		protected var _categoryMenu:ComboBox;
		protected var _targetProperty:String;
		
		public function DockDetailsFormView(targetProperty:String, container:DisplayObjectContainer, darkDialog:Boolean=true, existingButton:InteractiveObject=null)
		{
			super();
			if (!darkDialog)
				_textColor = 0x000000;
			_existingButton = existingButton;
			
			_mediator = new GadgetPublishingMediator(targetProperty, container);
			_targetProperty = targetProperty;
			
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		/**
		 * Override of addChild to force layout;
		 */
		override public function addChild(child:DisplayObject) : DisplayObject
		{
			super.addChild(child);
			child.addEventListener(Event.RESIZE, onResize);
			return child;
		}
		
		/**
		 * Override of addChildAt to force layout;
		 */
		override public function addChildAt(child:DisplayObject, index:int) : DisplayObject
		{
			super.addChildAt(child, index);
			child.addEventListener(Event.RESIZE, onResize);
			return child;
		}
		
		/**
		 * Override of removeChild to force layout;
		 */
		override public function removeChild(child:DisplayObject):DisplayObject
		{
			super.removeChild(child);            
			child.removeEventListener(Event.RESIZE, onResize);
			return child;
		}
		
		/**
		 * Override of removeChild to force layout;
		 */
		override public function removeChildAt(index:int):DisplayObject
		{
			var child:DisplayObject = super.removeChildAt(index);
			child.removeEventListener(Event.RESIZE, onResize);
			return child;
		}
		
		/**
		 * Internal handler for resize event of any attached component. Will redo the layout based on new size.
		 */
		protected function onResize(event:Event):void
		{
			dispatchEvent(new Event(Event.RESIZE));
		}
		
		protected function initialize(event:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			Style.setStyle(Style.LIGHT);
			const textShadow:DropShadowFilter = new DropShadowFilter(1,80,_textColor==0x000000?0xffffff:0,0.8,3,3,1.2,3);
			const darkShadow:DropShadowFilter = new DropShadowFilter(1,80,0,0.8,4,4,1,3);
			
			var singleFieldContainer:HBox = new HBox(this, PADDING*2 + 75, PADDING/3, PADDING/2);
			singleFieldContainer.alignment = HBox.MIDDLE;
			
			var infoIconContainer:Sprite = new Sprite();
			singleFieldContainer.addChild(infoIconContainer);
			singleFieldContainer.draw();
			singleFieldContainer.filters = [darkShadow];
			
			var headerText:RichLabel = new RichLabel(150);
			headerText.color = 0xffffff;
			headerText.size = FORM_HEAD_SIZE;
			headerText.bold = true;
			headerText.italic = true;
			headerText.height = 22;
			singleFieldContainer.addChild(headerText);
			
			var formContainer:VBox = new VBox(this, PADDING, PADDING*2, PADDING/3);
			var iconAndMetaDataContainer:HBox = new HBox(formContainer, 0, 0, PADDING*1.2);
			iconAndMetaDataContainer.alignment = HBox.TOP;
			
			var iconContainer:Sprite = new Sprite();
			iconContainer.addChild(DockIcons.getUnknown(DockIcons.SIZE_MEDIUM));
			iconContainer.visible = false;
			iconContainer.alpha = 0;
			iconContainer.filters = [darkShadow];
			iconAndMetaDataContainer.addChild(iconContainer);

			var fieldContainer:VBox = new VBox(iconAndMetaDataContainer, 0, 0, FIELD_SPACING);
			fieldContainer.addChild(createContainer(1, 15));

			singleFieldContainer = new HBox(fieldContainer);
			singleFieldContainer.alignment = HBox.MIDDLE;
			var label:LightLabel = new LightLabel(69, _textColor, false, 15);
			label.text = "Category";
			label.size = FORM_TEXT_SIZE;
			label.bold = true;
			label.filters = [textShadow];
			singleFieldContainer.addChild(label);
			
			_categoryMenu = new DockComboBox(singleFieldContainer, 0, 0, "", []);
			_categoryMenu.width = 125;
			_categoryMenu.defaultLabelColor = _textColor;
			_categoryMenu.labelSize = FORM_TEXT_SIZE;
			_categoryMenu.defaultStyle = Style.LIGHT;
			refreshCategoryMenu();

			singleFieldContainer = new HBox(fieldContainer);
			singleFieldContainer.alignment = HBox.MIDDLE;
			label = new LightLabel(69, _textColor, false, 15);
			label.text = "Author";
			label.size = FORM_TEXT_SIZE;
			label.bold = true;
			label.filters = [textShadow];
			singleFieldContainer.addChild(label);
			
			var authorLabel:LightLabel = new LightLabel(120, _textColor, true, 15);
			authorLabel.size = FORM_TEXT_SIZE;
			singleFieldContainer.addChild(authorLabel);

			var container:Sprite = this.createContainer(280, 30);
			container.scrollRect = new Rectangle(0, 0, 280, 28);
			formContainer.addChild(container);

			var nameBox:SearchComboBox = new SearchComboBox(container, 0, 10, "", []);
			nameBox.width = 280;
			nameBox.height = 30;
			nameBox.listItemHeight = 25;
			nameBox.listWidth = 270;
			nameBox.defaultLabelColor = _textColor;
			nameBox.labelSize = FORM_TITLE_SIZE;
			nameBox.labelBold = true;
			nameBox.defaultStyle = Style.LIGHT;
			nameBox.filters = [textShadow];
			
			var descriptionField:LightEditableLabel = new LightEditableLabel(280, 100, true);
			descriptionField.explicitLineBreak = false;
			descriptionField.editable = true;
			descriptionField.embedFonts = true;
			descriptionField.color = _textColor;
			descriptionField.size = FORM_TEXT_SIZE;
			descriptionField.bold = false;
			descriptionField.propertyType = Modifiers.FORM_TEXT_BOX;
			descriptionField.expandable = false;
			descriptionField.leading = 1.0;
			descriptionField.scrollRect = new Rectangle(0, 0, 280, 100);
			
			container = this.createContainer(280, 100);
			container.addChild(descriptionField);

			formContainer.addChild(container);
			
			singleFieldContainer = new HBox(formContainer, 0, 0, 30);
			singleFieldContainer.alignment = HBox.MIDDLE;
			// RATINGS
			var ratingContainer:RatingContainer = new RatingContainer(singleFieldContainer);
			ratingContainer.labelColor = _textColor;
			ratingContainer.labelSize = FORM_TEXT_SIZE;
			// SCREENSHOTS
			var screenshotContainer:Sprite = new Sprite();
            container = this.createContainer(140, 56);
            container.addChild(screenshotContainer);
			singleFieldContainer.addChild(container);
			singleFieldContainer.filters = [textShadow];

			// SPACING
			formContainer.addChild(createContainer(1, PADDING/2));

            //Separator Line
            var separatorline:Sprite = new Sprite();
            separatorline.graphics.lineStyle(1, 0xBBBBBB);
            separatorline.graphics.moveTo(0,-5);
            separatorline.graphics.lineTo(280,-5);
            formContainer.addChild(separatorline);

			// LICENSE CONTAINER
			var licenseContainer:VBox = new VBox(formContainer, 0, 0, PADDING/3);
			licenseContainer.blendMode = BlendMode.MULTIPLY;
			licenseContainer.filters = [textShadow];

            // EDIT LOCK
			var editLockCheckBox:CheckBox = new DockCheckBox(licenseContainer);
			editLockCheckBox.labelColor = _textColor;
			editLockCheckBox.labelSize = FORM_TEXT_SIZE;

			// SHOPP
			var shoppCheckBox:CheckBox = new DockCheckBox(licenseContainer);
			shoppCheckBox.labelColor = _textColor;
			shoppCheckBox.labelSize = FORM_TEXT_SIZE;
			
			// PRICE
			var priceContainer:HBox = new HBox(licenseContainer, 0, 0, 10);
			priceContainer.alignment = HBox.MIDDLE;
			container = this.createContainer(12, 1);
			priceContainer.addChild(container);
			
			label = new LightLabel(60, _textColor, false, 8);
			label.text = "Sell Price:";
			label.size = FORM_TEXT_SIZE;
			label.filters = [textShadow];
			priceContainer.addChild(label);
			
			var freeButton:DockRadioButton = new DockRadioButton(priceContainer, 0, 0, "Free", true);
			freeButton.labelColor = _textColor;
			freeButton.labelSize = FORM_TEXT_SIZE;
			freeButton.labelWidth = 50;
			freeButton.height = 10;
			freeButton.groupName = _mediator.targetProperty;
			var priceButton:DockRadioButton = new DockRadioButton(priceContainer, 0, 0, "");
			priceButton.labelColor = _textColor;
			priceButton.labelSize = FORM_TEXT_SIZE;
			priceButton.labelWidth = 50;
			priceButton.height = 10;
			priceButton.groupName = _mediator.targetProperty;
			
			var config:Configuration = new Configuration();
			var selectionFormat:SelectionFormat = new SelectionFormat(RichTextField.SELECTION_COLOR, RichTextField.SELECTION_ALPHA, BlendMode.MULTIPLY, _textColor);
			config.inactiveSelectionFormat = selectionFormat;
			config.focusedSelectionFormat = selectionFormat;
			
			var priceField:RichTextField = new RichTextField(80, 16, true, null, config);
			priceField.explicitLineBreak = true;
			priceField.color = _textColor;
			priceField.editable = true;
			priceField.embedFonts = true;
			priceField.size = FORM_TEXT_SIZE;
			priceField.filters = [textShadow];
			priceField.setPaddingParams({left:0, top:6, right:0, bottom:0});
			
			container = this.createContainer(50, 16);
			container.addChild(priceField);
			priceContainer.addChild(container);
			
			// REPUBLISHING
			var republishContainer:HBox = new HBox(licenseContainer, 0, 0, 10);
			republishContainer.alignment = HBox.MIDDLE;
			container = this.createContainer(12, 1);
			republishContainer.addChild(container);
			var republishCheckBox:CheckBox = new DockCheckBox(republishContainer);
			republishCheckBox.labelColor = _textColor;
			republishCheckBox.labelSize = FORM_TEXT_SIZE;
			
			// EMBEDDING
			var embedContainer:HBox = new HBox(licenseContainer, 0, 0, 10);
			embedContainer.alignment = HBox.MIDDLE;
			container = this.createContainer(12, 1);
			embedContainer.addChild(container);
			var embedCheckBox:CheckBox = new DockCheckBox(embedContainer);
			embedCheckBox.labelColor = _textColor;
			embedCheckBox.labelSize = FORM_TEXT_SIZE;
			
			if (!_existingButton) {
				formContainer.addChild(createContainer(1, PADDING/2));
				container = this.createContainer(PADDING/2, 1);
				formContainer.addChild(container);
				
				singleFieldContainer = new HBox(formContainer);
				singleFieldContainer.alignment = HBox.BOTTOM;
				var buttonContainer:HBox = new HBox(singleFieldContainer);
				
				var detailsButton:PushButton = new PushButton(buttonContainer, 0, 0, "Details");
				detailsButton.defaultStyle = Style.LIGHT;
				detailsButton.labelColor = _textColor;
				detailsButton.labelBold = true;
				detailsButton.filters = [textShadow];
				
				buttonContainer = new HBox(singleFieldContainer);
				
				var cancelButton:PushButton = new PushButton(buttonContainer, 0, 0, "Cancel");
				cancelButton.defaultStyle = Style.LIGHT;
				cancelButton.labelColor = _textColor;
				cancelButton.labelBold = true;
				cancelButton.filters = [textShadow];

				buttonContainer.addChild(cancelButton);

				var button:PushButton = new PushButton(buttonContainer);
				button.defaultStyle = Style.LIGHT;
				button.labelColor = _textColor;
				button.labelBold = true;
				button.width = 100;
				button.filters = [textShadow];
				
				buttonContainer.addChild(button);
				singleFieldContainer.spacing = this.width - detailsButton.width - buttonContainer.width - PADDING*1.5;
				singleFieldContainer.blendMode = BlendMode.MULTIPLY;
			}
			
			_mediator.handlePublishingEvents(headerText, infoIconContainer, nameBox, descriptionField, authorLabel, priceField, 
				ratingContainer, iconContainer, screenshotContainer, _existingButton?_existingButton:button, _categoryMenu, detailsButton, cancelButton);
			_mediator.handleLicenseControls(licenseContainer, embedContainer, embedCheckBox, republishContainer, republishCheckBox, shoppCheckBox, freeButton, priceButton, priceContainer, editLockCheckBox);
			Dock.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);
		}
		
		protected function handlePropertyChange(event:PropertyChangeEvent):void
		{
			switch(event.property) {
				case _targetProperty:
					if (event.newValue == null || event.newValue === event.oldValue) break;
				case "categories":
					refreshCategoryMenu();
					break;
				default:
					break;
			}
		}
		
		protected function refreshCategoryMenu():void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			if (!gadget) return;
			
			if (Dock.instance.categories) {
				Style.setStyle(Style.LIGHT);
				var categories:Array = [];
				var category:Object;
				var i:int;
				var length:int = Dock.instance.categories.length;
                for (i = 0; i < length; i++) {
					category = Dock.instance.categories[i];
					if ("projectTypeId" in category && (category.projectTypeId == gadget.projectTypeId || (category.projectTypeId == 1 && gadget.projectTypeId == 0))){
						categories.push(category);
                    }
				}
				_categoryMenu.items = categories;
				_categoryMenu.numVisibleItems = categories.length;
				if (!_categoryMenu.selectedItem) {
					if (_categoryMenu.items && _categoryMenu.items.length > 0)
						_categoryMenu.defaultLabel = _categoryMenu.items[0].label;
					_categoryMenu.selectedIndex = 0;
				}
				
				length = _categoryMenu.items.length;
				for (i = 0; i < length; i++) {
					if ("id" in _categoryMenu.items[i])
						if (_categoryMenu.items[i].id == gadget.categoryId)
							_categoryMenu.selectedIndex = i;
				}
			}
		}
		
		protected function createContainer(width:Number, height:Number):Sprite
		{	
			var container:Sprite = new Sprite();
			with(container.graphics){
				beginFill(0x000000, 0);
				drawRect(0, 0, width, height);
				endFill();
			}
			
			return container;
		}
	}
}
