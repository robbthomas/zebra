package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;
	import caurina.transitions.properties.ColorShortcuts;
	
	import com.alleni.author.view.text.RichLabel;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextFieldAutoSize;

public class RibbonContainerHeading extends Sprite
	{
		protected static const BUTTON_WIDTH:Number = 6;
		protected static const BUTTON_HEIGHT:Number = 5;
		protected static const CUSHION:Number = 4; // Space between the button and label
		protected static const TAB_WIDTH:Number = 116;
		public static const TAB_HEIGHT:Number = 12;
		protected static const LEFT_MARGIN:Number = 15;
		protected static const BUTTON_LEFT_OFFSET:Number = 15;
		protected static const ARROW_SIZE:Number = 9;
		
		protected var _button:Sprite;
        protected var _clickarea:Sprite;
		protected var _arrow:Sprite;
		protected var _label:RichLabel;
		protected var _w:Number; // width
		protected var _color:uint;
		protected var _enableInspectorBehavior:Boolean;
		
		protected var _h:Number; // height of the tab
		protected var _background:Sprite;
		protected var _backgroundBorder:Sprite;
		
		public function RibbonContainerHeading(color:uint, label:String, enableInspectorBehavior:Boolean = false)
		{
			super();
			
			_color = color;
			_enableInspectorBehavior = enableInspectorBehavior;

            _clickarea = new Sprite();
            _clickarea.graphics.clear();
            _clickarea.graphics.beginFill(0xFF0000,0.01);
            _clickarea.graphics.drawRect(-3,-3,BUTTON_WIDTH+6,BUTTON_HEIGHT+6);
            _clickarea.graphics.endFill();

			_button = new Sprite();
			_arrow = new Sprite();
	
			_background = new Sprite();
			_backgroundBorder = new Sprite();
			_label = new RichLabel();
			formatLabel();
			_label.text = label;
			_h = TAB_HEIGHT; // height of the tab
			
			drawBackground(_color);
			
			if(!_enableInspectorBehavior){
				this.addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
				this.addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			}
			
			drawButton();
			defineLayout();
			addChild(_background);
			addChild(_backgroundBorder);
			
			addChild(_label);
            _background.addChild(_clickarea);
			_background.addChild(_button);
			
			// Init Tweener ColorShortcuts (required for color tweening)
			ColorShortcuts.init();
		}
		
		protected function defineLayout():void 
		{
			_label.height = 10;
			_label.autoSize = TextFieldAutoSize.LEFT;
			
			// Center button and label
			
			var buttonLabelWidth:Number = BUTTON_WIDTH + _label.width + CUSHION;
			//_button.x = MessageCenterView.WIDTH_NO_BORDER/2 - buttonLabelWidth/2;
			_button.x = LEFT_MARGIN + BUTTON_LEFT_OFFSET;
			//_label.x = _button.x + BUTTON_WIDTH + CUSHION;
			_label.x = LEFT_MARGIN + BUTTON_LEFT_OFFSET + ARROW_SIZE;
			
			// Start with button in closed position
			_button.rotation = -90;
			_button.y = BUTTON_HEIGHT+buttonPadding;
            _clickarea.x = _button.x - 3;
            _clickarea.y = _button.y;


			_label.y = -2.5+buttonPadding;
		}

        public function get buttonPadding():int {
            if (this.parent as RibbonSubContainer && ((this.parent as RibbonSubContainer).libraryButtonsShowing) || this is RibbonChildContainerHeading) {
                 return 4;
            } else {
                return 0;
            }
        }
		
		private function formatLabel():void 
		{
			_label.color = 0xFFFFFF;
			if(this is RibbonChildContainerHeading == false) {
				_label.size = 10;
			}
			//_label.bold = true;
			//_label.italic = true;
			_label.name = label;
		}
		
		protected function drawBackground(bkColor:uint):void 
		{
			graphics.clear();
			graphics.lineStyle(1, 0, 1, true);
			graphics.beginFill(0x000000,0.5);
			graphics.drawRect(MessageCenterView.BORDER_WIDTH,0,MessageCenterView.WIDTH_NO_BORDER,_h);

			_background.graphics.clear();
			_background.graphics.beginFill(bkColor);
			_background.graphics.drawRoundRectComplex(17,1+buttonPadding,TAB_WIDTH,_h-3,8,8,0,0);
			
			_backgroundBorder.graphics.clear();
			_backgroundBorder.graphics.lineStyle(1, bkColor, 1, true);
			_backgroundBorder.graphics.drawRoundRectComplex(17,1+buttonPadding,TAB_WIDTH,_h-3,8,8,0,0);
		}
		
		protected function drawButton():void 
		{
			// The button sprite is an invisible circle with an arrow sprite child (this ensures rotation around the center point)
			_button.graphics.clear();
			_button.graphics.drawCircle(0,0,5);
			
			_arrow.graphics.clear();
			_arrow.graphics.beginFill(0xFFFFFF);
			_arrow.graphics.moveTo(0,0);
			_arrow.graphics.lineTo(BUTTON_HEIGHT, 0);
			_arrow.graphics.lineTo(BUTTON_HEIGHT/2, BUTTON_WIDTH);
			_arrow.graphics.lineTo(0,0);
			_arrow.y = -_arrow.width/2;
			_arrow.x = -_arrow.width/2;
			_button.addChild(_arrow);
		}
		
		public function expandSubContainer(value:Boolean):void{
			
			Tweener.addTween(_button, {rotation:(value?0:-90), time:0.10, transition:"linear"});
		}
		
		public function openSubcontainer(value:Boolean):void
		{
			if (_enableInspectorBehavior){  //inspector behavior
				
				if(value){
					this.addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
					this.addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
					this.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
					this._button.visible = true;
				}
				
				else{
					this.removeEventListener(MouseEvent.ROLL_OVER, rollOverListener);
					this.removeEventListener(MouseEvent.ROLL_OUT, rollOutListener);
					this.removeEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
					this._button.visible = false;
				}
				
				Tweener.addTween(_backgroundBorder, {_color:(value?_color:0x323232), time:0.15, transition:"linear"});
				Tweener.addTween(_button, {rotation:(value?0:-90), time:0.10, transition:"linear"});
			} else{
				Tweener.addTween(_button, {rotation:(value?0:-90), time:0.10, transition:"linear"});
			}
            var bkColor:uint = (value || !_enableInspectorBehavior)?_color:0x323232;
            drawBackground(bkColor);
            _button.y = BUTTON_HEIGHT+buttonPadding;
            _label.y = -2.5+buttonPadding;
		}
		
		private function rollOverListener(e:MouseEvent):void 
		{
			Tweener.addTween(_backgroundBorder, {_color:0xFFFFFF, time:0.15, transition:"linear"});
		}
		
		private function rollOutListener(e:MouseEvent):void 
		{
			Tweener.addTween(_backgroundBorder, {_color:_color, time:0.15, transition:"linear"});
		}
		
		private function mouseUpListener(e:MouseEvent):void 
		{
			//dispatchEvent(e);
		}
	
		public function get label():String 
		{
			return _label.text;
		}

        public function get labelTextObject():RichLabel {
            return _label;
        }
		
		override public function set height(value:Number) : void 
		{
			_h = value;
			drawBackground(_color);
		}
	}
}