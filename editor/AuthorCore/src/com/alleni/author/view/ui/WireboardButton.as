package com.alleni.author.view.ui
{
	import com.alleni.author.view.WorldContainer;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	
	public class WireboardButton extends Sprite implements IWireboardItem
	{
		private var _type:String;
		private var _context:ViewContext;
		private var _wireboard:WireboardView;
		private var _icon:Sprite;
		private var _plus:Sprite;
		private var _weight:Number
		
		public function WireboardButton(buttonType:String, context:ViewContext, wireboard:WireboardView)
		{
			super();
			_type = buttonType;
			_context = context;
			_wireboard = wireboard;
			_icon = new Sprite();
			WireboardPortView.drawIcon(_icon, buttonType, wireboard.side);
			addChild(_icon);
			_plus = new Sprite();
			drawPlus(_plus);
			addChild(_plus);
			_plus.mouseEnabled = false;
			_icon.mouseEnabled = false;
		}
		
		private function drawPlus(sprite:Sprite):void
		{
			const PL:Number = 2;
			var g:Graphics = sprite.graphics;
			g.lineStyle(2,0xffffff);
			g.moveTo(-PL, 0);
			g.lineTo(+PL, 0);
			g.moveTo(0, -PL);
			g.lineTo(0, +PL);
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function get weight():Number
		{
			return _weight;
		}
		
		public function set weight(value:Number):void
		{
			_weight = value;
		}
		
		public function get wireboard():WireboardView
		{
			return _wireboard;
		}
	}
}