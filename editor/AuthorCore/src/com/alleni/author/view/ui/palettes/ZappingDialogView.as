/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 1/3/12
 * Time: 11:17 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.ui.palettes {
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.PublishIcons;

import flash.display.DisplayObject;
import flash.display.GradientType;
import flash.display.InterpolationMethod;
import flash.display.Shape;
import flash.display.SpreadMethod;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.filters.DropShadowFilter;
import flash.geom.Matrix;

import mx.events.FlexEvent;

import spark.components.RichEditableText;

import spark.filters.GlowFilter;

    public class ZappingDialogView extends PopoverView{
        private var _targetProperty:String;
        private var _borderOverlay:Sprite;
        private var _shareTo:String;
        private var _shareMessage:Boolean;
        public static const PANEL_WIDTH:int = 450;
        public static var PANEL_HEIGHT:int = PANEL_WIDTH;

        public function ZappingDialogView(targetView:DisplayObject, targetProperty:String, allowedDirections:int=-1, minimumTargetWidth:Number=0, shareTo:String = NotificationNamesApplication.SHARE_TO_LINK, shareMessage:Boolean = false) {
            super(targetView, allowedDirections, minimumTargetWidth);
            _targetProperty = targetProperty;
            _width = PANEL_WIDTH;
            _height = shareMessage?PANEL_WIDTH:PANEL_WIDTH;
            PANEL_HEIGHT = _height;
            _shareTo = shareTo;
            _shareMessage = shareMessage;
        }

        override protected function initialize(event:Event=null):void
        {
            super.initialize(event);
            addPanel(new ZappingDialogFormView(_targetProperty, this, true, null, _shareTo, _shareMessage));
//            this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
//            this.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
        }

        public function onMouseDown(event:MouseEvent):void{
            var object:Object = event.target;
            if(!(event.target is RichEditableText)) {
                this.startDrag();
            }


        }
        public function onMouseUp(event:MouseEvent):void{
            var object:Object = event.target;
            this.stopDrag();
        }

        override protected function draw():void
		{
			if (!_background) {
				_background = new Sprite();
				addChild(_background);
                _borderOverlay = new Sprite();
				_background.alpha = 0.9;
				//_background.filters = [new GlowFilter(0x000000, 0.8, 3, 3, 2, 3, true)];
				filters = [new DropShadowFilter(2, 80, 0, 0.7, 15, 15, 0.9, 5)];
			} else {
                _background.graphics.clear();
                _borderOverlay.graphics.clear();
            }

			// fill top black portion which also fills arrow when pointing up
			_background.graphics.beginFill(FILL_COLOR);
            _background.graphics.drawRoundRect(0,0, _width, _height, CORNER_RADIUS, CORNER_RADIUS);
			var matrix:Matrix;

			// bottom gradient pointing from bottom left corner toward top right
			matrix = new Matrix();
			matrix.createGradientBox(_width+ARROW_MARGIN*2, 100, (Math.PI/1.6), 0, _height-100);
			_background.graphics.beginGradientFill(GradientType.LINEAR, [HIGHLIGHT_COLOR, HIGHLIGHT_COLOR], [0, 1], [0x00, 0xff], matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
			_background.graphics.drawRect(-ARROW_MARGIN, _height-200, _width+ARROW_MARGIN*2, _height);
			_background.graphics.endFill();

			if (!_backgroundMask) {
				_backgroundMask = new Sprite();
				addChild(_backgroundMask);
				_backgroundDecoration = PublishIcons.dialogDecoration;
				_background.addChild(_backgroundDecoration);
				_background.mask = _backgroundMask;
			} else _backgroundMask.graphics.clear();

			_backgroundDecoration.x = -17;
			_backgroundDecoration.y = _height - _backgroundDecoration.height + 17;

            _borderOverlay.graphics.lineStyle(3);
			_borderOverlay.graphics.drawRoundRect(0,0, _width, _height, CORNER_RADIUS, CORNER_RADIUS);
            _background.addChild(_borderOverlay);

			_backgroundMask.graphics.beginFill(0);
			_backgroundMask.graphics.lineStyle(1, 0, 0, true);
			_backgroundMask.graphics.drawRoundRect(0,0, _width, _height, CORNER_RADIUS, CORNER_RADIUS);
			_backgroundMask.graphics.endFill();

			this.target = _targetView;
			this.visible = true;
		}

        override protected function handleResize(child:DisplayObject):Function
		{
			return function(event:Event):void
			{
				//Nothing is needed here
			}
		}
    }
}
