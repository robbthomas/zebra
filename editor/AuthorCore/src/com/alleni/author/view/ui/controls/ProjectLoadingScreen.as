package com.alleni.author.view.ui.controls
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.events.Event;

	import mx.events.PropertyChangeEvent;

	public class ProjectLoadingScreen extends LoadingScreen
	{	
		private static const ANIMATION_DURATION:Number = 0.75;
		private static const ANIMATION_TRANSITION:String = "easeInOutQuart";
		private static const INITIAL_PROGRESS_FRACTION:Number = 0.3;
		
		private static var _instance:ProjectLoadingScreen;
		
		private var _initialAssetCount:int = -1;
		
		public function ProjectLoadingScreen(bootstrapLoadingEvent:Event=null, validInvocation:Boolean=false)
		{
			Utilities.assert(!_instance);
			super(bootstrapLoadingEvent, validInvocation);
			
			if (!validInvocation) {
				if (!bootstrapLoadingEvent) {
					detailText = "Nothing to load!";
					this.visible = false;
				} else {
					this.alpha = 0;
				}
			}
		}
		
		public static function get instance():ProjectLoadingScreen
		{
			return getInstanceWithEventAndInvocation();
		}
		
		public static function getInstanceWithEventAndInvocation(bootstrapLoadingEvent:Event=null, validInvocation:Boolean=false):ProjectLoadingScreen
		{
			if (!_instance) {
				_instance = new ProjectLoadingScreen(bootstrapLoadingEvent, validInvocation);
			} else {
				_instance.waitForStage();
				_instance.handleProjectLoading(bootstrapLoadingEvent as ApplicationEvent, validInvocation);
			}
			return _instance;
		}
		
		override protected function initialize(bootstrapLoadingEvent:Event=null, validInvocation:Boolean=false):void
		{
			super.initialize(bootstrapLoadingEvent, validInvocation);
			handleProjectLoading(bootstrapLoadingEvent as ApplicationEvent, validInvocation);
		}
		
		override protected function addListeners():void
		{
			super.addListeners();
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOADING, handleProjectLoading);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_CHANGING, handleProjectChanging);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_COMPLETE, handleProjectComplete);
        	Application.instance.progressBar.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleProgress);
		}
		
		override protected function removeListeners():void
		{
			super.removeListeners();
			ApplicationController.removeEventListener(NotificationNamesApplication.PROJECT_LOADING, handleProjectLoading);
			ApplicationController.removeEventListener(NotificationNamesApplication.PROJECT_CHANGING, handleProjectChanging);
			ApplicationController.removeEventListener(NotificationNamesApplication.PROJECT_COMPLETE, handleProjectComplete);
        	Application.instance.progressBar.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleProgress);
		}
		
		override protected function handleAddedToStage(e:Event):void
		{
			super.handleAddedToStage(e);
			if (!visibleAndOpaque) visible = true;
		}
		
		private function handleProjectLoading(bootstrapLoadingEvent:ApplicationEvent=null, validInvocation:Boolean=false):void
		{
			if (!validInvocation && (!bootstrapLoadingEvent || bootstrapLoadingEvent.data == null)) {
				this.visible = false;
				return;
			}
			this.visible = true;
			_progressFraction = INITIAL_PROGRESS_FRACTION; // todo: replace with progress from download aggregating state, gadget, and preloaded assets
			detailText = "Fetching your App";
		}
		
		private function handleProjectChanging(event:Event):void
		{
			const gadget:Project = Application.instance.document.project;
			if (!gadget) return;
			const name:String = gadget.publishedName?gadget.publishedName:gadget.name;
			detailText = '"' + name + '" is on the way!';
		}
		
		private function handleProgress(event:PropertyChangeEvent):void
		{
			if (event.property == "progress") {
				progressFraction = INITIAL_PROGRESS_FRACTION 
					+ (1-INITIAL_PROGRESS_FRACTION) * Application.instance.progressBar.progress;
			}
		}
		
		private function handleProjectComplete(event:ApplicationEvent):void
		{
			progressFraction = 1;
			if (event.type == NotificationNamesApplication.PROJECT_COMPLETE)
				detailText = "Ready!";
			else detailText = "Error...";
		}
		
		private function get visibleAndOpaque():Boolean
		{
			return (super.visible && this.alpha == 1);
		}
			
		override public function set visible(value:Boolean):void
		{
			if (value && visibleAndOpaque) return;
			if (value) {
				super.visible = true;
				this.mouseChildren = true;
				this.mouseEnabled = true;
				Tweener.addTween(this, {alpha:1, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION});
			} else {
//				if (alpha == 0) {
					super.visible = false;
					this.mouseChildren = false;
					this.mouseEnabled = false;
					removeListeners();
					if (this.parent) {
						this.parent.removeChild(this);
					}
//				} else {
//                    trace(">>>>>>>> Loading Screen visible false, add tween");
//					Tweener.addTween(this, {alpha:0, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION,
//						onComplete:function():void {
//                            trace(">>>>>>>> Loading Screen visible false, tween complete");
//							visible = false;
//						}});
//				}
			}
		}
	}
}