package com.alleni.author.view.ui.controls
{
	import flash.display.Sprite;
	
	public class DefaultCloseButton extends Button
	{
		
		public function DefaultCloseButton()
		{
			super();
			
			//BUILD BUTTON STATES
			_skin_unsel_down = buildCloseButton(0x54EBBE, 0x000000);
			_skin_unsel_roll = buildCloseButton(0x646464, 0x000000);
			_skin_unsel_up = buildCloseButton(0x000000, 0x484848);
		}
		
		private function buildCloseButton(color:uint, xColor:uint):Sprite{
			var tempSprite:Sprite = new Sprite();
			tempSprite.graphics.beginFill(color);
			tempSprite.graphics.drawEllipse(0,0,15,15);
			tempSprite.graphics.endFill();
			tempSprite.graphics.lineStyle(2,xColor);
			tempSprite.graphics.moveTo(5,5);
			tempSprite.graphics.lineTo(9,9);
			tempSprite.graphics.moveTo(9,5);
			tempSprite.graphics.lineTo(5,9);
			return tempSprite;
		}
	}
}