package com.alleni.author.view.ui
{	
	import com.alleni.author.application.ui.EditorUI;
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ui.controls.PopOutMenu;

import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class RibbonDropDownMenu extends AbstractRibbonControl
	{
		private static const DEFAULT_WIDTH:Number = 85;
		
		private var _h:Number;
		private var _buttonBorder:Sprite;
		private var _popOut:PopOutMenu;
		private var _xOffset:int = 0;
		private var _menuWidth:Number;
        protected var _isScalable:Boolean;
        protected var _doBeforeOpen:Function = null;
		
		protected var _dataProvider:Array;

		public function RibbonDropDownMenu(setValueCallback:Function, dataProvider:Array, isScaleable:Boolean)
		{
			super(setValueCallback);
			_isScalable = isScaleable;
			_dataProvider = dataProvider;
			_h = RibbonView.HEIGHT;
			_menuWidth = this.menuWidth;
			
			this.addEventListener(MouseEvent.CLICK, mouseDownListener);
            ApplicationController.addEventListener(NotificationNamesApplication.SCALE_CHANGED,scaleChanged);
            draw();
		}
		
		private function putPopUpBackOnRibbon(event:Event = null):void
		{
			if (!_popOut)
				return;
			
			const rawChildren:DisplayObjectContainer = EditorUI.instance.rawChildren;
			if (!rawChildren) return;
			
			if (_popOut.parent === rawChildren) {// contains is troublesome here as rawChildren is the stage
				rawChildren.removeChild(_popOut);
			
				var location:Point = new Point(0, 0);
				_popOut.x = location.x;
				_popOut.y = location.y;
				_popOut.scaleX = _popOut.scaleY = 1;
				addEventListener(Event.ENTER_FRAME, finishPopOutReplace);
			}
			EditorUI.instance.removeEventListener(MouseEvent.MOUSE_UP, putPopUpBackOnRibbon);	
		}
		
		private function finishPopOutReplace(event:Event):void
		{
			addChild(_popOut);
			removeEventListener(Event.ENTER_FRAME, finishPopOutReplace);
		}

        private function get editorTopLeftCorner():Point {
            var location:Point = new Point(0,0);
            location = localToGlobal(location);
            return location;
        }

		private function bringPopOutToFront():void
		{
            var location:Point = EditorUI.instance.globalToLocal(editorTopLeftCorner);

            if(contains(_popOut)){
				removeChild(_popOut);
                positionPopup(location,true);
				EditorUI.instance.rawChildren.addChild(_popOut);
            }
		}

        private function positionPopup(location:Point,doScaling:Boolean = false):void {
            if(_isScalable && doScaling){
                _popOut.scaleX = _popOut.scaleY = Application.instance.zoomLevel;
            }else{
                _popOut.scaleX = _popOut.scaleY = 1;
            }

            if(location.x + _popOut.popWidth > EditorUI.instance.width){ //Application.instance.width doesn't give updated width
                _xOffset = (location.x + _popOut.popWidth) - EditorUI.instance.width;
            }
            _popOut.x = location.x - _xOffset;
            _popOut.y = location.y;
        }

		protected function mouseDownListener(e:MouseEvent):void
		{
			ApplicationController.instance.hideToolTip();
            bringPopOutToFront();
			EditorUI.instance.addEventListener(MouseEvent.CLICK, putPopUpBackOnRibbon);
			e.stopPropagation();
		}

        private function scaleChanged(event:Event):void {
            if(_isScalable && !contains(_popOut)) {
                positionPopup(EditorUI.instance.globalToLocal(editorTopLeftCorner),true);
            }
        }
		
		private function itemChanged(event:Event):void
		{
			updateProperty(_popOut.selectedItem.data, RibbonView.DEFAULT);
			putPopUpBackOnRibbon();
		}
				
		protected function draw():void
		{
            if (_popOut == null)
			    _popOut = new PopOutMenu(EditorUI.instance);
            _popOut.doBeforeOpen = _doBeforeOpen;
            _popOut.isScalable = _isScalable;
			_popOut.closedHeight = 15;
			_popOut.closedWidth = 38;
			_popOut.labelSize = 11;
			_popOut.popWidth = _menuWidth;
			_popOut.dataProvider = _dataProvider;
			_popOut.defaultIndex = 0;
			_popOut.baseColor = 0x191919;
			_popOut.borderColor = 0x000000;
			_popOut.arrowScale = .75;
			_popOut.baseLabelOffsetX = -5;
			_popOut.baseLabelOffsetY = -3;
			_popOut.arrowOffsetX = 3;
			_popOut.baseLabelWidthAdjustment = -10;
			_popOut.direction = PopOutMenu.DIRECTION_BI;
			_popOut.drawComponent();
			_popOut.visible = true;
			//Move this to the mediator when done with initial development
			_popOut.addEventListener(PopOutMenu.ITEM_CHANGED, itemChanged);
			_popOut.addEventListener(PopOutMenu.POP_OUT_CLOSED, putPopUpBackOnRibbon);
			_popOut.addEventListener(PopOutMenu.POP_OUT_OPEN, hideCurrentToolTip);
			addChild(_popOut);
		}
		
		private function hideCurrentToolTip(event:Event = null):void
		{
		/*	if(_rb is RibbonView){
				(_rb as RibbonView).showTooltip(false);
			}*/
		}
		
		private function drawButtonBorder(color:uint):void 
		{
			var btnWidth:Number = 6;
			var btnHeight:Number = 5;
			
			_buttonBorder.graphics.clear();
			_buttonBorder.graphics.lineStyle(1, color);
			_buttonBorder.graphics.moveTo(0,_h/2-btnHeight/2);
			_buttonBorder.graphics.lineTo(btnWidth,_h/2-btnHeight/2);
			_buttonBorder.graphics.lineTo(btnWidth/2,_h/2-btnHeight/2+btnHeight);
			_buttonBorder.graphics.lineTo(0,_h/2-btnHeight/2);			
		}
		
		override public function setValue(value:Object, valueType:String):void 
		{
			_popOut.updateSelectedItemByData(value);
		}
		
		public function updateProperty(value:Object, valueType:String):void
		{
			valueChanged(value);
		}
		
		public function set args(array:Array):void
		{
			trace("Args are: "+_dataProvider);
			_dataProvider = array;
		}
		
		/*  TODO: 	This is a hack to adjust the width of the drop down based on the ribbon. PopOutMenus should be smart about sizing based on MenuItem widths
					ASAP - LML, 11-16-2010 */
		
		private function get menuWidth():Number
		{
		/*	var ribbon:RibbonView = _rb as RibbonView;
			var property:String = ribbon?ribbon.ribbon.modifierDescription.key:"";
			var value:Number = DEFAULT_WIDTH;
			
			switch (property) {
				case "fontFamily":
					value = 185;
					break;
				case "tweenType":
					value = 125;
					break;
				case "anchorPoint":
					value = 115;
					break;
			}*/

			return 200;//value;
		}
	}
}