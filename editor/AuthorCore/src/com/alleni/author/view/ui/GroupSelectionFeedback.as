package com.alleni.author.view.ui
{
    import com.alleni.author.document.Document;
    import com.alleni.author.model.AbstractObject;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.view.WorldContainer;
    import com.alleni.author.view.feedback.IFeedback;
    import com.alleni.taconite.model.GroupSelectionObject;
    import com.alleni.taconite.model.TaconiteModel;
    import com.alleni.taconite.view.PivotHandle;
    import com.alleni.taconite.view.TaconiteView;
    
    import flash.display.BitmapData;
    import flash.display.LineScaleMode;
    import flash.display.Sprite;
    import flash.geom.Rectangle;

    public class GroupSelectionFeedback extends TaconiteView implements IFeedback
	{
		private var _selectionViews:Vector.<TaconiteView>;
		private var _group:GroupSelectionObject;
		private var _rotationHandle:PivotHandle;

		public function GroupSelectionFeedback(context:WorldContainer, groupSelectionObject:GroupSelectionObject)
		{
			super(context,groupSelectionObject.model);	
			_group = groupSelectionObject;
			_selectionViews = collectIndividualViews();
			group.setGeometry(constructBoundsFromSelection());
			this.draw();
		}
		
		override public function initialize():void
		{
			var feedbackView:Sprite = WorldContainer(_context).feedbackGroupHandlesView;
			if (!feedbackView) return; // this must be the player - careful, never assume editor-specific views!
			feedbackView.addChild(this);
			this.draw();
			
			if (feedbackView.stage)
				feedbackView.stage.invalidate();
			
			super.initialize();
		}
		
		private function collectIndividualViews():Vector.<TaconiteView>
		{
			_selectionViews = new Vector.<TaconiteView>();
			var aObject:AbstractObject;
			for ( var i:int = 0; i < group.selection.selectedModels.length; i++ ) {
				aObject = (group.selection.selectedModels[i] as TaconiteModel).value as AbstractObject;
				if(aObject) {
					var view:TaconiteView = aObject.getView(worldContainer);
					if (view) {
						_selectionViews.push(view);
					}
				}
			}
			
			return _selectionViews;
		}
		
		private function constructBoundsFromSelection():Rectangle
		{
			var rect:Rectangle;
			var n:int = 0;
			for each(var view:TaconiteView in _selectionViews) {
                if (view.parent == null) {
                    _selectionViews.splice(_selectionViews.indexOf(view),1);
                    group.selection.selectedModels.splice(group.selection.selectedModels.indexOf(view.model),1);
                    continue;
                }
				if (rect == null) {
					rect = view.getBounds(worldContainer);
				} else {
					rect = view.getBounds(worldContainer).union(rect);
				}
			}
			return rect;
		}
		
		public function get rotationHandle():PivotHandle
		{
			return _rotationHandle;
		}

		private function get worldContainer():WorldContainer
		{
			return this.context as WorldContainer;
		}
		
		public function get group():GroupSelectionObject
		{
			return this.model.value as GroupSelectionObject;
		}

		public function draw():void
		{
            var whitePixels:Vector.<uint> = new Vector.<uint>();
            for (var i:int = 0; i<4; i++) {
                whitePixels[i] = 0x88FFFFFF;
            }
            var bitmap:BitmapData = new BitmapData(4,4,false,0x88000000);
            bitmap.setVector(new Rectangle(0,0,2,2), whitePixels);
            bitmap.setVector(new Rectangle(2,2,2,2), whitePixels);

            this.graphics.clear();
            this.graphics.lineStyle(1,0,0.5,false,LineScaleMode.NONE);
            this.graphics.lineBitmapStyle(bitmap);
            this.graphics.moveTo(0,0);
            this.graphics.lineTo(group.width,0);
            this.graphics.lineTo(group.width,group.height);
            this.graphics.lineTo(0,group.height);
            this.graphics.lineTo(0,0);

			this.x = group.x;
			this.y = group.y;
		}
		
		public function render():void
		{
			group.setGeometry(constructBoundsFromSelection());
			draw();
		}
	}
}