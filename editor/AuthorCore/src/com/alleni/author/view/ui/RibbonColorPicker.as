package com.alleni.author.view.ui
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.app.CursorController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.CursorNames;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ui.controls.ColorPicker;
	import com.alleni.author.view.ui.controls.IColorPickerParent;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class RibbonColorPicker extends AbstractRibbonControl
	{
		private const OUTLINE_COLOR:uint = 0x646464;
		
		private var _chipSize:Object = {width:35, height:12};
		private var _outlineColor:uint = OUTLINE_COLOR;
		private var _selectedColor:Number = 0xFFFFFF;

		private var _token:*;
		
		private var _parentControl:IColorPickerParent;
		/**
		 * This component makes it appear as though a Flex ColorPicker (UIComponent) is a child of a ribbon (Sprite). 
		 * @param context
		 * @param rb
		 * 
		 */
		public function RibbonColorPicker(setValueCallback:Function, w:Number=35, h:Number=12)
		{
			super(setValueCallback);
			
			_chipSize.width = w;
			_chipSize.height = h - 1;
			
			draw();
			
			this.addEventListener(MouseEvent.CLICK, mouseClickListener, false, 0, true);
			this.addEventListener(MouseEvent.MOUSE_OVER, mouseOverListener, false, 0, true);
			this.addEventListener(MouseEvent.MOUSE_OUT, mouseOutListener, false, 0, true);
		}
		
		private function draw():void
		{	
			graphics.clear();
			graphics.lineStyle(1, _outlineColor);
			graphics.beginFill(_selectedColor);
			graphics.drawRect(0,0,_chipSize.width,_chipSize.height);
		}
		
		private function getParentMessageCenter():void
		{
			if(!_parentControl){
				var p:DisplayObject = this.parent;
				while(p) {
					if(p as IColorPickerParent) {
						_parentControl = p as IColorPickerParent;
						return;
					}
					p = p.parent;
				}
			}
		}
		
		// Highligh/unhighlight the color chip
		private function mouseOverListener(e:MouseEvent):void
		{
			_outlineColor = OUTLINE_COLOR;
			draw();
		}
		
		private function mouseOutListener(e:MouseEvent):void
		{			
			_outlineColor = OUTLINE_COLOR;
			draw();	
		}
		
		/**
		 * Adds a Flex ColorPicker component to the feedback layer, positioning it under the ribbon parent
		 * of this RibbonColorPicker object. 
		 * @param e
		 * 
		 */
		private function mouseClickListener(e:MouseEvent):void
		{	
			ApplicationController.instance.captureMouseClicks(true);
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
			
			getParentMessageCenter();
			_parentControl.buildColorPickerIfNeeded();
			_token = ApplicationController.currentActionTree.openGroup("Pick Color");
			_parentControl.colorPicker.visible = true;
			_parentControl.colorPicker.currentColor = _selectedColor;
			var location:Point = (_parentControl as DisplayObject).globalToLocal(localToGlobal(new Point(x,y)));
			if (_parentControl.colorPicker.direction==ColorPicker.DISPLAY_BELOW) 
				_parentControl.colorPicker.y = location.y + 16;
			else if (_parentControl.colorPicker.direction==ColorPicker.DISPLAY_LEFT)
				_parentControl.colorPicker.y = location.y - 14;
			_parentControl.colorPicker.addEventListener(ColorPicker.UPDATE_COLOR, handleUpdateColor);
			EditorUI.instance.mouseChildren = false;
			EditorUI.instance.addEventListener(MouseEvent.MOUSE_MOVE, chooseColor);
			if(Application.instance.windowArrangement > 1){
				_parentControl.colorPicker.addEventListener(MouseEvent.MOUSE_MOVE, chooseColor);
			}
		/*	if(_rb is RibbonView){
				(_rb as RibbonView).showTooltip(false);
			}*/
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.CHANGE_CURSOR_TO, {name:CursorNames.EYEDROPPER, type:CursorController.CURSOR_TYPE_LOCKED_TOOL}));
		}
		
		private function chooseColor(event:MouseEvent):void
		{
			_parentControl.colorPicker.chooseColor(event);
		}
		
		private function handleUpdateColor(event:Event):void
		{
			ApplicationController.instance.captureMouseClicks(false);
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
			
			EditorUI.instance.mouseChildren = true;
			_parentControl.colorPicker.removeEventListener(MouseEvent.MOUSE_MOVE, chooseColor);
			EditorUI.instance.removeEventListener(MouseEvent.MOUSE_MOVE, chooseColor);
			_parentControl.colorPicker.removeEventListener(ColorPicker.UPDATE_COLOR, handleUpdateColor);
			selectedColor = _parentControl.colorPicker.newColor;
			_parentControl.colorPicker.visible = false;
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_CURSOR));
			
			valueChanged(_selectedColor.toString());
			if(_token != null) {
				ApplicationController.currentActionTree.closeGroup(_token);
				_token = null;
			}
		}
		
		
		override public function set height(value:Number) : void
		{
			_chipSize.height = value;
			draw();
		}
		
		override public function set width(value:Number) : void
		{
			_chipSize.width = value;
			draw();
		}
		
		public function set selectedColor(value:uint):void
		{
			_selectedColor = value;
			draw();
		}
		
		override public function setValue(value:Object, valueType:String):void
		{
			selectedColor = uint(value);
		}
	}
}