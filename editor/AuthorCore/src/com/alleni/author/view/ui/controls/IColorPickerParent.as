package com.alleni.author.view.ui.controls
{
	public interface IColorPickerParent
	{
		function buildColorPickerIfNeeded():void;
		function get colorPicker():ColorPicker;
	}
}