/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.ui
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.TooltipMediator;
	import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class MessageCenterButton extends Sprite
	{
		private static const W:Number = 10;
		private static const H:Number = 10;
		
		private var _icon:DisplayObject;

		
		public function MessageCenterButton()
		{
			super();
			new TooltipMediator().handleTooltip(this, getToolTipText());
			
			graphics.clear();
			graphics.beginFill(0,0);
			graphics.drawRect(0,0,W,H);
			graphics.endFill();
		}
		
		public function set currentIcon(icon:DisplayObject):void
		{
			if (_icon)
				removeChild(_icon);
			addChild(icon);
			_icon = icon;
		}
				
		protected function getToolTipText():String
		{
			return "";
		}
	}
}