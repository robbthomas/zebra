/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.ui
{
	import com.alleni.author.model.AbstractObject;

	public class MessageCenterLockWiresButton extends MessageCenterButton
	{
		private var _state:Boolean;
		
		override protected function getToolTipText():String
		{
			return "Locks wires \n(wires cannot be changed or deleted)";
		}
		
		public function MessageCenterLockWiresButton()
		{
			super();
			_state = false;
			currentIcon = MessageCenterIcons.lockWires(_state);
		}
		
		public function draw(locked:Boolean):void
		{
			if (_state != locked) {
				_state = locked;
				currentIcon = MessageCenterIcons.lockWires(_state);
			}
		}
	}
}
