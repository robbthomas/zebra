package com.alleni.author.view.ui.controls
{
	import com.alleni.author.definition.application.PublishIcons;
	
	import flash.display.Sprite;
	
	public class RatingStars extends Sprite
	{
		public function RatingStars()
		{
			super();
			index = 0;
		}
		
		public function set index(value:int):void
		{
			while (this.numChildren > 0)
				this.removeChildAt(0);
			
			const iconName:String = "ratings" + value.toString();
			
			if (iconName in PublishIcons)
				this.addChild(PublishIcons[iconName]);
		}
	}
}