package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.definition.application.DockIcons;
import com.alleni.author.model.objects.IListable;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	public class LoadingItemPlaceholder extends Sprite
	{
		private var _item:IListable;
		private var _thumbnail:DisplayObject;
		
		public function LoadingItemPlaceholder(item:IListable, stageX:Number, stageY:Number, thumbnail:DisplayObject=null)
		{
			super();
			_item = item;
			_thumbnail = thumbnail;
			if (!feedbackContainer) return;
						
			var pos:Point = feedbackContainer.globalToLocal(new Point(stageX, stageY));
			this.x = pos.x;
			this.y = pos.y;
			this.mouseChildren = false;
			this.mouseEnabled = false;
			
			feedbackContainer.addChild(this);
			
			if (stage) initialize(null);
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(event:Event):void
		{
			if (event)
				removeEventListener(event.type, initialize);
			
			var icon:DisplayObject;
			if (_thumbnail) {
				icon = _thumbnail;
			} else {
				if (_item as Project) {
					icon = DockIcons.getByType((_item as Project).categoryId);
				} else { // source is an asset
					var asset:Asset = (_item as Asset);
					icon = DockIcons.getByType(asset.type);
				}
			}

			if (icon)
				this.addChild(icon);
			
			visible = true;
		}

		override public function set visible(value:Boolean):void
		{
			if (value) {
				super.visible = true;
				alpha = 1;
				//Tweener.addTween(this, {alpha:1, time:0.5, transition:"easeOutQuart"});
			} else {
				if (alpha == 0) {
					super.visible = false;
					if (this.parent)
						this.parent.removeChild(this);
				} else {
					Tweener.addTween(this, {alpha:0, time:0.5, transition:"easeOutQuart",
						onComplete:function():void {
							visible = false;
						}});
				}
			}
		}
		
		private static function get feedbackContainer():DisplayObjectContainer
		{
			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			
			if (worldContainer 
				&& worldContainer.feedbackControlsView) {
				return worldContainer.feedbackControlsView;
			}
			LogService.error("LoadingItemPlaceholder tried to display without a feedback parent");
			return null;
		}
	}
}
