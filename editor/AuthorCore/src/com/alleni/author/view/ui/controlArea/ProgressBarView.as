package com.alleni.author.view.ui.controlArea
{
    import caurina.transitions.Tweener;

    import com.alleni.author.view.ui.controls.PlayerControls;
    import com.alleni.author.model.ui.ProgressBarModel;

    import flash.display.Shape;
    import flash.display.Sprite;
    import flash.events.Event;

    import mx.events.PropertyChangeEvent;

    public class ProgressBarView extends Sprite
    {
        private var _model:ProgressBarModel;
        private var _width:Number = 500;
        private var _height:Number = 1;
        private var _progress:Number = 0;
        private var _background:Shape = new Shape();
        private var _progressRect:Shape = new Shape();
        private var _oldLiquidWidth:Number = 0;

        private static const MIN_PIXEL_ADVANCE:Number = 1;

        public function ProgressBarView(model:ProgressBarModel)
        {
            super();
            _model = model;
            model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, propertyChangeListener);
            addChild(_background);
            addChild(_progressRect);
        }

        public function initialize():void
        {
            updateBackground();
            updateProgress();
            this.alpha = _model.visible ? 1 : 0;
            super.visible = _model.visible;
            _progress = _model.progress;
        }

        private function propertyChangeListener(e:PropertyChangeEvent):void
        {   
            switch(e.property) {
                case "visible":
                    this.visible = _model.visible;
                    break;
                case "progress":
                    _progress = _model.progress;
                    updateProgress();
                    break;
                default: // nothing
                    break;
            }
        }

        private function updateBackground():void
        {
            with (_background.graphics) {
                clear();
                beginFill(0x666666);
                drawRect(0, 0, _width, _height);
                endFill();
            }
        }

        private function updateProgress():void
        {
            with (_progressRect.graphics) {
                clear();
                beginFill(0x54EBBE);
                drawRect(0, 0, _width * _progress, _height);
                endFill();
            }
        }

        public function scaleChanged(event:Event):void
        {
            updateBackground();
            updateProgress();
        }

        public function handleMeasure():void
        {
            updateBackground();
            updateProgress();
        }


        override public function get width():Number
        {
            return _width;
        }

        override public function set width(value:Number):void
        {
            _width = value;
            updateBackground();
            updateProgress();
        }

        override public function get height():Number
        {
            return _height;
        }

        override public function set height(value:Number):void
        {
            _height = value;
            updateBackground();
            updateProgress();
        }
            
        override public function set visible(value:Boolean):void
        {
            if (value) {
                super.visible = true;
                Tweener.addTween(this, {alpha:1, time:PlayerControls.ANIMATION_DURATION, transition:PlayerControls.ANIMATION_TRANSITION});
            } else {
                if (alpha == 0) {
                    super.visible = false;
                } else {
                    Tweener.addTween(this, {alpha:0, time:PlayerControls.ANIMATION_DURATION, transition:PlayerControls.ANIMATION_TRANSITION,
                        onComplete:function():void {
                            visible = false;
                        }});
                }
            }
        }
    }
}
