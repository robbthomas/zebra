package com.alleni.author.view.ui.controls
{
	import caurina.transitions.Tweener;
	
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	public class MediaScrubber extends Sprite
	{
		private static const DEFAULT_WIDTH:Number = 60.0;
		private static const DEFAULT_HEIGHT:Number = 15.0;
		private static const COMPRESSED_HEIGHT:Number = 3.0;
			
		private var _background:Sprite;
		private var _loadingBar:Shape;
		private var _playBar:Shape;
		private var _expandable:Boolean = true;
		private var _defaultHeight:Number = DEFAULT_HEIGHT;
		
		public function MediaScrubber(expandable:Boolean=true)
		{
			_loadingBar = new Shape();
			_playBar = new Shape();
			if (!expandable)
				_defaultHeight = COMPRESSED_HEIGHT;
			_expandable = expandable;
			
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			this.graphics.beginFill(0x7d7d7d, 0.01);
			this.graphics.drawRect(0, 0, DEFAULT_WIDTH, _defaultHeight);
			
			const radius:Number = _expandable?0:2;
			
			_background = new Sprite();
			_background.graphics.lineStyle(0, 0, 0, true);
			_background.graphics.beginFill(0x0099ff, 0.5);
			_background.graphics.drawRoundRect(0, 0, DEFAULT_WIDTH, _defaultHeight, radius, radius);
			_background.y = _defaultHeight - COMPRESSED_HEIGHT;
			
			_loadingBar.graphics.lineStyle(0, 0, 0, true);
			_loadingBar.graphics.beginFill(0xffffff, 0.3);
			_loadingBar.graphics.drawRoundRect(0, 0, DEFAULT_WIDTH, _defaultHeight, radius, radius);
			_loadingBar.scaleX = 0;
			
			_playBar.graphics.lineStyle(0, 0, 0, true);
			_playBar.graphics.beginFill(0xffffff);
			_playBar.graphics.drawRoundRect(0, 0, DEFAULT_WIDTH, _defaultHeight, radius, radius);
			_playBar.scaleX = 0;
			
			_background.addChild(_loadingBar);
			_background.addChild(_playBar);
			this.addChild(_background);
			this.scrollRect = new Rectangle(0, 0, DEFAULT_WIDTH, _defaultHeight);
			
			this.addEventListener(MouseEvent.ROLL_OVER, handleRollOver, false, 0, true);
			this.addEventListener(MouseEvent.ROLL_OUT, handleRollOut, false, 0, true);
		}
		
		private function handleRollOver(event:MouseEvent):void
		{
			Tweener.addTween(_background,{y:0, time:0.25, transition:"easeOutQuart"});
		}
		
		private function handleRollOut(event:MouseEvent):void
		{
			Tweener.addTween(_background,{y:_defaultHeight - COMPRESSED_HEIGHT, time:0.25, transition:"easeOutQuart"});
		}
		
		public function set fractionLoaded(value:Number):void
		{
			_loadingBar.scaleX = value;
		}
		
		public function set playProgress(value:Number):void
		{
			_playBar.scaleX = value;
		}
	}
}