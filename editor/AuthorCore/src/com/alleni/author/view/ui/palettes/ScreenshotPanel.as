/**
 * Created by IntelliJ IDEA.
 * User: allenlearning
 * Date: 6/15/11
 * Time: 4:33 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.ui.palettes {
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.GadgetScreenshotMediator;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.gadgets.GadgetScreenshot;
	import com.alleni.author.model.gadgets.GadgetState;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.view.objects.CompositeView;
	import com.alleni.author.view.ui.components.HBox;
	import com.alleni.author.view.ui.components.Style;
	import com.alleni.author.view.ui.components.VBox;
	import com.alleni.author.view.ui.controls.ScreenshotButtonContainer;
	import com.alleni.author.view.ui.controls.ScreenshotThumbnailConatiner;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.display.Sprite;
	
	
	public class ScreenshotPanel extends Sprite {
	
	    private var _horziontalLayout:Boolean = true
	    private var _horzContainer:HBox;
	    private var _vertContainer:VBox;
	    private var _thumbnails:ScreenshotThumbnailConatiner;
	    private var _buttons:ScreenshotButtonContainer;
	    private var _mediator:GadgetScreenshotMediator;
	
	    public function ScreenshotPanel(mediator:GadgetScreenshotMediator, gadget:Project = null, composite:CompositeView = null)
		{
	        this.visible = false;
	        _mediator = mediator;
	
	        Style.setStyle(Style.DARK);
	        _horzContainer = new HBox(this, 0, 0, 10);
	        _horzContainer.alignment = HBox.MIDDLE;
	        this.addChild(_horzContainer);
	
	        _vertContainer = new VBox(this);
	        _vertContainer.alignment = VBox.CENTER;
	        _vertContainer.visible = false;
	        this.addChild(_vertContainer);
	
	        _thumbnails = new ScreenshotThumbnailConatiner(this, 0, 0, 5, gadget, _mediator, _mediator.isZapp);
	        _horzContainer.addChild(_thumbnails);
	
	        _buttons = new ScreenshotButtonContainer(this, 0, 0, 5, gadget, _mediator);
			_buttons.defaultStyle = Style.DARK;
	        _horzContainer.addChild(_buttons);
	
	        ApplicationController.addEventListener(NotificationNamesApplication.WORLD_REPOSITIONED, draw);
	        ApplicationController.addEventListener(GadgetScreenshotMediator.NEW_SHOT_CAPTURED, updateScreenshots);
	        this.draw();
	    }
	
	    private function switchToVertLayout():void
		{
	        trace("Switch to Vertical");
	//         TODO: Install switch to vertical method
	    }
	
	    private function switchToHorzLayout():void
		{
	        trace("Switch to Horizontal");
	//         TODO: Install switch to horizontal method
	    }
	
	    public function setupForGadget(gadget:Project):void
		{
	    }
	
	    public function setupForGadgetState(gadgetState:GadgetState):void
		{
	    }
	
	    public function draw(event:ApplicationEvent = null):void
		{
	        graphics.clear();
	        graphics.beginFill(0x000000);
	        graphics.drawRect(0,0,EditorUI.instance.bottomControlBar.width, 80);
	
	        if(EditorUI.instance.bottomControlBar.width < 670 && _horziontalLayout){
	            switchToVertLayout();
	        }else if(EditorUI.instance.bottomControlBar.width >= 670 && !_horziontalLayout){
	            switchToHorzLayout();
	        }
	        _horzContainer.y = 5;
	        _horzContainer.x = (this.width - 970)/2;
	//        _vertContainer.y = 5;
	//        _vertContainer.x = (this.width - 330)/2;
	    }
	
	    private function updateScreenshots(event:ApplicationEvent):void
		{
			if (!event || !event.data || !_mediator || !_thumbnails) return;
			
	        var newScreenShot:GadgetScreenshot = event.data.screenshot as GadgetScreenshot;
			if (!newScreenShot) return;
			
	        newScreenShot.visibleIndex = event.data.index;
            _thumbnails.selectContainer(newScreenShot.visibleIndex);
	        _mediator.pendingUploads[newScreenShot.visibleIndex] = newScreenShot;
            _mediator.sessionUploads[newScreenShot.visibleIndex] = newScreenShot;
	        _thumbnails.updateThumb(newScreenShot.imageBitmapData);
	    }
	
	    public function get thumbnails():ScreenshotThumbnailConatiner {
	        return _thumbnails;
	    }
	
	    override public function set visible(value:Boolean):void
		{
	        super.visible = value;
	        if(visible){
	            thumbnails.updateThumbnails();
	        }
	    }
	}
}
