package com.alleni.author.view.ui.controls
{
	import flash.display.Sprite;
	
	public class ScreenshotCaptureFrame extends Sprite
	{
	    private var _height:Number;
	    private var _width:Number;
	    private var _displayBackground:Number
	
	
	    public function ScreenshotCaptureFrame(width:Number, height:Number, displayBG:Boolean = false)
		{
	        super();
	        _width = width;
	        _height = height;
	        _displayBackground = displayBG?.75:0;
	        buildFrame();
			this.mouseChildren = false;
			this.mouseEnabled = false;
	    }
	
	    private function buildFrame():void
		{
	        graphics.lineStyle(20, 0, .6);
	        graphics.drawRoundRect(-10, -10, _width + 20, _height + 20, 10);
	
	        graphics.lineStyle(1,0xFFFFFF);
	        graphics.beginFill(0xBFBFBF, _displayBackground);
	        graphics.drawRect(-1, -1, _width + 2, _height + 2);
	        graphics.endFill();
	
	        graphics.lineStyle(1, 0);
	        graphics.drawRect(-2,-2, _width + 4, _height + 4);
	    }
	}
}
