package com.alleni.author.view.ui
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.WireboardMediator;
	import com.alleni.author.definition.InletDescription;
	import com.alleni.author.definition.OutletDescription;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.ui.InternalSmartWireAnchor;
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Point;
		
	public class WireboardPortView extends PortView implements IWireboardItem
	{
		private var _icon:Sprite = new Sprite();
		private var _wireboard:WireboardView;
		private var _weight:Number;
		private var _type:String;
		private var _context:WorldContainer;
		
		public function WireboardPortView(anchor:WireAnchor,context:WorldContainer,wireboard:WireboardView)
		{
			addChild(_icon);
			_wireboard = wireboard;
			_context = context;
			
			if (anchor.modifierDescription as PropertyDescription)
				_type = WireboardView.PROPERTY;
			else if (anchor.modifierDescription as InletDescription)
				_type = WireboardView.INLET;
			else
				_type = WireboardView.OUTLET;
			
			super(anchor,context);  // invokes draw() which relies on _type set above
		}
		
		override public function get wireEndpoint():Point
		{
			var xx:Number = (side == 0) ? 4 : -4;
			return new Point(xx,0);
		}
		
		override public function draw():void
		{
			_icon.x = 0;
			_icon.y = 0;
			drawIcon(_icon, _type, side, anchor.modifierDescription.readOnly);
			
			// don't draw the default wire-end, since we have our own larger graphic
			_wireEnd.graphics.clear();
		}
		
		public static function drawIcon(sprite:Sprite, type:String, side:int, readOnly:Boolean=false):void
		{
			var s:Graphics = sprite.graphics;
			s.clear();
			
			if (type == WireboardView.PROPERTY) {
				if (readOnly) {
					s.beginFill(PortView.PROPERTY_COLOR);
					s.drawRect(0,0,6,6);
				} else {
					s.beginFill(PortView.PROPERTY_COLOR);
					s.drawCircle(0,0,5);
				}
			} else {
				var isInlet:Boolean = (type == WireboardView.INLET);
				sprite.rotation = side*180+(isInlet ? 0 : 180);
				
				s.beginFill(isInlet ? PortView.INLET_COLOR : PortView.OUTLET_COLOR);
				s.moveTo(-5,-6);
				s.lineTo(6,-1);
				s.lineTo(6,1);
				s.lineTo(-5,6);
				s.lineTo(-5,-5);	
				s.endFill();
			}
		}
		
		public function averageWireAngle(global:Point):Number
		{
			// return average angle from "global" to other ends of the wires
			if (!anchor.wired)
				return WireboardView.UNWIRED_PORT_WEIGHT;
			var n:int = 0;
			var sum:Number = 0;
			var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
			for each (var w:Wire in wires) {
				var other:WireAnchor = (anchor == w.masterAnchor ? w.slaveAnchor : w.masterAnchor);
				var nearest:Point = other.nearestPortLocation(global);
				if (nearest) {
					var delta:Point = nearest.subtract(global);
					var angle:Number = Math.atan2(delta.y, delta.x);
					sum += angle;
					n++;
				}
			}
			var result:Number = (n==0) ? WireboardView.UNWIRED_PORT_WEIGHT : (sum / n);
//			trace("   avg: wireCount="+n, "sum="+sum, "angle="+result, "delta="+delta, "global="+global, "other="+other.hostProperty);
			return result;
		}
				
		private function get side():int
		{
			return _wireboard.side;
		}
		
		
		public function get type():String
		{
			return _type;
		}
		
		public function get weight():Number
		{
			return _weight;
		}
		
		public function set weight(value:Number):void
		{
			_weight = value;
		}
		
		public function get incompleteAnchor():Boolean
		{
			// pressing a plus-button on the wireboard creates an incomplete anchor
			//... that is an internal anchor without an external anchor
			return InternalSmartWireAnchor(anchor).other == null;
		}
		
		public function convertToCustomAnchor(fromAnchor:WireAnchor):PortView
		{
			// convert an incomplete anchor to a full custom anchor and return the inner anchor port
			return _wireboard.convertToCustomAnchor(anchor as InternalSmartWireAnchor, fromAnchor);
		}
	}
}