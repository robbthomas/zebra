package com.alleni.author.view.ui
{
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.view.text.LightEditableLabel;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;

	public class RibbonValueField extends AbstractRibbonControl
	{
		private static const VALUE_BACKGROUND_COLOR:uint = 0x191919;
		private var _valueBackgroundColor:uint = VALUE_BACKGROUND_COLOR;
		protected var _valueField:LightEditableLabel;
		private var _valueBackground:Sprite;
		private var _background:Boolean;
		private var _readOnly:Boolean = false;
		private var _formatted:Boolean;
		private var _propertyType:String;
		private var _prompt:String = "";
		
		
		public function RibbonValueField(setValueCallback:Function, width:Number, height:Number, background:Boolean=true, readOnly:Boolean = false, formatted:Boolean=false, propertyType:String = null)
		{
			super(setValueCallback, width, height);
			
			_background = background;
			_readOnly = readOnly;
			_formatted = formatted;  //this is no longer needed
			_propertyType = propertyType;
			
			initialize();
		}

		public function get prompt():String
		{
			return _prompt;
		}

		public function set prompt(value:String):void
		{
			_prompt = value;
			
			if(_valueField)
				_valueField.prompt = value;
		}

		public function get valueBackgroundColor():uint
		{
			return _valueBackgroundColor;
		}

		public function set valueBackgroundColor(value:uint):void
		{
			_valueBackgroundColor = value;
			drawValueBackground();
		}

		public function get textSize():uint
		{
			return _valueField.size;
		}

		public function set textSize(value:uint):void
		{
			_valueField.size = value;
		}

		public function get color():uint
		{
			return _valueField.color;
		}

		public function set color(value:uint):void
		{
			_valueField.color = value;
		}


        public function set bold(value:Boolean):void {
            _valueField.bold = value;
        }

        public function get bold():Boolean {
            return _valueField.bold;
        }

        override public function set width(value:Number):void
		{
			super.width = value;
			_valueField.width = _width;
			drawValueBackground();
		}

		override public function set height(value:Number):void
		{
			super.height = value;
			_valueField.height = _height;
			drawValueBackground();
		}
		
		public function set textAlign(value:String):void
		{
			_valueField.textAlign = value;
		}
		
		
		public function set expandable(value:Boolean):void
		{
			_valueField.expandable =  value;
		}
		
		private function initialize():void
		{	
			_valueBackground = new Sprite();
			addChild(_valueBackground);
			drawValueBackground();
			
			var _keyPressFunction:Function = function(charCode:uint):uint{
				
				if(_propertyType != Modifiers.FORMATTED_TEXT_TYPE){
					return TextEditMediator.closeOnEnter(charCode);
				}
				else{
					return null;
				}
			};
			
			_valueField = new LightEditableLabel(_width, _height, false, _propertyType);

			var _closeFunction:Function = function(val:Object):void{
				_valueField.edit = false;
				
				if(_propertyType == Modifiers.FORMATTED_TEXT_TYPE){
					_valueField.formattedText = val;
				}
				else{
					if(val is String){
						val = FormattedText.convertToString(val as String);
					} else if(val is FormattedText){
						val = (val as FormattedText).toString();
					}
					_valueField.text = val as String;
					
				}
				valueChanged(val);
			}
			
			var callBacks:Object = {okEditFunc:null,updateFunc:null,keyPressFunc:_keyPressFunction,closeFunc:_closeFunction, hittestFunc:null};
			setUpParams(_valueField, callBacks);
			_valueField.embedFonts = true;
			
			if(_readOnly)
				_valueField.editable = false;
			
			_valueField.explicitLineBreak = true;
			
			addChild(_valueField);
			formatValue();
		}
		
		private function drawValueBackground():void
		{
			// Background box for the value input
			_valueBackground.graphics.clear();
			_valueBackground.graphics.lineStyle(_background?1:undefined);
			_valueBackground.graphics.beginFill(_valueBackgroundColor, _background?1:.01);
			if(!_background){
				_valueBackground.addEventListener(MouseEvent.CLICK, callEdit);
			}
			_valueBackground.graphics.drawRect(0,0,_width,_height);
//			var shadow:DropShadowFilter = new DropShadowFilter(2,0);
//			shadow.inner = true;
//			_valueBackground.filters = [shadow];
		}
		
		private function callEdit(event:MouseEvent):void{
			openEditing();
		}
		
		private function formatValue():void
		{
			_valueField.color = RibbonView.LABEL_COLOR;
			_valueField.size = 11;
		}

		/*
		override protected function valueChanged(value:Object):void
		{
			if(_formatted) { 
				value = FormattedText.fromString(String(value)); 
			}
			super.valueChanged(value);
		}
		*/

		override public function setValue(value:Object, valueType:String):void
		{
			if(value != null){
				if (value as FormattedText){
					_valueField.formattedText = value;
				} else if(value is Array) {
                    _valueField.text = (value as Array).map(function(item:Object, index:Number, arr:Array):Object {return Modifiers.convertValue(Modifiers.STRING_TYPE, item);}).join(",");
				} else{
					_valueField.text = value.toString();
				}
			}
		}

		override public function openEditing():void
		{
			_valueField.mediator.openEditing()
		}

		override public function closeEditing(e:Event):void
		{
			_valueField.mediator.closeEditing();
		}

		public function set text(value:String):void
		{
			_valueField.text = value;
		}
		
		private function setUpParams(label:LightEditableLabel, callBacks:Object):void
		{
			label.mediator.setCallBacks(callBacks);
			label.setPaddingParams({left:3, top:3, right:3, bottom:3});
		}
		
	}
}