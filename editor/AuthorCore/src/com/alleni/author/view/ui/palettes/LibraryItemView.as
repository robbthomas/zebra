package com.alleni.author.view.ui.palettes
{
	import com.alleni.author.controller.ui.palettes.LibraryItemMediator;
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.model.objects.IListable;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.Library;
import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.RichLabel;
	import com.alleni.taconite.event.ModelUpdateEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	import mx.events.PropertyChangeEvent;
	

	public class LibraryItemView extends Sprite
	{
		private static const BACKGROUND_COLOR:uint 			= 0x000000;
		private static const TEXT_COLOR:uint 				= 0xC8C8C8;
		private static const SELECTED_BACKGROUND_COLOR:uint = 0x484848;
		private static const SELECTED_TEXT_COLOR:uint 		= 0xFFFFFF;
		private static const BACKGROUND_ALPHA:Number 		= 0.85;
		private static const BORDER_COLOR:uint 				= 0x7D7D7D;
		private static const SELECTED_BORDER_COLOR:uint 	= 0xC8C8C8;
		private static const BORDER_THICKNESS:Number 		= 1.0;
		private static const ICON_FRAME_SIZE:Number			= 25.0;
		private static const ICON_SIZE:Number				= ICON_FRAME_SIZE-1.0;
	
		public var isCompressedView:Boolean = false;  //allow for scrollbar
		
		private var _model:Library;
		
		private var _totalWidth:Number = 175.0;
		private var _itemThickness:int = 125;
		
		private var _nameField:LightEditableLabel;
		private var _countLabel:RichLabel;
		
		private var _item:IListable;
		
		private var _selected:Boolean = false;
		private var _scrollNeeded:Boolean;
		
		private var _thumbnail:ItemThumbnail;
		
		
		public function LibraryItemView(item:IListable, scrollNeeded:Boolean)
		{
			super();
			
			_item = item;
			_scrollNeeded = scrollNeeded;
			
			_nameField = new LightEditableLabel();
			_countLabel = new RichLabel();
			
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(event:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			if (_scrollNeeded) {
				isCompressedView = true;
				_itemThickness = 110;
				_totalWidth = 160;
			} else
				isCompressedView = false;
			
			_nameField.explicitLineBreak = true;
			_nameField.editable = true;
			_nameField.embedFonts = true;
			_nameField.size = 12;
			_nameField.x = 28;
			_nameField.y = 6;
			_nameField.width = _itemThickness - 3;
			_nameField.setPaddingParams({left:0, top:3, right:0, bottom:0});
			addChild(_nameField);
			
			_countLabel.x = _totalWidth - ICON_FRAME_SIZE;
			_countLabel.y = 3;
			_countLabel.mouseEnabled = false;
			_countLabel.size = 12;
			_countLabel.width = ICON_FRAME_SIZE;
			_countLabel.height = 22;
			_countLabel.center = true;
			addChild(_countLabel);
			
			this.item = _item;
			render();
			updateIcon();
			
			var mediator:LibraryItemMediator = new LibraryItemMediator();
			mediator.handleViewEvents(this, _item);
			mediator.nameField = _nameField;
			model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);
		}
		
		private function handlePropertyChange(event:PropertyChangeEvent):void
		{
			switch(event.property) {
				case "selectedItem":
					selected = (_item == model.selectedItem);
					break;
				default:
					break;
			}
		}
		
		private function get model():Library
		{
			if (!_model)
				_model = Library.instance;
			return _model;
		}
		
		public function set item(value:IListable):void
		{
			_item = value;	
			_item.model.addEventListener(ModelUpdateEvent.MODEL_UPDATE, handleItemModelUpdate, false, 4, true);
			
			_nameField.text = GadgetDescription.cleanName(_item);
			selected = (_item == model.selectedItem);
            updateUsageCount();
		}

		private function handleItemModelUpdate(e:ModelUpdateEvent):void
		{
			switch (e.property) {
				case "name":
					_nameField.text = GadgetDescription.cleanName(_item);
					break;
				case "icon":
					updateIcon(true);
					break;
				case "content":
					if (e.oldValue == null)
						updateIcon(true);
					break;
				case "type": // if type had been the wildcard, we must be getting the real type. update the icon in a volatile fashion.
					if (int(e.oldValue) == -1)
						updateIcon(true);
					break;
			}
		}

        public function updateUsageCount():void
        {
            var count:int = _model.usageCounts.getReplaceableUsageCount(_item as IReplaceable);
            _countLabel.text = count.toString();
        }
		
		private function render():void
		{
			var textColor:uint;
			var borderColor:uint;
			
			graphics.clear();
			
			if (!_selected) {
				graphics.beginFill(BACKGROUND_COLOR, BACKGROUND_ALPHA);
				borderColor = BORDER_COLOR
				textColor = TEXT_COLOR;
			} else {
				graphics.beginFill(SELECTED_BACKGROUND_COLOR, BACKGROUND_ALPHA);
				borderColor = SELECTED_BORDER_COLOR
				textColor = SELECTED_TEXT_COLOR;
			}
			
			graphics.lineStyle(BORDER_THICKNESS, BORDER_COLOR, .5, true);
			graphics.moveTo(ICON_FRAME_SIZE + 1, 0);
			graphics.lineTo(_totalWidth, 0);
			
			graphics.lineStyle();
			graphics.drawRect(ICON_FRAME_SIZE, 0, _totalWidth-ICON_FRAME_SIZE, ICON_FRAME_SIZE);
			graphics.lineStyle(BORDER_THICKNESS, BORDER_COLOR, 1.0, true);
			graphics.moveTo(_itemThickness + ICON_FRAME_SIZE, 0);
			graphics.lineTo(_itemThickness + ICON_FRAME_SIZE, ICON_FRAME_SIZE);
			graphics.moveTo(_totalWidth - 1, 0);
			graphics.lineTo(_totalWidth - 1, ICON_FRAME_SIZE);
			
			graphics.lineStyle(BORDER_THICKNESS, BORDER_COLOR, 1.0, true);
            graphics.endFill();
            graphics.beginFill(0x99AFC4, BACKGROUND_ALPHA);
			graphics.drawRect(0, 0, ICON_FRAME_SIZE, ICON_FRAME_SIZE);
			
			_nameField.color = textColor;
			_countLabel.color = textColor;
		}
		
		private function updateIcon(refresh:Boolean=false):void
		{
			if (!_thumbnail) {
				_thumbnail = new ItemThumbnail(_item, ICON_SIZE, BORDER_THICKNESS, BORDER_THICKNESS);
				this.addChild(_thumbnail);
			}
			_thumbnail.render(refresh);
		}
		
		private function set selected(value:Boolean):void
		{
			if (value != _selected) {
				_selected = value;
				render();
			}
		}
	}
}
