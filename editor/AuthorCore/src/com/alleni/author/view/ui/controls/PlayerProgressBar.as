package com.alleni.author.view.ui.controls
{
	import caurina.transitions.Tweener;

	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;

	import mx.events.PropertyChangeEvent;

	import com.alleni.taconite.graphics.GlassBar;

	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.model.ui.ProgressBarModel;

	public class PlayerProgressBar extends Sprite
	{
		private static const DEFAULT_BASE_COLOR:uint = 0x666666;
		private static const DEFAULT_THEME_COLOR:uint = 0x56BFFF;
		private static const PROGRESS_HEIGHT:Number = 6.0;

		private var _model:ProgressBarModel;
		private var _bar:GlassBar;
		private var _progress:Shape;
		private var _progressWidth:Number;
		private var _loadingLabel:LightLabel;
		
		public function PlayerProgressBar(model:ProgressBarModel)
		{//TODO: destruct when not visible/needed??
			_model = model;
			model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);

			_bar = new GlassBar(true);
			addChild(_bar);

			_progress = new Shape();
			addChild(_progress);
			_progressWidth = 100;

            _loadingLabel = new LightLabel(65, 0xbfbfbf, false);
            _loadingLabel.size = 16;
            _loadingLabel.text = "Loading";
            addChild(_loadingLabel);

			this.mouseChildren = false;
			this.mouseEnabled = false;

			if (this.stage) initialize();
			else this.addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(e:Event = null):void
		{
			if (e) e.target.removeEventListener(Event.ADDED_TO_STAGE, initialize);
			this.alpha = 0;
			super.visible = false;

			var mainShadow:DropShadowFilter = new DropShadowFilter(0.0, 270, 0, 0.5, 10.0, 10.0, 0.9, 3);
			var buttonShadow:DropShadowFilter = new DropShadowFilter(1,80,0,0.6,2,2,2,0.5);
			
			_bar.filters = [mainShadow];
			_bar.height = PlayerControls.CONTROL_BAR_HEIGHT;

			_loadingLabel.filters = [buttonShadow];
			_progress.filters = [buttonShadow];
		}

		private function handlePropertyChange(e:PropertyChangeEvent):void
		{
			switch(e.property) {
				case "visible":
					this.visible = _model.visible;
					break;
				case "progress":
					updateProgress();
					break;
				default: // nothing
					break;
			}
		}

		private function updateProgress():void
		{
			var frac:Number = _model.progress;
			with (_progress.graphics) {
				clear();
				beginFill(DEFAULT_BASE_COLOR);
				drawRect(0, 0, _progressWidth, PROGRESS_HEIGHT);
				beginFill(DEFAULT_THEME_COLOR);
				drawRect(0, 0, _progressWidth * frac, PROGRESS_HEIGHT);
				endFill();
			}
		}
		
		public function handleMeasure():void
		{
			if (!stage) return;
			
			var stageWidth:Number = stage.stageWidth;			
			var stageHeight:Number = stage.stageHeight;
			if (this.parent.scrollRect) {
				stageWidth = this.parent.scrollRect.width;
				stageHeight = this.parent.scrollRect.height;
			}
			
			_bar.width = stageWidth;
			_progressWidth = stageWidth / 3;

			_loadingLabel.x = (_bar.width - _loadingLabel.width - _progressWidth)/2;
			_loadingLabel.y = (_bar.height - _loadingLabel.height)/2;

			_progress.x = _loadingLabel.x + _loadingLabel.width;
			_progress.y = (_bar.height - PROGRESS_HEIGHT)/2 - 1;

			updateProgress();
		}
			
		override public function set visible(value:Boolean):void
		{
			if (value) {
				super.visible = true;
				
				handleMeasure();
            	this.y = -_bar.height;
				Tweener.addTween(this, {alpha:0.8, y:0, time:PlayerControls.ANIMATION_DURATION, transition:PlayerControls.ANIMATION_TRANSITION});
			} else {
				if (alpha == 0) {
					super.visible = false;
				} else {
					Tweener.addTween(this, {alpha:0, y:-_bar.height, time:PlayerControls.ANIMATION_DURATION, transition:PlayerControls.ANIMATION_TRANSITION,
						onComplete:function():void {
							visible = false;
						}});
				}
			}
		}
	}
}