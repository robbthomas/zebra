package com.alleni.author.view.ui.controls
{
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.util.ViewUtils;
	import com.alleni.author.view.text.RichLabel;

import flash.display.Bitmap;

import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.text.TextFieldAutoSize;
	
	import mx.core.BitmapAsset;
	import mx.core.UIComponent;
	
	public class ColorPicker extends Sprite
	{
		public static const DISPLAY_BELOW:String = "Display Below";
		public static const DISPLAY_ABOVE:String = "Display Above";
		public static const DISPLAY_LEFT:String = "Display Left";
		public static const DISPLAY_RIGHT:String = "Display Right";
		public static const ARROW_SHOW:String = "Arrow Show";
		public static const ARROW_HIDDEN:String = "Arrow Hidden";
		
		//EVENT
		public static const UPDATE_COLOR:String = "Update Color";
		
		public var mouseMoveColor:uint;  // for GraphicFillPanel to access the color set by chooseColor()
		
		public static const HEIGHT:Number = 175;
		public static const ARROW_HEIGHT:Number = 11;
		
		private var _size:Object = {height:175, width:230};
		private var _leftGutter:int = 6;
		private var _topGutter:int = 6;
		private var _currentColor:uint;
		private var _newColor:uint;
		public var _active:Boolean = false;
		private var _screenBitmapData:BitmapData;
		private var _pickerBitmapData:BitmapData;
		private var _arrow:Sprite;
		
		private var _appContainer:UIComponent;
		
		private var _chipGrid:Sprite;
		private var _colorChipArray:Array = [
			//GRAYS
			{label:"", data:0xFFFFFF}, {label:"", data:0xE8E8E8}, {label:"", data:0xD1D1D1}, 
			{label:"", data:0xB9B9B9}, {label:"", data:0xA2A2A2}, {label:"", data:0x8B8B8B},
			{label:"", data:0x747474}, {label:"", data:0x5D5D5D}, {label:"", data:0x464646}, 
			{label:"", data:0x2E2E2E}, {label:"", data:0x212121}, {label:"", data:0x000000},
			//COLORS
			{label:"", data:0xFF0000}, {label:"", data:0xFF8000}, {label:"", data:0xFFFF00}, 
			{label:"", data:0x80FF00}, {label:"", data:0x00FF00}, {label:"", data:0x00FF80},
			{label:"", data:0x00FFFF}, {label:"", data:0x0080FF}, {label:"", data:0x0000FF}, 
			{label:"", data:0x8000FF}, {label:"", data:0xFF00FF}, {label:"", data:0xFF0080}
			];
		private var _chipGridVertical:int = 2;
		private var _chipSize:Object = {height:9, width:18};
		private var _chipPadding:Object = {horz:0, vert:0};
		
		private var _selectedChipSize:Object = {height:16, width:100};
		private var _colorSelectionRectangle:Sprite;
		private var _selectedColorText:RichLabel;
		
		private var _colorRibbon:Sprite;
		[Embed(source="/assets/images/colorPicker/zebra-color-spectrum.png",compression="true",quality="90")]
		private var SpectrumArt:Class;
		private var _colorSpectrum:BitmapAsset = BitmapAsset(new SpectrumArt());
		
		private var _colorChart:Sprite;
		[Embed(source="/assets/images/colorPicker/spectrum_chart.jpg")]
		private var ChartArt:Class;
		private var _colorSpectrumChart:BitmapAsset = BitmapAsset(new ChartArt());
		
		private var _direction:String = DISPLAY_BELOW;
		
		public function ColorPicker(appContainer:UIComponent)
		{
			super();
			
			_appContainer = appContainer;
		}
		
		
		public function draw():void
		{
			drawBackground();
			drawArrow(_direction);
			
			_colorSelectionRectangle = new Sprite();
			drawSelectedColor(0xFFFFFF);
			_colorSelectionRectangle.x = _leftGutter;
			_colorSelectionRectangle.y = 6;
			addChild(_colorSelectionRectangle);
			
			_colorRibbon = drawColorSpectrum();
			_colorRibbon.x = _leftGutter;
			_colorRibbon.y = _colorSelectionRectangle.y + _colorSelectionRectangle.height;
			addChild(_colorRibbon);
			
			_chipGrid = drawChips();
			_chipGrid.x = _leftGutter;
			_chipGrid.y = _colorRibbon.y + _colorRibbon.height + 6;
			addChild(_chipGrid);
			
			_colorChart = drawColorChart();
			_colorChart.x = _leftGutter;
			_colorChart.y = _chipGrid.y + _chipGrid.height + 6;
			addChild(_colorChart);
		}
		
		
		//FUNCTIONAL METHODS
		private function findColor():uint
		{
			return _screenBitmapData.getPixel(_appContainer.mouseX, _appContainer.mouseY);
		}
		
		private function findColorThis():uint
		{
			return _pickerBitmapData.getPixel(this.mouseX, this.mouseY);
		}
		
		public function chooseColor(event:MouseEvent):void
		{
			if(!_active){
				if (_screenBitmapData) _screenBitmapData.dispose();
				if (_pickerBitmapData) _pickerBitmapData.dispose();

				_screenBitmapData = ViewUtils.instance.takeScreenshot(_appContainer);

				_pickerBitmapData = new BitmapData(this.width, this.height);
				_pickerBitmapData.draw(this, new Matrix());
				
				if(true)   // FILLTEST:  previously tested Application.windowArrangement > 0
					addEventListener(MouseEvent.CLICK, selectColor);
				
				_appContainer.addEventListener(MouseEvent.CLICK, selectColor);
				_active = true;
			}
			var color:uint;
			if(event.currentTarget == _appContainer){
				color = findColor();
			}else{
				color = findColorThis();
			}
			drawSelectedColor(color);
			mouseMoveColor = color;
		}
		
		public function selectColor(event:MouseEvent):void
		{
			trace("SELECTCOLOR");
			if(event.currentTarget == _appContainer){
				_newColor = findColor();
				_appContainer.removeEventListener(MouseEvent.CLICK, selectColor);
			}else{
				_newColor = findColorThis();
				removeEventListener(MouseEvent.CLICK, selectColor);
			}
			_active = false;
			dispatchEvent(new Event(UPDATE_COLOR));
		}
		
		public function convertColorToString(color:uint):String
		{
			var tempString:String = color.toString(16).toUpperCase();
			var leadingZeroNeeded:int = 6 - tempString.length;
			var i:int;
			for(i=0;i<leadingZeroNeeded;i++){
				tempString = "0"+tempString;
			}
			return tempString;
		}
		
		//BUILD VISUAL OBJECTS
		private function drawColorChart():Sprite
		{
			var tempSprite:Sprite = new Sprite();
			_colorSpectrumChart.height = 85;
			_colorSpectrumChart.width = 216;
			tempSprite.addChild(_colorSpectrumChart);
			return tempSprite;
		}
		
		private function drawChips():Sprite
		{
			var tempGrid:Sprite = new Sprite();
			var tempSprite:Sprite;
			var i:int;
			var j:int;
			var k:int = 0;
			var columns:Number = Math.ceil(_colorChipArray.length/_chipGridVertical);
			for(i=0;i<_chipGridVertical; i++){
				for(j=0;j<columns;j++){
					tempSprite = new Sprite();
					tempSprite.graphics.beginFill(_colorChipArray[k].data);
					tempSprite.graphics.lineStyle(1,0x000000);
					tempSprite.graphics.drawRect(0,0,_chipSize.width, _chipSize.height);
					tempSprite.graphics.endFill();
					tempSprite.x = j*(_chipSize.width + _chipPadding.horz);
					tempSprite.y = i*(_chipSize.height + _chipPadding.vert);
					k++;
					tempGrid.addChild(tempSprite);
				}
			}
			return tempGrid;
		}
		
		private function drawColorSpectrum():Sprite
		{
			var tempSprite:Sprite = new Sprite();
			_colorSpectrum.height = 18;
			_colorSpectrum.width = 216;
			tempSprite.addChild(_colorSpectrum as DisplayObject);
			return tempSprite;
		}
		
		private function drawSelectedColor(selectedColor:int):void
		{
			_colorSelectionRectangle.graphics.clear();
			_colorSelectionRectangle.graphics.lineStyle(1,0xFFFFFF);
			_colorSelectionRectangle.graphics.drawRect(0,0,100,16);
			_colorSelectionRectangle.graphics.beginFill(selectedColor);
			_colorSelectionRectangle.graphics.lineStyle(1,0x000000);
			_colorSelectionRectangle.graphics.drawRect(1,1,98,14);
			_colorSelectionRectangle.graphics.endFill();
			if(!_selectedColorText){
				_selectedColorText = new RichLabel();
			}
			if(_colorSelectionRectangle.contains(_selectedColorText)){
				_colorSelectionRectangle.removeChild(_selectedColorText);
			}
			_colorSelectionRectangle.addChild(_selectedColorText);
			_selectedColorText.color = 0xFFFFFF;
			_selectedColorText.autoSize = TextFieldAutoSize.LEFT;

			_selectedColorText.text = convertColorToString(selectedColor);
			_selectedColorText.size = 16;
			_selectedColorText.x = 106;
			_selectedColorText.y = -3;
			_colorSelectionRectangle.addChild(_selectedColorText);	
		}
		
		private function drawArrow(direction:String):void
		{
			if (_arrow == null)
				_arrow = new Sprite();
			_arrow.graphics.clear();
			switch(direction){
				case DISPLAY_ABOVE:
					_arrow.graphics.beginFill(0x000000,.9);
					_arrow.graphics.moveTo(0,0);
					_arrow.graphics.lineTo(10,11);
					_arrow.graphics.lineTo(19,0);
					_arrow.graphics.endFill();
					_arrow.graphics.lineStyle(1,0x828282);
					_arrow.graphics.moveTo(0,0);
					_arrow.graphics.lineTo(10,11);
					_arrow.graphics.lineTo(19,0);
					_arrow.x = ((_size.width/2) - 5 + 18);
					_arrow.y = _size.height - 1;
					break;
				case DISPLAY_LEFT:
					_arrow.graphics.beginFill(0x000000,.9);
					_arrow.graphics.moveTo(0,0);
					_arrow.graphics.lineTo(11,10);
					_arrow.graphics.lineTo(0,19);
					_arrow.graphics.endFill();
					_arrow.graphics.lineStyle(1,0x828282);
					_arrow.graphics.moveTo(0,0);
					_arrow.graphics.lineTo(11,10);
					_arrow.graphics.lineTo(0,19);
					_arrow.x = _size.width - 1;
					_arrow.y = 7;
					break;
				case DISPLAY_RIGHT:
					_arrow.graphics.beginFill(0x000000,.9);
					_arrow.graphics.moveTo(11,0);
					_arrow.graphics.lineTo(0,10);
					_arrow.graphics.lineTo(11,19);
					_arrow.graphics.endFill();
					_arrow.graphics.lineStyle(1,0x828282);
					_arrow.graphics.moveTo(11,0);
					_arrow.graphics.lineTo(0,10);
					_arrow.graphics.lineTo(11,19);
					_arrow.x = -10;
					_arrow.y = 7;
					break;
				case DISPLAY_BELOW:
				default:
					_arrow.graphics.beginFill(0x000000,.9);
					_arrow.graphics.moveTo(0,11);
					_arrow.graphics.lineTo(10,0);
					_arrow.graphics.lineTo(19,11);
					_arrow.graphics.endFill();
					_arrow.graphics.lineStyle(1,0x828282);
					_arrow.graphics.moveTo(0,11);
					_arrow.graphics.lineTo(10,0);
					_arrow.graphics.lineTo(19,11);
					_arrow.x = ((_size.width/2) - 5 + 18);
					_arrow.y = -10;
					break;
			}
			addChild(_arrow);
		}
		
		private function drawBackground():void
		{
			graphics.lineStyle(1,0x828282, 1, true);
			graphics.beginFill(0x000000);
			graphics.drawRoundRectComplex(0,0,_size.width, _size.height,0,0,8,8);
			graphics.endFill();
			
			filters = getDropShadow();
		}
		
		private function getDropShadow():Array
		{
			var dsfTop:DropShadowFilter = new DropShadowFilter();
			dsfTop.color = 0x000000;
			dsfTop.angle = 45;
			dsfTop.alpha = .75;
			dsfTop.distance = 10;
			dsfTop.quality = 10;
			dsfTop.blurX = 5;
			dsfTop.blurY = 5;
			
			return [dsfTop];
		}
		
		//GETTERS and SETTERS
		public function get newColor():uint
		{
			return _newColor;
		}
		public function set newColor(value:uint):void
		{
			_newColor = value;
		}
		
		public function get currentColor():uint
		{
			return _currentColor;
		}
		public function set currentColor(value:uint):void
		{
			_currentColor = value;
			drawSelectedColor(_currentColor);
		}
		
		public function set direction(value:String):void
		{
			_direction = value;
		}
		
		public function get direction():String
		{
			return _direction;
		}
		
		/**
		 * Adjust arrow on top edge of color picker. 
		 * @param value horizontal center point of arrow, relative to color picker.
		 * 
		 */
		public function set arrowX(value:Number):void
		{
			if (_arrow) {  // for GraphicFillPanel to adjust the location of the arrow
				_arrow.x = value - 11;
			}
		}
	}
}