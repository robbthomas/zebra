/**
 * PushButton.as
 * Keith Peters
 * version 0.9.9
 * 
 * A basic button component with a label.
 * 
 * Copyright (c) 2011 Keith Peters
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
package com.alleni.author.view.ui.components
{
	import com.alleni.author.view.text.RichLabel;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.TextFieldAutoSize;

	public class PushButton extends Component
	{
		protected var _back:Sprite;
		protected var _face:Sprite;
		protected var _label:RichLabel;
		protected var _labelColor:uint = Style.LABEL_TEXT;
		protected var _labelText:String = "";
		protected var _over:Boolean = false;
		protected var _down:Boolean = false;
		protected var _selected:Boolean = false;
		protected var _toggle:Boolean = false;
		protected var _centerLabel:Boolean = true;
		protected var _skinClass:Class;
		
		/**
		 * Constructor
		 * @param parent The parent DisplayObjectContainer on which to add this PushButton.
		 * @param xpos The x position to place this component.
		 * @param ypos The y position to place this component.
		 * @param label The string to use for the initial label of this component.
 		 * @param defaultHandler The event handling function to handle the default event for this component (click in this case).
		 */
		public function PushButton(parent:DisplayObjectContainer = null, xpos:Number = 0, ypos:Number =  0, label:String = "", defaultHandler:Function = null, SkinClass:Class=null)
		{
			super(parent, xpos, ypos);
			if(defaultHandler != null)
			{
				addEventListener(MouseEvent.CLICK, defaultHandler);
			}
			this.label = label;
			_skinClass = SkinClass;
			_width = 70;
			_height = 25;
		}
		
		/**
		 * Initializes the component.
		 */
		override protected function init():void
		{
			super.init();
			buttonMode = true;
			useHandCursor = true;
			setSize(90, 22);
		}
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren():void
		{
			_back = new Sprite();
			_back.filters = [getShadow(2, true)];
			_back.mouseEnabled = false;
			addChild(_back);
			
			_face = new Sprite();
			_face.mouseEnabled = false;
			_face.filters = [getShadow(1)];
			_face.x = 1;
			_face.y = 1;
			addChild(_face);
			
			_label = new RichLabel();
			_label.mouseEnabled = false;
			_label.color = _labelColor;
			_label.size = 14;
			addChild(_label);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseGoDown);
			addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
		}
		
		/**
		 * Draws the face of the button, color based on state.
		 */
		protected function drawFace():void
		{
			if (!_skinClass) {
				if (defaultStyle != null)
					Style.setStyle(defaultStyle);
				
				_face.graphics.clear();
				_face.graphics.lineStyle(1, 0x666666, 1, true);//242021 524f50
				
				var matrix:Matrix = new Matrix();
				matrix.createGradientBox(_width, _height, (Math.PI/2), 0, 0);
				
				this.filters = [new GlowFilter(Style.BUTTON_FACE, 0.7, 2, 2, 1, 3, true)];
				
				if(_down)
				{
					_label.alpha = 0.7;
					_face.graphics.beginGradientFill(GradientType.LINEAR, [Style.BUTTON_DOWN, Style.BUTTON_HIGHLIGHT], [1, 1], [0x00, 0xff], matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
				}
				else
				{
					_label.alpha = 1.0;
					_face.graphics.beginGradientFill(GradientType.LINEAR, [Style.BUTTON_FACE, Style.BUTTON_HIGHLIGHT], [1, 1], [0x00, 0xff], matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
				}
				_face.graphics.drawRoundRect(0, 0, _width - 2, _height - 2, 10, 10);
				_face.graphics.endFill();
			} else {
				
			}
		}
		
		
		///////////////////////////////////
		// public methods
		///////////////////////////////////
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			super.draw();
			
			_back.graphics.clear();
			_back.graphics.lineStyle(0, 0, 0, true);
			_back.graphics.beginFill(Style.BACKGROUND);
			_back.graphics.drawRoundRect(0, 0, _width, _height, 10, 10);
			_back.graphics.endFill();
			
			drawFace();
			_back.alpha = _active?1:0;
			_face.alpha = _active?1:0;
			_label.italic = !_active;
			
			_label.text = _labelText;
			_label.autoSize = TextFieldAutoSize.CENTER;
			if(_label.width > _width - 4)
			{
				_label.autoSize = TextFieldAutoSize.NONE;
				_label.width = _width - 4;
			}
			else
			{
				_label.autoSize = TextFieldAutoSize.CENTER;
			}
			_label.x = _centerLabel ? _width / 2 - _label.width / 2 : 0;
			_label.y = _height / 2 - _label.height / 2 - 1;
			
			if (_label.multiline) _label.scrollRect = new Rectangle(0, 0, _width, _height);
			else _label.scrollRect = null;
		}
		
		
		
		
		///////////////////////////////////
		// event handlers
		///////////////////////////////////
		
		/**
		 * Internal mouseOver handler.
		 * @param event The MouseEvent passed by the system.
		 */
		protected function onMouseOver(event:MouseEvent):void
		{
			_over = true;
			addEventListener(MouseEvent.ROLL_OUT, onMouseOut);
		}
		
		/**
		 * Internal mouseOut handler.
		 * @param event The MouseEvent passed by the system.
		 */
		protected function onMouseOut(event:MouseEvent):void
		{
			_over = false;
			if(!_down)
			{
				_face.filters = [getShadow(1)];
			}
			removeEventListener(MouseEvent.ROLL_OUT, onMouseOut);
		}
		
		/**
		 * Internal mouseOut handler.
		 * @param event The MouseEvent passed by the system.
		 */
		protected function onMouseGoDown(event:MouseEvent):void
		{
			_down = true;
			drawFace();
			_face.filters = [getShadow(1, true)];
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseGoUp);
		}
		
		/**
		 * Internal mouseUp handler.
		 * @param event The MouseEvent passed by the system.
		 */
		protected function onMouseGoUp(event:MouseEvent):void
		{
			if(_toggle  && _over)
			{
				_selected = !_selected;
			}
			_down = _selected;
			drawFace();
			_face.filters = [getShadow(1, _selected)];
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseGoUp);
		}
		
		
		
		
		///////////////////////////////////
		// getter/setters
		///////////////////////////////////
		
		/**
		 * Sets / gets the label text shown on this Pushbutton.
		 */
		public function set label(str:String):void
		{
			_labelText = str;
			draw();
		}
		public function get label():String
		{
			return _labelText;
		}
		
		public function set labelSize(value:int):void
		{
			_label.size = value;
		}
		
		public function set labelMultiline(value:Boolean):void
		{
			_label.multiline = value;
			_label.autoSize = TextFieldAutoSize.NONE;
			if (value) _label.width = _width;
		}
		
		public function set labelColor(value:uint):void
		{
			_labelColor = value;
			_label.color = value;
		}
		
		public function set labelBold(value:Boolean):void
		{
			_label.bold = value;
		}
		
		public function set selected(value:Boolean):void
		{
			if(!_toggle)
			{
				value = false;
			}
			
			_selected = value;
			_down = _selected;
			_face.filters = [getShadow(1, _selected)];
		}
		public function get selected():Boolean
		{
			return _selected;
		}
		
		public function set toggle(value:Boolean):void
		{
			_toggle = value;
		}
		public function get toggle():Boolean
		{
			return _toggle;
		}
		
		public function set centerLabel(value:Boolean):void
		{
			_centerLabel = value;
		}
		public function get centerLabel():Boolean
		{
			return _centerLabel;
		}
	}
}