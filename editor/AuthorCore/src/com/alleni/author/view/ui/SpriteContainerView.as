/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;
	
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.TaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;

	public class SpriteContainerView extends TaconiteView
	{	
		private var _background:Sprite;
		private var _h:Number;
		private var _w:Number;
		private var _actualHeight:Number;
		
		private var _topCornerRadius:Number;
		private var _bottomCornerRadius:Number;
		
		private var _fillAlpha:Number = 0;
		private var _backgroundColor:uint;
		private var _backgroundAlpha:Number;
		private var _borderColor:Number;
		private var _borderAlpha:Number;
		private var _borderThickness:Number;
		
		private var _time:Number;
		private var _transition:String;
		
		public function SpriteContainerView(context:ViewContext, model:TaconiteModel)
		{
			super(context, model);
			
			// Default values	
			_h = 0; 
			_w = 150;
			_topCornerRadius = 8;
			_bottomCornerRadius = 8;
			_backgroundColor = 0x191919;
			_backgroundAlpha = 0;
			_borderColor = 0;
			_borderThickness = 0;
			_time = 0.4;
			_transition = "easeOutExpo";
			
			// Draw container
			updateView();
		}
		
		override protected function updateView() : void
		{
			super.updateView();
			graphics.beginFill(_backgroundColor, _backgroundAlpha);
			graphics.lineStyle(_borderThickness, _borderColor, _borderAlpha, true);
			graphics.drawRoundRectComplex(0,0,_w,_h,_topCornerRadius,_topCornerRadius,_bottomCornerRadius,_bottomCornerRadius);
		}
	
	// Set width and height without sprite scaling
		
		override public function set width(value:Number):void
		{
			_w = value;
			updateView();
		}
		
		override public function set height(value:Number):void
		{	
			_h = value;
			updateView();
		}
		
		override public function get height():Number
		{
			return _h;
		}
		
		public function set actualHeight(value:Number):void
		{
			_actualHeight = value;
		}
		
		public function get actualHeight():Number
		{
			return _actualHeight;	
		}
		
	
	// Tween the height of the container
	
		public function tweenHeight(value:Number, updateFunction:Function=null):void
		{	
			if (updateFunction!=null)
				Tweener.addTween(this, {height:value, time:_time, transition:_transition, onUpdate:updateFunction});
			else
				Tweener.addTween(this, {height:value, time:_time, transition:_transition});
		}
		
		
	//  Property getters and setters
		
		public function set topCornerRadius(value:Number):void
		{
			_topCornerRadius = value;
		}
		
		public function set bottomCornerRadius(value:Number):void
		{
			_bottomCornerRadius = value;
		}
		
		public function set fillAlpha(value:Number):void
		{
			_fillAlpha = value;
		}
		
		public function setBackground(color:uint, alpha:Number):void
		{
			_backgroundColor = color;
			_backgroundAlpha = alpha;
		}
				
		public function set borderColor( value:uint ):void
		{
			_borderColor = value;
		}
		
		public function set borderAlpha( value:uint ):void
		{
			_borderAlpha = value;
		}
		
		public function set borderThickness( value:uint ):void
		{
			_borderThickness = value;
		}
		
		public function set transition( value:String ):void
		{
			_transition = value;
		}
		
		public function get transition():String
		{
			return _transition;
		}
		
		public function set time( value:Number ):void
		{
			_time = value;
		}
		
		public function get time():Number
		{
			return _time;
		}
	}
}