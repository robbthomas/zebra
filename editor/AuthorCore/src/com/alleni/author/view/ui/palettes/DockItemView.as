package com.alleni.author.view.ui.palettes
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.controller.ui.palettes.DockItemMediator;
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.application.DockIcons;
	import com.alleni.author.model.objects.IListable;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Asset;
	import com.alleni.author.service.ThumbnailLoadingPipeline;
	import com.alleni.author.view.ui.controls.RatingStars;
	import com.alleni.taconite.dev.BitmapUtils;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Rectangle;
	
	import mx.events.PropertyChangeEvent;
	
	public class DockItemView extends Sprite
	{
		public var draggable:Sprite;
		
		private var _item:IListable;
		private var _icon:DisplayObject;
		private var _rating:RatingStars;
		private var _mediator:DockItemMediator;
		
		public function DockItemView(item:IListable):void
		{
			_item = item;
			
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(event:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			var gadget:Project = _item as Project;
			
			if (!_icon) {
				if (gadget) {
					_icon = DockIcons.getByType(gadget.categoryId);
					_icon.visible = false;
					loadGadgetIcon(gadget);
				} else { // source is an asset
					var asset:Asset = Asset(_item);
					_icon = DockIcons.getByType(asset.type);
				
					// currently, the back-end is only rendering thumbnails for images
					// in the future, we will also generate thumbnails for video
					// as well as gadgets and other things.
					if (asset.type == AssetType.BITMAP) {
						ThumbnailLoadingPipeline.pushAsset(asset.id, asset.name, function(event:Event):void {
							var loaderInfo:LoaderInfo = LoaderInfo(event.target);
							thumbnail = loaderInfo.loader.content;
						});
					}
				}
				draggable = new Sprite();
				_mediator = new DockItemMediator().handleViewEvents(draggable, _item) as DockItemMediator;
				
				_icon.x = -DockIcons.SIZE_SMALL / 2 + (DockIcons.SIZE_SMALL - _icon.width) / 2;
				_icon.y = -DockIcons.SIZE_SMALL / 2 + (DockIcons.SIZE_SMALL - _icon.height) / 2;
				
				this.addChild(draggable);
				draggable.addChild(_icon);
			}
			
			if (gadget) {
				gadget.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
				
				var shadow:DropShadowFilter = new DropShadowFilter(1, 80, 0x000000, 0.5, 1, 1, 0.8, 2);
				_rating = new RatingStars();
				_rating.x = -DockIcons.SIZE_SMALL / 2;
				_rating.y = _icon.y + _icon.height + 5;
				_rating.filters = [shadow];
				_rating.index = gadget.averageRating;
				this.addChild(_rating);
			}
		}
		
		public function refreshItem(item:IListable):void
		{
			var gadget:Project = _item as Project;
			if (!gadget) return;
			
			gadget.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
			
			_item = item;
			if (_mediator) _mediator.refreshItem(item);

			gadget = _item as Project;
			if (!gadget) return;
			
			gadget.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
		}
		
		override public function getBounds(targetCoordinateSpace:DisplayObject):Rectangle
		{
			var bounds:Rectangle = super.getBounds(targetCoordinateSpace);
			bounds.y -= DockIcons.SIZE_SMALL*(3/4);
			return bounds;
		}
		
		private function handlePropertyChange(event:PropertyChangeEvent):void
		{
			switch (event.property) {
				case "averageRating":
					_rating.index = int(event.newValue);
					break;
				case "icon":
					loadGadgetIcon(_item as Project);
					break;
				default:
					break;
			}
		}
		
		private function loadGadgetIcon(gadget:Project):void
		{
			if (!gadget) return;
			if (gadget.icon) {
				ThumbnailLoadingPipeline.push(gadget.icon.smallURL, function(event:Event):void {
					var loaderInfo:LoaderInfo = LoaderInfo(event.target);
					(loaderInfo.loader.content as Bitmap).smoothing = true;
					thumbnail = loaderInfo.loader.content;
					_icon.visible = true;
					_icon.alpha = 0;
					Tweener.addTween(_icon, {alpha:1, time:0.3, transition:"easeInOutQuart"});
				});
			}
		}
		
		private function set thumbnail(value:DisplayObject):void
		{
			const SIZE:Number = DockIcons.SIZE_SMALL;
			
			if (draggable.contains(_icon))
				draggable.removeChild(_icon);
			if (value.width > SIZE || value.height > SIZE){
				_icon = BitmapUtils.bilinearDownscale(value as Bitmap, SIZE);
            }else{
                _icon = value;
            }

			_icon.x = -SIZE / 2 + (SIZE - _icon.width) / 2;
			_icon.y = -SIZE / 2 + (SIZE - _icon.height) / 2;
			draggable.addChild(_icon);
		}

        public function clearToolTip():void
		{
            _mediator.clearToolTip();
        }
	}
}
