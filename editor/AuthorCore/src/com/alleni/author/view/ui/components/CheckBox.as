/**
 * CheckBox.as
 * Keith Peters
 * version 0.9.9
 * 
 * A basic CheckBox component.
 * 
 * Copyright (c) 2011 Keith Peters
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
package com.alleni.author.view.ui.components
{
	import com.alleni.author.view.text.LightLabel;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class CheckBox extends Component
	{
		protected var _back:DisplayObject;
		protected var _button:DisplayObject;
		protected var _label:LightLabel;
		protected var _labelText:String = "";
		protected var _selected:Boolean = false;
		private var _upSkin:Class;
		private var _downSkin:Class;
		
		
		/**
		 * Constructor
		 * @param parent The parent DisplayObjectContainer on which to add this CheckBox.
		 * @param xpos The x position to place this component.
		 * @param ypos The y position to place this component.
		 * @param label String containing the label for this component.
		 * @param defaultHandler The event handling function to handle the default event for this component (click in this case).
		 */
		public function CheckBox(parent:DisplayObjectContainer = null, xpos:Number = 0, ypos:Number =  0, label:String = "", defaultHandler:Function = null, upSkin:Class=null, downSkin:Class=null)
		{
			_labelText = label;
			_upSkin = upSkin;
			_downSkin = downSkin;
			super(parent, xpos, ypos);
			if(defaultHandler != null)
			{
				addEventListener(MouseEvent.CLICK, defaultHandler);
			}
		}
		
		/**
		 * Initializes the component.
		 */
		override protected function init():void
		{
			super.init();
			buttonMode = true;
			useHandCursor = true;
			mouseChildren = false;
		}
		
		/**
		 * Creates the children for this component
		 */
		override protected function addChildren():void
		{
			if (!_upSkin) {
				_back = new Sprite();
				_back.filters = [getShadow(2, true)];
			} else {
				_back = new _upSkin();
			}
			addChild(_back);
			
			if (!_downSkin) {
				_button = new Sprite();
				_button.filters = [getShadow(1)];
			} else {
				_button = new _downSkin();
			}
			_button.visible = false;
			addChild(_button);
			
			_label = new LightLabel(250);
			addChild(_label);
			_label.text = _labelText;
			draw();
			
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		
		
		
		///////////////////////////////////
		// public methods
		///////////////////////////////////
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			super.draw();
			if (!_upSkin) {
				const backSkin:Sprite = _back as Sprite;
				backSkin.graphics.clear();
				backSkin.graphics.beginFill(Style.BACKGROUND);
				backSkin.graphics.drawRect(0, 0, 10, 10);
				backSkin.graphics.endFill();
			}
			
			if (!_downSkin) {
				const downSkin:Sprite = _button as Sprite;
				downSkin.graphics.clear();
				downSkin.graphics.beginFill(Style.BUTTON_FACE);
				downSkin.graphics.drawRect(2, 2, 6, 6);
			}
			
			_label.text = _labelText;
			_label.x = _button.width + 8;
			_label.y = (_button.height - _label.height) / 2 + 4;
			_width = _label.width + _button.width;
			_height = _button.height;
		}
		
		
		
		
		///////////////////////////////////
		// event handler
		///////////////////////////////////
		
		/**
		 * Internal click handler.
		 * @param event The MouseEvent passed by the system.
		 */
		protected function onClick(event:MouseEvent):void
		{
			selected = !selected;
		}
		
		
		
		
		///////////////////////////////////
		// getter/setters
		///////////////////////////////////
		
		/**
		 * Sets / gets the label text shown on this CheckBox.
		 */
		public function set label(str:String):void
		{
			_labelText = str;
			invalidate();
		}
		public function get label():String
		{
			return _labelText;
		}
		
		public function set labelColor(value:uint):void
		{
			_label.color = value;
		}
		public function get labelColor():uint
		{
			return _label.color;
		}
		
		public function set labelSize(value:uint):void
		{
			_label.size = value;
		}
		public function get labelSize():uint
		{
			return _label.size;
		}
		
		public function set labelText(value:String):void
		{
			_labelText = value;
			_label.text = value;
		}
		public function get labelText():String
		{
			return _label.text;
		}
		
		/**
		 * Sets / gets the selected state of this CheckBox.
		 */
		public function set selected(s:Boolean):void
		{
			_selected = s;
			_button.visible = _selected;
			_back.visible = !_selected && !_enabled || _enabled;
		}
		public function get selected():Boolean
		{
			return _selected;
		}

		/**
		 * Sets/gets whether this component will be enabled or not.
		 */
		public override function set enabled(value:Boolean):void
		{
			super.enabled = value;
			mouseChildren = false;
			this.alpha = 1;
			_button.alpha = value?1:0.5;
			_back.alpha = value?1:0.5;
		}
	}
}