package com.alleni.author.view.ui.components
{
	import flash.display.Sprite;
	
	public class DotPagingItem extends Sprite
	{
		private static const SELECTED_RADIUS:Number = 10.0;
		private static const UNSELECTED_RADIUS:Number = 6.0;
		
		private var _selected:Boolean = false;
		private var _index:int = 0;
		
		public function DotPagingItem(index:int, selected:Boolean)
		{
			super();
			_index = index;
			_selected = selected;
			
			init();
		}
		
		private function init():void
		{
			const RADIUS:Number = _selected?SELECTED_RADIUS:UNSELECTED_RADIUS;
			const POSITION:Number = (SELECTED_RADIUS - RADIUS) / 2;
			graphics.clear();
			graphics.beginFill(_selected?Style.HIGHLIGHTED_COLOR:Style.UNHIGHLIGHTED_COLOR, 0.7);
			graphics.drawEllipse(POSITION, POSITION, RADIUS, RADIUS);
			if (_selected) return;
			graphics.beginFill(0, 0);
			graphics.drawEllipse(0, 0, SELECTED_RADIUS, SELECTED_RADIUS);
		}
		
		public function get index():int
		{
			return _index;
		}
		
		public function set selected(value:Boolean):void
		{
			_selected = value;
			init();
		}
	}
}