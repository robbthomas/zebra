/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.ui
{
	import com.alleni.author.controller.ui.TooltipMediator;
	import com.alleni.author.controller.ui.WireDragMediator;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.InletDescription;
	import com.alleni.author.definition.OutletDescription;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.objects.AbstractTable;
import com.alleni.author.model.objects.Calculator;
import com.alleni.author.model.objects.LogicObject;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.taconite.dev.BitmapUtils;
	import com.alleni.taconite.dev.ColorUtils;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.CapsStyle;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.JointStyle;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	
	import mx.events.PropertyChangeEvent;

	public class PortView extends Sprite
	{
		public static const WIRE_CHANGED:String = "Wire Changed";
		
		private static const UNSELECTED_COLOR:uint = 0xC8C8C8; 	// grey
		public static const INLET_COLOR:uint = 0xFF3232;		// red
		public static const OUTLET_COLOR:uint = 0xFFB234;  	// orange
		public static const PROPERTY_COLOR:uint = 0x0099FF;	// blue
		private static const SELECTED_COLOR:uint = 0xFFFFFF;	// white

		private static const PROPERTY_WIDTH:Number = 3;
		private static const PROPERTY_SELECTED_WIDTH:Number = 8;

		private static const PROPERTY_READONLY_WIDTH:Number = 4.0;
		private static const PROPERTY_READONLY_THICKNESS:Number = 1;
		private static const PROPERTY_READONLY_SELECTED_WIDTH:Number = 6;
		private static const PROPERTY_READONLY_SELECTED_THICKNESS:Number = 2;

		private static const TRIANGLE_WIDTH:Number = 4.275;
		private static const TRIANGLE_HEIGHT:Number = 3.5;

		private static const TRIANGLE_SELECTED_WIDTH:Number = 8.55;
		private static const TRIANGLE_SELECTED_HEIGHT:Number = 7;

		private static const RIBBON_FILL_COLOR:uint = 0x282828;
		private static const RIBBON_HIGHLIGHT_COLOR:uint = 0x7D7D7D;

		private static const CROSS_WIDTH:Number = 6;
		private static const CROSS_THICKNESS:Number = 1;

		public static const WIDTH:Number = 20;
		
		private static const RADIUS:Number = 1;  // inactive port radius
		private static const COLOR:uint = 0xcbcbcb; // inactive port color
		
		private var _anchor:WireAnchor;
		private var _context:WorldContainer;
		
		private var _powerColor:uint;
		
		private var _angle:int;
		protected var _highlight:Shape = new Shape();
		protected var _wireEnd:Shape = new Shape();
		private var _outsideMessageCenter:Boolean;
		private var _unselectedColor:uint = UNSELECTED_COLOR;
		private var _uiRotation:Number;
		
		public function PortView(anchor:WireAnchor, context:WorldContainer, outsideMessageCenter:Boolean=false, unselectedColor:int=-1, uiRotation:Number = 0)
		{
			_anchor = anchor;
			_context = context;
			_anchor.register(this);
			_uiRotation = uiRotation;
			
			if (unselectedColor >= 0)
				_unselectedColor = unselectedColor;
			
			addChild(_highlight);
			addChild(_wireEnd);
			_wireEnd.x = wireEndpoint.x;
			_wireEnd.y = wireEndpoint.y;
			_outsideMessageCenter = outsideMessageCenter;
			draw();
			anchor.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);

			if (_outsideMessageCenter) {
				var tipText:String = anchor.modifierDescription.description;
				new TooltipMediator().handleTooltip(this, tipText);
			}

			new WireDragMediator(context).handlePortEvents(this, context.role);
			this.addEventListener(MouseEvent.MOUSE_OVER, addRollHighlight, false, 0, true);
			this.addEventListener(MouseEvent.MOUSE_OUT, removeRollHighlight, false, 0, true);
		}

		private function handlePropertyChange(e:PropertyChangeEvent):void
		{
			if(e.property == "wired") {
				dispatchEvent(new Event(WIRE_CHANGED));
				draw();
			}
		}
		
		public function get angle():int 
		{
			return _angle;
		}

		public function set angle(value:int):void 
		{
			_angle = value;
			_wireEnd.rotation = angle;
		}

		public function get worldContainer():WorldContainer 
		{
			return _context;
		}
		
		public function draw():void
		{
			// Set wire end angle
			_wireEnd.rotation = angle;
			
			// Draw port
			_highlight.graphics.clear();

			var key:String = _anchor.modifierDescription.key;

			graphics.clear();
			while(this.numChildren > 0) {
				this.removeChildAt(0);
			}
			addChild(_highlight);
			addChild(_wireEnd);

			if(isSpecialInlet) {
				switch (key) {
					case "judgeNow":
						drawJudgeNowPort();
						break;
					case "reset":
						drawResetPort();
						break;
					case "first":
						drawFirstPort();
						break;
					case "previous":
						drawPreviousPort();
						break;
					case "start":
						drawPlayPort();
						break;
					case "next":
						drawNextPort();
						break;
					case "last":
						drawLastPort();
						break;
					case "resume":
						drawResumePort();
						break;
					case "pause":
						drawPausePort();
						break;
					case "togglePause":
						drawTogglePausePort();
						break;
					case "stop":
						drawStopPort();
						break;
					case "gotoURL":
						drawGoToURLPort();
						break;
					default:
						LogService.error("Unknown special port type [" + key + "]");
				}
			} else if(key == "powerOn") {
				drawPowerOnPort();
			} else if(key == "powerOff") {
				drawPowerOffPort();
			}else {
				_highlight.alpha = 0;
				_highlight.graphics.beginFill(1, 0.01);
				_highlight.graphics.drawRect(0,0, WIDTH, RibbonView.HEIGHT);
				drawWireEnd(_wireEnd.graphics, _anchor.modifierDescription, _anchor.wired);
			}
		}
		
		private function addRollHighlight(event:MouseEvent):void
		{
			var key:String = _anchor.modifierDescription.key;
			if(isSpecialInlet) {
				BitmapUtils.tint(this, ColorUtils.brightenColor(INLET_COLOR, 40));
			}else{
				if (_anchor.isAdditionAnchor) {
					_wireEnd.filters = [new GlowFilter(0xffffff)];
				} else {
					var color:uint;
					if (_anchor.modifierDescription as PropertyDescription)
						color = PROPERTY_COLOR;
					else if (_anchor.modifierDescription as InletDescription)
						color = INLET_COLOR;
					else if (_anchor.modifierDescription as OutletDescription)
						color = OUTLET_COLOR;
					_highlight.alpha = 1;
					BitmapUtils.tint(_highlight, ColorUtils.brightenColor(color, 40));
				}
			}
		}
		
		private function removeRollHighlight(event:MouseEvent):void
		{
			var key:String = _anchor.modifierDescription.key;
			if(isSpecialInlet) {
				BitmapUtils.untint(this);
			} else {
				_wireEnd.filters = [];
				BitmapUtils.untint(_highlight);
				_highlight.alpha = 0;
			}
		}

        public function updateWireEnd():void{
            drawWireEnd(_wireEnd.graphics, _anchor.modifierDescription, _anchor.wired);
        }

		protected function drawWireEnd(g:Graphics, modifier:IModifier, attached:Boolean = false):void
		{
			g.clear();
			if(anchor.isAdditionAnchor) {
                var endColor:uint = (anchor.hostObject is Calculator)?0x000000:PROPERTY_COLOR;
                var crossColor:uint = 0xFFFFFF;
                for each(var obj:WireAnchor in anchor.hostObject.anchors[modifier.key]){
                    if(obj.wired == false && obj.hostPropertyIndex < anchor.hostPropertyIndex){
                        endColor = (obj.hostObject is Calculator)?PROPERTY_COLOR:0x000000;
                        crossColor = 0x7D7D7D;
                        break;
                    }
                }
				g.lineStyle(undefined);
				g.beginFill(endColor);
				g.drawCircle(0, 0, (PROPERTY_SELECTED_WIDTH+1)/2);
				g.endFill();

				g.lineStyle(CROSS_THICKNESS, crossColor, 1.0, true, "normal", CapsStyle.NONE);
				g.moveTo(0, -CROSS_WIDTH/2);
				g.lineTo(0, CROSS_WIDTH/2);
				g.moveTo(-CROSS_WIDTH/2, 0);
				g.lineTo(CROSS_WIDTH/2, 0);
				return;
			}
			
			if(modifier is PropertyDescription) {
				if(modifier.readOnly) {
					if(attached) {
						g.lineStyle(PROPERTY_READONLY_SELECTED_THICKNESS, PROPERTY_COLOR, 1.0, true, "normal", CapsStyle.NONE, JointStyle.MITER);
						drawSquare(g, PROPERTY_READONLY_SELECTED_WIDTH);
					} else {
						g.lineStyle(PROPERTY_READONLY_THICKNESS, 0x51A8FF, 1.0, true, "normal", CapsStyle.NONE, JointStyle.MITER);
						drawSquare(g, PROPERTY_READONLY_WIDTH);
					}
				} else {
					g.lineStyle(undefined);
					if(attached) {
						g.beginFill(PROPERTY_COLOR);
						g.drawCircle(0, 0, PROPERTY_SELECTED_WIDTH/2);
					} else {
						g.beginFill(0x51A8FF);
						g.drawCircle(0, 0, PROPERTY_WIDTH/2);
					}
					g.endFill();
				}
			} else {
				g.lineStyle(undefined);
				if(attached) {
					if(modifier is OutletDescription) {
						g.beginFill(OUTLET_COLOR);
					} else {
						g.beginFill(INLET_COLOR);
					}
					drawTriangle(g, TRIANGLE_SELECTED_WIDTH, TRIANGLE_SELECTED_HEIGHT,
							(modifier as InletDescription != null && !(this as WireboardPortView != null))
						||	(modifier as OutletDescription != null && (this as WireboardPortView != null)));
				} else {
					if(modifier is OutletDescription) {
						g.beginFill(OUTLET_COLOR);
					} else {
						g.beginFill(INLET_COLOR);
					}
					drawTriangle(g, TRIANGLE_WIDTH, TRIANGLE_HEIGHT,
							(modifier as InletDescription != null && !(this as WireboardPortView != null))
						||	(modifier as OutletDescription != null && (this as WireboardPortView != null)));
				}
				g.endFill();
			}
		}

		protected function drawSquare(g:Graphics, width:Number):void 
		{
			g.drawRect(-width/2, -width/2, width, width);
		}


		protected function drawTriangle(g:Graphics, width:Number, height:Number, flip:Boolean):void 
		{
			var x1:Number = flip ? height/2 : -height/2;
			var x2:Number = flip ? -height/2 : height/2;
			g.moveTo(x1, -width/2);
			g.lineTo(x2, 0);
			g.lineTo(x1, +width/2);
			g.lineTo(x1, -width/2);
		}
		
		private function drawTruthTablePort():void
		{
			graphics.beginFill(0x323232, 1); 
			graphics.drawCircle(WIDTH/2 - RADIUS/2, RibbonView.HEIGHT/2 - RADIUS/2, RADIUS);
		}
		
//		private function drawAddRowPort(logic:Boolean):void
//		{
//			const w:Number = 6;
//
//			graphics.beginFill(logic?0xA0A0A0:0x4A84C3);
//			graphics.drawCircle(WIDTH/2, RibbonView.HEIGHT/2, w);
//			graphics.lineStyle(2, 0x000000, 1.0, true, "normal", CapsStyle.NONE);
//			graphics.moveTo(WIDTH/2,(RibbonView.HEIGHT-w)/2);
//			graphics.lineTo(WIDTH/2,RibbonView.HEIGHT-(RibbonView.HEIGHT-w)/2);
//			graphics.moveTo((WIDTH-w)/2, RibbonView.HEIGHT/2);
//			graphics.lineTo(WIDTH-(WIDTH-w)/2, RibbonView.HEIGHT/2);
//		}
		
		private function drawGoToURLPort():void{
			var bg:Sprite = new Sprite();
			addChild(bg);
			bg.graphics.lineStyle(2, _anchor.wired?INLET_COLOR:_unselectedColor, 1, false, "normal", CapsStyle.NONE);
			bg.graphics.moveTo(4,4);
			bg.graphics.lineTo(14,4);
			bg.addChild(drawArrow1());
			bg.getChildAt(0).y = 4;
			bg.getChildAt(0).x = 14;
			
			drawBottomTriangle();
		}
		
		private function drawJudgeNowPort():void
		{
			var bg:Sprite = new Sprite();
			addChild(bg);
			
			bg.graphics.lineStyle(1.5,_anchor.wired?INLET_COLOR:_unselectedColor, 1.0, false, "normal", CapsStyle.NONE);
			
			bg.graphics.moveTo(-2.5, -5.5);
			bg.graphics.lineTo(-2.5, .5);
			
			bg.graphics.moveTo(2.5, -5.5);
			bg.graphics.lineTo(2.5, .5);
			
			bg.graphics.moveTo(0, -3);
			bg.graphics.lineTo(0, 5.5);
			
			bg.graphics.lineStyle(2.5,_anchor.wired?INLET_COLOR:_unselectedColor, 1.0, false, "normal", CapsStyle.NONE);
			
			bg.graphics.moveTo(-2.5, -3);
			bg.graphics.lineTo(2.5, -3);
			
			bg.rotation = 45;
			bg.x = ((WIDTH - 6.5)/2)+2;
			bg.y = ((RibbonView.HEIGHT - 11)/2)+2;
			
			drawBottomTriangle();
		}
		
		private function get line():DisplayObject
		{
			var shape:Shape = new Shape();
			shape.graphics.lineStyle(2,_anchor.wired?INLET_COLOR:_unselectedColor, 1.0, false, "normal", CapsStyle.NONE);
			shape.graphics.lineTo(0,4);
			
			return shape;
		}
		
		private function drawResetPort():void
		{
			drawBottomTriangle();
			
			var reset:Sprite = resetPort;
			reset.x += 6;
			
			addChild(reset);
		}

		private function get resetPort():Sprite
		{
			var bg:Sprite = new Sprite();
			
			var circle:Sprite = new Sprite();
			circle.graphics.lineStyle(2,_anchor.wired?INLET_COLOR:_unselectedColor,1.0,false,"normal",CapsStyle.NONE);
			circle.graphics.drawCircle(0,0,4);
			circle.x = 4;
			circle.y = 4;
			bg.addChild(circle);
			
			var mask:Sprite = new Sprite();
			mask.graphics.beginFill(0x0089C8);
			mask.graphics.drawRect(-1,-1,10,6);
			mask.graphics.drawRect(3,0,6,10);
			bg.addChild(mask);
			
			circle.mask = mask;
			
			var arrow:Sprite = new Sprite();
			arrow.graphics.beginFill(_anchor.wired?INLET_COLOR:_unselectedColor);
		//	arrow.graphics.lineStyle(1,_unselectedColor,1.0,false,"normal",CapsStyle.NONE);
			arrow.graphics.lineTo(4,4);
			arrow.graphics.lineTo(8,0);
			arrow.graphics.lineTo(0,0);
			
			arrow.x = -4;
			arrow.y = 3;
			
			bg.addChild(arrow);
			
			return bg;
			
		}
		
		private function drawPowerOnPort():void{
			var power:Sprite = powerOnPort;
			addChild(power);
		}
		
		private function drawPowerOffPort():void{
			var power:Sprite = powerOffPort;
			addChild(power);
		}
		
		private function get powerOffPort():Sprite{
			var bg:Sprite = new Sprite();
			
			bg.graphics.lineStyle(1,_anchor.wired?0x000000:0x000000,1.0,false,"normal",CapsStyle.NONE);
			bg.graphics.beginFill(_powerColor);
			bg.graphics.drawCircle(4,4,4.5);
			bg.graphics.endFill();
			
			if(_uiRotation == 90){
				bg.graphics.moveTo(4,0);
				bg.graphics.lineTo(4,8);
			}else{
				bg.graphics.moveTo(0,4);
				bg.graphics.lineTo(8,4);
			}
			
			bg.x = (WIDTH - bg.width)/2;
			bg.y = (RibbonView.HEIGHT - bg.height)/2;
			
			return bg;
		}
		
		private function get powerOnPort():Sprite{
			var bg:Sprite = new Sprite();
			
			bg.graphics.lineStyle(1,_anchor.wired?0x000000:0x000000,1.0,false,"normal",CapsStyle.NONE);
			bg.graphics.beginFill(_powerColor);
			bg.graphics.drawCircle(4,4,4.5);
			bg.graphics.endFill();
			
			bg.graphics.moveTo(4,4);
			if(_uiRotation == 90){
				bg.graphics.lineTo(8,4);
			}else{
				bg.graphics.lineTo(4,-3);
			}
			
			bg.x = (WIDTH - bg.width)/2;
			bg.y = (RibbonView.HEIGHT - bg.height)/2;
			
			return bg;
		}
		
		private function drawPlayPort():void
		{
			drawPlayTriangle(6);

			drawBottomTriangle();
		}

		private function drawPlayTriangle(startX:Number):void
		{
			with (graphics) {
				beginFill(_anchor.wired?INLET_COLOR:_unselectedColor);
				moveTo(startX+0,0);
				lineTo(startX+0,8);
				lineTo(startX+6,4);
				lineTo(startX+0,0);
				endFill();
			}
		}

		private function drawPausePort():void
		{
			drawBottomTriangle();
			drawVerticalBar(7);
			drawVerticalBar(11);
		}

		private function drawResumePort():void
		{
			drawBottomTriangle();
			drawVerticalBar(4);
			drawPlayTriangle(8);
		}
		
		private function drawLastPort():void
		{
			drawVerticalBar(9);
			
			drawBottomTriangle();
		}

		private function drawVerticalBar(startX:Number):void
		{
			with (graphics) {
				beginFill(_anchor.wired?INLET_COLOR:_unselectedColor);
				moveTo(startX+0,0);
				lineTo(startX+0,8);
				lineTo(startX+2,8);
				lineTo(startX+2,0);
				lineTo(startX+0,0);
				endFill();
			}
		}

		private function drawStopPort():void
		{
			drawBottomTriangle();
			const startX:Number = 6;
			const startY:Number = 0;
			with (graphics) {
				beginFill(_anchor.wired?INLET_COLOR:_unselectedColor);
				moveTo(startX+0,startY+0);
				lineTo(startX+0,startY+8);
				lineTo(startX+8,startY+8);
				lineTo(startX+8,startY+0);
				lineTo(startX+0,startY+0);
				endFill();
			}
		}

		private function drawTogglePausePort():void
		{
			drawBottomTriangle();
			drawVerticalBar(6);
			const startX:Number = 6;
			const startY:Number = 3;
			with (graphics) {
				beginFill(_anchor.wired?INLET_COLOR:_unselectedColor);
				moveTo(startX+0,startY+0);
				lineTo(startX+0,startY+2);
				lineTo(startX+8,startY+2);
				lineTo(startX+8,startY+0);
				lineTo(startX+0,startY+0);
				endFill();
			}
		}
		
		private function drawPreviousPort():void
		{
			var startX:Number = 6;
				
			drawBottomTriangle();
			
			with (graphics) {
				beginFill(_anchor.wired?INLET_COLOR:_unselectedColor);
				moveTo(startX+6,0);
				lineTo(startX+0,4);
				lineTo(startX+6,8);
				lineTo(startX+6,6);
				lineTo(startX+3,4);
				lineTo(startX+6,2);
				lineTo(startX+6,0);
				endFill();
			}
		}
		
		private function drawNextPort():void
		{	
			var startX:Number = 6;
			
			drawBottomTriangle();
			
			with (graphics) {
				beginFill(_anchor.wired?INLET_COLOR:_unselectedColor);
				moveTo(startX+0,0);
				lineTo(startX+6,4);
				lineTo(startX+0,8);
				lineTo(startX+0,6);
				lineTo(startX+3,4);
				lineTo(startX+0,2);
				lineTo(startX+0,0);
				endFill();
			}
		}
		
		private function drawFirstPort():void
		{
			drawLastPort(); // if the design is the single line (no triangle), then first and last port look the same
		}
		
		private function drawBottomTriangle():void
		{
			var w:Number = TRIANGLE_WIDTH;
			var h:Number = TRIANGLE_HEIGHT;
			var startY:Number = 10;
			if(_anchor.wired) {
				startY -= 1;
				w += 2;
				h += 2;
			}
			
			graphics.beginFill(_anchor.wired?INLET_COLOR:_unselectedColor);
			graphics.moveTo(WIDTH/2, startY);
			graphics.lineTo(WIDTH/2-w/2, startY+h);
			graphics.lineTo(WIDTH/2+w/2, startY+h);
			graphics.lineTo(WIDTH/2, startY);
			
			// drop point for wire larger than actual inlet to make wiring easier
			graphics.beginFill(0x00ff00, 0.01);
			graphics.drawEllipse(0, 0, WIDTH, WIDTH);
		}
		
		private function drawArrow1():Sprite{
			var _triangle:Sprite;
			_triangle = new Sprite();
			_triangle.graphics.clear();
			_triangle.graphics.beginFill(_anchor.wired?INLET_COLOR:_unselectedColor);
			_triangle.graphics.moveTo(-3.5, -3);
			_triangle.graphics.lineTo(3.5, 0);
			_triangle.graphics.lineTo(-3.5, 3);
			return _triangle;
		}
		
		public function get model():TaconiteModel
		{
			return _anchor.model;
		}
		
		public function get anchor():WireAnchor
		{
			return _anchor;
		}

		private function get isSpecialInlet():Boolean {

			if(_outsideMessageCenter && _anchor.modifierDescription is InletDescription && _anchor.hostObject is LogicObject) {
				switch (_anchor.modifierDescription.key) {
					case "judgeNow":
					case "reset":
					case "first":
					case "previous":
					case "start":
					case "next":
					case "last":
					case "stop":
					case "pause":
					case "togglePause":
					case "resume":
					case "gotoURL":
					return true;
				}
			}
			return false;
		}
		
		public function get wireEndpoint():Point
		{
			if(isSpecialInlet) {
				const h:Number = TRIANGLE_HEIGHT;
				const startY:Number = 10;
				return new Point(WIDTH/2,startY + h/2);
			} else {
				return new Point(WIDTH/2,RibbonView.HEIGHT/2);
			}
		}
		
		public function set powerColor(value:uint):void{
			_powerColor = value;
			draw();
		}
		
		override public function toString():String
		{
			return "[PortView anchor="+_anchor + "]";
		}
	}
}