/**
 * placeholder for future Alert-type popup to replace the standard Flash component which assumes our app lives only in one window. 
 */
package com.alleni.author.view.ui.controls
{
	import com.alleni.author.application.dialogs.AlertDialog;
	import com.alleni.author.controller.ui.ApplicationController;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	import mx.controls.Alert;
	import mx.core.IFlexModuleFactory;
	import mx.events.CloseEvent;
	
	public class Alert
	{
		public static var FIRST:uint = mx.controls.Alert.CANCEL; // Right most button
        public static var SECOND:uint = mx.controls.Alert.OK;
        public static var THIRD:uint = mx.controls.Alert.YES;
		public static var FOURTH:uint = mx.controls.Alert.NO;
		public static var NONMODAL:uint = mx.controls.Alert.NONMODAL;
		
		protected static var _alert:AlertDialog = null;
		private static var _closeHandler:Function = null;
		
		public static function show(text:String = "", title:String = "", flags:uint = 0x4, 
									closeHandler:Function = null, iconClass:Class = null, 
									defaultButtonFlag:uint = 0x4, moduleFactory:IFlexModuleFactory = null, mouse:Point = null):AlertDialog
		{
			if (_alert == null) {
				_alert = ApplicationController.instance.createPopUp(AlertDialog, false) as AlertDialog;
				_alert.titleText.text = title;
				_alert.bodyText.text = text;
				_alert.setupButtons(flags);
				_closeHandler = closeHandler;
				
				if (mouse) {
					_alert.x = mouse.x-_alert.width/2;
					_alert.y = mouse.y-_alert.baselinePosition;
				}
				_alert.addEventListener(Event.CLOSE, handleClose);
				
				switch(defaultButtonFlag) {
					case FOURTH:
						_alert.defaultButton = _alert.noButton;
						break;
					case THIRD:
						_alert.defaultButton = _alert.yesButton;
						break;
					case FIRST:
						_alert.defaultButton = _alert.okButton;
						break;
					case SECOND:
						_alert.defaultButton = _alert.cancelButton;
						break;
					default: // no default button
						break;
				}
			}
			return _alert;
		}
		
		public static function set buttonWidth(value:Number):void
		{
			mx.controls.Alert.buttonWidth = value;
		}
		
		public static function set buttonHeight(value:Number):void
		{
			mx.controls.Alert.buttonHeight = value;
		}
		
		public static function set cancelLabel(value:String):void
		{
			mx.controls.Alert.cancelLabel = value;
		}
		
		public static function set noLabel(value:String):void
		{
			mx.controls.Alert.noLabel = value;
		}
		
		public static function set okLabel(value:String):void
		{
			mx.controls.Alert.okLabel = value;
		}
		
		public static function set yesLabel(value:String):void
		{
			mx.controls.Alert.yesLabel = value;
		}
		
		private static function handleClose(event:Event):void
		{
			_alert.removeEventListener(Event.CLOSE, handleClose);
			_alert = null;
            if (_closeHandler != null) _closeHandler(event);
		}
	}
}
