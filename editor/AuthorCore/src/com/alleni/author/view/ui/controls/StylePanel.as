package com.alleni.author.view.ui.controls
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.view.ui.AbstractRibbonControl;
	import com.alleni.author.view.ui.MenuButton;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.model.ModelRoot;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	
	import mx.controls.CheckBox;
	import mx.controls.List;
	import mx.core.ClassFactory;
	import mx.core.UIComponent;
		
	
	public class StylePanel extends UIComponent
	{
		
		private var _appContainer:UIComponent;
		private var _host:AbstractRibbonControl;
		public static var instance:StylePanel;
		public static var list:List;
		private var _menu:PopOutMenu;
		
		private var _callback:Function;
	
		
		// dimensions of whole panel
		private static const WIDTH:Number = 231;
		private static const HEIGHT:Number = 200;
		private static const HEIGHT_GRADIENT:Number = 120;
		private static const CORNER_RADIUS:Number = 8;
		private static const BACKGROUND_COLOR:uint = 0x000000;
		private static const BACKGROUND_EDGE_COLOR:Number = 0x828282;
		
		private static const DIVIDER_LINE_COLOR:Number = 0x585858;
		private static const DIVIDER_LINE_Y:Number = 160;
		private static const DIVIDER_LINE2_Y:Number = 189;
		
		private static const FRAME_X:Number = 10;
		private static const FRAME_Y:Number = 9;
		private static const FRAME_WIDTH:Number = 250;
		private static const FRAME_HEIGHT:Number = 134;
		private static const FRAME_COLOR:uint = 0xc8c8c8;
		private static const LIST_WIDTH:uint = WIDTH * .9;
		
		// locations of controls
		
	
		private static const BUTTON_ICON_COLOR:uint = 0x969696;
		private static const X_MENU:Number = 2;
		private static const Y_MENU:Number = 67;
		private static const MENU_WIDTH:Number = 60;
		private static const MENU_HEIGHT:Number = 16;
		private static const FILL_WIDTH:Number = 145;
		private static const FILL_HEIGHT:Number = 30;
		private static const OK_X:Number = 178;
		private static const OK_Y:Number = 164;
		private static const OK_WIDTH:Number = 44;
		private static const OK_HEIGHT:Number = 20;
		private static const CANCEL_X:Number = 126;
		private static const CANCEL_Y:Number = 164;
		private static const CANCEL_WIDTH:Number = 44;
		private static const CANCEL_HEIGHT:Number = 20;
		
		
		private var _token:*;
		
		
		
		public function StylePanel(appContainer:UIComponent)
		{
			Utilities.assert(instance == null);
			instance = this;
			super();
			_appContainer = appContainer;
			// TODO: find a way to position in the ApplicationController
			EditorUI.instance.addPopOver(this);
			
			//add children to panel here
			
			createButton(OK_X,OK_Y, drawOkButton, okAction, "OK");
			createButton(CANCEL_X,CANCEL_Y, drawCancelButton, cancelAction, "Cancel");

			
			// dropshadow on whole table
			var dropShadow:DropShadowFilter = new DropShadowFilter();
			dropShadow.blurX = 15;
			dropShadow.blurY = 15;
			dropShadow.angle = 45;
			dropShadow.alpha = 0.90;
			dropShadow.distance = 6;
			//this.filters = [dropShadow];
		}
		
		public static function showPanel(appContainer:UIComponent, valueCallback:Function,  global:Point, host:AbstractRibbonControl, obj:AbstractObject = null):void
		{
			if (!instance)
				new StylePanel(appContainer);
			else
				instance.visible = true;
			
			instance._host = host;
			instance._token = ApplicationController.currentActionTree.openGroup("Pick Style");
			
			instance._callback = valueCallback;
			instance.x = global.x - 20;
			instance.y = global.y - 20;
			instance.finishShowPanel();
			instance.alpha = 1;
			
			
			showCheckBoxList();
		}
		
		public static function showCheckBoxList():void{
			
			
			if (!list)
				list = new CheckBoxList();
			
			
				list.id = "StyleList";
				list.x = FRAME_X
				list.y = FRAME_Y;
				list.height = FRAME_HEIGHT;
				list.width = LIST_WIDTH;
				
				//list.setStyle("textFormat", new TextFormat(Fonts.DEFAULT_FAMILY, 12, 0x000000));
				
				//list.itemRenderer = new ClassFactory(CheckBox); this works
				var cf:ClassFactory = new ClassFactory(CheckBox);
				list.itemRenderer = cf;
				//var checkboxArray:Array = [{label:"Style 1",  selected:true}, {label:"Style 2", selected:false}, {label:"Style 3", selected:true}];
				var checkboxArray:Array = fetchCSSAssets();
				
				if(checkboxArray.length > 0)
				list.dataProvider = checkboxArray;
				
				instance.addChild(list);
				list.visible = true;
		}
		
		public static function fetchCSSAssets():Array{
			var cssAssets:Array = new Array();
			var assets:ModelRoot = Application.instance.document.assetsRoot;
			
			for each(var asset:Asset in assets.valueChildren){
				
				if(asset.type == AssetType.CSS){
					var styleSheet:Object = new Object();
					styleSheet.label = asset.name;
					styleSheet.id = asset.assetID;
					styleSheet.selected = false;
					cssAssets.push(styleSheet);
				}
			}
			
			return cssAssets;
			
		}
		
		
		
		private function finishShowPanel():void
		{
			// force panel to be within screen bounds
			
			if (x > _appContainer.width - WIDTH) {
				x = _appContainer.width - WIDTH;
			}
			
			if (y > _appContainer.height - HEIGHT) {  // leave room below for the color picker
				y = _appContainer.height - HEIGHT;
			}
			
			setupPanelMode();
			
			
			ApplicationController.instance.captureMouseClicks(true);
		}
		
		public static function closePanel():void
		{
			if (instance)
				instance.closeThisPanel();
		}
		
		
		private function closeThisPanel():void
		{
			_host.closeEditing(null);
			this.visible = false;
			_callback = null;
			_menu = null;
			ApplicationController.instance.captureMouseClicks(false);
			
			if(_token != null) {
				ApplicationController.currentActionTree.closeGroup(_token);
				_token = null;
			}
		}
		
		public static function getStyles():Object{
			
			var selectedStyles:String = new String();  //replace with another data type
			var dp:* = list.dataProvider;
			var separator:String = "";
			
			
			for(var i:Number = 0; i < dp.length; i++){  //rows will exceed data items
			
				if(dp.getItemAt(i).selected == true){
					separator = selectedStyles.length > 0?";":"";
					selectedStyles += separator + dp.getItemAt(i).id;
				}
			}
			
			return selectedStyles;
			
		}
		
		
		private function updatePanelView():void
		{
			
				
		}
		
		public function setupPanelMode():void
		{
			drawBackground(this.graphics);
			updatePanelView();
		}
		
		
		private function drawBackground(graphics:Graphics):void
		{
			// background of whole panel
			graphics.clear();
			graphics.lineStyle(1, BACKGROUND_EDGE_COLOR);
			graphics.beginFill(BACKGROUND_COLOR);  
			graphics.moveTo(0, 0);
			graphics.lineTo(WIDTH, 0);
			graphics.lineTo(WIDTH, HEIGHT-CORNER_RADIUS);
			graphics.curveTo(WIDTH, HEIGHT, WIDTH-CORNER_RADIUS, HEIGHT);
			graphics.lineTo(CORNER_RADIUS, HEIGHT);
			graphics.curveTo(0, HEIGHT, 0, HEIGHT-CORNER_RADIUS);
			graphics.lineTo(0, 0);
			graphics.endFill();
			
			// horz divider lines
			graphics.moveTo(-1,DIVIDER_LINE_Y);
			graphics.lineTo(WIDTH-1,DIVIDER_LINE_Y);
			
			// fill view
			//graphics.lineStyle();  // no outline on frame
			//graphics.beginFill(FRAME_COLOR);
			//graphics.drawRect(FRAME_X, FRAME_Y, FRAME_WIDTH, FRAME_HEIGHT);
			//graphics.endFill();
		}
		
		
		private function createButton(xx:Number, yy:Number, drawButton:Function, action:Function, label:String):MenuButton
		{
			var btn:MenuButton = new MenuButton(drawButton,action);
			btn.toggle = false;
			btn.color = 0x00303030;
			btn.x = xx;
			btn.y = yy;
			btn.labelText = label;
			btn.labelFontSize = 11;
			btn.labelColor = 0xffffff;
			addChild(btn);
			return btn;
		}
		
		private function drawOkButton( btn:Sprite ):void
		{	
			const R:Number = 5; //rounding
			
			// fill the button with gray, round the left side corners
			btn.graphics.lineStyle(0,0,0, true);  // zero alpha, to eliminate line
			btn.graphics.drawRoundRect(0,0,OK_WIDTH,OK_HEIGHT,R);
		}
		
		private function drawCancelButton( btn:Sprite ):void
		{	
			const R:Number = 5; //rounding
			
			// fill the button with gray, round the left side corners
			btn.graphics.lineStyle(0,0,0, true);  // zero alpha, to eliminate line
			btn.graphics.drawRoundRect(0,0,CANCEL_WIDTH,CANCEL_HEIGHT,R);
		}
		
		private function okAction():void
		{
			closeThisPanel();
			
		}
		
		private function cancelAction():void
		{
			if(_token) {
				ApplicationController.currentActionTree.cancelGroup(_token);
				_token = null;
			}
			updatePanelView();
			closeThisPanel();
		}
		
		
		public static function get isActive():Boolean
		{
			if (instance)
				return instance.visible;  // true if the fill panel is active (so MC tooltips should not show)
			else
				return false;
		}
		
		
		
	}
}

	
import flash.events.Event;

import mx.controls.CheckBox;
import mx.controls.List;
import mx.controls.listClasses.IListItemRenderer;

class CheckBoxList extends List 
{
	
	function CheckBoxList(){
	   super();
	   overrideStyles();
	}
	
	override public function createItemRenderer(data:Object):IListItemRenderer
		{
		
		var cb:CheckBox = new CheckBox();
		cb.addEventListener(Event.CHANGE, onChangeHandler);
		
		           if(data.hasOwnProperty("label")){
						cb.label = data.label;
					}
		
				if(data.hasOwnProperty("selected")){
						cb.selected = data.selected;
				}
				
				return cb;
					
		
		return null;
	    }
	
	private function overrideStyles():void{
		this.setStyle("textSelectedColor",0x000000);
		this.setStyle("fontSize",14);
		this.setStyle("rollOverColor",0x000000);
		this.setStyle("textRollOverColor",0xFFFFFF);
		this.setStyle("color",0x000000);
		this.setStyle("selectionColor", 0x00FFFFFF);
	}
	
	private function onChangeHandler(event:Event):void
	        {
		            this.dataProvider.getItemAt(this.selectedIndex).selected = (event.target as CheckBox).selected;
	        }
}

	



	
