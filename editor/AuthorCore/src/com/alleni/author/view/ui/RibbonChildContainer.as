/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.ui
{
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.WireController;
import com.alleni.author.definition.OutletDescription;
import com.alleni.author.definition.action.ModifyRibbonAction;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.HelicopterWireAnchor;
import com.alleni.author.model.ui.MetaWireAnchor;
import com.alleni.author.model.ui.PathNodeWireAnchor;
import com.alleni.author.model.ui.SetterWireAnchor;
import com.alleni.author.model.ui.TriggerWireAnchor;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.feedback.PathNodeTransformationGhost;
import com.alleni.author.view.ui.editors.TransformationEditor;
import com.alleni.taconite.view.ViewContext;

import flash.display.Sprite;

import flash.events.MouseEvent;
import flash.geom.Rectangle;

import mx.events.PropertyChangeEvent;

public class RibbonChildContainer extends RibbonSubContainer
	{		
		private var _parentContainer:RibbonSubContainer;
		private var _ribbon:RibbonView;
		private var _childEditor:AbstractRibbonControl;
        private var _valueControlBackground:Sprite;
        private var _valueControlRectOpen:Rectangle;

        public function RibbonChildContainer(parentContainer:RibbonSubContainer, ribbon:RibbonView, viewContext:ViewContext, role:uint, label:String, color:uint, type:String)
		{
			_ribbon = ribbon;
			super(null, viewContext, role, label, color, type, parentContainer.messageCenter, false);

			_parentContainer = parentContainer;
			if(_ribbon.ribbon is HelicopterWireAnchor) {
				var heli:HelicopterWireAnchor = _ribbon.ribbon as HelicopterWireAnchor;
				_childEditor = AbstractRibbonControl.createControl(heli.childModifier, valueCallback, heli.hostObject, heli.childValue, RibbonView.VALUE_WIDTH, RibbonView.HEIGHT);
				_childEditor.x = ((_childEditor as AbstractSwitch) ? PortView.WIDTH : RibbonView.VALUE_X);
				_childEditor.y = 4;
				_childEditor.setValue(heli.childValue, heli.childModifier.type);
                heli.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, valueListener, false, 0, true);
				
				if(_childEditor is TransformationEditor && heli is SetterWireAnchor && SetterWireAnchor(heli).prototypeAnchor is PathNodeWireAnchor)
				{
					TransformationEditor(_childEditor).ghostClass = PathNodeTransformationGhost;
				}
                _valueControlRectOpen = new Rectangle(0,0,RibbonView.VALUE_X,RibbonView.HEIGHT);
                _header.labelTextObject.scrollRect = _valueControlRectOpen;

                if(isOpen == false && _childEditor) {
                    drawValueControlBackground();
                    _header.addChild(_childEditor)
				}
			} else {
				_childEditor = null;
			}
			super.showHeader = true;
        }

        private function valueListener(event:PropertyChangeEvent):void {
            if(event.property == "childValue") {
                var heli:HelicopterWireAnchor = HelicopterWireAnchor(_ribbon.ribbon);
                _childEditor.setValue(heli.childValue, heli.childModifier.type);
            }
        }

		private function valueCallback(value:Object):void {
            var oldChildValue:Object = HelicopterWireAnchor(_ribbon.ribbon).childValue;
			HelicopterWireAnchor(_ribbon.ribbon).childValue = value;
            ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(_ribbon.ribbon.hostObject,HelicopterWireAnchor(_ribbon.ribbon).childPath,oldChildValue));
		}
		
		override protected function doCallback():void
		{
			_parentContainer.handleSubContainerCallback();
		}
		
		override protected function set showHeader(value:Boolean):void
		{	
			if (value || !_ribbon.ribbon.wired)
				super.showHeader = value;
		}
		
		override protected function setupContainerHeading():void
		{
			// Create header
			_header = new RibbonChildContainerHeading(_color, _label, false);
			_header.y = -2;
			addChild(_header);
			
			_ribbon.addEventListener(MouseEvent.CLICK, handleHeadingClick, false, 0, true);
			_ribbon.addEventListener(MouseEvent.ROLL_OVER, handleHeadingRollOver, false, 0, true);
			_ribbon.addEventListener(MouseEvent.ROLL_OUT, handleHeadingRollOut, false, 0, true);
		}
		
		override protected function get headerHeight():Number
		{
			return 15;	
		}

        private function drawValueControlBackground():void {
            _valueControlBackground = new Sprite();
            _valueControlBackground.graphics.beginFill(0x191919);
            _valueControlBackground.graphics.lineStyle(1,0x000000);
            _valueControlBackground.graphics.drawRect(-3,1,RibbonView.VALUE_WIDTH+3,RibbonView.HEIGHT+1);
            _valueControlBackground.graphics.endFill();
            _valueControlBackground.x = RibbonView.VALUE_X + _ribbon.ribbonLibraryBtnPadding;
            _valueControlBackground.y = RibbonView.VALUE_Y;
            _header.addChild(_valueControlBackground);
        }

		override protected function open(closeOthers:Boolean = true):void {
			if(Application.instance.wireDragDescription != null && Application.instance.wireDragDescription.fromAnchor.modifierDescription is OutletDescription && this._ribbon.ribbon is SetterWireAnchor){
                return;
            }
            if(_childEditor && _childEditor.parent == _header) {
                _header.removeChild(_valueControlBackground);
				_header.removeChild(_childEditor);
			}
			super.open(closeOthers);
		}

		override protected function close(tween:Boolean = true):void {
            for each(var ribbon:RibbonView in this._ribbonViews){
                if(ribbon.rolled){
                    return;
                }
            }
            if(_childEditor) {
                _header.addChild(_valueControlBackground);
                _header.addChild(_childEditor);
				_childEditor.setValue(HelicopterWireAnchor(_ribbon.ribbon).childValue, HelicopterWireAnchor(_ribbon.ribbon).childModifier.type);
			}
			super.close(tween);
		}

        override public function addRibbonView(anchor:WireAnchor, childAnchor:WireAnchor):void {
            super.addRibbonView(anchor,childAnchor);
            updatePerceivedHeight();
        }
	}
}