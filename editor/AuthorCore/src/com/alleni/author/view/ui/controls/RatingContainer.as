package com.alleni.author.view.ui.controls
{
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.ui.components.HBox;
	
	import flash.display.DisplayObjectContainer;
	
	public class RatingContainer extends HBox
	{
		private var _ratingStars:RatingStars;
		private var _label:LightLabel;
		
		public function RatingContainer(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, spacing:Number=5)
		{
			super(parent, xpos, ypos, spacing);
			this.alignment = HBox.MIDDLE;
		}
		
		public function set index(value:int):void
		{
			if (!_ratingStars) return;
			_ratingStars.index = value;
		}
		
		public function set count(value:int):void
		{
			if (!_label) return;
			
			if (value > 0)
				_label.text = value.toString();
			else
				_label.text = "No Rating";
		}
		
		public function set labelColor(value:uint):void
		{
			if (!_label) return;
			_label.color = value;
		}
		
		public function set labelSize(value:int):void
		{
			if (!_label) return;
			_label.size = value;
		}
		
		override protected function addChildren():void 
		{
			super.addChildren();
			
			_label = new LightLabel(50, 0x000000, false, 15);
			this.addChild(_label);
			
			_ratingStars = new RatingStars();
			this.addChild(_ratingStars);
			
			count = 0;
		}
	}
}