/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 1/3/12
 * Time: 1:14 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.ui.palettes {
    import com.alleni.author.controller.ui.ZappingMediator;
    import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.application.DockIcons;
    import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Dock;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichLabel;
	import com.alleni.author.view.ui.components.HBox;
    import com.alleni.author.view.ui.components.PushButton;
    import com.alleni.author.view.ui.components.Style;
	import com.alleni.author.view.ui.components.VBox;
    import com.alleni.author.view.ui.components.ZappingPanelComboBox;
    import com.alleni.author.view.ui.components.ZappingPanelSearchComboBox;

	import flash.display.BlendMode;
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;

    import mx.events.PropertyChangeEvent;

    public class ZappingDialogFormView extends DockDetailsFormView{
        private static const separatorLineWidth:Number = 410;

        private var _shareMessage:Boolean;
        private var _shareLocation:String;

        public var buttonContainer:HBox;

        public function ZappingDialogFormView(targetProperty:String, container:DisplayObjectContainer, darkDialog:Boolean=true, existingButton:InteractiveObject=null, shareTo:String = NotificationNamesApplication.SHARE_TO_LINK, shareMessage:Boolean=false) {
            super(targetProperty, container, darkDialog, existingButton);

            _shareMessage = shareMessage;
            _mediator = new ZappingMediator(targetProperty, container, shareTo);

            switch(shareTo){
                case NotificationNamesApplication.SHARE_TO_EMAIL_LIST:
                    _shareLocation = "to a Guest List";
                    break;
                case NotificationNamesApplication.SHARE_TO_EMBED:
                    _shareLocation = "with an Embed Tag";
                    break;
                case NotificationNamesApplication.SHARE_TO_FACEBOOK:
                    _shareLocation = "on Facebook";
                    break;
                case NotificationNamesApplication.SHARE_TO_LINK:
                    _shareLocation = "with a Direct Link";
                    break;
                case NotificationNamesApplication.SHARE_TO_LINKED_IN:
                    _shareLocation = "on LinkedIn";
                    break;
                case NotificationNamesApplication.SHARE_TO_LMS:
                    _shareLocation = "package for an LMS";
                    break;
                case NotificationNamesApplication.SHARE_TO_SHOPP:
                    _shareLocation = "in the Shopp";
                    break;
                case NotificationNamesApplication.SHARE_TO_TWITTER:
                    _shareLocation = "on Twitter";
                    break;
                case NotificationNamesApplication.SHARE_TO_GUEST_LIST:
                    _shareLocation = "with a Guest List";
                    break;
            }
        }

        override protected function initialize(event:Event=null):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, initialize);

			Style.setStyle(Style.LIGHT);

            var formContainer:VBox = new VBox(this, PADDING + 10, PADDING, PADDING/3);

            // Title
			var singleFieldContainer:HBox = new HBox(formContainer, 0, 0, PADDING/2);
			singleFieldContainer.alignment = HBox.MIDDLE;

            var headerText:RichLabel = new RichLabel(150);
            headerText.color = 0;
			headerText.size = FORM_TITLE_SIZE;
			headerText.bold = true;
			headerText.height = 27;
			singleFieldContainer.addChild(headerText);

            // Line
            var firstSeparatorline:Sprite = new Sprite();
            firstSeparatorline.graphics.lineStyle(1);
            firstSeparatorline.graphics.moveTo(0,0);
            firstSeparatorline.graphics.lineTo(separatorLineWidth,0);
            formContainer.addChild(firstSeparatorline);

            var shareMessageText:LightLabel = new LightLabel(separatorLineWidth, 0, true, 20);
            if(_shareLocation == null){
                shareMessageText.text = "This app will be added to your App Collection"
            } else {
                shareMessageText.text = "Message: Before you can share "+_shareLocation+" you must zap the project."
            }
            formContainer.addChild(shareMessageText);

            var shareSeparatorline:Sprite = new Sprite();
            shareSeparatorline.graphics.lineStyle(1);
            shareSeparatorline.graphics.moveTo(0,0);
            shareSeparatorline.graphics.lineTo(separatorLineWidth,0);
            formContainer.addChild(shareSeparatorline);

            formContainer.addChild(createContainer(1,5));

            // ICON/Meta Data Container
			var iconAndMetaDataContainer:HBox = new HBox(formContainer, 0, 0, PADDING*1.2);
			iconAndMetaDataContainer.alignment = HBox.TOP;

            //Icon
			var iconContainer:Sprite = new Sprite();
			iconContainer.addChild(DockIcons.getUnknown(DockIcons.SIZE_MEDIUM));
			iconContainer.visible = false;
			iconContainer.alpha = 0;
            iconContainer.y = 35;
			iconAndMetaDataContainer.addChild(iconContainer);

			var fieldContainer:VBox = new VBox(iconAndMetaDataContainer, 0, 0, FIELD_SPACING);

            //Zapp Title
            singleFieldContainer = new HBox(fieldContainer);
			singleFieldContainer.alignment = HBox.MIDDLE;
			label = new LightLabel(69, _textColor, false, 15);
			label.text = "App Title:";
			label.size = FORM_TEXT_SIZE;
			label.bold = true;
			singleFieldContainer.addChild(label);

            var nameBox:ZappingPanelSearchComboBox = new ZappingPanelSearchComboBox(singleFieldContainer,0,0,"",[]);
            nameBox.y = 1;
			nameBox.width = 230;
			nameBox.height = 12;
			nameBox.listItemHeight = 15;
			nameBox.listWidth = 230;
			nameBox.defaultLabelColor = _textColor;
            nameBox.labelSize = FORM_TEXT_SIZE;
            singleFieldContainer.addChild(nameBox);

            // Category
			singleFieldContainer = new HBox(fieldContainer);
			singleFieldContainer.alignment = HBox.MIDDLE;
			var label:LightLabel = new LightLabel(69, _textColor, false, 15);
			label.text = "Category:";
			label.size = FORM_TEXT_SIZE;
			label.bold = true;
			singleFieldContainer.addChild(label);

			_categoryMenu = new ZappingPanelComboBox(singleFieldContainer, 0, 0, "", []);
			_categoryMenu.width = 235;
			_categoryMenu.defaultLabelColor = _textColor;
			_categoryMenu.labelSize = FORM_TEXT_SIZE;
			_categoryMenu.defaultStyle = Style.LIGHT;
			refreshCategoryMenu();

            // Author
			singleFieldContainer = new HBox(fieldContainer);
			singleFieldContainer.alignment = HBox.MIDDLE;
			label = new LightLabel(69, _textColor, false, 15);
			label.text = "Author";
			label.size = FORM_TEXT_SIZE;
			label.bold = true;
			singleFieldContainer.addChild(label);

			var authorLabel:LightLabel = new LightLabel(280, _textColor, true, 15);
			authorLabel.size = FORM_TEXT_SIZE;
			singleFieldContainer.addChild(authorLabel);

            // Company
            singleFieldContainer = new HBox(fieldContainer);
			singleFieldContainer.alignment = HBox.MIDDLE;
			label = new LightLabel(69, _textColor, false, 15);
			label.text = "Company:";
			label.size = FORM_TEXT_SIZE;
			label.bold = true;
			singleFieldContainer.addChild(label);

			var companyLabel:LightLabel = new LightLabel(280, _textColor, true, 15);
			companyLabel.size = FORM_TEXT_SIZE;
			singleFieldContainer.addChild(companyLabel);

            // Spacer
			var container:Sprite = this.createContainer(280, 30);
            formContainer.addChild(createContainer(1,5));

            // Line
            var secondSeparatorline:Sprite = new Sprite();
            secondSeparatorline.graphics.lineStyle(1);
            secondSeparatorline.graphics.moveTo(0,0);
            secondSeparatorline.graphics.lineTo(separatorLineWidth,0);
            formContainer.addChild(secondSeparatorline);

            // Spacer
			var container:Sprite = this.createContainer(280, 30);
            formContainer.addChild(createContainer(1,5));

            label = new LightLabel(125, _textColor, false, 15);
			label.text = "App Description:";
			label.size = FORM_TEXT_SIZE;
			label.bold = true;
            formContainer.addChild(label);

            // Zapp Description
			var descriptionField:LightEditableLabel = new LightEditableLabel(separatorLineWidth, 150, true);
			descriptionField.explicitLineBreak = false;
			descriptionField.editable = true;
			descriptionField.embedFonts = true;
			descriptionField.color = _textColor;
			descriptionField.size = FORM_TEXT_SIZE;
			descriptionField.bold = false;
			descriptionField.propertyType = Modifiers.FORM_TEXT_BOX;
			descriptionField.expandable = false;
			descriptionField.setPaddingParams({left:0, top:6, right:0, bottom:0});
			descriptionField.leading = 1.0;
			descriptionField.scrollRect = new Rectangle(0, 0, separatorLineWidth, 150);

            var textArea:Sprite = new Sprite();
            textArea.graphics.beginFill(0xFFFFFF);
            textArea.graphics.lineStyle(1, 0xCCCCCC);
            textArea.graphics.drawRect(-2, -2, separatorLineWidth, 150)
            textArea.addChild(descriptionField);

			container = this.createContainer(280, 150);
			container.addChild(textArea);

			formContainer.addChild(container);

            formContainer.addChild(createContainer(1,5));

            //Snapshots Title
			singleFieldContainer = new HBox(formContainer, 0, 0, 5);
			singleFieldContainer.alignment = HBox.MIDDLE;
			label = new LightLabel(69, _textColor, false, 15);
			label.text = "Snapshots:";
			label.size = FORM_TEXT_SIZE;
			label.bold = true;
			singleFieldContainer.addChild(label);

			// SCREENSHOTS
			var screenshotContainer:Sprite = new Sprite();
            container = this.createContainer(140, 26);
            container.addChild(screenshotContainer);
			singleFieldContainer.addChild(container);

			formContainer.addChild(createContainer(1, PADDING/2));
            container = this.createContainer(PADDING/2, 1);
            formContainer.addChild(container);

            // Line
            var thirdSeparatorline:Sprite = new Sprite();
            thirdSeparatorline.graphics.lineStyle(1);
            thirdSeparatorline.graphics.moveTo(0,-5);
            thirdSeparatorline.graphics.lineTo(separatorLineWidth,-5);
            formContainer.addChild(thirdSeparatorline);

            buttonContainer = new HBox(formContainer);

            container = this.createContainer(230, 1);
            buttonContainer.addChild(container);

            var cancelButton:PushButton = new PushButton(buttonContainer, 0, 0, "Cancel");
            cancelButton.defaultStyle = Style.LIGHT;
            cancelButton.labelColor = _textColor;
            cancelButton.labelBold = true;

            var button:PushButton = new PushButton(buttonContainer);
            button.defaultStyle = Style.LIGHT;
            button.labelColor = _textColor;
            button.labelBold = true;
            button.width = 100;

            buttonContainer.addChild(button);
            buttonContainer.blendMode = BlendMode.MULTIPLY;

			(_mediator as ZappingMediator).handleDialogEvents(headerText, nameBox, descriptionField, authorLabel,
				iconContainer, screenshotContainer, button, _categoryMenu, cancelButton, companyLabel, this);

			Dock.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);
		}
    }
}
