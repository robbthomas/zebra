package com.alleni.author.view.ui
{
	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;

	public class Poof extends Sprite
	{
		[Embed(source="/assets/images/poof.png",compression="true",quality="90")]
		private static var PoofImage:Class;

		private var _poof:Bitmap;
		private var _frac:Number = 1.0;
		private var _w:Number;
		private var _h:Number;

		public function Poof()
		{
			trace("Poof");
			_poof = Bitmap(new PoofImage());
			_w = _poof.bitmapData.width;
			_h = _poof.bitmapData.height;
			this.addChild(_poof);
		}
		
		public function set frac(value:Number):void
		{
			_frac = value;
			_poof.width = value*_w;
			_poof.height = value*_h;
			_poof.x = - value*_w/2;
			_poof.y = - value*_h/2;
		}
		
		public function get frac():Number
		{
			return _frac;
		}
	}
}