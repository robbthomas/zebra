package com.alleni.author.view.ui
{
import com.alleni.author.model.ui.NumberValueControlConstraint;

import flash.events.MouseEvent;
	import flash.geom.Point;
	

	public class RibbonColorSlider extends RibbonSlider
	{
		
		public static const COLORS:Vector.<uint> = Vector.<uint>([0xFFFFFF, 0xE8E8E8, 0xD1D1D1, 0xB9B9B9, 0xA2A2A2, 0x8B8B8B, 0x747474, 0x5D5D5D, 0x464646, 0x2E2E2E,
											0x212121, 0x000000, 0x282D4F, 0x515780, 0x777DA3, 0x999DBD, 0xB2BAD1, 0xB6C8D9, 0xB5C7B1, 0xC7C1A3,
											0xCFAC87, 0xBF8D6B, 0xAB7450, 0x8A5532, 0x803F13, 0x662A00, 0x571A00, 0x3B0000, 0x4A0000, 0x730E0E, 
											0xAB2529, 0xFF0000, 0xF83C12, 0xF15024, 0xF46A1A, 0xF98911, 0xFFAB1A, 0xFFC508, 0xFED90A, 0xFCED14,
											0xFEFA7F, 0xFCFEAF, 0xD5F08B, 0xA7DD39, 0x75CC41, 0x45B841, 0x14A340, 0x1D9141, 0x107A30, 0x066321,
											0x004537, 0x003B2B, 0x002B1F, 0x003329, 0x004237, 0x006063, 0x00777A, 0x009599, 0x00BABF, 0x00DBBF,
											0x70FCFF, 0xB3F3FF, 0x99E4FF, 0x80D5FF, 0x4DC4FF, 0x25ACFA, 0x0D89F5, 0x0072E8, 0x005EEC, 0x004DF5,
											0x0028FF, 0x0511FD, 0x0C05CF, 0x07037D, 0x04024F, 0x1E096B, 0x330EB5, 0x4917D4, 0x551FC4, 0x6026B3,
											0x673BA8, 0x8358C7, 0xAA7BF2, 0xBE94FF, 0xC8A3FF, 0xC683FF, 0xC363FF, 0x9C3ED4, 0x8421BA, 0x590978,
											0x3C024D, 0x4E144F, 0x820471, 0xB50079, 0xD40F67, 0xFC0043, 0xFC3569, 0xFC5682, 0xFC92AE, 0xFCB5C8]);
		
		private var _colorPicker:RibbonColorPicker;
		private var _colorWidth:Number;
		
		public static function getIndexOfColor(color:uint):int
		{
			var n:uint;
			var len:uint = COLORS.length;
			for (n=0; n<len; n++) {
				if (new uint(COLORS[n])==color)
					return n;
			}
			
			return -1;
		}
		
		public function RibbonColorSlider(setValueCallback:Function, valueMin:Number, valueMax:Number, colorPicker:RibbonColorPicker)
		{
			super(setValueCallback, new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, valueMin, valueMax));
			
			_colorPicker = colorPicker;
			_colorWidth = WIDTH/COLORS.length;
			this.addEventListener(MouseEvent.CLICK,handleClick);

			addColors();
		}
		
		private function addColors():void 
		{
			var colorHeight:Number = RibbonView.HEIGHT-3;
			
			var color:uint;
			var n:int;
			var k:int = COLORS.length;
			for (n=0; n<k; n++) {
				graphics.lineStyle(_colorWidth, COLORS[n]);
				graphics.moveTo(n*_colorWidth, 0);
				graphics.lineTo(n*_colorWidth, colorHeight);
			}
		}

		override protected function convertValueToPixels(value:Number) : Number {
			
			var pixels:Number;
			
			var factor:Number = COLORS.indexOf(value);
			pixels = factor*(WIDTH/COLORS.length);
			
		//	trace("Value to Pixels="+pixels);
			
			return pixels;
		}
				
		override protected function convertPixelsToValue(pixels:Number) : Number 
		{
			var value:Number;
			
			var colorIndex:int = Math.round(pixels/_colorWidth);
			if (colorIndex >= COLORS.length)
				value = COLORS[COLORS.length-1];
			else if (colorIndex < 0)
				value = COLORS[0];
			else
				value = COLORS[colorIndex];
		//	trace("colorIndex="+colorIndex, "value="+value);
			
			return value;
		}
		
		private function handleClick(e:MouseEvent):void
		{
		//	trace("Clicked color picker");
            if(_sliderUpdateHappened){
                _sliderUpdateHappened = false;
//              if the slider was used the pick the color ignore click, causes problems with undo if you don't return here.
                return;
            }
			var pixels:Number = this.globalToLocal(new Point(e.stageX,e.stageY)).x;
			this.setRibbonValue(numToString(convertPixelsToValue(pixels)));
		}
		
		override public function setValue(value:Object, valueType:String) : void 
		{
		//	trace("RibbonColorSlider, setting value="+value);
			var valueNum:Number = anythingToNum(value);
			selectedKnob.x = convertValueToPixels(valueNum);
			startingKnob.x = selectedKnob.x;			
		}
	}
}