package com.alleni.author.view.ui.palettes
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.view.ObjectView;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	public class CompositeDetailsView extends DockDetailsView
	{
		public function CompositeDetailsView(targetView:DisplayObject, targetProperty:String, allowedDirections:int=-1, minimumTargetWidth:Number=0)
		{
			super(targetView, targetProperty, allowedDirections, minimumTargetWidth);
		}
		
		override protected function getTargetBounds(targetCoordinateSpace:DisplayObject=null):Rectangle
		{
			var bounds:Rectangle = super.getTargetBounds(targetCoordinateSpace);
			
			var view:ObjectView = target as ObjectView;
			if (view && view.model.value as AbstractObject) {
				var ao:AbstractObject = view.model.value as AbstractObject;
				bounds.width = ao.width;
				bounds.height = ao.height;
			}
			return bounds;
		}
	}
}