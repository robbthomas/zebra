package com.alleni.author.view.ui.palettes
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.definition.application.AssetIcons;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.ScrollUpdateEvent;
import com.alleni.author.model.ScrollModel;
	import com.alleni.author.model.objects.IListable;
    import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.Library;
import com.alleni.author.util.cache.LRUCache;
	import com.alleni.author.view.PaletteVerticalScrollView;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichLabel;
	import com.alleni.author.view.ui.IDropHost;
	import com.alleni.author.view.ui.MenuButton;
import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
import com.alleni.author.view.ui.controls.Button;
	import com.alleni.author.view.ui.controls.PopOutMenu;
	import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.service.LogService;
	
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
import flash.events.MouseEvent;
import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextFieldAutoSize;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.events.PropertyChangeEvent;
	
	public class LibraryView extends Sprite implements IDropHost
	{
		private static const BACKGROUND_COLOR:uint = 0x000000;
		private static const BACKGROUND_ALPHA:Number = 1;
		private static const BORDER_COLOR:uint = 0x7D7D7D;
		private static const BORDER_THICKNESS:Number = 1;
		
		private static const RIBBON_HEIGHT:uint = 25;
		private static const TOP_MENU_HEIGHT:uint = 25;
		private static const BOTTOM_PADDNG_HEIGHT:uint = 5;
		private static const BOTTOM_MENU_AREA:uint = 20;
		
		private static const NUMBER_OF_TOP_MENUS:uint = 2;	
		private static const SORT_TYPE_DESCENDING:Boolean = true;
		private static const SORT_TYPE_ASCENDING:Boolean = false;
		private var _sortType:Boolean = SORT_TYPE_ASCENDING;  //for when ui allows changing sort type
		
		private var _model:Library;
		
		private var _filteredItems:ArrayCollection;
		private var _projectNameLabel:LightLabel;
		private var _resultText:RichLabel;
		private var _typeFilterMenu:PopOutMenu;
		private var _addButton:Button;
		private var _deleteButton:Button;
		private var _deletePanel:Sprite;
		private var _spacesNeeded:uint = 0;
		private var _itemArea:Sprite;
		private var _itemBorder:Shape;
		private var _scrollNeeded:Boolean = false;
		private var _scrollbar:PaletteVerticalScrollView;
		private var _scrollModel:ScrollModel;
		private var _scrollPosition:Number = 0;
		private var _itemCache:LRUCache;  // key=IListable, value=LibraryItemView

		
		public function LibraryView()
		{
			super();
			
			_filteredItems = new ArrayCollection();
			_itemCache = new LRUCache(300, 500);
			
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(event:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			var shadow:DropShadowFilter = new DropShadowFilter(1,80,0,0.8,2,2,2,1);
			
			_itemBorder = new Shape();
			_itemBorder.visible = false;
			_itemBorder.graphics.clear();
			_itemBorder.graphics.lineStyle(BORDER_THICKNESS, BORDER_COLOR);
			_itemBorder.graphics.moveTo(0, 0);
			_itemBorder.graphics.lineTo(175, 0);
			addChild(_itemBorder);
			
			_resultText = new RichLabel();
			_resultText.size = 14;
			_resultText.color = 0xBEBEBE;
			_resultText.x = 10;
			_resultText.mouseEnabled = false;
			_resultText.autoSize = TextFieldAutoSize.LEFT;
			addChild(_resultText);
			
			_addButton = new Button();
			_addButton.skin_unsel_down = AssetIcons.addAssetButtonDown;
			_addButton.skin_unsel_roll = AssetIcons.addAssetButtonOver;
			_addButton.skin_unsel_up = AssetIcons.addAssetButtonUp;
			_addButton.buildButton(false);
			_addButton.buttonMode = true;
            _addButton.toolTip = "Import resource";
			_addButton.addEventListener(Button.CLICK, handleAdd);
            _addButton.addEventListener(MouseEvent.ROLL_OVER, showTip);
            _addButton.addEventListener(MouseEvent.ROLL_OUT, hideTip);
			_addButton.x = 142;
			addChild(_addButton);
			
			_deleteButton = new Button();
			_deleteButton.skin_unsel_down = AssetIcons.deleteAssetButtonDown;
			_deleteButton.skin_unsel_roll = AssetIcons.deleteAssetButtonOver;
			_deleteButton.skin_unsel_up = AssetIcons.deleteAssetButtonUp;
			_deleteButton.buildButton(false);
			_deleteButton.buttonMode = true
			_deleteButton.x = 162;
            _deleteButton.toolTip = "Delete resource";
			_deleteButton.addEventListener(Button.CLICK, handleDelete);
            _deleteButton.addEventListener(MouseEvent.ROLL_OVER, showTip);
            _deleteButton.addEventListener(MouseEvent.ROLL_OUT, hideTip);
			_deleteButton.visible = model.selectedItem != null;
			addChild(_deleteButton);
			
			_itemArea = new Sprite();
			_itemArea.y = (TOP_MENU_HEIGHT * NUMBER_OF_TOP_MENUS + 1);
			addChild(_itemArea);
		
			_projectNameLabel = new LightLabel();
			_projectNameLabel.color = 0xFFFFFF;
			_projectNameLabel.y = 5;
			_projectNameLabel.x = 15;
			_projectNameLabel.width = 150;
			_projectNameLabel.text = Application.instance.document.name;
			_projectNameLabel.filters = [shadow];
			addChild(_projectNameLabel);
		
			_typeFilterMenu = new PopOutMenu(EditorUI.instance);
			_typeFilterMenu.closedHeight = 23;
			_typeFilterMenu.closedWidth = 165;
			_typeFilterMenu.popWidth = 170;
			_typeFilterMenu.baseType = PopOutMenu.BASE_TYPE_BOTH;
			_typeFilterMenu.itemsType = PopOutMenu.ITEMS_TYPE_BOTH;
			_typeFilterMenu.dataProvider = AssetDescription.fullSelectionData;
			_typeFilterMenu.direction = PopOutMenu.DIRECTION_DOWN;
			_typeFilterMenu.baseLabelOffsetX = 18;
			_typeFilterMenu.baseLabelOffsetY = 1;
			_typeFilterMenu.checkMarkScale = .5;
            _typeFilterMenu.baseAlpha = 0;
			_typeFilterMenu.defaultIndex = 0;
			_typeFilterMenu.drawComponent();
			_typeFilterMenu.x = 3;
			_typeFilterMenu.y = 26;
			_typeFilterMenu.addEventListener(PopOutMenu.ITEM_CHANGED, handleFilterChange);
			addChild(_typeFilterMenu);
			
			render();
			
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_NAME_CHANGED, handleProjectNameChanged);
			ApplicationController.addEventListener(NotificationNamesApplication.STAGE_RESIZED, render);
			model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);
		}
		
		private function handlePropertyChange(event:PropertyChangeEvent):void
		{
			switch(event.property) {
				case "selectedItem":
					if (model.selectedItem != null)
						_deleteButton.visible = true;
					else
						_deleteButton.visible = false;
					break;
				case "pageList":
					filter();
					break;
                case "usageCounts":
                    updateUsageCounts();
                    break;
				default:
					break;
			}
		}

        private function showTip(event:MouseEvent):void
        {
            var button:Button = event.currentTarget as Button;
            var point:Point = new Point(25, -8);
            ApplicationController.instance.displayToolTip(button.localToGlobal(point), ControlAreaToolTip.DISPLAY_RIGHT, button.toolTip, "Arrow Show");
        }

        private function hideTip(event:Event):void
        {
            ApplicationController.instance.hideToolTip();
        }

		private function handleFilterChange(event:Event):void
		{
			filter();
			_scrollPosition = 0;
		}
		
		private function get model():Library
		{
			if (!_model)
				_model = Library.instance;
			return _model;
		}
		
		private function handleProjectNameChanged(event:ApplicationEvent = null):void
		{
			_projectNameLabel.text = Application.instance.document.name;
		}
		
		private function get paletteHeight():uint
		{
			return (RIBBON_HEIGHT * _spacesNeeded) + (TOP_MENU_HEIGHT * NUMBER_OF_TOP_MENUS) + BOTTOM_MENU_AREA + BOTTOM_PADDNG_HEIGHT;
		}
		
		private function updateList():void
		{
			while (_itemArea.numChildren > 0)
				_itemArea.removeChildAt(0);

			var i:int = 0;
			var length:int = _filteredItems.length;	
			for (i = 0; i < length; i++) {
				var item:IListable = _filteredItems[i].item as IListable;
				var itemView:LibraryItemView = _itemCache.getValue(item) as LibraryItemView;
				if (itemView) {
					itemView.item = item;
				} else {
					itemView = new LibraryItemView(item, true);  //always display with _scrollNeeded=true
					_itemCache.setValue(item, itemView);
				}
				itemView.y = (25 * i);
				_itemArea.addChild(itemView);
			}
			
			if (i > 0) {
				_itemBorder.y = (TOP_MENU_HEIGHT * NUMBER_OF_TOP_MENUS) + (RIBBON_HEIGHT * i) + 1;
				_itemBorder.visible = true;
			} else
				_itemBorder.visible = false;
			
			render();
		}
		
		private function render(event:Event=null):void
		{
			var gradientShadow:DropShadowFilter = new DropShadowFilter(1, 90, 0, 0.8, 3, 3, 1, 3, true);
			var availableSpace:Number = EditorUI.instance.paletteHeight - ((TOP_MENU_HEIGHT * NUMBER_OF_TOP_MENUS) + BOTTOM_MENU_AREA + BOTTOM_PADDNG_HEIGHT);
			var possibleAssetSpaces:uint = availableSpace>0?Math.floor(availableSpace/RIBBON_HEIGHT):0;
			
			if (_filteredItems.length < 5)
				_spacesNeeded = 5;
			else
				_spacesNeeded = _filteredItems.length;
			
			if (_filteredItems.length > possibleAssetSpaces) {
				_spacesNeeded = possibleAssetSpaces;
				_scrollNeeded = true;
			} else
				_scrollNeeded = false;

			graphics.clear();
			graphics.beginFill(BACKGROUND_COLOR, BACKGROUND_ALPHA);
			graphics.lineStyle(BORDER_THICKNESS, BORDER_COLOR,1, true);
            makeGradient(this.graphics,175,TOP_MENU_HEIGHT)
			graphics.drawRoundRectComplex(0, 0, 175, paletteHeight, 0, 10, 0, 0);
            graphics.endFill();
            makeGradient(this.graphics,175,TOP_MENU_HEIGHT,TOP_MENU_HEIGHT);
            graphics.drawRect(0,TOP_MENU_HEIGHT,175,TOP_MENU_HEIGHT);
            graphics.endFill();

			this.filters = [gradientShadow];
			
			var i:int;
			var length:int = NUMBER_OF_TOP_MENUS + 1;
			for (i = 1; i< length; i++) {
				graphics.moveTo(0,i*TOP_MENU_HEIGHT);
				graphics.lineTo(175,i*TOP_MENU_HEIGHT);
			}
			
			graphics.moveTo(0, paletteHeight - (BOTTOM_PADDNG_HEIGHT + BOTTOM_MENU_AREA));
			graphics.lineTo(175, paletteHeight - (BOTTOM_PADDNG_HEIGHT + BOTTOM_MENU_AREA));
			graphics.moveTo(0, paletteHeight - BOTTOM_PADDNG_HEIGHT);
			graphics.lineTo(175, paletteHeight - BOTTOM_PADDNG_HEIGHT);
			graphics.endFill();
			
			_resultText.y = paletteHeight - BOTTOM_PADDNG_HEIGHT - BOTTOM_MENU_AREA;
			_addButton.y = paletteHeight - BOTTOM_PADDNG_HEIGHT - BOTTOM_MENU_AREA + 11;
			_deleteButton.y = paletteHeight - BOTTOM_PADDNG_HEIGHT - BOTTOM_MENU_AREA + 11;
			_resultText.text = _filteredItems.length + " found";
			
			refreshScrollbar(RIBBON_HEIGHT * possibleAssetSpaces);
            updateUsageCounts();
		}

        private function updateUsageCounts():void
        {
            var count:int = _itemArea.numChildren;
            for (var n:int = 0; n < count; n++) {
                var itemView:LibraryItemView = _itemArea.getChildAt(n) as LibraryItemView;
                itemView.updateUsageCount();
            }
        }
		
		private function refreshScrollbar(rectHeight:Number):void
		{
			if (_scrollNeeded) {
				var scrollRect:Rectangle = new Rectangle(0, 0, 160, rectHeight);
				_itemArea.scrollRect = scrollRect;
				if (!_scrollbar) {
					_scrollModel = new ScrollModel(scrollRect.height, 0, _itemArea.numChildren * RIBBON_HEIGHT);
					_scrollbar = new PaletteVerticalScrollView(_scrollModel, 13, RIBBON_HEIGHT * _spacesNeeded);
					_scrollbar.addEventListener(ScrollUpdateEvent.SCROLL, handleScroll);
					_scrollbar.x = 162 //_itemArea.width + 1;
					_scrollbar.y = _itemArea.y;
					addChild(_scrollbar);
				}
				_scrollModel.viewPortLength = scrollRect.height;
				_scrollModel.maxScrollPosition = _itemArea.numChildren * RIBBON_HEIGHT+2;
				_scrollbar.scrollControllerHeight = RIBBON_HEIGHT * _spacesNeeded;
				_scrollbar.scrollPosition = _scrollPosition;
			} else {
				if (_scrollbar) {
					if (this.contains(_scrollbar)) removeChild(_scrollbar);
					_scrollbar.removeEventListener(ScrollUpdateEvent.SCROLL, handleScroll);
					_scrollbar = null;
					_scrollModel = null;
					_itemArea.scrollRect = null;
				}
			}
		}
		
		private function handleScroll(event:ScrollUpdateEvent):void
		{
			if (!_scrollbar) return;
			var rect:Rectangle = _itemArea.scrollRect;
			
			if (event && event.params && (event.params.hasOwnProperty("scrollPosition"))) {
				rect.y = event.params.scrollPosition;
				_itemArea.scrollRect = rect;
			}
		}
		
		private function handleAdd(event:Event):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.IMPORT_ASSET));
		}
		
		private function handleDelete(event:Event):void
		{
			const deletePanelWidth:Number = 174;
			const deletePanelHeight:Number = paletteHeight - 2;
			const iconConstraint:Number = 100;
			
			var itemToDelete:IListable = model.selectedItem;
            var item:IReplaceable = itemToDelete as IReplaceable;
            if (!item) {
                return;
            }

            var models:Vector.<TaconiteModel> = AuthorController.instance.getObjectModelsForReplaceable(item);
            var modelsIncludingObjsInClosedComposites:Vector.<TaconiteModel> = AuthorController.instance.getObjectModelsForReplaceable(item, true);
            var modelsCannotDelete:int = modelsIncludingObjsInClosedComposites.length - models.length;

			// if we're currently editing an object which contains this item, delete is not allowed.
			if (ApplicationController.instance.authorController.isCurrentlyEditingObject(models)) {
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:53, inserts:[GadgetDescription.cleanName(itemToDelete)]}));
				return;
			}

			if (!_deletePanel)
				_deletePanel = new Sprite();
			else {
				_deletePanel.graphics.clear();
				while (_deletePanel.numChildren > 0)
					_deletePanel.removeChildAt(0);
			}
			
			_deletePanel.graphics.beginFill(0x000000, 0.8);
			_deletePanel.graphics.lineStyle(0, 0, 1, true);
			_deletePanel.graphics.drawRoundRectComplex(1, 1, deletePanelWidth, deletePanelHeight, 0, 9.95, 0, 0);
			_deletePanel.graphics.endFill();
			
			_deletePanel.graphics.lineStyle(1,BORDER_COLOR,1,true);
			_deletePanel.graphics.moveTo(10,27);
			_deletePanel.graphics.lineTo(165,27);
			_deletePanel.graphics.lineStyle();
			
			var thumbnail:DisplayObject = new ItemThumbnail(itemToDelete, iconConstraint).render();
			
			if (thumbnail) {
				_deletePanel.addChild(thumbnail);
				thumbnail.x = (deletePanelWidth - iconConstraint)/2;
				thumbnail.y = (deletePanelHeight - iconConstraint)/2;
				thumbnail.alpha = 0;
				Tweener.addTween(thumbnail, {alpha:1, time:0.5, transition:"easeOutCubic"});
			}
			
			var button:MenuButton;
			
			button = new MenuButton(drawButton, hideDeletePanel);
			button.toggle = false;
			button.color = 0x000000;
			button.x = 25;
			button.y = deletePanelHeight - 28;
			button.labelText = "Cancel";
			button.labelFontSize = 10;
			_deletePanel.addChild(button);
			
			button = new MenuButton(drawButton, function():void {
				deleteItem(itemToDelete)
			});
			button.toggle = false;
			button.color = 0x000000;
			button.x = 90;
			button.y = deletePanelHeight - 28;
			button.labelText = "Delete";
			button.labelFontSize = 10;
			_deletePanel.addChild(button);
			
			function drawButton(btn:Sprite):void
			{
				btn.graphics.lineStyle(0, 0, 1, true);
				btn.graphics.drawRoundRect(0,0,60,20,10,10);
			}
			
			var label:RichLabel;
			
			label = new RichLabel();
			label.text = 'Delete "' + itemToDelete.name + '"?';
			label.size = 14;
			label.width = 155;
			label.bold = true;
			label.color = 0xBEBEBE;
			label.x = 10;
			label.y = 7;
			label.mouseEnabled = false;
			label.center = true;
			_deletePanel.addChild(label);
			
            label = new RichLabel();
            label.text = 'Used in this event:';
            label.size = 10;
            label.width = 100;
            label.color = 0xBEBEBE;
            label.x = 11;
            label.y = 28;
            label.mouseEnabled = false;
            label.left = true;
            _deletePanel.addChild(label);

            var count:int = models.length;
            label = new RichLabel();
            label.text = count.toString();
            label.size = 10;
            label.width = 50;
            label.color = 0xBEBEBE;
            label.bold = true;
            label.x = 115;
            label.y = 28;
            label.mouseEnabled = false;
            label.right = true;
            _deletePanel.addChild(label);

            if (modelsCannotDelete > 0) {    // show how many instances are in close gadgets in this event
                label = new RichLabel();
                label.text = 'In closed gadgets:';
                label.size = 10;
                label.width = 100;
                label.color = 0xBEBEBE;
                label.x = 11;
                label.y = 48;
                label.mouseEnabled = false;
                label.left = true;
                _deletePanel.addChild(label);

                label = new RichLabel();
                label.text = modelsCannotDelete.toString();
                label.size = 10;
                label.width = 50;
                label.color = 0xBEBEBE;
                label.bold = true;
                label.x = 115;
                label.y = 48;
                label.mouseEnabled = false;
                label.right = true;
                _deletePanel.addChild(label);
            }

			addChild(_deletePanel);
		}

        private function deleteItem(itemToDelete:IListable):void
        {
            LibraryController.instance.deleteFromProject(itemToDelete as IReplaceable);

            if (this.contains(_deletePanel))
                Tweener.addTween(_deletePanel,{alpha:0, time:0.5, transition:"easeOutCubic", onComplete:hideDeletePanel});
        }

		private function hideDeletePanel():void
		{
			if (this.contains(_deletePanel)) {
				while (_deletePanel.numChildren > 0)
					_deletePanel.removeChildAt(0);
				removeChild(_deletePanel);
				_deletePanel = null;
			}
		}

		private function filter(sortField:SortField = null):void
		{
			if (!sortField){
				sortField = new SortField();
				sortField.name = "name";
				sortField.descending = _sortType;
			}

            var item:IListable;
			var sort:Sort = new Sort();
			sort.fields = [sortField];
			_filteredItems.removeAll();
			if (_typeFilterMenu.selectedItem.data == AssetType.ANY) {
				for each(item in model.pageList) {
                    _filteredItems.addItem({item:item, name:item.name});
                }
			} else {
				var n:int;
				var length:int = model.pageList.length;
				for (n = 0; n < length; n++) {
                    item = model.pageList[n];
					if (_typeFilterMenu.selectedItem.data == item.type) {
						_filteredItems.addItem({item:item, name:item.name});
                    }
                }
			}
			_typeFilterMenu.updateSelectedItemByData(_typeFilterMenu.selectedItem.data);
			
			_filteredItems.sort = sort;
			_filteredItems.refresh();

			updateList();
		}

		public function feedbackGraphicsDrawFor(graphics:Graphics):Function
		{
			return function():void {
				graphics.drawRoundRectComplex(0, 0, 175, paletteHeight, 0, 10, 0, 0);
			}
		}

        private function makeGradient(g:Graphics, w:Number = 400, h:Number = RIBBON_HEIGHT, yOffset:int = 0, xOffset:int = 0):void
		{
            var fillType:String = GradientType.LINEAR;
            var colors:Array = [0x46638A, 0x000000];
            var alphas:Array = [1, 1];
            var ratios:Array = [0x00, 0xFF];
            var matr:Matrix = new Matrix();
            matr.createGradientBox(w, h, Math.PI/2);
            matr.translate(xOffset,yOffset);
            var spreadMethod:String = SpreadMethod.PAD;
            g.beginGradientFill(fillType, colors, alphas, ratios, matr,spreadMethod);
        }

		
		override public function getBounds(targetCoordinateSpace:DisplayObject):Rectangle
		{
			var globalPosition:Point = this.localToGlobal(new Point(0, 0));
			var localPosition:Point = targetCoordinateSpace.globalToLocal(globalPosition);
			
			return new Rectangle(localPosition.x, localPosition.y, this.width, paletteHeight);
		}
	}
}
