package com.alleni.author.view.ui.controlArea
{
import com.alleni.author.view.text.RichLabel;

import flash.display.GradientType;
import flash.display.InterpolationMethod;
import flash.display.Shape;
import flash.display.SpreadMethod;
import flash.display.Sprite;
import flash.filters.DropShadowFilter;
import flash.filters.GlowFilter;
import flash.geom.Matrix;
import flash.text.TextFieldAutoSize;

public class ControlAreaToolTip extends Sprite
	{
		private static const FILL_ALPHA:Number = 0.9;
		private static const LINE_ALPHA:Number = 0;
		private static const LINE_THICKNESS:Number = 1.0;
				
		public static const DISPLAY_BELOW:String 	= "Display Below";
		public static const DISPLAY_ABOVE:String 	= "Display Above";
		public static const DISPLAY_LEFT:String 	= "Display Left";
		public static const DISPLAY_RIGHT:String 	= "Display Right";
		public static const ARROW_SHOW:String 		= "Arrow Show";
		public static const ARROW_HIDDEN:String 	= "Arrow Hidden";
		
		public var arrowDisplay:String = ARROW_SHOW;
		public var containerWidth:Number = 0;  // computed based on text dimensions
		public var containerHeight:Number = 0;

		private var _background:Sprite;
		private var _arrow:Shape;
		private var _displayDirection:String = DISPLAY_BELOW;
		private var _label:RichLabel;
        private var _fillColors:Array = [0xffffff, 0xa5a5a5];

		public function ControlAreaToolTip(fillColors:Array = null)
		{
			super();
            if (fillColors != null)
                _fillColors = fillColors;
			filters = [new GlowFilter(0x000000, 0.5, 2, 2, 3, 3), new DropShadowFilter(4.0, 90, 0x000000, 0.8, 10.0, 10.0, 0.8, 3)];
			mouseEnabled = false;
			_label = new RichLabel();
			this.labelString = "Some Default Text";
		}
		
		private function draw():void
		{
			buildBackground();		
			buildArrow();
			
			this.addChild(_background);
			
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(this.containerWidth, this.containerHeight, 90*Math.PI/180);
			
			this.graphics.beginGradientFill(GradientType.LINEAR, _fillColors, [1,1], [0, 255], matrix, SpreadMethod.PAD, InterpolationMethod.LINEAR_RGB);
			this.graphics.drawRect(-12, -12, this.containerWidth*2, this.containerHeight+12);
			this.graphics.endFill();
			this.mask = _background;
			
			buildLabel();
		}
		
		private function buildBackground():void
		{			
			if (!_background)
				_background = new Sprite();
			_background.graphics.clear();
			_background.graphics.lineStyle(LINE_THICKNESS, 0x000000, LINE_ALPHA, true);
			
			_background.graphics.beginFill(0x00ff00, 1);
			_background.graphics.drawRoundRect(0,0,this.containerWidth, this.containerHeight,16);
			
			/*if (!contains(_background))
				addChild(_background);*/
		}
		
		private function buildLabel():void
		{
			_label.color = 0x000000;
			_label.bold = true;
			_label.width = 90;
			_label.size = 13;
			_label.x = 5;
			_label.y = 2;
			_label.center = true;
			_label.filters = [new DropShadowFilter(1.0, 45, 0xffffff, 0.3, 1.0, 0.8, 0.9, 2)];
			this.addChild(_label);
		}
		
		private function buildArrow():void
		{
			if (!_arrow)
				_arrow = new Shape();
            if (!_background.contains(_arrow))
                _background.addChild(_arrow);

			_arrow.graphics.clear();
			if (this.arrowDisplay == ARROW_SHOW) {
				_arrow.graphics.beginFill(_fillColors[1], FILL_ALPHA);
				switch(_displayDirection) {
					case DISPLAY_ABOVE:
						_arrow.graphics.moveTo(12,0);
						_arrow.graphics.lineTo(6,11);
						_arrow.graphics.lineTo(0,0);
						_arrow.graphics.endFill();
						_arrow.graphics.lineStyle(LINE_THICKNESS, 0x000000, LINE_ALPHA, true);
                        _arrow.graphics.moveTo(12,0);
                        _arrow.graphics.lineTo(6,11);
                        _arrow.graphics.lineTo(0,0);
						_arrow.x = (this.containerWidth/2) - 5;
						_arrow.y = containerHeight - 1;
						break;
					case DISPLAY_LEFT:
						_arrow.graphics.moveTo(0,0);
						_arrow.graphics.lineTo(11,6);
						_arrow.graphics.lineTo(0,12);
						_arrow.graphics.endFill();
						_arrow.graphics.lineStyle(LINE_THICKNESS, 0x000000, LINE_ALPHA, true);
						_arrow.graphics.moveTo(0,0);
						_arrow.graphics.lineTo(11,6);
						_arrow.graphics.lineTo(0,12);
						_arrow.x = this.containerWidth;
						_arrow.y = 7;
						break;
					case DISPLAY_RIGHT:
						_arrow.graphics.moveTo(11,0);
						_arrow.graphics.lineTo(0,6);
						_arrow.graphics.lineTo(11,12);
						_arrow.graphics.endFill();
						_arrow.graphics.lineStyle(LINE_THICKNESS, 0x000000, LINE_ALPHA, true);
						_arrow.graphics.moveTo(11,0);
						_arrow.graphics.lineTo(0,6);
						_arrow.graphics.lineTo(11,12);
						_arrow.x = -11;
						_arrow.y = 7;
						break;
					case DISPLAY_BELOW:
					default:
						_arrow.graphics.moveTo(0,11);
						_arrow.graphics.lineTo(6,0);
						_arrow.graphics.lineTo(12,11);
						_arrow.graphics.endFill();
						_arrow.graphics.lineStyle(LINE_THICKNESS, 0x000000, LINE_ALPHA, true);
						_arrow.graphics.moveTo(0,11);
						_arrow.graphics.lineTo(6,0);
						_arrow.graphics.lineTo(12,11);
						_arrow.x = (this.containerWidth/2) - 5;
						_arrow.y = -11;
						break;
				}
			}
		}
		
		public function get displayDirection():String
		{
			return _displayDirection;
		}
		
		public function set displayDirection(value:String):void
		{
			_displayDirection = value;
			if (_arrow) {
				if (_background.contains(_arrow))
					_background.removeChild(_arrow);
				_arrow = null;
			}
		}
		
		public function get labelString():String
		{
			return _label.text;
		}
		
		public function set labelString(value:String):void
		{
			_label.text = value;
			_label.size = 14;
			this.containerWidth = _label.width + 10;  // the _label.width getter will immediately measure the text
			this.containerHeight = _label.height + 5;
			draw();
			_label.autoSize = TextFieldAutoSize.CENTER;
			_label.x = this.containerWidth/2 - _label.width/2;
		}
	}
}