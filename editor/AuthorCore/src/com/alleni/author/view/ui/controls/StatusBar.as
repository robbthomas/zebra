package com.alleni.author.view.ui.controls
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichLabel;
	import com.alleni.taconite.graphics.GlassBar;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;

	public class StatusBar extends Sprite
	{
		private static const TEXT_MARGIN:Number = 20.0;
		
		private var _bar:GlassBar;
		private var _titleText:RichLabel;
		
		public function StatusBar()
		{
			_bar = new GlassBar();
			_titleText = new RichLabel(500);
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}

		private function initialize(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			var mainShadow:DropShadowFilter = new DropShadowFilter(0.0, 90, 0, 0.7, 10.0, 10.0, 0.9, 3);
			var textShadow:DropShadowFilter = new DropShadowFilter(1,45,0,0.8,2,2,2,1);
			
			_bar.filters = [mainShadow];
			_bar.alpha = 0.8;
			this.addChild(_bar);
			
			/*var labelText:LightLabel = new LightLabel(200);
			labelText.color = 0xffffff;
			labelText.size = 38;
			labelText.bold = true;
			labelText.x = TEXT_MARGIN;
			labelText.filters = [textShadow];
			labelText.text = "ZEBRA";*/
			
			_titleText.y = -2;
			_titleText.alpha = 0.7;
			_titleText.color = 0xffffff;
			_titleText.size = 38;
			_titleText.bold = true;
			_titleText.right = true;
			_titleText.filters = [textShadow];
			_titleText.text = "STADSKANAAL";
			
			//this.addChild(labelText);
			this.addChild(_titleText);
			
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_NAME_CHANGED, function():void {
				_titleText.text = GadgetDescription.cleanName(Application.instance.document.project, true);
				_titleText.x = _bar.width - _titleText.width - TEXT_MARGIN; 
			});
		}
		
		override public function set width(value:Number):void
		{
			_bar.width = value;
			_titleText.x = value - _titleText.width - TEXT_MARGIN; 
		}
	}
}