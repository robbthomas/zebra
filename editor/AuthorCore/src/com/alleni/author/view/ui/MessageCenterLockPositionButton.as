/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.ui
{
	public class MessageCenterLockPositionButton extends MessageCenterButton
	{
		private var _state:Boolean;
		
		override protected function getToolTipText():String
		{
			return "Lock object position & prevent selection \n(1. Locked objects can still be wired, \n  2. can accept clicks during Run, but not during Edit, \n  3. can be double-clicked in Edit to see message center.)";
		}
		
		public function MessageCenterLockPositionButton()
		{
			super();
			_state = false;
			currentIcon = MessageCenterIcons.lockPosition(_state);
		}

		
		public function draw(locked:Boolean):void
		{
			if (_state != locked) {
				_state = locked;
				currentIcon = MessageCenterIcons.lockPosition(_state);
			}
		}
	}
}
