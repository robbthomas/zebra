package com.alleni.author.view.ui.controls
{
	import flash.display.Shape;
	
	import spark.effects.Fade;
	
	public class LoadingSpinnerTick extends Shape
	{
		private var _fade:Fade = new Fade(this);

		public function LoadingSpinnerTick(fromX:Number, fromY:Number, toX:Number, toY:Number, tickWidth:int, tickColor:uint)
		{
			this.graphics.lineStyle(tickWidth, tickColor, 1.0, false, "normal", "rounded");
			this.graphics.moveTo(fromX, fromY);
			this.graphics.lineTo(toX, toY);
		}
		
		public function fade(duration:Number):void
		{
			_fade.alphaFrom = 1.0;
			_fade.alphaTo = 0.1;
			_fade.duration = duration;
			_fade.play();
		}
	}
}