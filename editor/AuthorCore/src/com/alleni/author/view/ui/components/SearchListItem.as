package com.alleni.author.view.ui.components
{
	import flash.display.DisplayObjectContainer;
	
	public class SearchListItem extends ListItem
	{
		public function SearchListItem(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, data:Object=null)
		{
			super(parent, xpos, ypos, data);
		}
	}
}