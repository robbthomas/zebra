package com.alleni.author.view.ui.palettes
{
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.controller.ui.palettes.LibraryItemMediator;
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.model.objects.IListable;
	import com.alleni.author.model.ui.Library;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.RichLabel;
	import com.alleni.taconite.event.ModelUpdateEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.events.PropertyChangeEvent;
	
	
	public class VariableItemView extends Sprite
	{
		private static const BACKGROUND_COLOR:uint 			= 0x000000;
		private static const TEXT_COLOR:uint 				= 0xFFFFFF;
		private static const SELECTED_TEXT_COLOR:uint 		= 0x000000;
		private static const SELECTED_BACKGROUND_COLOR:uint = 0x91C4FF;
		private static const BACKGROUND_ALPHA:Number 		= 0.85;
		private static const SELECTED_BACKGROUND_ALPHA:Number = 1;
		private static const BORDER_COLOR:uint 				= 0x7D7D7D;
		private static const SELECTED_BORDER_COLOR:uint 	= 0xC8C8C8;
		private static const BORDER_THICKNESS:Number 		= 1.0;
		private static const ICON_FRAME_SIZE:Number			= 0;
		private static const ICON_SIZE:Number				= ICON_FRAME_SIZE-1.0;
		private static const ITEM_HEIGHT:Number             = 20;
		
		private static const OVERALL_WIDTH:Number 			= 175;
		private static const VARIABLE_COLUMN_WIDTH:Number      = 135;
		private static const COUNT_COLUMN_WIDTH:Number       = 25;
		private static const SCROLL_COLUMN_WIDTH:Number      = 15;
		
		
		public var isCompressedView:Boolean = false;  //allow for scrollbar
		
		
		private var _totalWidth:Number = OVERALL_WIDTH;
		
		private var _nameLabel:LightEditableLabel;

		private var _item:IListable;
		
		private var _selected:Boolean = false;
		private var _scrollNeeded:Boolean;
		
		private var _thumbnail:ItemThumbnail;
		
		
		public function VariableItemView(item:IListable, scrollNeeded:Boolean)
		{
			super();
			
			_item = item;
			_scrollNeeded = scrollNeeded;
			
			_nameLabel = new LightEditableLabel(100,16);

			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		

		public function set scrollNeeded(value:Boolean):void
		{
			_scrollNeeded = value;
		}
		

		private function initialize(event:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			if (_scrollNeeded) {
				isCompressedView = true;
			} else{
				isCompressedView = false;
			}
			
			_nameLabel.explicitLineBreak = true;
			_nameLabel.editable = true;
			_nameLabel.embedFonts = true;
			_nameLabel.mouseEnabled = true;
			_nameLabel.size = 12;
			_nameLabel.x = 28;
			_nameLabel.y = 3;
			_nameLabel.width = VARIABLE_COLUMN_WIDTH;
			_nameLabel.setPaddingParams({left:0, top:3, right:0, bottom:3});
			_nameLabel.mediator.setCallBacks({
				okEditFunc:
				function(e:MouseEvent):int{
					return TextEditMediator.doubleClickToEdit(e);
				},
				updateFunc:null,
				keyPressFunc:function(charCode:uint):uint{
					return TextEditMediator.closeOnEnter(charCode);
				},
				closeFunc:
				function(val:String):void{
					//_nameLabel.text = FormattedText.convertToString(val as String);
					_nameLabel.edit = false;
					////////if (
				},
				hittestFunc:
				function(x:Number, y:Number):Boolean{
					if(_nameLabel.hitTestPoint(x,y))
						return true;
					else
						return false;
				}
			});
			addChild(_nameLabel);

			this.item = _item;
			render();

		}
		
		private function handlePropertyChange(event:PropertyChangeEvent):void
		{
			
		}
		
		
		public function set item(value:IListable):void
		{
			_item = value;	
			var name:String;
			name = _item.name;
			
			_nameLabel.text = name;
		}
		
		public function get item():IListable{
			return _item;
		}
		
		private function handleItemModelUpdate(e:ModelUpdateEvent):void
		{
			
		}
		
		private function render():void
		{
			
			var textColor:uint;
			var borderColor:uint;
			
			graphics.clear();
			
			if (!_selected) {
				graphics.beginFill(BACKGROUND_COLOR, BACKGROUND_ALPHA);
				borderColor = BORDER_COLOR
				textColor = TEXT_COLOR;
			} else {
				graphics.beginFill(SELECTED_BACKGROUND_COLOR, SELECTED_BACKGROUND_ALPHA);
				borderColor = SELECTED_BORDER_COLOR
				textColor = SELECTED_TEXT_COLOR;
			}
			
			graphics.drawRect(BORDER_THICKNESS, 0, VARIABLE_COLUMN_WIDTH + COUNT_COLUMN_WIDTH,ITEM_HEIGHT);
			
			_nameLabel.color = textColor;

			graphics.endFill();
		}
		
		
		public function set selected(value:Boolean):void
		{
			if (value != _selected) {
				_selected = value;
				render();
			}
		}
	}
}