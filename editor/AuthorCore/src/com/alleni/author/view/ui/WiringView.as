package com.alleni.author.view.ui
{
	import com.alleni.author.model.ui.Wiring;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.TaconiteView;
	import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObjectContainer;
import flash.display.Sprite;

public class WiringView extends TaconiteView
	{
		private var _role:uint;

		public function WiringView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model);
			_role = role;
		}

		private function get wiring():Wiring
        {
        	return model.value as Wiring;
        }

        override public function get childViewContainer():DisplayObjectContainer
        {
            return this;  // enable TaconiteView to automatically add and delete child views
        }

        /**
         * Create a child view, which in our case will always be a view of a Wire.
         * @param child a child TaconiteModel
         * @return the appropriate type of view for the value object belonging to that child.
         */
        override public function createChildView(child:TaconiteModel):ITaconiteView
        {
        	var wireView:WireView = new WireView(context, child, _role);
        	wireView.initialize();
        	return wireView;
        }
		
		public function set wiresDragging(value:Boolean):void
		{
			this.mouseEnabled = !value;
			this.mouseChildren = !value;
		}
	}
}