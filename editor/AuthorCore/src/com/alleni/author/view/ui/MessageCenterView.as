/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.view.ui
{
import caurina.transitions.Tweener;

import com.alleni.author.Navigation.EventImageView;
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.ui.AbstractObjectDragMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AssetDragDescription;
import com.alleni.author.controller.ui.DragDescription;
import com.alleni.author.controller.ui.MessageCenterDiagramMediator;
import com.alleni.author.controller.ui.MessageCenterMediator;
import com.alleni.author.controller.ui.WireDragDescription;
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.ObjectIcons;
import com.alleni.author.definition.OutletDescription;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.Thumbnails;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.definition.action.ModifyObjectProperty;
import com.alleni.author.definition.action.UserAction;
import com.alleni.author.event.AnchorEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.LMS;
import com.alleni.author.model.objects.ProjectObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.InternalSmartWireAnchor;
import com.alleni.author.model.ui.SetterWireAnchor;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.util.Set;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.text.RichTextField;
import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
import com.alleni.author.view.ui.controls.ColorPicker;
import com.alleni.author.view.ui.controls.IColorPickerParent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.SelectableView;
import com.alleni.taconite.view.ViewContext;

import flash.display.BlendMode;
import flash.display.DisplayObject;
import flash.display.GradientType;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.filters.DropShadowFilter;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Timer;

import flashx.textLayout.edit.SelectionFormat;
import flashx.textLayout.elements.Configuration;

import mx.core.UIComponent;
import mx.events.CollectionEventKind;

public class MessageCenterView extends SelectableView implements IColorPickerParent
	{	
		// Constants used for specifying which subcontainer to open
		public static const ALL_CONTAINERS:String = "allRibbons";
		public static const PROPERTIES:String = "properties";
		public static const INLETS:String = "inlets";
		public static const OUTLETS:String = "outlets";
		
		// Public UI constants
		public static const WIDTH:Number = 150;
		public static const HEIGHT:Number = 20; // for positionMC
		public static const OBJECT_PADDING:Number = 10;
		public static const CUSHION:Number = 4;
		public static const BORDER_WIDTH:Number = 4;
		public static const WIDTH_NO_BORDER:Number = WIDTH-BORDER_WIDTH*2;
		public static const SELECTED_BORDER_COLOR:uint = 0xFFFF80; /* safety vest fire truck mustard yellow */
        public static const BORDER_COLOR:uint = 0xBFBFBF;
		public static const BACKGROUND_COLOR:uint = 0x191919;
		public static const BACKGROUND_ALPHA:Number = 0.85;

		// Private UI constants
		private static const TITLE_HEIGHT:Number = 20;
		private static const BOTTOM_HEIGHT:Number = 5;
		private static const MENU_HEIGHT:Number = 25;
		private static const ZERO:Point = new Point(0,0);
//		private static const SELECTED_BORDER_COLOR:uint = 0x53EBBD; /* mint green */

		private static const TOP_GRADIENT:uint = 0xBFBFBF; // Selected header top gradient
		private static const BOTTOM_GRADIENT:uint = 0x4D4D4D; // Selected header bottom gradient

        private var _objectView:ObjectView;
		
		// Graphics
		private var _titleBar:Sprite;
		private var _headerBottomCornerRadius:Number = 6;
				
		// Controls
		private var _titleBox:RichTextField;
		private var _wireLockButton:MessageCenterLockWiresButton;
		private var _positionLockButton:MessageCenterLockPositionButton;

		// Containers
		private var _mainContainer:Sprite;
		private var _menuBar:MessageCenterMenuBar;
		
		private var _inletContainerView:RibbonContainer;
		private var _outletContainerView:RibbonContainer;
		private var _propertyContainerView:RibbonContainer;
		private var _bottomHeight:Number;
		private var _mediator:AbstractObjectDragMediator;
		private var _h:Number = TITLE_HEIGHT;
		private var _shadow:DropShadowFilter;
        private var _titlePulse:Shape;
        private var _objectIcon:DisplayObject;
        private var _draggedSinceOpened:Boolean = false;

        public var nowOpening:Set = new Set(true);
        public var closeDeferred:Boolean = false;

        private var _titlePulseTimer:Timer = new Timer(1000);
        private var _titleHoverTimer:Timer = new Timer(150);

		// Color Picker
		private var _colorPicker:ColorPicker;

		// this property is made available to the DiagramMediator
		public static const yOffset:Number = Thumbnails.THUMB_NOMINAL_SIZE + OBJECT_PADDING/5;  // offset from yDiagram to top of msgCtr
		
		private var _role:uint;
		
		// get rid of this ASAP!
		private var _hasInitialized:Boolean = false;

        private var _titleEditable:Boolean = true;

        private var _proxyRolled:Boolean = false;

        private var _selfRolled:Boolean = false;

        private var _anyRolled:Boolean = false;

		/** 
		 * MessageCenterView manages all UI aspects of a message center. A message center's core is built through RibbonContainers (IN, OUT, PROP),
		 * which in turn contain RibbonSubContainers.
		 * 
		 * **/
		public function MessageCenterView(context:ViewContext, model:TaconiteModel, role:uint, objView:ObjectView=null)
		{	
//			trace("MessageCenterView creating for view="+objView, "obj="+model.value);
			super(context, model);
			_role = role;
            _objectView = objView;
            // setup titlebar now in case it is stolen before setup
            _titleBar = new Sprite();
            _titleBar.name = "MessageCenterTitleBar";
            _objectIcon = ObjectIcons.getIconFor(object);
			
			if (_role == ViewRoles.PRIMARY)
				_mediator = new MessageCenterMediator(context).handleViewEvents(this, _role);
			else if (_role == ViewRoles.DIAGRAM)
				_mediator = new MessageCenterDiagramMediator(context).handleViewEvents(this, _role);
			
			(_mediator as MessageCenterMediator).handleRibbonEvents(this);
			
			_mainContainer = new Sprite();
			drawMainContainer();
			
			addChild(_mainContainer as DisplayObject);
			
			_bottomHeight = 0;
			
			createTitleBox();

            if(_objectIcon) {
                var iconContainer:Sprite =  new Sprite();
                iconContainer.addChild(_objectIcon);
                iconContainer.graphics.beginFill(0,0.01);
                iconContainer.graphics.drawRect(0,0,_objectIcon.width, _objectIcon.height);
                _objectIcon.x = 4;
                _objectIcon.y = 2;
                _titleBar.addChild(iconContainer);
            }

			// Add Children
			addContainers();
//          addTitlePulse();
			addButtons();
			object.addEventListener(AnchorEvent.TYPE, handleAnchorChange);
//			doInitialClose();
			
			this.initialize();

            // EventPage MC needs to have its same ribbons showing after the Event is edited (obj is replaced)
            if (object.ribbonCategoriesToOpen) {
                openCategoriesByArray(object.ribbonCategoriesToOpen);
                object.ribbonCategoriesToOpen = null;
            }

            _mainContainer.visible = false;  // keep hidden until first update, else MC appears momentarily at topLeft
		}
	
		/**
		 * Initialize this view by adding a mediator specific to message center views.
		 */	
		override public function initialize():void 
		{
			super.initialize();
			
			_mediator.handleViewEvents(this, _role);
			this.selfRolled = false;
			addButtonListeners();
            updateContainerPositions();
            
            if (this.stage) {
                stage.addEventListener(MouseEvent.MOUSE_UP, handleDeferredClose);
            }
//	*** Uncomment this to open all containers by default ***
//			_inletContainerView.openAll();
//			_outletContainerView.openAll();
//			_propertyContainerView.openAll();
		}

        private function handleDeferredClose(e:Event):void {
             if(closeDeferred) {
                 closeContainer(ALL_CONTAINERS);
                 closeDeferred = false;
             }
        }
		
		private function createTitleBox():void
		{
			var _config:Configuration = new Configuration();
			var selectionFormat:SelectionFormat = new SelectionFormat(RichTextField.SELECTION_COLOR, 1,BlendMode.INVERT);
			_config.focusedSelectionFormat = selectionFormat;
			_titleBox = new RichTextField(WIDTH-37-(_objectIcon==null?0:_objectIcon.width), TITLE_HEIGHT-5,true,null,_config);
			_titleBox.formattable = false;
			_titleBox.embedFonts = true;
			_titleBox.x = 3+(_objectIcon == null ? 0 : _objectIcon.width);
			_titleBox.y = 4;
            _titleBox.setPadding(0,0,0,0);
			_titleBox.explicitLineBreak = true;
			_titleBox.color = 0xffffff;
			_titleBox.bold = true;
			_titleBox.text = object.title;
            _titleBox.editable = _titleEditable;
			(_mediator as MessageCenterMediator).handleTitleEvents(_titleBox);

			_titleBar.graphics.beginFill(0xBFBFBF,0);
			_titleBar.graphics.drawRoundRectComplex(0,0,WIDTH,TITLE_HEIGHT,8,8,8,8);
		}

        private function addTitlePulse():void
		{
            _titlePulse = new Shape();
            var matrix:Matrix = new Matrix();
            matrix.createGradientBox(TITLE_HEIGHT,WIDTH,Math.PI/2,0,0);
            _titlePulse.graphics.lineStyle(4,0xFFFF80);
            _titlePulse.graphics.lineGradientStyle(GradientType.LINEAR,[0xFFFF80,0xFFFF80],[1,0],[20,35],matrix);
            _titlePulse.graphics.drawRoundRect(1,1,WIDTH-2,TITLE_HEIGHT+4,11,11);
            _titlePulse.alpha = 0;
            _titleBar.addChild(_titlePulse);
        }

        private function pulseTitle(e:TimerEvent=null):void
		{
            if (this.mouseX > 0 && this.mouseX < this.width && this.mouseY > 0 && this.mouseY < this.height) {
                Tweener.addTween(_titlePulse,{alpha:0.8,time:_titlePulseTimer.delay/2000,transition:"easeInOutExpo",
					onComplete:function():void{
						Tweener.addTween(_titlePulse,{alpha:0,time:_titlePulseTimer.delay/2000,transition:"easeOutSine"})
					}});
            } else {
                _titlePulseTimer.removeEventListener(TimerEvent.TIMER, pulseTitle);
                _titlePulseTimer.stop();
            }

        }
		/**
		 * This method is called when the abstract object associated with this message center has a change in anchors. This method
		 * manages addition or removal of ribbon views based on the change in the anchors. 
		 * @param e
		 * 
		 */
        private function handleAnchorChange(e:AnchorEvent):void
        {
			if (e.anchor as InternalSmartWireAnchor)
				return;

	        var description:IModifier;
	        if (e.anchor.host as WireAnchor) {
		        description = WireAnchor(e.anchor.host).modifierDescription;
	        } else {
		        description = e.anchor.modifierDescription;
	        }
	        var container:RibbonContainer;
			if (description as InletDescription) {
				container = _inletContainerView;
//				container.openAll(); // TODO: This is required to show dynamically added setters
			} else if (description as PropertyDescription) {
				container = _propertyContainerView;
			} else if (description as OutletDescription) {
				container = _outletContainerView;
			}
            switch (e.kind) {
 	           	case AnchorEvent.ADD:
				    if(e.anchor.host as WireAnchor) {
					    container.addRibbon(e.anchor.host as WireAnchor, e.display, e.anchor);
				    } else {
				        container.addRibbon(e.anchor, e.display, null);
				    }
	                break;

	            case AnchorEvent.REMOVE:
				    if(e.anchor.host as WireAnchor) {
					    container.removeRibbon(e.anchor.host as WireAnchor, e.anchor);
				    } else {
				        container.removeRibbon(e.anchor, null);
				    }
	                break;
            }
        }
		
		private function drawMainContainer():void
		{
			_mainContainer.graphics.clear();
			_mainContainer.graphics.lineStyle(BORDER_WIDTH, selected ? SELECTED_BORDER_COLOR : BORDER_COLOR, 1, true);
			_mainContainer.graphics.beginFill(BACKGROUND_COLOR, BACKGROUND_ALPHA);
			_mainContainer.graphics.drawRoundRect(0, 0, WIDTH, _h, 10, 10);
			_mainContainer.filters = [containerShadow];
            dispatchEvent(new Event("MessageCenterRedraw"));
		}
		
		private function get containerShadow():DropShadowFilter
		{
			if (!_shadow) _shadow = new DropShadowFilter(1,45,0,0.6,3,3,0.8,3);
			if (selected) {
				_shadow.distance = 2.0;
				_shadow.blurX = _shadow.blurY = 6.0;
				return _shadow;
			} else {
				_shadow.distance = 1.0;
				_shadow.blurX = _shadow.blurY = 3.0;
				return _shadow;
			}
		}
		
		public function get role():uint
		{
			return _role;
		}

		
		private function doInitialClose():void
		{
			_propertyContainerView.closeOnLoad();
			_inletContainerView.closeOnLoad();
			_outletContainerView.closeOnLoad();
		}
		
		public function buildColorPickerIfNeeded():void
		{
			if(!_colorPicker){
				_colorPicker = new ColorPicker(EditorUI.instance);
				_colorPicker.draw();
				_colorPicker.x = -25;
				addChild(_colorPicker);
			}else{
				trace("Color Picker Present");
			}
		}
		
        /**
         * The object associated with this view's TaconiteModel.
         */
        private function get object():AbstractObject
        {
            return model.value as AbstractObject;
        }
		
		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object) : Boolean
		{
			switch (property) {
				case "xDiagram":
				case "yDiagram":
					if (_role==ViewRoles.DIAGRAM)
						_mediator.updateViewPosition();
					return true;  // avoid doing updateView for simple position changes
				//TODO: This isn't quite right, since _propsBtn can be selected in the diagram view, but we may not want to update it unnecessarily in the presentation view.	
				case "x":
				case "y":
					return true;
				case "locationXYR":
					return !menuBar.propsBtn.selected;  // return true (no update needed) if the properties are closed
				case "toDiagram":
					closeContainer(ALL_CONTAINERS);
					break;
				case "title":
					_titleBox.rawText = newValue as String;
					break;
				case "messageCenterVisible":
                    updateVisibility();
                    break;
//					// ensure MC of wired object in a gadget will show properly when gadget is opened
//					if (newValue == true) {
//						if (ApplicationController.instance.wireController.objectHasWires(model.value))
//							menuBar.open();
//					}
//					break;
			}
			return super.updateModelProperty(property, oldValue, newValue);
		}
		
		private function hasSetterRibbon():Boolean
		{
			for each (var anchor:WireAnchor in object.inletRibbons) {
				if (anchor as SetterWireAnchor)
					return true;
			}
			return false;
		}
		
        /**
         * Update this view by drawing the appropriate graphics.
         */
        override protected function updateView():void
        {
			super.updateView();

			// Update lock buttons
			wireLockButton.draw(object.messageCenterLocked);
			if(!(model.value is EventPage || model.value is ProjectObject || model.value is LMS)){
                positionLockButton.draw(object.locked);
            }

			// we'll only update the MC view when we're not running.
			// this is because we hide the whole layer containing MC's when we go to run.
			if (!Application.running || _role == ViewRoles.DIAGRAM) {
				if (!_titleBox.editing)
					_titleBox.text = object.title;
								
				_mediator.updateViewPosition();
                updateVisibility();
            }
        }

        private function updateVisibility():void
        {
            if (_objectView as EventImageView) {
                visible = object.messageCenterVisible; // ignore "active"
            } else {
                visible = object.messageCenterVisible && object.active && !object.childOfClosedComposite; // Hide or show message center
            }
        }

        override public function set visible(value:Boolean):void {
            super.visible = value;
            if (value && !(_objectView as EventImageView)) {
                dispatchEvent(new Event("MessageCenterRedraw"));
            }
        }

        override protected function updateStatus() : void
		{
			_titleBar.graphics.clear();
			drawMainContainer();
			_hasInitialized = true;			
        }

		/**
		 * Adds the Inlet, Outlet, and Properties buttons to the message center 
		 * 
		 */
		private function addButtons():void 
		{
		var wireLockPositionOffset:int = 32;

		// Menu Bar
			menuBar.y = titleHeight - 1;
			_mainContainer.addChild(menuBar);
			
		// Position-lock button
            if(!(model.value is EventPage || model.value is ProjectObject || model.value is LMS)){
                positionLockButton.draw(object.locked);
                positionLockButton.x = WIDTH - 17;
                positionLockButton.y = 4;
                _titleBar.addChild(positionLockButton);
            } else {
                wireLockPositionOffset = 17;
            }
			
		// Wire-lock button
			wireLockButton.draw(object.messageCenterLocked);
			wireLockButton.x = WIDTH - wireLockPositionOffset;
			wireLockButton.y = 4;
			_titleBar.addChild(wireLockButton);
		}
		
		private function get menuBar():MessageCenterMenuBar
		{
			if (!_menuBar)
				_menuBar = new MessageCenterMenuBar(this);
			return _menuBar;
		}
		
		private function get positionLockButton():MessageCenterLockPositionButton
		{
			if (!_positionLockButton)
				_positionLockButton = new MessageCenterLockPositionButton();
			return _positionLockButton;
		}
		
		private function get wireLockButton():MessageCenterLockWiresButton
		{
			if (!_wireLockButton)
				_wireLockButton = new MessageCenterLockWiresButton();
			return _wireLockButton;
		}

		private function addButtonListeners():void
		{
			wireLockButton.addEventListener(MouseEvent.CLICK, wireLockClickHandler, false, 0, true);
			positionLockButton.addEventListener(MouseEvent.CLICK, positionLockClickHandler, false, 0, true);

            _titleBar.addEventListener(MouseEvent.MOUSE_OVER,titleBoxMouseOverHandler);
			_titleBar.addEventListener(MouseEvent.MOUSE_OUT,titleBoxMouseOutHandler);
            menuBar.addEventListener(MouseEvent.MOUSE_OVER,titleBoxMouseOverHandler);
			menuBar.addEventListener(MouseEvent.MOUSE_OUT,titleBoxMouseOutHandler);
		}

        private function titleBoxMouseOverHandler(e:MouseEvent):void
		{
            _titleHoverTimer.addEventListener(TimerEvent.TIMER, openMenubar);

            _titleHoverTimer.reset();
            _titleHoverTimer.start();
            e.stopPropagation();
        }

        private function titleBoxMouseOutHandler(e:MouseEvent):void
		{
            _titleHoverTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, openMenubar);
            _titleHoverTimer.stop();
        }

        private function openMenubar(e:Event = null):void
		{
            _titlePulseTimer.removeEventListener(TimerEvent.TIMER,pulseTitle);
            Tweener.addTween(_titlePulse,{alpha:0,time:0.2,transition:"linear"});
            menuBar.open();
        }
		
		/**
		 * Adds sub-containers, such as the ribbon containers, to the message center 
		 * 
		 */
		private function addContainers():void
		{
			// Define main container
            if(_titleBar.parent == null) {
                // hasn't been stolen
			    _mainContainer.addChild(_titleBar);
            }
			_titleBar.addChild(_titleBox);
//			_mainContainer.width = WIDTH;
			
			// Create sub-containers
			_inletContainerView = new RibbonContainer(object.inletRibbons, INLETS , updateContainerPositions, context, role, this);
			_outletContainerView = new RibbonContainer(object.outletRibbons, OUTLETS, updateContainerPositions, context, role, this);
			_propertyContainerView = new RibbonContainer(object.propertyRibbons, PROPERTIES, updateContainerPositions, context, role, this);
			
			// Add children	
			_mainContainer.addChild(_inletContainerView);
			_mainContainer.addChild(_outletContainerView);
			_mainContainer.addChild(_propertyContainerView);
		}
		
		public function openContainer(value:String):void
		{
			switch (value) {
				case PROPERTIES:
					_propertyContainerView.openAll();
					_inletContainerView.closeAll();
					menuBar.inBtn.deselect();
					_outletContainerView.closeAll();
					menuBar.outBtn.deselect();
					break;
				case INLETS:
					_propertyContainerView.closeAll();
					menuBar.propsBtn.deselect();
					_inletContainerView.openAll();
					_outletContainerView.closeAll();
					menuBar.outBtn.deselect();
					break;
				case OUTLETS:
					_propertyContainerView.closeAll();
					menuBar.propsBtn.deselect();
					_inletContainerView.closeAll();
					menuBar.inBtn.deselect();
					_outletContainerView.openAll();
					break;
			}			
		}
		
		public function closeContainer(value:String):void
		{
			switch (value) {
				case PROPERTIES:
					if (!menuBar.propsBtn.selected)
						_propertyContainerView.closeAll();
					break;
				case INLETS:
					if (!menuBar.inBtn.selected)
						_inletContainerView.closeAll();
					break;
				case OUTLETS:
					if (!menuBar.outBtn.selected)
						_outletContainerView.closeAll();
					break;
				case ALL_CONTAINERS:
					if (!menuBar.propsBtn.selected) _propertyContainerView.closeAll();
					if (!menuBar.inBtn.selected) _inletContainerView.closeAll();
					if (!menuBar.outBtn.selected)_outletContainerView.closeAll();

					break;
			}
            updateHeight();
		}
		
		public function openCategory(category:String):void
		{
			if (_propertyContainerView) {
				menuBar.open();
	            menuBar.propsBtn.selected = true;
				_propertyContainerView.openCategory(category);
			}
		}

        public function openCategoriesByArray(array:Vector.<RibbonContainerInfo>):void
        {
            for each (var info:RibbonContainerInfo in array) {
                var container:RibbonContainer = containerForName(info.container);
                container.openCategory(info.category, false);
            }
        }

        public function get arrayOfOpenCategories():Vector.<RibbonContainerInfo>
        {
            var result:Vector.<RibbonContainerInfo> = new <RibbonContainerInfo>[];
            for each (var cont:String in [INLETS, OUTLETS, PROPERTIES]) {
                var container:RibbonContainer = containerForName(cont);
                var array:Vector.<RibbonContainerInfo> = container.arrayOfOpenCategories(cont);
                for each (var info:RibbonContainerInfo in array) {
                    result.push(info);
                }
            }
            return result;
        }

        private function containerForName(name:String):RibbonContainer
        {
            switch (name) {
                case INLETS:
                    return _inletContainerView;
                case OUTLETS:
                    return _outletContainerView;
                default:
                    return _propertyContainerView;
            }
        }

		override protected function addRollHighlight(e:MouseEvent) : void
		{
			this.selfRolled = true;
		}
		
		override protected function removeRollHighlight(e:MouseEvent) : void
		{
			// don't trigger rollout when rolling over a wire end
			if (!((e.localX > 0 && e.localX < 20) || (e.localX > WIDTH-20 && e.localX < WIDTH)))
				if(!(e.relatedObject as WireView) 
						&& !(e.relatedObject && (e.relatedObject.parent as ControlAreaToolTip
                                               ||e.relatedObject as UIComponent))){
					this.selfRolled = false;
				}
		}


        public function get proxyRolled():Boolean {
            return _proxyRolled;
        }

        public function set proxyRolled(value:Boolean):void {
//            trace("Proxy Rolled [" + (AbstractObject(model.value).title) + "] " + value);
            _proxyRolled = value;
            rolled = proxyRolled || selfRolled;
        }

        public function get selfRolled():Boolean {
            return _selfRolled;
        }

        public function set selfRolled(value:Boolean):void {
//            trace("Self Rolled [" + (AbstractObject(model.value).title) + "] " + value);
            _selfRolled = value;
            rolled = proxyRolled || selfRolled;
        }

        // Opening and closing message centers and containers
			
		/**
		 * Opens or closes a collapsed message center to reveal/hide the menu buttons 
		 * @param value: When true, open. When false, close.
		 * 
		 */
		public function set rolled(value:Boolean):void
		{
//            trace("Rolled [" + (AbstractObject(model.value).title) + "] " + value);
			if(Application.instance.wireDragDescription)
				Application.instance.wireDragDescription.shownMessageCenter = null;
			if(Application.instance.assetDragDescription)
				Application.instance.assetDragDescription.shownMessageCenter = null;

			if (object.wiringLocked && fullClose) {
				menuBar.close();
				updateContainerPositions();
				return;
			}
			
			if (Application.instance.draggingMessageCenter)
				return; // Don't open or close message center while we are dragging
				
            if(!value) {
            	// if mc can close all the way
				if (menuBar.selectedMenuButtons == 0 && Application.instance.wireDragDescription==null && Application.instance.assetDragDescription==null) {
					menuBar.close();
				}
//                trace("Rolled Timer [" + (AbstractObject(model.value).title) + "] stop");
                _anyRolled = false;
                _titlePulseTimer.stop();
                _titlePulseTimer.removeEventListener(TimerEvent.TIMER,pulseTitle);
                Tweener.addTween(_titlePulse,{alpha:0,time:0.2,transition:"linear"});
			} else if (!menuBar.isOpen) {
//                trace("Rolled Timer [" + (AbstractObject(model.value).title) + "] start");
                _anyRolled = true;
                _titlePulseTimer.addEventListener(TimerEvent.TIMER,pulseTitle);
                _titlePulseTimer.start();
                pulseTitle();
            }

			// Mouse over with a wire or asset
			if (Application.instance.wireDragDescription || Application.instance.assetDragDescription) {
				mouseOverWithWire(value);
			}
		}

        public function get rolled():Boolean {
            return _anyRolled;
        }
		
		private function mouseOverWithWire(value:Boolean):void
		{	
			var drag:DragDescription = Application.instance.wireDragDescription;
			if(drag == null) {
				drag = Application.instance.assetDragDescription;
			}
			// TODO: Update wires cannot update a wire we are dragging because it doesn't exist yet.
			//		Find a way to update "dragging" wires when trying to wire a message center to itself.
			//		Also, property self-wiring doesn't open property container when the menu button is deselected
			if (value) {
				// prevent wire from opening objects own ribbons inappropriately
				var propertySelfWire:Boolean = drag is WireDragDescription && WireDragDescription(drag).fromObj == model;
				if (!object.wiringLocked && !propertySelfWire) {
					drag.shownMessageCenter = this.model;
				} else if (drag as AssetDragDescription) {
					drag.shownMessageCenter = this.model;
				}
			}
			else if (!(drag as AssetDragDescription)) {
				if (!(drag as WireDragDescription) || (drag as WireDragDescription).fromObj != model) {
					closeContainer(ALL_CONTAINERS);
					if (fullClose) {
						menuBar.close();
					}
				}
			}
		}
			
		/**
		 * Updates the positions of the message center sub-containers. Currently, this is used in openRibbonContainer and 
		 * ultimately passed to the onUpdate parameter of the height tweening of sub-containers to ensure all the sub-containers 
		 * will update as an individual sub-container changes its height. 
		 * 
		 */
		
		public function updateContainerPositions():void
		{
			// update each container to be just below the one before it
			
			if (_inletContainerView && _outletContainerView && _propertyContainerView) {
				_inletContainerView.y = menuBar.y + menuBar.perceivedHeight + (this.fullClose? 0:0);
				_propertyContainerView.y = _inletContainerView.y  + _inletContainerView.perceivedHeight + (ribbonsShowingForContainer(_inletContainerView) ? RibbonContainer.PAD : 0);
				_outletContainerView.y = _propertyContainerView.y + _propertyContainerView.perceivedHeight + (ribbonsShowingForContainer(_propertyContainerView) ? RibbonContainer.PAD : 0);

				// Update ribbon anchor positions
				updateAnchors();
				updateHeight();
			}
		}
		
		private function ribbonsShowingForContainer(container:RibbonContainer):Boolean
		{
			/*trace("Ribbon showing for container="+container._type, "open="+container.open, "ribbonsShowing="+container.ribbonsShowing);
			trace("	>> return="+(container.open || container.ribbonsShowing));*/
			return container.open || container.ribbonsShowing;
		}
		
		private function updateHeight():void
		{
			// Update MC background height;
			_h = menuBar.perceivedHeight + _inletContainerView.perceivedHeight + _outletContainerView.perceivedHeight + _propertyContainerView.perceivedHeight + titleHeight;
			_h += fullClose ? 0 : BOTTOM_HEIGHT;
			_h += containerPadding;

			drawMainContainer();
			
		/*	trace("MC::updateHeight::menuBar.perceivedHeight="+menuBar.perceivedHeight, "inlet height="+_inletContainerView.perceivedHeight, "outlet height="+_outletContainerView.perceivedHeight, 
						"property height="+_propertyContainerView.perceivedHeight, "full close="+fullClose, "containerPadding="+containerPadding);*/
		}

		/**
		 * Update the yContainer values of the wire anchors, so wires will draw accurately
		 * to the port locations. 
		 * 
		 */
		private function updateAnchors():void
		{
			if (_propertyContainerView && _outletContainerView && _inletContainerView) {
				_propertyContainerView.updateAnchors();
				_outletContainerView.updateAnchors();
				_inletContainerView.updateAnchors();
			}
		}
		
		/**
		 * When true, message center can collapse all the way down to the title height 
		 * @return 
		 * 
		 */
		private function get fullClose():Boolean
		{
			var willFullClose:Boolean = menuBar.selectedMenuButtons==0 
				&& !_inletContainerView.ribbonsShowing
				&& !_propertyContainerView.ribbonsShowing
				&& !_outletContainerView.ribbonsShowing
				&& Application.instance.wireDragDescription==null
				&& Application.instance.assetDragDescription==null;
			//trace("full close: ", willFullClose);
			return willFullClose;		
		}
		
		private function get containerPadding():Number
		{
			return (_inletContainerView.open || _inletContainerView.ribbonsShowing ? RibbonContainer.PAD : 0) 
                    + (_propertyContainerView.open || _propertyContainerView.ribbonsShowing ? RibbonContainer.PAD : 0);
                          + (_outletContainerView.open || _outletContainerView.ribbonsShowing ? RibbonContainer.PAD : 0);
		}

		public function finishWireDrag():void 
		{
			if (!menuBar.propsBtn.selected) 
				closeContainer(PROPERTIES);
			if (!menuBar.inBtn.selected) 
				closeContainer(INLETS);
			if (!menuBar.outBtn.selected) 
				closeContainer(OUTLETS);
			
			_propertyContainerView.updateRibbonPositions();
			_inletContainerView.updateRibbonPositions();
			_outletContainerView.updateRibbonPositions();
			
			updateView();
		}
		
		/**
		 * Decide which container to open, given the modifier of the other end. 
		 * @param modifier
		 * @return 
		 * 
		 */
//		public static function containerForWire(anchor:WireAnchor):String
//		{
//			var modifier:IModifier = anchor.modifierDescription;  // TODO:  this needs InletDescription as done below
//			if (modifier is PropertyDescription)
//				return MessageCenterView.PROPERTIES;
//			else if (modifier is OutletDescription)
//				return MessageCenterView.INLETS;
//			else (modifier is InletDescription)
//				return MessageCenterView.OUTLETS;
//		}
		
		/**
		 * Return true if specified container is OK to connect to the wire. 
		 * @param type
		 * @param drag
		 * @return 
		 * 
		 */
		public static function containerOkForWire(container:String, drag:WireDragDescription):Boolean
		{	
			if (container == PROPERTIES)
				return true;  // any wire can open the props container
			var anchor:WireAnchor = drag.fromAnchor;
			var modifier:IModifier = anchor.modifierDescription;
			if (modifier is PropertyDescription)
				return true;  // property-wire can open any container
			
			// when dragging wire from an outlet, open the Inlets
			// but when dragging from a peg on the wireboard, open the same kind
			var isInlet:Boolean = (modifier is InletDescription);
			if (!(anchor is InternalSmartWireAnchor))
				isInlet = !isInlet;  // not a peg, so look for opposite kind
			switch(container) {
				case INLETS:  return isInlet;
				case OUTLETS: return !isInlet;
			}
			return false;
		}
		
		override public function toString():String
		{	
			// [MC #101 "my obj title" x=42 y=55]
            var title:String = "";
            if (_titleBox)
                title = _titleBox.text;
			return "[MsgCtr \"" + title + "\" mc.vis="+visible + " " + _objectView + "]";
		}
				
		private function wireLockClickHandler(e:MouseEvent):void
		{
			var undo:String = (object.messageCenterLocked) ? "Unlock Wiring" : "Lock Wiring";
			var oldValue:Boolean = object.messageCenterLocked;
			object.messageCenterLocked = !object.messageCenterLocked;
			var action:UserAction = ModifyObjectProperty.fromObject(object, undo, 
					"messageCenterLocked", -1, oldValue);
			ApplicationController.currentActionTree.commit(action);
            ApplicationController.instance.wireController.requestRedrawForObject(object);
		}
		
		private function positionLockClickHandler(e:MouseEvent):void
		{
			var undo:String = (object.locked) ? "Unlock Position" : "Lock Position";
			var oldValue:Boolean = object.locked;
			object.locked = !object.locked;
			var action:UserAction = ModifyObjectProperty.fromObject(object, undo, 
				"locked", -1, oldValue);
			ApplicationController.currentActionTree.commit(action);
		}

		override public function warnViewToDelete() : void
		{
//            trace("MC.warnViewToDelete view="+_objectView);
			super.warnViewToDelete();
            if (stage)
                stage.removeEventListener(MouseEvent.MOUSE_UP, handleDeferredClose);
			if (_mediator) // if we never created a message center, the mediator won't exist.
				_mediator.clearViewEvents();
			Tweener.removeTweens(this);
		}
		
		
		/**
		 * Provide the bounds of this message center view in parent coordinates. 
		 * @return = the bounds rectangle.
		 * 
		 */
		public function get myBounds():Rectangle
		{
			var rect:Rectangle = new Rectangle(x-BORDER_WIDTH/2, y-BORDER_WIDTH/2,
						WIDTH+BORDER_WIDTH, _mainContainer.height+BORDER_WIDTH);
			return rect;
		}
		
		public function get colorPicker():ColorPicker
		{
			return _colorPicker;
		}
		
		
		public function updatePosition(area:Rectangle):void
		{
			// called by render handler
			var objView:ObjectView = _objectView;
			if (objView) {
				if (objView.updateMessageCenterPosition(area, _h, draggingWireOver())) {
                    markItReady();
                }
            } else if (object is LMS || object is ProjectObject) {
                var location:Point = new Point(object.xMessageCenter, object.yMessageCenter);
                location = context.globalToLocal(location);
                x = location.x;
                y = location.y;
                markItReady();
            }
		}

        private function markItReady():void
        {
            if (!_mainContainer.visible) {
                _mainContainer.visible = true; // hidden at initial creation
                ApplicationController.instance.wireController.messageCenterCreatedAndReady(this);
            }
        }

        public function get readyForWires():Boolean
        {
            // MC has been positioned and is ready to have wires drawn to it
            return _mainContainer.visible;
        }
		
		private function draggingWireOver():Boolean
		{
			if (mouseX < 0 || mouseX > WIDTH || mouseY < 0 || mouseY > _h)
				return false;
			return Application.instance.wireDragDescription || Application.instance.assetDragDescription;
		}

		
		override public function getBounds(frame:DisplayObject):Rectangle
		{
			var global:Point = localToGlobal(ZERO);  // topLeft of MC
			var local:Point = frame.globalToLocal(global);
			return new Rectangle(local.x, local.y, WIDTH, _h);
		}
		
		override public function get okToSelect():Boolean
		{
			return true;  // allow invisible or locked object to be selected via clicking the MC
		}
		
		public function ribbonUnderPoint(stagePoint:Point):RibbonView
		{
			var ribbon:RibbonView = null;
			var containers:Array = [_inletContainerView, _outletContainerView, _propertyContainerView];
			for each (var container:RibbonContainer in containers) {
				ribbon = container.ribbonUnderPoint(stagePoint);
				if (ribbon)
					return ribbon;
			}
			return null;
		}

        private function get titleHeight():Number {
            return _titleBar.parent == _mainContainer ? TITLE_HEIGHT : TITLE_HEIGHT;
        }

        public function yieldTitleBar():Sprite {
            if(_titleBar && _titleBar.parent && _titleBar.parent == _mainContainer) {
                _mainContainer.removeChild(_titleBar);
//                menuBar.y = -1 + 5;
                updateContainerPositions();
            }
            return _titleBar;
        }


        public function get titleEditable():Boolean {
            return _titleEditable;
        }

        public function set titleEditable(value:Boolean):void {
            _titleEditable = value;
            if(_titleBox) {
                _titleBox.editable = value;
            }
        }

        override public function get height():Number {
            return _h;
        }

        public function get objectView():ObjectView {
            return _objectView
        }

        public function set objectView(view:ObjectView):void {
            _objectView = view;
        }

        public function get isOpen():Boolean {
            return _inletContainerView.open || _propertyContainerView.open || _outletContainerView.open;
        }

        public function resetDraggedSinceOpened():void {
            if (!isOpen)
                _draggedSinceOpened = false;
        }

        public function dragStarted():void {
            if(isOpen) _draggedSinceOpened = true;
        }

        public function get draggedSinceOpen():Boolean {
            return _draggedSinceOpened;
        }
}
}
