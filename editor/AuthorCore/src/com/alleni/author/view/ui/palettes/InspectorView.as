package com.alleni.author.view.ui.palettes
{
    import assets.icons.inspector.eyeWhite;
    import assets.icons.inspector.noWhite;
    
    import caurina.transitions.Tweener;
    
    import com.alleni.author.application.ui.EditorUI;
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.controller.ui.palettes.InspectorController;
    import com.alleni.author.definition.InletDescription;
    import com.alleni.author.definition.OutletDescription;
    import com.alleni.author.definition.PropertyDescription;
    import com.alleni.author.definition.ViewRoles;
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.event.AnchorEvent;
    import com.alleni.author.event.ScrollUpdateEvent;
    import com.alleni.author.model.ScrollModel;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.model.ui.InspectorValues;
    import com.alleni.author.model.ui.WireAnchor;
    import com.alleni.author.view.PaletteVerticalScrollView;
    import com.alleni.author.view.text.LightLabel;
    import com.alleni.author.view.ui.MessageCenterView;
    import com.alleni.author.view.ui.RibbonContainer;
    import com.alleni.taconite.event.ApplicationEvent;
    
    import flash.display.DisplayObject;
    import flash.display.GradientType;
    import flash.display.Graphics;
    import flash.display.Shape;
    import flash.display.SpreadMethod;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.filters.DropShadowFilter;
    import flash.geom.Matrix;
    import flash.geom.Point;
    import flash.geom.Rectangle;
    import flash.utils.Dictionary;
    
    import mx.collections.ArrayCollection;

	public class InspectorView extends Sprite
	{
		private static const HEADING_CUSHION:Number 	= 0//2;
		private static const TRANSITION:String			= "easeOutQuart";
		private static const TIME:Number				= 0.25;
		private static const MARGIN:Number 				= 1;
		private static const CELL_HEIGHT:uint 			= 25;
		private static const TOP_MARGIN:Number 			= 5;
		private static const BOTTOM_MARGIN:Number 		= 5;
		private static const LINE_THICKNESS:Number 		= 1;
		private static const LEFT_MARGIN:Number 		= 15;
		private static const GAP:Number 				= 6;
		private static const RIBBON_HEAD_HEIGHT:Number 	= 12;
		private static const RIBBON_HEIGHT:Number 		= 16;
		
		private var _propertyContainer:RibbonContainer;
		private var _inletContainer:RibbonContainer;
		private var _outletContainer:RibbonContainer;

		private var _cachedRibbonViews:Dictionary = new Dictionary;
		private var _titleBar:Sprite;
		private var _selectedObjLabel:LightLabel;
        private var _selectedObjIcon:DisplayObject;
		private var _messageCenterToggle:Sprite;
		private var _messageCenterToggleLabel:LightLabel;
		private var _messageCenterToggleIcon:Sprite;
        private var _mcToggleIcon:eyeWhite = new eyeWhite();
        private var _mcNoIcon:noWhite = new noWhite();

		private var _mcToggleOn:Boolean = false;
		private var _mcEnabled:Boolean;

		private var _scrollRect:Rectangle;
		private var _scrollbar:PaletteVerticalScrollView;
		private var _scrollModel:ScrollModel;
		private var _scrollPosition:Number = 0;
        private var _inspectorScrollPosition:Number = 0;
        private var _libraryScrollPosition:Number = 0;
		private var _ribbonScrollContainer:Sprite;

		private var _showInlets:Boolean = true;
		private var _showProperties:Boolean = true;
		private var _showOutlets:Boolean = true;

        private var _scrollHeight:Number = 0;
        private var _containerHeight:Number = 0;

		public function InspectorView()
		{
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}

		private function initialize(event:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			var shadow:DropShadowFilter = new DropShadowFilter(1,80,0,0.8,2,2,2,1);
			
			_titleBar = new Sprite();
			createMessageCenterToggleButton(shadow);
			draw();

			_selectedObjLabel = new LightLabel();
			_selectedObjLabel.color = 0xFFFFFF;
			_selectedObjLabel.text = "";
			_selectedObjLabel.y = 5;
			_selectedObjLabel.x = 15;
			_selectedObjLabel.width = 150;
			_selectedObjLabel.filters = [shadow];
			
			addChild(_titleBar);
			addChild(_messageCenterToggle);

			_titleBar.addChild(_selectedObjLabel);

			_ribbonScrollContainer = new Sprite();

			addChild(_ribbonScrollContainer);
			updateRibbons();

			ApplicationController.addEventListener(NotificationNamesApplication.WORLD_REPOSITIONED, worldRepositioned);

			_scrollRect = new Rectangle(0,0,160, _containerHeight);
			_scrollModel=new ScrollModel(_scrollRect.height, 0, 500);
			_scrollbar = new PaletteVerticalScrollView(_scrollModel, 200, _containerHeight);
			_scrollbar.x = 150; //_itemArea.width + 1;
			_scrollbar.y = _ribbonScrollContainer.y;
			_scrollbar.addEventListener(ScrollUpdateEvent.SCROLL, handleScroll);
			_scrollbar.scrollPosition = _scrollPosition;
			_ribbonScrollContainer.scrollRect = _scrollRect;
			addChild(_scrollbar);

			InspectorValues.instance.addEventListener(AnchorEvent.TYPE, handleAnchorChange);
		}

        private function worldRepositioned(event:ApplicationEvent):void
        {
            _containerHeight = event.data.height - _ribbonScrollContainer.y;
            updateScrollRect();
        }

		private function updateScrollRect():void
		{
			if (_scrollModel && _ribbonScrollContainer) {
				_scrollbar.scrollControllerHeight = _containerHeight;
				_scrollRect.height = _containerHeight;
				_ribbonScrollContainer.scrollRect = _scrollRect;
				_scrollModel.viewPortLength = _containerHeight;
				_scrollModel.maxScrollPosition = _scrollHeight;
			}
			if (_scrollbar) {
				if (this.contains(_scrollbar) && _containerHeight > _scrollHeight) {
					removeChild(_scrollbar)
				} else if (!this.contains(_scrollbar) && _containerHeight <= _scrollHeight) {
					addChild(_scrollbar);
				}
			}

            if (_scrollbar && Application.instance.ribbonLibraryVisible) {
                _scrollbar.scrollPosition = 0;
            } else if (_scrollbar && Application.instance.inspectorVisible) {
                _scrollbar.scrollPosition = 0;
            }
		}
		
		private function handleScroll(event:ScrollUpdateEvent):void
		{
			if (_scrollbar) {
				if (event && event.params && (event.params.hasOwnProperty("scrollPosition"))) {
					_scrollRect.y = event.params.scrollPosition;
					_ribbonScrollContainer.scrollRect = _scrollRect;

				}
			}
		}

        public function saveScrollBarPosition():void
		{
            if (!Application.instance.ribbonLibraryVisible && _scrollbar) {
                _libraryScrollPosition =_scrollbar.scrollPosition;
            } else if (!Application.instance.inspectorVisible && _scrollbar) {
                _inspectorScrollPosition =_scrollbar.scrollPosition;
            }
        }
		
		public function restoreScrollBarPosition():void{
			if (!Application.instance.ribbonLibraryVisible && _scrollbar) {
				_scrollbar.scrollPosition = _libraryScrollPosition;
			} else if (!Application.instance.inspectorVisible && _scrollbar) {
				_scrollbar.scrollPosition = _inspectorScrollPosition;
			}
		}

        public function resetScrollPosition():void
		{
            if (Application.instance.ribbonLibraryVisible && _scrollbar) {
				_scrollbar.scrollPosition = _libraryScrollPosition = 0;
            } else if (Application.instance.inspectorVisible && _scrollbar) {
                _scrollbar.scrollPosition = _inspectorScrollPosition = 0;
            }
        }
		
		private function createMessageCenterToggleButton(shadow:DropShadowFilter):void
		{
			_messageCenterToggle = new Sprite();
			_messageCenterToggleLabel = new LightLabel(100);
			_messageCenterToggleIcon = new Sprite();
            _messageCenterToggleIcon.y = 2;
			_messageCenterToggleLabel.text = "Message Center";
			_messageCenterToggleLabel.color = 0xffffff;
			_messageCenterToggleLabel.x = 20;
			_messageCenterToggleLabel.filters = [shadow];
			
			_messageCenterToggle.addChild(_messageCenterToggleLabel);
			_messageCenterToggle.addChild(_messageCenterToggleIcon);
			_messageCenterToggle.y = TOP_MARGIN + CELL_HEIGHT;
			_messageCenterToggle.x = 15;
			_messageCenterToggleLabel.graphics.beginFill(0xff0000, 0.01);
			_messageCenterToggleLabel.graphics.drawRect(-30, 0, EditorUI.RIGHT_PALETTE_WIDTH - GAP, 15);
			_messageCenterToggle.addEventListener(MouseEvent.CLICK, handleMessageCenterToggleClick);
			_messageCenterToggle.filters = [shadow];
		}

		private function draw():void 
		{
			drawTitleBackground(_titleBar, EditorUI.RIGHT_PALETTE_WIDTH - GAP, CELL_HEIGHT);
			
			// Message Center Toggle Switch
			updateMessageCenterToggle(true, true);
		}
		
		private function drawBorder(yPos:Number):void
		{
			var paletteHeight:Number = EditorUI.instance.paletteHeight;
			
			with(this.graphics){
				clear();
				lineStyle(LINE_THICKNESS,0x646464);
				drawRect(0,CELL_HEIGHT,EditorUI.RIGHT_PALETTE_WIDTH - GAP, paletteHeight + 4 -  (CELL_HEIGHT + TOP_MARGIN));
				
				drawDivider(yPos);
			}
		}
		
		private function drawTitleBackground(sprite:Sprite, width:Number, height:Number):void
		{
			var gradientShadow:DropShadowFilter = new DropShadowFilter(1, 90, 0, 0.8, 3, 3, 1, 3, true);
			with (sprite.graphics){
				clear();
                makeGradient(sprite.graphics,width,25);
				lineStyle(LINE_THICKNESS,0x646464,1,true);
				drawRoundRectComplex(0,0,width,height,15,0,0,0);
                endFill();

				moveTo(0, height + CELL_HEIGHT);
				lineTo(width, height + CELL_HEIGHT);	
			}
			sprite.filters = [gradientShadow];
			
			var secondaryGradient:Shape = new Shape();
			this.addChild(secondaryGradient);
			makeGradient(secondaryGradient.graphics,width,25,height);
			with (secondaryGradient.graphics){
				lineStyle(LINE_THICKNESS,0x646464,1,true);
				drawRect(0,height,width,height);
				endFill();
			}
			secondaryGradient.filters = [gradientShadow];
		}
		
		private function drawDivider(yPosition:Number):void
		{
			var rightPaletteWidth:Number = 0;
			with (this.graphics){
				lineStyle(LINE_THICKNESS,0x646464);
				moveTo(0, yPosition);
				lineTo(EditorUI.RIGHT_PALETTE_WIDTH - GAP, yPosition);
			}
		}
		
		public function updateMessageCenterToggle(on:Boolean, enabled:Boolean):void
		{
			_mcToggleOn = on;
			_mcEnabled = enabled;
			if (_mcToggleOn) { //On
                if(_messageCenterToggleIcon.contains(_mcNoIcon))
                    _messageCenterToggleIcon.removeChild(_mcNoIcon);
                _messageCenterToggleIcon.addChild(_mcToggleIcon);
            } else { // Off
                if(_messageCenterToggleIcon.contains(_mcToggleIcon))
                    _messageCenterToggleIcon.removeChild(_mcToggleIcon);
                _messageCenterToggleIcon.addChild(_mcNoIcon);
			}
		}

        public function setMessageCenterToggleVisibility(visible:Boolean):void
		{
            _messageCenterToggle.visible = visible;
        }

        public function setSelectedIcon(icon:DisplayObject):void
		{
            if (_selectedObjIcon && _titleBar.contains(_selectedObjIcon)) {
                _titleBar.removeChild(_selectedObjIcon);
            }
            _selectedObjIcon = icon;
            if (_selectedObjIcon) {
                _titleBar.addChild(_selectedObjIcon);
                _selectedObjIcon.x = 13;
                _selectedObjLabel.x = 18+_selectedObjIcon.width;
                _selectedObjIcon.y = 4;
            } else {
                _selectedObjLabel.x = 15;
            }
        }
		
		private function handleMessageCenterToggleClick(e:MouseEvent):void
		{
			if (_mcEnabled) {
				_mcToggleOn = !_mcToggleOn;
				updateMessageCenterToggle(_mcToggleOn, _mcEnabled);
				
				InspectorController.instance.toggleMessageCenters(_mcToggleOn);
			}
		}
		
		public function updateSelectedObjectName(name:String):void
		{
			if(_selectedObjLabel.text != name){
				_selectedObjLabel.alpha = 0;
				Tweener.addTween(_selectedObjLabel, {alpha:1, time:TIME, transition:TRANSITION});
				_selectedObjLabel.text = name;
			}
		}
		
		private function ribbonsShowingForContainer(container:RibbonContainer):Boolean
		{
			return container.open || container.ribbonsShowing;
		}

		public function updateContainerPositions():void
		{
			// update each container to be just below the one before it
			saveScrollBarPosition();
			_ribbonScrollContainer.y =  2 * CELL_HEIGHT + TOP_MARGIN;

			if (_inletContainer && _propertyContainer && _showInlets) {
				_propertyContainer.y = _inletContainer.y  + _inletContainer.perceivedHeight
					+ (ribbonsShowingForContainer(_inletContainer) ? RibbonContainer.PAD : 0);
			}
			if (_outletContainer && _inletContainer) {
				_outletContainer.y = _propertyContainer.y + _propertyContainer.perceivedHeight
					+ (ribbonsShowingForContainer(_propertyContainer) ? RibbonContainer.PAD : 0);
			}


			var h:int = 0;

			if (_outletContainer && _outletContainer.y + _outletContainer.perceivedHeight > h && _outletContainer.stage) {
				h= _outletContainer.perceivedHeight + _outletContainer.y;
            }
			if (_propertyContainer && _propertyContainer.y + _propertyContainer.perceivedHeight > h && _propertyContainer.stage) {
				h= _propertyContainer.perceivedHeight + _propertyContainer.y;
            }
			if (_inletContainer && _inletContainer.y + _inletContainer.perceivedHeight > h && _inletContainer.stage) {
				h= _inletContainer.perceivedHeight + _inletContainer.y;
            }
            _scrollHeight = h;

			updateScrollRect();
			
			restoreScrollBarPosition();
		}
		
		private function handleAnchorChange(e:AnchorEvent):void
		{
			var container:RibbonContainer;
			if(e.anchor.modifierDescription is InletDescription && _inletContainer) {
				container = _inletContainer;
				container.openAll(); // TODO: This is required to show dynamically added setters
			} else if(e.anchor.modifierDescription is PropertyDescription && _propertyContainer) {
				container = _propertyContainer;
			} else if(e.anchor.modifierDescription is OutletDescription && _outletContainer) {
				container = _outletContainer;
			}
			switch (e.kind) {
				case AnchorEvent.ADD:
					if (container)
						container.addRibbon(e.anchor, e.display, null);
					break;
				
				case AnchorEvent.REMOVE:
					if (container)
						container.removeRibbon(e.anchor, null);
					break;
				
				default:
					throw new Error("Unexpected AnchorEvent.kind: " + e.kind);
			}
		}

        private function makeGradient(g:Graphics, w:Number = 400, h:Number = RIBBON_HEIGHT, yOffset:int = 0, xOffset:int = 0):void
		{
            var fillType:String = GradientType.LINEAR;
            var colors:Array = [0x46638A, 0x000000];
            var alphas:Array = [1, 1];
            var ratios:Array = [0x00, 0xFF];
            var matr:Matrix = new Matrix();
            matr.createGradientBox(w, h, Math.PI/2);
            matr.translate(xOffset,yOffset);
            var spreadMethod:String = SpreadMethod.PAD;
            g.beginGradientFill(fillType, colors, alphas, ratios, matr,spreadMethod);
        }

		public function updateRibbons():void
		{
			_showInlets = _showOutlets = Application.instance.ribbonLibraryVisible;
			_showProperties = Application.instance.inspectorVisible || Application.instance.ribbonLibraryVisible;

			var anchor:WireAnchor;

            var oldPropContainer:RibbonContainer = _propertyContainer;
            var oldInContainer:RibbonContainer = _inletContainer;
            var oldOutContainer:RibbonContainer = _outletContainer;
			
			_propertyContainer = null;
            _inletContainer = null;
            _outletContainer = null;
			
			if (oldPropContainer != null) oldPropContainer.clearRibbonQueue();
            if (oldInContainer != null) oldInContainer.clearRibbonQueue();
            if (oldOutContainer != null) oldOutContainer.clearRibbonQueue();

            var fadeTime:Number = 0.8;
//            if ((oldPropContainer==null || oldPropContainer.perceivedHeight == 0) && (oldInContainer==null || oldInContainer.perceivedHeight==0) && (oldOutContainer==null || oldOutContainer.perceivedHeight==0))
//                fadeTime = 0.2;

			// Add back the containers we need.
            if (_showInlets) {
                var ins:ArrayCollection = new ArrayCollection();
                for(key in InspectorValues.instance.inletModifiers) {
                    anchor = InspectorValues.instance.anchors[key] as WireAnchor;
                    if (anchor.propertyInspectorVisible || Application.instance.ribbonLibraryVisible)
                        ins.addItem(anchor);
                }
                _inletContainer = new RibbonContainer(ins, MessageCenterView.INLETS, updateContainerPositions, EditorUI.instance.presentationContainer, ViewRoles.PRIMARY, null, true,_cachedRibbonViews,true,true,0);
                _ribbonScrollContainer.addChild(_inletContainer);
                _inletContainer.alpha = 0;
                Tweener.addTween(_inletContainer,{alpha:1,time:fadeTime,transition:"easeInExpo"});

                if(ins.length > 0) {
                    _inletContainer.openAll(false);
                }
            }
			if (_showProperties) {
				var p:ArrayCollection = new ArrayCollection();
				for(var key:String in InspectorValues.instance.modifiers) {
					anchor = InspectorValues.instance.anchors[key] as WireAnchor;
					if (anchor.propertyInspectorVisible || Application.instance.ribbonLibraryVisible)
						p.addItem(anchor);
				}
				_propertyContainer = new RibbonContainer(p, MessageCenterView.PROPERTIES, updateContainerPositions, EditorUI.instance.presentationContainer, ViewRoles.PRIMARY, null, true,_cachedRibbonViews,true,true,0);
				_ribbonScrollContainer.addChild(_propertyContainer);
                _propertyContainer.alpha = 0;
                Tweener.addTween(_propertyContainer,{alpha:1,time:fadeTime,transition:"easeInExpo"});
                _propertyContainer.tweenTime = 0.25;

				if(p.length > 0) {
					_propertyContainer.openAll(false);
				}
			}
			if (_showOutlets) {
				var outs:ArrayCollection = new ArrayCollection();
				for(key in InspectorValues.instance.outletModifiers) {
					anchor = InspectorValues.instance.anchors[key] as WireAnchor;
					if (anchor.propertyInspectorVisible || Application.instance.ribbonLibraryVisible)
						outs.addItem(anchor);
				}
				_outletContainer = new RibbonContainer(outs, MessageCenterView.OUTLETS, updateContainerPositions, EditorUI.instance.presentationContainer, ViewRoles.PRIMARY, null, true,_cachedRibbonViews,true,true,0);
				_ribbonScrollContainer.addChild(_outletContainer);
                _outletContainer.alpha = 0;
                Tweener.addTween(_outletContainer,{alpha:1,time:fadeTime,transition:"easeInExpo"});

				if(outs.length > 0) {
					_outletContainer.openAll(false);
				}
			}

            fadeTime = 1.3;
            if ((ins==null || ins.length == 0) && (outs==null || outs.length==0) && (p==null || p.length==0))
                fadeTime = 0.2;

			// Remove the containers if they're in the DisplayList. They will be added back if they are needed.
			if (oldPropContainer && _ribbonScrollContainer.contains(oldPropContainer)) {
				Tweener.addTween(oldPropContainer,{alpha:0,time:fadeTime, transition: "linear", onComplete:function():void{ _ribbonScrollContainer.removeChild(oldPropContainer); updateContainerPositions();}});
            } else {
                updateContainerPositions();
            }

			if (oldInContainer && _ribbonScrollContainer.contains(oldInContainer)) {
                Tweener.addTween(oldInContainer,{alpha:0,time:fadeTime, transition: "linear", onComplete:function():void{ _ribbonScrollContainer.removeChild(oldInContainer);}});
            } else {
                updateContainerPositions();
            }

            if (oldOutContainer && _ribbonScrollContainer.contains(oldOutContainer)) {
                Tweener.addTween(oldOutContainer,{alpha:0,time:fadeTime, transition: "linear", onComplete:function():void{ _ribbonScrollContainer.removeChild(oldOutContainer);}});
            } else {
                updateContainerPositions();
            }
		}
	}
}