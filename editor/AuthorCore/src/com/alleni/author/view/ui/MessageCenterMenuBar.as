package com.alleni.author.view.ui
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.controller.ui.WireDragDescription;
	import com.alleni.author.model.ui.Application;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.utils.Timer;

public class MessageCenterMenuBar extends Sprite
	{
		public static const OPEN_HEIGHT:Number = 20;
		
		private static const MENU_BUTTON_Y:Number = 1;
		private static const IN_BUTTON_X:Number = 3;
		private static const OUT_BUTTON_X:Number = 112;
		private static const PROPS_BUTTON_X:Number = 42;
		
		private var _open:Boolean = false;
		
		private var _inBtn:MenuButton;
		private var _outBtn:MenuButton;
		private var _propsBtn:MenuButton;

        private var _pendingContainer:String;
        private var _pendingTimer:Timer = new Timer(200);
		
		private var _mask:Sprite;
		private var _background:Sprite;
		
		private var _perceivedHeight:Number = 9;
		
		private var _view:MessageCenterView;
		
		public function MessageCenterMenuBar(view:MessageCenterView)
		{
			_view = view;
		
			_background = new Sprite();
			
			createButtons();
			setupMask();
			handleBtnEvents();

			super();
            open();
		}
		
		private function setupMask():void
		{
			_mask = new Sprite();
			_mask.graphics.beginFill(0xFF0000, 1.0);
			_mask.graphics.drawRect(0,0,MessageCenterView.WIDTH, OPEN_HEIGHT);
			_mask.height = 7;
			
			_background.mask = _mask;
			addChild(_mask);
		}

		private function createButtons():void
		{
			_inBtn = new MenuButton(drawInletButton, _view.openContainer, [MessageCenterView.INLETS], _view.closeContainer, [MessageCenterView.INLETS]);
			_inBtn.toggle = true;
			_inBtn.color = 0xFF3232;
			_inBtn.labelText = "IN";
            _inBtn.labelOpacity = 0;
			
			_outBtn = new MenuButton(drawOutletButton, _view.openContainer, [MessageCenterView.OUTLETS], _view.closeContainer, [MessageCenterView.OUTLETS]);
			_outBtn.toggle = true;
			_outBtn.color = 0xEA8900;
			_outBtn.labelText = "OUT";
            _outBtn.labelOpacity = 0;
			
			_propsBtn = new MenuButton(drawPropertiesButton, _view.openContainer, [MessageCenterView.PROPERTIES], _view.closeContainer, [MessageCenterView.PROPERTIES]);
			_propsBtn.toggle = true;
			_propsBtn.color = 0x0099FF;
			_propsBtn.labelText = "PROPERTIES";
            _propsBtn.labelOpacity = 0;
			
			_inBtn.x = IN_BUTTON_X;
			_inBtn.y = MENU_BUTTON_Y-1;
			_propsBtn.x = PROPS_BUTTON_X;
			_propsBtn.y = MENU_BUTTON_Y;
			_outBtn.x = OUT_BUTTON_X;
			_outBtn.y = MENU_BUTTON_Y-1;
			
			addChild(_background);
			
			_background.addChild(_inBtn);
			_background.addChild(_propsBtn);
			_background.addChild(_outBtn);
		}
		
		
		private function drawInletButton(btn:Sprite):void
		{	
			btn.graphics.moveTo(0,2);
			btn.graphics.lineTo(23,2);
			btn.graphics.lineTo(23, 0);
			btn.graphics.curveTo(30,3,34,7); // Curve
			btn.graphics.curveTo(30,12,23,15); // Curve
			btn.graphics.lineTo(23,13);
			btn.graphics.lineTo(0,13);
			btn.graphics.curveTo(3,8,0,2);// Curve
		}
		
		private function drawPropertiesButton(btn:Sprite):void
		{	
			btn.graphics.drawRoundRect(0,0,66,14,14,14);
		}
		
		private function drawOutletButton(btn:Sprite):void
		{
			btn.graphics.moveTo(0,2);
			btn.graphics.lineTo(23,2);
			btn.graphics.lineTo(23, 0);
			btn.graphics.curveTo(30,3,34,7); // Curve
			btn.graphics.curveTo(30,12,23,15); // Curve
			btn.graphics.lineTo(23,13);
			btn.graphics.lineTo(0,13);
			btn.graphics.curveTo(3,8,0,2);// Curve	
		}
		
		/* MOUSE EVENTS */	
		
		private function handleBtnEvents():void 
		{
            _inBtn.addEventListener(MouseEvent.ROLL_OVER,btnRolloverListener);
            _propsBtn.addEventListener(MouseEvent.ROLL_OVER,btnRolloverListener);
            _outBtn.addEventListener(MouseEvent.ROLL_OVER,btnRolloverListener);
            _inBtn.addEventListener(MouseEvent.ROLL_OUT,btnRolloutListener);
            _propsBtn.addEventListener(MouseEvent.ROLL_OUT,btnRolloutListener);
            _outBtn.addEventListener(MouseEvent.ROLL_OUT,btnRolloutListener);
            _pendingTimer.addEventListener(TimerEvent.TIMER, openPendingContainer);
		}

        private function btnRolloutListener(e:MouseEvent):void {
            _pendingTimer.stop();
        }
		
		private function btnRolloverListener(e:MouseEvent):void 
		{
			var drag:WireDragDescription = Application.instance.wireDragDescription;
			
			if(drag) {
				var container:String;
				if(e.target == _propsBtn) {
					container = MessageCenterView.PROPERTIES;
				} else if(e.target == _inBtn) {
					container = MessageCenterView.INLETS;
				} else if(e.target == _outBtn) {
					container = MessageCenterView.OUTLETS;
				} else {
					return;
				}
				if(drag.containerShouldOpen(container)) {
					startPendingContainerOpen(container);
				}
			}
		}

        private function openPendingContainer(e:TimerEvent = null):void {
            _view.openContainer(_pendingContainer);
            _pendingTimer.stop();
        }

        private function startPendingContainerOpen(container:String):void {
            _pendingContainer = container;
            _pendingTimer.reset();
            _pendingTimer.start();
        }
		
		
		public function open():void
		{
			if (_open)
				return;
			
			Tweener.addTween(_mask, {height:OPEN_HEIGHT, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});
            Tweener.addTween(_inBtn, {labelOpacity:1, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});
            Tweener.addTween(_propsBtn, {labelOpacity:1, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});
            Tweener.addTween(_outBtn, {labelOpacity:1, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});
            Tweener.addTween(_inBtn, {y:MENU_BUTTON_Y, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});
            Tweener.addTween(_outBtn, {y:MENU_BUTTON_Y, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});

			_open = true;
		}
		
		public function close():void
		{
			return; //This makes the buttons always show.
            if (!_open)
				return;
			
            Tweener.addTween(_mask, {height:7, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});
            Tweener.addTween(_inBtn, {labelOpacity:0, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});
            Tweener.addTween(_propsBtn, {labelOpacity:0, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});
            Tweener.addTween(_outBtn, {labelOpacity:0, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});
            Tweener.addTween(_inBtn, {y:MENU_BUTTON_Y-1, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});
            Tweener.addTween(_outBtn, {y:MENU_BUTTON_Y-1, time:0.15, transition:"easeOutExpo", onUpdate:handleTweenUpdate});

			_open = false;
		}

        public function get isOpen():Boolean {
            return _open;
        }
		
		private function handleTweenUpdate():void
		{
			_perceivedHeight = _mask.height+2;
			_view.updateContainerPositions();
		}
		
		/**
		 * Returns the number of menu buttons that are selected 
		 * @return a number between 0 and 3.
		 * 
		 */
		public function get selectedMenuButtons():Number
		{
			var opened:Number = 0;
			
			if(_inBtn.selected) opened++;
			if(_outBtn.selected) opened++;
			if(_propsBtn.selected) opened++;
			
			//trace("selectedMenuButton, oepend="+opened);
			return opened;
		}
		
		public function get inBtn():MenuButton
		{
			return _inBtn;
		}
		
		public function get propsBtn():MenuButton
		{
			return _propsBtn;
		}
		
		public function get outBtn():MenuButton
		{
			return _outBtn;
		}
		
		public function get perceivedHeight():Number
		{
			return _perceivedHeight;
		}
	}
}