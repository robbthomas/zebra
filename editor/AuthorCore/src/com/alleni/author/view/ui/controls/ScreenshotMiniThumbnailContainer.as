/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 7/11/11
 * Time: 3:07 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.view.ui.controls {
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.controller.ui.GadgetScreenshotMediator;
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.definition.application.PublishIcons;
	import com.alleni.author.model.gadgets.GadgetScreenshot;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.service.ThumbnailLoadingPipeline;
	import com.alleni.taconite.dev.BitmapUtils;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class ScreenshotMiniThumbnailContainer extends ScreenshotThumbnailConatiner{
	    public static const OPEN_SCREENSHOT_PANEL:String = "Open Screenshot Panel";
	    public static const UPDATE_SCREENSHOTS:String = "Update Screenshots";
	
	    private const MINI_BACKGROUND:uint = 0xBFBFBF;
	    private var _publishing:Boolean;
	
	    public function ScreenshotMiniThumbnailContainer(parent:DisplayObjectContainer = null, xpos:Number = 0, ypos:Number =  0, spacing:Number = 5, 
														 gadget:Project = null, mediator:GadgetScreenshotMediator = null, publishing:Boolean = true,
														 isZapp:Boolean = false):void
		{
	        _publishing = publishing;
	        _size = 26;
	        super(parent,  xpos,  ypos,  spacing, gadget, mediator, publishing, isZapp, _size);
	        _gadget.addEventListener(UPDATE_SCREENSHOTS, updateThumbnails);
            ApplicationController.instance.addEventListener(NotificationNamesApplication.SCREENSHOT_PANEL_CLOSED, updateThumbnails);
	    }
	
	    override public function buildThumbnail(screenshot:GadgetScreenshot):Sprite{
	        var thumbnail:Sprite = new Sprite();
	        ThumbnailLoadingPipeline.push(screenshot.getImageUrl("26"), function(event:Event):void {
									var loaderInfo:LoaderInfo = LoaderInfo(event.target);
									thumbnail.addChild(buildThumbnailFromLocal((loaderInfo.loader.content as Bitmap).bitmapData))
	                                renderContainer();
								});
	        return thumbnail;
	    }
	
	    override public function buildShotFrame(width:Number, height:Number, addCamera:Boolean = true):Sprite{
	        var shotFrame:Sprite = new Sprite();
	        shotFrame.graphics.beginFill(MINI_BACKGROUND);
	        shotFrame.graphics.drawRect(0, 0, width, height);
	        shotFrame.graphics.endFill();
	
	        return shotFrame;
	    }
	
	    override public function buildThumbnailFromLocal(imageData:BitmapData):Sprite{
	        var image:Bitmap = new Bitmap(imageData);
	        image = BitmapUtils.bilinearDownscale(image, 26, 26);
	        var thumbnail:Sprite = buildShotFrame(image.width, image.height);
	        image.x = (thumbnail.width-1 - image.width)/2;
	        image.y = (thumbnail.height-1 - image.height)/2;
	        thumbnail.addChild(image);
	        return thumbnail;
	    }
	
	    override public function renderContainer():void{
	        super.renderContainer();
			var camera:Sprite = buildCamera();
			camera.y = (26 - camera.height)/2;
			_topContainer.addChildAt(camera, 0);
			camera.mouseEnabled = _publishing;
			camera.mouseChildren = _publishing;
			camera.alpha = _publishing?1:0;
	    }
	
	    private function buildCamera():Sprite{
	        var camera:Sprite = new Sprite();
	        camera.addChild(PublishIcons.screenshotIconUp);
	        addFrameListeners(camera);
	        return camera;
	    }
	
	    override protected function addThumbListeners(screenshot:GadgetScreenshot, location:String):void{
	        //(_thumbnailDictionary[screenshot.visibleIndex] as Sprite).addEventListener(MouseEvent.CLICK, screenshotClickListener(screenshot.visibleIndex, location));
	    }
	
	    override protected function addFrameListeners(frame:Sprite):void{
	        frame.addEventListener(MouseEvent.CLICK, openMainPanel, false, 0, true);
	    }
	
	    private function openMainPanel(event:MouseEvent):void{
	        dispatchEvent(new Event(OPEN_SCREENSHOT_PANEL));
	    }
	
	    override public function selectContainer(index:int):void{
	        // Nothing is needed here
	    }
	}
}
