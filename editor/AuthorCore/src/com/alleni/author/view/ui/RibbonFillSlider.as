package com.alleni.author.view.ui
{
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.ui.WireAnchor;
	
	import flash.events.MouseEvent;

	public class RibbonFillSlider extends RibbonColorSlider
	{
		private var _fillPicker:RibbonFillPicker;
		
		public function RibbonFillSlider(setValueCallback:Function, valueMin:Number, valueMax:Number, fillPicker:RibbonFillPicker)
		{
			super(setValueCallback, valueMin, valueMax, null);
			_fillPicker = fillPicker;
		}
		
		override public function setValue(value:Object, valueType:String) : void 
		{
			super.setValue(value, valueType);
			// this gets called from modelUpdate ... so it will get gradient fill-specs
			// they don't affect the knob, but must be passed on to the fillPicker ... so the fillPicker can update
			if (value is GraphicFill) {
				var fillSpec:GraphicFill = value as GraphicFill;
				if (fillSpec.type == GraphicFill.SOLID_FILL) {  // ignore gradient fills ... only respond to solid colors
					var numValue:Number = fillSpec.color;
					selectedKnob.x = convertValueToPixels(numValue); 
					startingKnob.x = selectedKnob.x;
				}
				
				_fillPicker.setFillSpec(fillSpec);  // let the fill picker update its view
			}
		}
	}
}
