package com.alleni.author.view.ui
{
	public interface IWireboardItem
	{
		function get x():Number;
		function get y():Number;
		function set x(value:Number):void;
		function set y(value:Number):void;
		function get weight():Number;
		function set weight(value:Number):void;
		function get type():String;
	}
}