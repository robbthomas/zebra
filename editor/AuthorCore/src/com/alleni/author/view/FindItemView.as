package com.alleni.author.view
{
import com.alleni.author.controller.ui.palettes.FindItemMediator;
import com.alleni.author.definition.ObjectIcons;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.view.text.LightLabel;

import flash.display.DisplayObject;
import flash.display.Sprite;

import mx.core.UIComponent;

public class FindItemView extends Sprite
	{
		private var _obj:AbstractObject;
		private var _label:LightLabel;
		private var _icon:DisplayObject;
        private var _mediator:FindItemMediator;
        private var _menuHolder:UIComponent;

		private static const ICON_X:Number = 5;
		private static const ICON_Y:Number = 2;
		
		public static const TEXT_X:Number = 30;
		private static const TEXT_Y:Number = 2;
		private static const TEXT_COLOR:Number = 0xffffff;
		private static const FILL_COLOR:Number = 0x222222;
		
		public function FindItemView(obj:AbstractObject, width:Number, height:Number, menuHolder:UIComponent)
		{
			super();
			_obj = obj;
            _menuHolder = menuHolder;
			
			graphics.beginFill(FILL_COLOR);
			graphics.drawRect(0,0, width, height);
			graphics.endFill();
			
			_icon = ObjectIcons.getIconFor(obj);
			if (_icon) {
				_icon.x = ICON_X;
				_icon.y = ICON_Y;
				addChild(_icon);
			}
			
			_label = new LightLabel(width-TEXT_X, TEXT_COLOR);
			_label.x = TEXT_X;
			_label.y = TEXT_Y;
			_label.text = obj.title;
			addChild(_label);

            _mediator = new FindItemMediator();
            _mediator.handleViewEvents(this);
		}

        public function warnViewToDelete():void
        {
            _mediator.clearViewEvents();
        }

    public function get object():AbstractObject
    {
        return _obj;
    }

    public function get menuHolder():UIComponent
    {
        return _menuHolder;
    }


	}
}