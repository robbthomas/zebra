package com.alleni.author.view.test
{
	import com.alleni.author.controller.test.TestPanelController;
	import com.alleni.author.controller.text.TextEditMediator;
import com.alleni.author.definition.DoubleClickAction;
import com.alleni.author.definition.text.FormattedText;
import com.alleni.author.event.TestEvent;
	import com.alleni.author.model.test.TestScript;
import com.alleni.author.view.text.LightEditableLabel;
import com.alleni.author.view.text.RichTextField;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	
	import mx.core.UIComponent;
	import mx.events.PropertyChangeEvent;
	
	public class TestDirEntryView extends UIComponent
	{
		private var _model:TestScript;
		private var _nameText:LightEditableLabel;
		private var _openButton:Sprite;
		
		public static const FILL_COLOR:uint = 0xcccccc;
		public static const SELECT_COLOR:uint = 0xeeaa77;
		
		public static const WIDTH:Number = TestPanelController.WIDTH_DIRECTORY;
		private static const HEIGHT:Number = 19;
		private static const NAME_X:Number = 24;
		private static const NAME_Y:Number = 2;
		
		public function TestDirEntryView(model:TestScript)
		{
			super();
			_model = model;
			_nameText = addTextField(NAME_X);
			createOpenButton();
			update();
			_model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, changeListener);
			
			addEventListener(MouseEvent.CLICK, clickListener);
			addEventListener(MouseEvent.DOUBLE_CLICK, doubleClickListener);
			doubleClickEnabled = true;
			
			_openButton.addEventListener(MouseEvent.MOUSE_DOWN, doubleClickListener);
		}
		
		private function addTextField(xx:Number):LightEditableLabel
		{
			var field:LightEditableLabel = new LightEditableLabel(100,20);
			this.addChild(field);
			field.text = " ";
			field.color = 0;
			field.size = 14;
			field.x = xx;
			field.y = NAME_Y;
            field.mediator.setCallBacks({
                    okEditFunc: function(e:MouseEvent):int{return DoubleClickAction.EDIT;},
                    updateFunc:null,
                    keyPressFunc:TextEditMediator.closeOnEnter,
                    closeFunc: closeTextEditingCallback,
                    hittestFunc:null
                });

			return field;
		}

        private function closeTextEditingCallback(value:String):void
        {
            model.name = new FormattedText(value).toString();;
        }
		
		private function changeListener(event:PropertyChangeEvent):void
		{
			update();
		}
		
		private function createOpenButton():void
		{
			_openButton = new Sprite();
			this.addChild(_openButton);
			_openButton.x = 3;
			_openButton.y = 2;
			
			var g:Graphics = _openButton.graphics;
			g.beginFill(0xa0a0a0);
			g.drawRect(0,0, 18,HEIGHT-4);
			g.endFill();
		}
		
		public function update():void
		{
			var color:uint = (_model.selected) ? SELECT_COLOR : FILL_COLOR;
			graphics.clear();
			graphics.beginFill(color);
			graphics.drawRect(0,0, WIDTH, HEIGHT);
			graphics.endFill();
			
			if (_model.status != "") {
				color = (_model.status == TestScript.PASS) ? 0x00ff00 : 0xff0000;
				graphics.beginFill(color);
				graphics.drawRect(WIDTH-20,2, 18, HEIGHT-4);
				graphics.endFill();
			}
			if (_nameText.text != _model.name)
				_nameText.text = _model.name;
		}
		
		private function clickListener(evt:MouseEvent):void
		{
			trace("click entry");
			var event:TestEvent = new TestEvent(TestEvent.ITEM_CLICKED);
			event.model = _model;
			event.shiftKey = evt.shiftKey;
			event.ctrlKey = evt.ctrlKey;
			stage.dispatchEvent(event);
		}
		
		private function doubleClickListener(evt:MouseEvent):void
		{
			trace("doubleClick target="+evt.target);
			if (evt.target == this || evt.target == _openButton) {   // select this script
				var event:TestEvent = new TestEvent(TestEvent.ITEM_CLICKED);
				event.model = _model;
				event.doubleClick = true;
				stage.dispatchEvent(event);
			}
		}
		
		private function closeEditFunction(value:String):void
		{
			value = _nameText.text;  // plain text
			trace("TestDirEntryView closeEditFunction value="+value);
			if (value != _model.name) {
				_model.name = value;
				var event:TestEvent = new TestEvent(TestEvent.ITEM_RENAMED);
				event.model = _model;
				stage.dispatchEvent(event);
			}
		}

		
		public function get model():TestScript
		{
			return _model;
		}
	}
}
