package com.alleni.author.view.test
{
	import com.alleni.author.controller.test.TestPanelController;
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.event.TestEvent;
	import com.alleni.author.model.test.TestScript;
	import com.alleni.author.model.test.TestStep;
	import com.alleni.author.view.text.RichTextField;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.core.UIComponent;
	import mx.events.PropertyChangeEvent;
	
	public class TestStepView extends UIComponent
	{
		private var _model:TestStep;
		private var _numText:RichTextField;
		private var _typeText:RichTextField;
		private var _labelText:RichTextField;
		private var _valueText:RichTextField;
		
		public static const FILL_COLOR:uint = 0xcccccc;
		public static const SELECT_COLOR:uint = 0xeeaa77;

		private static const WIDTH:Number = TestPanelController.WIDTH_SCRIPT;
		private static const HEIGHT:Number = 15;
		private static const TEXT_Y:Number = 2;

		public function TestStepView(model:TestStep)
		{
			super();
			_model = model;
			_numText = addTextField(4);
			_typeText = addTextField(24, "type");
			_labelText = addTextField(80, "label");
			_valueText = addTextField(220, "value");
			update();
			_model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, changeListener);
			addEventListener(MouseEvent.MOUSE_DOWN, clickListener);
		}
		
		private function addTextField(xx:Number, fieldName:String=null):RichTextField
		{
			var field:RichTextField = new RichTextField(100, 20);
			this.addChild(field);		
			field.text = " ";
			field.color = 0;
			field.x = xx;
			field.y = TEXT_Y;
			field.setCompositionSize(NaN, 20); // auto width
			new TextEditMediator().handleTextEditEvents(field, null,null,TextEditMediator.closeOnEnter, 
					(fieldName!=null) ? function(ignored:String):void{_model[fieldName] = field.exportText()} : null);
			return field;
		}
		
		private function changeListener(event:PropertyChangeEvent):void
		{
			update();
		}
		
		public function update():void
		{
			var color:uint = (_model.selected) ? SELECT_COLOR : FILL_COLOR;
			graphics.clear();
			graphics.beginFill(color);
			graphics.drawRect(0,0, WIDTH, HEIGHT);
			graphics.endFill();
			
			if (_model.status != "") {
				color = (_model.status == TestScript.PASS) ? 0x00ff00 : 0xff0000;
				graphics.beginFill(color);
				graphics.drawRect(WIDTH-20,2, 18, HEIGHT-4);
				graphics.endFill();
			}
			_numText.text = _model.stepNumber.toString() + ".";
			_typeText.text = _model.type;
			_labelText.text = _model.label;
			_valueText.text = _model.value;
		}
		
		private function clickListener(evt:MouseEvent):void
		{
			trace("click step");
			var event:TestEvent = new TestEvent(TestEvent.ITEM_CLICKED);
			event.model = _model;
			event.shiftKey = evt.shiftKey;
			event.ctrlKey = evt.ctrlKey;
			event.altKey = evt.altKey;
			event.stagePoint = new Point(evt.stageX, evt.stageY);
			stage.dispatchEvent(event);
		}

		
		public function get model():TestStep
		{
			return _model;
		}
	}
}