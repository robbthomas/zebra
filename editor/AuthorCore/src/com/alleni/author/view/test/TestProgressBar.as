package com.alleni.author.view.test
{
	import com.alleni.author.model.test.TestLog;
	import com.alleni.author.model.test.TestScript;
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;
	
	public class TestProgressBar extends UIComponent
	{
		private var _model:TestLog;
		
		public function TestProgressBar()
		{
			super();
		}
		
		public function set model(value:TestLog):void
		{
			_model = value;
			if (_model) {
				_model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, updateListener);
				_model.allProjStatus.addEventListener(CollectionEvent.COLLECTION_CHANGE, changeListener);
				update();
			}
		}
		
		private function updateListener(event:PropertyChangeEvent):void
		{
			if (event.property == "allProjCount")
				update();
		}

		
		private function changeListener(event:CollectionEvent):void
		{
			trace("TestProgressBar:changeListener",event.kind);
			switch (event.kind) {
				case CollectionEventKind.ADD:
				case CollectionEventKind.REMOVE:
				case CollectionEventKind.REPLACE:
				case CollectionEventKind.RESET:
					update();
					break;
			}
		}

		private function update():void
		{
			graphics.clear();
			graphics.lineStyle(1);
			graphics.beginFill(0xcccccc);
			graphics.drawRect(0,0, width, height);
			graphics.endFill();
			
			var w:Number = width / _model.allProjCount;
			for (var n:int=0; n < _model.allProjStatus.length; n++) {
				var status:String = _model.allProjStatus[n];
				var xx:Number = n * w;
				var color:uint = colorForStatus(status);
				graphics.beginFill(color);
				graphics.drawRect(xx,0, w, height);
				graphics.endFill();
			}
		}
		
		private function colorForStatus(status:String):uint
		{
			switch (status) {
				case TestScript.FAIL:	return 0xff0000;
				case TestScript.PASS:	return 0x00ff00;
                case TestScript.UNSCRIPTED:	return 0x00ff00;
                default:  return 0xff8800;  // invalid-project, bad-script, etc

			}
		}
	}
}