package com.alleni.author.view.test
{
	import com.alleni.author.controller.test.TestPanelController;
	import com.alleni.author.model.test.TestScript;
	import com.alleni.author.model.test.TestStep;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	import mx.containers.VBox;
	import mx.core.UIComponent;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;
	
	import spark.core.IViewport;
	import spark.core.NavigationUnit;
	
	public class TestScriptView extends UIComponent implements IViewport
	{
		private static const WIDTH:Number = TestPanelController.WIDTH_SCRIPT;
		private static const HEIGHT:Number = TestPanelController.HEIGHT_SCRIPT;
		public static const SPACING:Number = 18;
		
		private var _model:TestScript;
		private var _childViews:UIComponent;

		
		public function TestScriptView()
		{
			super();
			
			_childViews = new UIComponent();
			addChild(_childViews);
			
			var clipping:Sprite = new Sprite();
			clipping.graphics.beginFill(0);
			clipping.graphics.drawRect(0,0, WIDTH, HEIGHT);
			clipping.graphics.endFill();
			mask = clipping;
			addChild(clipping);
			clipping.visible = false;
		}
		
		public function set model(value:TestScript):void
		{
			_model = value;
			verticalScrollPosition = 0;

			while (_childViews.numChildren > 0)
				_childViews.removeChildAt(0);
			
			if (_model) {
				_model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, updateListener);
				_model.steps.addEventListener(CollectionEvent.COLLECTION_CHANGE, changeListener);
				
				for (var n:int=0; n < _model.steps.length; n++) {
					createChildView(n);
				}
				updateViewPositions();
			}
		}
		
		private function updateListener(event:PropertyChangeEvent):void
		{
			
		}
		
		private function changeListener(event:CollectionEvent):void
		{
			switch (event.kind) {
				case CollectionEventKind.ADD:
					createChildView(event.location);
					updateViewPositions();
					break;
				case CollectionEventKind.REMOVE:
					_childViews.removeChildAt(event.location);
					updateViewPositions();
					break;
				case CollectionEventKind.REPLACE:
					throw new Error("TestScriptView: collection change Replace");
					break;
			}
		}
		
		private function createChildView(stepNum:int):void
		{
			var newModel:TestStep = _model.steps[stepNum];
			var newView:TestStepView = new TestStepView(newModel);
			_childViews.addChildAt(newView, stepNum);
		}
		
		private function updateViewPositions():void
		{
			for (var n:int=0; n < _model.steps.length; n++) {
				var child:DisplayObject = _childViews.getChildAt(n);
				if (child is TestStepView) {
					var stepView:TestStepView = child as TestStepView;
					stepView.y = n * SPACING;
					stepView.model.stepNumber = n + 1;
				}
			}
			contentHeight = _model.steps.length * SPACING;
		}
		
		// scrolling with Spark Scroller container
		
		private var _vertScrollPos:Number = 0;
		
		[Bindable] public var contentWidth:Number = WIDTH;
		[Bindable] public var contentHeight:Number;
		[Bindable] public var horizontalScrollPosition:Number = 0;
		[Bindable] public var clipAndEnableScrolling:Boolean = true;
		
		[Bindable]
		public function get verticalScrollPosition():Number
		{
			return _vertScrollPos;
		}
		
		public function set verticalScrollPosition(value:Number):void
		{
			_vertScrollPos = value;
			_childViews.y = -value;
		}
		
		public function getVerticalScrollPositionDelta(unit:uint):Number
		{
			switch (unit) {
				case NavigationUnit.UP:           return -1;
				case NavigationUnit.DOWN:         return +1;
				case NavigationUnit.PAGE_UP:      return -(HEIGHT-SPACING);
				case NavigationUnit.PAGE_DOWN:    return +(HEIGHT-SPACING);
				case NavigationUnit.HOME:         return -verticalScrollPosition;
				case NavigationUnit.END:          return 0;
			}
			return 0;
		}
		
		public function getHorizontalScrollPositionDelta(navigationUnit:uint):Number
		{
			return 0;
		}

	}
}