package com.alleni.author.view.test
{
	import com.alleni.author.controller.test.TestPanelController;
	import com.alleni.author.model.test.TestDirectory;
	import com.alleni.author.model.test.TestScript;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	
	import mx.containers.VBox;
	import mx.core.UIComponent;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;
	
	import spark.core.IViewport;
	import spark.core.NavigationUnit;
	
	public class TestDirectoryView extends UIComponent implements IViewport
	{
		private var _model:TestDirectory;
		private var _childViews:UIComponent;
		
		private static const WIDTH:Number = TestPanelController.WIDTH_DIRECTORY;
		private static const HEIGHT:Number = TestPanelController.HEIGHT_DIRECTORY;
		private static const SPACING:Number = 22;
		
		public function TestDirectoryView()
		{
			super();
			
			_childViews = new UIComponent();
			addChild(_childViews);
			
			var clipping:Sprite = new Sprite();
			clipping.graphics.beginFill(0);
			clipping.graphics.drawRect(0,0, WIDTH, HEIGHT);
			clipping.graphics.endFill();
			mask = clipping;
			addChild(clipping);
			clipping.visible = false;
		}
		
		public function set model(value:TestDirectory):void
		{
			
			trace("TestDirectoryView: setting model="+value);
			_model = value;
			
			while (_childViews.numChildren > 0)
				_childViews.removeChildAt(0);

			if (_model) {
				_model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, updateListener);
				_model.scripts.addEventListener(CollectionEvent.COLLECTION_CHANGE, changeListener);
				for (var n:int=0; n < _model.scripts.length; n++) {
					createChildView(n);
				}
				updateViewPositions();
			}
		}
		
		private function updateListener(event:PropertyChangeEvent):void
		{
			
		}
		
		private function changeListener(event:CollectionEvent):void
		{
			trace("directory change:",event.kind);
			switch (event.kind) {
				case CollectionEventKind.ADD:
					createChildView(event.location);
					updateViewPositions();
					break;
				case CollectionEventKind.REMOVE:
					_childViews.removeChildAt(event.location);
					updateViewPositions();
					break;
				case CollectionEventKind.REFRESH:
					updateViewOrder();
					updateViewPositions();
					break;
				case CollectionEventKind.REPLACE:
					throw new Error("TestScriptView: collection change Replace");
					break;
			}
		}
		
		private function createChildView(index:int):void
		{
			var newModel:TestScript = _model.scripts[index];
			var newView:TestDirEntryView = new TestDirEntryView(newModel);
			_childViews.addChildAt(newView, index);
		}
		
		private function updateViewOrder():void
		{
			trace("updateViewOrder");
			if (_childViews.numChildren > 1) {
				// remove all child views, then add them back in order
				var views:Dictionary = new Dictionary();  // indexed by model
				for (var n:int=_childViews.numChildren-1; n >= 0; n--) {
					var child:TestDirEntryView = _childViews.getChildAt(n) as TestDirEntryView;
					views[child.model] = child;
					_childViews.removeChildAt(n);
				}
				
				for (n=0; n < _model.scripts.length; n++) {
					var scriptModel:TestScript = _model.scripts[n];
					var view:TestDirEntryView = views[scriptModel];
					_childViews.addChild(view);
				}
			}
		}
		
		private function updateViewPositions():void
		{
			trace("updateViewPositions");
			for (var n:int=0; n < _model.scripts.length; n++) {
				var child:DisplayObject = _childViews.getChildAt(n);
				if (child is TestDirEntryView) {
					var entryView:TestDirEntryView = child as TestDirEntryView;
					entryView.y = n * SPACING;
				}
			}
			contentHeight = _model.scripts.length * SPACING;
		}
		
		// scrolling with Spark Scroller container
		
		private var _vertScrollPos:Number = 0;
		
		[Bindable] public var contentWidth:Number = WIDTH;
		[Bindable] public var contentHeight:Number;
		[Bindable] public var horizontalScrollPosition:Number = 0;
		[Bindable] public var clipAndEnableScrolling:Boolean = true;
		
		[Bindable]
		public function get verticalScrollPosition():Number
		{
			return _vertScrollPos;
		}
		
		public function set verticalScrollPosition(value:Number):void
		{
			_vertScrollPos = value;
			_childViews.y = -value;
		}
		
		public function getVerticalScrollPositionDelta(unit:uint):Number
		{
			trace("getVerticalScrollPositionDelta unit="+unit);
			switch (unit) {
				case NavigationUnit.UP:           return -1;
				case NavigationUnit.DOWN:         return +1;
				case NavigationUnit.PAGE_UP:      return -(HEIGHT-SPACING);
				case NavigationUnit.PAGE_DOWN:    return +(HEIGHT-SPACING);
				case NavigationUnit.HOME:         return -verticalScrollPosition;
				case NavigationUnit.END:          return 0;
			}
			return 0;
		}
		
		public function getHorizontalScrollPositionDelta(navigationUnit:uint):Number
		{
			return 0;
		}
		

	}
}