package com.alleni.author.view.test
{
	import com.alleni.author.controller.test.TestBitmap;
	import com.alleni.author.controller.test.TestPlayer;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.test.TestScript;
	import com.alleni.author.model.test.TestStep;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldView;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class TestFeedback extends Sprite
	{
		private var _worldView:WorldView;
		
		public function TestFeedback(worldView:WorldView)
		{
			super();
			_worldView = worldView;
		}
		
		public function showFeedback(step:TestStep, obj:AbstractObject=null):void
		{
			clearFeedback();
			setupDraw(step);
			
			switch (step.type) {
				case TestScript.BITMAP_STEP:
					drawFeedbackRect(step.rect);
					break;
				case TestScript.TEXT_STEP:
				case TestScript.BUTTON_STEP:
				case TestScript.SLIDER_STEP:
					if (obj) {
						var view:ObjectView = obj.getView();
						if (view) {
							var rect:Rectangle = view.getBounds(_worldView);
							drawFeedbackRect(rect);
						}
					}
					break;
				case TestScript.CLICK_STEP:
				case TestScript.POSITION_STEP:
					drawFeedbackPoint(step.point);
					break;
				case TestScript.DRAG_STEP:
					drawFeedbackDrag(step.dragPoints);
					break;
			}
		}
		
		public function showDragFeedback(point:Point):void
		{
			// show the current point while a drag is being simulated
			clearFeedback();
			blendMode = BlendMode.INVERT;
			graphics.lineStyle(5);
			alpha = 1;
			visible = true;
			drawFeedbackPoint(point);
		}
		
		private function setupDraw(step:TestStep):void
		{
			blendMode = BlendMode.INVERT;
			if (step.isStimulus)
				graphics.lineStyle(5);
			else
				graphics.lineStyle(3);
			alpha = 1;
			visible = true;
		}
		
		public function clearFeedback():void
		{
			graphics.clear();
			while (this.numChildren > 0)
				this.removeChildAt(0);
		}

		public function drawFeedbackPoint(worldPoint:Point):void
		{
			const W:Number = 15;
			var local:Point = parent.globalToLocal(_worldView.localToGlobal(worldPoint));
			graphics.moveTo(-W,-W);
			graphics.lineTo(W,W);
			graphics.moveTo(-W,+W);
			graphics.lineTo(+W,-W);
			x = local.x;
			y = local.y;
		}
		
		private function drawFeedbackRect(worldRect:Rectangle):void
		{
			var rect:Rectangle = TestPlayer.transformRect(worldRect, _worldView, parent);
			graphics.drawRect(0,0, rect.width, rect.height);
			x = rect.x;
			y = rect.y;
		}
		
		private function drawFeedbackDrag(points:Array):void
		{
			for (var n:int=0; n < points.length; n++) {
				var point:Point = points[n];
				point = parent.globalToLocal(_worldView.localToGlobal(point));
				if (n == 0)
					graphics.moveTo(point.x, point.y);
				else
					graphics.lineTo(point.x, point.y);
			}
			x = 0;
			y = 0;
		}
		
		public function showBitmapDiff(step:TestStep, dx:int, dy:int, option:int=0, scale:int=1):void
		{
			clearFeedback();
			setupDraw(step);
			if (step.asset.content as Bitmap && step.actualBitmap) {
				var correct:Bitmap = step.asset.content as Bitmap;
				var actual:BitmapData = shiftBitmap(step.actualBitmap, dx, dy);
				
				var diff:BitmapData;
				switch (option) {
					case 1:
						diff = scaleBitmap(actual,scale);
						break;
					case 2:
						diff = scaleBitmap(correct.bitmapData,scale);
						break;
					default:
						diff = TestBitmap.bitmapDiff(correct.bitmapData, actual);
				}
				var bitmap:Bitmap = new Bitmap(diff);
				addChild(bitmap);
				
				this.blendMode = BlendMode.NORMAL;  // other feedback are inverse, this is opaque
				var point:Point = step.rect.topLeft;
				point = parent.globalToLocal(_worldView.localToGlobal(point));
				x = point.x;
				y = point.y;
			}
		}
		
		private function shiftBitmap(input:BitmapData, dx:int, dy:int):BitmapData
		{
			var data:BitmapData = new BitmapData(input.width, input.height, false);
			var matrix:Matrix = new Matrix();
			matrix.tx = dx;
			matrix.ty = dy;
			data.draw(input, matrix);
			return data;
		}
		
		private function scaleBitmap(input:BitmapData, scale:int):BitmapData
		{
			var data:BitmapData = new BitmapData(input.width*scale, input.height*scale, false);
			var matrix:Matrix = new Matrix();
			matrix.scale(scale,scale);
			data.draw(input, matrix);
			return data;
		}
	}
}