/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/30/12
 * Time: 5:47 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ComputeMessageCenterPosition;
import com.alleni.author.controller.TransitionManager;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.TransitionTypes;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.ui.Application;
import com.alleni.author.util.ViewUtils;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.objects.ArenaControlBar;
import com.alleni.author.view.objects.ArenaView;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.lang.Geometry;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Graphics;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;

import mx.collections.IList;

public class PagerView extends ObjectView {
    
    protected var _pager:Pager;
    protected var _frame:Sprite;  // the background fill & frame, so we can cloak it
    private var _fill:Shape;
    private var _border:Shape;
    private var _boundsFrame:Shape; // exact bounds, so getBounds() will be accurate
    private var _hittestFrame:Shape; // 0.01 opacity background that is always present for pointer activity
    protected var _controlBar:ArenaControlBar; // paging controls
    protected var _pages:Sprite;  // the currently visible page views: current one and the one fading in
    protected var _background:Sprite;
    protected var _foreground:Sprite;
    private var _pagesMask:Sprite;
    private var _backgroundMask:Sprite;
    private var _foregroundMask:Sprite;
    protected var _pageView:IPageView;  // view of the current page
    private var _authorHints:Sprite;

    protected var _transitions:TransitionManager;

    private static const GRIP:Number = 5;

    
    public function PagerView(context:ViewContext, model:TaconiteModel, role:uint) {
        super(context, model,  role);
        
        _pager = model.value as Pager;

        // mask for all pages and all children
        _pagesMask = new Sprite();
        addChild(_pagesMask);
        _pagesMask.visible = false;
        _backgroundMask = new Sprite();
        addChild(_backgroundMask);
        _backgroundMask.visible = false;
        _foregroundMask = new Sprite();
        addChild(_foregroundMask);
        _foregroundMask.visible = false;

        // containers for the currently visible pages
        _background = new Sprite();
        addChild(_background);
        _pages = new Sprite();   // normally only one page is visible, but during transition (Fade) two are visible
        addChild(_pages);
        _foreground = new Sprite();
        addChild(_foreground);
        updateMasks();

        // the frame is a separate Sprite so it can be Cloaked without cloaking the contents
        _frame = new Sprite();
        _fill = new Shape();
        _border = new Shape();
        _frame.addChild(_fill);
        _frame.addChild(_border);
        _boundsFrame = new Shape();
        _hittestFrame = new Shape();
        addChildAt(_frame,1);  // behind the pageView
        addChildAt(_boundsFrame,1);  // behind the pageView
        addChildAt(_hittestFrame,0); // behind everything
    }

    override public function initialize():void
    {
        super.initialize();
        if (!object.loading) {   // nested arena objects may not see loading=true, since view is created after obj is finished
            afterLoad();
        }
        ApplicationController.addEventListener(NotificationNamesApplication.RECALCULATE_ARENA_CONTROLS_FOR_FLOW_CHANGE, recalcControlsForFlowVisibilityChanged, false, 0, true);
    }

    protected function recalcControlsForFlowVisibilityChanged(event:ApplicationEvent):void
    {
        recalcControls();
    }
    
    override protected function manageViewsForList(list:String):Boolean
    {
        return (list == "pages" || list == "masterPages");  // don't make views for "objects" list
    }

    override public function warnViewToDelete():void
    {
        super.warnViewToDelete();

        deleteControlBar();

        ApplicationController.removeEventListener(NotificationNamesApplication.RECALCULATE_ARENA_CONTROLS_FOR_FLOW_CHANGE, recalcControlsForFlowVisibilityChanged);
    }

    public function getViewForModel(page:IPage):IPageView
    {
        var index:int = _pager.pages.getItemIndex(page);
        if (index >= 0) {
            return getViewForListItem("pages", index) as IPageView;
        } else {
            index = _pager.masterPages.getItemIndex(page);
            return getViewForListItem("masterPages", index) as IPageView;
        }
    }

    override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
    {
        var result:Boolean = super.updateModelProperty(property, oldValue, newValue);

        switch (property) {
            case "loading":
                if (newValue == false) { // done loading
                    afterLoad();
                }
                break;
            case "currentPage":
                changePage(_pager.pageNumber, !_pager.preventTransitionEffect);
                return true;  // handled the update
            case "editingPage":
                showEditingPage(newValue as IPage);
                var wasEditingPage:IPage = oldValue as IPage;
                if(wasEditingPage && wasEditingPage.isMaster) {
                    // editing a master will pull it from the _background or _foreground sprites
                    // so when we finish editing, those sprites need to be fixed
                    rebuildMasters();
                }
                break;
            case "ignorePageChanges":
                if (newValue == false) {
                    changePage(_pager.pageNumber);
                }
                break;
            case "visible":
            case "showAll":
                recalcControls();
                break;
            case "active":
                if(newValue) {
                    recalcFrameAlpha();
                }
                recalcControls();
                return true;
            case "cloakFrame":
                recalcFrameAlpha();
                return true;
            case "controlsVisible":
                recalcControls();
                return true;
            case "width":
            case "height":
            case "lineThickness":
            case "anchorPoint":
                handleSizeChange();
                break;
            case "rearranging":
                if (newValue == false) {
                    rebuildMasters();
                    changePage(_pager.pageNumber);
                }
                break;
            case "pageHolderVisible":
                _pages.visible = _pager.pageHolderVisible;
                _background.visible = _pager.pageHolderVisible;
                _foreground.visible = _pager.pageHolderVisible;
                break;
        }
        return result;
    }
    
    protected function handleSizeChange():void
    {
        updateMasks();
    }
         
    protected function afterLoad():void
    {
        recalcControls();
        recalcFrameAlpha();
        rebuildMasters();
        changePage(_pager.pageNumber);
    }

    override protected function updateArrayProperty(property:String, kind:String, array:IList, index:int):Boolean
    {
        var result:Boolean = super.updateArrayProperty(property, kind, array, index);
        if (!object.loading) {
            var page:IPage;
//            trace("PagerView:updateArrayProperty prop="+property, "kind="+kind, "index="+index);
            switch (property) {
                case "pages":
                    switch (kind) {
                        case ModelEvent.ADD_ARRAY_ITEM:
                            if (_pageView && _pageView.page == _pager.replacingPage) {
                                var pageView:IPageView = getViewForModel(_pager.replacementPage);
                                immediatePageChange(pageView);
                                break;
                            }
                            // drop thru
                        case ModelEvent.REMOVE_ARRAY_ITEM:
                            changePage(_pager.pageNumber);
                            break;
                    }
                    break;
                case "masterPages":
                    rebuildMasters();
                    break;
            }
        }
        return result;
    }

    override protected function createChildren():void
    {
        // OPTION ONE  (see other option below)
        // create the page view if needed
        // This occurs when file is loaded.
        if (!_pageView && _pager.pages.length>0) {
            changePage(_pager.pageNumber);
        }
    }
    
    public function pageMasterListChanged(page:IPage):void
    {
        // this is coming from a controller, so the modelChange listener has a higher priority than TaconiteView does
        // ... so the TaconiteView managed-child-views may not be up to date yet.
        // thats why updateArrayProperty must also call rebuildMasters.
        if (_pageView && _pageView.page == page) {
            updateMasters(_pageView);
        }
    }

    public function pageForegroundChanged(page:IPage):void
    {
        rebuildMasters();
    }

    private function rebuildMasters():void
    {
        removeAllViews(_background);
        removeAllViews(_foreground);
        var count:int = _pager.masterPages.length;
        for (var n:int = 0; n < count; n++) {      // always include all masters ... then updateMasters will set selected views visible
            var masterView:IPageView = getViewForListItem("masterPages", n) as IPageView;
            if (masterView.page.isForeground) {
                _foreground.addChild(masterView.objView);
            } else {
                _background.addChild(masterView.objView);
            }
        }
        if (_pageView) {
            updateMasters(_pageView);
        }
    }

    protected function updateMasters(pageView:IPageView, otherView:IPageView = null):void
    {
        // when "otherView" is supplied, only show masters used by both views (during transition those masters remain fixed in place)
        var page:IPage = pageView.page;
        var other:IPage = (otherView != null) ? otherView.page : null;
        var count:int = _pager.masterPages.length;
        for (var n:int = 0; n < count; n++) {
            var masterView:IPageView = getViewForListItem("masterPages", n) as IPageView;
            masterView.objView.visible = masterShouldShow(page, other, masterView.page);
        }
    }

    private function masterShouldShow(page:IPage, other:IPage, master:IPage):Boolean
    {
        if (_pager.editingPage && _pager.editingPage.isMaster) {
            return (_pager.editingPage == master);  // only show the master being edited
        } else if (other) {
            return (!master.moveWithTransition && page.masters.getItemIndex(master) >= 0 && other.masters.getItemIndex(master) >= 0);  // during transition only show masters in both
        } else {
            return (page.masters.getItemIndex(master) >= 0);
        }
    }

    private function removeAllViews(container:Sprite):void
    {
        for (var n:int = container.numChildren-1; n >= 0; n--) {
            container.removeChildAt(n);
        }
    }

    override public function get graphics():Graphics
    {
        return _fill.graphics;  // VectorElementView.setupCommonAttributes applies fill to this
    }

    override protected function draw() : void
    {
        // actually drawing in the _fill since we overrode get graphics, above
        graphics.lineStyle(0,0,0);
        graphics.drawRect(object.left, object.top, object.width, object.height);

        _border.graphics.clear();
        if (object.lineThickness > 0) {
            _border.graphics.lineStyle(object.lineThickness, object.lineColor, object.lineAlpha/100, false);
            _border.graphics.drawRect(object.left, object.top, object.width, object.height);
        }

        with (_boundsFrame.graphics) {
            clear();
            beginFill(0x00FFFF,0.001);
            drawRect(object.left, object.top, object.width, object.height);
            endFill();
        }

        with (_hittestFrame.graphics) {
            clear();
            beginFill(0x00FFFF,0.001);
            drawRect(object.left-GRIP, object.top-GRIP, object.width+(GRIP*2), object.height+(GRIP*2));
            endFill();
        }
    }

    protected function set borderAlpha(alpha:Number):void
    {
        _border.alpha = alpha;
    }

    protected function updateMasks():void
    {
        if (includeBackstage) {
            _background.mask = null;
            _foreground.mask = null;
            _pages.mask = null;
        } else {
            _background.mask = _backgroundMask;
            _foreground.mask = _foregroundMask;
            _pages.mask = _pagesMask;
            updateMask(_backgroundMask);
            updateMask(_foregroundMask);
            updateMask(_pagesMask);
        }
    }

    protected function updateMask(mask:Sprite):void
    {
        with (mask.graphics) {
            clear();
            beginFill(0);
            drawRect(object.left, object.top, _pager.width-_pager.lineThickness, _pager.height-_pager.lineThickness);
            endFill();
        }
        var halfThick:Number = _pager.lineThickness / 2;
        mask.x = halfThick;
        mask.y = halfThick;
    }

    private function changePage(newPageNum:int, allowTransition:Boolean=false):void
    {
//			trace("PagerView:changePage num="+_pager.pageNumber, "current="+_pager.currentPage);
        if (_pager.editingPage) {
            return;
        }
        if (_pager.ignorePageChanges) {
//            trace(" ignorePageChanges");
            return;
        }
        if (newPageNum < 1 || newPageNum > _pager.pages.length) {
            return;
        }
        var formerPageNum:int = -1;
        if (_pageView) {
            formerPageNum = _pager.pages.getItemIndex(_pageView.object);
            if (formerPageNum >= 0) {
                ++formerPageNum;  // one-based
            }
        }
        if (formerPageNum < 0) {
            allowTransition = false;
        }
        var page:IPage = _pager.pages[newPageNum-1] as IPage;
        var backward:Boolean = (newPageNum < formerPageNum);

        terminateTransition();
        var transition:String;
        var transitionSecs:Number;
        var transTweenType:String;
        if (allowTransition) {
            if(_pager.setterTransition && _pager.setterTransition != TransitionTypes.DEFAULT) {
                transition = _pager.setterTransition;
                transitionSecs = _pager.setterTransitionSecs;
                transTweenType = _pager.setterTransTweenType;

                _pager.setterTransition = null;
                _pager.setterTransitionSecs = 0;
                _pager.setterTransTweenType = null;
            } else {
                var formerPage:IPage = _pager.pages[formerPageNum-1];
                var ep:EventPage = (backward ? formerPage : page) as EventPage;
                if (ep && ep.eventTransition != TransitionTypes.DEFAULT) {
                    transition = ep.eventTransition;
                    transitionSecs = ep.transitionSecs;
                    transTweenType = ep.transTweenType;
                } else {
                    transition = _pager.transition;
                    transitionSecs = _pager.transitionSecs;
                    transTweenType = _pager.transTweenType;
                }
            }
            if (backward) {
                transition = reverseTransition(transition);
            }
        }

        var pageView:IPageView = getViewForModel(page);
        pageView.objView.visible = true; // pages always visible; masters not always
        if (pageView && pageView == _pageView)
            return; // already at the desired page

        var transSecs:Number = 0;
        if (allowTransition && transition != "none" && transitionSecs > 0 && this.stage && !_pager.bindingsDisabled){
            // clamp time to within minimum and maximum
            transSecs = Math.min(Math.max(transitionSecs, minimumTransitionSeconds), maximumTransitionSeconds);
        }
        if (transSecs > 0) {
            _pager.transientTransitionInProgress = true; // set this early to prevent page being activated by EventFlowController
            beginTransitionEffect(pageView, _pageView, transition, transSecs, transTweenType, finishTransition);
        } else {
            immediatePageChange(pageView);
        }
    }

    protected function beginTransitionEffect(pageView:IPageView, oldPageView:IPageView, transition:String, transSecs:Number, transTweenType:String, completeListener:Function):void
    {
        // override by EventFlowView and ArenaView
    }


    protected function terminateTransition(fromTransition:Boolean=false):void
    {
        // page-change, possibly while transition is in progress
        // For arena simply click the next-page during a slow transition
        // Event-pages, wire a button to goto two different events.  (it will go to both in rapid succession)
        finishTransition(fromTransition);
    }

    private function finishTransition(fromTransition:Boolean=false):void
    {
        if (!fromTransition) {
            _pager.transientTransitionInProgress = false;
        }
        if (_transitions) {
            removeTransition();
        }
        if (stage) stage.invalidate();
    }

    protected function removeTransition():void
    {
        // override by EventFlowView & ArenaView to remove listener, etc
        if (_transitions) {
            if(_pages.contains(_transitions))
                _pages.removeChild(_transitions);
            _transitions = null;
        }
    }

    private function reverseTransition(transition:String):String
    {
        switch (transition) {
            case TransitionTypes.SLIDE_LEFT_RIGHT:  return TransitionTypes.SLIDE_RIGHT_LEFT;
            case TransitionTypes.SLIDE_RIGHT_LEFT:  return TransitionTypes.SLIDE_LEFT_RIGHT;
            case TransitionTypes.SLIDE_TOP_BOTTOM:  return TransitionTypes.SLIDE_BOTTOM_TOP;
            case TransitionTypes.SLIDE_BOTTOM_TOP:  return TransitionTypes.SLIDE_TOP_BOTTOM;
        }
        return transition;
    }

    protected function immediatePageChange(pageView:IPageView, fromTransition:Boolean=false):void
    {
        if (_pageView) {
            if(_pages.contains(_pageView.objView))
            {
                _pages.removeChild(_pageView.objView);
            }
        }
        if(_transitions != null){
            removeTransition();
        }
        _pageView = pageView;
        if (!fromTransition) {
            _pager.transientTransitionInProgress = false;
        }
        _pages.addChild(_pageView.objView);
        updateMasters(_pageView);   // show only the masters for the current page
    }

    private function showEditingPage(page:IPage):void
    {
        // "page" may be a master-page
        if (page) {
            var pageView:IPageView = getViewForModel(page);  // goto the requested page, independent of pageNumber
            immediatePageChange(pageView);
        } else {
            changePage(_pager.pageNumber);  // close editing, revert to the current page
        }
    }

    protected function get minimumTransitionSeconds():Number
    {
        return 0.05;  // don't bother prepping for a transition shorter than this
    }

    protected function get maximumTransitionSeconds():Number
    {
        return int.MAX_VALUE;    // overridden by ArenaView to account for playRate
    }


    protected function get includeBackstage():Boolean
    {
        return false;
    }

    public static function captureViewWithControlbars(pageView:IPageView):BitmapData
    {
        var data:BitmapData = ViewUtils.instance.takeObjectRectScreenshot(pageView.objView);
        var controllers:Vector.<ArenaControlBar> = PagerView.getArenaControllersForPage(pageView.page);
        var matrix:Matrix = new Matrix();
        for each (var one:ArenaControlBar in controllers) {
            var arenaView:ArenaView = one.pagerView as ArenaView;
            if (arenaView.controllerShouldShowInImage) {
                var wasVisible:Boolean = one.visible;
                var wasPlusMinus:Boolean = one.showPlusMinus;
                one.visible = true;
                one.showPlusMinus = false;
                matrix.tx = one.x;
                matrix.ty = one.y;
                data.draw(one, matrix, null, null, null, true);  // smoothing=true
                one.showPlusMinus = wasPlusMinus;
                one.visible = wasVisible;
            }
        }
        return data;
    }

    public static function getArenaControllersForPage(page:IPage):Vector.<ArenaControlBar>
    {
        var list:Vector.<ArenaControlBar> = new <ArenaControlBar>[];
        var layer:DisplayObjectContainer = feedbackControlsView;
        for (var n:int = layer.numChildren-1; n >= 0; n--) {
            var arenaControlBar:ArenaControlBar = layer.getChildAt(n) as ArenaControlBar;
            if (arenaControlBar) {
                var pagerView:PagerView = arenaControlBar.pagerView;
                if (pagerView.model.isDescendant(page.object.model)) {
                    list.push(arenaControlBar);
                }
            }
        }
        return list;
    }

    public function get controllerShouldShowInImage():Boolean
    {
        if (!_pager.controlsVisible || _pager.controlsCloaked) {
            return false;
        }
        var obj:AbstractObject = _pager;
        while (obj && !(obj is EventPage)) {
            if (!obj.visible) {
                return false;
            }
            var arena:Arena = obj.parent as Arena;
            if (arena && arena.currentPage != obj) {
                return false;
            }
            obj = obj.parent;
        }
        return true;
    }

    override public function getOnStageChildrenBelow(subjectView:ObjectView=null, path:Array=null):Vector.<ObjectView> {
        var pageListName:String = null;
        var pageIndex:Number = -1;
        if(subjectView != null) {
            pageListName = path.shift();    // NOTE: EventFlowView overrides the drag-object case (subjectView != null)
            pageIndex = path.shift();
        }
        var result:Vector.<ObjectView> = new Vector.<ObjectView>();
        var i:int, pageView:IPageView, thisPageIndex:int;
        for(i = 0; i < _background.numChildren; i++) {
            pageView = _background.getChildAt(i) as IPageView;
            result.push(pageView.objView);
            if(pageListName == "masterPages") {
                thisPageIndex = _pager.masterPages.getItemIndex(pageView.object);
                if (thisPageIndex == pageIndex) {
                    return result;    // dragged object is on this page so go no further
                }
            }
        }
        if (_pageView) {
            result.push(_pageView.objView);
            if(pageListName == "pages") {
                return result;  // dragged object is on this page so go no further
            }
        }
        for(i = 0; i < _foreground.numChildren; i++) {
            pageView = _foreground.getChildAt(i) as IPageView;
            result.push(pageView.objView);
            if(pageListName == "masterPages") {
                thisPageIndex = _pager.masterPages.getItemIndex(pageView.object);
                if (thisPageIndex == pageIndex) {
                    return result;    // dragged object is on this page so go no further
                }
            }
        }
        return result;
    }

    public function recalcControls():void
    {
        const backstage:Boolean = outsideEvents;
        const backstageVis:Boolean = Application.instance.flowVisible; //Perhaps not till Application.instance.disableLoadUnload ?
        const eventVis:Boolean = !Application.instance.flowVisible;
        const parentVisible:Boolean = backstage ? backstageVis : eventVis;
        var vis:Boolean = parentVisible && _pager.controlsVisible && _pager.active && _pager.visible && Application.instance.editWhileRunningInProgress == false || _pager.showAll == true;
//        trace("recalcControls parentVisible="+parentVisible, "controls="+_pager.controlsVisible, "outsideVel="+outsideTheVellum, "loading="+_pager.loading, "active="+_pager.active, "editWhileRun="+Application.instance.editWhileRunningInProgress, "vis="+vis, _pager.title);
        if (vis && !_pager.loading && _controlBar == null) {
            createControlBar();
        }

        if (_controlBar != null) {
            _controlBar.showPlusMinus = !Application.uiRunning && !object.childOfClosed;
            if(object.containingEventPage == AuthorController.instance.eventFlow.editingPage || Application.uiRunning){
                _controlBar.visible = vis;
            } else {
                _controlBar.visible = false;
            }
            if (vis) {
                _controlBar.updatePosition();  // make sure it doesn't momentarily show at 0,0
            }
        }
    }

    private function get world():World
    {
        return Application.instance.document.root.value as World;
    }

    private function get outsideTheVellum():Boolean
    {
        // to prevent an ancestor Arena unstaging the gadget being edited, don't show arena controlbars outside a gadget, when editing a gadget
        var editor:SmartObjectEditingMediator = ApplicationController.instance.authorController.currentVelumMediator;
        if (editor) {
            var composite:Composite = editor.editingObject as Composite;
            if (composite) {
                return !_pager.model.isDescendant(composite.model);  // true if not a child of the composite
            }
        }
        return false;
    }

    private function get outsideEvents():Boolean
    {
        var m:TaconiteModel = this.model;
        while(m != null) {
            if(m.value is EventPage) {
                return false;
            }
            m = m.parent;
        }
        return true;
    }

    protected function createControlBar():void
    {
        if (_controlBar==null) {   // primary view, not the one in diagram view
            _controlBar = new ArenaControlBar(model,_role,context, this);    // created with visible=false so it won't appear at topLeft
            feedbackControlsView.addChild(_controlBar);
            if (this.stage) {
                _controlBar.updatePosition();  // sets visibility and position
            } else {
                addEventListener(Event.ADDED_TO_STAGE, addedToStageListener);
            }
        }
    }

    private function addedToStageListener(e:Event):void
    {
        removeEventListener(Event.ADDED_TO_STAGE, addedToStageListener);
        if (_controlBar) {
            _controlBar.updatePosition(true);  // force=true to force update event tho view.stage is still null
        }
    }

    private function deleteControlBar():void
    {
        if (_controlBar) {
            feedbackControlsView.removeChild(_controlBar);
            _controlBar.destroy();
            _controlBar = null;
        }
    }

    public function get controlBar():ArenaControlBar
    {
        return _controlBar;
    }

    public function computeControlBarPosition():Point
    {
        if (_pager.controlsDocked && _controlBar) {  // docked
            return computeControlDockPosition();
        } else {
            return new Point(_pager.controlsGlobalX, _pager.controlsGlobalY);
        }
    }

    private function computeControlDockPosition():Point
    {
        var rotate:Number = computeNetRotation(); // sum of rotation of all containers plus this object
        var offset:Point = ComputeMessageCenterPosition.computeFlipbookControlPosition(_pager.width, _pager.height, rotate);  // offset from topLeft of obj
        var local:Point = new Point();
        local.x = _pager.left + _pager.width/2 + offset.x;  // offset from anchor point
        local.y = _pager.top + _pager.height/2 + offset.y;
        var global:Point = this.localToGlobal(local);
        return _controlBar.parent.globalToLocal(global);
    }

    /**
     *
     * @param point
     *
     */
    public function undockControlBar(point:Point):void
    {
        _pager.controlsDocked = false;

        _pager.controlsGlobalX = point.x;
        _pager.controlsGlobalY = point.y;
    }

    public function dockControlBar():void
    {
        _pager.controlsDocked = true;
    }

    public function controlBarNearDockPosition():Boolean
    {
        const SNAP_DISTANCE:Number = 20;

        var dockPoint:Point = computeControlDockPosition();
        var barPoint:Point = new Point(_pager.controlsGlobalX, _pager.controlsGlobalY);
        return (Geometry.distanceBetweenPoints(dockPoint.x, dockPoint.y, barPoint.x, barPoint.y) < SNAP_DISTANCE);
    }

    

    protected function recalcFrameAlpha():void
    {
        // cloaking of the frame is independent of cloaking the whole pager
        // "frame" includes the background fill of the pager
        if(Application.instance.editWhileRunningInProgress == true){
            return;
        }
        if (Application.running && _pager.cloakFrame)
            _frame.alpha = CLOAKING_ALPHA;
        else
            _frame.alpha = 1.0;
    }

    override protected function get accepting():Boolean
    {
        return false;  //  will not accept dragged objects;  only the page will accept them
    }

    override public function get releasing():Boolean
    {
        return false;  // make sure we do not release our pages (only the page will release its children)
    }
    override public function getBounds(space:DisplayObject):Rectangle
    {
        // children bounds may extend beyond bounds, but are clipped so we ignore them
        return _boundsFrame.getBounds(space);
    }

    public function getBoundsForChildren(space:DisplayObject):Rectangle
    {
        var rect:Rectangle = object.localBounds;
        var thick:Number = _pager.lineThickness/2 +1;
        rect.inflate(-thick, -thick);
        return rectToSpace(rect, space);
    }

    override public function handleRunningChanged():void {
        super.handleRunningChanged();
        recalcFrameAlpha();
        recalcControls();
        var page:IPage;
        var view:IPageView;
        for each(page in _pager.pages) {
            view = page.object.getView() as IPageView;
            if (view) {
                view.objView.handleRunningChanged();
            }
        }
        for each(page in _pager.masterPages) {
            view = page.object.getView() as IPageView;
            if (view) {
                view.objView.handleRunningChanged();
            }
        }
    }

    protected function positionContent():void
    {
        var xx:Number = _pager.left;
        var yy:Number = _pager.top;
        _background.x = xx;
        _background.y = yy;
        _foreground.x = xx;
        _foreground.y = yy;
        _pages.x = xx;
        _pages.y = yy;
    }
    
    
    override public function hitTestPoint(x:Number, y:Number, shapeFlag:Boolean=false):Boolean
    {
        return _hittestFrame.getBounds(_hittestFrame.stage).contains(x,y);
    }

    override public function set makeSafeForCaching(value:Boolean):void
    {
        if (_pageView) {
            _pageView.objView.makeSafeForCaching = value;
        }
        var count:int, n:int;
        var child:ObjectView;
        count = _foreground.numChildren;
        for(n = 0; n < count; n++) {
            child = _foreground.getChildAt(n) as ObjectView;
            child.makeSafeForCaching = value;
        }
        count = _background.numChildren;
        for(n = 0; n < count; n++) {
            child = _background.getChildAt(n) as ObjectView;
            child.makeSafeForCaching = value;
        }
    }

    public function get authorHints():Sprite
    {
        if(!_authorHints)
        {
            _authorHints = new Sprite();
            this.addChild(_authorHints);
            _authorHints.x = _frame.x;
            _authorHints.y = _frame.y;
            _authorHints.mouseEnabled = false;
        }
        return _authorHints;
    }

    protected static function get feedbackControlsView():Sprite
    {
        return WorldContainer(Application.instance.viewContext).feedbackControlsView;
    }


    private function assert(condition:*):void
    {
        if (condition==null || condition==false)
            throw new Error("PagerView");
    }
       
    
    
}
}
