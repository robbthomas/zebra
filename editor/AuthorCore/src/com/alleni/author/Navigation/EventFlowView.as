/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/30/12
 * Time: 5:47 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.TransitionManager;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.EventFlowController;
import com.alleni.author.model.ui.Application;
import com.alleni.author.util.ViewUtils;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ITaconiteView;
import com.alleni.taconite.view.ViewContext;

import flash.display.Bitmap;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Point;
import flash.geom.Rectangle;

public class EventFlowView extends PagerView {

    private var _overlaySprite:Sprite;
    private var _tokenForTransitionInputDisable:Object;
    private var _titleBar:FlowControlbar;

    public static const TITLE_BAR_Y_OFFSET:Number = -70;

    public function EventFlowView(context:ViewContext, model:TaconiteModel, role:uint) {
        super(context, model, role);
    }

    override public function initialize():void
    {
        super.initialize();
        ApplicationController.addEventListener(NotificationNamesApplication.STAGE_RESIZED, handleAppResize);
        ApplicationController.addEventListener(NotificationNamesApplication.SCALE_CHANGED, handleAppResize);

    }

    override public function warnViewToDelete():void
    {
        super.warnViewToDelete();
        ApplicationController.removeEventListener(NotificationNamesApplication.STAGE_RESIZED, handleAppResize);
        ApplicationController.removeEventListener(NotificationNamesApplication.SCALE_CHANGED, handleAppResize);
    }

    private function handleAppResize(e:Event=null):void
    {
        PseudoThread.add(new PseudoThreadWaitFrameRunnable());
        PseudoThread.add(new PseudoThreadSimpleRunnable("updateTitleBar", updateTitleBar));  // keep the controlbar visible when app window shrinks
    }

    override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
    {
        if (property == "visible") {
            // ignore this because we are controlling the view visibility directly in Application.set flowVisible
            return false;
        }
        var result:Boolean = super.updateModelProperty(property, oldValue, newValue);

        switch (property) {
            case "overlayImage":
                updateOverlayImage();
                break;
            case "titleBarPage":
            case "titleBarRunning":
            case "width":
            case "notFirstPage":
            case "notLastPage":
                updateTitleBar();
                break;
            case "titleBarVisible":
                updateTitleBarVisible();
                break;
        }
        return result;
    }

    private function get eventFlow():EventFlow {
        return object as EventFlow;
    }

    override protected function afterLoad():void {
        super.afterLoad();
        updateTitleBarVisible();
    }

    override public function set visible(value:Boolean):void
    {
        super.visible = value;
        if (value) {
            updateTitleBar();
        }
    }

    public function updateTitleBarVisible():void
    {
        if (eventFlow.titleBarVisible && !_titleBar) {
            _titleBar = new FlowControlbar(eventFlow, context, true);
            addChild(_titleBar);
            updateTitleBar();
        } else if (!eventFlow.titleBarVisible && _titleBar) {
            _titleBar.warnViewToDelete();
            removeChild(_titleBar)
            _titleBar = null;
        }
    }

    private function updateTitleBar():void {
        if (_titleBar) {
            var canvas:Rectangle = EditorUI.instance.canvasVisibleRect(this);
            _titleBar.y = TITLE_BAR_Y_OFFSET;
            _titleBar.x = 0;
            _titleBar.width = eventFlow.width;
            _titleBar.page = eventFlow.titleBarPage;
            _titleBar.hiliteButton(FlowControlbar.RUN_BUTTON, eventFlow.titleBarRunning);
            _titleBar.hiliteButton(FlowControlbar.EDIT_BUTTON, !eventFlow.titleBarRunning);
            _titleBar.update();  // ensure titleBarRunning will hide/show the plus buttons
        }
    }

    public function updateTitleBarImmediately():void
    {
        // skip the wait for the render event in FlowControlBar
        _titleBar.render();
    }

    private function getCaptureBitmap(view:IPageView, width:Number,  height:Number):Bitmap
    {
        var eventPage:EventPage = view.object as EventPage;
        eventPage.updateImage();  // capture the latest state of the old page (the existing page.image will be used for the destination page)
        if (eventPage.image) {
            return new Bitmap(eventPage.image);
        } else {
            return new Bitmap(ViewUtils.instance.takeObjectRectScreenshot(view.objView));
        }
    }

    override protected function get includeBackstage():Boolean
    {
        // includeBackstage: not the global backstage, just the page children drawn outside stage bounds
        return !Application.runningLocked;
    }

    override public function hitTestPoint(x:Number, y:Number, shapeFlag:Boolean=false):Boolean
    {
        if (includeBackstage) {
            return true;
        } else {
            return super.hitTestPoint(x, y, shapeFlag);  // checks against obj bounds
        }
    }

    override public function getOnStageChildrenBelow(subjectView:ObjectView=null, path:Array=null):Vector.<ObjectView>
    {
        if (eventFlow.editingEventPage) {
            // only the page being edited is active (which could be a master)
            if (path) {
                var listName:String = path.shift();
                var pageNum:int = path.shift();
            }
            var view:EventPageView = eventFlow.editingEventPage.getView() as EventPageView;
            return new <ObjectView>[view];
        }

        if (subjectView == null) {   // not dragging an object ... hittesting for lookUnderMouse
            return super.getOnStageChildrenBelow(subjectView, path);  // removes the first entry from "path" array
        }

        // prevent dragging an object from one event to another event (or master)
        // allow object to be dragged into arena in the same event or master where the object is
        var pageListName:String = path.shift();
        var pageIndex:Number = path.shift();
        var result:Vector.<ObjectView> = new Vector.<ObjectView>();
        var pageView:ObjectView = getViewForListItem(pageListName, pageIndex) as ObjectView;
        Utilities.assert(pageView);
        result.push(pageView);
        return result;
    }

    override public function handleRunningChanged():void
    {
        super.handleRunningChanged();
    }

    private function updateOverlayImage():void
    {
        if (_overlaySprite && this.contains(_overlaySprite)) {
            removeChild(_overlaySprite);
        }
        _overlaySprite = null;
        var flow:EventFlow = _pager as EventFlow;
        var page:EventPage = flow.overlayImage as EventPage;
        if (page) {     // if the page has no preview-image, don't create an overlay
            var pageView:IPageView = getViewForModel(flow.overlayImage);
            var sprite:Sprite = getCaptureSprite(pageView, null, true);  // fill=true
            if (sprite) {
                _overlaySprite = sprite;
                addChild(sprite);
            }
        }
    }

    override protected function beginTransitionEffect(pageView:IPageView, oldPageView:IPageView, transition:String, transSecs:Number, transTweenType:String, onComplete:Function):void
    {
        _tokenForTransitionInputDisable = ApplicationController.instance.disableInput("disableInputForTransition");
        var overlayToken:Object = EventFlowController.instance.showOverlayImage(eventFlow, oldPageView.page as EventPage, "PagerView.changePage");
        immediatePageChange(pageView, true);  // put new page on stage so it can build out properly
        applyTransitionMask(_background);
        applyTransitionMask(_foreground);
        pageView.objView.visible = false;  // visible=false essentially clips content to stage rect, since overlay is showing
        hideLayer(feedbackControlsView);
        PseudoThread.add(new PseudoThreadSimpleRunnable("EventFlowView:transitionAfterLoading", function():void {
                transitionAfterLoading(pageView, oldPageView, transition, transSecs, transTweenType, overlayToken, onComplete);
            }));
    }

    private function transitionAfterLoading(pageView:IPageView, oldPageView:IPageView, transition:String, transSecs:Number, transTweenType:String, overlayToken:Object, onComplete:Function):void
    {
        terminateTransition(true); // in case a transition got started during the loading time (button wired to goto Event1 and also wired to goto Event3)
        // must be done after the next EventPage builds out, so it will show the latest values ... changed by intra-event wiring
        var lastShot:Sprite = getCaptureSprite(oldPageView, pageView, false);  // fill=false
        var nextShot:Sprite = getCaptureSprite(pageView, oldPageView, false);
        var rect:Rectangle = new Rectangle(pageView.objView.x, pageView.objView.y, _pager.width, _pager.height);
        _transitions = new TransitionManager(lastShot, nextShot, rect, transition, transSecs, transTweenType);
        _pages.addChild(_transitions);
        applyTransitionMask(_transitions);  // this is done earlier to _background & _foreground
        updateMasters(pageView, oldPageView);  // show only the masters common to both

        PseudoThread.add(new PseudoThreadWaitFrameRunnable());
        PseudoThread.add(new PseudoThreadSimpleRunnable("EventFlowView::executeTransition", executeTransition, this, [overlayToken, onComplete]));
    }

    private function executeTransition(overlayToken:Object, onComplete:Function):void
    {
        _transitions.executeTransition();
        EventFlowController.instance.removeOverlayImage(eventFlow, overlayToken);

        _transitionOnCompleteFunc = onComplete;
        _transitions.addEventListener(Event.COMPLETE, transitionCompleteListener);
    }

    private var _transitionOnCompleteFunc:Function;  // allows for removeTransition to be called early to terminate the transition before it completes

    private function transitionCompleteListener(e:Event):void
    {
        PseudoThread.add(new PseudoThreadWaitFrameRunnable());    // let animation finish before showing arena controlbars
        PseudoThread.add(new PseudoThreadSimpleRunnable("EventFlowView:executeTransition:onComplete", function():void {
            updateMasters(_pageView);   // (during transition we were showing only masters common to both pages)
            _transitionOnCompleteFunc();
            _transitionOnCompleteFunc = null;
        }));
    }

    override protected function removeTransition():void
    {
        _transitions.removeEventListener(Event.COMPLETE, transitionCompleteListener);
        _transitionOnCompleteFunc = null;
        removeTransitionMask(_transitions);
        removeTransitionMask(_background);
        removeTransitionMask(_foreground);

        super.removeTransition();

        _pageView.objView.visible = true;
        showLayer(feedbackControlsView);
        if (_tokenForTransitionInputDisable) {
            ApplicationController.instance.enableInput(_tokenForTransitionInputDisable);
            _tokenForTransitionInputDisable = null;
        }
    }

    private function applyTransitionMask(sprite:Sprite):void
    {
        var msk:Sprite = new Sprite();
        this.addChild(msk);
        msk.visible = false;
        updateMask(msk);
        sprite.mask = msk;
    }

    private function removeTransitionMask(sprite:Sprite):void
    {
        this.removeChild(sprite.mask);
        sprite.mask = null;
    }

    private function getCaptureSprite(view:IPageView, otherView:IPageView=null, fill:Boolean=false):Sprite
    {
        var width:Number = _pager.width;
        var height:Number = _pager.height;
        var contentBitmap:Bitmap = getCaptureBitmap(view, width, height);
        if (contentBitmap == null) {
            return null;    // bail out if hit unsafe view
        }
        var addedContent:Boolean = false;  // for inserting page content between foreground & background

        // sprite will include master-pages used by "view" and not used by "otherView"
        var page:IPage = view.page;
        var other:IPage = (otherView ? otherView.page : null);
        var sprite:Sprite = new Sprite();
        if (fill) {
            with (sprite.graphics) {
                beginFill(0xffffff);
                drawRect(0,0, width, height);
                endFill();
            }
        }
        var count:int = _pager.masterPages.length;
        for (var n:int = 0; n < count; n++) {
            var masterView:IPageView = getViewForListItem("masterPages", n) as IPageView;
            var master:IPage = masterView.page;
            var inPage:Boolean = (page.masters.getItemIndex(master) >= 0);
            var inOther:Boolean = (other && other.masters.getItemIndex(master) >= 0);
            if (inPage && (master.moveWithTransition || !inOther)) {
                var masterBitmap:Bitmap = getCaptureBitmap(masterView, width, height);
                if (masterBitmap == null) {
                    return null;  // bail out if hit unsafe view
                } else {
                    if (master.isForeground && !addedContent) {  // add content before the first foreground
                        sprite.addChild(contentBitmap);
                        addedContent = true;
                    }
                    sprite.addChild(masterBitmap);
                }
            }
        }
        if (!addedContent) {
            sprite.addChild(contentBitmap);
        }
        return sprite;
    }

    private function hideLayer(layer:DisplayObjectContainer):void
    {
        if (layer) {
            layer.visible = false;
        }
    }

    private function showLayer(layer:DisplayObjectContainer):void
    {
        if (layer) {
            layer.visible = true;
        }
    }



    public function get titleBarOverStage():FlowControlbar
    {
        return _titleBar;
    }

    override protected function recalcVisibility():void
    {
    }

    override public function computeMessageCenterPosition():Point
    {
        return computeDockPosition();
    }


    override public function recalcControls():void
    {
    }

    private function get isPro():Boolean {
        return Application.instance.document.project.accountTypeId >= 3;
    }

    // disable features of ObjectView
    override protected function setDragMediator():void {}
    override protected function set rolloverGlow(value:Boolean):void {}
    override protected function updateFilters():void {}
    override protected function addMessageCenter():void {}
    override public function updateViewPosition():void {}
    override protected function updateStatus():void {}
    override protected function createFeedbackView():ITaconiteView {return null}

}
}
