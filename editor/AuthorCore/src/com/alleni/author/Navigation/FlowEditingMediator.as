/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/17/12
 * Time: 9:37 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.app.ClipboardController;
import com.alleni.author.controller.objects.IClipboardMediator;
import com.alleni.author.controller.objects.IEditingMediator;
import com.alleni.author.controller.objects.IUndoMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.DragInfo;
import com.alleni.author.definition.action.ActionGroup;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.definition.action.DestroyObjectAction;
import com.alleni.author.definition.action.ModifyObjectProperty;
import com.alleni.author.definition.action.PageNumberInfo;
import com.alleni.author.definition.action.PageOrderAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.document.ObjectSelection;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;

import mx.events.PropertyChangeEvent;

public class FlowEditingMediator implements IEditingMediator, IUndoMediator, IClipboardMediator {

    protected var _context:ViewContext;
    private var _flow:EventFlow;
    private var _controller:PagerController;
    private var _view:FlowView;
    private var _authorController:AuthorController;

    private static const NEAR_SCROLLBAR:Number = 60;

    public function FlowEditingMediator(context:ViewContext)
    {
        _context = context;
        _authorController = ApplicationController.instance.authorController;
    }

    public function handleViewEvents(view:FlowView):void
    {
        _view = view;
        _flow = view.sourceObj as EventFlow;
        _controller = _flow.controller as PagerController;
        _flow.addEventListener(PageAddedEvent.ADD_PAGE, handlePageAdded);
        view.scrollbar.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleScrollChange);
        ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_INITIALIZED, afterProjInit);
        ApplicationController.addEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED, updateButtonsForRunningState);
        view.scrollbar.completedChangeCallback = commitPageChangeForUndo;
        view.scrollbar.dragStartCallback = afterDragScrollbar;

        _authorController.startingEditing(this);
    }

    public function clearViewEvents() : void
    {
        _flow.removeEventListener(PageAddedEvent.ADD_PAGE, handlePageAdded);
        _view.scrollbar.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleScrollChange);
        ApplicationController.removeEventListener(NotificationNamesApplication.PROJECT_INITIALIZED, afterProjInit);
        ApplicationController.removeEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED, updateButtonsForRunningState);
        _authorController.closingEditing(this,false);
    }

    public function addListenersToItem(view:EventImageView):void
    {
        view.addEventListener(MouseEvent.DOUBLE_CLICK, doubleClickItem);
        view.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownOnItem);
    }

    private function afterProjInit(e:Event):void
    {
        _view.updateMastersInEventImages();
        updateControlBars();
    }

    public function handleVisibilityChange(visible:Boolean):void
    {
        if (visible) {
            clearButtonHilites();  // clear the Run/EditResume buttons when return to flow
        }
    }

    private function handlePageAdded(event:PageAddedEvent):void
    {
        if (_view.visible) {
            _view.tweenPageAdded(event.newPage as EventPage, event.existingPage as EventPage, event.after, event.inside);
        }
    }

    private function clearButtonHilites():void
    {
        for each (var bar:FlowControlbar in [_view.leftControlbar, _view.rightControlbar]) {
            if (bar) {
                bar.hiliteButton(FlowControlbar.RUN_BUTTON, false);
                bar.hiliteButton(FlowControlbar.EDIT_BUTTON, false);
            }
        }
    }

    private function updateControlBars():void
    {
        for each (var bar:FlowControlbar in [_view.leftControlbar, _view.rightControlbar, _flow.eventPagerView.titleBarOverStage]) {
            if (bar) {
                bar.update();
            }
        }
    }

    private function updateButtonsForRunningState(e:Event=null):void
    {
        _flow.titleBarRunning = Application.uiRunning;
    }

    public function afterDragScrollbar():void
    {
        if (!_authorController.selection.empty) {
            _authorController.clearSelection();
        }
    }

    public function handleScrollChange(event:PropertyChangeEvent):void
    {
        switch (event.property) {
            case "leftPageNumber":
                _flow.leftPageNumber = event.newValue as int;
                break;
            case "rightPageNumber":
                _flow.rightPageNumber = event.newValue as int;
                break;
        }
    }

    public function handleLeftPageChange(pageIndex:int):void
    {
        _view.scrollbar.itemCount = _view.itemCount;  // leftPageNumber setter will limit value based on itemCount
        _view.scrollbar.leftPageNumber = pageIndex;
        var pg:IPage = _flow.getPageForIndex(pageIndex);
        _view.leftControlbar.page = _flow.getPageForIndex(pageIndex);
    }

    public function handleRightPageChange(pageIndex:int):void
    {
        _view.scrollbar.itemCount = _view.itemCount;  // setter will limit value based on itemCount
        _view.scrollbar.rightPageNumber = pageIndex;
        _view.rightControlbar.page = _flow.getPageForIndex(pageIndex);
    }

    private function commitPageChangeForUndo(oldLeft:int, oldRight:int):void
    {
        var left:int = _flow.leftPageNumber;
        var right:int = _flow.rightPageNumber;
//        trace("commitPageChangeForUndo oldLeft="+oldLeft, "oldRight="+oldRight, "left="+left, "right="+right);
        if (left != oldLeft || right != oldRight) {
            var group:ActionGroup = new ActionGroup("Change page");
            var leftAction:ModifyObjectProperty = ModifyObjectProperty.fromObject(_flow, "x left", "leftPageNumber", -1, oldLeft);
            var rightAction:ModifyObjectProperty = ModifyObjectProperty.fromObject(_flow, "x right", "rightPageNumber", -1, oldRight);
            group.add(leftAction);
            group.add(rightAction);
            actionTree.commit(group);
        }
    }

    private function doubleClickItem(event:MouseEvent):void
    {
        var view:EventImageView = event.target as EventImageView;
        if (view) {
            _authorController.editEvent(view.page as EventPage, true);
        }
    }

    private static const THUMB_SCALE:Number = 0.2;
    private static const BREAK_DIST:Number = 3;

    private var _draggingThumb:Boolean;
    private var _itemClicked:EventImageView;
    private var _thumb:DisplayObject;
    private var _itemClickPoint:Point;
    private var _brokeAway:Boolean;
    private var _insPageNum:int;

    public function mouseDownOnItem(event:MouseEvent):void
    {
        var view:EventImageView = event.target as EventImageView;
        if (view && !_flow.doingEditTween) {
            var page:EventPage = view.page as EventPage;
//            trace("FlowEditingMediator mouseDownOnItem page="+page, "master="+page.isMaster);
            event.stopImmediatePropagation();
            _itemClicked = view;
            _itemClickPoint = new Point(event.stageX, event.stageY);
            _brokeAway = false;
            _context.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveItem);
            _context.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpItem);
        }
    }

    public function mouseMoveItem(event:MouseEvent):void
    {
        if (_flow.doingEditTween) {
            return;
        }
        var point:Point = new Point(event.stageX, event.stageY);
        if (_brokeAway || Point.distance(point, _itemClickPoint) > BREAK_DIST) {

            if (lastRemainingPage(_itemClicked.page)) {
                messageForCannotReorder(_itemClicked.page.isMaster);
                cleanupDragItem();
                return;
            }

            _brokeAway = true;
            _draggingThumb = true;
            if (!_thumb) {
                _thumb = _itemClicked.makeThumbnail(THUMB_SCALE);
                if (_thumb == null) {
                    cleanupDragItem();
                    return;
                }
                _context.addChild(_thumb);
            }
            setThumbPosition(event.stageX, event.stageY);
            moveInsertionMark(new Point(event.stageX, event.stageY));
        }
    }

    private function moveInsertionMark(global:Point):void
    {
        var scroll:FlowScrollbar = _view.scrollbar;
        var local:Point = scroll.globalToLocal(global);
        if (scroll.mouseY > -NEAR_SCROLLBAR) {  // mouse is near the scrollbar
            _insPageNum = scroll.pageNumberFromPosition(local.x);
            _insPageNum = legalIndexForPageReorder(_itemClicked.page, _insPageNum);
            local.x = scroll.positionFromPageNumber(_insPageNum);
            local.y = 0;
            var global:Point = scroll.localToGlobal(local);
            _view.placeInsertMark(global);

            // its legal for _insPageNum to be equal to the total count, when moving Event to end
            var maxIndex:int = _flow.masterPages.length + _flow.pages.length -1;
            var page:IPage = _flow.getPageForIndex(Math.min(_insPageNum, maxIndex));
            _view.scrollToRevealPage(page);
        } else {
            _view.placeInsertMark(null);  // mouse is too far above the scrollbar, so hide marker
        }
    }

    public function mouseUpItem(event:MouseEvent):void
    {
        if (!_flow.doingEditTween) {
            if (!_brokeAway) {
                _view.scrollToRevealPage(_itemClicked.page, true);
                _authorController.clearSelection();
            } else if (_view.scrollbar.mouseY < -NEAR_SCROLLBAR) {  // mouse too far above the scrollbar
                var point:Point = new Point(event.stageX, event.stageY);
                if (Point.distance(point, _itemClickPoint) > 50) {
                    message("Drag to scrollbar to reorder events.");
                }
            }
        }
        cleanupDragItem();
    }

    private function cleanupDragItem():void
    {
        _context.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveItem);
        _context.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpItem);
        if (_thumb) {
            _context.removeChild(_thumb);
            _thumb = null;
        }

        if (_view.insertMarkVisible) {
            setPageIndex(_itemClicked.page, _insPageNum);
            _view.placeInsertMark(null);
        }
        _itemClicked = null;
        _draggingThumb = false;
    }

    private function setThumbPosition(stageX:Number, stageY:Number):void
    {
        var point:Point = _thumb.parent.globalToLocal(new Point(stageX, stageY));
        _thumb.x = point.x - _thumb.width/2;
        _thumb.y = point.y - _thumb.height/2;
    }

    private function legalIndexForPageReorder(page:IPage, index:int):int
    {
        var masterCount:int = _flow.masterPages.length;
        var backCount:int = masterCount - foregroundMasterCount;
        if (page.isMaster) {
            if (page.isForeground) {
                return Math.min(Math.max(index, backCount), masterCount);  // stay to right of backgrounds, left of Events
            } else {
                return Math.min(Math.max(index, 0), backCount);
            }
        } else {
            return Math.max(index, masterCount);  // at least masterCount
        }
    }

    private function get foregroundMasterCount():int
    {
        var count:int = 0;
        for each (var master:IPage in _flow.masterPages) {
            if (master.isForeground) {
                ++count;
            }
        }
        return count;
    }

    private function messageForCannotReorder(isMaster:Boolean):void
    {
        if (isMaster) {
            message("Can't reorder since there is only one master.")
        } else {
            message("Can't reorder since there is only one event.")
        }
    }

    private function setPageIndex(page:IPage, flowIndex:int):void
    {
        var index:int;
        var current:int;
        var pageType:String;
        if (page.isMaster) {
            index =  flowIndex;
            current = _flow.masterPages.getItemIndex(page);
            pageType = "masters";
        } else {
            index =  flowIndex - _flow.masterPages.length;
            current = _flow.pages.getItemIndex(page);
            pageType = "eventss";
        }
        Utilities.assert(current >= 0);
        if (index > current) {
            --index;  // account for the vacancy left by removing from current location
        }

        var pageNum:int = _flow.pageNumber;  // currently not changing pageNumber
        var flowIndex:int = index;
        if (!page.isMaster) {
            flowIndex += _flow.masterPages.length;
        }
        var rightPageNum:int = Math.min(flowIndex+1, _flow.masterPages.length + _flow.pages.length -1);
        var leftPageNum:int = rightPageNum - 1;
        var newPageNums:PageNumberInfo = new PageNumberInfo(pageNum, _flow.initialPageNumber, leftPageNum, rightPageNum);

        _authorController.log("reorderPage number="+(index+1) + " was=" + (current+1), new <TaconiteModel>[page.object.model]);
        var action:PageOrderAction = PageOrderAction.fromObject(page, "Reorder "+pageType, index, newPageNums);
        action.perform();  // sets pager.reordering=true during the change
        actionTree.commit(action);
    }

    public static function lastRemainingPage(page:IPage):Boolean
    {
        var pager:Pager = page.object.parent as Pager;
        if (page.isMaster) {
            return (pager.masterPages.length == 1);
        } else {
            return (pager.pageCount == 1);
        }
    }

    private function message(str:String):void
    {
        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, [str]));
    }


//  IEditingMediator:
//      function getSelection(getGlobal:Boolean, strict:Boolean=true, omitLocked:Boolean=true):Vector.<DragInfo>;
//		function cancelEditing():Boolean;
//		function deleteEditing():Boolean;
//		function selectAll(includeLocked:Boolean):void;
//		function makeObject():Boolean;
//		function switchedObjects(oldObj:ITaconiteObject, newObj:ITaconiteObject):void;
//		function get editingObject():ITaconiteObject;
//		function get actionTree():ActionTree;
//		function get hasActiveState():Boolean;
//      function get MENULABEL_TYPE():String;
//		function activeMediatorChanged(editor:IEditingMediator):void;

    public function getSelection(getGlobal:Boolean, strict:Boolean=true, omitLocked:Boolean=true):Vector.<DragInfo> {
        var selection:ObjectSelection = _authorController.selection as ObjectSelection;

        if(selection != null) {
            return DragInfo.fromModelList(_flow, selection.selectedModels, getGlobal, strict, omitLocked);
        } else {
            return new Vector.<DragInfo>();
        }
    }

    public function deleteEditing():Boolean {
        var info:Vector.<DragInfo> = getSelection(false);  // omitLocked=true
        if(info.length > 0) {
            var models:Vector.<TaconiteModel> = DragInfo.toModels(info);
            ApplicationController.instance.authorController.log("delete", models);
            var action:DestroyObjectAction = DestroyObjectAction.fromSelection(models);
            action.perform();
            ApplicationController.currentActionTree.commit(action);
        }
        return true;
    }

    public function selectAll(includeLocked:Boolean):void
    {
        var models:Vector.<TaconiteModel> = _flow.modelsForSelectAll(includeLocked);
        ApplicationController.instance.authorController.selectModels(models);
    }

    public function get hasActiveState():Boolean
    {
        return true;
    }

    public function activeMediatorChanged(activeMediator:IEditingMediator):void
    {
        if (activeMediator is SmartObjectEditingMediator) {  // otherwise if opened path editing, keep showing the vellum
            var thisIsActive:Boolean = (activeMediator == this);
            if (thisIsActive)
                activate();
            else
                deactivate();
        }
    }

    private function activate():void
    {
    }

    private function deactivate():void
    {
    }

    public function cancelEditing():Boolean
    {
        return false;
    }

    public function makeObject():Boolean
    {
        return true;
    }

    public function switchedObjects(oldObj:ITaconiteObject, newObj:ITaconiteObject):void
    {
    }

    public function get editingObject():ITaconiteObject
    {
        return _flow;
    }

    public function get actionTree():ActionTree
    {
        return _flow.actionTree;
    }

    public function get MENULABEL_TYPE():String{
        return "FLOW";
    }

    public function canUndo():Boolean {
        return _flow.actionTree.canUndo;
    }

    public function canRedo():Boolean {
        return _flow.actionTree.canRedo;
    }

    public function undo():void {
        _authorController.log("undo " + _flow.actionTree.undoName);
        _flow.actionTree.undo();
    }

    public function redo():void {
        _authorController.log("redo " + _flow.actionTree.redoName);
        _flow.actionTree.redo();
    }

    public function undoName():String {
        return _flow.actionTree.undoName;
    }

    public function redoName():String {
        return _flow.actionTree.redoName;
    }

    public function cut(e:Event):void {
        ClipboardController.instance.cutClipboard(e);
    }

    public function copy(e:Event):void {
        ClipboardController.instance.copyClipboard(e);
    }

    public function paste(e:Event):void {
        ClipboardController.instance.pasteClipboard(e);
    }



}
}
