/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 9/26/12
 * Time: 8:28 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.view.text.LightLabel;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.TaconiteView;
import com.alleni.taconite.view.ViewContext;

import flash.display.Graphics;

import flashx.textLayout.formats.TextAlign;

import mx.collections.IList;

public class EventCacheTileView extends TaconiteView {

    private var _page:EventPage;
    private var _label:LightLabel = new LightLabel(WIDTH, 0xffffff);

    private static const WIDTH:Number = 20;
    private static const HEIGHT:Number = 20;

    public function EventCacheTileView(context:ViewContext, model:TaconiteModel) {
        super(context, model);
        _page = model.value as EventPage;
        addChild(_label);
        _label.textAlign = TextAlign.CENTER;
        _label.y = 3;
    }


    override protected function updateArrayProperty(property:String, modelEventKind:String, array:IList, index:int):Boolean {
        return true; // no updates needed
    }

    override protected function updateModelProperty(property:Object, oldValue:Object,  newValue:Object):Boolean {
        switch (property) {
            case "hasInternals":  // for hasInternals, request update by returning false
            case "pageNumber":
                return false;
        }
        return true; // no update
    }


    override protected function updateView():void {
        var g:Graphics = this.graphics;
        g.clear();
        g.beginFill(0x555555);
        g.drawRect(0,0, WIDTH, HEIGHT);
        g.endFill();
        if (_page.hasInternals) {
            var inset:Number = 0;
            g.beginFill(0x00bb00);
            g.drawRect(inset,inset, WIDTH-2*inset, HEIGHT-2*inset);
            g.endFill();
        }
        _label.text = computePageNumberText();
    }

    private function computePageNumberText():String
    {
        var num:int = _page.pageNumber;
        if (_page.isMaster) {
            const CAPITAL_A:int = 65;
            return String.fromCharCode(CAPITAL_A + num -1);
        } else {
            return num.toString();
        }
    }

    override public function get width():Number {
        return WIDTH;
    }

    override public function set width(value:Number):void {
        // ignore
    }

    override public function get height():Number {
        return HEIGHT;
    }

    override public function set height(value:Number):void {
        // ignore
    }

}
}
