/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/30/12
 * Time: 3:08 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.CompositeSaveController;
import com.alleni.author.controller.ui.CompositeSaveControllerDefaultContext;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.OutletDescription;
import com.alleni.author.definition.application.TransitionTypes;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ObjectInlets;
import com.alleni.author.model.ObjectOutlets;
import com.alleni.author.model.ObjectProperties;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.InternalSmartWireAnchor;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.persistence.GadgetData;
import com.alleni.author.persistence.GadgetDecoder;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.persistence.GadgetUtils;
import com.alleni.author.service.assets.AssetLoadOperation;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadDebugRunnable;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.factory.TaconiteFactory;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Point;
import flash.utils.getTimer;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectUtil;

public class EventPage extends SmartObject implements IPage {

    [Transient] public static const TACONITE_CHILDREN_PROPERTY:String = "objects";
    [Transient] public static const TACONITE_COLLECTIONS:Vector.<String> = new <String>["masters"];
    [Transient] public static const ALPHABET:Array = ["A","B","C","D","E","F","G","H","I","J","K","L"];

    // WARNING:  when adding a property, it may need to be in EventPageController.propertyNeedsInit()
    // (since this class doesn't register property ribbons)

    [Bindable] public var masters:IList = new ArrayCollection();

    // only for masters
    [Bindable] public var isForeground:Boolean;
    [Bindable] public var moveWithTransition:Boolean;

    [Bindable] public var resetOnEnter:Boolean = false;

    [Bindable] public var eventTransition:String = TransitionTypes.DEFAULT;  // use the Project transition
    [Bindable] public var transTweenType:String = "linear";
    [Bindable] public var transitionSecs:Number = 0;
    [Bindable] [Transient] public var currentlyEditing:Boolean = false;

    private var _pageNumber:Number;


    private var _hasInternals:Boolean = false;
    [Transient] public var cachedGadgetData:GadgetData = null;
    [Transient] public var cachedSelection:Vector.<Array> = new Vector.<Array>();
    [Transient] public var preventUnload:Boolean = false;
    [Transient] public var unsavedChanges:Boolean = false;

    [Transient] [Bindable] public var needMessageCenter:Boolean;
    [Transient] public var dockMessageCenterToStage:Boolean = !Application.instance.flowVisible;  // prevent flash of MC at wrong place when adding pages at stage
    [Transient] [Bindable] public var image:BitmapData;

    // childObjUsageCounts is loaded from the root gadget
    // (childObjUsageCounts is not in initialValues and is not in Project.content)
    [Transient] [Bindable] public var childObjUsageCounts:UsageCounts = null;  // asset/gadget usage in child objects


    public function EventPage() {
        super(false);  // don't register properties, so MC starts empty
        this.anchorPoint = new Point(0, 0);
    }

    public function registerPageRibbons():void
    {
        if(anchors["moveWithTransition"] != null || anchors["goThisPage"] != null) {
            return;
        }
        ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("currentlyEditing"), "currentlyEditing");
        if (isMaster) {
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("moveWithTransition"), "moveWithTransition");
        } else {
            ObjectInlets.instance.registerInlet(this, new InletDescription("goThisPage","","Go- this event", "Go to this event", Modifiers.DO, 1));

            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("resetOnEnter"), "resetOnEnter");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("eventTransition"), "eventTransition");  // similar to "transition" ribbon, but fewer choices
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("transTweenType"), "transTweenType");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("transitionSecs"), "transitionSecs");

            ObjectOutlets.instance.registerOutlet(this, new OutletDescription("onEnter", "", "On- entry", "Beginning to run event after loading resources", Modifiers.STATES));
            ObjectOutlets.instance.registerOutlet(this, new OutletDescription("onExit", "", "On- exit", "Exiting event", Modifiers.STATES));
        }

        ObjectInlets.instance.registerInlet(this,new InletDescription("resetObject", 	"1-4-00",	"Reset properties", "Reset to initial values the properties\nof this "+(isMaster ? "master" : "event")+" and its objects", Modifiers.DO, 34));
    }

    public function needsInternals():Boolean {
        var flow:EventFlow = parent as EventFlow;
        if(!flow.disableLoadUnload) {
            var editing:EventPage = flow.editingPage as EventPage;
            if(editing) {
                 // if any is editing only the editing one is needed
                return (editing == this || editing.masters.getItemIndex(this) >= 0);
            } else {
                // if the flow is not visible the current page and it's masters are needed
                var cur:EventPage = flow.currentPage as EventPage;
                return (cur == this || cur.masters.getItemIndex(this) >= 0);
            }
        }
//        trace("needsInternals default return="+_hasInternals, title);
        return _hasInternals;  // when at Flow, keep page in memory if its already in memory
    }

    public function loadInternals():void {
        var block:PseudoThreadDebugRunnable = new PseudoThreadDebugRunnable("EventPage::loadInternals [" + title + "]", true);
        PseudoThread.add(block);
        try {
            if (gadget == null) {
                hasInternals = true;  // stateToJsonWithoutRefs needs this when encoding gadget next time its edited
                PseudoThread.add(new PseudoThreadDebugRunnable("EventPage::loadInternals null gadget"));
                return;
            }
            if(_hasInternals || cachedGadgetData == null) {
                PseudoThread.add(new PseudoThreadDebugRunnable("EventPage::loadInternals loading already finished"));
                return;
            }
            if(this.loading) {
                PseudoThread.add(new PseudoThreadDebugRunnable("EventPage::loadInternals loading already in progress"));
                return;
            }
            this.loading = true;
            var thisEvent:EventPage = this;
            var assetHoldToken:Object;
            PseudoThread.add(new PseudoThreadWaitFrameRunnable()); // wait for previous Unload to finish
            PseudoThread.add(new PseudoThreadSimpleRunnable("EventPage::loadInternals Body [" + title + "]", function():void{
                trace(">>> Loading " + title);
                if(_hasInternals || cachedGadgetData == null) {
                    trace("Loading already finished somehow");
                    return;
                }
                thisEvent.loading = true;
                assetHoldToken = AssetLoadOperation.hold("EventPage::LoadInternals [" + title + "]");
                GadgetDecoder.buildOutComposite(thisEvent, gadget, cachedGadgetData, false, true, function():void{
                    thisEvent.loading = false;
                    thisEvent.resumePage();
                });
            }));
            PseudoThread.add(new PseudoThreadSimpleRunnable("EventPage::loadInternals unhold assets [" + title + "]", function():void{
                AssetLoadOperation.unhold(assetHoldToken);
                assetHoldToken = null;
            }));
        } finally {
            PseudoThread.add(block.end);
        }
    }

    public function resumePage():void
    {
        if (_hasInternals) {
            if (this.resetOnEnter) {
                resetObject(ApplicationController.instance.authorController.world);
            } else {
                if(Application.running) {
                    this.onResume();
                }
            }
            for each(var anchor:InternalSmartWireAnchor in this.innerAnchors) {
                if(anchor.other.valueNewerThanInside) {
                    anchor.other.trigger(); // simulate a change happening on the external side
                }
            }
            if(!isMaster){
                WireAnchor(anchors["onEnter"]).trigger();
            }
        }
    }

    public function unloadInternals(refreshImage:Boolean=true):Boolean {
        if(!_hasInternals || preventUnload) {
            return false;
        }
        var thisEvent:EventPage = this;
        trace(">>> Unloading " + title);
        thisEvent.suspendPage(refreshImage);
        saveResumeData();
        saveEditWhileRunChanges();
        thisEvent.loading = true;
        ApplicationController.instance.wireController.bindingsEnabled = false;
        for each(var wire:Wire in ApplicationController.instance.wireController.getWiresInScope(thisEvent)) {
            ApplicationController.instance.wireController.deleteWire(wire);
        }
        while(children.length > 0) {
            var obj:AbstractObject = children.getItemAt(children.length-1) as AbstractObject;
            removeChildAndDescendants(obj);
        }
        hasInternals = false;
        ApplicationController.instance.wireController.bindingsEnabled = true;
        thisEvent.loading = false;
        return true;
    }

    private function removeChildAndDescendants(obj:AbstractObject):void
    {
        AuthorController.instance.handleObjectRemoval(obj);
        for each (var ao:AbstractObject in obj.allObjects()) {
            if (ao && ao.controller) {
                ao.controller.warnBeforeDelete(ao);
            }
        }
        model.removeValueChildAndDescendants(obj);
    }

    public function suspendPage(refreshImage:Boolean=true):void
    {
        if (_hasInternals) {
            if (refreshImage) {
                updateImage();
            }
            if(Application.running) {
                this.onPause();
            }
        }
    }

    public function saveResumeData():void
    {
        var values:Object = GadgetEncoder.getValues(this);
        cachedGadgetData = new GadgetData(parent, values.initialValues, null, true, values.currentValues, GadgetDecoder.emptyObjectPropStorages(null), false);
    }


    [Transient]
    [Bindable]
    override public function get gadget():Project
    {
        return super.gadget;
    }

    override public function set gadget(newGadget:Project):void
    {
        if(this.gadget && gadget.previewImage) {
            gadget.previewImage.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, assetPropertyChangeHandler);
        }

        super.gadget = newGadget;

        // listen to gadget.previewImage load-completion when loading project.  After that, the most current image is page.image.
        if(this.loading && newGadget && newGadget.previewImage) {
            if (!setImageFromGadget()) {
                newGadget.previewImage.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, assetPropertyChangeHandler, false, 0, true);
            }
        }
    }

    private function assetPropertyChangeHandler(event:PropertyChangeEvent):void {
        switch(event.property) {
            case "content":
                setImageFromGadget();
                gadget.previewImage.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, assetPropertyChangeHandler);
                break;
        }
    }

    private function setImageFromGadget():Boolean
    {
        if (gadget.previewImage) {
            var bitmap:Bitmap = gadget.previewImage.content as Bitmap;
            if (bitmap) {
                image = bitmap.bitmapData;
                return true;
            }
        }
        return false;  // got nothing
    }

    public function getEventPageView():EventPageView
    {
        var flow:EventFlow = this.parent as EventFlow;
        return flow.eventPagerView.getViewForModel(this) as EventPageView;
    }

    public function updateImage(force:Boolean=false):void
    {
        // checking for "active" prevents getting a capture that misses the arena controlbars
        if ((_hasInternals || force) && this.active) {
            var shouldHide:Boolean = !Application.running;
            if (shouldHide) {
                var hiddenViews:Vector.<ObjectView> = hideHiddenObjects();
            }
            var pageView:EventPageView = getEventPageView();
            this.image = PagerView.captureViewWithControlbars(pageView);
            if (shouldHide) {
                restoreHiddenObjects(hiddenViews);
            }
        }
    }

    public function updateImageForResizeProject():void
    {
        if (_hasInternals) {
            var shouldHide:Boolean = !Application.running;
            if (shouldHide) {
                var hiddenViews:Vector.<ObjectView> = hideHiddenObjects();
            }
            var pageView:EventPageView = getEventPageView();
            this.image = PagerView.captureViewWithControlbars(pageView);
            if (shouldHide) {
                restoreHiddenObjects(hiddenViews);
            }
        }
    }

    private function hideHiddenObjects():Vector.<ObjectView>
    {
        var result:Vector.<ObjectView> = new <ObjectView>[];
        var list:Vector.<AbstractObject> = this.allObjects();
        for each (var obj:AbstractObject in list) {
            if (obj != this && (obj.hideOnRun || obj.cloakOnRun)) {
                var view:ObjectView = obj.getView();
                if (view && view.visible) {
                    view.visible = false;
                    result.push(view);
                }
            }
        }
        return result;
    }

    private function restoreHiddenObjects(views:Vector.<ObjectView>):void
    {
        for each (var view:ObjectView in views) {
            view.visible = true;
        }
    }

    public function createImageAsset():Asset
    {
        var asset:Asset = null;
        if (this.image) {
            asset = new Asset(null);
            asset.content = new Bitmap(image);
            asset.name = title;
        }
        return asset;
    }

    public function logEditWhileRunChanges():void
    {
        unsavedChanges = true;
        childObjUsageCounts = null;
    }

    public function saveEditWhileRunChanges():void
    {
        if (unsavedChanges && !editing) {
            if (objects.length > 0) {
                // will replace this object and the new one will have unsavedChanges=false
                new CompositeSaveController(new CompositeSaveControllerDefaultContext()).saveEditWhileRunChanges(this);
            }
        }
    }

    [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
    public function get pageNumber():int
    {
        return _pageNumber;
    }

    public function set pageNumber(value:int):void
    {
        var old:int = _pageNumber;
        if (value != old) {
            _pageNumber = value;
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "pageNumber", old, value));
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "title", "", title));
        }
    }

    private var _hadChildren:Boolean;

    [Bindable]
    public function get hadChildren():Boolean
    {
        return _hadChildren || gadget != null;  // gadget is null until after first edit, then Event is considered non-empty
    }

    public function set hadChildren(value:Boolean):void
    {
        _hadChildren = value;
    }

    [Transient]
    public function get isMaster():Boolean
    {
        var pager:Pager = parent as Pager;
        if (pager) {
            return (pager.masterPages.getItemIndex(this) >= 0);
        } else {
            return false;
        }
    }

    override public function get width():Number
    {
        return (parent != null) ? parent.width : 0;
    }

    override public function get height():Number
    {
        return (parent != null) ? parent.height : 0;
    }


    [Transient]
    public function get object():AbstractContainer
    {
        return this as AbstractContainer;
    }

    override public function get title():String
    {
        if (titleChangedByAuthor) {
             return super.title;
        } else if (isMaster) {
            var pager:Pager = parent as Pager;
            var index:int = pager.masterPages.getItemIndex(this);
            return "Master "+ALPHABET[index];
        } else {
            return "Event " + pageNumber.toString();
        }
    }

    // Event pages cannot be reordered or reparented at runtime
    override public function resetParent(world:World):void{}
    override public function resetZIndex():void{}

    // can always accept an object dragged outof arena onto the Event
    override public function get accepting():Boolean { return true; }
    override public function get releasing():Boolean { return true; }


    override public function toString():String
    {
        return "[" + shortClassName + " \"" + title+"\" gadget="+(gadget?gadget.projectId:"null") + " page="+pageNumber + " image="+(image!=null) + " active="+active + "]";
    }


    [Transient]
    [Bindable]
    public function get hasInternals():Boolean {
        return _hasInternals;
    }

    public function set hasInternals(value:Boolean):void {
        _hasInternals = value;
    }

    override protected function initializeValues():void {

        super.initializeValues();
        if(cachedGadgetData != null) {
            cachedGadgetData.currentValues = ObjectUtil.copy(cachedGadgetData.initialValues);
        }
        if(gadget != null && hasInternals == false) {
            setImageFromGadget();
        }
    }

    override public function updateProject(newProject:Project):void
    {
        super.updateProject(newProject);
        this.unsavedChanges = false;
        childObjUsageCounts = null; // trigger setupUsageCounts to update the counts
        setupUsageCounts();
    }


    public function setupUsageCounts():void
    {
        // usually childObjUsageCounts is loaded from the root Project
        // (childObjUsageCounts is not in initialValues and is not in Project.content)
        // but when a gadget is dragged from library or dock, it doesn't have current values
        // Also, undo/redo can end up with an empty usageCounts due to a reference to a gadget with null id.

        if (childObjUsageCounts == null && _hasInternals) {
            childObjUsageCounts = new UsageCounts();
            countChildUsages(childObjUsageCounts);
        }

        // copy/paste might create undesired inits...
        if ("childObjUsageCounts" in initialValues) {
            delete initialValues["childObjUsageCounts"];
        }
    }

    override public function getUsageCounts(usages:UsageCounts):void
    {
        setupUsageCounts();

        // calling super because SmartObject will countChildUsages directly
        super.getUsageCountsInSettersAndTriggers(usages);

        if (childObjUsageCounts && !editing) {  // use the cache
            usages.addFromList(this.childObjUsageCounts);
        } else {
            countChildUsages(usages);
        }

        // event gadget doesn't show in library, but count is relied upon
        usages.countGadget(gadget);
    }

    public function validate():String
    {
        var flow:EventFlow = this.parent as EventFlow;
        if (preventUnload) {
            return null;  // don't validate a page that is being replaced (gadget is obsolete)
        }

        if (objects.length > 0 && cachedGadgetData != null) {
            return "cachedGadgetData exists with children.";
        }

        var error:String;
        error = GadgetUtils.validateLiveObjects(this.allObjectsInWiringScope(), this.gadget);
        if (error != null) {
            return error;
        }
        return null;
    }


    override public function debugContent(properties:String = null):Object
    {
        var result:Object = {};
        if (gadget && gadget.content) {
            result.gadget = GadgetUtils.createDebugValuesContent(gadget.content);
        }
        if (cachedGadgetData && cachedGadgetData.initialValues) {
            result.initialValues = GadgetUtils.createDebugValuesContent(cachedGadgetData.initialValues, properties);
        }
        if (cachedGadgetData && cachedGadgetData.currentValues) {
            result.currentValues = GadgetUtils.createDebugValuesContent(cachedGadgetData.currentValues, properties);
        }
        if (objects.length > 0) {
            result.objects = super.debugContent(properties);
        }
        return result;
    }

    private function assert(condition:*):void
    {
        if (condition==null || condition==false) {
            throw new Error("EventPage");
        }
    }

}
}
