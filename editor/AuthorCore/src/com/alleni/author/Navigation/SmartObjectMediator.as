/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/31/12
 * Time: 4:09 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.ObjectFactory;
import com.alleni.author.controller.objects.CompositeVelumMediator;
import com.alleni.author.controller.objects.IEditingMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.DoubleClickAction;
import com.alleni.author.event.WireEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.RectangleObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.view.objects.ArenaControlBar;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.event.SelectEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.events.MouseEvent;

import mx.events.PropertyChangeEvent;

public class SmartObjectMediator {
    
    protected var _context:ViewContext;
    private var _view:SmartObjectView;
    private var _smart:SmartObject;
    private var _model:TaconiteModel;

    
    public function SmartObjectMediator(context:ViewContext, view:SmartObjectView) {
        _context = context;
        _view = view;
        _model = view.model;
        _smart = _model.value as SmartObject;

        _view.addEventListener(MouseEvent.DOUBLE_CLICK, doubleClickListener);
    }
				
    public function clearViewEvents() : void
    {
        _view.removeEventListener(MouseEvent.DOUBLE_CLICK, doubleClickListener);
    }


    public function closeEditing():void
    {

    }

    public static function editorToKeepOpen(objToClose:AbstractContainer):IEditingMediator
    {
        var container:AbstractContainer = objToClose;
        while (container) {
            if (container.editing && container is SmartObject) {
                var editor:IEditingMediator = ApplicationController.instance.authorController.editingMediatorForObject(container);
                if (editor) {  // topmost ancestor being edited: close editors down to this one
                    return editor;
                }
            }
            container = container.wireScopeContainer as SmartObject;  // filter out World
        }
        return null;  // keep nothing open
    }


    private function doubleClickListener(event:MouseEvent):void
    {
        switch (_view.doubleClickAction) {
            case DoubleClickAction.EDIT:
                handleDoubleClick(event);
                break;
            case DoubleClickAction.STOP:
                event.stopImmediatePropagation();
                break;
        }
    }

    protected function handleDoubleClick(event:MouseEvent):void
    {
    }

    public function openEditingThis():void
    {

    }

}
}
