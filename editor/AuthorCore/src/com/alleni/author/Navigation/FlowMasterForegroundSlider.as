/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 5/28/12
 * Time: 3:20 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.ui.TooltipMediator;
import com.alleni.author.definition.FlowTooltips;
import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
import com.alleni.taconite.view.ViewContext;

import flash.display.Shape;

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Point;

public class FlowMasterForegroundSlider extends Sprite {

    private var _page:IPage;
    private var _context:ViewContext;
    private var _thumb:Sprite;
    private var _thumbNormalGraphic:Shape;
    private var _thumbOverGraphic:Shape;
    private var _thumbDownGraphic:Shape;
    private var _draggingThumb:Boolean;
    private var _mouseOver:Boolean;
    private var _oldValue:Boolean;
    private var _breakaway:Boolean;
    private var _changeCallback:Function; // called on mouseUp if there was a change
    private var _tooltipMediator:TooltipMediator = new TooltipMediator();

    public static const WIDTH:Number = 18;
    public static const HEIGHT:Number = 25;
    public static const VERT_INSET:Number = 8;
    public static const TOP_Y:Number = VERT_INSET;
    public static const BOTTOM_Y:Number = HEIGHT - VERT_INSET;
    public static const THUMB_WIDTH:Number = 11;
    public static const THUMB_HEIGHT:Number = 7;
    public static const MAX_DELTA_FOR_TOGGLE:Number = 3;

    public function FlowMasterForegroundSlider(context:ViewContext, changeCallback:Function)
    {
        super();
        _context = context;
        _changeCallback = changeCallback;

        _thumb = new Sprite();
        addChild(_thumb);
        _thumb.x = WIDTH/2;
        _thumbNormalGraphic = createThumbNormal();     _thumb.addChild(_thumbNormalGraphic);
        _thumbOverGraphic = createThumbOver();         _thumb.addChild(_thumbOverGraphic);
        _thumbDownGraphic = createThumbDown();         _thumb.addChild(_thumbDownGraphic);
        _thumb.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
        _thumb.addEventListener(MouseEvent.ROLL_OVER, rollover);
        _thumb.addEventListener(MouseEvent.ROLL_OUT, rollout);

        drawTrack();
        update();
    }

    public function update():void
    {
        if (!_draggingThumb && _page) {
            _thumb.y = (_page.isForeground) ? TOP_Y : BOTTOM_Y;

            if (_page.isForeground) {
                _tooltipMediator.handleTooltip(this,FlowTooltips.FOREGROUND_MASTER,new Point(WIDTH/2,0),ControlAreaToolTip.DISPLAY_ABOVE,true);
            } else {
                _tooltipMediator.handleTooltip(this,FlowTooltips.BACKGROUND_MASTER,new Point(WIDTH/2,0),ControlAreaToolTip.DISPLAY_ABOVE,true);
            }
        }
        _thumbDownGraphic.visible = _draggingThumb;
        _thumbOverGraphic.visible = _mouseOver && !_draggingThumb;
        _thumbNormalGraphic.visible = !_mouseOver && !_draggingThumb;
    }


    private function rollover(event:MouseEvent):void
    {
        _mouseOver = true;
        update();
    }

    private function rollout(event:MouseEvent):void
    {
        _mouseOver = false;
        update();
    }

    private var _click:Point;
    private var _thumbStart:Point;

    private function mouseDown(event:MouseEvent):void
    {
        _oldValue = _page.isForeground;
        _click = new Point(event.stageX, event.stageY);
        _thumbStart = _thumb.localToGlobal(new Point(0,0));
        _draggingThumb = true;
        _breakaway = false;
        update();
        stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
        stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
    }

    private function mouseMove(event:MouseEvent):void
    {
        var global:Point = new Point(event.stageX, event.stageY);
        var delta:Point = global.subtract(_click);
        global = _thumbStart.add(delta);
        var local:Point = this.globalToLocal(global);
        _thumb.y = Math.min(Math.max(local.y, TOP_Y), BOTTOM_Y);
        if (delta.length > MAX_DELTA_FOR_TOGGLE) {
            _breakaway = true;
        }

        _page.isForeground = _thumb.y < (HEIGHT/2);
    }

    private function mouseUp(event:MouseEvent):void
    {
        if (stage) {
            stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
            stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
            _draggingThumb = false;
            _click = null;
            _thumbStart = null;

            // toggle if clicked on the control without moving mouse much
            if (_page.isForeground == _oldValue && !_breakaway) {
                _page.isForeground = !_page.isForeground;
            }

            if (_changeCallback != null && _page.isForeground != _oldValue) {
                _changeCallback();
            }
        }
        update();
    }

    public function set page(value:IPage):void
    {
        _page = value;
    }

    public function get page():IPage {
        return _page;
    }

    private function drawTrack():void
    {
        with (this.graphics) {
            lineStyle(1, 0xa3a3a3);
            moveTo(WIDTH/2, TOP_Y);
            lineTo(WIDTH/2, BOTTOM_Y)
        }
    }

    private function createThumbNormal():Shape
    {
        var th:Shape = new Shape();
        with (th.graphics) {
            beginFill(0xcccccc);
            drawRect(-THUMB_WIDTH/2, -THUMB_HEIGHT/2, THUMB_WIDTH, THUMB_HEIGHT);
            endFill();
        }
        return th;
    }

    private function createThumbOver():Shape
    {
        var th:Shape = new Shape();
        with (th.graphics) {
            beginFill(0x95c2f2);
            drawRect(-THUMB_WIDTH/2, -THUMB_HEIGHT/2, THUMB_WIDTH, THUMB_HEIGHT);
            endFill();
        }
        return th;
    }

    private function createThumbDown():Shape
    {
        var th:Shape = new Shape();
        with (th.graphics) {
            lineStyle(1, 0xffffff);
            beginFill(0x95c2f2);
            drawRect(-THUMB_WIDTH/2, -THUMB_HEIGHT/2, THUMB_WIDTH, THUMB_HEIGHT);
            endFill();
        }
        return th;
    }

}
}
