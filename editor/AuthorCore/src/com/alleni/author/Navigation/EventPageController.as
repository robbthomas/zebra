/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/30/12
 * Time: 5:50 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;

import flash.events.Event;

import mx.collections.IList;

public class EventPageController extends SmartObjectController {

    public static var instance:EventPageController = new EventPageController();

    public function EventPageController() {
        super();
    }

    override protected function handleModelCollectionUpdate(model:TaconiteModel, property:String, collection:IList, kind:String, index:int, item:Object, oldItem:Object):void
    {
        super.handleModelCollectionUpdate(model, property, collection, kind, index, item, oldItem);
        var page:EventPage = getObject(model) as EventPage;
        if (page && !page.loading) {
            switch (property) {
                case "masters":
                    switch (kind) {
                        case ModelEvent.ADD_ARRAY_ITEM:
                        case ModelEvent.REMOVE_ARRAY_ITEM:
                            pageMasterListChanged(page);   // do this even when pager.rearranging is true
                            break;
                    }
                    break;
            }
        }
    }

    override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
    {
        super.handleModelUpdate(model,property,newValue,oldValue,parent);
        var page:EventPage = getObject(model) as EventPage;
        if (page) {  // page itself is changing
            switch (property) {
                case "loading":
                    if (newValue == false) {
                        pageMasterListChanged(page);
                    }
                    break;
                case "isForeground":
                    pageForegroundChanged(page);
                    break;
                case "needMessageCenter":    // WireController also copies value from needMessageCenter to messageCenterVisible
                    page.messageCenterVisible = newValue && (ApplicationController.instance.wireController.getWiringLevel() > 0);
                    break;
            }
        }
    }
        
    override protected function activate(model:TaconiteModel):void
    {
        super.activate(model);

        var page:EventPage = model.value as EventPage;
        if (page != null && page.resetOnEnter) {
            page.resetObject(ApplicationController.instance.authorController.world);
        }
    }

    private function pageMasterListChanged(page:EventPage):void
    {
        var flow:EventFlow = page.object.parent as EventFlow;
        if (flow) {
            flow.eventPagerView.pageMasterListChanged(page);
            if (flow.flowView) {
                flow.flowView.pageMasterListChanged(page);
            }
        }
    }

    private function pageForegroundChanged(page:EventPage):void
    {
        var flow:EventFlow = page.object.parent as EventFlow;
        if (flow) {
            flow.eventPagerView.pageForegroundChanged(page);
            if (flow.flowView) {
                flow.flowView.pageForegroundChanged(page);
            }
        }
    }


    override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void {
        var pager:Pager = EventPage(object).parent as Pager;

        switch (modifier.key)
        {
            case "goStartPage":
                pager.pageNumber = 1;
                return;
            case "goNextPage":
                pager.pageNumber = Math.min(pager.pageNumber+1, pager.pageCount);
                return;
            case "goPrevPage":
                pager.pageNumber = Math.max(pager.pageNumber-1, 1);
                return;
            case "goEndPage":
                pager.pageNumber = pager.pageCount;
                return;
            case "goThisPage":
                var index:int = pager.pages.getItemIndex(object);
                if (index >= 0) {
                    pager.pageNumber = index+1;
                }
                return;
        }
        super.handleTrigger(object, modifier);
    }

    // since EventPage doesn't register any properties, we need to explicitly ask for inits
    private static var _needsInit:Vector.<String> = new <String>[
            "masters", "isMaster", "pageNumber", "viewBox", "x", "y", "visible", "title", "isForeground"
        ];

    override public function propertyNeedsInit(property:String, obj:AbstractObject):Boolean
    {
        if (super.propertyNeedsInit(property, obj)) {
            return true;
        } else {
            return (_needsInit.indexOf(property) >= 0);
        }
    }

}
}
