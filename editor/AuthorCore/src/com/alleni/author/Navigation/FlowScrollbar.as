/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/21/12
 * Time: 8:28 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import caurina.transitions.Tweener;

import com.alleni.author.controller.ui.TooltipRegistrar;
import com.alleni.author.definition.FlowTooltips;
import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;

import com.alleni.taconite.dev.Utilities;

import flash.display.GradientType;
import flash.display.Graphics;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Matrix;
import flash.geom.Point;

import mx.events.PropertyChangeEvent;

public class FlowScrollbar extends Sprite {

    private var _itemCount:int;
    private var _masterCount:int;
    private var _leftPageNumber:int = 0;
    private var _rightPageNumber:int = 1;

    private var _track:Sprite;
    private var _tickMarks:Shape;
    private var _leftSlider:ScrollSlider;
    private var _rightSlider:ScrollSlider;
    private var _tieBar:ScrollSlider;

    private var _width:Number = 100;
    private var _slidersTied:Boolean = false;
    public var _draggingAnySlider:Boolean;

    private var _dragStartCallback:Function;
    private var _completedChangeCallback:Function;
    private var _oldLeftNumber:int;
    private var _oldRightNumber:int;

    private var _tooltipRegistrar:TooltipRegistrar = new TooltipRegistrar();

    private static const TRACK_HEIGHT:int = 12;
    private static const TRACK_TICK_INSET:Number = 0;

    private static const SLIDER_HEIGHT:int = 12;
    private static const BUBBLE_CENTER_Y:Number = 12;

    private static const ANIMATE_SLIDER_MOVE:String = "easeOutExpo";
    private static const TIME_SLIDER_MOVE:Number = 0.25;


    public function FlowScrollbar() {

        _track = new Sprite();
        addChild(_track);
        _tickMarks = new Shape();
        _track.addChild(_tickMarks);
        updateTrack();
        _leftSlider = new ScrollSlider(this, internalDragStartCallback, dragLeftCallback, dragEndCallback);
        _leftSlider.y = TRACK_HEIGHT;
        _leftSlider.numberBubble = new NumberBubble(BUBBLE_CENTER_Y);
        addChild(_leftSlider);
        _rightSlider = new ScrollSlider(this, internalDragStartCallback, dragRightCallback, dragEndCallback);
        _rightSlider.y = _leftSlider.y;
        _rightSlider.numberBubble = new NumberBubble(BUBBLE_CENTER_Y);
        addChild(_rightSlider);
        _tieBar = new ScrollSlider(this, internalDragStartCallback, dragTieCallback, dragEndCallback);
        _tieBar.y = -SLIDER_HEIGHT;
        addChild(_tieBar);

        // hit-test sliders carefully, since label text can overlap
        _leftSlider.addEventListener(MouseEvent.MOUSE_DOWN, sliderMouseDown);
        _rightSlider.addEventListener(MouseEvent.MOUSE_DOWN, sliderMouseDown);
        _tieBar.addEventListener(MouseEvent.MOUSE_DOWN, sliderMouseDown);

        updateForPageCount();
    }

    override public function set width(value:Number):void
    {
        _width = value;
        updateTrack();
        updateForPageCount();
    }

    private function updateForPageCount():void
    {
        if (_itemCount > 2) {
            updateTrack();
            updateTickmarks();
            updateSlidersForPageCount();
            updateForPageNumbers();
        }

        _tooltipRegistrar.registerTooltip(_leftSlider,FlowTooltips.LEFT_STACK_SLIDER,new Point(_leftSlider.width/2-1,TRACK_HEIGHT+NumberBubble.RADIUS*2+3),ControlAreaToolTip.DISPLAY_BELOW,true);
        _tooltipRegistrar.registerTooltip(_rightSlider,FlowTooltips.RIGHT_STACK_SLIDER,new Point(_rightSlider.width/2-1,TRACK_HEIGHT+NumberBubble.RADIUS*2+3),ControlAreaToolTip.DISPLAY_BELOW,true);
        _tooltipRegistrar.registerTooltip(_tieBar,FlowTooltips.EVENT_FLOW_SLIDER,new Point(_tieBar.width/2-1,0),ControlAreaToolTip.DISPLAY_ABOVE,true);
    }

    private function updateForPageNumbers(tween:Boolean=false):void
    {
        _leftSlider.numberBubble.text = computePageNumberText(leftPageNumber);
        _leftSlider.numberBubble.isMaster = leftPageNumber < _masterCount;
        _rightSlider.numberBubble.text = computePageNumberText(rightPageNumber);
        _rightSlider.numberBubble.isMaster = rightPageNumber < _masterCount;

        if (!_draggingAnySlider) {
            var left:Number = positionFromPageNumber(leftPageNumber);
            var right:Number = positionFromPageNumber(rightPageNumber);
            if (tween) {
                Tweener.addTween(_leftSlider, {x:left, transition:ANIMATE_SLIDER_MOVE, time:TIME_SLIDER_MOVE, onComplete:tweenFinished});
                Tweener.addTween(_rightSlider, {x:right, transition:ANIMATE_SLIDER_MOVE, time:TIME_SLIDER_MOVE});
                Tweener.addTween(_tieBar, {x:left, transition:ANIMATE_SLIDER_MOVE, time:TIME_SLIDER_MOVE});
            } else {
                _leftSlider.x = left;
                _rightSlider.x = right;
                slidersTied = slidersTouching;
                _tieBar.x = left;
            }
        }
    }

    private function tweenFinished():void
    {
        slidersTied = slidersTouching;
    }

    private function computePageNumberText(index:int):String
    {
        if (index >= _masterCount) {
            index = index + 1 - _masterCount;
            return index.toString();
        } else {
            const CAPITAL_A:int = 65;
            return String.fromCharCode(CAPITAL_A + index);
        }
    }

    private function updateSlidersForPageCount():void
    {
        _leftSlider.normalGraphic = createLeftSliderGraphic();
        _leftSlider.numberBubble.x = Math.min(sectionWidth/2, sectionWidth-NumberBubble.RADIUS);

        _rightSlider.normalGraphic = createRightSliderGraphic();
        _rightSlider.numberBubble.x = Math.max(sectionWidth/2, NumberBubble.RADIUS);

        _tieBar.normalGraphic = createTieGraphic();
    }

    private function get leftmostSliderX():Number
    {
        return 0;
    }

    private function get sectionWidth():Number
    {
        return trackExtent / _itemCount;
    }

    private function get trackExtent():Number
    {
        return _width;
    }

    public function pageNumberFromPosition(xx:Number):int
    {
        // zero-based number result, but _itemCount is a legal value indicating move-to-end
        var num:int = Math.round((xx - leftmostSliderX) * _itemCount / trackExtent);
        num = Math.max(Math.min(num, _itemCount), 0);
        return num;
    }

    public function positionFromPageNumber(num:int):Number
    {
        return leftmostSliderX + num * trackExtent / _itemCount;
    }

    public function get slidersTouching():Boolean
    {
        return (_leftSlider.x + sectionWidth +1 >= _rightSlider.x);
    }

    private function updateTrack():void
    {
        if (_itemCount > 0) {  // lack of pages gives an infinite sectionWidth
            with (_track.graphics) {
                clear();
                var xx:Number = _masterCount * sectionWidth;
                beginFill(0x333333);
                drawRect(0,0, xx, TRACK_HEIGHT);
                endFill();

                lineStyle(1, 0xffffff);
                moveTo(0,0);
                lineTo(_width, 0);
                moveTo(0, TRACK_HEIGHT);
                lineTo(_width, TRACK_HEIGHT);
            }
        }
    }

    private function updateTickmarks():void
    {
        var spacing:Number = sectionWidth;
        _tickMarks.graphics.clear();
        _tickMarks.graphics.lineStyle(1, 0xffffff);
        var xx:Number = leftmostSliderX;
        for (var n:int = 0; n <= _itemCount-2; n++) {
            xx += spacing;
            _tickMarks.graphics.moveTo(xx, TRACK_TICK_INSET);
            _tickMarks.graphics.lineTo(xx, TRACK_HEIGHT-TRACK_TICK_INSET);
        }
    }

    private function createLeftSliderGraphic():Sprite
    {
        var sprite:Sprite = new Sprite();
        var w:Number = sectionWidth;
        var h:Number = SLIDER_HEIGHT;
        const C:Number = 8;  // C = length of curve
        with (sprite.graphics) {
            clear();
            lineStyle(1, 0xffffff);
            beginSliderFill(sprite.graphics, w);
            moveTo(0,0);
            lineTo(0, h-C);
            curveTo(0, h, C, h);
            lineTo(w, h);
            lineTo(w, 0);
            lineTo(0, 0);
            endFill();
        }
        return sprite;
    }

    private function createRightSliderGraphic():Sprite
    {
        var sprite:Sprite = new Sprite();
        var w:Number = sectionWidth;
        var h:Number = SLIDER_HEIGHT;
        const C:Number = 8;  // C = length of curve
        with (sprite.graphics) {
            clear();
            lineStyle(1, 0xffffff);
            beginSliderFill(sprite.graphics, w);
            moveTo(0,0);
            lineTo(0, h);
            lineTo(w-C, h);
            curveTo(w, h, w, h-C);
            lineTo(w, 0);
            lineTo(0, 0);
            endFill();
        }
        return sprite;
    }

    private function createTieGraphic():Sprite
    {
        var sprite:Sprite = new Sprite();
        var w:Number = sectionWidth*2;
        var h:Number = SLIDER_HEIGHT;
        const C:Number = 8;  // C = length of curve
        with (sprite.graphics) {
            clear();
            lineStyle(1, 0xffffff);
            beginTieFill(sprite.graphics, w);
            moveTo(0, h);
            lineTo(w, h);
            lineTo(w, C);
            curveTo(w, 0, w-C, 0);
            lineTo(C, 0);
            curveTo(0, 0, 0, C);
            lineTo(0, h);
            endFill();
        }
        return sprite;
    }

    private function beginSliderFill(g:Graphics, ww:Number):void
    {
        var gradColors:Array = [0x525252, 0x929292, 0x525252];
        var gradAlphas:Array = [1,1,1];
        var gradRatios:Array = [0,127,255];
        var gradMatrix:Matrix = new Matrix();
        gradMatrix.createGradientBox(ww, SLIDER_HEIGHT, 0);  // angle=0
        g.beginGradientFill(GradientType.LINEAR,gradColors,gradAlphas,gradRatios,gradMatrix);
    }

    private function beginTieFill(g:Graphics, ww:Number):void
    {
        var gradColors:Array = [0x525252, 0x929292, 0x525252, 0x929292, 0x525252];
        var gradAlphas:Array = [1,1,1,1,1];
        var gradRatios:Array = [0,63,127,190,255];
        var gradMatrix:Matrix = new Matrix();
        gradMatrix.createGradientBox(ww, SLIDER_HEIGHT, 0);  // angle=0
        g.beginGradientFill(GradientType.LINEAR,gradColors,gradAlphas,gradRatios,gradMatrix);
    }


    private function dragLeftCallback(global:Point):void
    {
        var local:Point = this.globalToLocal(global);
        if (local.x < _leftSlider.x) {
            slidersTied = false;
        }
        setLeftPosition(local.x);
        slidersTied = slidersTouching;
        if (slidersTied){
            setRightPosition(_leftSlider.x + sectionWidth);
        }
    }

    private function dragRightCallback(global:Point):void
    {
        var local:Point = this.globalToLocal(global);
        setRightPosition(local.x);
    }

    private function dragTieCallback(global:Point):void
    {
        var local:Point = this.globalToLocal(global);
        setLeftPosition(local.x);
        setRightPosition(_leftSlider.x + sectionWidth);
    }

    private function internalDragStartCallback():void
    {
        // remove tweens applied by dragEndCallback()
        Tweener.removeTweens(_leftSlider);
        Tweener.removeTweens(_rightSlider);
        Tweener.removeTweens(_tieBar);

        _oldLeftNumber = _leftPageNumber;
        _oldRightNumber = _rightPageNumber;

        if (_dragStartCallback) {
            _dragStartCallback();
        }
    }

    private function dragEndCallback():void
    {
        updateForPageNumbers(true);  // tween=true
        if (_completedChangeCallback) {
            _completedChangeCallback(_oldLeftNumber, _oldRightNumber);
        }
    }

    public function setLeftPosition(value:Number):void
    {
        var min:Number = leftmostSliderX;
        var max:Number = min + trackExtent - 2*sectionWidth;
        var xx:Number = Math.max(Math.min(value, max), min);
        _leftSlider.x = xx;
        _tieBar.x = xx;
        leftPageNumber = pageNumberFromPosition(xx);
    }

    private function setRightPosition(value:Number, allowPushOther:Boolean = true):void
    {
        if (value > _rightSlider.x) {
            slidersTied = false;
        }
        var min:Number = (allowPushOther ? leftmostSliderX : _leftSlider.x) + sectionWidth;
        var max:Number = leftmostSliderX + trackExtent - sectionWidth;
        var xx:Number = Math.max(Math.min(value, max), min);
        _rightSlider.x = xx;
        rightPageNumber = pageNumberFromPosition(xx);

        slidersTied = slidersTouching;
        if (slidersTied){
            setLeftPosition(_rightSlider.x - sectionWidth)
        }
    }

    [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
    public function get leftPageNumber():int
    {
        return _leftPageNumber;
    }

    public function set leftPageNumber(value:int):void
    {
        value = Math.max(Math.min(value, _itemCount-1), 0);
        if (value != _leftPageNumber) {
            var oldValue:int = _leftPageNumber;
            _leftPageNumber = value;
            updateForPageNumbers();
            this.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "leftPageNumber", oldValue, value));
        }
    }

    [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
    public function get rightPageNumber():int
    {
        return _rightPageNumber;
    }

    public function set rightPageNumber(value:int):void
    {
        value = Math.max(Math.min(value, _itemCount-1), 1);
        if (value != _rightPageNumber) {
            var oldValue:int = _rightPageNumber;
            _rightPageNumber = value;
            updateForPageNumbers();
            this.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "rightPageNumber", oldValue, value));
        }
    }

    public function get itemCount():int
    {
        return _itemCount;
    }

    public function set itemCount(value:int):void
    {
        if (value != _itemCount && value > 0) {
            _itemCount = value;
            updateForPageCount();
            updateForPageNumbers();
        }
    }

    public function set masterCount(value:int):void
    {
        if (value != _masterCount) {
            _masterCount = value;
            updateForPageCount();
            updateForPageNumbers();
        }
    }

    public function get slidersTied():Boolean
    {
        return _slidersTied;
    }

    public function set slidersTied(value:Boolean):void
    {
        _slidersTied = value;
        _tieBar.visible = value;
    }

    public function set completedChangeCallback(value:Function):void
    {
        _completedChangeCallback = value;
    }

    public function set dragStartCallback(value:Function):void
    {
        _dragStartCallback = value;
    }

    public function sliderMouseDown(event:MouseEvent):void
    {
        // careful hittest since slider label text can overlap when we have 100 events
        event.stopImmediatePropagation();
        if (_leftSlider.hitTestPoint(event.stageX, event.stageY, true)) {
            _leftSlider.dragMouseDown(event);
        } else if (_rightSlider.hitTestPoint(event.stageX, event.stageY, true)) {
            _rightSlider.dragMouseDown(event);
        } else if (_tieBar.hitTestPoint(event.stageX, event.stageY, true)) {
            _tieBar.dragMouseDown(event);
        }
    }

}
}


import caurina.transitions.Tweener;

import com.alleni.author.Navigation.FlowScrollbar;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.view.text.LightLabel;

import flash.display.Shape;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Point;

import flashx.textLayout.formats.TextAlign;

class ScrollSlider extends Sprite {

    private var _view:FlowScrollbar;
    private var _stateHolder:Sprite;
    private var _dragStartCallback:Function;
    private var _dragMoveCallback:Function;
    private var _dragEndCallback:Function;
    private var _normal:Sprite;
    private var _bubble:NumberBubble;

    public function ScrollSlider(view:FlowScrollbar, dragStartCallback:Function, dragMoveCallback:Function, dragEndCallback:Function)
    {
        _view = view;
        _stateHolder = new Sprite();
        addChild(_stateHolder);
        _dragStartCallback = dragStartCallback;
        _dragMoveCallback = dragMoveCallback;
        _dragEndCallback = dragEndCallback;
    }

    public function set normalGraphic(value:Sprite):void
    {
        if (_normal) {
            _stateHolder.removeChild(_normal);
        }
        _normal = value;
        _stateHolder.addChild(_normal);
    }

    public function get numberBubble():NumberBubble
    {
        return _bubble;
    }

    public function set numberBubble(value:NumberBubble):void
    {
        _bubble = value;
        addChild(_bubble);  // above _stateHolder
    }


    private var _click:Point;
    private var _sliderStart:Point;

    public function dragMouseDown(event:MouseEvent):void
    {
        event.stopImmediatePropagation();
        _dragStartCallback();
        _click = new Point(event.stageX, event.stageY);
        _sliderStart = this.localToGlobal(new Point(0,0));
        _view._draggingAnySlider = true;
        _view.stage.addEventListener(MouseEvent.MOUSE_MOVE, dragMouseMove);
        _view.stage.addEventListener(MouseEvent.MOUSE_UP, dragMouseUp);
    }

    private function dragMouseMove(event:MouseEvent):void
    {
        ApplicationController.instance.hideToolTip();
        var global:Point = new Point(event.stageX, event.stageY);
        var delta:Point = global.subtract(_click);
        global = _sliderStart.add(delta);
        _dragMoveCallback(global);
    }

    private function dragMouseUp(event:MouseEvent):void
    {
        if (stage) {
            stage.removeEventListener(MouseEvent.MOUSE_MOVE, dragMouseMove);
            stage.removeEventListener(MouseEvent.MOUSE_UP, dragMouseUp);
        }
        _view._draggingAnySlider = false;
        _dragEndCallback();
    }

    override public function hitTestPoint(xx:Number, yy:Number, shapeFlag:Boolean=false):Boolean
    {
        if (_stateHolder.hitTestPoint(xx, yy, shapeFlag)) {    // ignore bounds of label text
            return true;
        } else if (_bubble) {
            return _bubble.hitTestPoint(xx, yy, shapeFlag);
        } else {
            return false;
        }
    }
}

class NumberBubble extends Sprite {

    public static const RADIUS:Number = 9;
    private static const TEXT_Y:Number = -8;
    private static const TEXT_WIDTH:Number = 40;
    private static const TEXT_COLOR_NORMAL:uint = 0x000000;
    private static const TEXT_COLOR_MASTER:uint = 0xffffff;
    private static const THICK:Number = 1;

    private var _background:Shape;
    private var _label:LightLabel;
    private var _isMaster:Boolean;

    public function NumberBubble(yy:Number)
    {
        super();
        _background = new Shape();
        addChild(_background);
        _label = new LightLabel(TEXT_WIDTH, TEXT_COLOR_NORMAL);
        addChild(_label);
        _label.y = TEXT_Y;
        _label.x = - TEXT_WIDTH/2;
        _label.textAlign = TextAlign.CENTER;
        updateBubble();
        this.y = yy;
    }

    private function updateBubble():void
    {
        var line:uint = _isMaster ? 0xbbbbbb : 0x444444;
        var fill:uint = _isMaster ? 0x444444 : 0xbbbbbb;
        with (_background.graphics) {
            clear();
            lineStyle(THICK, line);
            beginFill(fill);
            drawCircle(0,0, RADIUS);
            endFill();
        }
    }



    public function set isMaster(value:Boolean):void
    {
        if (value != _isMaster) {
            _isMaster = value;
            _label.color = _isMaster ? TEXT_COLOR_MASTER : TEXT_COLOR_NORMAL;
            updateBubble();
        }
    }

    public function set text(value:String):void
    {
        _label.text = value;
    }

    override public function hitTestPoint(xx:Number, yy:Number, shapeFlag:Boolean=false):Boolean
    {
        return _background.hitTestPoint(xx, yy, shapeFlag);  // ignore bounds of label text
    }

}
