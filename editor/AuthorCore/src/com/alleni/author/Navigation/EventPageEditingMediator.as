/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/31/12
 * Time: 8:55 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.Navigation.EventPageVelumView;
import com.alleni.author.controller.objects.IClipboardMediator;
import com.alleni.author.controller.objects.IEditingMediator;
import com.alleni.author.controller.objects.IUndoMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.action.ActionTreeEvent;
import com.alleni.author.model.ui.Application;
import com.alleni.taconite.view.ViewContext;

import flash.display.Sprite;
import flash.events.MouseEvent;

public class EventPageEditingMediator extends SmartObjectEditingMediator implements IEditingMediator, IUndoMediator, IClipboardMediator {

    private var _event:EventPage;
    private var _view:EventPageView;

    public function EventPageEditingMediator(context:ViewContext, velum:SmartObjectVelumView, view:EventPageView, objMediator:SmartObjectMediator) {
        super(context, velum, view,  objMediator);
        _event = view.object as EventPage;
        _velum = velum;
        _view = view;

        startEditing();
        updateEditingBackground();
        actionTree.addEventListener(ActionTreeEvent.TYPE, ApplicationController.instance.authorController.handleMainEdit, false, 0, true);
        _view.context.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownVelumListener);
    }

    override public function clearViewEvents():void
    {
        super.clearViewEvents();
        actionTree.removeEventListener(ActionTreeEvent.TYPE, ApplicationController.instance.authorController.handleMainEdit);
        _view.context.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownVelumListener);
    }

    override public function updateVelum():void
    {
        // called in render loop
        _velum.drawVelum(_event.width, _event.height);

        updateWireboardsIfRequested(_event.width);
    }


    // ensure Event marquee still works even while editing a gadget
    override protected function activateMarquee():void {};
    override protected function deactivateMarquee():void {};
//    {
//        // overridden by EventPageEditingMediator
//        _velum.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownVelumListener);
//    }


    override protected function handleSizeChange():void
    {
        updateEditingBackground();
    }

    private function updateEditingBackground():void
    {
        with (_view.editingBackground.graphics) {
            clear();
            beginFill(0xff0000, 0.001);  // to register clicks for marquee
            drawRect(0,0, _event.width, _event.height);
            endFill();
        }
    }

    override public function get MENULABEL_TYPE():String{
        if(_event.isMaster){
            return "Master";
        }
        return "Event";
    }

    override protected function set velumVisible(value:Boolean):void {
        // only show when in project edit mode and for pro projects
        super.velumVisible = value && !Application.uiRunning && Application.instance.document.project.accountTypeId >= 3;
    }
}
}
