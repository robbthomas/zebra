/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 4/2/12
 * Time: 8:46 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.view.DashedRectView;
import com.alleni.author.view.ui.WireboardView;
import com.alleni.taconite.view.ViewContext;

import flash.display.Sprite;
import flash.geom.Rectangle;

public class SmartObjectVelumView extends Sprite {

    protected var _context:ViewContext;
    protected var _smart:SmartObject;
    protected var _holder:Sprite = new Sprite();  // normal velum graphics are in here, to hide when in perimeter mode
    protected var _wireboards:Vector.<WireboardView>;
    protected var _perimeterMode:Boolean;  // when true draw dotted outline only (for parent of current gadget)
    protected var _perimeterRect:DashedRectView;


    public function SmartObjectVelumView(context:ViewContext, smart:SmartObject, objMediator:SmartObjectMediator) {

        _context = context;
        _smart = smart;
        addChild(_holder);
        makeWireboards();
    }

    public function drawVelum(w:Number, h:Number):void
    {

    }


    public function get perimeterMode():Boolean
    {
        return _perimeterMode;
    }

    public function set perimeterMode(value:Boolean):void
    {
        _perimeterMode = value;
        if (_perimeterRect)
            _perimeterRect.visible = _perimeterMode;
        _holder.visible = !value;  // hide heading, etc, when in perimeter mode
    }


    protected function makeWireboards():void
    {
        _wireboards = new Vector.<WireboardView>();
        _wireboards.push(	new WireboardView(_smart, _context, 0),
            new WireboardView(_smart, _context, 1));

        _holder.addChild(_wireboards[0]);
        _holder.addChild(_wireboards[1]);

        _wireboards[0].other = _wireboards[1];
        _wireboards[1].other = _wireboards[0];
    }

    protected function updateWireboardPositions(rect:Rectangle):void
    {
        if ( _wireboards != null ) {
            if ( _wireboards.length > 1 ) {
                _wireboards[0].y = rect.top;
                _wireboards[1].y = rect.top;
                _wireboards[0].x = rect.left- WireboardView.WIDTH;
                _wireboards[1].x = rect.right;
                _wireboards[0].draw(rect.height);
                _wireboards[1].draw(rect.height);
            }
        }
    }

    public function get wireboards():Vector.<WireboardView>
    {
        return _wireboards;
    }
}
}
