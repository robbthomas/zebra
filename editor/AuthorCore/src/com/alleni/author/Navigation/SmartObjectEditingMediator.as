/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/31/12
 * Time: 4:10 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.MarqueeSelectionMediator;
import com.alleni.author.controller.app.ClipboardController;
import com.alleni.author.controller.objects.IEditingMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.palettes.ToolboxController;
import com.alleni.author.definition.DragInfo;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.definition.action.DestroyObjectAction;
import com.alleni.author.definition.action.UserAction;
import com.alleni.author.event.AnchorEvent;
import com.alleni.author.event.WireEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.InternalSmartWireAnchor;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.WorldView;
import com.alleni.author.view.ui.WireboardView;
import com.alleni.taconite.document.ObjectSelection;
import com.alleni.taconite.event.ModelUpdateEvent;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.Sprite;

import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Timer;

public class SmartObjectEditingMediator implements IEditingMediator {

    protected var _context:ViewContext;
    protected var _velum:SmartObjectVelumView;
    protected var _smartView:SmartObjectView;
    protected var _smart:SmartObject;
    protected var _model:TaconiteModel;
    protected var _authorController:AuthorController;
    protected var _objMediator:SmartObjectMediator;
    private var _reqUpdateWireboards:Boolean;
    private var _hideVelum:Boolean;

    protected function set velumVisible(value:Boolean):void {
        _velum.visible = value;
    }

    public function SmartObjectEditingMediator(context:ViewContext, velum:SmartObjectVelumView, view:SmartObjectView, objMediator:SmartObjectMediator) {
        _context = context;
        _velum = velum;
        _smartView = view;
        _model = view.model;
        _smart = view.object as SmartObject;
        _objMediator = objMediator;
        _authorController = ApplicationController.instance.authorController;
        requestUpdateWireboards();
    }

    public function clearViewEvents():void
    {
        deactivate();
        _authorController.closingEditing(this, true);
    }


    protected function startEditing():void
    {
        // called on creation of the velum
        _authorController.startingEditing(this);  // will cause activate
    }

    public function updateVelum():void
    {
        // called in the render loop
    }

    protected function updateWireboardsIfRequested(velumWidth:Number):void
    {
        if (_reqUpdateWireboards) {
            updateWireboards(velumWidth);
        }
    }

    public function requestUpdateWireboards():void
    {
        _reqUpdateWireboards = true;
        if (_context.stage)
            _context.stage.invalidate();
    }

    private function updateWireboards(velumWidth:Number):void
    {
        // move each inner anchor to the side nearest its wire ends
        /*
        Detail:  its not simply which side the objects are closest to, because that causes
        it to switch sides too easily.  The important case is an object slightly left of center
        and drawing a new wire from that object to the right edge.  The new wire would immediately
        snap to the left side even tho dragged by the user to the right side.  Very bad.
        And this effect is even more pronounced with wide objects such as tables, and when
        the velum is not much wider than the object.

        Solution is to have averageWireEndGlobal look at the ports nearest to the current
        side.  This gives a preference for keeping the port on the current side.
        */
        var center:Point = _velum.localToGlobal(new Point(velumWidth/2,0));
        var centerX:Number = center.x;
        for each (var anchor:InternalSmartWireAnchor in _smart.innerAnchors) {
            var portLoc:Point = anchor.averagePortLocation;  // "average" not really needed ... there is only one port
            if (portLoc) {
                var avg:Point = averageWireEndGlobal(anchor, portLoc);  // use ports nearest to "portLoc"
                if (avg)
                    anchor.portSide = (avg.x < centerX) ? WireboardView.LEFT : WireboardView.RIGHT;
            } else if (anchor.portSide != WireboardView.LEFT && anchor.portSide != WireboardView.RIGHT) {
                var wireEnd:Point = averageWireEndGlobal(anchor, center);
                if (wireEnd) {
                    anchor.portSide = (wireEnd.x < centerX) ? WireboardView.LEFT : WireboardView.RIGHT;
                }
            }
        }
        _velum.wireboards[0].recalc();
        _velum.wireboards[1].recalc();
        _reqUpdateWireboards = false;
    }

    private function averageWireEndGlobal(anchor:WireAnchor, global:Point):Point
    {
        var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
        if (wires.length == 0)
            return null;
        var sumX:Number = 0;
        var sumY:Number = 0;
        var count:int = 0;
        for each (var wire:Wire in wires) {
            var other:WireAnchor = (wire.masterAnchor == anchor) ? wire.slaveAnchor : wire.masterAnchor;
            var loc:Point = other.nearestPortLocation(global);
            if (loc) {
                sumX += loc.x;
                sumY += loc.y;
                ++count;
            }
        }
        if (count == 0)
            return null;
        else
            return new Point(sumX / count, sumY / count);
    }

    public function hilitePort(anchor:WireAnchor, hilite:Boolean):void
    {
        var target:ExternalSmartWireAnchor;
        if (anchor as ExternalSmartWireAnchor)
            target = anchor as ExternalSmartWireAnchor;
        else
            target = findWiredCustomRibbon(anchor);
        if (target) {
            _velum.wireboards[0].hiliteAnchor(target,hilite);
            _velum.wireboards[1].hiliteAnchor(target,hilite);
        }
    }
				
    private function findWiredCustomRibbon(anchor:WireAnchor):ExternalSmartWireAnchor
    {
        var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
        for each (var wire:Wire in wires) {
            var other:WireAnchor = (wire.masterAnchor == anchor) ? wire.slaveAnchor : wire.masterAnchor;
            if (other as InternalSmartWireAnchor)
                return InternalSmartWireAnchor(other).other;
        }
        return null;
    }

    public function hideVelum():void
    {
        _hideVelum = true;
        velumVisible = false;
    }

    protected function updateVelumMode(thisIsActive:Boolean, parentIsActive:Boolean):void
    {
        // for nested gadget editing, show perimeter box around the gadget that is parent of the current velum gadget
        if (_hideVelum) {
            velumVisible = false;
        } else if (thisIsActive) {
            velumVisible = true;
            _velum.perimeterMode = false;
        } else {
            velumVisible = parentIsActive;
            _velum.perimeterMode = parentIsActive;
        }
    }

    protected function activate():void
    {
        activateMarquee();
        _context.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpCaptureListener, true);
        _model.addEventListener(ModelUpdateEvent.MODEL_UPDATE, modelUpdateListener);
        _smart.model.addEventListener(WireEvent.COMPLETE, wireCompleteListener);
        _smart.addEventListener(AnchorEvent.TYPE, anchorListener);
        _smart.activeVelum = true;
        requestUpdateWireboards();
    }

    protected function deactivate():void
    {
        deactivateMarquee();
        _context.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpCaptureListener, true);
        _model.removeEventListener(ModelUpdateEvent.MODEL_UPDATE, modelUpdateListener);
        _smart.model.removeEventListener(WireEvent.COMPLETE, wireCompleteListener);
        _smart.removeEventListener(AnchorEvent.TYPE, anchorListener);
        ApplicationController.instance.hideToolTip();  // in case tooltip left on wireboard
        _smart.activeVelum = false;
    }

    protected function activateMarquee():void
    {
        // overridden by EventPageEditingMediator
        _velum.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownVelumListener);
    }

    protected function deactivateMarquee():void
    {
        // overridden by EventPageEditingMediator
        _velum.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownVelumListener);
    }

    private function modelUpdateListener(e:ModelUpdateEvent):void
    {
        handleModelUpdate(e.source, e.property as String, e.newValue, e.parent);
    }
		
    protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, parent:TaconiteModel):void
    {	
        if (model == _model) {  // update applies to the composite itself
            switch (property) {
                case "width":
                case "height":
                    handleSizeChange();
                    break;
            }
        } else {  // child update
            // when MC goes visible, then the wires will show, so recalc wireboards
            if (property == "messageCenterVisible" && newValue == true) {
                var timer:Timer = new Timer(700,1);  // wait for MC to open and ribbons to create
                timer.addEventListener(TimerEvent.TIMER_COMPLETE, function(evt:TimerEvent):void{requestUpdateWireboards()} );
                timer.start();
            }
        }
    }

    protected function handleSizeChange():void
    {
    }
    
    protected function mouseDownVelumListener(event:MouseEvent):void
    {
        if (Application.instance.currentTool == ToolboxController.ARROW) {
            new MarqueeSelectionMediator(_context, worldView, _smart.objects).handleMouseDown(event);
        }
    }

    private function mouseUpCaptureListener(evt:MouseEvent):void
    {
        // after drag obj:  recalc wireboards, to uncross wires by sorting pegs
        requestUpdateWireboards();
    }
		
    private function wireCompleteListener(event:WireEvent):void
    {
        requestUpdateWireboards();
    }
		
    private function anchorListener(event:AnchorEvent):void
    {
        if (event.anchor is ExternalSmartWireAnchor)
            requestUpdateWireboards();
    }

    protected function get worldView():WorldView
    {
        return WorldContainer(_context).worldView;
    }

   

//  IEditingMediator:
//      function getSelection(getGlobal:Boolean, strict:Boolean=true, omitLocked:Boolean=true):Vector.<DragInfo>;
//		function cancelEditing():Boolean;
//		function deleteEditing():Boolean;
//		function selectAll(includeLocked:Boolean):void;
//		function makeObject():Boolean;
//		function switchedObjects(oldObj:ITaconiteObject, newObj:ITaconiteObject):void;
//		function get editingObject():ITaconiteObject;
//		function get actionTree():ActionTree;
//		function get hasActiveState():Boolean;
//      function get MENULABEL_TYPE():String;
//		function activeMediatorChanged(editor:IEditingMediator):void;

    public function selectAll(includeLocked:Boolean):void {
        _authorController.selectModels(_smart.modelsForSelectAll(includeLocked));
    }

    public function getSelection(getGlobal:Boolean, strict:Boolean=true, omitLocked:Boolean=true):Vector.<DragInfo> {
        var selection:ObjectSelection = ApplicationController.instance.authorController.selection as ObjectSelection;

        if(selection != null) {
            for each (var m:TaconiteModel in selection.selectedModels) {
                var obj:AbstractObject = m.value as AbstractObject;
            }
            return DragInfo.fromModelList(_smart, selection.selectedModels, getGlobal, strict, omitLocked);
        } else {
            return new Vector.<DragInfo>();
        }
    }


    public function get hasActiveState():Boolean
    {
        return true;
    }

    public function activeMediatorChanged(activeMediator:IEditingMediator):void
    {
        if (activeMediator is SmartObjectEditingMediator) {  // otherwise if opened path editing, keep showing the vellum
            var thisIsActive:Boolean = (activeMediator == this);
            if (thisIsActive)
                activate();
            else
                deactivate();
            var activeLevel:int = _authorController.editingMediatorIndex(activeMediator);
            var myLevel:int = _authorController.editingMediatorIndex(this);
            updateVelumMode(thisIsActive, activeLevel == myLevel+1);  // pass true if parent velum is active
        }
    }
        
    public function cancelEditing():Boolean
    {
        if(_smart.actionsDestroyOriginal == null) {
            return false;
        }
        makeObject();
        // revert the gadget
        var destroy:UserAction = _smart.actionsDestroyOriginal;
        destroy.perform();
        destroy.rollback();
        return true;
    }

    public function makeObject():Boolean
    {
        if (_smart.editing) {
            _objMediator.closeEditing();
        }
        return true;
    }

    public function switchedObjects(oldObj:ITaconiteObject, newObj:ITaconiteObject):void
    {
    }

    public function get editingObject():ITaconiteObject
    {
        return _smart;
    }

    public function deleteEditing():Boolean {
        var info:Vector.<DragInfo> = getSelection(false);  // omitLocked=true
        if(info.length > 0) {
            var models:Vector.<TaconiteModel> = DragInfo.toModels(info);
            var action:DestroyObjectAction = DestroyObjectAction.fromSelection(models);
            action.perform();
            ApplicationController.currentActionTree.commit(action);
        }
        return true;
    }

    public function get actionTree():ActionTree
    {
        return _smart.actionTree;
    }

    public function get MENULABEL_TYPE():String{
        return "MENULABEL_TYPE";
    }

    public function canUndo():Boolean {
        return _smart.actionTree.canUndo;
    }

    public function canRedo():Boolean {
        return _smart.actionTree.canRedo;
    }

    public function undo():void {
        _authorController.log("undo " + _smart.actionTree.undoName);
        _smart.actionTree.undo();
    }

    public function redo():void {
        _authorController.log("redo " + _smart.actionTree.redoName);
        _smart.actionTree.redo();
    }

    public function undoName():String {
        return _smart.actionTree.undoName;
    }

    public function redoName():String {
        return _smart.actionTree.redoName;
    }

    public function cut(e:Event):void {
        ClipboardController.instance.cutClipboard(e);
    }

    public function copy(e:Event):void {
        ClipboardController.instance.copyClipboard(e);
    }

    public function paste(e:Event):void {
        ClipboardController.instance.pasteClipboard(e);
    }


}
}
