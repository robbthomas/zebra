/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/31/12
 * Time: 8:55 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.action.DestroyObjectAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.objects.ArenaControlBar;
import com.alleni.taconite.document.ISelection;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;


import flash.events.MouseEvent;

public class EventPageMediator extends SmartObjectMediator {

    private var _event:EventPage;
    private var _view:EventPageView;
    private var _velum:EventPageVelumView;
    private var _flow:EventFlow;

    public function EventPageMediator(context:ViewContext, view:SmartObjectView)
    {
        super(context, view);
        _view = view as EventPageView;
        _event = view.object as EventPage;
        _flow = _event.parent as EventFlow;
        ApplicationController.addEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED, handleRunningStateChanged, false, 0, true);
    }

    override public function clearViewEvents():void
    {
        super.clearViewEvents();
        ApplicationController.removeEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED, handleRunningStateChanged);
    }

    override protected function handleDoubleClick(event:MouseEvent):void
    {
        if (!Application.runningLocked && !_event.isMaster) {
            ApplicationController.instance.authorController.editEvent(_event, false); // tween
        }
        event.stopImmediatePropagation();
    }

    override public function openEditingThis():void
    {
        ApplicationController.instance.authorController.clearSelection();

        var pager:EventFlow = _event.parent as EventFlow;
        pager.editingPage = _event;
        pager.wasEditingEvent = -1;

        // create deletion action for use in GadgetSaveController
        _event.actionsDestroyOriginal = DestroyObjectAction.fromObject(_event);

        _event.editing = true;
        _event.activeVelum = true;  // used by obj.childOfClosed
        _event.scopeParent = null;  // not used since layering is not changed for EventPage editing
        _view.visible = true;  // page view visibility is controlled directly by PagerView

        createVelumView();

        // enable objects to be added to the composite
        _event.accepting = true;
        _event.hadChildren = true;
        _event.showAllObjects(Application.instance.showAll);
        _event.complete = true;
    }

    public function restoreSelection():void
    {
        var restoredSelection:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
        for each (var path:Array in _event.cachedSelection){
            try {
//                trace("restoreSelection: path="+path.join(","), "objects.len="+_event.objects.length);
                var obj:AbstractObject = _event.resolveObjectPathArray(path);
//                trace("  obj="+obj);
                restoredSelection.push(obj.model);
            } catch(e:Error) {
//                trace("   failure restoring selection");
                // the path doesn't resolve. Either resolve returned null and we got a null error getting model, or the function threw a resolve error, ignore
            }
        }
        ApplicationController.instance.authorController.selectModels(restoredSelection);
        ApplicationController.instance.wireController.showWiresForLevel();
        ArenaControlBar.updateAllControlbarVisibility();
    }
    
    override public function closeEditing():void
    {
        trace("EventPageMediator:closeEditing", _event, "parent="+_event.parent, "objects="+_event.objects.length);
        if (_event.editing == false)
            return;  // already closed

        _event.showAllObjects(false);
        _event.updateImage();
        var pager:EventFlow = _event.parent as EventFlow;
        pager.wasEditingEvent = pager.getModelIndex(pager.editingPage);
        pager.editingPage = null;
        deleteVelumView();  // closes editing mediator
//        _model.removeEventListener(WireEvent.COMPLETE, wireCompleteListener);
        _event.cachedSelection = new Vector.<Array>();
        var selection:ISelection = ApplicationController.instance.authorController.selectionCache;
        if(selection){
            for each (var m:TaconiteModel in selection.selectedModels) {
                try {
                    var obj:AbstractObject = m.value as AbstractObject;
                    var path:Array = _event.objectPathArray(obj);
//                    trace(" path="+path.join(","));
                    _event.cachedSelection.push(path);
                } catch(e:Error) {
                    // likely the object is not in the event. Ignore
//                    trace("  failure saving selection")
                }
            }
        }

        ApplicationController.instance.authorController.deselectDescendantsOf(_event);
        ApplicationController.instance.authorController.clearSelectionCache();

        ApplicationController.instance.wireController.showWiresForLevel();
        _event.editing = false;  // (enables snapToPath, so must be after recalcBounds)
        _event.activeVelum = false;
        _event.complete = true;  // show MC, there will be one
			
        _event.scopeParent = null;
        ArenaControlBar.updateAllControlbarVisibility();
    }

    private function createVelumView():void
    {
        if (_velum == null) {
            _velum = new EventPageVelumView(_context, _view, this);
            _view.addChild(_velum);  // add to the EventView itself
            _velum.drawVelum(_event.width, _event.height);
            _view.editingBackground.visible = true;
            _velum.visible = !Application.uiRunning && Application.instance.document.project.accountTypeId >= 3;
            _context.stage.invalidate();
        }
    }

    public function deleteVelumView():void
    {
        if (_velum) {
            _velum.warnViewToDelete();  // _velum is not an ObjectView so it doesn't get this automatically
            if (_view.contains(_velum)) {
                _view.removeChild(_velum);
            }
            _velum = null;
        }
        _view.editingBackground.visible = false;
    }

    private function handleRunningStateChanged(event:ApplicationEvent = null):void{
        if(_velum) {
            _velum.visible = !Application.uiRunning && Application.instance.document.project.accountTypeId >= 3;
        }
    }
}
}
