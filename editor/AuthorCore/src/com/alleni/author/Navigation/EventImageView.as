/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/28/12
 * Time: 1:59 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.ui.TooltipMediator;
import com.alleni.author.definition.FlowTooltips;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.model.GraphicFill;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.GraphicFillView;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.view.ITaconiteView;
import com.alleni.taconite.view.ViewContext;

import flash.display.Bitmap;

import flash.display.DisplayObject;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.geom.Point;
import flash.utils.Dictionary;

import mx.collections.IList;


public class EventImageView extends ObjectView {

    private static const PAGE_BORDER_THICK:Number = 1;
    private static const PAGE_BORDER_COLOR:uint = 0x222222;
    private static const VEIL_COLOR:uint = 0x676767;

    private var _eventImage:EventImage;
    private var _eventImageHolder:Sprite = new Sprite();
    private var _emptyPage:EmptyPageView;
    private var _background:Sprite = new Sprite();
    private var _foreground:Sprite = new Sprite();
    private var _page:EventPage;
    private var _width:Number;
    private var _height:Number;
    private var _tooltipMediator:TooltipMediator = new TooltipMediator();


    public function EventImageView(context:ViewContext,  page:EventPage, imageWidth:Number, imageHeight:Number) {

        super(context, page.object.model, ViewRoles.PRIMARY);
        _width = imageWidth;
        _height = imageHeight;
        _page = page;
        _emptyPage = new EmptyPageView(imageWidth, imageHeight, pageTitle(page));  // always show the empty page while loading
        addChild(_background);
        addChild(_emptyPage);
        addChild(_eventImageHolder);
        addChild(_foreground);
        this.mouseChildren = false;
        this.doubleClickEnabled = true;
        updateView();
    }

    override public function initialize():void
    {
        super.initialize();
        scaleX = 1.0;
        scaleY = 1.0;
        visible = true;
        if (!_page.object.loading) {
            afterLoad();
        }
    }

    private function afterLoad():void
    {
        if (!_eventImage) {
            _eventImage = new EventImage(_page, _width, _height, this);
            _eventImageHolder.addChild(_eventImage);
            rebuildMasters();
            _emptyPage.labelText = pageTitle(_page);
        }
        render();
    }

    override public function warnViewToDelete():void
    {
        super.warnViewToDelete();
        if (_eventImage) {
            _eventImage.warnViewToDelete();
        }
        var image:EventImage;
        var n:int;
        for (n = 0; n < _foreground.numChildren; n++) {
            image = _foreground.getChildAt(n) as EventImage;
            image.warnViewToDelete();
        }
        for (n = 0; n < _background.numChildren; n++) {
            image = _background.getChildAt(n) as EventImage;
            image.warnViewToDelete();
        }
    }

    public function rebuildMasters():void
    {
        // similar to code in PagerView
        var cache:Dictionary = new Dictionary();
        removeMasterPageViews(_background, cache);  // adds removed views to cache, doesn't call warnViewToDelete
        removeMasterPageViews(_foreground, cache);
        var pager:Pager = _page.parent as Pager;
        var count:int = pager.masterPages.length;
        for (var n:int = 0; n < count; n++) {
            var master:EventPage = pager.masterPages.getItemAt(n) as EventPage;
            if (masterShouldShow(_page, master)) {
                var image:EventImage = cache[master];
                if (image) {
                    delete cache[master];
                } else {
                    image = new EventImage(master, _width, _height, this);
                }
                if (master.isForeground) {
                    _foreground.addChild(image);
                } else {
                    _background.addChild(image);
                }
            }
        }
        for each (var leftover:EventImage in cache) {
            leftover.warnViewToDelete();
        }
    }

    private function masterShouldShow(current:IPage, master:IPage):Boolean
    {
        return (current.masters.getItemIndex(master) >= 0);
    }

    private function removeMasterPageViews(container:Sprite, cache:Dictionary):void
    {
        // add to cache and don't call warnViewToDelete
        for (var n:int = container.numChildren-1; n >= 0; n--) {
            var child:EventImage = container.getChildAt(n) as EventImage;
            container.removeChildAt(n);
            cache[child.page] = child;
        }
    }

    public function setSize(w:Number, h:Number):void
    {
        _width = w;
        _height = h;
        if (_eventImage) {
            _eventImage.setSize(w, h);
        }
        _emptyPage.setSize(w, h);

        var n:int;
        var image:EventImage;
        for (n = 0; n < _foreground.numChildren; n++) {
            image = _foreground.getChildAt(n) as EventImage;
            image.setSize(w, h);
        }
        for (n = 0; n < _background.numChildren; n++) {
            image = _background.getChildAt(n) as EventImage;
            image.setSize(w, h);
        }
        updateView();

        var tooltip:String = (page.isMaster? FlowTooltips.HOVER_OVER_MASTER : FlowTooltips.HOVER_OVER_EVENT);
        _tooltipMediator.handleTooltip(this, tooltip, new Point(this.width/2,this.height+7), ControlAreaToolTip.DISPLAY_BELOW, true);

    }

    override public function set x(value:Number):void{};   // redirected so ObjectView won't affect this view's coordinates
    override public function set y(value:Number):void{};

    public function get itemX():Number
    {
        return super.x;
    }

    public function set itemX(value:Number):void
    {
        super.x = value;
    }

    public function get itemY():Number
    {
        return super.y;
    }

    public function set itemY(value:Number):void
    {
        super.y = value;
    }

    override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
    {
        // not calling super

        switch (property) {
            case "loading":
                if(newValue == false) {
                    afterLoad();
                    return true;
                }
                break;
            case "title":
            case "titleChangedByAuthor":
            case "pageNumber":
            case "isMaster":
                _emptyPage.labelText = pageTitle(_page);
                return true;
            case "hadChildren":
                _emptyPage.visible = !_page.hadChildren;
                return false;
            case "complete":
            case "messageCenter":
            case "messageCenterVisible":
                return super.updateModelProperty(property,  oldValue, newValue);
        }
        return true;
    }

    override protected function updateArrayProperty(property:String, kind:String, array:IList, index:int):Boolean
    {
        var result:Boolean = super.updateArrayProperty(property, kind, array, index);
        if (!object.loading) {
//            trace("PagerView:updateArrayProperty prop="+property, "kind="+kind, "index="+index);
            switch (property) {
                case "masters":
                    switch (kind) {
                        case ModelEvent.ADD_ARRAY_ITEM:
                        case ModelEvent.REMOVE_ARRAY_ITEM:
                        case ModelEvent.CHANGE_ARRAY_ITEM:
                            rebuildMasters();
                            break;
                    }
                    break;
            }
        }
        return result;
    }


    private function pageTitle(page:EventPage):String {
        var pager:Pager = _page.parent as Pager;
        return _page.loading ? "Loading" : _page.title;
    }

    override public function render():void
    {
        if (_eventImage && _eventImage.haveImage && _page.hadChildren) {
            _emptyPage.visible = false;
        }
        drawBackground(this.graphics);
    }

    public function drawBackground(g:Graphics):void
    {
        var fill:GraphicFill;
        var arena:Pager = _page.object.parent as Pager;
        if (arena) {
            fill = arena.fillSpec;
        } else {
            fill = new GraphicFill();
        }

        var w:Number = _width;
        var h:Number = _height;
        var thick:Number;
        var color:uint;
        thick = PAGE_BORDER_THICK;
        color = PAGE_BORDER_COLOR;
        var half:Number = thick/2;

        g.clear();
        g.lineStyle(thick,color);
        GraphicFillView.beginGraphicFill(g, fill, w, h, 0,0);
        g.drawRect(-half,-half, w+thick, h+thick);
        g.endFill();
    }

    public function get page():IPage
    {
        return _page;
    }

    override public function get width():Number
    {
        return _width;
    }

    override public function get height():Number
    {
        return _height;
    }

    public function makeThumbnail(scale:Number):Sprite
    {
        var w:Number = _width*scale;
        var h:Number = _height*scale;

        var image:DisplayObject;
        if (_page.image) {
            var bitmap:Bitmap = new Bitmap(_page.image);
            bitmap.scaleX = w / bitmap.bitmapData.width;
            bitmap.scaleY = h / bitmap.bitmapData.height;
            image = bitmap;
        } else {
            image = new EmptyPageView(w, h,  pageTitle(_page));   // scaled bitmap wouldn't be readable: EmptyPageView will adjust to thumb size
        }
        var sprite:Sprite = new Sprite();
        sprite.addChild(image);
        const THICK:Number = 1.5
        const HALF:Number = THICK/2;
        with (sprite.graphics) {
            lineStyle(THICK);
            beginFill(0xffffff);
            drawRect(-HALF, -HALF, w+THICK, h+THICK);
            endFill();
        }
        return sprite;
    }

    override protected function computeDockPosition():Point
    {
        var global:Point;
        if (_page.dockMessageCenterToStage) {   // while editing page, center the MC below the vellum, which is defined by EventFlowView
            return messageCenterPositionBelowStage;
        } else {
            return feedbackUIView.globalToLocal(this.localToGlobal(new Point(_width/2, _height + FlowView.MESSAGE_CENTER_Y_OFFSET)));
        }
    }

    public function get messageCenterPositionBelowStage():Point
    {
        var flow:EventFlow = object.parent as EventFlow;
        var mainView:EventFlowView = flow.eventPagerView;
        return feedbackUIView.globalToLocal(mainView.localToGlobal(new Point(flow.width/2, flow.height)));
    }

    private function get feedbackUIView():Sprite
    {
        return WorldContainer(_context).feedbackUIView;
    }

    // disable features of ObjectView
    override public function updateViewPosition():void {};
    override protected function setDragMediator():void {};
    override protected function recalcVisibility():void {};
    override protected function set rolloverGlow(value:Boolean):void {};
    override protected function updateFilters():void {};
    override protected function updateStatus():void {};
    override protected function createFeedbackView():ITaconiteView {return null};

    override protected function registerView():void
    {
        // since this is an image of the view and not a "real" view, do not register it, so obj.getView() won't see it.
    }


    override public function toString():String
    {
        return "[" + _viewClassName + " \"" + object.title + "\" page="+_page.title + " image=" + _eventImage +  "]";
    }

}
}

import com.alleni.author.Navigation.EventImageView;
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.IPage;
import com.alleni.author.model.project.Project;

import flash.display.Bitmap;

import flash.display.Sprite;

import mx.events.PropertyChangeEvent;


class EventImage extends Sprite {

    private var _page:EventPage;
    private var _image:Bitmap = null;
    private var _width:Number;
    private var _height:Number;
    private var _view:EventImageView;
    private var _haveImage:Boolean;

    public function EventImage(page:EventPage, imageWidth:Number, imageHeight:Number, eventImageView:EventImageView)
    {
        _page = page;
        _width = imageWidth;
        _height = imageHeight;
        _view = eventImageView;
        page.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, propertyChangeListener, false, 0, true);
        changeDisplay();
    }

    public function warnViewToDelete():void
    {
        _page.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, propertyChangeListener, false);
    }

    private function propertyChangeListener(event:PropertyChangeEvent):void {
        switch(event.property) {
            case "image":
                changeDisplay();
                break;
        }
    }

    private function changeDisplay():void {
        if (_page.image) {
            displayedImage = new Bitmap(_page.image);
            _haveImage = true;
            _view.render();  // allow EventImageView to hide the empty-page
        }
    }

    private function set displayedImage(image:Bitmap):void {
        if(_image && contains(_image)) {
            removeChild(_image);
        }
        _image = image;
        _image.smoothing = true;
        sizeImage();
        addChild(_image);
    }

    public function get haveImage():Boolean
    {
        return _haveImage;
    }

    public function setSize(w:Number, h:Number):void
    {
        _width = w;
        _height = h;
        sizeImage();
    }

    private function sizeImage():void {
        var b:Bitmap = _image;
        if (b) {
            b.scaleX = _width / b.bitmapData.width;
            b.scaleY = _height / b.bitmapData.height;
        }
    }

    public function get page():EventPage
    {
        return _page;
    }


    override public function toString():String
    {
        return "[EventImage _image=" + _image + " page="+_page.title + "]";
    }


}
