/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/17/12
 * Time: 9:52 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import assets.buttons.eventFlowEdit;
import assets.buttons.eventFlowNext;
import assets.buttons.eventFlowPrev;
import assets.buttons.eventFlowResume;
import assets.buttons.eventFlowRun;


import flash.display.CapsStyle;
import flash.display.DisplayObject;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;



public class FlowButtons {

    private static const PLUS_NORMAL_FILL:uint = 0xcccccc;
    private static const PLUS_OVER_FILL:uint = 0x95c2f2;
    private static const PLUS_DOWN_FILL:uint = 0x54ebbe;
    private static const BK_OUT:uint = 0x3a3a3a;  // (no longer matters since backgroundAlpha=0)
    private static const BK_OVER:uint = 0x95c2f2;
    private static const BK_DOWN:uint = 0x54EBBE;
    private static const ICON_OUT:uint = 0xa6a6a6;
    private static const ICON_OVER:uint = 0x000000;
    private static const ICON_DOWN:uint = 0x000000;
    private static const BLACK:uint = 0x000000;


    public static function get plusButton():FlowButton
    {
        var btn:FlowButton = new FlowButton(getCircleBackground(10), plusIcon, BLACK, BLACK, BLACK,  PLUS_NORMAL_FILL, PLUS_OVER_FILL, PLUS_DOWN_FILL);
        btn.positionIcon(false, -4.25, -4.25);
        return btn;
    }

    private static function get plusIcon():Sprite
    {
        const LEN:Number = 8.5;
        const HALF:Number = LEN/2;
        var icon:Sprite = new Sprite();
        icon.graphics.lineStyle(2, BLACK, 1.0, false, "normal", CapsStyle.NONE);
        icon.graphics.moveTo(HALF,0);
        icon.graphics.lineTo(HALF,LEN);
        icon.graphics.moveTo(0,HALF);
        icon.graphics.lineTo(LEN,HALF);

        return icon;
    }

    public static function get minusButton():FlowButton
    {
        var btn:FlowButton = new FlowButton(getCircleBackground(10), minusIcon, ICON_OUT, BLACK, BLACK,  PLUS_NORMAL_FILL, PLUS_OVER_FILL, PLUS_DOWN_FILL, 0);  // backgroundAlpha=0
        btn.positionIcon(false, -4.25, -4.25);
        return btn;
    }

    private static function get minusIcon():Sprite
    {
        const LEN:Number = 8.5;
        const HALF:Number = LEN/2;
        var icon:Sprite = new Sprite();
        icon.graphics.lineStyle(2, BLACK, 1.0, false, "normal", CapsStyle.NONE);
        icon.graphics.moveTo(0,HALF);
        icon.graphics.lineTo(LEN,HALF);

        return icon;
    }

    public static function get nextButton():FlowButton
    {
        var fxg:DisplayObject = new eventFlowNext();
        var icon:Sprite = new Sprite();
        icon.addChild(fxg);
        var btn:FlowButton = new FlowButton(getCircleBackground(10), icon, ICON_OUT,ICON_OVER,ICON_DOWN, BK_OUT, BK_OVER, BK_DOWN,0);
        btn.positionIcon(false, -3, -5);
        return btn;
    }

    public static function get prevButton():FlowButton
    {
        var fxg:DisplayObject = new eventFlowPrev();
        var icon:Sprite = new Sprite();
        icon.addChild(fxg);
        var btn:FlowButton = new FlowButton(getCircleBackground(10), icon, ICON_OUT,ICON_OVER,ICON_DOWN,  BK_OUT, BK_OVER, BK_DOWN,0);
        btn.positionIcon(false, -6, -5);
        return btn;
    }

    public static function get runButton():FlowButton
    {
        var fxg:DisplayObject = new eventFlowRun();
        var icon:Sprite = new Sprite();
        icon.addChild(fxg);
        var btn:FlowButton = new FlowButton(getCircleBackground(10), icon, ICON_OUT,ICON_OVER,ICON_DOWN,  BK_OUT, BK_OVER, BK_DOWN,0);
        btn.positionIcon(false, -3, -5);
        return btn;
    }

    public static function get resumeButton():FlowButton
    {
        var fxg:DisplayObject = new eventFlowResume();
        var icon:Sprite = new Sprite();
        icon.addChild(fxg);
        var btn:FlowButton = new FlowButton(getCircleBackground(10), icon, ICON_OUT,ICON_OVER,ICON_DOWN,  BK_OUT, BK_OVER, BK_DOWN,0);
        btn.positionIcon(false, -6, -5);
        return btn;
    }

    public static function get editButton():FlowButton
    {
        var fxg:DisplayObject = new eventFlowEdit();
        var icon:Sprite = new Sprite();
        icon.addChild(fxg);
        var btn:FlowButton = new FlowButton(getCircleBackground(10), icon, ICON_OUT,ICON_OVER,ICON_DOWN,  BK_OUT, BK_OVER, BK_DOWN,0);
        btn.positionIcon(false, -6, -5);
        return btn;
    }

    private static function getCircleBackground(radius:Number):Sprite
    {
        var bg:Sprite = new Sprite();
        bg.graphics.beginFill(PLUS_NORMAL_FILL);
        bg.graphics.drawCircle(0,0,radius);

        return bg;
    }


}
}
