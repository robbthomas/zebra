/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/30/12
 * Time: 2:13 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.objects.CompositeController;
import com.alleni.author.controller.objects.IEditingMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.CompositeSaveControllerTarget;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.controller.ui.WireCreateUtil;
import com.alleni.author.definition.AbstractModifierDescription;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.InspectorRibbonDescriptions;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.OutletDescription;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.definition.action.CreateObjectAction;
import com.alleni.author.definition.action.DestroyObjectAction;
import com.alleni.author.definition.action.UserAction;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ObjectInlets;
import com.alleni.author.model.ObjectProperties;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.InternalSmartWireAnchor;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.persistence.GadgetData;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.persistence.GadgetUtils;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.model.TaconiteModel;

import flash.geom.Rectangle;
import flash.utils.Dictionary;

import mx.collections.IList;

public class SmartObject extends AbstractContainer implements CompositeSaveControllerTarget
	{


    [Transient]
        public function get project():Project {
            return _gadget;
        }

    [Transient]
        public function get canReplaceOtherCopies():Boolean {
            return this is Composite && objects.length > 0;
        }

    [Transient]
        public function get savableParent():CompositeSaveControllerTarget {
            return wireScopeContainer as SmartObject;
        }

    [Transient]
        public function get hasChanges():Boolean {
            var ep:EventPage = this as EventPage;
            if (ep && ep.unsavedChanges) {
                return true;
            }
            var ed:IEditingMediator = ApplicationController.instance.authorController.editingMediatorForObject(this);
            return ed.actionTree.canUndo;
        }

    [Transient]
        public function get shouldBeDestroyed():Boolean {
            return (this is Composite) && objects.length <= 0;
        }

    [Transient]
        public function get hasParent():Boolean {
            return parent != null;
        }

    public function updateProject(newProject:Project):void {
        // override by EventPage
        this.gadget = newProject;
        actionTree.clear();
    }

    private function findListContainingObj():Object
    {
        var parentModel:TaconiteModel = model.parent;
        var names:Vector.<String> = parentModel.getAllValueListProperties();
        for each (var name:String in names) {
            var list:IList = parentModel.value[name];
            if (list.getItemIndex(this) >= 0) {
                return name;
            }
        }
        return -1;
    }

    [Transient]
    public function get actionsDestroyOriginal():UserAction {
        return _actionsDestroyOriginal;
    }

    public function set actionsDestroyOriginal(action:UserAction):void {
        _actionsDestroyOriginal = action
    }

    public function makeDestroyAction():UserAction {
        return DestroyObjectAction.fromObject(this);
    }

    public function makeCreateAction():UserAction {
        return CreateObjectAction.fromObject(this);
    }

    public function closeVellum(saving:Boolean) {
        Utilities.assert(this.hasParent, "SmartObject::closeVellum this.hasParent");
        var editor:IEditingMediator = ApplicationController.instance.authorController.editingMediatorForObject(this);
        if (editor) {
            if(this is EventPage && saving) {
                EventPage(this).preventUnload = true; // prevent unloading because we need internals for the save (cleared in CompositeSaveController)
            }
            editor.makeObject();  // close vellum, recalcBounds
        }
    }

    public function recreateWithProject(newProject:Project, values:Object):CompositeSaveControllerTarget {
        var listName:Object = findListContainingObj();   // -1 if not a listname
        if (listName is String) {
            var list:IList = parent[listName] as IList;
            listName = [listName, list.getItemIndex(this)+1];  // eg. ["pages",3] to add replacement after index=2
        }
        var newObj:SmartObject = ApplicationController.instance.authorController.addObjectForName(this.shortClassName, 0, 0, 100, 100, false, this.parent, listName) as SmartObject;
        var gd:GadgetData = new GadgetData(this.parent, values.initialValues, null, true, values.currentValues, null, true);
        GadgetController.instance.configureContainerAndDisplay(newProject, newObj, NaN, NaN, gd);
        Utilities.assert(newObj.gadget != null, "SmartObject::recreateWithProject newObj.gadget");
        return newObj;
    }

    public function save():Project {
        return GadgetController.instance.saveForSmartObject(this);
    }

    public function discardChanges() {
        Utilities.assert(this.hasParent, "SmartObject::discardChanges: this.hasParent");
        var editor:IEditingMediator = ApplicationController.instance.authorController.editingMediatorForObject(this);
        if (editor) {
            editor.cancelEditing();  // close vellum, recalcBounds
        }
    }

    public function getCurrentValues():Object {
        return GadgetEncoder.getValues(this);
    }

    [Bindable]
		public var viewBox:Rectangle;

		private var _gadget:Project;

		[Transient]
		public var innerAnchors:Dictionary = new Dictionary();
        private var _externalAnchorsByKey:Dictionary = new Dictionary();

		[Transient]
		public var actionTree:ActionTree = new ActionTree();

		[Transient]
		public var scopeParent:AbstractContainer;

		[Transient]
		public var activeVelum:Boolean;

		[Transient]
		public var replaceablePropertyChangeListener:Function;

		[Transient]
        private var _actionsDestroyOriginal:UserAction;

		[Transient]
		[Bindable]
		public var draggingChild:Boolean;  // prevent rollover glow while dragging a child


		public function SmartObject(setupProperties:Boolean=true)
		{
			super(setupProperties);

			accepting = false;
			releasing = false;

            if(!setupProperties) {
                return;
            }

			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineThickness"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineColor"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineAlpha"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillSpec"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillAlpha"));

            ObjectInlets.instance.registerInlet(this, new InletDescription("resetObject", "", "Reset properties", "Reset properties to initial values\nof this gadget and its objects", Modifiers.DO, 34));


            var inspectorShowing:Array = InspectorRibbonDescriptions.COMPOSITE_INSPECTOR_RIBBONS;
			for each (var key:String in inspectorShowing) {
				const wireAnchor:WireAnchor = anchors[key] as WireAnchor;
				if (wireAnchor) {
					wireAnchor.propertyInspectorVisible = true;
				}
			}
		}

		override public function addAnchor(anchor:WireAnchor, display:Boolean):void
		{
			super.addAnchor(anchor, display);
			const externalSmartWireAnchor:ExternalSmartWireAnchor = anchor as ExternalSmartWireAnchor;
			if(externalSmartWireAnchor && externalSmartWireAnchor.other) {
				addInnerAnchor(externalSmartWireAnchor.other);
			}
		}

		override public function removeAnchor(anchor:WireAnchor):void
		{
			super.removeAnchor(anchor);
			const externalSmartWireAnchor:ExternalSmartWireAnchor = anchor as ExternalSmartWireAnchor;
			if(externalSmartWireAnchor && externalSmartWireAnchor.other) {
				removeInnerAnchor(externalSmartWireAnchor.other);
			}
		}

		public function addInnerAnchor(inner:InternalSmartWireAnchor):void
		{
			if(innerAnchors[inner.modifierDescription.key] == null) {
				innerAnchors[inner.modifierDescription.key] = inner;
                _externalAnchorsByKey["external-"+inner.other.externalID] = inner.other;
            }
		}

		public function removeInnerAnchor(inner:InternalSmartWireAnchor):void
		{
			if (innerAnchors[inner.modifierDescription.key] == inner) {
				delete innerAnchors[inner.modifierDescription.key];
                delete _externalAnchorsByKey["external-"+inner.other.externalID];
			}
		}

        public function externalAnchorForKey(key:String):ExternalSmartWireAnchor {
            return _externalAnchorsByKey[key];
        }

        //TODO REMOVE
        [Deprecated]
		public function addCustomAnchor(anchor:WireAnchor):ExternalSmartWireAnchor
		{
			var fromInside:Boolean;
			if (anchor.hostObject == this && anchor as InternalSmartWireAnchor)
				fromInside = true;
			else
				fromInside = CompositeController.withinScope(anchor.hostObject, this); /* the wire is being dragged from an item in the composite */
			trace("addCustomAnchor: fromInside="+fromInside, "fromHost="+anchor.hostObject, "this="+this.title);

			// setup internal anchor
			var inner:InternalSmartWireAnchor = new InternalSmartWireAnchor();
			inner.hostObject = this;
			inner.other.hostObject = this;

            var label:String = WireCreateUtil.nextLabelFromPrevious(anchor);

            var d:AbstractModifierDescription = anchor.modifierDescription as AbstractModifierDescription;
			if ((d is OutletDescription && fromInside) || (d as InletDescription && !fromInside)) {
				inner.modifierDescription = new OutletDescription("internal-"+inner.internalID,d.id,label,"","CUSTOM");
				inner.other.modifierDescription = new OutletDescription("external-"+inner.other.externalID,d.id,label,"","CUSTOM");
			} else if ((d is InletDescription && fromInside) || (d is OutletDescription && !fromInside)) {
                if(this is EventPage) {
                    return null; // inlet custom anchors not allowed for event pages
                }
				inner.modifierDescription = new InletDescription("internal-"+inner.internalID,d.id,label,"","CUSTOM");
				inner.other.modifierDescription = new InletDescription("external-"+inner.other.externalID, d.id,label,"","CUSTOM");
			} else {
				var copiedDescription:PropertyDescription = anchor.modifierDescription as PropertyDescription;
				inner.modifierDescription = new PropertyDescription("internal-"+inner.internalID,d.id,label,"","CUSTOM", 0, false, -1, copiedDescription.type, copiedDescription.constraints);
				inner.other.modifierDescription = new PropertyDescription("external-"+inner.other.externalID,d.id,label,"","CUSTOM",0, false, -1, copiedDescription.type, copiedDescription.constraints, copiedDescription.writeable);
			}

			addAnchor(inner.other, true);
			return inner.other;
		}

        //TODO REMOVE
        [Deprecated]
		private function labelForCustomRibbon(anchor:WireAnchor, objTitle:String=null):String
		{
			if (objTitle == null) {
				objTitle = anchor.hostObject.title;
			}
			return objTitle + ": " + anchor.modifierDescription.label;
		}

		/**
		 * Given a set of wire anchors, find wired custom ribbons on this composite.
		 * If the anchors are inside nested gadgets, keep looking until this composite is reached.
		 * If no custom anchor is found, return an empty array.
		 * @param anchors
		 * @param outerGadget
		 * @return
		 *
		 */
		public function findCustomAnchors(anchors:Array):Array
		{
			var result:Array = [];
			for each (var anchor:WireAnchor in anchors) {
				var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
				for each (var wire:Wire in wires) {
					var other:* = (wire.slaveAnchor == anchor) ? wire.masterAnchor : wire.slaveAnchor;
					var internalSmartWireAnchor:InternalSmartWireAnchor = other as InternalSmartWireAnchor;
					if (internalSmartWireAnchor) {
						var extAnchor:WireAnchor = internalSmartWireAnchor.other;
						result.push(extAnchor);
					}
				}
			}

			// nested gadgets: look for custom ribbons at each level
			if (result.length > 0) {
				anchor = result[0];
				if (anchor.hostObject != this)
					return findCustomAnchors(result);
			}
			return result;
		}

        override public function getUsageCounts(usages:UsageCounts):void
        {
            super.getUsageCounts(usages);  // count assets in Setters on obj itself
            countChildUsages(usages);
        }

        protected function countChildUsages(usages:UsageCounts):void
        {
            var thisObj:AbstractObject = this;
            allObjectsInScope(function(obj:AbstractObject):Boolean {
                if (obj != thisObj) {
                    obj.getUsageCounts(usages);
                }
                return true;
            });
        }

		[Transient]
		public function get scaleX():Number
		{
		    return 1;
		}

		[Transient]
		public function get scaleY():Number
		{
			return 1;
		}


        [Transient]
        [Bindable]
        public function get gadget():Project
        {
            return _gadget;  // gadget is null until after first edit, then Event is considered non-empty
        }

        public function set gadget(value:Project):void
        {
            _gadget = value;
        }

		[Transient]
		override public function get okToSelect():Boolean
		{
			return !editing && super.okToSelect;
		}

		override protected function get okToSnapToPath():Boolean
		{
			return (super.okToSnapToPath && !editing);
		}

		override public function toString():String
		{
            return "[" + shortClassName + " \"" + title+"\" gadget="+(gadget?gadget.projectId:"null") + "]";
		}

        override public function onRestoreInitial():void {
            super.onRestoreInitial();
            var anchor:Object;
            for each(anchor in innerAnchors) {
                var other:ExternalSmartWireAnchor = InternalSmartWireAnchor(anchor).other;
                if(other && other.modifierDescription as PropertyDescription) {
                    var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor as WireAnchor);
                    if(wires.length > 0) {
                        var wire:Wire = wires[0] as Wire;
                        if(wire.masterAnchor == anchor) {
                            anchor.value = wire.slaveAnchor.value;
                        } else {
                            anchor.value = wire.masterAnchor.value;
                        }
                    }
                }
            }
        }


}
}
