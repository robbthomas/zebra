/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 10/29/12
 * Time: 8:53 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.view.ui.IconButton;

import flash.display.Sprite;

public class FlowButton extends IconButton {

    private static const BUTTON_HILITE_COLOR:uint = 0xffffff;  // hilight the pencil & run buttons when used
    private static const BUTTON_DISABLE_COLOR:uint = 0x737373;  // next & prev buttons gray-out when at last-page or first-page

    private var _outColor:uint;
    private var _hilite:Boolean;
    private var _enabled:Boolean = true;

    public function FlowButton(background:Sprite, icon:Sprite, iconOutColor:uint=0x7D7D7D, iconOverColor:uint=0xB4B4B4, iconDownColor:uint=0xFFFFFF,
                                    backgroundOutColor:uint=0x0099FF, backgroundOverColor:uint=0x0065C3, backgroundDownColor:uint=0x1A305C,
                                    backgroundAlpha:Number=1)
    {
        super(background, icon, iconOutColor, iconOverColor, iconDownColor, backgroundOutColor, backgroundOverColor, backgroundDownColor, backgroundAlpha);
        _outColor = iconOutColor;
        this.iconOutColor = _outColor;
    }

    public function get hilite():Boolean {
        return _hilite;
    }

    public function set hilite(value:Boolean):void {
        if (value != _hilite) {
            _hilite = value;
            update();
        }
    }

    public function get enabled():Boolean {
        return _enabled;
    }

    public function set enabled(value:Boolean):void {
        if (value != _enabled) {
            _enabled = value;
            mouseChildren = value;
            mouseEnabled = value;
            update();
        }
    }

    public function get outColor():uint {
        return _outColor;
    }

    public function set outColor(value:uint):void {
        if (value != _outColor) {
            _outColor = value;
            update();
        }
    }

    private function update():void {
        if (_hilite) {
            iconOutColor = BUTTON_HILITE_COLOR;
        } else if (!_enabled) {
            iconOutColor = BUTTON_DISABLE_COLOR;
        } else {
            iconOutColor = _outColor;
        }
    }
}
}
