/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/16/12
 * Time: 9:39 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import caurina.transitions.Tweener;

import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ModelCollectionEvent;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.view.ITaconiteView;
import com.alleni.taconite.view.ViewContext;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.PixelSnapping;
import flash.display.Shape;
import flash.display.Sprite;
import flash.display.Stage;
import flash.events.Event;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.ui.Keyboard;
import flash.utils.Dictionary;

import mx.collections.IList;

public class FlowView extends ObjectView {

    private var _flow:EventFlow;
    private var _mediator:FlowEditingMediator;
    private var _scale:Number = 1.0;   // scaling of the image views (scaling is not used on this view as a whole; the controls draw in proportion to the scale of the items)
    private var _scaleConfigured:Boolean;
    private var _renderDisabled:Boolean;
    private var _bitmapOfOtherMC:Sprite;  // image of the MC for the "other" page ... the one not being edited

    private var _items:Dictionary;  // key=IPage, value=ItemView

    private var _holder:Sprite;
    private var _previousRightNumber:int;

    private var _leftControlBar:FlowControlbar;
    private var _rightControlBar:FlowControlbar;
    private var _insertMark:Shape;


    private var _scrollbar:FlowScrollbar;

    private static const SCALE_FACTOR:Number = 0.85;  // how much of the application window to cover

    private static const CONTROL_BAR_OFFSET:Number = -30;
    private static const SCROLLBAR_Y_OFFSET:Number = 28;
    private static const BELOW_CANVAS:Number = 30;
    private static const SPACING_CENTER:Number = 60;

    private static const STACK_DEPTH:int = 5;
    private static const TWEEN_TYPE:String = "easeOutExpo";   // flipping pages
    private static const TWEEN_TIME:Number = 0.5;
    private static const TWEEN_TIME_EDIT:Number = 0.5;      // opening event to edit
    private static const TWEEN_TYPE_EDIT:String = "easeInOutCubic";



    private static const PAGE_HORZ_OFFSETS:Array = [0, 15, 25, 32, 37];  // how much page-edge index by depth

    public static const MESSAGE_CENTER_Y_OFFSET:Number = 65;




    public function FlowView(flow:EventFlow,  context:ViewContext) {

        super(context, flow.model, ViewRoles.PRIMARY);

//        trace("- FlowView created");
        assert(!flow.flowView);
        _flow = flow;
        _context = context;

        _holder = new Sprite();
        addChild(_holder);
        _leftControlBar = new FlowControlbar(_flow, context, false);
        addChild(_leftControlBar);
        _rightControlBar = new FlowControlbar(_flow, context, false);
        addChild(_rightControlBar);
        createScrollbar();  // referenced by mediator
        createItemViews();  // references _mediator
        _mediator = new FlowEditingMediator(context);
        _mediator.handleViewEvents(this);
        createInsertMark();
        _flow.model.addEventListener(ModelEvent.MODEL_CHANGE, handleModelChange);
        ApplicationController.addEventListener(NotificationNamesApplication.STAGE_RESIZED, handleAppResize);
        ApplicationController.addEventListener(NotificationNamesApplication.SCALE_CHANGED, handleAppResize);

        if (flow.rightPageNumber == flow.leftPageNumber)
            ++flow.rightPageNumber;
        _scrollbar.itemCount = itemCount;
        _scrollbar.masterCount = _flow.masterPages.length;
        _scrollbar.leftPageNumber = flow.leftPageNumber;
        _scrollbar.rightPageNumber = flow.rightPageNumber;
        _previousRightNumber = 0;
        _scrollbar.slidersTied = (flow.leftPageNumber+1 == flow.rightPageNumber);
    }

    override public function initialize():void
    {
        super.initialize();
        scaleX = 1.0;   // never changes when _scale changes
        scaleY = 1.0;
        if (!_flow.loading) {
            afterLoad();
        }
    }

    private function afterLoad():void
    {
        createItemViews();
        _flow.leftPageNumber = Math.max(_flow.masterPages.length-1, 0);  // the last master
        _mediator.handleLeftPageChange(_flow.leftPageNumber);
        _flow.rightPageNumber = _flow.leftPageNumber+1;  // the first page
        _mediator.handleRightPageChange(_flow.rightPageNumber);
        handleAppResize();
    }

    public function updateMastersInEventImages():void
    {
        for each (var image:EventImageView in _items) {
            image.rebuildMasters();
        }
    }

    override public function warnViewToDelete():void
    {
        super.warnViewToDelete();
        _flow.model.removeEventListener(ModelEvent.MODEL_CHANGE, handleModelChange);
        ApplicationController.removeEventListener(NotificationNamesApplication.STAGE_RESIZED, handleAppResize);
        ApplicationController.removeEventListener(NotificationNamesApplication.SCALE_CHANGED, handleAppResize);
        _mediator.clearViewEvents();
        for each (var image:EventImageView in _items) {
            image.warnViewToDelete();
        }
    }

    override public function set visible(value:Boolean):void
    {
        if (value == super.visible) {
            return;
        }
//        trace("- flowView vis="+value);
        super.visible = value;
        if (value) {
            render();
        } else if (!_flow.doingEditTween) {  // setting visible=false, so hide all MCs
            for (var n:int = _holder.numChildren-1; n >= 0; n--) {
                var item:EventImageView = _holder.getChildAt(n) as EventImageView;
                var page:EventPage = item.page as EventPage;
                if (page != _flow.editingPage) {   // except MC tweened into place for editing a page
                    page.needMessageCenter = false;
                }
            }
            ApplicationController.instance.wireController.showWiresForLevel();
        }
        _mediator.handleVisibilityChange(value);
    }

    private function handleAppResize(e:Event=null):void
    {
        updateSize();
    }

    private function requestUpdateSize():void
    {
        // test by resize-app during tweenBackFromEdit:
        // the views should jump in size after tween is done; controlbars should resize to match
        PseudoThread.add(new PseudoThreadWaitFrameRunnable());
        PseudoThread.add(new PseudoThreadSimpleRunnable("FlowView.updateSize", updateSize));
    }

    private function updateSize():void
    {
        // called whether flow is visible or not
        if (this.parent && _flow.parent && !_flow.loading && !_flow.doingEditTween) {
            this.x = _flow.width/2;   // 0,0 is center of EventFlowView
            this.y = _flow.height/2;

            var canvas:Rectangle = EditorUI.instance.canvasVisibleRect(this.parent);
            var zoom:Number = Application.instance.zoomLevel;
            // include zoom-level so author can get more room for backstage by going to 50% and shrinking the flow view
            var sx:Number = canvas.width * zoom / (2*_flow.width + SPACING_CENTER + 2*37);
            var sy:Number = canvas.height * zoom / (_flow.height + 200);
            var scale:Number = SCALE_FACTOR * Math.min(sx, sy);
            _scale = scale;
            _scaleConfigured = true;
            var imageWidth:Number = computeImageWidth;
            var imageHeight:Number = computeImageHeight;
            for each (var image:EventImageView in _items) {
                image.setSize(imageWidth, imageHeight);
            }
            placeControlBars();
            if (this.visible && !_flow.loading) {
                updateFlow();
            }
        }
    }

    private function placeControlBars():void
    {
        _leftControlBar.width = computeImageWidth;  // controlBar.width enforces a minimum
        _leftControlBar.x = -_leftControlBar.width - SPACING_CENTER/2;
        _leftControlBar.y = computeControlBarY;
        _leftControlBar.stageControlsFrac = 0;
        _rightControlBar.width = computeImageWidth;
        _rightControlBar.x = SPACING_CENTER/2;
        _rightControlBar.y = computeControlBarY;
        _rightControlBar.stageControlsFrac = 0;
    }

    private function createItemViews():void{
        var source:Pager = _flow;
        _items = new Dictionary();

        var count:int = itemCount;
        for (var n:int = 0; n < count; n++){
            var page:EventPage = _flow.getPageForIndex(n);
            createItemView(page);
        }
    }

    private function createItemView(page:EventPage):EventImageView
    {
        var w:Number = _flow.width * _scale;
        var h:Number = _flow.height * _scale;
        var view:EventImageView = _items[page];
        if (view == null) {
            view = new EventImageView(_context, page, w, h);
            view.initialize();
            _mediator.addListenersToItem(view);
            _items[page] = view;
        }
        return view;
    }

    public function get itemCount():int
    {
        return _flow.masterPages.length + _flow.pages.length;
    }

    public function getItemView(index:int):EventImageView
    {
        var page:IPage = _flow.getPageForIndex(index);
        if (page) {
            return _items[page];
        }
        return null;
    }

    public function getModelView(page:IPage):EventImageView
    {
        var index:int = _flow.getModelIndex(page);
        return getItemView(index);
    }

    override protected function updateView():void
    {
        if (visible && !_flow.doingEditTween) {
            super.updateView();
        }
    }

    override public function render():void
    {
        // not calling super
        if (!_flow.loading && !_renderDisabled && this.visible && leftPage && !_flow.ignorePageListChanges) {
            var tween:Boolean = (_scrollbar.slidersTied && _scrollbar._draggingAnySlider);
            updateFlow(tween);
        }
    }

    private function updateFlow(tween:Boolean=false):void
    {
        updateStacks(tween);
        updateControlBars();
        updateScrollbars();
        if (!tween || wiringLevel >= 4) {
            ApplicationController.instance.wireController.showWiresForLevel();
        }
    }

    private function updateControlBars():void
    {
        _leftControlBar.page = leftPage;
        _leftControlBar.update();
        _rightControlBar.page = rightPage;
        _rightControlBar.update();
    }

    private function updateStacks(tween:Boolean=false, updateMessageCenters:Boolean=true):void
    {
        if (_renderDisabled) {
            return;  // removing tween from an image (below) causes tweenForEdit to fail to call its onComplete
        }

        var pager:Pager = _flow;
        var left:int = pager.leftPageNumber;
        var right:int = pager.rightPageNumber;
        var forward:Boolean = (right > _previousRightNumber);
        _previousRightNumber = right;

//        trace("updateStacks: left="+left, "right="+right, "forward="+forward, "tween="+tween, "count="+itemCount);
        assert(!_flow.ignorePageListChanges);

        // build "desired" array of views, in layer order.
        var desired:Vector.<EventImageView> = new Vector.<EventImageView>();
        var view:EventImageView;
        var page:EventPage;
        var n:int;
        for (n = left-STACK_DEPTH+1; n <= left; n++) {
            view = getItemView(n);
            if (view) {
                desired.push(view);
            }
        }
        var leftCount:int = desired.length;
        var rightInsert:int = (forward) ? 0 : leftCount;  // when going forward, left pile is above right, since the item being animated is already part of the left stack as regards layering
        for (n = right; n <= right + STACK_DEPTH-1; n++) {
            view = getItemView(n);
            if (view) {
                desired.splice(rightInsert, 0, view);  // place highest index at bottom
            }
        }
//        trace("-");
//        trace("Desired: len="+desired.length);
//        for (n = 0; n < desired.length; n++) {    // debug dump
//            view = desired[n];
//            trace("  n="+n, view);
//        }

        // remove the undesired views from _holder
        for (n = _holder.numChildren-1; n >= 0; n--) {
            view = _holder.getChildAt(n) as EventImageView;
            if (desired.indexOf(view) < 0) {
                _holder.removeChildAt(n);
                Tweener.removeTweens(view); // ensure MC.vis doesn't get updated further
                page = view.page as EventPage;
                page.needMessageCenter = false;
            }
        }

        // add the desired views, in layer order and place them
        var myStage:Stage = _context.stage;
        for (n = 0; n < desired.length; n++) {
            view = desired[n];
            _holder.addChild(view);
            page = view.page as EventPage;
            var index:int = _flow.getModelIndex(page);
            assert(index >= 0);
            var depth:int = (index <= left) ? left - index : index - right;  // depth in stack:  0=top of either stack
            if (updateMessageCenters) {
                if (depth == 0) {
                    createPageMessageCenter(view);
                    page.needMessageCenter = true;
                    page.dockMessageCenterToStage = false;
                    page.messageCenterRepositioned = false;
                } else {
                    page.needMessageCenter = false;
                }
            }
            view.scaleX = 1.0; // restore scale in case it was edited earlier
            view.scaleY = 1.0;
            var newX:Number = computeImageX(index, depth);
            var newY:Number = computeImageY(index, depth);
            if (tween && (forward ? (index == left) : (index == right))) {
                Tweener.addTween(view, {itemX:newX, itemY:newY, transition:TWEEN_TYPE, time:TWEEN_TIME, onUpdate:function():void{myStage.invalidate();}});
            } else {
                view.itemX = newX;
                view.itemY = newY;
                Tweener.removeTweens(view);
            }
        }
    }

    public function computeImagesBounds():Rectangle {
        var iw:Number = computeImageWidth;
        var ih:Number = computeImageHeight;

        // below functions abuse the parameters that computeImage<Foo> take.
        var top:Number = computeImageY(0, 0); // parameters are unused
        var bottom:Number = top + ih;
        var left:Number = computeImageX(_flow.leftPageNumber, PAGE_HORZ_OFFSETS.length-1); // ensure left with maximum offset
        var right:Number = computeImageX(_flow.rightPageNumber, PAGE_HORZ_OFFSETS.length-1) + iw; // ensure right with maximum offset

        var topLeft:Point = new Point(left, top);
        var bottomRight:Point = new Point(right,  bottom);

        topLeft = localToGlobal(topLeft);
        bottomRight = localToGlobal(bottomRight);

        return new Rectangle(topLeft.x, topLeft.y,  bottomRight.x - topLeft.x,  bottomRight.y - topLeft.y);
    }

    public function moveOutsideBounds(desiredPoint:Point, offset:Boolean):Point {
        var bounds:Rectangle = computeImagesBounds();
        if(bounds.containsPoint(desiredPoint) == false) {
            return new Point(0, 0); // if we are not in the flow bounds, no worries
        }
        // find the closest point along the boundary to our point
        var distance:Number = Infinity;
        var offsetVector:Point;
        var candidateDistance:Number;

        // up
        candidateDistance = desiredPoint.y - bounds.top;
        if(candidateDistance < distance) {
            distance = candidateDistance;
            offsetVector = new Point(0, -candidateDistance);
        }
        // down
        candidateDistance = bounds.bottom - desiredPoint.y;
        if(candidateDistance < distance) {
            distance = candidateDistance;
            offsetVector = new Point(0, candidateDistance);
        }
        // left
        candidateDistance = desiredPoint.x - bounds.left;
        if(candidateDistance < distance) {
            distance = candidateDistance;
            offsetVector = new Point(-candidateDistance, 0);
        }
        // right
        candidateDistance = bounds.right - desiredPoint.x;
        if(candidateDistance < distance) {
            distance = candidateDistance; // useless but kept for symmetry
            offsetVector = new Point(candidateDistance, 0);
        }
        if(!offset) {
            offsetVector.x += desiredPoint.x;
            offsetVector.y += desiredPoint.y;
        }
        return offsetVector
    }

    private function computeImageX(n:int, depth:int):Number
    {
        var offset:Number = PAGE_HORZ_OFFSETS[depth];
        if (n <= _flow.leftPageNumber) {
            return -computeImageWidth - SPACING_CENTER/2 - offset;
        } else {
            return SPACING_CENTER/2 + offset;
        }
    }

    private function computeImageY(n:int, depth:int):Number
    {
        return -computeImageHeight/2;
    }

    private function computeItemTopLeft(item:EventImageView):Point
    {
        var left:int = _flow.leftPageNumber;
        var right:int = _flow.rightPageNumber;
        var index:int = _flow.getModelIndex(item.page);
        assert(index >= 0);
        var depth:int = (index <= left) ? left - index : index - right;  // depth in stack:  0=top of either stack
        var newX:Number = computeImageX(index, depth);
        var newY:Number = computeImageY(index, depth);
        return new Point(newX, newY);
    }

    private function computeItemCenter(item:EventImageView):Point
    {
        var point:Point = computeItemTopLeft(item);
        point.x += computeImageWidth/2;
        point.y += computeImageHeight/2;
        return point;
    }

    private function get computeImageWidth():Number
    {
        return _flow.width * _scale;
    }

    private function get computeImageHeight():Number
    {
        return _flow.height * _scale;
    }

    public function pageMasterListChanged(page:IPage):void
    {
        updateView();
    }

    public function pageForegroundChanged(master:IPage):void
    {
        for each (var image:EventImageView in _items) {
            if (image.page.masters.getItemIndex(master) >= 0) {
                image.rebuildMasters();
            }
        }
        updateView();
    }

    private function createInsertMark():void
    {
        const W:Number = 14; // width of one side
        const H:Number = 24;
        const T:Number = 7;
        const I:Number = 12; // inseam
        var mark:Shape = new Shape();
        with (mark.graphics) {
            beginFill(0x54EBBE);
            moveTo(0, 0);  // 0,0 is top center point of the mark
            lineTo(W, H);
            lineTo(W-T, H);
            lineTo(0, H-I);
            lineTo(-W+T, H);
            lineTo(-W, H);
            lineTo(0, 0);
            endFill();
        }
        mark.visible = false;
        addChild(mark);
        _insertMark = mark;
    }

    public function placeInsertMark(global:Point):void
    {
        if (global) {
            _insertMark.visible = true;
            var local:Point = globalToLocal(global);
            _insertMark.x = local.x;
            _insertMark.y = local.y + 27;  // down a bit from top of scrollbar
//            trace("placeInsertMark local="+local);
        } else {
            _insertMark.visible = false;
        }
    }

    public function get insertMarkVisible():Boolean
    {
        return _insertMark.visible;
    }

    private function get computeControlBarY():Number
    {
        return -computeImageHeight/2 + CONTROL_BAR_OFFSET;
    }

    private function createScrollbar():void
    {
        _scrollbar = new FlowScrollbar();
        addChild(_scrollbar);
    }

    private function updateScrollbars():void
    {
        var w:Number = _flow.width * _scale * 2 + SPACING_CENTER;
        w = Math.max(w, 460); // min scrollbar width coordinates with min control-bar width
        _scrollbar.width = w;
        _scrollbar.x = -w/2;
        _scrollbar.y = computeScrollbarY;

        _scrollbar.itemCount = itemCount;
        _scrollbar.masterCount = _flow.masterPages.length;
        _scrollbar.visible = (_scrollbar.itemCount > 2);
    }

    private function get computeScrollbarY():Number
    {
        return computeImageHeight/2 + SCROLLBAR_Y_OFFSET;
    }

    private function handlePagesRearranged():void {

        // the actions here are needed even when flowVisible=false
//        trace("FlowView::handlePagesRearranged");
        var toRemove:Vector.<EventPage> = new Vector.<EventPage>();
        var toAdd:Vector.<EventPage> = new Vector.<EventPage>();
        for(var item:Object in _items) {
            toRemove.push(item);
        }
        for each(var pageList:IList in [_flow.masterPages, _flow.pages]) {
            for each(var page:EventPage in pageList) {
                if(_items[page] == null) {
                    toAdd.push(page);
                } else {
                    toRemove.splice(toRemove.indexOf(page), 1);
                }
            }
        }
        for each(var removePage:EventPage in toRemove) {
            var view:EventImageView = _items[removePage];
            view.warnViewToDelete();
            delete _items[removePage];

        }
        for each(var addPage:EventPage in toAdd) {
            createItemView(addPage);
        }
        var forward:Boolean = (_flow.rightPageNumber > _previousRightNumber);
        if (this.visible) {  // avoid changing MC visibility while flow not showing, eg. Run while editing page
            updateFlow();
        }

        _mediator.handleLeftPageChange(_flow.leftPageNumber);
        _mediator.handleRightPageChange(_flow.rightPageNumber);
        updateMastersInEventImages();
        updateView();
    }

    private function handleModelChange(e:ModelEvent):void
    {
        var pager:Pager;
        var ce:ModelCollectionEvent = e as ModelCollectionEvent;
        if (ce && ce.parent.value == _flow) {
            pager = ce.parent.value as Pager;
            switch (ce.property) {
                case "masterPages":
                case "pages":
                    if (!pager.loading && !pager.ignorePageListChanges) {
                        switch (e.kind) {
                            case ModelEvent.ADD_ARRAY_ITEM:
                                pageAdded(ce.item as EventPage);
                                break;
                            case ModelEvent.REMOVE_ARRAY_ITEM:
                                pageRemoved(ce.item as EventPage, (ce.property == "masterPages"));
                                break;
                        }
                    }
            }
        }
    }

    private function pageAdded(page:EventPage):void
    {
        if (_items[page] == null) {
            createItemView(page);
        }
        updateView();
    }

    public function tweenPageAdded(newPage:EventPage, existing:EventPage, after:Boolean, insideButton:Boolean):void
    {
        updateFlow();
        // only the after-button on left-page and the before-button on right-page will do a tween...
        // ... if you try to tween the outside-add, it looks funny because if its the rightmost page, there is nothing under it.
        if (insideButton) {
            var item:EventImageView = _items[newPage];
            var other:EventImageView = _items[existing];
            _renderDisabled = true;  // prevent render from removing tween from item
            var destX:Number = item.itemX;
            item.itemX = other.itemX;
            Tweener.addTween(item, {itemX:destX, transition:TWEEN_TYPE, time:TWEEN_TIME, onComplete:function():void {
                _renderDisabled = false;
                updateView();  // just in case tween was wrong
            }});
        }
    }

    private function pageRemoved(page:EventPage, wasMaster:Boolean):void
    {
        // PagerController reduces the leftPageNumber & rightPageNumber as necessary when a page is removed
        var item:EventImageView = _items[page] as EventImageView;
        if (item) {
            if (!_flow.rearranging) {
                item.warnViewToDelete();
                delete _items[page];
            }
            if (_holder.contains(item)) {
                _holder.removeChild(item);
            }
        }
        updateView();  // will remove the view from stage
    }

    override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
    {
        // not calling super
//        trace("flowView: prop="+property, "newValue="+newValue);

        if (property == "loading" && newValue == false) {
            afterLoad();
        } else if(property == "ignorePageListChanges" && newValue == false) {
            handlePagesRearranged();
        } else if (!_flow.loading && !_flow.ignorePageListChanges) {
            switch (property) {
//                case "width":
//                case "height":
                      // don't update size here: instead EventFlowController.rebuildAllEventImages() is called by settings dialog.
//                    break;
                case "leftPageNumber":
                        _mediator.handleLeftPageChange(newValue as int);
                    return _renderDisabled;  // request update (return false) unless disabled
                case "rightPageNumber":
                        _mediator.handleRightPageChange(newValue as int);
                    return _renderDisabled;
                case "editingPage":
                    var wasEditingPage:IPage = oldValue as IPage;
                    if(wasEditingPage && wasEditingPage.isMaster) {
                        // editing a master will pull it from the _background or _foreground sprites
                        // so when we finish editing, those sprites need to be fixed
                        updateMastersInEventImages();
                    }
                    break;
                case "rearranging":
                    if (newValue == false) {
                        updateMastersInEventImages();
                        updateControlBars();
                    }
                    break;
                case "messageCenterVisible":
                    return true;  // prevent looping (since updateFlow calls showWiresForLevel)
            }
        }
        return false;
    }

    public function tweenForEditEvent(page:EventPage, onComplete:Function):void
    {
        var token:Object = ApplicationController.instance.disableInput("tweenForEditEvent");
        assert(!_flow.doingEditTween);  // if tween fails to finish, its probably because updateFlow was called during tween, and removed the tween from the image
        assert(!_renderDisabled);

        scrollToRevealPage(page);
        _flow.titleBarPage = page;
        var bar:FlowControlbar = (page == leftPage) ? _leftControlBar : _rightControlBar;
        var buttonName:String = _flow.titleBarRunning ? FlowControlbar.RUN_BUTTON : FlowControlbar.EDIT_BUTTON;
        bar.hiliteButton(buttonName, true);   // hilite same button as will be hilited in titlebar

        // capture bitmap of other-page MC
        var otherPage:IPage = (page == leftPage) ? rightPage : leftPage;
        var otherImage:EventImageView = getModelView(otherPage);
        _bitmapOfOtherMC = captureMessageCenterBitmap(otherImage);
        if (_bitmapOfOtherMC) {
            addChild(_bitmapOfOtherMC);
        }

        // find the destination coordinates, in the EventFlowView
        var pageImage:EventImageView = getModelView(page);
        var pagerView:PagerView = _flow.eventPagerView;
        var topLeft:Point = pagerView.localToGlobal(new Point(0, 0));
        var botRight:Point = pagerView.localToGlobal(new Point(_flow.width, _flow.height));
        _context.stage.addEventListener(Event.ENTER_FRAME, function(e:Event):void{
            _context.stage.removeEventListener(Event.ENTER_FRAME, arguments.callee);   // wait for any pending renders, for smoother tween
            EventPage(otherPage).needMessageCenter = false;
            ApplicationController.instance.wireController.showWiresForLevel();  // hide wires for the "other" MC
            tweenForEditNow(pageImage, topLeft, botRight, token, onComplete);
        });
    }

    private function tweenForEditNow(pageImage:EventImageView, topLeft:Point, botRight:Point, token:Object, onComplete:Function):void
    {
        topLeft = _holder.globalToLocal(topLeft);
        botRight = _holder.globalToLocal(botRight);
        var size:Point = botRight.subtract(topLeft);
        var destScale:Number = size.x / computeImageWidth;
        var canvas:Rectangle = EditorUI.instance.canvasVisibleRect(this);
        _flow.doingEditTween = true;
        _renderDisabled = true;
        tweenViewState(pageImage, true, topLeft, destScale, canvas, tweenTimeEdit, function():void{
                _context.stage.addEventListener(Event.ENTER_FRAME, function(e:Event):void{
                    _context.stage.removeEventListener(Event.ENTER_FRAME, arguments.callee);   // use enter-frame so Tweener won't catch my errors
                    if (onComplete != null) {
                        onComplete();   // sets flowVisible = false ...
                    }
                    placeControlBars();  // ... hidden since flowVisible=false
                    for each (var item:EventImageView in _items) {
                        item.alpha = 1.0;
                    }
                    _flow.doingEditTween = false;
                    _renderDisabled = false;
                    requestUpdateSize(); // in case app-resize during tween
                    ApplicationController.instance.enableInput(token);
                });
            });
    }

    public function tweenBackFromEdit(page:EventPage, onComplete:Function = null):void
    {
        var token:Object = ApplicationController.instance.disableInput("tweenBackFromEdit");
        assert(!_flow.doingEditTween);  // if tween fails to finish, its probably because updateFlow was called during tween, and removed the tween from the image
        assert(!_renderDisabled);

        scrollToRevealPage(page);
        updateStacks(false, false);  //get the right views into the stacks  (tween=false, updateMessageCenters=false)

        // capture bitmap of other-page MC
        var otherPage:EventPage = (page == leftPage) ? rightPage : leftPage;
        var otherImage:EventImageView = getModelView(otherPage);
        _bitmapOfOtherMC = captureMessageCenterBitmap(otherImage);
        if (_bitmapOfOtherMC) {
            addChild(_bitmapOfOtherMC);
            var point:Point = computeItemCenter(otherImage);
            _bitmapOfOtherMC.x = point.x - MessageCenterView.WIDTH / 2;
            _bitmapOfOtherMC.y = 9999999;  // prevent flash at current MC location (it will be placed by tweenViewState)
        }
        otherPage.needMessageCenter = false;

        ApplicationController.instance.requestEnterFrameAction(function():void{
            tweenBackFromEditNow(page, token, onComplete);
        })
    }

    private function tweenBackFromEditNow(page:EventPage, token:Object, onComplete:Function):void
    {
        // first snap it to the off-screen configuration, in case the app window size has changed or in case tweenForEditEvent wasn't done in the first place
        // ... then tween to flow view state
        var frame:EventFlowView = _flow.eventPagerView;
        var viewToEdit:EventImageView = getModelView(page);
        var topLeft:Point = _holder.globalToLocal(frame.localToGlobal(new Point(0,0)));
        var botRight:Point = _holder.globalToLocal(frame.localToGlobal(new Point(page.width, page.height)));
        var size:Point = botRight.subtract(topLeft);
        var destScale:Number = size.x / computeImageWidth;
        var canvas:Rectangle = EditorUI.instance.canvasVisibleRect(this);
        tweenViewState(viewToEdit, true, topLeft, destScale, canvas, 0);     // goingToEdit=true, time=0
        topLeft = computeItemTopLeft(viewToEdit);
        _flow.doingEditTween = true;
        _renderDisabled = true;    // ensure render() won't remove the tweens
        Application.instance.flowVisible = true;  // calls render()
        WorldContainer(context).feedbackUIView.visible = true; // if running, let the MC show during tween back (pause will show it soon anyway)

        tweenViewState(viewToEdit, false, topLeft, 1.0, canvas, tweenTimeEdit, function():void{
            _flow.doingEditTween = false;
            _renderDisabled = false;
            requestUpdateSize(); // in case app-resize during tween
            updateView();
            ApplicationController.instance.requestEnterFrameAction(function():void{
                if (onComplete != null) {
                    onComplete();
                }
                ApplicationController.instance.enableInput(token);
            });
        });
    }

    private function tweenViewState(viewToEdit:EventImageView, goingToEdit:Boolean, topLeft:Point, destScale:Number, canvas:Rectangle, tweenTime:Number, onComplete:Function=null):void
    {
        viewToEdit.alpha = 1;
        Tweener.addTween(viewToEdit, {itemX:topLeft.x, itemY:topLeft.y, scaleX:destScale, scaleY:destScale, time:tweenTime, transition:tweenTypeEdit, onComplete:onComplete});

        var rightViewToEdit:Boolean = (viewToEdit == rightView);
        var otherView:EventImageView = (rightViewToEdit) ? leftView : rightView;
        var leftBarDest:Point, rightBarDest:Point;
        var leftBarWidth:Number, rightBarWidth:Number;
        var leftStageControls:Number, rightStageControls:Number;
        if (goingToEdit) {
            var stageBar:Point = globalToLocal(_flow.eventPagerView.localToGlobal(new Point(0, EventFlowView.TITLE_BAR_Y_OFFSET)));
            if (rightViewToEdit) {
                rightBarDest = stageBar;
                rightBarWidth = _flow.width;
                rightStageControls = 1.0;
                leftBarDest = new Point(canvas.left - computeImageWidth, computeControlBarY);
                leftBarWidth = computeImageWidth;
                leftStageControls = 0;
            } else {  // edit left page
                leftBarDest = stageBar;
                leftBarWidth = _flow.width;
                leftStageControls = 1.0;
                rightBarDest = new Point(canvas.right, computeControlBarY);
                rightBarWidth = computeImageWidth;
                rightStageControls = 0;
            }
        } else {  // going to flow
            leftBarDest = computeItemTopLeft(leftView);
            rightBarDest = computeItemTopLeft(rightView);
            leftBarDest.y += CONTROL_BAR_OFFSET;
            rightBarDest.y += CONTROL_BAR_OFFSET;
            leftBarWidth = computeImageWidth;
            rightBarWidth = computeImageWidth;
            leftStageControls = 0;
            rightStageControls = 0;
        }
        Tweener.addTween(_leftControlBar, {x:leftBarDest.x, y:leftBarDest.y, width:leftBarWidth, stageControlsFrac:leftStageControls, time:tweenTime, transition:tweenTypeEdit});
        Tweener.addTween(_rightControlBar, {x:rightBarDest.x, y:rightBarDest.y, width:rightBarWidth, stageControlsFrac:rightStageControls, time:tweenTime, transition:tweenTypeEdit});

        var scrollbarY:Number = (goingToEdit) ? (canvas.bottom+BELOW_CANVAS) : (computeScrollbarY);
        var destMessageCenterY:Number = scrollbarY - SCROLLBAR_Y_OFFSET + MESSAGE_CENTER_Y_OFFSET;   // editing MC goes here when at flow
        var otherMessageCenterY:Number = destMessageCenterY;
        if (tweenTime > 0 && !goingToEdit) {     // test by adding custom ribbons to make the two MCs different heights
            otherMessageCenterY = Math.min(otherMessageCenterY, canvas.bottom - ObjectView.MC_EDGE - otherView.messageCenterView.height - MessageCenterView.OBJECT_PADDING);  // keep MC bitmap within window bounds, similar to ObjectView.updateMessageCenterPosition
        }
        Tweener.addTween(_scrollbar, {y:scrollbarY, time:tweenTime, transition:tweenTypeEdit});

        var leftX:Number = computeImageX(_flow.leftPageNumber, 0);   // topLeft of image at top of the left stack
        var rightX:Number = computeImageX(_flow.rightPageNumber, 0);
        for (var n:int = _holder.numChildren-1; n >= 0; n--) {
            var item:EventImageView = _holder.getChildAt(n) as EventImageView;
            var page:EventPage = item.object as EventPage;
            var point:Point = computeItemTopLeft(item);
            if (item != viewToEdit) {
                item.scaleX = 1.0;  // in case edit Ev2, then Run, then tween back
                item.scaleY = 1.0;
                item.itemY = point.y;
                if (goingToEdit) {   // going to edit:  move image just outside the stage
                    var leftSide:Boolean = _flow.getModelIndex(page) < _flow.getModelIndex(viewToEdit.page);
                    var offsetX:Number = (leftSide) ? canvas.left - computeImageWidth - leftX : canvas.right - rightX;
                    Tweener.addTween(item, {itemX:point.x + offsetX, alpha:0.0, time:tweenTime, transition:tweenTypeEdit});
                } else {
                    item.alpha = 0;
                    Tweener.addTween(item, {itemX:point.x, alpha:1.0, time:tweenTime, transition:tweenTypeEdit});
                }
            }
            var mc:MessageCenterView = item.messageCenterView;
            page.needMessageCenter = (item == viewToEdit);  // hide "other" MC since we are showing bitmap
            if (mc && (item == leftView || item == rightView)) {  // animate the MC for the two top views
                if (item == viewToEdit) {
                    if (!page.messageCenterRepositioned) {
                        item.undockMessageCenter(item.computeMessageCenterPosition());   // undock so we can animate by setting page.xMessageCenter
                    }
                    tweenMCBeingEdited(item, goingToEdit, mc, destMessageCenterY, tweenTime);
                } else if (_bitmapOfOtherMC) {  // the other top view
                    tweenOtherMC(item, (canvas!=null), otherMessageCenterY, tweenTime);
                }
            }
        }
    }

    private function tweenMCBeingEdited(view:EventImageView, goingToEdit:Boolean, mc:MessageCenterView, destMessageCenterY:Number, tweenTime:Number):void
    {
        var page:EventPage = view.page as EventPage;
        var mcPoint:Point;
        if (goingToEdit) {
            mcPoint = view.messageCenterPositionBelowStage;
        } else {      // returning from edit
            mcPoint = computeItemCenter(view);
            mcPoint.y = destMessageCenterY;
            mcPoint = mc.parent.globalToLocal(this.localToGlobal(mcPoint));
        }
        Tweener.addTween(view.object, {xMessageCenter:mcPoint.x, yMessageCenter:mcPoint.y, time:tweenTime, transition:tweenTypeEdit, onComplete:function():void{
            if (tweenTime > 0) {  // ignore the initial snap-back
                finishTweenMCForEdit(view, goingToEdit, true);
            }
        }});
    }

    private function tweenOtherMC(view:EventImageView, goingToEdit:Boolean, destMessageCenterY:Number, tweenTime:Number):void
    {
        var page:EventPage = view.object as EventPage;
        destMessageCenterY += MessageCenterView.OBJECT_PADDING;  // account for difference between obj.yMessageCenter and the actual MC.y
        Tweener.addTween(_bitmapOfOtherMC, {y:destMessageCenterY, time:tweenTime, transition:tweenTypeEdit, onComplete:function():void{
            if (tweenTime > 0) {  // don't remove MC bitmap on the initial snap-back
                removeChild(_bitmapOfOtherMC);
                _bitmapOfOtherMC = null;
                finishTweenMCForEdit(view, goingToEdit, false);
            }
        }});
    }

    private function finishTweenMCForEdit(item:EventImageView, goingToEdit:Boolean, editedItem:Boolean):void
    {
        var page:EventPage = item.page as EventPage;
        // both the edited and the "other" view will come here, And after edit-tween & after tween-back-to-flow.
        page.needMessageCenter = !(goingToEdit && !editedItem);  // hide "other" MC while editing
        page.dockMessageCenterToStage = (goingToEdit && editedItem);
        item.dockMessageCenter();
        updateMCposition(item);
    }

    public function scrollToRevealPage(page:IPage, tween:Boolean = false):void
    {
        var flow:EventFlow = page.object.parent as EventFlow;
        var index:int = flow.getIndexForPage(page as EventPage);
        Utilities.assert(index >= 0);
        if (index >= 0) {
            _renderDisabled = true;  // prevent a subsequent render from wiping out the tweens created below
            var tied:Boolean = (flow.leftPageNumber+1 == flow.rightPageNumber);
            if (index > flow.rightPageNumber) {
                flow.rightPageNumber = index;
                if (tied) {
                    flow.leftPageNumber = index -1;
                }
            } else if (index < flow.leftPageNumber) {
                flow.leftPageNumber = index;
                if (tied) {
                    flow.rightPageNumber = index +1;
                }
            }
            _renderDisabled = false;
        }

        if (visible) {
            updateFlow(tween);
            updateMCposition(leftView);
            updateMCposition(rightView);
        }
    }

    private function updateMCposition(view:EventImageView):void
    {
        var mc:MessageCenterView = view.messageCenterView;
        if (mc && mc.parent) {
            var area:Rectangle = EditorUI.instance.messageCenterPlacementArea(mc.parent);
            mc.updatePosition(area);
        }
    }

    private function captureMessageCenterBitmap(view:EventImageView):Sprite
    {
        if (wiringLevel < 1) {
            return null;  // no MC should show
        }
        // bitmap of MC, with x,y set to local coordinates matching the original MC location
        var page:EventPage = view.page as EventPage;
        page.needMessageCenter = true;
        updateMCposition(view);  // sets mc.mainContainer.visible = true  (else MC fails to appear first time)
        var mc:MessageCenterView = view.messageCenterView;
        if (mc) {
            const EXTRA:Number = 10;  // ensure the outline of the MC is captured
            var ww:Number = mc.width + 2 * EXTRA;
            var hh:Number = mc.height + 2 * EXTRA;
            var matrix:Matrix = new Matrix();
            matrix.translate(EXTRA, EXTRA);
            var bmd:BitmapData = new BitmapData(ww, hh, true, 0x000000ff);
            bmd.draw(mc, matrix, null, null, null, true);
            var bitmap:Bitmap = new Bitmap(bmd,  PixelSnapping.AUTO, true);
            var result:Sprite = new Sprite();
            result.addChild(bitmap);
            bitmap.x = -EXTRA;
            bitmap.y = -EXTRA;
            var point:Point = this.globalToLocal(mc.localToGlobal(new Point(0,0)));
            result.x = point.x;
            result.y = point.y;
            return result;
        } else {
            return null;
        }
    }

    private function createPageMessageCenter(item:EventImageView):void
    {
        var obj:AbstractObject = item.object;
        obj.complete = true;
        obj.messageCenter = true;
    }

    public function get sourceObj():Pager
    {
        return _flow;
    }

    public function get scrollbar():FlowScrollbar
    {
        return _scrollbar;
    }


    public function get leftControlbar():FlowControlbar
    {
        return _leftControlBar;
    }

    public function get rightControlbar():FlowControlbar
    {
        return _rightControlBar;
    }

    public function get leftPage():EventPage
    {
        return _flow.getPageForIndex(_flow.leftPageNumber);
    }

    public function get rightPage():EventPage
    {
        return _flow.getPageForIndex(_flow.rightPageNumber);
    }

    public function get leftView():EventImageView
    {
        return getItemView(_flow.leftPageNumber);
    }

    public function get rightView():EventImageView
    {
        return getItemView(_flow.rightPageNumber);
    }

    public function get mediator():FlowEditingMediator
    {
        return _mediator;
    }

    public static function get tweenTimeEdit():Number
    {
        return enableSlowTween ? 7 : TWEEN_TIME_EDIT;
    }

    public static function get tweenTypeEdit():String
    {
        return enableSlowTween ? "linear" : TWEEN_TYPE_EDIT;
    }

    private static function get enableSlowTween():Boolean
    {
        return Keyboard.capsLock && TaconiteFactory.getEnvironmentImplementation().username == "sbirth";
    }

    override public function getOnStageChildrenBelow(subjectView:ObjectView=null, path:Array=null):Vector.<ObjectView> {
        var total:int = _holder.numChildren
        var result:Vector.<ObjectView> = new Vector.<ObjectView>(total);
        for(var i:int = 0; i < total; i++) {
            result[i] = _holder.getChildAt(i) as ObjectView;
        }
        return result;
    }

    private function get wiringLevel():int {
        return ApplicationController.instance.wireController.getWiringLevel();
    }

    // disable features of ObjectView
    override protected function setDragMediator():void {};
    override protected function recalcVisibility():void {};
    override protected function set rolloverGlow(value:Boolean):void {};
    override protected function updateFilters():void {};
    override protected function addMessageCenter():void {};
    override public function updateViewPosition():void {};
    override protected function updateStatus():void {};
    override protected function createFeedbackView():ITaconiteView {return null};


    private function assert(condition:*):void
    {
        if (condition==null || condition==false)
            throw new Error("FlowView");
    }

}
}

