/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 5/28/12
 * Time: 9:57 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {

import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.action.ActionGroup;
import com.alleni.author.definition.action.ModifyObjectProperty;
import com.alleni.author.definition.action.PageNumberInfo;
import com.alleni.author.definition.action.PageOrderAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.view.objects.ArenaControlBar;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.ModelCollectionEvent;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.Sprite;
import flash.events.MouseEvent;

import mx.events.PropertyChangeEvent;

public class FlowControlbarMediator {

    private var _bar:FlowControlbar;
    private var _context:ViewContext;
    private var _flow:EventFlow;
    private var _page:IPage;
    private var _controller:PagerController;
    private var _authorController:AuthorController;

    public function FlowControlbarMediator(flow:EventFlow, context:ViewContext) {
        _context = context;
        _flow = flow;
        _controller = _flow.controller as PagerController;
        _authorController = ApplicationController.instance.authorController;
    }

    public function handleViewEvents(bar:FlowControlbar):void
    {
        _bar = bar;
        _flow.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, flowPropertyChange);
    }

    public function clearViewEvents():void
    {
        _flow.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, flowPropertyChange);
        page = null;
    }
    
    private function flowPropertyChange(event:PropertyChangeEvent):void {
        if (_bar.isTitleBar) {
            switch (event.property) {
                case "editingPage": // enable next/prev buttons (when paused you can page to a master)
                    _bar.updateButtons();
                    break;
            }
        }
    }

    public function get page():IPage
    {
        return _page;
    }

    public function set page(value:IPage):void
    {
        if (value != _page) {
            if (_page) {
                _page.object.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, pagePropertyChange);
                _page.object.model.removeEventListener(ModelEvent.MODEL_CHANGE, handleModelChange);
            }
            if (value) {
                _page = value;
                _page.object.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, pagePropertyChange);
                _page.object.model.addEventListener(ModelEvent.MODEL_CHANGE, handleModelChange);
            }
            _bar.updateButtons();
        }
    }

    private function pagePropertyChange(event:PropertyChangeEvent):void {
        switch (event.property) {
            case "title":
            case "titleChangedByAuthor":
            case "isForeground":
                _bar.update();
                break;
        }
    }

    private function handleModelChange(e:ModelEvent):void
    {
        var ce:ModelCollectionEvent = e as ModelCollectionEvent;
        if (ce && ce.parent.value == _page && !_page.object.loading) {
            switch (ce.property) {
                case "masters":
                    _bar.update();
                    break;
            }
        }
    }

    public function handleEventsForLabel(foregroundBackgroundLabel:Sprite):void
    {
        foregroundBackgroundLabel.addEventListener(MouseEvent.CLICK, foregroundBackgroundLabelClick);
    }

    public function editPage(event:MouseEvent):void
    {
        event.stopImmediatePropagation();
        _authorController.editEvent(page as EventPage, true);
        // don't hilite button here; its done in tweenForEdit
    }

    private function updateButtonHilite(running:Boolean):void
    {
        _bar.hiliteButton(FlowControlbar.RUN_BUTTON, running);
        _bar.hiliteButton(FlowControlbar.EDIT_BUTTON, !running);
    }

    public function resumePage(event:MouseEvent):void
    {
        event.stopImmediatePropagation();
        runOrResume(true);
    }

    public function runPage(event:MouseEvent):void
    {
        event.stopImmediatePropagation();
        runOrResume(false);
    }

    private function runOrResume(resume:Boolean):void
    {
        var index:int = _flow.pages.getItemIndex(page);
        if (index >= 0) {
            _authorController.runEventCommand(index+1, resume);
            ArenaControlBar.updateAllControlbarVisibility();
        }
        updateButtonHilite(true);
    }

    public function pageForward(event:MouseEvent):void
    {
        event.stopImmediatePropagation();
        AuthorController.instance.gotoNextPage(+1, true);
    }

    public function pageBackward(event:MouseEvent):void
    {
        event.stopImmediatePropagation();
        AuthorController.instance.gotoNextPage(-1, true);
    }

    public function deletePage(event:MouseEvent):void
    {
        event.stopImmediatePropagation();
        if (!FlowEditingMediator.lastRemainingPage(page)) {
            _authorController.log("deleteEventPage",new <TaconiteModel>[page.object.model]);
            _controller.deletePage(_flow, page);
        } else if (page.isMaster) {
            message("You cannot delete the only master.");
        } else {
            message("You cannot delete the only event.");
        }
    }

    public function addPageBefore(event:MouseEvent):void
    {
        event.stopImmediatePropagation();
        addPage(false, event.altKey);  // after=false
    }

    public function addPageAfter(event:MouseEvent):void
    {
        event.stopImmediatePropagation();
        addPage(true, event.altKey);
    }

    private function addPage(after:Boolean, duplicate:Boolean):void
    {
        if ((page.isMaster) ? (_flow.masterPages.length < EventFlow.MAX_MASTERS) : (_flow.pages.length < EventFlow.MAX_PAGES)) {
            _authorController.addEventPageBy(page, after, duplicate);
        } else if (page.isMaster) {
            message("You cannot have more than 12 masters.");
        } else {
            message("You cannot have more than 100 events.");
        }
    }

    private function foregroundBackgroundLabelClick(e:MouseEvent):void
    {
        if (page.isMaster) {
            page.isForeground = !page.isForeground;  // toggle foreground
            commitForegroundChange();
        }
    }

    public function commitForegroundChange():void
    {
        // commit the change of "isForeground" on this page
        // and reorder masters as needed to ensure all background masters are first
        // and scroll the flow as needed to ensure left and right pages are still showing
        var newValue:Boolean = page.isForeground;
        var oldIndex:int = _flow.masterPages.getItemIndex(page);
        var newIndex:int = oldIndex;
        var oldLeft:int = _flow.leftPageNumber;
        var newLeft:int = oldLeft;
        var oldRight:int = _flow.rightPageNumber;
        var newRight:int = oldRight;

        if (newValue) {  // switched to foreground: put this at the boundary of foreground / background masters
            var hb:int = highestBackgroundIndex;
            if (hb >= 0) {
                newIndex = (oldIndex < hb) ? hb : hb+1;
            }
        } else {
            var lf:int = lowestForegroundIndex;
            if (lf >= 0) {
                newIndex = (oldIndex > lf) ? lf : lf-1;
            }
        }
        if (oldLeft == oldIndex) {
            newLeft = newIndex;
            if (newRight <= newLeft) {
                newRight = newLeft+1;
            }
        } else if (oldRight == oldIndex) {
            newRight = Math.max(newIndex, 1);
            if (newRight <= newLeft) {
                newLeft = newRight-1;
            }
        }
        var newPageNums:PageNumberInfo = new PageNumberInfo(_flow.pageNumber, _flow.initialPageNumber, newLeft, newRight);
//        trace("commitForegroundChange f="+page.isForeground, "newIndex="+newIndex, "newLeft="+newLeft, "newRight="+newRight);
        var group:ActionGroup = new ActionGroup("Set Foreground");
        group.add(ModifyObjectProperty.fromObject(page.object, "Set Foreground", "isForeground", -1, !newValue));
        group.add(PageOrderAction.fromObject(page, "Reorder page", newIndex, newPageNums));
        group.perform();
        _authorController.currentActionTree.commit(group);
    }

    private function get highestBackgroundIndex():int
    {
        for (var n:int = _flow.masterPages.length-1; n >= 0; n--) {
            var master:IPage = _flow.masterPages[n];
            if (!master.isForeground) {
                return n;
            }
        }
        return -1;
    }

    private function get lowestForegroundIndex():int
    {
        for (var n:int = 0; n < _flow.masterPages.length; n++) {
            var master:IPage = _flow.masterPages[n];
            if (master.isForeground) {
                return n;
            }
        }
        return -1;
    }

    public function handleEventsForTile(tile:FlowMasterTile):void
    {
        tile.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownTile);
        tile.addEventListener(MouseEvent.ROLL_OVER, mouseOverTile);
        tile.addEventListener(MouseEvent.ROLL_OUT, mouseOutTile);
    }


    public function mouseDownTile(event:MouseEvent):void
    {
        var tile:FlowMasterTile = event.target as FlowMasterTile;
        if (tile) {
            toggleAssignMaster(tile.index);
            _bar.tileToHilite = null; // remove hilite so author can see assignment
        }
    }

    public function mouseOverTile(event:MouseEvent):void
    {
        var tile:FlowMasterTile = event.target as FlowMasterTile;
        if (tile && tile.index < _flow.masterPages.length) {
            _bar.tileToHilite = tile;
            var master:IPage = _flow.masterPages[tile.index];
            showRolloverMaster(master);
            dumpDebugInfoForTile(master);
        }
    }


    public function mouseOutTile(event:MouseEvent):void
    {
        hideRolloverMaster();
        var tile:FlowMasterTile = event.target as FlowMasterTile;
        if (tile && tile == _bar.tileToHilite) {
            _bar.tileToHilite = null;
        }
    }

    private function showRolloverMaster(master:IPage):void
    {
        if (master != _flow.masterShownForRollover) {
            hideRolloverMaster();
        }
        if (page.masters.getItemIndex(master) < 0) {  // only if its not already assigned
            _flow.masterShownForRollover = master;
            _controller.assignMaster(master, page, true);  // same as a real assign, but don't Commit to undo log
        }
    }

    private function hideRolloverMaster():void
    {
        if (_flow.masterShownForRollover) {
            _controller.assignMaster(_flow.masterShownForRollover, page, false);
            _flow.masterShownForRollover = null;
        }
    }

    private function toggleAssignMaster(index:int):void {
        hideRolloverMaster();

        if (index < _flow.masterPages.length) {
            var master:IPage = _flow.masterPages[index];
            var assign:Boolean = (page.masters.getItemIndex(master) < 0);
            _authorController.log(assign ? "assignMaster" : "unAssignMaster", new <TaconiteModel>[master.object.model]);
            _controller.assignMaster(master,  page, assign, true); // commit=true    (note: PseudoThread used inside)
        }
    }

    private function get titleBarOverStage():FlowControlbar
    {
        return _flow.eventPagerView.titleBarOverStage;
    }

    private function message(str:String):void
    {
        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, [str]));
    }

    private function dumpDebugInfoForTile(master:IPage):void
    {
//        return;

        trace("-");
        trace("Flow pageNumber="+_flow.pageNumber, "init="+_flow.initialPageNumber);
        ApplicationController.instance.wireController.showWiresForLevel();
        var ep:EventPage = page as EventPage;
        if (ep) {
            var ev:EventImageView = _flow.flowView.getModelView(page);
            if (ev) {
                trace("page="+ep, "title="+ep.title, "changeByAuthor="+ep.titleChangedByAuthor, "pageNum="+ep.pageNumber);
                trace("  needMC="+ep.needMessageCenter, "MCvis="+ep.messageCenterVisible, "docked="+(!ep.messageCenterRelative), "MC.xy="+ep.xMessageCenter,ep.yMessageCenter);
                trace("image="+ev);
                trace("  x="+ev.x, "y="+ev.y, "alpha="+ev.alpha);
                trace("MC="+ev.messageCenterView);
                if (ev.messageCenterView) {
                    trace("  mc.vis="+ev.messageCenterView.visible);
                }
                ApplicationController.instance.dumpDisableInputTokens();
            }
            trace("objects.length="+ep.objects.length, "hasInternals="+ep.hasInternals);
        }
        PagerController.instance.dumpMasters(page);
    }

    private function assert(condition:*):void
    {
        if (condition==null || condition==false)
            throw new Error("FlowBarBaseMediator");
    }

}
}
