/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 4/18/12
 * Time: 4:37 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.objects.IEditingMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Objects;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.model.TaconiteObject;
import com.alleni.taconite.view.ITaconiteView;
import com.alleni.taconite.view.TaconiteViewFactory;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObjectContainer;

import flash.display.Sprite;
import flash.geom.Point;

public class BackstageView extends ObjectView {

    private var _flow:EventFlow;
    private var _childContainer:Sprite;


    public function BackstageView(context:ViewContext, model:TaconiteModel, role:uint)
    {
        super(context, model, role);
        _flow = object as EventFlow;
        preventMessageCenter = true;

        _childContainer = new Sprite();
        addChild(_childContainer);
        trace("Backstage:",this);
    }

    override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
    {
        if (property == "visible") {
            // ignore this because we are controlling the view visibility directly in Application.set flowVisible
            return false;
        }
        return super.updateModelProperty(property, oldValue, newValue);
    }

    override public function get childViewContainer():DisplayObjectContainer
    {
        return _childContainer;  // enable ObjectView to automatically add and delete child views
    }

    private function get backstageIsActive():Boolean
    {
        return Application.instance.flowVisible && Application.uiRunning == false;
    }

    override protected function get accepting():Boolean
    {
        return backstageIsActive;
    }

    override public function get releasing():Boolean
    {
        return backstageIsActive;
    }

    override public function exactHitTest(stagePoint:Point):Boolean
    {
        return backstageIsActive;
    }

    override public function hitTestPoint(x:Number, y:Number, shapeFlag:Boolean = false):Boolean {
//        trace("backstage.hitTestPoint backActive="+backstageIsActive, "stage="+stage);
        return backstageIsActive;
    }

    override public function handleRunningChanged():void
    {
        // not calling super
        recalcVisibility();
    }

    override protected function recalcVisibility():void
    {
        // visibility is set in function set flowVisible
    }

    override protected function draw():void
    {
//        with (graphics) {          // red square for debug
//            clear();
//            beginFill(0xff0000);
//            drawRect(-30,-30, 30, 30);
//            endFill();
//        }
    }

    // disable features of ObjectView
    override protected function setDragMediator():void {};
    override protected function set rolloverGlow(value:Boolean):void {};
    override protected function updateFilters():void {};
    override protected function addMessageCenter():void {};
    override public function updateViewPosition():void {};
    override protected function updateStatus():void {};
    override protected function createFeedbackView():ITaconiteView {return null};

}
}
