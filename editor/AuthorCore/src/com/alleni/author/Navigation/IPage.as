/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/29/12
 * Time: 7:48 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import mx.collections.IList;

public interface IPage {
    function get pageNumber():int;
    function set pageNumber(val:int):void;
    function get objects():IList;
    function get hadChildren():Boolean;
    function set hadChildren(val:Boolean):void;
    function get object():AbstractContainer;
    function get isMaster():Boolean;
    function get isForeground():Boolean;
    function set isForeground(value:Boolean):void;
    function get moveWithTransition():Boolean;
    function set moveWithTransition(value:Boolean):void;
    function get masters():IList;
}
}
