/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/30/12
 * Time: 5:49 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.objects.ContainerController;
import com.alleni.author.controller.objects.IReplaceableReceiver;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.controller.ui.ReplacementController;
import com.alleni.author.definition.DragInfo;
import com.alleni.author.definition.GadgetDescription;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.view.objects.ArenaControlBar;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.model.TaconiteModel;

public class SmartObjectController extends ContainerController {

    public function SmartObjectController() {
        super();
    }
    

    override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
    {		
        super.handleModelUpdate(model,property,newValue,oldValue,parent);
        var smart:SmartObject = model.value as SmartObject;
			
        // check to see if our asset has been reset
        switch (property) {
            case "gadget":
                handleGadgetUpdatedFor(smart, oldValue as Project);
                break;
        }
    }
		
    override protected function handleModelChange(kind:String, parent:TaconiteModel, child:TaconiteModel, index:int):void
    {
        super.handleModelChange(kind,parent,child,index);
        switch(kind)
        {
            case ModelEvent.ADD_CHILD_MODEL:
                // when an arena (or gadget containing arena) is moved onto the velum (or drawn on the vellum),
                // ... it will need to show its control bar
                ArenaControlBar.updateAllControlbarVisibility();
                break;
        }
    }
		
    private function handleGadgetUpdatedFor(smart:SmartObject, oldGadget:Project):void
    {
        var gadget:Project = smart.gadget;
			
        if (gadget) {
            // if we didn't have a gadget before, or if the title is the default from the gadget name, update the title.
            // this will avoid updating the title with the gadget name if the author has set their own object title.
            if (!oldGadget || !smart.titleChangedByAuthor)
                setSafeTitle(smart);
        }
    }
		
    private function setSafeTitle(smart:SmartObject):void
    {
        const gadget:Project = smart.gadget;
        if (!smart || !gadget) return;
        smart.title = GadgetDescription.cleanName(gadget);
    }
		
    protected function get cleanSelection():Vector.<TaconiteModel>
    {
        return ApplicationController.instance.authorController.cleanSelection;
    }
		
    protected function get cleanSelectionIncludingLocked():Vector.<TaconiteModel>
    {
        return DragInfo.toModels(ApplicationController.instance.authorController.currentEditingMediator.getSelection(false, false, false));
    }


    public static function trimStrayWires(smart:SmartObject):void
    {
        var children:Array = GadgetEncoder.collectChildren(smart, null, true, false);  // ids=null, recurse=true
        var compositeIndex:int = children.indexOf(smart);
        if (compositeIndex >= 0)
            children.splice(compositeIndex, 1);
        var wires:Array = ApplicationController.instance.wireController.getWiresInclusive(children);
        var wire:Wire;
        for each (wire in wires) {
            if (wire.masterAnchor.wiringScope != smart || wire.slaveAnchor.wiringScope != smart)
                deleteWire(wire);
        }
    }

    private static function deleteWire(wire:Wire):void
    {
        ApplicationController.instance.wireController.deleteWire(wire);
        wire.masterAnchor.wired = ApplicationController.instance.wireController.getWiresForAnchor(wire.masterAnchor).length>0;
        wire.slaveAnchor.wired = ApplicationController.instance.wireController.getWiresForAnchor(wire.slaveAnchor).length>0;
    }


    public static function okToEdit(smart:SmartObject):Boolean
    {
        if (smart.gadget == null) {
            return true;
        } else {
            return GadgetController.instance.confirmCanEdit(smart.gadget);
        }
    }

    override public function assetReplaceAll(obj:AbstractObject, oldAsset:IReplaceable, newAsset:IReplaceable):void
    {
        super.assetReplaceAll(obj, oldAsset, newAsset);
        if (obj.editing || editWhileRunning(obj)) {
            obj.allObjectsInScope(function(ao:AbstractObject):Boolean {
                if (ao != obj) {
                    ao.controller.assetReplaceAll(ao, oldAsset, newAsset);
                }
                return true;
            });
        }
    }

    override public function assetReplaceAllWhole(obj:AbstractObject, oldAsset:IReplaceable, newAsset:IReplaceable):void
    {
        if (obj.editing || editWhileRunning(obj)) {
            obj.allObjectsInScope(function(ao:AbstractObject):Boolean {
                if (ao != obj) {
                    var receiver:IReplaceableReceiver = ao as IReplaceableReceiver;
                    if (receiver && receiver.replaceable == oldAsset) {
                        replaceWholeObject(ao, newAsset);
                    }
                }
                return true;
            });
        }
    }

    private function replaceWholeObject(oldObj:AbstractObject, newAsset:IReplaceable):void
    {
        var newObj:AbstractObject;
        var gadget:Project = newAsset as Project;
        if (gadget) {
            newObj = AuthorController.instance.addObjectForName("Composite", 0,0,0,0,false, oldObj.parent);
            GadgetController.instance.configureContainerAndDisplay(gadget, newObj as SmartObject, oldObj.x, oldObj.y);
        } else {
            newObj = AssetController.instance.configureContainerAndDisplay(newAsset as Asset, oldObj.x, oldObj.y, oldObj.parent);
        }

        PseudoThread.add(new PseudoThreadSimpleRunnable("replaceWholeObject", function():void {
            ReplacementController.replaceObjectWithUndo(newObj, oldObj);
        }));
    }

    private function editWhileRunning(obj:AbstractObject):Boolean
    {
        return obj is EventPage && Application.instance.quaziPauseModo;
    }
		    
}
}
