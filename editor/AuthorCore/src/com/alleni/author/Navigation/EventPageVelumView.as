/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/31/12
 * Time: 8:56 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.view.ui.WireboardView;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.Sprite;
import flash.geom.Rectangle;
import flash.ui.ContextMenu;

public class EventPageVelumView extends SmartObjectVelumView {

    private var _event:EventPage;
    private var _editMediator:EventPageEditingMediator;


    public function EventPageVelumView(context:ViewContext, eventPageView:EventPageView, objMediator:EventPageMediator) {

        super(context,  eventPageView.eventPage, objMediator);
        _event = eventPageView.eventPage;

        _editMediator = new EventPageEditingMediator(context, this, eventPageView, objMediator);
        drawVelum(_event.width, _event.height);
    }

    public function warnViewToDelete():void
    {
        _editMediator.clearViewEvents();
        _editMediator = null;
    }


    override public function drawVelum(w:Number, h:Number):void
    {
        // don't do a fill here, since wireboards are in front, so a fill would prevent clicking child objects
        updateWireboardPositions(new Rectangle(0,0, w, h));
    }
}
}
