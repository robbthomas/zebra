/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 4/1/12
 * Time: 7:33 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.view.text.LightLabel;

import flash.display.Sprite;

import flashx.textLayout.formats.TextAlign;

public class EmptyPageView extends Sprite {

    private var _width:Number;
    private var _height:Number;
    private var _label:LightLabel = new LightLabel();
    private var _labelText:String = "";

    public function EmptyPageView(width:Number=1,  height:Number=1,  label:String = "") {
        _width = width;
        _height = height;
        _labelText = label;

        _label.color = 0x000000;
        addChild(_label);
        _label.x = 0;
        _label.textAlign = TextAlign.CENTER;
        _label.text = _labelText;
        updateView();
        alpha = 0.3;
    }

    public function get labelText():String
    {
        return _labelText;
    }

    public function set labelText(value:String):void
    {
        if (value != _labelText) {
            _labelText = value;
            _label.text = _labelText;
        }
    }

    override public function get width():Number
    {
        return _width;
    }

    override public function set width(value:Number):void
    {
        if (value != _width) {
            _width = value;
            updateView();
        }
    }

    override public function get height():Number
    {
        return _height;
    }

    override public function set height(value:Number):void
    {
        if (value != _height) {
            _height = value;
            updateView();
        }
    }

    public function setSize(w:Number, h:Number):void {
        if(w != _width || h != _height) {
            _width = w;
            _height = h;
            updateView();
        }
    }

    private function updateView():void
    {
        if (_width > 0 && _height > 0) {
            const THICK:Number = 6;
            with (graphics) {
                clear();
                beginFill(0x888888);
                drawRect(0,0, _width, _height);
                endFill();
                lineStyle(THICK, 0xffffff);
                moveTo(0+THICK, 0+THICK);
                lineTo(_width-THICK, _height-THICK);
                moveTo(0+THICK, _height-THICK);
                lineTo(_width-THICK, 0+THICK);
            }
            var fontSize:int = computeLabelFontsize;
            _label.size = fontSize;
            _label.width = _width;
            _label.y = (_height / 6) + 10 - fontSize;
        }
    }

    private function get computeLabelFontsize():int
    {
        var size:Number = _width / 5;
        size = Math.min(30, Math.max(9, size));
        return int(size);
    }

}
}
