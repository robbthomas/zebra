/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/30/12
 * Time: 8:28 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.model.AbstractObject;
import com.alleni.author.view.ObjectView;

public interface IPageView {
    function get objView():ObjectView;
    function get page():IPage;
    function get object():AbstractObject;
}
}
