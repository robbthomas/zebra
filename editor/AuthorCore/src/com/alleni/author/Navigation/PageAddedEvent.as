/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 5/3/12
 * Time: 12:58 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import flash.events.Event;

public class PageAddedEvent extends Event {

    public static const ADD_PAGE:String = "addPage";

    public var newPage:IPage;
    public var existingPage:IPage;
    public var after:Boolean;
    public var inside:Boolean;

    public function PageAddedEvent(type:String, newPage:IPage, existingPage:IPage, after:Boolean, inside:Boolean)
    {
        super(type);
        this.newPage = newPage;
        this.existingPage = existingPage;
        this.after = after;
        this.inside = inside;
    }

    override public function clone():Event
    {
        return new PageAddedEvent(type, newPage, existingPage, after, inside);
    }

}
}
