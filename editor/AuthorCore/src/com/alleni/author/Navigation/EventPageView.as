/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/30/12
 * Time: 5:48 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ITaconiteView;
import com.alleni.taconite.view.ViewContext;

import flash.display.Sprite;
import flash.geom.Point;

public class EventPageView extends SmartObjectView implements IPageView {

    private var _page:EventPage;
    private var _mediator:EventPageMediator;
    private var _editingBackground:Sprite;


    public function EventPageView(context:ViewContext, model:TaconiteModel, role:uint) {
        super(context, model, role);
        _page = model.value as EventPage;

        _editingBackground = new Sprite();
        addChildAt(_editingBackground,0); // behind child objects
        _mediator = new EventPageMediator(context, this);
        this.doubleClickEnabled = true;
    }

    override public function warnViewToDelete():void
    {
        super.warnViewToDelete();
        _mediator.clearViewEvents();
    }

    override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
    {
        if (property == "visible") {
            // ignore this because we are controlling the view visibility directly in PagerView, especially for masters
            return true;
        }

        var result:Boolean = super.updateModelProperty(property,oldValue,newValue);

        switch (property) {
            case "loading":
                if (newValue == false) {
                    afterLoad();
                }
                break;
            case "isMaster":
                return false;
        }
        return result;
    }

    private function afterLoad():void
    {
    }

    override public function acceptChild(subjectView:ObjectView, stagePoint:Point):ObjectView
    {
        var view:ObjectView = super.acceptChild(subjectView, stagePoint);
        trace("EventPageView: super.acceptChild="+view);
        if (view) {
            return view;
        } else {
            return this;
        }
    }

    private function get includeBackstage():Boolean
    {
        // includeBackstage: not the global backstage, just the page children drawn outside stage bounds
        return !Application.runningLocked;
    }


    override public function hitTestPoint(x:Number, y:Number, shapeFlag:Boolean = false):Boolean {
        if (includeBackstage) {
            return true;
        }
        var p:ObjectView = parentObjectView;
        if(p) {
            return p.hitTestPoint(x,  y, shapeFlag); // hit test against the arena if we have one
        } else {
            return true; // we have no parent, so assume hit if we are even bothering asking
        }
    }

    override public function exactHitTest(stagePoint:Point):Boolean {
        if (includeBackstage) {
            return true;
        }
        var p:ObjectView = parentObjectView;
        if(p) {
            return p.exactHitTest(stagePoint); // hit test against the arena if we have one
        } else {
            return true; // we have no parent, so assume hit if we are even bothering asking
        }
    }

    override public function openEditing():void
    {
        _mediator.openEditingThis();
    }

    public function restoreSelection():void
    {
        _mediator.restoreSelection();
    }

    override public function get width():Number
    {
        return object.width;
    }

    override public function get height():Number
    {
        return object.height;
    }


    // disable features of ObjectView
    override protected function set rolloverGlow(value:Boolean):void {};
    override protected function updateFilters():void {};
    override protected function addMessageCenter():void {};
    override protected function updateStatus():void {};
    override protected function createFeedbackView():ITaconiteView {return null};
    override protected function recalcVisibility():void {};
    override protected function setDragMediator():void {};
    override protected function setSelectionMediator():void {};


    public function get eventPage():EventPage
    {
        return object as EventPage;
    }

    public function get objView():ObjectView
    {
        return this;
    }

    public function get page():IPage
    {
        return eventPage;
    }

    public function get editingBackground():Sprite
    {
        return _editingBackground;
    }


    override public function toString():String
    {
        return "[" + _viewClassName + " \"" + object.title + "\" page="+eventPage.pageNumber + " #="+_serial + "]";
    }


}
}
