/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/29/12
 * Time: 5:57 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ObjectInlets;
import com.alleni.author.model.ObjectProperties;
import com.alleni.author.model.objects.*;
import com.alleni.author.model.ui.Application;
import com.alleni.taconite.dev.Utilities;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.events.PropertyChangeEvent;

public class  Pager extends AbstractContainer {

    private var _pageNumber:int = 1;

    [Transient] [Bindable] public var masterPages:ArrayCollection = new ArrayCollection();
    [Transient] [Bindable] public var pages:ArrayCollection = new ArrayCollection();

    [Bindable] public var historyStack:ArrayCollection = new ArrayCollection();  // array of int pageNumbers
    [Bindable] public var historyCursor:int = -1; // position when going back

    [Bindable] public var pageCount:int = 0;
    [Bindable] public var leftPageNumber:int = 0;
    [Bindable] public var rightPageNumber:int = 1;
    [Bindable] public var notFirstPage:Boolean;
    [Bindable] public var notLastPage:Boolean;
    [Bindable] public var transition:String = "none";
    [Bindable] public var transitionSecs:Number = 0;
    [Bindable] public var transTweenType:String = "linear";
    [Transient] public var setterTransition:String = null;
    [Transient] public var setterTransitionSecs:Number = 0;
    [Transient] public var setterTransTweenType:String = null;
    [Bindable] public var objCount:int = 0;
    [Bindable] public var resumeControlsVisible:Boolean; // When the project is paused what is the visible state of the arena controls when the project resumes.
    private var _controlsVisible:Boolean = true;
    private var _controlsCloaked:Boolean = true;
    [Bindable] public var controlsDocked:Boolean = true;
    [Bindable] public var controlsGlobalX:Number = 0;  // current controls location in global coordinates
    [Bindable] public var controlsGlobalY:Number = 0;
    [Bindable] public var cloakFrame:Boolean = false;

    [Bindable] [Transient]	public var editingPage:IPage;

    [Bindable]  public var gridVisible:Boolean = false;
    [Bindable]  public var gridMaximize:Boolean = false;
    [Bindable]	public var gridThumbHeight:Number=40;
    [Bindable]	public var gridX:Number;
    [Bindable]	public var gridY:Number;
    [Bindable]	public var gridWidth:Number = 200;
    [Bindable]	public var gridHeight:Number = 400;
    [Bindable]	public var gridTitles:Boolean = true;

    [Transient] [Bindable] public var transientTransitionInProgress:Boolean = false;
    [Transient] public var changePageAfterTransition:Boolean;   // coordinates timing of page-activation & lazyload with transition effects (both EventFlow & Arena)
    [Transient] public var oldPage:IPage = null;
    [Transient] public var newPage:IPage = null;

    [Transient] [Bindable] public var ignorePageChanges:Boolean;
    [Transient] [Bindable] public var ignorePageListChanges:Boolean;
    [Transient] [Bindable] public var rearranging:Boolean;
    [Transient] [Bindable] public var lazyLoading:Boolean;
    [Transient] [Bindable] public var replacingPage:IPage;
    [Transient] [Bindable] public var replacementPage:IPage;
    [Transient] [Bindable] public var pageHolderVisible:Boolean = true;

    [Transient] public var transientMirroredProperties:Vector.<String> = new Vector.<String>();
    [Transient] public var transientDoingCopyToPages:int = 0;
    [Transient] public var transientDoingCopyFromPage:int = 0;

    private var _preventTransitionEffect:Boolean;

    [Bindable]
    public function get controlsVisible():Boolean {
        return _controlsVisible;
    }

    public function set controlsVisible(value:Boolean):void {
        _controlsVisible = value;
        if(Application.running == false && !loading && _wireController.bindingsEnabled){  //This happens when we are setting initial value, works like pauseRunningState.
            resumeControlsVisible = !controlsCloaked && value;
        }
    }

    [Bindable]
    public function get controlsCloaked():Boolean {
        return _controlsCloaked;
    }

    public function set controlsCloaked(value:Boolean):void {
        _controlsCloaked = value;
        if(Application.running == false && !loading && _wireController.bindingsEnabled){  //This happens when we are setting initial value, works like pauseRunningState.
            resumeControlsVisible = !_controlsCloaked && !(initialValues.hasOwnProperty("controlsVisible") && (initialValues.controlsVisible == false));
        }
    }

    public function Pager() {

        super();

        // properties mirrored between Pager & the current IPage
//        addMirroredProperties(["objCount"]);
        ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("objCount"), "objCount");
        // end of mirrored

        ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("pageCount"), "pageCount");
        ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("pageNumber"), "pageNumber");
        ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("notFirstPage"), "notFirstPage");
        ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("notLastPage"), "notLastPage");
        ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("transition"), "transition");
        ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("transTweenType"), "transTweenType");
        ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("transitionSecs"), "transitionSecs");

        ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("goStartPage"));
        ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("goNextPage"));
        ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("goPrevPage"));
        ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("goEndPage"));
    }

    protected function addMirroredProperties(propNames:Array):void
    {
        // the input is an Array because the Arena list of layout props is also shared with inspector
        for each (var prop:String in propNames) {
            transientMirroredProperties.push(prop);
        }
    }

    override protected function initializeValues():void
    {
        _preventTransitionEffect = true;
        super.initializeValues();
        objCount = currentPage.objects.length;
        while (historyStack.length > 0) {
            historyStack.removeItemAt(0);
        }
        historyStack.addItem(pageNumber);
        historyCursor = -1;
        _preventTransitionEffect = false;
        PagerController(controller).afterRestoreInitialValues(this);
    }

    override protected function pauseRunningState():void {
        super.pauseRunningState();
        resumeControlsVisible = _controlsVisible;
        if(_controlsCloaked && initialValues.hasOwnProperty("controlsVisible") && (initialValues.controlsVisible == true) && Application.instance.editWhileRunningInProgress == false) {
            _controlsVisible = true;
        }
    }

    override protected function resumeRunningState():void {
        super.resumeRunningState();
        _controlsVisible = resumeControlsVisible;
    }

    [Transient] public function get currentPage():IPage
    {
        var index:int = pageNumber-1;
        if (index >= 0 && index < pages.length) {
            var page:IPage = pages.getItemAt(index) as IPage;
            Utilities.assert(page.object.parent)
            return page;
        }
        return null;
    }


    [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
    public function get pageNumber():int
    {
        return _pageNumber;
    }

    public function set pageNumber(value:int):void
    {
        if (value < 1) value = 1;
        if (!loading) {  // when not loading, limit the upper value
            if (value > pageCount)
                value = pageCount;
        }
        var oldPage:IPage = currentPage;
        var oldPageNumber:int = _pageNumber;
        if (value != oldPageNumber) {    // only dispatch event if value changed, and ensure only legal values are dispatched
            _pageNumber = value;
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "pageNumber", oldPageNumber, value));
            if (currentPage) {
                dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "currentPage", oldPage, currentPage));
            }
        }
    }

    public function setPageNumberWithoutTransitionEffect(value:int):void
    {
        _preventTransitionEffect = true;
        pageNumber = value;
        _preventTransitionEffect = false;
    }

    public function get preventTransitionEffect():Boolean
    {
        return _preventTransitionEffect;
    }

    public function getModelIndex(page:IPage):int
    {
        var index:int = masterPages.getItemIndex(page);
        if (index >= 0) {
            return index;
        }
        index = pages.getItemIndex(page);
        if (index >= 0) {
            return index + masterPages.length;
        } else {
            return -1;
        }
    }

    [Transient]
    public function get initialPageNumber():int
    {
        return ("pageNumber" in initialValues) ? initialValues["pageNumber"] : 1;
    }

    public function set initialPageNumber(value:int):void
    {
        if (value < 1) value = 1;
        if (!loading) {  // when not loading, limit the upper value
            if (value > pageCount)
                value = pageCount;
        }
        initialValues["pageNumber"] = value;
    }


    override protected function initializeRunningState():void {
        super.initializeRunningState();
        transientTransitionInProgress = false;
    }

    [Transient]
    public function get pageClassname():String
    {
        return null;
    }


}
}
