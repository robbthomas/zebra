/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 5/28/12
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.ui.TooltipRegistrar;
import com.alleni.author.definition.FlowTooltips;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.view.text.LightLabel;
import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
import com.alleni.taconite.view.ViewContext;

import flash.display.GradientType;

import flash.display.Graphics;

import flash.display.Shape;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.utils.Dictionary;


public class FlowControlbar extends Sprite {

    private var _flow:EventFlow;
    private var _context:ViewContext;
    private var _mediator:FlowControlbarMediator;
    private var _isTitleBar:Boolean;
    private var _tooltipRegistrar:TooltipRegistrar;
    private var _page:IPage;
    private var _leftHolder:Sprite;  // holds background/foreground slider & master-tiles for masking to avoid collision with pencil icon
    private var _leftMask:Shape;
    private var _titleBackground:Sprite;
    private var _titleTruncator:Sprite;
    private var _masterSlider:FlowMasterForegroundSlider;
    private var _title:LightLabel;
    private var _tilesHolder:Sprite;
    private var _tileToHilite:FlowMasterTile;
    private var _rightButtonsHolder:Sprite;
    private var _editButton:FlowButton;
    private var _resumeButton:FlowButton;
    private var _runButton:FlowButton;
    private var _pagingButtonsHolder:Sprite;
    private var _nextButton:FlowButton;
    private var _prevButton:FlowButton;
    private var _deleteButton:FlowButton;
    private var _addBeforeButton:FlowButton;
    private var _addAfterButton:FlowButton;
    private var _divider1:Sprite;
    private var _divider2:Sprite;
    private var _width:Number = 1;
    private var _stageControlsFrac:Number = 1;
    private var _buttonDictionary:Dictionary = new Dictionary();
    private var _coverDictionary:Dictionary = new Dictionary();

    public static const EDIT_BUTTON:String = "editButton";
    public static const RUN_BUTTON:String = "runButton";

    private static const MIN_WIDTH:Number = 200;
    private static const SCRUNCH_WIDTH:Number = 300; // for smaller width, place controls near the ends
    public static const CONTROL_BAR_HEIGHT:Number = 25;
    private static const TILES_INSET:Number = 26;
    private static const TILE_SPACING:Number = FlowMasterTile.WIDTH;

    private static const EVENT_COLOR:uint = 0x9a9a9a;
    private static const MASTER_COLOR:uint = 0x000000;
    private static const ALL_COLORS:Array = [EVENT_COLOR,MASTER_COLOR];

    private static const TITLE_BACKGROUND:String = "titleBackground";
    private static const TITLE_TRUNCATOR:String = "titleTruncator";
    private static const ALL_COVERS:Array = [TITLE_BACKGROUND,TITLE_TRUNCATOR];

    public function FlowControlbar(flow:EventFlow, context:ViewContext, isTitleBar:Boolean) {
        _tooltipRegistrar = new TooltipRegistrar();
        _flow = flow;
        _context = context;
        _isTitleBar = isTitleBar;
        _stageControlsFrac = isTitleBar ? 1.0 : 0;
        _mediator = new FlowControlbarMediator(_flow, context);
        _mediator.handleViewEvents(this);

        _leftHolder = new Sprite();
        addChild(_leftHolder);
        _leftMask = new Shape();  // mask is full size of bar, prevents title from going beyond right end
        _leftMask.visible = false;
        addChild(_leftMask);
        _leftHolder.mask = _leftMask;

        _tilesHolder = createTiles();
        _leftHolder.addChild(_tilesHolder);

        createControls();
        update();

        // debug the placement of title within the bar, as width changes
//        addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void{
//            _clickX = e.stageX;
//            stage.addEventListener(MouseEvent.MOUSE_MOVE, debugMouseMove);
//            stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void{
//                stage.removeEventListener(MouseEvent.MOUSE_MOVE, debugMouseMove);
//            })
//        });
    }

    private var _clickX:Number;

    private function debugMouseMove(event:MouseEvent):void   // for debug code above
    {
        width = event.stageX - _clickX;
    }

    public function warnViewToDelete():void
    {
        _mediator.clearViewEvents();
    }

    private function createControls():void
    {
        _masterSlider = new FlowMasterForegroundSlider(_context, _mediator.commitForegroundChange);
        _leftHolder.addChild(_masterSlider);

        _titleBackground = new Sprite();
        _leftHolder.addChild(_titleBackground);

        _title = new LightLabel(500);
        _title.size = 14;
        _title.y = 4;
        _leftHolder.addChild(_title);
        _mediator.handleEventsForLabel(_title);

        _titleTruncator = new Sprite();
        _leftHolder.addChild(_titleTruncator);

        for each (var color:uint in ALL_COLORS) {
            createCover(color, TITLE_BACKGROUND, 4, _titleBackground, _coverDictionary);
            createCover(color, TITLE_TRUNCATOR, 8, _titleTruncator, _coverDictionary);
        }
        updateCovers();

        _rightButtonsHolder = new Sprite();
        addChild(_rightButtonsHolder);
        _rightButtonsHolder.y = CONTROL_BAR_HEIGHT/2;  // each button 0,0 is center of button
        _editButton = addRightButton(FlowButtons.editButton, _mediator.editPage);
        _resumeButton = addRightButton(FlowButtons.resumeButton, _mediator.resumePage);
        _runButton = addRightButton(FlowButtons.runButton, _mediator.runPage);
        _deleteButton = addRightButton(FlowButtons.minusButton, _mediator.deletePage);
        _addBeforeButton = addRightButton(FlowButtons.plusButton, _mediator.addPageBefore);
        _addAfterButton = addRightButton(FlowButtons.plusButton, _mediator.addPageAfter);

        _nextButton = addRightButton(FlowButtons.nextButton, _mediator.pageForward);
        _prevButton = addRightButton(FlowButtons.prevButton, _mediator.pageBackward);
        _pagingButtonsHolder = new Sprite();
        _pagingButtonsHolder.addChild(_prevButton);
        _pagingButtonsHolder.addChild(_nextButton);
        _rightButtonsHolder.addChild(_pagingButtonsHolder);

        _buttonDictionary[EDIT_BUTTON] = _editButton;
        _buttonDictionary[RUN_BUTTON] = _runButton;

        _divider1 = addRightDivider();
        _divider2 = addRightDivider();
    }

    private function addRightButton(button:FlowButton, clickHandler:Function):FlowButton
    {
        _rightButtonsHolder.addChild(button);
        button.addEventListener(MouseEvent.CLICK, clickHandler);
        return button;
    }

    private function addRightDivider():Sprite
    {
        const H:Number = 16;
        var div:Sprite = new Sprite();
        var g:Graphics = div.graphics;
        g.lineStyle(1, 0x595959);
        g.moveTo(0, -H/2);
        g.lineTo(0, +H/2);
        _rightButtonsHolder.addChild(div);
        return div;
    }

    public function update():void
    {
        addEventListener(Event.RENDER, render);
    }

    public function render(e:Event=null):void
    {
        removeEventListener(Event.RENDER, render);
        if (_page) {
            updateFill();
            updateCovers();
            var isMaster:Boolean = _page.isMaster;
            updateTiles();
            _runButton.visible = !isMaster;
            _masterSlider.visible = isMaster;
            var titleText:String = pageOrdinalText + ". " + _page.object.title;
            if (isMaster) {
                _masterSlider.update();
                titleText += (_page.isForeground ? " (Foreground)" : " (Background)");
            }
            _title.text = titleText;
            _title.color = titleColor;
            placeControls();
        }
        updateButtons();
    }

    private function updateFill():void
    {
        var g:Graphics = this.graphics;
        g.clear();
        g.beginFill(fillColor);
        g.drawRect(0, 0, _width, CONTROL_BAR_HEIGHT);
        g.endFill();
        g = _leftMask.graphics;
        g.clear();
        g.beginFill(0xffdddd);
        g.drawRect(0, 0, _width, CONTROL_BAR_HEIGHT);
        g.endFill();
    }

    private function get fillColor():uint
    {
        if (_page && _page.isMaster) {
            return MASTER_COLOR;
        } else {
            return EVENT_COLOR;
        }
    }

    private function get titleColor():uint
    {
        if (_page && _page.isMaster) {
            return 0xb2b2b2;
        } else {
            return 0x000000;
        }
    }

    private function get buttonOutColor():uint
    {
        if (_page && _page.isMaster) {
            return 0xa6a6a6;
        } else {
            return 0x000000;
        }
    }


    private var _debugCount:int;

    private function placeControls():void
    {
        _rightButtonsHolder.graphics.clear();
        _debugCount = 0;

        // 0,0 is topLeft of control bar
        _masterSlider.x = 20;
        _tilesHolder.x = TILES_INSET;

        _addBeforeButton.x = 0;
        _addAfterButton.x = _width;

        var stageFrac:Number = stageControlsFrac;
        var flowFrac:Number = 1 - stageFrac;
        var eventFrac:Number = (page && page.isMaster) ? 0 : 1.0;
        var plusFrac:Number = _flow.titleBarRunning ? flowFrac : 1.0;

        _addBeforeButton.scaleX = _addBeforeButton.scaleY = plusFrac;
        _addAfterButton.scaleX = _addAfterButton.scaleY = plusFrac;

        // layout the buttons, starting at right going toward left
        const SPACING:Number = 26;
        var xx:Number = _width - 14; // left of the Plus button
        xx = placeOne(xx, _deleteButton, SPACING, flowFrac);
        xx = placeOne(xx, _divider1, 5);
        xx = placeOne(xx, _pagingButtonsHolder, 2*SPACING, stageFrac);
        xx = placeOne(xx, _divider2, 5, stageFrac * eventFrac);
        xx = placeOne(xx, _resumeButton, SPACING, stageFrac * eventFrac);
        xx = placeOne(xx, _runButton, SPACING, eventFrac);
        xx = placeOne(xx, _editButton, SPACING);

        _prevButton.x = -SPACING/2;  // positions within _pagingButtonsHolder
        _nextButton.x = +SPACING/2;

        // place title-cover so pencil won't collide with title (title will be chopped off)
        var leftmostButton:Number = xx;
        _titleTruncator.x = leftmostButton;

        if (_page.isMaster) {
            _title.x = _masterSlider.x + 30;
            _titleBackground.x = _title.x;
            return;
        }

        /////  place title for Event (non-master)  ////

        // center the title
        var titleWidth:Number = _title.getTextBounds(this).width;
        _title.x = (_width - titleWidth) / 2;

        // push right if hitting empty tiles, until buttons (pencil) push it leftward
        var emptyTitles:Number = _tilesHolder.x + _tilesHolder.width;
        if (_title.x < emptyTitles) {
            _title.x = Math.min(emptyTitles, leftmostButton - titleWidth);
        }

        // stop before covering real tiles
        var realTiles:Number = _tilesHolder.x + _flow.masterPages.length * TILE_SPACING;
        _title.x = Math.max(_title.x, realTiles);

        // background covers the tiles behind title
        _titleBackground.x = _title.x;
    }

    public function updateButtons():void
    {
        var color:uint = buttonOutColor;
        _editButton.outColor = color;
        _runButton.outColor = color;
        _resumeButton.outColor = color;
        _nextButton.outColor = color;
        _prevButton.outColor = color;
        _deleteButton.outColor = color;
        if (_flow.editingEventPage) {   // when editing a page, you can move between masters & pages
            var flowIndex:int = _flow.getIndexForPage(_flow.editingEventPage);
            _prevButton.enabled = (flowIndex > 0);
            _nextButton.enabled = (flowIndex+1 < (_flow.pages.length + _flow.masterPages.length));
        } else {
            var pageIndex:int = _flow.pageNumber-1;
            _prevButton.enabled = (pageIndex > 0);
            _nextButton.enabled = (pageIndex+1 < _flow.pages.length);
        }
    }

    private function placeOne(xx:Number, one:Sprite, ww:Number, frac:Number = 1):Number
    {
        // place one control, starting at right and moving to left
        one.scaleX = frac;
        one.scaleY = frac;
        ww = ww * frac;
        xx -= ww;
        one.x = xx + ww/2; // center control within this space

//        ++_debugCount;
//        _rightButtonsHolder.graphics.lineStyle(4, 0xff0000);
//        var yy:Number = CONTROL_BAR_HEIGHT + 4 + 4*_debugCount;
//        _rightButtonsHolder.graphics.moveTo(xx, yy);
//        _rightButtonsHolder.graphics.lineTo(xx + ww,  yy);

        return xx;
    }

    private function get pencilX():Number
    {
        const scrunch:Boolean = (_width < SCRUNCH_WIDTH);
        return scrunch ? (_width - 120) : (_width - 140);   // was 80  100 TODO account for forward/back buttons
    }

    private function refreshTooltips():void {
        if (_page == null) return;
        var offset:Point = new Point(0,-CONTROL_BAR_HEIGHT/3);
        if (_page.isMaster) {
            _tooltipRegistrar.registerTooltip(_editButton, FlowTooltips.EDIT_THIS_MASTER,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
            _tooltipRegistrar.registerTooltip(_deleteButton, FlowTooltips.DELETE_THIS_MASTER,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
            _tooltipRegistrar.registerTooltip(_addBeforeButton, FlowTooltips.ADD_MASTER_BEFORE,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
            _tooltipRegistrar.registerTooltip(_addAfterButton, FlowTooltips.ADD_MASTER_AFTER,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
        } else {
            _tooltipRegistrar.registerTooltip(_editButton, FlowTooltips.EDIT_THIS_EVENT,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
            _tooltipRegistrar.registerTooltip(_runButton, FlowTooltips.RUN_FROM_THIS_EVENT_FORWARD,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
            _tooltipRegistrar.registerTooltip(_resumeButton, FlowTooltips.RESUME_FROM_THIS_EVENT,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
            _tooltipRegistrar.registerTooltip(_deleteButton, FlowTooltips.DELETE_THIS_EVENT,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
            _tooltipRegistrar.registerTooltip(_addBeforeButton, FlowTooltips.ADD_BEFORE_EVENT,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
            _tooltipRegistrar.registerTooltip(_addAfterButton, FlowTooltips.ADD_EVENT_AFTER,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
            _tooltipRegistrar.registerTooltip(_prevButton, FlowTooltips.PREV_PAGE,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
            _tooltipRegistrar.registerTooltip(_nextButton, FlowTooltips.NEXT_PAGE,offset,ControlAreaToolTip.DISPLAY_ABOVE,true);
        }
    }

    override public function get width():Number
    {
        return _width;
    }

    override public function set width(value:Number):void
    {
        var newWidth:Number = Math.max(value, MIN_WIDTH);
        if (newWidth != _width) {
            _width = newWidth;
            update();
        }
    }


    private function createCover(color:uint, selector:String, gradWidth:Number, holder:Sprite, dictionary:Dictionary):void
    {
        var cover:Sprite = new Sprite();
        var g:Graphics = cover.graphics;
        g.beginFill(color);
        g.drawRect(0, 0, 9999999, CONTROL_BAR_HEIGHT);
        g.endFill();

        var edge:Shape = createGradientEdge(color, gradWidth);
        cover.addChild(edge);
        edge.x = -gradWidth;

        holder.addChild(cover);
        dictionary[selector + color.toString()] = cover;
    }

    private function updateCovers():void
    {
        var currentColor:uint = fillColor;
        for each (var selector:String in ALL_COVERS) {
            for each (var color:uint in ALL_COLORS) {
                var cover:Sprite = _coverDictionary[selector + color.toString()];
                cover.visible = (color == currentColor);
            }
        }
    }

    private function createGradientEdge(color:uint, gradWidth:Number):Shape
    {
        var edge:Shape = new Shape();
        var g:Graphics = edge.graphics;
        var matrix:Matrix = new Matrix(); // matrix seems to give smoother gradient
        matrix.createGradientBox(gradWidth, CONTROL_BAR_HEIGHT, 0, 0, 0);
        g.beginGradientFill(GradientType.LINEAR, [color,color], [0, 1.0], [0, 255], matrix);  // alpha 0..1 from left to right
        g.drawRect(0, 0, gradWidth, CONTROL_BAR_HEIGHT);  // negative x coord seemed to cause trouble here
        g.endFill();
        return edge;
    }

    public function get stageControlsFrac():Number
    {
        // used by FlowView.tweenToEdit
        return _stageControlsFrac;
    }

    public function set stageControlsFrac(value:Number):void
    {
        _stageControlsFrac = value;
        update();
    }

    private function get pageOrdinalText():String
    {
        if (_page == null) {
            return "";
        }
        var index:int = _flow.masterPages.getItemIndex(_page);
        if (index >= 0) {
            const CAPITAL_A:int = 65;
            return String.fromCharCode(CAPITAL_A + index);
        } else {
            return page.pageNumber.toString();
        }
    }

    private function createTiles():Sprite
    {
        var holder:Sprite = new Sprite();
        var count:int = EventFlow.MAX_MASTERS;
        while (holder.numChildren < count) {
            var tile:FlowMasterTile = new FlowMasterTile();
            var n:int = holder.numChildren;
            tile.index = n;
            tile.x = n * TILE_SPACING;
            holder.addChild(tile);
            _mediator.handleEventsForTile(tile);
        }
        return holder;
    }

    private function updateTiles():void
    {
        var holder:Sprite = _tilesHolder;
        if (page.isMaster) {
            holder.visible = false;
        } else {
            holder.visible = true;
            for (var n:int = 0; n < EventFlow.MAX_MASTERS; n++) {
                var tile:FlowMasterTile = holder.getChildAt(n) as FlowMasterTile;
                var assigned:Boolean, foreground:Boolean, empty:Boolean, hilite:Boolean;
                var master:IPage;

                if (n < _flow.masterPages.length) {
                    master =  _flow.masterPages[n];
                    empty = false;
                    foreground = master.isForeground;
                    assigned = (master != _flow.masterShownForRollover) && (page.masters.getItemIndex(master) >= 0);
                    hilite = (tile == _tileToHilite);
                } else {
                    master = null;
                    empty = true;
                    foreground = false;
                    assigned = false;
                    hilite = false;
                }
                tile.updateTile(empty, assigned, hilite, foreground);
                var tip:String = ((master as EventPage) == null ? FlowTooltips.MASTERS : (master as EventPage).title+"\n(click to show or hide)");
                _tooltipRegistrar.registerTooltip(tile,tip,new Point(tile.width/2,0),ControlAreaToolTip.DISPLAY_ABOVE,true);
            }
        }
    }

    public function get page():IPage
    {
        return _page;
    }

    public function set page(value:IPage):void
    {
        if (value != _page) {
            _page = value;
            if (value) {
                _masterSlider.page = value;
                _mediator.page = value;
                update();
                refreshTooltips();
            }
        }
    }

    public function get tileToHilite():FlowMasterTile
    {
        return _tileToHilite;
    }


    public function set tileToHilite(value:FlowMasterTile):void
    {
        _tileToHilite = value;   // for mouseOver
        updateTiles();
    }

    public function get isTitleBar():Boolean
    {
        return _isTitleBar;
    }

    /**
     * The titleBar Run/Edit buttons are driven by flow.titleBarRunning,
     * but FlowEditingMediator calls this to control buttons on the Flow bars.
     * @param buttonName
     * @param hilite
     */
    public function hiliteButton(buttonName:String, hilite:Boolean):void
    {
        var btn:FlowButton = _buttonDictionary[buttonName] as FlowButton;
        btn.hilite = hilite;
    }
}
}
