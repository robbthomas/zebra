/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/29/12
 * Time: 7:32 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.adobe.fileformats.vcard.Email;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ObjectFactory;
import com.alleni.author.controller.objects.ContainerController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.action.ActionGroup;
import com.alleni.author.definition.action.AddPageAction;
import com.alleni.author.definition.action.DeletePageAction;
import com.alleni.author.definition.action.LibraryRemoveAction;
import com.alleni.author.definition.action.PageMasterAction;
import com.alleni.author.definition.action.PageNumberInfo;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.EventFlowController;
import com.alleni.author.model.ui.Application;
import com.alleni.author.persistence.GadgetDecoder;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;

import mx.collections.IList;

public class PagerController extends ContainerController {

    public static var instance:PagerController = new PagerController();

    public function PagerController() {
        super();
    }

    override protected function handleModelCollectionUpdate(model:TaconiteModel, property:String, collection:IList, kind:String, index:int, item:Object, oldItem:Object):void
    {
//        trace("PagerController.handleModelCollectionUpdate pager="+model.value, "prop="+property, "kind="+kind, "index="+index, "item="+item);
        super.handleModelCollectionUpdate(model, property, collection, kind, index, item, oldItem);

        // NOTE:  pager.rearranging=true when reordering pages
        var pager:Pager = model.value as Pager;
        var page:IPage = item as IPage;
        if (pager && page as EventPage && kind == ModelEvent.ADD_ARRAY_ITEM) {
            EventPage(page).registerPageRibbons();  // even while loading, need to register page ribbons when the page can tell if its a master or not
        }
        if (pager && !pager.loading) {
            switch (property) {
                case "pages":
                    switch (kind) {
                        case ModelEvent.ADD_ARRAY_ITEM:
                        case ModelEvent.REMOVE_ARRAY_ITEM:
                            pager.pageCount = pager.pages.length;
                            renumberPages(pager);
                            updateFirstLast(pager);
                            break;
                    }

                case "masterPages":
                    switch (kind) {
                        case ModelEvent.REMOVE_ARRAY_ITEM:
                        case ModelEvent.ADD_ARRAY_ITEM:
                            renumberMasters(pager);
                            break;
                    }
            }
        }
    }

    private function get world():World
    {
        return Application.instance.document.root.value as World;
    }


    private function renumberPages(pager:Pager):void
    {
        var count:int = pager.pages.length;
        for (var n:int = 0; n < count; n++) {
            var page:IPage = pager.pages.getItemAt(n) as IPage;
            page.pageNumber = n+1;
        }
    }

    private function renumberMasters(pager:Pager):void
    {
        var count:int  = pager.masterPages.length;
        for (var n:int = 0; n < count; n++) {
            var page:IPage = pager.masterPages.getItemAt(n) as IPage;
            page.pageNumber = n+1;
        }
    }

    override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
    {	
        super.handleModelUpdate(model,property,newValue,oldValue,parent);
        var pager:Pager = model.value as Pager;
        
        
        if (pager) {
//                trace("PagerController::handleModelUpdate: prop="+property, "val="+newValue, "pager="+pager.title);

            switch (property) {
                case "loading":
                    if (newValue == false) { // done loading
                        afterLoad(pager);
                    }
                    break;
            }

            // do this regardless of whether bindings are enabled or disabled
            switch (property) {
                case "pageNumber":
                    updateFirstLast(pager);
                    break;
            }
				
            if (pager.bindingsDisabled) {    // bindingsDisabled: loading or resetting
                switch (property) {
                    case "pageNumber":
                        if (!pager.loading) {
                            // if pageNumber is reset by Edit-reset command, respond to it in render loop
//                            ApplicationController.instance.requestLayoutAction(function():void{
//                                handlePageChange(pager);
//                            });
                        }
                        break;
                }
            } else if (!pager.loading) {  // bindings enabled, not loading
                var page:IPage = pager.currentPage;
                mirrorValueToPages(pager, property, newValue);
                switch (property) {
                    case "complete":  // object has just been created or loaded
                        createFirstPageIfNeeded(pager); 
                        break;
                    case "pageCount":  // handle case of a binding attempting to change this property
                        pager.pageCount = pager.pages.length;
                        break;
                    case "pageNumber":
                        if (page) {
                            pager.objCount = page.objects.length;
                            logPageToHistory(pager, newValue as int);
                        }
                        break;
                    case "fillSpec":
                    case "fillAlpha":
                        if (page) {
                            pager.currentPage.hadChildren = true;  // author changed fill, so remove the gray
                            ApplicationController.instance.requestUpdateView(pager.currentPage.object.getView());
                        }
                        break;
                }
            }
        }

        if (model.value is IPage) {  // page itself is changing
            var page:IPage = model.value as IPage;
            pager = page.object.parent as Pager;
            if (!pager.bindingsDisabled && !page.object.loading){
                if (page == pager.currentPage) {  // mirror page values to pager, only if value is from current page
                    mirrorValueFromPage(pager, property, newValue);
                    switch (property) {
                        case "objCount":
                            pager.objCount = page.objects.length;
                            break;
                    }
                }else{
                    switch (property) {
                        case "title":
                            renumberPages(pager);
                            renumberMasters(pager);
                            break;
                    }
                }
            }
        }
    }

    protected function afterLoad(pager:Pager):void
    {
        createFirstPageIfNeeded(pager);
        pager.pageCount = pager.pages.length;
        pager.pageNumber = pager.pageNumber;  // limit to legal value
        pager.objCount = pager.currentPage.objects.length;
    }
    
    public function createFirstPageIfNeeded(pager:Pager):void
    {
        if (pager.pages.length == 0) {
            var page:IPage = addPageAt(pager, false, 0);
            if (pager is EventFlow) {
                var master:IPage = addPageAt(pager, true, 0);  // master=true
                page.masters.addItem(master);
            }
            pager.pageCount = pager.pages.length;
            renumberPages(pager);
        }
    }

    private function updateFirstLast(pager:Pager):void
    {
        pager.notFirstPage = pager.pageNumber > 1;    // (notFirstPage & notLastPage don't have inits)
        pager.notLastPage = pager.pageNumber < pager.pages.length;

    }
    
    public function addPageBy(pager:Pager, existing:IPage, after:Boolean, duplicate:Boolean=false):void
    {
//        trace("addPageBy existing="+existing, "after="+after, "duplicate="+duplicate);
        var oldNumbers:PageNumberInfo = PageNumberInfo.fromObject(pager);
        var isMaster:Boolean = false;
        var indexWithinList:int = pager.pages.getItemIndex(existing);
        if (indexWithinList < 0) {
            indexWithinList = pager.masterPages.getItemIndex(existing);
            isMaster = true;
        }
        var buttonIndex:int = (isMaster ? indexWithinList : indexWithinList + pager.masterPages.length);  // index of Event where Add button pressed
        var additionIndex:int = (after ? indexWithinList+1 : indexWithinList);
        var inside:Boolean = (buttonIndex == pager.leftPageNumber) ? after : !after;
        if(duplicate) {
            var json:Object = GadgetEncoder.buildCopyContentJson(new <TaconiteModel>[AbstractObject(existing).model], false);
            GadgetDecoder.makePasteContent(json, pager, [((isMaster)?"masterPages":"pages"),additionIndex],true, true, function(result:Vector.<AbstractObject>):void {
                var pastedPage:IPage = result[0] as IPage;
                if(!isMaster) {
                    pager.pageCount = pager.pages.length;
                }
                (isMaster)?renumberMasters(pager):renumberPages(pager);   // postAddPage needs valid pageNumber for undo action
                postAddPage(pager, pastedPage, existing, oldNumbers, buttonIndex, additionIndex, after, inside, isMaster);
                postCopyPage(pastedPage, existing);
            });
        } else {
            var page:IPage = addPageAt(pager, isMaster, additionIndex) as IPage;
            copySettingsToNewPage(existing, page);
            postAddPage(pager, page, existing,  oldNumbers, buttonIndex, additionIndex, after, inside, isMaster);
        }
    }

    private function postAddPage(pager:Pager, page:IPage, existing:IPage, oldNumbers:PageNumberInfo, buttonIndex:int,  additionIndex:int, after:Boolean, inside:Boolean, isMaster:Boolean) {
        adjustPageNumbersForAddition(pager, buttonIndex, additionIndex, after, isMaster);
        pager.dispatchEvent(new PageAddedEvent(PageAddedEvent.ADD_PAGE, page, existing, after, inside));
        ApplicationController.currentActionTree.commit(AddPageAction.fromObject(page, pager, oldNumbers));
        requestUpdatePageNumber(pager);
    }

    private function postCopyPage(page:IPage, existing:IPage):void
    {
        var newEvent:EventPage = page as EventPage;
        if (newEvent) {
            var oldEvent:EventPage = existing as EventPage;
            newEvent.image = oldEvent.image;  // copy this transient property in case of runtime changes such as toggling a checkbox or moving a slider
            if (oldEvent.childObjUsageCounts) {
                newEvent.childObjUsageCounts = oldEvent.childObjUsageCounts.clone();
            }
        }
    }

    private function adjustPageNumbersForAddition(pager:Pager, buttonIndex:int, additionIndex:int, after:Boolean, isMaster:Boolean):void
    {
        var flowIndex:int = additionIndex;
        var oldLeft:int = pager.leftPageNumber;
        var oldRight:int = pager.rightPageNumber;
        var tied:Boolean = (pager.leftPageNumber+1 == pager.rightPageNumber);
        if (!isMaster) {
            var oldInit:int = pager.initialPageNumber;
            pager.pageNumber = computePagerNumberAfterAddition(pager.pageNumber, additionIndex+1);  // +1 to give one-based number
            pager.initialPageNumber = (oldInit >= additionIndex+1 && oldInit > 1) ? oldInit+1 : oldInit;    // (initial value was changed by setting current value, above)
            flowIndex += pager.masterPages.length;
        }

        var maxIndex:int = pager.masterPages.length + pager.pages.length -1;
        if (tied) {
            // rule: the inside buttons show the new page on the opposite side.  Outside buttons show new page on same side as the button.
            if (!after) {   // clicked "before" button on either side: show new page on left
                pager.leftPageNumber = Math.min(flowIndex, maxIndex-1);  // maxIndex-1 leaves room for right slider
                pager.rightPageNumber = pager.leftPageNumber+1;
            } else if (buttonIndex == oldRight) {    // Add clicked on right-side page
                pager.rightPageNumber = flowIndex;
                pager.leftPageNumber = pager.rightPageNumber-1;
            }
        } else {
            // when sliders not tied, new page always shows on same side as button
            if (buttonIndex == oldLeft) {   // Add clicked on left-side page, so make the new page show on left
                pager.leftPageNumber = Math.min(flowIndex, maxIndex-1);  // maxIndex-1 leaves room for right slider
                ++pager.rightPageNumber;
            } else if (buttonIndex == oldRight) {    // Add clicked on right-side page
                pager.rightPageNumber = flowIndex;
            }
        }
//        trace("adjustPageNumbersForAddition p="+pager.pageNumber, "left="+pager.leftPageNumber, "right="+pager.rightPageNumber, "oldLeft="+oldLeft,oldRight, "isMaster="+isMaster, "after="+after);
    }

    /**
     * The new page has been added already.  Overridden by ArenaController.
     * @param pager
     * @param additionPageNumber = one-based number of new page
     * @return = pageNumber value for the Pager.
     */
    protected function computePagerNumberAfterAddition(oldPageNumber:int, additionPageNumber:int):int
    {
        if (Application.instance.flowVisible) {
            // at flow view we don't change the pageNumber ... not trying to show the new page.
            return oldPageNumber;
        } else {
            return additionPageNumber;
        }
    }

    public function addPageAt(pager:Pager, isMaster:Boolean, index:int):IPage
    {
        // add page at end of list
        var page:AbstractObject = ObjectFactory.createForName(pager.pageClassname, null);  // parent=null
        var p:IPage = page as IPage;
        if (isMaster) {
            pager.masterPages.addItemAt(page, index);   // add at bottom of grid
            //page.title = "Master " + EventPage.ALPHABET[index];
        } else {
            pager.pages.addItemAt(page, index);
            trace("page.parent="+page.parent);
            pager.pageCount = pager.pages.length;
            mirrorAllToPage(pager, p);
//            page.setupInspectorRibbons();
        }

        page.width = pager.width;
        page.height = pager.height;
        handleCreationInitialize(page);
        page.complete = true;
        return p;
    }

    public function copyMasters(fromList:IList, toList:IList):void
    {
        var master:IPage;
        for each (master in fromList) {
            if (master != null && toList.getItemIndex(master) < 0) {
                toList.addItem(master);
            }
        }
        for (var index:int = toList.length-1; index >= 0; index--) {
            master = toList[index];
            if (master == null || fromList.getItemIndex(master) < 0) {
                toList.removeItemAt(index);
            }
        }
    }

    public function deletePage(pager:Pager, page:IPage):void
    {
        var pageType:String = "";
        if(page.isMaster){
            pageType = "Master";
        }else if (page is EventPage){
            pageType = "Event";
        } else {
           pageType = "Page";
        }

        var group:ActionGroup = new ActionGroup("Delete "+pageType);

        // if "page" is a master, remove references to it before deleting the page
        if (page.isMaster) {
            var master:IPage = page;
            for each (var pg:IPage in pager.pages) {
                if (pg.masters.getItemIndex(master) >= 0) {
                    group.add(PageMasterAction.fromObject(pg, master, false));
                }
            }
        }

        var newNumbers:PageNumberInfo = pageNumbersAfterDeletion(pager, page);
        group.add(DeletePageAction.fromObject(page.object, pager, newNumbers));

        // remove gadget from gadgets-model unless some other page is also using it (duplicated page)
        var ep:EventPage = page as EventPage;
        if (ep && ep.gadget) {
            var count:int = world.eventFlow.getItemGlobalUsageCount(ep.gadget);
            if (count == 1) {
                group.add(LibraryRemoveAction.fromItem(ep.gadget));
            }
        }
        group.perform();
        ApplicationController.currentActionTree.commit(group);
        LibraryController.instance.updateForAssetChange();
    }

    private function pageNumbersAfterDeletion(pager:Pager, page:IPage):PageNumberInfo
    {
        var numbers:PageNumberInfo = PageNumberInfo.fromObject(pager);
        trace("pageNumbersAfterDeletion original="+numbers);

        // set "index" to the location of the page being deleted
        var isMaster:Boolean = false;
        var index:int = pager.pages.getItemIndex(page);
        if (index >= 0) {
            // adjust pageNumber and initialPageNumber
            if (index < pager.pageNumber-1) {   // pageNumber-1 so both are zero-based
                numbers.pageNumber = Math.max(numbers.pageNumber-1, 1);   // decrement to keep runtime looking at same page
            }
            var oldInit:int = numbers.initialPageNumber;
            if (index < oldInit-1) {
                numbers.initialPageNumber = Math.max(oldInit-1, 1);  // decrement
            }
        } else {
            index = pager.masterPages.getItemIndex(page);
            isMaster = true;
        }
        Utilities.assert(index >= 0);

        if (isMaster) {
            if (numbers.leftPageNumber > index) {
                --numbers.leftPageNumber;
            }
            if (numbers.rightPageNumber > index) {
                --numbers.rightPageNumber;
            }
        } else {
            index += pager.masterPages.length;   // on FlowView the index numbers include masters
            if (numbers.leftPageNumber > index) {
                --numbers.leftPageNumber;
            }
            if (numbers.rightPageNumber > index) {
                --numbers.rightPageNumber;
            }
        }

        if (numbers.leftPageNumber == numbers.rightPageNumber) {
            ++numbers.rightPageNumber;
        }

        // limit according to the reduced page counts
        var masterCount:int = pager.masterPages.length;
        var pageCount:int = pager.pages.length;
        if (isMaster) {
            --masterCount;
        } else {
            --pageCount;
        }
        if (masterCount + pageCount <= 2) {
            numbers.leftPageNumber = 0;
            numbers.rightPageNumber = 1;
        } else {
            numbers.rightPageNumber = Math.min(numbers.rightPageNumber, masterCount+pageCount-1);
            numbers.leftPageNumber = Math.min(numbers.leftPageNumber, numbers.rightPageNumber-1);
        }
        trace("  pageNumbersAfterDeletion result="+numbers);
        return numbers;
    }

    public function assignMaster(master:IPage, page:IPage, value:Boolean, commit:Boolean=false):void
    {
        // if value=false then UnAssign
//        trace("assignMaster master="+master, "page="+page, "value="+value);
        var existing:Boolean = (page.masters.getItemIndex(master) >= 0);
        if (value != existing) {
            if (commit) {
                var action:PageMasterAction = PageMasterAction.fromObject(page, master, value);
                action.perform();  // queues PseudoThread actions
                ApplicationController.currentActionTree.commit(action);
            } else {
                // for show-on-rollover: don't use PageMasterAction because the pseudothread can leave tile in ON state
                if (value) {
                    if (page.masters.getItemIndex(master) < 0) {
                        page.masters.addItem(master);
                    }
                } else {  // unassign
                    var n:Number = page.masters.getItemIndex(master);
                    if (n >= 0) {
                        page.masters.removeItemAt(n);
                    }
                }
            }
            if (value) {   // start lazyload of master-page content
                var efc:EventFlowController = this as EventFlowController;
                if (efc) {
                    var flow:EventFlow = page.object.parent as EventFlow;
                    efc.changePage(flow, flow.currentEventPage);
                }
            }
        }
    }

    public function afterRestoreInitialValues(pager:Pager):void
    {
        // override in EventFlow
        updatePagesActive(pager);
    }

    public function updatePagesActive(pager:Pager, deactivateOutgoingPageOnly:Boolean=false):void
    {
        // activate the specified page & descendant objects, deactivate others
        activatePageN(pager, pager.pageNumber, deactivateOutgoingPageOnly);
    }
    
    /**
     * Activate page "num" and deactivate any other active page. 
     * But note that this pager may not be active, so then don't active page.
     * @param pager
     * @param num
     * 
     */
    protected function activatePageN(pager:Pager, num:int, deactivateOutgoingPageOnly:Boolean=false):void
    {	
//        trace("PagerController::activatePageN:  num="+num, "pager.active="+pager.active, pager);
        var page:IPage;
        var active:Boolean;
        for (var n:int = pager.pages.length-1; n >= 0; n--) {
            page = pager.pages[n];
            active = (pager.active && (n+1) == num);
            if (active && deactivateOutgoingPageOnly) continue;
            activateObject(page.object, active);  // activate/deactivate this page & children
        }
        page = pager.pages[num-1];
        for each (var master:IPage in pager.masterPages) {
            active = (pager.active && page.masters.getItemIndex(master) >= 0);
            if (active && deactivateOutgoingPageOnly) continue;
            activateObject(master.object, active);
        }
    }

    public function activateForPageTransitionEffect(pager:Pager, p1:IPage, p2:IPage):void
    {
        // deactive all pages;  activate only the masters common to p1 & p2
//        trace("PagerController::activateForPageTransitionEffect:  p1="+p1.object.title, "p2="+p2.object.title, pager);
        for each (var page:IPage in pager.pages) {  // one-based
            activateObject(page.object, false);  // deactivate all pages
        }
        for each (var master:IPage in pager.masterPages) {
            var active:Boolean = (p1.masters.getItemIndex(master) >= 0) && (p2.masters.getItemIndex(master) >= 0);
            activateObject(master.object, active);
        }
    }

    public function activateObject(object:AbstractObject, active:Boolean):void
    {
//        trace("  PagerController::activateObject: active="+active, "wasActive="+object.active, object);
        // recurse to all descendants, even if already active, in case changePage is called before child gadget loaded
        object.active = active;

        var container:AbstractContainer = object as AbstractContainer;
        if (container) {
            var pager:Pager = container as Pager;
            if (pager) {
                activatePageN(pager, pager.pageNumber);  // nested pager: activate page only if pager.active
            } else {
                for each (var obj:AbstractObject in container.objects) {
                    activateObject(obj, active);  // recurse to all descendants
                }
            }
        }
    }
		
    override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
    {
        super.handleTrigger(object,modifier);

        var flow:EventFlow = object as EventFlow;
        if (flow) {
            if (flow.lazyLoading || flow.transientTransitionInProgress) {
                return;  // don't allow clock-driven logic to request a page-change when EventFlow is busy
            }
        }
			
        var pager:Pager = object as Pager;
        var key:String = modifier.key;
        switch (key)
        {
            case "goStartPage":
                sliceHistory(pager);
                pager.pageNumber = 1;
                trace(key, "stack="+stackText(pager));
                break;
            case "goNextPage":
                sliceHistory(pager);
                pager.pageNumber = Math.min(pager.pageNumber+1, pager.pageCount);
                trace(key, "stack="+stackText(pager));
                break;
            case "goPrevPage":
                sliceHistory(pager);
                pager.pageNumber = Math.max(pager.pageNumber-1, 1);
                trace(key, "stack="+stackText(pager));
                break;
            case "goEndPage":
                sliceHistory(pager);
                pager.pageNumber = pager.pageCount;
                trace(key, "stack="+stackText(pager));
                break;
            case "goBack":
                goBack(pager);
                trace(key, "stack="+stackText(pager));
                break;
            case "goForward":
                goForward(pager);
                trace(key, "stack="+stackText(pager));
                break;
        }
    }

    private function goBack(pager:Pager):void
    {
        var cursor:int = pager.historyCursor;
        if (pager.historyStack.length > 1 && cursor != 0) {
            cursor = (cursor < 0) ? pager.historyStack.length-2 : cursor-1;
            var num:int = pager.historyStack[cursor];
            pager.historyCursor = cursor;
            pager.pageNumber = num;
        }
    }

    private function goForward(pager:Pager):void
    {
        var stackLen:int = pager.historyStack.length;
        var cursor:int = pager.historyCursor;
        if (cursor >= 0) {
            cursor = Math.min(cursor+1, stackLen-1);
            var num:int = pager.historyStack[cursor];
            pager.historyCursor = (cursor < stackLen-1) ? cursor : -1;  // done going back & forward: cursor goes inactive
            pager.pageNumber = num;
        }
    }

    private function logPageToHistory(pager:Pager, pageNum:int):void
    {
        if (pager.historyCursor >= 0) {
            return;    // ignore page-changes caused by Back & Forward buttons
        }
        var stack:IList = pager.historyStack;
        if (stack.length > 0) {
            var top:int = stack.getItemAt(stack.length-1) as int;
            if (pageNum == top) {
                return;   // prevent duplicate stack entries
            }
        }
        stack.addItem(pageNum);
    }

    private function sliceHistory(pager:Pager):void
    {
        // discard stack entries above the cursor
        var cursor:int = pager.historyCursor;
        if (cursor >= 0) {
            var stack:IList = pager.historyStack;
            cursor = Math.min(cursor, stack.length-1-1);
            while (stack.length-1 > cursor) {
                stack.removeItemAt(stack.length-1);
            }
            pager.historyCursor = -1;  // cursor no longer active
        }
    }

    private function stackText(pager:Pager):String
    {
        // for debug tracing
        var stack:IList = pager.historyStack;
        var cursor:int = pager.historyCursor;
        var str:String = "[";
        for (var n:int=0; n < stack.length; n++) {
            var p:int = stack[n];
            var digits:String = p.toString();
            str += (n == cursor) ? ("<" + digits + ">") : digits;
                str += " ";
        }
        str += "cursor=" + cursor.toString() + "]";
        return str;
    }

    override public function propertyNeedsInit(property:String, object:AbstractObject):Boolean{
        switch (property) {
            case "width":
            case "height":
                return true;
            default:
                return super.propertyNeedsInit(property, object);
        }
    }

    protected function mirrorValueToPage(pager:Pager, page:IPage, property:String, value:*):void
    {
        if (page && pager.transientMirroredProperties.indexOf(property) >= 0) {
//            trace("mirrorValueToPage busy="+pager.transientDoingCopyFromPage, "prop="+property);
            if (pager.transientDoingCopyFromPage==0) {
                ++pager.transientDoingCopyToPages;
                page[property] = value;
                --pager.transientDoingCopyToPages;
            }
        }
    }

    protected function mirrorValueToPages(pager:Pager, property:String, value:*):void
    {
        if (pager.transientMirroredProperties.indexOf(property) >= 0) {
//            trace("mirrorValueToPage busy="+pager.transientDoingCopyFromPage, "prop="+property);
            if (pager.transientDoingCopyFromPage==0) {
                var page:IPage;
                ++pager.transientDoingCopyToPages;
                for each (page in pager.masterPages) {
                    page[property] = value;
                }
                for each (page in pager.pages) {
                    page[property] = value;
                }
                --pager.transientDoingCopyToPages;
            }
        }
    }

    protected function mirrorValueFromPage(pager:Pager, property:String, value:*):void
    {
        if (pager.transientMirroredProperties.indexOf(property) >= 0) {
//            trace("mirrorValueFromPage busy="+pager.transientDoingCopyToPages, "prop="+property);
            if (pager.transientDoingCopyToPages==0) {
                ++pager.transientDoingCopyFromPage;
                pager[property] = value;
                --pager.transientDoingCopyFromPage;
            }
        }
    }

    protected function mirrorAllFromPage(page:IPage, pager:Pager):void
    {
//        trace("mirrorAllFromPage");
        ++pager.transientDoingCopyFromPage;
        for each (var property:String in pager.transientMirroredProperties) {
            pager[property] = page[property];
        }
        --pager.transientDoingCopyFromPage;
    }

    protected function mirrorAllToPage(pager:Pager, page:IPage):void
    {
//        trace("mirrorAllToPage");
        ++pager.transientDoingCopyToPages;
        for each (var property:String in pager.transientMirroredProperties) {
            page[property] = pager[property];
        }
        --pager.transientDoingCopyToPages;
    }

    public function requestUpdatePageNumber(pager:Pager):void
    {
        // override by ArenaController
    }

    public function replacePage(pager:Pager, newPage:IPage, oldPage:IPage):void
    {
        var list:IList = pager.model.findListContainingValueObj(newPage);
        Utilities.assert(list);
        Utilities.assert(list == pager.model.findListContainingValueObj(oldPage));
        var index:int = list.getItemIndex(oldPage);
        pager.replacingPage = oldPage;
        pager.replacementPage = newPage;
        pager.model.setChildIndex(newPage.object, index);
        pager.replacingPage = null;
        pager.replacementPage = null;
        copySettingsToNewPage(oldPage, newPage);
        newPage.pageNumber = oldPage.pageNumber;
    }

    protected function copySettingsToNewPage(oldPage:IPage, newPage:IPage):void
    {
        // called for add-page
        copyMasters(oldPage.masters, newPage.masters);
        newPage.isForeground = oldPage.isForeground;
    }

    public function dumpMasters(page:IPage):void
    {
        trace("masters for ",page, "count="+page.masters.length);
        for each (var master:IPage in page.masters) {
            trace("   master="+master);
        }
    }

}
}
