package com.alleni.author.Navigation
{
import com.alleni.author.controller.objects.CompositeMediator;
	import com.alleni.author.definition.Objects;
	import com.alleni.author.definition.ViewRoles;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.Composite;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.feedback.CompositeFeedback;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.model.TaconiteObject;
import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.TaconiteView;
	import com.alleni.taconite.view.TaconiteViewFactory;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;

import flashx.textLayout.events.ModelChange;

public class SmartObjectView extends ObjectView
	{
		private var _smart:SmartObject;
        private var _childContainer:Sprite;


		public function SmartObjectView(context:ViewContext, model:TaconiteModel, role:uint)
		{
			super(context, model, role);
			_smart = object as SmartObject;

			_childContainer = new Sprite();
			addChild(_childContainer);
			placeChildContainer();
		}

		override public function get childViewContainer():DisplayObjectContainer
		{
			return _childContainer;  // enable ObjectView to automatically add and delete child views
		}

		override protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
		{
//			trace("SmartObjectView:updateModelProperty prop="+property, "val="+newValue,  "scaleX="+this.scaleX);

			var result:Boolean = super.updateModelProperty(property,oldValue,newValue);
			
			switch (property) {
				case "width":
				case "height":
				case "viewBox":
				case "anchorPoint":
				case "scale":
					placeChildContainer();
					break;
				case "editing":
				case "draggingChild":
					if (_smart.editing || _smart.draggingChild) {
						super.rolloverGlow = false;   // prevent ghosts by disabling composite rollover glow while editing
					}
					break;
			}
			return result;
		}


		private function placeChildContainer():void
		{
			_childContainer.x = - object.anchorPoint.x * _smart.width;
			_childContainer.y =  - object.anchorPoint.y * _smart.height;
			_childContainer.scaleX = _smart.scaleX;
			_childContainer.scaleY = _smart.scaleY;
//            trace("place container: scaleX="+_childContainer.scaleX, "this.scalex="+this.scaleX);
		}

		override public function getBounds(space:DisplayObject):Rectangle
		{
			// examine child views individually, so an Arena object with large content will be limited to its clipping bounds
			var result:Rectangle = new Rectangle();
			var i:int;
			var length:int = _childContainer.numChildren;
			for (i = 0; i < length; i++) {
				var objView:ObjectView = _childContainer.getChildAt(i) as ObjectView;
                if(objView.visible && !objView.object.cloakedNow){
                    var bounds:Rectangle = objView.getBounds(space);
                    result = result.union(bounds);
                }
			}
			return result;
		}

		override public function releaseChild(subjectView:ObjectView, newParent:ObjectView, stagePoint:Point):Boolean
		{
			return (newParent.model.isDescendant(this.model));
		}

        override public function handleRunningChanged():void {
            super.handleRunningChanged();
            for(var i:int = 0; i < _childContainer.numChildren; i++) {
                var child:ObjectView = _childContainer.getChildAt(i) as ObjectView;
                if(child) {
                    child.handleRunningChanged();
                }
            }
        }

        override public function showAllChildren(superClass:Class, onlyVisible:Boolean=false):void
        {
            showSelf(onlyVisible);

            var container:DisplayObjectContainer = this.getChildAt(0) as DisplayObjectContainer;
            var i:int;
            var length:int = container.numChildren;
            for(i = 0; i < length; i++) {
                var child:TaconiteView = container.getChildAt(i) as TaconiteView;
                if(child as superClass)
                    child.showAllChildren(superClass, onlyVisible);
            }
        }

        public function openEditing():void
        {
        }

	}
}
