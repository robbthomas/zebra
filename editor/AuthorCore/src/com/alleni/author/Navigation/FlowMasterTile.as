/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 5/3/12
 * Time: 12:33 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {


import flash.display.Shape;
import flash.display.Sprite;

class FlowMasterTile extends Sprite {
    public static const WIDTH:Number = 14;
    private static const HEIGHT:Number = 25;
    private static const VERT_INSET:Number = 8;
    private static const TOP_Y:Number = VERT_INSET;
    private static const BOTTOM_Y:Number = HEIGHT - VERT_INSET;

    public var index:int;

    private var _holder:Sprite;
    private var _emptyMark:Shape;
    private var _unassignedMark:Shape;
    private var _assignedMark:Shape;
    private var _hiliteMark:Shape;  // for mouseOver


    public function FlowMasterTile()
    {
        super();
        _holder = createHolder();
        addChild(_holder);
        _emptyMark = createEmptyMark();             _holder.addChild(_emptyMark)
        _assignedMark = createAssignedMark();       _holder.addChild(_assignedMark);
        _unassignedMark = createUnassignedMark();   _holder.addChild(_unassignedMark);
        _hiliteMark = createHiliteMark();         _holder.addChild(_hiliteMark);

        with (this.graphics) {
            beginFill(0, 0.01);  // mouse shield
            drawRect(0,0, WIDTH, HEIGHT);
            endFill();
        }
    }

    public function updateTile(empty:Boolean, assigned:Boolean, hilite:Boolean, foreground:Boolean):void
    {
        _emptyMark.visible = empty;
        _assignedMark.visible = assigned;
        _hiliteMark.visible = (hilite && !empty);
        _unassignedMark.visible = (!assigned && !empty);

        if (empty) {
            _holder.y = HEIGHT/2;
        } else {
            _holder.y = foreground ? TOP_Y : BOTTOM_Y;
        }
    }

    private function createHolder():Sprite
    {
        // this holder moves up & down with "foreground" value
        var holder:Sprite = new Sprite();
        holder.mouseEnabled = false;  // ensure mouseDown target==tile
        return holder;
    }

    private function createAssignedMark():Shape
    {
        const W:Number = 9;
        const H:Number = 8;
        var mark:Shape = new Shape();
        with (mark.graphics) {
            beginFill(0x000000);
            drawRect(WIDTH/2 - W/2, -H/2, W, H);
        }
        return mark;
    }

    private function createHiliteMark():Shape
    {
        const W:Number = 9;
        const H:Number = 8;
        var mark:Shape = new Shape();
        with (mark.graphics) {
            beginFill(0xffffff);
            drawRect(WIDTH/2 - W/2, -H/2, W, H);
        }
        return mark;
    }

    private function createUnassignedMark():Shape
    {
        const W:Number = 9;
        const H:Number = 8;
        var mark:Shape = new Shape();
        with (mark.graphics) {
            lineStyle(1, 0x000000);
            drawRect(WIDTH/2 - W/2, -H/2, W, H);
        }
        return mark;
    }

    private function createEmptyMark():Shape
    {
        const W:Number = 5;
        const H:Number = 4;
        var mark:Shape = new Shape();
        with (mark.graphics) {
            lineStyle(1, 0x000000);
            drawRect(WIDTH/2 - W/2, -H/2, W, H);
        }
        return mark;
    }

    override public function toString():String
    {
        return "[MasterTile" + index.toString() + "]";
    }


}

}
