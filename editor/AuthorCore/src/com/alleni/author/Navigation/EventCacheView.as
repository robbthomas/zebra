/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 9/26/12
 * Time: 8:28 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.Navigation {
import com.alleni.author.controller.text.TextEditMediator;
import com.alleni.author.controller.ui.TooltipMediator;
import com.alleni.author.definition.DoubleClickAction;
import com.alleni.author.definition.text.FormattedText;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.view.text.LightEditableLabel;
import com.alleni.author.view.text.LightLabel;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ITaconiteView;
import com.alleni.taconite.view.TaconiteView;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObject;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.utils.Timer;

import flashx.textLayout.formats.TextAlign;

import mx.collections.IList;

public class EventCacheView extends TaconiteView {

    private var _flow:EventFlow;
    private var _tilesHolder:Sprite = new Sprite();
    private var _cacheSizeField:LightEditableLabel;
    private var _cacheSizeFieldHolder:Sprite;
    private var _cacheSizeFieldBackground:Shape;
    private var _cacheSizeFieldRollover:Boolean;
    private var _width:Number;

    private static const WIDTH:Number = 5200;
    private static const HEIGHT:Number = 24;

    public function EventCacheView(context:ViewContext, model:TaconiteModel) {
        super(context, model);
        _flow = model.value as EventFlow;

        var label:LightLabel = new LightLabel();
        addChild(label);
        label.y = 5;
        label.x = 5;
        label.text = "Cache:";

        _cacheSizeFieldHolder = createEditSizeField();
        addChild(_cacheSizeFieldHolder);
        _cacheSizeFieldHolder.x = 50;
        _cacheSizeFieldHolder.y = 2;

        addChild(_tilesHolder);
    }

    override public function initialize():void
    {
        super.initialize();
        rebuildViews();
    }

    override protected function manageViewsForList(property:String):Boolean {
        return (property=="pages" || property=="masterPages");  // false for "children" - backstage
    }

    override public function createChildView(child:TaconiteModel):ITaconiteView {
        var tile:ITaconiteView = new EventCacheTileView(context, child);
        tile.initialize();
        return tile;
    }

    override protected function updateArrayProperty(property:String, modelEventKind:String, array:IList, index:int):Boolean {
        rebuildViews();
        return false;  // update the tile positions
    }

    override protected function updateModelProperty(property:Object, oldValue:Object,  newValue:Object):Boolean {
        return true;  // ignore
    }

    private function rebuildViews():void
    {
        while (_tilesHolder.numChildren > 0) {
            _tilesHolder.removeChildAt(0);
        }
        var n:int;
        var tile:EventCacheTileView;
        var count:int = viewCountForList("masterPages");
        for (n = 0; n < count; n++) {
            tile = getViewForListItem("masterPages", n) as EventCacheTileView;
            _tilesHolder.addChild(tile);
        }
        count = viewCountForList("pages");
        for (n = 0; n < count; n++) {
            tile = getViewForListItem("pages", n) as EventCacheTileView;
            _tilesHolder.addChild(tile);
        }
    }

    override protected function updateView():void {
        var n:int;
        var xx:Number = 100;
        const spacing:Number = 2;
        var tile:DisplayObject;
        var count:int = viewCountForList("masterPages");
        for (n = 0; n < count; n++) {
            tile = getViewForListItem("masterPages", n) as DisplayObject;
            _tilesHolder.addChild(tile);
            tile.x = xx;
            tile.y = 2;
            xx += tile.width + spacing;
        }
        count = viewCountForList("pages");
        for (n = 0; n < count; n++) {
            tile = getViewForListItem("pages", n) as DisplayObject;
            _tilesHolder.addChild(tile);
            tile.x = xx;
            tile.y = 2;
            xx += tile.width + spacing;
        }
        _width = xx;

        with (this.graphics) {
            clear();
            beginFill(0);
            drawRect(0,0, _width, HEIGHT);
            endFill();
        }
    }

    private static const BACK_ALPHA:Number = 0.5;
    private static const INITIAL_CLICK_TIME:Number = 100;  // prevent 2nd click of a double-click from deselecting the text

    private function createEditSizeField():Sprite {
        const W:Number = 20;
        const H:Number = 20;

        var holder:Sprite = new Sprite();
        var background:Shape = new Shape();
        holder.addChild(background);
        with (background.graphics) {
            lineStyle(1,0xdddddd);
            beginFill(0x000000);
            drawRect(0,0, W,H);
            endFill();
        }
        background.alpha = BACK_ALPHA;

        var tx:LightEditableLabel = new LightEditableLabel(W-4, H-4);
        holder.addChild(tx);
        tx.x = 2;
        tx.y = 2;
        tx.textAlign = TextAlign.CENTER;
        tx.size = 12;
        tx.text = _flow.cacheSize.toString();
        tx.editable = true;
        tx.expandable = false;
        tx.mediator.setCallBacks({
                okEditFunc: okToOpenFieldCallback,
                updateFunc:null,
                keyPressFunc:TextEditMediator.closeOnEnter,
                closeFunc: closeFieldCallback,
                hittestFunc:null
            });
        const tip:String = "Number of events to keep in memory during authoring.";
        new TooltipMediator().handleTooltip(tx, tip);
        holder.addEventListener(MouseEvent.ROLL_OVER, rolloverField);
        holder.addEventListener(MouseEvent.ROLL_OUT, rolloutField);
        _cacheSizeField = tx;
        _cacheSizeFieldBackground = background;
        return holder;
    }

    public function okToOpenFieldCallback(event:MouseEvent):int
    {
        if (event.type == MouseEvent.CLICK) {
            stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownStage, true, int.MAX_VALUE);
            var timer:Timer = new Timer(INITIAL_CLICK_TIME,1);
            timer.addEventListener(TimerEvent.TIMER_COMPLETE, fieldTimerListener);
            timer.start();
            return DoubleClickAction.EDIT;
        } else {
            return DoubleClickAction.IGNORE;
        }
    }

    private function closeFieldCallback(value:String):void {
        _cacheSizeField.edit = false;
        var text:String = new FormattedText(value).toString();
        var size:int = Number(text);
        if (size > 0) {
            if (size > 99) {
                size = 99;
            }
            _flow.cacheSize = size;
            _cacheSizeField.text = size.toString();
        }
        if (!_cacheSizeFieldRollover) {
            _cacheSizeFieldBackground.alpha = BACK_ALPHA;
        }
    }

    private function fieldTimerListener(e:TimerEvent):void {
        stage.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownStage, true);
    }

    private function mouseDownStage(e:MouseEvent):void {
        e.stopImmediatePropagation();
    }

    private function rolloverField(e:MouseEvent):void {
        _cacheSizeFieldRollover = true;
        _cacheSizeFieldBackground.alpha = 1.0;
    }

    private function rolloutField(e:MouseEvent):void {
        _cacheSizeFieldRollover = false;
        if (!_cacheSizeField.edit) {
            _cacheSizeFieldBackground.alpha = BACK_ALPHA;
        }
    }


}
}
