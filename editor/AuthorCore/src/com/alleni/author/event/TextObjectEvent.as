package com.alleni.author.event
{
	import flash.events.Event;

	public class TextObjectEvent extends Event
	{
		public static const OPEN_EDITING:String = "openEditing";
		
		public function TextObjectEvent(type:String)
		{
			super(type);
		}
	}
}