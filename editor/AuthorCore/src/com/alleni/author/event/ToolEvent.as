package com.alleni.author.event
{
	import flash.events.Event;
	
	public class ToolEvent extends Event
	{
		public static const TOOL_APPLIED:String = "toolApplied";

		public function ToolEvent(type:String)
		{
			super(type);
		}
	}
}
