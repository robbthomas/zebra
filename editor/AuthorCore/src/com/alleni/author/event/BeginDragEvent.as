package com.alleni.author.event {
	import com.alleni.author.view.WorldContainer;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	public class BeginDragEvent extends Event
	{
		public static const BEGIN_DRAG:String = "beginDrag";
		
		public var location:Point;
		public var container:WorldContainer;
		public var oldPositions:Array;
		
		public function BeginDragEvent(location:Point, container:WorldContainer, oldPositions:Array)
		{
			super(BEGIN_DRAG);
			this.location = location;
			this.container = container;
			this.oldPositions = oldPositions;
		}
		
		override public function clone():Event
		{
			return new BeginDragEvent(location, container, oldPositions);
		}
	}
}