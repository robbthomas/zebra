package com.alleni.author.event
{
	import flash.events.Event;
	
	public class GadgetEvent extends Event
	{
		public static const COMPLETE:String = "complete";
		public static const UPDATE:String = "update";
		public static const DELETE:String = "delete";
		public static const ERROR:String = "error";
		
		// command errors
		public static const STATE_ERROR:String = "stateError";
		public static const LOAD_FAILED:String = "loadFailed";
		public static const SAVE_FAILED:String = "saveFailed";
		public static const SAVE_NO_PERMISSION:String = "saveNoPermission";
		public static const LOAD_NO_PERMISSION:String = "loadNoPermission";
		public static const NAME_IN_USE:String = "nameInUse";
		
		public static const PUBLISHED_NAME_IN_USE:String = "publishedNameInUse";
		public static const REPUBLISH_NOT_ALLOWED:String 	= "republishNotAllowed";
		public static const PUBLISHED_PRICE_TOO_LOW:String = "publishedPriceTooLow";
		
		public static const PURCHASE_FAILED:String = "purchaseFailed";
		
		public var id:String;
		
		
		public function GadgetEvent(type:String, id:String)
		{
			super(type);
			this.id = id;
		}
		
		override public function clone():Event
		{
			return new GadgetEvent(type, id);
		}
	}
}