package com.alleni.author.event
{
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.events.Event;
	
	public class ReplacementEvent extends Event
	{
		public static const COMPLETE:String = "replacementComplete";
		public static const CANCEL:String = "replacementCancelled";		
		
		public var status:String;
				
		public function ReplacementEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new ReplacementEvent(type);
		}
	}
}
