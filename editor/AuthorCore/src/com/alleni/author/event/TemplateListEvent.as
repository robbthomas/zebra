package com.alleni.author.event {
	import com.alleni.author.model.ui.WireAnchor;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	public class TemplateListEvent extends Event
	{
		public static const COMPLETE:String = "templateComplete";
		public static const ERROR:String = "templateError";
		protected var _list:ArrayCollection;
		
		private var _kind:String;
	
		public function TemplateListEvent(kind:String,
										  listTemplate:ArrayCollection=null)
		{
			super(kind);
			_kind = kind;
			_list = listTemplate;
		}
		
		override public function clone():Event
		{
			return new TemplateListEvent(kind, _list);
		}
	
		public function get kind():String
		{
			return _kind;
		}
		
		public function get list():ArrayCollection {
			return _list;
		}
	}
}