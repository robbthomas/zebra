package com.alleni.author.event
{
	import flash.events.Event;

	public class ScrollUpdateEvent extends Event
	{	
		public static const SCROLL:String = "scroll";
	
		public var params:Object;
		
		public function ScrollUpdateEvent(type:String, params:Object, bubbles:Boolean = false, cancelable:Boolean = false)
		{		
			super(type, bubbles, cancelable);
			this.params = params;
		}
		
		override public function clone():Event
		{
			return new ScrollUpdateEvent(type, params, bubbles, cancelable);
		}

        [Transient]
        override public function toString():String
        {
            return "[ScrollUpdateEvent type=" + type + " params=" + params + "]";
        }

	}
}