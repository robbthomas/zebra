package com.alleni.author.event
{
	import com.alleni.author.definition.action.ObjectShape;
	
	import flash.events.Event;
	import flash.geom.Point;

	public class PointDragEvent extends Event
	{
		public static const POINT_DRAG:String = "pointDrag";
		
		// values of "kind"
		public static const SWAP_HANDLES:String = "swapHandles";
		
		public var kind:String;
		public var role:uint;
		public var oldRatio:Number;
		public var oldShape:ObjectShape;
		public var lastMousePoint:Point;
		public var maintainAspectInitially:Boolean;
		
		public function PointDragEvent(kind:String, role:uint, oldRatio:Number, oldShape:ObjectShape, lastMousePoint:Point, maintainAspectInitially:Boolean)
		{
			super(POINT_DRAG);
			this.kind = kind;
			this.role = role;
			this.oldRatio = oldRatio;
			this.oldShape = oldShape;
			this.lastMousePoint = lastMousePoint;
			this.maintainAspectInitially = maintainAspectInitially;
		}
		
		override public function clone():Event
		{
			return new PointDragEvent(kind,role,oldRatio,oldShape,lastMousePoint,maintainAspectInitially);
		}
	}
}