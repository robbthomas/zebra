package com.alleni.author.event {
	import com.alleni.author.model.ui.WireAnchor;
	
	import flash.events.Event;
	
	public class AnchorEvent extends Event
	{
		public static const TYPE:String = "AnchorEvent";
		public static const ADD:String = "add";
		public static const REMOVE:String = "remove";
		public static const DRAGGED_OUT:String = "draggedOut";
	
		private var _kind:String;
		private var _anchor:WireAnchor;
		private var _display:Boolean;
	
		public function AnchorEvent(kind:String, anchor:WireAnchor, display:Boolean)
		{
			super(TYPE);
			_kind = kind;
			_anchor = anchor;
			_display = display;
		}
		
		override public function clone():Event
		{
			return new AnchorEvent(kind, anchor, display);
		}
	
		public function get kind():String
		{
			return _kind;
		}
	
		public function get anchor():WireAnchor
		{
			return _anchor;
		}

		public function get display():Boolean {
			return _display;
		}
	}
}