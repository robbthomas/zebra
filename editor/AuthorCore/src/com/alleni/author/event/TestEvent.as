package com.alleni.author.event
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.test.TestScript;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	public class TestEvent extends Event
	{
		public static const ITEM_CLICKED:String = "testItemClicked";
		public static const ITEM_RENAMED:String = "testItemRenamed";
		
		public var obj:AbstractObject;
		public var model:*;
		public var doubleClick:Boolean;
		public var shiftKey:Boolean;
		public var ctrlKey:Boolean;
		public var altKey:Boolean;
		public var stagePoint:Point;
		
		
		public function TestEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new TestEvent(type);
		}
	}
}