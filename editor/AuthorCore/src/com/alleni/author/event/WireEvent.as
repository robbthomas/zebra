package com.alleni.author.event
{
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.model.ui.Wire;

import com.alleni.author.model.ui.WireAnchor;

import flash.events.Event;

	public class WireEvent extends Event
	{
		public static const TRIGGER:String = "trigger";
		public static const COMPLETE:String = "complete";
		public static const DRAG_NEW_WIRE:String = "dragNewWire";
		public static const DETACH:String = "detach";
        
        public var wire:Wire;
		public var role:uint;
		public var existing:Boolean;
		public var oldAnchor:WireAnchor;
        
        public function WireEvent(type:String, wire:Wire, existing:Boolean=false, oldAnchor:WireAnchor = null, role:uint=0)
        {
            super(type);
			this.wire = wire;
	        this.existing = existing;
	        this.oldAnchor = oldAnchor;
			this.role = role;
        }
        
        override public function clone():Event
        {
            return new WireEvent(type, wire, existing, oldAnchor, role);
        }
	}
}