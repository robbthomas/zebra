package com.alleni.author.event
{
	import com.alleni.author.model.ui.Asset;
	
	import flash.events.Event;

	public class AssetEvent extends Event
	{
		public static const COMPLETE:String = "complete";
		public static const UPDATE:String = "update";
		public static const DELETE:String = "delete";
		public static const ERROR:String = "error";
		
		public var asset:Asset;
		public var data:Object;
		
		public function AssetEvent(type:String, asset:Asset, data:Object = null)
		{
			super(type);
			this.asset = asset;
			this.data = data;
		}
		
		override public function clone():Event
		{
			return new AssetEvent(type, asset, data);
		}
	}
}