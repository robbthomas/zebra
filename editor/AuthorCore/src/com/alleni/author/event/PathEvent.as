/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.event
{
	import com.alleni.author.model.objects.PathNode;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	public class PathEvent extends Event
	{
		public static const INITIAL_CLICK:String = "initialClick";
		public static const OPEN_EDITING:String = "openPathEditing";
		public static const ADD_POINT:String = "addPoint";
		public static const DELETE_POINT:String = "deletePoint";
		public static const FINISH_EXTENDING:String = "finishExtending";
		public static const REQUEST_EXTEND:String = "reqExtend";
		public static const REQUEST_FLASH:String = "reqFlash";
		public static const HILITE_ANCHOR:String = "hiliteAnchor";
		
		public var node:PathNode;
		public var index:int;
		public var yes:Boolean;
		public var point:Point;
		
		public function PathEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		
		override public function clone():Event
		{
			var event:PathEvent = new PathEvent(type);
			event.node = node;
			event.index = index;
			event.yes = yes;
			event.point = point.clone();
			return event;
		}

	}
}