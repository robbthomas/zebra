package com.alleni.author.event
{
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.events.Event;
	
	public class CompositeEvent extends Event
	{
		public static const UNMAKE_OBJ:String = "compositeUnmakeObj";
		public static const RECALC_BOUNDS:String = "compositeRecalcBounds";
		public static const ABSORB_OBJECTS:String = "compositeAbsorbObjects";
		public static const REMOVE_HILITE:String = "compositeRemoveHilite";
		
		public var models:Vector.<TaconiteModel>;
		
		public function CompositeEvent(type:String, models:Vector.<TaconiteModel>=null)
		{
			super(type);
			this.models = models;
		}
		
		override public function clone():Event
		{
			return new CompositeEvent(type, models);
		}
	}
}
