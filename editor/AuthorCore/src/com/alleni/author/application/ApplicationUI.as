package com.alleni.author.application
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.controller.AuthorController;
	import com.alleni.author.controller.app.MenuController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.ModelRoot;
	import com.alleni.taconite.service.LogService;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.UncaughtErrorEvent;
	import flash.display.StageDisplayState;
	import flash.system.Capabilities;
	
	import mx.core.IVisualElement;
	import mx.core.UIComponent;
	import mx.events.ResizeEvent;
	
	import spark.components.Group;

	public class ApplicationUI extends Group
	{
		public static var instance:ApplicationUI = null;
		
		public var invokedAsEditor:Boolean = false;
		public var rawChildren:UIComponent;
		private var _mouseEater:UIComponent;
        private var _mouseIsDown:Boolean;

		protected var _presentationContainer:WorldContainer;
		
		public function ApplicationUI()
		{
			super();
			Utilities.assert(instance == null);
			instance = this;
			this.percentWidth = 100;
			this.percentHeight = 100;
		}

		override protected function createChildren():void
		{
			super.createChildren();
			rawChildren = new UIComponent();
		}
		
		override protected function childrenCreated():void
		{
			super.childrenCreated();
			systemManager.loaderInfo.uncaughtErrorEvents.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, handleUncaughtErrorEvent);

			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOADING, handleProjectLoading);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_CHANGING, handleProjectChanging);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_INITIALIZED, handleShowProjectUponInitializationOrFailure);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOAD_FAILED, handleShowProjectUponInitializationOrFailure);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_COMPLETE, handleProjectComplete);
			ApplicationController.addEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED, handleRunningChanged);
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_FULL_SCREEN, handleToggleFullScreen);
			ApplicationController.addEventListener(NotificationNamesApplication.RESET_APPLICATION_STATE, handleResetApplicationState);
		}

		/*
		 * support full screen interactive where available (AIR and Flash Player 11.3+)
		 * with backward compatibility fallback to basic full screen.
		 */
		protected static var _fullScreenState:String = null;
		private static const FULL_SCREEN_INTERACTIVE:String = "FULL_SCREEN_INTERACTIVE";
		private static const ALLOWS_FULL_SCREEN_INTERACTIVE:String = "allowsFullScreenInteractive";
		private static const CONTENT_SCALE_FACTOR:String = "contentsScaleFactor";
		public static function get allowsFullScreenInteractive():Boolean {
			return (FULL_SCREEN_INTERACTIVE in StageDisplayState
					&& ALLOWS_FULL_SCREEN_INTERACTIVE in instance.stage
					&& (!(CONTENT_SCALE_FACTOR in instance.stage) || instance.stage[CONTENT_SCALE_FACTOR] == 1) // due to Adobe's hidpi bug with dialog
					);
		}

		protected function handleToggleFullScreen(event:Event):void
		{
			// always recheck when we have content scale factor, in case window was dragged from hidpi to a non-hidpi display
			if (_fullScreenState == null || (CONTENT_SCALE_FACTOR in stage)) {
				if (allowsFullScreenInteractive) {
					_fullScreenState = StageDisplayState[FULL_SCREEN_INTERACTIVE];
				} else {
					_fullScreenState = StageDisplayState.FULL_SCREEN;
				}
			}

			if (stage.displayState != _fullScreenState) {
				try {
					stage.displayState = _fullScreenState;
					_fullScreenState = stage.displayState;
				} catch(se:SecurityError) {
					if (FULL_SCREEN_INTERACTIVE in StageDisplayState
							&& _fullScreenState == StageDisplayState[FULL_SCREEN_INTERACTIVE]) {
						_fullScreenState = StageDisplayState.FULL_SCREEN;
						try {
							stage.displayState = _fullScreenState;
						} catch (e:Error) {
							LogService.error("Unable to toggle into full screen or full screen interactive due to error: " + e)
						}
					}
				}
			} else {
				stage.displayState = StageDisplayState.NORMAL;
			}
		}

		protected function get isFullScreen():Boolean
		{
			return stage != null && _fullScreenState != null && stage.displayState.substr(0, 5) == _fullScreenState.substr(0, 5);
		}
		
		protected function handleUncaughtErrorEvent(event:UncaughtErrorEvent):void
		{
			// prevent exception bubbling in Editor and in Zebra Player on consumer Flash Player
			// exception popups will occur on 11.5+ non-debug players unless suppressed.
			// stack traces for non-debug player will lack classpath and line numbers since debug symbols are not compiled in.
			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor 
					|| !Capabilities.isDebugger) {
				event.preventDefault();
				event.stopImmediatePropagation();
			}
			var message:String;
			
			if (event.error is Error) {
				// ZZX-1170 Error attempting to execute IME command. We do not use IME and this is not a crash.
				// This exception is the result of an apparent incompatibility between MX containers and Flash 11.
				if (event.error.errorID == 2063) return;

				message = Error(event.error).getStackTrace();
                if(message == null) {
                    message = Error(event.error).toString();
                }
			} else if (event.error is ErrorEvent) {
				message = ErrorEvent(event.error).text;
			} else {
				message = event.error.toString();
			}
			LogService.error("Uncaught Error: " + message);
            captureMouseClicks(true);
			
			var info:String = ApplicationController.instance.authorController.sessionInfo;
            TaconiteFactory.getEnvironmentImplementation().handleError(info + "\n\n" + message + "\n\n" + LogService.dump);
		}
		
		public function captureMouseClicks(value:Boolean, callback:Function=null):void
		{
			if (value) {
                if(_mouseEater == null) {
                    _mouseEater = new UIComponent();
                    _mouseEater.graphics.clear();
                    _mouseEater.graphics.beginFill(0x00ff00, .001);
                    _mouseEater.graphics.drawRect(0, 0, this.width, this.height);
                    this.rawChildren.addChild(_mouseEater);
                    if (callback != null)
                        _mouseEater.addEventListener(MouseEvent.MOUSE_DOWN, callback);
                }
			} else {
                if (_mouseEater) {
                    if (callback != null)
                        _mouseEater.removeEventListener(MouseEvent.MOUSE_DOWN, callback);
                    if (this.rawChildren.contains(_mouseEater))
                        this.rawChildren.removeChild(_mouseEater);
                    _mouseEater = null;
                }
			}
            MenuController.instance.enabled = !value;
		}
		
		protected function toJavascriptString(input:String):String
		{
			return "'" + input
				.replace(/\\/, "\\\\")
				.replace(/\n/, "\\n")
				.replace(/\t/, "\\t")
				.replace(/\b/, "\\b")
				.replace(/\f/, "\\f")
				.replace(/\r/, "\\r")
				.replace(/\v/, "\\v")
				.replace(/\"/, "\\\"")
				.replace(/\'/, "\\\'")
				+ "'"
		}
		
		override public function set initialized(value:Boolean):void
		{
			super.initialized = value;
			
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UI_READY));
			rawChildren.percentWidth = 100;
			rawChildren.percentHeight = 100;
			rawChildren.addEventListener(ResizeEvent.RESIZE, handleMeasure);
			this.addElement(rawChildren);

            addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener,true, int.MAX_VALUE);
            addEventListener(MouseEvent.MOUSE_UP, mouseUpListener, true, int.MAX_VALUE);
		}

		protected function handleFullScreen(event:Event):void
		{

		}
		
		protected function handleProjectLoading(event:ApplicationEvent):void
		{
		}
		
		protected function handleProjectChanging(event:ApplicationEvent):void
		{
		}
		
		protected function handleShowProjectUponInitializationOrFailure(event:ApplicationEvent):void
		{	
		}
		
		protected function handleProjectComplete(event:ApplicationEvent):void
		{
		}
		
		protected function handleResetApplicationState(event:ApplicationEvent):void
		{
			if (_presentationContainer)
				handleProjectComplete(null);
		}

        protected function handleRunningChanged(event:ApplicationEvent):void {
            if(_presentationContainer) {
                _presentationContainer.worldView.handleRunningChanged();
            }
        }
		
		protected function handleMeasure(event:ResizeEvent=null):void
		{
		}
		
		public function initializeWithModels(world:ModelRoot, wiring:ModelRoot):void
		{
			// implement in subclass
		}
		
		protected function initializeFreshPresentationContainer():void
		{
			_presentationContainer.visible = false;
			ApplicationController.instance.refreshViewContext(_presentationContainer);
			_presentationContainer.initialize();
			
			handleMeasure();				
		}
		
		protected function showPresentationContainer():void
		{
			if (_presentationContainer) {
				_presentationContainer.alpha = 0;
				_presentationContainer.visible = true;
				Tweener.addTween(_presentationContainer,{alpha:1, time:1.0, transition:"easeOutQuart"});
			}
		}
		
		public function addPopOver(element:IVisualElement):void
		{
			this.addElement(element);
		}
		
		protected function getFreshPrimaryWorldContainer(world:ModelRoot, wiring:ModelRoot):WorldContainer
		{
			var controller:AuthorController = ApplicationController.instance.authorController;
			return new WorldContainer(controller, world, wiring, ViewRoles.PRIMARY);
		}
		
		protected function getFreshDiagramWorldContainer(world:ModelRoot, wiring:ModelRoot):WorldContainer
		{
			var controller:AuthorController = ApplicationController.instance.authorController;
			return new WorldContainer(controller, world, wiring, ViewRoles.DIAGRAM, true);
		}

        private function mouseDownListener(event:MouseEvent):void
        {
            _mouseIsDown = true;
        }

        private function mouseUpListener(event:MouseEvent):void
        {
            _mouseIsDown = false;
        }

        public function get mouseIsDown():Boolean
        {
            return _mouseIsDown;
        }
        public function set mouseIsDown(value:Boolean):void{
            _mouseIsDown = value;
        }

	}
}
