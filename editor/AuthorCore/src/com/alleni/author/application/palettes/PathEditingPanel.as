/**
 * ¬¨¬®¬¨¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.application.palettes
{
	import assets.images.*;
	
	import com.alleni.author.controller.objects.IEditingMediator;
	import com.alleni.author.controller.objects.PathEditingMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.palettes.ToolboxController;
	import com.alleni.author.model.objects.PathObject;
	import com.alleni.author.model.ui.Application;
	
	import flash.display.Bitmap;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	import mx.containers.ApplicationControlBar;
	import mx.events.PropertyChangeEvent;
	
	public class PathEditingPanel extends Sprite
	{
		private var _path:PathObject;
		private var _creation:Boolean;
		private var _sliderTrack:Sprite;
		private var _slider:Sprite;
		private static var _smoothing:Number = PathObject.DEFAULT_SMOOTHING;  // the value of the slider
		private var _addButton:Sprite;
		private var _sliderClickX:Number;
		private var _sliderMouseDown:Boolean;
		
		private static const OVERLAY_Y:Number = 62;
		private static const OVERLAY_WIDTH:Number = 86;
		private static const OVERLAY_HEIGHT:Number = 170;
		private static const GRAPHIC_X:Number = 3;
		private static const GRAPHIC_Y:Number = OVERLAY_Y+0;
		
		private static const SLIDER_Y:Number = 84;
		private static const SLIDER_HEIGHT:Number = 9;
		private static const SLIDER_LEFT:Number = 19;
		private static const SLIDER_RIGHT:Number = 54;

		private static const SLIDER_FILL:uint = 0xffffff;
		private static const SLIDER_ROLL:uint = 0xaaaaff;
		private static const SLIDER_DOWN:uint = 0x2222ff;
		private static const TRACK_BLUE:uint = 0x00abfd;
		private static const TRACK_GRAY:uint = 0x6d6d6d;
		
		private static const SLIDER_MIN:Number = 15;
		private static const SLIDER_MAX:Number = 100;
		
		private static const ADD_X:Number = 27;
		private static const ADD_Y:Number = 194;
		private static const ADD_WIDTH:Number = 58;
		private static const ADD_HEIGHT:Number = 13;
		
		private static const ADD_FILL:uint = 0xaaaaff;  
		private static const ADD_ALPHA:Number = 0.0;
		private static const ADD_ROLL_ALPHA:Number = 0.5;
		private static const ADD_DOWN_ALPHA:Number = 1.0;
		private static const ADD_DOWN_BLEND:String = BlendMode.INVERT;  

		
		
		public function PathEditingPanel(creation:Boolean=true, path:PathObject=null)
		{
			super();
			
			_path = path;
			_creation = creation;
			var overlay:DisplayObject;
			if (creation)
				overlay = new pathCreationOverlay();  // load .fxg file
			else
				overlay = new pathEditingOverlay();
			addChild(overlay);
			overlay.x = GRAPHIC_X;
			overlay.y = GRAPHIC_Y;
			drawBackground();

			_sliderTrack = new Sprite();
			addChild(_sliderTrack);
			_sliderTrack.x = SLIDER_LEFT;
			_sliderTrack.y = SLIDER_Y;
			
			if (_path) {
				smoothing = _path.pathSpacing;
			}
			_slider = new Sprite();
			addChild(_slider);
			_slider.x = SLIDER_LEFT + (SLIDER_RIGHT - SLIDER_LEFT) * (smoothing - SLIDER_MIN) / (SLIDER_MAX - SLIDER_MIN);
			_slider.y = SLIDER_Y;
			drawSlider(_slider.graphics, SLIDER_FILL);
			drawSliderTrack(_sliderTrack.graphics, _slider.x - SLIDER_LEFT);
			_slider.addEventListener(MouseEvent.ROLL_OVER, sliderMouseListener);
			_slider.addEventListener(MouseEvent.ROLL_OUT, sliderMouseListener);
			_slider.addEventListener(MouseEvent.MOUSE_DOWN, sliderMouseListener);
			
			if (!creation) {
				_addButton = new Sprite();
				addChild(_addButton);
				_addButton.x = ADD_X;
				_addButton.y = ADD_Y;
				_addButton.alpha = ADD_ALPHA;
				drawButton(_addButton.graphics);
				_addButton.addEventListener(MouseEvent.ROLL_OVER, addButtonMouseListener);
				_addButton.addEventListener(MouseEvent.ROLL_OUT, addButtonMouseListener);
				_addButton.addEventListener(MouseEvent.MOUSE_DOWN, addButtonMouseListener);
			}
			
			Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
		}
		
		public function closePanel():void
		{
			if (this.parent && parent.contains(this))
				parent.removeChild(this);

			Application.instance.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
		}
		
		private function closeEditing():void
		{
			if (editor)
				editor.makeObject();
		}
		
		private function get editor():PathEditingMediator
		{
			var ed:IEditingMediator = ApplicationController.instance.authorController.currentEditingMediator;
			if (ed is PathEditingMediator)
				return ed as PathEditingMediator;
			else
				return null;
		}
		
		public function get smoothing():Number
		{
			return _smoothing;
		}
		
		public function set smoothing(value:Number):void
		{
			_smoothing = value;
			if (_path)
				_path.pathSpacing = value;
		}
		
		private function drawSlider(g:Graphics, color:uint):void
		{
			g.clear();
			g.lineStyle(1);
			g.beginFill(color);
			g.drawRoundRect(-1,-2, SLIDER_HEIGHT+3, SLIDER_HEIGHT+3, 2,2);
			g.endFill();
		}
		
		private function drawSliderTrack(g:Graphics, xx:Number):void
		{
			// xx is location of boundary between blue & gray areas
			g.clear();
			g.beginFill(TRACK_BLUE);
			g.drawRect(0,0, xx, SLIDER_HEIGHT);
			g.endFill();
			g.beginFill(TRACK_GRAY);
			g.drawRect(xx,0, SLIDER_RIGHT-SLIDER_LEFT - xx, SLIDER_HEIGHT);
			g.endFill();
		}
		
		private function drawButton(g:Graphics):void
		{
			g.clear();
			g.beginFill(ADD_FILL, 1.0);
			g.drawRoundRect(0,0, ADD_WIDTH, ADD_HEIGHT, 3,3);
			g.endFill();
		}
		
		private function handleApplicationPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "currentTool") {
				// click on Arrow tool, etc, remove this panel from over the toolbox
				if (event.newValue != ToolboxController.ANIMATION_PATH) {
					closeEditing();
					closePanel();  // maybe we weren't editing ... had just selected the path tool, then selected another tool
				}
			}
		}
		
		public function editingClosed():void
		{
			_path = null;
			if (Application.instance.currentTool != ToolboxController.ANIMATION_PATH)
				closePanel();
		}
		
		private function sliderMouseListener(event:MouseEvent):void
		{
			switch (event.type) {
				case MouseEvent.ROLL_OVER:
					drawSlider(_slider.graphics, SLIDER_ROLL);
					break;
				case MouseEvent.ROLL_OUT:
					if (!_sliderMouseDown)
						drawSlider(_slider.graphics, SLIDER_FILL);
					break;
				case MouseEvent.MOUSE_DOWN:
					_sliderMouseDown = true;
					drawSlider(_slider.graphics, SLIDER_DOWN);
					stage.addEventListener(MouseEvent.MOUSE_MOVE, sliderMouseListener);
					stage.addEventListener(MouseEvent.MOUSE_UP, sliderMouseListener);
					_sliderClickX = _slider.mouseX;
					break;
				case MouseEvent.MOUSE_UP:
					_sliderMouseDown = false;
					drawSlider(_slider.graphics, SLIDER_ROLL);
					stage.removeEventListener(MouseEvent.MOUSE_MOVE, sliderMouseListener);
					stage.removeEventListener(MouseEvent.MOUSE_UP, sliderMouseListener);
					break;
				case MouseEvent.MOUSE_MOVE:
					_slider.x = Math.max(SLIDER_LEFT, Math.min(SLIDER_RIGHT, _slider.x + _slider.mouseX - _sliderClickX));
					smoothing = SLIDER_MIN + (SLIDER_MAX - SLIDER_MIN) * (_slider.x - SLIDER_LEFT) / (SLIDER_RIGHT - SLIDER_LEFT);
					drawSliderTrack(_sliderTrack.graphics, _slider.x - SLIDER_LEFT);
					break;
			}
		}
		
		private function addButtonMouseListener(event:MouseEvent):void
		{
			switch (event.type) {
				case MouseEvent.ROLL_OVER:
					_addButton.alpha = ADD_ROLL_ALPHA;
					break;
				case MouseEvent.ROLL_OUT:
					_addButton.alpha = ADD_ALPHA;
					break;
				case MouseEvent.MOUSE_DOWN:
					_addButton.alpha = ADD_DOWN_ALPHA;
					_addButton.blendMode = ADD_DOWN_BLEND;
					stage.addEventListener(MouseEvent.MOUSE_UP, addButtonMouseListener);
					break;
				case MouseEvent.MOUSE_UP:
					stage.removeEventListener(MouseEvent.MOUSE_UP, addButtonMouseListener);
					_addButton.alpha = ADD_ROLL_ALPHA;
					_addButton.blendMode = BlendMode.NORMAL;
					if (_path && editor) {
						editor.addRibbonToSelectedNode();
					}
					break;
			}
		}
		
		private function drawBackground():void
		{
			graphics.clear();
			graphics.beginFill(0xDCDCDC);
			graphics.drawRoundRect(0, OVERLAY_Y, OVERLAY_WIDTH, OVERLAY_HEIGHT, 20, 20);
			graphics.endFill();
		}
		
		public function hitTestWholeToolbox(stageX:Number, stageY:Number):Boolean
		{
			var bounds:Rectangle = getBounds(stage);
			bounds.y -= OVERLAY_Y+10;  // account for this overlay being placed below the toolbox tools
			bounds.height += OVERLAY_Y+10;
			return bounds.contains(stageX, stageY);
		}
		
	}
}
