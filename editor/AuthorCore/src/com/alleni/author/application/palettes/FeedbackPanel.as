package com.alleni.author.application.palettes
{
	import com.alleni.author.application.ApplicationUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.Styles;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.DrawingPreset;
	import com.alleni.author.model.ui.Feedback;
	import com.alleni.author.model.ui.QAVellumSettings;
	import com.alleni.author.service.FeedbackOperation;
	import com.alleni.author.view.ui.controls.PopOutMenu;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.containers.Form;
	import mx.containers.FormItem;
	import mx.containers.VBox;
	import mx.controls.HRule;
	import mx.events.PropertyChangeEvent;
	import mx.validators.StringValidator;
	
	import spark.components.Button;
	import spark.components.CheckBox;
	import spark.components.HGroup;
	import spark.components.Label;
	import spark.components.RadioButton;
	import spark.components.RadioButtonGroup;
	import spark.components.TextArea;
	import spark.components.TextInput;

	public class FeedbackPanel extends VBox
	{
		private static const HEADER_TEXT:String = "Help us improve ZebraZapps.";
		private static const BODY_TEXT:String = "We'd love to hear how your experience is going! Your feedback will be kept private.";
		
		private static const FORM_WIDTH:Number = 440.0;
		
		private var _form:Form;
		
		private var _summaryItem:FormItem;
		private var _summaryMenu:PopOutMenu;
		private var _ticketTypeItem:FormItem;
		private var _issueMenu:PopOutMenu;
		private var _drawingItem:FormItem;
		private var _drawingMenu:PopOutMenu;
		private var _description:TextArea;
		private var _thumbContainer:Sprite;
		private var _clearDrawingBtn:Button;
		private var _summaryField:TextInput;
		private var _addScreenShotButton:Button;
		private var _submitBtn:Button;
		private var _xOffset:int = 0;
		private var _menuWidth:Number;
		private var _screenshotText:Label;
		private var drawingCheckbox:CheckBox;
		private var _adminUser:Boolean;

		private var _disclosurePleaseContactMeCheckBox:CheckBox;
		private var _disclosureCodeReviewAllowedCheckBox:CheckBox;
		
		private var _typeRadio:RadioButtonGroup;
		private var _typeBugRadioButton:RadioButton;
		private var _typeFeatureRadioButton:RadioButton;
		
		private var _priorityRadio:RadioButtonGroup;
		private var _priorityBlockerRadioButton:RadioButton;
		private var _priorityMajorRadioButton:RadioButton;
		private var _priorityMinorRadioButton:RadioButton;
		private var _priorityNoneRadioButton:RadioButton;
		
		private var _featurePriorityRadio:RadioButtonGroup;
		private var _featurePriorityBlockerRadioButton:RadioButton;
		private var _featurePriorityUIRadioButton:RadioButton;
		private var _featurePriorityConsiderRadioButton:RadioButton;

		
		public function FeedbackPanel()
		{
			super();
			this.styleName = "default";
			this.horizontalScrollPolicy = "off";
			this.verticalScrollPolicy = "off";
			this.width = 475;
			_adminUser = TaconiteFactory.getEnvironmentImplementation().isAdmin;
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			// flex bug causes this to be called more than once in certain circumstances
			// let's ensure we only create one form in that case.
			if (_form)
				return;
			
			_form = new Form();
			_form.styleName = "feedback";
			var item:FormItem;
			var rule:HRule;

            var wikiLabel:Label = new Label();
            wikiLabel.text = "Do you have a question?";
            wikiLabel.styleName = "lightLabel";
            _form.addChild(wikiLabel);

            var wikiBody:Label = new Label();
            wikiBody.text = "When you have a question, ZebraZapps User Help is the first place to go! It has helpful information, tutorials, tips, and more! You can quickly search for an answer here, and ideas, too!";
            wikiBody.width = FORM_WIDTH;
            _form.addChild(wikiBody);

            var buttonForm:FormItem = new FormItem();
            buttonForm.width = FORM_WIDTH;
            buttonForm.setStyle("horizontalAlign", "right");

            var wikiButton:Button = new Button();
            wikiButton.label = "Visit User Help";
            wikiButton.addEventListener(MouseEvent.CLICK, wikiButtonClick);
            buttonForm.addChild(wikiButton);
            _form.addChild(buttonForm);

            rule = new HRule();
			rule.width = FORM_WIDTH;
			_form.addChild(rule);

			var headerLabel:Label = new Label();
			headerLabel.text = HEADER_TEXT;
			headerLabel.styleName = "lightLabel";
			_form.addChild(headerLabel);
			
			var bodyLabel:Label = new Label();
			bodyLabel.width = FORM_WIDTH;
			bodyLabel.height = 35;
			bodyLabel.text = BODY_TEXT;
			_form.addChild(bodyLabel);

			if (!_adminUser) {
	            // Disclosure selection
				item = new FormItem();
				item.label = "";
				_disclosurePleaseContactMeCheckBox = new CheckBox();
				_disclosurePleaseContactMeCheckBox.addEventListener(MouseEvent.CLICK, handleDrawingCheckboxClick);
				_disclosurePleaseContactMeCheckBox.width = 300;
				_disclosurePleaseContactMeCheckBox.height = 20;
				_disclosurePleaseContactMeCheckBox.label = "Please contact me";
				item.addChild(_disclosurePleaseContactMeCheckBox);
				_disclosureCodeReviewAllowedCheckBox = new CheckBox();
				_disclosureCodeReviewAllowedCheckBox.addEventListener(MouseEvent.CLICK, handleDrawingCheckboxClick);
				_disclosureCodeReviewAllowedCheckBox.width = 300;
				_disclosureCodeReviewAllowedCheckBox.height = 20;
				_disclosureCodeReviewAllowedCheckBox.label = "Yes, you may look at my project code";
				item.addChild(_disclosureCodeReviewAllowedCheckBox);
				_form.addChild(item);
			}

            // Summary selection
			_summaryItem = new FormItem();
			_summaryItem.label = "Summary:";
			if (_adminUser) {
				_summaryField = new TextInput();
				_summaryField.width = 328;
				_summaryItem.addChild(_summaryField);
			}
			_form.addChild(_summaryItem);
			
			// Ticket Type (Admin Users Only)
			if (_adminUser) {
				_ticketTypeItem = new FormItem();
				_ticketTypeItem.direction = "horizontal";
				_ticketTypeItem.label = "Ticket Type:";
				_typeRadio = new RadioButtonGroup();
				_typeRadio.addEventListener(Event.CHANGE, updatePriority)
				
				_typeBugRadioButton = new RadioButton();
				_typeBugRadioButton.label = "Bug";
				_typeBugRadioButton.value = 0;
				_typeBugRadioButton.selected = true;
				_typeBugRadioButton.group = _typeRadio;
				
				_typeFeatureRadioButton = new RadioButton();
				_typeFeatureRadioButton.label = "Feature";
				_typeFeatureRadioButton.value = 1;
				_typeFeatureRadioButton.group = _typeRadio;
				
				_ticketTypeItem.addChild(_typeBugRadioButton);
				_ticketTypeItem.addChild(_typeFeatureRadioButton);
				_form.addChild(_ticketTypeItem);
			}
			
			// Bug Priority (Admin Users Only)
			if (_adminUser) {
				item = new FormItem();
				item.direction = "horizontal";
				item.label = "Bug Priority:";
				_priorityRadio = new RadioButtonGroup();
				_priorityRadio.enabled = true;
				
				_priorityBlockerRadioButton = new RadioButton();
				_priorityBlockerRadioButton.label = Feedback.PRIORITY_OPTIONS[0].label;
				_priorityBlockerRadioButton.value = Feedback.PRIORITY_OPTIONS[0].index;
				_priorityBlockerRadioButton.group = _priorityRadio;
				
				_priorityMajorRadioButton = new RadioButton();
				_priorityMajorRadioButton.label = Feedback.PRIORITY_OPTIONS[1].label;
				_priorityMajorRadioButton.value = Feedback.PRIORITY_OPTIONS[1].index;
				_priorityMajorRadioButton.group = _priorityRadio;
				
				_priorityMinorRadioButton = new RadioButton();
				_priorityMinorRadioButton.label = Feedback.PRIORITY_OPTIONS[2].label;
				_priorityMinorRadioButton.value = Feedback.PRIORITY_OPTIONS[2].index;
				_priorityMinorRadioButton.group = _priorityRadio;
				
				_priorityNoneRadioButton = new RadioButton();
				_priorityNoneRadioButton.label = Feedback.PRIORITY_OPTIONS[3].label;
				_priorityNoneRadioButton.value = Feedback.PRIORITY_OPTIONS[3].index;
				_priorityNoneRadioButton.group = _priorityRadio;
				_priorityNoneRadioButton.selected = true;
				
				item.addChild(_priorityBlockerRadioButton);
				item.addChild(_priorityMajorRadioButton);
				item.addChild(_priorityMinorRadioButton);
				item.addChild(_priorityNoneRadioButton);
				_form.addChild(item);
			}
			
			// Feature Priority (Admin Users Only)
			if (_adminUser) {
				item = new FormItem();
				item.direction = "horizontal";
				item.label = "Feature Priority:";
				_featurePriorityRadio = new RadioButtonGroup();
				_featurePriorityRadio.enabled = false;
				
				_featurePriorityBlockerRadioButton = new RadioButton();
				_featurePriorityBlockerRadioButton.label = Feedback.FEATURE_IMPORTANCE[0].label;
				_featurePriorityBlockerRadioButton.group = _featurePriorityRadio;
				
				_featurePriorityUIRadioButton = new RadioButton();
				_featurePriorityUIRadioButton.label = Feedback.FEATURE_IMPORTANCE[1].label;
				_featurePriorityUIRadioButton.group = _featurePriorityRadio;
				
				_featurePriorityConsiderRadioButton = new RadioButton();
				_featurePriorityConsiderRadioButton.label = Feedback.FEATURE_IMPORTANCE[2].label;
				_featurePriorityConsiderRadioButton.group = _featurePriorityRadio;
				_featurePriorityConsiderRadioButton.selected = true;
				
				item.addChild(_featurePriorityBlockerRadioButton);
				item.addChild(_featurePriorityUIRadioButton);
				item.addChild(_featurePriorityConsiderRadioButton);
				_form.addChild(item);
			}
			
			// Description field
			item = new FormItem();
			item.label = "Description:";
			_form.addChild(item);
			_description = new TextArea();
			_description.width = FORM_WIDTH;
			_description.height = 100;
			_description.maxChars = 1000;
			_form.addChild(_description);
			
			// screenshot
			_drawingItem = new FormItem();
			_drawingItem.direction = "horizontal";
			_drawingItem.label = "Draw On Screen"
			drawingCheckbox = new CheckBox();
			drawingCheckbox.addEventListener(MouseEvent.CLICK, handleDrawingCheckboxClick);
			drawingCheckbox.width = 30;
			drawingCheckbox.height = 30;
			_drawingItem.addChild(drawingCheckbox);
			_form.addChild(_drawingItem);
			
			item = new FormItem();
			item.direction = "horizontal";
			item.label = "Screenshots";
			_screenshotText = new Label;
			_screenshotText.text = "0";
			item.addChild(_screenshotText);
			_form.addChild(item);
			
			rule = new HRule();
			rule.width = FORM_WIDTH;
			_form.addChild(rule);
			
			// Control buttons
			var group:HGroup = new HGroup();
			group.paddingRight = 5;
			group.paddingLeft = 100;
			
			_clearDrawingBtn = new Button();
			_clearDrawingBtn.label = "Clear Drawing";
			_clearDrawingBtn.id = "clear";
			_clearDrawingBtn.addEventListener(MouseEvent.CLICK, clearDrawing);
			_clearDrawingBtn.enabled = false;
			_clearDrawingBtn.styleName = "grayButton";
			group.addElement(_clearDrawingBtn);
			
			_addScreenShotButton = new Button();
			_addScreenShotButton.label = "Add Screenshot";
			_addScreenShotButton.id = "screenshotButton";
			_addScreenShotButton.styleName = "grayButton";
			_addScreenShotButton.addEventListener(MouseEvent.CLICK, addScreenShot);
			group.addElement(_addScreenShotButton);
			var btn:Button = new Button();
			btn.label = "Cancel";
			btn.id = "cancel";
			btn.addEventListener(MouseEvent.CLICK, cancelSession);
			group.addElement(btn);
			_submitBtn = new Button();
			_submitBtn.label = "Submit Issue";
			_submitBtn.id = "submit";
			_submitBtn.styleName = "grayButton";
			_submitBtn.addEventListener(MouseEvent.CLICK, processForm);
			_submitBtn.enabled = Feedback.instance.enabled;
			
			group.addElement(_submitBtn);
			_form.addChild(group);
			
			this.addChild(_form);
		}

		private function handleDrawingCheckboxClick(e:Event):void {
			Feedback.instance.toggleVellum = drawingCheckbox.selected;
			_drawingMenu.enabled = drawingCheckbox.selected;
			_clearDrawingBtn.enabled = drawingCheckbox.selected;
		}

		private function handleDrawingMenuChanged(e:Event):void
		{
			QAVellumSettings.drawingPreset = _drawingMenu.selectedIndex;
		}

		private function validateSummary(e:Event):void
		{
			// PJK commented this out because it appears to do nothing :) 8JAN13
			//if (_summaryMenu.selectedIndex > 0)
			//	formIsValid;
			if (_summaryMenu.selectedIndex == Feedback.ISSUE_INDEX) {
				_issueMenu.selectedIndex = 0;
				_issueMenu.visible = true;
			} else {
				_issueMenu.visible = false;
			}
		}
		
		override public function set initialized(value:Boolean):void
		{
			super.initialized = value;
			
			Feedback.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);
			FeedbackOperation.instance.addEventListener(OperationFaultEvent.FAULT, handleFault, false, 0, true);
			FeedbackOperation.instance.addEventListener(OperationFaultEvent.UNAUTHORIZED, handleFault, false, 0, true);

			if (_adminUser) {
				// set up validators for required fields
				var val:StringValidator = new StringValidator();
				val.source = _summaryField;
				val.property = "text";
				val.tooShortError = "Please enter a summary";
				val.requiredFieldError = val.tooShortError;
				val.minLength = 2;
				val.requiredFieldError = val.tooShortError;
				val.trigger = _submitBtn;
				val.triggerEvent = "rollOver";
			}

			var formItemPos:Point;

			_issueMenu = new PopOutMenu(ApplicationUI.instance, Feedback.ISSUE_OPTIONS, PopOutMenu.DIRECTION_BI);
			if (_adminUser) {
				formItemPos = this.globalToLocal(_ticketTypeItem.localToGlobal(new Point(0, 0)));
				_issueMenu.x = formItemPos.x + _ticketTypeItem.width + 20.0;
				_issueMenu.y = formItemPos.y;
				_issueMenu.closedHeight = 25;
				_issueMenu.closedWidth = 140;
				_issueMenu.popWidth = 140;
				_issueMenu.baseLabelOffsetX = -2;
				_issueMenu.baseLabelOffsetY = 1;
				_issueMenu.arrowOffsetX = 3;
				_issueMenu.drawComponent();
				_issueMenu.addEventListener(PopOutMenu.POP_OUT_CLOSED, putPopUpBackOn);
			} else {
				_issueMenu.visible = false;
			}
			this.rawChildren.addChild(_issueMenu);

			var presetArray:Array = [];
			for each (var preset:DrawingPreset in Styles.SKETCHING_PRESETS)
				presetArray.push({label:preset.label});
			
			_drawingMenu = new PopOutMenu(ApplicationUI.instance, presetArray, PopOutMenu.DIRECTION_UP);
			formItemPos = this.globalToLocal(_drawingItem.localToGlobal(new Point(0, 0)));
			_drawingMenu.x = formItemPos.x + _drawingItem.width;
			_drawingMenu.y = formItemPos.y + 2;
			_drawingMenu.closedHeight = 25;
			_drawingMenu.closedWidth = 120;
			_drawingMenu.popWidth = 120;
			_drawingMenu.defaultIndex = 2;
			_drawingMenu.baseLabelOffsetX = -2;
			_drawingMenu.baseLabelOffsetY = 1;
			_drawingMenu.arrowOffsetX = 3;
			_drawingMenu.addEventListener(PopOutMenu.ITEM_CHANGED, handleDrawingMenuChanged);
			_drawingMenu.drawComponent();
			_drawingMenu.enabled = false;
			this.rawChildren.addChild(_drawingMenu);

			if (!_adminUser) {
				_summaryMenu = new PopOutMenu(ApplicationUI.instance, Feedback.FEEDBACK_OPTIONS, PopOutMenu.DIRECTION_DOWN);
				formItemPos = this.globalToLocal(_summaryItem.localToGlobal(new Point(0, 0)));
				_summaryMenu.x = formItemPos.x + _summaryItem.width;
				_summaryMenu.y = formItemPos.y;
				_summaryMenu.closedHeight = 25;
				_summaryMenu.closedWidth = 200;
				_summaryMenu.popWidth = 200;
				_summaryMenu.baseLabelOffsetX = -2;
				_summaryMenu.baseLabelOffsetY = 1;
				_summaryMenu.arrowOffsetX = 3;
				_summaryMenu.drawComponent();
				_summaryMenu.addEventListener(PopOutMenu.ITEM_CHANGED, validateSummary);
				this.rawChildren.addChild(_summaryMenu);
			}
		}

        private function wikiButtonClick(event:MouseEvent):void{
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.OPEN_WIKI));
        }

		private function addScreenShot(event:MouseEvent):void
		{
			FeedbackOperation.instance.pushScreenshot();
			_screenshotText.text = Feedback.instance.screenShots.length.toString();
		}
		
		private function updatePriority(event:Event):void
		{
			_priorityRadio.enabled = _typeBugRadioButton.selected;
			_featurePriorityRadio.enabled = _typeFeatureRadioButton.selected;
		}

		private function putPopUpBackOn(event:Event = null):void
		{
			if(_issueMenu != null){
				if(this.rawChildren.contains(_issueMenu)){
					this.rawChildren.removeChild(_issueMenu);
					var location:Point = new Point(_issueMenu.x, _issueMenu.y);
					//location = globalToLocal(location);
					_issueMenu.x = location.x + _xOffset;
					_issueMenu.y = location.y;
					_xOffset = 0;
					addEventListener(Event.ENTER_FRAME, finishPopOutReplace);
				}
			}
			this.removeEventListener(MouseEvent.MOUSE_UP, putPopUpBackOn);
		}

		private function finishPopOutReplace(event:Event):void
		{
			this.rawChildren.addChild(_issueMenu);
			this.removeEventListener(Event.ENTER_FRAME, finishPopOutReplace);
		}

		private function bringPopOutToFront(event:Event):void
		{
			if (this.rawChildren.contains(_issueMenu)){
				this.rawChildren.removeChild(_issueMenu);
				var location:Point = new Point(_issueMenu.x, _issueMenu.y);
				
				if (location.x + _issueMenu.popWidth > this.width){
					_xOffset = (location.x + _issueMenu.popWidth) - this.width;
				}
				_issueMenu.x = location.x - _xOffset;
				_issueMenu.y = location.y;
				this.rawChildren.addChild(_issueMenu);
				this.addEventListener(MouseEvent.CLICK, putPopUpBackOn);
			}
		}
		
		private function handleFault(event:OperationFaultEvent):void
		{
			resetForm(true);
		}
		
		private function handlePropertyChange(event:PropertyChangeEvent):void
		{
			switch (event.property) {
				case "enabled":
					_submitBtn.enabled = Feedback.instance.enabled;
					break;
				case "pending":
					if (!Feedback.instance.pending)
						resetForm();
					else
						formEnabled = !Feedback.instance.pending;
					break;
			}
		}
		
		/**
		 * reset form; this is done following a successful ticket submission both
		 * as a signal that it was a success, and to clear out the fields for 
		 * the next ticket. 
		 * @param fromFault
		 * 
		 */
		public function resetForm(fromFault:Boolean=false):void
		{
			if (!fromFault) {
                //505
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:505}));
				
				if (!_adminUser) {
					_summaryMenu.selectedIndex = 0;
					_issueMenu.visible = false;
				} else {
					_summaryField.text = "";
					_typeBugRadioButton.selected = true;
					_priorityRadio.enabled = true;
					_priorityNoneRadioButton.selected = true;
					_featurePriorityRadio.enabled = false;
					_featurePriorityConsiderRadioButton.selected = true;
					_issueMenu.selectedIndex = 0;
				}

				_screenshotText.text = Feedback.instance.screenShots.length.toString();
				_description.text = "";
				_disclosureCodeReviewAllowedCheckBox.selected = false;
				_disclosurePleaseContactMeCheckBox.selected = false;

				clearDrawing(null);
			}
			formEnabled = true;
		}
		
		private function set formEnabled(value:Boolean):void
		{
			var enabledAlpha:Number = value?1.0:0.1;
			_form.enabled = value;
			_issueMenu.alpha = enabledAlpha;
			if(!_adminUser) _summaryMenu.alpha = enabledAlpha;
			_drawingMenu.enabled = value && Feedback.instance.toggleVellum;
		}
		
		/**
		 * close the interface 
		 * @param e
		 * 
		 */
		private function cancelSession(e:Event):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.HIDE_QA_WINDOW));
			QAVellumSettings.vellum.visible = false;
		}
		
		/**
		 * begin the ticket submission process by sending the login information the user entered. 
		 * while the submission is occurring, disable the form so the user knows this is happening.
		 * @param e
		 * 
		 */
		private function processForm(e:Event):void
		{
			if (formIsValid) {
				if(_adminUser){
					Feedback.instance.summary = _summaryField.text;				
					if (_typeRadio.selectedValue == 0){
						Feedback.instance.assignee = Feedback.BUG_ASSIGNEE;
						Feedback.instance.project = Feedback.PROJECT_KEY_BUG;
						Feedback.instance.typeIndex = 5;
					}else if(_typeRadio.selectedValue == 1){
						Feedback.instance.project = Feedback.PROJECT_KEY_FEATURE;
						Feedback.instance.typeIndex = 6;
					}
					Feedback.instance.priorityIndex = int(_priorityRadio.selectedValue);
					Feedback.instance.featureImportance = _featurePriorityRadio.selection.label;
					if(!(_issueMenu.selectedItem.data is int)){
						Feedback.instance.associatedFeatureId = _issueMenu.selectedItem.data.toString();
					}
				}else{
					Feedback.instance.priorityIndex = 6;
					Feedback.instance.disclosureCodeReviewAllowed = _disclosureCodeReviewAllowedCheckBox.selected;
					Feedback.instance.disclosurePleaseContactMe = _disclosurePleaseContactMeCheckBox.selected;
					Feedback.instance.summary = _summaryMenu.selectedItem.label;
					Feedback.instance.typeIndex = Feedback.FEEDBACK_TYPES_AND_PRIORITIES[_summaryMenu.selectedIndex].type;
					if (_summaryMenu.selectedIndex == Feedback.ISSUE_INDEX && _issueMenu.selectedIndex > 0) {
						Feedback.instance.summary += " " + _issueMenu.selectedItem.label;
					}
				}				
				Feedback.instance.description = _description.text;

				FeedbackOperation.instance.execute();
			}
		}
		
		private function get formIsValid():Boolean
		{
			if (_adminUser) {
				if (_summaryField.errorString)
					return false;
			} else {
				if (_summaryMenu.selectedIndex > 0) {
					_summaryMenu.graphics.clear();
					return true;
				} else {
					_summaryMenu.graphics.lineStyle(1, 0xff0000, 0.6, true);
					_summaryMenu.graphics.drawRoundRect(-2, -2, _summaryMenu.width+2, _summaryMenu.itemHeight+10, 2, 2);
					return false;
				}
			}
			return true;
		}
		
		private function clearDrawing(event:Event):void
		{
			QAVellumSettings.vellum.clear();
		}
	}
}
