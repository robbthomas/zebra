/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.application.palettes
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.Styles;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.DrawingPreset;
	import com.alleni.author.model.ui.Feedback;
	import com.alleni.author.model.ui.QAVellumSettings;
	import com.alleni.author.service.FeedbackOperation;
	import com.alleni.author.view.ui.controls.PopOutMenu;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.ByteArray;
	
	import mx.containers.Form;
	import mx.containers.FormItem;
	import mx.containers.VBox;
	import mx.containers.ViewStack;
	import mx.controls.HRule;
	import mx.events.PropertyChangeEvent;
	import mx.validators.StringValidator;
	
	import spark.components.Button;
	import spark.components.CheckBox;
	import spark.components.HGroup;
	import spark.components.TextArea;
	import spark.components.TextInput;
	
	public class QAPanel extends VBox
	{
		private var _form:Form;
		private var _summary:TextInput;
		private var _username:TextInput;
		private var _password:TextInput;
		private var _description:TextArea;
		private var _type:PopOutMenu;
		private var _priority:PopOutMenu;
		private var _presets:PopOutMenu;
		private var _thumbContainer:Sprite;
		private var _vellumToggle:CheckBox;
		private var _drawingBox:VBox;
		private var _drawingStack:ViewStack;
		private var _drawingBoxHeight:Number;
		private var _clearDrawingBtn:Button;
		private var _submitBtn:Button;
		
		private var _thumbSent:Boolean = false;
		
		public function QAPanel()
		{
			super();
			this.styleName = "default";
			this.horizontalScrollPolicy = "off";
			this.verticalScrollPolicy = "off";
			this.width = 355;
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			// flex bug causes this to be called more than once in certain circumstances
			// let's ensure we only create one form in that case.
			if (_form)
				return;
			
			// issue tracking form
			_form = new Form();
			_form.horizontalScrollPolicy = "off";
			
			// Summary field (required)
			var item:FormItem = new FormItem();
			item.label = "Summary:";
			item.required = true;
			_summary = new TextInput();
			_summary.width = 200;
			_summary.maxChars = 60;
			item.addChild(_summary);
			_form.addChild(item);
			
			// visual separator
			var rule:HRule = new HRule();
			rule.width = 325;
			_form.addChild(rule);
			
			// Username field (required)
			item = new FormItem();
			item.label = "Username:";
			item.required = true;
			_username = new TextInput();
			_username.width = 200;
			_username.maxChars = 15;
			item.addChild(_username);
			_form.addChild(item);
			
			// Password field (required)
			item = new FormItem();
			item.label = "Password:";
			item.required = true;
			_password = new TextInput();
			_password.width = 200;
			_password.maxChars = 15;
			_password.displayAsPassword = true;
			item.addChild(_password);
			_form.addChild(item);
			
			// Description field
			item = new FormItem();
			item.label = "Description:";
			_form.addChild(item);
			_description = new TextArea();
			_description.width = 325;
			_description.height = 100;
			_description.maxChars = 1000;
			_form.addChild(_description);
			
			// Priority field
			item = new FormItem();
			item.label = "Priority:";
			_form.addChild(item);
			
			// Type field
			item = new FormItem();
			item.label = "Type:";
			_form.addChild(item);
			
			// Attach Project and/or Screenshot field
			item = new FormItem();
			item.label = "Attach:";
			var attachProjectCheckbox:CheckBox = new CheckBox();
			attachProjectCheckbox.selected = true;
			attachProjectCheckbox.label = "Current Project";
			attachProjectCheckbox.addEventListener(MouseEvent.CLICK, function():void {
				Feedback.instance.toggleAttachProject = attachProjectCheckbox.selected;
			});
			item.addChild(attachProjectCheckbox);
			_form.addChild(item);
			item = new FormItem();
			item.direction = "horizontal";
			var screenshotCheckbox:CheckBox = new CheckBox();
			screenshotCheckbox.label = "Screenshot   ";
			screenshotCheckbox.addEventListener(MouseEvent.CLICK, function():void {
				Feedback.instance.toggleAttachScreenshot = screenshotCheckbox.selected;
				_thumbContainer.visible = screenshotCheckbox.selected;
			});
			screenshotCheckbox.width = 200;
			item.addChild(screenshotCheckbox);
			_thumbContainer = new Sprite();
			_thumbContainer.visible = false;
			item.rawChildren.addChild(_thumbContainer);
			_thumbContainer.x = 200;
			_form.addChild(item);
			/*
			var presets:DropDownList = new DropDownList();
			var presetIList:IList = new ArrayList();
			for each(var preset:DrawingPreset in Styles.DRAWING_PRESETS)
			presetIList.addItem(preset);
			presets.dataProvider = presetIList;
			presets.selectedIndex = 2;
			presets.addEventListener(IndexChangeEvent.CHANGE, handleChangeDrawingPreset);
			*/
			
			// Drawing toggle for vellum overlay
			item = new FormItem();
			//item.label = "Drawing...";
			_vellumToggle = new CheckBox();
			_vellumToggle.label = "Drawing...";
			_vellumToggle.addEventListener(MouseEvent.CLICK, toggleVellum);
			item.addChild(_vellumToggle);
			_form.addChild(item);
			
			// Drawing selection UI
			item = new FormItem();
			_drawingStack = new ViewStack();
			var nullVBox:VBox = new VBox();
			_drawingStack.addChild(nullVBox);
			_drawingStack.resizeToContent = true;
			_drawingStack.selectedIndex = 0;
			_drawingBox = new VBox();
			_drawingBox.styleName = "formBox";
			var boxItem:FormItem = new FormItem();
			boxItem.direction = "horizontal";
			boxItem.label = "Weight:";
			_drawingBox.addChild(boxItem);
			boxItem = new FormItem();
			boxItem.direction = "horizontal";
			boxItem.label = "Color:";
			
			//boxItem.addChild(presets);
			
			//_drawingBox.addChild(boxItem);
			//_drawingStack.addChild(_drawingBox);
			//item.addChild(_drawingStack);
			_form.addChild(item);
			
			// visual separator
			rule = new HRule();
			rule.width = 325;
			_form.addChild(rule);
			
			// Cancel and Submit buttons
			var group:HGroup = new HGroup();
			group.paddingRight = 5;
			group.paddingLeft = 95;
			_clearDrawingBtn = new Button();
			_clearDrawingBtn.label = "Clear Drawing";
			_clearDrawingBtn.id = "clear";
			_clearDrawingBtn.addEventListener(MouseEvent.CLICK, clearDrawing);
			_clearDrawingBtn.enabled = false;
			group.addElement(_clearDrawingBtn);
			var btn:Button = new Button();
			btn.label = "Cancel";
			btn.id = "cancel";
			btn.addEventListener(MouseEvent.CLICK, cancelSession);
			group.addElement(btn);
			_submitBtn = new Button();
			_submitBtn.label = "Submit Issue";
			_submitBtn.id = "submit";
			_submitBtn.addEventListener(MouseEvent.CLICK, processForm);
			_submitBtn.enabled = Feedback.instance.enabled;
			group.addElement(_submitBtn);
			_form.addChild(group);
			
			// Add the form to the panel
			this.addChild(_form);
			
			
			// pop-outs must be added separately to keep them on top of the UIComponent-based UI.
			// these would not have to use literal positions, we could use localToGlobal on item positions.
			// since this UI is probably changing anyway, that has not been implemented yet.
			_type = new PopOutMenu(this, Feedback.TYPE_OPTIONS, PopOutMenu.DIRECTION_DOWN);
			_type.x = 95;
			_type.y = 285;
			_type.closedHeight = 25;
			_type.closedWidth = 100;
			_type.popWidth = 100;
			_type.defaultIndex = Feedback.TYPE_DEFAULT;
			_type.baseLabelOffsetX = -2;
			_type.baseLabelOffsetY = 1;
			_type.arrowOffsetX = 3;
			_type.drawComponent();
			this.rawChildren.addChild(_type);
			
			_priority = new PopOutMenu(this, Feedback.PRIORITY_OPTIONS, PopOutMenu.DIRECTION_DOWN);
			_priority.x = 95;
			_priority.y = 255;
			_priority.closedHeight = 25;
			_priority.closedWidth = 60;
			_priority.popWidth = 60;
			_priority.defaultIndex = Feedback.PRIORITY_DEFAULT;
			_priority.baseLabelOffsetX = -2;
			_priority.baseLabelOffsetY = 1;
			_priority.arrowOffsetX = 3;
			_priority.drawComponent();
			this.rawChildren.addChild(_priority);
			
			var presetArray:Array = [];
			for each(var preset:DrawingPreset in Styles.SKETCHING_PRESETS)
			presetArray.push({label:preset.label});
			
			_presets = new PopOutMenu(this, presetArray, PopOutMenu.DIRECTION_UP);
			_presets.x = 200;
			_presets.y = 372;
			_presets.closedHeight = 25;
			_presets.closedWidth = 105;
			_presets.popWidth = 105;
			_presets.defaultIndex = 2;
			_presets.baseLabelOffsetX = -2;
			_presets.baseLabelOffsetY = 1;
			_presets.arrowOffsetX = 3;
			_presets.addEventListener(PopOutMenu.ITEM_CHANGED, handleChangeDrawingPreset);
			_presets.drawComponent();
			this.rawChildren.addChild(_presets);
		}
		
		/**
		 * create the form and its elements as the panel comes online. 
		 * 
		 */
		override public function set initialized(value:Boolean):void
		{
			super.initialized = value;
			
			// set up validators for required fields
			var val:StringValidator = new StringValidator();
			val.source = _summary;
			val.property = "text";
			val.tooShortError = "Please enter a ticket summary";
			val.requiredFieldError = val.tooShortError;
			val.minLength = 2;
			val.requiredFieldError = val.tooShortError;
			val.trigger = _submitBtn;
			val.triggerEvent = "rollOver";

			val = new StringValidator();
			val.source = _username;
			val.property = "text";
			val.tooShortError = "Please enter your username";
			val.requiredFieldError = val.tooShortError;
			val.minLength = 2;
			val.requiredFieldError = val.tooShortError;
			val.trigger = _submitBtn;
			val.triggerEvent = "rollOver";
			
			val = new StringValidator();
			val.source = _password;
			val.property = "text";
			val.tooShortError = "Please enter your password";
			val.requiredFieldError = val.tooShortError;
			val.minLength = 2;
			val.requiredFieldError = val.tooShortError;
			val.trigger = _submitBtn;
			val.triggerEvent = "rollOver";
			
			// grab username and password from local store if they exist
			var storedBytes:ByteArray = TaconiteFactory.getLocalStoreImplementation().getItem("qaUsername");
			if (storedBytes != null) { 
				_username.text = storedBytes.readUTFBytes(storedBytes.length);
				storedBytes.clear();
			}
			storedBytes = TaconiteFactory.getLocalStoreImplementation().getItem("qaPassword");
			if (storedBytes != null) _password.text = storedBytes.readUTFBytes(storedBytes.length);
			
			Feedback.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);
			FeedbackOperation.instance.addEventListener(OperationFaultEvent.FAULT, handleFault, false, 0, true);
			FeedbackOperation.instance.addEventListener(OperationFaultEvent.UNAUTHORIZED, handleFault, false, 0, true);
		}
		
		private function handleFault(event:OperationFaultEvent):void
		{
			resetForm(true);
			if (event.type == OperationFaultEvent.UNAUTHORIZED) {
				_username.errorString = "Please check your username";
				_password.errorString = "Please check your password";
			}	
		}
		
		private function handlePropertyChange(event:PropertyChangeEvent):void
		{
			switch (event.property) {
				case "enabled":
					_submitBtn.enabled = Feedback.instance.enabled;
					break;
				case "pending":
					if (!Feedback.instance.pending)
						resetForm();
					else
						formEnabled = !Feedback.instance.pending;
					break;
				case "screenshotThumb":
					while (_thumbContainer.numChildren > 0)
						_thumbContainer.removeChildAt(0);
					_thumbContainer.addChild(Feedback.instance.screenshotThumb);
					break;
			}
		}
		
		/**
		 * reset form; this is done following a successful ticket submission both
		 * as a signal that it was a success, and to clear out the fields for 
		 * the next ticket. 
		 * @param fromFault
		 * 
		 */
		private function resetForm(fromFault:Boolean=false):void
		{
			if (!fromFault) { 
				_summary.text = "";
				_summary.errorString = "";
				_description.text = "";
				_type.selectedIndex = Feedback.TYPE_DEFAULT;
				_priority.selectedIndex = Feedback.PRIORITY_DEFAULT;
			}
			formEnabled = true;
		}
		
		private function set formEnabled(value:Boolean):void
		{
			var enabledAlpha:Number = value?1.0:0.1;
			_form.enabled = value;
			_type.alpha = enabledAlpha;
			_priority.alpha = enabledAlpha;
			_presets.alpha = enabledAlpha;
		}
		
		/**
		 * close the QA interface 
		 * @param e
		 * 
		 */
		private function cancelSession(e:MouseEvent):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.HIDE_QA_WINDOW));
			QAVellumSettings.vellum.visible = false;
		}
		
		/**
		 * begin the ticket submission process by sending the login information the user entered. 
		 * while the submission is occurring, disable the form so the user knows this is happening.
		 * @param e
		 * 
		 */
		private function processForm(e:MouseEvent):void
		{
			if (_summary.errorString || _username.errorString || _password.errorString) LogService.error("QAPanel: invalid form");
			else {
				/*Feedback.instance.username = _username.text; credentials originate at the model, now
				Feedback.instance.password = _password.text;*/
				Feedback.instance.summary = _summary.text;
				Feedback.instance.description = _description.text;
				Feedback.instance.priorityIndex = _priority.selectedIndex;
				Feedback.instance.typeIndex = _type.selectedIndex;
				
				FeedbackOperation.instance.execute();
			}
		}
		
		private function clearDrawing(event:Event):void
		{
			QAVellumSettings.vellum.clear();
		}
		
		private function handleChangeDrawingPreset(event:Event):void
		{
			QAVellumSettings.drawingPreset = _presets.selectedIndex;		
		}
		
		private function toggleVellum(e:Event=null):void
		{
			Feedback.instance.toggleVellum = _vellumToggle.selected;
			if (_vellumToggle.selected) 
				_drawingStack.selectedIndex = 1;
			else
				_drawingStack.selectedIndex = 0;
			_clearDrawingBtn.enabled = _vellumToggle.selected;
		}
	}
}