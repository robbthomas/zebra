/**
 * ¬¨¬®¬¨¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.application.palettes
{
	import assets.images.*;
	
	import com.alleni.author.controller.objects.PathEditingMediator;
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.palettes.ToolboxController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.ToolEvent;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.objects.Path;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	
	import mx.events.PropertyChangeEvent;
	
	
	public class PolygonEditingPanel extends Sprite
	{
		private var _path:Path;
		private var _curve:Sprite;
		
		private static var _numSides:int = 5;
		private var _sidesText:RichTextField;
		private var _textMediator:TextEditMediator;
		private var _previewButton:Sprite;
		private var _closing:Boolean;
		
		private static const POLYGON_MIN_SIDES:Number = 3;
		private static const POLYGON_MAX_SIDES:Number = 128;

		
		private static const OVERLAY_Y:Number = 62;
		private static const OVERLAY_WIDTH:Number = 86;
		private static const OVERLAY_HEIGHT:Number = 170;
		private static const GRAPHIC_X:Number = 3;
		private static const GRAPHIC_Y:Number = OVERLAY_Y+0;
		private static const POLY_RADIUS:Number = 37.5;
		private static const POLY_X:Number = 6;
		private static const POLY_Y:Number = GRAPHIC_Y + 27;
		
		private static const SIDES_FIELD_X:Number = 42;
		private static const SIDES_FIELD_Y:Number = OVERLAY_Y + 117;
		private static const SIDES_FIELD_WIDTH:Number = 32;
		private static const SIDES_FIELD_HEIGHT:Number = 15;
		
		private static const PREVIEW_X:Number = 12;
		private static const PREVIEW_Y:Number = OVERLAY_Y + 151;
		private static const PREVIEW_WIDTH:Number = 60;
		private static const PREVIEW_HEIGHT:Number = 13;
		private static const PREVIEW_FILL:uint = 0xaaaaff;  
		private static const PREVIEW_ALPHA:Number = 0.0;
		private static const PREVIEW_ROLL_ALPHA:Number = 0.5;
		private static const PREVIEW_DOWN_ALPHA:Number = 1.0;
		private static const PREVIEW_DOWN_BLEND:String = BlendMode.INVERT;  
		
		/*
			The Preview pushbutton is a filled-roundrect that is placed above the graphic in the .fxg file.
			This approach allows using the exact artistic font & look from the designer.
		
			The edit-text field is kept active and selected at all times (except while entering a number)
			Drawing a polygon (after double-clicking the tool to lock it in) will re-select the text afterward.
			Pressing enter will update the preview and then re-select the text.
			Same for arrow keys.
		*/
		
		public function PolygonEditingPanel()
		{
			super();
			
			drawBackground();
			
			var overlay:DisplayObject = new polygonCreationOverlay();  // load .fxg file
			addChild(overlay);
			overlay.x = GRAPHIC_X;
			overlay.y = GRAPHIC_Y;
			
			_path = new Path();
			_curve = new Sprite();
			_curve.x = POLY_X;
			_curve.y = POLY_Y;
			addChild(_curve);
			
			setupView();
			Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
			addEventListener(Event.ADDED_TO_STAGE, initialize);
			_closing = false;
		}
		
		public static function get numSides():int
		{
			return _numSides;
		}
		
		public function initialize(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyListener, false, 1000);  // priority=1000
			_sidesText.addEventListener(KeyboardEvent.KEY_DOWN, keyListener, false, 1000);  // priority=1000
			ApplicationController.instance.addEventListener(ToolEvent.TOOL_APPLIED, toolAppliedListener);

			updateView();
		}
		
		public function closePanel():void
		{
			_closing = true;
			_textMediator.closeEditing(true);
			Application.instance.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, keyListener);
			_sidesText.removeEventListener(KeyboardEvent.KEY_DOWN, keyListener);
			ApplicationController.instance.removeEventListener(ToolEvent.TOOL_APPLIED, toolAppliedListener);
			stage.removeEventListener(Event.ENTER_FRAME, selectTextListener);

			if (this.parent && parent.contains(this))
				parent.removeChild(this);
		}
		
		private function handleApplicationPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "currentTool") {
				// click on Arrow tool, etc, remove this panel from over the toolbox
				if (event.newValue != ToolboxController.POLYGON) {
					closePanel(); 
				}
			}
		}
		
		private function toolAppliedListener(event:ToolEvent):void
		{
			// just drew a polygon:  force the text to re-select
			if (this.stage)
				updateView();
			else
				ApplicationController.instance.removeEventListener(ToolEvent.TOOL_APPLIED, toolAppliedListener);
		}
		
		private function keyListener(event:KeyboardEvent):void
		{
			trace("key="+event.keyCode, "oldSides="+_numSides, "bubble="+event.bubbles);
			switch (event.keyCode) {
				case Keyboard.UP:
					_numSides = int(_sidesText.text);
					++_numSides;
					break;
				case Keyboard.DOWN:
					_numSides = int(_sidesText.text);
					--_numSides;
					break;
				case Keyboard.LEFT:
				case Keyboard.DOWN:
					break;
				default:
					return;  // don't stopPropagation
			}
			event.stopImmediatePropagation();
			updateView();  // do preview
		}

		
		private function setupView():void
		{
			_sidesText = createTextField(SIDES_FIELD_X, SIDES_FIELD_Y, SIDES_FIELD_WIDTH, SIDES_FIELD_HEIGHT, enterKeyListener);
			addChild(_sidesText);
			
			_previewButton = new Sprite();
			addChild(_previewButton);
			_previewButton.x = PREVIEW_X;
			_previewButton.y = PREVIEW_Y;
			_previewButton.alpha = PREVIEW_ALPHA;
			drawButton(_previewButton.graphics);
			_previewButton.addEventListener(MouseEvent.ROLL_OVER, previewButtonMouseListener);
			_previewButton.addEventListener(MouseEvent.ROLL_OUT, previewButtonMouseListener);
			_previewButton.addEventListener(MouseEvent.MOUSE_DOWN, previewButtonMouseListener);
		}
		
		private function drawButton(g:Graphics):void
		{
			g.clear();
			g.beginFill(PREVIEW_FILL, 1.0);
			g.drawRoundRect(0,0, PREVIEW_WIDTH, PREVIEW_HEIGHT, 3,3);
			g.endFill();
		}
		
		private function previewButtonMouseListener(event:MouseEvent):void
		{
			switch (event.type) {
				case MouseEvent.ROLL_OVER:
					_previewButton.alpha = PREVIEW_ROLL_ALPHA;
					break;
				case MouseEvent.ROLL_OUT:
					_previewButton.alpha = PREVIEW_ALPHA;
					break;
				case MouseEvent.MOUSE_DOWN:
					_previewButton.alpha = PREVIEW_DOWN_ALPHA;
					_previewButton.blendMode = PREVIEW_DOWN_BLEND;
					stage.addEventListener(MouseEvent.MOUSE_UP, previewButtonMouseListener);
					_numSides = int(_sidesText.text);
					updateView();  // do preview
					break;
				case MouseEvent.MOUSE_UP:
					stage.removeEventListener(MouseEvent.MOUSE_UP, previewButtonMouseListener);
					_previewButton.alpha = PREVIEW_ROLL_ALPHA;
					_previewButton.blendMode = BlendMode.NORMAL;
					break;
			}
		}
		
		private function enterKeyListener(value:String):void
		{
			// pressed the Enter key
			_numSides = int(_sidesText.text);
			updateView();
		}
				
		private function updateView():void
		{
			_numSides = Math.max(POLYGON_MIN_SIDES, Math.min(POLYGON_MAX_SIDES, _numSides));
			_sidesText.rawText = formatNumber(_numSides);
			_path.buildPolygon(POLY_RADIUS, _numSides);
			
			var g:Graphics = _curve.graphics;
			g.clear();
			g.lineStyle(2, 0x577da6);
			g.beginFill(0xbfd6ed);
			_path.drawPath(g);
			g.endFill();
			
			if (!_closing) {
				selectAll(_sidesText, false);  // de-select so it will re-select
				stage.addEventListener(Event.ENTER_FRAME, selectTextListener);
			}
		}
		
		private function selectTextListener(event:Event):void
		{
			// can't seem to open text editing without waiting for Enter event
			// likewise, select-all must be done separate from open-editing
			if (_sidesText.editing == false) {
				trace("+ open text editing");
				_textMediator.openEditing();
			} else if (_sidesText.selectionStart == _sidesText.selectionEnd && _sidesText.text.length > 0) {
				trace("+ select all text");
				selectAll(_sidesText);
			} else {
				trace("+ refresh text selection");
				_sidesText.refreshSelection();
				stage.removeEventListener(Event.ENTER_FRAME, selectTextListener);
			}
		}
		
		private function createTextField(xxx:Number, yyy:Number, www:Number, hhh:Number, onCloseEditing:Function):RichTextField
		{
			var background:Sprite = new Sprite();
			background.graphics.beginFill(0xffffff);
			background.graphics.drawRect(0,0, www, hhh);
			background.graphics.endFill();
			background.x = xxx;
			background.y = yyy;
			addChild(background);
			
			var field:RichTextField = new RichTextField(www+50,hhh+10);  // fudge size to make text work
			field.embedFonts = true;
			field.x = xxx - 10;  // fudge to place the text properly
			field.y = yyy + 2;
			_textMediator = new TextEditMediator();
			_textMediator.handleTextEditEvents(field, null, null, TextEditMediator.closeOnEnter, onCloseEditing, textHitTestFunc, false);
			return field;
		}
		
		private function textHitTestFunc(globalX:Number, globalY:Number):Boolean
		{
			return true;  // do not close editing, when double-click polygon tool
		}
		
		private function selectAll(field:RichTextField, select:Boolean=true):void
		{
			if (field.textFlow.interactionManager) {
				if (select)
					field.textFlow.interactionManager.selectAll();
				else
					field.textFlow.interactionManager.selectRange(0,0);
				field.textFlow.interactionManager.setFocus();
			}
		}
		
		private function formatNumber(value:Number):String
		{
			var text:String = value.toString();
			if (text.indexOf(".") != -1) {  // has decimal point
				// limit to one decimal place
				return text.replace(/(\.[0-9])[0-9]*/,"$1");
			} else {
				return text;
			}
		}

		private function drawBackground():void
		{
			graphics.clear();
			graphics.beginFill(0xDCDCDC);
			graphics.drawRoundRect(0, OVERLAY_Y, OVERLAY_WIDTH, OVERLAY_HEIGHT, 20, 20);
			graphics.endFill();
		}
	}
}
