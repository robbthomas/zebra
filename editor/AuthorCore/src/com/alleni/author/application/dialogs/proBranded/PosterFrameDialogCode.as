package com.alleni.author.application.dialogs.proBranded
{
	import spark.components.Group;

	public class PosterFrameDialogCode extends Group
	{
		public static const IMAGE_WIDTH:int = 100;
		public static const IMAGE_HEIGHT:int = 70;
		
		public function PosterFrameDialogCode()
		{
			super();
		}
	}
}