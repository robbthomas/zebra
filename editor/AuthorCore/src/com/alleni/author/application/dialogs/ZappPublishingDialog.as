package com.alleni.author.application.dialogs
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Dock;
	import com.alleni.author.view.ui.palettes.DockDetailsView;
	import com.alleni.author.view.ui.palettes.PopoverView;
	import com.alleni.author.view.ui.palettes.ZappingDialogView;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import mx.core.UIComponent;
	
	public class ZappPublishingDialog extends UIComponent
	{
		private static var _instance:ZappPublishingDialog;
		public static var howToShare:String = "";
        public static var shareMessage:Boolean = false;
        private var panel:DisplayObject;

		public function ZappPublishingDialog()
		{
			Utilities.assert(!_instance);
			_instance = this;
			super();
			Dock.instance.publishingCandidate = Application.instance.document.project;
			const panel:DisplayObject = new ZappingDialogView(null, "publishingCandidate", 0, PopoverView.POINT_NONE, howToShare, shareMessage);
			panel.addEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
			addChild(panel);
		}
		
		public static function get instance():ZappPublishingDialog
		{
			return _instance;
		}
		
		public static function requestHide():Boolean
		{
			if (!_instance || !_instance.stage) return false;
			
			_instance.handleClose(null);
			
			return true;
		}
		
		private function handleAddedToStage(event:Event):void
		{
			event.target.removeEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
			
			const panel:DisplayObject = event.target as DisplayObject;
            panel.addEventListener(Event.ENTER_FRAME, function(event:Event):void{
                event.currentTarget.removeEventListener(event.type, arguments.callee);
                panel.parent.x -= ZappingDialogView.PANEL_WIDTH/2;
                panel.parent.y -= ZappingDialogView.PANEL_HEIGHT/2;
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
            })
			panel.addEventListener(Event.CLOSE, handleClose);
			this.addEventListener(Event.REMOVED_FROM_STAGE, handleRemovedFromStage);
		}

		private function handleClose(event:Event):void
		{
			if (event) event.target.removeEventListener(Event.CLOSE, handleClose);
			ApplicationController.instance.removePopup(this);
		}
		
		private function handleRemovedFromStage(event:Event):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
			this.removeEventListener(Event.REMOVED_FROM_STAGE, handleRemovedFromStage);
			_instance = null;
		}
	}
}
