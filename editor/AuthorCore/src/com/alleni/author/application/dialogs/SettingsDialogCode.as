package com.alleni.author.application.dialogs {
	import caurina.transitions.Tweener;

import com.alleni.author.Navigation.EventPage;

import com.alleni.author.application.ApplicationUI;
	import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.app.ProjectController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.GadgetController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.definition.application.PlayerIcons;
	import com.alleni.author.document.Document;
	import com.alleni.author.model.World;
import com.alleni.author.model.objects.EventFlowController;
import com.alleni.author.model.objects.EventFlowController;
import com.alleni.author.model.objects.ProjectObject;
import com.alleni.author.model.objects.TemplateValueObject;
    import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;
import com.alleni.author.service.users.UserService;
import com.alleni.author.util.StringUtils;
import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
	import com.alleni.author.view.ui.controls.Alert;
	import com.alleni.author.view.ui.controls.PlayerControls;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.display.DisplayObject;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Point;

import mx.controls.CheckBox;

import mx.core.UIComponent;
	import mx.events.CloseEvent;
import mx.events.FlexEvent;
	import mx.events.ResizeEvent;

import spark.components.Button;

import spark.components.RadioButton;
	import spark.components.RadioButtonGroup;
import spark.components.RichEditableText;
import spark.components.TextArea;
	import spark.components.TextInput;
	
	public class SettingsDialogCode extends DialogContainer {

		protected static const KEY_CODE_ENTER:int = 13;
		protected static const RADIO_USER_CHOICE:String = "menu";
		protected static const RADIO_RESUME:String = "resume";
		
		private var _watermark:DisplayObject;
		private var _watermarkPosition:String;
		private var _bubble:ControlAreaToolTip;
		private var _oldFilename:String;
        private var _oldWidth:Number;
        private var _oldHeight:Number;
        private var _origWiringVisibility:Boolean;
        private var _validName:Boolean = true;

        [Bindable]
        public var accountTypeId:int;
		
		[Bindable]public var onNextOpen:RadioButtonGroup;
		public var fileName:TextInput;
		public var widthId:TextInput;
		public var heightId:TextInput;
		public var descriptionField:TextArea;
		public var watermarkComponent:UIComponent;
        public var onNextOpenBox:UIComponent;
        public var rdoRestart:RadioButton;
		public var rdoResume:RadioButton;
		public var rdoUser:RadioButton;
        public var okButton:Button;
        public var btnCreate:Button;
        public var upgradeToPro:CheckBox;

        protected var _project:Project;


		public function SettingsDialogCode() {
			
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete, false, 0, true);
			ApplicationController.addEventListener(NotificationNamesApplication.INVOKE_PROJECT, forceClosePopup);
            const world:World = Application.instance.document.root.value as World;
            onNextOpen = new RadioButtonGroup();
            onNextOpen.addEventListener(Event.CHANGE, handleOnNextChange)
            if(world.project != null){
                onNextOpen.selectedValue = world.project.onNextOpen;
            }
            _oldWidth = world.width;
            _oldHeight = world.height;
            _project = Application.instance.document.project;
            accountTypeId = (Application.instance.customerType.toLowerCase() == UserService.USER_PRO) ? 3 : 2;
		}

        [Bindable]
        public function get projectUpgradeAvailable():Boolean{
            return  accountTypeId == 3 && _project.accountTypeId < 3;
        }

        private function forceClosePopup(event:ApplicationEvent):void{
            ApplicationController.instance.removePopup(this);
        }

		protected function onCreationComplete(event:Event):void
		{
			setMyFocus();
			fileName.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
//            this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
//            this.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);

		}

        public function onMouseDown(event:MouseEvent):void{
            var object:Object = event.target;
            if(!(event.target is RichEditableText)) {
                this.startDrag();
            }


        }
        public function onMouseUp(event:MouseEvent):void{
            var object:Object = event.target;
            this.stopDrag();
        }

		public function handleOnNextChange(e:Event){
            var selectedOptionId:String = (e.currentTarget as RadioButtonGroup).selectedValue as String;

        }

		public function setMyFocus():void {
			this.focusManager.setFocus(fileName);
		}
		
		protected function onKeyUp(e:KeyboardEvent):void {
			if(e.keyCode == KEY_CODE_ENTER)
				handleOKClick();
		}
		
		override protected function createChildren():void {
			
			super.createChildren();
			trace("createChildren focus="+ApplicationUI.instance.stage.focus);
			const document:Document = Application.instance.document;
			const world:World = document.root.value as World;
			widthId.text = world.width.toString();
			heightId.text = world.height.toString();
			initRadioGroup(world);
			descriptionField.text = document.project.description;
			fileName.text = Application.instance.document.name;
			if (document.documentId != null) {
				_oldFilename = fileName.text; // remember to give alert if user changes the name of an existing project
			}
			createWatermark();
		}
		
		protected function initRadioGroup(world:World):void {
			if(world && world.project && world.project.onNextOpen) 
				switch(world.project.onNextOpen.toLowerCase()) {
					case RADIO_RESUME:
						rdoResume.selected=true;
						break;
					
					case RADIO_USER_CHOICE:
						rdoUser.selected=true;
						break;
					
					default:
						rdoRestart.selected=true;
						break;
				}
		}
		
		override public function set initialized(value:Boolean):void
		{
			super.initialized = value;
            hideWiring();    // hide MC so it doesn't show below the dialog, which is confusing
			if (value) {
				var counter:int = 0;   // wait for dialog to be placed
				var myStage:Stage = stage;
				myStage.addEventListener(Event.ENTER_FRAME, function(e:Event):void{
					if (++counter > 4) {
						myStage.removeEventListener(Event.ENTER_FRAME, arguments.callee);
						afterCreation();
					}
				});
			}
		}

        private function hideWiring():void
        {
            _origWiringVisibility = ApplicationController.instance.hideWiring();
        }

        private function restoreWiring():void
        {
            ApplicationController.instance.restoreWiring(_origWiringVisibility);
        }

		private function afterCreation():void
		{
			// this stuff needs to be done after rendering the dialog
			
			if (Application.instance.document.documentId == null) {
				// put a tooltip bubble above filename field for new project
				createBubble();
			}
		}
		
		override protected function handleRemovedFromStage(event:Event):void {
			super.handleRemovedFromStage(event);
			removeBubble();
            restoreWiring();
		}

		protected function createWatermark():void {
			_watermark = PlayerIcons.logoIcon;
			var watermarkScale:Number = PlayerControls.WATERMARK_WIDTH / _watermark.width;
			_watermark.scaleX = _watermark.scaleY = watermarkScale;
			_watermark.filters = [new GlowFilter(0, 1.0, 2.0, 2.0, 1.5, 3)];
			_watermark.alpha = 0.5;
			var watermarkRect:Sprite = new Sprite();
			drawWaterMarkRect(watermarkRect);
			watermarkRect.addChild(_watermark);
			var watermarkButton:SimpleButton = new SimpleButton(watermarkRect, watermarkRect, watermarkRect, watermarkRect);
			watermarkButton.addEventListener(MouseEvent.CLICK, handleWatermarkClick);
			_watermark.y = 2;
			updatePosition();
			watermarkButton.y = 5;
			watermarkComponent.addChild(watermarkButton);

            watermarkComponent.addEventListener(ResizeEvent.RESIZE, function(e:ResizeEvent):void {
                drawWaterMarkRect(watermarkRect);
                updatePosition();
            });
			
			const gadget:Project = Application.instance.document.project;
			if (gadget) {
				_watermarkPosition = gadget.watermarkPosition;
				updatePosition();
			}
		}

        private function drawWaterMarkRect(watermarkRect:Sprite):void {
            watermarkRect.graphics.beginFill(0xffffff, 0);
            watermarkRect.graphics.lineStyle();
            watermarkRect.graphics.drawRect(0, 0, watermarkComponent.width, 35);
            watermarkRect.graphics.endFill();
            watermarkRect.graphics.lineStyle(1, 0, 0.2);
            watermarkRect.graphics.moveTo(0, 0);
            watermarkRect.graphics.lineTo(0, 35);
            watermarkRect.graphics.lineTo(watermarkComponent.width, 35);
            watermarkRect.graphics.lineTo(watermarkComponent.width, 0);
        }
		
		private function handleWatermarkClick(e:Event):void {
			if (!_watermark) return;
			switch (_watermarkPosition) { // switch unnecessary now but for future flexibility
				case "right":
					_watermarkPosition = "left";
					break;
				case "left":
				default:
					_watermarkPosition = "right";
					break;
			}
			updatePosition();
		}
		
		private function updatePosition():void {
			if (!_watermark) return;
			switch (_watermarkPosition) { // switch unnecessary now but for future flexibility
				case "right":
					_watermark.x = watermarkComponent.width - _watermark.width - 20;
					break;
				case "left":
				default:
					_watermark.x = 10;
					break;
			}
		}
		
		
		private function createBubble():void {
            //TODO: We should not be using the stage for this, but this is to stop a crash if the stage is gone before we try to place the bubble.
            if(stage == null || !(this is TemplateDialog)){
                return;
            }
            _bubble = new ControlAreaToolTip([0xffff00, 0xbbbb00]);
			(this as TemplateDialog).tipBubble.addChild(_bubble);
			_bubble.displayDirection = ControlAreaToolTip.DISPLAY_ABOVE; // bubble above fileName, for new project
			_bubble.labelString = "Please name\nyour new project.";
		}

		private function removeBubble():void {
			if (_bubble && (this as TemplateDialog).tipBubble.contains(_bubble)) {
				(this as TemplateDialog).tipBubble.removeChild(_bubble);
				_bubble = null;
			}
		}
		
		
		public function handleOKClick():void {
			if(validateSettingParams()){
				saveProject();
            }
		}

        protected function generateValueObject():TemplateValueObject {
            var result:TemplateValueObject = new TemplateValueObject(fileName.text);
            result.width = Number(widthId.text);
            result.height = Number(heightId.text);
            result.accountTypeId = accountTypeId;
            result.onNextOpen = onNextOpen.selectedValue as String
            result.description = descriptionField.text;
            result.watermarkPosition = _watermarkPosition;
            return result;
        }
		
		protected function saveProject():void {
			const document:Document = Application.instance.document;
            var templateVO:TemplateValueObject = generateValueObject();
            templateVO.apply(document, this is TemplateDialogCode);
			
			ApplicationController.instance.removePopup(this);
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_SETTINGS_CHANGED));

            // rebuild event-flow images if width or height was changed by this dialog
            const world:World = document.root.value as World;
            if (world.width != _oldWidth || world.height != _oldHeight) {
                EventFlowController.instance.rebuildAllEventImages(world.eventFlow);   // will save when done
            } else {
                if(this is TemplateDialogCode){
                    ProjectController.instance.isTemplateDialogSave = true;
                }
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SAVE_PROJECT));
            }
		}
		
		protected function validateSettingParams():Boolean {
			removeBubble();  // so the bubble won't overlap alerts and look funny

            if(_validName == false){
                return false;
            }

			if (!ProjectController.instance.validateFileName(fileName.text)) {
				// alert is displayed by validateFileName();
				return false;
			}
			
			if (_oldFilename != null && _oldFilename != fileName.text) {
				Alert.cancelLabel = "Cancel";
				Alert.okLabel = "Rename";
				var prompt:String = "Please confirm renaming this project to:\n \"" + fileName.text + "\"";
				Alert.show(prompt, "Rename Project", Alert.FIRST | Alert.SECOND, alertRenameSelection, null, Alert.FIRST);
				return false;
			}
			
			var w:Number = Number(widthId.text);
			var h:Number = Number(heightId.text);
			if (w > 3600 || h > 3600) {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:73}));
				return false;
			}

            if (w < 50 || h < 50) {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:74}));
				return false;
			}

            if (upgradeToPro != null && upgradeToPro.selected){
                ApplicationController.instance.stripIllegalWires();
                _project.accountTypeId = 3;
                ApplicationController.instance.clearUndo();
                EventFlowController.instance.afterSwitchToProfessionalProject();
            }

			return true;
		}
		
		override protected function handleCancelClick():void {
			super.handleCancelClick();
			if (Application.instance.document.documentId == null) {
				Application.instance.window.exit();
				//ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.REQUEST_QUIT_APPLICATION));
            }
		}
		
		private function alertRenameSelection(e:CloseEvent):void {
			trace("alertRename: detail=" + e.detail);
			if (e.detail == Alert.SECOND) {
				_oldFilename = null;
                handleOKClick();
			}
		}
		
		private function renameCompletion(completed:Boolean, errorMsg:String):void {
			if (completed) {
				trace("SettingsDialog rename COMPLETED");
				_oldFilename = null;
				handleOKClick();
			} else {
				trace("SettingsDialog rename FAIL");
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, ["Rename failed",errorMsg]));
			}
		}
		
		
		public function checkValidSize(event:Event):void {
			if (Number((event.currentTarget as TextInput).text) > 3600) {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:73}));
				(event.currentTarget as TextInput).setStyle("color", 0xFF0000);
			}
            if (Number((event.currentTarget as TextInput).text) < 50){
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:74}));
				(event.currentTarget as TextInput).setStyle("color", 0xFF0000);
            }
		}
		
		public function updateColor(event:Event):void {
			(event.currentTarget as TextInput).setStyle("color", 0x000000);
		}
		
		public function filenameChanged(e:Event):void
		{
			if (_bubble) {
				Tweener.addTween(_bubble, {alpha:0, time:0.5, transition:"easeOutQuart"});
			}
            if(StringUtils.isNameSafeForServer(this.fileName.text).valid == false){
                this.fileName.setStyle("color", 0xFF0000);
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:999, inserts:["project", StringUtils.isNameSafeForServer(this.fileName.text).reason]}));
                if(okButton){
                    okButton.enabled = false;
                }
                if(btnCreate){
                    btnCreate.enabled = false;
                }
                _validName = false;
            }else{
                this.fileName.setStyle("color", 0x000000);
                if(okButton){
                    okButton.enabled = true;
                }
                if(btnCreate){
                    btnCreate.enabled = true;
                }
                _validName = true;
            }
		}

        protected function get onNextOpenFromProject():String
        {
            try {
                var value:String = World(Application.instance.document.root.value).project.onNextOpen;
                switch(value) {
                    case "menu": return "User Choice *";
                    case "resume": return "Resume *";
                    default: return "Restart";
                }
            } catch(e:Error) {
            }
            return "Restart";
        }

        protected function get isPro():Boolean
        {
			return accountTypeId == 3;
        }

        private function get world():World
        {
            const document:Document = Application.instance.document;
            return document.root.value as World;
        }

//        protected function get onNextOpenToProject():String
//        {
//            var value:String = onNextOpen.selectedValue as String;
//            switch(value) {
//                case "User Choice *": return "menu";
//                case "Resume *": return "resume";
//                default: return "restart";
//            }
//        }
	}
}