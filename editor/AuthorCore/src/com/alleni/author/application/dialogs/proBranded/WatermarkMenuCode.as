package com.alleni.author.application.dialogs.proBranded
{
	import com.alleni.author.view.ui.controls.PlayerControls;

	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	import mx.events.EffectEvent;
	import mx.events.FlexEvent;
	
	import spark.components.Button;
	import spark.components.CheckBox;
	import spark.components.Group;
	import spark.effects.Fade;
	import spark.effects.Move;
	import spark.layouts.HorizontalLayout;
	import spark.primitives.BitmapImage;

	public class WatermarkMenuCode extends Canvas
	{
		public static const WATERMARK_BAR_WIDTH:int = 400;
		public static const WATERMARK_XPOS:int = 100;
		public static const WATERMARK_XPOS_NEXT:int = 320;
		public static const WATERMARK_YPOS:int = 75;
		
		public var btnLogo:BitmapImage;
		public var btnAudio:BitmapImage;
		public var btnEmbed:BitmapImage;
		public var btnShare:BitmapImage;
		public var btnPopup:BitmapImage;
		public var btnScreen:BitmapImage;
		
		public var chkAudio:CheckBox;
		public var chkEmbed:CheckBox;
		public var chkShare:CheckBox;
		public var chkPopup:CheckBox;
		public var chkFullScreen:CheckBox;
		
		public var btnWatermark:Button;
		public var btnPosition:Button;
		
		public var watermarkPosition:String;
		public var watermarkComponent:UIComponent;
		public var alignBar:HorizontalLayout;
		public var menuIcons:Group;
		
		protected var playerControls:PlayerControls;
		protected var fadeIn:Fade;
		protected var fadeOut:Fade;
		protected var moveLeft:Move;
		protected var moveRight:Move;

		public function WatermarkMenuCode()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete, false, 0, true);
		}
		
		protected function onCreationComplete(e:Event):void {
			chkAudio.addEventListener(MouseEvent.CLICK, onClickChkAudio, false, 0, true);
			chkEmbed.addEventListener(MouseEvent.CLICK, onClickChkEmbed, false, 0, true);
			chkShare.addEventListener(MouseEvent.CLICK, onClickChkShare, false, 0, true);
			chkPopup.addEventListener(MouseEvent.CLICK, onClickChkPopup, false, 0, true);
			chkFullScreen.addEventListener(MouseEvent.CLICK, onClickChkFullScreen, false, 0, true);
			btnPosition.addEventListener(MouseEvent.CLICK, onClickPosition, false, 0, true);
		}
		
		protected function onClickChkAudio(e:Event):void {
			btnAudio.visible = !btnAudio.visible;
		}
		
		protected function onClickChkEmbed(e:Event):void {
			btnEmbed.visible = !btnEmbed.visible;	
		}
		
		protected function onClickChkShare(e:Event):void {
			btnShare.visible = !btnShare.visible;	
		}
		
		protected function onClickChkPopup(e:Event):void {
			btnPopup.visible = !btnPopup.visible;	
		}
		
		protected function onClickChkFullScreen(e:Event):void {
			btnScreen.visible = !btnScreen.visible;
		}
		
		protected function onClickIcon(e:Event):void {
			
		}
		
		protected function onClickPosition(e:Event):void {
			if(!fadeOut) {
				fadeOut = new Fade(btnLogo);
				fadeOut.alphaFrom = 1.0;
				fadeOut.alphaTo = 0.0;
				fadeOut.duration = 500;
				fadeOut.addEventListener(EffectEvent.EFFECT_END, onFadeOut, false, 0, true);
			}
			fadeOut.play();
		}
		
		protected function onFadeOut(e:Event):void {
			switch(alignBar.horizontalAlign) {
				case "left":
					if(!moveRight){
						moveRight = new Move(menuIcons);
						moveRight.duration = 1000;
						moveRight.xFrom = WATERMARK_XPOS;
						moveRight.xTo = WATERMARK_XPOS_NEXT;
					}
					moveRight.play();
					alignBar.horizontalAlign = "right";
					break;
				
				case "right":
					if(!moveLeft) {
						moveLeft = new Move(menuIcons);
						moveLeft.duration = 1000;
						moveLeft.xFrom = WATERMARK_XPOS_NEXT;
						moveLeft.xTo = WATERMARK_XPOS;
					}
					alignBar.horizontalAlign = "left";
					moveLeft.play();
					break;
			}
			
			if(!fadeIn) {
				fadeIn = new Fade(btnLogo);
				fadeIn.alphaFrom = 0.0;
				fadeIn.alphaTo = 1.0;
				fadeIn.duration = 1000;
			}
			fadeIn.play();
		}

	}
}