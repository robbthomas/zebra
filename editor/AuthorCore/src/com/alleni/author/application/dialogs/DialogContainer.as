package com.alleni.author.application.dialogs
{
	import com.alleni.author.application.ApplicationUI;
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.view.ui.controls.LoadingSpinner;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.graphics.GradientEntry;
	import mx.graphics.LinearGradient;
	import mx.managers.IFocusManagerComponent;
	
	import spark.components.BorderContainer;
	import flash.display.Sprite;

	public class DialogContainer extends BorderContainer {
		private static var _defaultButton:IFlexDisplayObject;
		private static var _progressSpinnerHolder:UIComponent;
        private var _disableInputToken:Object;
		
		public function DialogContainer() {
			this.minWidth = 100;
			this.minHeight = 80;
			this.styleName = "dialog";
			this.addEventListener(Event.ADDED_TO_STAGE, handleAddedToStage, false, 0, true);

			var fill:LinearGradient = new LinearGradient();
			
			var g1:GradientEntry = new GradientEntry(0xE1E1E1, 0.75, 1);
			var g2:GradientEntry = new GradientEntry(0x9B9B9B, 1, 1);
			
			fill.entries = [ g1, g2];
			fill.rotation = 90;
			this.backgroundFill = fill;
		}
		
		public function set showProgress(value:Boolean):void
		{
			if (_progressSpinnerHolder) {
				while (_progressSpinnerHolder.numChildren > 0)
					_progressSpinnerHolder.removeChildAt(0);
			}
			if (value) {
				if (!_progressSpinnerHolder)
					_progressSpinnerHolder = new UIComponent();
				
				var progressSpinner:LoadingSpinner = new LoadingSpinner(50, true);
				progressSpinner.alpha = 0.5;
				_progressSpinnerHolder.addChild(progressSpinner);
				addElement(_progressSpinnerHolder);

				_progressSpinnerHolder.x = progressSpinnerX - progressSpinner.width / 2;
				_progressSpinnerHolder.y = progressSpinnerY - progressSpinner.height / 2;
			} else {
				if (_progressSpinnerHolder && this.contains(_progressSpinnerHolder)) {
					removeElement(_progressSpinnerHolder);
				}
				_progressSpinnerHolder = null;
			}
		}

		protected function get progressSpinnerX():Number {
			return this.width / 2;
		}

		protected function get progressSpinnerY():Number {
			return this.height / 2;			
		}
		
		override public function set initialized(value:Boolean):void
		{
			super.initialized = value;
			super.defaultButton = _defaultButton;
			if (_defaultButton)
				this.focusManager.setFocus(_defaultButton as IFocusManagerComponent);
			this.filters = [new DropShadowFilter(2, 80, 0, 0.8, 12, 12, 0.7, 3)];
		}
		
		protected function handleAddedToStage(event:Event):void
		{
			this.addEventListener(Event.REMOVED_FROM_STAGE, handleRemovedFromStage);
			this.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			_disableInputToken = ApplicationController.instance.disableInput("DialogContainer");
		}
		
		protected function handleRemovedFromStage(event:Event):void
		{
			showProgress = false;
            ApplicationController.instance.enableInput(_disableInputToken);
			this.removeEventListener(Event.REMOVED_FROM_STAGE, handleRemovedFromStage);
			this.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		}
		
		private function handleMouseDown(event:Event):void
		{
		}
		
		override public function set defaultButton(value:IFlexDisplayObject):void
		{
			super.defaultButton = value;
			_defaultButton = value;
			if (_defaultButton)
				this.focusManager.setFocus(_defaultButton as IFocusManagerComponent);
		}
		
		protected function handleCancelClick():void
		{
			(ApplicationUI.instance as EditorUI).unsetInitialMouseEater();
			ApplicationController.instance.removePopup(this);
		}
	}
}