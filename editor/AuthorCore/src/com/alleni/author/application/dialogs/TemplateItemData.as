package com.alleni.author.application.dialogs {
import com.alleni.author.model.gadgets.GadgetScreenshot;
import com.alleni.author.model.project.Project;

import flash.events.EventDispatcher;

public class TemplateItemData extends EventDispatcher{
    private var _project:Project;

    [Bindable]
    public function get hasDimensions():Boolean{
        var testMe:Boolean = (width != null && height != null && width != String(NaN) && height != String(NaN));
        return testMe;
    }

    public function TemplateItemData(project:Project) {
        _project = project;
    }

    [Bindable]
    public var checked:Boolean;

    [Bindable]
    public function get name():String {
        return _project.publishedName.substr(4);
    }

    [Bindable]
    public function get width():String {
        return String(_project.width);
    }

    [Bindable]
    public function get height():String {
        return String(_project.height);
    }

    [Bindable]
    public function get description():String {
        return _project.description;
    }

    [Bindable]
    public function get screenshotUrl():String {
        if(_project.screenshots) {
            var first:GadgetScreenshot = null;
            for each(var screenshot:GadgetScreenshot in _project.screenshots.screenShots) {
                if(first == null || first.visibleIndex > screenshot.visibleIndex) {
                    first = screenshot;
                }
            }
            if(first) {
                return first.getLargestImageUrl();
            }
        }
        if(_project.icon) {
            return _project.icon.fullURL;
        }
        return "";
    }

    public function get sortField():String {
        return _project.publishedName;
    }

    public function get project():Project {
        return _project;
    }

    public function get accountTypeId():int {
        if(_project.accountTypeId == 3) {
            return 3;
        } else {
            return 2; // unknown assume creator
        }
    }

    public function get projectLevel():String {
        if(_project.accountTypeId == 3) {
            return "Pro Project";
        } else {
            return "Creator Project"; // unknown assume creator
        }
    }
}
}
