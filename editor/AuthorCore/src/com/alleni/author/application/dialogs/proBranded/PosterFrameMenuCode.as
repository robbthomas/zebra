package com.alleni.author.application.dialogs.proBranded
{
	import com.alleni.author.controller.ui.GadgetScreenshotMediator;
	
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import mx.containers.VBox;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class PosterFrameMenuCode extends VBox
	{
		public static const BOX_WIDTH:int = 200;
		public static const BOX_HEIGHT:int = 120;
		public static const IMAGE_WIDTH:int = 100;
		public static const IMAGE_HEIGHT:int = 70;
		public static const PRE_PLAY_LABEL:String = "Start Graphic";
		public static const END_PLAY_LABEL:String = "Quit Graphic";
		
		public function PosterFrameMenuCode()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete, false, 0, true);
		}
		
		protected function onCreationComplete(e:Event):void {
		}
		
		protected var screenshot:GadgetScreenshotMediator;
		
		protected function onDisplayDock(e:Event):void {
			//screenshot = GadgetScreenshotMediator.instance;
			//screenshot.initializeScreenshotPanelFor(null, null, false);
			var dlg:PosterFrameDialog = new PosterFrameDialog();
			dlg.width = stage.width;
			dlg.height = 200;
			dlg.y = stage.height / 2;
			PopUpManager.addPopUp(dlg, this, true);
		}
		
		protected function renderTextLabel	(str:String, 
											uic:UIComponent)
											:void {
			var textFormat:TextFormat = new TextFormat("Myriad Pro", 12, 0, true);
			var textField:TextField = new TextField();
			//textField.embedFonts = true;
			textField.text = str;
			
			textField.x = (IMAGE_WIDTH - textField.textWidth) / 2;
			textField.y = (IMAGE_HEIGHT - textField.textHeight) / 2;
			uic.addChild(textField);
		}
		
		protected function onClickChkReplay(e:Event):void {
			var obj:Object = GadgetScreenshotMediator.instance;
		}
		
		protected function onClickChkPlay(e:Event):void {
			
		}
	}
}