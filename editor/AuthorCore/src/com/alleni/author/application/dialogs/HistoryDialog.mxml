<?xml version="1.0" encoding="utf-8"?>
<dialogs:DialogContainer xmlns:mx="library://ns.adobe.com/flex/mx" xmlns:s="library://ns.adobe.com/flex/spark"
		   xmlns:fx="http://ns.adobe.com/mxml/2009"
		   xmlns:app="com.alleni.author.application.*"
		   width="345" height="340"
		   xmlns:dialogs="com.alleni.author.application.dialogs.*"
		   defaultButton="{okButton}">
	<fx:Script>
		<![CDATA[
        import com.alleni.author.controller.app.ProjectController;
        import com.alleni.author.controller.app.ProjectLoadingSequencer;
        import com.alleni.author.controller.ui.ApplicationController;
        import com.alleni.author.definition.application.NotificationNamesApplication;
        import com.alleni.author.model.ui.Application;
        import com.alleni.author.util.StringUtils;
        import com.alleni.taconite.event.ApplicationEvent;
        import com.alleni.taconite.factory.IEnvironment;
        import com.alleni.taconite.factory.TaconiteFactory;

        import mx.collections.ArrayCollection;

        import spark.components.gridClasses.GridColumn;
        import spark.components.gridClasses.GridSelectionMode;
        import spark.skins.spark.DefaultGridItemRenderer;

        /***
         * History dialog can be used in these situations:
         * 1. open-project History button  (back and cancel both active)
         * 2. autosave-opened-alert in ProjectController  (cancel only)
         * 3. History button on site (back only)
         */
        [Bindable]
        private var _historyCollection:ArrayCollection = new ArrayCollection;	// array of {bullet:String, date:String, time:String}
        private var _historyRawData:Array = [];
        private var _initialLoadCompleted:Boolean;

        private var _environ:IEnvironment = TaconiteFactory.getEnvironmentImplementation();


        private static const SELECTION_COLOR:uint = 0x8FC7FF;
        private static const ROLLOVER_COLOR:uint = 0xDCDCDC;


        override public function set initialized(value:Boolean):void {
            super.initialized = value;
            if (value) {
                focusManager.setFocus(historyList);
                backButton.visible = OpenDialog.nowOpen || Application.instance.showHistory;  // no back button for autosave-alert
                cancelButton.visible = !Application.instance.showHistory;
            }
        }

        private function historyListCreated(e:Event):void {
            trace("historyListCreated");
            trace("cols=" + historyList.columnHeaderGroup);
            focusManager.setFocus(historyList);

            var c1:GridColumn = new GridColumn("bullet");
            c1.width = 20;
            var c2:GridColumn = new GridColumn("date");
            c2.width = 114;
            var c3:GridColumn = new GridColumn("time");
            c3.width = 140;
            historyList.columns = new ArrayCollection([c1, c2, c3]);
            historyList.columnHeaderGroup.height = 0;

            loadHistory();
        }


        private function get selectedVersionID():String {
            var n:int = historyList.selectedIndex;
            if (n < 0) {
                return null;
            } else {
                var item:Object = _historyRawData[n];
                return item.id;
            }
        }

        private function handleDoubleClick(event:MouseEvent):void {
            trace("target=" + event.target);
            if (event.target is Label || event.target is DefaultGridItemRenderer) {   // ensure double-click on scrollbar will not open project
                handleOkClick();
            }
        }

        private function handleOkClick(e:Event = null):void {
            if (selectedVersionID != null) {
                Application.instance.showHistory = false;
                loadVersion(selectedVersionID);
            }
        }

        private function loadVersion(versionID:String):void {
            Application.instance.projectFromOtherUser = OpenDialog.browsingOtherUser;
            ProjectLoadingSequencer.instance.requestLoad(versionID, false, false, true);
            closeMe();
            closeOpenDialog();
        }

        override protected function handleCancelClick():void {
            super.handleCancelClick();
            closeOpenDialog();
        }

        private function handleBackClick(event:Event):void {
            super.handleCancelClick();
            if (Application.instance.showHistory) {
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.REQUEST_QUIT_APPLICATION));
            }
        }

        private function closeMe():void {
            ApplicationController.instance.removePopup(this);
        }

        private function closeOpenDialog():void {
            if (OpenDialog.nowOpen) {
                ApplicationController.instance.removePopup(OpenDialog.nowOpen);
            }
        }

        private function get selectedProjectID():String {
            if (Application.instance.showHistory) {
                return _environ.invokedProjectID;
            } else if (OpenDialog.nowOpen) {
                return OpenDialog.selectedProjectID;
            } else {
                return Application.instance.document.documentId;
            }
        }

        [Bindable]
        private function get selectedProjectName():String {
            var truncatedProjectName:String;

            if (Application.instance.showHistory) {
                truncatedProjectName = _environ.projectName;
            } else if (OpenDialog.nowOpen) {
                truncatedProjectName = OpenDialog.selectedProjectName;
            } else {
                truncatedProjectName = Application.instance.document.name;
            }

            truncatedProjectName = StringUtils.truncate(truncatedProjectName, 190);

            return truncatedProjectName
        }

        private function loadHistory():void {
            var name:String = selectedProjectName;
            trace("loadHistory proj=" + id);
            if (name != null) {
                ProjectController.instance.loadGadgetHistory(name, historyLoadCallback);
            }
            _historyCollection.source = [];
            showProgress = true;
        }

        private function historyLoadCallback(items:Array):void {
            trace("historyCallback: items.len=" + items.length);
            _historyRawData = items;
            var data:Array = [];
            for (var n:int = 0; n < items.length; n++) {
                var item:Object = items[n];
                var autosave:Boolean = ("autosave" in item) && item.autosave;
                var bullet:String = autosave ? "•" : "";
                var dateNum:Number = item.date;
                var date:Date = new Date(dateNum);
                var line:Object = {bullet:bullet, date:formatDate(date), time:formatTime(date)};
                data.push(line);
            }
            _historyCollection.source = data;

            if (Application.instance.document && Application.instance.document.documentId != null) {
                selectHistoryItem(Application.instance.document.documentId);
            }
            showProgress = false;
        }

        private function formatTime(date:Date):String {
            var hour:int = date.hours;
            var part:String = "AM";
            if (hour >= 12) {
                part = "PM";
                if(hour > 12){
                    hour -= 12;
                }
            }
            if(hour == 0){
                hour = 12;
            }
            return twoDigits(hour) + ":" + twoDigits(date.minutes) + " " + part;
        }

        private function formatDate(date:Date):String {
            return twoDigits(date.month + 1) + "-" + twoDigits(date.date) + "-" + date.fullYear.toString();
        }

        private function twoDigits(num:Number):String {
            var str:String = num.toString();
            if (str.length < 2)
                str = "0" + str;
            return str;
        }

        private function selectHistoryItem(docID:String):void {
            for (var n:int = 0; n < _historyRawData.length; n++) {
                var item:Object = _historyRawData[n];
                if (item.id == docID) {
                    historyList.selectionMode = GridSelectionMode.SINGLE_ROW;
                    historyList.selectedIndex = n;
                    break;
                }
            }
        }

        ]]>
		
	</fx:Script>
	<mx:VBox styleName="dialog" width="100%" height="100%" horizontalScrollPolicy="off" verticalScrollPolicy="off" >
		<mx:HBox>
			<s:Label id="openProjectLabel" styleName="dialogLabel" fontSize="14" text="Project History"/>
		</mx:HBox>
        <mx:VBox id="historyProjNameBox" height="26" width="291" borderStyle="inset" backgroundColor="0x636363">
            <s:Label id="historyProjName" paddingLeft="5" paddingTop="4" styleName="dialogLabel" fontSize="14" color="0xffffff" text="Project &quot;{selectedProjectName}&quot;"/>
        </mx:VBox>
        <mx:HBox id="historyHeadingBox" height="20" width="100%" borderStyle="inset">
            <s:Label paddingLeft="26" paddingTop="4" fontSize="12" text="Date saved"/>
            <s:Label paddingLeft="49" paddingTop="4" fontSize="12" text="Time saved"/>
        </mx:HBox>
        <s:DataGrid id="historyList" x="10" width="291"  dataProvider="{_historyCollection}" creationComplete="{historyListCreated(event)}"  horizontalScrollPolicy="off"
                 height="136"
                 fontSize="14" color="0x000000"
                 rollOverColor="{ROLLOVER_COLOR}" selectionColor="{SELECTION_COLOR}" contentBackgroundColor="0xFFFFFF"
                 doubleClickEnabled="true" doubleClick="handleDoubleClick(event)">
        </s:DataGrid>
        <mx:VBox id="historyFooterBox" height="20" width="100%" borderStyle="inset">
            <s:Label paddingLeft="8" paddingTop="3"  fontSize="12" text="•	 auto-saved"/>
        </mx:VBox>
		<mx:HBox styleName="dialogButtons" width="100%">
			<mx:HBox styleName="leftButtons" width="50%" horizontalAlign="left">
				<s:Button id="backButton" styleName="grayButton" label="Back" click="handleBackClick(event)"/>
			</mx:HBox>
			<mx:HBox styleName="rightButtons" width="50%" horizontalAlign="right">
			<s:Button id="cancelButton" label="Cancel" styleName="grayButton" click="handleCancelClick()"/>
			<s:Button id="okButton" label="OK" x="-20" styleName="grayButton" click="handleOkClick(event)"/>
			</mx:HBox>
		</mx:HBox>
	</mx:VBox>
</dialogs:DialogContainer>
