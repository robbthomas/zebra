package com.alleni.author.application.dialogs.proBranded
{
	import com.alleni.author.document.Document;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	
	import flash.events.Event;
	
	import mx.events.FlexEvent;
	import com.alleni.author.application.dialogs.DialogContainer;

	public class ProBrandedDialogCode extends DialogContainer
	{
		public static const DIALOG_WIDTH:int = 600;
		public static const DIALOG_HEIGHT:int = 600;
		
		public var watermarkMenu:WatermarkMenu;
		
		public function ProBrandedDialogCode()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete, false, 0, true);
		}
		
		protected function onCreationComplete(e:Event):void {
			
		}
		
		public function handleOKClick():void {
			const document:Document = Application.instance.document;
			const gadget:Project = document.project;
			if (gadget) {
				gadget.watermarkPosition = watermarkMenu.watermarkPosition;
			}
		}
		
	}
}