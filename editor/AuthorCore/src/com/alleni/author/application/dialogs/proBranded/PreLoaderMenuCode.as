// http://blog.flexexamples.com/2009/07/24/creating-a-simple-text-editor-using-the-spark-textarea-control-in-flex-4/

package com.alleni.author.application.dialogs.proBranded
{	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.ColorPicker;
	import mx.events.ColorPickerEvent;
	import mx.events.FlexEvent;
	
	import spark.components.Button;
	import spark.components.ButtonBar;
	import spark.components.DropDownList;
	import spark.components.TextArea;
	import spark.components.ToggleButton;
	import spark.events.IndexChangeEvent;
	
	public class PreLoaderMenuCode extends VBox
	{
		public var textArea:TextArea;
		public var listFont:DropDownList;
		public var listSize:DropDownList;
		public var btnBold:ToggleButton;
		public var btnItalic:ToggleButton;
		public var btnUnderline:ToggleButton;
		public var btnLine:ToggleButton;
		public var cpColor:ColorPicker;
		public var txtAlignBB:ButtonBar;
		
		public function PreLoaderMenuCode()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete, false, 0, true);
		}
		
		protected function onCreationComplete(e:Event):void {
		}
		
	}
}