/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.application.dialogs
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.view.ui.controls.Alert;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;
	import mx.events.CloseEvent;

	public class GadgetReplaceAllDialog
	{
		// possible results from the replace-all dialogs:
		public static const REPLACE_ALL_BUTTON:String = "replaceAllButton";
		public static const REPLACE_THIS_BUTTON:String = "replaceThisButton";
		public static const DISCARD_CHANGES_BUTTON:String = "discardChangesButton";
		public static const CONTINUE_EDITING_BUTTON:String = "continueEditingButton";
		
		// private buttons within the trio of Replace-all / Cancel-alert / Details-dialog
		private static const CANCEL_BUTTON:String = "cancelButton";
		private static const DETAILS_BUTTON:String = "detailsButton";
		private static const BACK_BUTTON:String = "backButton";  

		
		private static var _replaceAllCompletionCallback:Function;
		private static var _instanceCount:int;
		private static var _detailsData:IList;

		
		/**
		 * Show the gadget replace-all dialog and other related dialogs. 
		 * @param count = number of additional references to the gadget, besides the one being closed.
		 * @param completionCallback = function completion(buttonName:String):void
		 * @param detailsData = IList  ... list of strings for the details page.
		 * 
		 * The user can bounce around between the three dialogs: replace-all, cancel-alert & details.
		 * The calling code will not see that navigation; only the final choice.
		 */
		public static function showReplaceAllDialog(count:int, completionCallback:Function, detailsData:IList):void
		{
			_replaceAllCompletionCallback = completionCallback;
			_instanceCount = count;  // in case BACK button is pressed in Cancel dialog
			_detailsData = detailsData;
			
			// from left to right...
			var flags:uint = Alert.SECOND|Alert.FIRST|Alert.THIRD;
			Alert.cancelLabel = "Cancel";
			Alert.okLabel = "Replace all";
			Alert.yesLabel = "Replace this";

			// similar to ReplacementController
			Alert.buttonWidth = 80;
			var headingMsg:String;
			var promptMsg:String;
			if (count == 1) {
				headingMsg = "Gadget used in one other place:";
				promptMsg = "Would you like to replace it there also?";
			} else {
				headingMsg = "Gadget used in " + count.toString() + " other places:";
				promptMsg = "Would you like to replace it in all those places?";
			}
			Alert.show(promptMsg, headingMsg, flags, replaceAllAlertCloseHandler, null, Alert.FIRST);  // default button = replace-all (OK)
		}
		
		private static function replaceAllAlertCloseHandler(event:CloseEvent):void
		{
			switch (event.detail) {
				case Alert.FIRST:
					showCancelAlert();
					break;
				case Alert.SECOND:
					_replaceAllCompletionCallback(REPLACE_ALL_BUTTON);
					cleanup();
					break;
				case Alert.THIRD:
					_replaceAllCompletionCallback(REPLACE_THIS_BUTTON);
					cleanup();
					break;
			}
		}
		
		private static function showDetails():void
		{
			var dialog:GadgetReplaceDetailsDialog = ApplicationController.instance.createPopUp(GadgetReplaceDetailsDialog, true) as GadgetReplaceDetailsDialog;
			trace("DETAILS DIAL="+dialog);
			dialog.dataProvider = _detailsData;
			dialog.closeCallback = handleDetailsClosed;
		}
		
		private static function handleDetailsClosed():void
		{
			trace("DETAILS CLOSED");
			showReplaceAllDialog(_instanceCount, _replaceAllCompletionCallback, _detailsData);
		}
		
		private static function showCancelAlert():void
		{
			EditorUI.instance.stage.addEventListener(Event.ENTER_FRAME, showCancelAlertAfterWaiting);
		}
		
		private static function showCancelAlertAfterWaiting(e:Event):void
		{
			EditorUI.instance.stage.removeEventListener(Event.ENTER_FRAME, showCancelAlertAfterWaiting);
			
			var flags:uint;
			flags = Alert.FOURTH|Alert.FIRST|Alert.SECOND;
			Alert.buttonWidth = 80;
			
			// left to right order
			Alert.cancelLabel = "Back";
			Alert.okLabel = "Continue editing";
			Alert.noLabel = "Discard changes";
			
			var headingMsg:String;
			var promptMsg:String;
			Alert.show("You can discard changes or continue editing", "Cancel:", flags, cancelAlertCloseHandler, null, Alert.SECOND);  // default button = Cancel
		}
		
		private static function cancelAlertCloseHandler(event:CloseEvent):void
		{
			switch (event.detail) {
				case Alert.FIRST:  // BACK_BUTTON:  return to the replace-all alert
					EditorUI.instance.stage.addEventListener(Event.ENTER_FRAME, showReplaceAllAfterWaiting);
					break;
				case Alert.SECOND:
					_replaceAllCompletionCallback(CONTINUE_EDITING_BUTTON);
					cleanup();
					break;
				case Alert.FOURTH:
					_replaceAllCompletionCallback(DISCARD_CHANGES_BUTTON);
					cleanup();
					break;
			}
		}
		
		private static function showReplaceAllAfterWaiting(e:Event):void
		{
			// back button was pressed, so return to the replace-all alert
			EditorUI.instance.stage.removeEventListener(Event.ENTER_FRAME, showReplaceAllAfterWaiting);
			showReplaceAllDialog(_instanceCount, _replaceAllCompletionCallback, _detailsData);
		}
		
		private static function cleanup():void
		{
			_detailsData = null;
		}
	}
}
