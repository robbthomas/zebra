// This is disposable code.
// 1st Template implementation loads template and saves it into whatever project settings user has defined.
// What should happen in the future is that the cloning and setting occurs on the server side.
// All this should change with Multi-user project.
package com.alleni.author.application.dialogs
{
	import com.alleni.author.controller.AuthorController;
	import com.alleni.author.controller.app.ProjectLoadingSequencer;
	import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.GadgetType;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.GadgetEvent;
    import com.alleni.author.event.ScrollUpdateEvent;
	import com.alleni.author.model.ScrollModel;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.EventFlowController;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.service.project.ProjectListOperation;
	import com.alleni.author.service.users.UserService;
	import com.alleni.author.view.PaletteVerticalScrollView;
	import com.alleni.taconite.event.ApplicationEvent;

	import flash.display.Sprite;

	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
    import mx.collections.ISortField;
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.controls.RadioButtonGroup;
	import mx.core.ClassFactory;
	import mx.core.Container;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.events.ItemClickEvent;
	import mx.events.ResizeEvent;
	import mx.core.IVisualElement;

    import spark.collections.Sort;
    import spark.collections.SortField;
    import spark.components.Button;
	import spark.components.DataGroup;
	import spark.components.Label;
	import spark.components.List;
	import spark.components.RichEditableText;
	import spark.components.TextArea;
	import spark.events.TextOperationEvent;
	import spark.layouts.VerticalLayout;
	import spark.primitives.BitmapImage;

	import flash.geom.Point;
	
	public class TemplateDialogCode extends SettingsDialogCode
	{
		public static const LIST_HEIGHT:int = 417;
		public static const LIST_WIDTH:int = 368;
		public static const PADDING:int = 5;
		public static const SCROLL_PADDING:int = 10;
		public static const TEMPLATE_CONTROLS_WIDTH:Number = 321.0;
		public static const MARGIN:int = 22;
		public static const NOTE_HEIGHT:int = 222;
		public static const NEXT_OPEN_BOX_HEIGHT:int = 75;
		
		public var list:List;
		public var btnCancel:Button;
        public var txtAccountType:Label;
		public var imgChecked:IVisualElement;
        public var imgUnchecked:IVisualElement;
        public var tipBubble:UIComponent;

        public var projectTypeGroup:RadioButtonGroup;
		[Bindable]protected var _data:ArrayCollection;
		protected var _selectedIndex:int = 0;

        public var scrollbarContainer:Box;
        private var _scrollModel:ScrollModel;
		private var _scrollPosition:Number = 0;
		protected var _scrollbar:PaletteVerticalScrollView;

		
		override protected function createChildren():void {
			super.createChildren();
			checkedPosition = false;
		}
		
		override public function set initialized(value:Boolean):void {
			super.initialized = value;

		    if (value) {
				validateVariables();
				
				trace('Init note height', isPro, onNextOpenBox.height, TemplateDialogCode.NOTE_HEIGHT - (isPro?onNextOpenBox.height:0));
				descriptionField.height = TemplateDialogCode.NOTE_HEIGHT - (isPro?NEXT_OPEN_BOX_HEIGHT:0);
				
	            addScrollBarControl();

				this.data = generateDefaultData();
				this.addEventListener(Event.ENTER_FRAME, handleCheckComponents);

		    	addListeners();
			}
		}
		
		override protected function handleAddedToStage(event:Event):void
		{
			super.handleAddedToStage(event);
			if (this.initialized) {
				addListeners();
			}
		}
		
		override protected function handleRemovedFromStage(event:Event):void
		{
			super.handleRemovedFromStage(event);
			removeListeners();
		}

		private function addListeners():void
		{
            UserService.instance.addEventListener(NotificationNamesApplication.UPDATE_CUSTOMER_TYPE, handleCustomerTypeChange);

			onNextOpenBox.addEventListener(ResizeEvent.RESIZE, handleOnNextOpenResize);
			list.addEventListener(MouseEvent.CLICK, onClickListTemplate);
			btnCancel.addEventListener(MouseEvent.CLICK, onClickButtonCancel);
			btnCreate.addEventListener(MouseEvent.CLICK, onClickButtonCreate);
			widthId.addEventListener(TextOperationEvent.CHANGE, onChangeSize);
			heightId.addEventListener(TextOperationEvent.CHANGE, onChangeSize);
			projectTypeGroup.addEventListener(ItemClickEvent.ITEM_CLICK, onChangeProjectLevel);
			widthId.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			heightId.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);

			if (_scrollbar != null) {
				_scrollbar.addEventListener(ScrollUpdateEvent.SCROLL, handleScroll);
			}
		}

		private function removeListeners():void
		{
			UserService.instance.removeEventListener(NotificationNamesApplication.UPDATE_CUSTOMER_TYPE, handleCustomerTypeChange);

			onNextOpenBox.removeEventListener(ResizeEvent.RESIZE, handleOnNextOpenResize);
			list.removeEventListener(MouseEvent.CLICK, onClickListTemplate);
			btnCancel.removeEventListener(MouseEvent.CLICK, onClickButtonCancel);
			btnCreate.removeEventListener(MouseEvent.CLICK, onClickButtonCreate);
			widthId.removeEventListener(TextOperationEvent.CHANGE, onChangeSize);
			heightId.removeEventListener(TextOperationEvent.CHANGE, onChangeSize);
			projectTypeGroup.removeEventListener(ItemClickEvent.ITEM_CLICK, onChangeProjectLevel);
			widthId.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			heightId.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp);

			if (_scrollbar != null) {
				_scrollbar.removeEventListener(ScrollUpdateEvent.SCROLL, handleScroll);
			}
		}

		private function handleCheckComponents(event:Event):void
		{
			// check dimensions. creation is complete, but there are so many components
			// and boxes here that sometimes this dialog is presented while things are
			// still repositioning.
			var listWidth:Number = this.list.width;
			var p:Point = this.globalToLocal(this.list.localToGlobal(new Point(listWidth, 0)));
			if (p.x > listWidth) {
				this.removeEventListener(Event.ENTER_FRAME, handleCheckComponents);
				
				loadTemplateList();
				visible = true;
			}
		}

		private function handleOnNextOpenResize(e:ResizeEvent):void
		{
			trace('Update note height', isPro, onNextOpenBox.height, TemplateDialogCode.NOTE_HEIGHT - (isPro?onNextOpenBox.height:0));
			descriptionField.height = TemplateDialogCode.NOTE_HEIGHT - (isPro?NEXT_OPEN_BOX_HEIGHT:0);
		}

        public function handleCustomerTypeChange(event:Event):void
        {
            if(Application.instance.customerType == "PRO"){
                onNextOpenBox.includeInLayout = true;
                onNextOpenBox.visible = true;
                descriptionField.height = descriptionField.height - onNextOpenBox.height;
            }else{
                onNextOpenBox.includeInLayout = false;
                onNextOpenBox.visible = false;
                descriptionField.height = descriptionField.height + onNextOpenBox.height;
            }
        }

		override public function set showProgress(value:Boolean):void
		{
			super.showProgress = value;
			this.btnCreate.enabled = !value;
		}

		override protected function get progressSpinnerX():Number {
			var p:Point = this.globalToLocal(this.list.localToGlobal(new Point(this.list.width/2, 0)));
			return p.x - 10;
		}

        public function addScrollBarControl():void{
            _scrollModel = new ScrollModel(LIST_HEIGHT, 0, 500);
            _scrollbar = new PaletteVerticalScrollView(_scrollModel, 200, LIST_HEIGHT);
			_scrollbar.scrollPosition = _scrollPosition;
        }

        public function handleScroll(event:ScrollUpdateEvent):void{
            list.dataGroup.verticalScrollPosition = _scrollbar.scrollPosition;
        }

		public function selectListItem(index:int):void {
			list.selectedIndex = index;
			_selectedIndex = index;
		}
		
		override protected function createWatermark():void {
			super.createWatermark();
			
			// draw arrows
			var arrowRect:UIComponent = new UIComponent();
			arrowRect.graphics.beginFill(0x00);
			arrowRect.graphics.moveTo(50, 15);
			arrowRect.graphics.lineTo(60, 10);
			arrowRect.graphics.lineTo(60, 14);
			arrowRect.graphics.lineTo(100, 14);
			arrowRect.graphics.lineTo(100, 16);
			arrowRect.graphics.lineTo(60, 16);
			arrowRect.graphics.lineTo(60, 20);
			arrowRect.graphics.lineTo(50, 15);
			
			arrowRect.graphics.moveTo(250, 15);
			arrowRect.graphics.lineTo(240, 10);
			arrowRect.graphics.lineTo(240, 14);
			arrowRect.graphics.lineTo(200, 14);
			arrowRect.graphics.lineTo(200, 16);
			arrowRect.graphics.lineTo(240, 16);
			arrowRect.graphics.lineTo(240, 20);
			arrowRect.graphics.lineTo(250, 15);
			arrowRect.graphics.endFill();
			arrowRect.x = 16;
			arrowRect.y = 13;
			watermarkComponent.addChild(arrowRect);
		}
		
		override protected function onKeyUp(e:KeyboardEvent):void {
			if(e.keyCode == KEY_CODE_ENTER)
				onClickButtonCreate(null);
		}
		
		public function onChangeSize(e:Event):void {
			// stage size change is an automatically switch to "blank" selection
			selectListItem(0);
		}

        public function onChangeProjectLevel(e:Event):void {
			// project level change is an automatically switch to "blank" selection
			selectListItem(0);
            if(projectTypeGroup.selection.data == 3){
                onNextOpenBox.includeInLayout = true;
                onNextOpenBox.visible = true;
//                if(descriptionField.height != descriptionField.height - onNextOpenBox.height){
//                    descriptionField.height = descriptionField.height - onNextOpenBox.height;
//                }
            }else{
                onNextOpenBox.includeInLayout = false;
                onNextOpenBox.visible = false;
//                if(descriptionField.height != descriptionField.height + onNextOpenBox.height){
//                    descriptionField.height = descriptionField.height + onNextOpenBox.height;
//                }
            }
        }
		
		public function validateVariables():void {
			descriptionField.height = TemplateDialogCode.NOTE_HEIGHT - (isPro?NEXT_OPEN_BOX_HEIGHT:0);
			txtAccountType.text = Application.instance.customerType + " ACCOUNT";
			onNextOpenBox.includeInLayout = isPro;
			onNextOpenBox.visible = isPro;
		}

		
		protected function set checkedPosition(position:Boolean):void {
			imgChecked.visible = position;
			imgChecked.includeInLayout = position;
			imgUnchecked.visible = !position;
			imgUnchecked.includeInLayout = !position;
		}
		
		protected function onClickListTemplate(event:Event):void {
			_selectedIndex = list.selectedIndex;
			
			switch(list.selectedIndex) {
				case -1:		// no selection
					return;
					
				case 0:			// use existing dimensions
					this.projectTypeGroup.enabled = true;
                    this.projectTypeGroup.getRadioButtonAt(1).enabled = Application.instance.customerType.toLowerCase() == UserService.USER_CREATOR?false:true;
                    break;
				default:		// new selection
					this.widthId.text = _data[_selectedIndex].width;
					this.heightId.text = _data[_selectedIndex].height;

                    if(_data[_selectedIndex].width == NaN || _data[_selectedIndex].height == NaN){ // This indicates a new blank project
                        this.projectTypeGroup.enabled = true;
                        this.projectTypeGroup.selection.selected = false;
                        this.projectTypeGroup.getRadioButtonAt(1).enabled = accountTypeId==2?false:true;

                    } else {
                        this.projectTypeGroup.enabled = false;
                        this.projectTypeGroup.selection.selected = false;
                        this.projectTypeGroup.selectedValue = _data[_selectedIndex].accountTypeId == 3?"Professional":"Creator";
                    }

                    accountTypeIdChanged();
			}
		}
		
		protected function onClickButtonCreate(e:Event):void {
			
			switch(_selectedIndex) {
				default:					// validate; load and save template
					if(!validateSettingParams()) 											// correct setting parameters ?
						return;
					
					var prjId:String = _data[list.selectedIndex].project.projectId;					// go load template project
					Application.instance.projectFromOtherUser = true;
					ProjectLoadingSequencer.instance.requestLoad(prjId, false, false, false, generateValueObject());

					ApplicationController.instance.removePopup(this);
					break;
					
				case 0:						// create a standard project the good old way.
					this.handleOKClick();	
					return;
					
				case -1:					// nothing is selected, signal user to choose ?
					return;
			}
			// if it gets here, we are not leaving the dialog.
		}

        override public function handleOKClick():void {
            if (projectTypeGroup.selectedValue == "Professional") {
                AuthorController.instance.closeEditors(null, function():void{   // ensure saveComplete won't take us back into editing Event
                    superHandleOKClick();    // set accountTypeId and save project
                    EventFlowController.instance.afterSwitchToProfessionalProject();
                });
            } else {
                super.handleOKClick();
            }
        }

        private function superHandleOKClick():void
        {
            super.handleOKClick();
        }

		protected function onClickButtonCancel(e:Event):void {
			super.handleCancelClick();
        }

        protected function handleNoteScroll(event:ScrollUpdateEvent):void {
			if (event && event.params && (event.params.hasOwnProperty("scrollPosition"))) {
				var pos:Number = event.params.scrollPosition;  //now displaying infinity
				descriptionField.scroller.verticalScrollBar.value = pos;
			}
		}

		public function set data(list:ArrayCollection):void {
			_data = list;
			this.list.dataProvider = list;
            if(_scrollModel != null){
                this.addEventListener(Event.ENTER_FRAME, adjustScrollBarContentHeight);
            }
            onChangeSize(null);
		}

        public function adjustScrollBarContentHeight(event:Event):void{
            this.removeEventListener(Event.ENTER_FRAME, adjustScrollBarContentHeight);
            _scrollModel.setMinMaxScrollPosition(0, this.list.dataGroup.contentHeight + 80);
            scrollbarContainer.rawChildren.addChild(_scrollbar);
        }

        public function loadTemplateList():void {
            var op:ProjectListOperation = new ProjectListOperation(true, GadgetType.PROJECT);
            op.templates = true;
            op.addEventListener(Event.COMPLETE, handleTemplateListComplete);
        	op.addEventListener(GadgetEvent.ERROR, handleTemplateListError);
            op.execute();

			showProgress = true;
        }

        public function generateDefaultData():ArrayCollection {
            var templates:ArrayCollection = new ArrayCollection();
            var blank:Project = new Project();
            blank.projectTypeId = GadgetType.PROJECT;
            blank.publishedName = "000_" + BLANK_NAME;
            blank.description = DEFAULT_DESCRIPTION;
            blank.accountTypeId = accountTypeId = (Application.instance.customerType.toLowerCase() == UserService.USER_PRO) ? 3 : 2; //Sets both blank template and dialog default.
            templates.addItem(new TemplateItemData(blank));
            return templates;
        }

        protected function accountTypeIdChanged():void{
            accountTypeId = projectTypeGroup.selection["data"];
        }

		// default template
		public static const BLANK_NAME:String = "Blank";
		public static const DEFAULT_NAME:String = "Name";
		public static const ALL_LEVELS:String = "All Project Levels";
		public static const DEFAULT_DESCRIPTION:String = "Create a blank project";

        private function handleTemplateListComplete(event:Event):void {
			showProgress = false;

            var op:ProjectListOperation = event.target as ProjectListOperation;
            op.removeEventListener(Event.COMPLETE, handleTemplateListComplete);
        	op.removeEventListener(GadgetEvent.ERROR, handleTemplateListError);
            var templates:ArrayCollection = generateDefaultData();

            for each(var project:Project in op.projects) {
                if(Application.instance.customerType.toLowerCase() == UserService.USER_CREATOR && project.accountTypeId > 2){
                    //If the user is a creator and the template is a not for creators, skip it and move on.
                    continue;
                }
                templates.addItem(new TemplateItemData(project));
            }

            var sortfieldName:ISortField = new SortField("sortField");
            var sort:Sort = new Sort();
            sort.fields = [sortfieldName];
            templates.sort = sort;
            data = templates;
        }

        private function handleTemplateListError(event:Event):void {
			showProgress = false;

            var op:ProjectListOperation = event.target as ProjectListOperation;
            op.removeEventListener(Event.COMPLETE, handleTemplateListComplete);
        	op.removeEventListener(GadgetEvent.ERROR, handleTemplateListError);
            //TODO notify user of failure to load templates
            data = generateDefaultData();
        }
	}
}
