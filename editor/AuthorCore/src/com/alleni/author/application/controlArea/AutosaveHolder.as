package com.alleni.author.application.controlArea
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	
	
	public class AutosaveHolder extends Canvas
	{
		/*
			Adds rollover graphic to the auto-save label & TextInput objects in topControlArea.
			For the label both rollover & downGraphic are supplied.
			For the minutes field only rolloverGraphic is supplied.
		*/
		public var rolloverGraphic:Class;
		private var _rolloverGraphic:DisplayObject;
		
		public var downGraphic:Class;
		private var _downGraphic:DisplayObject;
		
		private var _background:UIComponent = null;
		
		public var dx:Number;  // offsets from topLeft of container to the desired topLeft of rollover graphic
		public var dy:Number;

		
		public function AutosaveHolder()
		{
			super();
			addEventListener(MouseEvent.ROLL_OVER, rollover);
			addEventListener(MouseEvent.ROLL_OUT, rollout);
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			addEventListener(MouseEvent.MOUSE_UP, mouseUp);
		}
		
		private function get rolloverImage():DisplayObject
		{
			if (_rolloverGraphic == null && rolloverGraphic != null) {
				_rolloverGraphic = new rolloverGraphic() as DisplayObject;
				_rolloverGraphic.x = dx;
				_rolloverGraphic.y = dy;
			}
			return _rolloverGraphic;
		}
		
		private function get downImage():DisplayObject
		{
			if (_downGraphic == null && downGraphic != null) {
				_downGraphic = new downGraphic() as DisplayObject;
				_downGraphic.x = dx;
				_downGraphic.y = dy;
			}
			return _downGraphic;
		}
		
		
		private function rollover(e:Event):void
		{
			if (rolloverImage && _background == null) {  // _background is present during editing: suppress rollover graphic
				rawChildren.addChild(rolloverImage);
			}
		}
		
		private function rollout(e:Event=null):void
		{
			if (rolloverImage && rawChildren.contains(rolloverImage)) {
				rawChildren.removeChild(rolloverImage);
			}
		}
		
		private function mouseDown(event:Event):void
		{
			if (downImage) {
				rawChildren.addChild(downImage);
			}
		}
		
		private function mouseUp(event:Event):void
		{
			if (downImage && rawChildren.contains(downImage)) {
				rawChildren.removeChild(downImage);
			}
		}
		
		public function set fillWhite(value:Boolean):void
		{
			if (value) {
				addBackground();
				rollout();  // starting to edit text, so hide rollover graphic
			} else {
				removeBackground();
			}
		}
		
		private function addBackground():void
		{
			if (_background == null) {
				_background = new UIComponent();
				with (_background.graphics) {
					beginFill(0xffffff);
					drawRoundRect(2,1, width-5, height-3, 8,8);
					endFill();
				}
				addChildAt(_background, 0);
			}
		}
		
		private function removeBackground():void
		{
			if (_background && contains(_background)) {
				removeChild(_background);
				_background = null;
			}
		}
	}
}