package com.alleni.author.application.controlArea {
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.DragDescription;
import com.alleni.author.controller.ui.WireController;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.TaconiteModel;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;

import flash.events.MouseEvent;
import flash.geom.ColorTransform;

import flash.geom.Point;
import flash.geom.Rectangle;

import mx.core.UIComponent;
import mx.events.MoveEvent;

public class MessageCenterProxyButton extends UIComponent {
    private var _messageCenter:MessageCenterView;
    private var bgSprite:Sprite = new Sprite();

    private var blueTint:ColorTransform = new ColorTransform(0.502, 0.639, 0.773, 1);
    private var noTint:ColorTransform = new ColorTransform();

    public function MessageCenterProxyButton() {
        focusEnabled = false;
        buttonMode = true;
        width = MessageCenterView.WIDTH;
        height = 20;
        this.addEventListener(MouseEvent.CLICK, handleClick);
        this.addEventListener(MouseEvent.ROLL_OVER, rollOverHandler);
        this.addEventListener(MouseEvent.ROLL_OUT, rollOutHandler);
        addChild(bgSprite);
        bgSprite.x = 2;
        bgSprite.y = 2;

        scrollRect = new Rectangle(0, -2, MessageCenterView.WIDTH+4, EditorUI.TOP_BAR_HEIGHT-2);
        draw(false, false, false, 20);
        visible = false;
        addEventListener(Event.ENTER_FRAME, handleEnterFrame,  false, 0, true);
    }

    private function handleEnterFrame(event:Event):void {
        if(stage == null) {
            return;
        }
        positionMessageCenter();
    }

    override public function set visible(value:Boolean):void {
        super.visible = value;
        this.includeInLayout = value;
        if(!value && _messageCenter != null) {
            _messageCenter.proxyRolled = false;
        }
    }

    protected function draw(visible:Boolean, selected:Boolean, rolled:Boolean, height:Number):void {
//        trace("Redraw Proxy [" + (_messageCenter?AbstractObject(_messageCenter.model.value).title:null) + "]" + " visible: " + visible + " selected: " + selected + " rolled: " + rolled);
        var width:Number;
        var color:uint;

        var ct:ColorTransform = null;
        var child:DisplayObject = getChildByName("MessageCenterTitleBar");

        if(selected) {
            color = MessageCenterView.SELECTED_BORDER_COLOR;
            width = MessageCenterView.BORDER_WIDTH;
            ct = noTint;
        } else if(visible) {
            color = MessageCenterView.BORDER_COLOR;
            width = MessageCenterView.BORDER_WIDTH;
            ct = noTint;
        } else if(rolled) {
//            color = 0x6B91BA;
            color = 0x80A3C5;
            width = 2;
            ct = blueTint;
        } else {
            color = 0x777777;
            width = 2;
            ct = blueTint;
        }

        if(child) {
            child.transform.colorTransform = ct;
        }
        bgSprite.graphics.clear();
        bgSprite.graphics.lineStyle(width, color, 1, true);
        bgSprite.graphics.beginFill(MessageCenterView.BACKGROUND_COLOR, MessageCenterView.BACKGROUND_ALPHA);
        bgSprite.graphics.drawRoundRect(0, 0, MessageCenterView.WIDTH, height, 10, 10);
    }


    protected function rollOverHandler(event:MouseEvent):void {
        if(_messageCenter == null) {
            return;
        }
        var drag:DragDescription = Application.instance.wireDragDescription;
        if (drag == null)
            drag = Application.instance.assetDragDescription;
        if (drag != null && !_messageCenter.selected && !(_messageCenter.model.value as AbstractObject).wiringLocked && _messageCenter.role==ViewRoles.PRIMARY) {
            var prevModel:TaconiteModel = drag.shownMessageCenter;
            if (prevModel != _messageCenter.model) {
                drag.shownMessageCenter = _messageCenter.model;
            }
        }
        _messageCenter.proxyRolled = true;
        messageCenterRedrawHandler(null);
    }

    protected function rollOutHandler(event:MouseEvent):void {
        if(_messageCenter == null) {
            return;
        }
        _messageCenter.proxyRolled = false;
        messageCenterRedrawHandler(null);
    }

    protected function positionMessageCenter():void {
        if(_messageCenter == null) {
            return;
        }
        var obj:AbstractObject = _messageCenter.model.value as AbstractObject;
        var pos:Point = localToGlobal(new Point(width/2 + 2, +14));
        obj.xMessageCenter = pos.x - MessageCenterView.WIDTH/2;
        obj.yMessageCenter = pos.y;
        ApplicationController.instance.wireController.requestRedrawForObject(obj);
    }

    protected function handleClick(event:MouseEvent):void {
        if(_messageCenter == null) {
            return;
        }
        positionMessageCenter();
        ApplicationController.instance.authorController.selectSingleModel(_messageCenter.model, false);
    }

    public function get messageCenter():MessageCenterView {
        return _messageCenter;
    }

    public function set messageCenter(value:MessageCenterView):void {
        if(_messageCenter) {
            _messageCenter.removeEventListener("MessageCenterRedraw", messageCenterRedrawHandler);
            _messageCenter.proxyRolled = false;
        }
        var child:DisplayObject = getChildByName("MessageCenterTitleBar");
        if(child) {
            removeChild(child);
        }
        _messageCenter = value;
        if(_messageCenter) {
            this.visible = true;
            child = _messageCenter.yieldTitleBar();
            child.y = 4;
            addChild(child);
            _messageCenter.addEventListener("MessageCenterRedraw", messageCenterRedrawHandler);
            _messageCenter.proxyRolled = false;
            positionMessageCenter();
        } else {
            this.visible = false;
        }
    }

    protected function messageCenterRedrawHandler(event:Event):void {
        draw(_messageCenter.visible && _messageCenter.stage != null, _messageCenter.selected, _messageCenter.rolled, _messageCenter.height);
    }
}
}
