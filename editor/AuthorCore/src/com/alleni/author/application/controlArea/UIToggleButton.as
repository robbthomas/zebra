package com.alleni.author.application.controlArea
{	
	import spark.components.ToggleButton;

	import com.alleni.author.application.skins.UIToggleButtonSkin;
	
	public class UIToggleButton extends ToggleButton
	{
		public var disabledSkin:Class;
		public var downSkin:Class;
		public var overSkin:Class;
		public var upSkin:Class;
		public var selectedDisabledSkin:Class;
		public var selectedDownSkin:Class;
		public var selectedOverSkin:Class;
		public var selectedUpSkin:Class;

		public function UIToggleButton() {
			super();
		}

		override public function stylesInitialized():void {
			super.stylesInitialized();
			this.setStyle("skinClass", UIToggleButtonSkin);
		}
	}
}