<?xml version="1.0" encoding="utf-8"?>
<!--
/**
* ApplicationUI.mxml
*
* This is the top level application UI container for Author Core applications.
*
* © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
* 
* @author Allen Skunkworks
*/
-->
<application:ApplicationUI xmlns:mx="library://ns.adobe.com/flex/mx"
		   xmlns:s="library://ns.adobe.com/flex/spark"
		   xmlns:fx="http://ns.adobe.com/mxml/2009"
		   xmlns:application="com.alleni.author.application.*"
		   xmlns:controlArea="com.alleni.author.application.controlArea.*">
	
	<mx:UIComponent id="projectContainer" styleName="blackBackground" width="100%" height="100%" 
					top="{TOP_BAR_HEIGHT}" bottom="{BOTTOM_BAR_HEIGHT}" left="{CONTROL_AREA_WIDTH}" right="{CONTROL_AREA_WIDTH}" />
	
	<controlArea:LeftControlBar id="leftControlBar" top="{TOP_BAR_HEIGHT}" left="0" bottom="0" width="{LEFT_BAR_WIDTH}" />
	<controlArea:RightControlBar id="rightControlBar" top="{TOP_BAR_HEIGHT}" right="0" bottom="0" width="{RIGHT_BAR_WIDTH}" />
	<controlArea:TopControlBar id="topControlBar" top="0" left="0" right="0" height="{TOP_BAR_HEIGHT}" />
	<controlArea:BottomControlBar id="bottomControlBar" bottom="0" left="{CONTROL_AREA_WIDTH}" right="{CONTROL_AREA_WIDTH}" height="{BOTTOM_BAR_HEIGHT}" />

	<mx:UIComponent id="dockContainer" width="100%" height="100%" />
	<mx:Box x="20" y="100" id="floatingPaletteArea"/>
	
	<fx:Script>
		<![CDATA[
        import assets.images.busyPattern;

        import caurina.transitions.Tweener;

        import com.alleni.author.application.palettes.ToolboxPanel;

        import com.alleni.author.controller.ui.ApplicationController;
        import com.alleni.author.controller.ui.AssetController;
        import com.alleni.author.controller.ui.palettes.ToolboxController;
        import com.alleni.author.definition.application.NotificationNamesApplication;
        import com.alleni.author.model.World;
        import com.alleni.author.model.ui.Application;
        import com.alleni.author.view.WorldContainer;
        import com.alleni.author.view.ui.MessageCenterView;
        import com.alleni.author.view.ui.controls.PlayerControls;
        import com.alleni.taconite.dev.Utilities;
        import com.alleni.taconite.event.ApplicationEvent;
        import com.alleni.taconite.model.ModelRoot;

        import flash.filters.BlurFilter;

        import mx.containers.Box;
        import mx.containers.HDividedBox;
        import mx.containers.dividedBoxClasses.BoxDivider;
        import mx.events.DividerEvent;
        import mx.events.ResizeEvent;

        public static const PROJECT_VIEW_PRESENTATION:uint = 1;
        public static const PROJECT_VIEW_DIAGRAM:uint = 2;
        public static const PROJECT_VIEW_SPLIT:uint = 4;

        public static const ANIMATION_DURATION:Number = 0.25;
        public static const ANIMATION_IN_ALGORITHM:String = "easeOutCubic";
        public static const ANIMATION_OUT_ALGORITHM:String = "easeInCubic";

        public static const TOP_BAR_HEIGHT:Number = 30.0;
        public static const BOTTOM_BAR_HEIGHT:Number = 25.0;
        public static const BOTTOM_CONTROLS_WIDTH:Number = 200.0;
        public static const LEFT_PALETTE_WIDTH:Number = 180.0;
        public static const RIGHT_PALETTE_WIDTH:Number = 156.0 + 15;
        public static const CONTROL_AREA_WIDTH:Number = 20.0;
        public static const LEFT_BAR_WIDTH:Number = LEFT_PALETTE_WIDTH + CONTROL_AREA_WIDTH;
        public static const RIGHT_BAR_WIDTH:Number = RIGHT_PALETTE_WIDTH + CONTROL_AREA_WIDTH;

        public var leftBox:Box = new Box();
        public var rightBox:Box = new Box();

        public var preloadMouseEaterToken:Object;

        // Accessor to this singleton object.
        public static var instance:EditorUI = null;

        public var stagingBackground:Sprite;
        public var stageDarkness:Sprite;
        public var busyPattern:Sprite;

        private var _diagramContainer:WorldContainer;
        private var _projectViewMode:uint;
        private var _divided:HDividedBox = null;
        private var _playerControls:PlayerControls = null;

        override protected function childrenCreated():void {
            super.childrenCreated();
            Utilities.assert(instance == null);
            instance = this;
        }

        override public function set initialized(value:Boolean):void {
            ApplicationController.addEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED, runningStateChangeListener);

            stagingBackground = new Sprite();
            var matrix:Matrix = new Matrix();
            matrix.createGradientBox(this.stage.stageWidth, this.stage.stageHeight, Math.PI / 2);

            stagingBackground.graphics.beginGradientFill(GradientType.LINEAR, [0x3D3D3D, 0x666666], [1,1], [0,127], matrix, SpreadMethod.PAD);
            stagingBackground.graphics.drawRect(0, 0, this.stage.stageWidth, this.stage.stageHeight);
            stagingBackground.graphics.endFill();
            stagingBackground.x = -15;
            stagingBackground.y = -15;
            leftBox.rawChildren.addChild(stagingBackground);
            busyPattern = new Sprite();
            busyPattern.visible = false;
            leftBox.rawChildren.addChild(busyPattern);
            rightBox.styleName = "whiteBackground";

            super.initialized = value;
            preloadMouseEaterToken = ApplicationController.instance.disableInput("preloadMouseEaterToken");
        }

        public function unsetInitialMouseEater():void {
            if (preloadMouseEaterToken != null) {
                ApplicationController.instance.enableInput(preloadMouseEaterToken);
                preloadMouseEaterToken = null;
            }
        }

        override protected function handleProjectLoading(event:ApplicationEvent):void {
            if (_presentationContainer && !_presentationContainer.info.testView)
                Tweener.addTween(_presentationContainer, {alpha:0, time:0.5, transition:"easeOutQuart"});
        }

        override protected function handleShowProjectUponInitializationOrFailure(event:ApplicationEvent):void {
            showPresentationContainer();
        }

        override protected function handleProjectComplete(event:ApplicationEvent):void {
            unsetInitialMouseEater();
            ladiesAndGentlemen(Application.uiRunning);
        }

        private function runningStateChangeListener(event:ApplicationEvent):void {
            ladiesAndGentlemen(Application.uiRunning);
        }

        public function ladiesAndGentlemen(running:Boolean):void {
            // "running" is an argument because when tween-back-to-flow, we're still actually running but must hide curtain before tween starts
            var world:World = presentationContainer.worldView.world;

            var w:int = world.width;
            var h:int = world.height;
            var curtain:Shape = presentationContainer.stageCurtain;
            var previewContainer:DisplayObjectContainer;

            if (running && Application.locked) {
                if (!_playerControls) {
                    _playerControls = new PlayerControls();
                    previewContainer = new Sprite();
                    previewContainer.addChild(_playerControls);
                    previewContainer.x = presentationContainer.worldView.x;
                    previewContainer.y = presentationContainer.worldView.y;
                    presentationContainer.addChild(previewContainer);
                }
                curtain.alpha = 0;
                curtain.graphics.clear();
                curtain.graphics.beginFill(0);
                curtain.graphics.drawRect(0, -2000, -3000, 5000);
                curtain.graphics.drawRect(0, h, w, 5000);
                curtain.graphics.drawRect(w, -2000, 3000, 5000);
                curtain.graphics.drawRect(0, 1, w, -3000);

                Tweener.addTween(curtain, {alpha:1.0, time:0.5, transition:ANIMATION_OUT_ALGORITHM,
                    onComplete:function():void {
                        previewContainer = _playerControls.parent;
                        presentationContainer.worldView.scrollRect = previewContainer.scrollRect = new Rectangle(0, 0, world.width, world.height);
                        _playerControls.visible = true;
                    }});
            } else {
                if (_playerControls) {
                    previewContainer = _playerControls.parent;
                    if (presentationContainer.contains(previewContainer))
                        presentationContainer.removeChild(previewContainer);
                    _playerControls = null;
                }
                presentationContainer.worldView.scrollRect = null;

                Tweener.addTween(curtain, {alpha:0, time:0.5, transition:ANIMATION_IN_ALGORITHM,
                    onComplete:function():void {
                        curtain.graphics.clear();
                    }});
            }
        }

        override public function initializeWithModels(world:ModelRoot, wiring:ModelRoot):void {
            setContainers(getFreshPrimaryWorldContainer(world, wiring), getFreshDiagramWorldContainer(world, wiring));
        }

        override protected function handleMeasure(event:ResizeEvent = null):void {
            super.handleMeasure(event);
            refigureWorldSizes();
            leftControlBar.handleMeasure();
            rightControlBar.handleMeasure();
            topControlBar.handleMeasure();
            bottomControlBar.handleMeasure();
            Application.instance.width = this.width;
            Application.instance.height = this.height;
            stagingBackground.width = this.width;
            stagingBackground.height = this.height;
            updateBusyPattern();
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.STAGE_RESIZED));
            if (_playerControls) _playerControls.handleMeasure();
        }

        private function handleMouseDown(event:MouseEvent):void {
            switch (Application.instance.currentTool) {
                case ToolboxController.ZOOM:
                    if (event.shiftKey)
                        ApplicationController.instance.handleZoomOut();
                    else
                        ApplicationController.instance.handleZoomIn();
                    break;
                default:
                    trace("Unhandled mouse down on EditorUI");
                    break;
            }
        }

        private function refigureWorldSizes():void {
            switch (_projectViewMode) {
                case PROJECT_VIEW_PRESENTATION:
                    setSize(leftBox, _presentationContainer, projectContainer.width, projectContainer.height);
                    break;
                case PROJECT_VIEW_DIAGRAM:
                    setSize(rightBox, _diagramContainer, projectContainer.width, projectContainer.height);
                    break;
                case PROJECT_VIEW_SPLIT:
                    var divider:BoxDivider = _divided.getDividerAt(0);
                    _divided.width = projectContainer.width;
                    _divided.height = projectContainer.height;
                    setSize(leftBox, _presentationContainer, divider.x, projectContainer.height);
                    setSize(rightBox, _diagramContainer, _divided.width - divider.width - divider.x, projectContainer.height);
                    break;
                default:
                    //trace("unhandled refigureWorldSizes in EditorUI");
                    break;
            }
        }

        private function setSize(box:Box, container:WorldContainer, width:int, height:int):void {
            box.width = width;
            box.height = height;

            container.reposition();

            box.scrollRect = new Rectangle(0, 0, width, height);
        }

        public function get projectViewMode():uint {
            return _projectViewMode;
        }

        public function set projectViewMode(value:uint):void {
            if (!_divided) {
                _divided = new HDividedBox();
                _divided.liveDragging = true;
                _divided.addEventListener(DividerEvent.DIVIDER_DRAG, function(e:Event):void {
                    invalidateSize();
                });
            }

            if (projectContainer.contains(_divided)) {
                projectContainer.removeChild(_divided);
                _divided.removeChild(leftBox);
                _divided.removeChild(rightBox);
            }
            if (projectContainer.contains(leftBox))
                projectContainer.removeChild(leftBox);
            if (projectContainer.contains(rightBox))
                projectContainer.removeChild(rightBox);

            switch (value) {
                case PROJECT_VIEW_PRESENTATION:
                    projectContainer.addChild(leftBox);
                    break;
                case PROJECT_VIEW_DIAGRAM:
                    projectContainer.addChild(rightBox);
                    break;
                case PROJECT_VIEW_SPLIT:
                    _divided.addChild(leftBox);
                    _divided.addChild(rightBox);
                    projectContainer.addChild(_divided);
                    var divider:BoxDivider = _divided.getDividerAt(0);
                    _divided.width = projectContainer.width;
                    _divided.height = projectContainer.height;
                    divider.x = projectContainer.width / 2;
                    setSize(leftBox, _presentationContainer, divider.x, projectContainer.height);
                    setSize(rightBox, _diagramContainer, _divided.width - divider.width - divider.x, projectContainer.height);
                    break;
            }
            _projectViewMode = value;
            handleMeasure();
            if (value != PROJECT_VIEW_DIAGRAM)
                dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_SETTINGS_CHANGED));
        }

        private function swapContainers():void {
            if (_projectViewMode == PROJECT_VIEW_PRESENTATION)
                projectViewMode = PROJECT_VIEW_DIAGRAM;
            else
                projectViewMode = PROJECT_VIEW_PRESENTATION;
        }

        public function get presentationContainer():WorldContainer {
            return _presentationContainer;
        }

        public function get diagramContainer():WorldContainer {
            return _diagramContainer;
        }

        private function setContainers(presentationContainer:WorldContainer, diagramContainer:WorldContainer):void {
            if (_presentationContainer) {
                _presentationContainer.warnViewsToDelete();
                leftBox.rawChildren.removeChild(_presentationContainer);
                _presentationContainer = null;
            }
            _presentationContainer = presentationContainer;
            leftBox.rawChildren.addChild(_presentationContainer);
            initializeFreshPresentationContainer();

            if (_diagramContainer) {
                _diagramContainer.warnViewsToDelete();
                rightBox.rawChildren.removeChild(_diagramContainer);
            }
            _diagramContainer = diagramContainer;
            rightBox.rawChildren.addChild(_diagramContainer);
            _diagramContainer.initialize();
        }

        public function addFloatingPalette(view:DisplayObject):void {
            floatingPaletteArea.addChild(view);
        }

        public function addToBottomPalette(view:DisplayObject):void {
            bottomControlBar.palette.addChild(view);
        }

        public function removeFromBottomPalette(view:DisplayObject):void {
            if (view != null) {
                if (bottomControlBar.palette.contains(view)) {
                    bottomControlBar.palette.removeChild(view);
                }
            }
        }

        public function addToLeftPalette(view:DisplayObject):void {
            leftControlBar.palette.addChild(view);
        }

        public function addToRightPalette(view:DisplayObject):void {
            rightControlBar.palette.addChild(view);
            view.x = 4; // may not keep this, or will consolidate it into a global style
        }

        public function get paletteHeight():Number {
            return projectContainer.height;
        }

        public function showLeftPalette(show:Boolean):void {
            //hide palette first and then on complete do what show requires -- this supports switching from variable to asset library
            Tweener.addTween(leftControlBar.palette, {x:-165, time:.1,
                transition:ANIMATION_OUT_ALGORITHM,
                onComplete:function():void {
                    Tweener.addTween(leftControlBar.palette, {x:(show ? CONTROL_AREA_WIDTH : -165), time:ANIMATION_DURATION,
                        transition:show ? ANIMATION_IN_ALGORITHM : ANIMATION_OUT_ALGORITHM, onUpdate:(show ? leftPaletteDisplaceToolbox : null),
                        onComplete:function():void {
                            leftControlBar.palette.mouseEnabled = show;
                            leftPaletteDisplaceToolbox();
                        }})
                }});

        }

        public function showRightPalette(show:Boolean):void {
            Tweener.addTween(rightControlBar.palette, {x:(show ? 0 : RIGHT_PALETTE_WIDTH), time:ANIMATION_DURATION,
                transition:show ? ANIMATION_IN_ALGORITHM : ANIMATION_OUT_ALGORITHM, onUpdate:(show ? rightPaletteDisplaceToolbox : null),
                onComplete:function():void {
                    rightControlBar.palette.mouseEnabled = show;
                    rightPaletteDisplaceToolbox();
                }});
        }

        private function leftPaletteDisplaceToolbox():void {
            var fudge:int = 10;
            var leftPaletteX:int = this.globalToLocal(leftControlBar.localToGlobal(new Point(leftControlBar.palette.x, 0))).x;
            if (Application.instance.toolboxX < leftPaletteX + LEFT_PALETTE_WIDTH - fudge) {
                Application.instance.toolboxX = leftPaletteX + LEFT_PALETTE_WIDTH - fudge;
            }
        }

        private function rightPaletteDisplaceToolbox():void {
            var fudge:int = 30;
            var rightPaletteX:int = this.globalToLocal(rightControlBar.localToGlobal(new Point(rightControlBar.palette.x, 0))).x;
            if (Application.instance.toolboxX > rightPaletteX - ToolboxPanel.instance.width - fudge) {
                Application.instance.toolboxX = rightPaletteX - ToolboxPanel.instance.width - fudge;
            }
        }

        public function canvasVisibleRect(container:DisplayObjectContainer):Rectangle {
            var tl:Point = new Point();
            var br:Point = new Point();

            tl.x = leftControlBar.x + leftControlBar.palette.x + LEFT_PALETTE_WIDTH;
            tl.y = topControlBar.y + topControlBar.height;
            br.x = rightControlBar.x + rightControlBar.palette.x;
            br.y = bottomControlBar.y;
            var topLeft:Point = container.globalToLocal(localToGlobal(tl));
            var botRight:Point = container.globalToLocal(localToGlobal(br));

            return new Rectangle(topLeft.x, topLeft.y, botRight.x - topLeft.x, botRight.y - topLeft.y);
        }

        public function messageCenterPlacementArea(container:DisplayObjectContainer):Rectangle {
            var tl:Point = new Point();
            var br:Point = new Point();

            tl.x = leftControlBar.x + CONTROL_AREA_WIDTH + ((leftControlBar.palette.x < 0) ? 0 : LEFT_PALETTE_WIDTH);  // re: tween in showLeftPalette() above
            tl.y = topControlBar.y + topControlBar.height;
            br.x = rightControlBar.x + rightControlBar.palette.x;
            br.y = bottomControlBar.y - MessageCenterView.HEIGHT;
            var topLeft:Point = container.globalToLocal(localToGlobal(tl));
            var botRight:Point = container.globalToLocal(localToGlobal(br));

            return new Rectangle(topLeft.x, topLeft.y, botRight.x - topLeft.x, botRight.y - topLeft.y);
        }

        private function updateBusyPattern():void {
            var fxg:DisplayObject = new assets.images.busyPattern();
            var data:BitmapData = new BitmapData(70, 70, true, 0x000000ff);
            data.draw(fxg);
            var g:Graphics = busyPattern.graphics;
            g.clear();
            g.beginBitmapFill(data, null, true); // repeat=true
            g.drawRect(0, 0, stage.width, stage.height);
            g.endFill();
        }
        ]]>
	</fx:Script>
</application:ApplicationUI>
