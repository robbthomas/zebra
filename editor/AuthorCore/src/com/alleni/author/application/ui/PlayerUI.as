package com.alleni.author.application.ui
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ApplicationUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.World;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ui.controls.PlayerControls;
	import com.alleni.author.view.ui.controls.PlayerProgressBar;
	import com.alleni.author.view.ui.controls.ProjectLoadingScreen;
	import com.alleni.author.view.ui.controls.StatusBar;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.ModelRoot;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	import flash.geom.Rectangle;
	
	import mx.events.ResizeEvent;

	public class PlayerUI extends ApplicationUI
	{
		private static const MINIMUM_WIDTH:Number 	= 350.0;
		private static const MINIMUM_HEIGHT:Number 	= 200.0;
		
		public static var instance:PlayerUI = null;
		
		private static var _statusBar:StatusBar = null;
		private static var _controls:PlayerControls = null;
		private static var _progressBar:PlayerProgressBar = null;
		private static var _loadingScreen:ProjectLoadingScreen = null;
		private static var _statusBarListener:Sprite = null;
		
		public function PlayerUI()
		{
			super();
			Utilities.assert(instance == null);
			instance = this;
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			_controls = new PlayerControls();
			_controls.alpha = 0.8;

			_progressBar = new PlayerProgressBar(Application.instance.progressBar);
		}
		
		override protected function childrenCreated():void
		{
			super.childrenCreated();
			ApplicationController.addEventListener(NotificationNamesApplication.NOTIFY_NO_EMBED_PERMISSION, handleShowProjectUponInitializationOrFailure);
		}
		
		override public function set initialized(value:Boolean):void
		{
			
			// temporary listeners
			/*stage.addEventListener(KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent):void {
				var ch:String = String.fromCharCode(event.charCode);
				
				if (ch == "*") {
					LogPaletteController.instance.initializeLogger();
					LogPaletteController.instance.setVisible(true);
				}
			});*/
			
			// temporary listener to trigger the status bar controls upon full screen
			stage.addEventListener(FullScreenEvent.FULL_SCREEN, handleFullScreen);
			
			/*_statusBarListener = new Sprite();
			_statusBarListener.graphics.beginFill(0x330000, 0.01);
			_statusBarListener.graphics.drawRect(0, 0, this.width, 50);
			_playerControls.addChild(_statusBarListener);
			
			_statusBarListener.addEventListener(MouseEvent.MOUSE_OVER, function():void {
			if (!_statusBar) {
			_statusBar = new StatusBar();
			_statusBar.alpha = 0;
			_playerControls.addChild(_statusBar);
			handleMeasure();
			Tweener.addTween(_statusBar,{alpha:1, time:0.5, transition:"easeOutQuart"});
			_statusBar.addEventListener(MouseEvent.CLICK, function():void {
			toggleFullScreen();
			});
			_statusBar.addEventListener(MouseEvent.ROLL_OUT, function():void {
			if (_statusBar) {
			Tweener.addTween(_statusBar,{alpha:0, time:0.5, transition:"easeOutQuart",
			onComplete:function():void {
			_playerControls.removeChild(_statusBar);
			_statusBar = null;
			}});
			}
			});
			}
			}); */
			
			const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			_loadingScreen = ProjectLoadingScreen.getInstanceWithEventAndInvocation(null, (environment.invokedFile || environment.invokedPurchaseID || environment.invokedProjectID));
			stage.addChild(_loadingScreen);
			stage.addChild(_controls);
			handleMeasure();
			super.initialized = value;
		}
		
		override protected function handleProjectLoading(event:ApplicationEvent):void
		{
			if (!event && event.data == null || !this.initialized) return;
			
			Tweener.addTween(_presentationContainer,{alpha:0, time:0.75, transition:"easeOutQuart",
				onComplete:function():void {
					_presentationContainer.visible = false;
				}});
			_loadingScreen = ProjectLoadingScreen.getInstanceWithEventAndInvocation(event);

			if (!stage.contains(_loadingScreen)) {
				stage.addChild(_loadingScreen);
			}
			handleMeasure();
		}
		
		override protected function handleShowProjectUponInitializationOrFailure(event:ApplicationEvent):void
		{
			switch(event.type) {
				case NotificationNamesApplication.PROJECT_LOAD_FAILED:
				case NotificationNamesApplication.NOTIFY_NO_EMBED_PERMISSION:
					handleProjectComplete(event);
					break;
				default:
					// nothing to do
					break;
			}
		}
		
		override protected function handleProjectComplete(event:ApplicationEvent):void
		{
			if (_loadingScreen) {
				_loadingScreen.visible = false;
				_loadingScreen = null;
			}
			showPresentationContainer();
			
			if (_controls)
				_controls.visible = true;

			stage.addChild(_progressBar);
		}
		
		override protected function showPresentationContainer():void
		{
			super.showPresentationContainer();
			
			if (this.width >= MINIMUM_WIDTH && this.height >= MINIMUM_HEIGHT) {
				if (!_controls) {
					_controls = new PlayerControls();
					stage.addChild(_controls);
				}
			}
			this.addEventListener(Event.ENTER_FRAME, function(event:Event):void {
				event.currentTarget.removeEventListener(Event.ENTER_FRAME, arguments.callee);
				handleMeasure();
			});
		}
		
		override protected function handleFullScreen(event:Event):void
		{
			super.handleFullScreen(event);

			if (_controls != null)
				_controls.fullScreen = isFullScreen;
			handleMeasure();
		}
		
		override protected function handleMeasure(event:ResizeEvent=null):void
		{
			super.handleMeasure(event);
			
			var scale:Number;
			var contentX:Number = 0;
			var contentY:Number = 0;
			var scalingHeight:Number = this.height;
	
			if (_controls) {
				_controls.handleMeasure();
				// uncomment if control bar should have dedicated space
				//scalingHeight -= PlayerControls.CONTROL_BAR_HEIGHT;
			}
			if (_progressBar) {
				_progressBar.handleMeasure();
			}
			
			if (Application.instance.document) {
				var world:World = Application.instance.document.root.value as World;
				if (this.width / scalingHeight > world.width / world.height) {
					scale = scalingHeight / world.height;
					contentX = (this.width - world.width*scale) / 2;
				} else {
					scale = this.width / world.width;
					contentY = (scalingHeight - world.height*scale) / 2;
				}	
				_presentationContainer.scrollRect = new Rectangle(0, 0, world.width, world.height);
				_presentationContainer.scaleX = scale;
				_presentationContainer.scaleY = scale;
				_presentationContainer.x = contentX;
				_presentationContainer.y = contentY;
			}
			
			if (_statusBarListener)
				_statusBarListener.width = this.width;
			if (_statusBar)
				_statusBar.width = this.width;
			if (_loadingScreen) {
				_loadingScreen.width = this.stage.stageWidth;
				_loadingScreen.height = this.stage.stageHeight;
			}
		}
		
		override public function initializeWithModels(world:ModelRoot, wiring:ModelRoot):void
		{
			if (_presentationContainer) {
				_presentationContainer.warnViewsToDelete();
				if (rawChildren.contains(_presentationContainer))
					rawChildren.removeChild(_presentationContainer);
				_presentationContainer = null;
			}
			_presentationContainer = getFreshPrimaryWorldContainer(world, wiring);
			rawChildren.addChild(_presentationContainer);
			initializeFreshPresentationContainer();
		}
	}
}