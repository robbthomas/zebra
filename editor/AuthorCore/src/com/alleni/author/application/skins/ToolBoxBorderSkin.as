package com.alleni.author.application.skins
{
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	
	import mx.core.EdgeMetrics;
	import mx.core.IBorder;
	import mx.skins.ProgrammaticSkin;

	
	public class ToolBoxBorderSkin extends ProgrammaticSkin implements IBorder
	{
		//DropShadowFilter(distance:Number = 4.0, angle:Number = 45, color:uint = 0, alpha:Number = 1.0, blurX:Number = 4.0, blurY:Number = 4.0, strength:Number = 1.0, quality:int = 1, inner:Boolean = false, knockout:Boolean = false, hideObject:Boolean = false)

		private var _dsFilter:DropShadowFilter = new DropShadowFilter(1, 90, 0x000000, 0.75, 6, 6, 0.9, BitmapFilterQuality.HIGH);
		private var _dsFilter1:DropShadowFilter = new DropShadowFilter(1, 0,0x000000,.75,1,1,.5, BitmapFilterQuality.LOW);
		private var _dsFilter2:DropShadowFilter = new DropShadowFilter(1, 90,0x000000,.75,1,1,.5, BitmapFilterQuality.LOW);
		private var _dsFilter3:DropShadowFilter = new DropShadowFilter(1, 180,0x000000,.75,1,1,.5, BitmapFilterQuality.LOW);
		private var _dsFilter4:DropShadowFilter = new DropShadowFilter(1, 270,0x000000,.75,1,1,.5, BitmapFilterQuality.LOW);
		
		override protected function updateDisplayList(w : Number, h : Number) : void
		{
			super.updateDisplayList( w, h );
			
			graphics.clear();
			
			drawRoundRect( 0, 15, w - 2, h - 15, 10, 0xDCDCDC, 1 );
			
			drawRoundRect( 
				borderMetrics.left, borderMetrics.top - 9, 
				w - 2, h - borderMetrics.top - borderMetrics.bottom, 
				10, 0xDCDCDC, 1 );  
			
			drawHeader(graphics, w, h);
			//drawCloseButton(graphics,w, h);
			
			filters = [_dsFilter];
		}
		
		private function drawHeader(graphics:Graphics, w:Number, h:Number):void
		{
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(89, 15, (Math.PI/180)*90, 0, 00);
			graphics.beginGradientFill(GradientType.LINEAR,[0xFFFFFF, 0xDCDCDC, 0x969696],[1,1,1],[0,56,242],matrix);
			graphics.drawRoundRectComplex(0,borderMetrics.top - 15,w-2,15,14,0,0,0);
			graphics.endFill();
		}
		
		private function drawCloseButton(graphics:Graphics, w:Number, h:Number):void
		{
			graphics.beginFill(0x777777);
			graphics.drawCircle(10, borderMetrics.top - 7,3.5);
			graphics.endFill();
		}
		
		public function get borderMetrics() : EdgeMetrics
		{
			//return new EdgeMetrics( 0, 30, 0, 40 );
			//left top right bottom
			return new EdgeMetrics( 0, 15, 0, 10 );
		}
	}
}