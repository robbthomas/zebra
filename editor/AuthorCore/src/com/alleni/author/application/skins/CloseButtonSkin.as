package com.alleni.author.application.skins
{
	import flash.display.CapsStyle;
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	
	import mx.core.EdgeMetrics;
	import mx.core.IBorder;
	import mx.skins.ProgrammaticSkin;
	
	
	public class CloseButtonSkin extends ProgrammaticSkin implements IBorder
	{
		override protected function updateDisplayList(w : Number, h : Number) : void
		{
			super.updateDisplayList( w, h );
			
			graphics.clear();
			
			switch (name) {
				case "closeButtonUpSkin":
					drawUpButton(graphics,0x969696);
					break;
				case "closeButtonOverSkin":
					drawOverButton(graphics,0x646464);
					break;
				case "closeButtonDownSkin":
					drawDownButton(graphics,0xFFFFFF);
					break;
			}
		}
		
		
		private function drawX(graphics:Graphics, color:uint):void{
			with(graphics){
				lineStyle(2,color,1,false,"normal",CapsStyle.SQUARE);
				moveTo(3,3);
				lineTo(8,8);
				moveTo(3,8);
				lineTo(8,3);
				
			}
		}
		
		
		private function drawDownButton(graphics:Graphics, color:uint):void
		{
			with(graphics){
				beginFill(0x646464);
				drawCircle(6,6,6);
				endFill();
			}
			drawX(graphics,color);
		}
		
		private function drawOverButton(graphics:Graphics, color:uint):void
		{
			
			with(graphics){
				beginFill(0x969696,.1);
				drawCircle(6,6,6);
				endFill();
			}
			
			drawX(graphics,color);
		}
		
		private function drawUpButton(graphics:Graphics, color:uint):void
		{
			with(graphics){
				beginFill(0x969696,.1);
				drawCircle(6,6,6);
				endFill();
			}
			
			drawX(graphics,color);
		}
		
		
		
		public function get borderMetrics() : EdgeMetrics
		{
			//left top right bottom
			return new EdgeMetrics( 0, 15, 0, 10 );
		}
	}
}