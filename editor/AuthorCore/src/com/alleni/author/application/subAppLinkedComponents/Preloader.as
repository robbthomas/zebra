package com.alleni.author.application.subAppLinkedComponents
{
	import com.alleni.author.view.ui.controls.LoadingScreen;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	
	import mx.events.FlexEvent;
	import mx.events.RSLEvent;
	import mx.preloaders.IPreloaderDisplay;
	
	public class Preloader extends Sprite implements IPreloaderDisplay
	{	
		private static const MINIMUM_WIDTH_FOR_SPLASH:Number = 500.0;
		private static const MINIMUM_HEIGHT_FOR_SPLASH:Number = 375.0;
		
		// IPreloader properties
		protected var _backgroundAlpha:Number;
		protected var _backgroundColor:uint;
		protected var _backgroundImage:Object;
		protected var _backgroundSize:String;
		protected var _preloader:Sprite; 
		protected var _stageHeight:Number;
		protected var _stageWidth:Number;
		
		// custom preloader thingies
		protected var _rslDownloading:Boolean;
		protected var _rslFraction:Number;
		protected var _swfFraction:Number;
		protected var _rslBytes:Number;
		protected var _rslTotal:Number;
		
		private static var _view:LoadingScreen = null;
		private static var _progressScaleMultiplier:Number = 1.0;
		
		public function Preloader()
		{
			super();
			_view = new LoadingScreen(null, false, LoadingScreen.TYPE_PANEL);
			
			_backgroundAlpha = 1;
			_backgroundColor = 0x272727;
			_backgroundSize  = "";
			_stageWidth      = MINIMUM_WIDTH_FOR_SPLASH;
			_stageHeight     = MINIMUM_HEIGHT_FOR_SPLASH;
			
			_rslDownloading = false;
			_rslFraction    = 1; // in case RSL's are already downloaded
			_swfFraction    = 0;
			_rslBytes       = 0;
			_rslTotal       = 0;
		}
		
		public function get backgroundAlpha():Number
		{
			if (!isNaN(_backgroundAlpha))
				return _backgroundAlpha;
			else
				return 1;
		}
		
		public function set backgroundAlpha(value:Number):void
		{
			_backgroundAlpha = value;
		}
		
		public function get backgroundColor():uint
		{
			return _backgroundColor;
		}
		
		public function set backgroundColor(value:uint):void
		{
			_backgroundColor = value;
		}
		
		public function get backgroundImage():Object
		{
			return _backgroundImage;
		}
		
		public function set backgroundImage(value:Object):void
		{
			_backgroundImage = value;
		}
		
		public function get backgroundSize():String
		{
			return _backgroundSize;
		}
		
		public function set backgroundSize(value:String):void
		{
			_backgroundSize = value;
		}
		
		protected static function set isEditor(value:Boolean):void
		{
			_view.isEditor = value;
			if (!value) _progressScaleMultiplier = 0.3;
		}
		
		public function set preloader(value:Sprite):void
		{
			_preloader = value;
			
			// Loading RSL's into cache
			_preloader.addEventListener(RSLEvent.RSL_PROGRESS, rslProgressHandler);
			_preloader.addEventListener(RSLEvent.RSL_COMPLETE, rslCompleteHandler);
			_preloader.addEventListener(RSLEvent.RSL_ERROR   , rslErrorHandler   );
			
			// Application Loading
			_preloader.addEventListener(ProgressEvent.PROGRESS, progressHandler);    
			_preloader.addEventListener(Event.COMPLETE        , completeHandler);
			
			// Initialization 
			_preloader.addEventListener(FlexEvent.INIT_PROGRESS, initProgressHandler);
			_preloader.addEventListener(FlexEvent.INIT_COMPLETE, initCompleteHandler);
			
			//if (this.stageWidth >= MINIMUM_WIDTH_FOR_SPLASH && this.stageHeight >= MINIMUM_HEIGHT_FOR_SPLASH) {
			isEditor = (!("isEditor" in this.loaderInfo.parameters) || "isEditor" in this.loaderInfo.parameters && this.loaderInfo.parameters["isEditor"] == "true");
			addChild(_view);
		}
		
		public function get stageHeight():Number 
		{
			return _stageHeight;
		}
		
		public function set stageHeight(value:Number):void 
		{
			if( !isNaN(value) && value != _stageHeight )
				_stageHeight = value;
		}
		
		public function get stageWidth():Number 
		{
			return _stageWidth;
		}
		
		public function set stageWidth(value:Number):void 
		{
			if( !isNaN(value) && value != _stageWidth )
				_stageWidth = value;
		}
		
		public function initialize():void
		{
			draw();
		}
		
		protected function draw():void
		{
			_view.progressFraction = _swfFraction * _progressScaleMultiplier;
			
			if (_swfFraction > 0) {
				if (_swfFraction < 1)
					updateStatus("Loading application...");
				/*else
					updateStatus("OPENING APP!");*/
			} else
				updateStatus("Launching...");

			/*trace("_rslDownloading:" + _rslDownloading);
			trace("_rslFraction:" + _rslFraction);
			trace("_swfFraction:" + _swfFraction);
			trace("_rslBytes:" + _rslBytes);
			trace("_rslTotal:" + _rslTotal);*/
		}
		
		private function updateStatus(msg:String):void
		{
			//_view.bodyText = msg;
			_view.overrideDetailText = msg;
		}
		
		protected function rslProgressHandler(event:RSLEvent):void
		{
			_rslDownloading = true;	
			_rslBytes       = event.bytesLoaded;        
			_rslTotal       = event.bytesTotal;
			_rslFraction    = _rslBytes / _rslTotal;
			
			draw();
		}
		
		protected function rslCompleteHandler(event:RSLEvent):void
		{
			_rslFraction = 1;
			draw();
		}
		
		protected function rslErrorHandler(event:RSLEvent):void
		{
			// handle error condition here 
		}
		
		protected function progressHandler(event:ProgressEvent):void
		{
			var swfBytes:Number = event.bytesLoaded;
			var swfTotal:Number = event.bytesTotal;
			
			if (_rslDownloading) {
				swfBytes -= _rslBytes;
				swfTotal -= _rslTotal;
			}
			
			_swfFraction = swfBytes / swfTotal;
			draw();
		}
		
		protected function completeHandler(event:Event):void
		{
			_swfFraction = 1;
			draw();
		}
		
		protected function initProgressHandler(event:FlexEvent):void
		{
			// this is dispatched during application initializion phases such as calls to measure(), commitProperties(), updateDisplayList(), blah, blah
		}
		
		protected function initCompleteHandler(event:FlexEvent):void
		{
			// Loading RSL's into cache
			_preloader.removeEventListener(RSLEvent.RSL_PROGRESS, rslProgressHandler);
			_preloader.removeEventListener(RSLEvent.RSL_COMPLETE, rslCompleteHandler);
			_preloader.removeEventListener(RSLEvent.RSL_ERROR   , rslErrorHandler   );
			
			// Application Loading
			_preloader.removeEventListener(ProgressEvent.PROGRESS, progressHandler);    
			_preloader.removeEventListener(Event.COMPLETE        , completeHandler);
			
			// Initialization 
			_preloader.removeEventListener(FlexEvent.INIT_PROGRESS, initProgressHandler);
			_preloader.removeEventListener(FlexEvent.INIT_COMPLETE, initCompleteHandler);
			
			dispatchEvent(new Event(Event.COMPLETE));
		}
	}
}