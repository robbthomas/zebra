package com.alleni.author.service
{
    import com.alleni.author.model.project.Project;
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.net.URLRequestHeader;
	
	public class CommerceOperation extends AbstractOperation
	{
		private static const PATH:String = "shoppe/purchase";
		
		public function CommerceOperation()
		{
			super();
		}
		
		protected function purchaseGadget(gadget:Project):void
		{
			if (!gadget) return;
			
			var operation:RestHttpOperation = new RestHttpOperation(PATH + "/" + gadget.projectId, null, RestHttpOperation.POST, true);
			operation.customHeaders.push(new URLRequestHeader("command", TaconiteFactory.getEnvironmentImplementation().password));
			operation.displayName = "Purchasing '" + gadget.publishedName + "'";
			operation.addEventListener(OperationFaultEvent.COMMAND_ERROR, handleCommandError);
			BorealOperation.addListeners(operation, handleComplete, handleError);
			operation.execute();
		}
		
		protected function handleCommandError(event:OperationFaultEvent):void
		{	
			// defined by specific needs of subclass operations
		}
	}
}