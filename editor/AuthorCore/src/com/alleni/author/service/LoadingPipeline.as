package com.alleni.author.service
{
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.service.IOperation;
	
	import flash.utils.Dictionary;

	/**
	 * The most basic intent of this class is to allow for operations to be executed in order and to
	 * prevent the system from undertaking too many HTTP operations at once.
	 * 
	 * Although the limit is somewhat arbitrary, and despite the fact that operations in this system
	 * are generally passed onto the browser in order to perform the actual loading, the Flash Player
	 * can only undertake so many operations at once. As a result, it is beneficial to have the option
	 * of passing certain operations which could be numerous, such as for loading thumbnails and assets,
	 * into a queue to be executed in order. This also can have an aesthetic benefit in certain UI 
	 * scenarios.
	 * 
	 * In general, this centralizes scheduling for operation execution and transparently passes common
	 * event handlers onto BorealOperation which has accommodations for various important aspects of
	 * fault tolerance.
	 * 
	 * @author pkrekelberg
	 * 
	 */
	public class LoadingPipeline
	{
		private static var _instance:LoadingPipeline;
		
		protected static const LIMIT:int = 10; // limit the queue to 10 concurrent operations
		protected static const FIRST_TIME_DELAY:int = 3 * 1000; // 3 second delay
		protected static const DELAY:int = 5 * 1000 // 5 second delay
		
		protected static var _pendingLoadQueue:Vector.<IOperation> = new Vector.<IOperation>();
		protected static var _activeLoadQueue:Vector.<IOperation> = new Vector.<IOperation>();
		protected static var _modalOperations:Dictionary = new Dictionary(); // key: IOperation.operationId
		
		public function LoadingPipeline()
		{
			Utilities.assert(!_instance);
		}
		
		private static function get instance():LoadingPipeline
		{
			if (!_instance)
				_instance = new LoadingPipeline();
			return _instance;
		}
		
		/**
		 * 
		 * @param operation the operation to push onto the queue
		 * @param onlyExecuteOneAtATime whether or not this is an operation which makes sense to queue when there is an existing identical pending operation.
		 * if true, when one of these is encountered, it will (and should be) ignored. The purpose of this is to prevent loading up the queue with duplicate
		 * pending non-essential operations.
		 * 
		 */
		protected static function pushOrWait(operation:IOperation, onlyExecuteOneAtATime:Boolean=false):Boolean
		{
			if (onlyExecuteOneAtATime && _modalOperations[operation.operationId])
				return false;
			if (_activeLoadQueue.indexOf(operation) < 0 && _pendingLoadQueue.indexOf(operation) < 0) {
				if (_activeLoadQueue.length == LIMIT)
					_pendingLoadQueue.push(operation);
				else
					pushOntoActiveQueue(operation);
				
				if (onlyExecuteOneAtATime && !_modalOperations[operation.operationId])
					_modalOperations[operation.operationId] = operation;
				return true;
			}
			return false;
		}
		
		protected static function pushOntoActiveQueue(operation:IOperation):void
		{
			_activeLoadQueue.push(operation);
			operation.execute();
		}
		
		protected static function pushNextPending():void
		{
			if (_activeLoadQueue.length < LIMIT && _pendingLoadQueue.length > 0)
				pushOntoActiveQueue(_pendingLoadQueue.shift());
		}
		
		protected static function handleOperationComplete(operation:IOperation):void
		{
			instance.clearAndPushNext(operation);
		}
		
		protected function clearAndPushNext(operation:IOperation):void
		{
			var index:int = _activeLoadQueue.indexOf(operation);
			if (index > -1) {
				_activeLoadQueue.splice(index, 1);
				pushNextPending();
			}
			_modalOperations[operation.operationId] = null;
		}
		
		/**
		 * Give up because we can't try again due to the nature of the error encountered 
		 * @param event
		 * 
		 */
		protected static function handleUnrecoverableError(event:OperationFaultEvent):void
		{
			handleOperationComplete(event.target as IOperation);
		}
	}
}