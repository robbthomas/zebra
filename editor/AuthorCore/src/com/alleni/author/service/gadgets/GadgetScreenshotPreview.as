/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 7/9/11
 * Time: 3:38 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.service.gadgets {
import com.alleni.author.view.text.LightLabel;
import com.alleni.author.view.ui.controls.ScreenshotThumbnailConatiner;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.MouseEvent;

public class GadgetScreenshotPreview extends Sprite {
    public static const GAGDET_SCREENSHOTS:String = "Gadget Screenshots";
    public static const ZAPP_SCREENSHOTS:String = "App Screenshots";

    private const TITLE_BAR_HEIGHT:Number = 30;

    private const CONTAINER_HEIGHT:Number = 450;
    private const CONTAINER_WIDTH:Number = 460;

    private const BOTTOM_BAR_HEIGHT:Number = 40;

    private var _spawner:ScreenshotThumbnailConatiner;

    private var _titleBar:Sprite;
    private var _titleIconType:String;
    private var _titleIcon:Bitmap;
    private var _titleLabel:LightLabel;
    private var _titleString:String;
    private var _titleCloseButton:Sprite;

    private var _mainPanel:Sprite;
    private var _mainPanelScreenshotBitmap:Sprite;

    private var _bottomBar:Sprite;

    public function GadgetScreenshotPreview(type:String,  title:String, bitmap:Sprite, spawner:ScreenshotThumbnailConatiner) {
        _spawner = spawner;

        _titleIconType = type;
        _titleString = title;

        _mainPanelScreenshotBitmap = bitmap;

        _titleBar = buildTitileBar();
        _mainPanel = buildMainPanel();
        _mainPanel.y = TITLE_BAR_HEIGHT;
        if(_mainPanelScreenshotBitmap != null){
            if(_mainPanelScreenshotBitmap.width > 0){
                centerImage();
            }
            _mainPanel.addChild(_mainPanelScreenshotBitmap);
        }
        _bottomBar = buildBottomBar();
        _bottomBar.y = TITLE_BAR_HEIGHT + CONTAINER_HEIGHT;

        this.addChild(_titleBar);
        this.addChild(_mainPanel);
        this.addChild(_bottomBar);
    }

    public function centerImage():void{
        _mainPanelScreenshotBitmap.x = (460 - _mainPanelScreenshotBitmap.width)/2;
        _mainPanelScreenshotBitmap.y = (450 - _mainPanelScreenshotBitmap.height)/2;
    }

    private function buildTitileBar():Sprite{
        var bar:Sprite = new Sprite();
        bar.graphics.beginFill(0x000000);
        bar.graphics.drawRoundRectComplex(0, 0, CONTAINER_WIDTH, TITLE_BAR_HEIGHT, 10, 10, 0, 0);

        bar.graphics.endFill();

        //TODO: Add Icon to Bar

        _titleLabel = new LightLabel();
        _titleLabel.text = _titleString;
        _titleLabel.x = 40;
        _titleLabel.y = 5;
        bar.addChild(_titleLabel);

        var closeButton:Sprite = new Sprite();
        closeButton.graphics.beginFill(0xFFFFFF);
        closeButton.graphics.drawCircle(0,0,8);
        closeButton.graphics.endFill();
        closeButton.y = 15;
        closeButton.x = CONTAINER_WIDTH - 20;
        closeButton.addEventListener(MouseEvent.CLICK, closePreview);
        bar.addChild(closeButton);

        return bar;
    }

    private function buildMainPanel():Sprite{
        var panel:Sprite = new Sprite();
        panel.graphics.beginFill(0x000000, .65);
        panel.graphics.drawRect(0, 0, CONTAINER_WIDTH, CONTAINER_HEIGHT);
        panel.graphics.endFill();

        return panel;
    }

    private function buildBottomBar():Sprite{
        var bar:Sprite = new Sprite();
        bar.graphics.beginFill(0x000000);
        bar.graphics.drawRect(0, 0, CONTAINER_WIDTH, BOTTOM_BAR_HEIGHT);
        bar.graphics.endFill();

        return bar;
    }

    public function closePreview(event:MouseEvent = null):void {
        _spawner.killPreviewPanel();
    }
}
}
