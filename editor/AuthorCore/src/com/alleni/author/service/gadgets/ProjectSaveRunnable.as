/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 7/19/12
 * Time: 9:02 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.service.gadgets {
import com.alleni.author.event.GadgetEvent;
import com.alleni.author.service.project.ProjectSaveOperation;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadLock;
import com.alleni.author.util.thread.PseudoThreadOperationRunnable;
import com.alleni.taconite.dev.Utilities;

import flash.events.Event;

public class ProjectSaveRunnable extends PseudoThreadOperationRunnable {
    private static var _savingQueue:Vector.<ProjectSaveRunnable> = new Vector.<ProjectSaveRunnable>();
    private static var _queueHeldFor:ProjectSaveRunnable = null;
    private static var _asyncLock:PseudoThreadLock = new PseudoThreadLock();

    protected static var count:Number = 0;
    protected var num:Number = count++;

    override public function get name():String {
        return super.name + " (" + num + ")";
    }

    private static function genQueueNames():String {
        var length:int = _savingQueue.length;
        var result:Vector.<String> = new Vector.<String>(length);
        for(var i:int = 0; i < length; i++) {
            result[i] = _savingQueue[i].name;
        }
        return result.join(",");
    }

    public function ProjectSaveRunnable(operation:ProjectSaveOperation, synchronous:Boolean = true) {
        super(operation, synchronous);
    }

    override public function run():void {
        if(_savingQueue.length > 0 && this != _savingQueue[0]) {
            // there are items waiting, and we are not first in line
            var queueNames:String = genQueueNames();
            trace(Utilities.timestamp + " ProjectSaveRunnable::run [", name, "]" + " project name = " + ProjectSaveOperation(_operation).projectName, "pausing for currentlyRunning [", queueNames, "]");
            // items in _savingQueue must be asynchronous if they is still running when our run gets called
            _savingQueue.push(this); // drop it into the queue right away, but dont execute yet
            if(_synchronous) {
                _queueHeldFor = this;
//                _asyncLock.lock("Lock during async [" + queueNames + "] until " + _queueHeldFor.name);
            }
            return;
        }
        trace(Utilities.timestamp + " ProjectSaveRunnable::run Currently running = " + name + " project name = " + ProjectSaveOperation(_operation).projectName);
        if(_savingQueue.length == 0 || this != _savingQueue[0]) {
            // make sure normal runs get into the queue, and make sure we don't re-add the one at the front of the queue
            _savingQueue.unshift(this);
        }
        super.run();
    }

    override protected function handleOperationComplete(event:Event):void {
        trace(Utilities.timestamp + " ProjectSaveRunnable::handleOperationComplete [", name, "] " + " project name = " + ProjectSaveOperation(_operation).projectName);
        _savingQueue.splice(_savingQueue.indexOf(this), 1); // remove this item from the queue
        if(_savingQueue.length > 0) {
            var next:ProjectSaveRunnable = _savingQueue[0];
//            trace("ProjectSaveRunnable::handleOperationComplete resuming [",next.name,"]");
//            trace("ProjectSaveRunnable::handleOperationComplete remaining = [" + genQueueNames() + "]");
            PseudoThread.add(next);
            if(next == _queueHeldFor) {
//                trace("ProjectSaveRunnable::handleOperationComplete unpausing");
                _queueHeldFor = null;
                _asyncLock.unlock();
            }
        }
        super.handleOperationComplete(event);
    }

    override protected function addListeners():void {
        super.addListeners();
        _operation.addEventListener(GadgetEvent.ERROR, handleOperationComplete);
        _operation.addEventListener(GadgetEvent.SAVE_NO_PERMISSION, handleOperationComplete);
        _operation.addEventListener(GadgetEvent.SAVE_FAILED, handleOperationComplete);
        _operation.addEventListener(GadgetEvent.PUBLISHED_NAME_IN_USE, handleOperationComplete);
        _operation.addEventListener(GadgetEvent.REPUBLISH_NOT_ALLOWED, handleOperationComplete);
        _operation.addEventListener(GadgetEvent.PUBLISHED_PRICE_TOO_LOW, handleOperationComplete);
        _operation.addEventListener(GadgetEvent.NAME_IN_USE, handleOperationComplete);
    }
}
}
