package com.alleni.author.service.gadgets
{
	import com.alleni.author.model.gadgets.GadgetState;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.Event;

	public class GadgetStateLoadOperation extends GadgetStateOperation
	{	
		private var _state:GadgetState;
		
		public function GadgetStateLoadOperation(gadgetID:String, externalUserId:String = null)
		{
			super(gadgetID, externalUserId);
		}
		
		override public function execute():void
		{
			super.load();
		}
		
		override protected function handleComplete(event:Event):void
		{
			var restOp:RestHttpOperation;
			
			if (event)
				restOp = event.target as RestHttpOperation;
			
			if (restOp && restOp.resultDecoded) {
                _state = GadgetState.fromJson(restOp.resultDecoded);
            } else {
                LogService.error("Unable to load Gadget State for gadget '" + _gadgetID + "'.");
            }

			super.handleComplete(event);
		}
		
		public function get state():GadgetState
		{
			return _state;
		}
	}
}
