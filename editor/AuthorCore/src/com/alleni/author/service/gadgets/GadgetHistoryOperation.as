package com.alleni.author.service.gadgets
{
	import com.alleni.author.event.GadgetEvent;
	import com.alleni.author.service.BorealOperation;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	
	public class GadgetHistoryOperation extends AbstractOperation
	{
		private var _resultList:Array;
		private var _gadgetName:String;
		private var _userID:String;
		
		public function GadgetHistoryOperation(name:String, userID:String=null)
		{
			_userID = userID;
            _gadgetName = name;
		}
		
		override public function execute():void
		{
			var data:Object = {projectName:_gadgetName, history:true}
            var operation:RestHttpOperation = new RestHttpOperation("project/searchHistory", data, RestHttpOperation.POST, true);
            operation.displayName = "Loading history";
			BorealOperation.addListeners(operation, handleComplete, handleError);
			operation.execute();
		}
		
		override protected function handleComplete(e:Event):void
		{
			_resultList = RestHttpOperation(e.target).resultDecoded.projects as Array;
			
			super.handleComplete(e);
		}
		
		public function get resultList():Array
		{
			return _resultList;
		}
		
		public function get listForUI():Array
		{
			var arrayOfLabels:Array = [];
			
			for each (var object:Object in _resultList) {
				arrayOfLabels.push({date:object.createdDateTime, id:object.projectId, autosave:object.autoSave, publishId:object.projectId});
			}
			arrayOfLabels.sortOn("date", Array.DESCENDING|Array.NUMERIC);
			return arrayOfLabels;
		}
		
		override public function handleError(e:ErrorEvent):void
		{
			LogService.error("GadgetHistoryOperation: " + e.text);
			dispatchEvent(new GadgetEvent(GadgetEvent.ERROR, null));
		}
	}
}