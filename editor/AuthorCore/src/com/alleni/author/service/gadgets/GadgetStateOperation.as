package com.alleni.author.service.gadgets
{
import com.alleni.author.definition.BorealErrorCodes;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.gadgets.GadgetState;
import com.alleni.author.model.gadgets.Gadgets;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.test.TestPanel;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.Assets;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.service.BorealOperation;
import com.alleni.taconite.event.OperationFaultEvent;
import com.alleni.taconite.model.TaconiteObject;
import com.alleni.taconite.service.AbstractOperation;
import com.alleni.taconite.service.LogService;
import com.alleni.taconite.service.RestHttpOperation;

public class GadgetStateOperation extends AbstractOperation
	{
		private static const GADGETSTATE_METHOD:String = "gadgetstate";
		
		protected var _gadgetID:String;
        protected var _externalUserId:String;
		
		public function GadgetStateOperation(gadgetID:String, externalUserId:String = null)
		{
			super();
			_gadgetID = gadgetID;
            _externalUserId = externalUserId;
		}
		
		override public function execute():void
		{
			// overridden by method-specific subclasses
		}
		
		protected function load():void
		{
			var operation:RestHttpOperation = new RestHttpOperation(GADGETSTATE_METHOD + "/gadget/" + _gadgetID, null, RestHttpOperation.GET);
			if(_externalUserId) {
                operation.queryString = {externalUserId:_externalUserId};
            }
            operation.displayName = "Loading state";
			operation.addEventListener(OperationFaultEvent.COMMAND_ERROR, handleCommandError);
			BorealOperation.addListeners(operation, handleComplete, handleError);
			operation.execute();
		}
		
		protected function save(id:String, rootObject:TaconiteObject, gadgets:Gadgets,  assets:Assets, scripts:TestPanel):void
		{
			var abstractObject:AbstractObject = rootObject as AbstractObject;
			if (!abstractObject)
				return;
			
			var path:String = GADGETSTATE_METHOD;


			
			var state:GadgetState = GadgetEncoder.getState(abstractObject, GadgetEncoder.SERIALIZED_LOCATION_EXISTING);
			state.running = Application.running;
            state.locked = Application.locked;
			state.scripts = (scripts == null) ? null : scripts.toJson();
            if(gadgets) {
                var gadgetArr:Array = [];
                for each(var g:Project in gadgets.children) {
                    gadgetArr.push(g);
                }
                state.gadgets = gadgetArr;
            }
            if(assets) {
                var assetArr:Array = [];
                for each(var a:Asset in assets.children) {
                    assetArr.push(a);
                }
                state.assets = assetArr;
            }
			
			var data:Object = {rootGadget:_gadgetID, content:state, authorMode:(_externalUserId==null), externalUserId:_externalUserId};
//			if (Utilities.UUID_REGEX.test(id))
//				data.id = id;
			
			var operation:RestHttpOperation = new RestHttpOperation(path, data, RestHttpOperation.POST);
			operation.displayName = "Saving state";
			BorealOperation.addListeners(operation, handleComplete, handleError);
			operation.execute();
		}

		private function handleCommandError(event:OperationFaultEvent):void
		{
			event.stopImmediatePropagation();
			event.stopPropagation();
			
			var operation:RestHttpOperation = event.target as RestHttpOperation;
			
			if (!operation)
				return;
			
			var error:Object = operation.resultDecoded;
			
			if (BorealErrorCodes.isGadgetStateNotFound(error)) {
				handleComplete(null); // generally, the operation will proceed without a loaded state
				return;
			} else
				LogService.error("Unhandled command error for '"+operation.displayName+"' in FileManagementController.");
			
			BorealOperation.displayDefaultCommandErrorAlert(operation);
		}
	}
}
