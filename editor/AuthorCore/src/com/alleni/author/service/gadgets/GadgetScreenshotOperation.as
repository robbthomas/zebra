package com.alleni.author.service.gadgets
{
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.FileReference;
	
	public class GadgetScreenshotOperation extends GadgetImageOperation
	{
		public var loader:Loader;
		
		public function GadgetScreenshotOperation(file:FileReference, id:String)
		{
			super(file, id);
		}

        override protected function handleFileLoadComplete(event:Event):void
		{
			if (!_file || !_file.data) return;

            loader = new Loader();
            loader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleScreenshotLoadComplete);
            loader.loadBytes(_file.data);
		}

        private function handleScreenshotLoadComplete(event:Event):void
		{
            dispatchEvent(event);
        }
    }
}