package com.alleni.author.service.gadgets
{
	import com.alleni.author.definition.BorealErrorCodes;
	import com.alleni.author.definition.BorealErrorTypes;
	import com.alleni.author.event.GadgetEvent;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.service.BorealOperation;
	import com.alleni.author.service.CommerceOperation;
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;

	public class GadgetCommerceOperation extends CommerceOperation
	{
		private var _gadget:Project;
		
		public function GadgetCommerceOperation(gadget:Project)
		{
			super();
			_gadget = gadget;
		}
		
		override public function execute():void
		{
			purchaseGadget(_gadget);
		}
		
		override protected function handleCommandError(event:OperationFaultEvent):void
		{
			event.stopPropagation();
			event.stopImmediatePropagation();
			
			var error:Object = RestHttpOperation(event.target).resultDecoded;
			
			if (BorealErrorTypes.isBusiness(error)) {
				if (BorealErrorCodes.isPurchaseFailed(error) || BorealErrorCodes.isBadAccount(error)) {
					dispatchEvent(new GadgetEvent(GadgetEvent.PURCHASE_FAILED, _gadget.id));
					return;
				} else
					LogService.error("Unhandled business command error for '" + _gadget.name + "' in GadgetCommerceOperation.");
			} else if (BorealErrorTypes.isInternal(error)) {
				LogService.error("Unhandled internal command error for '" + _gadget.name + "' in GadgetCommerceOperation.");
			} else
				LogService.error("Unhandled command error for '" + _gadget.name + "' in GadgetCommerceOperation.");
			BorealOperation.displayDefaultCommandErrorAlert(RestHttpOperation(event.target));
		}
	}
}