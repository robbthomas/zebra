package com.alleni.author.service.gadgets
{
	import com.alleni.author.event.GadgetEvent;
	import com.alleni.author.service.OperationQueue;
	import com.alleni.taconite.service.IOperation;
	
	public class GadgetSavingQueue extends OperationQueue
	{
		public function GadgetSavingQueue()
		{
			super();
		}
		
		override protected function addListeners(op:IOperation):void
		{
			super.addListeners(op);
			op.addEventListener(GadgetEvent.ERROR, handleError);
			op.addEventListener(GadgetEvent.SAVE_NO_PERMISSION, handleError);
			op.addEventListener(GadgetEvent.SAVE_FAILED, handleError);
			op.addEventListener(GadgetEvent.PUBLISHED_NAME_IN_USE, handleError);
			op.addEventListener(GadgetEvent.REPUBLISH_NOT_ALLOWED, handleError);
			op.addEventListener(GadgetEvent.PUBLISHED_PRICE_TOO_LOW, handleError);
			op.addEventListener(GadgetEvent.NAME_IN_USE, handleError);
		}
	}
}