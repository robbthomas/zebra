package com.alleni.author.service.gadgets
{
    import com.alleni.author.model.gadgets.GadgetScreenshot;
    import com.alleni.author.model.gadgets.GadgetScreenshotReference;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.service.RestPipeline;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import mx.graphics.codec.PNGEncoder;
	
	public class GadgetScreenshotsOperation extends AbstractOperation
	{
	    private var _publishedId:String;
	    private var _gadget:Project;

	    private var _pendingOperations:int;
	    private var _completedOperations:int = 0;
	
	    private var _pendingUploads:Dictionary;
	    private var _pendingDelete:Dictionary;
	
	
	    public function GadgetScreenshotsOperation(pendingDelete:Dictionary, pendingUploads:Dictionary, gadget:Project)
		{
	        _pendingDelete = pendingDelete;
	        _pendingUploads = pendingUploads;
            _gadget = gadget;
	    }
	
	    private function executeUploadScreenshotOperation(bitmapData:BitmapData, displayIndex:int):void
		{
	        var path:String = "/project/" + _publishedId + "/screenshot/" + displayIndex;
	        var data:ByteArray = new PNGEncoder().encode(bitmapData);
	        var operation:RestHttpOperation = new RestHttpOperation(path, data, RestHttpOperation.POST);
			operation.displayName = "Uploading screenshot for published item '" + _publishedId + "' with index: " + displayIndex;
	        RestPipeline.push(operation, handleUploadComplete);
	        _pendingOperations++;
	    }
	
	    private function executeDeleteScreenshotOperation(id:String):void
		{
	        var path:String = "/publishedImage/" + id;
	
	        var operation:RestHttpOperation = new RestHttpOperation(path, null, RestHttpOperation.DELETE);
			operation.displayName = "Deleting screenshot with ID '" + id + "'";
			RestPipeline.push(operation, handleComplete);
	        _pendingOperations++;
	    }
	
	    override public function execute():void
		{
			var screenshot:GadgetScreenshot;
	        for each(screenshot in _pendingDelete){
	            executeDeleteScreenshotOperation(screenshot.screenshotDBID);
	        }
	        for each(screenshot in _pendingUploads){
	            executeUploadScreenshotOperation(screenshot.imageBitmapData, screenshot.visibleIndex);
	        }
	    }
	
	    public function set publishedId(value:String):void
		{
	        _publishedId = value;
	    }

        private function handleUploadComplete(e:Event):void {
            //make screenshot here
            var index:int = e.currentTarget.resultDecoded.displayOrder;
            var screenshot:GadgetScreenshot = GadgetScreenshot.fromJson(e.currentTarget.resultDecoded, _pendingUploads[index]);
            _pendingUploads[index] = screenshot;
            handleComplete(e);
         }
	
	    override protected function handleComplete(e:Event):void
		{
	        _completedOperations++;
	        if(_completedOperations == _pendingOperations){
                if( _gadget.screenshots == null){
                    _gadget.screenshots = new GadgetScreenshotReference();
                }
                var screenshot:GadgetScreenshot;
                for each(screenshot in _pendingDelete){
                    delete _gadget.screenshots.screenShots[screenshot.visibleIndex];
                }
                for each(screenshot in _pendingUploads){
                    _gadget.screenshots.screenShots[screenshot.visibleIndex] = screenshot;
                }
	            dispatchEvent(new Event(Event.COMPLETE));
	        }
	    }
	
	    public function get pendingUploads():Dictionary
		{
	        return _pendingUploads;
	    }
	
	    public function get pendingDelete():Dictionary
		{
	        return _pendingDelete;
	    }
	}
}
