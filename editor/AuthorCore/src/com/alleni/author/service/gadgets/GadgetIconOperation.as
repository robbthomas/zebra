package com.alleni.author.service.gadgets
{
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.model.gadgets.GadgetIconReference;
import com.alleni.author.model.project.Project;
import com.alleni.taconite.service.RestHttpOperation;

import flash.events.Event;
import flash.net.FileReference;
	
	public class GadgetIconOperation extends GadgetImageOperation
	{
        public function GadgetIconOperation(file:FileReference, id:String)
		{
            super(file, id);
        }

        override protected function handleLoadIconDataComplete(e:Event):void{
            var iconInformation:Object = (e.target as RestHttpOperation).resultDecoded;
            if("iconMap" in iconInformation){
                var gadget:Project = GadgetController.instance.model.getById(iconInformation.projectId)
                if(gadget != null){
                    gadget.icon = GadgetIconReference.fromJson(iconInformation.iconMap);
                }
            }
        }
    }
}