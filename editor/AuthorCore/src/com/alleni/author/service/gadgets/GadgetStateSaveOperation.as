package com.alleni.author.service.gadgets
{
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.event.GadgetEvent;
import com.alleni.author.model.gadgets.Gadgets;
import com.alleni.author.model.test.TestPanel;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Assets;
import com.alleni.author.view.ui.controls.Alert;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.model.TaconiteObject;
import com.alleni.taconite.service.LogService;
import com.alleni.taconite.service.RestHttpOperation;

import flash.events.ErrorEvent;

import flash.events.Event;

public class GadgetStateSaveOperation extends GadgetStateOperation
	{
		private var _id:String;
		private var _rootObject:TaconiteObject;
		private var _scripts:TestPanel;
        private var _gadgets:Gadgets;
        private var _assets:Assets;

        private var _stateId:String;
		
		public function GadgetStateSaveOperation(gadgetID:String, externalUserId:String, id:String, rootObject:TaconiteObject, gadgets:Gadgets,  assets:Assets, scripts:TestPanel=null)
		{
			super(gadgetID, externalUserId);
			_id = id;
			_rootObject = rootObject;
			_scripts = scripts;
            _gadgets = gadgets;
            _assets = assets;
		}
		
		override public function execute():void
		{
			super.save(_id, _rootObject, _gadgets, _assets,  _scripts);
		}


        override protected function handleComplete(e:Event):void
        {
            trace("SAVE STATE COMPLETE");
            if(e.target is RestHttpOperation) {
                var result:Object = RestHttpOperation(e.target).resultDecoded;
                if(result && "id" in result) {
                    _stateId = result.id;
                }
            }
            super.handleComplete(e);
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_STATE_SAVED));
        }

        override public function handleError(e:ErrorEvent):void
        {
            trace("SAVE STATE ERROR");
            LogService.error("GadgetStateOperation: " + e.text);
            dispatchEvent(new GadgetEvent(GadgetEvent.STATE_ERROR, _gadgetID));
            if (TaconiteFactory.getEnvironmentImplementation().isAdmin) {
                Alert.show("Error saving project STATE.  Error="+e.text, "Error saving", Alert.FIRST);
            }
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_STATE_SAVED, e.text));
        }

        public function get stateId():String {
            return _stateId;
        }
}
}
