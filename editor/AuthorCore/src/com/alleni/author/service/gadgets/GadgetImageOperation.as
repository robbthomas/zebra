package com.alleni.author.service.gadgets
{
	import com.alleni.author.event.GadgetEvent;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.service.RestPipeline;
    import com.alleni.author.service.project.ProjectLoadOperation;
    import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	public class GadgetImageOperation extends AbstractOperation
	{
		private static const PATH:String = "icon";

		public var id:String;
		protected var _file:FileReference;
		protected var _gadget:Project;
		protected var _publishedImageId:String;

		public function GadgetImageOperation(file:FileReference, id:String)
		{
			super();
			_file = file;
			this.id = id;
		}

		public function get gadget():Project
		{
			return _gadget;
		}

		public function get data():Vector.<ByteArray>
		{
			if (!_file.data) return null;

			var dataList:Vector.<ByteArray> = new Vector.<ByteArray>();
			dataList.push(_file.data)
			return dataList;
		}

		override public function execute():void
		{
			if (!_file) return;

			if (_file.data) {
				handleFileLoadComplete(null);
				return;
			}

			_file.addEventListener(ProgressEvent.PROGRESS, handleFileProgress, false, 0, true);
			_file.addEventListener(IOErrorEvent.IO_ERROR, handleFileIOError, false, 0, true);
			_file.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleFileSecurityError, false, 0, true);
			_file.addEventListener(Event.COMPLETE, handleFileLoadComplete, false, 0, true);
			_file.load();
		}

		protected function handleFileProgress(event:ProgressEvent):void
		{
		}

		protected function handleFileIOError(event:IOErrorEvent):void
		{
			var file:FileReference = FileReference(event.target);
			LogService.error("IO error for file " + file.name + ": " + event.text);
		}

		protected function handleFileSecurityError(event:SecurityErrorEvent):void
		{
			var file:FileReference = FileReference(event.target);
			LogService.error("Security error for file " + file.name + ": " + event.text);
		}

		protected function get path():String
		{
			return "project/" + this.id + "/" + PATH; // /zephyr/shoppe/{publishId}/icon
		}

		protected function handleFileLoadComplete(event:Event):void
		{
			if (!_file || !_file.data) return;

			if (event)
				handleUpdate(null);

			if (!this.id)
				return;

			var operation:RestHttpOperation = new RestHttpOperation(path, _file.data, RestHttpOperation.POST);
			operation.displayName = "Uploading icon for published item '" + this.id + "'";;
			RestPipeline.push(operation, handleUploadComplete);
		}

		protected function handleUploadComplete(e:Event):void
		{
			var publishedImageId:String;
			var uploadOperation:RestHttpOperation = e.target as RestHttpOperation;
			if (uploadOperation && "publishedImageId" in uploadOperation.resultDecoded) _publishedImageId = uploadOperation.resultDecoded.publishedImageId;
			
			checkMetadataToSeeIfRenderingIsComplete(null);
		}
		
		protected function checkMetadataToSeeIfRenderingIsComplete(e:Event):void
		{
			if (!_publishedImageId) {
				LogService.error("GadgetImageOperation::checkMetadataToSeeIfRenderingIsComplete encountered an issue with ID: " + _publishedImageId);
				return;
			}
			var operation:RestHttpOperation = new RestHttpOperation("publishedImage/" + _publishedImageId, null, RestHttpOperation.GET);
			operation.displayName = "Request rendering status for published image '"+_publishedImageId+"'";
			//	operation.addEventListener(OperationFaultEvent.COMMAND_ERROR, handleMetadataComplete); // for debugging
			RestPipeline.push(operation, handleMetadataComplete);
		}
		
		protected function handleMetadataComplete(e:Event):void
		{
			var renderingComplete:Boolean = true;
			
			var metadataOperation:RestHttpOperation = e.target as RestHttpOperation;
			if (metadataOperation && "renderingComplete" in metadataOperation.resultDecoded) 
				renderingComplete = metadataOperation.resultDecoded.renderingComplete as Boolean;
			
			if (_publishedImageId) {
				if (!renderingComplete) { // we have a valid published image but rendering is not yet complete
					var timer:Timer = new Timer(1000, 1); // check after a second
					timer.addEventListener(TimerEvent.TIMER, checkMetadataToSeeIfRenderingIsComplete);
					timer.start();
				} else { // rendering is complete. we can load the full item metadata and report completion once we receive it
					handleLoadIconDataComplete(e)
                    var operation:ProjectLoadOperation = new ProjectLoadOperation(this.id);
					operation.addEventListener(GadgetEvent.ERROR, handleLoadError);
					operation.addEventListener(Event.COMPLETE, handleLoadComplete);
					operation.execute();
				}
			} else
				LogService.error("GadgetImageOperation::handleMetadataComplete encountered an issue with operation: " + metadataOperation + " and ID: " + _publishedImageId);
		}
		
		protected function handleLoadComplete(e:Event):void
		{
            var operation:ProjectLoadOperation = e.target as ProjectLoadOperation;
			_gadget = operation.project;
			handleComplete(null);
		}

        protected function handleLoadIconDataComplete(e:Event):void{

        }

		protected function handleLoadError(event:Event):void
		{
			dispatchEvent(event);
		}
	}
}