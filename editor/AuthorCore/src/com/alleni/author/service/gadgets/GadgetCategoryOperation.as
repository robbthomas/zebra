package com.alleni.author.service.gadgets
{
	import com.alleni.author.service.RestPipeline;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.Event;
	
	public class GadgetCategoryOperation extends AbstractOperation
	{
		private var _categories:Array;
		
		public function GadgetCategoryOperation()
		{
			super();
		}
		
		override public function execute():void
		{
			var operation:RestHttpOperation = new RestHttpOperation("project/categories", null, RestHttpOperation.GET, true);
			operation.displayName = "Loading category list";
			RestPipeline.push(operation, handleComplete, handleError);
		}
		
		override protected function handleComplete(e:Event):void
		{
			_categories = RestHttpOperation(e.target).resultDecoded as Array;
			if (!_categories) return;
			
			var i:int;
			var length:int = _categories.length;
			for (i = 0; i < length; i++)
				_categories[i].label = _categories[i].name;			
			_categories.sortOn("label", Array.CASEINSENSITIVE);
			
			super.handleComplete(e);
		}
		
		public function get categories():Array
		{
			return _categories;
		}
	}
}