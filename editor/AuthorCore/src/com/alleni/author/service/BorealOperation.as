package com.alleni.author.service
{
	import com.alleni.author.controller.app.LoginMediator;
import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.BorealErrorCodes;
	import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.service.EchoService;
import com.alleni.author.view.ui.WarningWindow;
import com.alleni.author.view.ui.controls.Alert;
import com.alleni.taconite.controller.EventController.LEC;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.service.FilterOperation;
	import com.alleni.taconite.service.IOperation;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

    import mx.events.CloseEvent;

public class BorealOperation
	{
		private static const MAX_FAULTS_BEFORE_WARNING:int = 2;
		private static const MAX_OPERATION_ATTEMPTS:int = 3;
		private static const OPERATION_REATTEMPT_DELAY:int = 5 * 1000; // 5 seconds
		
		private static var _silentFaultCount:int = 0;
		private static var _lec:LEC;

        public static var showNetworkBadMessage:Boolean = true;
        public static var suppressAlerts:Boolean = false;

		protected static function get lec():LEC
		{
			if (!_lec)
				_lec = new LEC(); // local event controller to ensure operations are always cleaned up following complete or unrecoverable error events.
			return _lec;
		}
		
		public static function addListeners(operation:IOperation, completeHandler:Function, faultPrehandler:Function=null):void
		{
			lec.add(operation, Event.COMPLETE, function(event:Event):void {
				removeListeners(operation);
				_silentFaultCount = 0;
				completeHandler(event);
			});
			lec.add(operation, OperationFaultEvent.REDIRECT, function(event:OperationFaultEvent):void {
				clearIfNotReattempting(event, operation, faultPrehandler);
				handleRedirect(event);
			});
			lec.add(operation, OperationFaultEvent.COMMAND_ERROR, function(event:OperationFaultEvent):void {
				clearIfNotReattempting(event, operation, faultPrehandler);
				handleCommandError(event);
			});
			lec.add(operation, OperationFaultEvent.UNAUTHORIZED, function(event:OperationFaultEvent):void {
				clearIfNotReattempting(event, operation, faultPrehandler);
				handleUnauthorized(event);
			});
			lec.add(operation, OperationFaultEvent.NOT_FOUND, function(event:OperationFaultEvent):void {
				clearIfNotReattempting(event, operation, faultPrehandler);
				handleNotFound(event);
			});
			lec.add(operation, OperationFaultEvent.INTERNAL_SERVER_ERROR, function(event:OperationFaultEvent):void {
				clearIfNotReattempting(event, operation, faultPrehandler);
				handleInternalServerError(event);
			});
			lec.add(operation, OperationFaultEvent.SERVICE_UNAVAILABLE, function(event:OperationFaultEvent):void {
				clearIfNotReattempting(event, operation, faultPrehandler);
				handleServiceUnavailable(event);
			});
			lec.add(operation, OperationFaultEvent.FAULT, function(event:OperationFaultEvent):void {
				clearIfNotReattempting(event, operation, faultPrehandler);
				handleGeneralFault(event);
			});
		}
		
		public static function removeListeners(operation:IOperation):void
		{
			lec.removeObjEvents(operation);
		}
		
		private static function clearIfNotReattempting(event:Event, operation:IOperation, faultPrehandler:Function):void
		{
			if (!isWorthReattempting(operation)) {
				if (faultPrehandler != null)
					faultPrehandler(event);
				removeListeners(operation);
			}
		}
		
		private static function resetApplicationState():void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_APPLICATION_STATE));
		}

        public static function handleSendEmail(restOp:RestHttpOperation):Function{
            return function(event:CloseEvent):void{
    			switch (event.detail) {
                    case Alert.SECOND:
		    			LogService.instance.sendAsEmail("Error "+BorealErrorCodes.getCode(restOp.resultDecoded));
			    		break;
                    case Alert.FIRST:
		    			// cancel is the default
	    			default:
	    				// no action
	    				break;
                }
			}
		}

		public static function displayDefaultCommandErrorAlert(restOp:RestHttpOperation):void
		{
			if(restOp.path == 'echo') {
                LogService.error("Echo service failed");
            } else {
                if (restOp.resultDecoded != null){
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:101, closeFunction:handleSendEmail(restOp), inserts:[restOp.displayName]}));
                }else{
                    LogService.error("'" + restOp.displayName + "' encountered an unknown command error with no response content.");
                }
            }
            resetApplicationState();
		}
		
		public static function displayDefaultInternalServerErrorAlert(restOp:RestHttpOperation):void
		{
			if(restOp.path == 'echo') {
                LogService.error("Echo service failed");
            } else {
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:411, closeFunction:handleSendEmail(restOp), inserts:[restOp.displayName]}));
            }
			resetApplicationState();
		}

		private static function handleGeneralFault(event:OperationFaultEvent):void
		{
			var restOp:RestHttpOperation = restOperationForError(event);

            if (suppressAlerts) {
                trace("BorealOperation: network err but alerts are suppressed:", event.error);
                return;
            }

			if (!restOp || !reattempt(restOp, event)) {
				if (_silentFaultCount < MAX_FAULTS_BEFORE_WARNING && restOp.attempts < MAX_OPERATION_ATTEMPTS) {
					// don't worry the user just yet.
					LogService.error(_silentFaultCount + ": Fault detected, but waiting before warning user.");
					_silentFaultCount++;
				} else {
					LogService.error(_silentFaultCount + ": Loss of network detected.");
					if(showNetworkBadMessage){
                        if (/^Error #2031/.exec(OperationFaultEvent(event).error.text)){
                            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:26}));
                        }else if (/^Error #2044/.exec(OperationFaultEvent(event).error.text)
                            || /^Error #2048/.exec(OperationFaultEvent(event).error.text)){
                            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:27}));
                        }else {
                            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:26}));
                            LogService.error("Error for '" + restOp.displayName + "': " + OperationFaultEvent(event).error.text
                                + ". This is an error which has not yet been classified with type '"+OperationFaultEvent(event).error.type+"'.");
                        }
                    }
                    showNetworkBadMessage = false;
                    //EchoService.connectionNotAvailable();
					resetApplicationState();
				}
			}
		}
		
		private static function handleRedirect(event:OperationFaultEvent):void
		{
			var restOp:RestHttpOperation = restOperationForError(event);

            if (!restOp || !reattempt(restOp, event)) {
                if(restOp.path == 'echo') {
                    LogService.error("Echo service failed");
                } else {
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:101, closeFunction:handleSendEmail(restOp), inserts:[restOp.displayName]}));
                }
                resetApplicationState();
            }
		}
		
		private static function handleCommandError(event:OperationFaultEvent):void
		{
			var restOp:RestHttpOperation = restOperationForError(event);
			
			if (!restOp)
				return;
			displayDefaultCommandErrorAlert(restOp);
		}
		
		private static function handleUnauthorized(event:OperationFaultEvent):void
		{
			// display login dialog, passing along the operation attempted for another try.
			new LoginMediator().handleLoginEvents(event.target as IOperation);
		}
		
		private static function handleNotFound(event:OperationFaultEvent):void
		{
			var restOp:RestHttpOperation = restOperationForError(event);

			if (!restOp || !reattempt(restOp, event)) {
                if(restOp.path == 'echo') {
                    LogService.error("Echo service failed");
                } else {
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:101, closeFunction:handleSendEmail(restOp), inserts:[restOp.displayName]}));
                }
                resetApplicationState();
            }
		}
		
		private static function handleInternalServerError(event:OperationFaultEvent):void
		{
			var restOp:RestHttpOperation = restOperationForError(event);
			if (!restOp)
				return;
			displayDefaultInternalServerErrorAlert(restOp);
		}
		
		private static function handleServiceUnavailable(event:OperationFaultEvent):void
		{
			var restOp:RestHttpOperation = restOperationForError(event);
			
			if (!restOp || !reattempt(restOp, event)) {
                if(restOp.path == 'echo') {
                    LogService.error("Echo service failed");
                } else {
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:101, closeFunction:handleSendEmail(restOp), inserts:[restOp.displayName]}));
                }
                resetApplicationState();
            }
		}

        private function handleSendEmail(event:CloseEvent):void{
            //SEND EMAIL HERE
        }

		private static function reattempt(operation:RestHttpOperation, event:OperationFaultEvent):Boolean
		{
			if (!isWorthReattempting(operation))
				return false; // we have already tried this
			
			var reattemptTimer:Timer = new Timer(OPERATION_REATTEMPT_DELAY, 1);
			reattemptTimer.addEventListener(TimerEvent.TIMER, function():void {
				LogService.debug("Reattempting '"+operation.displayName+"' which encountered error condition '"+OperationFaultEvent(event).error.text+"' on attempt: " + operation.attempts);
				operation.execute();
			});
			reattemptTimer.start();
			
			return true;
		}
		
		private static function isWorthReattempting(operation:IOperation):Boolean
		{
			var restOp:RestHttpOperation = operation as RestHttpOperation;
			return (restOp && restOp.reattemptOnFault && restOp.attempts <= MAX_OPERATION_ATTEMPTS);
		}
		
		private static function restOperationForError(event:OperationFaultEvent):RestHttpOperation
		{
			var restOp:RestHttpOperation;
			if (event.target as FilterOperation) {
				if ((event.target as FilterOperation).operation as RestHttpOperation)
					return (event.target as FilterOperation).operation as RestHttpOperation;
			} else if (event.target as RestHttpOperation) {
				return (event.target as RestHttpOperation);
			}	
			return null;
		}
	}
}
