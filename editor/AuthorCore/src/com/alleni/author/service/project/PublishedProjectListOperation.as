/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 9/19/12
 * Time: 9:09 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.service.project {
import com.alleni.author.event.GadgetEvent;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Dock;
import com.alleni.author.service.BorealOperation;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.service.AbstractOperation;
import com.alleni.taconite.service.LogService;
import com.alleni.taconite.service.RestHttpOperation;

import flash.events.ErrorEvent;

import flash.events.Event;

public class PublishedProjectListOperation extends AbstractOperation{
    private var _projectTypeId:uint;
    private var _publishedProjects:Vector.<Project>;

    public function PublishedProjectListOperation(projectTypeId:uint) {
        displayName = "Loading Names of Published Projects";
        _projectTypeId = projectTypeId;
    }

    override public function execute():void{
        var operation:RestHttpOperation
        var query:Object = {published:true, projectTypeId:_projectTypeId, sortColumn:"name", order:"asc"};
        query.authorMemberId = TaconiteFactory.getEnvironmentImplementation().userId;
        operation = new RestHttpOperation("project/tiny", null, RestHttpOperation.GET);
        operation.queryString = query;
        BorealOperation.addListeners(operation, handleComplete,  handleError);
        //super.handleComplete(new Event(Event.COMPLETE));
        operation.execute();
    }

    override public function get result():*{
        return _publishedProjects;
    }

    override protected function handleComplete(event:Event):void{
        var operation:RestHttpOperation = RestHttpOperation(event.target);
        BorealOperation.removeListeners(operation);
        var operationResult:Object = operation.resultDecoded;
        Dock.instance.userItemList = Project.fromJsonList(operationResult.projects);
        for each(var project:Project in Dock.instance.userItemList){
            project.projectTypeId = _projectTypeId;
        }
        super.handleComplete(event);
    }

    override public function handleError(event:ErrorEvent):void{
        LogService.error("PublishedProjectListOperation error: " + event.text);
        trace("PublishedProjectListOperation error: " + event.text);
        dispatchEvent(new GadgetEvent(GadgetEvent.ERROR, null));
    }
}
}
