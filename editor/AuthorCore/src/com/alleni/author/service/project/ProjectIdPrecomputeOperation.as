package com.alleni.author.service.project {
import com.alleni.author.service.BorealOperation;
import com.alleni.taconite.service.AbstractOperation;
import com.alleni.taconite.service.RestHttpOperation;

import flash.events.ErrorEvent;
import flash.events.Event;

public class ProjectIdPrecomputeOperation extends AbstractOperation {
    private var _quantity:int;
    private var _ids:Array;

    public function ProjectIdPrecomputeOperation() {
    }
    override public function execute():void {
        var body:Object = {
            size: _quantity
        }
        var restHttpOp:RestHttpOperation = new RestHttpOperation("project/precomputeIds", body, RestHttpOperation.POST);
        BorealOperation.addListeners(restHttpOp, handleComplete);
        restHttpOp.displayName = "ProjectIdPrecomputeOperation";
        restHttpOp.execute();
    }

    override public function get result():* {
        return _ids;
    }

    override public function handleError(e:ErrorEvent):void {
        super.handleError(e);
    }

    override protected function handleComplete(e:Event):void {
        var operation:RestHttpOperation = RestHttpOperation(e.target);
        BorealOperation.removeListeners(operation);
        var operationResult:Object = operation.resultDecoded;
        _ids = operationResult as Array;
        super.handleComplete(e);
    }

    public function get ids():Array {
        return _ids;
    }
}
}
