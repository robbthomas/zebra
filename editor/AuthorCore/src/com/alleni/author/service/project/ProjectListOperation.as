package com.alleni.author.service.project {
import com.alleni.author.definition.BorealErrorCodes;
import com.alleni.author.event.GadgetEvent;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Dock;
import com.alleni.author.service.BorealOperation;
import com.alleni.taconite.event.OperationFaultEvent;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.service.AbstractOperation;
import com.alleni.taconite.service.LogService;
import com.alleni.taconite.service.RestHttpOperation;

import flash.events.ErrorEvent;
import flash.events.Event;

public class ProjectListOperation extends AbstractOperation {
    public static const ORDER_ASCENDING:String = "ASC";
    public static const ORDER_DESENDING:String = "DESC";

    private var _projects:Vector.<Project>;

    public var published:Boolean;
    public var projectTypeId:int

    public var size:Number = NaN;
    public var offset:Number = NaN;

    public var categoryId:int = -1;

    public var sort:int = -1;
    public var order:String = ORDER_ASCENDING;

    public var userName:String = null;
    public var ownerAccountId:String = null;

    public var templates:Boolean = false;
    public var inStore:Boolean = false;
    public var isForDock:Boolean = false;
    public var searchField:String = null;
    public var useMini:Boolean = false;

    public function ProjectListOperation(published:Boolean, projectTypeId:int) {
        displayName = "List Projects";
        this.published = published;
        this.projectTypeId = projectTypeId;
    }

    override public function execute():void {
        var operation:RestHttpOperation;
        var query:Object = {published:published, projectTypeId:projectTypeId};
        if(userName != null) {
            query.userName = userName;
        } else if(!published) {
            query.authorMemberId = TaconiteFactory.getEnvironmentImplementation().userId;
        }

        if(size > 0 && offset >= 0) {
            query.size = size;
            query.offset = offset;
        }

		if(categoryId >= 0 && (categoryId != Dock.CATEGORY_ALL)) {
            query.categoryId = categoryId;
        }
        if(searchField != null){
            query.genericSearch = searchField;
        }
        switch (sort) {
            case Dock.SORT_CATEGORY:
                query.sort = "categoryId";
                query.order = order;
                break;
            case Dock.SORT_NAME:
                query.sort = "name";
                query.order = order;
                break;
            case Dock.SORT_PRICE:
                query.sort = "price";
                query.order = order;
                break;
            case Dock.SORT_DATE:
            default:
                query.sort = "editedDateTime";
                order = ORDER_DESENDING; // Remove this when we allow the user to switch order.
                query.order = order;
                break;
        }
        if(isForDock){
            if(inStore){
                query.storeList = true;
            }else{
                query.ownerAccountId = TaconiteFactory.getEnvironmentImplementation().accountId;
            }
        }
        if(templates) {
            query.templates = true;
            query.sort = "name";
            query.order = ORDER_ASCENDING;
        }
        var pathAppendage:String = useMini?"/mini":"";
        operation = new RestHttpOperation("project"+pathAppendage, null, RestHttpOperation.GET);
        operation.queryString = query;
        operation.displayName = displayName;
        BorealOperation.addListeners(operation, handleComplete, handleError);
        operation.execute();
    }

    override public function get result():* {
        return _projects;
    }

    override public function handleError(e:ErrorEvent):void {
        LogService.error("ProjectListOperation error: " + e.text);
        trace("ProjectListOperation error: " + e.text);
        dispatchEvent(new GadgetEvent(GadgetEvent.ERROR, null));
    }

    override protected function handleComplete(e:Event):void {
        var operation:RestHttpOperation = RestHttpOperation(e.target);
        BorealOperation.removeListeners(operation);
        var operationResult:Object = operation.resultDecoded;
        var hasMoreData:Boolean = operationResult.hasMoreData;
        //var totalCount:Boolean = operationResult.totalCount;
        size = operationResult.totalCount;
        _projects = Project.fromJsonList(operationResult.projects);
        for each(var p:Project in _projects) {
            p.nameIsFromDatabase = true;
            p.publishedNameIsFromDatabase = true;
        }
        super.handleComplete(e);
    }

    public function get projects():Vector.<Project> {
        return _projects;
    }
}
}
