package com.alleni.author.service.project
{
    import com.alleni.author.controller.ui.GadgetController;
    import com.alleni.author.definition.BorealErrorCodes;
    import com.alleni.author.event.GadgetEvent;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.service.BorealOperation;
    import com.alleni.taconite.event.OperationFaultEvent;
    import com.alleni.taconite.service.AbstractOperation;
    import com.alleni.taconite.service.LogService;
    import com.alleni.taconite.service.RestHttpOperation;
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.taconite.event.ApplicationEvent;

    import flash.events.ProgressEvent;
    import flash.events.ErrorEvent;
    import flash.events.Event;

    public class ProjectLoadOperation extends AbstractOperation
    {
        private var _projectId:String;
        private var _project:Project;
        private var _loadAuthorState:Boolean;
        private var _externalUserId:String;
        private var _purchased:Boolean;

        public function ProjectLoadOperation(projectId:String, loadAuthorState:Boolean=false, externalUserId:String = null, purchased:Boolean=false) {
            _projectId = projectId;
            _loadAuthorState = loadAuthorState;
            _externalUserId = externalUserId;
            _purchased = purchased;
            displayName = "Loading Project";
        }

        override public function execute():void {
            _project = GadgetController.instance.model.getById(_projectId);
            if (_project && _project.content != null) {
                this.dispatchEvent(new Event(Event.COMPLETE));
                return;
            }

            var operation:RestHttpOperation;
            var query:Object = null;
            var url:String = "/" + projectId;
            if(_purchased) {
                // player uses a pure url method
                // in this case the projectId is actually an AccountProjectNumbersId
                url = "player" + url;
                if(_externalUserId && _externalUserId.length > 0) {
                    url = url + "/" + _externalUserId;
                }
            } else {
                // normal project loading has query parameters
                url = "project" + url;
                query = {authorMode:_loadAuthorState}
                if(_externalUserId && _externalUserId.length > 0) {
                    query.externalUserId = _externalUserId;
                }
            }
            operation = new RestHttpOperation(url, query, RestHttpOperation.GET);
            operation.addEventListener(OperationFaultEvent.COMMAND_ERROR, handleCommandError);
            operation.addEventListener(ProgressEvent.PROGRESS, handleProgress);
            BorealOperation.addListeners(operation, handleComplete, handleError);
            operation.displayName = displayName;
            operation.execute();
        }

        override public function get result():* {
            return project;
        }

        override public function handleError(e:ErrorEvent):void {
            super.handleError(e);
        }
        
        private function handleProgress(event:ProgressEvent):void {
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_LOAD_PROGRESS, 
                event.bytesLoaded/event.bytesTotal));
        }

        override protected function handleComplete(e:Event):void {
            var operation:RestHttpOperation = RestHttpOperation(e.target);
            BorealOperation.removeListeners(operation);
            var operationResult:Object = operation.resultDecoded;
            _project = Project.fromJson(operationResult);
            _project.nameIsFromDatabase = true;
            _project.publishedNameIsFromDatabase = true;
            super.handleComplete(e);
        }

        protected function handleCommandError(event:OperationFaultEvent):void {
            var operation:RestHttpOperation = event.target as RestHttpOperation;

            if (!operation)
                return;

            var error:Object = operation.resultDecoded;

            if (BorealErrorCodes.isLoadFailed(error)) {
                dispatchEvent(new GadgetEvent(GadgetEvent.LOAD_FAILED, projectId));
                return;
            } else if (BorealErrorCodes.isNotAllowed(error)) {
                dispatchEvent(new GadgetEvent(GadgetEvent.LOAD_NO_PERMISSION, projectId));
                return;
            } else {
                LogService.error("Unhandled command error for '"+operation.displayName+"' in ProjectLoadOperation.");
            }

            BorealOperation.displayDefaultCommandErrorAlert(operation);
        }

        public function get projectId():String {
            return _projectId;
        }

        public function get project():Project {
            return _project;
        }
    }
}
