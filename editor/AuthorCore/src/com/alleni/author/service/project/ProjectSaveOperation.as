package com.alleni.author.service.project {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.BorealErrorCodes;
import com.alleni.author.definition.BorealErrorTypes;
import com.alleni.author.definition.GadgetType;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.event.GadgetEvent;
import com.alleni.author.model.gadgets.GadgetScreenshot;
import com.alleni.author.model.project.Project;
import com.alleni.author.service.BorealOperation;
import com.alleni.author.service.gadgets.GadgetIconOperation;
import com.alleni.author.service.gadgets.GadgetScreenshotsOperation;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.OperationFaultEvent;
import com.alleni.taconite.persistence.json.JSON;
import com.alleni.taconite.service.AbstractOperation;
import com.alleni.taconite.service.HttpFormData;
import com.alleni.taconite.service.LogService;
import com.alleni.taconite.service.RestHttpOperation;

import flash.display.Bitmap;

import flash.events.ErrorEvent;
import flash.events.Event;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import mx.graphics.codec.PNGEncoder;

public class ProjectSaveOperation extends AbstractOperation {
    private var _project:Project;
    private var _publish:Boolean;
    private var _replaceExisting:Boolean;
    private var _id:String;

    public var pendingIcon:ByteArray;
    public var pendingUploads:Dictionary;
    public var pendingDelete:Dictionary;

    public function ProjectSaveOperation(project:Project, publish:Boolean, replaceExisting:Boolean = false) {
        _project = project;
        _publish = publish;
        _replaceExisting = replaceExisting;
        switch(project.projectTypeId) {
            case GadgetType.EVENT: displayName = "Saving Event"; break;
            case GadgetType.GENERIC: displayName = "Saving Gadget"; break;
            default : displayName = "Saving Project";
        }
    }

    override public function execute():void {
        if (_project.pendingIconOperation) {
            var pendingIconOperation:GadgetIconOperation = _project.pendingIconOperation;
            pendingIcon = pendingIconOperation.data[0];
            _project.pendingIconOperation = null;
        }
        if (_project.pendingScreenshotOperation) {
            var pendingScreenshotOperation:GadgetScreenshotsOperation = _project.pendingScreenshotOperation;
            pendingDelete = pendingScreenshotOperation.pendingDelete;
            pendingUploads = pendingScreenshotOperation.pendingUploads;
            _project.pendingScreenshotOperation = null;
        }
        trace(Utilities.timestamp + " ProjectSaveOperation::execute save name=" + _project.name);
        _project.published = _publish;
        var path:String = "project";
        var body:Object = _project.toJson();
        if(_replaceExisting) {
            if(_publish) {
                body.ignoreDuplicatePublishedName = true;
            } else {
                body.ignoreDuplicateName = true;
            }
        } else {
            // body will already have ignore filled in based on whether the name has been changed
            // override this for published so it will always error unless explicitly set
            if(_publish) {
                body.ignoreDuplicatePublishedName = false;
            }
        }
        var form:HttpFormData;
        if(pendingIcon != null || pendingDelete != null || pendingUploads != null || (_project.previewImage != null && _project.previewImage.assetID == null)) {
            path = "project/multipart";
            form = new HttpFormData();
            var projectBytes:ByteArray = new ByteArray();
            projectBytes.writeUTFBytes(com.alleni.taconite.persistence.json.JSON.encode(body));
            form.addFile("project", "project", "application/json", projectBytes);
            body = form;
        }
        if(pendingIcon != null) {
            form.addFile("icon", "icon", "application/octet-stream", pendingIcon);
        }
        if(pendingDelete != null) {
            for each(var screenshotDelete:GadgetScreenshot in pendingDelete){
                form.addVariable("deletedScreenshots", screenshotDelete.screenshotDBID);
   	        }
        }
        if(pendingUploads != null) {
            for each(var screenshotUpload:GadgetScreenshot in pendingUploads){
                var name:String = String(screenshotUpload.visibleIndex);
                var data:ByteArray = new PNGEncoder().encode(screenshotUpload.imageBitmapData);
                form.addFile("screenshots", name, "image/png", data);
   	        }
        }
        if(_project.previewImage != null && _project.previewImage.assetID == null) {
            form.addFile("previewImage", _project.previewImage.name, "image/png"/*pendingXXXXX.mimeType*/, new PNGEncoder().encode((_project.previewImage.content as Bitmap).bitmapData));
        }
        var restHttpOp:RestHttpOperation = new RestHttpOperation(path, body, RestHttpOperation.POST);
        restHttpOp.addEventListener(OperationFaultEvent.COMMAND_ERROR, handleCommandError);
        BorealOperation.addListeners(restHttpOp, handleComplete);
        restHttpOp.displayName = displayName;
        restHttpOp.execute();
    }

    override public function get result():* {
        return _id;
    }

    override public function handleError(e:ErrorEvent):void {
        super.handleError(e);
    }

    protected function handleCommandError(event:OperationFaultEvent):void
    {
        event.stopPropagation();
        event.stopImmediatePropagation();

        var error:Object = RestHttpOperation(event.target).resultDecoded;
        var id:String = null;

        if (BorealErrorTypes.isBusiness(error)) {
            if (BorealErrorCodes.isNameInUse(error)) {
                id = (error != null && "id" in error) ? error.id : null;
                dispatchEvent(new GadgetEvent(GadgetEvent.NAME_IN_USE, id));
                return;
            } else if (BorealErrorCodes.isPublishedNameInUse(error)) {
                id = (error != null && "id" in error) ? error.id : null;
                dispatchEvent(new GadgetEvent(GadgetEvent.PUBLISHED_NAME_IN_USE, id));
                return;
            } else if (BorealErrorCodes.isPublishedPriceTooLow(error)) {
                if (_project != null && error != null && "minPrice" in error) {
                    _project.minPrice = error.minPrice;
                    id = _project.id;
                }
                dispatchEvent(new GadgetEvent(GadgetEvent.PUBLISHED_PRICE_TOO_LOW, id));
                return;
            } else if (BorealErrorCodes.isPublishFailed(error) || BorealErrorCodes.isSaveFailed(error)) {
                id = (_project != null) ? _project.id : null;
                dispatchEvent(new GadgetEvent(GadgetEvent.SAVE_FAILED, _project.id));
                return;
            } else
                LogService.error("Unhandled business command error for '" + _project.name + "' in PublishedSaveOperation.");
        } else if (BorealErrorTypes.isInternal(error)) {
            if (BorealErrorCodes.isNotAllowed(error)) {
                dispatchEvent(new GadgetEvent(GadgetEvent.SAVE_NO_PERMISSION, null));
                return;
            } else if (BorealErrorCodes.isPublishedNotFound(error)) {
                dispatchEvent(new GadgetEvent(GadgetEvent.SAVE_FAILED, null));
                return;
            } else if (BorealErrorCodes.isRepublishNotAllowed(error)) {
                id = (_project != null) ? _project.id : null;
                dispatchEvent(new GadgetEvent(GadgetEvent.REPUBLISH_NOT_ALLOWED, _project.id));
                return;
            } else
                LogService.error("Unhandled internal command error for '" + _project.name + "' in PublishedSaveOperation.");
        } else
            LogService.error("Unhandled command error for '" + _project.name + "' in PublishedSaveOperation.");
        BorealOperation.displayDefaultCommandErrorAlert(RestHttpOperation(event.target));
    }

    override protected function handleComplete(e:Event):void {
        var operation:RestHttpOperation = RestHttpOperation(e.target);
        BorealOperation.removeListeners(operation);
        var operationResult:Object = operation.resultDecoded;
        Project.fromJson(operationResult, _project);
        _id = _project.projectId;
        trace(Utilities.timestamp + " ProjectSaveOperation::handleComplete saved gadget ID="+_id + " name=" + _project.name);
        _project.nameIsFromDatabase = true;
        _project.publishedNameIsFromDatabase = true;
//        if (_project.pendingIconOperation || _project.pendingScreenshotOperation) {
//            if (_project.pendingIconOperation) {
//                var pendingIconOperation:GadgetIconOperation = _project.pendingIconOperation;
//                pendingIconOperation.id = _id;
//                //pendingIconOperation.addEventListener(Event.COMPLETE, handleScreenshotOrIconComplete);
//                pendingIconOperation.execute();
//                _project.pendingIconOperation = null;
//            }
//            if (_project.pendingScreenshotOperation) {
//                var pendingScreenshotOperation:GadgetScreenshotsOperation = _project.pendingScreenshotOperation;
//                pendingScreenshotOperation.publishedId = _id;
//                //pendingScreenshotOperation.addEventListener(Event.COMPLETE, handleScreenshotOrIconComplete);
//                pendingScreenshotOperation.execute();
//                _project.pendingScreenshotOperation = null;
//            }
//            //return;
//        }
        super.handleComplete(e);
        if(_project.projectTypeId == GadgetType.PROJECT) {
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_STATE_SAVED)); //TODO GADGET_PROJECT shim for those watching for save complete
        }
    }

    public function get id():String {
        return _id;
    }

    public function get projectName():String {
        return _project.name;
    }
}
}
