package com.alleni.author.service.project {
import com.alleni.author.model.project.Project;
import com.alleni.author.service.BorealOperation;
import com.alleni.taconite.event.OperationFaultEvent;
import com.alleni.taconite.service.AbstractOperation;
import com.alleni.taconite.service.RestHttpOperation;

import flash.events.ErrorEvent;
import flash.events.Event;

public class ProjectRenameOperation extends AbstractOperation {
    private var _project:Project;
    private var _newName:String;

    public function ProjectRenameOperation(project:Project, newName:String) {
        _project = project;
        _newName = newName;
    }

    override public function execute():void {
        var restHttpOp:RestHttpOperation = new RestHttpOperation("project/rename/" + _project.id, {ignoreDuplicateName:true, newName:_newName}, RestHttpOperation.POST);
        restHttpOp.addEventListener(OperationFaultEvent.COMMAND_ERROR, handleCommandError);
        BorealOperation.addListeners(restHttpOp, handleComplete);
        restHttpOp.displayName = "ProjectRenameOperation";
        restHttpOp.execute();
    }

    override protected function handleComplete(e:Event):void {
        _project.name = _newName;
        super.handleComplete(e);
    }

    protected function handleCommandError(event:OperationFaultEvent):void
    {
        event.stopPropagation();
        event.stopImmediatePropagation();

        var error:Object = RestHttpOperation(event.target).resultDecoded;
        var errorMessage:String = "";
        if(error.hasOwnProperty("message")) {
            errorMessage = error.message;
        }

        this.dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, errorMessage));
    }
}
}
