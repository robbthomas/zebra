package com.alleni.author.service.assets
{
	import com.alleni.author.model.ui.Asset;
	
	public class AssetSaveOperation extends AssetOperation
	{	
		private var _predefinedType:int = -1;
		private var _predefinedMime:String;
		
		public function AssetSaveOperation(asset:Asset, name:String, data:*=null, predefinedType:int=-1, predefinedMime:String=null)
		{
			super(asset, data);
			_name = name;
			_predefinedType = predefinedType;
			_predefinedMime = predefinedMime;
		}
		
		override public function execute():void
		{
			save(_predefinedType, _predefinedMime);
		}
	}
}