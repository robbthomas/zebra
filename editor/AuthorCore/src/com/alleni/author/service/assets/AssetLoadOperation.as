package com.alleni.author.service.assets
{
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.service.ILoadOperation;
	import com.alleni.author.util.Set;
	import com.alleni.author.util.thread.PseudoThread;
	import com.alleni.author.util.thread.PseudoThreadWaitForAssetsRunnable;
	import com.alleni.taconite.dev.Utilities;
	
	import flash.events.ErrorEvent;

	public class AssetLoadOperation extends AssetOperation implements ILoadOperation
	{
		private var _preloadMedia:Boolean;
        public var dataOnly:Boolean = false;
		
		private static var _queue:Vector.<AssetLoadOperation>;

        private static var holdingSet:Set = new Set(true);
		
		public function AssetLoadOperation(asset:Asset, preloadMedia:Boolean=true)
		{
			super(asset, null);
			_preloadMedia = preloadMedia;
		}
		
		private static function get queue():Vector.<AssetLoadOperation>
		{
			if (!_queue)
				_queue = new Vector.<AssetLoadOperation>();
			return _queue;
		}

        /**
         * Prevents assets from loading and returns a token object.
         * This token should be passed back to unhold and then discarded.
         * The thread will remain paused until all distributed tokens are either
         * returned or garbage collected.
         * @param source A description of the source of the pausing (ex. class or operation name)
         * @return token
         */
        public static function hold(source:String="Unnamed"):Object
        {
			//trace("AssetLoadOperation::hold(", source, ")", holding());
            var token:Object = {name:source};
            holdingSet.add(token);
            return token;
        }

        /**
         * Releases a token representing a previous hold.
         * This token must be a return value of the hold function.
         * Also, a token should only be released once.
         * @param token
         */
        public static function unhold(token:Object):void
        {
			//trace("AssetLoadOperation::unhold(", token.name, ")", holding());
            holdingSet.remove(token);
            if(holdingSet.isEmpty && queue.length > 0) {
                var operation:AssetLoadOperation;
                for each(operation in queue)
                    operation.execute();
                queue.splice(0, queue.length);
                PseudoThread.add(new PseudoThreadWaitForAssetsRunnable());
            }
        }

        public static function holding():String {
            return "[" + holdingSet.allItems.map(function(token:Object, index:int, array:Array):String{return token.name}).join(",") + "]";
        }

		override public function execute():void
		{
			if(!holdingSet.isEmpty) {
				queue.push(this);
                return;
			}
			if (!Utilities.UUID_REGEX.test(_asset.assetID)) {	
				handleError(new ErrorEvent(ErrorEvent.ERROR, false, false, "Unable to honor load request for asset with ID '"+_asset.assetID+"', this ID is not a valid UUID."));
				return;
			}

            //If this needs to be called twice for a single asset it might not call complete, double check
            if(_asset.pendingData){
                return;
            }

            if(dataOnly) {
                loadData(_preloadMedia);
                return;
            }
			
			_asset = AssetController.instance.model.getById(_asset.assetID);
			if (_asset != null) {
				if (_asset.content != null) { // it's already been loaded, so pull from cache
					handleComplete(null);
					return;
				}
				
				if (_asset.upToDate && _preloadMedia && !_asset.content) {
					_asset.pendingData = true;
                    load(_preloadMedia);
					return;
				}
			}
			load(_preloadMedia);
		}
		
		public function get id():String
		{
			if (_asset != null)
				return _asset.assetID;
			return null;
		}
	}
}