package com.alleni.author.service.assets
{
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.BorealErrorCodes;
	import com.alleni.author.event.AssetEvent;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.service.RestPipeline;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.factory.IFile;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.FontCompilerOperation;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	import com.alleni.taconite.service.SoundCompilerOperation;
	
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.media.Sound;
	import flash.net.URLRequestHeader;
	import flash.net.URLStream;
	import flash.system.Capabilities;
	import flash.utils.ByteArray;
	
	import mx.graphics.codec.PNGEncoder;
	import mx.utils.Base64Encoder;

	public class AssetOperation extends AbstractOperation
	{
		private static const PATH:String = "asset";
		private static const LOAD_COMPRESSED_DATA_FROM_BYTE_ARRAY:String = "loadCompressedDataFromByteArray";
		private static const MEDIA_ENCODE_ERROR:String = "We cannot import this file. Please re-encode and try again.";
		
		protected var _asset:Asset;
		
		private var _data:*;
		protected var _name:String;
		
		public function AssetOperation(asset:Asset, data:*=null)
		{
			_asset = asset;
			_data = data;
		}
		
		public function get asset():Asset
		{
			return _asset;
		}
		
		public function set name(value:String):void
		{
			_name = value;
		}
		
		override public function execute():void
		{
			var stream:URLStream;
			
			if (_asset.type < 0 && _data) {
				LogService.error("AssetOperation::execute: Unsupported asset type: " + _asset.type);
				return;
			}
			
			if (_data as IFile) {
				/*var file:IFile = IFile(_data);								TODO
				_name = Utilities.FILENAME_FROM_URL_REGEX.exec(file.url);
				
				stream = new URLStream();
				stream.addEventListener(Event.COMPLETE, writeToStore);
				stream.addEventListener(IOErrorEvent.IO_ERROR, BorealOperation.handleErrorAlert("load local asset"));
				stream.load(new URLRequest(file.url));*/
			} else if (_asset.type == AssetType.TEXT && _data as String) {
				_name = Utilities.uniqueDateBasedName + ".txt";
				if(_asset.name == null || _asset.name == "")
				_asset.name = _name;
				_data = new ByteArray();
				ByteArray(_data).writeUTFBytes(_data);
				save();
			} else if (_data as String) {
				// probably a URL...
				/*_name = Utilities.FILENAME_FROM_URL_REGEX.exec(_data); 		TODO
				stream = new URLStream();
				stream.addEventListener(Event.COMPLETE, writeToStore);
				stream.addEventListener(IOErrorEvent.IO_ERROR, BorealOperation.handleErrorAlert("load remote asset"));
				stream.load(new URLRequest(_data));*/
			} else if (_data as BitmapData) {
				// bitmap pasted in
				var pngEncoder:PNGEncoder = new PNGEncoder();
				_data = pngEncoder.encode(_data as BitmapData);
				_name = Utilities.uniqueDateBasedName + ".png";
				save();
			} else if (_asset.type == AssetType.SVG && _data as XML) {
				_name = Utilities.uniqueDateBasedName + ".svg";
				//_asset.name = _name;
				_data = new ByteArray();
				ByteArray(_data).writeUTFBytes(XML(_data).toXMLString());
				save();
			} else
				LogService.error("AssetOperation::execute reached an unhandled state, this should not have happened");
		}

		public static function getThumbnailOperation(id:String, name:String, notReadyHandler:Function):RestHttpOperation
		{
			var operation:RestHttpOperation = new RestHttpOperation("thumbnail/" + id, null, RestHttpOperation.GET);
			operation.displayName = "Loading thumbnail for '" + name + "'";
            //operation.cacheContent = true;
			operation.addEventListener(OperationFaultEvent.COMMAND_ERROR, function(e:OperationFaultEvent):void{
				var restOp:RestHttpOperation = e.target as RestHttpOperation;
				e.stopPropagation();
				e.stopImmediatePropagation();
				notReadyHandler(restOp);
			});
			return operation;
		}
				
		protected function save(predefinedType:int=-1, predefinedMime:String=null):void
		{
			var operation:RestHttpOperation = new RestHttpOperation(PATH);
			
			var mimeType:String = _asset?_asset.mimeType:predefinedMime;
			if (!mimeType)
				mimeType = AssetDescription.getMimeTypeForExtension(_name);
			if (!mimeType && predefinedType > 0)
				mimeType = AssetDescription.guessMimeByType(predefinedType);
			
			var encoder:Base64Encoder = new Base64Encoder();
			encoder.insertNewLines = false;
			encoder.encode(_name);
			var name64:String = encoder.toString();
			encoder.encode(mimeType);
			var mime64:String = encoder.toString();
			encoder.drain();
			
			if (_asset && Utilities.UUID_REGEX.test(_asset.assetID)) {
				operation.customHeaders.push(new URLRequestHeader("name64", name64));
				operation.method = RestHttpOperation.PUT; // Update
				operation.path += "/" + _asset.assetID;
			} else {
				Utilities.assert(mimeType != null); // otherwise we can go no further
				
				Utilities.assert(_asset == null); // we should not have an asset at this point. if we do it is an orphan with an invalid id
				
				operation.method = RestHttpOperation.POST; // Create
				operation.path += "/" + name64 + "/" + mime64;
				
				Utilities.assert(_data != null);
				if (_data as XML == null)
					_data.position = 0;  // reset byteArray
			}
			
			operation.data = _data;
			operation.displayName = "Saving asset " + _name;
			LogService.debug("Operation: Saving asset " + _name + " with mime: " + mimeType);

			RestPipeline.push(operation, function (event:Event):void {
				if (!_asset) { // this is a new asset
					_asset = new Asset(RestHttpOperation(event.target).resultDecoded.id);
					_asset.name = _name;
					_asset.type = (predefinedType>=0 ? predefinedType : AssetDescription.validateFileType(_name));
					_asset.mimeType = mimeType;
				}
				if (RestHttpOperation(event.target).method == RestHttpOperation.POST) { // before dispatching complete, grab content
					LogService.debug("Operation: Asset saved with id " + _asset.assetID);
					assignContentAndDispatchComplete(null, _data);
				} else { // support update of asset metadata
					LogService.debug("Operation: Asset '"+_asset.name+"' updated with id " + _asset.assetID);
					handleComplete(event);
				}
			}, handleError);
		}
		
		protected function load(preloadMedia:Boolean=true):void
		{
            if(_asset.fileID != null) {
                // assume asset metadata info is already loaded
                loadData(preloadMedia);
                return;
            }
			var operation:RestHttpOperation = new RestHttpOperation(PATH + "/info/" + _asset.assetID, 
				null, RestHttpOperation.GET);
			operation.displayName = "Loading asset " + _asset.assetID;
			LogService.debug("Operation: Loading asset " + _asset.assetID);

			RestPipeline.push(operation, function (event:Event):void {
				var decoded:Object = RestHttpOperation(event.target).resultDecoded;
				
				if (decoded != null && AssetDescription.validateMimeType(decoded.mimeType) > -1) {
					_asset.type = AssetDescription.validateMimeType(decoded.mimeType);
					_asset.fileID = decoded.fileID;
					_asset.name = decoded.name;
                    LogService.info("$$$ AssetLoadOperation: Loaded asset id="+_asset.assetID + " name="+_asset.name);

					LogService.debug("[" + _asset.name + "] Loaded: " + decoded.mimeType + " : " +  _asset.type + " PRELOADING: "+preloadMedia);
					loadData(preloadMedia);
				} else {
					handleError(new ErrorEvent(ErrorEvent.ERROR, false, false, "Asset '" + decoded.name + "' Has an invalid type :" + decoded.mimeType));
					LogService.debug("[" + decoded.name + "] Loaded: INVALID TYPE "+AssetDescription.validateMimeType(decoded.mimeType)+": " + decoded.mimeType);
				}
			}, handleError);
		}
		
		protected function loadData(preloadMedia:Boolean=true):void
		{
			var operation:RestHttpOperation = new RestHttpOperation(null);
			operation.path = PATH + "/stream/" + _asset.assetID;
			operation.cacheContent = true;
			operation.addEventListener(ProgressEvent.PROGRESS, handleDataProgress);
			// capture command error if the asset data was not found!
			operation.addEventListener(OperationFaultEvent.COMMAND_ERROR, handleCommandError);
			
			LogService.debug("Loading Asset '"+_asset.name+"' Type " + _asset.type);
			operation.displayName = "Loading asset " + _asset.name + " data";
			
			switch (_asset.type) {
				case AssetType.BITMAP:
				case AssetType.SWF:
				case AssetType.SVG:
				case AssetType.XML:
				case AssetType.TEXT:
				case AssetType.CSS:
				case AssetType.FONT:
					RestPipeline.push(operation, assignContentAndDispatchComplete, handleError);
					break;
				case AssetType.VIDEO:
					operation = null;
					if (!AssetController.instance.isWaitingForContent(_asset))
						assignContentAndDispatchComplete(null);
					break;
				case AssetType.AUDIO:
					if (preloadMedia) {
						RestPipeline.push(operation, assignContentAndDispatchComplete, handleError);
					} else {
						operation = null;
						if (!AssetController.instance.isWaitingForContent(_asset))
							assignContentAndDispatchComplete(null, null, false);
					}
					break;
				default:
					operation = null;
					LogService.error("AssetOperation::load reached an unhandled state, this should not have happened");
					break;
			}
		}
		
		private function assignContentAndDispatchComplete(event:Event, data:*=null, preloadMedia:Boolean=true):void
		{
			if (data == null && event != null)
				data = RestHttpOperation(event.target).resultByteArray;
			
			switch (_asset.type) {
				case AssetType.BITMAP:
				case AssetType.SWF:
					var loader:Loader = new Loader();
					loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e:Event):void {
						_asset.content = loader.content;
						handleComplete(event);
					});
					loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleError);
					loader.loadBytes(data);
					break;
				case AssetType.SVG:
					var viewBox:Array;
					_asset.content = new XML(data);
					// extract width/height from svg
					if(_asset.content.@viewBox.toString() != '')
					{
						viewBox = _asset.content.@viewBox.toString().split(' ');
						// insert @width/@height so svgweb can render
						_asset.content.@width = viewBox[2];
						_asset.content.@height = viewBox[3];
					}
					handleComplete(event);
					break;
				case AssetType.XML:
					_asset.content = new XML(data);
					handleComplete(event);
					break;
				case AssetType.TEXT:
				case AssetType.CSS:
					_asset.content = new String(data);
					handleComplete(event);
					break;
				case AssetType.VIDEO:
					handleComplete(event);
					break;
				case AssetType.AUDIO:
					if (!preloadMedia) {
						handleComplete(event);
						break;
					}

					var sound:Sound = new Sound();
					if (LOAD_COMPRESSED_DATA_FROM_BYTE_ARRAY in sound) {
						var bytes:ByteArray = data;
						bytes.position = 0;
						try {
							sound[LOAD_COMPRESSED_DATA_FROM_BYTE_ARRAY](bytes, bytes.length);
							_asset.content = sound;
							handleComplete(event);
						} catch(e:Error) {
							LogService.error("Error decoding sound: " + e + " Falling back on best-effort stream from server.");
							var errorEvent:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR);
							errorEvent.text = MEDIA_ENCODE_ERROR;
							handleError(errorEvent);
						}
					} else {
						sound = null;
						var soundCompiler:SoundCompilerOperation = new SoundCompilerOperation(data);
						soundCompiler.addEventListener(OperationFaultEvent.FAULT, function(event:OperationFaultEvent):void {
							LogService.error("Error compiling preloaded sound: " + event.error.text + " Falling back on best-effort stream from server.");
							event.error.text = MEDIA_ENCODE_ERROR;
							handleError(event.error);
						}, false, 0, true);
						soundCompiler.addEventListener(Event.COMPLETE, function():void {
							_asset.content = soundCompiler.result as Sound;
							handleComplete(event);
						}, false, 0, true);
						soundCompiler.execute();
					}
					break;
				case AssetType.FONT:
					var fontCompiler:FontCompilerOperation = new FontCompilerOperation(data, _asset.name);
					fontCompiler.addEventListener(OperationFaultEvent.FAULT, function(event:OperationFaultEvent):void {
						LogService.error("Error compiling font: " + event.error.text);
						handleError(event.error);
					}, false, 0, true);
					fontCompiler.addEventListener(Event.COMPLETE, function(event:Event):void {
						_asset.content = fontCompiler.result as Sound;
						handleComplete(event);
					}, false, 0, true);
					fontCompiler.execute();
					break;
				default:
					trace("Loading asset: type not recognized: type="+asset.type, "id="+_asset.assetID);
					LogService.error("AssetOperation::decodeDataForContent reached an unhandled state, this should not have happened");
					break;
			}
		}
		
		override protected function handleComplete(e:Event):void
		{
			dispatchEvent(new AssetEvent(AssetEvent.COMPLETE, _asset));
			if (!e) return;
			var operation:RestHttpOperation = e.target as RestHttpOperation;
			if (!operation) return;
			operation.removeEventListener(ProgressEvent.PROGRESS, handleDataProgress);
			operation.removeEventListener(OperationFaultEvent.COMMAND_ERROR, handleCommandError);
		}
		
		private function handleDataProgress(e:Event):void
		{
			var operation:RestHttpOperation = e.target as RestHttpOperation;
			if (!operation) return;
			_progressFraction = operation.progressFraction;
			dispatchEvent(e);
		}
		
		private function handleCommandError(e:OperationFaultEvent):void
		{
			var operation:RestHttpOperation = e.target as RestHttpOperation;
			if (!operation) return;
			if (BorealErrorCodes.getCode(operation.resultDecoded) == BorealErrorCodes.ASSET_DATA_NOT_FOUND) {
				e.stopPropagation();
				e.stopImmediatePropagation();
				handleError(e.error);
			}
		}
		
		override public function handleError(e:ErrorEvent):void
		{
			trace("Asset ERROR",e.text, "id="+_asset.assetID, "type="+_asset.type);
			LogService.error("AssetOperation: " + e.text);
			dispatchEvent(new AssetEvent(AssetEvent.ERROR, _asset, e.text));
			var operation:RestHttpOperation = e.target as RestHttpOperation;
			if (!operation) return;
			operation.removeEventListener(ProgressEvent.PROGRESS, handleDataProgress);
			operation.removeEventListener(OperationFaultEvent.COMMAND_ERROR, handleCommandError);
		}
	}
}