package com.alleni.author.service.assets
{
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.Assets;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.Event;
	import com.alleni.author.service.BorealOperation;

	public class AssetListOperation extends AbstractOperation
	{
		private var _resultAssets:Assets;
		public var size:int;
		public var offset:int;
		
		public function AssetListOperation(size:int=100, offset:int=0)
		{
			_resultAssets = new Assets();
			this.size = size;
			this.offset = offset;
		}
		
		override public function execute():void
		{
			var operation:RestHttpOperation = new RestHttpOperation("asset.json?size=" + this.size + "&offset=" + this.offset, null, RestHttpOperation.GET, true);
			operation.displayName = "Loading asset list";
			BorealOperation.addListeners(operation, handleComplete);
			operation.execute();
		}
		
		override protected function handleComplete(e:Event):void
		{
			var assetArray:Array = RestHttpOperation(e.target).resultDecoded as Array;
			var i:int;
			var tempAsset:Asset;
			for(i = 0 ; i < assetArray.length ; i++) {
				tempAsset = new Asset(assetArray[i].id);
				tempAsset.type = AssetDescription.validateMimeType(assetArray[i].mimeType);
				tempAsset.name = assetArray[i].name;
				
				_resultAssets.assets.addItem(tempAsset);
			}
			super.handleComplete(e);
		}
		
		public function get resultAssets():Assets
		{
			return _resultAssets;
		}
	}
}