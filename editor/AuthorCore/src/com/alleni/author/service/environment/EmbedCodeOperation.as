package com.alleni.author.service.environment
{
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.service.AbstractOperation;
	
	import flash.events.Event;
	
	public class EmbedCodeOperation extends AbstractOperation
	{
		private static var _environment:IEnvironment;
		
		public var embedCode:String;
		
		public function EmbedCodeOperation()
		{
			super();
		}
		
		override public function execute():void
		{
			handleComplete(null);
		}
		
		override protected function handleComplete(e:Event):void
		{// DEPRECATED. MAY NO LONGER NEED THIS OPERATION AT ALL. LEAVING FOR POSTERITY FOR NOW.
			/*var document:Document = Application.instance.document;
			if (!document) return;
			var gadget:Gadget = document.gadget;
			if (!gadget) return;
			
			const embedURI:String = environment.getEmbedURIForPurchaseId(gadget.userAwarePurchaseId);
			if (!embedURI) return;
			
			embedCode = "<iframe title='"+GadgetDescription.cleanName(gadget, true)+"' class='zapp' \n" +
				"type='text/html' width='" + width + "px' height='" + height + "px' \n" +
				"src='" + embedURI + "' \n" + 
				"frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>";*/
			super.handleComplete(e);
		}
		
		private static function get environment():IEnvironment
		{
			if (!_environment)
				_environment = TaconiteFactory.getEnvironmentImplementation();
			return _environment;
		}
	}
}