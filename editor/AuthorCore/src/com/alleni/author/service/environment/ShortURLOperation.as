package com.alleni.author.service.environment
{
	import com.alleni.taconite.persistence.json.JSONDecoder;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.LogService;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	public class ShortURLOperation extends AbstractOperation
	{
		private static const BITLY_LOGIN:String = "zebras";
		private static const BITLY_API_KEY:String = "R_0e832a73116164b8ffef4c71c6182d39";
		private static var _sourceURL:String;
		
		public var url:String;
		
		public function ShortURLOperation(sourceURL:String)
		{
			super();
			_sourceURL = sourceURL;
		}
		
		override public function execute():void
		{
			const apiURL:String = "http://api.bitly.com/v3/shorten?login=" + BITLY_LOGIN + "&apikey=" + BITLY_API_KEY + "&longUrl=" + _sourceURL + "&format=json";
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, function(event:IOErrorEvent):void {
				LogService.error("NOT handling HTTP error: " + event.type + " : " + event.text);
			});
			urlLoader.addEventListener(Event.COMPLETE, handleComplete);
			urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			urlLoader.load(new URLRequest(apiURL));
		}
		
		override protected function handleComplete(e:Event):void
		{
			/*
			example of Bitly Pro API response for reference
			{
				"status_code": 200, 
				"data": {
					"url": "http://bit.ly/cmeH01", 
					"hash": "cmeH01", 
					"global_hash": "1YKMfY", 
					"long_url": "http://betaworks.com/", 
					"new_hash": 0
				}, 
				"status_txt": "OK"
			}*/
			
			var urlLoader:URLLoader = e.target as URLLoader;
			var bytes:ByteArray = urlLoader.data as ByteArray;
			var resultDecoded:Object;
			
			if (bytes != null && bytes.length > 0)
				resultDecoded = new JSONDecoder(bytes.readUTFBytes(bytes.length), true).getValue();
			if (resultDecoded != null && "data" in resultDecoded)
				if ("url" in resultDecoded.data)
					url = resultDecoded.data.url;
			
			super.handleComplete(e);
		}
	}
}