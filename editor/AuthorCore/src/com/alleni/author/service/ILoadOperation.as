package com.alleni.author.service
{
	import com.alleni.taconite.service.IOperation;
	
	public interface ILoadOperation extends IOperation
	{
		function get id():String;
	}
}