package com.alleni.author.service
{
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.app.ProjectController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.palettes.FeedbackController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.document.Document;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Feedback;
	import com.alleni.author.model.ui.QAVellumSettings;
	import com.alleni.author.util.ViewUtils;
	import com.alleni.author.view.ui.controls.Alert;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.EmailOperation;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.events.ErrorEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	import mx.events.CloseEvent;
	import mx.graphics.codec.PNGEncoder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.LoadEvent;
	import mx.rpc.soap.Operation;
	import mx.rpc.soap.WebService;
	import mx.utils.Base64Encoder;
	
	public class FeedbackOperation extends AbstractOperation
	{
		private static const MAX_ATTEMPTS:int = 3;
		private static const OPERATION_REATTEMPT_DELAY:int = 2 * 1000; // 2 seconds
		private static const CREATE_ISSUE_OPERATION:String = "createIssue";
		
		private static var _instance:FeedbackOperation;
		
		private var _username:String;
		private var _currentVersion:String;
		private var _versionID:String;
		private var _authToken:String;
		private var _projectKey:String;
		private var _ticketKey:String;
		
		private var _webService:WebService;
		private var _description:String;
		
		public function FeedbackOperation()
		{
			Utilities.assert(!_instance);
			super();
			Feedback.instance.enabled = false;
		}
		
		public static function get instance():FeedbackOperation
		{
			if (_instance == null)
				_instance = new FeedbackOperation();
			return _instance;
		}
		
		public function initialize():void
		{
			this.refreshScreenshot();  //request was made to refresh screenshot every time qa panel is reopened
			
			if (!Feedback.instance.enabled) {
				if (!_webService) {
					_currentVersion = TaconiteFactory.getAppDescriptorImplementation().version;
					
					_webService = new WebService();
					
					_projectKey = Feedback.PROJECT_KEY_LIVE;
					
					_webService.wsdl = Feedback.JIRA_WSDL;
					_webService.addEventListener(LoadEvent.LOAD, handleLoad);
					_webService.addEventListener(FaultEvent.FAULT, handleFault);
					_webService.login.addEventListener(ResultEvent.RESULT, handleInitialLogin);
					_webService.login.addEventListener(FaultEvent.FAULT, handleLoginFault);
					
					_webService.getVersions.resultFormat = "e4x";
					_webService.getVersions.addEventListener(ResultEvent.RESULT, handleVersionsResult);
					_webService.getVersions.addEventListener(FaultEvent.FAULT, handleFault);
				}
				
				_webService.disconnect();
				
				// load the WSDL so we can use the SOAP service
				_webService.loadWSDL();
			}
		}
		
		/**
		 * once the WSDL is loaded, send login information for build user so we can ensure the system has our current version. 
		 * @param e
		 * 
		 */
		private function handleLoad(e:LoadEvent):void
		{
			LogService.debug("FeedbackOperation: Success loading WSDL");
			LogService.debug("FeedbackOperation: Assertively requesting auth token.");
			_authToken = null;
			_webService.logout();
			_webService.setRemoteCredentials(Feedback.CONTROL_USERNAME, Feedback.CONTROL_PASSWORD);
			_webService.login.send(Feedback.CONTROL_USERNAME, Feedback.CONTROL_PASSWORD);
		}
		
		/**
		 * once the build user is logged in, get the list of versions for our project 
		 * @param e
		 * 
		 */
		private function handleInitialLogin(e:ResultEvent):void
		{
			_authToken = String(e.result);		
			LogService.debug("FeedbackOperation: Success logging in. authToken=" + _authToken);
			
			// BEGIN skip getVersions and potential addVersion. this has been causing a lot of trouble.
			_versionID = Feedback.DEFAULT_VERSION_ID;
			Feedback.instance.enabled = true;
			// END skip
			
			//_webService.getVersions.send(_authToken, _projectKey);
		}
		
		/**
		 * once we have the list of versions, look through the list for the version we're running.
		 * if it is in the list, enable the form and grab the version ID so we can submit tickets.
		 * if it isn't in the list, send a request to add the version we're running as. 
		 * @param e
		 * 
		 */
		private function handleVersionsResult(e:ResultEvent):void
		{
			LogService.debug("FeedbackOperation: Success loading versions.");
			_webService.getVersions.removeEventListener(ResultEvent.RESULT, handleVersionsResult);
			_webService.getVersions.removeEventListener(FaultEvent.FAULT, handleFault);
			var versionsList:XMLList = XMLList(e.result);
			
			for each (var versionRef:XML in versionsList) {
				if (_currentVersion.match(new RegExp("(^"+versionRef.name.text()+"$)", "i"))) {
					_versionID = versionRef.id.text();
					LogService.debug("FeedbackOperation: Version ID "+_versionID+" already found.");
					Feedback.instance.enabled = true;
					break;
				}
			}
			if (_versionID == null) {
				_webService.addVersion.resultFormat = "e4x";
				_webService.addVersion.addEventListener(ResultEvent.RESULT, handleAddVersionResult);
				_webService.addVersion.addEventListener(FaultEvent.FAULT, handleFault);
				_webService.addVersion.send(_authToken, _projectKey, {name: _currentVersion});
			}
		}		
		
		/**
		 * result from adding the version we're running to the project.
		 * enable the form and grab the version ID so we can submit tickets. 
		 * @param e
		 * 
		 */
		private function handleAddVersionResult(e:ResultEvent):void
		{
			_webService.removeVersion.addEventListener(ResultEvent.RESULT, handleAddVersionResult);
			_webService.removeVersion.addEventListener(FaultEvent.FAULT, handleFault);
			_versionID = XMLList(e.result)..id;
			LogService.debug("FeedbackOperation: Added Version ID "+_versionID+".");
			Feedback.instance.enabled = true;
		}
		
		override public function execute():void
		{
			_username = Feedback.instance.username;
			_webService.login.removeEventListener(ResultEvent.RESULT, handleInitialLogin);
			_webService.login.addEventListener(ResultEvent.RESULT, handleLogin);
			Feedback.instance.pending = true;
			LogService.debug("FeedbackOperation execute: Assertively refreshing auth token "+_authToken+" by logging out first.");
			_webService.disconnect();
			_webService.loadWSDL();
		}
		
		/**
		 * a login service fault. 
		 * @param e
		 * 
		 */
		private function handleLoginFault(event:FaultEvent):void
		{
			LogService.error("Feedback Login Fault: " + event.fault.toString());
			var errorEvent:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR, false, false, "There was an error logging onto the Feedback system.");
			var operationFaultEvent:OperationFaultEvent = new OperationFaultEvent(OperationFaultEvent.UNAUTHORIZED, errorEvent);
			handleFault(event, operationFaultEvent);
		}
		
		/**
		 * a general web service fault. 
		 * @param e
		 * 
		 */
		private function handleFault(faultEvent:FaultEvent, operationFaultEvent:OperationFaultEvent=null):void
		{
			if (!operationFaultEvent) {
				operationFaultEvent = new OperationFaultEvent(OperationFaultEvent.FAULT, 
					new ErrorEvent(ErrorEvent.ERROR, false, false, "There was an error accessing the Feedback system."))
				LogService.error("Feedback Fault: " + faultEvent.fault.toString());
			}
            if (faultEvent.target as Operation) {
                if (!reattempt(faultEvent.target as Operation)) {
                    dispatchEvent(operationFaultEvent);
                    displayFault(faultEvent, operationFaultEvent);
                    attempts = 0; // reset so we have 'n' tries again
                }
            }
		}
		
		private function reattempt(operation:Operation):Boolean
		{
			if (attempts > MAX_ATTEMPTS)
				return false; // we have already tried this
			
			var reattemptTimer:Timer = new Timer(OPERATION_REATTEMPT_DELAY, 1);
			reattemptTimer.addEventListener(TimerEvent.TIMER, function():void {
				LogService.error("Reattempting '"+operation.name+"' which encountered an error condition on attempt: " + attempts.toString());
				attempts++;
				if (operation.name == "login")
					operation.send(Feedback.CONTROL_USERNAME, Feedback.CONTROL_PASSWORD);
				else {
					if (attempts > 1) {
						_username = Feedback.DEFAULT_FEEDBACK_USERNAME;
					}
					sendCreateTicketOperation(operation);
				}
			});
			reattemptTimer.start();
			
			return true;
		}
		
		private function displayFault(faultEvent:FaultEvent, operationFaultEvent:OperationFaultEvent):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:51, closeFunction:handleFeedbackAlertDecision}));
		}
		
		private function handleFeedbackAlertDecision(faultEvent:FaultEvent):Function
		{	
			return function(event:CloseEvent):void {
				var operation:Operation = faultEvent.target as Operation;
				switch (event.detail) {
					case Alert.FIRST:
						var emailOperation:EmailOperation = new EmailOperation();
						emailOperation.addressTo = "support@zebrazapps.com";
						emailOperation.subject = "[Zebra Feedback] ";
						if (operation.name == CREATE_ISSUE_OPERATION) {
							emailOperation.subject += Feedback.instance.summary;
							emailOperation.body = _description + "\n\n\n---\nRaw ticket contents:\n" +operation.request + "\n---";
						}
						emailOperation.body += "\nLog Contents:\n" + LogService.dump;
						emailOperation.execute();
						break;
					default:
						// no action
						break;
				}
			}
		}
		
		/**
		 * login using the entered parameters succeeded, so create the ticket. 
		 * @param e
		 * 
		 */
		private function handleLogin(e:ResultEvent=null):void
		{
			if (Feedback.instance.username != Feedback.DEFAULT_FEEDBACK_USERNAME) { // if we've used a specific username
				// encrypt and store the username and password to save cross-session
				var bytes:ByteArray = new ByteArray();
				bytes.writeUTFBytes(Feedback.instance.username);
				TaconiteFactory.getLocalStoreImplementation().setItem("qaUsername", bytes);
				bytes.clear();
				bytes.writeUTFBytes(Feedback.instance.password);
				TaconiteFactory.getLocalStoreImplementation().setItem("qaPassword", bytes);
			}
			if (!_authToken && e)
				_authToken = String(e.result);
			
			LogService.debug("LogService: trying to createIssue with token " + _authToken);
			
			var affectsVersions:XML = <multiRef xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
						 <id xsi:type="xsd:string">{_versionID}</id>
					  </multiRef>;
			
			var operation:Operation = _webService.getOperation(CREATE_ISSUE_OPERATION) as Operation;
			operation.resultFormat = "e4x";
			operation.addEventListener(ResultEvent.RESULT, issueCreated);
			operation.addEventListener(FaultEvent.FAULT, handleFault);
			
			_description = Feedback.instance.description 
				+ "\n\n[ Logged by user " + TaconiteFactory.getEnvironmentImplementation().username;
			
			var document:Document = Application.instance.document;
			if (TaconiteFactory.getEnvironmentImplementation().email.length > 0 && TaconiteFactory.getEnvironmentImplementation().email != "undefined")
				_description += " <" + TaconiteFactory.getEnvironmentImplementation().email + "> ";
			_description += " on system " + TaconiteFactory.getEnvironmentImplementation().serverName;
			_description += "\nEnvironment details: " + TaconiteFactory.getEnvironmentImplementation().containerDetails + " ]";
            _description += "\n[ Current document: " + document.name + " ]";
            _description += "\n[ Current document ID: " + document.project.projectId + " ]";
            var flow:EventFlow = AuthorController.instance.eventFlow;
            if (flow) {
                _description += "\n[ CurrentEvent: " + flow.currentEventPage + "EditingEvent: " + flow.editingEventPage + " ]";
            }
            if (Feedback.instance.disclosurePleaseContactMe 
            		|| Feedback.instance.disclosureCodeReviewAllowed) {
				_description += "\n\n[ Requested personal contact: ";
				_description += (Feedback.instance.disclosurePleaseContactMe ? "YES" : "NO") + " ]";
				_description += "\n[ Code review is allowed: ";
				_description += (Feedback.instance.disclosureCodeReviewAllowed ? "YES" : "NO") + " ]";
			}

			// ensure required fields are accounted for
			if (Feedback.instance.summary == null || !/^[\w\s]{2}/.exec(Feedback.instance.summary))
				Feedback.instance.summary = "Invalid Summary Given";

			sendCreateTicketOperation(operation);
		}

		private function sendCreateTicketOperation(operation:Operation):void
		{
			operation.request = <createIssue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://soap.rpc.jira.atlassian.com" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
						<in0 xsi:type="xsd:string">{_authToken}</in0>
						<in1 xsi:type="bean:RemoteIssue" xmlns:bean="http://beans.soap.rpc.jira.atlassian.com">
							<assignee xsi:type="xsd:string">{Feedback.instance.assignee}</assignee>
							<description xsi:type="xsd:string">{_description}</description>
							<project xsi:type="xsd:string">{Feedback.instance.project}</project>
							<priority xsi:type="xsd:string">{Feedback.instance.priorityIndex}</priority>
							<reporter xsi:type="xsd:string">{_username}</reporter>
							<summary xsi:type="xsd:string">{Feedback.instance.summary}</summary>
							<type xsi:type="xsd:string">{Feedback.instance.typeIndex}</type>
							<customFieldValues xsi:type="jir:ArrayOf_tns1_RemoteCustomFieldValue" soapenc:arrayType="bean:RemoteCustomFieldValue[]" xmlns:jir="http://gbas-01566:8080/jira/rpc/soap/jirasoapservice-v2">
								<item>								
									<customfieldId xsi:type="xsd:string">customfield_10023</customfieldId>
									<values xsi:type="xsd:string">10291</values>
								</item>
								<item>
									<customfieldId xsi:type="xsd:string">customfield_10061</customfieldId>
									<values xsi:type="xsd:string">{Feedback.instance.associatedFeatureId}</values>
								</item>
								<item>
									<customfieldId xsi:type="xsd:string">customfield_10050</customfieldId>
									<values xsi:type="xsd:string">{Feedback.instance.featureImportance}</values>
								</item>
								<item>
									<customfieldId xsi:type="xsd:string">customfield_10052</customfieldId>
									<values xsi:type="xsd:string">{TaconiteFactory.getEnvironmentImplementation().email}</values>
								</item>
								<item>
									<customfieldId xsi:type="xsd:string">customfield_10051</customfieldId>
									<values xsi:type="xsd:string">{TaconiteFactory.getEnvironmentImplementation().username}</values>
								</item>
							</customFieldValues>
							</in1>
					  </createIssue>;
			operation.send();
		}
		
		private function issueCreated(e:ResultEvent):void
		{
			var operation:Operation = e.target as Operation;
			operation.removeEventListener(ResultEvent.RESULT, issueCreated);
			operation.removeEventListener(FaultEvent.FAULT, handleFault);
			operation = null;
			_ticketKey = XMLList(e.result)..key[0];
			validateTicket();
		}
		
		private function validateTicket():void
		{
			var operation:Operation = _webService.getOperation("getIssue") as Operation;
			operation.resultFormat = "e4x";
			operation.addEventListener(ResultEvent.RESULT, handleResult);
			operation.addEventListener(FaultEvent.FAULT, ticketNotAvailable);
			operation.send(_authToken, _ticketKey);
		}
		
		private function ticketNotAvailable(event:FaultEvent):void
		{
			validateTicket();
		}
		
		/**
		 * handle the successful ticket submission and send any requested attachments.
		 * @param e
		 * 
		 */
		private function handleResult(e:ResultEvent):void
		{
			var attachmentNamesXML:XMLList = new XMLList();
			var attachmentNames:Array = [];
			var attachmentContentsXML:XMLList = new XMLList();
			var attachmentContents:Array = [];
			var encoder:Base64Encoder = new Base64Encoder();
			var i:int;
			
			if (Feedback.instance.toggleAttachProject) {
				var projectDump:String = ProjectController.instance.projectDump;
				if (projectDump != null) {
					encoder.encode(ProjectController.instance.projectDump);
					attachmentNames.push(_ticketKey + "." + ProjectController.DEFAULT_EXTENSION);
					attachmentContents.push(encoder.toString());
					encoder.reset();
				}
			}
			
			if (Feedback.instance.toggleAttachLog && LogService.dump.length > 0) {
				encoder.encode(LogService.dump);
				
				attachmentNames.push(_ticketKey + ".log");
				attachmentContents.push(encoder.toString());
				encoder.reset();
			}
			
			if (Feedback.instance.screenShots.length > 0) {
				var counter:uint = 0;
				var screenShotCount:int = Feedback.instance.screenShots.length;
				for (i = 0 ; i < screenShotCount ; i++) {
					var data:ByteArray = new PNGEncoder().encode(Feedback.instance.screenShots[i]);
					encoder.encodeBytes(data);
					
					attachmentNames.push(_ticketKey + " - " + (i+1).toString() + ".png");
					attachmentContents.push(encoder.flush());
					encoder.reset();
				}
			}
			
			for (i = 0 ; i < attachmentNames.length ; i++) {
				attachmentNamesXML += <filename>{attachmentNames[i]}</filename>;
				attachmentContentsXML += <file>{attachmentContents[i]}</file>;
			}
			
			if (attachmentNames.length > 0) {
				var operation:Operation = _webService.getOperation("addBase64EncodedAttachmentsToIssue") as Operation;
				operation.addEventListener(ResultEvent.RESULT, handleAttachmentResult);
				operation.addEventListener(FaultEvent.FAULT, handleFault);
				operation.request = <addBase64EncodedAttachmentsToIssue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://soap.rpc.jira.atlassian.com" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
							 <in0 xsi:type="xsd:string">{_authToken}</in0>
							 <in1 xsi:type="xsd:string">{_ticketKey}</in1>
							 <in2 xsi:type="jir:ArrayOf_xsd_string" soapenc:arrayType="xsd:string[]" xmlns:jir="http://jira.alleni.com/jira/rpc/soap/jirasoapservice-v2">{attachmentNamesXML}</in2>
							 <in3 xsi:type="jir:ArrayOf_xsd_string" soapenc:arrayType="xsd:string[]" xmlns:jir="http://jira.alleni.com/jira/rpc/soap/jirasoapservice-v2">{attachmentContentsXML}</in3>
						  </addBase64EncodedAttachmentsToIssue>;
				operation.send();
			} else
				Feedback.instance.pending = false;
		}
		
		/**
		 * handle successful submission of attachments to the ticket. 
		 * @param e
		 * 
		 */
		private function handleAttachmentResult(e:ResultEvent):void
		{
			LogService.debug("Attachment RESULT: " + XMLList(e.result));
			
			// attachment succeeded so we can clear out the screenshots.
			var i:int;
			var screenShotCount:int = Feedback.instance.screenShots.length;
			for (i = 0 ; i < screenShotCount ; i++)
				Feedback.instance.screenShots[i].dispose();
			Feedback.instance.screenShots.splice(0, screenShotCount);
			
			Feedback.instance.pending = false;
		}
		
		/**
		 * enable or disable screenshot feature 
		 * @param e
		 * 
		 */
		public function refreshScreenshot():void
		{
			/* todo: update for vector of screenshots 
			screenshot; 
				
			var bmd:BitmapData = null;
			var newHeight:Number = 25;
			var newWidth:Number = 25 * _bmpd.width / _bmpd.height;
				
			bmd = new BitmapData(newWidth, newHeight);
			var wscale:Number = newWidth / _bmpd.width;
			var hscale:Number = newHeight / _bmpd.height;
				
			var trans:Matrix = new Matrix();
			trans.scale(wscale, hscale);
			bmd.draw(_bmpd, trans, null, null, null, true);
			Feedback.instance.screenshotThumb = new Bitmap(bmd);*/
		}
		
		public function pushScreenshot():void
		{
			Feedback.instance.screenShots.push(screenshot);
		}
		
		private function get screenshot():BitmapData
		{
			var w:uint = TaconiteFactory.getAppDescriptorImplementation().screenBounds.width;
			var h:uint = TaconiteFactory.getAppDescriptorImplementation().screenBounds.height;

			var bmd:BitmapData = new BitmapData(w, h, false, 0x000000);
			bmd.perlinNoise(500, 500, 1, Math.random() * 100, false, true, 6, true);

			if (ApplicationController.instance.diagramWindowVisible) {
				layerScreenshot(bmd, ApplicationController.instance.diagramWindowController.displayObject,
					ApplicationController.instance.diagramWindowController.originX,
					ApplicationController.instance.diagramWindowController.originY);
			}
			
			FeedbackController.instance.setVisible(false);
			layerScreenshot(bmd, Application.instance.window.displayObject.stage,
				Application.instance.window.originX,
				Application.instance.window.originY);
			FeedbackController.instance.setVisible(true);
			
			if (Feedback.instance.toggleVellum && QAVellumSettings.vellum.displayObject)
				layerScreenshot(bmd, QAVellumSettings.vellum.displayObject);

			return bmd;
		}
		
		private function layerScreenshot(bmd:BitmapData, drw:DisplayObject, x:Number=0, y:Number=0):void
		{
			var w:uint = TaconiteFactory.getAppDescriptorImplementation().screenBounds.width;
			var h:uint = TaconiteFactory.getAppDescriptorImplementation().screenBounds.height;
			var pt:Point = new Point(x, y);
			
			if (!bmd || (bmd.width < w || bmd.height < h)) {
				bmd = new BitmapData(w, h, false, 0x000000);
				bmd.perlinNoise(500, 500, 1, Math.random() * 100, false, true, 6, true);
			}

            try {
                var layerBmpd:BitmapData = ViewUtils.instance.takeScreenshot(drw, w, h);
                var rect:Rectangle = new Rectangle(0, 0, w, h);
                bmd.copyPixels(layerBmpd, rect, pt);
            } catch (e:Error) {
                LogService.error("Error taking screenshot: "+e.message);
            }
		}
	}
}
