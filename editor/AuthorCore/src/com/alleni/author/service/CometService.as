package com.alleni.author.service {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.DataReceivedEvent;
import com.alleni.author.document.Document;
import com.alleni.taconite.controller.NotificationController;
import com.alleni.taconite.factory.TaconiteFactory;

import com.alleni.taconite.service.LogService;

import flash.events.EventDispatcher;

import mx.utils.UIDUtil;

import org.cometd.CometClient;

public class CometService extends EventDispatcher {

	private var _client:CometClient;
	protected var notificationController:NotificationController;

	private static var _instance:CometService = null;
	public static function get instance():CometService {
		if(_instance == null) {
			_instance = new CometService();
		}
		return _instance;
	}


	public function CometService() {
		_client = new CometClient();
		_client.cometURL = TaconiteFactory.getEnvironmentImplementation().apiURI + "comet";
		_client.addHttpFaultListener(httpFaultHandler)
		_client.debugHandler = debugMessageHandler;
		_client.init();
		notificationController = new NotificationController(this);
	}

	private function httpFaultHandler(arg:Object):void {
		trace(arg);
	}

	private static var dummyID:String = UIDUtil.createUID();

	public static function getChannel(key:String, projectSpecific:Boolean):String {
		if(projectSpecific) {
			var projectId:String = Document(ApplicationController.instance.authorController.document).documentId;
			if(projectId == null) {
				projectId = dummyID;
			}
			return "/" + projectId + "/" + key;
		} else {
			return "/global/" + key;
		}
	}

	public function publish(channel:String, data:Object):void {
		_client.publish(channel, data);
	}

	public static function disconnect():void {
		_instance._client.disconnect();
		_instance.notificationController.removeAllEvents();
		_instance = null;
	}

	override public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
		notificationController.addEventListener(type, listener, useCapture, priority, useWeakReference);
		super.addEventListener(type, listener, useCapture, priority, useWeakReference);
		_client.subscribe(type, function(data:Object):void {
			var channel:String = type;
			subscriptionHandler(channel, data);
		});
	}

	override public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
		if(hasEventListener(type) == false) {
			_client.unsubscribe(type);
		}
		notificationController.removeEventListener(type, listener, useCapture);
		super.removeEventListener(type, listener, useCapture);
	}

	private function subscriptionHandler(channel:String,  data:Object):void {
		if("data" in data) {
			dispatchEvent(new DataReceivedEvent(channel, data.data));
		}
	}

	private function debugMessageHandler(msg:String):void {
		LogService.debug(msg);
	}
}
}
