package com.alleni.author.service.tincan
{
	import mx.utils.Base64Encoder;

	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.author.document.Document;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.model.objects.LMS;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.taconite.event.ApplicationEvent;

	public class TinCanClient
	{
		private static const DEBUG_ENDPOINT:String = "http://cloud.scorm.com/ScormEngineInterface/TCAPI/public/";
		private static const DEBUG_AUTH:String = "Basic VGVzdFVzZXI6cGFzc3dvcmQ=";
		public static const DEFAULT_ENDPOINT:String = "https://watershed.ws/tc/";
		public static const DEFAULT_USERNAME:String = "tincan@zebrazapps.com";
		public static const DEFAULT_PASSWORD:String = "7emm79fw";
		public static const DEFAULT_EMAIL:String = "tincan@zebrazapps.com";
		public static const DEFAULT_ACTIVITY_TYPE:String = "interaction";
		public static const DEFAULT_VERB:String = "attempted";
		private static var _defaultAuth:String = null;
	
		private static const INITIALIZE_METHOD:String = "TCProtoClient_initialize";
		private static const SEND_STATEMENT_METHOD:String = "TCProtoClient_sendArbitraryStatement";

		private static var _instance:TinCanClient;
		private static var _environment:IEnvironment;

		function TinCanClient() {
			Utilities.assert(_instance == null);
		}
		
		private static function get instance():TinCanClient {
			if (_instance == null) {
				_instance = new TinCanClient();
			}
			return _instance;
		}

		private static function get defaultAuth():String {
			if (_defaultAuth == null) {
				_defaultAuth = getBasicAuthForCredentials(DEFAULT_USERNAME, DEFAULT_PASSWORD);
			}
			return _defaultAuth;
		}

		private static function get environment():IEnvironment {
			if (_environment == null) {
				_environment = TaconiteFactory.getEnvironmentImplementation();
				Utilities.assert(_environment != null);
				_environment.addExternalCallback("handleLRSResponse", handleLRSResponse);
			}
			return _environment;
		}

		private static function getBasicAuthForCredentials(username:String, password:String):String {
			var encoder:Base64Encoder = new Base64Encoder();
			encoder.insertNewLines = false;
			encoder.encode(username + ":" + password);
			return "Basic " + encoder.toString();
		}

		public static function initiateActivity(endpoint:String, username:String, password:String, 
				activityId:String, activityName:String, activityDescription:String, activityType:String,
				actorName:String, apiVersion:String=null):Object {
			var auth:String = getBasicAuthForCredentials(username, password);
			var lrs:Object = {endpoint:endpoint, auth:auth}; // {endpoint:DEBUG_ENDPOINT, auth:DEBUG_AUTH};
			var tcConfig:Object = {
				lrs:lrs,
				activityId:activityId,
				activityName:activityName,
				activityDescription:activityDescription,
				activityType:activityType,
				actorName:actorName,
				actorEmail:actorName.replace(/\s/g, "") + "-" + DEFAULT_EMAIL
			};

			if (apiVersion != null) {
				tcConfig["apiVersion"] = apiVersion;
			}

			return environment.callExternal(INITIALIZE_METHOD, tcConfig);
		}

		public static function formatStatementPreview(l:LMS):String {
			var preview = l.actor_name + " " + l.statement_verb + " '" + l.activity_name;

			if (l.statement_include_module) {
				preview += " module " + l.module_index;
			}
			preview += "'";

			if (l.statement_include_score) {
				var score:String;
				if (l.score_sendAs == "percent") {
					score = String(l.score_percent) + "%";
				} else {
					score = String(l.score) + " (range " + l.score_min + "-" + l.score_max + ")";
				}
				preview += " with score " + score;
			}

			if (l.statement_include_completion || l.statement_include_success) {
				var completionResult:String = "";
				if (l.result_completion) {
					completionResult = "completion was achieved";
				} else {
					completionResult = "completion was not achieved";
				}

				var successResult:String = "";
				if (l.result_success) {
					successResult = "success was achieved";
				} else {
					successResult = "success was not achieved";
				}

				preview += " ";
				if (l.statement_include_completion && !l.statement_include_success) {
					preview += completionResult;
				} else if (!l.statement_include_completion && l.statement_include_success) {
					preview += successResult;
				} else { // l.statement_include_completion && l.statement_include_success
					preview += completionResult + " and " + successResult;
				}
			}
			return preview;
		}

		public static function sendStatement(verb:String, moduleIndex:int=-1,
				scoreScaled:Number=-1, scoreRaw:Number=-1, scoreMin:Number=-1, scoreMax:Number=-1,
				success:Boolean=false, includeSuccess:Boolean=false,
				completion:Boolean=false, includeCompletion:Boolean=false,
				apiVersion:String=null):String {
			var statementObject:Object;

			statementObject = {verb:verb};
			if (moduleIndex > -1) {
				statementObject["moduleIndex"] = moduleIndex;
			}

			if (apiVersion != null) {
				statementObject["apiVersion"] = apiVersion;
			}

			var includeScore:Boolean = scoreScaled > -1 || scoreRaw > -1;
			if (includeScore || includeSuccess || includeCompletion) {
				var resultObject = {};
				if (includeScore) {
					var scoreObject = {};
					if (scoreScaled > -1) {
						scoreObject = {scaled:scoreScaled};
					} else {
						scoreObject = {raw:scoreRaw, min:scoreMin, max:scoreMax};
					}
					resultObject["score"] = scoreObject;
				}
				if (includeSuccess) {
					resultObject["success"] = success;
				}
				if (includeCompletion) {
					resultObject["completion"] = completion;
				}
				statementObject["result"] = resultObject;
			}

			var statement = environment.callExternal(SEND_STATEMENT_METHOD, statementObject);
			if (statement == null) {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.LRS_REQUEST_FAILED));
				return "";
			}
			return statement;
		}

        private static function handleLRSResponse(responseObj:Object):void {
        	var initializing:Boolean = "initialized" in responseObj;

        	if ("status" in responseObj) {
        		var status = responseObj.status;
        		delete responseObj.status;

        		LogService.info("handleLRSResponse LRS status: " + status);
        		if (initializing) {
	        		ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.LRS_INITIALIZED, responseObj));
        		} else {
	        		if (status >= 200 && status < 400) {
	        			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.LRS_REQUEST_SUCCEEDED, responseObj));
	        		} else {
	        			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.LRS_REQUEST_FAILED, responseObj));
	        		}
	        	}
        	} else {
        		LogService.info("handleLRSResponse LRS request failed. LRS response lacked status property.");
        		ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.LRS_REQUEST_FAILED, responseObj));
        	}
        }
	}
}