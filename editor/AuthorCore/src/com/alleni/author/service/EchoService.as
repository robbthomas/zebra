package com.alleni.author.service
{
    import com.alleni.author.application.ApplicationUI;
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.view.text.LightLabel;
    import com.alleni.taconite.dev.Utilities;
    import com.alleni.taconite.lang.TaconiteTimer;
	import com.alleni.taconite.service.RestHttpOperation;
    import flash.display.Sprite;
    import flash.events.Event;
	import flash.events.EventDispatcher;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

public class EchoService extends EventDispatcher
	{
		private static const KEEPALIVE_INTERVAL:int = 5 * 1000; // 5 seconds
        private static const TEST_RECONNECT_INTERVAL:int = 1000; // 1 second
		private static const SLEEP_THRESHOLD:int = KEEPALIVE_INTERVAL * 2;
        public static const INPUT_TOKEN:String = "Input Token";
		
		private static var _instance:EchoService;
		
		private static var _lastKeepAliveRequest:int;
		private static var _lastKeepAliveResponse:int;
		
		private static var _taconiteTimer:TaconiteTimer;
        private static var _timeoutTimer:Timer;
        private static var _disableToken:Object;

        private static var networkUnavailableMessage:Sprite;

		public function EchoService():void
		{
			Utilities.assert(!_instance);
		}
		
		public static function get instance():EchoService
		{
			if (!_instance){
                _instance = new EchoService();
            }
			return _instance;
		}
		
		public static function start():void
		{
			instance.startTimer();
		}
		
		protected function startTimer():void
		{
			_taconiteTimer = TaconiteTimer.instance;
			_taconiteTimer.registerListener(updateTimer);
			_lastKeepAliveRequest = _taconiteTimer.milliseconds;
			updateTimer(null);
		}

		public static function stop():void
		{
			instance.stopTimer();
		}
		
		protected function stopTimer():void
		{
			_taconiteTimer.unregisterListener(updateTimer);
		}
	
		protected function updateTimer(event:TimerEvent):Boolean
		{
			var dueForEcho:Boolean = false;
			var interval:int = RestHttpOperation.networkAvailable?KEEPALIVE_INTERVAL:TEST_RECONNECT_INTERVAL;

			if (!event || _taconiteTimer.milliseconds > _lastKeepAliveRequest + interval) {
				dueForEcho = true;
//				trace("#%%%%#%#%#%#%#%%%%#%#%#% The network is in use right now:",RestHttpOperation.networkInUse);
				if ((_taconiteTimer.milliseconds - _lastKeepAliveRequest) > SLEEP_THRESHOLD)
					dueForEcho = false; // must have been sleeping, don't bother as network interface is probably not up yet.

                _lastKeepAliveRequest = _taconiteTimer.milliseconds;

//				if (!dueForEcho || RestHttpOperation.networkInUse == true)
//					return false;
                if (!dueForEcho)
					return dueForEcho;

				const epoch:Number = new Date().time; // for latency calculations
				
				var query:Object = {timestamp:epoch};
				query.message = "hello";
				
				var operation:RestHttpOperation = new RestHttpOperation("echo", query, RestHttpOperation.POST);
				operation.displayName = "Phone Home";
				operation.reattemptOnFault = false;
				BorealOperation.addListeners(operation, handleResponse);
//                _timeoutTimer = new Timer(4 * 1000, 1);
//                _timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, handleTimeout);
//                _timeoutTimer.start();
//                trace("#%%%%#%#%#%#%#%%%%#%#%#% Echo Timeout Timer Started");
				operation.execute();
			}
			return dueForEcho;
		}

        private function handleTimeout(event:TimerEvent){
            RestHttpOperation.networkAvailable = false;
            connectionNotAvailable();
//            trace("#%%%%#%#%#%#%#%%%%#%#%#% Echo Timeout Timer Ended, echo timed out");
        }

		private function handleResponse(e:Event):void
		{
            var operation:RestHttpOperation = e.target as RestHttpOperation;
			
			if (!operation) return;
            if(_timeoutTimer != null){
                _timeoutTimer.stop();
                _timeoutTimer = null;
            }
//            trace("#%%%%#%#%#%#%#%%%%#%#%#% Echo Timeout Timer Cleared, echo successful");
            RestHttpOperation.networkAvailable = true;
            BorealOperation.showNetworkBadMessage = true;
            connectionAvailable();
			const response:Object = operation.resultDecoded;
			
			_lastKeepAliveResponse = _taconiteTimer.milliseconds;
		}

        private static function buildNetworkUnavailableMessage():void{
            if(networkUnavailableMessage == null){
                networkUnavailableMessage = new Sprite();
                networkUnavailableMessage.graphics.clear();
                networkUnavailableMessage.graphics.beginFill(0x000000, .80);
                networkUnavailableMessage.graphics.drawRect(0, 0, ApplicationUI.instance.width + 300, ApplicationUI.instance.height + 300);
                networkUnavailableMessage.graphics.endFill();
                var messageText:LightLabel = new LightLabel(200, 0xFFFFFF, true, 150);
                messageText.text = "You have lost network connection, please restore it to contiune.\nBETTER WORDING ON ITS WAY!"
                messageText.size = 18;
                messageText.bold = true;
                messageText.x = (ApplicationUI.instance.width - 200)/2;
                messageText.y = (ApplicationUI.instance.height - 150)/2;
                networkUnavailableMessage.addChild(messageText);
            }
        }

        public static function connectionNotAvailable():void{
            //trace("#%%%%#%#%#%#%#%%%%#%#%#% Connection not available");
            if(ApplicationController.instance.disabledTokenInDict(_disableToken) == false){
                _disableToken = ApplicationController.instance.disableInput(INPUT_TOKEN);
                buildNetworkUnavailableMessage();
                ApplicationUI.instance.stage.addChildAt(networkUnavailableMessage, ApplicationUI.instance.stage.numChildren)
                ApplicationUI.instance.mouseEnabled = false;
                ApplicationUI.instance.mouseChildren = false;
            }
        }

        public static function connectionAvailable():void{
            //trace("#%%%%#%#%#%#%#%%%%#%#%#% Connection is available");
            if(ApplicationController.instance.disabledTokenInDict(_disableToken)){
                ApplicationController.instance.enableInput(_disableToken);
                if(ApplicationUI.instance.stage.contains(networkUnavailableMessage)){
                    ApplicationUI.instance.stage.removeChild(networkUnavailableMessage);
                }
                ApplicationUI.instance.mouseEnabled = true;
                ApplicationUI.instance.mouseChildren = true;
                networkUnavailableMessage = null;
            }
        }
	}
}