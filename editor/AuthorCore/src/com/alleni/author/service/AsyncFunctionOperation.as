/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 6/15/12
 * Time: 4:34 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.service {
import com.alleni.taconite.service.AbstractOperation;

import flash.events.Event;

public class AsyncFunctionOperation extends AbstractOperation {
    private var _f:Function;


    /**
     * Operation to be used with an OperationQueue.
     * Performs an operation defined by the function parameter passed in on this constructor.
     * The next operation in the queue will not execute until after signalCompletion() has been called.
     * @param displayName
     * @param function(signalCompletion:Function):void
     */
    public function AsyncFunctionOperation(displayName:String, f:Function) {
        this.displayName = displayName;
        _f = f;
    }

    override public function execute():void {
//        trace("AsyncFunctionOperation:"+displayName)
        _f(signalCompletion);
    }

    private function signalCompletion():void
    {
        this.dispatchEvent(new Event(Event.COMPLETE));
    }
}
}
