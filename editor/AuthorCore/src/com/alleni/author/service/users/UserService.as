package com.alleni.author.service.users
{
import com.alleni.author.controller.app.FontController;
import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.GadgetType;
    import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.GadgetEvent;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Dock;
	import com.alleni.author.service.EchoService;
    import com.alleni.author.service.RestPipeline;
    import com.alleni.author.service.project.ProjectListOperation;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	
	public class UserService extends EchoService
	{
		public static const USER_COLLECTOR:String = "collector";
        public static const USER_CREATOR:String = "creator";
        public static const USER_PRO:String = "pro";
        public static const USER_ENTERPRISE:String = "TBD";

        private static var _instance:UserService;
		private static var _metaDataLoaded:Boolean = false;
		private static var _userPublishedOperation:ProjectListOperation;
		private static var _userMetadataOperation:RestHttpOperation;

        private static var _stalledApplicationEvent:ApplicationEvent;


        public static function get metaDataLoaded():Boolean {
            return _metaDataLoaded;
        }

        public static function set stalledApplicationEvent(value:ApplicationEvent):void {
            _stalledApplicationEvent = value;
        }

        public function UserService()
		{
			super();
		}
		
		public static function get instance():UserService
		{
			if (!_instance) _instance = new UserService();
			return _instance;
		}
		
		public static function start():void
		{
			instance.startTimer();
		}
		
		override protected function updateTimer(event:TimerEvent):Boolean
		{
			if (!super.updateTimer(event)) return false;

			if (_metaDataLoaded) return false;

			if (!_userMetadataOperation) {
				_userMetadataOperation = new RestHttpOperation("user/metadata");
				_userMetadataOperation.displayName = "User Information";
				_userMetadataOperation.reattemptOnFault = true;
			}
			RestPipeline.push(_userMetadataOperation, handleResponse, null, true);
			return true;
		}
		
		private function cleanUpUserList(e:Event=null):void
		{
			_userPublishedOperation = null;
		}
		
		private function handleResponse(e:Event):void
		{
			var operation:RestHttpOperation = e.target as RestHttpOperation;
			
			if (!operation) return;
			
			const response:Object = operation.resultDecoded;
			const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			
			if (!environment.accountId && "accountId" in response)
				environment.accountId = response.accountId;
			
			if (!environment.userId && "userId" in response)
				environment.userId = response.userId;
			
			if ("accountName" in response)
				environment.accountName = response.accountName;
			
			if ("companyName" in response)
				environment.companyName = response.companyName;
			
			if ("fullName" in response)
				environment.userFullName = response.fullName;
			
			if ("email" in response)
				environment.email = response.email;

			if ("isAdmin" in response) {
				environment.isAdmin = (response.isAdmin == true);
				Application.instance.isAdmin = environment.isAdmin;
			}

            if ("customerType" in response){
                environment.customerType = response.customerType;
                Application.instance.customerType = environment.customerType;
				dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UPDATE_CUSTOMER_TYPE));
                if (!ApplicationController.instance.disabledInput) {
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
                }
            }

			_metaDataLoaded = true; // assume everything was as needed. in the event we wish to continue polling, add response validation

            FontController.instance;

            if(_stalledApplicationEvent) {
                var evt:ApplicationEvent = _stalledApplicationEvent;
                _stalledApplicationEvent = null;
                ApplicationController.instance.dispatchEvent(evt);
            }
		}
	}
}