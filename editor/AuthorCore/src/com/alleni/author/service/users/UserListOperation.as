package com.alleni.author.service.users
{
	import com.alleni.author.model.ui.User;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import com.alleni.author.service.BorealOperation;
	
	public class UserListOperation extends AbstractOperation
	{
		private var _resultList:Array;
		
		public function UserListOperation()
		{
			super();
		}
		
		override public function execute():void
		{
			var operation:RestHttpOperation = new RestHttpOperation("user");
			operation.displayName = "Loading user list";
			operation.method = RestHttpOperation.GET;
			BorealOperation.addListeners(operation, handleComplete);
			operation.execute();
		}
		
		override protected function handleComplete(e:Event):void
		{
			_resultList = RestHttpOperation(e.target).resultDecoded as Array;
			
			super.handleComplete(e);
		}
	
		public function get resultList():Array
		{
			return _resultList;
		}
		
		public function get listForUI():Array
		{
			var arrayOfLabels:Array = [];
			
			for each (var object:Object in _resultList) {
				var username:String = object.firstName + " " + object.lastName;
				if (!username || /^\s+$/.test(username)) username = object.username;
				else username += " <" + object.email + ">";
				var lastName:String = (!object.lastName || /^\s+$/.test(object.lastName)) ? username : object.lastName;
				arrayOfLabels.push({label:username, id:object.id, lastName:lastName});
			}
			arrayOfLabels.sortOn("lastName", Array.CASEINSENSITIVE);
			
			return arrayOfLabels;
		}
		
		public function get users():Array
		{
			var users:Array = []
			
			var user:User;
			for each (var userObject:Object in _resultList) {
				user = new User();
				if ("dateCreated" in userObject) user.dateCreated = new Date(userObject.dateCreated);
				if ("description" in userObject) user.description = userObject.description;
				if ("email" in userObject) user.email = userObject.email;
				if ("emailShow" in userObject) user.emailShow = userObject.emailShow;
				if ("enabled" in userObject) user.enabled = userObject.enabled;
				if ("id" in userObject) user.id = userObject.id;
				if ("username" in userObject) user.username = userObject.username;
				if ("lastUpdated" in userObject) user.lastUpdated = new Date(userObject.lastUpdated);
				if ("userRealName" in userObject) user.userRealName = userObject.userRealName;
				users.push(user);
			}
			return users;
		}
	}
}