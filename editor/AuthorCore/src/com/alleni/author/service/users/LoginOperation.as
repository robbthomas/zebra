package com.alleni.author.service.users
{
	import com.alleni.author.service.BorealOperation;
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.Event;
	
	import mx.utils.Base64Encoder;
	
	public class LoginOperation extends AbstractOperation
	{
		
		public function setCredentials(username:String, password:String, server:String):void
		{
			if (username != null) {
				LogService.error("LoginOperation: user="+username + " server="+server);
				trace("LoginOperation: user="+username + " server="+server);
				var env:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
				env.username = username;
				env.password = password;
				env.serverName = server;
				LogService.error("  environ: user="+env.username + " server="+env.serverName + " api="+env.apiURI);
				trace("  environ: user="+env.username +  " server="+env.serverName + " api="+env.apiURI);
			}
		}
		
		override public function execute():void
		{
			this.attempts++;
			var operation:RestHttpOperation = new RestHttpOperation("echo",
				{message:"login"},
				RestHttpOperation.POST);
			this.displayName = "User Login";
			operation.displayName = this.displayName;
			operation.reattemptOnFault = false;
			operation.addEventListener(OperationFaultEvent.UNAUTHORIZED, handleUnauthorized, false, 10);
			BorealOperation.addListeners(operation, handleResult, function(e:Event):void{handleError(null)});
			operation.execute();
		}
		
		private function handleUnauthorized(event:OperationFaultEvent):void
		{
			event.stopImmediatePropagation();
			event.stopPropagation();
			
			var password:String = TaconiteFactory.getEnvironmentImplementation().password;
			var encodedPassword:String = "";
			
			if (password != null) {
				var encoder:Base64Encoder = new Base64Encoder();
				encoder.encodeUTFBytes(password);
				encodedPassword = encoder.flush();
			}
			LogService.error("Failure to log in for " + TaconiteFactory.getEnvironmentImplementation().username + " ("+encodedPassword+")");
			handleError(null);
		}

		private function handleResult(event:Event):void
		{
			var operation:RestHttpOperation = RestHttpOperation(event.target);
			
			if (operation) {
				dispatchEvent(new Event(Event.COMPLETE));
				LogService.debug("Login success for " + TaconiteFactory.getEnvironmentImplementation().username);
			}
		}
	}
}