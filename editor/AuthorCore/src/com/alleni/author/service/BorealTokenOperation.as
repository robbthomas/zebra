package com.alleni.author.service
{
	import com.alleni.author.model.BorealToken;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;

	public class BorealTokenOperation extends AbstractOperation
	{
		private var _token:BorealToken;
		
		public function BorealTokenOperation()
		{
		}
		
		override public function execute():void
		{
			var operation:RestHttpOperation = new RestHttpOperation("auth/session/token", null, RestHttpOperation.POST);
			operation.displayName = "Request Security Token";
			BorealOperation.addListeners(operation, handleComplete);
			operation.execute();
		}
		
		override protected function handleComplete(e:Event):void
		{
			var operation:RestHttpOperation = RestHttpOperation(e.target);
			
			if (operation) {
				if (operation.resultDecoded.hasOwnProperty("token"))
					_token = new BorealToken(operation.resultDecoded["token"], 120*60*1000); // 120 minutes is the default timeout (in milliseconds)
				
				if (_token.content.length > 0) {
					super.handleComplete(e)
						
					// TODO: delete token as needed, and handle an HTTP 400 response when we need to request a new token
				} else // feedback token failure to user
					handleError(new ErrorEvent(ErrorEvent.ERROR, false, false, this.displayName + ": Failure to fetch security token for network access."));
			}		
		}
		
		override public function get result():*
		{
			return _token;
		}
	}
}