package com.alleni.author.service
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.app.SupportUtilities;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.service.CommandOperation;
	
	public class CommandOperation extends com.alleni.taconite.service.CommandOperation
	{
		protected const AU:RegExp = 				/^au\s([\w\/]+)/i;
		protected const EGG:RegExp = 				/^egg/i;
		
		protected const AU_DUMP_MODELS:RegExp = 	/dumpModels/i;
		protected const AU_DUMP_OBJECTS:RegExp = 	/dumpObjects/i;
		protected const AU_DUMP_SELECTION:RegExp = 	/dumpSelection/i;
		protected const AU_DUMP_VIEWS:RegExp = 		/dumpViews/i;
		protected const AU_DUMP_CONTAINERS:RegExp = /dumpContainers/i;
		protected const AU_DUMP_UNDO:RegExp = 		/dumpUndo/i;
		
		protected const SYNCHRONOUS_HELP_OUTPUT:String = "au [dumpModels|dumpObjects|dumpSelection|dumpViews|dumpContainers|dumpUndo]";
		
		public function CommandOperation(cmd:String)
		{
			super(cmd);
		}
		
		public static function get COMMAND_INLINE_REGEXP():String
		{
			return com.alleni.taconite.service.CommandOperation.COMMAND_INLINE_REGEXP;
		}
		
		override protected function synchronousCommand(cmd:String):String
		{
			if (HELP.exec(cmd)) {
				return HELP_OUTPUT + ", " + SYNCHRONOUS_HELP_OUTPUT;
			} else if (AU.exec(cmd)) {
				var action:String = AU.exec(cmd)[1];
				return executeAuAction(action);
			} else if (EGG.exec(cmd)) {
				ApplicationController.instance.authorController.addObjectForName("CoolCircle", Math.random()*800, Math.random()*600);
				ApplicationController.instance.authorController.addObjectForName("SpinnySquare", Math.random()*800, Math.random()*600);
				return "you found it!";
			} else
				return "";
		}
		
		private function executeAuAction(action:String):String
		{
			var buffer:String = "";
			if (AU_DUMP_MODELS.exec(action)) {
				buffer += SupportUtilities.dumpModels();
			} else if (AU_DUMP_OBJECTS.exec(action)) {
				buffer += SupportUtilities.dumpObjects();
			} else if (AU_DUMP_SELECTION.exec(action)) {
				buffer += SupportUtilities.dumpSelection();
			} else if (AU_DUMP_VIEWS.exec(action)) {
				buffer += WorldContainer(Application.instance.viewContext).worldView.dumpViews();
			} else if (AU_DUMP_CONTAINERS.exec(action)) {
				buffer += Utilities.dumpDisplayObjectContainer(Application.instance.viewContext.parent, "ApplicationUI");
			} else if (AU_DUMP_UNDO.exec(action)) {
				//buffer += Application.instance.document.undoHistory.dump();
			}
			return buffer;
		}
	}
}