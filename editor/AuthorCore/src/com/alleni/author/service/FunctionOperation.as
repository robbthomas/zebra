/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 6/15/12
 * Time: 4:34 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.service {
import com.alleni.taconite.service.AbstractOperation;

public class FunctionOperation extends AbstractOperation {
    private var _f:Function;
    private var _thisArg:*;
    private var _argArray:*;
    public function FunctionOperation(displayName:String, f:Function, thisArg:* = null,  argArray:* = null) {
        this.displayName = displayName;
        _f = f;
        _thisArg = thisArg;
        _argArray = argArray;
    }

    override public function execute():void {
        trace("FunctionOperation:"+displayName)
        _f.apply(_thisArg, _argArray);
        handleComplete(null);
    }
}
}
