package com.alleni.author.service
{
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.Event;

	public class RestPipeline extends LoadingPipeline
	{
		public function RestPipeline()
		{
			super();
		}
		
		public static function push(operation:RestHttpOperation, completeHandler:Function, faultPrehandler:Function=null, onlyExecuteOneAtATime:Boolean=false):void
		{
			BorealOperation.addListeners(operation, function(event:Event):void {
				completeHandler(event);
				handleOperationComplete(operation);
			}, faultPrehandler);
			//operation.addEventListener(ProgressEvent.PROGRESS, handleProgress);
			if (!pushOrWait(operation, onlyExecuteOneAtATime)) // whether or not this ended up on the queue. if not, we need to clean up.
				BorealOperation.removeListeners(operation);
		}
		
		/*override protected function clearAndPushNext(operation:IOperation):void
		{
			super.clearAndPushNext(operation);
			operation.removeEventListener(ProgressEvent.PROGRESS, handleProgress);
		}*/
	}
}