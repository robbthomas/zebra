package com.alleni.author.service
{
	import com.alleni.taconite.model.ModelRoot;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.service.TaconiteDocumentData;
	
	public class DocumentData extends TaconiteDocumentData
	{
		public var wiringRoot:ModelRoot;
		public var assetsRoot:ModelRoot;
		
		public function DocumentData(root:TaconiteModel, wiringRoot:ModelRoot, assetsRoot:ModelRoot, documentId:String)
		{
			super(root, documentId);
			this.wiringRoot = wiringRoot;
			this.assetsRoot = assetsRoot;
		}
	}
}