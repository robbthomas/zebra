package com.alleni.author.service
{
	import com.alleni.author.service.assets.AssetOperation;
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.service.IOperation;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	// TODO: cache thumbnails so we return a copy of the cached bits instead of honoring a new request.
	public class ThumbnailLoadingPipeline extends LoadingPipeline
	{
		private static var _environment:IEnvironment;
		
		public function ThumbnailLoadingPipeline()
		{
			super();
		}
		
		private static function get environment():IEnvironment
		{
			if (!_environment)
				_environment = TaconiteFactory.getEnvironmentImplementation();
			return _environment;
		}
		
		public static function push(url:String, completeHandler:Function):Loader
		{
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, completeHandler);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleError);
			loader.load(new URLRequest(url), environment.loaderContext);
			return loader;
		}
		
		public static function pushAsset(id:String, name:String, completeHandler:Function):Loader
		{
			var operation:RestHttpOperation = AssetOperation.getThumbnailOperation(id, name, handleNotReady);
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, completeHandler);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleError);
			BorealOperation.addListeners(operation, function(event:Event):void {
				var resultOperation:IOperation = IOperation(event.target);
				loader.loadBytes(resultOperation.result);
				handleOperationComplete(resultOperation);
			}, handleUnrecoverableError);
			pushOrWait(operation);
			return loader;
		}
		
		private static function handleError(e:IOErrorEvent):void
		{
			LogService.debug("Thumbnail IOError: " + e);
		}
		
		private static function handleNotReady(operation:IOperation):void
		{
			var index:int = _activeLoadQueue.indexOf(operation);
			if (index > -1) {
				_activeLoadQueue.splice(index, 1);
				pushNextPending();
				var timer:Timer;
				if (operation.attempts > 6)
					LogService.error("Giving up accessing thumbnail for " + operation.displayName);
				else if (operation.attempts > 1)
					timer = new Timer(DELAY, 1);
				else
					timer = new Timer(FIRST_TIME_DELAY, 1);
				if (timer) {
					timer.addEventListener(TimerEvent.TIMER, function():void {
						pushOrWait(operation);
					});
					timer.start();
				}
			}
		}
	}
}
/*	public class ThumbnailLoadingPipeline extends LoadingPipeline updated version of this class per DEV-2451
	{
		private static var _instance:ThumbnailLoadingPipeline;
		
		private var _tokenOperation:BorealTokenOperation;
		private var _token:BorealToken;
		private var _pendingTokenQueue:Vector.<IOperation> = new Vector.<IOperation>();
		
		
		public function ThumbnailLoadingPipeline()
		{
			Utilities.assert(!_instance);
			super();
		}
		
		public static function get instance():ThumbnailLoadingPipeline
		{
			if (!_instance)
				_instance = new ThumbnailLoadingPipeline();
			return _instance;
		}
		
		public function push(id:String, name:String, completeHandler:Function):void
		{
			if (_token && _token.valid
					|| (_token && _token.content == ""))
				pushAndValidateToken(id, name, completeHandler);
			else {
				_token = new BorealToken("");
				var tokenOperation:BorealTokenOperation = new BorealTokenOperation();
				tokenOperation.displayName = "Fetch thumbnail token";
				tokenOperation.addEventListener(Event.COMPLETE, function(e:Event):void {
					tokenOperation.removeEventListener(Event.COMPLETE, arguments.callee);
					_token = IOperation(e.target).result as BorealToken;
					pushAndValidateToken(id, name, completeHandler);
				});
				tokenOperation.execute();	
			}
		}
		
		private function pushAndValidateToken(id:String, name:String, completeHandler:Function):void
		{
			var operation:RestHttpOperation = AssetOperation.getThumbnailOperation(id, name, handleNotReady);
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, completeHandler);
			BorealOperation.addListeners(operation, function(event:Event):void {
				var resultOperation:IOperation = IOperation(event.target);
				loader.loadBytes(resultOperation.result);
				handleOperationComplete(resultOperation);
			}, handleUnrecoverableError);
			
			if (_token && _token.valid) {
				var pathWithAuth:String = "auth/" + _token.content + "/";
				while (_pendingTokenQueue.length > 0) {
					var o:RestHttpOperation = _pendingTokenQueue.shift();
					o.path = pathWithAuth + o.path;
					pushOrWait(o);
				}
				operation.path = pathWithAuth + operation.path;
				pushOrWait(operation);
			} else
				_pendingTokenQueue.push(operation);
		}

		private function handleNotReady(operation:IOperation):void
		{
			var index:int = _activeLoadQueue.indexOf(operation);
			if (index > -1) {
				_activeLoadQueue.splice(index, 1);
				pushNextPending();
				var timer:Timer;
				if (operation.attempts > 3)
					LogService.error("Giving up accessing thumbnail for " + operation.displayName);
				else if (operation.attempts > 1)
					timer = new Timer(DELAY, 1);
				else
					timer = new Timer(FIRST_TIME_DELAY, 1);
				if (timer) {
					timer.addEventListener(TimerEvent.TIMER, function():void {
						pushOrWait(operation);
					});
					timer.start();
				}
			}
		}
	}
}*/