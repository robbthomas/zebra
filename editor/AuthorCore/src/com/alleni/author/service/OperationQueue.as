/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.service
{
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.taconite.service.IOperation;
	import com.alleni.taconite.service.LogService;
	
	import flash.events.Event;

	/**
	 * Execute operations in sequence, in the background. 
	 * @author steve
	 * 
	 */
	public class OperationQueue
	{
		private var _ops:Vector.<IOperation> = new Vector.<IOperation>();
		private var _nowDoing:IOperation;
		
		
		public function OperationQueue()
		{
		}
		
		public function push(op:IOperation):void
		{
			_ops.push(op);
			continueExecuting();
		}
		
		private function continueExecuting():void
		{
			if (_ops.length > 0) {
                if(_nowDoing == null) {
                    var op:IOperation = _ops[0];
                    addListeners(op);
                    _nowDoing = op;
                    op.execute();
                }
            }
		}
		
		protected function addListeners(op:IOperation):void
		{
			op.addEventListener(Event.COMPLETE, handleComplete);
		}

        private function finishOperation(event:Event):void {
			if (_nowDoing == null) {
				LogService.error("OperationQueue does not recognize recently completed operation " + event.target);
				return;
			}

			var n:int = _ops.indexOf(_nowDoing);
			if (n >= 0)
				_ops.splice(n, 1);
			_nowDoing = null;

			continueExecuting();
        }
		
		private function handleComplete(event:Event):void
		{
            finishOperation(event);
		}
		
		protected function handleError(event:Event):void
		{
			finishOperation(event);
		}
	}
}