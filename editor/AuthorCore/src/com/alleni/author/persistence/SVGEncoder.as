/*
Incomplete styles, position, and other properties are not addressed in this code yet

*/

package com.alleni.author.persistence
{
import com.alleni.author.model.objects.Drawing;
import com.alleni.author.model.objects.VectorObject;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.objects.DrawingView;

import flash.display.DisplayObject;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.xml.XMLNode;

public class SVGEncoder
	{
		
		
		protected var objects:Array = new Array();
		protected var _drawing:Drawing;
		
		
		/**
		 * 
		 * @param bounds
		 * 
		 */
		public function SVGEncoder(drawing:Drawing)
		{
			_drawing = drawing;
		}
		
		/**
		 * 
		 * @param object
		 * 
		 */
		public function addView(object:ObjectView):void{
			objects.push(object);
		}
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get xml():XML
		{
			var _xml:XML = <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>;
			var _defs:XML = <defs/>;
			_xml.@width = _drawing.width;
			_xml.@height = _drawing.height;
			_xml.@viewBox = "0 0 " + _drawing.width + " " + _drawing.height;
			_xml.@version = new Date().getTime();
			for each (var object:ObjectView in objects){
				// grab svg defs from object
				_defs.appendChild(object.getSVGDef())
				// pass bounds so svg coordinates can be calculated for nested svg object
				_xml.appendChild(object.getSVG(localToTarget));
			}
			_xml.prependChild(_defs);
//			trace("");
//			trace(_xml.toXMLString());
//			trace("");
			return _xml;
		}
		
		private function localToTarget(view:DisplayObject, pt:Point):Point
		{
			var global:Point = view.localToGlobal(pt);
			var result:Point = _drawing.getView().globalToLocal(global);
			return result;
		}
	}
	
	
}