package com.alleni.author.persistence
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.GadgetType;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.gadgets.GadgetState;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.AbstractExternalObject;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.Assets;
import com.alleni.author.model.ui.CalcPropertyWireAnchor;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.PathNodeWireAnchor;
import com.alleni.author.model.ui.SetterWireAnchor;
import com.alleni.author.model.ui.TriggerWireAnchor;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.model.ui.VariableWireAnchor;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.WorldContainer;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.factory.SerializerImplementation;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.persistence.json.JSON;

import flash.geom.Point;
import flash.utils.Dictionary;

import mx.collections.IList;
import mx.utils.ObjectUtil;


	public class GadgetEncoder
	{
		private static var _serializer:SerializerImplementation;
		private static const ZERO:Point = new Point(0, 0);

        public static const SERIALIZED_LOCATION_GENERATED:String = 'SERIALIZED_LOCATION_GENERATED';
        public static const SERIALIZED_LOCATION_NONE:String =      'SERIALIZED_LOCATION_NONE';
        public static const SERIALIZED_LOCATION_EXISTING:String =  'SERIALIZED_LOCATION_EXISTING';
		
		public function encode(project:Project, object:AbstractContainer=null, saveState:Boolean=false, metaDataOnly:Boolean=false, autosave:Boolean=false, gadgets:Vector.<Project>=null, assets:Assets=null):*
		{

			if (object && !metaDataOnly) {
                var content:Object = buildContentJson(object, gadgets, assets);
                project.initialValues = content.initialValues;
                delete content.initialValues;
                project.gadgets = content.gadgets;
//                delete content.gadgets;
//                delete content.assets;
                project.content = content;
                project.content.watermarkPosition = project.watermarkPosition;
                project.width = object.width;
                project.height = object.height;
                if(saveState) {
                    project.state = getState(object, SERIALIZED_LOCATION_NONE);
                    project.state.running = Application.instance.document == null ? false : Application.instance.document.running;
                    project.state.locked = Application.instance.document == null ? false : Application.instance.document.locked;
                    project.state.scripts = project.scripts;
                } else {
                    project.state = null;
                }
            }

            if(object is World) {
                project.projectTypeId = GadgetType.PROJECT;
            } else if(object is EventPage) {
                project.projectTypeId = GadgetType.EVENT;
                project.name = object.title;
                project.previewImage = EventPage(object).createImageAsset();
            } else if(object is SmartObject) {
                project.projectTypeId = GadgetType.GENERIC;
                project.name = object.title;
            }

            project.autoSave = autosave;

			trace("GadgetEncoder:encode: autosave="+autosave,"name="+project.name);
			
			if (metaDataOnly && false /*TODO stop sending content when the server supports this*/) {
				project.content = null; // this is a metadata-only update which should NOT include content.
                project.initialValues = null;
                project.state = null;
            }
			
			return project;
		}
		
		private static function instanceToJson(obj:AbstractObject, objectIds:Dictionary, encodeRoot:Boolean = true):Object
		{
			var result:Object = {};
			var composite:SmartObject = obj as SmartObject;
			
			if (composite) {
                if (composite.gadget) {
                    result.gadget = composite.gadget;
                }
			}
			result.className = obj.shortClassName;
            var anchorInfo:Object = anchorsToJson(obj,objectIds);
            result.customAnchors = anchorInfo.customAnchors;
            result.inspectorVisible = anchorInfo.inspectorVisible;
            result.mcVisible = anchorInfo.mcVisible;
            result.inletKeys = anchorInfo.inletKeys;

			if(result.customAnchors == null || result.customAnchors.length == 0) {
				delete result.customAnchors;
			}
			var container:AbstractContainer = obj as AbstractContainer;
			if (container) {
			    result.children = childrenToJson(container, objectIds);
			}
            var valueProps:Vector.<String> = obj.model.getAllValueProperties();
            if(valueProps.length != 0) {
                result.valueChildren = {};
                for each(var valueProp:String in valueProps) {
                    if(valueProp in obj && obj[valueProp] is AbstractObject) {
                        result.valueChildren[valueProp] = instanceToJson(obj[valueProp] as AbstractObject, objectIds);
                    }
                }
            }
            var valueListProps:Vector.<String> = obj.model.getAllValueListProperties();
            if(valueListProps.length != 0) {
                result.valueChildrenLists = {};
                for each(var valueListProp:String in valueListProps) {
                    if(valueListProp in obj && obj[valueListProp] is IList) {
                        var valueList:IList = obj[valueListProp] as IList;
                        result.valueChildrenLists[valueListProp] = [];
                        for(var valueListIndex:int=0; valueListIndex < valueList.length; valueListIndex++) {
                            result.valueChildrenLists[valueListProp][valueListIndex] = instanceToJson(valueList[valueListIndex] as AbstractObject, objectIds)
                        }
                    }
                    if(valueListProp in obj && obj[valueListProp] is AbstractObject) {
                        result.valueChildrenLists[valueListProp] = instanceToJson(obj[valueListProp] as AbstractObject, objectIds);
                    }
                }
            }
			return result;
		}
		
		protected static function childrenToJson(obj:AbstractContainer, objectIds:Dictionary):Array
		{
			var result:Array = [];
			for each (var child:AbstractObject in obj.children)
				result.push(instanceToJson(child, objectIds));
			return result;
		}
		
		protected static function generateObjectIds(obj:AbstractObject, generateSerializedLocation:Boolean):Dictionary
		{
			var result:Dictionary = new Dictionary();
			result[obj] = [];
			collectChildren(obj, result, true, generateSerializedLocation);
			return result;
		}
		
		protected static function stateToJson(obj:AbstractObject, initial:Boolean, globalPosition:Boolean, objectIds:Dictionary, newSerializedLocation:String):Object
		{
			var state:Object = stateToJsonWithoutRefs(obj, initial, globalPosition);
			addObjectRefs(obj, state, objectIds, obj as AbstractContainer, newSerializedLocation);
			return state;
		}
		
		protected static function stateToJsonWithoutRefs(obj:AbstractObject, initial:Boolean, globalPosition:Boolean=false):Object
		{
            var result:Object = null;
            if(obj is EventPage && !EventPage(obj).hasInternals && EventPage(obj).cachedGadgetData) {
                if(!initial) {
                    // prefer data current values if we aren't looking for initial
                    result = ObjectUtil.copy(EventPage(obj).cachedGadgetData.currentValues);
                }
                if(!result) {
                    // if we didn't want current or it wasn't there, use initial
                    result = ObjectUtil.copy(EventPage(obj).cachedGadgetData.initialValues);
                }
                if(!result) {
                    // if it wasn't there, default to values on the gadget
                    result = ObjectUtil.copy(EventPage(obj).gadget.initialValues);
                }
            }
            var encodedChildren:Boolean = true; // assume we already have the children encoded from the above block
            if(result == null) {
                result = {};
                encodedChildren = false;
            }
			if (initial) {
				result.values = ObjectUtil.copy(obj.initialValues);
				var externalObject:AbstractExternalObject = obj as AbstractExternalObject;
				if (externalObject)
					result.values.asset = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(externalObject.asset));
			} else {
				result.values = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(obj));
			}
            if (!("shortClassName" in result.values)) {
                result.values.className = obj.shortClassName;
            }

			if (globalPosition) {
				var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
				var pt:Point = ZERO;
				
				pt = obj.getView(worldContainer).localToGlobal(pt);
				pt = worldContainer.worldView.globalToLocal(pt);
				result.values.x = pt.x;
				result.values.y = pt.y;
			}
			if ("autosaveMinutes" in obj) {
				result.autosaveMinutes = obj["autosaveMinutes"];
			}
			result.anchorValues = {};
            for each(var a:Object in obj.anchors) {
                var anchor:WireAnchor = a as WireAnchor;
                if(anchor) {
                    var values:Object = initial ? ObjectUtil.copy(anchor.initialValues) : anchor.getCurrentValues();
                    for(var prop:String in values) {
                        // only set this if values is not empty
                        result.anchorValues[anchor.modifierDescription.key] = values;
                        break;
                    }
                }
            }
            if(encodedChildren) {
                // dont need to do any of the below as we already have everything inside
                return result;
            }
			var container:AbstractContainer = obj as AbstractContainer;
			if (container) {
				result.children = [];
				for each(var child:Object in container.children)
					result.children.push(stateToJsonWithoutRefs(child as AbstractObject, initial));
			}
            var valueProps:Vector.<String> = obj.model.getAllValueProperties();
            if(valueProps.length != 0) {
                result.valueChildren = {};
                for each(var valueProp:String in valueProps) {
                    if(valueProp in obj && obj[valueProp] is AbstractObject) {
                        result.valueChildren[valueProp] = stateToJsonWithoutRefs(obj[valueProp] as AbstractObject, initial);
                    }
                }
            }
            var valueListProps:Vector.<String> = obj.model.getAllValueListProperties();
            if(valueListProps.length != 0) {
                result.valueChildrenLists = {};
                for each(var valueListProp:String in valueListProps) {
                    if(valueListProp in obj && obj[valueListProp] is IList) {
                        var valueList:IList = obj[valueListProp] as IList;
                        result.valueChildrenLists[valueListProp] = [];
                        for(var valueListIndex:int=0; valueListIndex < valueList.length; valueListIndex++) {
                            result.valueChildrenLists[valueListProp][valueListIndex] = stateToJsonWithoutRefs(valueList[valueListIndex] as AbstractObject, initial)
                        }
                    }
                }
            }
			return result;
		}
		
		public static function addObjectRefs(obj:AbstractObject, state:Object, objectIds:Dictionary, root:AbstractContainer, newSerializedLocation:String):void
		{
            addObjectPropertyRefs(state.values,  objectIds,  root);
            switch(newSerializedLocation) {
                case SERIALIZED_LOCATION_NONE:
                    // this would look up the empty string or continue to use a useless location
                    // null makes it more obvious that there is no location for the root
                    state.values["serializedLocation"] = null;
                    break;
                case SERIALIZED_LOCATION_GENERATED:
                    // override the existing location with the new path we just generated
                    var generatedId:String = Array(objectIds[obj]).join(",");
                    state.values["serializedLocation"] = generatedId;
                    obj.serializedLocation = generatedId;
                    obj.initialValues.serializedLocation = generatedId;
                    break;
                case SERIALIZED_LOCATION_EXISTING:
                    // leave it alone
                    break;
                default:
                    throw new Error("Unknown value for serializedLocation [" + newSerializedLocation + "]");

            }
            addObjectPropertyRefs(state.anchorValues,  objectIds,  root);
            if(obj is SmartObject) {
                // recreate objectIds from this composite as a root
                // in this way all current/initial values of children
                // will have object reference IDs rooted at the closest parent composite
                if(obj == root && newSerializedLocation == SERIALIZED_LOCATION_NONE) {
                    // children of root can continue to use passed in values
                    newSerializedLocation = SERIALIZED_LOCATION_GENERATED;
                } else {
                    // grand children however cannot.
                    newSerializedLocation = SERIALIZED_LOCATION_EXISTING;
                }
                objectIds = new Dictionary();
                objectIds[obj] = [];
                collectChildren(obj, objectIds, true, newSerializedLocation == SERIALIZED_LOCATION_GENERATED);
            }
			var container:AbstractContainer = obj as AbstractContainer;
			if (container && "children" in state) {
				var i:int;
				var children:IList = container.children;
				var length:int = children.length;
				for (i = 0; i < length; i++) {
					addObjectRefs(children[i] as AbstractObject, state.children[i], objectIds, root, newSerializedLocation);
                }
			}
            if("valueChildren" in state) {
                for(var valueProp:String in state.valueChildren) {
                    if(valueProp in obj && obj[valueProp] is AbstractObject) {
                        addObjectRefs(obj[valueProp] as AbstractObject, state.valueChildren[valueProp], objectIds,  root, newSerializedLocation);
                    }
                }
            }
            if("valueChildrenLists" in state) {
                for(var valueListProp:String in state.valueChildrenLists) {
                    if(valueListProp in obj && obj[valueListProp] is IList) {
                        var valueList:IList = obj[valueListProp] as IList;
                        for(var valueListIndex:int=0; valueListIndex < valueList.length; valueListIndex++) {
                            addObjectRefs(obj[valueListProp][valueListIndex] as AbstractObject, state.valueChildrenLists[valueListProp][valueListIndex], objectIds,  root, newSerializedLocation);
                        }
                    }
                }
            }
		}

        private static function addObjectPropertyRefs(obj:Object,  objectIds:Dictionary, root:AbstractContainer):void {
            for(var prop:String in obj) {
                if(obj[prop] != null && obj[prop].hasOwnProperty("type") && obj[prop]["type"] == "AbstractObjectReference") {
                    var id:String = obj[prop]["id"];
                    var ao:AbstractObject = root.findObjByUID(id);
                    if(ao && ao in objectIds) {
                        obj[prop]["id"] = objectIds[ao];
                    } else {
                        obj[prop]["id"] = id;
                    }
                } else if(obj[prop] is Object) {
                    addObjectPropertyRefs(obj[prop],  objectIds,  root);
                }
            }
        }

		private static function convertObjectRef(obj:Object, state:Object, prop:Object, objectIds:Dictionary):void
		{
			if(prop in obj) {
				if(obj[prop] in objectIds) {
					state[prop] = objectIds[obj[prop]];
				} else if(obj[prop] as Array) {
					state[prop] = [];
					var i:int;
					var length:int = obj[prop].length;
					for(i = 0; i < length; i++) {
						convertObjectRef(obj[prop], state[prop], i, objectIds);
					}
				} else if(obj[prop] as AbstractObject) {
					state[prop] = (obj[prop] as AbstractObject).uid;
				} else {
					state[prop] = null;
				}
			}
		}
		
		private static function anchorsToJson(object:AbstractObject, objectIds:Dictionary):Object
		{
            var mcVisibleKeys:Array = [];
            var inspectorVisibleKeys:Array = [];
            var customAnchors:Array = [];

            var inletKeys:Object = {};

			for each(var anchorObj:Object in object.anchors) {
				var anchor:WireAnchor = anchorObj as WireAnchor;
                if(anchor == null) {
                    continue;
                }
				
				if (anchor as SetterWireAnchor || anchor as TriggerWireAnchor || anchor as CalcPropertyWireAnchor || anchor as VariableWireAnchor || anchor as PathNodeWireAnchor)
					customAnchors.push(serializer.serializeWireAnchor(anchor as WireAnchor, objectIds));
                if (anchor && anchor.messageCenterVisible)
                    mcVisibleKeys.push(anchor.modifierDescription.key);
                if (anchor && anchor.propertyInspectorVisible)
                    inspectorVisibleKeys.push(anchor.modifierDescription.key);

                if(anchor is SetterWireAnchor == false && anchor.modifierDescription is InletDescription) {
                    var key:String = anchor.keyPress;
                    if(key && key.length > 0) {
                        inletKeys[anchor.modifierDescription.key] = anchor.keyPress;
                    }
                }
			}
			return packAnchorInfo(mcVisibleKeys,inspectorVisibleKeys,customAnchors,inletKeys);
		}

        public static function packAnchorInfo(mcVisibleKeys:Array = null,inspectorVisibleKeys:Array = null,customAnchors:Array = null,inletKeys:Object = null):Object {
            // call packAnchorInfo() for an empty anchorInfo object that can be treated like a full one (it can be added to, its' members iterated over, etc.)
            if (mcVisibleKeys == null) mcVisibleKeys = [];
            if (inspectorVisibleKeys == null) inspectorVisibleKeys = [];
            if (customAnchors == null) customAnchors = [];
            if (inletKeys == null) inletKeys = new Object();

            return {
                mcVisible:mcVisibleKeys,
                inspectorVisible:inspectorVisibleKeys,
                customAnchors:customAnchors,
                inletKeys:inletKeys
            };
        }
		
		private static function edgeAnchorsToJson(object:AbstractObject):Array
		{
			var result:Array = [];
			for each (var anchorObj:Object in object.anchors) {
				if (anchorObj as ExternalSmartWireAnchor)
					result.push(serializer.serializeWireAnchor(anchorObj as ExternalSmartWireAnchor));
			}
			return result;
		}


		/**
         * This is a breadth first iteration with no recursion.
         * It does not iterate to children of SmartObjects, unless the root is a SmartObject and recurseRoot is true
		 * @param root object for which the children and children's children should be collected
		 * @param ids dictionary of object->id where root is assumed to exist if ids exists.
		 *        It will be populated with ids for each child.
         *        ids can be null if id generation is not needed
         * @param recurseRoot flag may be set to true to force stepping to children of the root even if it is a composite
		 * @return array containing all descendent children of the root including the root
		 */
		public static function collectChildren(root:AbstractObject, ids:Dictionary, recurseRoot:Boolean, generateSerializedLocation:Boolean):Array
		{
			var result:Array = [root];
			for each (var candidateParent:AbstractObject in result) {
				var container:AbstractContainer = candidateParent as AbstractContainer;
                var valueProps:Vector.<String> = candidateParent.model.getAllValueProperties();
                var valueListProps:Vector.<String> = candidateParent.model.getAllValueListProperties();
				if ((container || valueProps.length > 0 || valueListProps.length > 0) && ((recurseRoot && candidateParent == root) || !(candidateParent as SmartObject))) {
					if (ids) {
						var parentId:Array = ids[candidateParent];
						var childIndex:int = 0;
					}
                    if(container) {
                        for each (var child:AbstractObject in container.children) {
                            result.push(child);
                            if (ids) {
                                if(generateSerializedLocation || child.serializedLocation == null) {
                                    ids[child] = parentId.concat(childIndex);
                                } else {
                                    ids[child] = child.serializedLocation.split(",");
                                }
                                childIndex++;
                            }
                        }
                    }
                    if(valueProps.length > 0) {
                        for each(var valueProp:String in valueProps) {
                            if(valueProp in candidateParent) {
                                var valueChild:AbstractObject = candidateParent[valueProp] as AbstractObject;
                                if(valueChild) {
                                    result.push(valueChild);
                                    if(ids) {
                                        if(generateSerializedLocation || valueChild.serializedLocation == null) {
                                            ids[valueChild] = parentId.concat(valueProp);
                                        } else {
                                            ids[valueChild] = valueChild.serializedLocation.split(",");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if(valueListProps.length > 0) {
                        for each(var valueListProp:String in valueListProps) {
                            if(valueListProp in candidateParent) {
                                var valueList:IList = candidateParent[valueListProp] as IList;
                                if(valueList) {
                                    for(var valueListIndex:int=0; valueListIndex < valueList.length; valueListIndex++) {
                                        var valueListChild:AbstractObject = valueList[valueListIndex] as AbstractObject;
                                        result.push(valueListChild);
                                        if(ids) {
                                            if(generateSerializedLocation || valueListChild.serializedLocation == null) {
                                                ids[valueListChild] = parentId.concat([valueListProp, valueListIndex]);
                                            } else {
                                                ids[valueListChild] = valueListChild.serializedLocation.split(",");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
				}
			}
			return result;
		}
		
		private static function wiresToJson(container:AbstractContainer):Array
		{
			var ids:Dictionary = new Dictionary();
			ids[container] = [];
			var objects:Array = collectChildren(container, ids, true, true); // if we are saving wires we will always want new IDs
			var wires:Array = ApplicationController.instance.wireController.getWiresExclusive(objects);
			var result:Array = [];

			for each (var wire:Wire in wires) {
                if(wire.masterAnchor.hostObject == container && wire.slaveAnchor.hostObject == container) {
                    continue; // wires to and from the gadget MC are considered outside the gadget
                }
				result.push(serializer.serializeWire(wire, ids));
            }
			return result;
		}

		private static function buildContentJson(container:AbstractContainer, gadgets:Vector.<Project>, assets:Assets):Object
		{
            trace("Building content json for " + container.title);
            if (gadgets != null) {
                trace("   gadgets.length="+gadgets.length);
            }
			var result:Object = {};
			var objectIds:Dictionary = generateObjectIds(container, true);
			result.initialValues = stateToJson(container, true, false, objectIds, SERIALIZED_LOCATION_NONE);
			if (container is SmartObject) {
				delete result.initialValues.values["x"];
				delete result.initialValues.values["y"];
				delete result.initialValues.values["rotation"];
				delete result.initialValues.values["rawRotation"];
				delete result.initialValues.values["scale"];
				delete result.initialValues.values["locationXYR"];
				delete result.initialValues.values["layer"];
				delete result.initialValues.values["targetCollide"];
				delete result.initialValues.values["targetDrop"];
				delete result.initialValues.values["snapTo"];
				delete result.initialValues.values["snap"];
				delete result.initialValues.values["pathPercent"];
				delete result.initialValues.values["pathValue"];
				delete result.initialValues.values["pathMaximum"];
                delete result.initialValues.values["pathMinimum"];
                delete result.initialValues.values["masters"];
                delete result.initialValues.values["pageNumber"];
			}

            var instance:Object = instanceToJson(container,  objectIds, false);
            result.children = instance.children;
            if("valueChildren" in instance) {
                result.valueChildren = instance.valueChildren;
            }
            if("valueChildrenLists" in instance) {
                result.valueChildrenLists = instance.valueChildrenLists;
            }

            // for root gadget we list entire library assets & gadgets.  For composite & eventPage we only list the referenced ones
            var gadgetsArray:Array = [];
            for each (var g:Project in gadgets) {
                gadgetsArray.push(g);
            }
			result.gadgets = gadgetsArray;
			result.assets = assets.assets.toArray();

            // usageCount cache on each EventPage
            if (container is World) {
                result.usageCounts = World(container).eventFlow.usageCountsJson();
            }

			result.edgeAnchors = edgeAnchorsToJson(container);
			result.wiring = wiresToJson(container);
			return result;
		}

		public static function buildCopyContentJson(list:Vector.<TaconiteModel>, globalPositions:Boolean):Object
		{
			var children:Array = [];
			var initialValues:Array = [];
			var allObjects:Array = [];
			var objectIds:Dictionary = new Dictionary();
            var usage:UsageCounts = new UsageCounts();
			var selectionIndex:int = 0;
			var ao:AbstractObject;
			var commonParent:AbstractObject = (list.length > 0) ? AbstractObject(list[0].value).parent : null;

            var m:TaconiteModel;
			for each (m in list) {
				ao = m.value as AbstractObject;

				objectIds[ao] = [selectionIndex]; // seed the root ids (there is no id [] here)
				allObjects = allObjects.concat(collectChildren(ao, objectIds, false, true));
                ao.getUsageCounts(usage);
				
				if (ao.parent != commonParent)
					commonParent = null;

				selectionIndex++;
			}
			selectionIndex = 0;
			for each (m in list) {
				ao = m.value as AbstractObject;

				children.push(instanceToJson(ao, objectIds));
				initialValues.push(stateToJsonWithoutRefs(ao, false, globalPositions));
				addObjectRefs(ao, initialValues[selectionIndex], objectIds, ApplicationController.instance.authorController.world, SERIALIZED_LOCATION_NONE);
				
				selectionIndex++;
			}
			var wires:Array = ApplicationController.instance.wireController.getWiresInclusive(allObjects);
			var wireJson:Array = [];
			
			for each (var wire:Wire in wires)
                if(!wire.masterAnchor.isCreatorHiddenAnchor() && !wire.slaveAnchor.isCreatorHiddenAnchor()) {
                    wireJson.push(serializer.serializeWire(wire, objectIds));
                }

            // gather assets & gadgets in case this copy is pasted into a different project
            var assets:Vector.<Asset> = usage.getAssetsList();
            var assetsJson:Array = [];
            for each (var a:Asset in assets) {
                assetsJson.push(a.referenceJSON);
            }

            var gadgets:Vector.<Project> = usage.getGadgetsList();
            var gadgetsJson:Array = [];
            for each (var g:Project in gadgets) {
                gadgetsJson.push(g.referenceJSON);
            }

			return {
				initialValues:{children:initialValues},
				children:children,
				wiring:wireJson,
				commonParent:commonParent ? commonParent.uid : null,
                assets:assetsJson,
                gadgets:gadgetsJson
			};
		}
		
		public static function buildDeleteContentJson(selection:Vector.<TaconiteModel>):Object
		{
			var children:Array = [];
			var initialValues:Array = [];
			var currentValues:Array = [];
			var parents:Array = [];
			var paths:Array = [];
			var ids:Array = [];
			var allObjects:Array = [];
			var objectIds:Dictionary = new Dictionary();
			var selectionIndex:int = 0;
			var m:TaconiteModel;
			var object:AbstractObject;
			
			for each (m in selection) {
				object = m.value as AbstractObject;
				
				objectIds[object] = [selectionIndex]; // seed the root ids (there is no id [] here)
				allObjects = allObjects.concat(collectChildren(object, objectIds, false, true));

				selectionIndex++;
			}
			selectionIndex = 0;
			for each (m in selection) {
				object = m.value as AbstractObject;

				children.push(instanceToJson(object, objectIds));
				initialValues.push(stateToJsonWithoutRefs(object, true, false));
				currentValues.push(stateToJsonWithoutRefs(object, false, false));

                    var parent:AbstractContainer = object.parent as AbstractContainer;
                    parents.push(parent.uid);
                    var path:Array = parent.model.findPathToValueObj(object);
                    paths.push(path.length == 1 ? path[0] : path);

				ids.push(object.uid);
				addObjectRefs(object, initialValues[selectionIndex], objectIds, ApplicationController.instance.authorController.world, SERIALIZED_LOCATION_NONE);
				addObjectRefs(object, currentValues[selectionIndex], objectIds, ApplicationController.instance.authorController.world, SERIALIZED_LOCATION_NONE);

				selectionIndex++;
			}
			var wires:Array = ApplicationController.instance.wireController.getWiresInclusive(allObjects);
			var wireJson:Array = [];
			
			for each (var wire:Wire in wires) {
                if(!wire.masterAnchor.isCreatorHiddenAnchor() && !wire.slaveAnchor.isCreatorHiddenAnchor()) {
                    wireJson.push(serializer.serializeWire(wire, objectIds));
                }
            }
			
			return {
				initialValues:{children:initialValues},
				currentValues:{children:currentValues},
				parents:parents,
				paths:paths,
				ids:ids,
				children:children,
				wiring:wireJson
			};
		}
		
		public static function getState(rootObject:AbstractObject, newSerializedLocation:String):GadgetState
		{
            Utilities.assert(newSerializedLocation == SERIALIZED_LOCATION_NONE || newSerializedLocation == SERIALIZED_LOCATION_EXISTING);
			var state:GadgetState = new GadgetState();
			var objectIds:Dictionary = generateObjectIds(rootObject, newSerializedLocation == SERIALIZED_LOCATION_GENERATED);
			state.initialValues = stateToJson(rootObject, false, false, objectIds, newSerializedLocation);
			return state;
		}

        public static function getValues(rootObject:AbstractObject):Object
        {
            var result:Object = {};
            var objectIds:Dictionary = generateObjectIds(rootObject, false);
            result.initialValues = stateToJson(rootObject, true, false, objectIds, SERIALIZED_LOCATION_EXISTING);
            result.currentValues = stateToJson(rootObject, false, false, objectIds, SERIALIZED_LOCATION_EXISTING);
            return result;
        }

		private static function get serializer():SerializerImplementation
		{
			if (!_serializer)
				_serializer = new SerializerImplementation();
			return _serializer;
		}
	}
}
