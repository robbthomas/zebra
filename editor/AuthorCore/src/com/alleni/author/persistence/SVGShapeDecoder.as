package com.alleni.author.persistence
{
	public class SVGShapeDecoder
	{
		public function SVGShapeDecoder()
		{
		}
		
		public static function getStyleValueForName(style:String, name:String):String
		{
			var properties:Array = style.split(";");
			
			for each (var property:String in properties) {
				var nameValue:Array = property.split(":");
				if (nameValue[0] == name)
					return nameValue[1];
			}
			
			return "";
		}
	}
}