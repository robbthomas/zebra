package com.alleni.author.persistence
{
	import flash.utils.Dictionary;

	/*
	CSS encoder contains a dictionary of selectors, each of which has a dictionary of properties
	*/

	public class CSSEncoder
	{
		
		private var _selectors:Dictionary = new Dictionary();;
		
		public function CSSEncoder()
		{
			
		}
		
		public function addSelector(selector:String):void{
			
			if(_selectors[selector] == null){
				
				_selectors[selector] = new Dictionary();
			}
		}
		
		public function addProperty(selector:String, property:String, value:String):void{
			
			addSelector(selector);
			var props:Dictionary = _selectors[selector];
			props[property] = value;
			
		}
		
		 
		 private function makePropertyValuePair(property:String, value:Object):String{
			   var line:String  = property;
				line =  line.concat(": ");
				line =  line.concat(value);
				line =  line.concat(";");
			   return line;
		 }
		
		public function makeCSS():String{
			
			var css:String = "";
			
			for(var skey:* in _selectors){
				
				css = css.concat(skey);
				css = css.concat(" { ");
				var _properties:Dictionary = _selectors[skey];
				
			
				for(var key:* in _properties){
					css = css.concat(makePropertyValuePair(key,_properties[key]));
				}
				
				css = css.concat(" }");
			
			}
			
			return css;
		}
		
		
		public function test():void{
			
			/*
			body {  
			color: navy;
			font-family: "arial"; }
			.title {  text-decoration: bold;
			color: green; 
			}
			
			*/
			
			addSelector("body");
			addProperty("body","text-decoration", "none");
			addProperty("body","color", "navy");
			
			addSelector(".title");
			addProperty(".title","text-decoration", "bold");
			addProperty(".title","color", "green");
			
			trace("CSS:     ", makeCSS());
			
		}
	
	}
}