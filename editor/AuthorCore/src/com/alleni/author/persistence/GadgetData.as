package com.alleni.author.persistence
{
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.AbstractContainer;

	/**
	 * data used while Gadgets are being constructed
	 * 
	 */
	public class GadgetData
	{
		public var parent:AbstractObject;
		public var initialValues:Object = null;
        public var anchorInfo:Object = null;
        public var setComplete:Boolean = true;
		public var currentValues:Object = null;
		public var objectPropStorage:Object = null;
		public var newUID:Boolean = true;
		public var overriddenValues:Object = null;
        public var stackTrace:Object;

        public function GadgetData(parent:AbstractObject, initialValues:Object=null, anchorInfo:Object=null, setComplete:Boolean=true, currentValues:Object=null, objectPropStorage:Object=null, newUID:Boolean=true, overriddenValues:Object=null)
		{
            new Error().getStackTrace();
			this.parent = parent;
			this.initialValues = initialValues;
            this.anchorInfo = anchorInfo;
			this.setComplete = setComplete;
			this.currentValues = currentValues;
			this.objectPropStorage = objectPropStorage;
			this.newUID = newUID;
			this.overriddenValues = overriddenValues;
		}

        private function get customAnchors():Array {
            if (anchorInfo != null && anchorInfo.customAnchors as Array)
                return anchorInfo.customAnchors;
            else
                return [];
        }
		
		public function toString():String
		{
			var prop:String;
			
			var dump:String = "GadgetData: \n parent="+parent+
				"\n setterAnchors:";
				
			for (prop in customAnchors)
				dump += "\n    customAnchors["+prop+"]: "+customAnchors[prop];
			
			dump += "\n setComplete:"+setComplete+
				"\n currentValues:";
			
			for (prop in currentValues)
				dump += "\n    currentValues["+prop+"]: "+currentValues[prop];
			
			dump += "\n objectPropStorage:";
			
			for (prop in objectPropStorage)
				dump += "\n    objectPropStorage["+prop+"]: "+objectPropStorage[prop];
				
			dump += "\n newUID:"+newUID+
				"\n overriddenValues:";
			
			for (prop in overriddenValues)
				dump += "\n    overriddenValues["+prop+"]: "+overriddenValues[prop];
			
			return dump;
		}
	}
}