package com.alleni.author.persistence
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.PagerController;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ObjectFactory;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.document.Document;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.gadgets.GadgetState;
import com.alleni.author.model.gadgets.Gadgets;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.ToggleButtonGroup;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.test.TestPanel;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.Assets;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.model.ui.Wiring;
import com.alleni.author.service.assets.AssetLoadOperation;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadDebugRunnable;
import com.alleni.author.util.thread.PseudoThreadRunnable;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.factory.SerializerImplementation;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.persistence.AtomData;
import com.alleni.taconite.persistence.IAtomDecoder;

import flash.geom.Rectangle;
import flash.net.URLRequest;
import flash.net.navigateToURL;
import flash.utils.Dictionary;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.utils.ObjectUtil;

public class GadgetDecoder implements IAtomDecoder
	{
        private static const GREEN_THREAD_ENABLED:Boolean = true;

		private static var _serializer:SerializerImplementation;
        private static var _disableInputToken:Object;

		public function decode(encoded:*, rootId:String):AtomData
		{
			var gadgets:Gadgets = new Gadgets();
			var root:Project = Project.fromJson(encoded);
            if(encoded.children) {
                for each(var child:Project in Project.fromJsonList(encoded.children)) {
                    gadgets.gadgets.addItem(child);
                }
            }
			
			return new AtomData(root, gadgets);
		}
		
		public static function makePasteContent(json:Object, parent:AbstractContainer, index:Object=null, setComplete:Boolean=true, withWires:Boolean=true,  postProcess:Function=null):void
		{
			var objects:Dictionary = new Dictionary();
			var document:Document = Application.instance.document;
			var result:Vector.<AbstractObject> = new Vector.<AbstractObject>();
			var objectPropStorage:Object = emptyObjectPropStorages(null);
            var objectDefinitions:Object = objectDefinitionStorage(json);
			var initialValues:Object;
			var currentValues:Object;

            var buildoutCount:int = estimateBuildoutCount(json);

            startThread(buildoutCount, [
                function():void {
                    ToggleButtonGroup.startDeserializing();
                },
                function():void {
			
                    var i:int;
                    var length:int = json.children.length;
                    for (i = 0; i < length; i++) {
                        initialValues = json.initialValues.children[i];
                        var child:AbstractObject = createChild(objectDefinitions, initialValues, i, parent, objects, [], setComplete, currentValues, objectPropStorage, index?index:-1);
                        result.push(child);
                    }
                },
                function():void {
                    ToggleButtonGroup.stopDeserializing();
                    setObjectProps(objectPropStorage, objects, ApplicationController.instance.authorController.world);
                    if(withWires) {
                        buildWiresFromJson(json.wiring, document.wiringRoot.value as Wiring, objects);
                    }
                },
                function():void {
                    // don't set zIndex here ... do it in ClipboardController with bindings on, in case StackOrder ribbon is wired
                    if(postProcess) {
                        postProcess(result);
                    }
                }
            ], !setComplete);
		}

        public static function getClipboardAssets(json:Object):Vector.<Asset>
        {
            var result:Vector.<Asset> = new <Asset>[];
            if ("assets" in json) {
                for each (var assetJson:Object in json.assets) {
                    var asset:Asset = AssetController.instance.requestForID(assetJson.id);
                    if (asset) {
                        result.push(asset);
                    }
                }
            }
            return result;
        }

        public static function getClipboardGadgets(json:Object):Vector.<Project>
        {
            var result:Vector.<Project> = new <Project>[];
            if ("gadgets" in json) {
                for each (var gadgetJson:Object in json.gadgets) {
                    var gadget:Project = GadgetController.instance.requestForID(gadgetJson.id);
                    if (gadget) {
                        result.push(gadget);
                    }
                }
            }
            return result;
        }

		public static function makeUndeleteContent(json:Object, completionCallback:Function=null):void
		{
			var objects:Dictionary = new Dictionary();
			var world:World = ApplicationController.instance.authorController.world;
			var objectPropStorage:Object = emptyObjectPropStorages(null);
            var objectDefinitions:Object = objectDefinitionStorage(json);
            var buildoutCount:int = estimateBuildoutCount(json);
            var topLevelObjects:Vector.<AbstractObject> = new <AbstractObject>[];
			
            startThread(buildoutCount, [
                function():void {
                    ToggleButtonGroup.startDeserializing();
                },
                function():void {
                    var i:int;
                    var length:int = json.children.length;
                    for (i = 0; i < length; i++) {
                        var parent:AbstractContainer = world.findObjByUID(json.parents[i]) as AbstractContainer;
                        if (parent == null)
                            parent = world;
                        var path:Object = json.paths[i];
//                        trace("makeUndeleteContent parent="+parent, "path="+path);

                        var obj:AbstractObject = createChild(objectDefinitions, json.initialValues.children[i], i, parent, objects, [], true,
                                json.currentValues.children[i], objectPropStorage, path, false);
                        if (path is Number) {
                            parent.setChildrenZPositions(true);
                        }
                        topLevelObjects.push(obj);
			        }
                },
                function():void {
                    ToggleButtonGroup.stopDeserializing();
                    setObjectProps(objectPropStorage, objects, world);
                    buildWiresFromJson(json.wiring, Application.instance.document.wiringRoot.value as Wiring, objects);
                    for each (var ao:AbstractObject in topLevelObjects) {
                        PagerController.instance.activateObject(ao, true);
                        var ep:EventPage = ao as EventPage;
                        if (ep) {
                            if (ep.needsInternals() && !ep.hasInternals) {
                                ep.loadInternals();    // uses PseudoThread
                            }
                        }
                    }
                    if (completionCallback != null) {
                        PseudoThread.add(new PseudoThreadSimpleRunnable("makeUndeleteContent.completion", completionCallback));
                    }
                }
            ]);
		}
		
		public static function createWorld(world:World, gadget:Project, wiring:Wiring, state:GadgetState=null, postProcess:Function=null):void
		{
            AuthorController.instance.log("GadgetDecoder:  gadget="+gadget);
            ApplicationController.instance.wireController.bindingsEnabled = false;
			//PerformanceTimer.begin("createWorld");
			var currentValues:Object;
			if (state != null)
				currentValues = ("initialValues" in state)?state.initialValues:null;

			var objects:Dictionary = new Dictionary();
			var objectPropStorage:Object = emptyObjectPropStorages(null);
            var objectDefinitions:Object = objectDefinitionStorage(gadget.content);
            var buildoutCount:int = estimateBuildoutCount(gadget.content);

            startThread(buildoutCount, [
                function():void {
                    ToggleButtonGroup.startDeserializing();
                },
                function():void {
                    buildOutObject(world,
                        [],
                        gadget.initialValues,
                        objectDefinitions,
                        objects,
                        [],
                        true,
                        currentValues,
                        objectPropStorage,
						null,
						true);  // newUIDs
                },
                function():void {
                    ToggleButtonGroup.stopDeserializing();
                    setObjectProps(objectPropStorage, objects, world);
                    buildWiresFromJson(gadget.content.wiring, wiring, objects);

                    if (state && state.scripts != null) {
                        gadget.scripts = new TestPanel();
                        gadget.scripts.loadFromJson(state.scripts);
                    }
                    ApplicationController.instance.wireController.bindingsEnabled = true;
				},
                function():void {
                    if ("usageCounts" in gadget.content) {
                        world.eventFlow.loadUsageCountsJson(gadget.content.usageCounts);
                    }
                    if((postProcess != null)) {
                        postProcess();
                    }
                }
            ]);
		}

        private static function startThread(buildoutCount:int, functions:Array, startImmediately:Boolean = false):void {

//            if (GREEN_THREAD_ENABLED == false) {
//                threadingStart(null);
//                for each(var func:Function in functions) {
//                    func();
//                }
//                threadingComplete(null);
//                return;
//            }

            var runnables:Vector.<PseudoThreadRunnable> = new Vector.<PseudoThreadRunnable>();
            var r:PseudoThreadRunnable;

            for each(var f:Object in functions) {
                r = f as PseudoThreadRunnable;
                if(r == null && f is Function) {
                    r = new PseudoThreadSimpleRunnable("GadgetDecoder::startThread AutoRunnable", f as Function);
                }
			    runnables.push(r);
            }
            var disableInputToken:Object;
            var holdAssetsToken:Object;
            runnables.unshift(new PseudoThreadSimpleRunnable("Begin decoding", function():void {
                disableInputToken = ApplicationController.instance.disableInput("GadgetDecoder::startThread");
    			holdAssetsToken = AssetLoadOperation.hold("GadgetDecoder::startThread");
            }));
            var group:PseudoThreadDebugRunnable = new PseudoThreadDebugRunnable("GadgetDecoder::startThread", true);
            runnables.unshift(group);
            runnables.push(new PseudoThreadSimpleRunnable("Finish decoding", function():void {
                ApplicationController.instance.enableInput(disableInputToken);
                disableInputToken = null;
                AssetLoadOperation.unhold(holdAssetsToken);
                holdAssetsToken = null;
            }));
            runnables.push(group.end);
            if(startImmediately) {
                for each(r in runnables) {
                    r.run();
                }
            } else {
//                PseudoThread.instance.addEventListener(PseudoThreadEvent.THREAD_START, threadingStart);
//                PseudoThread.instance.addEventListener(PseudoThreadEvent.THREAD_PROGRESS, threadingProgress);
//                PseudoThread.instance.addEventListener(PseudoThreadEvent.THREAD_COMPLETE, threadingComplete);
                PseudoThread.addAll(runnables);
            }
        }
		
//		private static function threadingStart(e:PseudoThreadEvent):void
//		{
//			//PerformanceTimer.begin("thread");
//			//trace("THREADING::start");
//            if(e && e.context.total < 30) {
//                return;
//            }
//            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.OBJECT_BUILD_OUT_START, "Creating Objects..."));
//		}
//		private static function threadingProgress(e:PseudoThreadEvent):void
//		{
////			trace("THREADING::progress="+e.context.progressString+" ("+e.context.executeCounter+" out of "+e.context.runnables.length+")"+ " estimated " + e.context.estimatedCount);
//            if(e.context.total < 30) {
//                return;
//            }
//			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.OBJECT_BUILD_OUT_UPDATE, "Creating Objects "+ e.context.progressString+" ("+e.context.executeCounter+" out of "+e.context.total+")"));
//		}
//		private static function threadingComplete(e:PseudoThreadEvent):void
//		{
//			//PerformanceTimer.end("thread");
//			//PerformanceTimer.end("createWorld");
//			PseudoThread.instance.removeEventListener(PseudoThreadEvent.THREAD_START, threadingStart);
//			PseudoThread.instance.removeEventListener(PseudoThreadEvent.THREAD_PROGRESS, threadingProgress);
//			PseudoThread.instance.removeEventListener(PseudoThreadEvent.THREAD_COMPLETE, threadingComplete);
//			//trace("THREADING::complete");
//			//PerformanceTimer.end("loadProject");
//			//PerformanceTimer.dump();˙
//			//PerformanceTimer.clear();
//
//			AssetLoadOperation.holding = false;
//
//            if(e && e.context.total < 30) {
//                return;
//            }
//			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.OBJECT_BUILD_OUT_UPDATE, "Creating Objects: 100%"));
//		}

    /**
     * estimate the number of objects in json content.
     * Includes objects in nested gadgets, but doesn't look into EventPage objects.
     * So calling this on the root gadget will count only the backstage objects.
     * @param json
     * @return number of objects.
     */
        public static function estimateBuildoutCount(json:Object):int {
            if(json == null) {
                trace("No content for estimateBuildoutCount");
                return 0;
            }
            var result:int = 0;
            var gadgetStack:Array = [json];
            while(gadgetStack.length > 0) {
                var objectStack:Array = [gadgetStack.pop()];
                while(objectStack.length > 0) {
                    var o:Object = objectStack.pop();
                    var child:Object;
                    if("children" in o) {
                        for each(child in o.children) {
                            objectStack.push(child);
                        }
                    }
                    if ("valueChildrenLists" in o) {
                        for each (var list:Object in o.valueChildrenLists) {
                            for each(child in list) {
                                if (("className" in child) && child.className == "EventPage") {
//                                    trace("skip eventPage");
                                } else {
                                    objectStack.push(child);
                                }
                            }
                        }
                    }
                    if ("valueChildren" in o) {
                        for each(child in o.valueChildren) {
                            objectStack.push(child);
                        }
                    }
                    if("gadget" in o) {
                        var nextG:Project = o.gadget as Project;
                        if(nextG != null) {  // actual Project not json   (seems to never occur)
                            if (nextG.content != null) {
                                gadgetStack.push(nextG.content);
                            }
                        } else if(o.gadget != null && "id" in o.gadget) {
                            var id:String = o.gadget.id as String;
                            if (id != null) {
                                nextG = GadgetController.instance.getExistingGadget(id);
                                if (nextG != null && nextG.content != null) {
                                    gadgetStack.push(nextG.content);
                                }
                            }
                        }
                    }
                    result++;
                }
            }
            return result;

        }

		public static function buildOutComposite(smart:SmartObject, gadget:Project, gadgetData:GadgetData, constructSelf:Boolean,  constructChildren:Boolean, postProcess:Function=null):void
		{
            var buildOutFunction:Function;

//            trace("buildOutComposite constructSelf="+constructSelf, "constructChildren="+constructChildren, smart);
            // pick the correct function that will be called to build this object
            // commented out conditions are implicitly true
            if(constructSelf && constructChildren) {
                buildOutFunction = buildOutObject;
            } else if(constructSelf /*&& !constructChildren*/) {
                buildOutFunction = buildOutObjectSelfOnly;
            } else if(/*!constructSelf &&*/ constructChildren) {
                buildOutFunction = buildOutObjectChildrenOnly;
            } else /*if(!constructSelf && !constructChildren)*/ {
                throw new Error("Must construct either self or children or both");
            }

			var initialValues:Object = gadgetData.initialValues;
            var anchorInfo:Object = gadgetData.anchorInfo;
			var setComplete:Boolean = gadgetData.setComplete;
			var currentValues:Object = gadgetData.currentValues;
			var outsideObjectPropStorages:Object = gadgetData.objectPropStorage;
			var overriddenValues:Object = gadgetData.overriddenValues;
			var newUID:Boolean = gadgetData.newUID;
			
			var objects:Dictionary = new Dictionary();
            if(!constructSelf) {
                objects[[].join(',')] = smart;
                // since we are not constructing this object
                // we need to manually put it's id in the dictionary
            }
			var objectPropStorages:Object = emptyObjectPropStorages(outsideObjectPropStorages);
            var objectDefinitions:Object = objectDefinitionStorage(gadget.content);
			var anchors:Array = [];
			if(gadget.content.edgeAnchors is Array) {
				anchors = anchors.concat(gadget.content.edgeAnchors)
			}
            if(anchorInfo == null) {
                anchorInfo = {
                    mcVisible:[],
                    inspectorVisible:[],
                    customAnchors:[],
                    inletKeys:{}
                };
            }
			if(anchorInfo.customAnchors is Array) {
                anchorInfo.customAnchors = anchors.concat(anchorInfo.customAnchors)
			}

			var initial:Object = initialValues?initialValues:gadget.initialValues;
			var current:Object = currentValues?currentValues:initial;


			maybeThreaded("ToggleButtonGroup.startDeserializing()", function():void {
                ToggleButtonGroup.startDeserializing();
            });
            var buildOutArguments:Array = [
                smart,
                anchorInfo,
                initial,
                objectDefinitions,
                objects,
                [],
                setComplete,
                current,
                objectPropStorages,
                overriddenValues,
                newUID
            ];
			maybeThreaded("buildOutFunction", buildOutFunction, null, buildOutArguments);
            maybeThreaded("finish Gadget", function():void {
                if(anchors.every(function(element:*, index:int, arr:Array):Boolean{
                    return element.kind != "setter" || element.customModifier != "show";
                })) {
                    smart.controller.createInitialRibbons(smart);
                }
                ToggleButtonGroup.stopDeserializing();
                if(constructChildren) {
                    buildWiresFromJson(gadget.content.wiring, Application.instance.document.wiringRoot.value as Wiring, objects);
                    if(smart is EventPage) {
                        EventPage(smart).hasInternals = true;
                        EventPage(smart).cachedGadgetData = null;
                    }
                } else {
                    if(smart is EventPage) {
                        EventPage(smart).cachedGadgetData = gadgetData;
                    }
                }
                setObjectProps(objectPropStorages, objects, smart);

                // composites adopt their initial viewbox from the gadget scaling settings
                if (gadget.width > 0 && gadget.height > 0)
                    smart.viewBox = new Rectangle(0, 0, gadget.width, gadget.height);
            });
            if(postProcess != null) {
                maybeThreaded("postProcess", postProcess);
            }
		}

        private static function fixInitialValues(obj:Object, objectPropStorage:Object):Object {
            for(var prop:String in obj) {
                if(obj[prop] != null && obj[prop].hasOwnProperty("type") && obj[prop]["type"] == "AbstractObjectReference") {
                    var id:Object = obj[prop]["id"];
                    if(objectPropStorage != null) {
                        if(prop in objectPropStorage == false) {
                            objectPropStorage[prop] = new Dictionary();
                        }
                        objectPropStorage[prop][obj] = id;
                    }
                } else if(obj[prop] is Object) {
                    fixInitialValues(obj[prop],  objectPropStorage);
                }
            }
            return obj;
        }

        private static function anchorDepth(key:String):Number {
            var parts:Array = key.split(";");
            return (parts.length - 1)/2;
        }
		
		private static function buildOutObjectPrerun(obj:AbstractObject, anchorInfo:Object, initialValues:Object, objectDefinitions:Object,  objects:Dictionary, id:Array, setComplete:Boolean, currentValues:Object, objectPropStorages:Object, overriddenValues:Object=null, newUID:Boolean=true):void
		{
//			trace("buildOutObjectPrerun obj="+obj);
			var myCurrentPropStorage:Object = (obj as SmartObject)?objectPropStorages.parent.current:objectPropStorages.current;
			var myInitialPropStorage:Object = (obj as SmartObject)?objectPropStorages.parent.initial:objectPropStorages.initial;
            var childAnchors:Array = anchorInfo.customAnchors as Array;
            if (childAnchors == null) childAnchors = [];

			childAnchors.sort(function(lhs:Object,  rhs:Object):Number {
				var order:Array = ["pathNode","edge", "var","calc","setter","trigger"];
				var l:int = order.indexOf(lhs.kind);
				var r:int = order.indexOf(rhs.kind);
				if(r < 0) return -1;
				if(l < 0) return 1;
				if(l < r) return -1;
				if(r < l) return 1;

				if(!("key" in lhs))
				{
					return -1;	
				}
				if(!("key" in rhs))
				{
					return 1;	
				}
                var l2:Number = anchorDepth(lhs.key);
                var r2:Number = anchorDepth(rhs.key);
                if(l2 < r2) return -1;
                if(r2 < l2) return 1;
				return 0;
			});
			var a:Object;
			for each(a in childAnchors)
				serializer.deserializeWireAnchor(a, obj, -1, newUID, myCurrentPropStorage);

			var current:Object = currentValues?currentValues.values:initialValues.values;
			serializer.copyDeserializedToAbstractObject(current, obj, newUID, myCurrentPropStorage);

            var anchorCurrent:Object;
            var key:String;
            var anchor:WireAnchor;

            if(currentValues) {
                if("anchorValues" in currentValues) {
                    anchorCurrent = currentValues.anchorValues;
                }
            } else {
                if("anchorValues" in initialValues) {
                    anchorCurrent = initialValues.anchorValues;
                }
            }
            if(anchorCurrent) {
                for(key in anchorCurrent) {
                    anchor = obj.anchors[key] as WireAnchor;
                    if(anchor) {
                        anchor.setCurrentValues(anchorCurrent[key], myCurrentPropStorage);
                    }
                }
            }

            if(initialValues == null) {
                var js:String = "alert('Error loading gadget please Greg, mention this message and tell him what you were doing.');";
                navigateToURL(new URLRequest("javascript:"+js+"void(0);"));
                throw new Error("Corrupted object initialValues " + obj.title);
            }
			obj.initialValues = fixInitialValues(ObjectUtil.copy(initialValues.values), myInitialPropStorage);
            obj.initialValues.uid = obj.uid;

            if("anchorValues" in initialValues) {
                for(key in initialValues.anchorValues) {
                    anchor = obj.anchors[key] as WireAnchor;
                    if(anchor) {
                        anchor.initialValues = fixInitialValues(anchorCurrent[key], myInitialPropStorage);
                    }
                }
            }
            var location:String = obj.serializedLocation;
            if(location == null || obj is SmartObject) {
                // smart objects are a root, so for this objects dictionary
                // we don't want the serializedLocation within it's parent
                // that will be used in the line after creating a smart object in createChild
                location =id.join(",");
            }
			objects[location] = obj;

            for each (var anchorObj:Object in obj.anchors) {
                var anchor:WireAnchor = anchorObj as WireAnchor;
                if(anchor == null)
                    continue;
                if (anchorInfo.mcVisible as Array && anchorInfo.mcVisible.indexOf(anchor.modifierDescription.key) != -1)
                    anchor.messageCenterVisible = true;
                if (anchorInfo.inspectorVisible as Array && anchorInfo.inspectorVisible.indexOf(anchor.modifierDescription.key) != -1)
                    anchor.propertyInspectorVisible = true;
                if (anchorInfo.inletKeys && anchor.modifierDescription.key in anchorInfo.inletKeys)
                    anchor.keyPress = anchorInfo.inletKeys[anchor.modifierDescription.key];
            }
		}


		private static function buildOutObjectRun(obj:AbstractObject, anchorInfo:Object, initialValues:Object, objectDefinitions:Object,  objects:Dictionary, id:Array, setComplete:Boolean, currentValues:Object, objectPropStorages:Object, overriddenValues:Object=null, newUID:Boolean=true):void
		{
            var children:Array = initialValues.hasOwnProperty("children") ? initialValues.children : [];
            for (var childrenIndex:int = 0; childrenIndex < children.length; childrenIndex++) {
                var childInitialValues:Object = initialValues.children[childrenIndex];
                var childCurrentValues:Object;
                if (currentValues && currentValues.children && currentValues.children.length > childrenIndex)
                    childCurrentValues = currentValues.children[childrenIndex];

                createChild(objectDefinitions, childInitialValues, childrenIndex, obj as AbstractContainer, objects, id, setComplete, childCurrentValues, objectPropStorages, -1, newUID);
            }

            var valueChildren:Object = initialValues.hasOwnProperty("valueChildren") ? initialValues.valueChildren : {};
            for(var valueProp:String in valueChildren) {
                if(valueChildren.hasOwnProperty(valueProp)) {
                    var valueInitialValues:Object = initialValues.valueChildren[valueProp];
                    var valueCurrentValues:Object;
                    if (currentValues && "valueChildren" in currentValues && valueProp in currentValues.valueChildren) {
                        valueCurrentValues = currentValues.valueChildren[valueProp];
                    }
                    if(valueProp in valueChildren) {
                        createChild(objectDefinitions, valueInitialValues, valueProp, obj as AbstractContainer, objects, id, setComplete, valueCurrentValues, objectPropStorages, valueProp, newUID);
                    }
                }
            }

            var valueChildrenLists:Object = initialValues.hasOwnProperty("valueChildrenLists") ? initialValues.valueChildrenLists : {};
            for(var valueListProp:String in valueChildrenLists) {
                if(valueChildrenLists.hasOwnProperty(valueListProp)) {
                    var valueListsInitialValues:Object = initialValues.valueChildrenLists[valueListProp];
                    var valueListsCurrentValues:Object;
                    if (currentValues && "valueChildrenLists" in currentValues && valueListProp in currentValues.valueChildrenLists) {
                        valueListsCurrentValues = currentValues.valueChildrenLists[valueListProp];
                    }
                    obj[valueListProp] = new ArrayCollection();
                    for(var valueListIndex:int=0; valueListIndex < valueListsInitialValues.length; valueListIndex++) {
                        var valueListChildInitialValues:Object = valueListsInitialValues[valueListIndex];
                        var valueListChildCurrentValues:Object = (valueListsCurrentValues != null) ? valueListsCurrentValues[valueListIndex] : null;
                        createChild(objectDefinitions, valueListChildInitialValues, [valueListProp,valueListIndex], obj, objects, id,  setComplete, valueListChildCurrentValues, objectPropStorages, [valueListProp,valueListIndex], newUID);
                    }
                }
            }
		}


		private static function buildOutObjectPostrun(obj:AbstractObject, anchorInfo:Object, initialValues:Object, objectDefinitions:Object, objects:Dictionary, id:Array, setComplete:Boolean, currentValues:Object, objectPropStorages:Object, overriddenValues:Object=null, newUID:Boolean=true):void
		{
//            trace("buildOutObjectPostrun obj="+obj);
			for (var property:String in overriddenValues) {
				if (property in obj) {
					obj[property] = overriddenValues[property];
					obj.initialValues[property] = ObjectUtil.copy(overriddenValues[property]);
				}
			}
			
			obj.loading = false;
			if (setComplete) {
				obj.complete = true;
			}
		}

        private static function descBuild(obj:AbstractObject, initialValues:Object):String {
            return " ["+obj.shortClassName + " : '" + initialValues.values.title + "' " + initialValues.values.serializedLocation + "]"
        }

        private static function buildOutObject(obj:AbstractObject, anchorInfo:Object, initialValues:Object, objectDefinitions:Object, objects:Dictionary, id:Array, setComplete:Boolean, currentValues:Object, objectPropStorages:Object, overriddenValues:Object=null, newUID:Boolean=true):void
        {
            var desc:String = descBuild(obj, initialValues);
            var group:PseudoThreadDebugRunnable = new PseudoThreadDebugRunnable("buildOutObject"+desc, true);
            if(PseudoThread.running()) {
                PseudoThread.add(group);
            }
            maybeThreaded("buildOutObjectPrerun"+desc, buildOutObjectPrerun, null, arguments);
            maybeThreaded("buildOutObjectRun"+desc, buildOutObjectRun, null, arguments);
            maybeThreaded("buildOutObjectPostrun"+desc, buildOutObjectPostrun, null, arguments);
            if(PseudoThread.running()) {
                PseudoThread.add(group.end);
            }
        }

        private static function buildOutObjectSelfOnly(obj:AbstractObject, anchorInfo:Object, initialValues:Object, objectDefinitions:Object, objects:Dictionary, id:Array, setComplete:Boolean, currentValues:Object, objectPropStorages:Object, overriddenValues:Object=null, newUID:Boolean=true):void
        {
            var desc:String = descBuild(obj, initialValues);
            var group:PseudoThreadDebugRunnable = new PseudoThreadDebugRunnable("buildOutObjectSelfOnly"+desc, true);
            if(PseudoThread.running()) {
                PseudoThread.add(group);
            }
            maybeThreaded("buildOutObjectPrerun"+desc, buildOutObjectPrerun, null, arguments);
            maybeThreaded("buildOutObjectPostrun"+desc, buildOutObjectPostrun, null, arguments);
            if(PseudoThread.running()) {
                PseudoThread.add(group.end);
            }
        }

        private static function buildOutObjectChildrenOnly(obj:AbstractObject, anchorInfo:Object, initialValues:Object, objectDefinitions:Object, objects:Dictionary, id:Array, setComplete:Boolean, currentValues:Object, objectPropStorages:Object, overriddenValues:Object=null, newUID:Boolean=true):void
        {
            var desc:String = descBuild(obj, initialValues);
            var group:PseudoThreadDebugRunnable = new PseudoThreadDebugRunnable("buildOutObjectChildrenOnly"+desc, true);
            if(PseudoThread.running()) {
                PseudoThread.add(group);
            }
            maybeThreaded("buildOutObjectRun"+desc, buildOutObjectRun, null, arguments);
            if(PseudoThread.running()) {
                PseudoThread.add(group.end);
            }
        }

        private static function maybeThreaded(name:String, f:Function, thisarg:Object=null, args:Array=null):void {
            if(GREEN_THREAD_ENABLED && PseudoThread.running()) {
               PseudoThread.instance.context.add(new PseudoThreadSimpleRunnable("GadgetDecoder::maybeThreaded " + name, f,  thisarg, args));
            } else {
                f.apply(thisarg, args);
            }
        }

		private static function createChild(objectDefinitions:Object, initialValues:Object, index:Object, parent:AbstractObject, objects:Dictionary, parentId:Array, setComplete:Boolean, currentValues:Object, objectPropStorages:Object, zIndex:Object=-1, newUID:Boolean=true):AbstractObject
		{
            var definitionId:String = parentId.concat(index).join(",");
            if(initialValues != null && initialValues.hasOwnProperty("values")) {
                if(initialValues.values.hasOwnProperty("serializedLocation") && initialValues.values.serializedLocation is String) {
                    definitionId = initialValues.values.serializedLocation;
                }
            }
            var json:Object = objectDefinitions[definitionId];
            // persist whether or not ribbons always show
            var mcVisible:Array = null;
            var inspectorVisible:Array = null;
            var customAnchors:Array = null;
            var inletKeys:Object = null;
            if(json != null){  //New gadgets (created or brought in from the cloud) this stuff is handled by the gadget when it is built
                if ("mcVisible" in json) {
                    mcVisible = json.mcVisible as Array;
                }
                if ("inspectorVisible" in json) {
                    inspectorVisible = json.inspectorVisible as Array;
                }
                if ("customAnchors" in json) {
                    customAnchors = json.customAnchors;
                }
                if ("inletKeys" in json) {
                    inletKeys = json.inletKeys;
                }

                var anchorInfo:Object = GadgetEncoder.packAnchorInfo(mcVisible,inspectorVisible,customAnchors,inletKeys);

                if (json.className == "Composite" || json.className == "EventPage") {
                    if ("gadget" in json && json.gadget != null) {
                        var gadgetData:GadgetData = new GadgetData(parent, initialValues, anchorInfo, setComplete, currentValues, objectPropStorages, newUID);
                        var smart:SmartObject;
                        if(json.gadget is Project) {
                            smart = GadgetController.instance.requestForProjectAndCreateContainer(json.gadget as Project, -1, json.className, gadgetData, zIndex);
                        } else {
                            smart = GadgetController.instance.requestForIDAndCreateContainer(json.gadget.id, -1, json.className, gadgetData, zIndex);
                        }
                        objects[definitionId] = smart;
                        return smart;
                    }
                }
            }
//            trace("@ createChild class="+json.className);
			var child:AbstractObject = ObjectFactory.loadForName(json.className, parent, zIndex);
			buildOutObject(child, anchorInfo, initialValues, objectDefinitions, objects, parentId.concat(index), setComplete, currentValues, objectPropStorages, null, newUID);

            return child;
		}
		
		private static function setObjectProps(objectPropStorages:Object, objects:Dictionary, root:AbstractContainer):void
		{
            for each(var kind:String in ["current", "initial"]) {
                var objectPropStorage:Object = objectPropStorages[kind];
                var json:Boolean = kind == "initial";
                for(var prop:String in objectPropStorage) {
                    for (var key:Object in objectPropStorage[prop]) {
                        var val:Object = objectPropStorage[prop][key];
                        setObjectProp(key, val, prop, objects, root, json);
                    }
                }
            }
		}
		
		private static function setObjectProp(key:Object, val:Object, prop:Object, objects:Dictionary, root:AbstractContainer, json:Boolean):void
		{	
			var id:String;
			if(val as Array) {
				id = val.join(",")
			} else {
				id = val as String;
			}
			var target:AbstractObject;
			if(id in objects) {
				target = objects[id];
			} else {
				target = root.findObjByUID(id);
			}
			if(target == null) {
				key[prop] = null;
			} else if(json) {
				key[prop] = target.referenceJSON;
			} else {
				key[prop] = target;
			}
		}
		
		public static function emptyObjectPropStorages(parent:Object):Object
		{
			return {
                parent: parent?parent:{},
                current: {},
                initial: {}
            };
		}

        public static function objectDefinitionStorage(content:Object):Object
        {
            var result:Object = {};

            var stack:Vector.<Object> = new <Object>[{content:content, id:[]}];

            while(stack.length > 0) {
                var current:Object = stack.pop();

                result[current.id.join(",")] = current.content;

                var children:Array = current.content.hasOwnProperty("children") ? current.content.children : [];
                for (var childrenIndex:int = 0; childrenIndex < children.length; childrenIndex++) {
                    stack.push({content:children[childrenIndex], id:current.id.concat(childrenIndex)});
                }

                var valueChildren:Object = current.content.hasOwnProperty("valueChildren") ? current.content.valueChildren : {};
                for(var valueProp:String in valueChildren) {
                    if(valueChildren.hasOwnProperty(valueProp)) {
                        stack.push({content:valueChildren[valueProp], id:current.id.concat(valueProp)});
                    }
                }

                var valueChildrenLists:Object = current.content.hasOwnProperty("valueChildrenLists") ? current.content.valueChildrenLists : {};
                for(var valueListProp:String in valueChildrenLists) {
                    if(valueChildrenLists.hasOwnProperty(valueListProp)) {
                        var valueList:Array = valueChildrenLists[valueListProp] as Array;
                        var valueListId:Array = current.id.concat(valueListProp);
                        for(var valueListIndex:int=0; valueListIndex < valueList.length; valueListIndex++) {
                            stack.push({content:valueList[valueListIndex], id:valueListId.concat(valueListIndex)});
                        }
                    }
                }
            }
            return result;
        }
		
		private static function buildWiresFromJson(wires:Array, wiringRoot:Wiring, objects:Dictionary):void
		{
			if (!wires)
				return;
			
			for each (var w:Object in wires) {
				var model:TaconiteModel = serializer.deserializeWire(w, wiringRoot, -1, objects);
				if(model != null) {
					var wire:Wire = model.value as Wire;
					wire.fireCompleteEvent(true);
				}
			}
			ApplicationController.instance.wireController.showWiresForLevel();
		}
		
		private static function get serializer():SerializerImplementation
		{
			if (!_serializer)
				_serializer = new SerializerImplementation();
			return _serializer;
		}
	}
}
