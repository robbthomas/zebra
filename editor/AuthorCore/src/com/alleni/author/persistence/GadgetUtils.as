/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 10/11/12
 * Time: 12:07 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.persistence {
import assets.icons.messageCenter.gadgetEditBlack;

import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.project.Project;

public class GadgetUtils {

    public static function validateLiveObjects(objects:Vector.<AbstractObject>, gadget:Project):String
    {
        for each (var obj:AbstractObject in objects) {

            var error:String = validateInitialZIndex(obj);
            if (error != null) {
                return error;
            }

            var error:String = validateSerializedLocation(obj, gadget);
            if (error != null) {
                return error;
            }

            var smart:SmartObject = obj as SmartObject;
            if (smart) {
                error = validateLiveObjects(smart.allObjectsInWiringScope(), smart.gadget);
                if (error != null) {
                    return error;
                }
            }
        }
        return null;
    }

    private static function validateInitialZIndex(obj:AbstractObject):String
    {
        var objIsPage:Boolean = (obj is ArenaPage || obj is EventPage || obj is EventFlow);
        if (!objIsPage) {
            // objects other than pages should have zIndex
            if (!("zIndex" in obj.initialValues)) {
                return ("missing zIndex init obj="+obj.title);
            }
            if ("parent" in obj.initialValues) {   // parent is optional
                var initPar:AbstractContainer = obj.getInitialParent(AuthorController.instance.world);
                if (initPar == null) {
                    return ("can't find init parent obj="+obj.title);
                } else {
//                    trace("found initial parent for "+obj.title);
                }
            }
        }
        return null;
    }

    public static function validateSerializedLocation(obj:AbstractObject, gadget:Project):String {
        if (gadget == null || gadget.content == null) {
            return null;
        }
        var blueprint:Object = gadget.content;
        var blue:Object = null;
        if (obj.serializedLocation == null) {  // serializedLocation is created on project-save so doesn't exist for new objects.  Or for backstage objects.
            return null;  // brand-new object has no blueprint
        } else {
            blue = getJsonChildByPath(obj.serializedLocation, blueprint);
        }
        if (blue == null) {
            return "Can't find blueprint for " + jsonChildToString(null, obj);
        }
        var blueprintClassname:Object = blue.className;
        var className:String = obj.shortClassName;
        if (className == blueprintClassname) {
//            trace("valid serializedLocation obj: "+obj.title, obj.shortClassName, " loc="+obj.serializedLocation);
            return null;
        } else {
            return "Invalid obj.serializedLocation: blueprintClass="+blueprintClassname + " objClass="+className + " obj="+obj.title;
        }
    }

     private static function getJsonChildByPath(path:String, blueprint:Object):Object {
         var blue:Object = blueprint;
         var location:Array = path.split(",");
         var result:Object = blueprint;
         var n:int = 0;
         while (n < location.length) {
             var token:String = location[n++];  // n++ also done below
             var listName:String;
             var index:int = -1;
             var array:Array = null;
             const ALL_DIGITS:RegExp = /\d+$/;
             if (token.match(ALL_DIGITS)) {
                 index = int(token);
                 array = blue["children"] as Array;
             } else if ("valueChildrenLists" in blue) {
                 listName = token;
                 token = location[n++];
                 index = int(token);
                 array = blue.valueChildrenLists[listName] as Array;
             }
             if (array && index >= 0 && index < array.length) {
                 blue = array[index];
             } else {
                 return null;  // no such list or element
             }
         }
         return blue;
     }

    /**
     * Dump gadget.content or initialValues or currentValues.
     * @param json
     * @param indent
     * @param listRootObj
     */
     public static function collectChildGadgets(json:Object, indent:String = "", listRootObj:Boolean=false):Array {
        var result:Array = [];
         if (json == null) {
             return result;
         }
         const INDENT:String = "  ";
         var child:Object, childGadgets:Array;
         var array:Array;
         if("children" in json) {
             array = json.children;
             if (array && array.length > 0) {
                 for each (child in array) {
                     childGadgets = collectChildGadgets(child, indent+INDENT+INDENT, true);
                     result = result.concat(childGadgets);
                 }
             }
         } else if("gadget" in json) {   //  only in blueprint, not in values
             var id:String = json.gadget["id"];
             if (id != null) {
                 result.push(id);
                 var gadget:Project = GadgetController.instance.requestForID(id);
                 if (gadget.content) {
                     childGadgets = collectChildGadgets(gadget.content, indent+INDENT+INDENT, false);
                     result = result.concat(childGadgets);
                 } else {
                     trace(indent + INDENT+ INDENT + "missingGadgetContentForID="+id);
                 }
             }
         }

        if ("valueChildrenLists" in json) {
            for (var listName:String in json.valueChildrenLists) {
                array = json.valueChildrenLists[listName];
                if (array && array.length > 0) {
                    for each(child in array) {
                        childGadgets = collectChildGadgets(child, indent+INDENT+INDENT, true);
                        result = result.concat(childGadgets);
                    }
                }
            }
        }

        if ("valueChildren" in json) {
            for each(child in json.valueChildren) {
                childGadgets = collectChildGadgets(child, indent+INDENT+INDENT, true);
                result = result.concat(childGadgets);
            }
        }
        return result;
     }


    /**
     * Dump gadget.content or initialValues or currentValues.
     * @param json
     * @param indent
     * @param listRootObj
     */
     public static function dumpJsonOutline(json:Object, indent:String = "", listRootObj:Boolean=false):void {
         if (json == null) {
             trace(indent + "json=null");
             return;
         }
         const INDENT:String = "  ";
         var child:Object;
         var array:Array;
         if (listRootObj) {
             dumpJsonChild(indent, json);
         }
         if("children" in json) {
             array = json.children;
             if (array && array.length > 0) {
                 trace(indent + INDENT + "children:");
                 for each (child in array) {
                     dumpJsonOutline(child, indent+INDENT+INDENT, true);
                 }
             }
         } else if("gadget" in json) {   //  only in blueprint, not in values
             var id:String = json.gadget["id"];
             trace(indent + INDENT + "Gadget: ID="+id);
             if (id != null) {
                 var gadget:Project = GadgetController.instance.requestForID(id);
                 if (gadget.content) {
                     dumpJsonOutline(gadget.content, indent+INDENT+INDENT, false);
                 } else {
                     trace(indent + INDENT+ INDENT + "missingGadgetContentForID="+id);
                 }
             }
         }

         if ("valueChildrenLists" in json) {
             for (var listName:String in json.valueChildrenLists) {
                 array = json.valueChildrenLists[listName];
                 if (array && array.length > 0) {
                     trace(indent + INDENT + listName + ":");
                     for each(child in array) {
                         dumpJsonOutline(child, indent+INDENT+INDENT, true);
                     }
                 }
             }
         }

        if ("valueChildren" in json) {
            trace(indent + INDENT + "valueChildren:");
            for each(child in json.valueChildren) {
                dumpJsonOutline(child, indent+INDENT+INDENT, true);
            }
        }

     }

    private static function dumpJsonChild(indent:String, obj:*):void {
         trace(indent + jsonChildToString(null, obj));
    }

    public static function createDebugValuesContent(json:*, properties:String = null):Object {
        if(!("children" in json || "valueChildrenLists" in json || "gadget" in json)) {
            return null;
        }
        var result:Object = new Object();
        var index:String;
        var child:Object;
        var array:Array;
        var resultChildren:Object;
        var child:Object;
        var childStr:String;
        var childObj:Object;
        var props:Array = null;

        if (properties != null) {
            props = properties.split(",");
        }

        if("children" in json) {
            array = json.children;
            if (array && array.length > 0) {
                resultChildren = {};
                for (index in array) {
                    child = array[index];
                    childStr = jsonChildToString(index, child, props);
                    childObj = createDebugValuesContent(child, properties);
                    resultChildren[childStr] = childObj;
                }
                result.children = resultChildren;
            }
        }
        if ("valueChildren" in json) {
            for (var childName:String in json.valueChildren) {
                child = json.valueChildren[childName];
                childStr = jsonChildToString(index, child, props);
                childObj = createDebugValuesContent(child, properties);
                result[childStr] = childObj;
            }
        }
        if ("valueChildrenLists" in json) {
            for (var listName:String in json.valueChildrenLists) {
                array = json.valueChildrenLists[listName];
                resultChildren = {};
                for (index in array) {
                    child = array[index];
                    childStr = jsonChildToString(index, child, props);
                    childObj = createDebugValuesContent(child, properties);
                    resultChildren[childStr] = childObj;
                }
                result[listName] = resultChildren;
            }
        }

        if("gadget" in json) {   //  only in blueprint, not in values
            var id:String = json.gadget["id"];
            if (id != null) {
                var gadget:Project = GadgetController.instance.requestForID(id);
                if (gadget && gadget.content) {
                    resultChildren = createDebugValuesContent(gadget.content, properties);
                    result.gadget = resultChildren;
                }
            }
        }
        return result;
     }


     private static function jsonChildClassName(obj:Object):String {
         if ("className" in obj) {      // in gadget blueprint its className
             return obj.className;
         } else if ("values" in obj && "className" in obj.values) {    // in INITs its called "className"
             return obj.values.className;
         } else if ("values" in obj && "shortClassName" in obj.values) {
             return obj.values.shortClassName;                 // in current-values its "shortClassName"
         } else {
             return "[" + (typeof obj) + "]";
         }
     }

     private static function jsonChildToString(index:String,  obj:*, props:Array = null):String {
         var result:String = ((index == null) ? "" : (index + ": ")) + jsonChildClassName(obj);
         if (props != null && "values" in obj) {
             for each (var propName:String in props) {
                 if (propName in obj.values) {
                     var propValue:* = obj.values[propName];
                     if (propValue is String) {
                         result += " " + propName + '="' + propValue + '"';
                     } else {
                         result += " " + propName + "=" + propValue;
                     }
                 }
             }
         }
         return result;
     }
}
}
