package com.alleni.author.util
{
	import flash.geom.Point;
	
	public class PresetPoint extends Point
	{
		public function PresetPoint(x:Number=0, y:Number=0)
		{
			super(x, y);
		}
		
		override public function toString():String
		{
			return "[PresetPoint " + super.toString() + "]";
		}
		
		public function toJson():Object {
			return {type:"PresetPoint", x:x, y:y};
		}

		public static function fromJson(json:Object):PresetPoint 
		{
			return new PresetPoint(json.x, json.y);
		}
	}
}