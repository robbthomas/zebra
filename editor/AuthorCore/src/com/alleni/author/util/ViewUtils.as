package com.alleni.author.util
{
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.WorldView;
import com.alleni.author.view.ui.WireboardView;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.service.LogService;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Graphics;
import flash.display.PixelSnapping;
import flash.display.Sprite;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;

public class ViewUtils
	{
		private static var _instance:ViewUtils;
        private static const MAX_BITMAPDATA_SIZE:int = 8000;  // leaving a buffer for filters
		

		public function ViewUtils():void
		{
			Utilities.assert(!_instance);	
		}
		
		public static function get instance():ViewUtils
		{
			if (!_instance)
				_instance = new ViewUtils();
			return _instance;
		}

        private function get worldView():WorldView
        {
            return WorldContainer(Application.instance.viewContext).worldView;
        }
		
		public function takeScreenshot(container:DisplayObject, width:int=-1, height:int=-1):BitmapData
		{
			var matrix:Matrix = new Matrix();
			if (width < 0) width = container.width;
			if (height < 0) height = container.height;
			var bmd:BitmapData = new BitmapData(width, height, true, 0x000000ff);
            worldView.makeSafeForCaching = true;
			try {
				bmd.draw(container, matrix, null, null, null, true);
			} catch(e:Error) {
				LogService.error("Security error in ViewUtils::takeScreenshot for container: " + container);
            }
            worldView.makeSafeForCaching = false;
			return bmd;
		}

        /**
         * Capture the worldContainer view, including wireboards, title-bar and event MC.
         * @return
         */
        public function captureEditingWorld():Sprite
        {
            var data:BitmapData = new BitmapData(EditorUI.instance.width, EditorUI.instance.height, true, 0x00ff0000);
            var result:Sprite = new Sprite();

            // replicate the stage background gradient, so the wireboards don't flicker when this bitmap is applied
            const EDGE_WIDTH:Number = WireboardView.WIDTH;
            var background:Sprite = EditorUI.instance.stagingBackground;
            var flow:EventFlow = AuthorController.instance.eventFlow;
            var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
            var topLeft:Point = worldContainer.localToGlobal(new Point(-EDGE_WIDTH,0));
            var botRight:Point = worldContainer.localToGlobal(new Point(flow.width+EDGE_WIDTH,flow.height));
            var extent:Point = botRight.subtract(topLeft);
            var clip:Rectangle = new Rectangle(topLeft.x, topLeft.y, extent.x, extent.y);
            var g:Graphics = result.graphics;
            g.beginFill(0x272727);  // color under the background gradient, by experiment
            g.drawRect(clip.x, clip.y, clip.width, clip.height);
            g.endFill();
            data.draw(background, background.transform.concatenatedMatrix, background.transform.concatenatedColorTransform, null, clip, true);

            // the world content
            worldView.makeSafeForCaching = true;
            data.draw(worldContainer, worldContainer.transform.concatenatedMatrix);
            worldView.makeSafeForCaching = false;

            // result sprite has background under the wireboards
            var bitmap:Bitmap = new Bitmap(data, PixelSnapping.AUTO, true);
            result.addChild(bitmap);
            return result;
        }



        /**
         * Take object screenshot for rollover.  The clipping rectangle is intersected with application bounds.
         * And the bitmap size is limited in case a text object has huge dimensions.
         * @param objectView
         * @return
         */
        public function takeObjectScreenshot(objectView:ObjectView):BitmapData
        {
            var object:AbstractObject = objectView.object;
            var matrix:Matrix = new Matrix();
            matrix.tx = -(object.left);
            matrix.ty = -(object.top);

            // ensure non-zero dimensions
            var width:Number = (object.width > MAX_BITMAPDATA_SIZE)? MAX_BITMAPDATA_SIZE: ((object.width > 1) ? object.width : 1);
            var height:Number = (object.height > MAX_BITMAPDATA_SIZE)? MAX_BITMAPDATA_SIZE: ((object.height > 1) ? object.height : 1);

            var bmd:BitmapData = new BitmapData(width, height, true, 0x000000ff);
            var rect:Rectangle = new Rectangle(object.left,object.top,width, height);
            var container:DisplayObject = (objectView.context as WorldContainer).parent;
            var rect2:Rectangle = container.getBounds(objectView);
            var safe:Boolean;
//            trace("takeObjectScreenshot:: obj", object.width, object.height,  "capped", width, height,  "stage", rect2.x,  rect2.y,  rect2.width,  rect2.height);
            rect =  rect2.intersection(rect);
            rect.offset(-object.left,  -object.top);

            try {
                try {  // try the easy way in case no 3rd party content exists...
                    bmd.draw(objectView, matrix, null, null, rect, true);
                } catch (e:Error) {
                    objectView.makeSafeForCaching = true;  // ... the hard way when 3rd party content does exist
                    safe = true;
                    bmd.draw(objectView, matrix, null, null, rect, true);
                }
            } catch(e:Error) {
                LogService.error("Security error in ViewUtils::takeObjectScreenshot for object: " + objectView);
            }
            if (safe) {
                objectView.makeSafeForCaching = false;
            }
            return bmd;
        }

        /**
         * Take object screenshot based on AbstractObject bounds.  Does not intersect with app bounds.
         * @param objectView
         * @return
         */
        public function takeObjectRectScreenshot(objectView:ObjectView):BitmapData
        {
            var object:AbstractObject = objectView.object;
            var matrix:Matrix = new Matrix();
            matrix.tx = -(object.left);
            matrix.ty = -(object.top);

            // ensure non-zero dimensions
            var width:Number = (object.width > MAX_BITMAPDATA_SIZE)? MAX_BITMAPDATA_SIZE: ((object.width > 0) ? object.width : 1);
            var height:Number = (object.height > MAX_BITMAPDATA_SIZE)? MAX_BITMAPDATA_SIZE: ((object.height > 0) ? object.height : 1);
            var bmd:BitmapData = new BitmapData(width, height, true, 0x000000ff);
            var rect:Rectangle = new Rectangle(object.left,object.top,width, height);
            var safe:Boolean;
//            trace("takeObjectScreenshot:: obj", object.width, object.height,  "capped", width, height,  "stage", rect2.x,  rect2.y,  rect2.width,  rect2.height);
            rect.offset(-object.left,  -object.top);

            try {
                try {  // try the easy way in case no 3rd party content exists...
                    bmd.draw(objectView, matrix, null, null, rect, true);
                } catch (e:Error) {
                    objectView.makeSafeForCaching = true;  // ... the hard way when 3rd party content does exist
                    safe = true;
                    bmd.draw(objectView, matrix, null, null, rect, true);
                }
            } catch(e:Error) {
                LogService.error("Security error in ViewUtils::takeObjectScreenshot for object: " + objectView);
            }
            if (safe) {
                objectView.makeSafeForCaching = false;
            }
            return bmd;
        }

        /**
         * Capture bitmap for Arena object, accounting for the arena being at a fractional-coordinate location.
         * @param view
         * @param rect = area to capture in view local coordinates.
         * @param parentView = prepare the view to be added here, and to end up with exactly same appearance as original view.
         * @return
         */
        public static function captureArenaBitmap(view:ObjectView, rect:Rectangle, parentView:DisplayObjectContainer, fillColor:uint):Sprite
        {
            // for Arena transition effects: account for arena being at fractional-coordinate location...
            // to prevent a one-pixel-shift, we must exactly align the captured bitmap with the physical screen pixels
            // (not a Flash bug, just a hardware reality)
            var parentConcat:Matrix = parentView.transform.concatenatedMatrix;
            if (parentConcat.a == 0 || parentConcat.d == 0) {
                return new Sprite();  // avoid division by zero, at end of func
            }
            var viewConcat:Matrix = view.transform.concatenatedMatrix;

            var matrix:Matrix = new Matrix();
            matrix.scale(viewConcat.a, viewConcat.d);  // make bitmap resolution exactly match the physical screen pixel resolution

            var xOffset:Number = rect.left * viewConcat.a;  // usually a negative number, since anchorPoint is Center
            var yOffset:Number = rect.top * viewConcat.d;   // times viewConcat.scaleY since matrix was scaled above
            var tx:Number = viewConcat.tx + xOffset;   // tx is in stage units
            var ty:Number = viewConcat.ty + yOffset;
            var txFractional:Number = (tx>0) ? tx - Math.floor(tx) : tx - Math.ceil(tx);  // fractional amount (can be negative)
            var tyFractional:Number = (ty>0) ? ty - Math.floor(ty) : ty - Math.ceil(ty);

            matrix.translate(txFractional - xOffset, tyFractional - yOffset);  // offset by fraction minus anchorPoint (matrix is already scaled to stage units)

            var width:Number = rect.width * viewConcat.a + 1;
            var height:Number = rect.height * viewConcat.d + 1;
            width = Math.ceil(Math.max(1, Math.min(MAX_BITMAPDATA_SIZE, width)));
            height = Math.ceil(Math.max(1, Math.min(MAX_BITMAPDATA_SIZE, height)));
            var bmd:BitmapData = new BitmapData(width, height, false, fillColor);    // color fills the fractional pixel due to rounding up width&height
            var appliedMakeSafe:Boolean;
            try {
                try {  // try the easy way in case no 3rd party content exists...
                    bmd.draw(view, matrix, null, null, null, true);
                } catch (e:Error) {
                    view.makeSafeForCaching = true;  // ... the hard way when 3rd party content does exist
                    appliedMakeSafe = true;
                    bmd.draw(view, matrix, null, null, null, true);
                }
            } catch(e:Error) {
                LogService.error("Security error in ViewUtils::captureArenaBitmap for object: " + view);
            }
            if (appliedMakeSafe) {
                view.makeSafeForCaching = false;
            }

            var bitmap:Bitmap = new Bitmap(bmd, PixelSnapping.NEVER, false);
            bitmap.scaleX = 1/parentConcat.a;
            bitmap.scaleY = 1/parentConcat.d;
            bitmap.x = -txFractional / parentConcat.a;
            bitmap.y = -tyFractional / parentConcat.d;

            var sprite:Sprite = new Sprite();
            sprite.addChild(bitmap);
            return sprite;
        }

        /** Notes on captureArenaBitmap:
         *
         * When arena.fillOpacity is less than 100%, this function is called to capture worldView instead of arenaView.
         * The code accounts for arena.zoom scaling and application scaling.
         * Found that localToGlobal didn't give sensible results in this context (eg. verifying the bitmap lands at whole-pixel location).
         * Didn't try to account for rotation.
         * Thanks to Micah Fitch for the basic txFractional algorithm.
         */
	}
}