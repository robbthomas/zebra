/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 1/25/11
 * Time: 7:48 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.cache {
public class LRUNode extends SimpleCacheNode {

	public var prev:LRUNode = null;
	public var next:LRUNode = null;

	public function LRUNode(key:Object, value:Object) {
		super(key, value)
	}

    public function toString():String
    {
        return "[LRUNode " + key + "]";
    }
}
}
