/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 1/25/11
 * Time: 7:23 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.cache {
import flash.utils.getTimer;

public class SimpleCacheNode implements CacheNode {

	private var _key:Object;
	private var _value:Object;

	public function SimpleCacheNode(key:Object, value:Object) {
		_key = key;
		_value = value;
	}

	public function get key():Object {
		return _key;
	}

	public function get value():Object {
		return _value;
	}

	public function set value(value:Object):void {
		_value = value
	}
}
}
