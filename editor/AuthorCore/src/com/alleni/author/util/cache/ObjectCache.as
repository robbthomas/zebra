/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 1/25/11
 * Time: 6:51 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.cache {
public interface ObjectCache {
	function setValue(key:Object, value:Object):void;
	function getValue(key:Object):Object;
	function get length():int;
}
}
