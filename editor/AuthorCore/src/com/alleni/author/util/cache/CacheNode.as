/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 1/25/11
 * Time: 6:52 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.cache {
public interface CacheNode {

	function get key():Object;
	function get value():Object;
	function set value(value:Object):void;
}
}
