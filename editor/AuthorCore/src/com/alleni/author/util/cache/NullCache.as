/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 1/25/11
 * Time: 7:07 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.cache {
public class NullCache implements ObjectCache {
	public function NullCache() {
	}

	public function setValue(key:Object, value:Object):void {
	}

	public function getValue(key:Object):Object {
		return null;
	}

	public function get length():int {
		return 0;
	}
}
}
