package com.alleni.author.util.cache {
import com.alleni.author.util.cache.LRUCache;

import flash.utils.Dictionary;
	
	public class LRUCache implements ObjectCache
	{
		private var _size:int = 0;
		private var _dictionary:Dictionary = new Dictionary();
	
		private var _softMax:int;
		private var _hardMax:int;
	
		// linked list ends
		private var _head:LRUNode = null;
		private var _tail:LRUNode = null;
	
		public function LRUCache(softMax:int=20, hardMax:int=200) {
            setSize(softMax, hardMax);
        }

        public function setSize(softMax:int, hardMax:int):void {
			if(softMax < 1) {
				_softMax = 1;
			}else{
				_softMax = softMax;
			}
			if(hardMax < _softMax) {
				_hardMax = _softMax;
			}else{
				_hardMax = hardMax;
			}
            pruneList();
		}
	
		public function setValue(key:Object, value:Object):void {
			var node:LRUNode = _dictionary[key];
			if(node == null) {
				createNode(key, value);
			} else {
				node.value = value;
				snipNode(node);
				makeHead(node);
			}
		}
	
        public function getValue(key:Object):Object {
            var node:LRUNode = _dictionary[key];
            if(node != null) {
                snipNode(node);
                makeHead(node);
            }
            if(node)
                return node.value;
            else
                return null;
        }

        public function getValueWithoutUpdating(key:Object):Object {
            var node:LRUNode = _dictionary[key];
            if(node)
                return node.value;
            else
                return null;
        }

        public function remove(key:Object):void {
            var node:LRUNode = _dictionary[key];
            if (node) {
                snipNode(node);
                delete _dictionary[key];
                --_size;
            }
        }

		public function get length():int {
			return _size;
		}
	
		/**
		 * Remove a node from the linked list
		 * @param node
		 */
		private function snipNode(node:LRUNode):void {
			if(node.prev != null) {
				node.prev.next = node.next;
			}
			if(node.next !=null) {
				node.next.prev = node.prev;
			}
            if (_tail == node) {
                _tail = node.prev;
            }
            if (_head == node) {
                _head = node.next;
            }
		}
	
	
		/**
		 * Removes nodes until we are down to the soft maximum
		 */
		private function pruneList(reserveSpace:int = 0):void {
			while((_size > _softMax || _size+reserveSpace > _hardMax) && _tail != null) {
				delete _dictionary[_tail.key];
				_tail = _tail.prev;
				_size--;
			}
			if(_tail == null) {
				_head = null;
			} else {
				_tail.next = null;
			}
		}
	
		/**
		 * Makes a node to be the current head node
		 * @param node
		 */
		private function makeHead(node:LRUNode):void {
			node.next = _head;
            node.prev = null;
			if(_head != null) {
				_head.prev = node;
			}
			if(_tail == null) {
				_tail = node;
			}
			_head = node;
		}
	
		private function createNode(key:Object, value:Object):CacheNode {
			if(_size >= _hardMax) {
				pruneList(1);
			}
			var node:LRUNode = new LRUNode(key, value);
			makeHead(node);
			_dictionary[key] = node;
			_size++;
			return node;
		}
	
        public function listKeys():String {
            var result:String = "";
            for(var n:LRUNode=_head; n != null; n = n.next) {
                result += n.key;
            }
            return result;
        }

        public function get allItems():Array {
            var result:Array = [];
            for(var n:LRUNode=_head; n != null; n = n.next) {
                result.push(n.key);
            }
            return result;
        }

        public function dump():void {
            trace("LRUCache: head="+_head, "tail="+_tail);
            for each (var node:LRUNode in _dictionary) {
                trace("  node="+node, "prev="+node.prev, "next="+node.next);
            }
        }

        public function validate():void {
            if (_head) {
                assert(_head.prev == null);
            }
            if (_tail) {
                assert(_tail.next == null);
            }
            var count:int = 0;
            for(var n:LRUNode=_head; n != null; n = n.next) {
                if (n.next == null) {
                    assert(_tail == n);
                }
                assert (++count <= physicalLength);
                assert(n.next != n);
                assert(n.prev != n);
            }
            assert(logicalLength == _size);
            assert(physicalLength == _size);
            assert(_size <= _hardMax);
        }

        private function get logicalLength():int {
            var result:int = 0;
            for(var n:LRUNode=_head; n != null; n = n.next) {
                ++result;
            }
            return result;
        }

        private function get physicalLength():int {
            var result:int = 0;
            for (var key:Object in _dictionary) {
                ++result;
            }
            return result;
        }

        private function assert(condition:*):void
        {
            if (condition==null || condition==false) {
                throw new Error("LRUCache");
            }
        }
	}
}
