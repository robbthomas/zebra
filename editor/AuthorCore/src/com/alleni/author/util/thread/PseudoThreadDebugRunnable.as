/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 7/24/12
 * Time: 5:20 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.thread {
public class PseudoThreadDebugRunnable extends PseudoThreadRunnable {
    private var _name:String;
    private var _group:Boolean;
    private var _end:PseudoThreadRunnable;
    private var _begin:PseudoThreadRunnable;
    public function PseudoThreadDebugRunnable(name:String, group:Boolean = false, previous:PseudoThreadRunnable=null) {
        _name = name;
        _group = group;
        _begin = previous;
        if(_group && (previous == null)) {
            _end = new PseudoThreadDebugRunnable(null,  true, this);
        }
    }

    override public function get name():String {
        return _name ? _name : (begin ? begin.name : "Debug unnamed");
    }

    public function get group():Boolean {
        return _group;
    }

    public function get end():PseudoThreadRunnable {
        return _end;
    }

    public function get begin():PseudoThreadRunnable {
        return _begin;
    }
}
}
