/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 7/23/12
 * Time: 3:34 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.thread {
import com.alleni.author.application.ApplicationUI;

import flash.events.Event;
import flash.events.EventDispatcher;

public class PseudoThreadWaitFrameRunnable extends PseudoThreadRunnable {
    private var runningLock:PseudoThreadLock = new PseudoThreadLock();

    public function PseudoThreadWaitFrameRunnable() {
    }
    override public function run():void {
        runningLock.lock(name);
        ApplicationUI.instance.addEventListener(Event.ENTER_FRAME, handleEnterFrame);
    }

    protected function handleEnterFrame(event:Event):void {
        event.target.removeEventListener(event.type, arguments.callee);
        runningLock.unlock();
    }

    override public function get name():String {
        return "Wait one frame";
    }
}
}
