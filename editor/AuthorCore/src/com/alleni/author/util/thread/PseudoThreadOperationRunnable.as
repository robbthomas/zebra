/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 7/19/12
 * Time: 4:31 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.thread {
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.service.IOperation;

import flash.events.Event;

public class PseudoThreadOperationRunnable extends PseudoThreadRunnable {
    protected var _operation:IOperation;
    protected var runningLock:PseudoThreadLock = new PseudoThreadLock();
    protected var _synchronous:Boolean;

    public function PseudoThreadOperationRunnable(operation:IOperation, synchronous:Boolean = true) {
        _operation = operation;
        _synchronous = synchronous;
    }

    override public function get name():String {
        if(_operation && _operation.displayName) {
            if(_operation.displayName) {
                return _operation.displayName;
            } else {
                return new Utilities().shortClassName(_operation) + " unnamed";
            }
        } else {
            return "IOperation none";
        }
    }

    override public function run():void {
        if(_synchronous) {
            runningLock.lock(name);
        }
        addListeners();
        _operation.execute();
    }

    protected function addListeners():void {
        _operation.addEventListener(Event.COMPLETE, handleOperationComplete);
    }

    protected function handleOperationComplete(event:Event):void {
        runningLock.unlock();
    }
}
}
