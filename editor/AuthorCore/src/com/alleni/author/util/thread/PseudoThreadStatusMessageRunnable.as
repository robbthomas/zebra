/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 7/24/12
 * Time: 11:35 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.thread {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.taconite.event.ApplicationEvent;

public class PseudoThreadStatusMessageRunnable extends PseudoThreadSimpleRunnable {
    public function PseudoThreadStatusMessageRunnable(msg:String) {
        super("Status message [" + msg + "]", ApplicationController.instance.dispatchEvent, ApplicationController.instance, [new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, msg)]);
    }
}
}
