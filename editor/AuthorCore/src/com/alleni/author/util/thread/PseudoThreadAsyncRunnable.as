/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 7/19/12
 * Time: 2:34 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.thread {

/**
 * This class encapsulates a function in the form
 * <pre>
 *     function myFunction(onComplete:Function):void {
 *         //your code here
 *         onComplete();
 *     }
 * </pre>
 * The function onComplete does not need to be called immediately, but can be stored
 * and called later. This runnable will not be considered ever finished unless the
 * onComplete is called. It should not be called more than once. This runnable
 * will block until onComplete is called.
 */
public class PseudoThreadAsyncRunnable extends PseudoThreadSimpleRunnable {
    private var runningLock:PseudoThreadLock = new PseudoThreadLock();

    /**
     *
     * @param name Runnable name
     * @param f Function to run taking a single function argument that is to be called
     * when the asynchronous operation is completed
     * @param thisarg the "this" argument to be passed on to f
     */
    public function PseudoThreadAsyncRunnable(name:String, f:Function, thisarg:Object=null) {
        super(name, f, thisarg, [onComplete]);
    }

    override public function run():void {
        runningLock.lock(name);
        super.run();
    }

    protected function onComplete():void {
        runningLock.unlock();
    }
}
}
