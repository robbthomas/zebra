/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 7/23/12
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.thread {
public class PseudoThreadLock {
    private var _pauseToken:Object;
    public function PseudoThreadLock() {
    }

    public function get locked():Boolean {
        return _pauseToken != null;
    }

    public function lock(name:String = null):void {
        if(locked) {
            throw new Error("Lock [" + name + "] is already held.");
        }
        _pauseToken = PseudoThread.instance.pause(name);
    }

    public function unlock():void {
        if(locked) {
            PseudoThread.instance.resume(_pauseToken);
            _pauseToken = null;
        }
    }
}
}
