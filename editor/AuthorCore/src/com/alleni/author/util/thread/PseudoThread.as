package com.alleni.author.util.thread
{

    import com.alleni.author.util.ForceRender;
    import com.alleni.author.util.PerformanceTimer;
import com.alleni.author.util.Set;

import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.utils.getTimer;
	
	/**
	 * Simulated Threading Class used to break up
	 * large processes and allow the UI to update
	 * rather than freeze the screen.
	 */
	
	public class PseudoThread extends EventDispatcher
	{
        private var TIME_SLICE:Number = 300;

		// number of milliseconds we think it takes to render the screen
		public var RENDER_DEDUCTION:int = 10;
        public static var DEBUG_ONE_RUNNABLE_PER_FRAME:Boolean = false;
		
		private var _threadContext:PseudoThreadContext = null;
		private var timeStart:Number;
		private var timeDue:Number;

        private var pausingSet:Set = new Set(true);
		
		private var mouseEvent:Boolean;
		private var keyEvent:Boolean;
		
		private static var _instance:PseudoThread = null;
		public var contextQueue:Vector.<PseudoThreadContext> = new Vector.<PseudoThreadContext>();
		
		public static function get instance():PseudoThread
		{
			if(_instance == null) 
			{
//                trace("PseudoThread::instance create");
				_instance = new PseudoThread();
			}
				
			return _instance;
		}

        public static function running():Boolean {
            return _instance != null;
        }

        /**
         * Quick and dirty either add to current context or start new context
         * TODO likely remove in favor of knowing the context ahead of time
         * @param runnable
         */
        public static function add(runnable:PseudoThreadRunnable):void {
            if(PseudoThread.instance.context && !PseudoThread.instance.context.finished) {
                PseudoThread.instance.context.add(runnable);
            } else {
                PseudoThread.instance.start(new PseudoThreadContext(new <PseudoThreadRunnable>[runnable]));
            }
        }

        /**
         * Quick and dirty either add to current context or start new context
         * TODO likely remove in favor of knowing the context ahead of time
         * @param runnable
         */
        public static function addAll(runnables:Vector.<PseudoThreadRunnable>):void {
            if(PseudoThread.instance.context && !PseudoThread.instance.context.finished) {
                PseudoThread.instance.context.addAll(runnables);
            } else {
                PseudoThread.instance.start(new PseudoThreadContext(runnables));
            }
        }
		
		/**
		 * Constructor 
		 * 
		 * 
		 */
		public function PseudoThread()
		{
			if(PseudoThread._instance)
				throw new Error("PseudoThread is a singleton, use PseudoThread.instance instead");

			// add high priority listener for ENTER_FRAME
			ForceRender.stage.addEventListener(Event.ENTER_FRAME, enterFrameHandler, false, 100);

			// attach render listener
			ForceRender.stage.addEventListener(Event.RENDER, renderHandler);
			ForceRender.stage.invalidate();
		}
		
		public function start(context:PseudoThreadContext):void
		{
			// initialize instance if not already
			PseudoThread.instance;
			// place context into queue
			contextQueue.push(context);
			if(_threadContext == null) {
                startNext();
            }
		}

        /**
         * Pauses this thread and returns a token object.
         * This token should be passed back to resume and then discarded.
         * The thread will remain paused until all distributed tokens are either
         * returned or garbage collected.
         * @param source A description of the source of the pausing (ex. class or operation name)
         * @return token
         */
        public function pause(source:String="Unnamed"):Object
        {
//            trace("PseudoThread::pause(", source, ")");
            var token:Object = {name:source};
            pausingSet.add(token);
            return token;
        }

        /**
         * Releases a token representing a previous pause.
         * This token must be a return value of this thread's pause function.
         * Also, a token should only be released once.
         * @param token
         */
        public function resume(token:Object):void
        {
//            trace("PseudoThread::resume(", token.name, ")");
            pausingSet.remove(token);
        }
		
		public function get context():PseudoThreadContext
		{
			return _threadContext;
		}
		
		
		
		private function startNext():void {
			if(contextQueue.length > 0) {
				_threadContext = contextQueue.shift();
//                trace("PseudoThread::startNext ", _threadContext.total, contextQueue.length);
				// add handlers for mouse and keyboard interruptions
				// TODO: allow the context to choose whether or not to listen to these
				//_systemManager.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
				//_systemManager.stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
                // send start event
				timeDue = 0;
                dispatchEvent(new PseudoThreadEvent(PseudoThreadEvent.THREAD_START,_threadContext));
			} else {
                _threadContext = null;
//                trace("PseudoThread::startNext end");

                // OPTIONAL - this cleans up the thread when not in use
                ForceRender.stage.removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
                ForceRender.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
                ForceRender.stage.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
				ForceRender.stage.removeEventListener(Event.RENDER, renderHandler);
                // END OPTIONAL

                _instance = null;
                // create PseudoThreadEvent and give it the context
            }
		}
		
		private function end():void {
//            trace("PseudoThread::end context");
            dispatchEvent(new PseudoThreadEvent(PseudoThreadEvent.THREAD_COMPLETE,_threadContext));
			startNext();
		}
		
		/**
		 * This handler fires everytime the empty UIComponent
		 * enters a new frame and calculates roughly how much
		 * time this fake thread has to process before having
		 * to update the UI in order to maintain the previous
		 * frame rate. It also uses the UIComponent's drawing
		 * API to trigger a RENDER event.
		 * 
		 * @param event
		 * 
		 */
		private function enterFrameHandler(event:Event):void
		{
            if(_threadContext == null) {
                return;
            }
			timeStart = getTimer();
			timeDue = timeStart + TIME_SLICE;

            ForceRender.forceThisFrame();
		}
		
		/**
		 * This handler is executed everytime the empty UI
		 * Component dispatches a RENDER event and is used
		 * to execute the thread function continuously when 
		 * processing time is available and terminates
		 * itself when the thread function returns false.
		 * 
		 * @param event
		 * 
		 */
		private function renderHandler(event:Event):void
		{
			var anyWork:Boolean = false;
			var moreWork:Boolean = true;
			//trace("Render Start");
			//PerformanceTimer.begin("PseudoThread::renderHandler");
            if(_threadContext == null) {
				
				//trace("Render Quick End");
				//PerformanceTimer.end("PseudoThread::renderHandler");
                return;
            }
			// if a mouse or keyboard activity
			// interrupts this process, use
			// an estimate on how long it takes
			// to render the screen to adjust
			// the timeDue value.
			if (mouseEvent || keyEvent)
				timeDue -= RENDER_DEDUCTION;
			// when it is appropriate, execute
			// the thread function
//            trace("PsuedoThread:: [ timer = "+getTimer()+ ", timeDue = "+timeDue+" , framerate = "+ForceRender.stage.frameRate);
			while (pausingSet.isEmpty && getTimer() < timeDue)
			{
//				try
//				{
					// run thread function, if thread
					// function returns false, then 
					// go ahead and terminate all the
					// handlers and remove empty 
					// UIComponent and dispatch an 
					// event indicating this thread has
					// completed its work.
					anyWork = true;
					moreWork = _threadContext.doNext();
					if(!moreWork)
					{
						break;
					}
                    if(DEBUG_ONE_RUNNABLE_PER_FRAME){
                        break;
                    }

//				} catch(error:Error)
//				{
//					var e:PseudoThreadEvent = new PseudoThreadEvent(PseudoThreadEvent.THREAD_ERROR,_threadContext);
//					e.error = error;
//					dispatchEvent(e);
//					throw error; // dont swallow this for now - SJ
//				}
			}
			// reset tracking variables
			mouseEvent = false;
			keyEvent = false;

            if(anyWork && moreWork) {
//                trace("PseudoThread::renderHandler halting for this frame");
            }
			
			if(anyWork) {
				dispatchEvent(new PseudoThreadEvent(PseudoThreadEvent.THREAD_PROGRESS,_threadContext));
			}
			
			//trace("Render End");
			//PerformanceTimer.end("PseudoThread::renderHandler");
			if(!moreWork) {
				end();
			}
		}

		/**
		 * Handler to track whether or not a mouse event
		 * has interrupted this process.
		 *  
		 * @param event
		 * 
		 */
		private function mouseMoveHandler(event:Event):void
		{
			mouseEvent = true;
		}
		
		/**
		 * Handler to track whether or not a keyboard event
		 * has interrupted this process.
		 *  
		 * @param event
		 * 
		 */
		private function keyDownHandler(event:Event):void
		{
			keyEvent = true;
		}
	} 
}
