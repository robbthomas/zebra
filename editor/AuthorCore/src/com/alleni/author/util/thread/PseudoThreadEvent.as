package com.alleni.author.util.thread
{
	import flash.events.Event;
	
	public class PseudoThreadEvent extends Event
	{
		public static const THREAD_START:String = "threadStart";
		public static const THREAD_PROGRESS:String = "threadProgress";
		public static const THREAD_COMPLETE:String = "threadComplete";
		public static const THREAD_ERROR:String = "threadError";
		
		
		public var context:PseudoThreadContext;
		public var error:Error;
		
		public function PseudoThreadEvent(type:String, context:PseudoThreadContext, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this.context = context;
			super(type, bubbles, cancelable);
		}
	}
}