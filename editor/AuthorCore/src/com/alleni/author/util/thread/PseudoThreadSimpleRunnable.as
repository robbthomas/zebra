package com.alleni.author.util.thread
{
	public class PseudoThreadSimpleRunnable extends PseudoThreadRunnable
	{
        private var _name:String;
		private var _f:Function;
		private var _this:Object;
		private var _args:Array;
		
		public function PseudoThreadSimpleRunnable(name:String, f:Function, thisarg:Object=null, args:Array=null)
		{
            _name = name;
			_f = f;
			_this = thisarg;
			_args = args;
		}
		
		override public function run():void {
			_f.apply(_this, _args);
		}

        override public function get name():String {
            return _name;
        }
    }
}