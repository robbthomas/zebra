package com.alleni.author.util.thread
{
	import com.alleni.author.util.PerformanceTimer;
import com.alleni.taconite.dev.Utilities;

import flash.errors.IllegalOperationError;
	
	import mx.collections.IList;

	public dynamic class PseudoThreadContext
	{
        /**
         * Execute counter is the index that will be executed next
         * It is -1 if no more executions will take place (Context is finished)
         */
		public var executeCounter:int = 0;
		public var insertCounter:int;
        public var estimatedCount:int;
        public var runnables:Vector.<PseudoThreadRunnable>;
        public var debugGroups:Vector.<PseudoThreadDebugRunnable> = new Vector.<PseudoThreadDebugRunnable>();
        public var indent:String = "";
		/**
		 * Constructor
		 * 
		 * 	@param runnables
		 * 		Vector of runnables to be executed.
		 * 
		 * 	@param counter
		 * 		Optional position to start processing in the collection.
		 * 		The default is zero.
		 */
		public function PseudoThreadContext(runnables:Vector.<PseudoThreadRunnable>=null, estimatedCount:int = 0)
		{
			this.runnables = runnables;
            if(this.runnables == null) {
                this.runnables = new <PseudoThreadRunnable>[];
            }
            this.estimatedCount = estimatedCount;
            this.insertCounter = this.runnables.length;
		}
		
		public function get progress():Number
		{
			return (executeCounter < 0) ? 1 : (executeCounter/total);
		}

		public function get total():Number
		{
			return Math.max(runnables.length, estimatedCount);
		}

		public function get progressString():String
		{
			return int(progress*100)+"%";
		}

        public function get finished():Boolean
        {
            return executeCounter < 0;
        }
		
		public function add(runnable:PseudoThreadRunnable):void
		{
//            trace("Pseudo.add ins="+insertCounter, "exe="+executeCounter, runnable.name);
			if(finished) {
                throw new IllegalOperationError("Trying to add to a finished thread context");
			} else {
                if(runnables.length > 0 && insertCounter == executeCounter) {

                }
                runnables.splice(insertCounter, 0, runnable);
            	insertCounter++;
			}
		}

        public function addAll(runnables:Vector.<PseudoThreadRunnable>):void
        {
            for each(var runnable:PseudoThreadRunnable in runnables) {
                add(runnable);
            }
        }
		
		public function doNext():Boolean
		{
			//PerformanceTimer.begin("PseudoThreadContext::doNext");
			if(executeCounter < runnables.length)
			{
                var current:PseudoThreadRunnable = runnables[executeCounter];
//                var text:String = current.name;
//                var debug:PseudoThreadDebugRunnable = current as PseudoThreadDebugRunnable;
//                if(debug && debug.group) {
//                    if(debug.end) {
//                        text = indent + text + " {";
//                        debugGroups.push(debug);
//                        indent = indent + "  ";
//                    } else if(debug.begin) {
//                        text = "} " + text;
//                        if(debugGroups.length > 0) {
//                            var other:PseudoThreadDebugRunnable = debugGroups.pop();
//                            indent = indent.substr(2);
//                            if(other != debug.begin) {
//                                text += " !!! Error mismatched debug group";
//                            }
//                        } else {
//                            text += " !!! Error mismatched debug group";
//                        }
//                        text = indent + text;
//                    } else {
//                        text += " !!! Error unpaired debug group";
//                        text = indent + text;
//                    }
//                } else {
//                    text = indent + text;
//                }
//                trace(new Date().toUTCString() + " PseudoThreadContext::doNext [", text, "]", "ins="+insertCounter, "exe="+executeCounter);
                // update the next to run and the insertion point before
                // calling run. We want these values to be constant during
                // and after the run call up to the next run call.
                executeCounter++;
                insertCounter = executeCounter;
				current.run();
//                trace(new Date().toUTCString() + " PseudoThreadContext::doNext [", text, "]", "finish");
				//PerformanceTimer.end("PseudoThreadContext::doNext");
				return true;
			}
            executeCounter = -1;
			//PerformanceTimer.end("PseudoThreadContext::doNext");
//            trace(Utilities.timestamp + " PseudoThreadContext::doNext finished");
			return false;
		}
	}
}
