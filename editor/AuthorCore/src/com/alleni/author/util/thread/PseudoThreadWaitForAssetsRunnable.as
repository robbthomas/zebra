/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 7/24/12
 * Time: 11:02 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util.thread {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.taconite.event.ApplicationEvent;

public class PseudoThreadWaitForAssetsRunnable extends PseudoThreadRunnable {
    private var runningLock:PseudoThreadLock = new PseudoThreadLock();

    /**
     *  PseudoThreadWaitForAssetsRunnable should only be invoked via AssetLoadOperation.unhold()
     *  else a deadlock may be created.
     *
     *  That's because a queued unhold may never be reached, since this operation stops PseudoThread.
     */
    public function PseudoThreadWaitForAssetsRunnable() {
    }

    override public function run():void {
        if(AssetController.instance.requestedCount == 0) {
            return; // No assets to wait for. None anymore at least.
        }
        runningLock.lock(name);
        ApplicationController.addEventListener(NotificationNamesApplication.ASSET_REQUEST_PROGRESS, handleAssetRequestProgress);
    }

    protected function handleAssetRequestProgress(event:ApplicationEvent):void {
        if (AssetController.instance.requestedCount == 0) {
            ApplicationController.removeEventListener(NotificationNamesApplication.ASSET_REQUEST_PROGRESS, handleAssetRequestProgress);
            runningLock.unlock();
        }
    }

    override public function get name():String {
        return "Wait for assets";
    }
}
}
