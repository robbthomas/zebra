package com.alleni.author.util
{
	public class StringUtils
	{
		public static const SERVER_KEY_WORDS:Array = ["undefined", "null", "false", "true", "NaN", "nan"];
        public static const SERVER_KEY_CHARS:Array = ["?", "$", "#", "%"];

        public function StringUtils()
		{
		}
		
		/* These font metrics are based on Myriad_pro (Zebra) 14 point */
	
		public static function getCharSize(character:String):Number
		{
			switch(character){
				
				case "a":
					return 5.393;
					break;
				case "b":
					return 6.486;
					break;
				case "c":
					return 5.351;
					break;
				case "d":
					return 3.388;
					break;
				case "e":
					return 5.981;
					break;
				case "f":
					return 4.427;
					break;
				case "g":
					return 6.346;
					break;
				case "h":
					return 5.771;
					break;
				case "i":
					return 1.527;
					break;
				case "j":
					return 3.194;
					break;
				case "k":
					return 5.687;
					break;
				case "l":
					return 1.223;
					break;
				case "m":
					return 9.75;
					break;
				case "n":
					return 5.827;
					break;
				case "o":
					return 6.625;
					break;
				case "p":
					return 6.486;
					break;
				case "q":
					return 6.388;
					break;
				case "r":
					return 3.404;
					break;
				case "s":
					return 4.426;
					break;
				case "t":
					return 4.021;
					break;
				case "u":
					return 5.771;
					break;
				case "v":
					return 6.416;
					break;
				case "w":
					return 9.848;
					break;
				case "x":
					return 6.205;
					break;
				case "y":
					return 6.374;
					break;
				case "z":
					return 5.491;
					break;

                case "A":
					return 7.9;
					break;
				case "B":
					return 5.953;
					break;
				case "C":
					return 7.172;
					break;
				case "D":
					return 7.76;
					break;
				case "E":
					return 5.323;
					break;
				case "F":
					return 5.084;
					break;
				case "G":
					return 7.76;
					break;
				case "H":
					return 7.032;
					break;
				case "I":
					return 1.527;
					break;
				case "J":
					return 4.16;
					break;
				case "K":
					return 6.668;
					break;
				case "L":
					return 5.267;
					break;
				case "M":
					return 9.637;
					break;
				case "N":
					return 7.088;
					break;
				case "O":
					return 8.628;
					break;
				case "P":
					return 5.813;
					break;
				case "Q":
					return 10.996;
					break;
				case "R":
					return 6.149;
					break;
				case "S":
					return 5.701;
					break;
				case "T":
					return 6.99;
					break;
				case "U":
					return 6.961;
					break;
				case "V":
					return 7.816;
					break;
				case "W":
					return 11.528;
					break;
				case "X":
					return 7.298;
					break;
				case "Y":
					return 7.382;
					break;
				case "Z":
					return 6.892;
					break;
                case " ":
                    return 3;
                    break;

                case "1":
                    return 3.04;
                    break;
                case "2":
                    return 5.813;
                    break;
                case "3":
                    return 5.687;
                    break;
                case "4":
                    return 6.668;
                    break;
                case "5":
                    return 5.659;
                    break;
                case "6":
                    return 6.248;
                    break;
                case "7":
                    return 5.743;
                    break;
                case "8":
                    return 6.149;
                    break;
                case "9":
                    return 6.122;
                    break;
                case "0":
                    return 6.191;
                    break;
                case "-":
                    return 3.459;
                    break;
                case "=":
                    return 7.228;
                    break;
                case "!":
                    return 1.653;
                    break;
                case "@":
                    return 9.175;
                    break;
                case "#":
                    return 5.939;
                    break;
                case "$":
                    return 5.225;
                    break;
                case "%":
                    return 10.239;
                    break;
                case "^":
                    return 6.653;
                    break;
                case "&":
                    return 7.999;
                    break;
                case "*":
                    return 4.973;
                    break;
                case "(":
                    return 2.788;
                    break;
                case ")":
                    return 2.788;
                    break;
                case "_":
                    return 7.004;
                    break;
                case "+":
                    return 7.228;
                    break;


                case ",":
                    return 2.213;
                    break;
                case ".":
                    return 1.667;
                    break;
                case "/":
                    return 4.861;
                    break;
                case ";":
                    return 2.255;
                    break;
                case "'":
                    return 2.087;
                    break;
                case "[":
                    return 2.563;
                    break;
                case "]":
                    return 2.563;
                    break;
                case "\\":
                    return 4.763;
                    break;
                case "<":
                    return 6.5;
                    break;
                case ">":
                    return 6.5;
                    break;
                case "?":
                    return 4.342;
                    break;
                case ":":
                    return 1.653;
                    break;
                case '"':
                    return 4.16;
                    break;
                case "{":
                    return 3.236;
                    break;
                case "}":
                    return 3.236;
                    break;
                case "|":
                    return .938;
                    break;
				default:
					return 8.7;
			}
		}

        /* Myraid Pro Regular size multipliers
        2nd value in chart is the width of a lowercase 'a'.
        18- 6.932 = 1.28536
        16- 6.163 = 1.14277
        14- 5.393 = 1
        13- 5.005 = .928054
        12- 4.617 = .856109
        11- 4.235 = .785277
         */

        public static function getFontSizeMultiplier(fontSize:int):Number{
            switch(fontSize){
                case 11:
                    return .785277;
                    break;
                case 12:
                    return .856109
                    break;
                case 13:
                    return .928054;
                    break;
                case 14:
                    return 1;
                    break;
                case 16:
                    return 1.14277;
                    break;
                case 18:
                    return 1.28536;
                    break;
                default:
                    return fontSize/14;
            }
        }

		public static function truncate(string:String, sizeLimit:Number, ellipses:String = "...", fontSize:int = 14):String
		{	
			var ellipsesWidth:Number = getStringLengthInPixels(ellipses);

			var stringLength:Number = 0;
			var truncatedString:String = "";

			var index:uint = 0;
			var charLength:Number = 0;

            var multiplier:Number = getFontSizeMultiplier(fontSize);

            if(string != null){
                if((getStringLengthInPixels(string)) <= sizeLimit){
                    truncatedString = string;
                }else{

                    while((stringLength < (sizeLimit - ellipsesWidth)) && (index < string.length)){
                        charLength = getCharSize(string.charAt(index)) + 1.1;
                        truncatedString += string.charAt(index);
                        index++;
                        stringLength += charLength;
                        }
                }

                if (truncatedString.length < string.length){
                    truncatedString += ellipses;
                }
            }
			return truncatedString;
		}
		
		public static function getStringLengthInPixels(string:String):Number
		{	
			var stringLength:Number = 0;
			var index:uint = 0;
			var charLength:Number = 0;
			
			while(index < string.length){
				charLength = getCharSize(string.charAt(index)) + .8;
				index++;
				stringLength += charLength;
			}
			
			return stringLength;
		}
		
		public static function stripWhitespace(string:String):String
		{
			const leadingWhitespace:RegExp = /^[ \t]+/;
			const trailingWhitespace:RegExp = /[ \t]+$/;
			
			if (leadingWhitespace.exec(string) || trailingWhitespace.exec(string)) {
				// trim leading spaces
				string = string.replace(leadingWhitespace, "");
				// trim trailing spaces
				string = string.replace(trailingWhitespace, "");
			}
			
			return string;
		}

        public static function isNameSafeForServer(name:String):Object{
            var validName:Boolean = true;
            var badString:String = null;
            var wordList:Array = name.split(" ");
            var keyString;
            for each(keyString in StringUtils.SERVER_KEY_CHARS){
                if(name.indexOf(keyString) > -1){
                    validName = false;
                    badString = keyString
                    return {valid:validName, reason:badString};
                }
            }
            for each(keyString in StringUtils.SERVER_KEY_WORDS){
                if(wordList.indexOf(keyString) > -1){
                    validName = false;
                    badString = keyString
                    return {valid:validName, reason:badString};
                }
            }

            return {valid:validName};
        }
	}
}