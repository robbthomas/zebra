package com.alleni.author.util
{
	
	import com.alleni.author.definition.AST;
	import com.alleni.author.definition.Lexer;
	import com.alleni.author.definition.Parser;
	import com.alleni.author.model.objects.TextObject;
import com.alleni.savana.Ast;
import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import flashx.textLayout.elements.SpanElement;
	import flashx.textLayout.elements.TextFlow;
	
	public class TextParser
	{
		
		
		
		private var _date:Date = new Date();
		private var _ast:Ast = null
		private var _textObject:TextObject;
		private var _userID:String;
		private var _runTimeValues:Dictionary = new Dictionary();
		private var _leafElements:Dictionary = new Dictionary();
		private var _environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
		
		public function TextParser(text:TextObject)
		{
			_textObject = text;
			
		}
		
		/*
		The following string is processed in this way:
		
		The first number is {random() * {t1}} and the second number is {random() * 100}
		
		When initialize is false:
		The {#t1} variables are replaced with their values and the result is buffered to circumvent future parsing
		A dictionary of runtime values is reset
		The first level tokens are found and parsed for nested tokens 
		The tokens are replaced by values
		
		When initialize is true
		The buffered text is parsed
		The tokens serve as keys of a dictionary and replaced by dictionary values
		
		If the textobject is set to valid==false, then source is passed and everything not in the dictionary is re-evaluated
		
		*/
		
		public function parse(text:String, initialize:Boolean):String{
			
			var tokens:Array;
		
			if(initialize){
				
				_textObject.buffer = text = substituteInput(text);
				_runTimeValues = new Dictionary();
				tokens = findTokens(text);
			
				if(tokens.length < 1){
					_textObject.buffer = "";
					return text;
				}
				
			}
			
			else{
				text = restoreValues(text);
				tokens = findTokens(text);
			}
			
			var variable:String;
			
			for each (var token:String in tokens){
				variable = token.substring(1, token.length - 1);
				variable = parseToken(variable);
				text = evalToken(text,token,variable);
			}
			
			
			return text;
			
			
		}
		
		
		
		public function parseTextFlow(textFlow:TextFlow, initialize:Boolean = false):void{
			_textObject.vars.unmarkVariables();
			
			if(initialize){
				
				/*
				check if any tokens such as {t1}
				if there are no tokens, return
				*/
				if (!textFlow) return;
				
				var textFlowText:String  = textFlow.getText();
				var tokens:Array = findTokens(textFlowText);
				if(tokens.length < 1){
					
					_textObject.buffer = "";
					_textObject.hasEmbeddedVariables = false;
					return;
				}
				else{
					_textObject.hasEmbeddedVariables = true;
				}
				
				for (var key:Object in _leafElements){
					delete _leafElements[key];
				}
			}
			
			  var leaf:SpanElement = new SpanElement(); 
			            leaf = SpanElement(textFlow.getFirstLeaf()); 
			            parseLeaf(leaf, initialize);
			           
			            while(leaf = SpanElement(leaf.getNextLeaf())) { 
				parseLeaf(leaf, initialize);
			            } 
			
			textFlow.flowComposer.updateAllControllers();
			_textObject.vars.sweep();
		}
		
		private function parseLeaf(leaf:SpanElement, initialize:Boolean = false):void{
			
			var text:String;
			
			if(initialize){
				this._leafElements[leaf] = leaf.text;
				text = leaf.text;
			}
			
			else{
				text = _leafElements[leaf]
			}
			
			text = parse(text,initialize);
			
			leaf.text = text;
		}
		
		
		private function parseToken(text:String):String{
			
			var tokens:Array = findTokens(text);
			
			if(tokens.length > 0){
			
				for each(var token:String in tokens){
					var variable:String = token.substring(1, token.length - 1);
					text = evalToken(text, token,variable) 
				}
			}
			
			
			return text;
		}
		
		private function evalToken(text:String, token:String, variable:String):String{
			
			token = escape(token);
			var regEx:RegExp = new RegExp(token); 
			var value:String = eval(variable);
			
			if(token != "\\{t1\\}"){
				_runTimeValues[variable] = value;		
			}
			
			text = text.replace(regEx, value);
			return text;
		}
		
		
		//will be replaced with something more dynamic, based on dynamic ribbons -- currently hardwired to the t1 ribbon
		
		private function substituteInput(text:String):String{
			if(text){
				text = text.replace(/(?i)\#t1/g, _textObject.inputVal);
			}
			return text;
		}
		
		//when running restore initial values -- randomly generated numbers, for example, don't regenerate 
		
		private function restoreValues(text:String):String{
			
			var value:String;
			
			for (var key:Object in _runTimeValues){
				
				value = _runTimeValues[key];
				key = escape("{" + key + "}");
				var regEx:RegExp = new RegExp(key, 'g'); 
				text = text.replace(regEx, value);
				
			}
			
			return text;
		}
		
		
		
		private function escape(s:String):String
		{
			var newString:String = 
				s.replace(
					new RegExp("([{}\(\)\^$&.\*\?\/\+\|\[\\\\]|\]|\-)","g"),
					"\\$1");
			return newString;
		}
		
		
		private function eval(variable:String):String{
			
			switch(variable.toLowerCase()){
				
				case "t1":
					return _textObject.inputVal;
					
				default:
					
					return calc(variable);
					
			}
			
		}
		
		
		
		private function calc(expression:String):String {
			
			var result:String = "";
			
			try{
			
				_ast = new Ast(expression);
				_textObject.vars.registerVariables(_ast.variables(Ast.defaultEnvironment()));
				var env:Object = _textObject.vars.environment();
				result = String(_ast.evaluate(env));
			} catch(e:Object) {

				result = "error!";
			}
			
			return result;
			}
		
		
		private function findTokens(text:String):Array{
			
			var tokens:Array = new Array();
			
			if(text == null)
				return tokens;
			
			var openBraceCounter:uint = 0;
			var startIndex:uint = 0;
			var endIndex:uint = 0;
			
			for(var i:uint; i < text.length; i++){
				
				if(text.charAt(i) == "{"){
					if(openBraceCounter == 0)
					startIndex = i;
					openBraceCounter++;
				}
				
				if(text.charAt(i) == "}"){
					openBraceCounter--;
					if(openBraceCounter == 0){
						endIndex = i;
						var token:String = text.substring(startIndex,endIndex + 1);
						tokens.push(token);
					}
				}
			}//for
			
			return tokens;
		}
		

	}
}