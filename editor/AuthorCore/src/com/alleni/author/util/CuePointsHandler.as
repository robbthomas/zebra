package com.alleni.author.util
{
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.events.FlexEvent;

	public class CuePointsHandler
	{
		private var _cuePoints:ArrayCollection = new ArrayCollection();
		private var _numericSortField:SortField = new SortField();
		private var _cursor:IViewCursor = _cuePoints.createCursor();
		private var _callBack:Function;
	
		
		public function CuePointsHandler(callBack:Function)
		{
			_callBack = callBack;
			
			_numericSortField.name = "Time";
			_numericSortField.numeric = true;
			
			var numericDataSort:Sort = new Sort();
			numericDataSort.fields = [_numericSortField];
			
			_cuePoints.sort = numericDataSort;
			_cursor.addEventListener( FlexEvent.CURSOR_UPDATE, onCursorUpdate );
		}
		
		public function addCuePoint(cuePoint:Point):void
		{	
			_cuePoints.addItem({Time:cuePoint.x, ID:cuePoint.y});
			_cuePoints.refresh();
		}
		
		public function findCuePoint(time:Number):void
		{
			if (_cuePoints.length > 0)
				_cursor.findFirst({Time:time});
		}
		
		private function onCursorUpdate( e:FlexEvent ) :void
		{
			if (e.target.current && e.target.current.ID)
				_callBack(e.target.current.ID);
		}
		
		public function clear():void
		{	
			_cuePoints.removeAll();
		}
	}
}