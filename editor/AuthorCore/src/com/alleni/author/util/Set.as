package com.alleni.author.util {
import flash.utils.Dictionary;

public class Set {
    private var _dictionary:Dictionary;
    private var _size:int=0;
    public function Set(weak:Boolean) {
        _dictionary = new Dictionary(weak);
    }

    public function add(item:Object):Boolean {
        if (item in _dictionary) {
            return false;
        } else {
            _size++;
            _dictionary[item] = true;
            return true;
        }
    }

    public function remove(item:Object):Boolean {
        if (item in _dictionary) {
            _size--;
            delete _dictionary[item];
            return true;
        } else {
            return false;
        }
    }

    public function hasItem(item:Object):Boolean {
        return (item in _dictionary);
    }

    public function get size():int {
        return _size;
    }
    
    public function get cardinality():int {
        return size;
    }

    public function get isEmpty():Boolean {
        return (size == 0);
    }

    public function get allItems():Array {
        var result:Array = [];
        var item:Object;
        for(item in _dictionary) {
            result.push(item);
        }
        return result;
    }
}
}
