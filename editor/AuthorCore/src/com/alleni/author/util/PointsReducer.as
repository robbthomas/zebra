/**
 * Based on the Douglas-Peuker algorithm, which is an algorithm for reducing the number of points in a curve that is approximated by a series of points.
 * 
 * 
 */
package com.alleni.author.util
{
	import flash.geom.Point;
		
	public class PointsReducer
	{	
		/**
		 * @param points an array of Point
		 * @param epsilon points further away from the line than epsilon are retained, others are dropped
		 */
		public function reduce(points:Vector.<Point>, epsilon:Number):Vector.<Point>
		{
			//trace("Number of Points: ",points.length);
			
			var dMax:Number  = 0;
			var index:Number = 0;
			
			for(var i:Number = 1; i <= points.length - 2; i++){  //was points.length - 2
				var distance:Number = distanceFromLine(points[0], points[points.length - 1], points[i]);  //reversed from peuker
				
				if(distance >= dMax) {	
					dMax = distance;
					index = i; //this is the worst point
				}
			}//for
			
			var resultList:Vector.<Point>;
			
			if (dMax >= epsilon) {
				var segment1:Vector.<Point> = points.slice(0,index + 1); 
				var segment2:Vector.<Point> = points.slice(index, points.length);  //slice is up to but not including second arg
				
				var results1:Vector.<Point> = reduce(segment1, epsilon);
				var results2:Vector.<Point> = reduce(segment2, epsilon);
				
				var resultsSubset:Vector.<Point> = results1.slice(0,results1.length - 1);
				
				resultList = resultsSubset.concat(results2);	
			}
			else{
				resultList = new Vector.<Point>();
				resultList.push(points[0]);
				resultList.push(points[points.length - 1]);	
			}
			return resultList;	
		}//simplify
		
		protected function distanceFromLine(a:Point, b:Point, c:Point):Number
		{
			var v1:Point = vector(a, b);
			var v2:Point = vector(a, c);
			
			var value:Number = crossProduct(v1, v2) /getLength(a, b);
			
			return Math.abs(value);	
		}
		
		protected function getLength(a:Point, b:Point):Number
		{
			return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));	
		}
		
		public function vector(a:Point, b:Point):Point
		{
			var vX:Number = b.x - a.x;
			var vY:Number = b.y - a.y;
			
			return new Point(vX, vY);	
		}
		
		public function crossProduct(a:Point, b:Point):Number
		{
			return a.x * b.y - a.y * b.x;
		}
	}
}