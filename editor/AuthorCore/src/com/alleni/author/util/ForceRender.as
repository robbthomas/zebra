/**
 * Created by IntelliJ IDEA.
 * User: jessecoyle
 * Date: 10/31/11
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util {

import flash.display.Stage;

import mx.core.FlexGlobals;
import mx.core.UIComponent;
import mx.managers.ISystemManager;

public class ForceRender {

    private var renderHelper:UIComponent;
    private static var instance:ForceRender;

    {
        instance = new ForceRender();
    }

    public function ForceRender() {
        // getting systemManager from FlexGlobals
        // NOTE: this will not work unless this is used in a Flex app
		var top:UIComponent = FlexGlobals.topLevelApplication as UIComponent;
		if ("mainComponentForRender" in top) {
			top = top["mainComponentForRender"];  // mainComponentForRender is implemented in AuthorDesktop, since topLevelApplication is does not render in AIR
		}

        var _systemManager:ISystemManager = top.systemManager;
        // create an empty UIComponent to track render events
        renderHelper = new UIComponent();
        _systemManager.addChild(renderHelper);
    }

    public static function forceThisFrame():void {
        instance.renderHelper.stage.invalidate();
        instance.renderHelper.graphics.clear();
        instance.renderHelper.graphics.moveTo(0, 0);
        instance.renderHelper.graphics.lineTo(0, 0);
    }

    public static function get stage():Stage {
        return instance.renderHelper.stage;
    }
}
}
