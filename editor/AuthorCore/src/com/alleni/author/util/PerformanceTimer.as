/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 3/8/11
 * Time: 12:09 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.util {
import com.alleni.author.model.ui.Application;

import flash.display.DisplayObject;
import flash.events.Event;

import flash.utils.getTimer;

public class PerformanceTimer {

	private static var root:Object = {
				parent:current,
				name:"[[[Root]]]",
				count:0,
				badEnds:0,
				totalTime:0,
				startTime:-1,
				children:{}
			};

	private static var current:Object = root;

	public static var enabled:Boolean = true;
	public function PerformanceTimer() {
	}

	private static function grabTimer(name:String):Object {
		if(name in current.children) {
			return current.children[name];
		} else {
			var result:Object = {
				parent:current,
				name:name,
				count:0,
				badStarts:0,
				badEnds:0,
				totalTime:0,
				startTime:-1,
				children:{}
			};
			current.children[name] = result;
			return result;
		}
	}

	public static function begin(name:String):void {
		if(enabled) {
			var timer:Object = grabTimer(name);
			if(timer.startTime > 0) {
				timer.badStarts++;
			}
			current = timer;
			timer.startTime = getTimer();
		}
	}

	public static function end(name:String):void {
		if(enabled) {
			var timer:Object = current;
			if(timer.name != name) {
				timer.badEnds++;
				return;
			}
			if(timer.startTime < 0) {
				timer.badEnds++;
				return;
			}
			var duration:int = getTimer() - timer.startTime;
			timer.totalTime += duration;
			timer.startTime = -1;
			timer.count++;
			current = timer.parent;
		}
	}

	public static function dump():void {
		trace("<<<<<<< Timer dump ");
		var totals:Object = printTimers(root, {});
		trace("  <<<<<<< Total Timer dump ");
		var names:Array = [];
		var name:Object;
		for (name in totals) {
			names.push(name)
		}
		names.sort();
		for each (name in names) {
			var total:Object = totals[name];
			trace(total.name, ": count[", total.count, "] totalTime[", total.totalTime, "] averageTime[", total.totalTime/total.count + "] badStarts[", total.badStarts, "] badEnds[", total.badEnds, "] selfTime[", total.self, "]");
		}
		trace("<<<<<<< End Timer dump ");
	}

	private static function printTimers(root:Object, totals:Object, prefix:String=""):Object {
		var names:Array = [];
		var name:Object;
		for (name in root.children) {
			names.push(name)
		}
		names.sort();
		trace(prefix + "{");
		var comma:Boolean = false;
		for each (name in names) {
			var timer:Object = root.children[name];
            var self:Number = selfTime(timer);
			if(name in totals) {
				totals[name].count += timer.count;
				totals[name].totalTime += timer.totalTime;
				totals[name].badStarts += timer.badStarts;
				totals[name].badEnds += timer.badEnds;
                totals[name].self += self;
			} else {
				totals[name] = {name:name, count:timer.count, totalTime:timer.totalTime, badStarts:timer.badStarts, badEnds:timer.badEnds, self:self};
			}
			trace(prefix + (comma?",":"") + "\"" + timer.name, "- count", timer.count, "totalTime", timer.totalTime, "averageTime", timer.totalTime/timer.count, "selfTime", self + "\" :");
			comma = true;
			printTimers(timer, totals, prefix + "  ");
		}
		trace(prefix + "}");
		return totals;
	}

    private static function selfTime(root:Object):Number {
        var result:Number = root.totalTime;
        for each(var child:Object in root.children) {
            result -= child.totalTime;
        }
        return result;
    }

	public static function clear():void {
		root =  {
				parent:current,
				name:"[[[Root]]]",
				count:0,
				badEnds:0,
				totalTime:0,
				startTime:-1,
				children:{}
			};
		current = root;
	}


    public static function addFrameScaffolding(view:DisplayObject):void {
        view.stage.addEventListener(Event.RENDER, happensBeforeRender,false,int.MAX_VALUE); // listen for rendering
        view.stage.addEventListener(Event.RENDER, happensAfterRender,false,int.MIN_VALUE); // listen for rendering
        view.stage.addEventListener(Event.ENTER_FRAME, happensBeforeEnterFrame,false,int.MAX_VALUE); // listen for rendering
        view.stage.addEventListener(Event.ENTER_FRAME, happensAfterEnterFrame,false,int.MIN_VALUE); // listen for rendering
        view.stage.addEventListener(Event.EXIT_FRAME, happensAfterExitFrame,false,int.MIN_VALUE); // listen for rendering
    }

    public static var startTimingFlag:Boolean =false;
    private static var currentlyTiming:Boolean = false;
    public static var stopTimingFlag:Boolean = false;

    private static function happensBeforeEnterFrame(e:Event):void {
        if(currentlyTiming) {
            PerformanceTimer.end("Rendering");
        }
        if(startTimingFlag) {
            startTimingFlag = false;
            currentlyTiming = true;
            PerformanceTimer.clear();
        }
        if(stopTimingFlag) {
            stopTimingFlag = false;
            currentlyTiming = false;
            PerformanceTimer.dump();
            PerformanceTimer.clear();
        }
        if(currentlyTiming) {
            PerformanceTimer.begin("EnterFrame");
        }
    }
    private static function happensAfterEnterFrame(e:Event):void {
        if(currentlyTiming) {
            PerformanceTimer.end("EnterFrame");
            PerformanceTimer.begin("Regular stuff");
        }
    }
    private static function happensAfterExitFrame(e:Event):void {
        if(currentlyTiming) {
            PerformanceTimer.end("Regular stuff");
            PerformanceTimer.begin("Rendering");
        }
    }
    private static function happensBeforeRender(e:Event):void {
        if(currentlyTiming) {
            PerformanceTimer.begin("Render setup");
        }
    }
    private static function happensAfterRender(e:Event):void {
        if(currentlyTiming) {
            PerformanceTimer.end("Render setup");
        }
    }
}
}
