package com.alleni.author.util
{
	import flash.geom.Point;
	
	public class CustomPoint extends Point
	{
		public function CustomPoint(x:Number=0, y:Number=0)
		{
			super(x, y);
		}
		
		override public function toString():String
		{
			return "[CustomPoint " + super.toString() + "]";
		}
		
		public function toJson():Object
		{
			return {type:"CustomPoint", x:x, y:y};
		}
				
		public static function fromJson(json:Object):CustomPoint 
		{
			return new CustomPoint(json.x, json.y);
		}
		
		public function clonePoint():CustomPoint
		{
			return new CustomPoint(x,y);
		}
	}
}