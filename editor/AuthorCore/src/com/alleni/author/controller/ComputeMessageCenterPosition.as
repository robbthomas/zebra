package com.alleni.author.controller
{
	import com.alleni.author.view.objects.ArenaControlBar;
	import com.alleni.author.view.ui.MessageCenterView;
	
	import flash.geom.Point;

	public class ComputeMessageCenterPosition
	{
		/**
		 * Compute message center position, along a line drawn from center of object, at specified angle.
		 * As close to the object as possible.
		 * @param width = width of object.
		 * @param height = height of object.
		 * @param angleIn = angle in degrees, with 0 being down, +1 going counter clockwise.
		 * @return = offsets from the topLeft of object.
		 * 
		 */
		public static function computeMessageCenterPosition( width:Number, height:Number, angleIn:Number ):Point
		{
			var pt:Point = computeControllerPosition(width,height,angleIn,MessageCenterView.WIDTH);
			// trace("{width:"+width+", height:"+height+", angle:"+angleIn+", resultX:"+pt.x+", resultY:"+pt.y+"},");
			return pt;
		}
		
		public static function computeFlipbookControlPosition( width:Number, height:Number, angleIn:Number ):Point
		{
			return computeControllerPosition(width,height,angleIn+180,ArenaControlBar.WIDTH);
		}
		
		
		private static function computeControllerPosition( width:Number, height:Number, angleIn:Number, controlWidth:Number):Point
		{
			//trace("computeControllerPosition: width="+width, "height="+height, "angle="+angleIn, "controlWidth="+controlWidth);
			
			// the angle of the line from topLeft to bottomRight
			var diagAngle:Number = Math.atan2(width,height);
			
			// length of line from center to corner
			var centerToCorner:Number = Math.sqrt((width/2)*(width/2) + (height/2)*(height/2));
			
			// angle increment covering horizontal portion of a corner.  cornerAngle = half of the corner.
			var cornerAngle:Number;

			var ratio:Number = controlWidth/(2*centerToCorner);
			if (ratio < 1.0)
				cornerAngle = Math.asin(ratio);
			else
				cornerAngle = Math.PI/2;  // max corner angle is 90 (for half the corner)  this occurs when obj is smaller than MC width
			
			// normalize the input angle, forcing min angle to be left end of bottom edge, not including corner
			var angle:Number = (angleIn*Math.PI/180) % (2*Math.PI);
			if (angle > Math.PI)
				angle -= 2*Math.PI;
			while (angle < Math.min(-diagAngle + cornerAngle, 0))  // the min() prevents bottomLeft corner wrapping around into bottomRight corner, for a thin object
				angle += 2*Math.PI;
			
			//trace("computeMessageCenterPosition w="+int(width), "h="+int(height), "diag="+deg(diagAngle), "corner="+deg(cornerAngle), "angleIn="+int(angleIn), "angle="+deg(angle));
			
			// try the bottom edge
			var point:Point = new Point();
			if (angle <= diagAngle - cornerAngle) {
				//trace("bottom");
				point.x = 0;
				point.y = height/2;
				return positionAlongEdge(point,height/2,1,0,angle,controlWidth); // faceAngle = zero, meaning straight down
			}
			
			// bottom right corner  
			// (spans 2*cornerAngle, but limit to prevent wrapping into next corner ... for very thin objects)
			if (angle <= Math.min(diagAngle + cornerAngle, Math.PI/2)) {
				//trace("bottomRight");
				point.x = width/2;
				point.y = height/2;
				return positionAlongCorner(point,centerToCorner,diagAngle,angle);
			}
			
			// try the right edge
			if (angle < Math.PI - diagAngle - cornerAngle) {
				//trace("right");
				point.x = width/2;
				point.y = 0;
				return positionAlongEdge(point, width/2, 0,1, angle - Math.PI/2,controlWidth);  // faceAngle = 90, meaning rightward
			}
			
			// upper right corner
			if (angle <= Math.min(Math.PI - diagAngle + cornerAngle, Math.PI)) {
				point.x = width/2;
				point.y = -height/2;
				//trace("upperRight");
				return positionAlongCorner(point,centerToCorner,Math.PI - diagAngle,angle);
			}
			
			// top side
			if (angle < Math.PI + diagAngle - cornerAngle) {
				point.x = 0;
				point.y = -height/2;
				//trace("top side");
				return positionAlongEdge(point, height/2, -1,0, angle - Math.PI,controlWidth);  // faceAngle = 180
			}
			
			// upper left corner
			if (angle <= Math.min(Math.PI + diagAngle + cornerAngle, 3*Math.PI/2)) {
				point.x = -width/2;
				point.y = -height/2;
				//trace("upperLeft");
				return positionAlongCorner(point,centerToCorner,Math.PI + diagAngle,angle);
			}
			
			// try the left edge
			if (angle < 2*Math.PI - diagAngle - cornerAngle) {  // left side begins at 360 - diag
				//trace("left");
				point.x = -width/2;
				point.y = 0;
				return positionAlongEdge(point, width/2, 0,-1, angle-3*Math.PI/2,controlWidth);  // faceAngle = 270
			}
			
			// must be the lower left corner
			point.x = -width/2;
			point.y = height/2;
			//trace("lowerLeft");
			return positionAlongCorner(point,centerToCorner,2*Math.PI - diagAngle,angle);
		}
		
		
		/**
		 * Compute position of MC along an object edge. 
		 * @param handlePoint = location of handle at center of edge, offset from topLeft of object
		 * @param centerDist = distance from above point to center of object
		 * @param faceCos = cosine of angle of direction this edge faces.
		 * @param faceSin = sine of angle of direction edge faces.
		 * @param angle = angle in radians of the line along which the MC will be placed.  Zero means straight out from edge (perp to edge)
		 * @return offsets from topLeft of object to the MC location.
		 * 
		 */
		private static function positionAlongEdge( handlePoint:Point, centerDist:Number, faceCos:Number, faceSin:Number, angle:Number, controlWidth:Number ):Point
		{
			var point:Point = handlePoint.clone();  // location of handle in middle of edge
			if (angle < 0.01 && angle > -0.01) 
				return point;  // when angle is near zero (radians), use the bottom point
			
			var posAngle:Number = Math.abs(angle);
			var dx:Number = (controlWidth/2 * Math.sin(posAngle) + centerDist) * Math.tan(posAngle);
			if (angle < 0)  dx = -dx;
			var dy:Number = controlWidth * Math.sin(posAngle) / 2;
			
			var ddx:Number = dx*faceCos + dy*faceSin;
			var ddy:Number = -dx*faceSin + dy*faceCos;
			point.x += ddx;
			point.y += ddy;
			return point;
		}
		
		/**
		 * Compute location of MC on an object corner.
		 * This is used for the span of angle where the width of the MC touches the corner. 
		 * @param handlePoint = location of the corner: offset from topLeft of object.
		 * @param centerToCorner = length of diagonal from center of object to corner.
		 * @param diagAngle = the angle of the line from center to this corner.
		 * @param angle = the line along which the MC will be placed.
		 * @return MC location, as offsets from the topLeft of object.
		 * 
		 */
		private static function positionAlongCorner( handlePoint:Point, centerToCorner:Number, diagAngle:Number, angle:Number ):Point
		{
			var b:Number = angle - diagAngle;
			var dx:Number = centerToCorner * Math.sin(b) * Math.cos(angle);
			var dy:Number = -centerToCorner * Math.sin(b) * Math.sin(angle);
			var point:Point = handlePoint.clone();
			point.x += dx;
			point.y += dy;
			return point;
		}
		
		
		/**
		 * For debug tracing, show a radian angle value as integer degrees. 
		 * @param radians = the angle.
		 * @return string representing the angle.
		 * 
		 */
		private static function deg( radians:Number ):String
		{
			var degrees:int = radians * 180 / Math.PI;
			return degrees.toString();
		}

	}
}