package com.alleni.author.controller
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.display.DisplayObject;
	import flash.display.Stage;
	import flash.geom.Point;
	
	import mx.core.FlexGlobals;
	import mx.managers.SystemManager;

	public class ObjectAlignmentManager
	{
		public static const LEFT_BASED:String = "Left Based";
        public static const CENTER_BASED:String = "Center Based";
        public static const RIGHT_BASED:String = "Right Based";

        public static const TOP_BASED:String = "Top Based";
        public static const MIDDLE_BASED:String = "Middle Based";
        public static const BOTTOM_BASED:String = "Bottom Based";

        public static const ANCHOR_BASED:String = "Anchor Based";

		/**
		 * 
		 * @param ao
		 * @return 
		 * 
		 */
		private static function localToGlobal(ao:AbstractObject):Point
		{
			return ao.getView().parent.localToGlobal(new Point(ao.x,ao.y));
		}	
		
		/**
		 * 
		 * @param model
		 * @return 
		 * 
		 */
		private static function o(model:TaconiteModel):AbstractObject
		{
			return AbstractObject(model.value);
		}
		
		/**
		 * 
		 * @param ao
		 * @return 
		 * 
		 */
		private static function centerX(ao:AbstractObject):Number
		{
			return (ao.x+ao.left)+(ao.width/2);
		}
		
		/**
		 * 
		 * @param ao
		 * @return 
		 * 
		 */
		private static function centerY(ao:AbstractObject):Number
		{
			return (ao.y+ao.top)+(ao.height/2);
		}
		
		/**
		 * 
		 * @param selectedModels
		 * @return 
		 * 
		 */
		private static function findFurthestLeft(selectedModels:Vector.<TaconiteModel>, basedOn:String = CENTER_BASED):Number
		{
			var furthest:Number = Number.MAX_VALUE;
			for each (var m:TaconiteModel in selectedModels)
			{
                var locationX:Number;
                switch(basedOn){
                    case LEFT_BASED:
                        if(localToGlobal(o(m)).x + o(m).left < furthest) furthest = localToGlobal(o(m)).x + o(m).left;
                        break;
                    case CENTER_BASED:
                       locationX = o(m).getView().parent.localToGlobal(new Point(centerX(o(m)), centerY(o(m)))).x;
                       if(locationX < furthest) furthest = locationX;
                       break;
                    case RIGHT_BASED:
                        if(localToGlobal(o(m)).x + o(m).right < furthest) furthest = localToGlobal(o(m)).x + o(m).right;
                        break;
                    case ANCHOR_BASED:

                        if(localToGlobal(o(m)).x < furthest) furthest = localToGlobal(o(m)).x;
                }
				
			} 
			return furthest;
		}
		
		/**
		 * 
		 * @param selectedModels
		 * @return 
		 * 
		 */
		private static function findFurthestRight(selectedModels:Vector.<TaconiteModel>, basedOn:String = CENTER_BASED):Number
		{
			var furthest:Number = Number.MIN_VALUE;
			for each (var m:TaconiteModel in selectedModels)
			{
                var locationX:Number;
                switch(basedOn){
                    case LEFT_BASED:
                        if(localToGlobal(o(m)).x + o(m).left > furthest) furthest = localToGlobal(o(m)).x + o(m).left;
                        break;
                    case CENTER_BASED:
                       locationX = o(m).getView().parent.localToGlobal(new Point(centerX(o(m)), centerY(o(m)))).x;
                       if(locationX > furthest) furthest = locationX;
                       break;
                    case RIGHT_BASED:
                        if(localToGlobal(o(m)).x + o(m).right > furthest) furthest = localToGlobal(o(m)).x + o(m).right;
                        break;
                    case ANCHOR_BASED:
                        if(localToGlobal(o(m)).x > furthest) furthest = localToGlobal(o(m)).x;
                }
				
			}
			return furthest;
		}
		
		/**
		 * 
		 * @param selectedModels
		 * @return 
		 * 
		 */
		private static function findFurthestTop(selectedModels:Vector.<TaconiteModel>, basedOn:String = MIDDLE_BASED):Number
		{
			var furthest:Number = Number.MAX_VALUE;
			for each (var m:TaconiteModel in selectedModels)
			{
				var locationY:Number;
                switch(basedOn){
                    case TOP_BASED:
                        if(localToGlobal(o(m)).y + o(m).top < furthest) furthest = localToGlobal(o(m)).y + o(m).top;
                        break;
                    case MIDDLE_BASED:
                       locationY = o(m).getView().parent.localToGlobal(new Point(centerX(o(m)), centerY(o(m)))).y;
                       if(locationY < furthest) furthest = locationY;
                       break;
                    case BOTTOM_BASED:
                        if(localToGlobal(o(m)).y + o(m).bottom < furthest) furthest = localToGlobal(o(m)).y + o(m).bottom;
                        break;
                    case ANCHOR_BASED:
                        if(localToGlobal(o(m)).y < furthest) furthest = localToGlobal(o(m)).y;
                }
				
			}
			return furthest;
		}
		
		/**
		 * 
		 * @param selectedModels
		 * @return 
		 * 
		 */
		private static function findFurthestBottom(selectedModels:Vector.<TaconiteModel>, basedOn:String = MIDDLE_BASED):Number
		{
			var furthest:Number = Number.MIN_VALUE;
			for each (var m:TaconiteModel in selectedModels)
			{
                var locationY:Number;
                switch(basedOn){
                    case TOP_BASED:
                        if(localToGlobal(o(m)).y + o(m).top > furthest) furthest = localToGlobal(o(m)).y + o(m).top;
                        break;
                    case MIDDLE_BASED:
                       locationY = o(m).getView().parent.localToGlobal(new Point(centerX(o(m)), centerY(o(m)))).y;
                       if(locationY > furthest) furthest = locationY;
                       break;
                    case BOTTOM_BASED:
                        if(localToGlobal(o(m)).y + o(m).bottom > furthest) furthest = localToGlobal(o(m)).y + o(m).bottom;
                        break;
                    case ANCHOR_BASED:
                        if(localToGlobal(o(m)).y > furthest) furthest = localToGlobal(o(m)).y;
                }
			}
			return furthest;
		}
		
		/**
		 * 
		 * @param selectedModels
		 * 
		 */
		public static function objAlignLeft(selectedModels:Vector.<TaconiteModel>,useAnchors:Boolean=false):void
		{
			var furthest:Number = findFurthestLeft(selectedModels, LEFT_BASED);
			
			for each (var m:TaconiteModel in selectedModels)
			{
				o(m).x = o(m).getView().parent.globalToLocal(new Point(furthest,0)).x - o(m).left;
			}
		}
		
		/**
		 * 
		 * @param selectedModels
		 * 
		 */
		public static function objAlignRight(selectedModels:Vector.<TaconiteModel>,useAnchors:Boolean=false):void
		{
			var furthest:Number = findFurthestRight(selectedModels, RIGHT_BASED);

			for each (var m:TaconiteModel in selectedModels)
			{
				o(m).x = o(m).getView().parent.globalToLocal(new Point(furthest,0)).x - o(m).right;
			}
		}
		
		/**
		 * 
		 * @param selectedModels
		 * 
		 */
		public static function objAlignTop(selectedModels:Vector.<TaconiteModel>,useAnchors:Boolean=false):void
		{
			var furthest:Number = findFurthestTop(selectedModels, TOP_BASED);
			
			for each (var m:TaconiteModel in selectedModels)
			{
				o(m).y = o(m).getView().parent.globalToLocal(new Point(0,furthest)).y - o(m).top;
			}
		}
		
		/**
		 * 
		 * @param selectedModels
		 * 
		 */
		public static function objAlignBottom(selectedModels:Vector.<TaconiteModel>,useAnchors:Boolean=false):void
		{
			var furthest:Number = findFurthestBottom(selectedModels, BOTTOM_BASED);
			
			for each (var m:TaconiteModel in selectedModels)
			{
				o(m).y = o(m).getView().parent.globalToLocal(new Point(0,furthest)).y - o(m).bottom;
			}
		}
		
		public static function objAlignHorizontal(selectedModels:Vector.<TaconiteModel>,useAnchors:Boolean=false):void
		{
			var top:Number = findFurthestTop(selectedModels);
			var bottom:Number = findFurthestBottom(selectedModels);
			var middle:Number = (top+bottom)/2;
			var applyAnchorAdjust:Boolean = true;
			
			for each (var m:TaconiteModel in selectedModels)
			{
				o(m).y = o(m).getView().parent.globalToLocal(new Point(0,middle)).y;
				// adjust for anchor, if needed
				if(o(m).y != centerY(o(m))) o(m).y -= (o(m).height/2)+o(m).top;
			}
		}
		
		public static function objAlignVertical(selectedModels:Vector.<TaconiteModel>,useAnchors:Boolean=false):void
		{
			var left:Number = findFurthestLeft(selectedModels);
			var right:Number = findFurthestRight(selectedModels);
			var middle:Number = (left+right)/2;
			
			for each (var m:TaconiteModel in selectedModels)
			{
				
				o(m).x = o(m).getView().parent.globalToLocal(new Point(middle,0)).x;
				// adjust for anchor, if needed
				if(o(m).x != centerX(o(m))) o(m).x -= (o(m).width/2)+o(m).left;
			}
		}
		
		public static function objDistributeHorizontal(selectedModels:Vector.<TaconiteModel>,useAnchors:Boolean=false):void
		{
			var left:Number = findFurthestLeft(selectedModels, CENTER_BASED);
			var right:Number = findFurthestRight(selectedModels, CENTER_BASED);
			var spacing:Number = (right-left)/(selectedModels.length-1);
			var current:Number = left;
			var sorted:Array = new Array();
			var objToDist:ObjectToDistribute;
			
			for (var i:uint = 0; i<selectedModels.length; i++)
			{
				objToDist = new ObjectToDistribute();
				objToDist.ao = o(selectedModels[i]);
				objToDist.globalX = localToGlobal(objToDist.ao).x;
				sorted[sorted.length] = objToDist;
			}
			
			sorted.sortOn("globalX",Array.NUMERIC);
			
			for each (var otd:ObjectToDistribute in sorted)
			{
				otd.ao.x = otd.ao.getView().parent.globalToLocal(new Point(current,0)).x 
				if(otd.ao.x != centerX(otd.ao)) otd.ao.x -= (otd.ao.width/2)+otd.ao.left;
				current+=spacing;
			}
		}
		
		public static function objDistributeVertical(selectedModels:Vector.<TaconiteModel>,useAnchors:Boolean=false):void
		{
			var top:Number = findFurthestTop(selectedModels);
			var bottom:Number = findFurthestBottom(selectedModels);
			var spacing:Number = (bottom-top)/(selectedModels.length-1);
			var current:Number = top;
			var sorted:Array = new Array();
			var objToDist:ObjectToDistribute;
			
			for (var i:uint = 0; i<selectedModels.length; i++)
			{
				objToDist = new ObjectToDistribute();
				objToDist.ao = o(selectedModels[i]);
				objToDist.globalY = localToGlobal(objToDist.ao).y;
				sorted[sorted.length] = objToDist;
			}
			
			sorted.sortOn("globalY",Array.NUMERIC);
			
			for each (var otd:ObjectToDistribute in sorted)
			{
				otd.ao.y = otd.ao.getView().parent.globalToLocal(new Point(0,current)).y
				if(otd.ao.y != centerY(otd.ao)) otd.ao.y -= (otd.ao.height/2)+otd.ao.top;
				current+=spacing;
			}
		}
	}
}
import com.alleni.author.model.AbstractObject;

class ObjectToDistribute
{
	public var ao:AbstractObject;
	public var globalX:Number;
	public var globalY:Number;
}