/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.FlowView;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.app.ProjectController;
import com.alleni.author.controller.objects.PathEditingMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.controller.ui.GadgetController;
	import com.alleni.author.controller.ui.WireController;
	import com.alleni.author.controller.ui.palettes.InspectorController;
	import com.alleni.author.controller.ui.palettes.ToolboxController;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.definition.action.CreateObjectAction;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.TextObjectEvent;
	import com.alleni.author.event.ToolEvent;
	import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.AbstractExternalObject;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.TextObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.WorldView;
	import com.alleni.taconite.controller.DragMediator;
	import com.alleni.taconite.definition.HandleRoles;
	import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.ModelCollectionEvent;
import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;

    /**
     * Mediator for the WorldView that adds a new clock at a clicked location, but for
     * a drag gesture draws a marquee rectangle that selects enclosed objects.
     */
    public class WorldMediator extends DragMediator
    {
        private var _worldView:WorldView;
		private var _role:uint;
		
		private var _wireController:WireController = ApplicationController.instance.wireController;
		private var _authorController:AuthorController = ApplicationController.instance.authorController;
		private var _assetController:AssetController = AssetController.instance;
		
		private var _createdObject:AbstractObject;
		private var _createPoint:Point;
		private var _drewPathObj:Boolean;
		
		private var _origCenter:Point;
		private var _zoomMarqueeMediator:MarqueeMediator;
		private var _zoomingIn:Boolean = true;

		
        public function WorldMediator(context:ViewContext, role:uint)
        {
            super(context);
			_role = role;
        }
        
        /**
         * When asked to work with an ObjectView, take note of the view and add a listener for mouseDown.
         */
        public function handleViewEvents(view:WorldView):WorldMediator
        {
            _worldView = view;
			//trace("WorldMediator::handleViewEvents", view, "role="+view.role, "mediatorSerial="+_mediatorSerial);
			
			_context.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);  // object creation anywhere, and marquee on backstage
			view.addEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);

			Application.instance.viewContext.addEventListener(TaconiteFactory.getClipboardImplementation().DRAG_ENTER, context.controller.handleDragEnter);
			Application.instance.viewContext.addEventListener(TaconiteFactory.getClipboardImplementation().DRAG_DROP, context.controller.handleDragDrop);
			
			// model events which impact the views
			if (_worldView.role == ViewRoles.PRIMARY) {
				_worldView.model.addEventListener(ModelEvent.MODEL_CHANGE, handleModelChange, false, 0, true);
			}
			
			if (_worldView.stage) handleAddedToStage(null);
			else _worldView.addEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
			
			return this;
        }
		
		private function handleAddedToStage(event:Event):void
		{
			_worldView.removeEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);

			if (_role == ViewRoles.PRIMARY) WorldContainer(_context).addEventListener(Event.ENTER_FRAME, lookUnderMouse);
		}

		public function clearViewEvents():void
		{
			//trace("WorldMediator::clearViewEvents: mediatorSerial="+_mediatorSerial);
			
			_context.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			_worldView.removeEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
			
			Application.instance.viewContext.removeEventListener(TaconiteFactory.getClipboardImplementation().DRAG_ENTER, context.controller.handleDragEnter);
			Application.instance.viewContext.removeEventListener(TaconiteFactory.getClipboardImplementation().DRAG_DROP, context.controller.handleDragDrop);
			
			_worldView.model.removeEventListener(ModelEvent.MODEL_CHANGE, handleModelChange);
		}
		
		private function handleModelChange(e:ModelEvent):void
		{
            if (!e.moving) {   // not reparenting or re-layering
                var ce:ModelCollectionEvent = e as ModelCollectionEvent;
                if (ce) {
                    if (ce.parent.isValueListProperty(ce.property)) {   // "pages" & "masterPages" lists but not "masters" list
                        switch (ce.kind) {
                            case ModelEvent.REMOVE_ARRAY_ITEM:
                                handleObjectRemoved(ce.item as AbstractObject);
                                break;
                        }
                    }
                } else {
                    switch (e.kind) {
                        case ModelEvent.REMOVE_CHILD_MODEL:
                            handleObjectRemoved(e.child.value as AbstractObject);
                            break;
                    }
                }
            }
		}

        private function handleObjectRemoved(obj:AbstractObject):void
        {
            // remove wires for objects being removed
            _wireController.handleObjectDeleted(obj);
        }

        override public function handleMouseDown(e:MouseEvent):void
        {
            if(_createdObject != null) {
                // Somehow we managed to see a mouse down before we saw a mouse up from creating the previous object.
                // This is likely because the mouse moved outside the window and then the mouse button went up.
                // In this case we'll ignore this mouse down so that the next mouse up will finish this object.
                return;
            }
            var flowView:FlowView = World(_worldView.object).eventFlow.flowView;
            if (flowView && flowView.visible && flowView.hitTestPoint(e.stageX, e.stageY)) {
                if (Application.instance.currentTool != ToolboxController.MOVE && Application.instance.currentTool != ToolboxController.ZOOM) {
                    return;  // ignore attempt to draw ontop of flowView
                }
            }
//			trace("WorldMediator::handleMouseDown role="+_worldView.role,"tool="+Application.instance.currentTool,
//					"editing="+World(_worldView.model.value).editing, "mediatorSerial="+_mediatorSerial);
			_worldView.stage.focus = _worldView.stage; // focus the stage on mouse down by default for proper hotkey support

			if (!Application.runningLocked) {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.STAGE_MOUSE_DOWN));
                InspectorController.instance.listeningToSelection = false;
			}

			var initialDragResize:Boolean = true;
			drewPathObj = false;
			var controller:AuthorController = _authorController;
            _createdObject = null;
            switch (Application.instance.currentTool) {
                case ToolboxController.ARROW:
                    // pass this event onto the Marquee mediator in case we are selecting objects
                    if (!Application.runningLocked) {
                        if (!editingPath && _worldView.world.eventFlow.editingPage == null) {
                            // marquee to select backstage objects
                            new MarqueeSelectionMediator(context, _worldView, _worldView.world.eventFlow.objects).handleMouseDown(e);
                            break;  // stop propagation
                        }
                    }
                    return;  // don't stop propagation .. let vellum mediator have it
                case ToolboxController.MOVE:
                    _origCenter = WorldContainer(_context).centerOffset;
                    _dragPoint = new Point(e.stageX, e.stageY);
                    break;
                case ToolboxController.ZOOM:
                    if (e.shiftKey) {
                        _zoomingIn = false;
                        ApplicationController.instance.handleZoomOut();
                    } else {
                        _zoomingIn = true;
                        // create mediator for zoom marquee
                        _zoomMarqueeMediator = new MarqueeMediator(_context,_worldView,handleZoomMarquee,handleZoomClick);
                        _zoomMarqueeMediator.handleMouseDown(e);
                    }
                    break;
                case ToolboxController.ARENA:
                    _createdObject = addObjectAtMouse("Arena");
                    break;
                case ToolboxController.AUDIO:
                    _createdObject = addObjectAtMouse("Audio");
                    break;
                case ToolboxController.BUTTON:
                    _createdObject = addObjectAtMouse("PushButton");
                    break;
                case ToolboxController.CHECKBOX:
                    _createdObject = addObjectAtMouse("Checkbox");
                    break;
                case ToolboxController.RADIOBUTTON:
                    _createdObject = addObjectAtMouse("RadioButton");
                    break;
                case ToolboxController.CLOCK:
                    _createdObject = addObjectAtMouse("Clock");
                    break;
                case ToolboxController.INPUTTEXT:
                    _createdObject = addObjectAtMouse("InputText");
                    break;
                case ToolboxController.ELLIPSE:
                    _createdObject = addObjectAtMouse("Oval");
                    break;
                case ToolboxController.RECTANGLE:
                    _createdObject = addObjectAtMouse("RectangleObject");
                    break;
                case ToolboxController.LINE:
                    _createdObject = addObjectAtMouse("Line");
                    break;
                case ToolboxController.ANIMATION_PATH:
                    _createdObject = PathEditingMediator.mouseDownWithTool(e.stageX, e.stageY);
                    drewPathObj = true;
                    initialDragResize = false;
                    break;
                case ToolboxController.POLYGON:
                    _createdObject = addObjectAtMouse("PathObject", 10, 10);
                    PathEditingMediator.setupPolygon(_createdObject);
                    break;
                case ToolboxController.SLIDER:
                    _createdObject = addObjectAtMouse("Slider");
                    break;
                case ToolboxController.TEXT:
                    _createdObject = addObjectAtMouse("TextObject");
                    break;
                case ToolboxController.TRUTHTABLE:
                    _createdObject = addObjectAtMouse("TruthTable");
                    break;
                case ToolboxController.ANSWERTABLE:
                    _createdObject = addObjectAtMouse("AnswerTable");
                    break;
                case ToolboxController.STATETABLE:
                    _createdObject = addObjectAtMouse("StateTable");
                    break;
                case ToolboxController.GOTOURL:
                    _createdObject = addObjectAtMouse("GotoURL");
                    break;
                case ToolboxController.VIDEO:
                    _createdObject = addObjectAtMouse("VideoObject");
                    break;
                case ToolboxController.MAP:
                    _createdObject = addObjectAtMouse("MapObject");
                    break;
                case ToolboxController.QUEUE:
                    _createdObject = addObjectAtMouse("QueueConnection");
                    break;
                case ToolboxController.BROADCAST_CONNECTION:
                    _createdObject = addObjectAtMouse("BroadcastConnection");
                    break;
                case ToolboxController.METRICS_CONNECTION:
                    _createdObject = addObjectAtMouse("MetricsConnection");
                    break;
                case ToolboxController.CALCULATOR:
                    _createdObject = addObjectAtMouse("Calculator");
                    break;
                default:
                    break;
            }

            if (_createdObject != null) {
                // turn on draggingHandle immediately
                // to more accurately reflect the state
                // of this object immediately after creation
                _createdObject.draggingHandle = true;
                _createPoint = new Point(_worldView.stage.mouseX, _worldView.stage.mouseY);

                // initialize inspector, message center, etc. at the controller
                _createdObject.controller.handleCreationInitialize(_createdObject);

                if(initialDragResize) {
                    var mediator:PointDragMediator = new PointDragMediator(context, HandleRoles.RESIZE_RIGHT + HandleRoles.RESIZE_DOWN,
                        _createdObject, false, false);
                    mediator.groupName = null;
                    mediator.handleDragging(_createdObject.getView(_context), e);
                }
                // toggle off draggingHandle if object has special needs
                if(!initialDragResize || _createdObject as TextObject) {
                    _createdObject.draggingHandle = false;
                }
            }

            // pass down to the supermediator so we get the handlers set up in case we need them
            super.handleMouseDown(e);
        }

        private function addObjectAtMouse(className:String, width:Number = 0, height:Number = 0):AbstractObject
        {
            // add object to the current event
            // at obj.complete=true, ObjectController will drop into container (eg. gadget vellum or Arena)
            var flow:EventFlow = World(_worldView.object).eventFlow;
            var parent:AbstractContainer = flow.editingPage == null ? flow : flow.editingPage as EventPage;
            return _authorController.addObjectForName(className, _worldView.mouseX, _worldView.mouseY, width, height, true, parent);
        }
		
		private function handleZoomMarquee(zoomRect:Rectangle):void
		{
			ApplicationController.instance.handleZoomInRect(zoomRect);
			_zoomMarqueeMediator = null;
		}
		
		private function handleZoomClick():void
		{
			if (!_zoomingIn) {
				ApplicationController.instance.handleZoomOut();
			} else {
				ApplicationController.instance.handleZoomIn();
			}
			_zoomMarqueeMediator = null;
		}
		
		
		private function lookUnderMouse(e:Event):void
		{
			if (!_context.stage) return;
			var w:WorldContainer = _context as WorldContainer;
			
            // shift the previous views to the old list
			w.oldViewsUnderMouse = w.viewsUnderMouse;
            // collect a new candidate view
			var newView:ObjectView = w.worldView.findViewUnderPoint(new Point(w.stage.mouseX,w.stage.mouseY));

            // collect all views under the mouse
            w.viewsUnderMouse = new Vector.<ObjectView>();
            while (newView != null) {
                w.viewsUnderMouse.push(newView);
                newView = newView.parentObjectView;
            }

            // find the point in both lists where they diverge (they both probably end with WorldView, and may share other common parents
            // these common parents get neither MouseOver or MouseOut
            var commonOldIndex:int = w.oldViewsUnderMouse.length;
            var commonNewIndex:int = w.viewsUnderMouse.length;
            while(commonOldIndex > 0 && commonNewIndex > 0 && w.oldViewsUnderMouse[commonOldIndex-1] == w.viewsUnderMouse[commonNewIndex-1]) {
                commonOldIndex--;
                commonNewIndex--;
            }

			// uncomment below to get a trace whenever view under mouse changes.
//			if(commonOldIndex > 0 || commonNewIndex > 0) {
//				var names:Function = function(vector:Vector.<ObjectView>):String {
//					var result:Array = [];
//					for each(var view:ObjectView in vector) {
//						result.push(view?view.object.title:null);
//					}
//					return result.join(",");
//				};
//				trace(names(w.oldViewsUnderMouse), "--", names(w.viewsUnderMouse));
//			}

            // iterate over both lists going only up to the point where they share a common root
            var i:int;
            var v:ObjectView;
            for(i = 0; i < commonOldIndex; i++) {
                v = w.oldViewsUnderMouse[i];
                v.interceptMouseOut(null, v);
            }
            for(i = 0; i < commonNewIndex; i++) {
                v = w.viewsUnderMouse[i];
                v.interceptMouseOver(null, v);
            }
		}
		
		
		override protected function handleMouseMove(e:MouseEvent):void
		{
//			var v:ObjectView = CollisionMediator.getViewUnderPoint(new Point(e.stageX, e.stageY)) as ObjectView;
//			if (v)
//				var m:TaconiteModel = v.model;
//			trace("WorldMediator, modelUnderPoint="+m);
			
			var current:Point = new Point(e.stageX, e.stageY);
			if (!_dragStarted
				&& e.buttonDown
				&& _dragPoint != null
				&& (Math.abs(current.subtract(_dragPoint).x) >= minimumDrag
					|| Math.abs(current.subtract(_dragPoint).y) >= minimumDrag))
			{
				_dragStarted = true;
				handleDragStart(e);
			}
			
			if (_dragStarted) {
				handleDragMove(e);
			}
			if(_stopPropagation) {
				e.stopPropagation();
			}
		}
		
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
		{
			if (Application.instance.currentTool == ToolboxController.MOVE) {
				var current:Point = new Point(e.stageX, e.stageY);
				WorldContainer(_context).centerOffset = _origCenter.add(current.subtract(_dragPoint));
			}
		}
		
		override protected function handleMouseUp(e:MouseEvent) : void
		{
            if(_createdObject != null && _createdObject.getView() != null) {
                if (!Application.runningLocked && !editingPath)
                    InspectorController.instance.listeningToSelection = true;

                super.handleMouseUp(e);

                // set obj.complete = true, changeContainerAsNeeded, enforce minimum size ... via controller
                // so that the Drawing object can avoid these actions
                _createdObject.controller.handleCreationComplete(_createdObject, _createPoint);

				if (_createdObject as TextObject) {
	                // send event to text obj so it can open for editing
    	            _createdObject.getView().dispatchEvent(new TextObjectEvent(TextObjectEvent.OPEN_EDITING));
				}
                if (!drewPathObj)
                    ApplicationController.currentActionTree.commit(CreateObjectAction.fromObject(_createdObject));
            } else {
				super.handleMouseUp(e);
				InspectorController.instance.listeningToSelection = true;
			}
			
            // switch back to the arrow tool
            if (!drewPathObj && !(_authorController.currentEditingMediator as PathEditingMediator)) {
                var toolEvent:ToolEvent = new ToolEvent(ToolEvent.TOOL_APPLIED);
                ApplicationController.instance.dispatchEvent(toolEvent);
            }
			_createdObject = null;
            drewPathObj = false;
		}
		
		private function handleDoubleClick(event:MouseEvent):void
		{
			// PLAYERTODO
			// double-clicked world or another object and it bubbled to world, so Pause if currently running
			if (Application.running && !Application.locked) {
				ApplicationController.instance.authorController.pause();
			}
		}
		
		private function get editingPath():Boolean
		{
			return _authorController.currentEditingMediator as PathEditingMediator;
		}

        public function get drewPathObj():Boolean {
            return _drewPathObj;
        }
        public function set drewPathObj(value:Boolean):void {
            _drewPathObj = value;
            ProjectController.instance.isDrawingPath == value;
        }
    }
}

