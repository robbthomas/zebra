package com.alleni.author.controller
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.collision.CollisionDetector;
	
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Point;

	public class CollisionMediator
	{

		public static function viewIsOpaqueUnderPoint(view:ITaconiteView,point:Point):Boolean
		{
			var localPoint:Point = DisplayObject(view).globalToLocal(point);
			var matrix:Matrix = new Matrix();
			matrix.translate(-localPoint.x,-localPoint.y);

			var bmd:BitmapData = new BitmapData(1,1,true,0);
			try {
				bmd.draw(DisplayObject(view), matrix);
			} catch (e:Error) { // this was misbehaving when attempting to draw 3rd-party content. localize exception handling.
				return true;
			}
			return (bmd.getPixel32(0,0) > 0);
		}
		
		
		public static function checkDropTarget(obj:AbstractObject):void
		{
			var view:ObjectView = obj.getView();
			var dropped:Array = [];
			for each(var target:AbstractObject in obj.droppedPossibilities) {
				var targetView:ObjectView = target.getView();
				if(targetView && targetView.visible && targetView.alpha != 0 && view && CollisionDetector.instance.detectOverlap(view, targetView)) {
					dropped.push(target);
				}
			}
			obj.droppedOn = dropped;
		}
		
	}
}