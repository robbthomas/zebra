/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.action.ModifyRibbonAction;
	import com.alleni.author.definition.action.ObjectReshapeAction;
	import com.alleni.author.definition.action.ObjectShape;
	import com.alleni.author.event.PointDragEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.Line;
	import com.alleni.author.model.objects.TextObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.objects.LineView;
	import com.alleni.taconite.controller.DragMediator;
	import com.alleni.taconite.definition.HandleRoles;
	import com.alleni.taconite.definition.Handles;
	import com.alleni.taconite.lang.Geometry;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.TaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;

    /**
     * Mediator to resize an object in response to a handle being dragged.
     */
    public class PointDragMediator extends DragMediator
    {
        public var groupName:String = "Drag Handle";
    	
        private var _object:AbstractObject;
        private var _dragRole:uint;
        private var _view:ObjectView;
        private var _model:TaconiteModel;
		private var _oldRotation:int;
		private var _oldAnchorPoint:Point;
        private var _localHandlePosition:Point;
		
		// this info will be passed to other handle, when swapping drag, when handles are dragged past each other
		// (can only happen during dragResize(), not dragRotate or dragPivot)
		private var _originalRatio:Number;
	    private var _oldShape:ObjectShape;
		private var _lastMouseMovePoint:Point;
		private var _maintainApect:Boolean; // For objects like the clock and checkbox, initial draw should maintain aspect ratio
		       
		
        public function PointDragMediator(context:ViewContext, dragRole:uint, value:AbstractObject = null, maintainApectInitially:Boolean=false, stopPropagation:Boolean=true)
        {
            super(context, stopPropagation);
			_dragRole = dragRole;
            _object = value;
			_maintainApect = maintainApectInitially;
        }
        
		
        /**
         * When asked to work with a feedback handle, take note of the view and add a listener for mouseDown.
         */
        public function handleViewEvents(view:ITaconiteView, handle:Sprite):void
        {
            _view = view as ObjectView;
            _model = view.model;
            _object = view.model.value as AbstractObject;
			handle.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			if (HandleRoles.isMovePivot(_dragRole)) {
				handle.addEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
				handle.doubleClickEnabled = true;
			}
			_object.addEventListener(PointDragEvent.POINT_DRAG, swapHandlesListener);
        }
		
		private function get pivotHandleMovesWholeObject():Boolean
		{
			return HandleRoles.isMovePivot(_dragRole) && (_object.snapTo || !_object.hasCustomAnchor);
		}
		
		override protected function handleDragEnd(e:MouseEvent) : void
		{
			super.handleDragEnd(e);
			
			_maintainApect = false;
			_object.draggingHandle = false;
		}

        public function set localHandlePosition(p:Point):void
		{
            _localHandlePosition = p;
        }

        /**
         * At the start of a drag, capture the size of the selected object.
         */
        override protected function handleDragStart(event:MouseEvent):void
        {
			super.handleDragStart(event);

		    _originalRatio = (_object.width==0?1:_object.width) / (_object.height==0?1:_object.height);
	        _oldShape = ObjectShape.fromObject(_object);
	        _oldRotation = _object.rotation;
			_oldAnchorPoint = _object.anchorPoint;
			_object.draggingHandle = true;
			
			var ao:AbstractObject = _object as AbstractObject;
			if (ao && !_maintainApect)
				_maintainApect = ao.maintainAspect;
        }
		
		public function handleDragging(view:TaconiteView, e:MouseEvent):void
		{
			_view = view as ObjectView;
			handleMouseDown(e);
		}
        
		override public function handleMouseDown(e:MouseEvent):void
		{
			trace("mouseDown role="+_dragRole);
			if (pivotHandleMovesWholeObject) {
				_view.interceptMouseDown(e);  // drag whole object, when click pivot-handle and not in custom-anchor mode
				return;
			}
			if(_stopPropagation) {
				e.stopPropagation();
			}
			if(_context.stage == null || e.altKey || e.ctrlKey) {
				return;
			}
			
			_dragStarted = false;
			_lastMouseMovePoint = null;
			_dragPoint = WorldContainer(_context).globalToLocal(new Point(e.stageX, e.stageY));
			addDragListeners();
		}
		
		private function handleDoubleClick(e:MouseEvent):void
		{
			if (pivotHandleMovesWholeObject) {
				_view.dispatchEvent(e);  // allow editing a path or gadget by double-click pivot handle (eg. for small obj)
			}
		}
		
		private function addDragListeners():void
		{
			_context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_context.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			_context.stage.addEventListener(KeyboardEvent.KEY_DOWN, handleShiftChange);
			_context.stage.addEventListener(KeyboardEvent.KEY_UP, handleShiftChange);
		}

        private function removeDragListeners():void
        {
            _context.stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
            _context.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
            _context.stage.removeEventListener(KeyboardEvent.KEY_DOWN, handleShiftChange);
            _context.stage.removeEventListener(KeyboardEvent.KEY_UP, handleShiftChange);
        }

		protected function handleShiftChange(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.SHIFT && _lastMouseMovePoint != null) {
				// the shift key has changed, so re-run the last mouse-move since constraint is different now
				var local:Point = _view.globalToLocal(_lastMouseMovePoint);
				var event:MouseEvent = new MouseEvent(MouseEvent.MOUSE_MOVE, false, false, local.x, local.y, 
							null, e.ctrlKey, e.altKey, e.shiftKey);
				handleMouseMove(event);
			}		
		}

		override protected function handleMouseMove(e:MouseEvent):void
		{
			if(_view.stage == null){
                removeDragListeners();
                return;
            }

            _lastMouseMovePoint = new Point(e.stageX, e.stageY);
			
			if (!_dragStarted
				&& e.buttonDown
				&& _dragPoint != null
				&& (Math.abs(WorldContainer(_context).mouseX -_dragPoint.x) >= minimumDrag
					|| Math.abs(WorldContainer(_context).mouseY -_dragPoint.y) >= minimumDrag))
			{
				_dragStarted = true;
				handleDragStart(e);
			}
			
			if (_dragStarted) {
				handleDragMove(e);
				e.updateAfterEvent();
			}
			if (_stopPropagation) {
				e.stopPropagation();
			}
		}
		
        /**
         * For each move during the drag, resize the model appropriately.
         */
		override protected function handleDragMove(event:MouseEvent, doneDragging:Boolean=false):void
        {
			if (_object.editingLocked) // Prevent resizing and rotating locked objects
				return;
			
			if(HandleRoles.isRotate(_dragRole))
				dragRotate(event);
			else if (HandleRoles.isMovePivot(_dragRole))
				dragPivot(event);
			else
				dragResize(event);
		}
		
		private function dragRotate(event:MouseEvent):void
		{
			var mouse:Point = new Point(_view.parent.mouseX, _view.parent.mouseY);
			var handle:Point = _view.parent.globalToLocal(rotationHandleGlobalPosition(_view));
			var handleAngle:Number = Math.atan2(handle.y - _view.y, handle.x - _view.x)+Math.PI;
			var mouseAngle:Number = Math.atan2(mouse.y - _view.y, mouse.x - _view.x)+Math.PI;

            var drot1:Number = ((mouseAngle - handleAngle+Math.PI*2)%(Math.PI*2)) * 180 / Math.PI;
            var drot2:Number = ((mouseAngle - handleAngle-Math.PI*2)%(Math.PI*2)) * 180 / Math.PI;
//            trace(drot1," ",drot2);
//            trace(mouseAngle," ",handleAngle);
   			var rot:Number = (Math.abs(drot1)<Math.abs(drot2)? _object.rotation+drot1 : _object.rotation + drot2);
			
			if(event.shiftKey) {
				rot = Math.floor(rot/45 + .5)*45;
			}
			_object.rotation = rot;
		}
		
		private function dragPivot(event:MouseEvent):void
		{
			if (Math.abs(_object.width) > 0.1 && Math.abs(_object.height) > 0.1 && _object.hasCustomAnchor)  {
				var mouse:Point = _view.globalToLocal(new Point(event.stageX, event.stageY));
				
				var anchorX:Number = (mouse.x - _object.left) / _object.width;
				var anchorY:Number = (mouse.y - _object.top) / _object.height;
				_object.anchorPoint = new Point(anchorX, anchorY);
			}
		}

		private function dragResize(event:MouseEvent):void
		{	
			// bail out if Zoom=0, since its non-sensible and involves division by zero
			if (_object.scale == 0)
				return;

	        // grab the current bounding box of the object
	        var topLeft:Point = new Point(_object.left, _object.top);
	        var bottomRight:Point = new Point(_object.right, _object.bottom);
            var textIsAutoSizing:Boolean = _object is TextObject && (_object as TextObject).autoSizeEffective;

	        // the new desired position for the handle in this object's coordinate space
	        var mouse:Point = new Point(_view.mouseX, _view.mouseY);

	        if (event.shiftKey || _maintainApect && !textIsAutoSizing) {
				if (_view as LineView) {
					if(HandleRoles.isResizeLeft(_dragRole) && HandleRoles.isResizeUp(_dragRole)){
						mouse = Geometry.snapToDirection(bottomRight, mouse, true);
					}else if(HandleRoles.isResizeRight(_dragRole) && HandleRoles.isResizeDown(_dragRole)){
						mouse = Geometry.snapToDirection(topLeft, mouse, true);
					}
				} else {
					var xdir:Number = (HandleRoles.isResizeRight(_dragRole)? 1:-1);
					var ydir:Number = (HandleRoles.isResizeDown(_dragRole)? 1:-1);
                    var anchorPointOffset:Point = new Point(_object.anchorPoint.x*_object.width-_object.width/2,_object.anchorPoint.y*_object.height-_object.height/2);

                    if (_dragRole == HandleRoles.RESIZE_DOWN || _dragRole == HandleRoles.RESIZE_UP)
                        xdir = 0;
                    if (_dragRole == HandleRoles.RESIZE_LEFT || _dragRole == HandleRoles.RESIZE_RIGHT)
                        ydir = 0;


		        	mouse = Geometry.closestPointOnLineSegment(new Point(0, 0), new Point(100*_originalRatio*xdir, 100*ydir), mouse.add(anchorPointOffset), false);
                    if (_dragRole == HandleRoles.RESIZE_DOWN || _dragRole == HandleRoles.RESIZE_UP)
                        mouse.x = mouse.y*_originalRatio;
                    if (_dragRole == HandleRoles.RESIZE_LEFT || _dragRole == HandleRoles.RESIZE_RIGHT)
                        mouse.y = mouse.x/_originalRatio;

                    mouse = mouse.subtract(anchorPointOffset);

                    if (_dragRole == HandleRoles.RESIZE_DOWN || _dragRole == HandleRoles.RESIZE_UP) {
                        if (_dragRole == HandleRoles.RESIZE_DOWN)
                            bottomRight.x = mouse.x;
                        else
                            topLeft.x = mouse.x;
                    }
                    if (_dragRole == HandleRoles.RESIZE_LEFT || _dragRole == HandleRoles.RESIZE_RIGHT) {
                        if (_dragRole == HandleRoles.RESIZE_RIGHT)
                            bottomRight.y = mouse.y;
                        else
                            topLeft.y = mouse.y;
                    }
				}
	        }

	        // move the bounds of the object to get the handle to the mouse
			if(HandleRoles.isResizeUp(_dragRole) && !textIsAutoSizing){
				topLeft.y = mouse.y;
            }
	        if(HandleRoles.isResizeDown(_dragRole) && !textIsAutoSizing){
				bottomRight.y = mouse.y;
			}
	        if(HandleRoles.isResizeRight(_dragRole)){
				bottomRight.x = mouse.x;
			}
	        if(HandleRoles.isResizeLeft(_dragRole)){
				topLeft.x = mouse.x;
			}

	        // make sure width and height are not negative & swap the drag to another handle
			// (we will NOT change the role of any handle or mediator)
	        var tmp:Number;
			var newRole:uint = _dragRole;  // in case dragging will be swapped to another handle
	        if(_object is Line == false) {
				if(bottomRight.x < topLeft.x) {
					tmp = topLeft.x;
					topLeft.x = bottomRight.x;
					bottomRight.x = tmp;
					newRole = HandleRoles.flipHorizontal(newRole);
				}
				if(bottomRight.y < topLeft.y) {
					tmp = topLeft.y;
					topLeft.y = bottomRight.y;
					bottomRight.y = tmp;
					newRole = HandleRoles.flipVertical(newRole);
				}
				if (newRole != _dragRole) {
					// swap this drag operation to the other handle/mediator
					removeDragListeners();
					_dragStarted = false;
					_object.dispatchEvent(new PointDragEvent(PointDragEvent.SWAP_HANDLES, newRole,
								_originalRatio, _oldShape, _lastMouseMovePoint, _maintainApect));
				}
			}

	        // compute how the anchor point must be moved to keep everything else equal
	        var newLocation:Point = new Point();
	        newLocation.x = (bottomRight.x - topLeft.x) * _object.anchorPoint.x + topLeft.x;
	        newLocation.y = (bottomRight.y - topLeft.y) * _object.anchorPoint.y + topLeft.y;
	        newLocation = _view.parent.globalToLocal(_view.localToGlobal(newLocation));
	        _object.x = newLocation.x;
	        _object.y = newLocation.y;

	        // set the new bounds
	        _object.width = bottomRight.x - topLeft.x;
	        _object.height = bottomRight.y - topLeft.y;
        }

		override protected function handleMouseUp(e:MouseEvent):void
		{
			if(_view.stage == null){
                removeDragListeners();
                return;
            }

            super.handleMouseUp(e);

			if(_object.complete) {
				if(HandleRoles.isRotate(_dragRole)) {
					ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(_object, "rotation", _oldRotation, "Rotate " + _object.title));
					ApplicationController.instance.authorController.log("rotate " + _object.logStr);
				} else if(HandleRoles.isMovePivot(_dragRole)) {
					ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(_object, "anchorPoint", _oldAnchorPoint, "Adjust anchor " + _object.title));
					ApplicationController.instance.authorController.log("edit AnchorPoint " + _object.logStr);
				} else {
					ApplicationController.currentActionTree.commit(ObjectReshapeAction.fromObject(_object, _oldShape));
					ApplicationController.instance.authorController.log("reshape " + _object.logStr);
				}
			}
			_object.draggingHandle = false;
			removeDragListeners();
		}
		
		private function swapHandlesListener(event:PointDragEvent):void
		{
			if (event.kind == PointDragEvent.SWAP_HANDLES && event.role == _dragRole) {
				trace("swapHandles role="+event.role);
				_originalRatio = event.oldRatio;
				_oldShape = event.oldShape;
				_lastMouseMovePoint = event.lastMousePoint;
				_maintainApect = event.maintainAspectInitially;
				_dragStarted = true;
				addDragListeners();
			}
		}
		
		/**
		 * Compute position of rotation handle in local coordinates. 
		 * @param view
		 * @return 
		 * 
		 */
		private function rotationHandleGlobalPosition(view:ObjectView):Point
		{
            if (_localHandlePosition) {
                return view.localToGlobal(_localHandlePosition);
            } else {
                var obj:AbstractObject = view.object;
                var global:Point = view.localToGlobal(new Point(obj.right, obj.bottom));
                var radians:Number = view.computeNetRotation() * Math.PI / 180;
                global.x += Handles.ROTATION_HANDLE_OFFSET * Math.cos(radians);
                global.y += Handles.ROTATION_HANDLE_OFFSET * Math.sin(radians);
                return global;
            }
		}

        public function warnToDelete():void
		{
            removeDragListeners();
			_object.removeEventListener(PointDragEvent.POINT_DRAG, swapHandlesListener);
        }
	}
}
