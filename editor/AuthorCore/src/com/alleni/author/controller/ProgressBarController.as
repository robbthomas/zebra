package com.alleni.author.controller
{
    import com.alleni.author.controller.app.ProjectController;
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.model.ui.ProgressBarModel;
    import com.alleni.author.util.thread.PseudoThread;
    import com.alleni.author.util.thread.PseudoThreadContext;
    import com.alleni.author.util.thread.PseudoThreadEvent;
    import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
    import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
    import com.alleni.taconite.event.ApplicationEvent;

    import flash.events.Event;
    import flash.utils.getTimer;

    public class ProgressBarController
    {
        private static var _instance:ProgressBarController;
        private var _estimatedConstructionFrames:int;
        private var _startTime:Number;
        private var _phaseName:String;
        private var _withIndicator:Boolean;
        private var _projectLoadFraction:Number;
        private var _assetLoadFraction:Number;
        private var _objectProgressBracket:Number;
        private var _objectStartingFrame:int = 0;
        private var _buildingBackstage:Boolean;
        private var _estimatedBackstageFrames:int;
        private var _backstageBuildFraction:Number;
        private var _initialProjectLoading:Boolean;

        public static function get instance():ProgressBarController
        {
            if (_instance == null) {
                _instance = new ProgressBarController();
            }
            return _instance;
        }

        public function ProgressBarController()
        {
            if (_instance != null) {
                throw new Error("ProgressBarController");
            }
        }

        public function startProjectLoadProgress():void
        {
            _startTime = getTimer();
            _initialProjectLoading = true;
            _estimatedConstructionFrames = 1;
            _estimatedBackstageFrames = 1;
            _projectLoadFraction = 0;
            _assetLoadFraction = 0;
            _backstageBuildFraction = 0;
            _objectProgressBracket = 0;
            model.progress = 0;
            model.visible = true;
        }

        public function startBackstageBuildProgress(constructionFrames:int):void
        {
            _estimatedBackstageFrames = constructionFrames;
            _projectLoadFraction = 1;
            _buildingBackstage = true;
            model.visible = true;
            PseudoThread.instance.addEventListener(PseudoThreadEvent.THREAD_PROGRESS, handleThreadProgress);
        }

        public function startLazyLoadProgress(constructionFrames:int):void
        {
            // this is called during initial load, and when changing pages thereafter
            _estimatedConstructionFrames = constructionFrames;
            _projectLoadFraction = 1;
            _backstageBuildFraction = 1;
            _assetLoadFraction = 0;
            _objectProgressBracket = 0;
            _buildingBackstage = false;

            if (!model.visible) {    // doing a page-change after the initial load
                model.progress = 0;
                model.visible = true;
            }
            if (PseudoThread.instance.context && PseudoThread.instance.context.executeCounter >= 0) {
                _objectStartingFrame = PseudoThread.instance.context.executeCounter;
            } else {
                _objectStartingFrame = 0;
            }
            PseudoThread.instance.addEventListener(PseudoThreadEvent.THREAD_PROGRESS, handleThreadProgress);
        }

        public function set projectLoadFraction(value:Number):void
        {
            _projectLoadFraction = value;
            updateProgress();
        }

        public function set assetLoadFraction(value:Number):void
        {
            _assetLoadFraction = value;
            updateProgress();
        }

        public function set objectProgressBracket(value:Number):void
        {
            _objectProgressBracket = value;
            updateProgress();
        }

        private function handleThreadProgress(e:Event):void
        {
            updateProgress();
        }

        private function updateProgress():void
        {
            var objectProgress:Number = 0;
            var context = PseudoThread.instance.context;
            if (context && context.executeCounter >= 0) {
                if (_buildingBackstage) {
                    _backstageBuildFraction = Math.min(context.executeCounter / _estimatedBackstageFrames, 1);
                } else {
                    objectProgress = Math.min((context.executeCounter - _objectStartingFrame) / _estimatedConstructionFrames, 1);
                }
            }
            objectProgress = Math.max(objectProgress, _objectProgressBracket);
            if (model.visible) {
                updateStatusMessage();
                if (_initialProjectLoading) {
                    model.progress = _projectLoadFraction * 0.2 + _backstageBuildFraction * 0.1 + objectProgress * 0.3 + _assetLoadFraction * 0.4;
                } else {
                    model.progress = objectProgress * 0.6 + _assetLoadFraction * 0.4;
                }
//                trace("progress="+model.progress, "projLoad="+_projectLoadFraction, "backstage="+_backstageBuildFraction, "objects="+objectProgress, "bracket="+_objectProgressBracket, "assets="+_assetProgressFraction,
//                        "buildingBackstage="+_buildingBackstage);
            }
        }

        public function stop():void
        {
            ProgressBarController.instance.objectProgressBracket = 1;
            model.visible = false;
            updateStatusMessage();
            PseudoThread.instance.removeEventListener(PseudoThreadEvent.THREAD_PROGRESS, handleThreadProgress);
            _initialProjectLoading = false;
            _buildingBackstage = false;
        }

        public function enqueuePhaseWithName(name:String, withIndicator:Boolean=false):void
        {
            PseudoThread.add(new PseudoThreadSimpleRunnable("phaseName="+name,
                function():void {
                    setPhaseName(name, withIndicator);
                }
            ));
            PseudoThread.add(new PseudoThreadWaitFrameRunnable()); // it takes 2 frames to update status text
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
        }

        public function setPhaseName(name:String, withIndicator:Boolean=false):void
        {
            _phaseName = name;
            _withIndicator = withIndicator;
            if (model.visible) {
                updateStatusMessage();
            }
        }

        private function updateStatusMessage():void
        {
            if (model.visible) {
                if (_phaseName != null) {
                    ApplicationController.instance.dispatchEvent(
                        new ApplicationEvent(
                            (_withIndicator ? NotificationNamesApplication.SET_USER_MESSAGE_WITH_INDICATOR : NotificationNamesApplication.SET_USER_MESSAGE),
                            _phaseName));
                }
            } else {
                if (!ProjectController.instance.doingProjectSave) {
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE));
                } // else nothing
            }
        }

        private function get model():ProgressBarModel
        {
            return Application.instance.progressBar;
        }
    }
}
