/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller
{
    import caurina.transitions.Tweener;
    
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.controller.ui.GroupSelectionController;
    import com.alleni.author.controller.ui.palettes.InspectorController;
    import com.alleni.author.definition.application.NotificationNamesEditor;
    import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
    import com.alleni.author.model.objects.Arena;
    import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
    import com.alleni.author.view.WorldContainer;
    import com.alleni.author.view.WorldView;
    import com.alleni.author.view.objects.ArenaView;
    import com.alleni.author.view.objects.LogicObjectView;
    import com.alleni.taconite.controller.DragMediator;
    import com.alleni.taconite.event.ApplicationEvent;
    import com.alleni.taconite.model.TaconiteModel;
    import com.alleni.taconite.view.ViewContext;
    
    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;
    
    import mx.collections.IList;

    /**
     * Mediator for the WorldView that draws a marquee rectangle that selects enclosed objects.
     */
    public class MarqueeSelectionMediator extends DragMediator
    {
        protected var _worldView:WorldView;
        protected var _marquee:Shape;
        protected var _worldDragRect:Rectangle;
		private var _worldStart:Rectangle;
        private var _objects:IList;
        
        public function MarqueeSelectionMediator(context:ViewContext, worldView:WorldView, objects:IList)
        {
            super(context);
            _worldView = worldView;
            _objects = objects;
        }
        
		override public function handleMouseDown(e:MouseEvent):void
		{
			if (_context.stage == null || Application.running)
				return;
			_dragStarted = false;
			
			_dragPoint = new Point(WorldContainer(_context).mouseX, WorldContainer(_context).mouseY);
			_context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_context.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			if (_stopPropagation)
				e.stopPropagation();
		}
		
        /**
         * At the start of a drag, capture the sizes of all selected objects so that we
         * can resize them all by the same delta later on.
         */
        override protected function handleDragStart(e:MouseEvent):void
        {
            _marquee = new Shape();
			WorldContainer(_context).feedbackHandlesView.addChild(_marquee);
            
            _worldStart = dragEndpointRect;
        }
        
        /**
         * For each move during the drag, resize the models appropriately.
         */
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
        {
            _worldDragRect = _worldStart.union(dragEndpointRect);
            
			if (_marquee) {
            	_marquee.graphics.clear();
	            _marquee.graphics.beginFill(0xeeeeee,0.2);
	            _marquee.graphics.lineStyle(1,0x999999,0.5);
	            _marquee.graphics.drawRect(_worldDragRect.x, _worldDragRect.y, _worldDragRect.width, _worldDragRect.height);
			}
        }
		
		/**
		 * Handle mouse motion during the drag by initiating it if necessary when the mouse
		 * exceeds the distance threshold, and then calling the move-handling function. 
		 */
		override protected function handleMouseMove(e:MouseEvent):void
		{
			if (!_dragStarted
				&& e.buttonDown
				&& _dragPoint != null
				&& (Math.abs(WorldContainer(_context).mouseX -_dragPoint.x) >= minimumDrag
				|| Math.abs(WorldContainer(_context).mouseY -_dragPoint.y) >= minimumDrag))
				{
				_dragStarted = true;
				handleDragStart(e);
			}
			
			if (_dragStarted)
			{
				handleDragMove(e);
			}
			if(_stopPropagation) {
				e.stopPropagation();
			}
		}
		
		override protected function handleClick(e:MouseEvent) : void
		{
			if (!Application.runningLocked)
                InspectorController.instance.listeningToSelection = true;
            super.handleClick(e);
			if (!e.ctrlKey)
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.DESELECT));
		}
		
        override protected function handleDragEnd(e:MouseEvent):void
        {
			if (_marquee)
				Tweener.addTween(_marquee,{alpha:0, time:.25, transition:"easeOutCubic", onComplete:function():void{
					WorldContainer(_context).feedbackHandlesView.removeChild(_marquee);
					_marquee = null;
				}}); 
            
            if (!e.ctrlKey && !e.shiftKey)
                context.controller.clearSelection();
            
            var models:Vector.<TaconiteModel> = modelsIntersectingRect(_worldDragRect);
            context.controller.modifyMultiSelection(GroupSelectionController.growSelection(models));
			InspectorController.instance.listeningToSelection = true;
        }
		
		protected function modelsIntersectingRect(rect:Rectangle):Vector.<TaconiteModel>
		{
			var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
			addModelsIntersecting(_objects, models);
			return models;
		}
		
		protected function addModelsIntersecting(objects:IList, models:Vector.<TaconiteModel>):void
		{
			// be careful to ignore clipped-off portion of a large object inside an arena
			// (ArenaView.hittestObject() accounts for this)
			for each (var ao:AbstractObject in objects)
			{
				trace("addModelsIntersecting ok="+ao.okToSelect, ao);
				if (ao.okToSelect) {
					var testView:ObjectView = ao.getView(_worldView.context);
					if(testView is LogicObjectView){
						if (ao.okToSelect) {
							var view:ObjectView = ao.getView();
							var bounds:Rectangle = view.getBounds(_worldView);
							if (bounds.intersects(_worldDragRect)) {
								models.push(ao.model);
							}
						}
 					}else{
						if (testView != null && testView.hitTestObject(_marquee)) {
							if (!(ao as ArenaPage)) {
								models.push(ao.model);
							}
						}
					}
				}
				if (ao is Arena) {  // even if arena is locked, still allow selecting children
					var arenaView:ArenaView = ao.getView() as ArenaView;
					if (arenaView==null || arenaView.hitTestObject(_marquee)) {  // avoid selecting clipped-off parts of arena children
						addModelsIntersecting((ao as Arena).currentArenaPage.objects, models);
					}
				}else if(ao is ArenaPage){
                    var arenaView:ArenaView = ao.getView() as ArenaView;
					if (arenaView==null || arenaView.hitTestObject(_marquee)) {  // avoid selecting clipped-off parts of arena children
						addModelsIntersecting((ao as ArenaPage).objects, models);
					}
                }
			}
		}

        private function get dragEndpointRect():Rectangle
        {
            return new Rectangle(_worldView.mouseX, _worldView.mouseY, 1, 1)
        }
   }
}
