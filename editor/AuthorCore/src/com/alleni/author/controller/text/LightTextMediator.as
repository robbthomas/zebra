package com.alleni.author.controller.text
{
	import com.alleni.author.application.ui.EditorUI;
    import com.alleni.author.controller.objects.IEditingMediator;
    import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.palettes.InspectorController;
	import com.alleni.author.definition.DoubleClickAction;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.definition.text.TextSelection;
    import com.alleni.author.model.ui.ForwardingWireAnchor;
    import com.alleni.author.view.text.ILightEditableText;
	import com.alleni.author.view.text.ILightText;
	import com.alleni.author.view.text.IRichEditor;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.LightTextEditor;
	import com.alleni.author.view.text.RichTextField;
    import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.ITaconiteObject;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.engine.ElementFormat;
	import flash.text.engine.FontDescription;
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	import flash.utils.getTimer;
	
	import flashx.textLayout.container.ScrollPolicy;
	import flashx.textLayout.events.SelectionEvent;
	import flashx.textLayout.events.UpdateCompleteEvent;
	import flashx.textLayout.formats.TextLayoutFormat;

	
	public class LightTextMediator extends EventDispatcher
	{
		private var _lightText:ILightEditableText;
		
		private var _beginIndex:int = -1;
		private var _endIndex:int = -1;
		public var _richTextEditor:IRichEditor;
		private var _lightTextEditor:LightTextEditor;
		private var _callBacks:Object;
		private var _callBack_closeFunc:Function;
		private var _propertyType:String;
		
		private const BACKGROUND_COLOR:uint = 0xFFFFFF;
		private const FOREGROUND_COLOR:uint = 0x000000;
		private const LINECOLOR:uint 		= 0x000000;
		private const MARGIN:Number = 3;
		private const MIN_WIDTH:Number = 60;
		private const MIN_STRING_WIDTH:Number = 200;
		private const MIN_NUMBER_WIDTH:Number = 100;
		private const MAX_WIDTH:Number = 300;
		
		private var _textSelection:TextSelection;
		private var _selectedTextElements:Vector.<ITaconiteObject>;
		
		private var _defaultClickOutside:Boolean = false;
		private var _allowInspector:Boolean = false;
		private var _title:String = "Popup Selection";
		private var _lastTitle:String = "";
		private var _flags:uint = 0;
		private var _maxWidth:Number = MAX_WIDTH;
		private var _minWidth:Number = MIN_WIDTH;
		private const invokedAsEditor:Boolean = TaconiteFactory.getEnvironmentImplementation().invokedAsEditor;
		
		private static const INITIAL_POSITION_SET:uint = 1 << 1;
		
		public var maxChars:int = 0;
		
		public function LightTextMediator(lightText:ILightEditableText)
		{
			_lightText = lightText;
			
			var sprite:Sprite = _lightText as Sprite;
			if (sprite.stage) initialize();
			else sprite.addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(event:Event=null):void
		{
			var sprite:Sprite = _lightText as Sprite;
			if (!sprite) return;
			sprite.removeEventListener(Event.ADDED_TO_STAGE, initialize);
			addListeners();
		}
		
		public function addListeners():void
		{
			var sprite:Sprite = _lightText as Sprite;
			if (!sprite) return;
			sprite.addEventListener(MouseEvent.CLICK, handleMouseDownOrDouble);
			sprite.addEventListener(MouseEvent.DOUBLE_CLICK, handleMouseDownOrDouble);
		}
		
		public function removeListeners():void
		{
			var sprite:Sprite = _lightText as Sprite;
			if (!sprite) return;
			sprite.removeEventListener(MouseEvent.CLICK, handleMouseDownOrDouble);
			sprite.removeEventListener(MouseEvent.DOUBLE_CLICK, handleMouseDownOrDouble);
		}
		
		
		private static const DBL_TIME:uint = 300;  // mSec for double click
		private var _lastClickTime:uint = 0;
		
		private function handleMouseDownOrDouble(event:MouseEvent):void
		{
			if (!_callBacks)
				return;
			
			var okEditFunc:Function = _callBacks.okEditFunc;
			
			if (okEditFunc == null) {
				openEditing();
				event.stopPropagation();
			} else {
				var action:int = okEditFunc(event);
				doAction(action, event);
		
				if (!_lightText.edit) {
					var time:uint = getTimer();
					if ((time - _lastClickTime) < DBL_TIME) {
						event = new MouseEvent(MouseEvent.DOUBLE_CLICK);
						action = okEditFunc(event);
						doAction(action, event);
					}
				}
				_lastClickTime = time;
			}
		}
		
		private function doAction(action:int, event:MouseEvent):void
		{
			switch (action) {
				case DoubleClickAction.EDIT:
					openEditing();
					event.stopPropagation();
					break;
				case DoubleClickAction.STOP:
					event.stopPropagation();
					break;
				default:
					if (_lightText.edit)
						event.stopPropagation();
			}
		}


        private function concurrentEditing(m:IEditingMediator):Boolean {
            var textMediator:TextEditMediator = m as TextEditMediator;
            var lightEditableLable:LightEditableLabel = _lightText as LightEditableLabel;
            if (textMediator == null || lightEditableLable == null || lightEditableLable.associatedRibbonView == null) {
                return false;
            } else {
                if (lightEditableLable.associatedRibbonView.ribbon is ForwardingWireAnchor &&
                        (lightEditableLable.associatedRibbonView.ribbon as ForwardingWireAnchor).selectedObjects[0] is TextSelection) {
                    return true
                }
                return false;
            }
        }

        public function openEditing():void
		{

			if(!_lightText.editable || _lightText.edit)
				return;
            var concurrent:Boolean = false;
            while(ApplicationController.instance.authorController.editing && ApplicationController.instance.authorController.currentEditingMediator is TextEditMediator) {
                concurrent = concurrentEditing(ApplicationController.instance.authorController.currentEditingMediator);
                if (concurrent) break;

                // make sure other editors are closed out
                // specifically one we may be stealing the richedit control from so it is cleaned up
                // before we start and then finally get around to the mediator.openEditing(); at the bottom
                ApplicationController.instance.authorController.currentEditingMediator.makeObject();
            }
			var sprite:Sprite = _lightText as Sprite;
			if (!sprite) return;
			
			var mediator:TextEditMediator;
			_minWidth = _lightText.expandedWidth > -1 ? _lightText.expandedWidth: MIN_WIDTH;
			
			clearFlag(INITIAL_POSITION_SET);
			
			//a secondary editor allows an expanded field to be used to change the property of another expanded field -- e.g. change font size
			
			if(EditorUI.instance.presentationContainer.contains(sprite)){
				_richTextEditor = ApplicationController.instance.authorController.getRichTextEditor();
				mediator = ApplicationController.instance.authorController.getTextEditMediator();
				_lightTextEditor = ApplicationController.instance.authorController.getLightTextEditor();
			}
			
			else{
				_richTextEditor = ApplicationController.instance.authorController.getSecondaryRichTextEditor();
				mediator = ApplicationController.instance.authorController.getSecondaryTextEditMediator();
				_lightTextEditor = ApplicationController.instance.authorController.getSecondaryLightTextEditor();
			}
			
			mediator.maxChars = this.maxChars;
			
			var cb:Object = getCallBacks();
			var okEditFunc:Function =  cb.okEditFunc;
			var updateFunc:Function = cb.updateFunc;
			var keyPressFunc:Function = cb.keyPressFunc;
			var closeFunc:Function;
			var hitTestFunc:Function; 
			
			//the following code should eventually refer to the editing layer on the view context.  Need some rework of classes before this can happen
			
			/*
			'Expandable' controls whether or not the expanded field is displayed. In some cases, such as the answerjudging table (numerical order), the expanded field is not needed.
			 The DefaultClickOutside is set to true only Formattable text, which enables the inspector to control font properties.
			If a table row is not wired to formattable text, the interaction with the inspector will not be available.
			*/
			
			if(_lightText.expandable){
				if(EditorUI.instance.presentationContainer.contains(sprite)){
					_lightTextEditor.editingView = EditorUI.instance.presentationContainer.editingView;
					richEditorImplementation(true);
					 closeFunc= this.closeFunction;
					_callBack_closeFunc = cb.closeFunc;
				} else if(EditorUI.instance.rightControlBar.contains(sprite)) {  
					_lightTextEditor.editingView = EditorUI.instance.rightControlBar.editingView;
					richEditorImplementation();
					closeFunc= this.closeFunction;
					_callBack_closeFunc = cb.closeFunc;
				} else{
					richTextFieldImplementation();
					closeFunc = cb.closeFunc;
					hitTestFunc = cb.hittestFunc;
				}
			} else{  //no expandable text
				richTextFieldImplementation();
				closeFunc = cb.closeFunc;
				hitTestFunc = cb.hittestFunc;
			}
			
			mediator.handleTextEditEvents(_richTextEditor as RichTextField, okEditFunc, updateFunc, keyPressFunc, closeFunc,hitTestFunc,_defaultClickOutside, _allowInspector);
			_lightText.edit = true;
			mediator.openEditing(concurrent);
		}
		
		private function removeSelectedTextElements():void
		{
			if(!invokedAsEditor)
				return;
			
			_selectedTextElements = Vector.<ITaconiteObject>([]);
			InspectorController.instance.selectedTextElements = _selectedTextElements;
		}
		

		public function closeEditing():void
		{
			removeSelectedTextElements();

			if(_lightText.edit) {
				var mediator:TextEditMediator = ApplicationController.instance.authorController.getTextEditMediator();
				mediator.closeEditing(true);
			}

			if (_lightTextEditor)
				_lightTextEditor.visible = false;
		}
		
		private function richEditorImplementation(listenToSelection:Boolean = false):void
		{
			_lightTextEditor.setEditor(_richTextEditor);
			_lightTextEditor.setBackgroundColor(BACKGROUND_COLOR);
			_lightTextEditor.setLineColor(LINECOLOR);
			
			if(_lightText.text != _lightText.prompt){
				_richTextEditor.rawText = _lightText.text;  //must use raw text here
			}
			else{
				_richTextEditor.rawText  = "";
			}
			
			var _tempFormat:ElementFormat = createTextFormat(_lightText,FOREGROUND_COLOR,2, true);
			var dimensions:Point = computeSize();
			
			with (_richTextEditor){
				textWidth = dimensions.x - MARGIN;
				textHeight = _lightText.textHeight + MARGIN;
				textFormat = _tempFormat;
				setPaddingParams({left:MARGIN, top:3, right:MARGIN, bottom:0}); //top must be 3
				visible = true;
			}
			
			with (_lightTextEditor){
				setReferenceObject(Sprite(_lightText));
				reposition();
			}
			
			var _propertyType:String = getPropertyType();
			
			switch(_propertyType){
				case Modifiers.FORMATTED_TEXT_TYPE:
					_richTextEditor.explicitLineBreak = true;
					_richTextEditor.captureEnter = false;
					_richTextEditor.importFormat(_lightText.formattedText);
					_richTextEditor.addUpdateListener(handleTextFlowUpdate);
					_richTextEditor.setCompositionSize(dimensions.x,dimensions.y);
					
					if(listenToSelection){
						_richTextEditor.addSelectionListener(selectionChanged);
					}
					_textSelection = new TextSelection(null,_richTextEditor as RichTextField);
					_selectedTextElements = Vector.<ITaconiteObject>([_textSelection]);
					_defaultClickOutside = true;
					_allowInspector = true;
					break;
				case Modifiers.STRING_TYPE:
					_richTextEditor.explicitLineBreak = false;
					_richTextEditor.captureEnter = true;
					_richTextEditor.addUpdateListener(handleTextFlowUpdate);
					_richTextEditor.setCompositionSize(dimensions.x, dimensions.y);
					_defaultClickOutside = true;
					_allowInspector = false;
					break;
				case Modifiers.FORM_TEXT_BOX:
					_richTextEditor.explicitLineBreak = true;
					_richTextEditor.captureEnter = false;
					_richTextEditor.setCompositionSize(dimensions.x, dimensions.y);
					_allowInspector = false;
					_defaultClickOutside = false;
					break;
				default:
					_richTextEditor.explicitLineBreak = false;
					_richTextEditor.captureEnter = true;
					_richTextEditor.addUpdateListener(handleTextFlowUpdate);
					_richTextEditor.setCompositionSize(dimensions.x, dimensions.y);
					_defaultClickOutside = true;
					_allowInspector = false;
					break;
			}
			
			_lightTextEditor.visible = true;
			_lightTextEditor.refreshSelection();
		}

		private function richTextFieldImplementation():void
		{
			_lightText.parent.addChild(_richTextEditor as Sprite);
			var _tempFormat:ElementFormat = createTextFormat(_lightText, _lightText.color);
			var dimensions:Point = computeSize();
			
			with (_richTextEditor){
				x = _lightText.x;
				y = _lightText.y;
//				trace("rich text field x, y; lighttext x, y", x,y,_lightText.x,_lightText.y);
//				trace("rtf parent", parent);
				textWidth = _lightText.textWidth;
				textHeight = _lightText.textHeight;
				textFormat = _tempFormat;
				textAlign = _lightText.textAlign;
				lineHeight = _lightText.leading * 0.95;
				setPaddingParams(_lightText.getPaddingParams()); 
				rawText = _lightText.text;
				explicitLineBreak = _lightText.explicitLineBreak;
				visible = true;
			}
			
			var _propertyType:String = getPropertyType();
			
			switch(_propertyType){
				case Modifiers.FORM_TEXT_BOX:
					_richTextEditor.explicitLineBreak = false;
					_richTextEditor.captureEnter = false;
					trace("setting comp size: " + dimensions.x, dimensions.y);
					_richTextEditor.setCompositionSize(dimensions.x, dimensions.y);
					_richTextEditor.verticalScrollPolicy = ScrollPolicy.OFF;
					break;
			}
		}
		
		private function onMouseUp(event:MouseEvent):void
		{
			removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		public function setEditorVisible(value:Boolean):void
		{
			if(_richTextEditor)
				_richTextEditor.visible = value;
		}
		
		public function setCallBacks(callBacks:Object):void
		{
			_callBacks = callBacks;
		}
		
		public function getCallBacks():Object
		{
			return _callBacks;
		}
		
		public function closeFunction(value:String):void
		{
			if (getPropertyType() ==  Modifiers.FORMATTED_TEXT_TYPE)
				removeSelectedTextElements();
			
			_lastTitle = "";
			if(_richTextEditor)
				_richTextEditor.removeUpdateListener(handleTextFlowUpdate);
			_lightTextEditor.visible = false;
			
			var formattedText:FormattedText = new FormattedText(value);		
			_callBack_closeFunc(formattedText);
		}
		
		private function handleTextFlowUpdate(event:UpdateCompleteEvent):void
		{
			var dimensions:Point = computeSize();
			var textLength:Number = dimensions.x;
			var textHeight:Number = dimensions.y;
			
			var _propertyType:String = getPropertyType();
			
			switch(_propertyType){
				case Modifiers.FORM_TEXT_BOX:
					_lightTextEditor.setCompositionSize(textLength,20);
					_lightTextEditor.setSize(Math.max(_richTextEditor.containerWidth, _minWidth),_richTextEditor.containerHeight);
					break;
				case Modifiers.FORMATTED_TEXT_TYPE:
					_lightTextEditor.setCompositionSize(textLength,textHeight);
					_lightTextEditor.setSize(Math.max(_richTextEditor.containerWidth, _minWidth),_richTextEditor.containerHeight);
					break;
				case Modifiers.STRING_TYPE:
					_lightTextEditor.setCompositionSize(textLength,textHeight);
					_lightTextEditor.setSize(Math.max(_richTextEditor.containerWidth, _minWidth),_richTextEditor.containerHeight);
					break;
				default:
					_lightTextEditor.setCompositionSize(textLength,textHeight);
					_lightTextEditor.setSize(Math.max(_richTextEditor.containerWidth, _minWidth),_richTextEditor.containerHeight);
					break;
			}
			
			//only do this once to prevent the textfield from hopping around when editing
//			if (!testFlag(INITIAL_POSITION_SET) && !_lightTextEditor.outOfBounds){
				_lightTextEditor.reposition();
//				this.setFlag(INITIAL_POSITION_SET);
//			}
		}
		
		private function computeSize():Point
		{
			var dimensions:Point = new Point();
			dimensions.x = _lightText.textWidth;
			dimensions.y = _lightText.textHeight;
			var spaceNeeded:Number;
			
			var _propertyType:String = getPropertyType();
			
			switch(_propertyType){
				case Modifiers.FORMATTED_TEXT_TYPE:
					_richTextEditor.formattable = true;
					spaceNeeded = _richTextEditor.text.length * 7;
					dimensions.x =  Math.min(Math.max(spaceNeeded,MIN_STRING_WIDTH), _maxWidth);
					dimensions.y = NaN;
					break;
				case Modifiers.STRING_TYPE:
					spaceNeeded = _richTextEditor.text.length * 7;
					dimensions.x =  Math.min(Math.max(spaceNeeded,MIN_STRING_WIDTH), _maxWidth);
					dimensions.y = NaN;
					break;
				case Modifiers.FORM_TEXT_BOX:
					_richTextEditor.formattable = false;
					dimensions.x = _lightText.textWidth;
					dimensions.y = _lightText.textHeight;
					break;
				default:
					spaceNeeded = _richTextEditor.text.length * 7;
					dimensions.x =  Math.min(Math.max(spaceNeeded,MIN_STRING_WIDTH), _maxWidth);
					dimensions.y = NaN;
					break;
			}
			return dimensions;
		}
		
		private function createTextFormat(text:ILightText, formatColor:uint = FOREGROUND_COLOR, boostFontSizeAmount:Number = 0, forceBold:Boolean = false):ElementFormat
		{
			var _tempFormat:ElementFormat =  text.textFormat as ElementFormat;
			
			_tempFormat.color = formatColor;
			_tempFormat.fontSize = _tempFormat.fontSize + boostFontSizeAmount;
			
			//bold, italic and color are available to users through accessor methods
			var _tempFontDescription:FontDescription = new FontDescription();
			
			if(forceBold){
				_tempFontDescription.fontWeight = FontWeight.BOLD;
			}else{
				_tempFontDescription.fontWeight = text.bold?FontWeight.BOLD:FontWeight.NORMAL;
			}
			_tempFontDescription.fontPosture = text.italic?FontPosture.ITALIC:FontPosture.NORMAL;
			
			//fontLookup and fontName are hardcoded when LightTextBase gets initialized
			//if we want more control, then accessor methods need to  be added to the iLightText interface
			_tempFontDescription.fontLookup = _tempFormat.fontDescription.fontLookup;
			_tempFontDescription.fontName = _tempFormat.fontDescription.fontName;
			_tempFontDescription.cffHinting = _tempFormat.fontDescription.cffHinting;
			_tempFormat.fontDescription = _tempFontDescription;
			
			return _tempFormat;
		}
		
		private function getPropertyType():String
		{
			var lightEditableLabel:LightEditableLabel = _lightText as LightEditableLabel;
			if(lightEditableLabel)
				return lightEditableLabel.propertyType;
			return null;
		}
		
		private function selectionChanged(selectionEvent:SelectionEvent):void
		{
			var layoutFormat:TextLayoutFormat = _richTextEditor.selectionFormat as TextLayoutFormat;
			if (!layoutFormat) return;
		
			_textSelection.format = layoutFormat;
			
			if(_textSelection.format) {
				if (!invokedAsEditor) return;
			    InspectorController.instance.selectedTextElements = _selectedTextElements;
			
				if(_title != _lastTitle) {
					InspectorController.instance.setTitle(_title);
					_lastTitle = _title;
				}
			}
		}
		
		protected function testFlag(mask:uint):Boolean
		{
			return (_flags & mask) != 0;
		}
		
		protected function setFlag(mask:uint):void
		{
			_flags |= mask;
		}
		
		protected function clearFlag(mask:uint):void
		{
			_flags &= ~mask;
		}
	}
}