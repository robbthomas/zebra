package com.alleni.author.controller.text
{
	import com.alleni.author.controller.ui.ApplicationController;
	
	import flash.events.Event;
	
	import flashx.textLayout.edit.EditManager;
	import flashx.undo.IUndoManager;
	
	public class EditManager extends flashx.textLayout.edit.EditManager
	{
		public function EditManager(undoManager:IUndoManager=null)
		{
			super(undoManager);
		}
		
		override public function editHandler(event:Event):void
		{
			super.editHandler(event);
			
			switch (event.type) {
				case Event.CUT:
					ApplicationController.cut(event);
					break;
				case Event.COPY:
					ApplicationController.copy(event);
					break;
				default:
					break;
			}
		}
	}
}