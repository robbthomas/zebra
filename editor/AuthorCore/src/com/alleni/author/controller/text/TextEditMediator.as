/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller.text
{
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.app.MenuController;
import com.alleni.author.controller.objects.IClipboardMediator;
import com.alleni.author.controller.objects.IEditingMediator;
import com.alleni.author.controller.objects.IUndoMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.DoubleClickAction;
import com.alleni.author.definition.DragInfo;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.objects.TextObjectView;
import com.alleni.author.view.text.RichTextField;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.TextEditEvent;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.model.ITaconiteObject;

import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import flash.ui.Mouse;
import flash.ui.MouseCursor;
import flash.utils.getTimer;

import flashx.textLayout.elements.TextFlow;
import flashx.textLayout.events.FlowOperationEvent;
import flashx.textLayout.events.UpdateCompleteEvent;
import flashx.textLayout.operations.FlowOperation;
import flashx.textLayout.operations.InsertTextOperation;

public class TextEditMediator implements IEditingMediator, IUndoMediator, IClipboardMediator
	{
		private const _MENULABEL_TYPE:String = "Text";

		// return values from keyPressed callback function
		public static const REQ_NULL:int = 0;
		public static const REQ_IGNORE:int = 1;	// ignore this keypress
		public static const REQ_CLOSE:int = 2;	// close the text object without inserting key

		private const invokedAsEditor:Boolean = TaconiteFactory.getEnvironmentImplementation().invokedAsEditor;
		private var _text:RichTextField;

		private var _okEditFunc:Function;
		private var _updateFunc:Function;
		private var _keyPressFunc:Function;
		private var _closeFunc:Function;
		private var _hittestFunc:Function;
		private var _defaultClickOutside:Boolean;
		private var _allowInspector:Boolean;
        private var _editWhileRunning:Boolean;
		
		public var maxChars:int = 0;
        
		public function TextEditMediator()
        {
        }	
		
		/**
		 * Mediator to handle text edit events: 
		 * @param text
		 * Callbacks:
		 * @param okEditFunc  function(event:Event):int  return IGNORE, EDIT, STOP  from DoubleClickAction
		 * @param updateFunc  function(value:String):void
		 * @param keyPressFunc  function(charCode:uint):int   return REQ_NULL, REQ_IGNORE or REQ_CLOSE.
		 * @param closeFunc  function(value:String):void
		 * @param hittestFunc function(stageX,stageY):Boolean   true if click in within bounds of text area.
		 * 
		 */
		public function handleTextEditEvents(text:RichTextField, okEditFunc:Function=null, updateFunc:Function=null, keyPressFunc:Function=null, 
											 closeFunc:Function=null, hittestFunc:Function=null, defaultClickOutside:Boolean = true, allowInspector:Boolean = false):TextEditMediator
		{
			_text = text;
			_okEditFunc = okEditFunc;
			_updateFunc = updateFunc;
			_keyPressFunc = keyPressFunc;
			_closeFunc = closeFunc;
			_hittestFunc = hittestFunc;
			_defaultClickOutside = defaultClickOutside;
			_allowInspector = allowInspector;
			
			_text.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDownOrDouble);
//			_text.addEventListener(MouseEvent.DOUBLE_CLICK, handleMouseDownOrDouble);  // was not firing
			_text.textFlow.addEventListener(UpdateCompleteEvent.UPDATE_COMPLETE, handleUpdateEvent);
			_text.textFlow.addEventListener(FlowOperationEvent.FLOW_OPERATION_BEGIN, textContainerManager_flowOperationBeginHandler);
			
			return this;
		}
		
		/**
		 *  @private
		 *  Called when the TextFlow dispatches an 'operationBegin'
		 *  event before an editing operation.
		 */     
		private function textContainerManager_flowOperationBeginHandler(event:FlowOperationEvent):void
		{
			var op:FlowOperation = event.operation;
			
			// The text flow's generation will be incremented if the text flow
			// is modified in any way by this operation.
			
			if (op as InsertTextOperation) {
				var insertTextOperation:InsertTextOperation =
					InsertTextOperation(op);
				var textFlow:TextFlow = event.target as TextFlow;
				
				var textToInsert:String = insertTextOperation.text;

				if (maxChars != 0) {
					var currentText:String = textFlow.getText(0);
					var length1:int = currentText.length;
					var length2:int = textToInsert.length;
					if (length1 + length2 > maxChars)
						textToInsert = "";
				}
				
				insertTextOperation.text = textToInsert;
			}
		}
		
		private static const DBL_TIME:uint = 300;  // mSec for double click
		private var _lastClickTime:uint = 0;
		
		private function handleMouseDownOrDouble(event:MouseEvent):void
		{
			if (_okEditFunc == null) {
				openEditing();
				event.stopPropagation();
			} else {
				var action:int = _okEditFunc(event);
				doAction(action, event);
				
				if (!_text.editing) {
					// in addition to the MOUSE_DOWN, also do DOUBLE_CLICK when appropriate
					// ... since a bug is preventing double-click getting here
					var time:uint = getTimer();
					if ((time - _lastClickTime) < DBL_TIME) {
						event = new MouseEvent(MouseEvent.DOUBLE_CLICK);
						action = _okEditFunc(event);
						doAction(action, event);
					}
				}
				_lastClickTime = time;
			}
		}
		
		private function doAction(action:int, event:MouseEvent):void
		{
			switch (action) {
				case DoubleClickAction.EDIT:
					openEditing();
					event.stopPropagation();
					break;
				case DoubleClickAction.STOP:
					event.stopPropagation();
					break;
				default:
					if (_text.editing)
						event.stopPropagation();
			}
		}
		
				
		private function handleKeyboard(event:KeyboardEvent):Boolean
		{
			var charCode:uint = event.charCode;
			var keyRequest:int = REQ_NULL;
			if (_keyPressFunc != null)
				keyRequest = _keyPressFunc(charCode);  // allow client code to filter keys
			
            if (keyRequest == REQ_CLOSE)
				ApplicationController.instance.resetKeyboardFocus(true);

            event.stopPropagation();
			return (keyRequest != REQ_NULL);  // true to ignore the key
		}
		
		private function handleCopy(event:ApplicationEvent = null):void
		{
			ApplicationController.instance.authorController.dispatchEvent(new TextEditEvent(TextEditEvent.COPY, null));
		}
		
		private function handlePaste(event:ApplicationEvent = null):void
		{
			ApplicationController.instance.authorController.dispatchEvent(new TextEditEvent(TextEditEvent.PASTE, null));
		}
		
		private function handleCut(event:ApplicationEvent = null):void
		{
			ApplicationController.instance.authorController.dispatchEvent(new TextEditEvent(TextEditEvent.CUT, null));
		}
		
		private function handleSelectAll(event:ApplicationEvent = null):void
		{
			ApplicationController.instance.authorController.dispatchEvent(new TextEditEvent(TextEditEvent.SELECT_ALL, null));
		}
		
		private function handleUndo(event:ApplicationEvent = null):void
		{
			ApplicationController.instance.authorController.dispatchEvent(new TextEditEvent(TextEditEvent.UNDO, null));
		}
		
		private function handleRedo(event:ApplicationEvent = null):void
		{
			ApplicationController.instance.authorController.dispatchEvent(new TextEditEvent(TextEditEvent.REDO, null));
		}
		
		public function openEditing(concurrent:Boolean = false):void
		{
			if(!_text.editable || _text.editing)
				return;
            if(Application.running && (_text.parent is TextObjectView)){
                ApplicationController.instance.authorController.editWhileRunningStart();
                _editWhileRunning = true;
            }

			// strange order:  you would think we should requestKeyboardFocus first
			// ... but for some reason that causes the backspace key to fail on the rich text.
			ApplicationController.instance.authorController.startingEditing(this,concurrent);
			
			_text.editing = true;
			_text.keyHandler = handleKeyboard;

            //TODO this line wouldn't ever work because of starting editing line above
			ApplicationController.instance.requestKeyboardFocus(this, true);
			_text.setFocus();
		//	_text.textFlow.lineHeight = "95%"; // affect leading to make this easier to work with when selection appears
			
			/*for ribbon fields, close editing should be called when user clicks off an inspector field onto some other part of the inspector (e.g. slider)
			The LightTextMediator sets this flag to false so that the default listener is the stage and not just the viewContext.
			
			When defaultClickOutside is set to true, then view, rather than the stage, listens for the click
			*/
			
			if(_defaultClickOutside && _allowInspector){
				Application.instance.viewContext.addEventListener(MouseEvent.MOUSE_DOWN, listenForClickOutside, true);
			} else if(_defaultClickOutside){
				Application.instance.viewContext.addEventListener(MouseEvent.MOUSE_DOWN, listenForClickOutside, true);
				
				if (invokedAsEditor)
					EditorUI.instance.rightControlBar.addEventListener(MouseEvent.MOUSE_DOWN, listenForClickOutside, true);
			} else{
				Application.instance.viewContext.stage.addEventListener(MouseEvent.MOUSE_DOWN, listenForClickOutside, true);
			}
		}
        
        public function closeEditing(stealFocus:Boolean):Boolean
        {
            if (_editWhileRunning) {
                ApplicationController.instance.authorController.editWhileRunningEnd();
                _editWhileRunning = false;
            }

            if(_text == null)
				return true;

        	if (stealFocus) {
				if (_closeFunc != null)
					//_closeFunc(_text.extractTextFromTextFlow());  04-5-11
					_closeFunc(_text.exportMarkup());
        		_text.keyHandler = null;
        		_text.editing = false;
				_text.dispatchEvent(new TextEditEvent(TextEditEvent.EDIT_COMPLETE, _text.text));
				
				ApplicationController.instance.authorController.closingEditing(this, true);
				Mouse.cursor = MouseCursor.ARROW;
				
				// ideally, this needs to be whatever is the view context of the current text object.
				
				/*
				if defaultclickoutside was set to true, then listen only on the viewcontext, not the entire stage.  This allows clicking on the menu.
				if defaultclickoutside is set to false, then any click outside of the text will close the text
				the listenForClickOutside function may execute a hittest function that returns a true and prevents text from closing.
				*/
				
				if(_defaultClickOutside && _allowInspector){
					Application.instance.viewContext.removeEventListener(MouseEvent.MOUSE_DOWN, listenForClickOutside, true);
				} else if(_defaultClickOutside){
					Application.instance.viewContext.removeEventListener(MouseEvent.MOUSE_DOWN, listenForClickOutside, true);
					
					if (invokedAsEditor)
						EditorUI.instance.rightControlBar.removeEventListener(MouseEvent.MOUSE_DOWN, listenForClickOutside, true);
				} else{
					Application.instance.viewContext.stage.removeEventListener(MouseEvent.MOUSE_DOWN, listenForClickOutside, true);
				}
	        }
            MenuController.instance.syncTopMenuBarItems();
			return !_text.editing;
        }
		
		
		/**
		 * Pass this function as the "keyPress" callback, to make the Enter key close editing. 
		 * @param charCode
		 * @return 
		 * 
		 */
		public static function closeOnEnter(charCode:uint):int
		{
			if (charCode == Keyboard.ENTER)
				return TextEditMediator.REQ_CLOSE;
			else
				return TextEditMediator.REQ_NULL;
		}
		
		/**
		 * Pass this function as the "okToEdit" callback, to require enter key for editing. 
		 * @param doubleClick
		 * @return 
		 * 
		 */
		public static function doubleClickToEdit(event:MouseEvent):int
		{
			if (event.type == MouseEvent.DOUBLE_CLICK)
				return DoubleClickAction.EDIT;
			else
				return DoubleClickAction.IGNORE;
		}

		public static function singleClickToEdit(event:MouseEvent):int
		{
			if (event.type == MouseEvent.CLICK)
				return DoubleClickAction.EDIT;
			else
				return DoubleClickAction.IGNORE;
		}
		

		private function handleUpdateEvent(evt:UpdateCompleteEvent):void
		{
			if (_text.editing && _updateFunc != null) {
				var value:String = _text.extractTextFromTextFlow();
				_updateFunc(_text, value);
			}
		}
		
		/*
		 if hittestfunc is not null, execute it; if it returns true, then return
		if there is no hittest func defined, are we clicking on text.  If yes, then return.
		*/
		
		private function listenForClickOutside(event:MouseEvent):void
		{
            var clickedOutsideOfText:Boolean = _text.parent && event.target as DisplayObject && !_text.parent.contains(event.target as DisplayObject);
			if (_text.editing && clickedOutsideOfText) {
				if (_hittestFunc != null) {
					if (_hittestFunc(event.stageX, event.stageY))
						return;  // click is within this text area ... ignore it
				} else {
					if (_text.hitTestPoint(event.stageX, event.stageY))
						return;
				}
				ApplicationController.instance.resetKeyboardFocus(true);
			//	event.stopPropagation();  
			}
		}
		
		public function get hasActiveState():Boolean
		{
			return false;  // otherwise the gadget velum will hide when start editing text
		}
		
		public function activeMediatorChanged(mediator:IEditingMediator):void
		{
		}

        public function getSelection(getGlobal:Boolean, strict:Boolean=true, omitLocked:Boolean=true):Vector.<DragInfo>
		{
            return new Vector.<DragInfo>();
        }

        public function cancelEditing():Boolean
		{
            //TODO handle close editing without persisting value
			return false;
		}

		public function deleteEditing():Boolean
		{
			return false;
		}

		public function makeObject():Boolean
		{
			closeEditing(true);
			return true;
		}

		public function switchedObjects(oldObj:ITaconiteObject, newObj:ITaconiteObject):void
		{
		}

		public function get editingObject():ITaconiteObject
		{
			return null;
		}

        public function get editing():Boolean {
            return _text.editing;
        }

        public function get actionTree():ActionTree
		{
			return null;
		}

        public function get MENULABEL_TYPE():String{
            return _MENULABEL_TYPE;
        }

		public function canUndo():Boolean
		{
			return true;//_text.canUndo(); since we cant listen for changes always have the menu enabled
		}

		public function canRedo():Boolean
		{
			return true;//_text.canRedo(); since we cant listen for changes always have the menu enabled
		}

		public function undo():void
		{
			_text.handleUndo();
		}

		public function redo():void
		{
			_text.handleRedo();
		}

		public function undoName():String
		{
			return "Text Editing";
		}

		public function redoName():String
		{
			return "Text Editing";
		}

		public function selectAll(includeLocked:Boolean):void
		{
			_text.handleSelectAll();
		}

		public function cut(e:Event):void
		{
			_text.handleCut(e);
		}

		public function copy(e:Event):void
		{
			_text.handleCopy(e);
		}

		public function paste(e:Event):void
		{
			_text.handlePaste(e);
		}
	}
}
