package com.alleni.author.controller.text
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.service.RestPipeline;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.ProgressEvent;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import flash.system.LoaderContext;
	import flash.text.*;
	import flash.utils.ByteArray;

	public class FontLoader extends Sprite
	{
		private static const PATH:String = "/system/font";
		
		private static var _instance:FontLoader;
		public var queueProgressFraction:Number = 0;
		private var _requestedQueue:Vector.<Asset>;
		
		
		public function FontLoader()
		{
			Utilities.assert(!_instance);
			_requestedQueue = new Vector.<Asset>();
		}
		
		static public function get instance():FontLoader
		{
			if (!_instance)
				_instance = new FontLoader();
			return _instance;
		}
		
		/**
		 * should only be called by FontController::requestByName, nowhere else
		 * @param font
		 * @param fontName
		 * 
		 */
		public function _loadFontByData(fontFile:String, fontName:String):void
		{
			var asset:Asset;
			// is the font already in the loading queue
			for each(asset in _requestedQueue)
				if (asset.name == fontName)
					return;

			asset = new Asset(fontName);
			asset.name = fontName;
			asset.type = AssetType.FONT;

			if (_requestedQueue.length == 0)
				queueProgressFraction = 0;
			_requestedQueue.push(asset);
			var operation:RestHttpOperation = new RestHttpOperation(PATH + "/" + fontFile);
			operation.displayName = "Loading font " + asset.name;

            operation.cacheContent = true;
			operation.addEventListener(ProgressEvent.PROGRESS, handleProgress);
			asset.loadOperation = operation;
			RestPipeline.push(operation, handleFontDataLoaded);


//            var request:URLRequest = new URLRequest(TaconiteFactory.getEnvironmentImplementation().apiURI + PATH + "/" + fontFile+"?method=GET");
//            trace(request.url);
//
//            var urlLoader:URLLoader = new URLLoader();
//			urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
//            urlLoader.load(request);
//            var testFontLoader:Loader = new Loader();
//            testFontLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, fontComplete);
//            testFontLoader.load(request);


		}

        private function fontComplete(event:Event):void{
            trace("Is font cache");
        }

		private function handleProgress(e:ProgressEvent):void
		{
			var asset:Asset;
			queueProgressFraction = 0;
			for each (asset in _requestedQueue)
				if (asset.loadOperation)
					queueProgressFraction += asset.loadOperation.progressFraction;
				else
					queueProgressFraction += 1;
			queueProgressFraction /= _requestedQueue.length;
		}
		
		private function handleFontDataLoaded(event:Event):void
		{
			event.target.removeEventListener(ProgressEvent.PROGRESS, handleProgress);
			var data:ByteArray = RestHttpOperation(event.target).resultByteArray;
			if (!data || data.length == 0) {
				LogService.error("FontLoader::handleFontDataLoaded was asked to handle data loaded for an empty buffer");
				return;
			}
			loadFontFromBuffer(data);
		}
		
		private function loadFontFromBuffer(data:ByteArray):void
		{
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, fontLoadedFromBuffer);
			var loaderContext:LoaderContext = new LoaderContext();
			loaderContext.allowCodeImport = true;
			loader.loadBytes(data, loaderContext);
		}

		/*
		The swfs were compiled with a property named fonts
		public var fonts:Array = [Normal, Italic, BoldItalic, Bold];
		Each of the elements of the array refers to a class.
		*/
		private function fontLoadedFromBuffer(event:Event):void
		{
			var FontLibrary:Class = event.target.applicationDomain.getDefinition("FontLibrary") as Class;
		    var fontLibrary:* = new FontLibrary();
			var font:*;
			for each(font in fontLibrary.fonts){
				Font.registerFont(font);
			}
			updateLoadingQueue();
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.FONTS_PROGRESS));
		}
		
		/*
		If the font is registered, remove from loading queue
		*/
		private function updateLoadingQueue():void
		{
			var fonts:Array = Font.enumerateFonts(false);
			var fontObj:Object;
			for each(fontObj in fonts){
				if("fontName" in fontObj)
					removeFontFromLoadingQueue(fontObj.fontName);
			} 
		}
		
		private function removeFontFromLoadingQueue(fontName:String):void
		{
			var index:int = -1;
			var asset:Asset;
			for each(asset in _requestedQueue) {
				if (asset.name == fontName) {
					_requestedQueue.splice(_requestedQueue.indexOf(asset), 1);
					asset.loadOperation = null;
					return;
				}
			}
		}
		
		public function get requestedCount():uint
		{
			return _requestedQueue.length;
		}
	}
}