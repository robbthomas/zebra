package com.alleni.author.controller
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.definition.application.TransitionTypes;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.view.ObjectView;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	
	public class TransitionManager extends Sprite
	{
		private var _transition:String;
		private var _time:Number;
		private var _tweenType:String;
		private var _rectangle:Rectangle;
		private var _largeScale:Number = 1;
		private var _largeAlpha:Number = 1;
		
		private var _oldView:DisplayObject;
		private var _newView:DisplayObject;

		private var _maskingSprites:Dictionary;
		private var _maskingSpriteDistance:Dictionary;
		private var _maskingSpriteStartingPoint:Dictionary;
		private var _maskingSpriteStartScale:Dictionary;
		private var _maskingSpriteScaleDistance:Dictionary;
		private var _maskingSpriteAlphaStart:Dictionary;
		private var _maskingSpriteAlphaDistance:Dictionary;
		
		public var tweenPercent:Number = 0;
		
		public function TransitionManager(oldView:DisplayObject, newView:DisplayObject, rectangle:Rectangle, transition:String = TransitionTypes.NONE, time:Number = 1, tweenType:String = "linear", pointX:Number = 0, pointY:Number = 0)
		{
			_oldView = oldView==null?new Sprite():oldView;
			_newView = newView==null?new Sprite():newView;

            _largeScale = _oldView.scaleX;
			
			if(oldView != null && oldView is ObjectView){
				_largeScale = ((_oldView as ObjectView).model.value as AbstractObject).scale * .01;
				_largeAlpha = ((_oldView as ObjectView).model.value as AbstractObject).alpha * .01;
				_oldView.visible = true;
				_oldView.scaleX = _oldView.scaleY = _largeScale;
			}
			
			if(newView != null  && newView is ObjectView){
				_largeScale = ((_newView as ObjectView).model.value as AbstractObject).scale * .01;
				_largeAlpha = ((_newView as ObjectView).model.value as AbstractObject).alpha * .01;
				_newView.visible = true;
				_newView.scaleX = _newView.scaleY = _largeScale;
			}
			
			_rectangle = rectangle;
			_transition = transition;
			if(newView == null){
				switch(_transition){
					case TransitionTypes.FADE_IN:
						_transition = TransitionTypes.FADE_OUT;
						break;
					case TransitionTypes.ZOOM_IN:
						_transition = TransitionTypes.ZOOM_OUT;
						break;
					case TransitionTypes.WIPE_LEFT_RIGHT:
						_transition = TransitionTypes.WIPE_LEFT_RIGHT_IN;
						break;
				}
			}
			_time = time;
			_tweenType = tweenType;
			_maskingSprites = new Dictionary();
			_maskingSpriteDistance = new Dictionary();
			_maskingSpriteStartingPoint = new Dictionary();
			_maskingSpriteStartScale = new Dictionary();
			_maskingSpriteScaleDistance = new Dictionary();
			_maskingSpriteAlphaStart = new Dictionary();
			_maskingSpriteAlphaDistance = new Dictionary();
			
			prepareTransition();
            tweenPercent = 0;
            updateTransition();  // prevent a blink at beginning with IRIS_OUT_CIRCLE and others
		}
		
		public function executeTransition():void{
			if(_transition == TransitionTypes.NONE){
				dispatchEvent(new Event(Event.COMPLETE));
				return;
			}
			tweenPercent = 0;
			Tweener.addTween(this, {tweenPercent:1.0, time:_time, transition:_tweenType, onUpdate:updateTransition, onComplete:transitionComplete});
		}
		
		public function updateTransition():void{
			if(_transition != TransitionTypes.NONE){
				for each(var obj:Object in _maskingSprites){
					if(obj is DisplayObject){
						switch(_transition){
							case TransitionTypes.IRIS_OUT_CIRCLE:
							case TransitionTypes.IRIS_IN_CIRCLE:
							case TransitionTypes.IRIS_OUT_SQUARE:
							case TransitionTypes.IRIS_IN_SQUARE:
							case TransitionTypes.ZOOM_OUT:
							case TransitionTypes.ZOOM_IN:
								if(obj != _maskingSprites["newMask"] && obj != _maskingSprites["oldMask"] && obj != _maskingSprites["static"]){
									obj.scaleX =  (_maskingSpriteStartScale[obj] as Point).x + (_maskingSpriteScaleDistance[obj] as Point).x * tweenPercent;
									obj.scaleY = (_maskingSpriteStartScale[obj] as Point).y + (_maskingSpriteScaleDistance[obj] as Point).y * tweenPercent;
								}
							case TransitionTypes.WIPE_BOTTOM_TOP:
							case TransitionTypes.WIPE_TOP_BOTTOM:
							case TransitionTypes.WIPE_RIGHT_LEFT:
							case TransitionTypes.WIPE_LEFT_RIGHT:
							case TransitionTypes.BARN_DOOR_CLOSE_HORIZONTAL:
							case TransitionTypes.BARN_DOOR_OPEN_HORIZONTAL:
							case TransitionTypes.BARN_DOOR_CLOSE_VERTICAL:
							case TransitionTypes.BARN_DOOR_OPEN_VERTICAL:
							case TransitionTypes.SLIDE_LEFT_RIGHT:
							case TransitionTypes.SLIDE_RIGHT_LEFT:
							case TransitionTypes.SLIDE_TOP_BOTTOM:
							case TransitionTypes.SLIDE_BOTTOM_TOP:
								if(obj != _maskingSprites["newMask"] && obj != _maskingSprites["oldMask"] && obj != _maskingSprites["static"]){
									obj.x = (_maskingSpriteStartingPoint[obj] as Point).x + (_maskingSpriteDistance[obj] as Point).x * tweenPercent;
									obj.y = (_maskingSpriteStartingPoint[obj] as Point).y + (_maskingSpriteDistance[obj] as Point).y * tweenPercent;
								}
								break;
							case TransitionTypes.FADE_IN:
							case TransitionTypes.FADE_OUT:
							case TransitionTypes.FADE_CROSS:
								if(obj != _maskingSprites["newMask"] && obj != _maskingSprites["oldMask"] && obj != _maskingSprites["static"]){
									obj.alpha = (_maskingSpriteAlphaStart[obj] as Number) + (_maskingSpriteAlphaDistance[obj] as Number) * tweenPercent;
								}
								break;
						}
					}else if(obj is Array){
						for each(var subObj:Object in obj){
							switch(_transition){
								case TransitionTypes.VENETIAN_BLINDS:
								case TransitionTypes.VERTICAL_BLINDS:
									if(subObj != _maskingSprites["newMask"] && subObj != _maskingSprites["oldMask"] && subObj != _maskingSprites["static"]){
										subObj.scaleX =  (_maskingSpriteStartScale[obj] as Point).x + (_maskingSpriteScaleDistance[obj] as Point).x * tweenPercent;
										subObj.scaleY = (_maskingSpriteStartScale[obj] as Point).y + (_maskingSpriteScaleDistance[obj] as Point).y * tweenPercent;
									}
									break;
							}
						}
					}
				}
			}else{
				Tweener.removeTweens(this);
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}
		

		private function prepareTransition():void{
			var newMasks:Sprite = null;
			var oldMasks:Sprite = null;
			
			switch(_transition){
				case TransitionTypes.BARN_DOOR_OPEN_VERTICAL:
					newMasks = new Sprite();
					oldMasks = null;
					
					var newTopSprite:Sprite = drawRect(_rectangle.width + 2, (_rectangle.height/2) + 40);
					newTopSprite.x = 0;
					newTopSprite.y = -((_rectangle.height/2) + 40);
					var newBottomSprite:Sprite = drawRect(_rectangle.width + 2, (_rectangle.height/2) + 40);
					newBottomSprite.x = 0;
					newBottomSprite.y = _rectangle.y + _rectangle.height;
					
					addChild(_oldView);
					addChild(_newView);
					addChild(newMasks);
					
					newMasks.addChild(newTopSprite);
					newMasks.addChild(newBottomSprite);
					_maskingSprites["newTopSprite"] = newTopSprite;
					_maskingSprites["newBottomSprite"] = newBottomSprite;
					_maskingSprites["newMask"] = newMasks;
					_newView.mask = newMasks;
					_maskingSpriteDistance[newTopSprite] = new Point(newTopSprite.x, _rectangle.height/2);
					_maskingSpriteDistance[newBottomSprite] = new Point(newTopSprite.x, -(_rectangle.height/2));
					_maskingSpriteStartingPoint[newTopSprite] = new Point(newTopSprite.x, newTopSprite.y);
					_maskingSpriteStartingPoint[newBottomSprite] = new Point(newBottomSprite.x, newBottomSprite.y);
					break;
				case TransitionTypes.BARN_DOOR_CLOSE_VERTICAL:
					newMasks = null
					oldMasks = new Sprite();
					
					var oldTopSprite:Sprite = drawRect(_rectangle.width + 2, (_rectangle.height/2) + 40);
					oldTopSprite.x = 0;
					oldTopSprite.y = -40;
					var oldBottomSprite:Sprite = drawRect(_rectangle.width + 2, (_rectangle.height/2) + 40);
					oldBottomSprite.x = 0;
					oldBottomSprite.y = _rectangle.height/2;
					
					addChild(_newView);
					addChild(_oldView);
					addChild(oldMasks);
					
					oldMasks.addChild(oldTopSprite);
					oldMasks.addChild(oldBottomSprite);
					_maskingSprites["oldTopSprite"] = oldTopSprite;
					_maskingSprites["oldBottomSprite"] = oldBottomSprite;
					_maskingSprites["oldMask"] = oldMasks;
					_oldView.mask = oldMasks;
					_maskingSpriteDistance[oldTopSprite] = new Point(oldTopSprite.x, -((_rectangle.height/2) + 40));
					_maskingSpriteDistance[oldBottomSprite] = new Point(oldBottomSprite.x, _rectangle.y + _rectangle.height);
					_maskingSpriteStartingPoint[oldTopSprite] = new Point(oldTopSprite.x, oldTopSprite.y);
					_maskingSpriteStartingPoint[oldBottomSprite] = new Point(oldBottomSprite.x, oldBottomSprite.y);
					break;
				case TransitionTypes.BARN_DOOR_CLOSE_HORIZONTAL:
					newMasks = new Sprite();
					oldMasks = null
					
					var newLeftSprite:Sprite = drawRect((_rectangle.width/2) + 40, _rectangle.height + 2);
					newLeftSprite.x = -((_rectangle.width/2) + 40);
					newLeftSprite.y = 0;
					var newRightSprite:Sprite = drawRect((_rectangle.width/2) + 40, _rectangle.height + 2);
					newRightSprite.x = _rectangle.x + _rectangle.width;
					newRightSprite.y = 0;
					
					addChild(_oldView);
					addChild(_newView);
					addChild(newMasks);
					
					newMasks.addChild(newLeftSprite);
					newMasks.addChild(newRightSprite);
					_maskingSprites["newLeftSprite"] = newLeftSprite;
					_maskingSprites["newRightSprite"] = newRightSprite;
					_maskingSprites["newMask"] = newMasks;
					_newView.mask = newMasks;
					_maskingSpriteDistance[newLeftSprite] = new Point(_rectangle.width/2, newLeftSprite.y);
					_maskingSpriteDistance[newRightSprite] = new Point(-(_rectangle.width/2), newRightSprite.y);
					_maskingSpriteStartingPoint[newLeftSprite] = new Point(newLeftSprite.x, newLeftSprite.y);
					_maskingSpriteStartingPoint[newRightSprite] = new Point(newRightSprite.x, newRightSprite.y);
					break;
				case TransitionTypes.BARN_DOOR_OPEN_HORIZONTAL:
					newMasks = null;
					oldMasks = new Sprite();
					
					var oldLeftSprite:Sprite = drawRect((_rectangle.width/2) + 40, _rectangle.height + 2);
					oldLeftSprite.x = -40;
					oldLeftSprite.y = 0;
					var oldRightSprite:Sprite = drawRect((_rectangle.width/2) + 40, _rectangle.height + 2);
					oldRightSprite.x = _rectangle.width/2;
					oldRightSprite.y = 0;
					
					addChild(_newView);
					addChild(_oldView);
					addChild(oldMasks);
					
					oldMasks.addChild(oldLeftSprite);
					oldMasks.addChild(oldRightSprite);
					_maskingSprites["oldLeftSprite"] = oldLeftSprite;
					_maskingSprites["oldRightSprite"] = oldRightSprite;
					_maskingSprites["oldMask"] = oldMasks;
					_oldView.mask = oldMasks;
					_maskingSpriteDistance[oldLeftSprite] = new Point(-(_rectangle.width/2), oldLeftSprite.y);
					_maskingSpriteDistance[oldRightSprite] = new Point(_rectangle.width/2, oldRightSprite.y);
					_maskingSpriteStartingPoint[oldLeftSprite] = new Point(oldLeftSprite.x, oldLeftSprite.y);
					_maskingSpriteStartingPoint[oldRightSprite] = new Point(oldRightSprite.x, oldRightSprite.y);
					break;
				case TransitionTypes.WIPE_LEFT_RIGHT:
					newMasks = new Sprite();
					oldMasks = null
					
					var newLeftSprite:Sprite = drawRect(_rectangle.width + 40, _rectangle.height + 2);
					newLeftSprite.x = _rectangle.x - (_rectangle.width + 40);
					newLeftSprite.y = _rectangle.y;
					
					addChild(_oldView);
					addChild(_newView);
					addChild(newMasks);
					
					newMasks.addChild(newLeftSprite);
					_maskingSprites["newLeftSprite"] = newLeftSprite;
					_maskingSprites["newMask"] = newMasks;
					_newView.mask = newMasks;
					_maskingSpriteDistance[newLeftSprite] = new Point(_rectangle.width, 0);
					_maskingSpriteStartingPoint[newLeftSprite] = new Point(newLeftSprite.x, newLeftSprite.y);
					break;
				case TransitionTypes.WIPE_LEFT_RIGHT_IN:
					oldMasks = new Sprite();
					newMasks =  null
					
					var oldLeftSprite:Sprite = drawRect(_rectangle.width + 40, _rectangle.height + 2);
					oldLeftSprite.x = _rectangle.x + _rectangle.width;
					oldLeftSprite.y = _rectangle.y;
					
					addChild(_newView);
					addChild(_oldView);
					addChild(oldMasks);
					
					oldMasks.addChild(oldLeftSprite);
					_maskingSprites["oldLeftSprite"] = oldLeftSprite;
					_maskingSprites["oldMask"] = newMasks;
					_oldView.mask = oldMasks;
					_maskingSpriteDistance[oldLeftSprite] = new Point(-(_rectangle.width), 0);
					_maskingSpriteStartingPoint[oldLeftSprite] = new Point(oldLeftSprite.x, oldLeftSprite.y);
					break;
				case TransitionTypes.WIPE_RIGHT_LEFT:
					newMasks = new Sprite();
					oldMasks = null
					
					var newRightSprite:Sprite = drawRect(_rectangle.width + 40, _rectangle.height + 2);
					newRightSprite.x = _rectangle.width;
					newRightSprite.y = 0;
					
					addChild(_oldView);
					addChild(_newView);
					addChild(newMasks);
					
					newMasks.addChild(newRightSprite);
					_maskingSprites["newLeftSprite"] = newRightSprite;
					_maskingSprites["newMask"] = newMasks;
					_newView.mask = newMasks;
					_maskingSpriteDistance[newRightSprite] = new Point(-(_rectangle.width), newRightSprite.y);
					_maskingSpriteStartingPoint[newRightSprite] = new Point(newRightSprite.x, newRightSprite.y);
					break;
				case TransitionTypes.WIPE_TOP_BOTTOM:
					newMasks = new Sprite();
					oldMasks = null
					
					var newTopSprite:Sprite = drawRect(_rectangle.width + 2, _rectangle.height + 40);
					newTopSprite.y = -(_rectangle.height + 40);
					newTopSprite.x = 0;
					
					addChild(_oldView);
					addChild(_newView);
					addChild(newMasks);
					
					newMasks.addChild(newTopSprite);
					_maskingSprites["newTopSprite"] = newTopSprite;
					_maskingSprites["newMask"] = newMasks;
					_newView.mask = newMasks;
					_maskingSpriteDistance[newTopSprite] = new Point(newTopSprite.x, _rectangle.height);
					_maskingSpriteStartingPoint[newTopSprite] = new Point(newTopSprite.x, newTopSprite.y);
					break;
				case TransitionTypes.WIPE_BOTTOM_TOP:
					newMasks = new Sprite();
					oldMasks = null
					
					var newBottomSprite:Sprite = drawRect(_rectangle.width + 2, _rectangle.height + 40);
					newBottomSprite.y = _rectangle.height;
					newBottomSprite.x = 0;
					
					addChild(_oldView);
					addChild(_newView);
					addChild(newMasks);
					
					newMasks.addChild(newBottomSprite);
					_maskingSprites["newBottomSprite"] = newBottomSprite;
					_maskingSprites["newMask"] = newMasks;
					_newView.mask = newMasks;
					_maskingSpriteDistance[newBottomSprite] = new Point(newBottomSprite.x, -(_rectangle.height));
					_maskingSpriteStartingPoint[newBottomSprite] = new Point(newBottomSprite.x, newBottomSprite.y);
					break;
				case TransitionTypes.VERTICAL_BLINDS:
					newMasks = new Sprite();
					oldMasks = null;
					
					var i:uint;
					var blindWidth:Number = _rectangle.width/10;
					
					var newBlindSprite:Sprite;
					var blinds:Array = [];
					for(i=0; i<10; i++){
						newBlindSprite = new Sprite();
						newBlindSprite.graphics.beginFill(0x000000);
						newBlindSprite.graphics.drawRect(0, 0, blindWidth, _rectangle.height);
						newBlindSprite.graphics.endFill();
						newBlindSprite.scaleX = 0;
						newBlindSprite.x = i * blindWidth;
						blinds.push(newBlindSprite);
						newMasks.addChild(newBlindSprite);
					}
					
					addChild(_oldView);
					addChild(_newView);
					addChild(newMasks);
					
					_maskingSprites["blinds"] = blinds;
					_maskingSprites["newMask"] = newMasks;
					_newView.mask = newMasks;
					_maskingSpriteStartScale[blinds] = new Point(0, 1);
					_maskingSpriteScaleDistance[blinds] = new Point(1, 1);
					break;
				case TransitionTypes.VENETIAN_BLINDS:
					newMasks = new Sprite();
					oldMasks = null;
					
					var i:uint;
					var blindHeight:Number = _rectangle.height/10;
					
					var newBlindSprite:Sprite;
					var blinds:Array = [];
					for(i=0; i<10; i++){
						newBlindSprite = new Sprite();
						newBlindSprite.graphics.beginFill(0x000000);
						newBlindSprite.graphics.drawRect(0, 0, _rectangle.width, blindHeight);
						newBlindSprite.graphics.endFill();
						newBlindSprite.scaleX = 0;
						newBlindSprite.y = i * blindHeight;
						blinds.push(newBlindSprite);
						newMasks.addChild(newBlindSprite);
					}
					
					addChild(_oldView);
					addChild(_newView);
					addChild(newMasks);
					
					_maskingSprites["blinds"] = blinds;
					_maskingSprites["newMask"] = newMasks;
					_newView.mask = newMasks;
					_maskingSpriteStartScale[blinds] = new Point(1, 0);
					_maskingSpriteScaleDistance[blinds] = new Point(1, 1);
					break;
				case TransitionTypes.IRIS_IN_CIRCLE:
					newMasks = null;
					oldMasks = new Sprite();
					var circleFullRadius:Number = (Math.sqrt((Math.pow(_rectangle.height,2) + Math.pow(_rectangle.width,2))))/2;
					var circleSmallScale:Number = 0;
					var circleSmallRadius:Number = circleSmallRadius * ((Math.sqrt((Math.pow(_rectangle.height,2) + Math.pow(_rectangle.width,2))))/2);
				
					var oldCircleSprite:Sprite = new Sprite();
					oldCircleSprite.graphics.beginFill(0x000000);
					oldCircleSprite.graphics.drawCircle(0, 0, circleFullRadius);
					oldCircleSprite.graphics.endFill();
					oldCircleSprite.y = _rectangle.height/2;
					oldCircleSprite.x = _rectangle.width/2;
					
					addChild(_newView);
					addChild(_oldView);
					addChild(oldMasks);
					oldMasks.addChild(oldCircleSprite);
					
					_maskingSprites["oldCircleSprite"] = oldCircleSprite;
					_maskingSprites["oldMask"] = oldMasks;
					_oldView.mask = oldMasks;
					_maskingSpriteDistance[oldCircleSprite] = new Point(0, 0);
					_maskingSpriteStartingPoint[oldCircleSprite] = new Point(oldCircleSprite.x, oldCircleSprite.y);
					_maskingSpriteStartScale[oldCircleSprite] = new Point(1, 1);
					_maskingSpriteScaleDistance[oldCircleSprite] = new Point(-1, -1);
					break;
				case TransitionTypes.IRIS_OUT_CIRCLE:
					newMasks = new Sprite();
					oldMasks = null;
					var circleFullRadius:Number = (Math.sqrt((Math.pow(_rectangle.height,2) + Math.pow(_rectangle.width,2))))/2;
					var circleSmallScale:Number = 0;
					var circleSmallRadius:Number = circleSmallRadius * ((Math.sqrt((Math.pow(_rectangle.height,2) + Math.pow(_rectangle.width,2))))/2);
					
					var newCircleSprite:Sprite = new Sprite();
					newCircleSprite.graphics.beginFill(0x000000);
					newCircleSprite.graphics.drawCircle(0, 0, circleFullRadius);
					newCircleSprite.graphics.endFill();
					newCircleSprite.y = _rectangle.height/2;
					newCircleSprite.x = _rectangle.width/2;
					
					addChild(_oldView);
					addChild(_newView);
					addChild(newMasks);
					newMasks.addChild(newCircleSprite);
					
					_maskingSprites["newCircleSprite"] = newCircleSprite;
					_maskingSprites["newMask"] = newMasks;
					_newView.mask = newMasks;
					_maskingSpriteDistance[newCircleSprite] = new Point(0, 0);
					_maskingSpriteStartingPoint[newCircleSprite] = new Point(newCircleSprite.x, newCircleSprite.y);
					_maskingSpriteStartScale[newCircleSprite] = new Point(0, 0);
					_maskingSpriteScaleDistance[newCircleSprite] = new Point(1, 1);
					break;
				case TransitionTypes.IRIS_IN_SQUARE:
					newMasks = null;
					oldMasks = new Sprite();
					
					var oldRectSprite:Sprite = new Sprite();
					oldRectSprite.graphics.beginFill(0x000000);
					oldRectSprite.graphics.drawRect(0, 0, _rectangle.width, _rectangle.height);
					oldRectSprite.graphics.endFill();
					oldRectSprite.y = 0;
					oldRectSprite.x = 0;
					
					addChild(_newView);
					addChild(_oldView);
					addChild(oldMasks);
					oldMasks.addChild(oldRectSprite);
					
					_maskingSprites["oldRectSprite"] = oldRectSprite;
					_maskingSprites["oldMask"] = oldMasks;
					_oldView.mask = oldMasks;
					_maskingSpriteDistance[oldRectSprite] = new Point(_rectangle.width/2, _rectangle.height/2);
					_maskingSpriteStartingPoint[oldRectSprite] = new Point(oldRectSprite.x, oldRectSprite.y);
					_maskingSpriteStartScale[oldRectSprite] = new Point(1, 1);
					_maskingSpriteScaleDistance[oldRectSprite] = new Point(-1, -1);
					break;
				case TransitionTypes.IRIS_OUT_SQUARE:
					newMasks = new Sprite();
					oldMasks = null;
					
					var newRectSprite:Sprite = new Sprite();
					newRectSprite.graphics.beginFill(0x000000);
					newRectSprite.graphics.drawRect(0, 0, _rectangle.width, _rectangle.height);
					newRectSprite.graphics.endFill();
					newRectSprite.y = _rectangle.height/2;
					newRectSprite.x = _rectangle.width/2;
					newRectSprite.scaleX = newRectSprite.scaleY = 0;
					
					addChild(_oldView);
					addChild(_newView);
					addChild(newMasks);
					newMasks.addChild(newRectSprite);
					
					_maskingSprites["newRectSprite"] = newRectSprite;
					_maskingSprites["newMask"] = newMasks;
					_newView.mask = newMasks;
					_maskingSpriteDistance[newRectSprite] = new Point(-(_rectangle.width/2), -(_rectangle.height/2));
					_maskingSpriteStartingPoint[newRectSprite] = new Point(newRectSprite.x, newRectSprite.y);
					_maskingSpriteStartScale[newRectSprite] = new Point(0, 0);
					_maskingSpriteScaleDistance[newRectSprite] = new Point(1, 1);
					break;
				case TransitionTypes.SLIDE_LEFT_RIGHT:
					_newView.x = -(_rectangle.width);
					addChild(_oldView);
					addChild(_newView);
					
					_maskingSprites["newView"] = _newView;
					_maskingSprites["oldView"] = _oldView;
					_maskingSpriteStartingPoint[_newView] = new Point(_newView.x, _newView.y);
					_maskingSpriteStartingPoint[_oldView] = new Point(_oldView.x, _oldView.y);
					_maskingSpriteDistance[_newView] = new Point(_rectangle.width, 0);
					_maskingSpriteDistance[_oldView] = new Point(_rectangle.width, 0);
					break;
				case TransitionTypes.SLIDE_RIGHT_LEFT:
					_newView.x = _rectangle.width;
					addChild(_oldView);
					addChild(_newView);
					
					_maskingSprites["newView"] = _newView;
					_maskingSprites["oldView"] = _oldView;
					_maskingSpriteStartingPoint[_newView] = new Point(_newView.x, _newView.y);
					_maskingSpriteStartingPoint[_oldView] = new Point(_oldView.x, _oldView.y);
					_maskingSpriteDistance[_newView] = new Point(-(_rectangle.width), 0);
					_maskingSpriteDistance[_oldView] = new Point(-(_rectangle.width), 0);
					break;
				case TransitionTypes.SLIDE_TOP_BOTTOM:
					_newView.y = -(_rectangle.height);
					addChild(_oldView);
					addChild(_newView);
					
					_maskingSprites["newView"] = _newView;
					_maskingSprites["oldView"] = _oldView;
					_maskingSpriteStartingPoint[_newView] = new Point(_newView.x, _newView.y);
					_maskingSpriteStartingPoint[_oldView] = new Point(_oldView.x, _oldView.y);
					_maskingSpriteDistance[_newView] = new Point(0, _rectangle.height);
					_maskingSpriteDistance[_oldView] = new Point(0, _rectangle.height);
					break;
				case TransitionTypes.SLIDE_BOTTOM_TOP:
					_newView.y = _rectangle.height;
					addChild(_oldView);
					addChild(_newView);
					
					_maskingSprites["newView"] = _newView;
					_maskingSprites["oldView"] = _oldView;
					_maskingSpriteStartingPoint[_newView] = new Point(_newView.x, _newView.y);
					_maskingSpriteStartingPoint[_oldView] = new Point(_oldView.x, _oldView.y);
					_maskingSpriteDistance[_newView] = new Point(0, -(_rectangle.height));
					_maskingSpriteDistance[_oldView] = new Point(0, -(_rectangle.height));
					break;
				case TransitionTypes.FADE_IN:
					_newView.alpha = 0;
					addChild(_oldView);
					addChild(_newView);
					
					_newView.alpha = 0;
                    _maskingSprites["newView"] = _newView;
					_maskingSpriteAlphaStart[_newView] = 0;
					_maskingSpriteAlphaDistance[_newView] = _largeAlpha;
					break;
				case TransitionTypes.FADE_OUT:
					addChild(_newView);
					addChild(_oldView);
					_maskingSprites["oldView"] = _oldView;
					_maskingSpriteAlphaStart[_oldView] = 1;
					_maskingSpriteAlphaDistance[_oldView] = -1 * _largeAlpha;
					break;
				case TransitionTypes.FADE_CROSS:
					addChild(_newView);
					addChild(_oldView);

					_newView.alpha = 0;
                    _maskingSprites["newView"] = _newView;
					_maskingSpriteAlphaStart[_newView] = 0;
					_maskingSpriteAlphaDistance[_newView] = _largeAlpha;

					_maskingSprites["oldView"] = _oldView;
					_maskingSpriteAlphaStart[_oldView] = 1;
					_maskingSpriteAlphaDistance[_oldView] = -1 * _largeAlpha;
					break;
				case TransitionTypes.ZOOM_IN:
					if(!(_newView is ObjectView)){
						_newView.y = _rectangle.height/2;
						_newView.x = _rectangle.width/2;
					}
					_newView.scaleX = _newView.scaleY = 0;
					addChild(_oldView);
					addChild(_newView);
										
					_maskingSprites["newView"] = _newView;
					_maskingSpriteDistance[_newView] = new Point(_newView is ObjectView?0:-(_rectangle.width/2), _newView is ObjectView?0:-(_rectangle.height/2));
					_maskingSpriteStartingPoint[_newView] = new Point(_newView.x, _newView.y);
					_maskingSpriteStartScale[_newView] = new Point(0, 0);
					_maskingSpriteScaleDistance[_newView] = new Point(_largeScale, _largeScale);
					break;
				case TransitionTypes.ZOOM_OUT:
					addChild(_newView);
					addChild(_oldView);
										
					_maskingSprites["oldView"] = _oldView;
					_maskingSpriteDistance[_oldView] = new Point(_oldView is ObjectView?0:_rectangle.width/2, _oldView is ObjectView?0:_rectangle.height/2);
					_maskingSpriteStartingPoint[_oldView] = new Point(_oldView.x, _oldView.y);
					_maskingSpriteStartScale[_oldView] = new Point(_largeScale, _largeScale);
					_maskingSpriteScaleDistance[_oldView] = new Point(-1 * _largeScale, -1 * _largeScale);
					break;
			}
            if (oldMasks) {
                oldMasks.visible = false;  // prevent masks showing with eventFlow transitions, since TransitionManager is left on-screen for a bit after completion
            }
            if (newMasks) {
                newMasks.visible = false;
            }
		}
		
		private function drawRect(theWidth:Number, theHeight:Number):Sprite{
			var newRect:Sprite = new Sprite();
			newRect.graphics.beginFill(0x000000);
			newRect.graphics.drawRect(0,0,theWidth,theHeight);
			newRect.graphics.endFill();
			return newRect;
		}
		
		private function transitionComplete():void{
			_newView.mask = null;
			_oldView.mask = null;
			dispatchEvent(new Event(Event.COMPLETE));
		}

        public function terminate():void
        {
            Tweener.removeTweens(this);
        }

	}
}
