/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller
{
import com.alleni.author.application.ApplicationUI;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.WireController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.InspectorPresets;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.OutletDescription;
import com.alleni.author.definition.PositionAndAngle;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.action.ModifyRibbonAction;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.AbstractTable;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.HelicopterWireAnchor;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.InspectorValues;
import com.alleni.author.model.ui.InternalSmartWireAnchor;
import com.alleni.author.model.ui.SetterWireAnchor;
import com.alleni.author.model.ui.TriggerWireAnchor;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.controller.IChildController;
import com.alleni.taconite.controller.IChildMediator;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.ModelCollectionEvent;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.event.ModelUpdateEvent;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.persistence.json.JSON;
import com.alleni.taconite.view.ITaconiteView;

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.MouseEvent;
import flash.geom.Point;

import mx.collections.ArrayCollection;
import mx.collections.IList;

public class ObjectController extends EventDispatcher implements IChildController, IChildMediator
	{
		public static var instance:ObjectController = new ObjectController();
				
		protected static var _wiring:WireController = ApplicationController.instance.wireController;

        private var _application:Application = Application.instance;

		protected static function getObject(model:TaconiteModel):AbstractObject
		{
			return model.value as AbstractObject;
		}

        /**
         * This is called by the object factory upon object creation. This ensures any handlers are set up
         * which need to be listening even if we are not "running"...
         * @param model
         *
         */
        public function initializeObject(model:TaconiteModel):void
        {
        	var object:AbstractObject = getObject(model);

        	if (!object.controlled) {
	        	model.addEventListener(ModelUpdateEvent.MODEL_UPDATE, modelUpdateListener, false, 5);
				model.addEventListener(ModelEvent.MODEL_CHANGE, modelChangeListener, false, 5);
    			object.controlled = true;
        	}
	        if (!object.loading) {
		        object.initialValues = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(object));
		        delete object.initialValues.uid;
                for (var initProp:String in object.initialValues) {
                    if(!propertyNeedsInit(initProp, object)) {
                        delete object.initialValues[initProp];
                    }
                }

                createInitialRibbons(object);

                if(object is World) {
                    var world:World = object as World;
                    if(world.project == null) {
                        ObjectFactory.createForName("ProjectObject", world, "project");
                        world.project.complete = true;
                        world.project.active = true;
                        ObjectFactory.createForName("EventFlow", world, "eventFlow", 0,0, world.width, world.height);
                        world.eventFlow.complete = true;
                        world.eventFlow.active = true;
                        World(object).project.complete = true;
                    }
                }
	        }
			var property:String;
			for (property in object.modifiers) {
				var prop:PropertyDescription = object.modifiers[property] as PropertyDescription;
				if (prop == null || prop.indexed == false) {
					continue;
				}
				var anchors:IList = object.anchors[property] as IList;
				if (anchors == null) {
					anchors = new ArrayCollection();
					object.anchors[property] = anchors;
				}
				var numProps:int = IList(object[property]).length;
				var numAnchors:int = anchors.length;
				var index:int;
				for (index = numAnchors; index < numProps; index++) {
					anchors.addItemAt(prop.createAnchor(object,  property, index), index);
				}
				if (numAnchors == numProps && numAnchors < prop.autoAddUpTo) {
					anchors.addItemAt(prop.createAnchor(object,  property, index), numAnchors);
				}
			}
        }

        public function createInitialRibbons(object:AbstractObject):void
		{
            var addShow:Boolean = true;
            var addHide:Boolean = true;
            var addDropped:Boolean = true;
            var addCollide:Boolean = true;
            var addCurrentPageSetter:Boolean = true;

            for (var key:String in object.anchors){
                var anchor:* = object.anchors[key];
                if(anchor is WireAnchor){
                    if(anchor is SetterWireAnchor){
                        if((anchor as SetterWireAnchor).label == "Show"){
                            addShow = false;
                        }

                        if((anchor as SetterWireAnchor).label == "Hide"){
                            addHide = false;
                        }

                        if((anchor as SetterWireAnchor).setterProperty == "currentPage" && (anchor as SetterWireAnchor).inValue == null){
                            addCurrentPageSetter == false;
                        }
                    }
                    if(anchor is TriggerWireAnchor){
                        if((anchor as TriggerWireAnchor).triggerProperty == "droppedOn" && (anchor as TriggerWireAnchor).condition == null){
                            addDropped = false;
                        }
                        if((anchor as TriggerWireAnchor).triggerProperty == "collidingWith" && (anchor as TriggerWireAnchor).condition == null){
                            addCollide = false;
                        }
                    }
                }

            }

            var visibleAnchor:WireAnchor = object.anchors["visible"];
            if(visibleAnchor) {
                if(addShow){
                    var show:SetterWireAnchor = new SetterWireAnchor(object.anchors["visible"], false, null, false, true, "show");
                }
                if(addHide){
                    var hide:SetterWireAnchor = new SetterWireAnchor(object.anchors["visible"], false, null, false, false, "hide");
                }
            }
            var droppedAnchor:WireAnchor = object.anchors["droppedOn"];
            if(droppedAnchor && addDropped) {
                new TriggerWireAnchor(droppedAnchor, false);
            }
            var collidingAnchor:WireAnchor = object.anchors["collidingWith"];
            if(collidingAnchor && addCollide) {
                new TriggerWireAnchor(collidingAnchor, false);
            }
            var currentPageAnchor:WireAnchor = object.anchors["currentPage"];
            if(currentPageAnchor && addCurrentPageSetter) {
                new SetterWireAnchor(currentPageAnchor, false);
            }
        }
		
		public function handleCreationInitialize(object:AbstractObject):void
		{
			// Create an object using object type preset values (if they exist)
			var values:InspectorPresets = InspectorValues.instance.getPreset(object.shortClassName) as InspectorPresets;
			var property:String;
			for (property in values) {
				if (property in object) object[property] = values[property];
			}
			for each (property in (values.messageCenterVisible as Array)) {
				if (property in object.anchors) (object.anchors[property] as WireAnchor).messageCenterVisible = true;
			}
			for each (property in (values.propertyInspectorVisible as Array)) {
				if (property in object.anchors) (object.anchors[property] as WireAnchor).propertyInspectorVisible = true;
			}
		}
			
		public function handleCreationComplete(object:AbstractObject, location:Point):void
		{
			// NOTE: the PathController overrides this function
			
			// enforce minimum obj size, in case user clicked without moving mouse
			if(Math.abs(object.width) < 3 && Math.abs(object.height) < 3) {
				object.width = 25;
				object.height = 25;
			}
			
			// drop object into any container it may be sitting on
			ObjectDragMediator.dropIntoContainer(object.getView());
			object.setInitialZIndex();

            objectCompleted(object);
		}

        public function objectCompleted(object:AbstractObject):void
        {
			// signal object completion; this makes the message center appear among other things
            // this is used for most cases of object creation, but not the ones where message center creation is deferred, like PathObject and TextObject
			object.complete = true;
            object.active = true;
        }
        
		private function modelUpdateListener(e:ModelUpdateEvent):void
		{
            if (_application.testPanelVisible) {
                ++_application.testObjectActivitySerial;
            }
			handleModelUpdate(e.source, e.property as String, e.newValue, e.oldValue, e.parent);
		}
		
		private function modelChangeListener(e:ModelEvent):void
		{
			const modelCollectionEvent:ModelCollectionEvent = e as ModelCollectionEvent;
			if (modelCollectionEvent) {
				handleModelCollectionUpdate(modelCollectionEvent.parent, modelCollectionEvent.property, modelCollectionEvent.collection as IList, 
					modelCollectionEvent.kind, modelCollectionEvent.index, modelCollectionEvent.item, modelCollectionEvent.oldItem);
			} else {
				handleModelChange(e.kind, e.parent, e.child, e.index);
			}
		}
		
		protected function handleModelChange(kind:String, parent:TaconiteModel, child:TaconiteModel, index:int):void
		{	
		}
		
		protected function handleModelCollectionUpdate(model:TaconiteModel, property:String, collection:IList, kind:String, index:int, item:Object, oldItem:Object):void
		{
			var obj:AbstractObject = getObject(model);
			if (obj as AbstractTable && AbstractTable(obj).rearranging)
				return;
			if (!obj.loading && _wiring.bindingsEnabled) {
				if (!Application.running && propertyNeedsInit(property, obj)) {
					obj.initialValues[property] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(obj[property]));
				}
			}
			var prop:PropertyDescription = obj.modifiers[property] as PropertyDescription;
			if (prop == null || prop.indexed == false)
				return;
			var anchors:IList = obj.anchors[property] as IList;
			switch(kind) {
				case ModelEvent.ADD_ARRAY_ITEM:
					if(anchors == null) {
						anchors = new ArrayCollection();
						obj.anchors[property] = anchors;
					}

					// prevent other damn controllers from adding
					// another anchor after we've already added one
					var needed:int = obj[property].length;
					if(obj[property].length < prop.autoAddUpTo) {
						needed++;
						if(index == IList(obj[property]).length-1) {
							// adding at the end position, so actually insert after the current end
							// to preserve newly created wires
							index++;
						}
					}
					if(anchors.length >= needed)
						break;
					// end damn parent controller hack

					anchors.addItemAt(prop.createAnchor(obj,  property, index), index);
					break;
				case ModelEvent.REMOVE_ARRAY_ITEM:
					anchors.removeItemAt(index);
					break;
				case ModelEvent.CHANGE_ARRAY_ITEM:
					_wiring.anchorTriggered(anchors[index]);
					break;
			}
		}

		protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
		{
			var obj:AbstractObject = getObject(model);
			var prop:PropertyDescription = obj.modifiers[property] as PropertyDescription;
//			trace("ObjectController:handleModelUpdate prop="+property, "value="+newValue, "obj="+model.value);

			// update initialValues if this model update was made during authoring
			// but first make sure we aren't loading document or initializing for Run command
			if (!obj.loading && _wiring.bindingsEnabled && this === obj.controller) {
				if (!Application.running && propertyNeedsInit(property, obj)) {
					var abstractObject:AbstractObject = obj[property] as AbstractObject;
					if (abstractObject) {
                        obj.initialValues[property] = abstractObject.referenceJSON;
                    } else {
						obj.initialValues[property] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(newValue));   // this is also done in ObjectDragMediator, to adjust position while running
					}
                    if ((oldValue as Asset) || (newValue as Asset)) {
                        LibraryController.instance.updateForAssetChange();
                    }
//					trace("ObjectController: set INIT prop="+property, "val="+newValue, "obj="+obj);
				}
			}
			
			// transmit the new value to the other bound properties (whether paused or running)
			if (_wiring.bindingsEnabled)  {// not doing initialization of properties for Run command
				switch (property) {
					// Do nothing with properties that can't be bound
					case "thumbnailScale":
						break;
					case "active":
						if (newValue as Boolean)
							activate(model);
						else
							deactivate(model);
						break;
					case "locationXYR":
						if(obj.isAnimationPath && !obj.dragging && !obj.draggingHandle) {
							obj.resnapAll();
						}
						break;
					case "snapTo":
					case "pathOn":
					case "pathPercent":
						_wiring.anchorTriggered(obj.anchors[property]);
						if (obj.snapTo)
							obj.snapObjectToPath();  // snap to current pathPercent
						break;
					case "width":
					case "height":
					case "rotation":
					case "scale":
						if (obj.isAnimationPath)
							obj.resnapAll();
						break;
					case "pathMinimum":
					case "pathMaximum":
                        if(!obj.loading) {
                            var val:Number = obj.pathValue;
                            var min:Number = Math.min(obj.pathMinimum, obj.pathMaximum);
                            var max:Number = Math.max(obj.pathMinimum, obj.pathMaximum);
                            if(obj.pathValue > max) {
                                obj.pathValue = max;
                            } else if(obj.pathValue < min) {
                                obj.pathValue = min;
                            }
                        }
						break;
					case "draggingHandle":
						if (newValue == false && obj.snapTo)
							obj.snapObjectToPath();
						break;
					case "collidingWith":
					case "droppedOn":
						//ApplicationController.instance.requestRender(); // bootstrap in lieu of it ocurring organically since these may not constitute a graphics change
						ApplicationUI.instance.rawChildren.addEventListener(Event.ENTER_FRAME, function(event:Event):void{
                            ApplicationUI.instance.stage.invalidate();
                            event.currentTarget.graphics.moveTo(0, 0);
                            event.currentTarget.graphics.lineTo(0, 0);
                            event.currentTarget.removeEventListener(event.type, arguments.callee);
                        });
						break;
				}
				_wiring.anchorTriggered(obj.anchors[property]);
			}
			var anchor:WireAnchor = obj.anchors[property] as WireAnchor;
			if (anchor) {
				anchor.valueChanged();
			}
		}
		
		protected function activate(model:TaconiteModel):void
		{
		}
		
		protected function deactivate(model:TaconiteModel):void
		{
			// (ObjectView will help by removing the handles)
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.DESELECT_OBJECT, model));
		}
		
		public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			var obj:AbstractObject = AbstractObject(object);
			switch (modifier.key) {
				// Hide & Show are now done via SetterWireAnchor
				case "cloak":
					obj.cloakedNow = true;
					break;
				case "bringFront":
                case "bringForward":
                case "sendBack":
                case "sendBackward":
					arrangeObject(object as AbstractObject, modifier.key);
					break;
				case "putBack":
					obj.putBack();
                    if (Application.running) {
                        CollisionMediator.checkDropTarget(obj);
                    }
					break;
				case "snapToNearest":
				case "snapToValue":
					var view:ObjectView = obj.getView();
					if (view && obj.snapTo) {
						var global:Point = view.stagePosition;
						obj.pathOn = true;
						if (modifier.key == "snapToNearest") {
							obj.snapTo.moveObjAlongPath(obj, global);
						} else {
							obj.snapObjectToPath();
						}
					}
					break;
				case "resetObject":
					obj.resetObject(ApplicationController.instance.authorController.world);
					break;
				default:
					break;
			}
		}

		/**
		 * Return true if the specified property is needs to have an init value created.
		 * Mostly this means registered properties that appear in ribbons...
		 * But certain high-level properties must NOT be given inits.
		 * 
		 * For example, rotation is a high-level property that affects x,y and rawRotation.
		 * Likewise, x affects x,y and rawRotation. 
		 * So if inits were applied to these high-level properties, and also applied to the
		 * low-level properties, the result would be incorrect.
		 * 
		 * Therefore we are careful to do inits for x,y and rawRotation; not for x, etc.
		 * Similar situation for knobX of a slider, which affects slider.value.
		 * 
		 * The properties given a "false" value here, have [Transient] marked on the model variable.
		 * But we don't have a handy way to check for a property being transient, at this level.
		 * 
		 * Also:  there are certain ribbons which are tied to more than one property.
		 * That is, a single Modifier is used with multiple property variables having different names.
		 * But at this time there are no init exceptions resulting from that.
		 * 
		 * Modifiers having property variable name different from modifier key can be listed by debug
		 * code in ObjectProperties::registerProperty()
		 * 
		 * WorldMediator:  this function is also called by WorldMediator, to prevent storing inits
		 * for properties that shouldn't have them.  You might think that would be adequate, but because
		 * a file could have been stored by a previous version, we check it here at init time also.
		 * @param property
		 * @return 
		 * 
		 */
		public function propertyNeedsInit(property:String, obj:AbstractObject):Boolean
		{
			switch (property) {
				case "rawRotation": // ensure init is created for the stored rotation ("rotation" is a computed value)
				case "x":
				case "y":
				case "width":
				case "height":
					return true;
				case "locationXYR":
				case "rotation":	// rawRotation gets the init, instead of "rotation" getting an init
				case "knobX":		// the slider "value" gets the init
				case "knobY":
				case "movedBy":
				case "dragging":
				case "calcTweenValue":
				case "percentElapsed":
				case "percentRemaining":
                case "column":
                case "activeColumn":
                case "currentPage":
                case "pathPercent":
					return false;
				case "messageCenter":
				case "xMessageCenter":
				case "yMessageCenter":
				case "messageCenterRepositioned":
				case "messageCenterRelative":
				case "allStateValues":
				case "portValue":
                case "port":
				case "logicRowName":
				case "stateValueExpressions":
				case "stateValueBound":
				case "stateValueName":
				case "stateValueType":
				case "stateValueTypeConstraints":
                case "guidePositions":
				case "currentTweenValues":
				case "allTweenValues":
				case "allLogicValues":
				case "allLogicFulfilled":
                case "branches":
				case "numColumns":
				case "ticking":
				case "path":
				case "viewBox":
				case "calcInputValues":
				case "calcInputDefaultNames":
				case "calcInputNames":
				case "calcInputTypes":
				case "calcInputConstraints":	
				case "calcOutputValues":
				case "calcOutputExpressions":
				case "calcOutputTypes":
				case "calcOutputConstraints":
				case "asset":  // since the Drawing ribbon is "replaceable"
				case "title": // get into initial values for gadget saving
				case "titleChangedByAuthor": // get into initial values for gadget saving
                case "toggleButtonGroup":
                case "toggleButtonValue":
                case "toggleButtonGroupValue":
                case "groupID":
				case "contentWidth":  // ribbon is read-only, but still needs init
				case "contentHeight":
				case "locked":
				case "messageCenterLocked":
				case "widthScale":
				case "heightScale":
                case "vars":
				case "controlsGlobalX":  // Arena
				case "controlsGlobalY":
                case "controlsDocked":
                    // support onRun properties
                case "resumeVisible":
                case "resumeCloaked":
                case "resumeTicking":
                case "resumeControlsVisible":
                case "resumePlaying": //Should I play when I return to an event with a stateTable
                case "started":
                case "pageNumber":
                case "hadChildren":
                case "interactive":
					return true;
				default:  // normal cases: the existence of a ribbon property indicates it gets init
					var prop:PropertyDescription = obj.modifiers[property];
					return prop != null && prop.readOnly == false;
			}
		}

		/* mouse events passed from the mediators. handle object-specific UI events
		by overriding these. */
		
		public function updatePosition(model:TaconiteModel, point:Point):void
		{
			var object:AbstractObject = getObject(model);
			var pos:PositionAndAngle = object.locationXYR;
			pos.x = point.x;
			pos.y = point.y;
			object.locationXYR = pos;  // setting this gives fewer model updates than setting x,y separately
		}		
						
		public function handleMouseOver(objectView:ITaconiteView, e:MouseEvent):void
		{
			var obj:AbstractObject = ObjectView(objectView).object;
			if (Application.running && obj.enabled) {
				if (_wiring.sendTrigger(obj, Modifiers.instance.fetch("rollOver"))) {
					logEvent("rollOver", obj);
				}
			}
		}
		
		public function handleMouseOut(objectView:ITaconiteView, e:MouseEvent):void
		{
			var obj:AbstractObject = ObjectView(objectView).object;
			if (Application.running && obj.enabled) {
				if (_wiring.sendTrigger(objectView.model.value, Modifiers.instance.fetch("rollOut"))) {
					logEvent("rollOut", obj);
				}
			}
		}
		
        /**
         * A mouse click passed in from the object drag mediator 
         * @param model the model of the view receiving the event
         * @param e
         * 
         */
        public function handleClick(objectView:ITaconiteView, e:MouseEvent):void
        {
			var view:ObjectView = objectView as ObjectView;
			var obj:AbstractObject = view.object;
			var logged:Boolean = false;
			if ((Application.running || view.selected) && obj.enabled) {
				while (obj && !(obj as World)) {
                   if (triggerable(obj,"click")) {
                        if (_wiring.sendTrigger(obj, Modifiers.instance.fetch("click"))) {
                            if (!logged) {
                                logEvent("click", obj);
                                logged = true;
                            }
                        }
                   }
					obj = obj.parent;
				}
			}
        }
		
		
        /**
         * A mouse down passed in from the object drag mediator
         * Disallow dragging while editing! 
         * @param model the model of the view receiving the event
         * @param e
         * @return whether or not we'll allow a drag
         * 
         */
        public function handleMouseDown(objectView:ITaconiteView, e:MouseEvent):Boolean
        {
			var view:ObjectView = objectView as ObjectView;
			var obj:AbstractObject = view.object;
			var logged:Boolean = false;
			if ((Application.running || view.selected) && obj.enabled) {
				while (obj && !(obj as World)) {
                    if (triggerable(obj,"clickStart")) {
                        if (_wiring.sendTrigger(obj, Modifiers.instance.fetch("clickStart"))) {
                            if (!logged) {
                                logEvent("mouseDown", obj);
                                logged = true;
                            }
                        }
                    }
					obj = obj.parent;
				}
			}

	    	return !obj.editing;
        }

        protected function triggerable(obj:AbstractObject,key:String):Boolean {
            // override this to prevent triggering while attempting to edit an object (e.g. in TextObjectController).
            return true;
        }

		protected function logEvent(str:String, obj:AbstractObject):void
		{
			ApplicationController.instance.authorController.log(str + obj.logStr);
		}
        
		/**
		 * A mouse move passed in from the object drag mediator 
		 * @param model the model of the view receiving the event
		 * @param e
		 * 
		 */
		public function handleMouseMove(objectView:ITaconiteView, e:MouseEvent):void
		{
		}
        
		/**
		 * A mouse up passed in from the object drag mediator 
		 * @param model the model of the view receiving the event
		 * @param e
		 * 
		 */
		public function handleMouseUp(objectView:ITaconiteView, e:MouseEvent):void
		{
			var view:ObjectView = objectView as ObjectView;
			var obj:AbstractObject = view.object;
			var logged:Boolean = false;
			if ((Application.running || view.selected) && obj.enabled) {
				while (obj && !(obj as World)) {
					trace("ObjectController CLICK RELEASE: "+obj);
                    if (triggerable(obj,"clickRelease")) {
                        if (_wiring.sendTrigger(obj, Modifiers.instance.fetch("clickRelease"))) {
                            if (!logged) {
                                logEvent("mouseUp", obj);
                                logged = true;
                            }
                        }
                    }
					obj = obj.parent;
				}
			}
		}

		public function updateMouseOver(model:TaconiteModel, over:Boolean):void
		{
		}
		
		public function arrangeObject(object:AbstractObject, action:String):void
		{
			trace("ObjectController::arrangeObject: "+action, object);
            var container:AbstractContainer = object.parent;
            var index:int = container.objects.getItemIndex(object);

			switch (action) {
				case "bringFront":
					container.setChildIndexWithinLayer(object, container.objects.length-1);
					break;
				case "bringForward":
                    container.setChildIndexWithinLayer(object, index+1);
					break;
				case "sendBackward":
                    container.setChildIndexWithinLayer(object, index-1);
					break;
                case "sendBack":
                    container.setChildIndexWithinLayer(object, 0);
                    break;
			}
            container.setChildrenZPositions(!Application.running); // ensure other objects zIndex is updated
		}
		
		public function findAnchorForObj(obj:AbstractObject, modifier:IModifier, index:uint=0):WireAnchor
		{
			if(modifier == null) {
				return null;
			}
			var path:Array = modifier.key.split("/");
			if(path.length > 1) {
				var parent:WireAnchor = findAnchorForObj(obj, new InletDescription(path[0]));
				var child:WireAnchor;
				for each(child in parent.childAnchors) {
					if(child.modifierDescription.key == path[1]) {
						return child;
					}
				}
				return null;
			}
			var entry:Object;
			var i:int = 0;
			if(modifier.indexed) {
				for each (entry in obj.anchors) {
					var anchors:IList = entry as IList;
					if(anchors != null && anchors.length <= index && anchors.length > 0 && WireAnchor(anchors[0]).modifierDescription == modifier) {
						for(i = anchors.length; i <= index; i++) {
							if(modifier as OutletDescription) {
								anchors.addItemAt(OutletDescription(modifier).createAnchor(obj, i), i);
							} else if(modifier as InletDescription) {
								anchors.addItemAt(InletDescription(modifier).createAnchor(obj, i), i);
							}
						}
					}
					if(anchors != null && anchors.length > index && WireAnchor(anchors[index]).modifierDescription == modifier) {
						return WireAnchor(anchors[index]);
					}
				}
				anchors = new ArrayCollection();
				obj.anchors[modifier.key] = anchors;
				if(modifier as OutletDescription) {
					anchors.addItemAt(OutletDescription(modifier).createAnchor(obj, 0), 0);
				} else if(modifier as InletDescription) {
					anchors.addItemAt(InletDescription(modifier).createAnchor(obj, 0), 0);
				}
			} else {
				var anchor:WireAnchor;
				for each (entry in obj.anchors) {
					anchor = entry as WireAnchor;
					var extAnchor:ExternalSmartWireAnchor = entry as ExternalSmartWireAnchor;
					var intAnchor:InternalSmartWireAnchor = entry as InternalSmartWireAnchor;
					if (anchor) {
						if (anchor.modifierDescription.key == modifier.key)
							return anchor;
						if (extAnchor) {
							if (extAnchor.modifierDescription.key == modifier.key)
								return extAnchor;
							else if (extAnchor.other.modifierDescription.key == modifier.key)
								return extAnchor.other;
						}
						if (intAnchor != null) {
							if (intAnchor.modifierDescription.key == modifier.key)
								return intAnchor;
							else if (intAnchor.other.modifierDescription.key == modifier.key)
								return intAnchor.other;
						}
					}
				}
			}
			return null;
		}


        /**
         * Let the object prepare itself to be deleted.  Does not propagate to descendants.
         * @param object
         */
		
		public function warnBeforeDelete(object:AbstractObject):void
		{		
		}

        /**
         * The author has dropped an asset on a ribbon.
         * This function provides Undo support for the change.
         * Its overridden by ToggleButtonController and others.
         * @param obj
         * @param property
         * @param asset
         */
        public function assetDroppedOnRibbon(obj:AbstractObject, anchor:WireAnchor, asset:IReplaceable):void
        {
            var oldAsset:IReplaceable = anchor.value as IReplaceable;
            anchor.value = asset;
            var title:String = "Set " + anchor.label + " on " + obj.title;
            ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(obj, anchor.path, oldAsset));  // anchor.path supports dropping inside setter
        }

        public function assetDroppedOnObject(obj:AbstractObject, asset:IReplaceable):void
        {
            // override by ExternalObjectController
        }

        public function assetReplaceAll(obj:AbstractObject, oldAsset:IReplaceable, newAsset:IReplaceable):void
        {
            for each(var key:* in obj.anchors) {
                var anchor:WireAnchor = key as WireAnchor;
                if (anchor) {
                    var copter:HelicopterWireAnchor = anchor as HelicopterWireAnchor;  // setter or trigger
                    if (copter) {
                        if (copter.assetID == oldAsset.id) {
                            var valueRibbon:WireAnchor = copter.childAnchors[0];
                            assetDroppedOnRibbon(obj, valueRibbon, newAsset);
                        }
                    } else if (anchor.modifierDescription is PropertyDescription) {
                        var current:Asset = anchor.value as Asset;
                        var initID:String = null;
                        if (anchor.hostProperty in obj.initialValues) {
                            initID = Asset.idFromJson(obj.initialValues[anchor.hostProperty]);
                        }
                        if ((current && current.id == oldAsset.id) || (initID == oldAsset.id)) {
                            assetDroppedOnRibbon(obj, anchor, newAsset);
                        }
                    }
                }
            }
        }

        public function assetReplaceAllWhole(obj:AbstractObject, oldAsset:IReplaceable, newAsset:IReplaceable):void
        {
            // override by SmartObjectController & EventFlowController
        }

	}
}
