package com.alleni.author.controller
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.WireController;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.utils.Dictionary;

	/**
	 * Controller for tweens on child-ribbons and in tables.
	 * This is not a generic tween controller ... it is specific to anchors with tweening. 
	 * 
	 */
	public class TweenController
	{
		private var _type:String;
		private var _startValue:*;
		private var _endValue:*;
		private var _finalFill:GraphicFill;

		public static var tweenCycles:int = 0;

				
		
		public function TweenController(start:*, end:*, type:String):void
		{
			_type = type;
			
			switch (type) {
				case Modifiers.COLOR_TYPE:
					trace(" color tween");
					_startValue = uint(start);
					_endValue = uint(end);
					break;
				case Modifiers.POSITION_TYPE:
					_startValue = Modifiers.convertValue(Modifiers.POSITION_TYPE, start);
					_endValue = Modifiers.convertValue(Modifiers.POSITION_TYPE, end);
					break;
				case Modifiers.FILL_TYPE:
					if (start is GraphicFill && end is GraphicFill) {
						_startValue = GraphicFill(start).clone();
						_endValue = GraphicFill(end).clone();
						_finalFill = GraphicFill(end).clone();
						GraphicFill.setupTween(_startValue, _endValue);  // modify _startValue & _endValue to be compatible
					}
					break;
				default:
					_startValue = start;
					_endValue = end;
					break;
			}
//			trace("TweenController: _start="+_startValue, "_end="+_endValue);
		}

		public static function cannotTween(type:String):Boolean {
			return [Modifiers.STRING_TYPE, Modifiers.BOOLEAN_TYPE, Modifiers.OBJECT_TYPE, Modifiers.ENUMERATED_TYPE,
				Modifiers.FORMATTED_TEXT_TYPE, Modifiers.IMAGE_ASSET_TYPE, Modifiers.AUDIO_ASSET_TYPE, Modifiers.VIDEO_ASSET_TYPE,
				Modifiers.TEXT_ASSET_TYPE, Modifiers.FONT_ASSET_TYPE, Modifiers.FONT_ENUMERATED_TYPE, Modifiers.POINT_TYPE,
                Modifiers.VERTICAL_ALIGN_TYPE, Modifiers.HORIZONTAL_ALIGN_TYPE].indexOf(type) >= 0;
		}

		
		public function computeTween(frac:Number):*
		{
			++tweenCycles;

			if(cannotTween(_type)) {
				var result:* = (frac < 1.0) ? _startValue : _endValue;
				trace("computeTween: jump to",result);
				return result;
			}
			
			switch (_type) {
				case Modifiers.COLOR_TYPE:
					return computeColorTween(_startValue as uint, _endValue as uint, frac);
				case Modifiers.POSITION_TYPE:
					return computePositionAndAngle(_startValue as PositionAndAngle, _endValue as PositionAndAngle, frac);
				case Modifiers.FILL_TYPE:
					if (frac >= 1.0)
						return _finalFill;
					return GraphicFill.computeTween(_startValue as GraphicFill, _endValue as GraphicFill, frac);
				case Modifiers.POINT_TYPE:
					var a:Point = Point(_startValue);
					var b:Point = Point(_endValue);
					var diff:Point = b.subtract(a);
					return a.add(new Point(frac*diff.x,frac*diff.y));
				default:
					var start:Number = Number(_startValue);
					var end:Number = Number(_endValue);
					return start + frac * (end - start)
			}
		}
		
				
		public static function computeColorTween(start:uint, end:uint, frac:Number):uint
		{
			var redStart:uint = (start >> 16) & 0xff;
			var greenStart:uint = (start >> 8) & 0xff;
			var blueStart:uint = start & 0xff;
			
			var redEnd:uint = (end >> 16) & 0xff;
			var greenEnd:uint = (end >> 8) & 0xff;
			var blueEnd:uint = end & 0xff;
			
			var red:uint = redStart + frac * (redEnd - redStart);
			var green:uint = greenStart + frac * (greenEnd - greenStart);
			var blue:uint = blueStart + frac * (blueEnd - blueStart);
			
			if (red > 255)	red = 255;
			if (blue > 255) blue = 255;
			if (green > 255) green = 255;
			
			var result:uint = (red << 16) | (green << 8) | blue;
			
//			trace("color: frac="+int(frac*100), "start="+start.toString(16), " end="+end.toString(16), " result="+result.toString(16));
			return result;
		}
		
		private function computePositionAndAngle(start:PositionAndAngle, end:PositionAndAngle, frac:Number):PositionAndAngle
		{
			// the "location" property is being tweened
			var pos:PositionAndAngle = new PositionAndAngle();
			pos.x = start.x + frac * (end.x - start.x);
			pos.y = start.y + frac * (end.y - start.y);
			pos.angle = start.angle + frac * (end.angle - start.angle);
			return pos;
		}
		
		
		
		public static function getStartValue(obj:Object, outputProp:String, outputIndex:int):*
		{
			// because of target obj location potentially being adjusting while running ... 
			// we don't know whether calcTweenValue is current
			// ... so get starting value from the target object (from the first target object we happen to see)
			var anchor:WireAnchor;
			if (outputIndex == -1) {
				anchor = obj.anchors[outputProp] as WireAnchor;
			} else {
				anchor = obj.anchors[outputProp][outputIndex] as WireAnchor;
			}
			var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
			for each (var wire:Wire in wires) {
				var value:* = getStartValueFromWire(wire, anchor);
				trace("getStartValueFromWire="+value);
				if (value != null)
					return value;
			}
			return null;
		}
		
		
		private static function getStartValueFromWire(wire:Wire, ownAnchor:WireAnchor):*
		{
			var anchor:WireAnchor;
			
			if (wire.masterAnchor != ownAnchor) {
				anchor = wire.masterAnchor;
			} else if (wire.slaveAnchor != ownAnchor) {
				anchor = wire.slaveAnchor;
			}
			
			if (anchor) {
				var target:AbstractObject = anchor.hostObject;
				if (target != null) {
					var targetProp:String = anchor.hostProperty;
					var index:int;
					if (anchor.modifierDescription.indexed)
						return target[targetProp][anchor.hostPropertyIndex];
					else
						return target[targetProp];
				}
			}
			return null;
		}
	}
}