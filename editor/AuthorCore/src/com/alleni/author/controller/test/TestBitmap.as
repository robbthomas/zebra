package com.alleni.author.controller.test
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;

	public class TestBitmap
	{
		
		public static function bitmapExactMatch(one:BitmapData, two:BitmapData):Boolean
		{
			trace("bitmapExactMatch");
			var diff:BitmapData = bitmapDiff(one, two);
			if (diff == null)
				return false;
			return allPixelsBlack(diff);
		}
		
		public static function bitmapTolerantMatch(one:BitmapData, two:BitmapData, tolerance:int):Boolean
		{
			trace("bitmapTolerantMatch factor="+tolerance);
			var diff:BitmapData = bitmapDiff(one, two);
			if (diff == null) 
				return false;
			
			return mostlyBlack(diff, tolerance);
		}
		
		public static function bitmapDeviation(one:BitmapData, two:BitmapData):int
		{
			var diff:BitmapData = bitmapDiff(one, two);
			if (allPixelsBlack(diff))
				return 0;
			
			var factor:int;
			for (factor=1; factor < 10; factor++) {
				if (mostlyBlack(diff, factor))
					return factor-1;
			}
			return factor;
		}
		
		private static function allPixelsBlack(bitmap:BitmapData):Boolean
		{
			var rect:Rectangle = new Rectangle(0,0, bitmap.width, bitmap.height);
			var bytes:ByteArray = bitmap.getPixels(rect);
			bytes.position = 0;
			while (bytes.bytesAvailable > 0) {
				var word:int = bytes.readInt();
				var color:uint = word & 0xffffff;
				if (color != 0) {
					trace("  color=#"+color.toString(16));
					return false;
				}
			}
			return true;
		}
		
		
		public static function bitmapDiff(one:BitmapData, two:BitmapData):BitmapData
		{
			trace("\nbitmapDiff");
			if (one.width != two.width || one.height != two.height) {
				trace("  different size bitmaps");
				return null;
			}
			
			var rect:Rectangle = new Rectangle(0,0, one.width, one.height);
			var oneBytes:ByteArray = one.getPixels(rect);
			var twoBytes:ByteArray = two.getPixels(rect);
			var comboBytes:ByteArray = new ByteArray();
			oneBytes.position = 0;
			twoBytes.position = 0;
			while (oneBytes.bytesAvailable > 0) {
				var oneWord:uint = oneBytes.readUnsignedInt();
				var twoWord:uint = twoBytes.readUnsignedInt();
				var word:uint = ((oneWord & 0xf0f0f0) == (twoWord & 0xf0f0f0)) ? 0xff000000 : 0xffffffff;
				comboBytes.writeUnsignedInt(word);
//				if (word & 0xffffff)
//					trace("  ",oneWord.toString(16), twoWord.toString(16));
			}
			comboBytes.position = 0;
			var combo:BitmapData = new BitmapData(rect.width, rect.height, false);
			combo.setPixels(rect, comboBytes);
			
			return combo;
		}
		
		
		public static function grabBitmap(view:DisplayObject, rect:Rectangle):BitmapData
		{
			var data:BitmapData = new BitmapData(rect.width, rect.height, false);
			var matrix:Matrix = new Matrix();
			matrix.tx = -rect.x;
			matrix.ty = -rect.y;
			data.draw(view, matrix);
			return data;
		}
		
		
		private static function mostlyBlack(bitmap:BitmapData, tolerance:int):Boolean
		{
			var rect:Rectangle = new Rectangle(0,0, bitmap.width, bitmap.height);
			var xLimit:int = rect.width - tolerance;
			var yLimit:int = rect.height - tolerance;
			for (var y:int=0; y < yLimit; y++) {
				for (var x:int=0; x < xLimit; x++) {
					var color:uint = bitmap.getPixel(x,y) & 0xffffff;
					if (rectAllWhite(bitmap, new Rectangle(x,y, tolerance, tolerance)))
						return false;
				}
			}
			return true;
		}
		
		
		private static function rectAllWhite(bitmap:BitmapData, rect:Rectangle):Boolean
		{
			var xLimit:int = rect.right;
			var yLimit:int = rect.bottom;
			for (var y:int=rect.top; y < yLimit; y++) {
				for (var x:int=rect.left; x < xLimit; x++) {
					var color:uint = bitmap.getPixel(x,y) & 0xffffff;
					if (color != 0xffffff) {
						return false;  // not all white
					}
				}
			}
			return true;
		}

	}
}