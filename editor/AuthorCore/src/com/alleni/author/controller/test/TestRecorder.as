package com.alleni.author.controller.test
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.ReplacementController;
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.Objects;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.action.ActionTreeEvent;
	import com.alleni.author.event.AssetEvent;
	import com.alleni.author.event.TestEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.Checkbox;
	import com.alleni.author.model.objects.PushButton;
	import com.alleni.author.model.objects.RadioButton;
	import com.alleni.author.model.objects.Slider;
	import com.alleni.author.model.objects.TextObject;
	import com.alleni.author.model.test.TestLog;
	import com.alleni.author.model.test.TestPanel;
	import com.alleni.author.model.test.TestScript;
	import com.alleni.author.model.test.TestStep;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.service.assets.AssetSaveOperation;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldView;
	import com.alleni.author.view.objects.OvalView;
	import com.alleni.author.view.objects.PushButtonView;
	import com.alleni.author.view.test.TestPanelView;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.taconite.controller.TaconiteController;
	import com.alleni.taconite.factory.SerializerImplementation;
	import com.alleni.taconite.lang.Geometry;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.persistence.json.JSON;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	
	import mx.core.UIComponent;
	import mx.graphics.codec.PNGEncoder;

	public class TestRecorder
	{
		private var _model:TestScript;
		private var _log:TestLog;
		private var _controller:TestPanelController;
		private var _worldView:WorldView;
		private var _clickTitle:String;
		private var _clickGlobal:Point;
		private var _clickObj:AbstractObject;
		private var _dragPoints:Array;
		private var _promptedListener:Boolean;
		private var _grabStart:Point;
		private var _grabRect:Rectangle;
		private var _grabMarquee:Sprite;
		private var _verifiesToUpdate:Array;
        private var _delayRequest:int;  // delay amount in enter-frame cycles
        private var _delayCounter:int;  // actual amount delayed since last activity

		
		public function TestRecorder(model:TestScript, log:TestLog, worldView:WorldView, controller:TestPanelController)
		{
			trace("TestRecorder starting");
			_model = model;
			_log = log;
			_controller = controller;
			_worldView = worldView;
			
			_worldView.stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener, true);
		}
		
		public function stopRecording():void
		{
			_worldView.stage.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener, true);
			endPromptedListening();
		}
		
		
		private function mouseDownListener(event:MouseEvent):void
		{
			var global:Point = new Point(event.stageX, event.stageY);
			var view:ObjectView = _worldView.findViewUnderPoint(global);
			if (view && view!=_worldView && !_promptedListener && !_controller.hitTestPanel(global)) {
				_model.selectAll(false);  // deselect existing script steps
				_worldView.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener, true);
				_worldView.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener, true);
				_clickGlobal = global;
				_clickTitle = view.object.title;
				_clickObj = view.object;
				_dragPoints = null;
			}
		}
		
		private function mouseMoveListener(event:MouseEvent):void
		{
			if (!(_clickObj is Slider)) {
				var global:Point = new Point(event.stageX, event.stageY);
				var breakaway:Boolean = (3 < Geometry.distanceBetweenPoints(global.x,global.y, _clickGlobal.x,_clickGlobal.y));
				if (breakaway || _dragPoints != null)
					addDragPoint(global);
			}
		}
		
		private function addDragPoint(global:Point, force:Boolean=false):void
		{
			var local:Point = _worldView.globalToLocal(global);
			if (_dragPoints == null) {
				_dragPoints = [local];
			} else {
				var prev:Point = _dragPoints[_dragPoints.length-1];
				if (force || local.subtract(prev).length > 50.0)
					_dragPoints.push(local);
			}
		}
		
		private function mouseUpListener(event:MouseEvent):void
		{
			_worldView.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener, true);
			_worldView.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpListener, true);
			
			var global:Point = new Point(event.stageX, event.stageY);
			if (_dragPoints == null) {
				// click
				if (_clickObj is PushButton || _clickObj is Checkbox || _clickObj is RadioButton)
					addButtonStep(_clickObj);
				else if (_clickObj is Slider)
					addSliderStep(_clickObj);
				else
					addClickStep(_clickGlobal, _clickTitle);
			} else {  // drag
				addDragPoint(global, true);  // true=force this point to be added
				addDragStep(_clickGlobal, _dragPoints, _clickTitle);
			}
		}
		
		private function addClickStep(global:Point, title:String):void
		{
			var local:Point = _worldView.globalToLocal(global);
			var step:TestStep = new TestStep();
			step.type = TestScript.CLICK_STEP;
			step.label = _controller.formatPoint(local);;
			step.selected = true;
			step.point = local;
			_model.steps.addItem(step);
			trace("click step added",local);
			dittoPreviousVerifies(step);
		}
		
		
		private function addButtonStep(obj:AbstractObject):void
		{
			var step:TestStep = new TestStep();
			step.type = TestScript.BUTTON_STEP;
			step.label = obj.title;  // either a Pushbutton or a Checkbox
			step.objRef = obj;
			step.selected = true;
			_model.steps.addItem(step);
			trace("button step added");
			dittoPreviousVerifies(step);
		}
		
		private function addSliderStep(obj:AbstractObject):void
		{
			var slider:Slider = obj as Slider;
			var step:TestStep = new TestStep();
			step.type = TestScript.SLIDER_STEP;
			step.label = obj.title; 
			step.objRef = obj;
			step.value = slider.sliderValue.toString();
			step.selected = true;
			_model.steps.addItem(step);
			trace("slider step added");
			dittoPreviousVerifies(step);
		}
		
		private function addDragStep(global:Point, dragPoints:Array, title:String):void
		{
			var local:Point = _worldView.globalToLocal(global);
			var step:TestStep = new TestStep();
			step.type = TestScript.DRAG_STEP;
			step.label = _controller.formatPoint(local);;
			step.selected = true;
			step.point = local;
			step.dragPoints = dragPoints;
			_model.steps.addItem(step);
			trace("click drag added");
			dittoPreviousVerifies(step);
		}

								
		public function startListeningForTextClick():void
		{
			_worldView.stage.addEventListener(MouseEvent.MOUSE_DOWN, textClickListener,true);
			_controller.promptWithCancel("Click a text object to add a verify step");
			_promptedListener = true;
		}
		
		public function stopListeningForTextClick():void
		{
			_worldView.stage.removeEventListener(MouseEvent.MOUSE_DOWN, textClickListener,true);
			_controller.removePrompt();
			_promptedListener = false;
		}

		
		private function textClickListener(event:MouseEvent):void
		{
			trace("Text CLICK");
			event.stopImmediatePropagation();
			var view:ObjectView = _worldView.findViewUnderPoint(new Point(event.stageX, event.stageY));
			if (view.object is TextObject) {
				stopListeningForTextClick();
				addTextStep(view.object as TextObject);
			}
		}
		
		private function addTextStep(textObj:TextObject):void
		{
			var step:TestStep = new TestStep();
			step.type = TestScript.TEXT_STEP;
			step.label = textObj.title;
			step.objRef = textObj;
			step.selected = true;
			setCorrectValue(step, _worldView);
			_model.steps.addItem(step);
			_controller.scrollTo(step);
		}
		
		
		public function startListeningForPositionClick():void
		{
			_worldView.stage.addEventListener(MouseEvent.MOUSE_DOWN, positionClickListener,true);
			_controller.promptWithCancel("Click any object to verify its position");
			_promptedListener = true;
		}
		
		public function stopListeningForPositionClick():void
		{
			_worldView.stage.removeEventListener(MouseEvent.MOUSE_DOWN, positionClickListener,true);
			_controller.removePrompt();
			_promptedListener = false;
		}
		
		private function positionClickListener(event:MouseEvent):void
		{
			trace("Position CLICK");
			event.stopImmediatePropagation();
			var view:ObjectView = _worldView.findViewUnderPoint(new Point(event.stageX, event.stageY));
			if (view) {
				stopListeningForPositionClick();
				addPositionStep(view.object);
			}
		}
		
		private function addPositionStep(obj:AbstractObject):void
		{
			var step:TestStep = new TestStep();
			step.type = TestScript.POSITION_STEP;
			step.label = obj.title;
			step.selected = true;
			step.objRef = obj;
			setCorrectValue(step, _worldView);
			_model.steps.addItem(step);
			_controller.scrollTo(step);
		}
		
		public function startListeningForRibbonClick():void
		{
			_controller.pause();  // allow showing message centers
			_worldView.stage.addEventListener(MouseEvent.MOUSE_DOWN, ribbonClickListener,true);
			_controller.promptWithCancel("Click any object to verify its ribbon");
			_promptedListener = true;
		}
		
		public function stopListeningForRibbonClick():void
		{
			_worldView.stage.removeEventListener(MouseEvent.MOUSE_DOWN, ribbonClickListener,true);
			_controller.removePrompt();
			_promptedListener = false;
			_controller.resume();
		}
		
		private function ribbonClickListener(event:MouseEvent):void
		{
			trace("Ribbon CLICK");
			var ribbon:RibbonView = ReplacementController.instance.ribbonUnderPoint(new Point(event.stageX, event.stageY));
			if (ribbon && ribbon.ribbon.modifierDescription is PropertyDescription && !(ribbon.ribbon.value is Array)) {  // Array such as CollidingWith not allowed yet
				stopListeningForRibbonClick();
				event.stopImmediatePropagation();
				addRibbonStep(ribbon.model.value as WireAnchor);
			}
		}
		
		private function addRibbonStep(anchor:WireAnchor):void
		{
			var obj:AbstractObject = anchor.hostObject;
			
			var step:TestStep = new TestStep();
			step.type = TestScript.RIBBON_STEP;
			step.label = obj.title + " / " + anchor.modifierDescription.key;
			step.selected = true;
			step.objRef = obj;
			step.anchorRef = anchor;
			setCorrectValue(step, _worldView);
			_model.steps.addItem(step);
			_controller.scrollTo(step);
		}
		
		private function get marqueeParent():Sprite
		{
			return EditorUI.instance.presentationContainer.feedbackControlsView;
		}
		
		public function startListeningForScreenGrab():void
		{
			_worldView.stage.addEventListener(MouseEvent.MOUSE_DOWN, grabMouseDown, true);
			_controller.promptWithCancel("Click screen and drag to capture a bitmap");
			_promptedListener = true;
		}
		
		public function stopListeningForScreenGrab():void
		{
			_worldView.stage.removeEventListener(MouseEvent.MOUSE_DOWN, grabMouseDown, true);
			_controller.removePrompt();
			_promptedListener = false;
		}
		
		public function endPromptedListening():void
		{
			stopListeningForTextClick();
			stopListeningForPositionClick();
			stopListeningForScreenGrab();
			stopListeningForRibbonClick();
			_promptedListener = false;
		}
		
		private function grabMouseDown(event:MouseEvent):void
		{
			event.stopImmediatePropagation();  // prevent selection marquee from messing up the screen grab
			_grabStart = new Point(event.stageX, event.stageY);
			_grabRect = new Rectangle();
			_grabMarquee = new Sprite();
			marqueeParent.addChild(_grabMarquee);
			_worldView.stage.addEventListener(MouseEvent.MOUSE_MOVE, grabMouseMove);
			_worldView.stage.addEventListener(MouseEvent.MOUSE_UP, grabMouseUp);
		}

		private function grabMouseMove(event:MouseEvent):void
		{
			_grabRect.topLeft = _grabStart;
			_grabRect.width = 1; _grabRect.height = 1;
			var thisRect:Rectangle = new Rectangle(event.stageX, event.stageY, 1,1);
			_grabRect = _grabRect.union(thisRect);
			
			var rect:Rectangle = TestPlayer.transformRect(_grabRect, _worldView.stage, marqueeParent);
			trace("marq rect="+rect);
			_grabMarquee.graphics.clear();
			_grabMarquee.graphics.lineStyle(2);
			_grabMarquee.graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
		}

		private function grabMouseUp(event:MouseEvent):void
		{
			marqueeParent.removeChild(_grabMarquee);
			_worldView.stage.removeEventListener(MouseEvent.MOUSE_DOWN, grabMouseDown, true);
			_worldView.stage.removeEventListener(MouseEvent.MOUSE_MOVE, grabMouseMove);
			_worldView.stage.removeEventListener(MouseEvent.MOUSE_UP, grabMouseUp);

			if (_grabRect.width > 1 && _grabRect.height > 1) {
				var rect:Rectangle = TestPlayer.transformRect(_grabRect, _worldView.stage, _worldView);
				createBitmapStepFromScreen(rect); 
			}
			stopListeningForScreenGrab();
		}
		
		private function createBitmapStepFromScreen(input:Rectangle):void
		{
			_model.selectAll(false);
			var rect:Rectangle = new Rectangle(Math.round(input.x), Math.round(input.y), Math.round(input.width), Math.round(input.height));			
			var step:TestStep = new TestStep();
			step.type = TestScript.BITMAP_STEP;
			step.label = "[" + rect.x + "," + rect.y + "," + rect.width + "," + rect.height + "]";
			step.rect = rect;
			step.selected = true;
			setCorrectValue(step, _worldView);
			_model.steps.addItem(step);
			_controller.scrollTo(step);
		}
		
		
		private function dittoPreviousVerifies(newStep:TestStep):void
		{
			trace("ditto="+_controller.panelModel.ditto);
			_controller.scrollTo(newStep);
			if (_controller.panelModel.ditto == false)
				return;
			
			// find the range of steps between the previous stimulus (button-click) and this one
			// (or if no previous stimulus, grab the verifies from the top)
			var newStepNum:int = _model.steps.getItemIndex(newStep);
			var prevStepNum:int = -1;
			for (var n:int = newStepNum-1; n >= 0; n--) {
				var step:TestStep = _model.steps[n];
				if (step.isStimulus) {
					prevStepNum = n;
					break;
				}
			}
			
			// copy that range and put it after the new step
			var dest:int = newStepNum+1;
			_verifiesToUpdate = [];
			for (var k:int = prevStepNum+1; k < newStepNum; k++) {
				var verify:TestStep = _model.steps[k];
                if (TestPlayer.onScreen(verify)) {
                    verify = verify.clone();
                    verify.selected = true;
                    _verifiesToUpdate.push(verify);  // remember to update the correct-value, after allowing button click to propagate on wires
                    _model.steps.addItemAt(verify, dest++);
                    _controller.scrollTo(verify);
                }
			}
			_worldView.stage.addEventListener(Event.ENTER_FRAME, updateVerifiesListener);
			_worldView.stage.invalidate();
            _delayRequest = 5; // give the wires time to propagate (and arena layout)
            _delayCounter = 0;
		}
		
		private function updateVerifiesListener(event:Event):void
		{
            // similar to code in TestPlayer:enterFrameListener

            if (TestPanelController.instance.senseActivity) {
                _delayCounter = 0;  // start over, since we currently have activity
                _worldView.stage.invalidate();  // ensure that (for example) arena layout gets a Render event
            } else if (++_delayCounter >= _delayRequest) {
				_worldView.stage.removeEventListener(Event.ENTER_FRAME, updateVerifiesListener);
				for each (var step:TestStep in _verifiesToUpdate) {
					setCorrectValue(step, _worldView);
				}
				_verifiesToUpdate = null;
			} else {
				_worldView.stage.invalidate();
			}
		}
		
		public static function setCorrectValue(step:TestStep, worldView:WorldView):void
		{
			var controller:TestPanelController = TestPanelController.instance;
			var obj:AbstractObject;
			switch (step.type) {
				case TestScript.BITMAP_STEP:
					controller.feedback.clearFeedback();
					var data:BitmapData = TestBitmap.grabBitmap(worldView, step.rect);
					step.value = "";
					saveBitmap(step, data);
					break;
				case TestScript.TEXT_STEP:
					obj = step.objRef;
					if (obj is TextObject) {
						step.value = TextObject(obj).output.toString();
					}
					break;
				case TestScript.POSITION_STEP:
					obj = step.objRef;
					if (obj) {
						step.point = TestPlayer.getObjPosition(obj, worldView);
						step.value = (step.point) ? controller.formatPoint(step.point) : "offScreen";
						trace("setCorrectValue: step="+step.label, "obj="+obj.title, "obj.y="+obj.y, "point="+step.point);
					}
					break;
				case TestScript.RIBBON_STEP:
					obj = step.objRef;
					if (obj) {
						if (step.anchorRef) {
							var val:* = step.anchorRef.value;
							step.ribbonValue = val;
							step.value = (val == null) ? "null" : val.toString();
						}
					}
					break;
			}
		}
		
		private static function saveBitmap(step:TestStep, bitmap:BitmapData):void
		{
			trace("saveBitmap data="+bitmap);
			if (step.asset && step.asset.content) {
				var existing:Bitmap = step.asset.content as Bitmap;
				if (TestBitmap.bitmapExactMatch(existing.bitmapData, bitmap)) {
					trace("  keeping same bitmap");
					TestPanelController.instance.noteSizeInStep(step);
					return;  //  its identical image ... no need to replace it
				}
			}
			
			var rect:Rectangle = new Rectangle(0,0, bitmap.width, bitmap.height);
			
			var pngEncoder:PNGEncoder = new PNGEncoder();
			var bytes:ByteArray = pngEncoder.encode(bitmap);

			var operation:AssetSaveOperation = new AssetSaveOperation(null, TestStep.BITMAP_NAME, bytes);
			operation.addEventListener(Event.COMPLETE, function(e:AssetEvent):void{
					handleBitmapSaveComplete(e, step, bitmap);} );
			operation.execute();
			trace("  creating new bitmap: len="+bytes.length, rect);
		}
		
		private static function handleBitmapSaveComplete(e:AssetEvent, step:TestStep, bitmap:BitmapData):void
		{
			var asset:Asset = e.asset;
			asset.upToDate = true;
			asset.content = new Bitmap(bitmap);
			step.asset = asset;
			step.assetID = asset.assetID; // is this necessary to cache both the asset and its id? Looks like all you need is the asset, which can get you an ID --PJK
			TestPanelController.instance.noteSizeInStep(step);
			trace("handleBitmapSaveComplete asset="+asset);
		}
	}
}
