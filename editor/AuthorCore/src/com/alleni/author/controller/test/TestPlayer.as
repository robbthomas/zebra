package com.alleni.author.controller.test
{
import com.alleni.author.application.ApplicationUI;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.Checkbox;
import com.alleni.author.model.objects.Slider;
import com.alleni.author.model.objects.TextObject;
import com.alleni.author.model.test.TestLog;
import com.alleni.author.model.test.TestPanel;
import com.alleni.author.model.test.TestScript;
import com.alleni.author.model.test.TestStep;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.WorldView;
import com.alleni.author.view.objects.SliderView;
import com.alleni.author.view.test.TestFeedback;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.display.Stage;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;

public class TestPlayer
	{
		private var _script:TestScript;
		private var _panel:TestPanel;
		private var _log:TestLog;
		private var _controller:TestPanelController;
        private var _delayRequest:int;  // delay amount in enter-frame cycles
        private var _delayCounter:int;  // actual amount delayed since last activity
		private var _nextStep:int;
		private var _nextFunction:Function;
		private var _completionFunction:Function;
		private var _allPassed:Boolean;
		private var _worldView:WorldView;
		private var _dragPoints:Array;
		private var _nextPoint:int;
		
		private var _container:WorldContainer = WorldContainer(Application.instance.viewContext);
		
		public function TestPlayer(model:TestPanel, log:TestLog, completionCallback:Function, controller:TestPanelController)
		{
			_script = model.script;
			_panel = model;
			_log = log;
			_controller = controller;
			_worldView = _container.worldView;
			trace("play");
            _container.addEventListener(Event.ENTER_FRAME, enterFrameListener);
			_nextStep = 0;
			_allPassed = true;
            _script.status = TestScript.PASS;   // remains in this state till end of script, unless there is an exception or an invalid step
			schedule(doStepAndAdvance,3);  // extra delay after reset project
			_completionFunction = completionCallback;
		}
		
		private function get feedback():TestFeedback
		{
			// Note: this value changes when project finishes loading, so don't cache it
			return _controller.feedback;
		}
		
		public function stopPlaying():void
		{
			trace("stopPlaying");
			_container.removeEventListener(Event.ENTER_FRAME, enterFrameListener);
			_completionFunction(_script);
			feedback.clearFeedback();
		}
		
		private function schedule(func:Function, delay:int=0):void
		{
			if (_nextFunction == null) {
				_nextFunction = func;
				_delayRequest = (delay==0) ? Number(_script.delay) : delay;
                _delayCounter = 0;
				stage.invalidate();  // ensure that (for example) arena layout gets a Render event
			}
		}

        private function enterFrameListener(event:Event):void
        {
            // similar to TestRecorder:updateVerifiesListener()

            if (TestPanelController.instance.senseActivity) {
                _delayCounter = 0;  // start over, since we currently have activity
                stage.invalidate();  // ensure that (for example) arena layout gets a Render event
            } else if (++_delayCounter > _delayRequest) {
                var func:Function = _nextFunction;
                _nextFunction = null;
                func();
            }
        }

		private function doStepAndAdvance():void
		{
			if (_nextStep < _script.steps.length && !_log.crashed && _script.status == TestScript.PASS) {
				var step:TestStep = _script.steps[_nextStep];
				if (_panel.autoFix)
					TestRecorder.setCorrectValue(step, _worldView);
				doStep(step);
//				doStepSafely(step);
				advance();
			} else {  // reached the end, or crashed  (exception will have already set status)
                if (_script.status == TestScript.PASS && !_allPassed) {
                    _script.status = TestScript.FAIL;
                }
				stopPlaying();
			}
		}
		
//		private function doStepSafely(step:TestStep):void
//		{
//			try {
//				doStep(step);
//			} catch (e:Error) {
//                _script.status = TestScript.EXCEPTION;
//				_controller.logException(e.toString());
//			}
//		}
	
		
		private function doStep(step:TestStep):void
		{
			switch (step.type) {
				case TestScript.BUTTON_STEP:
					doButtonStep(step);
					break;
				case TestScript.SLIDER_STEP:
					doSliderStep(step);
					break;
				case TestScript.CLICK_STEP:
					doClickStep(step);
					break;
				case TestScript.DRAG_STEP:
					doDragStep(step);
					break;
				case TestScript.TEXT_STEP:
					doTextStep(step);
					break;
				case TestScript.POSITION_STEP:
					doPositionStep(step);
					break;
				case TestScript.RIBBON_STEP:
					doRibbonStep(step);
					break;
				case TestScript.BITMAP_STEP:
					doBitmapStep(step);
					break;
			}
		}
		
		private function advance():void
		{
			if (_nextFunction == null) {
				++_log.countSteps;
				++_nextStep;
				schedule(doStepAndAdvance);
			}
		}
		
		private function doButtonStep(step:TestStep):void
		{
			trace("doButtonStep title="+step.label);
			var button:AbstractObject = findObj(step);
			if (button) {
				var view:ObjectView = button.getView();
				if (button is Checkbox) {
					button.controller.handleTrigger(button, Modifiers.instance.fetch("toggle"));
				} else {
					button.controller.handleMouseOver(button.getView(), null);
					button.controller.handleMouseDown(button.getView(), null);
					button.controller.handleMouseUp(button.getView(), null);
				}
				var rect:Rectangle = view.getBounds(_worldView);
				feedback.showFeedback(step,button);
			}
		}
		
		private function doSliderStep(step:TestStep):void
		{
			trace("doSliderStep title="+step.label);
			var obj:AbstractObject = findObj(step);
			if (obj is Slider) {
				var slider:Slider = obj as Slider;
				slider.sliderValue = Number(step.value);
				feedback.showFeedback(step,slider);
				stage.invalidate();
			}
		}
		
		private function doClickStep(step:TestStep):void
		{
			if (step.point == null) {
				logInvalid(step, "missing Point");
			} else {
				var global:Point = _worldView.localToGlobal(step.point);  // step.point is world coords
				var view:ObjectView = _worldView.findViewUnderPoint(global);
				trace("doClickStep global="+global, "view="+view);
				if (view) {
					var local:Point = view.globalToLocal(global);
					var event:MouseEvent = new MouseEvent(MouseEvent.MOUSE_DOWN,true,false,local.x,local.y);
					view.dispatchEvent(event);
					event = new MouseEvent(MouseEvent.MOUSE_UP,true,false,local.x,local.y);
					view.dispatchEvent(event);
					feedback.showFeedback(step);
				}
			}
		}
		
		private function doDragStep(step:TestStep):void
		{
			var global:Point = _worldView.localToGlobal(step.point);
			var view:Sprite = _worldView.findViewUnderPoint(global);
			trace("doDragStep global="+global, "view="+view);
			if (view) {
				if (view is SliderView) {
					var knob:Sprite = view.getChildAt(0) as Sprite;
					if (knob && knob.hitTestPoint(global.x,global.y)) {
						view = knob;
						trace("drag knob",knob);
					}
				}

				var local:Point = view.globalToLocal(global);
				var event:MouseEvent = new MouseEvent(MouseEvent.MOUSE_DOWN,true,false,local.x,local.y);
				view.dispatchEvent(event);
				_dragPoints = step.dragPoints;
				_nextPoint = 0;
				schedule(doDragPoint);
			} else {
				// failed to drag anything... just move on
				advance();
			}
		}
		
		private function doDragPoint():void
		{
			var worldPoint:Point = _dragPoints[_nextPoint++];
			var global:Point = _worldView.localToGlobal(worldPoint);
			trace("doDragPoint",global);
			var event:MouseEvent = new MouseEvent(MouseEvent.MOUSE_MOVE,true,false, global.x, global.y);
			event.buttonDown = true;
			stage.dispatchEvent(event);
			feedback.showDragFeedback(worldPoint);  // draw X at the current point
			
			if (_nextPoint < _dragPoints.length) {
				schedule(doDragPoint,2);
			} else {
				schedule( function():void{doDragMouseUp(global)} );
			}
		}
		
		private function doDragMouseUp(global:Point):void
		{
			trace("doDragMouseUp global="+global);
			var event:MouseEvent = new MouseEvent(MouseEvent.MOUSE_UP,false,false, global.x, global.y);
			stage.dispatchEvent(event);
			feedback.clearFeedback();
			advance();
		}
		
		private function doTextStep(step:TestStep):void
		{
			trace("doTextStep: text="+step.label);
			if (step.value != null) {
				var obj:AbstractObject = findObj(step);
				if (obj is TextObject) {
					var textObj:TextObject = obj as TextObject;
					feedback.showFeedback(step, textObj);
					var actual:String = textObj.output.toString();
					verifyString(step, step.value, actual);
				}
			}
		}
		
		private function doPositionStep(step:TestStep):void
		{
            var obj:AbstractObject = findObj(step);
            trace("doPositionStep: title="+step.label, "point="+step.point, "obj="+obj);
            if (obj) {
                if (step.point) {  // point=null means the view is off-stage
                    feedback.showFeedback(step, obj);
                }
                var actual:Point = getObjPosition(obj, _worldView);
                verifyPoint(step, step.point, actual);
            }
		}
		
        public static function getObjPosition(obj:AbstractObject, worldView:WorldView):Point
  		{
  			var view:ObjectView = obj.getView();
              if (view) {
                  var global:Point = view.localToGlobal(new Point(0,0));
                  return worldView.globalToLocal(global);
              } else {
                  return null;
              }
  		}

        public static function onScreen(step:TestStep):Boolean
  		{
            if (step.type == TestScript.BITMAP_STEP) {
                return true;  // bitmaps don't depend on an AbstractObject
            }
            var obj:AbstractObject = step.objRef;
            if (obj) {
                var view:ObjectView = obj.getView();
                return (view != null && view.stage != null);
            }
            return false;
  		}

		
		private function doRibbonStep(step:TestStep):void
		{
			trace("doRibbonStep: title="+step.label);
			var obj:AbstractObject = step.objRef;
			if (obj) {
				var anchor:WireAnchor = step.anchorRef;
				if (anchor) {
					var actual:* = anchor.value;
					var correct:* = step.ribbonValue;
					var equal:Boolean = Modifiers.isEqualTo(actual, correct);

					if (equal) {
						logPass(step);
					} else {
						var actualStr:String = (actual != null) ? actual.toString() : "null";
						var correctStr:String = (correct != null) ? correct.toString() : "null";
						logFailStrings(step, correctStr, actualStr);
					}

					return;
				}
			}
			logInvalid(step, "Can't find ribbon");
		}

		
		private function doBitmapStep(step:TestStep):void
		{
			if (step.rect) {
				feedback.clearFeedback();
				var data:BitmapData = TestBitmap.grabBitmap(_worldView, step.rect);
				step.actualBitmap = data;
				if (data && step.asset && step.asset.content as Bitmap) {
					var bitmap:Bitmap = step.asset.content as Bitmap;
					verifyBitmap(step, bitmap.bitmapData, data);
					feedback.showFeedback(step);
				} else {
					logInvalid(step, "Missing bitmap");
				}
			}
		}
		
		private function findObj(step:TestStep):AbstractObject
		{
			var obj:AbstractObject = step.objRef;
			// wireScopeContainer==null indicates a deleted object
			if (obj == null || obj.parent == null || obj.wireScopeContainer == null) {
				var title:String = (obj) ? obj.title : "null";
				logInvalid(step, "Missing object: "+step.label + " ref="+title);
				return null;
			}
			return obj;
		}
		
		private function verifyString(step:TestStep, correct:String, actual:String):void
		{
			if (actual == correct) {
				logPass(step);
			} else {
				logFailStrings(step, correct, actual);
			}
		}
		
		private function verifyPoint(step:TestStep, correct:Point, actual:Point):void
		{
            const TOLERANCE:Number = 0.4;
            if (correct == null && actual == null) {
                logPass(step);
            } else if (correct && actual && correct.subtract(actual).length < TOLERANCE) {
				logPass(step);
			} else if (correct == null || actual == null) {
                logFailStrings(step, correct ? "onScreen":"offScreen", actual ? "onScreen":"offScreen");
            } else {
				logFailStrings(step, correct.toString(), actual.toString());
			}
		}
		
		private function verifyBitmap(step:TestStep, correct:BitmapData, actual:BitmapData):void
		{
			if (TestBitmap.bitmapExactMatch(correct, actual)) {
				logPass(step);
			} else {
				var deviation:int = TestBitmap.bitmapDeviation(correct, actual);
				trace("bitmap deviation="+deviation);
				var text:String = "Bitmap: "+_script.name+" #"+step.stepNumber+" deviation="+deviation;
				_log.detailLog += text + "\n";

				if (deviation > _log.bitmapTolerance) {
					logFail(step, "bitmaps differ by "+deviation+" pixels");
				} else {
					logPass(step);
				}
				if (deviation > _log.bitmapDeviation)
					_log.bitmapDeviation = deviation;
			}
		}
		
		private function logPass(step:TestStep):void
		{
			step.status = TestScript.PASS;
			++_log.countVerified;
		}
		
		private function logFailStrings(step:TestStep, correct:String, actual:String):void
		{
			var text:String = "correct="+correct + " actual="+actual;
			logFail(step, text);
		}
		
		
		private function logFail(step:TestStep, message:String):void
		{
			step.status = TestScript.FAIL;
			++_log.countFailed;
			_allPassed = false;
			var stepNum:int = _script.steps.getItemIndex(step) + 1;
			var text:String = "FAIL: "+_script.name+" #"+stepNum+" "+step.type+"/"+step.label + "  " + message;
			_log.detailLog += text + "\n";
			trace(text);
		}
		
		private function logInvalid(step:TestStep, message:String):void
		{
			step.status = TestScript.FAIL;
            _script.status = TestScript.INVALID_SCRIPT;
			++_log.countFailed;
			_allPassed = false;
			var stepNum:int = _script.steps.getItemIndex(step) + 1;
			var text:String = "INVALID: "+_script.name+" #"+stepNum+" "+step.type+"/"+step.label + " error="+message;
			_log.detailLog += text + "\n";
			trace(text);
		}


		public static function transformRect(rect:Rectangle, fromView:DisplayObject, toView:DisplayObject):Rectangle
		{
			// compute topLeft
			var global:Point = fromView.localToGlobal(rect.topLeft);
			var topLeft:Point = toView.globalToLocal(global);
			// compute botRight
			global = fromView.localToGlobal(rect.bottomRight);
			var botRight:Point = toView.globalToLocal(global);
			var result:Rectangle = new Rectangle();
			result.topLeft = topLeft;
			result.bottomRight = botRight;
			return result;
		}

        private function get stage():Stage
        {
            return ApplicationUI.instance.stage;
        }
	}
}
