package com.alleni.author.controller.test
{
import com.alleni.author.application.ApplicationUI;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.app.ProjectController;
import com.alleni.author.controller.app.ProjectLoadingSequencer;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.definition.GadgetType;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.event.TestEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.test.TestLog;
import com.alleni.author.model.test.TestPanel;
import com.alleni.author.model.test.TestScript;
import com.alleni.author.model.test.TestStep;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.service.BorealOperation;
import com.alleni.author.service.project.ProjectListDialogOperation;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.WorldView;
import com.alleni.author.view.test.TestDirEntryView;
import com.alleni.author.view.test.TestFeedback;
import com.alleni.author.view.test.TestPanelView;
import com.alleni.author.view.test.TestScriptView;
import com.alleni.author.view.test.TestStepView;
import com.alleni.author.view.text.RichTextField;
import com.alleni.taconite.controller.ui.SecondaryWindowController;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.factory.TaconiteFactory;

import flash.desktop.Clipboard;
import flash.desktop.ClipboardFormats;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Stage;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.ui.Keyboard;
import flash.utils.getTimer;

import mx.core.UIComponent;

public class TestPanelController extends SecondaryWindowController
	{
		public static var _instance:TestPanelController;

		public static const WIDTH:Number = 400;
		public static const HEIGHT:Number = 600;
		public static const WIDTH_DIRECTORY:Number = 367;
		public static const HEIGHT_DIRECTORY:Number = 496;
		public static const WIDTH_SCRIPT:Number = 370;
		public static const HEIGHT_SCRIPT:Number = 440;
        private static const PAGE_PAUSE_TIME:int = 1000;
		
		private var _model:TestPanel;
		private var _log:TestLog;
		private var _viewContainer:UIComponent;
		private var _view:TestPanelView;
		private var _recorder:TestRecorder;
		private var _player:TestPlayer;
		private var _playAllScripts:Array;
		private var _playAllScriptNum:int;
		private var _projectsList:Vector.<ProjectInfo>;
		private var _projNum:int;
		private var _projStatus:String = "";
		private var _feedback:TestFeedback;
		private var _assetController:AssetController = AssetController.instance;
		private var _assetRequestCount:int = 0;
        private var _lastActivitySerial:int;  // value from ObjectController

        // separate from _model so it can be kept across project-loads during playAllProjects
        // edits to view.username update this var and _model.username
        [Bindable] public var username:String = null;
        [Bindable] public var templates:Boolean = false;

		
		public static function get instance():TestPanelController
		{
			if (_instance == null)
				_instance = new TestPanelController();
			return _instance;
		}
		
		public function TestPanelController()
		{
			super(new Rectangle(0,25,WIDTH,HEIGHT), "TEST PANEL", false, true);  // resizable=false, utility=true
			_window.addEventListener(Event.CLOSE, handleWindowClose);
            BorealOperation.suppressAlerts = true;  // prevent "lost network" alert
			
            initializeModel();

			_log = new TestLog();
			createFeedback();
			Application.instance.viewContext.info.testView = true;
			loadAssets();
			
			stage.addEventListener(TestEvent.ITEM_CLICKED, clickEntryListener);
			stage.addEventListener(TestEvent.ITEM_RENAMED, renameEntryListener);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_INITIALIZED, handleProjectInitialized,false,1000);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_COMPLETE, handleProjectComplete);
		}

        private function initializeModel():void
        {
            _model = Application.instance.document.scripts;
            if (_model == null) {
                _model = new TestPanel();
                Application.instance.document.project.scripts = _model;
            }
            _model.scriptName = "Untitled";
        }
		
		public function initializeTestPanel():void
		{
			// container for view, so we can delete & replace the view when project changes
			// (since ITaconiteWindow doesn't have a removeChild function)
			if (_viewContainer == null) {
				_viewContainer = new UIComponent();
				_window.addElement(_viewContainer);
			} else if (_view) {
				// get rid of previous view (occurs when switching projects after opening panel)
				_viewContainer.removeChild(_view);
			}
			
			_view = new TestPanelView();
			_viewContainer.addChild(_view);
			_view.model = _model;
			_view.log = _log;
			_view.width = WIDTH;
			_view.height = HEIGHT;
			_view.addEventListener(MouseEvent.MOUSE_DOWN, listenForClickOutsideText);

            if (username == null) {  // initial load of first project (not doing playAllProjects or loading another proj manually)
                if (_model.username != null) {
                    username = _model.username;
                } else {
                    username = TaconiteFactory.getEnvironmentImplementation().username;
                }
                templates = _model.templates;
                gotoPage(_model.panelTab);
            }
		}
		
		private function handleProjectInitialized(event:ApplicationEvent):void
		{
			trace("INITIALIZED",Application.instance.document.name);
            initializeModel();
			_log.projectName = Application.instance.document.name;
			_view.model = _model;
			_view.log = _log;
			createFeedback();
		}
		
		private function handleProjectComplete(event:ApplicationEvent):void
		{
			trace("TEST PROJECT_COMPLETE proj="+Application.instance.document.name, "_assetRequestCount="+_assetRequestCount);
			loadAssets();
			
			if (_projectsList) {
				_view.addEventListener(Event.ENTER_FRAME, waitForTestAssets);
			}
		}
				
		private function waitForTestAssets(e:Event):void
		{
			// wait for asset loading and ensure project shows for a moment before running the tests
			if (_assetRequestCount == 0) {
				_view.removeEventListener(Event.ENTER_FRAME, waitForTestAssets);
				playLoadedProject();
			}
		}
		
		private function loadAssets():void
		{
			_assetRequestCount = 0;
			for each (var script:TestScript in _model.directory.scripts) {
				for each (var step:TestStep in script.steps) {
					if (step.assetID != null && step.asset==null) {
						requestLoadAsset(step);
						++_assetRequestCount;
					}
				}
			}
			ApplicationController.addEventListener(NotificationNamesApplication.ASSET_REQUEST_PROGRESS, handleAssetProgress);
		}
		
		private function requestLoadAsset(step:TestStep):void
		{
			var asset:Asset = _assetController.requestForID(step.assetID, -1, true);
			if (asset && asset.content) {
				step.asset = asset;
				noteSizeInStep(step);
			}
		}
		
		private function handleAssetProgress(event:ApplicationEvent):void
		{
			// the event doesn't include the ID of the just-loaded asset
			var stillWaitingForAssets:int = 0;
			for each (var script:TestScript in _model.directory.scripts) {
				for each (var step:TestStep in script.steps) {
					if (step.assetID != null && step.asset == null) {
						
						var asset:Asset = _assetController.requestForID(step.assetID, -1, true);
						if (asset && asset.content) {
							step.asset = asset;
							noteSizeInStep(step);
						} else {
							++stillWaitingForAssets;
						}
					}
				}
			}
			_assetRequestCount = stillWaitingForAssets;
		}

        public function get senseActivity():Boolean
        {
            if (ApplicationController.instance.disabledInput) {
//                trace("- inputDisabled");
//                ApplicationController.instance.dumpDisableInputTokens();
                return true;
            }
            if (PseudoThread.instance.context) {
//                trace("- PseudoThread");
                return true;
            }
            if (Application.instance.testObjectActivitySerial > _lastActivitySerial) {
                _lastActivitySerial = Application.instance.testObjectActivitySerial;
//                trace("activitySerial="+_lastActivitySerial);
                return true;
            }
            if (ApplicationController.instance.renderActionsPending) {
//                trace("- renderActions");
                return true;
            }
            if (ApplicationController.instance.wireController.triggersPending) {
//                trace("- triggersPending");
                return true;
            }
            return false;
        }

		
		public function noteSizeInStep(step:TestStep):void
		{
			if (step.asset.content is Bitmap) {
				var bitmap:Bitmap = step.asset.content as Bitmap;
				var data:BitmapData = bitmap.bitmapData;
				var len:int = data.width * data.height;
				step.value = int(len/1024+1).toString() + "K";
			}
		}		
		
		private function handleWindowClose(event:Event):void
		{
			setVisible(false);
			Application.instance.testPanelVisible = false;
		}
		
		override public function setVisible(value:Boolean):void
		{
			super.setVisible(value);
			if (value && !_view) {
                initializeTestPanel();  // includes gotoPage using stored data
            }
            _model.panelVisible = value;
		}
		
		public function newButton():void
		{
			var script:TestScript = new TestScript();
			script.name = "Untitled";
			_model.directory.scripts.addItem(script);
			_model.directory.sort();
		}
		
		
		private function gotoPage(page:int):void
		{
			_view.tabs.selectedIndex = page;
            _model.panelTab = page;
		}
		
				
		public function deleteButton():void
		{
			var selection:Array = _model.directory.selectionSet;
			for each (var script:TestScript in selection) {
				var index:int = _model.directory.scripts.getItemIndex(script);
				if (index >= 0)
					_model.directory.scripts.removeItemAt(index);
			}
		}
		
		public function duplicateButton():void
		{
			trace("DUPLICATE");
			var selection:Array = _model.directory.selectionSet;
			if (selection.length > 0) {
				for each (var script:TestScript in selection) {
					var newScript:TestScript = script.clone();
					_model.directory.scripts.addItem(newScript);
				}
			}
		}

		
		public function allButton():void
		{
			_model.directory.selectAll(!_model.directory.allSelected);
		}
				
		
		public function recordButton():void
		{
			if (_log.nowRecording) {
				stopRecording();
			} else if (_model.script) {
				trace("RECORD");
				stopRecording();
				stopPlaying();
				_model.script.selectAll(false);
				
				if (_model.script.steps.length == 0)
					run();
				else
					resume();
				_recorder = new TestRecorder(_model.script, _log, worldView, this);
				_log.nowRecording = true;
				
				_model.script.clearStatus();
			}
			_feedback.clearFeedback();
		}
		
		private function stopRecording():void
		{
			if (_recorder) {
				_recorder.stopRecording();
				_recorder = null;
				_log.nowRecording = false;
			}
			_feedback.clearFeedback();
		}

		public function fixAllButton():void
		{
			playButton();
			_model.autoFix = true;
		}
		
		public function copyLogsButton():void
		{
			var text:String = "Status="+_log.allProjActivity + "\nSummary log:\n" + _log.summaryLog + "\nDetail log:\n" + _log.detailLog;
			var systemClipboard:Clipboard = Clipboard.generalClipboard;
			systemClipboard.clear();
			systemClipboard.setData(ClipboardFormats.TEXT_FORMAT, text);
		}
		
		public function playButton():void
		{
			if (_log.nowPlaying) {
				stopPlaying();
			} else if (_model.script) {
				trace("PLAY");
				stopRecording();
				stopPlaying();
				_model.autoFix = false;
				_model.script.selectAll(false);
				clearLog(_model.scriptName);
				
				run();
                _model.script.clearStatus();
				_player = new TestPlayer(_model, _log, playCompletionCallback, this);
				_log.nowPlaying = true;
			}
			_feedback.clearFeedback();
		}
		
		private function playCompletionCallback(script:TestScript):void
		{
			_player = null;
			_log.nowPlaying = false;
			logStatistics();
		}
		
		
		public function playSelectedButton():void
		{
			if (_log.nowPlayingSelected) {
				stopPlaying();  // Play button is alternate-action
			} else {
				stopRecording();
				stopPlaying();
				if (_projectsList == null)
					clearLog("Play selected scripts:");
				
				_model.autoFix = false;
				var selection:Array = _model.directory.selectionSet;
				if (selection.length > 0) {
					_model.directory.clearStatus();
					run();
					_playAllScripts = selection;
					_log.nowPlayingSelected = true;
					_playAllScriptNum = 0;
					
					_model.script = _playAllScripts[0];
					logDetail("Script \"" + _model.script.name + '"');
					_player = new TestPlayer(_model, _log, playAllScriptsCompletionCallback, this);
				}
			}
		}

		
		private function playAllScriptsCompletionCallback(script:TestScript):void
		{
			trace("playAllScriptsCompletionCallback status="+_projStatus);
			if (_playAllScripts) {
				if (_projStatus != TestScript.FAIL) {
					_projStatus = script.status;
				}
				if (script.status == TestScript.FAIL && Keyboard.capsLock) {  // stop on failure
					stopPlaying();
					_projectsList = null;
					gotoPage(TestPanel.SCRIPT_PAGE);
					return;
				}
				++_playAllScriptNum;
				if (_playAllScriptNum < _playAllScripts.length && !_log.crashed) {
					run();
					var script:TestScript = _playAllScripts[_playAllScriptNum];
					_model.script = script;
                    logDetail("Script \"" + script.name + '"');
					_player = new TestPlayer(_model, _log, playAllScriptsCompletionCallback, this);
				} else {  // finished playing them all
					trace(" did all");
					_player = null;
					_log.nowPlayingSelected = false;
					_playAllScripts = null;
					
					if (_projectsList) 
						advanceToNextProject();
					else
						logStatistics();
				}
			}
		}

		
		private function stopPlaying():void
		{
			_log.nowPlayingSelected = false;
			_playAllScripts = null;
			if (_player) {
				_player.stopPlaying();
				_player = null;
			}
			_log.nowPlaying = false;
		}
		
		
		public function playAllProjectsButton():void
		{
			gotoPage(TestPanel.ALLPROJ_PAGE);
			playAllProjects();
		}
		
		
		public function playAllProjects():void
		{
            if (_log.nowPlayingAllProjs) {
                return;
            }

			trace("PLAY ALL PROJ");
			stopRecording();
			stopPlaying();
			
			// load _projectsList
			requestListOfProjects(username);
			clearLog("");
			_log.summaryLog = Utilities.prettyDate + "  user=" + username + "\n";
		}

		private function requestListOfProjects(username:String):void
		{
			trace("requestListOfProjects user="+username);
            var op:ProjectListDialogOperation = new ProjectListDialogOperation(false, GadgetType.PROJECT);
            if (templates) {
                op.templates = true;  // when using this, do not set useMini or username
            } else {
                op.userName = encodeURIComponent(username);
                op.useMini = true;
            }

			op.addEventListener(Event.COMPLETE, function(event:Event):void {
                op.removeEventListener(Event.COMPLETE, arguments.callee);
                handleLoadProjectsComplete(op.projects);
            });

			op.execute();
		}
		
		private function handleLoadProjectsComplete(projects:Vector.<Project>):void
		{
            var startingProjectNum:int = 0;

			// array of Project, with each having name & id
			_projectsList = new <ProjectInfo>[];
			trace("proj list len",projects.length);
            for (var n:int = startingProjectNum; n < projects.length; n++) {
                var proj:Project = projects[n];
                var ok:Boolean = isOkProject(proj);
                trace("  ", proj.name, (ok)?"":" (bad)", "n="+n);
                if (ok) {
                    _projectsList.push(new ProjectInfo(proj.name, proj.id));
                }
            }
            trace(" good.len="+_projectsList.length);
            _projectsList.sort(compareNames);

            if (_projectsList.length > 0) {
                _log.allProjCount = _projectsList.length;
                _log.allProjSuccess = true;
                _log.allProjStatus.removeAll();
                _log.nowPlayingAllProjs = true;
                _projNum = 0;
				loadNextProject();
			}
		}

        private function compareNames(aInfo:ProjectInfo, bInfo:ProjectInfo):int
        {
            var a:String = aInfo.name.toUpperCase();
            var b:String = bInfo.name.toUpperCase();
            if (a == b) {
                return 0;
            } else if (a > b) {
                return +1;
            } else {
                return -1;
            }
        }


		private var _badNames:Array = ["Creator: 683_Quad: red_800x600"];
		
		private function isOkProject(proj:Project):Boolean
		{
//            if (proj.name.indexOf("2013.01.22.11.11") < 0) {
//                return false;
//            }

//            var date:Date = new Date(proj.createdDateTime);
//            if (date.fullYear < 2013) {
//                return false;
//            }

			for (var n:int = _badNames.length-1; n >= 0; n--) {
				var name:String = _badNames[n];
				if (proj.name == name)
					return false;
			}	
			return true;
		}
		
		private function loadNextProject():void
		{
            var info:ProjectInfo = _projectsList[_projNum];
			// stay at this _projNum so we can record status at end

			trace('loadNextProject name="'+info.name+'"', "id="+info.id, "num="+_projNum);
			_log.allProjActivity = "Loading project: " + info.name;
			ProjectLoadingSequencer.instance.requestLoad(info.id);
		}
		
		private function playLoadedProject():void
		{
			_log.allProjActivity = "Playing project: " + _log.projectName;
			stage.addEventListener(Event.ENTER_FRAME, waitForProjectLoad);
		}
		
		private function waitForProjectLoad(event:Event):void
		{
            if (senseActivity) {
                return;
            }
			trace("project loaded: ",Application.instance.document.name);
			stage.removeEventListener(Event.ENTER_FRAME, waitForProjectLoad);

            logDetail("Project \""+Application.instance.document.name+ '"' + " projNum="+_projNum);
            ++_log.countProjects;
            _projStatus = TestScript.PASS;

            if (checkForInvalidProject()) {
                advanceToNextProject();
            } else if (countEnabledScripts() > 0) {
				selectEnabledScripts();
                playSelectedButton();
			} else {
                // this project has no scripts, or they are all disabled
                _projStatus = TestScript.UNSCRIPTED;
                runDefaultScript();
            }
		}

        private function checkForInvalidProject():Boolean
        {
            var error:String = AuthorController.instance.eventFlow.validateToString();
            if (error != null) {
                _projStatus = TestScript.INVALID_PROJECT;
                logDetail("INVALID_PROJECT: " + error);
                return true;
            } else if (GadgetController.instance.hasExtraGadgets()) {
                _projStatus = TestScript.INVALID_PROJECT;
                logDetail("Has EXTRA-GADGETS");
                return true;
            } else {
                return false;
            }
        }

        private function runDefaultScript():void
        {
            trace("runDefaultScript", _log.projectName);
            run();
            resetTimer(PAGE_PAUSE_TIME);
            PseudoThread.add(new PseudoThreadSimpleRunnable("", resetTimer, this, [PAGE_PAUSE_TIME])); // ensure wait after loading page in a project with only one page
            stage.addEventListener(Event.ENTER_FRAME, defaultScriptTask);
        }

        private var _defaultScriptWaitTime:int = 0;

        private function resetTimer(mSec:int):void
        {
            _defaultScriptWaitTime = getTimer() + mSec;
        }

        private function defaultScriptTask(e:Event):void
        {
            if (senseActivity) {
                resetTimer(PAGE_PAUSE_TIME);
                return;
            }
            if (_defaultScriptWaitTime > 0) {
                if (getTimer() < _defaultScriptWaitTime) {
                    return;
                }
            }

            var valid:Boolean = !checkForInvalidProject();
            var flow:EventFlow = AuthorController.instance.eventFlow;
            if (valid && flow.pageNumber < flow.pages.length && _log.nowPlayingAllProjs) {  // stop-button clears nowPlayingAllProjs
                ++flow.pageNumber;
                resetTimer(PAGE_PAUSE_TIME);
            } else {
                stage.removeEventListener(Event.ENTER_FRAME, defaultScriptTask);
                advanceToNextProject();
            }
        }

		private function advanceToNextProject():void
		{
			trace("-- advanceToNextProject projNum=",_projNum);
			if (_projectsList == null) {
                trace("  _projectsList==null")
                return;
            }
			if (_projStatus != TestScript.PASS) {
				logSummary(_projStatus + ":  " + _log.projectName);
				_log.allProjSuccess = false;
			}
			_log.allProjStatus.addItem(_projStatus);
			
			++_projNum;
			if (_projNum < _projectsList.length && !_log.crashed) {
				loadNextProject();
			} else {
				_projectsList = null;
				logStatistics();
				_log.allProjActivity = "All done";
				_log.nowPlayingAllProjs = false;
                logSummary("final status: success="+ (_log.allProjSuccess && !_log.crashed));
			}
		}
		
		public function stopButton():void
		{
			trace("STOP");
			stopRecording();
			stopPlaying();
			
			if (_projectsList) {
				_projectsList = null;
				_log.allProjActivity = "Stopped";
				_log.nowPlayingAllProjs = false;
			}
		}
		
		public function textButton():void
		{
			trace("TEXT");
			if (_recorder && _model.script) {
				_recorder.startListeningForTextClick();
			}
		}
	
		public function bitmapButton():void
		{
			trace("BITMAP");
			if (_recorder && _model.script) {
				_recorder.startListeningForScreenGrab();
			}
		}
		
		public function positionButton():void
		{
			trace("BITMAP");
			if (_recorder && _model.script) {
				_recorder.startListeningForPositionClick();
			}
		}
		
		public function ribbonButton():void
		{
			trace("RIBBON");
			if (_recorder && _model.script) {
				_recorder.startListeningForRibbonClick();
			}
		}

		
		public function cutButton():void
		{
			trace("CUT");
			if (_model.script) {
				copySteps();
				_model.script.deleteSelected();
			}
		}
		
		public function copyButton():void
		{
			trace("COPY");
			if (_model.script)
				copySteps();
		}
		
		public function pasteButton():void
		{
			trace("PASTE");
			if (_model.script)
				pasteSteps();
		}
		
		public function deleteStepsButton():void
		{
			trace("DELETE STEPS");
			if (_model.script)
				_model.script.deleteSelected();
		}
		
		private function copySteps():void
		{
			var scrap:Array = [];
			var selected:Array = _model.script.selectionSet;
			for each (var step:TestStep in selected) {
				scrap.push(step.clone());
			}
			_model.clipboard = scrap;
		}
		
		private function pasteSteps():void
		{
			var index:int = -1;
			if (_model.script.selectionSet.length > 0) {
				var lastSel:TestStep = _model.script.selectionSet[_model.script.selectionSet.length-1];
				index = _model.script.steps.getItemIndex(lastSel);
			}
			if (index < 0)
				index = _model.script.steps.length-1;
			++index;
			_model.script.selectAll(false);
			trace("paste steps at index="+index);
			
			for each (var step:TestStep in _model.clipboard) {
				var newStep:TestStep = step.clone();
				newStep.selected = true;
				_model.script.steps.addItemAt(newStep, index++);
				scrollTo(newStep);
			}
		}
		
		public function scrollTo(step:TestStep):void
		{
			if (_model.script) {
				var index:int = _model.script.steps.getItemIndex(step);
				var yy:Number = (index + 1) * TestScriptView.SPACING;
				var yyy:Number = yy - _view.scriptScroller.height;
				if (yyy > _view.scriptView.verticalScrollPosition)
					_view.scriptView.verticalScrollPosition = yyy;
			}
		}
		
		public function allStepsButton():void
		{
			if (_model.script)
				_model.script.selectAll(!_model.script.allSelected);
		}
		
		public function cancelButton():void
		{
			if (_recorder)
				_recorder.endPromptedListening();
			_log.promptVisible = false;
		}

		private var _clickPoint:Point;
        private var _clickEntry:TestScript;
        private var _clickStep:TestStep;

		private function clickEntryListener(event:TestEvent):void
		{
			trace("TestPanel: clickEntryListener dbl="+event.doubleClick,event.model);
			if (event.model is TestScript) {
				clickDirEntry(event);
			} else if (event.model is TestStep) {
				clickStep(event);
			}
		}
		
		private function clickDirEntry(event:TestEvent):void
		{
            var oldClickEntry:TestScript = _clickEntry;
			var entry:TestScript = event.model;
            _clickEntry = entry;
			if (event.ctrlKey) {
                _model.directory.select(entry, !entry.selected);
			} else if (entry.selected == false) {
				if (event.shiftKey && oldClickEntry) {
                    _model.directory.selectRange(oldClickEntry, entry);
                } else {
                    _model.directory.selectAll(false);  // deselect all
                    _model.directory.select(entry, true);
                }
			}
			if (event.doubleClick && entry.selected) {
				_model.script = event.model;
				gotoPage(TestPanel.SCRIPT_PAGE);
			}
		}
		
		private function clickStep(event:TestEvent):void
		{
            var oldClickStep:TestStep = _clickStep;
			var step:TestStep = event.model;
			_clickPoint = event.stagePoint.clone();
			_clickStep = step;
			showFeedbackForStep(step, event.altKey);
			if (event.ctrlKey) {
				step.selected = !step.selected;
			} else if (step.selected == false) {
				if (event.shiftKey && oldClickStep) {
                    _model.script.selectRange(oldClickStep, step);
                } else {
                    _model.script.selectAll(false);  // deselect all
                    _model.script.select(step, true);
                }
			}
		}
		
		private function showFeedbackForStep(step:TestStep, altKey:Boolean):void
		{
			switch (step.type) {
				case TestScript.BUTTON_STEP:
				case TestScript.POSITION_STEP:
				case TestScript.TEXT_STEP:
					var obj:AbstractObject = step.objRef;
                    if (obj) {
                        _feedback.showFeedback(step, obj);
                    }
					break;
				case TestScript.BITMAP_STEP:
					if (step.asset && step.actualBitmap)
						showBitmapDiff(step, altKey);
					else
						_feedback.showFeedback(step);
					break;
				default:
					_feedback.showFeedback(step);
					break;
			}
		}
		
		private var _altKeyForBitmapDiff:Boolean;
		
		private function showBitmapDiff(step:TestStep, altKey:Boolean):void
		{
			_feedback.showBitmapDiff(step, 0,0);

			_altKeyForBitmapDiff = altKey;
			_view.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener, true);
			_view.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener, true);
		}
		
		private function mouseMoveListener(event:MouseEvent):void
		{
			var point:Point = new Point(event.stageX, event.stageY);
			var offset:Point = point.subtract(_clickPoint);
			
			if (_altKeyForBitmapDiff) {  // for alt-click show correct when mouse slides up, actual otherwise
				var option:int = (offset.y < 0) ? 2 : 1;  //  2=correct, 1=actual
				_feedback.showBitmapDiff(_clickStep, 0,0, option, 16);
			} else {
				_feedback.showBitmapDiff(_clickStep, offset.x,offset.y);
			}
		}
		
		private function mouseUpListener(event:MouseEvent):void
		{
			_view.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpListener, true);
			_view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener, true);
			_feedback.visible = false;
		}
		
		
		private function renameEntryListener(event:TestEvent):void
		{
			if (event.model is TestScript) {
				_model.directory.sort();
			}
		}

		public function promptWithCancel(message:String):void
		{
			_log.prompt = message;
			_log.promptVisible = true;
		}
		
		public function removePrompt():void
		{
			_log.promptVisible = false;
		}
		
		
		private function createFeedback():void
		{
			_feedback = new TestFeedback(worldView);
			EditorUI.instance.presentationContainer.feedbackControlsView.addChild(_feedback);
		}
		
		
		
		private function clearLog(info:String):void
		{
			// TODO: when playing all-projects, the username may be different
			_log.detailLog = info + "  " + Utilities.prettyDate + "   user=" + username + "\n";
			_log.clearStatistics();
		}

		public function logException(error:String):void
		{
			var name:String = (_model.script) ? _model.script.name : "";
            logDetail("\"" + _log.projectName + "\"  \"" + name + "\"  CRASH="+error);
			_log.crashed = true;
		}
		
		private function logStatistics():void
		{
			var seconds:int = (getTimer() - _log.startTime) / 1000;
			_log.detailLog += "\n" + "Projects tested="+_log.countProjects + "\n" +
					"Steps performed="+_log.countSteps + "\n" +
					"Steps verified="+_log.countVerified + "\n" +
                    "Steps failed="+_log.countFailed + "\n" +
                    "Warnings="+_log.countWarnings + "\n" +
					"Largest pixel deviation="+_log.bitmapDeviation + "\n" +
					"Time in seconds="+seconds + "\n";
			if (_log.crashed)
				_log.detailLog += "CRASHED\n";
		}

		private function addDescendantsToArray(parent:AbstractContainer, array:Array):void
		{
			for each (var obj:AbstractObject in parent.objects) {
				array.push(obj);
				if (obj is AbstractContainer)
					addDescendantsToArray(obj as AbstractContainer, array);
			}
		}

		private function listenForClickOutsideText(event:MouseEvent):void
		{
			var global:Point = worldView.localToGlobal(new Point(0,0));
			if (event.stageY > global.y) {
				if (_view.stage.focus is RichTextField) {
					var text:RichTextField = _view.stage.focus as RichTextField;
					if (!text.hitTestPoint(event.stageX, event.stageY)) {
						if (text.parent is TestDirEntryView || text.parent is TestStepView) {
							trace("click outside: focus="+_view.stage.focus);
							ApplicationController.instance.resetKeyboardFocus(true);
						}
					}
				}
			}
		}
		
        private function countEnabledScripts():int
  		{
            var result:int = 0;
  			for each (var script:TestScript in _model.directory.scripts) {
  				var title:String = script.name;
  				if (!title.match(/DISABLED/)) {
                      ++result;
                  }
  			}
            return result;
  		}

        private function selectEnabledScripts():void
  		{
  			_model.directory.selectAll(false);  // deselect
  			for each (var script:TestScript in _model.directory.scripts) {
  				var title:String = script.name;
  				if (!title.match(/DISABLED/))
  					_model.directory.select(script, true);
  			}
  		}

        public function focusInListener(e:FocusEvent):void
        {
            trace("focusIn");
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
        }

        public function focusOutListener(e:FocusEvent):void
        {
            trace("focusOut");
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
        }

		
		public function formatPoint(point:Point):String
		{
			return "[" + formatCoordinate(point.x) + ", " + formatCoordinate(point.y) + "]";
		}
		
		private function formatCoordinate(xx:Number):String
		{
			var digits:String = xx.toString();
			var decimal:int = digits.indexOf(".");
			if (decimal >= 0)
				return digits.substring(0, decimal+2);
			else
				return digits;
		}


		public function hitTestPanel(global:Point):Boolean
		{
			return _view.hitTestPoint(global.x, global.y);
		}
		
		public function get panelModel():TestPanel
		{
			return _model;
		}
		
		public function get logModel():TestLog
		{
			return _log;
		}
		
		public function get feedback():TestFeedback
		{
			return _feedback;
		}
				
		
		private function get worldView():WorldView
		{
			return WorldContainer(Application.instance.viewContext).worldView;
		}
		
		private function run():void
		{
			worldView.context.info.testView = true;
            ApplicationController.instance.authorController.setLocked(true);
//			pause();
			ApplicationController.instance.authorController.run();
		}
		
		public function resume():void
		{
			if (!Application.running)
				ApplicationController.instance.authorController.resume();
		}
		
		public function pause():void
		{
			if (Application.running)
				ApplicationController.instance.authorController.pause();
		}
		
		public static function get testRunning():Boolean
		{
			return (_instance != null);
		}

        public function logSummary(str:String):void
        {
            _log.summaryLog += str + "\n";
        }

        public function logDetail(str:String):void
        {
            _log.detailLog += str + "\n";
        }

        private function get stage():Stage
        {
            return ApplicationUI.instance.stage;
        }

		
	}
}

class ProjectInfo {
    function ProjectInfo(name:String, id:String)
    {
        this.name = name;
        this.id = id;
    }

    public var name:String;
    public var id:String;
}