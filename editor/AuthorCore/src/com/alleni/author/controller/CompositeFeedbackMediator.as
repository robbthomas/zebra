package com.alleni.author.controller
{
	import com.alleni.author.application.ApplicationUI;
	import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Dock;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.service.project.PublishedProjectListOperation;
import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.ui.palettes.CompositeDetailsView;
	import com.alleni.author.view.ui.palettes.PopoverView;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class CompositeFeedbackMediator
	{
		private var _view:ObjectView;
		private static var _publishingPanel:CompositeDetailsView;
		
		public function CompositeFeedbackMediator()
		{
		}
		
		public function handleFeedbackEvents(view:ObjectView, publishButton:DisplayObject):void
		{
			_view = view;
			publishButton.addEventListener(MouseEvent.MOUSE_DOWN, handlePublishMouseDown);
		}
		
		private function handlePublishMouseDown(event:Event):void
		{
		    event.stopImmediatePropagation();
            prepPublishNameData(1);
        }

        public function prepPublishNameData(projectType:uint):void{
            var publishedProjectList:PublishedProjectListOperation = new PublishedProjectListOperation(projectType);
            publishedProjectList.addEventListener(Event.COMPLETE, openPublishDialog);
            publishedProjectList.execute();
		}

        public function openPublishDialog(event:Event):void{
           var composite:Composite = _view.model.value as Composite;
			if (!composite)
				return;

            if(composite.gadget.projectTypeId == 1){
                var usages:UsageCounts = new UsageCounts();
                composite.getUsageCounts(usages);
                for each (var g:Project in usages.getGadgetsList()) {
                    if(g.permissions.canRepublish == false){
                        return;
                    }
                }
            }

			if (publishingPanel.visible && Dock.instance.publishingCandidate == composite.gadget) {
				publishingPanel.visible = false;
			} else {
				if (!_view.stage) return;
                Application.instance.publishingInProgress = true;
				Dock.instance.publishingCandidate = composite.gadget;
				ApplicationUI.instance.rawChildren.addChild(publishingPanel);
				publishingPanel.addEventListener(Event.CLOSE, handlePanelClose, false, 0, true);

				if (publishingPanel.target)
					publishingPanel.target.removeEventListener(Event.RENDER, handleViewRender);

				publishingPanel.resetDirection();
				publishingPanel.target = _view;

				_view.addEventListener(Event.RENDER, handleViewRender, false, 0, true);
				_view.addEventListener(Event.REMOVED_FROM_STAGE, handleViewRemoved, false, 0, true);
			}
        }

		private static function get publishingPanel():CompositeDetailsView
		{
			if (!_publishingPanel)
				_publishingPanel = new CompositeDetailsView(null, "publishingCandidate", PopoverView.POINT_LEFT|PopoverView.POINT_RIGHT|PopoverView.POINT_DOWN, 160);
			return _publishingPanel;
		}
		
		public static function requestHidePublishingPanel():Boolean
		{
			if (!_publishingPanel) return false;
			
			_publishingPanel.visible = false;
			
			return true;
		}
		
		private function handlePanelClose(event:Event):void
		{
			event.currentTarget.removeEventListener(Event.CLOSE, handlePanelClose);
			_view.removeEventListener(Event.RENDER, handleViewRender);
			_view.removeEventListener(Event.REMOVED_FROM_STAGE, handleViewRemoved);
			
			const container:DisplayObjectContainer = ApplicationUI.instance.rawChildren;
			if (container.contains(publishingPanel))
				container.removeChild(publishingPanel);
		}
		
		private static function handleViewRender(event:Event):void
		{
			const view:DisplayObject = event.target as DisplayObject;
			publishingPanel.target = view;
		}
		
		private static function handleViewRemoved(event:Event):void
		{
			requestHidePublishingPanel();
		}
	}
}
