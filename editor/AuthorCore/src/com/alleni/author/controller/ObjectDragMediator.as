/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller
{
import com.alleni.author.Navigation.BackstageView;
import com.alleni.author.Navigation.EventFlowView;
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.EventPageView;
import com.alleni.author.Navigation.FlowView;
    import com.alleni.author.Navigation.IPageView;
import com.alleni.author.Navigation.PagerController;
import com.alleni.author.Navigation.PagerView;
    import com.alleni.author.Navigation.SmartObject;
    import com.alleni.author.Navigation.SmartObjectController;
    import com.alleni.author.Navigation.SmartObjectEditingMediator;
    import com.alleni.author.Navigation.SmartObjectView;
    import com.alleni.author.controller.app.ClipboardController;
    import com.alleni.author.controller.objects.ContainerController;
    import com.alleni.author.controller.ui.AbstractObjectDragMediator;
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.controller.ui.GroupSelectionController;
    import com.alleni.author.controller.ui.palettes.InspectorController;
    import com.alleni.author.controller.ui.palettes.ToolboxController;
    import com.alleni.author.definition.DoubleClickAction;
    import com.alleni.author.definition.DragInfo;
    import com.alleni.author.definition.DropIntoContainerEvent;
    import com.alleni.author.definition.Modifiers;
    import com.alleni.author.definition.action.ActionGroup;
    import com.alleni.author.definition.action.CreateObjectAction;
    import com.alleni.author.definition.action.ModifyRibbonAction;
    import com.alleni.author.definition.action.ObjectDragAction;
    import com.alleni.author.definition.action.ObjectLocation;
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.model.AbstractObject;
    import com.alleni.author.model.World;
    import com.alleni.author.model.objects.AbstractContainer;
    import com.alleni.author.model.objects.Arena;
    import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.PathObject;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.view.ObjectView;
    import com.alleni.author.view.WorldContainer;
    import com.alleni.author.view.WorldView;
    import com.alleni.author.view.objects.ArenaPageView;
    import com.alleni.author.view.objects.ArenaView;
    import com.alleni.taconite.controller.IChildMediator;
    import com.alleni.taconite.definition.HandleRoles;
    import com.alleni.taconite.document.ObjectSelection;
    import com.alleni.taconite.event.ApplicationEvent;
    import com.alleni.taconite.model.GroupSelectionObject;
    import com.alleni.taconite.model.TaconiteModel;
    import com.alleni.taconite.view.ITaconiteView;
    import com.alleni.taconite.view.TaconiteGrip;
    import com.alleni.taconite.view.ViewContext;
    
    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;
    import flash.utils.Dictionary;
    

	/**
     * Mediator to drag selected objects from one place to another.
     */
    public class ObjectDragMediator extends AbstractObjectDragMediator
    {
        private var _viewBeingDragged:ObjectView; // the view being directly dragged: if dragging path this is the path not a dependant.  If dragging gadget, this is the gadget not a child obj.
		private var _rolled:Boolean = false;
	    private var _animation:PathObject = null;
		private var _clickedWhileUnselected:Boolean;
		private var _lastDisplacement:Point;
		private var _lastPos:Point;
		private var _clickOffset:Point;  // offset from firstSelectedView.stagePoint to the active point for dropping into arena
        private var _rotationHandle:Sprite;
        private var _rotationHandleMediator:PointDragMediator;
		private static var _copying:Boolean = false;
		private static var _draggingNow:Boolean = false;
		private static var _upEvent:MouseEvent;
		private static var _upEventObject:MouseEvent;

		private static var _applicationController:ApplicationController;

	    private var _oldLocations:Dictionary;
		private var _startParent:AbstractContainer;  // at startDrag, all selected objects are gathered into this parent
		private var _oldSnapTo:AbstractObject;

        public function ObjectDragMediator(context:ViewContext)
        {
            super(context);
			if (!_applicationController)
				_applicationController = ApplicationController.instance;
        }

        /**
         * When asked to work with an ObjectView, take note of the view and add a listener for mouseDown.
         */
       	override public function handleViewEvents(view:ITaconiteView, role:uint):AbstractObjectDragMediator
        {
			_view = ObjectView(view);
			_view.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			_view.doubleClickEnabled = true;
			_view.addEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
			_view.model.addEventListener(DropIntoContainerEvent.DROP_INTO_CONTAINER, handleDropIntoContainer);

			return this;
        }


		override public function clearViewEvents():void
		{
			_view.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			_view.removeEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
			_view.model.removeEventListener(DropIntoContainerEvent.DROP_INTO_CONTAINER, handleDropIntoContainer);
		}

	    private static function getViewToSelect(view:ObjectView):ObjectView
		{
			// dragging is affected by:  Movable ribbon, Lock (pencil icon on MC), Running state.
			/****
			Dragging of objects follows these rules:

			Movable: when this ribbon is true, the object can be dragged by the student (run or run-lock)

			Locked:  when the pencil-lock is on, the object cannot be moved by the author.

				When an object cannot be moved, then its containing parent objects are examined according to the
				same rules.  If an arena object meets the criteria for dragging, then it will be moved with all
				of its contents.

				Fallback case:  when not using runningLocked, if the cannot be moved by the above, then parents
				will be scanned again to find an "unlocked" object that can be adjusted.
			****/
			trace("getMovableView: view="+view, "movable="+view.object.movable, "canSelect="+viewCanSelect(view));

            var page:EventPage = view.object.containingEventPage;
            if(Application.uiRunning == false){
                if (page && page.isMaster && !page.editing) {
                    return null;  // can't drag a master graphic unless editing the master page
                }
            }

			if (viewCanSelect(view))
				return view;

			// find movable ancestor, so dragging a locked/unmovable arena child will drag the arena, if the arena is movable
		    var parent:ObjectView = view.parentObjectView;
		    while (parent != null) {
			    if (parent as WorldView)
				    break;
			    if (viewCanSelect(parent))
				    return parent;
			    parent = parent.parentObjectView;
		    }

			if (!Application.runningLocked) {
				// fallback to allow adjustment of obj position while running:
				//   allow dragging the object itself, when no movable parent is found
				var unlockedView:ObjectView = findUnlockedView(view);
				if (unlockedView) {
					return unlockedView;
				}
			}

			return null;
	    }

		private static function viewCanSelect(view:ObjectView):Boolean
		{
			if (view is ArenaPageView || view is EventPageView || view is BackstageView || view is EventFlowView)
				return false;

			var obj:AbstractObject = view.object;
			if (obj.runtimeRotation && Application.running)
				return true;

			if (obj.childOfClosedComposite) {
				if (locationIsWiredToCustomRibbon(obj))
					return true;
				if (!(obj.movable && Application.running))
					return false;
			}

			if (Application.runningLocked)
				return obj.movable;
			
			if (Application.running) {
				return obj.movable;
			} else {
				return !obj.locked;
			}
		}

		private static function findUnlockedView(view:ObjectView):ObjectView
		{
			while (view != null) {
				if (view as SmartObjectView && (view.object as SmartObject).editing)
					return null;
				if (!(view as WorldView || view as ArenaPageView)) {
					if (!view.object.locked && !view.object.childOfClosedComposite)
						return view;
				}
				view = view.parentObjectView;
			}
			return null;
		}

		private static function locationIsWiredToCustomRibbon(obj:AbstractObject):Boolean
		{
			var accessible:SmartObject = obj.accessibleObject as SmartObject;
			if (accessible) {
				var anchors:Array = [obj.anchors["locationXYR"]];
				var custom:Array = accessible.findCustomAnchors(anchors);
				trace("locationIsWiredToCustomRibbon: outer="+accessible, "custom="+custom, (custom.length > 0));
				return custom.length > 0;
			}
			return false;
		}

		/**
         * Handle the event at the start of the drag.
         * All coordinates used are Stage coordinates, so it doesn't matter what the mouse event's
         * target is.
         */
		override public function handleMouseDown(e:MouseEvent):void
		{
			trace("ObjectDragMediator:handleMouseDown ",object);
			var stagePoint:Point = new Point(e.stageX, e.stageY);
			var objView:ObjectView = _view as ObjectView;
			var viewUnderPoint:ObjectView;
			if (objView.exactHitTest(stagePoint)) {
				viewUnderPoint = objView;
			} else {
				// mouse click dropped thru a transparent part ... find the object the click lands on
				trace("ObjectDragMediator: clicked transparent part of",objView.model.value);
				var worldView:ObjectView = (_context as WorldContainer).worldView;
				viewUnderPoint = worldView.findViewUnderPoint(stagePoint);
				trace("  view under point="+viewUnderPoint);
			}

			if (viewUnderPoint) {
				if (viewUnderPoint != objView) {
					trace("  sending click to",viewUnderPoint.model.value);
					viewUnderPoint.interceptMouseDown(e);  // send event to drag-mediator of the other object
				} else { // click will be handled by this object (but may select outer gadget)
                    mouseDownThisObject(e);
                }
            }
        }

        public function mouseDownThisObject(e:MouseEvent):void
        {
            var objView:ObjectView = _view as ObjectView;
            var dragOK:Boolean = objView.interceptObjMouseDown(e);  // runtime click of button, etc
            _clickedWhileUnselected = !objView.selected;

            var selectView:ObjectView = getViewToSelect(objView);
            trace("select view="+selectView);
            if (selectView) {
                var obj:AbstractObject = selectView.model.value as AbstractObject;
                _viewBeingDragged = selectView;
                var groupModels:Vector.<TaconiteModel>;
                var selectModel:TaconiteModel = selectView.model;
                var groupSelection:GroupSelectionObject = selectModel.value.group;
                if (groupSelection == null && obj.groupID != null) {
                    groupSelection = GroupSelectionController.makeGroupFromObjectGroupID(obj);
                }

                if (obj.runtimeRotation && Application.running)
                    _context.controller.selectSingleModel(obj.model, false);
                else if (e.shiftKey || e.ctrlKey) {
                    if(groupSelection) {
                        groupModels = GroupSelectionController.modelsForGroup(groupSelection);
                        _context.controller.modifyMultiSelection(groupModels);
                        GroupSelectionController.showFeedbackFor(groupSelection.selection, true, true);
                    } else {
                        _context.controller.modifySelection(selectModel);
                    }
                } else {
                    if(groupSelection) {
                        if (!selectView.selected) {
                            groupModels = GroupSelectionController.modelsForGroup(groupSelection);//.concat(WorldContainer(_context).document.selection.selectedModels);
                            _context.controller.selectModels(groupModels);
                            GroupSelectionController.showFeedbackFor(groupSelection.selection, true, false);
                        }
                    } else {
                        if (Application.runningLocked)
                            InspectorController.instance.listeningToSelection = false;
                        selectIfNotSelected(selectModel, e.shiftKey);
                    }
                }

                if (!obj.movable && !(obj.runtimeRotation && Application.running) && Application.runningLocked)
                    dragOK = false;  // selected for runtime rotation ... but can't move

                if (dragOK) {
                    if (selectView == objView) {
                        setupDragging(e);
                        if (!obj.movable && obj.runtimeRotation && Application.running)
                            setupRotation(objView, e);
                    } else {
                        selectView.interceptSetupDragging(e);
                        if (!obj.movable && obj.runtimeRotation && Application.running)
                            setupRotation(selectView, e);
                    }
                }
                if (_stopPropagation)
                    e.stopPropagation();
            }
		}

        private function setupRotation(objView:ObjectView,mouseDownEvent:MouseEvent):void
		{
            _rotationHandle = new Sprite();
            _context.addChild(_rotationHandle);
            _rotationHandleMediator = new PointDragMediator(objView.context,HandleRoles.ROTATE);
            _rotationHandleMediator.localHandlePosition = new Point(objView.mouseX,objView.mouseY);
            _rotationHandleMediator.handleViewEvents(objView,_rotationHandle);
            _rotationHandleMediator.handleMouseDown(mouseDownEvent);
        }

        private function teardownRotation():void
		{
            if (_rotationHandle && _context.contains(_rotationHandle))
                _context.removeChild(_rotationHandle);
            _rotationHandle = null;
            if (_rotationHandleMediator)
                _rotationHandleMediator.warnToDelete();
            _rotationHandleMediator = null;
        }

		public function setupDragging(e:MouseEvent):void
		{
			if (!(e.target as TaconiteGrip)) {
				_dragAllowed = true;
				_dragStarted = false;
				_draggingNow = false;
				_dragPoint = new Point(e.stageX, e.stageY);
				_context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
				_context.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			}
		}

		protected function selectIfNotSelected(model:TaconiteModel, extend:Boolean):void
		{
			var authorController:AuthorController = _applicationController.authorController;
			var selection:ObjectSelection = authorController.selection as ObjectSelection;

			if (selection)
				if (!selection.contains(model))
					authorController.selectSingleModel(model, extend);
		}

		protected function selectEachIfNotSelected(models:Vector.<TaconiteModel>, extend:Boolean):void
		{
			var first:Boolean = true;
			var selection:ObjectSelection = _applicationController.authorController.selection as ObjectSelection;

			for each (var model:TaconiteModel in models) {
				if (selection)
					if (!selection.contains(model))
						if (first)
							selectIfNotSelected(model,false);
						else
							_applicationController.authorController.modifySelection(model);
				first = false;
			}
		}

		public function preventDrag(prevent:Boolean=true):void
		{
			_dragAllowed = !prevent;
		}

        /**
         * Handle mouse motion during the drag by initiating it if necessary when the mouse
         * exceeds the distance threshold, and then calling the move-handling function.
         */
		public function forwardToHandleMouseMove(e:MouseEvent):void
		{
			handleMouseMove(e);
		}

        override protected function handleMouseMove(e:MouseEvent):void
        {
//			trace("ObjectDragMediator:handleMouseMove",e.stageX,e.stageY);
			if (Application.instance.currentTool == ToolboxController.ARROW && _view.stage) {
				var delta:Point = new Point(e.stageX, e.stageY).subtract(_dragPoint);  // for TestScripts: use e.stageX, not stage.mouseX
				if (!_dragStarted && e.buttonDown && _dragPoint && (Math.abs(delta.x) >= minimumDrag || Math.abs(delta.y) >= minimumDrag))
				{
					if(object.isAnimationPath) {
						// select all objects using this path
						selectDependants(object.wireScopeContainer, _view as ObjectView);
					}
					handleDragStart(e);
					if (_oldPositions.length > 0) {
						_dragStarted = true;
						_draggingNow = true;
						containerDragStart(e);
					}
				}

				if (_dragStarted && firstSelectedView) {
					containerDragMove(e);
					// we have to handle animationPath reparenting
					// outside of the "requestDragMove()" feature
					// because the delay creates a flicker on the
					// object that is getting reparented

					handleDragMove(e);
				}
				if(_stopPropagation) {
					e.stopPropagation();
				}
				_mouseMoveTimestamp = _taconiteTimer.milliseconds;
			}
        }

		/**
		 * Does a recursive inspection of all objects
		 * and reparents them if they are assigned to animationPath.
		 * @param container
		 * @param view
		 * @param parent
		 *
		 */
		private function reparentDependants(container:AbstractContainer,view:ObjectView,parent:TaconiteModel):void
		{
			var child:AbstractObject;
			for each(child in container.objects) {
				if(child.pathOn && child.snapTo && child.snapTo == view.model.value && child.model.parent != parent) {
					_oldLocations[child] = ObjectLocation.fromObject(child);
					child.model.changeParent(parent);
					child.setInitialZIndex();
				}
				var container:AbstractContainer = child as AbstractContainer;
				if(container) {
					reparentDependants(container, view, parent);
				}
			}
		}

		/**
		 * Does a recursive inspection of all objects and selects
		 * them if they are currently assigned to animationPath.
		 * @param container
		 * @param view
		 * @return
		 *
		 */
		private function selectDependants(container:AbstractContainer,view:ObjectView):void
		{
			var dependantModels:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
			for each(var child:AbstractObject in container.objects) {
				if(child.pathOn && child.snapTo && child.snapTo == view.model.value) {
					if(dependantModels.indexOf(child.model) == -1) {
						dependantModels.push(child.model);
					}
				}
				var container:AbstractContainer = child as AbstractContainer;
				if(container) {
					selectDependants(container, view);
				}
			}
			_applicationController.authorController.modifyMultiSelection(dependantModels);
		}

        /**
         * Handle the end of the drag or click gesture.
         */
        override protected function handleMouseUp(e:MouseEvent):void
        {
			trace("ObjectDragMediator:handleMouseUp", _upEvent, e);
			if (_upEvent === e) return;
            trace("ObjectDragMediator:handleMouseUp setting upEvent");
			_upEvent = e;
			
            teardownRotation();
			var object:AbstractObject = _view.model.value as AbstractObject;
			if (Application.running) {
				CollisionMediator.checkDropTarget(object);
			}

        	super.handleMouseUp(e);

            if (!Application.runningLocked)
                InspectorController.instance.listeningToSelection = true;

            _viewBeingDragged = null;
        }


        /**
         * At the start of a drag, capture the positions of all selected objects so that we
         * can move them all by the same delta later on.
         */
        override protected function handleDragStart(e:MouseEvent):void
        {
			var authorController:AuthorController = _applicationController.authorController;

        	if (_dragAllowed && Application.instance.currentTool == ToolboxController.ARROW) {
                var ao:AbstractObject = _view.model.value as AbstractObject;
				_copying = e.altKey && !e.ctrlKey && !ao.childOfClosedMasterPage && !ao.childOfClosedComposite;  // prevent copying child of closed gadget (otherwise possible while running)
	        	if (!_copying) {
					//NotificationController.send(new Notification(NotificationNamesEditor.DESELECT_ALL));
		            if (!authorController.selection.contains(_view.model))
						authorController.selectSingleModel(_view.model);
		        } else {
                    // duplicateSelection clears selection set
					ClipboardController.instance.duplicateSelection(false, object.parent); // setComplete=false, so creation of MC is deferred till mouseUp
                    _viewBeingDragged = null;  // will be set below
		        }
                var parentInfo:DragInfo = null;
                var editor:SmartObjectEditingMediator = ApplicationController.instance.authorController.currentVelumMediator;
                if (editor && object.model.isDescendant(editor.editingObject.model)) {
                    parentInfo = DragInfo.fromModel(editor.editingObject.model);
                }
	            _oldPositions = [];
				var m:TaconiteModel;
				const selectedModels:Vector.<TaconiteModel> = authorController.selection.selectedModels;
	            for each (m in selectedModels) {
					var obj:AbstractObject = m.value as AbstractObject;
					var smart:SmartObject = obj as SmartObject;
                    if (_viewBeingDragged == null && obj.snapTo == null)  // when dragging a path with objs snapped to it, _viewBeingDragged is the path
                        _viewBeingDragged = obj.getView(_context);

					if ((obj.movable || !(obj.locked || obj.childOfClosedMasterPage)) && !(smart && smart.editing)) {
		            	// store original global position of each object, into _oldPositions, in order of selection set
                        var info:DragInfo = DragInfo.fromModel(m, true, parentInfo);
                        if(info == null) {
                            continue;
                        }
                        _oldPositions.push(info);
                        obj.dragging = true;
                        obj.zIndex = -1;

						if (Application.running && obj.enabled) {
							_applicationController.wireController.sendTrigger(obj, Modifiers.instance.fetch("dragStart"));
							obj.cancelLocationTweens();
						}
					}
	            }

				var top:TaconiteModel = filterOldPositions(_view.model);
				if (_oldPositions.length == 0)
					return;
				if (firstSelectedView) {
					_clickOffset = _dragPoint.subtract(firstSelectedView.stagePosition);
				}
                if(obj != null){
                    if(Application.running && !obj.movable && !obj.runtimeRotation) {
                        ApplicationController.instance.authorController.editWhileRunningStart();
                    }
                }

				// creation of animation path:  restrictions to simplify Undo: single obj & paused
				if(e.ctrlKey && !e.altKey && !Application.running && "snapTo" in object.anchors && object.pathOn) {
					_animation = createAnimationPath();  // create path and add position of the object
				} else {
					_animation = null;
				}

		        if(_copying == false) {
					_oldLocations = new Dictionary();
					var oldInfo:DragInfo;
					for each(oldInfo in _oldPositions) {
						_oldLocations[oldInfo.model.value] = ObjectLocation.fromObject(oldInfo.model.value as AbstractObject);
					}
		        }

				var parentView:ObjectView = findAcceptingView((top.value as AbstractObject).getView(_context), new Point(e.stageX, e.stageY));
				_startParent = TaconiteModel(parentView?parentView.model:top.parent).value as AbstractContainer;
		        var allSameParent:Boolean = _oldPositions.every(function callback(item:*, index:int, array:Array):Boolean {
					return (item.model as TaconiteModel).parent == _startParent.model;
		        });
		        var oldParents:Dictionary = new Dictionary();
		        if(!allSameParent) {
					var dragInfo:DragInfo;
			        for each(dragInfo in _oldPositions) {
						var dragObj:AbstractObject = dragInfo.model.value as AbstractObject;
						var parentModel:TaconiteModel = dragInfo.model.parent;

				        if(!Application.running || !dragObj.movable) {
				            oldParents[parentModel.value as AbstractContainer] = 1;
				        }
				        if(parentModel == _startParent.model) {
							_startParent.model.setChildIndex(dragObj, _startParent.objects.length-1);
				        } else {
							changeParentObject(dragObj, _startParent);
				        }
			        }
					var p:Object;
			        for (p in oldParents) {
				        AbstractContainer(p).setChildrenZPositions(true);
			        }
		        }
	        }
        }


		private function filterOldPositions(dragged:TaconiteModel):TaconiteModel
		{
			_oldPositions.filter(function(info:DragInfo, index:int, arr:Array):void {
				trace("  ", info.global, info.model, info.zArray);
			});

			// omit cloaked objects at runtime
			if (Application.running) {
				_oldPositions = _oldPositions.filter( function(info:DragInfo, index:int, arr:Array):Boolean {
                        return (!(info.model.value as AbstractObject).cloakOnRun || (info.model.value as AbstractObject).visible); } );
			}

			// ensure _oldPositions can be iterated through and change parent will preserve z ordering
			_oldPositions.sort(function(a:DragInfo, b:DragInfo):Number{
				var bLen:int = b.zArray.length;
				var i:int;
				var length:int = a.zArray.length;

				for(i = 0; i < length; i++) {
					if(i >= bLen) {
						// a is some child of b
						return 1;
					} else if(a.zArray[i] > b.zArray[i]) {
						return 1;
					} else if(a.zArray[i] < b.zArray[i]) {
						return -1;
					}
					// continue to check next position
				}
				// b is some child of a
				return -1;
			});

			_oldPositions.filter(function(info:DragInfo, index:int, arr:Array):void {
				trace("  ", info.global, info.model, info.zArray);
			});
			// if we are already moving/reparenting an ancestor, there is no need to move/reparent the child
			// in fact this has produced buggy behavior
			_oldPositions = _oldPositions.filter(function(element:DragInfo, index:int, arr:Array):Boolean {
				var info:DragInfo;
				for each(info in _oldPositions) {
					//				        trace("Testing: ", element.model.value, obj.model.value);
					var p:TaconiteModel = (element.model as TaconiteModel).parent;
					while(p != null) {
						if(p == info.model) {
							//						        trace(" obj is parent");
							if(element.model == dragged) {
								dragged = p;
								// we are filtering out element because p is an ancestor
								// so we'll assume we're dragging p instead of element
							}
							return false;
						}
						p = p.parent;
					}
					//						trace(" obj is NOT parent");
				}
				//					trace("no parent");
				return true;
			});

			_oldPositions.filter(function(element:DragInfo, index:int, arr:Array):void {
				trace("  ", element.global, element.model, element.zArray);
			});
			return dragged;
		}

		/**
		 * For each move during the drag, position the models appropriately given the drag distance
		 * from the starting point.
		 */
		override protected function handleDragMove(event:MouseEvent, withMomentum:Boolean=false):void
		{
			// schedule the move in render event, so MCs will stay in sync with graphics,
			// and to reduce the backlog of mouse-move events when dragging many objects
			if (withMomentum && Application.running) {
				requestInertial(_view, handleDragMoveNow, event, _lastDisplacement);
            } else if (!(object.runtimeRotation && Application.running && !object.movable)) {
				_applicationController.requestDragMove(handleDragMoveNow, event);
            }
		}

		private function handleDragMoveNow(e:MouseEvent, altPoint:Point = null):Point
		{
			var stagePoint:Point = new Point();
			if (altPoint != null) { // if someone passes in a point, use that instead of the MouseEvent.
				stagePoint = altPoint;
			} else {
				stagePoint = new Point(e.stageX, e.stageY);
			}
			if (_oldPositions == null || _oldPositions.length == 0)
				return null;

			if (_lastPos)
				_lastDisplacement = stagePoint.subtract(_lastPos);
			_lastPos = stagePoint;
			var delta:Point = stagePoint.subtract(_dragPoint);

			if (e.shiftKey) {  // constrain drag to horz / vert axis
				if (Math.abs(delta.x) < Math.abs(delta.y))
					delta.x = 0;
				else
					delta.y = 0;
			}

			if (firstSelectedView && _dragAllowed && Application.instance.currentTool == ToolboxController.ARROW) {
				var i:int = 0;
				if(_animation) {
					extendAnimationPath(_animation, e.shiftKey);
				}

				var worldView:WorldView = (_view.context as WorldContainer).worldView;
				var world:World = worldView.world;
				var acceptingView:ObjectView = null;  // wait to find the view, until after move object since it may be constrained by path and therefore affect result

				var info:DragInfo;
				for each (info in _oldPositions) {
					var global:Point = info.global.add(delta);
					var obj:AbstractObject = info.model.value as AbstractObject;
					var objView:ObjectView = obj.getView(_context);
					if (!_copying)
						global = constrainToWorldAndArena(global, objView);
					obj.setStagePosition(global);  // set x,y and constrain to path, if any
					if (Application.running && obj.enabled)
						_applicationController.wireController.sendTrigger(obj, Modifiers.instance.fetch("dragMove"));
					else if ("noMessageCenter" in obj)
						_applicationController.wireController.requestRedrawForObject(obj);

					// when an obj is constrained to a path inside arena, we don't want to pop out of arena just because cursor leaves arena even tho path keeps object inside
					// so instead of using raw mouse position, we an offset from firstSelectedView.stagePosition
					if (objView == firstSelectedView && _clickOffset) {
						var activePoint:Point = objView.stagePosition.add(_clickOffset);
						acceptingView = findAcceptingView(objView, activePoint);
					}
					if(acceptingView) {
						var oldParent:TaconiteModel = info.model.parent;
						if (acceptingView.model != oldParent) {
							changeParentView(objView, acceptingView);
							if (objView == firstSelectedView && obj.isAnimationPath) {
								// if this view is an animationPath then reparent all its followers as well
								reparentDependants(ApplicationController.instance.authorController.world, objView, acceptingView.model);
							}
						}
					}

					if (!(Application.running && obj.movable)) {
						obj.initialValues["x"] = obj.x;
						obj.initialValues["y"] = obj.y;
						obj.initialValues["parent"] = obj.parent.referenceJSON;
						obj.initialValues["zIndex"] = AbstractContainer(obj.parent).objects.getItemIndex(obj);
						//						trace("ObjectDragMediator: set INIT position="+obj.x+","+obj.y);
					}
				}
				if (acceptingView) {
					_applicationController.authorController.log("dropIntoContainer " + acceptingView.object.logStr);
				}
			}
			return _lastDisplacement;
		}

		override protected function handleDragEnd(e:MouseEvent) : void
		{
			var ao:AbstractObject;
			var authorDrag:Boolean;

            if (_viewBeingDragged) containerDragEnd(e);   // let ArenaPageController place obj in grid, before we log undo actions

			if(_copying) {
				var added:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
				var info:DragInfo;
				for each (info in _oldPositions) {
					added.push(info.model);
				}
				ApplicationController.currentActionTree.commit(CreateObjectAction.fromSelection(added));
			} else if(_oldPositions && _oldPositions.length == 1){
				ao = AbstractObject(DragInfo(_oldPositions[0]).model.value);
				if(Application.running == false || ao.movable == false) {
					authorDrag = true;
					ao.setInitialZIndex(true);
					group = new ActionGroup("Drag " + ao.title);
					if (_animation) {
						extendAnimationPath(_animation, e.shiftKey);
						finishAnimationPath(ao, _animation);  // sets ao.snapTo
						group.add(CreateObjectAction.fromObject(_animation));
						group.add(ModifyRibbonAction.fromObject(ao, "snapTo", _oldSnapTo));
					}
					group.add(ObjectDragAction.fromObject(ao, _oldLocations[ao]));
					ApplicationController.currentActionTree.commit(group);
				}
			} else {
                if (_oldPositions)
		    		var group:ActionGroup = new ActionGroup("Drag " + _oldPositions.length + " objects");
				var key:Object;
				for(key in _oldLocations) {
					ao = key as AbstractObject;
					if(!Application.running || !ao.movable) {
						authorDrag = true;
						var old:ObjectLocation = _oldLocations[ao];
						ao.setInitialZIndex(true);
						group.add(ObjectDragAction.fromObject(ao, old));
					}
				}
				if (group && group.length > 0)
					ApplicationController.currentActionTree.commit(group);
			}

			_startParent.setChildrenZPositions(authorDrag);  // account for loss of members at the previous parent

            ApplicationController.instance.authorController.editWhileRunningEnd();
			// trim wires that now cross the velum border  TODO: provide Undo support for this
			if (_applicationController.authorController.currentVelumMediator)
				SmartObjectController.trimStrayWires(_applicationController.authorController.currentVelumMediator.editingObject as SmartObject);

			// Delay message center creation until drag end for copied (option-drag) objects
			if (_copying) {
				var m:TaconiteModel;
				for each (m in context.controller.selection.selectedModels) {
					setComplete(m.value as AbstractObject);
				}
			}
			ao = _view.model.value as AbstractObject;
			if (Application.running && ao.enabled)
				_applicationController.wireController.sendTrigger(_view.model.value, Modifiers.instance.fetch("dragRelease"));

			
			_draggingNow = false;
			for each (var m2:TaconiteModel in context.controller.selection.selectedModels) {
				var obj:AbstractObject = (m2.value as AbstractObject);
				obj.dragging = false;
				obj.zIndex = obj.model.parent.getChildIndex(obj.model);
			}
			_applicationController.authorController.log(_copying ? "dragCopy" : "drag", context.controller.selection.selectedModels);


//            trace("-")
//            trace("startParent="+_startParent);
//            for each (var thing:AbstractObject in _startParent.objects) {
//                trace("   ", thing.title, "z="+thing.zIndex, "initZ="+thing.initialValues.zIndex);
//            }

			_startParent = null;
			_oldPositions = null;
			_oldLocations = null;
			_clickOffset = null;
			if (object as PathObject && !_copying) {
				_applicationController.authorController.selectSingleModel(object.model);
			}
		}
		
		private function setComplete(top:AbstractObject):void
		{
			top.complete = true;
            var container:AbstractContainer = top as AbstractContainer;
            if (container) {
                var objects:Vector.<AbstractObject> = container.allObjects();
                for each (var obj:AbstractObject in objects) {
                    obj.complete = true;
                }
            }
            PagerController.instance.activateObject(top,  true);
		}

		override public function updateViewPosition():void
		{
			ObjectView(_view).updateViewPosition();
		}

		private function handleDropIntoContainer(evt:DropIntoContainerEvent):void
		{
			dropIntoContainer(_view as ObjectView, evt.location);
		}
		
		public static function get draggingNow():Boolean
		{
			return _draggingNow;
		}
		
		public static function get copyingNow():Boolean
		{
			return _draggingNow && _copying;
		}

		private function objInList(list:Array, obj:AbstractObject):Boolean
		{
			for each (var info:DragInfo in list) {
				if (info.model.value == obj) {
					return true;
				}
			}
			return false;
		}


		///// view-based reparenting and coordinate functions  (static functions)

		public static function dropIntoContainer(view:ObjectView, stagePoint:Point=null):void
		{
			var acceptingView:ObjectView = findAcceptingView(view as ObjectView, stagePoint);
            trace("dropIntoContainer: acceptingView="+acceptingView);
			if(acceptingView) {
				changeParentView(view, acceptingView);
			}
		}

		public static function changeParentObject(subject:AbstractObject, newParent:AbstractObject):void
		{
			var context:ViewContext = Application.instance.viewContext;
			var view:ObjectView = subject.getView(context);
			var acceptingView:ObjectView = newParent.getView(context);
			changeParentView(view,acceptingView);
		}

		public static function changeParentView(view:ObjectView, acceptingView:ObjectView):void
		{
			var parent:TaconiteModel = acceptingView.model;
			var obj:AbstractObject = view.model.value as AbstractObject;
			trace("ObjectDragMediator dropping: "+obj, "  INTO  ", parent.value);
			var global:Point = view.stagePosition;

			// change the parent of the model
			view.model.changeParent(parent);
			obj.setStagePosition(global);
			
		}

		private static function findAcceptingView(subjectView:ObjectView, stagePoint:Point):ObjectView
		{
			if (subjectView as WorldView)
				return null;  // ignore attempt to reparent world

			if (stagePoint == null)
				stagePoint = subjectView.stagePosition;

			// let the views recursively find a view to accept this one
			var worldView:WorldView = WorldContainer(subjectView.context).worldView;
			var acceptingView:ObjectView = worldView.acceptChild(subjectView,stagePoint);
//            trace("findAcceptingView stagePoint="+stagePoint, "view="+acceptingView, "subject="+subjectView);
			
			// if none, stop here
			if(!acceptingView)
				return null;

//            if(subjectView.parentObjectView != acceptingView) {
//                trace("Want to move from ", subjectView.parentObjectView.object, "to", acceptingView.object);
//            }

			// while editing gadget, permit dragging object onto velum regardless of its Z-order position
			// but allow dragging object into an arena thats a child of the composite
			var editor:SmartObjectEditingMediator = ApplicationController.instance.authorController.currentVelumMediator;
			if (editor) {
				var view:ObjectView = (editor.editingObject as AbstractObject).getView();
				if (view && !view.contains(acceptingView) && view.hitTestPoint(stagePoint.x, stagePoint.y))
					return view;
			}
			
			// is the current parent willing to release the view?
			if (!_copying) {  // when making a copy, always release it
				var parentView:ObjectView = subjectView.parentObjectView;
				if (parentView && !parentView.releaseChild(subjectView, acceptingView, stagePoint)) {
					return null;
				}
			}

			if(acceptingView.model != subjectView.model.parent) {
				return acceptingView;
			} else {
				return null;
			}
		}

		private function getFirstSelectedObj():AbstractObject
		{
			if (_oldPositions.length == 0)
				return null;
			var oldPos:Object = _oldPositions[0];
			var model:TaconiteModel = oldPos.model;
			return AbstractObject(model.value);
		}

		private function get firstSelectedView():ObjectView
		{
			if (_oldPositions == null || _oldPositions.length == 0)
				return null;
			var oldPos:Object = _oldPositions[0];
			var model:TaconiteModel = oldPos.model;
			if (model != null)
				var obj:AbstractObject = model.value as AbstractObject;

			var view:ObjectView;
			if (obj != null)
				view = obj.getView(_context);
			return view;
		}


		public static function setStagePosition(view:ObjectView, stagePoint:Point):void
		{
			var local:Point = view.parent.globalToLocal(stagePoint);
			var obj:AbstractObject = view.model.value as AbstractObject;
			obj.x = local.x;
			obj.y = local.y;
		}

		private function createAnimationPath():PathObject
		{
			var obj:AbstractObject = getFirstSelectedObj();
			_oldSnapTo = obj.snapTo;
			obj.snapTo = null;  // release from old path so we can drag it
			var global:Point = obj.getView().stagePosition;
			var path:PathObject = _applicationController.authorController.addObjectForName("PathObject", 0,0, 0, 0, false) as PathObject;
			path.setupInspectorRibbons();
			var local:Point;

			path.model.changeParent(obj.parent.model);
			path.setInitialZIndex();
			local = path.getView().globalToLocal(global);
			path.addPoint(0, local, false);
			path.hideOnRun = true;
			path.excludeFromContainerLayouts = true;
			
			// make the object movable
			obj.movable = true;
			//MSG 731
            ApplicationController.instance.dispatchEvent(
				new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:731, inserts:[obj.title]}) );

			return path;
		}

		private function extendAnimationPath(path:PathObject, shift:Boolean):void
		{
			var obj:AbstractObject = getFirstSelectedObj();
			var global:Point = obj.getView().stagePosition;
			var local:Point = path.getView().globalToLocal(global);
			if (path.nodeCount==1 || !shift)  // when shift is held down, path will have only two points
				path.extendPath(local, shift);
			else
				path.setPoint(path.nodeCount-1, local);
		}

		private function finishAnimationPath(obj:AbstractObject, path:PathObject):void
		{
			path.finish();
			path.excludeFromContainerLayouts = false;
			obj.pathPercent = 100;
			obj.snapTo = path;
            obj.controller.objectCompleted(obj);
		}


		/////  handle mouse events for AbstractObject class, using object controller for _view.model

		private var _runtimeClick:Point;
		private var _runtimeDragStart:Boolean;  // detect 3-pixel breakaway on runtime clicks
		private var _controllerCache:IChildMediator;

		private function get controller():IChildMediator
		{
			if (!_controllerCache)
				_controllerCache = (_view.model.value as AbstractObject).controller as IChildMediator;
			return _controllerCache;
		}

		public function handleObjMouseDown(e:MouseEvent):Boolean
		{
			trace("ObjectDragMediator:handleObjMouseDown for", _view.model.value);
			_context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleObjMouseMove);
			_context.stage.addEventListener(MouseEvent.MOUSE_UP, handleObjMouseUp);
			_runtimeClick = new Point(e.stageX, e.stageY);
			_runtimeDragStart = false;
			
			return controller.handleMouseDown(_view, e);  // runtime click of button, etc.   Return true if drag allowed
		}
		
		private function handleObjMouseMove(e:MouseEvent):void
		{
			if (Application.running) {
				var delta:Point = new Point(e.stageX, e.stageY).subtract(_runtimeClick);
				if (Math.abs(delta.x) >= minimumDrag || Math.abs(delta.y) >= minimumDrag)
					_runtimeDragStart = true;
				controller.handleMouseMove(_view, e);
			}
		}
		
		public function handleObjMouseUp(e:MouseEvent):void
		{
			_context.stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleObjMouseMove);
			_context.stage.removeEventListener(MouseEvent.MOUSE_UP, handleObjMouseUp);

            trace("ObjectDragMediator:handleObjMouseUp", _upEventObject, e);
			if (_upEventObject === e) return;
            trace("ObjectDragMediator:handleObjMouseUp setting _upEvent");
			_upEventObject = e;
            var upView:ObjectView = WorldContainer(_context).viewUnderMouse;

            if(upView && (Application.running || upView.object.actLikeRunning || (upView.selected && !_clickedWhileUnselected))) {
                upView.object.controller.handleMouseUp(upView, e);
            }
					
			if ((Application.running || (_view.selected && !_clickedWhileUnselected))  ) {
				if (!_runtimeDragStart && !_dragStarted) {
					controller.handleClick(_view, e);
				}
			}
		}
		

		
		public function handleMouseOver(e:MouseEvent = null,view:ObjectView = null):void
		{
			if (view) {
				if (!_rolled && view == _view) {
					_rolled = true;
					view.object.transientPointerOver = true;
					controller.handleMouseOver(_view, e);
				}
			}
		}
		
		public function handleMouseOut(e:MouseEvent = null,view:ObjectView = null):void
		{			
			if (view) {
				if (_rolled && view == _view) {
					view.object.transientPointerOver = false;
					controller.handleMouseOut(_view, e);
					_rolled = false;
				}
			}
		}
				
		
		private function handleDoubleClick(e:MouseEvent):void
		{
			var stagePoint:Point = new Point(e.stageX, e.stageY);
			var objView:ObjectView = _view as ObjectView;
			if (!objView.exactHitTest(stagePoint)) {
				var worldView:ObjectView = WorldContainer(_context).worldView;
				var viewUnderPoint:ObjectView = worldView.findViewUnderPoint(stagePoint);

                // allow double-click on transparent area of bitmap to open a PathObject or Gadget for editing underneath the bitmap
				if (viewUnderPoint != null && viewUnderPoint != objView && viewUnderPoint != e.target) {
                    var outerView:ObjectView = getViewToSelect(viewUnderPoint);  // closed gadget containing the clicked object
                    if (outerView && outerView != e.target) {   // avoid dispatch to target (creates recursive loop)
                        e.stopImmediatePropagation();
                        outerView.dispatchEvent(e);  // send to the other object
                    }
				}
			} else {  // double click for this _view
				if (object.editingLocked && !object.childOfClosed) {  // select locked model, so author can unlock it
					_applicationController.authorController.selectSingleModel(_view.model);
					e.stopPropagation();
				} else {
					var action:int = objView.doubleClickAction;
					if (action == DoubleClickAction.STOP)
						e.stopPropagation();
				}
			}
		}
		
		
		
		
		/**
		 * Calls overrideable childStartDrag() on ContainerControllers.
         * This is restricted to ArenaPage children.
		 *  
		 * @param e
		 * 
		 */
		private function containerDragStart(e:MouseEvent):void
		{
			var containerController:ContainerController = controllerForChildDrag;
			if (containerController)
				containerController.childStartDrag(e, _viewBeingDragged);
		}
		
		/**
		 * Calls overrideable childMoveDrag() on ContainerControllers.
		 * 
		 * @param e
		 * 
		 */
		private function containerDragMove(e:MouseEvent):void
		{
			var containerController:ContainerController = controllerForChildDrag;
			if (containerController) {
				containerController.childMoveDrag(e, _viewBeingDragged);
			}
        }
		
		/**
		 * Calls overrideable childEndDrag() on ContainerControllers.
		 *  
		 * @param e
		 * 
		 */
		private function containerDragEnd(e:MouseEvent):void
		{
			var containerController:ContainerController = controllerForChildDrag;
			if (containerController)
				containerController.childEndDrag(e, _viewBeingDragged);
		}
		
		private function get controllerForChildDrag():ContainerController
		{
            if (_viewBeingDragged) {
                var obj:AbstractObject = _viewBeingDragged.object;
                if (obj && obj.parent) {
                    var arena:ArenaPage = obj.parent as ArenaPage;
                    var composite:Composite = obj.parent as Composite;
                    if (arena) {
                        return arena.controller as ContainerController;
                    } else if (composite) {
                        return composite.controller as ContainerController;
                    } else if (obj.parent is World) {
                        return obj.parent.controller as ContainerController;
                    }

                }
            }
			return null;
		}
		
		private function constrainToWorldAndArena(global:Point, view:ObjectView):Point
		{
			// constrain object to world for runlock, and to Arena when releasing=F
			var rect:Rectangle = view.getBounds(view.stage);
			var anchor:Point = view.localToGlobal(new Point(0,0));
			var oldCtr:Point = new Point(rect.left + rect.width/2, rect.top + rect.height/2);
			var offset:Point = oldCtr.subtract(anchor);
			var center:Point = global.add(offset);  // the requested center point

            var bounds:Rectangle;
            var worldView:WorldView = (_view.context as WorldContainer).worldView;
         	var world:World = worldView.world;

            var dx:Number = 0;
        	var dy:Number = 0;

            var parentView:ObjectView = view.parentObjectView;

            if(parentView is BackstageView && (view is IPageView) == false) {
                // This is an item on the backstage of the pager
                var offsetVector:Point = worldView.flowView.moveOutsideBounds(center, true);
                return global.add(offsetVector);
            }

			// set bounds to world, if runlock
			if (Application.runningLocked) {
				var worldOrigin:Point = worldView.localToGlobal(new Point(0,0));
				var worldFrame:Point = worldView.localToGlobal(new Point(world.width,world.height)).subtract(worldOrigin);
				bounds = new Rectangle(worldOrigin.x, worldOrigin.y, worldFrame.x, worldFrame.y);
			}
			// intersect with arena bounds, if releasing=F
			if (parentView is ArenaPageView) {
				var arenaView:ArenaView = parentView.parentObjectView as ArenaView;
				if (arenaView) {
					var arena:Arena = arenaView.object as Arena;
					if (arena && (arena.releasing == false || view.object.acceptIntoOutOfArena == false)) {
						var abounds:Rectangle = arenaView.getBoundsForChildren(view.stage);
						abounds.inflate(-rect.width/2, -rect.height/2);  // keep arena child completely inside
						if (abounds.width < 0) {   // normalize rect: for child that's larger than arena
							abounds.x += abounds.width;
							abounds.width = -abounds.width;
						}
						if (abounds.height < 0) {
							abounds.y += abounds.height;
							abounds.height = -abounds.height;
						}
							
						if (bounds)
							bounds = bounds.intersection(abounds);
						else
							bounds = abounds;
					}
				}
			}
			
			if (bounds == null) {
				return global; 
			} else {  // force obj center to be within "bounds"
				if (bounds.left > center.x)
					dx = bounds.left - center.x;
				if (bounds.right < center.x)
					dx = bounds.right - center.x;
				
				if (bounds.top > center.y)
					dy = bounds.top - center.y;
				if (bounds.bottom < center.y)
					dy = bounds.bottom - center.y;
				
				return new Point(global.x + dx, global.y + dy);
			}
		}

    }
}
