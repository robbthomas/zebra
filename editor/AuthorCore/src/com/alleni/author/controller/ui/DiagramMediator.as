package com.alleni.author.controller.ui
{
	import com.alleni.author.controller.ui.palettes.ToolboxController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.objects.AbstractContainer;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class DiagramMediator extends AbstractObjectDragMediator
	{

		public function DiagramMediator(context:ViewContext)
		{
			super(context,false);  // stopPropagation=false, so you can edit truthTable fill
		}
		
		
		/**
		 * When asked to work with a view, take note of the view and add a listener for mouseDown.
		 */
		override public function handleViewEvents(view:ITaconiteView, role:uint):AbstractObjectDragMediator
		{
			_view = ObjectView(view);
			_view.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			
			return this;
		}
		
		/**
		 * At the start of a drag, capture the positions of all selected objects so that we
		 * can move them all by the same delta later on.
		 */
		override protected function handleDragStart(e:MouseEvent):void
		{
			if (_dragAllowed && Application.instance.currentTool == ToolboxController.ARROW) {
				_oldPositions = [];
				for each (var model:TaconiteModel in context.controller.selection.selectedModels)
				{
					model.value["complete"] = true;
					_oldPositions.push(new Point(model.value["thumbnailScale"], model.value["thumbnailScale"]));
				}
			}
		}		
		/**
		 * For each move during the drag, scale the models appropriately given the drag distance
		 * from the starting point.
		 */
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
		{
			if (_dragAllowed) {
				var i:int = 0;
				for each (var model:TaconiteModel in context.controller.selection.selectedModels) {
					var newScale:Number = (Point(_oldPositions[i++]).y-documentDragDelta.y/20)*1.2;
					if (newScale < 1) newScale = 1;
					if (newScale > 10) newScale = 10;
					model.value["thumbnailScale"] = newScale;
				}
			}
		}
		
		
		override protected function handleDragEnd(e:MouseEvent) : void
		{
			super.handleDragEnd(e);
			
			Application.instance.draggingMessageCenter = false;
			
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_CURSOR));
		}
		
		// Position the object view when dragging it's message center
		override public function updateViewPosition() : void
		{
			// do not move thumbnails of containers, because that would cause all children thumbnails to move
			if (!(object is AbstractContainer)) {
				_view.x = object.xDiagram;
				_view.y = object.yDiagram;
			}
		}
		
		override public function handleMouseDown(e:MouseEvent):void
		{
			if(_context.stage == null) {
				return;
			}
			_dragStarted = false;
			_dragPoint = new Point(WorldContainer(_context).mouseX, WorldContainer(_context).mouseY);
			_context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_context.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			if(_stopPropagation) {
				e.stopPropagation();
			}
		}
		
		override protected function handleMouseMove(e:MouseEvent):void
		{
			if (!_dragStarted
				&& e.buttonDown
				&& _dragPoint != null
				&& (Math.abs(WorldContainer(_context).mouseX -_dragPoint.x) >= minimumDrag
					|| Math.abs(WorldContainer(_context).mouseY -_dragPoint.y) >= minimumDrag))
			{
				_dragStarted = true;
				handleDragStart(e);
			}
			
			if (_dragStarted)
			{
				handleDragMove(e);
			}
			if(_stopPropagation) {
				e.stopPropagation();
			}
		}
		
		override protected function get documentDragDelta():Point
		{
			return  new Point(WorldContainer(_context).mouseX, WorldContainer(_context).mouseY).subtract(_dragPoint);
		}
		
		
		
		override public function clearViewEvents():void
		{
			_view.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		}

	}
}