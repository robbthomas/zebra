package com.alleni.author.controller.ui
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.controller.app.CursorController;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.objects.AuthorNoteView;
	import com.alleni.taconite.controller.DragMediator;
	import com.alleni.taconite.controller.TaconiteController;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.MouseCursor;
	
	public class AuthorNoteMediator extends DragMediator
	{
		
		private var _role:uint;
		private var _authorNoteView:AuthorNoteView;
		private var _oldPositions:Array;  /* of points */
		
		public function AuthorNoteMediator(context:ViewContext)
		{
			super(context);
		}
		
		/**
		 * When asked to work with a ObjectView, take note of the view and add a listener for mouseDown.
		 */
		public function handleViewEvents(view:AuthorNoteView, role:uint):void
		{
			_authorNoteView = view;
			_authorNoteView.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			_role = role;
            ApplicationController.addEventListener(NotificationNamesEditor.UPDATE_NOTE_VISIBILITY, view.updatePositionAndVisibility);
		}
		
		override protected function get documentDragDelta():Point
		{
			return  new Point(WorldContainer(_context).mouseX, WorldContainer(_context).mouseY).subtract(_dragPoint);
		}
		
		override public function handleMouseDown(e:MouseEvent):void
		{
			if(_context.stage == null) {
				return;
			}
			_dragStarted = false;
			
			_dragPoint = new Point(WorldContainer(_context).mouseX, WorldContainer(_context).mouseY);
			_context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_context.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			if(_stopPropagation) {
				e.stopPropagation();
			}
		}

		
		override protected function handleMouseMove(e:MouseEvent):void
		{
			var mouseX:Number = WorldContainer(_context).mouseX;
			var mouseY:Number = WorldContainer(_context).mouseY;
			trace("mouseMove: dragPoint="+_dragPoint, "mouse=",mouseX,mouseY, "started="+_dragStarted, "button="+e.buttonDown, "oldPos="+_oldPositions);
			
			if (!_dragStarted
				&& e.buttonDown
				&& _dragPoint != null
				&& (Math.abs(mouseX -_dragPoint.x) >= minimumDrag
					|| Math.abs(mouseY -_dragPoint.y) >= minimumDrag))
			{
				_dragStarted = true;
				handleDragStart(e);
			}
			
			if (_dragStarted)
			{
				handleDragMove(e);
			}
			if(_stopPropagation) {
				e.stopPropagation();
			}
		}

		
		/**
		 * At the start of a drag, capture the positions of all selected objects so that we
		 * can move them all by the same delta later on.
		 */
		override protected function handleDragStart(e:MouseEvent):void
		{
			if (_role == ViewRoles.PRIMARY) {

				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.CHANGE_CURSOR_TO, {name:MouseCursor.HAND, type:CursorController.CURSOR_TYPE_STANDARD}));
				
				if (!context.controller.selection.contains(_authorNoteView.model))
				{
					context.controller.selectSingleModel(_authorNoteView.model);
				}
				
				_oldPositions = [];
				for each (var m:TaconiteModel in context.controller.selection.selectedModels) {
					var objView:ObjectView = AbstractObject(m.value).getView(_context);
					_oldPositions.push(objView.computeAuthorNotePosition());  // topLeft of authornote in stage coordinates
				}
			}
		}

		/**
		 * For each move during the drag, position the models appropriately given the drag distance
		 * from the starting point.
		 */
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
		{
			if (_role == ViewRoles.PRIMARY) {
				var i:int = 0;
				for each (var m:TaconiteModel in context.controller.selection.selectedModels)
				{
					var oldPoint:Point = _oldPositions[i++];
					adjustNotePosition(m.value as AbstractObject, oldPoint, documentDragDelta);
				}
				
				_authorNoteView.stage.invalidate();  // trigger the render handler
			}
		}
		
		private function adjustNotePosition(obj:AbstractObject, oldPoint:Point, delta:Point):void
		{
			var stagePoint:Point = oldPoint.add(delta);
			var objView:ObjectView = obj.getView(_context);
			var local:Point = objView.globalToLocal(stagePoint);
			obj.xAuthorNote = local.x - obj.left;
			obj.yAuthorNote = local.y - obj.top;
		}
		
		public function clearViewEvents():void
		{
			_authorNoteView.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		}
	}
}