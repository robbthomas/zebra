/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller.ui
{
	import com.alleni.author.controller.objects.RichTextController;
	import com.alleni.author.view.text.IRichEditor;
	
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import com.alleni.author.definition.text.ActionMap;

	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	import com.alleni.author.model.ui.Application;
	

	import flashx.textLayout.container.ContainerController;

	public class RichContainerController extends ContainerController
	{
		private var _keyHandler:Function;
		public var captureEnter:Boolean = false;
		
		public function RichContainerController(arg0:Sprite, arg1:Number=100, arg2:Number=100)
		{
			super(arg0, arg1, arg2);
		}
		
		
		override public function keyDownHandler(event:KeyboardEvent) : void
		{
			
			if(event.target as IRichEditor){
				
				var editor:IRichEditor = event.target as IRichEditor;
				
				
				var textController:RichTextController = editor.textController as RichTextController;
				
				if(editor.formattable){
					
					if(event.ctrlKey){  
					
						if (event.keyCode == Keyboard.B)
							textController.perform(ActionMap.BOLD_ACTION);
					
						if (event.keyCode == Keyboard.I)
							textController.perform(ActionMap.ITALIC_ACTION);
					
						if (event.keyCode == Keyboard.U)
							textController.perform(ActionMap.UNDERLINE_ACTION);
					
						event.stopImmediatePropagation();
					}
					
				}
				
				
			}
				
		
			if (_keyHandler != null) {
				if (_keyHandler(event))  // returns true to ignore key
					return;
			}

			// if we're capturing enter, we can't pass it to the supercontroller, because that will insert it as
			// a carriage return. we want to capture it, likely to use it to cancel out of editing mode.
			if (!captureEnter || event.keyCode != 13)
				super.keyDownHandler(event);
		}
		
		public function set keyHandler(value:Function):void
		{
			_keyHandler = value;	
		}
		
		public function get keyHandler():Function
		{
			return _keyHandler;
		}
	}
}