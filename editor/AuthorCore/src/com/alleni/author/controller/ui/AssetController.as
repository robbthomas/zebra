package com.alleni.author.controller.ui
{
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ProgressBarController;
import com.alleni.author.definition.AssetDescription;
import com.alleni.author.definition.AssetLoadingReport;
import com.alleni.author.definition.AssetType;
import com.alleni.author.definition.application.ApplicationStrings;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.event.AssetEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.AbstractExternalObject;
import com.alleni.author.model.objects.MediaObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.Assets;
import com.alleni.author.service.assets.AssetLoadOperation;
import com.alleni.author.service.assets.AssetSaveOperation;
import com.alleni.author.util.Set;
import com.alleni.author.util.cache.LRUCache;
import com.alleni.taconite.controller.ITaconiteController;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.factory.IEnvironment;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.service.IOperation;
import com.alleni.taconite.service.LogService;

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.ProgressEvent;
import flash.geom.Point;

import mx.collections.ArrayList;
import mx.collections.IList;
import mx.events.PropertyChangeEvent;
import mx.events.PropertyChangeEventKind;

public class AssetController extends EventDispatcher
	{
		private static var _instance:AssetController;

		private var _controller:AuthorController;
		
		public var queueProgressFraction:Number = 0;
		private var _requestedQueue:Vector.<Asset>;
		private var _requestedWithContentQueue:Vector.<Asset>;
        private var _requestedPreviewImageQueue:Vector.<Asset>;
		private var _assetCache:LRUCache;

		
		public function AssetController()
		{
			Utilities.assert(!_instance);
			_requestedQueue = new Vector.<Asset>();
			_requestedWithContentQueue = new Vector.<Asset>();
            _requestedPreviewImageQueue = new Vector.<Asset>();
			_assetCache = new LRUCache(50, 200);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOADING, handleProjectLoading);
		}
		
		public static function get instance():AssetController
		{
			if (!_instance)
				_instance = new AssetController();
			return _instance;
		}
		
		/**
		 * cache and clear deprecated items 
		 * @param event
		 * 
		 */
		private function handleProjectLoading(event:Event):void
		{
			if (!model) return;
			while (model.assets.length > 0)
				removeFromModel(model.assets[0]);
		}
		
		public function get freshModel():Assets
		{
			if (model)
				return model;
			return new Assets();
		}

		public function set controller(value:ITaconiteController):void
		{
			_controller = value as AuthorController;
		}

        public function requestAssetsForSet(assetSet:Set):Boolean
        {
			var remainingCount = 0;
			if (assetSet != null) {
				var size:int = assetSet.size;
				if (size > 0) {
					var asset:Asset;
					var assetArray:Array = assetSet.allItems;

	           		for each (asset in assetArray) {
		            	if (asset.content == null) {
		            		remainingCount++;
		                    if (!isWaitingForContent(asset)) {
		                        requestForID(asset.assetID, -1, true);
		                    } // else it must already have been requested
		            	}
		            }
		        }
		    }
		    return remainingCount == 0;
        }

        public function assetsForContainer(container:AbstractContainer, preloadOnly:Boolean=true, leftToLoadOnly:Boolean=false):Set
        {
            var assetSet:Set = new Set(false);
            var ao:AbstractObject;
            var mo:MediaObject;
            var needsPreload:Boolean = false;
            var leftToLoad:Boolean = false;
            var asset:Asset;
            var objs:Vector.<AbstractObject> = container.allObjects();
            for each (ao in objs) {
                var aeo:AbstractExternalObject = ao as AbstractExternalObject;
                if (aeo != null) {
                	asset = aeo.asset;
                	mo = aeo as MediaObject;
                	needsPreload = (mo == null || mo.preload);
					leftToLoad = !leftToLoadOnly || (asset != null && asset.content == null) || asset == null;
                    if (asset != null && Utilities.UUID_REGEX.test(asset.assetID)
                    		&& (!preloadOnly || (needsPreload && leftToLoad))) {
                		assetSet.add(aeo.asset);
                    }
                }
            }
            return assetSet;
        }

		public function attachFor(obj:AbstractExternalObject, asset:Asset):void
		{
			if (obj == null || obj.asset && obj.asset === asset)
				return;

            addToModel(asset);
			if (obj.asset) { // detachment of old asset
				if (obj.replaceablePropertyChangeListener != null)
					obj.asset.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, obj.replaceablePropertyChangeListener);
				obj.replaceablePropertyChangeListener = null;
			}
			
			// attachment of new asset
			obj.asset = asset;
			obj.replaceablePropertyChangeListener = function(event:PropertyChangeEvent):void {
				obj.dispatchEvent(new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE, false, false, PropertyChangeEventKind.UPDATE, "asset", asset, asset));
			}
			asset.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, obj.replaceablePropertyChangeListener);
		}
		

		public function addToModel(asset:Asset, index:int=-1):Boolean
		{
			if (!model.getById(asset.assetID)) {
				if (index < 0 || index > model.assets.length)
					model.assets.addItem(asset);
				else
					model.assets.addItemAt(asset, index);
				return true;
			}
			return false;
		}
		
		public function removeFromModel(asset:Asset):Boolean
		{
			if (!asset)
				return false;
			var index:int = model.assets.getItemIndex(asset);
			if (index >= 0) {
				model.assets.removeItemAt(index);
				_assetCache.setValue(asset.assetID, asset);
				return true;
			}
			return false;
		}

        public function addToPreviewQueue(asset:Asset):Boolean
        {
            if (requestedCount == 0) {
         	    queueProgressFraction = 0;
            }
            if(_requestedPreviewImageQueue.indexOf(asset) > -1) {
                return false;
            }
            var assetLoadOperation:AssetLoadOperation = new AssetLoadOperation(asset);
            assetLoadOperation.dataOnly = true;
            assetLoadOperation.addEventListener(Event.COMPLETE, previewFinishHandler);
            assetLoadOperation.addEventListener(AssetEvent.ERROR, previewFinishHandler);
            assetLoadOperation.execute();
            return true;
        }

        private function previewFinishHandler(event:Event):void {
            var op:AssetLoadOperation = event.target as AssetLoadOperation;
            if(_requestedPreviewImageQueue.indexOf(op.asset) > -1) {
                op.removeEventListener(Event.COMPLETE, previewFinishHandler);
                op.removeEventListener(AssetEvent.ERROR, previewFinishHandler);
                _requestedPreviewImageQueue.splice(_requestedPreviewImageQueue.indexOf(op.asset), 1);
            }
        }
		
		private function addToQueue(asset:Asset, contentOnly:Boolean=false):Boolean
		{
			if (requestedCount == 0)
				queueProgressFraction = 0;
			if (contentOnly) {
				if (_requestedWithContentQueue.indexOf(asset) > -1)
					return false;
				_requestedWithContentQueue.push(asset);
			} else {
				if (_requestedQueue.indexOf(asset) > -1)
					return false;
				_requestedQueue.push(asset);
			}
			return true;
		}
		
		private function verifyRemovalFromQueue(asset:Asset, fromError:Boolean=false):Boolean
		{
			if (_requestedQueue.indexOf(asset) > -1) {
				if (asset.loadOperation)
					asset.loadOperation.removeEventListener(ProgressEvent.PROGRESS, handleLoadProgress);
				asset.loadOperation = null;
				_requestedQueue.splice(_requestedQueue.indexOf(asset), 1);
				return true;
			}	
			if (_requestedWithContentQueue.indexOf(asset) > -1) {
				if (asset.content || fromError) {
					if (asset.loadOperation)
						asset.loadOperation.removeEventListener(ProgressEvent.PROGRESS, handleLoadProgress);
					asset.loadOperation = null;
					_requestedWithContentQueue.splice(_requestedWithContentQueue.indexOf(asset), 1);
					return true;
				}
			}
			return false;
		}
		
		public function isWaitingForContent(asset:Asset):Boolean
		{
			return (_requestedWithContentQueue.indexOf(asset) > -1);
		}

		private static function addOperationListeners(operation:IOperation, completeHandler:Function):void
		{
			operation.addEventListener(AssetEvent.ERROR, handleError);
			operation.addEventListener(AssetEvent.COMPLETE, function(event:Event):void {
				operation.removeEventListener(event.type, arguments.callee);
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE));
				completeHandler(event);
			});
		}
		
		private static function handleError(e:AssetEvent):void
		{
			var operation:IOperation = e.target as IOperation;
			if (operation != null) {
				operation.removeEventListener(AssetEvent.ERROR, handleError);
			}
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE));
			var asset:Asset = e.asset;
			if (asset == null) {
				LogService.error("Handling error for asset=null");
				return;
			}
			if (instance.verifyRemovalFromQueue(asset, true))
				LogService.error("Handling error for asset '"+asset.assetID+"': removed from loading queue");
			else
				LogService.error("Handling error for asset '"+asset.assetID+"': was not on loading queue");
			if (instance.removeFromModel(asset))
				LogService.error("Handling error for asset '"+asset.assetID+"': removed from assets model");
			else
				LogService.error("Handling error for asset '"+asset.assetID+"': was not in assets model");
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.ASSET_REQUEST_PROGRESS, asset));
		
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, 
				["Sorry...", e.data]));

		}

		public function insertFromBitmap(data:*, localX:Number=0, localY:Number=0, name:String=null):void
		{
			insertWithDataAndType(data, localX, localY, AssetType.BITMAP, ".png", name);
		}
		
		public function insertFromSVG(data:*, localX:Number=0, localY:Number=0, name:String=null):void
		{
			insertWithDataAndType(data, localX, localY, AssetType.SVG, ".svg", name);
		}
		
		public function insertFromText(data:*, localX:Number=0, localY:Number=0, name:String=null):void
		{
			insertWithDataAndType(data, localX, localY, AssetType.TEXT, ".txt", name);
		}
		
		public function insertFromURL(url:String, localX:Number=0, localY:Number=0, name:String=null):void
		{
			if (name == null)
				name = Utilities.FILENAME_FROM_URL_REGEX.exec(url);
			
			// TODO: discern correct type data from URL to reenable pasting from URL which worked in A-Series
			insertWithDataAndType(url, localX, localY, AssetType.TEXT, ".txt", name);
		}
		
		public function importAndReportBackOnComplete(name:String, data:*, completeHandler:Function):void
		{
			var assetSaveOperation:AssetSaveOperation = new AssetSaveOperation(null, name, data);
			addOperationListeners(assetSaveOperation, completeHandler);
			assetSaveOperation.execute();
		}
		
		private function insertWithDataAndType(data:*, localX:Number, localY:Number, type:int, extension:String, name:String=null):void
		{
			if (name == null)	
				name = Utilities.uniqueDateBasedName + extension;
			
			var assetSaveOperation:AssetSaveOperation = new AssetSaveOperation(null, name, data, type);
			addOperationListeners(assetSaveOperation, handleComplete);
		}
		
		private function handleComplete(e:AssetEvent):void
		{
            e.target.removeEventListener(e.type, arguments.callee);
            var asset:Asset = e.asset;
            if (asset && Utilities.UUID_REGEX.exec(asset.assetID)) {
                asset.upToDate = true;

                addToModel(asset);
                if (verifyRemovalFromQueue(asset))
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.ASSET_REQUEST_PROGRESS, asset));
            }
		}

		public function requestForID(id:String, index:int=-1, preloadMedia:Boolean=true, existingAsset:Asset = null):Asset
		{
            if (model == null)
                return existingAsset;

			var asset:Asset = model.getById(id);
            if(asset == null){
                asset = (_assetCache.getValue(id) as Asset);
            }
            if(asset == null) {
                asset = existingAsset;
            }

			if (asset) {
				if (!preloadMedia || asset.content)
					return asset;
					
				if (_requestedQueue.indexOf(asset) > -1)
					_requestedQueue.splice(_requestedQueue.indexOf(asset), 1); // pull it off to make sure the content request is accounted for
			} else {
				asset = new Asset(id);
				addToModel(asset, index);
			}
			
			if (addToQueue(asset, preloadMedia)) {
				asset.loadOperation = load(asset, preloadMedia);
				asset.loadOperation.addEventListener(ProgressEvent.PROGRESS, handleLoadProgress);
			}
			return asset;
		}

        public function loadContent(asset:Asset, onComplete:Function = null):void
        {
            if (asset.content != null) {
                if (onComplete != null) {
                    onComplete(asset);
                    return;
                }
            }
            var op:IOperation = load(asset, true);
            op.addEventListener(Event.COMPLETE, function(e:Event):void {
                if (onComplete != null) {
                    onComplete(asset);
                }
            });
            op.addEventListener(AssetEvent.ERROR, function(e:Event):void {
                if (onComplete != null) {
                    onComplete(null);  // asset=null signals failure
                }
            });
        }
		
		private function handleLoadProgress(e:ProgressEvent):void
		{
			var asset:Asset;
            var op:AssetLoadOperation = e.target as AssetLoadOperation;
            if (op != null) {
                asset = op.asset;
            } else {
                asset = null;
            }
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.ASSET_REQUEST_PROGRESS, asset));
		}

		public function getQueueAsSet():Set {
			var asset:Asset;
			var set:Set = new Set(false);
			var queue:Vector.<Asset>;
			for each (queue in allQueues) {
				for each (asset in queue) {
					set.add(asset);
				}
			}
			return set;
		}

		private function get allQueues():Array {
			return [_requestedQueue, _requestedWithContentQueue, _requestedPreviewImageQueue];
		}
		
		public function get requestedCount():uint
		{
			var count:int = 0;
			var queue:Vector.<Asset>;
			for each (queue in allQueues) {
				count += queue.length;
			}
			return count;
		}

		public function getLoadProgressForSet(assetSet:Set):AssetLoadingReport {
			var progressFraction:Number = 0;
			var remainingCount = assetSet.size;
			if (assetSet != null) {
				var size:int = assetSet.size;
				if (size > 0) {
					var asset:Asset;
					var assetArray:Array = assetSet.allItems;

	           		for each (asset in assetArray) {
						var op:IOperation = asset.loadOperation;
						if (op != null) {
							progressFraction += op.progressFraction;
						} else {
							progressFraction += 1.0;
							remainingCount--;
						}
					}
					progressFraction /= size;
				}
			}
			var report:AssetLoadingReport = new AssetLoadingReport(remainingCount, progressFraction);
			return report;
		}

		public function showLoadProgressFor(event:ApplicationEvent, count:int, progressFraction:Number):void {
            var phaseName:String = ApplicationStrings.LOADING + " ";
            if (count > 1) {
                phaseName += count + " assets";
            } else {
                var asset:Asset = event.data as Asset;
                phaseName += '"' + asset.name + '"';
            }
            ProgressBarController.instance.setPhaseName(phaseName, true);
            ProgressBarController.instance.assetLoadFraction = progressFraction;
        }
		
        public function getExistingAsset(id:String):Asset
  		{
              var asset:Asset = model.getById(id);
              if(asset == null){
                  asset = (_assetCache.getValue(id) as Asset);
              }
              return asset;
  		}

        public function existsInModel(asset:Asset):Boolean
  		{
            return (model.assets.getItemIndex(asset) >= 0);
  		}

		private function load(asset:Asset, preloadMedia:Boolean=true):IOperation
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.ASSET_REQUESTED, asset.id));
			
			var name:String = "";
			if (asset.name != null)
				name = asset.name;
			
			var assetLoadOperation:AssetLoadOperation = new AssetLoadOperation(asset, preloadMedia);
			assetLoadOperation.displayName = asset.name != "" ? "Loading asset "+asset.name+"..." : "Loading assets...";
			addOperationListeners(assetLoadOperation, handleComplete);
            assetLoadOperation.execute();
			return assetLoadOperation;
		}
		
		public function get model():Assets
		{
			if (!Application.instance.document) return null;
			return Application.instance.document.assetsRoot.value as Assets;	
		}
		
		/**
		 * available for reuse. This list may not include certain assets which cannot be reused, such as within private gadgets. 
		 * @return 
		 * 
		 */
		public function get availableModel():IList
		{
			var availableItems:ArrayList = new ArrayList();
			const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			var asset:Asset;
			for each (asset in model.assets) {
				/*trace("asset: " + asset.accountId + " vs. me: " + environment.accountId);
				if (!asset.accountId || asset.accountId == environment.accountId)*/
					availableItems.addItem(asset);
			}
			return availableItems;
		}
		
		public function configureContainerAndDisplay(asset:Asset, dropX:Number=NaN, dropY:Number=NaN, parent:AbstractContainer=null):AbstractExternalObject
		{
			var shortClassName:String = AssetType.getContainerName(asset.type);
			if (!shortClassName) return null;
		
			// Create the object for this asset
			
			var defaultSize:Point;
			switch (shortClassName) {
				case "VideoObject":
				case "TextObject":
					defaultSize = AssetDescription.DEFAULT_VIDEO_SIZE;
					break;
				default:
					defaultSize = new Point(0, 0);
					break;
			}
			
			if (isNaN(dropX)) dropX = 0;
			if (isNaN(dropY)) dropY = (Application.instance.document.root.value as World).height/2;
			
			var objectForAsset:AbstractExternalObject = _controller.addObjectForName(shortClassName, dropX, dropY,
					defaultSize.x, defaultSize.y, false, parent) as AbstractExternalObject;
			objectForAsset.replaceable = asset;

			objectForAsset.controller.handleCreationInitialize(objectForAsset);
			objectForAsset.controller.handleCreationComplete(objectForAsset, new Point(dropX, dropY));
            if(Application.running) {
                objectForAsset.onResume();
            }
			return objectForAsset;
		}
		
		// left for posterity when we need to handle clipboard paste
		//  probably best elsewhere, but it happened to have originated here!
		/*private static function testClipboardType(e:MouseEvent):int
		{
		var clipboard:ISystemClipboard = TaconiteFactory.getClipboardImplementation(e);
		
		if (draggedAsset) {
		return draggedAsset.type;
		} else {
		if (clipboard.hasFormat(clipboard.FILE_LIST_FORMAT)) {
		return AssetDescription.validateFileType((clipboard.getData(clipboard.FILE_LIST_FORMAT) as Array)[0].extension);
		} else if (!clipboard.hasFormat(clipboard.FILE_LIST_FORMAT) && clipboard.hasFormat(clipboard.URL_FORMAT)) {
		return AssetDescription.validateFileType(clipboard.getData(clipboard.URL_FORMAT) as String);
		} else {
		if (clipboard.hasFormat(clipboard.TEXT_FORMAT)) {
		return -1;
		} else if (clipboard.hasFormat(clipboard.BITMAP_FORMAT)) {
		return AssetType.IMAGE;
		} else if (clipboard.hasFormat(clipboard.HTML_FORMAT)) {
		return -1;
		} else {
		// unsupported clipboard contents
		}
		}
		}
		return -1;
		}*/
	}
}
