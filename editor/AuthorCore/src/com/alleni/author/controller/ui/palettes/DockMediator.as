package com.alleni.author.controller.ui.palettes
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.GadgetController;
	import com.alleni.author.model.objects.IListable;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Dock;
	import com.alleni.author.model.gadgets.Gadgets;
	import com.alleni.author.util.cache.LRUCache;
	import com.alleni.author.view.ui.DropFeedback;
	import com.alleni.author.view.ui.palettes.DockDetailsView;
	import com.alleni.author.view.ui.palettes.DockItemView;
	import com.alleni.author.view.ui.palettes.PopoverView;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filters.GlowFilter;
	
	import mx.events.PropertyChangeEvent;

	public class DockMediator
	{
		private var _view:DisplayObjectContainer;
		private var _itemCache:LRUCache;
		private var _detailsView:DockDetailsView;
		
		private static var _glow:GlowFilter;
		
		public function DockMediator()
		{
		}
		
		public function handleSelectionEvents(view:DisplayObjectContainer, model:EventDispatcher, itemCache:LRUCache):void
		{
			_view = view;
			_itemCache = itemCache;
			
			model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
		}
		
		private function handlePropertyChange(event:PropertyChangeEvent):void
		{
			switch(event.property) {
				case "lastSuccessfullyPublished": // if possible, propagate just-published values
					if (event.newValue == null) // a publish operation may be is in progress.
						break;
					var gadgetJustPublished:Project = event.newValue as Project;
					
					var gadgets:Gadgets;
					var gadget:Project;
					gadgets = Dock.instance.pageList;
					if (gadgets) {
						gadget = gadgets.getById(gadgetJustPublished.id);
						if (!gadget) return;
						gadget.copyFromPublished(gadgetJustPublished);
					}
					gadgets = GadgetController.instance.model;
					if (gadgets) {
						gadget = gadgets.getById(gadgetJustPublished.id);
						if (!gadget) return;
						gadget.copyFromPublished(gadgetJustPublished);
					}
				case "pageList":
					if (Dock.instance.lastSuccessfullyPublished == null // a publish operation may be is in progress.
						|| !Dock.instance.pageList)  // the page list is null. the dock is not yet open.
						break;
					var count:int = 0;
					const limit:int = 20;
					_view.addEventListener(Event.RENDER, function():void {
						if (!Dock.instance.lastSuccessfullyPublished) return;
						var view:DockItemView = _itemCache.getValue(Dock.instance.lastSuccessfullyPublished.projectId) as DockItemView;
						if (!view) return;
						var viewToHighlight:DisplayObject = view.draggable;
						
						if (viewToHighlight) {
							// fade-in glow
							glow.alpha = 0;
							Tweener.addTween(glow, {alpha:1, time:0.5, transition:EditorUI.ANIMATION_IN_ALGORITHM
								,onUpdate:function():void {
									viewToHighlight.filters = [glow];
								}
								,onComplete:function():void {
									Tweener.addTween(glow, {alpha:0, time:0.5, transition:EditorUI.ANIMATION_IN_ALGORITHM
										,onUpdate:function():void {
											viewToHighlight.filters = [glow];
										}
										,onComplete:function():void {
											viewToHighlight.filters = DockItemMediator.baseFilters;
										}
									});
								}
							});
						}
						
						if (viewToHighlight || count > limit)
							_view.removeEventListener(Event.RENDER, arguments.callee);
						count++;
					});
					break;
				case "selectedItem":
					selectedItem = event.newValue as IListable;
					break;
				default:
					break;
			}
		}
		
		private function set selectedItem(value:IListable):void
		{
			if (!value) {
				if (_detailsView) _detailsView.visible = false;
				return;
			}
			
			var id:String;
			if ("publishedId" in value)
				id = value["publishedId"];
			else id = value.id;
			
			var detailsTargetView:DisplayObject = _itemCache.getValue(id) as DockItemView;
			if (_detailsView) {
				if (!_view.contains(_detailsView))
					_view.addChild(_detailsView);
				_detailsView.target = detailsTargetView;
			} else {
				_detailsView = new DockDetailsView(detailsTargetView, "selectedItem", PopoverView.POINT_DOWN|PopoverView.POINT_UP);
				_view.addChild(_detailsView);
			}
		}
		
		private static function get glow():GlowFilter
		{
			if (!_glow)
				_glow = new GlowFilter(DropFeedback.COLOR, 0.9, 4, 4, 4, 3);
			return _glow;
		}
	}
}