package com.alleni.author.controller.ui
{
import com.alleni.author.definition.GadgetDescription;
import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.event.GadgetEvent;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Dock;
    import com.alleni.author.service.gadgets.GadgetCommerceOperation;
import com.alleni.author.util.StringUtils;
import com.alleni.author.view.ui.controls.Alert;
    import flash.events.ErrorEvent;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.service.AbstractOperation;
	import com.alleni.taconite.service.IOperation;

	import flash.events.Event;
	
	import mx.events.CloseEvent;

	public class CommerceController
	{
		private static var _instance:CommerceController;
		
		public function CommerceController()
		{
			Utilities.assert(!_instance);
		}
		
		public static function get instance():CommerceController
		{
			if (!_instance)
				_instance = new CommerceController();
			return _instance;
		}
		
		public function validateGadgetPurchase(gadget:Project):IOperation
		{
			var validationOperation:IOperation = new AbstractOperation();
			
			if (GadgetController.instance.confirmOwnership(gadget)) {
				return null;
			} else {
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:47, closeFunction:handleGadgetPurchaseConfirmation(gadget, validationOperation), inserts:[gadget.publishedName, StringUtils.truncate(gadget.description,125,"...",12), gadget.price>0?GadgetDescription.prettyPrice(gadget.price):"Free", gadget.propertyType]}));
			}
			return validationOperation;
		}
		
		private function handleGadgetPurchaseConfirmation(gadget:Project, validationOperation:IOperation):Function
		{
			return function(event:CloseEvent):void
			{
				switch (event.detail) {
                   case Alert.FIRST:
						var commerceOperation:GadgetCommerceOperation = new GadgetCommerceOperation(gadget);
						commerceOperation.addEventListener(Event.COMPLETE, handleCommerceComplete(gadget, validationOperation));
						commerceOperation.addEventListener(GadgetEvent.PURCHASE_FAILED, handlePurchaseFailed(gadget, validationOperation));
						commerceOperation.addEventListener(ErrorEvent.ERROR, handleCommerceError(gadget, validationOperation));
						commerceOperation.execute();
						break;
					default:
						handleCommerceCancelled(gadget, validationOperation);
						break;
				}
			}
		}

        private function handleCommerceComplete(gadget:Project, validationOperation:IOperation):Function
		{
			return function(event:Event):void {
				event.target.removeEventListener(event.type, arguments.callee);
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:501, inserts:[gadget.publishedName]}));
				Dock.instance.lastSuccessfullyPurchased = gadget;
				handleOwnershipConfirmed(gadget, validationOperation);
			}
		}

		private function handlePurchaseFailed(gadget:Project, validationOperation:IOperation):Function
		{
			return function(event:Event):void {
				event.target.removeEventListener(event.type, arguments.callee);
				// show user feedback as to nature of commerce error

                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:211, closeFunction:handleCheckMyAccount}));
				handleCommerceCancelled(gadget, validationOperation);
			}
		}

		private function handleCommerceError(gadget:Project, validationOperation:IOperation):Function
		{
			return function(event:Event):void {
				event.target.removeEventListener(event.type, arguments.callee);
				// show user feedback as to nature of commerce error
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:210, closeFunction:handleCheckMyAccount}));

				handleCommerceCancelled(gadget, validationOperation);
			}
		}

        private function handleCheckMyAccount(event:CloseEvent):void{
            switch (event.detail) {
					case Alert.FIRST:
                        trace("Check my Account");
					default:
						break;
				}
        }
		
		private function handleOwnershipConfirmed(gadget:Project, validationOperation:IOperation):void
		{
			validationOperation.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		private function handleCommerceCancelled(gadget:Project, validationOperation:IOperation):void
		{
			validationOperation.dispatchEvent(new Event(Event.CANCEL));
		}
	}
}
