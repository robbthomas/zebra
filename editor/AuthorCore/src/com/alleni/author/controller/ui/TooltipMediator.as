package com.alleni.author.controller.ui
{
	import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
	import com.alleni.taconite.dev.Utilities;

	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class TooltipMediator
	{
		private var _view:InteractiveObject;
		private var _tooltipText:String;
		private var _location:Point;
		private var _direction:String;
		private var _arrow:Boolean;
		
		public var textCallback:Function;  // function(view):String to provide a dynamic tooltip
		
		
		public function TooltipMediator()
		{
		}
		
		public function handleTooltip(view:InteractiveObject, tooltip:String, location:Point=null, direction:String=null, arrow:Boolean=false):void
		{
            // remove old listeners so the view isn't left hangin' if we start using a different view.
            removeListeners();
            Utilities.assert(tooltip!=null);

			_view = view;
			_tooltipText = tooltip;
			_location = location;
			_direction = direction;
			_arrow = arrow;
			_view.addEventListener(MouseEvent.ROLL_OVER, rolloverListener);
			_view.addEventListener(MouseEvent.ROLL_OUT, rolloutListener);
		}

        public function removeListeners():void {
            if (_view) {
                _view.removeEventListener(MouseEvent.ROLL_OVER, rolloverListener);
                _view.removeEventListener(MouseEvent.ROLL_OUT, rolloutListener);
            }
        }
		
		private function rolloverListener(event:MouseEvent):void
		{
			var location:Point = _location;
			if (!location)
				location = new Point(0, _view.height+5); // tempZMI offset
			var direction:String = _direction;
			if (!direction)
				direction = ControlAreaToolTip.DISPLAY_BELOW;
			var arrow:String = _arrow ? ControlAreaToolTip.ARROW_SHOW : ControlAreaToolTip.ARROW_HIDDEN;
			
			var global:Point = _view.localToGlobal(location);
			var text:String = (textCallback==null) ? _tooltipText : textCallback(_view);
			if (text && text != '')
				ApplicationController.instance.displayToolTip(global, direction, text, arrow); 
		}
		
		private function rolloutListener(event:MouseEvent):void
		{
			ApplicationController.instance.hideToolTip();
		}
	}
}