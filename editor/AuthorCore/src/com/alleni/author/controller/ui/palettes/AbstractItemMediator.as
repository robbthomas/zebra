package com.alleni.author.controller.ui.palettes
{
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.objects.CompositeController;
import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.controller.ui.AssetDragDescription;
	import com.alleni.author.controller.ui.DragDropMediator;
	import com.alleni.author.controller.ui.DropFeedbackController;
	import com.alleni.author.controller.ui.GadgetController;
	import com.alleni.author.controller.ui.ReplacementController;
import com.alleni.author.definition.action.CreateObjectAction;
import com.alleni.author.definition.action.LibraryAddAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.IListable;
import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.view.ui.LoadingItemPlaceholder;
import com.alleni.author.view.ui.palettes.LibraryView;
	import com.alleni.taconite.controller.DragMediator;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.TaconiteModel;

import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class AbstractItemMediator extends DragMediator
	{
		protected var _view:DisplayObjectContainer;
		protected var _item:IListable;
		protected var _dropContainer:DisplayObject;
        private var _gadgetPlaceholder:LoadingItemPlaceholder;
		protected static var _replacementController:ReplacementController;
		protected static var _dropFeedbackController:DropFeedbackController;

		public function AbstractItemMediator()
		{
			super(Application.instance.viewContext, true);
		}
		
		public function handleViewEvents(view:DisplayObjectContainer, item:IListable):AbstractItemMediator
		{
			_view = view;
			_item = item;
			_view.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			return this;
		}
		
		private function get replacementController():ReplacementController
		{
			if (!_replacementController)
				_replacementController = ReplacementController.instance;
			return _replacementController;
		}
		
		private function get dropFeedbackController():DropFeedbackController
		{
			if (!_dropFeedbackController)
				_dropFeedbackController = DropFeedbackController.instance;
			return _dropFeedbackController;
		}
		
		override public function handleMouseDown(e:MouseEvent):void
		{
			if (!_context.stage)
				_context = Application.instance.viewContext;
			super.handleMouseDown(e);
		}
		
		/**
		 * Handle the start of a drag gesture.
		 */
		override protected function handleDragStart(e:MouseEvent):void
		{
			DragDropMediator.startDrag(_view, _item);
			
			if (_item as Asset) {
				var drag:AssetDragDescription = new AssetDragDescription(Application.instance.viewContext);
				Application.instance.assetDragDescription = drag;
			}
		}
		
		/**
		 * Handle an intermediate position of a drag gesture. 
		 */
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
		{
			// test mouse point for potential replacement candidates
			replacementController.testForReplacement(e);
			_dropContainer = dropFeedbackController.testForDropContainer(e, this is LibraryItemMediator);
			_view.stage.invalidate();  // allow messageCenters to move, when dragging items over objects
		}
		
		override protected function handleDragEnd(event:MouseEvent):void
		{
            if (_dropContainer == null) {
                cleanupAfterDropItem();
                return;
            } // prevent things from being created behind ui panels

            var withContainer:Boolean = !(_dropContainer as LibraryView || ReplacementController.instance.isPending); // don't create a container if item was dropped straight onto Library
            if (_item is Project) {
                // thumbnail showing while gadget loads, only for Gadgets, for now
                _gadgetPlaceholder = new LoadingItemPlaceholder(_item, event.target.x, event.target.y, DragDropMediator.dragThumbnailCopy);
            }

            var worldMousePoint:Point = DragDropMediator.worldMousePoint;
            var gadget:Project = _item as Project;
            if (gadget) {
                dropGadgetWithUndo(gadget, worldMousePoint.x,  worldMousePoint.y, withContainer);
            } else {
                dropAssetWithUndo(_item as Asset, worldMousePoint.x, worldMousePoint.y, event.altKey, withContainer);
            }
            EditorUI.instance.mouseIsDown = false;
		}

        protected function dropAssetWithUndo(requestedAsset:Asset, xWorld:Number, yWorld:Number, copy:Boolean, withContainer:Boolean):void
        {
            AssetController.instance.loadContent(requestedAsset, function(asset:Asset):void {
                if (asset) {
                    dropAssetNow(asset, xWorld, yWorld, copy, withContainer);
                } else {
                    cleanupAfterDropItem();  // failure to load
                }
            });
        }

        protected function dropAssetNow(asset:Asset, xWorld:Number, yWorld:Number, copy:Boolean, withContainer:Boolean):void
        {
            var wasRunning:Boolean = ApplicationController.instance.authorController.editWhileRunningStart();
            if (!ReplacementController.instance.executeIfPending(asset)) {
                if (withContainer) {
                    var obj:AbstractObject = AssetController.instance.configureContainerAndDisplay(asset, xWorld, yWorld);
                    ApplicationController.currentActionTree.commit(CreateObjectAction.fromObject(obj));
                    AuthorController.instance.selectSingleModel(obj.model);
                }
            }
            cleanupAfterDropItem();
            if (wasRunning) ApplicationController.instance.authorController.editWhileRunningEnd();
        }

        protected function dropGadgetWithUndo(draggedGadget:Project, xWorld:Number, yWorld:Number, withContainer:Boolean):void
        {
             // prevent recursive gadget reference
            var models:Vector.<TaconiteModel> = AuthorController.instance.getObjectModelsForReplaceable(draggedGadget, true);
            if (ApplicationController.instance.authorController.isCurrentlyEditingObject(models)) {
                cleanupAfterDropItem();
                replacementController.reset();
                CompositeController.instance.recursiveGadgetAlert();
                return;
            }

            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, "Loading "+draggedGadget.name));
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            PseudoThread.add(new PseudoThreadSimpleRunnable("dropGadgetWithUndo", dropGadgetWithUndoNow, this, [draggedGadget, xWorld, yWorld, withContainer]));
        }

        private function dropGadgetWithUndoNow(draggedGadget:Project, xWorld:Number, yWorld:Number, withContainer:Boolean):void
        {
            var inputToken:Object = ApplicationController.instance.disableInput("dropGadgetWithUndo");

            GadgetController.instance.buyAndLoad(draggedGadget, function(gadget:Project) {
                if (gadget) {
                    if (withContainer) {
                        createComposite(gadget, xWorld, yWorld, inputToken);
                    } else {
                        dropRawGadget(gadget);
                        cleanupAfterDropItem();
                        ApplicationController.instance.enableInput(inputToken);
                    }
                } else { // gadget=null: cancel or error
                    cleanupAfterDropItem();
                    ApplicationController.instance.enableInput(inputToken);
                }
            });
        }

        private function createComposite(gadget:Project, xWorld:Number, yWorld:Number, inputToken:Object):void
        {
            var wasRunning:Boolean = ApplicationController.instance.authorController.editWhileRunningStart();
            var composite:Composite = AuthorController.instance.addObjectForName("Composite") as Composite;
            var position:Point = filteredPosition(composite, xWorld, yWorld);

            var hackishlyAddedToLibrary:Vector.<LibraryAddAction> = new Vector.<LibraryAddAction>()
            for each(var p:Project in gadget.children) {
                if (!GadgetController.instance.existsInModel(p)) {
                  var action:LibraryAddAction =  new LibraryAddAction(p);
                  action.perform();
                  hackishlyAddedToLibrary.push(action)
                }
            }
            GadgetController.instance.configureContainerAndDisplay(gadget, composite, position.x, position.y, null, function():void{
                AuthorController.instance.dropIntoContainer(composite);
                var groupToken:Object = ApplicationController.currentActionTree.openGroup("Drag "+composite.gadget.name);
                ApplicationController.currentActionTree.commit(CreateObjectAction.fromObject(composite));
                for each(var action:LibraryAddAction in hackishlyAddedToLibrary) {
                    action.rollback();
                }
                addToLibrary(gadget);
                ApplicationController.currentActionTree.closeGroup(groupToken);

                PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                PseudoThread.add(new PseudoThreadSimpleRunnable("selectCreatedComposite", function():void {
                    AuthorController.instance.selectSingleModel(composite.model);
                    cleanupAfterDropItem();
                    if (wasRunning) ApplicationController.instance.authorController.editWhileRunningEnd();
                    ApplicationController.instance.enableInput(inputToken);
                }));
            });
        }

        private function dropRawGadget(gadget:Project):void
        {
            // either doing a replacement with construction tape, or dragged from cloud to library
            addToLibrary(gadget);   // separate action to add cloud gadget to library (too difficult to combine with replace-all actions)
            ReplacementController.instance.executeIfPending(gadget);  // commits its own undo; own editWhileRunningStart
        }

        private function addToLibrary(gadget:Project):void
        {
            // only commits actions when dragged from cloud, tho it executes on drag from library
            if (!GadgetController.instance.existsInModel(gadget)) {
                var token:Object = ApplicationController.currentActionTree.openGroup("Add to library: "+gadget.name);
                addOneToLib(gadget);
                for each (var nestedGadget:Project in gadget.children) {
                    addOneToLib(nestedGadget);
                }
                for each (var asset:Asset in gadget.assets) {
                    addOneToLib(asset);
                }
                ApplicationController.currentActionTree.closeGroup(token);
            }
        }

        private function addOneToLib(item:IReplaceable):void
        {
            var libraryAdd:LibraryAddAction = LibraryAddAction.fromItem(item);
            libraryAdd.perform();
            ApplicationController.currentActionTree.commit(libraryAdd);
        }

        protected function cleanupAfterDropItem():void
        {
            Application.instance.assetDragDescription = null;
            dropFeedbackController.reset();
            if (_gadgetPlaceholder) _gadgetPlaceholder.visible = false;
            _gadgetPlaceholder = null;
            LibraryController.instance.updateForAssetChange();
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE));
        }

        private function filteredPosition(composite:Composite, dropX:Number, dropY:Number):Point
        {
            var updatedPoint:Point = new Point(dropX, dropY);
            if(composite.parent is EventFlow){   // global backstage ... constrain position to say outside the flow view
                updatedPoint = (composite.parent as EventFlow).flowView.moveOutsideBounds(updatedPoint, false);
            }
            return updatedPoint;
        }
	}
}