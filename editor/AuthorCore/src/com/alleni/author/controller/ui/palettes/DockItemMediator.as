package com.alleni.author.controller.ui.palettes
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.model.objects.IListable;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Dock;
	import com.alleni.author.view.text.RichLabel;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.utils.Timer;

	public class DockItemMediator extends AbstractItemMediator
	{
		private static const SCALE_ROLLED:Number 			= 1.05;
		
		private static const SHADOW_DISTANCE:Number 		= 1.0;
		private static const SHADOW_DISTANCE_ROLLED:Number 	= 2.0;
		private static const SHADOW_BLUR:Number 			= 2.0;
		private static const SHADOW_BLUR_ROLLED:Number 		= 4.0;
		
		private static var _dropShadow:DropShadowFilter;
		private static var _overlayShadow:DropShadowFilter;
		private static var _nameOverlay:Sprite;
		private static var _nameLabel:RichLabel;
		private var _filterable:DisplayObject;
        private static var _tooltipTimer:Timer;
		
		public function DockItemMediator()
		{
			super();
		}
		
		private static function get dropShadow():DropShadowFilter
		{
			if (!_dropShadow)
				_dropShadow = new DropShadowFilter(SHADOW_DISTANCE, 80.0, 0x000000, 0.8, SHADOW_BLUR, SHADOW_BLUR, 0.8, 2);
			return _dropShadow;
		}
		
		private static function get overlayShadow():DropShadowFilter
		{
			if (!_overlayShadow)
				_overlayShadow = new DropShadowFilter(SHADOW_DISTANCE, 80.0, 0x000000, 0.8, SHADOW_BLUR, SHADOW_BLUR, 0.8, 2);
			return _overlayShadow;
		}
		
		public static function get baseFilters():Array
		{
			return [dropShadow];
		}
		
		override public function handleViewEvents(view:DisplayObjectContainer, item:IListable):AbstractItemMediator
		{
			view.addEventListener(MouseEvent.MOUSE_OVER, handleMouseOver);
			view.addEventListener(MouseEvent.MOUSE_OUT, handleMouseOut);

			view.filters = baseFilters;
				
			return super.handleViewEvents(view, item);
		}
		
		public function refreshItem(item:IListable):void
		{
			_item = item;
		}
		
		override public function handleMouseDown(e:MouseEvent):void
		{
			if (Dock.instance.listInProgress) return;
			else super.handleMouseDown(e);
		}
		
		override protected function handleClick(e:MouseEvent):void
		{
			if (_nameOverlay && _view.stage.contains(_nameOverlay)) {
				_view.stage.removeChild(_nameOverlay);
			}
            if (Dock.instance.selectedItem === _item ) {
                Dock.instance.selectedItem = null;
            } else {
                Dock.instance.selectedItem = _item;
            }
		}
		
		private function handleMouseOver(event:MouseEvent):void
		{
			if (Dock.instance.listInProgress || Dock.instance.selectedItem === _item) return;
			
			Tweener.addTween(_view, {scaleX:SCALE_ROLLED, scaleY:SCALE_ROLLED, time:0.05, 
				transition:EditorUI.ANIMATION_IN_ALGORITHM});
			
			Tweener.addTween(overlayShadow, {blurX:SHADOW_BLUR_ROLLED, blurY:SHADOW_BLUR_ROLLED, distance:SHADOW_DISTANCE_ROLLED, time:0.05, 
				transition:EditorUI.ANIMATION_IN_ALGORITHM, 
				onUpdate:function():void{
					_view.filters = [overlayShadow];
				}
			});
			
			if (!_nameOverlay) {
				var shadow:DropShadowFilter = new DropShadowFilter(1,80,0x000000,0.8,2,2,1.5,3);
				_nameLabel = new RichLabel(150);
				_nameLabel.mouseChildren = false;
				_nameLabel.mouseEnabled = false;
				_nameLabel.size = 12;
				_nameLabel.bold = true;
				_nameLabel.x = 5;
				_nameLabel.filters = [shadow];
				
				_nameOverlay = new Sprite();
				_nameOverlay.addChild(_nameLabel);
				_nameOverlay.mouseChildren = false;
				_nameOverlay.mouseEnabled = false;
				_nameOverlay.filters = [shadow];
			}
			_nameLabel.text = (_item as Project).publishedName;
			
			var stagePoint:Point = _view.localToGlobal(new Point(0,0));
			
			const W:Number = _nameLabel.textWidth+15;
			_nameOverlay.graphics.clear();
			_nameOverlay.graphics.lineStyle(0, 0xffffff, 0.2, true);
			_nameOverlay.graphics.beginFill(0, 0.5);
			_nameOverlay.graphics.drawRoundRect(0, 0, W, 20, 15, 15);
			_nameOverlay.graphics.endFill();
			_nameOverlay.x = stagePoint.x - _view.width/2 - (W - _view.width)/2;
			_nameOverlay.y = stagePoint.y + 26;
			
			_view.stage.addChild(_nameOverlay);
			_nameOverlay.alpha = 0;
			
			Tweener.addTween(_nameOverlay, {alpha:1, time:0.2, transition:EditorUI.ANIMATION_IN_ALGORITHM });
            startToolTipTimer(true);
		}

        private function stopToolTipTimer():void
		{
			if (_tooltipTimer) {
				_tooltipTimer.stop();
				_tooltipTimer.removeEventListener(TimerEvent.TIMER, handleTimedTooltip);
				_tooltipTimer.removeEventListener(TimerEvent.TIMER, handleTimedTooltipEnd);
				_tooltipTimer = null;
			}
		}

        private function startToolTipTimer(show:Boolean):void
		{
			stopToolTipTimer();
			_tooltipTimer = new Timer(show?ApplicationController.TOOLTIP_DELAY:5000, 1);
			_tooltipTimer.addEventListener(TimerEvent.TIMER, show?handleTimedTooltip:handleTimedTooltipEnd);
			_tooltipTimer.start();
		}

		private function handleTimedTooltip(event:Event):void
		{
			if(_nameOverlay) {
				_nameOverlay.visible = true;
				startToolTipTimer(false);
			}
		}

		private function handleTimedTooltipEnd(event:Event):void
		{
            if (_view.stage && _nameOverlay && _view.stage.contains(_nameOverlay)) {
               Tweener.addTween(_nameOverlay, {alpha:0, time:0.2, transition:EditorUI.ANIMATION_IN_ALGORITHM,
                   onComplete:function():void {
                       _view.stage.removeChild(_nameOverlay);
                   }
               });
            }
		}

        public function clearToolTip():void
		{
			if (!_view.stage) return;
            if(_nameOverlay && _view.stage.contains(_nameOverlay)){
                _view.stage.removeChild(_nameOverlay);
            }
        }

		private function handleMouseOut(event:MouseEvent):void
		{
			Tweener.addTween(_view, {scaleX:1.0, scaleY:1.0, time:0.05, 
				transition:EditorUI.ANIMATION_OUT_ALGORITHM});
			
			dropShadow.blurX = SHADOW_BLUR;
			dropShadow.blurY = SHADOW_BLUR;
			dropShadow.distance = SHADOW_DISTANCE;
			_view.filters = baseFilters;
			
			if (_view.stage && _nameOverlay && _view.stage.contains(_nameOverlay)) {
				Tweener.addTween(_nameOverlay, {alpha:0, time:0.2, transition:EditorUI.ANIMATION_IN_ALGORITHM, 
					onComplete:function():void {
						_view.stage.removeChild(_nameOverlay);
					}
				});
			}
		}
	}
}