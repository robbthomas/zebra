package com.alleni.author.controller.ui.palettes
{
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.GadgetController;
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.DoubleClickAction;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.event.AssetEvent;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.Library;
	import com.alleni.author.service.assets.AssetSaveOperation;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.service.LogService;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class LibraryItemMediator extends AbstractItemMediator
	{	
		public function LibraryItemMediator()
		{
			super();
		}
		
		override protected function handleClick(e:MouseEvent):void
		{
			Library.instance.selectedItem = _item;

			if (_item as Asset)
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SELECT_ASSET, _item));
			else // implicit _item as Gadget since Library currently contains either Asset or Gadget
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SELECT_GADGET, _item));
		}
		
		override protected function handleDragEnd(event:MouseEvent):void
		{
			super.handleDragEnd(event);
			if (_dropContainer)
				Library.instance.selectedItem = _item;
		}
		
		public function set nameField(value:LightEditableLabel):void
		{
			value.mediator.setCallBacks({
				okEditFunc:function(e:MouseEvent):int{
					var gadget:Project = _item as Project;
					if (gadget && !GadgetController.instance.confirmCanPublish(gadget)) return DoubleClickAction.IGNORE; // if author can't publish, disallow name edit
					return TextEditMediator.doubleClickToEdit(e);
				},
				updateFunc:null,
				keyPressFunc:function(charCode:uint):uint{
					return TextEditMediator.closeOnEnter(charCode);
				},
				closeFunc:function(val:String):void{
					value.text = FormattedText.convertToString(val as String);
					value.edit = false;
					var oldName:String = _item.name;
					
					if (_item.type == AssetType.GADGET) {
						GadgetController.instance.setName(_item as Project, value.text);
					} else {// item must be an asset
						_item.name = value.text;
						var assetSaveOperation:AssetSaveOperation = new AssetSaveOperation(_item as Asset, _item.name);
						assetSaveOperation.addEventListener(AssetEvent.ERROR, function():void{
							value.text = oldName; // failed. revert.
							_item.name = oldName;
						});
						assetSaveOperation.addEventListener(Event.COMPLETE, function():void{
							LogService.debug("Success changing name for '"+_item.name+"'");
						});
						assetSaveOperation.execute();
					}
				},
				hittestFunc:function(x:Number, y:Number):Boolean{
					if(value.hitTestPoint(x,y))
						return true;
					else
						return false;
				}
			});
		}
	}
}