package com.alleni.author.controller.ui
{
	/*
		Gadget saving:
		There is a GadgetSavingQueue that saves gadgets in background during editing.  When the enter key is pressed to
		close gadget editing, that gadget is pushed onto the end of the queue.  Meanwhile, gadgets are saved from the beginning
		of the queue.  This enables closing of multiple nested gadgets with no waiting.
	
		The project is saved in the same manner, so any pending gadgets will be saved before the project-gadget.
	*/

import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.PagerController;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ObjectFactory;
import com.alleni.author.definition.GadgetDescription;
import com.alleni.author.definition.GadgetType;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.event.GadgetEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.gadgets.Gadgets;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.TemplateValueObject;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.Assets;
import com.alleni.author.model.ui.Dock;
import com.alleni.author.model.ui.ExternalContentObject;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.persistence.GadgetData;
import com.alleni.author.persistence.GadgetDecoder;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.service.gadgets.ProjectSaveRunnable;
import com.alleni.author.service.project.ProjectLoadOperation;
import com.alleni.author.service.project.ProjectRenameOperation;
import com.alleni.author.service.project.ProjectSaveOperation;
import com.alleni.author.util.cache.LRUCache;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadContext;
import com.alleni.author.util.thread.PseudoThreadOperationRunnable;
import com.alleni.author.util.thread.PseudoThreadRunnable;
import com.alleni.author.view.ui.controls.Alert;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.factory.IEnvironment;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.service.IOperation;
import com.alleni.taconite.service.LogService;

import flash.events.ErrorEvent;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.utils.Dictionary;

import mx.collections.ArrayCollection;

import mx.collections.IList;
import mx.events.CloseEvent;
import mx.events.PropertyChangeEvent;
import mx.events.PropertyChangeEventKind;

public class GadgetController extends EventDispatcher
	{
		private static var _instance:GadgetController;
		
		// static vars, since the ProjectController extends GadgetController
		private static var _requestedQueue:Vector.<String>;
		private static var _gadgetCache:LRUCache;


		public function GadgetController()
		{
			Utilities.assert(!_instance);
			initialize();
			_gadgetCache = new LRUCache(50, 200);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOADING, handleProjectLoading);
		}
		
		public static function get instance():GadgetController
		{
			if (!_instance)
				_instance = new GadgetController();
			return _instance;
		}
		
		/**
		 * cache and clear deprecated items 
		 * @param event
		 * 
		 */
		private function handleProjectLoading(event:Event):void
		{
			if (!model) return;
			while (model.gadgets.length > 0)
				removeFromModel(model.gadgets[0]);
			initialize();
		}

        public function get freshModel():Gadgets
  		{
  			if (model) return model;
  			return new Gadgets();
  		}

		public function hasExtraGadgets():Boolean
		{
            var found:Dictionary = new Dictionary();
            var foundExtras:Boolean = false;
            for each(var child:Project in model.gadgets) {
                var id:String = child.projectId;
                if (id in found) {
                    var count:int = found[id];
                    ++count;
                    found[id] = count;
                    foundExtras = true;
                } else {
                    found[id] = 1;
                }
            }
            if (foundExtras) {
                AuthorController.instance.log("gadgetController:hasExtraGadgets: DUPLICATE GADGETS");
                for (id in found)  {
                    count = found[id];
                    if (count > 1) {
                        AuthorController.instance.log("    Duplicate gadget id="+id + " count="+count);
                    }
                }
            }
            return foundExtras;
		}

        public function gadgetsListForProjectSave():Vector.<Project>
        {
            // remove unused EventPage gadgets and duplicate gadgets, in case left by accident
            var result:Vector.<Project> = new <Project>[];
            var usages:UsageCounts = new UsageCounts();
            var found:Dictionary = new Dictionary(); // key=GadgetID
            AuthorController.instance.eventFlow.getUsageCounts(usages);
            for (var n:int = 0; n < model.gadgets.length; n++) {
                var gadget:Project = model.gadgets[n];
                var id:String = gadget.projectId;
                if (gadget.projectTypeId == GadgetType.EVENT && usages.getGadgetUsageCount(gadget) == 0) {
                    AuthorController.instance.log("$$ Skip unused EventPage gadget="+gadget);
                } else if (id in found) {
                    AuthorController.instance.log("$$ Skip duplicate gadget reference="+gadget);
                } else {
                    result.push(gadget);
                    if (id != null) {
                        found[id] = true;
                    }
                }
            }
            if (result.length != model.gadgets.length) {
                AuthorController.instance.log("gadgetsListForProjectSave: gadgetsModel="+model.gadgets.length + " gadgetsForSave="+result.length);
                var name:String = Application.instance.document.name;
                if (okToAlert) {
                    warningAboutDuplicateGadgetsOnSave();
                }
            }
            return result;
        }

        private function warningAboutDuplicateGadgetsOnSave():void
        {
            Alert.okLabel = "OK";
         	Alert.buttonWidth = 90;
            Alert.show("Automatically fixing the project.\nIt has duplicate gadget references or unused Event gadgets", "Internal Error");
        }

        public function get okToAlert():Boolean
        {
            const env:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
            return env.isAdmin;
        }


		private function initialize():void
		{
			_requestedQueue = new Vector.<String>();
		}
		
		public function attachFor(obj:SmartObject, gadget:Project):void
		{
			if (obj.gadget && obj.gadget.id == gadget.id)
				return;
			
			if (obj.gadget) {
				obj.gadget.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, obj.replaceablePropertyChangeListener);
				obj.replaceablePropertyChangeListener = null;
			}
			
			obj.gadget = gadget;
			obj.replaceablePropertyChangeListener = function(event:PropertyChangeEvent):void {
				obj.dispatchEvent(new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE, false, false, PropertyChangeEventKind.UPDATE, "gadget", gadget, gadget));
			}
			gadget.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, obj.replaceablePropertyChangeListener);
		}

		public function addToModel(gadget:Project, index:int=-1):Boolean
		{
			if (!gadget)
				return false;
				
			if (model.gadgets.getItemIndex(gadget) < 0) {
				if (index < 0 || index > model.gadgets.length)
					model.gadgets.addItem(gadget);
				else
					model.gadgets.addItemAt(gadget, index);
				return true;
			}
			return false;
		}
		
		public function removeFromModel(gadget:Project):Boolean
		{
			if (!gadget)
				return false;
			var index:int = model.gadgets.getItemIndex(gadget);
			if (index >= 0) {
				model.gadgets.removeItemAt(index);
				_gadgetCache.setValue(gadget.id, gadget);
				return true;
			}
			return false;
		}

        public function existsInModel(gadget:Project):Boolean
        {
            return (model.gadgets.getItemIndex(gadget) >= 0);
        }

		public function get model():Gadgets
		{
			if (Application.instance.document)
				return Application.instance.document.gadgetsRoot.value as Gadgets;
			else
				return new Gadgets();
		}
		
		/**
		 * available for reuse. This list may not include certain assets which cannot be reused,
		 * such as children of 3rd-party gadgets.  Or EventPage gadgets.
		 * @return 
		 * 
		 */
		public function get availableModel():IList
		{
			var result:ArrayCollection = new ArrayCollection();
            var showEvents:Boolean = Application.instance.showEventGadgetsInLibrary;
            for each (var gadget:Project in model.gadgets) {
                if (showEvents || gadget.projectTypeId != GadgetType.EVENT) {
                    result.addItem(gadget);
                }
            }
            return result;
		}

		public function confirmCanPublish(contentObject:ExternalContentObject):Boolean
		{
			const gadget:Project = contentObject as Project;
			if (!gadget) return true;
			const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			return gadget.projectId != null
				&& (!gadget.accountId || (gadget.accountId == environment.accountId));
		}
		
		public function confirmCanEdit(gadget:Project):Boolean
		{
            if (gadget.permissions && gadget.permissions.canEdit) {
                return true;
            } else if (confirmCanPublish(gadget)) {
				if (Dock.instance.publishingCandidate == null) {
					return true;
				} else {
					return (Dock.instance.publishingCandidate.id != gadget.id);  // cannot edit the gadget being published
				}
			}
			return false;
		}
		
		public function confirmOwnership(gadget:Project):Boolean
		{
            if(gadget.permissions) {
			    return gadget.permissions.owned;
            }
            return true;
		}
		
		public function confirmRepublishIsAllowed(gadget:Project):Boolean
		{
			if (gadget.permissions)
				return gadget.permissions.canRepublish;
			return false;
		}
		
		public function confirmEmbedIsAllowed(gadget:Project):Boolean
		{
			if (gadget.permissions)
				return gadget.permissions.canEmbed;
			return false;
		}

        /*
            buyAndLoad does interactive purchase if necessary and then loads the gadget content.
            The onComplete(gadget) is called after the gadget is loaded. (gadget may be different from the input gadget)
            If the user cancels the purchase (or if an error occurs), onComplete will be called with gadget=null.
         */
		public function buyAndLoad(gadget:Project, onComplete:Function=null):void
		{
            if (gadget.content == null) {
                var existing:Project = getExistingGadget(gadget.id);
                if (existing && existing.content) {
                    gadget = existing;   // already got it from cloud... use the one in the library
                }
            }
            if (gadget.content != null) {  // already have content no need to load
                if (onComplete != null) {
                    onComplete(gadget);
                }
                return;
            }

            _loadContentCompleteCallback = onComplete;
			var validate:IOperation = CommerceController.instance.validateGadgetPurchase(gadget);
            if (validate) {
                validate.addEventListener(Event.COMPLETE, function(e:Event):void {
                    load(gadget.id);
                 });
                validate.addEventListener(Event.CANCEL, function(e:Event):void {
                    finishBuyAndLoad(null);
                });
            } else {  // null operation indicates gadget is already owned
                load(gadget.id);
            }
		}

        private var _loadContentCompleteCallback:Function;

        private function finishBuyAndLoad(gadget:Project):void
        {
            if (_loadContentCompleteCallback != null) {
                _loadContentCompleteCallback(gadget);
            }
            _loadContentCompleteCallback = null;
        }

		public function requestForProjectAndCreateContainer(gadget:Project, index:int, className:String, gadgetData:GadgetData, zIndex:Object = -1):SmartObject
		{
			var smart:SmartObject = createContainerFor(gadgetData, className, zIndex);
            if(gadget.content) {
                requestForProject(gadget, index, false, smart, gadgetData);
            } else {
			    requestForID(gadget.id, index, false, smart, gadgetData);
            }
			return smart;
		}

        public function requestForIDAndCreateContainer(id:String, index:int, className:String, gadgetData:GadgetData, zIndex:Object = -1):SmartObject
  		{
  			var smart:SmartObject = createContainerFor(gadgetData, className, zIndex);
  			requestForID(id, index, false, smart, gadgetData);
  			return smart;
  		}

        public function requestForProject(gadget:Project, index:int=-1, asACopy:Boolean=false, smart:SmartObject=null, gadgetData:GadgetData=null):void
        {
            if (smart)
                configureContainerAndDisplay(gadget, smart, NaN, NaN, gadgetData);
        }
		
		public function requestForID(id:String, index:int=-1, asACopy:Boolean=false, smart:SmartObject=null, gadgetData:GadgetData=null):Project
		{
			var gadget:Project = model.getById(id);
			if (!gadget) // not in the model? maybe it was before and we cached it
				gadget = _gadgetCache.getValue(id) as Project;
			
			//var parent:Composite;
			if (gadget && gadget.content != null) {
				requestForProject(gadget, index, asACopy, smart, gadgetData);
				return gadget;
			} else {
				gadget = new Project(id);
			}
			if (addToQueue(id))
				load(id, asACopy, false, null, smart, NaN, NaN, gadgetData);
			return gadget;
		}

        public function getExistingGadget(id:String):Project
        {
            var gadget:Project = model.getById(id);
            if(gadget == null){
                gadget = (_gadgetCache.getValue(id) as Project);
            }
            return gadget;
        }

		private function isOnQueue(id:String):Boolean
		{
			if (_requestedQueue.indexOf(id) > -1)
				return true;
			return false;
		}
		
		private function addToQueue(id:String):Boolean
		{
			if (isOnQueue(id))
				return false;
			_requestedQueue.push(id);
			return true;
		}
		
		private function verifyRemovalFromQueue(id:String, fromError:Boolean=false):Boolean
		{
			if (isOnQueue(id)) {
				_requestedQueue.splice(_requestedQueue.indexOf(id), 1);
				return true;
			}
			return false;
		}
		
		protected function loadPurchased(id:String, asACopy:Boolean=false, loadAuthorState:Boolean=false, externalUserId:String = null):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.GADGET_REQUESTED, id));
			
			var operation:ProjectLoadOperation = new ProjectLoadOperation(id, loadAuthorState, externalUserId, true);
			operation.displayName = "Loading...";
			operation.addEventListener(GadgetEvent.LOAD_FAILED, handleLoadFailed);
			operation.addEventListener(GadgetEvent.LOAD_NO_PERMISSION, handleLoadNoPermission);
			operation.addEventListener(GadgetEvent.ERROR, handleError);
			operation.addEventListener(Event.COMPLETE, handleLoadComplete(asACopy));
			operation.execute();
		}
		
		protected function load(id:String, asACopy:Boolean=false, loadAuthorState:Boolean=false, externalUserId:String = null, smart:SmartObject=null, dropX:Number=NaN, dropY:Number=NaN,
								gadgetData:GadgetData=null, createOnNullContainer:Boolean=false, published:Project=null, loadVersion:Boolean=false, templateVO:TemplateValueObject=null):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.GADGET_REQUESTED, id));
			
			var operation:ProjectLoadOperation = new ProjectLoadOperation(id, loadAuthorState, externalUserId, false /*TODO needs to be apn for (published != null)*/);
			operation.displayName = "Loading...";
			operation.addEventListener(GadgetEvent.LOAD_FAILED, handleLoadFailed);
			operation.addEventListener(GadgetEvent.LOAD_NO_PERMISSION, handleLoadNoPermission);
			operation.addEventListener(GadgetEvent.ERROR, handleError);
			operation.addEventListener(Event.COMPLETE, handleLoadComplete(asACopy, smart, dropX, dropY, gadgetData, createOnNullContainer, published, templateVO));
			operation.execute();
		}
		
		public function getGadgetFromCompletedOperation(event:Event):Project
		{
			if (event.target as ProjectLoadOperation) {
				var gadgetLoadOperation:ProjectLoadOperation = event.target as ProjectLoadOperation;
				var project:Project = gadgetLoadOperation.project;
				return project;
			} else if (event.target as ProjectSaveOperation) {
				var gadgetSaveOperation:ProjectSaveOperation = event.target as ProjectSaveOperation;
				return model.getById(gadgetSaveOperation.id);
			} else
				return null;
		}
		
		protected function handleLoadComplete(asACopy:Boolean=false, smart:SmartObject=null, dropX:Number=NaN, dropY:Number=NaN,
											  gadgetData:GadgetData=null, createOnNullContainer:Boolean=false, published:Project=null, templateVO:TemplateValueObject=null):Function
		{
            // override by ProjectController, so this is only for gadgets dragged from cloud or library
			return function(e:Event):void
			{
				var gadget:Project = getGadgetFromCompletedOperation(e);
                finishBuyAndLoad(gadget);
			}
		}
		
		public function createContainerFor(gadgetData:GadgetData, className:String, zIndex:Object = -1):SmartObject
		{
			var parent:AbstractObject;
			if (gadgetData){
				parent = gadgetData.parent;	
            }else{
				parent = AuthorController.instance.currentContainer;
            }
			return ObjectFactory.loadForName(className, parent, zIndex) as SmartObject;
		}
		
		public function configureContainerAndDisplay(gadget:Project, smart:SmartObject, dropX:Number=NaN, dropY:Number=NaN, gadgetData:GadgetData=null, onComplete:Function = null):void
		{
			if (gadgetData == null)
				gadgetData = new GadgetData(smart.parent);
			
			if (gadgetData.overriddenValues == null)
				gadgetData.overriddenValues = new Object();

			if (!isNaN(dropX) && !isNaN(dropY)){
                gadgetData.overriddenValues.x = dropX;
                gadgetData.overriddenValues.y = dropY;
            }

            attachFor(smart, gadget);
            smart.loading = true;
            var isEventPage:Boolean = (smart is EventPage);
            var constructChildren:Boolean = !isEventPage; // by default events do not load children (we could guess the initial page and load it right away here)
            GadgetDecoder.buildOutComposite(smart, gadget, gadgetData, true, constructChildren, function():void{
                    if (smart.parent && smart.parent.active && !isEventPage) {
                        PagerController.instance.activateObject(smart, true);  // obj.active=true on gadget child objects
                    }
                    if (!smart.titleChangedByAuthor) {
                        smart.title = GadgetDescription.cleanName(gadget);
                    }
                    if (Application.running) {
                        smart.onResume();
                    }
                    if (onComplete != null) {
                        onComplete();
                    }
                });
		}
		
		protected function handleLoadFailed(event:Event):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_LOAD_FAILED)); 
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, "Loading failed."));
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE_WITH_DELAY));
            finishBuyAndLoad(null);

            //TODO PROJECT_GADGET replace logic
//			if (!(event.target as PurchasedLoadOperation)) {
//				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:40, closeFunction:handleErrorAlertDecision}));
//			}
		}
		
		protected function handleLoadNoPermission(event:Event):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_LOAD_FAILED)); 
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, "Unable to load."));
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE_WITH_DELAY));
            finishBuyAndLoad(null);

			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:41, closeFunction:handleErrorAlertDecision}));
		}
		
		protected function handleError(event:GadgetEvent):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE));
            finishBuyAndLoad(null);

			if (instance.verifyRemovalFromQueue(event.id, true))
				LogService.error("Handling error for gadget '"+event.id+"': removed from loading queue");
			else
				LogService.error("Handling error for gadget '"+event.id+"': was not on loading queue");
			if (instance.removeFromModel(instance.model.getById(event.id)))
				LogService.error("Handling error for gadget '"+event.id+"': removed from model");
			else
				LogService.error("Handling error for gadget '"+event.id+"': was not in model");
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.GADGET_REQUEST_FULFILLED, event.id));
		}

        private function handleErrorAlertDecision(event:CloseEvent):void
        {
            switch (event.detail) {
                case Alert.SECOND:
                    LogService.instance.sendAsEmail("Error loading gadget");
                    break;
                default:
                    // no action
                    break;
            }
        }
		
		public function saveForSmartObject(smart:SmartObject):Project
		{
			var gadget:Project;
			if (smart.gadget) {
				gadget = smart.gadget.clone();  // clone the gadget (doesn't encode content)
			} else {
				gadget = new Project();
			}
            gadget.projectTypeId = (smart is EventPage) ? GadgetType.EVENT : GadgetType.GENERIC;

			if (smart.viewBox) {
				gadget.width = smart.viewBox.width;
				gadget.height = smart.viewBox.height;
			}
			gadget.license.edit = true;

            // include all referenced assets & nested gadgets
            var usages:UsageCounts = getUsageCountsForSave(smart);
            var gadgets:Vector.<Project> = new <Project>[];
			for each (var g:Project in usages.getGadgetsList()) {
                gadgets.push(g);
                trace("saveForSmartObject:", g, "container="+smart);
            }
            var assets:Assets = new Assets();
			for each (var a:Asset in usages.getAssetsList()) {
                assets.assets.addItem(a);
                trace("saveForSmartObject:", a, "container="+smart);
            }

			save(gadget, smart, false, false, gadgets, assets, false, false, null, false);  // gadget, container, saveState, metaDataOnly, gadgets, assets, autosave, callback, replaceExisting
			return gadget;
		}

        private function getUsageCountsForSave(smart:SmartObject):UsageCounts
        {
            var oldGadget:Project = smart.gadget;
            smart.gadget = null;  // remove old revision so it isn't counted (restored below)
            var ep:EventPage = smart as EventPage;
            if (ep) {
                ep.childObjUsageCounts = null;  // clear the usageCounts cache to force a fresh counting
            }
            var usages:UsageCounts = new UsageCounts();
            smart.getUsageCounts(usages);
            smart.gadget = oldGadget;  // restore for benefit of CompositeSaveController
            return usages;
        }
		
		public function setName(gadget:Project, name:String, callback:Function=null):void
		{
			var oldName:String = gadget.name;  // in case of failure
			gadget.name = name;
			save(gadget, null, false, true, null, null, false, false, callback, false);  // metaDataOnly=true
		}
				
		public function save(gadget:Project, container:AbstractContainer, saveState:Boolean, metaDataOnly:Boolean, gadgets:Vector.<Project>,  assets:Assets, autosave:Boolean, templateSave:Boolean, completionCallback:Function, replaceExisting:Boolean):void
		{
			var name:String = gadget.name;

            var project:Project = new GadgetEncoder().encode(gadget, container, saveState, metaDataOnly, autosave, gadgets, assets);

            var operation:ProjectSaveOperation = new ProjectSaveOperation(project, false, replaceExisting);
			operation.addEventListener(GadgetEvent.SAVE_FAILED, genericSaveFailure);
			operation.addEventListener(GadgetEvent.SAVE_NO_PERMISSION, handleSaveNoPermission(name));
			operation.addEventListener(GadgetEvent.NAME_IN_USE, handleNameInUse(name, templateSave));
			operation.addEventListener(GadgetEvent.ERROR, handleError);
			operation.addEventListener(Event.COMPLETE, handleSaveOneComplete(gadget, operation, completionCallback));
			addToSavingQueue(operation, false);  // execute the save operations in sequence, in the background
		}

		private function handleSaveOneComplete(gadget:Project, operation:ProjectSaveOperation, completionCallback:Function):Function
		{
			return function(event:Event):void
			{
				if (gadget.name == null || gadget.name.length == 0)
					gadget.name = gadget.projectId;
				
				if (completionCallback != null)
					completionCallback(gadget, gadget.projectId);
			}
		}

        private function genericSaveFailure(event:GadgetEvent):void{
            var op:ProjectSaveOperation = event.target as ProjectSaveOperation;
            var errorMessage:String = "Error saving gadget "+op.projectName;
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:55, inserts:[errorMessage]}));
        }

		public function addToSavingQueue(operation:IOperation, synchronous:Boolean = true):void
		{
            var runnable:PseudoThreadRunnable;
			if(operation is ProjectSaveOperation) {
                runnable = new ProjectSaveRunnable(operation as ProjectSaveOperation, synchronous);
            } else {
                runnable = new PseudoThreadOperationRunnable(operation);
            }
            if(PseudoThread.instance.context) {
                PseudoThread.instance.context.add(runnable);
            } else {
                PseudoThread.instance.start(new PseudoThreadContext(new <PseudoThreadRunnable>[runnable]));
            }
		}
		
		protected function handleSaveFault(event:Event):void
		{
		}
		
		protected function handleNameInUse(name:String, templateSave:Boolean):Function
		{
			return function(event:GadgetEvent):void
			{
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:42, closeFunction:handleSaveOver(name, event.id, templateSave), inserts:[name]}));
			}
		}
		
		protected function handleSaveNoPermission(name:String):Function
		{
			return function(event:GadgetEvent):void
			{
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:43, closeFunction:handleSaveAsCopy(name), inserts:[name, name]}));
			}
		}
		
		protected function handleSaveOver(name:String, objectID:String, templateSave:Boolean):Function
		{
			return function(event:CloseEvent):void {
				// placeholder for saving one gadget over another
			};
		}

        protected function handleSaveAsCopy(name:String):Function{
            return function(event:CloseEvent):void{
                //Save As Copy Code
            }
        }

        /**
         * renameGadget
         * completion = function(success:Boolean, errorMessage:String):void
         *
         * Has only been tested with projects, not individual gadgets.
         */
        public function renameGadget(project:Project, newName:String, completion:Function):void
        {
            var op:ProjectRenameOperation = new ProjectRenameOperation(project, newName);
            op.addEventListener(ErrorEvent.ERROR, function(e:ErrorEvent):void{
                completion(false, e.text as String);
            });
            op.addEventListener(Event.COMPLETE,  function(e:Event):void{
                completion(true, "");
            });
            op.execute();
        }

		public function dump():void
		{
			trace("");
			trace("Dump gadgets:");
			for each (var g:Project in model.children) {
				trace("  ",g);
			}
		}
	}
}
