package com.alleni.author.controller.ui
{
import com.alleni.author.Navigation.FlowView;
import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.palettes.DockController;
	import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.IListable;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.DropFeedback;
	import com.alleni.taconite.dev.Utilities;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;

	public class DropFeedbackController
	{
		private static var _instance:DropFeedbackController;
		
		
		public function DropFeedbackController()
		{
			Utilities.assert(!_instance);
		}
		
		public static function get instance():DropFeedbackController
		{
			if (!_instance)
				_instance = new DropFeedbackController();
			return _instance;
		}
		
		public function testForDropContainer(e:MouseEvent, isLibraryItem:Boolean = false):DisplayObject
		{
            var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
            var view:DisplayObject = LibraryController.instance.getViewIfOver(e);

//            trace("testForDropContainer view="+view, "related="+DragDropMediator.relatedObject);
			if (view) { // allow drag from dock to library
                // clear out the view reference to prevent dropping on the library for now.
				view = null;
			} else if (DockController.instance.getViewIfOver(e)) {
				//view = DockController.instance.getViewIfOver(e);
				view = null; // no drop on dock yet
			} else { // potentially over the world or a specific object within

				if (!worldContainer) { 
					view = null;
				} else {
					var isOverStagingArea:Boolean = ApplicationController.instance.isOverStagingArea(e);
					if (worldContainer.worldView.hitTestPoint(e.stageX, e.stageY) && isOverStagingArea)
						view = worldContainer.worldView;
					else if (isOverStagingArea)
						view = EditorUI.instance.stagingBackground;
					else
						view = null;
				}
			}
			DragDropMediator.highlight = (view != null);
            if(!isLibraryItem){
			    DropFeedback.showPendingDrop(view)
            }
				
			return view;
		}
		
		public function reset():void
		{
			DropFeedback.clear();	
		}
	}
}