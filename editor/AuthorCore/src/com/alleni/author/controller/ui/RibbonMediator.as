package com.alleni.author.controller.ui
{
	import com.alleni.taconite.controller.DragMediator;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;

	public class RibbonMediator extends DragMediator
	{
		private var _view:DisplayObject;
		
		public function RibbonMediator(context:ViewContext)
		{
			super(context);		
		}
		
		/**
		 * When asked to work with a ObjectView, take note of the view and add a listener for mouseDown.
		 */
		public function handleViewEvents(view:DisplayObject):void
		{
			_view = view;
			_view.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		}
		
		/**
		 * We really want the mouse down event so we will aggressively stop propagation. 
		 * @param e
		 * 
		 */
		override public function handleMouseDown(e:MouseEvent):void
		{
			super.handleMouseDown(e);
			e.stopImmediatePropagation();
		}
		
		/**
		 * Handle the termination of a gesture during which the mouse effectively did not move. 
		 */
		override protected function handleClick(e:MouseEvent):void
		{
			// use this to handle the click event
		}
	}
}