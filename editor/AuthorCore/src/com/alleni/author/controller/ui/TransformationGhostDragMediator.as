package com.alleni.author.controller.ui
{
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.feedback.TransformationGhost;
	import com.alleni.taconite.controller.DragMediator;
	import com.alleni.taconite.definition.Handles;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class TransformationGhostDragMediator extends DragMediator
	{
		private var _ghost:TransformationGhost;
		private var _rotation:Boolean;
		private var _initialDragPoint:Point;
	
		private var _initialPos:Point;
		private var _initialRotation:Number;
		
		private var _feedBackView:Sprite = WorldContainer(Application.instance.viewContext).feedbackHandlesView;
	
		public function TransformationGhostDragMediator(context:ViewContext, ghost:TransformationGhost, rotation:Boolean)
		{
			super(context, true);
			_ghost = ghost;
			_rotation = rotation;
			(rotation?_ghost.rotationHandle:_ghost).addEventListener(MouseEvent.MOUSE_DOWN, this.handleMouseDown);
		}
	
		private function feedBackToLocal(pt:Point):Point
		{
			return _ghost.target.getView(Application.instance.viewContext).parent.globalToLocal(_feedBackView.localToGlobal(pt));
		}
		
		override public function handleMouseDown(e:MouseEvent):void
		{
			super.handleMouseDown(e);
			_initialDragPoint = feedBackToLocal(new Point(_context.mouseX, _context.mouseY));
			_initialPos = feedBackToLocal(new Point(_ghost.x, _ghost.y));
			_initialRotation = _ghost.rotation;
		}
	
		override protected function handleMouseMove(e:MouseEvent):void
		{
			if (!_dragStarted
				&& e.buttonDown
				&& (Math.abs(_ghost.mouseX - _initialDragPoint.x) >= minimumDrag
					|| Math.abs(_ghost.mouseY - _initialDragPoint.y) >= minimumDrag))
			{
				_dragStarted = true;
				handleDragStart(e);
			}
	
			super.handleMouseMove(e)
		}
	
		override protected function handleDragStart(e:MouseEvent):void
		{
			super.handleDragStart(e);
		}
		
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
		{
			super.handleDragMove(e);
			var dragPoint:Point = feedBackToLocal(new Point(_context.mouseX, _context.mouseY));
			
			if(_rotation) {
		            var handle:Point = new Point();
		            handle.x = _ghost.target.right + Handles.ROTATION_HANDLE_OFFSET;
		            handle.y = _ghost.target.bottom;
			        var handleAngle:Number = Math.atan2(handle.y, handle.x);
					// delta angle of mouse-move, since previous mouse-move & view update
			        var mouseAngle:Number = Math.atan2(_ghost.mouseY, _ghost.mouseX);
			      	var rot:Number = _ghost.rotation + (mouseAngle - handleAngle) * 180 / Math.PI;
	
					if(e.shiftKey) {
						rot = Math.floor(rot/45 + .5)*45;
					}
				_ghost.editor.transformationFromGhost(new PositionAndAngle(
						_initialPos.x,
						_initialPos.y,
						rot))
			} else {
				if(e.shiftKey) {
					if(_dragPoint.x < _dragPoint.y) {
						_dragPoint.x = 0;
					} else {
						_dragPoint.y = 0;
					}
				}
				_ghost.editor.transformationFromGhost(new PositionAndAngle(
						dragPoint.x - _initialDragPoint.x + _initialPos.x,
						dragPoint.y - _initialDragPoint.y + _initialPos.y,
						_initialRotation))
			}
		}
	
		override protected function handleDragEnd(e:MouseEvent):void
		{
			super.handleDragEnd(e);
		}
	}
}