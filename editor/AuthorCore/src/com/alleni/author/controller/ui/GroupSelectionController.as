package com.alleni.author.controller.ui
{
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.ui.GroupSelectionFeedback;
import com.alleni.taconite.document.ISelection;
import com.alleni.taconite.document.ObjectSelection;
import com.alleni.taconite.model.GroupSelectionObject;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.model.TaconiteObject;
import com.alleni.taconite.view.TaconiteView;

import flash.utils.Dictionary;

	public class GroupSelectionController
	{
		private static var _feedbackList:Dictionary = new Dictionary();
		private static var _selectedGroups:Vector.<GroupSelectionObject> = new Vector.<GroupSelectionObject>;
				
		public static function makeGroup(selection:ISelection):GroupSelectionObject {
			var group:GroupSelectionObject = new GroupSelectionObject(selection);
			for each (var model:TaconiteModel in selection.selectedModels) {
					if (model.value.group) {
						dischargeGroup(model.value.group);
					}
					(model.value as TaconiteObject).group = group;
			}
			
			var feedback:GroupSelectionFeedback = new GroupSelectionFeedback(WorldContainer(Application.instance.viewContext),group);
			feedback.initialize();
			_feedbackList[group.uid] = feedback;
			
			return group;
		}
		
		public static function makeGroupFromModels(models:Vector.<TaconiteModel>):GroupSelectionObject {
			var world:World = Application.instance.document.root.value as World;
			return makeGroup(new ObjectSelection(world.modelRoot, models));
		}
		
		public static function dischargeSelection(selection:ISelection):Vector.<GroupSelectionObject> {
			var result:Vector.<GroupSelectionObject> = new Vector.<GroupSelectionObject>();
			for each (var group:GroupSelectionObject in topGroupsFromModels(selection.selectedModels)) {
				result.push(group);
				dischargeGroup(group);
			}
			return result;
		}
		
		public static function dischargeGroup(group:GroupSelectionObject):ISelection {
			var models:Vector.<TaconiteModel> = group.selection.selectedModels;
			for each (var model:TaconiteModel in models) {
				if ((model.value as TaconiteObject).group == group)
					(model.value as TaconiteObject).group = null;
			}
			
			if (_feedbackList[group.uid]) {
				WorldContainer(Application.instance.viewContext).feedbackGroupHandlesView.removeChild(_feedbackList[group.uid]);
				(_feedbackList[group.uid] as TaconiteView).warnViewToDelete();
				delete _feedbackList[group.uid];
			}
			
			return new ObjectSelection(Application.instance.document.root,models);
		}

		public static function flattenSelection(s:Vector.<TaconiteModel>):Vector.<TaconiteModel> {
			trace ("flattenSelection");
			var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>;
			for each (var model:TaconiteModel in s) {
                if (model.parent == null) continue;
				if (model.value as GroupSelectionObject) {
					models = models.concat(flattenSelection((model.value as GroupSelectionObject).selection.selectedModels));
				} else {
					models.push(model);
				}
			}
			return models;
		}
		
		public static function growSelection(s:Vector.<TaconiteModel>):Vector.<TaconiteModel> {
			var sel:Vector.<TaconiteModel> = flattenSelection(s);
			for each (var g:GroupSelectionObject in topGroupsFromModels(s)) {
				sel = sel.concat(modelsForGroup(g));
			}
			return sel;
		}
		
		public static function modelsForGroup(g:GroupSelectionObject):Vector.<TaconiteModel> {
			var models:Vector.<TaconiteModel> = flattenSelection(g.selection.selectedModels);
			return models;
		}
		
		public static function topGroupsFromModels(s:Vector.<TaconiteModel>):Vector.<GroupSelectionObject> {
			//trace("America's Next Top Models for groups");
			var groups:Vector.<GroupSelectionObject> = new Vector.<GroupSelectionObject>();
			for each (var model:TaconiteModel in s) {
				var group:GroupSelectionObject = model.value.group;
                if (group == null && model.value.groupID != null) {
                    group = makeGroupFromObjectGroupID(model.value);
                }
				if (group != null && group.group == null && groups.indexOf(group) == -1) {
					groups.push(group);
				}
			}
			return groups;
		}

        public static function makeGroupFromObjectGroupID(obj:TaconiteObject):GroupSelectionObject {
            var world:World = Application.instance.document.root.value as World;
            var queue:Array = [obj.model.parent.value];
            var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
            while(queue.length > 0) {
                var parent:AbstractContainer = queue.shift();
                for each(var candidate:TaconiteObject in parent.objects) {
                    if(candidate.groupID == obj.groupID) {
                        models.push(candidate.model);
                    } else if(candidate is AbstractContainer && !(candidate is Composite)) {
                        queue.push(candidate);
                    }
                }
            }
            return makeGroup(new ObjectSelection(world.modelRoot,models));
        }
		
		public static function showFeedbackFor(selection:ISelection,show:Boolean = true,extend:Boolean = false):void {
			if (!extend || selection == null) {
				for each (var f:GroupSelectionFeedback in _feedbackList) {
					f.visible = false;
				}
			}
			
			if (selection == null)
				return;

			for each (var g:GroupSelectionObject in topGroupsFromModels(selection.selectedModels)) {
				if (_feedbackList[g.uid] != null) {
					(_feedbackList[g.uid] as GroupSelectionFeedback).visible = show;
				} else {
					var newFeedback:GroupSelectionFeedback = new GroupSelectionFeedback(WorldContainer(Application.instance.viewContext),g);
					_feedbackList[g.uid] = newFeedback;
					newFeedback.visible = show;
					newFeedback.initialize();
				}
			}
		}
		
		public function get selectedGroups():Vector.<GroupSelectionObject> {
			var groups:Vector.<GroupSelectionObject> = new Vector.<GroupSelectionObject>();
			for each (var g:GroupSelectionFeedback in _feedbackList) {
				if (g.visible)
					groups.push(g);
			}
			return groups;
		}

		public static function hideFeedbackFor(selection:ISelection):void {
			showFeedbackFor(selection,false);
		}
	}
}