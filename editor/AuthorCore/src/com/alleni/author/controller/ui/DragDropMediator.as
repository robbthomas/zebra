package com.alleni.author.controller.ui
{
	import com.alleni.author.definition.application.MouseSprites;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ui.DropFeedback;
	import com.alleni.taconite.dev.BitmapUtils;
	import com.alleni.taconite.dev.Utilities;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class DragDropMediator
	{
		private static var _instance:DragDropMediator;
		
		public static  var relatedObject:*;
		
		private static var _dragThumbnail:DisplayObject;
		private static var _shadow:DropShadowFilter;
		private static var _glow:GlowFilter;
		private static var _highlighted:Boolean;
		private static var _disclosureIcon:DisplayObject;
		
		public function DragDropMediator()
		{
			Utilities.assert(!_instance);
		}
		
		public static function get instance():DragDropMediator
		{
			if (_instance == null)
				_instance = new DragDropMediator();
			return _instance;
		}
		
		public static function get worldMousePoint():Point
		{
			return new Point(Application.instance.viewContext.mouseX, Application.instance.viewContext.mouseY);
		}
		
		public static function get dragThumbnailCopy():DisplayObject
		{
			if (_disclosureIcon) _disclosureIcon.visible = false;
			var copy:DisplayObject = BitmapUtils.cacheMe(_dragThumbnail, null, true);
			if (_disclosureIcon) _disclosureIcon.visible = true;
			return copy;
		}
		
		public static function startDrag(drawable:DisplayObject, sourceObject:*=null):void
		{
			relatedObject = sourceObject;
			
			var bounds:Rectangle = drawable.getBounds(drawable);
			
			var dragBitmapData:BitmapData = new BitmapData(bounds.width+5, bounds.height+5, true, 0x00ff0000);
			var matrix:Matrix = new Matrix();
			matrix.tx = -bounds.x;
			matrix.ty = -bounds.y;
			
			// hang onto the drawable's filters for a moment while we render the drag thumb, then add them back
			var filterHolder:Array = drawable.filters;
			drawable.filters = [];
			dragBitmapData.draw(drawable, matrix, null, BlendMode.MULTIPLY, null, false);
			drawable.filters = filterHolder;
			filterHolder = null;
				
			var dragBitmap:Bitmap = new Bitmap(dragBitmapData);
			dragBitmap.alpha = 0.6;
			var sprite:Sprite = new Sprite();
			sprite.addChild(dragBitmap);
			
			var dragOffset:Point;
			var originItem:Sprite = drawable as Sprite;
			
			if (originItem)
				dragOffset = originItem.localToGlobal(new Point(0,0));
			else
				dragOffset = new Point(dragBitmap.width/2, dragBitmap.height/2);
			sprite.x = dragOffset.x + bounds.x;
			sprite.y = dragOffset.y + bounds.y;
			sprite.alpha = 0.8;
			drawable.stage.addEventListener(MouseEvent.MOUSE_UP, handleDrop);
			sprite.startDrag();
			sprite.filters = [shadow];
			
			_dragThumbnail = sprite;
			drawable.stage.addChild(_dragThumbnail);
		}
		
		private static function get shadow():DropShadowFilter
		{
			if (!_shadow)
				_shadow = new DropShadowFilter(4.0, 80.0, 0x000000, 0.5, 4.0, 4.0, 0.8, 3);
			return _shadow;
		}
		
		private static function get glow():GlowFilter
		{
			if (!_glow)
				_glow = new GlowFilter(DropFeedback.COLOR, 0.9, 4, 4, 4, 3);
			return _glow;
		}
		
		public static function set highlight(value:Boolean):void
		{
			if (!_dragThumbnail) return;
			
			var sprite:Sprite = _dragThumbnail as Sprite;
			
			if (value && !_highlighted) {
				_highlighted = true;
				
				// fade-in glow on drag thumbnail
				/*glow.alpha = 0;
				Tweener.addTween(glow, {alpha:1, time:0.5, transition:EditorUI.ANIMATION_IN_ALGORITHM, onUpdate:function():void {
					if (!_dragThumbnail) return;
					_dragThumbnail.filters = [shadow, glow];
				}});*/
				
				// "plus" disclosure icon
				_disclosureIcon = MouseSprites.plus;
				_disclosureIcon.x = _dragThumbnail.mouseX - 1;
				_disclosureIcon.y = _dragThumbnail.mouseY + 12;
				sprite.addChild(_disclosureIcon);
			} else {
				if (value || !_highlighted) return;
				
				_highlighted = false;
				/*Tweener.addTween(glow, {alpha:0, time:0.5, transition:EditorUI.ANIMATION_IN_ALGORITHM, 
					onUpdate:function():void {
						if (!_dragThumbnail) return;
						_dragThumbnail.filters = [shadow, glow];
					},
					onComplete:function():void {
						if (!_dragThumbnail) return;
						_dragThumbnail.filters = [shadow];
					}
				});*/
				
				if (!_disclosureIcon) return;
				if (sprite.contains(_disclosureIcon))
					sprite.removeChild(_disclosureIcon);
				_disclosureIcon = null;
			}
		}
		
		private static function handleDrop(event:MouseEvent):void
		{
			event.target.removeEventListener(event.type, handleDrop);
			if (_dragThumbnail && _dragThumbnail.parent) {
				if (_dragThumbnail.parent.getChildIndex(_dragThumbnail) > -1)
					_dragThumbnail.parent.removeChild(_dragThumbnail);
			}
		}
	}
}