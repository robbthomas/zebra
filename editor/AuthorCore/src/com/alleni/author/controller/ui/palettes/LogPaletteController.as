package com.alleni.author.controller.ui.palettes
{
	import com.alleni.author.application.palettes.LogPanel;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.taconite.controller.ui.SecondaryWindowController;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.service.LogService;
	
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	public class LogPaletteController extends SecondaryWindowController
	{
		public static var instance:LogPaletteController = new LogPaletteController();
		
		private var _logPanel:LogPanel;
		
		public function LogPaletteController()
		{
			super(new Rectangle(0, 25, 700, 500), TaconiteFactory.getAppDescriptorImplementation().longName + ": " + "LOGGING", false, true);  // resizable=false, utility=true
			_window.addEventListener(Event.CLOSE, handleWindowClose);

			/*if (LogService.enabled)  do not always create the logger, only when requested.
				initializeLogger();*/
		}
		
		public function initializeLogger():void
		{
			if (!_logPanel)
				_logPanel = new LogPanel();
			_window.addElement(_logPanel);
			if (!LogService.enabled) LogService.enabled = true;
		}
		
		private function handleWindowClose(event:Event):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.HIDE_LOG_WINDOW));
		}
		
		override public function setVisible(value:Boolean):void
		{
			super.setVisible(value);
			if (value && !_logPanel)
				initializeLogger();
		}
		
		override public function exit():void
		{
			super.exit();
			// something?
		}
		
		override protected function activate():void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
		}
		
		override protected function deactivate():void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
		}
	}
}