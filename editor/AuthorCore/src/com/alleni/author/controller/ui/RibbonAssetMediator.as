package com.alleni.author.controller.ui
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.controller.ui.palettes.AbstractItemMediator;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.model.objects.IListable;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.Poof;
	import com.alleni.author.view.ui.editors.RibbonAssetEditor;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class RibbonAssetMediator extends AbstractItemMediator
	{
		private var _ribbonAssetView:RibbonAssetEditor;
		

		public function RibbonAssetMediator()
		{
			super();
		}
		
		override public function handleViewEvents(view:DisplayObjectContainer, item:IListable):AbstractItemMediator
		{
			_ribbonAssetView = view as RibbonAssetEditor;
			return super.handleViewEvents(view,item);
		}

		override public function handleMouseDown(e:MouseEvent):void
		{
			var asset:Asset = _ribbonAssetView.asset;
			if (asset) {
				_item = asset;
				super.handleMouseDown(e);
			} else {
				e.stopImmediatePropagation();
			}
		}
		
		override protected function handleDragStart(e:MouseEvent):void
		{
			super.handleDragStart(e);  // captures image of view for dragging

			if (!e.altKey) {  // when Alt key held down, its a drag-copy
				_ribbonAssetView.asset = null;
			}
		}
		
		override protected function dropAssetWithUndo(item:Asset, x:Number, y:Number, altKey:Boolean, withContainer:Boolean):void
		{
			if (ReplacementController.instance.isPending || altKey) {
				super.dropAssetWithUndo(item, x, y, altKey, withContainer);
			} else {
				ReplacementController.instance.reset();  // dragged out of ribbon to empty place on stage -- clear the ribbon or table cell
				poof(x, y);
			}
            LibraryController.instance.updateForAssetChange();
		}
		
		private var _poof:Poof;
		
		private function poof(xx:Number, yy:Number):void
		{
			var container:DisplayObjectContainer = poofParent;
			_poof = new Poof();
			var global:Point = worldView.localToGlobal(new Point(xx,yy));
			var local:Point = container.globalToLocal(global);
			_poof.x = local.x;
			_poof.y = local.y;
			container.addChild(_poof);
			
			_poof.frac = 1;
			Tweener.addTween(_poof, {frac:0.5, time:0.7, onComplete:removePoof});
		}
		
		private function removePoof():void
		{
			poofParent.removeChild(_poof);
		}
		
		private static function get poofParent():DisplayObjectContainer
		{
			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			return worldContainer.feedbackControlsView;
		}
		
		private static function get worldView():DisplayObjectContainer
		{
			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			return worldContainer.worldView as DisplayObjectContainer;
		}
	}
}
