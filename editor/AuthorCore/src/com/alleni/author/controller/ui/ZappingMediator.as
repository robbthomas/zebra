/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 1/3/12
 * Time: 2:28 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.ui {
    import caurina.transitions.Tweener;

    import com.alleni.author.application.dialogs.ZappPublishingDialog;
    import com.alleni.author.controller.app.ProjectController;
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.gadgets.GadgetIconReference;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Dock;
	import com.alleni.author.service.ThumbnailLoadingPipeline;
	import com.alleni.author.service.gadgets.GadgetIconOperation;
    import com.alleni.author.service.users.UserService;
import com.alleni.author.util.StringUtils;
import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichLabel;
	import com.alleni.author.view.ui.components.ComboBox;
	import com.alleni.author.view.ui.components.PushButton;
	import com.alleni.author.view.ui.components.SearchComboBox;
	import com.alleni.author.view.ui.components.Style;
import com.alleni.author.view.ui.palettes.ZappingDialogFormView;
import com.alleni.taconite.dev.BitmapUtils;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.service.LogService;

	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
    import flash.utils.ByteArray;

	import mx.events.CollectionEvent;
	import mx.events.PropertyChangeEvent;

	import spark.components.Button;

    public class ZappingMediator extends GadgetPublishingMediator {
        private var _shareTo:String;
        private var _iconMediumUri:String;
        private var _companyLabel:LightLabel;
        private var _formView:ZappingDialogFormView;

        public function ZappingMediator(targetProperty:String, container:DisplayObjectContainer, shareTo:String = NotificationNamesApplication.SHARE_TO_LINK) {
            _shareTo = shareTo;
            super(targetProperty, container);
        }

        /**
		 * monster method and class. easy candidate for refactor once basic functionality is validated. since functionality has been changing,
		 * we have not yet had the opportunity.
		 *
		 */
         public function handleDialogEvents(headerLabel:RichLabel, nameBox:SearchComboBox, descriptionField:LightEditableLabel, authorLabel:LightLabel,
												iconContainer:DisplayObjectContainer, screenshotThumbnails:Sprite, button:InteractiveObject, categoryMenu:ComboBox,
												cancelButton:InteractiveObject, companyLabel:LightLabel, formView:ZappingDialogFormView = null):void{
            _headerLabel = headerLabel;
			_screenshotThumbnails = screenshotThumbnails;
            _container.addEventListener(Event.CLOSE, handleContainerClose);
			setNameBox(nameBox, 50);
            setDescriptionField(descriptionField, 725);
			_authorLabel = authorLabel;
			_iconContainer = iconContainer;
			_cancelButton = cancelButton;
			this.button = button;
			_categoryMenu = categoryMenu;
			_categoryMenu.addEventListener(Event.SELECT, handleCategorySelect);
            _companyLabel = companyLabel;
            _iconContainer.addEventListener(MouseEvent.CLICK, handleIconClick);
			_iconContainer.addEventListener(MouseEvent.MOUSE_OVER, handleIconOver);
			_iconContainer.addEventListener(MouseEvent.MOUSE_OUT, handleIconOut);
            _formView = formView;
			updateValues(true);

			GadgetController.instance.model.gadgets.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleCollectionChange);
            Dock.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
			Dock.instance[_targetProperty].addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);

			_cancelButton.addEventListener(MouseEvent.CLICK, handleCancelClick);

            ApplicationController.addEventListener(NotificationNamesApplication.SCREENSHOT_PANEL_CLOSED, screenshotPublishDialogShow);
            ApplicationController.addEventListener(NotificationNamesApplication.SCREENSHOT_PANEL_SHOWN, screenshotPublishDialogHide);
		}

        /**
		 *
		 * @param reselect whether or not we're onto a different item
		 *
		 */
		override protected function updateValues(reselect:Boolean=false, fromFindOperation:Boolean=false):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;

			if (!gadget)
				return;

			if (reselect) {
				showProgress = false;
				if (GadgetController.instance.confirmCanPublish(gadget) && !gadget.minPrice) {
					// Ultimately, minprice will be maintained by business rules, so the final safeguard will be the back-end logic
					//  which will respond with cmd error if this is still too low.
					if (gadget.minPrice < 25) gadget.minPrice = 25;
				}
				if (!gadget.published && !_pendingFindOperation) {
                    //TODO GADGET_PROJECT still needed?
//					_pendingFindOperation = new PublishedFindOperation(gadget);
//					_pendingFindOperation.addEventListener(Event.COMPLETE, function():void {
//						showProgress = false;
//						_pendingFindOperation = null;
//						updateValues(false, true);
//					});
//					showProgress = true;
//					GadgetController.instance.addToSavingQueue(_pendingFindOperation);

					_nameText = GadgetDescription.cleanName(gadget, true);
					_descriptionText = gadget.description != ""?gadget.description:"{Enter Description}";
					_categoryId = gadget.categoryId;
				}
			}

			if (reselect && !_pendingFindOperation || fromFindOperation) {
				_nameText = GadgetDescription.cleanName(gadget, true);
				_descriptionText = gadget.description != ""?gadget.description:"{Enter Description}";
				_categoryId = gadget.categoryId;
			}

			if (!_pendingFindOperation || fromFindOperation) {
				refreshIcon(null, reselect);
            	refreshScreenshot(reselect);
			}

			const canPublish:Boolean = GadgetController.instance.confirmCanPublish(gadget);
			const IS_ZAPP:Boolean = getIsZappFor(Dock.instance[_targetProperty]);

			if (PUBLISHING && canPublish)
				_headerLabel.text = IS_ZAPP?"Zap Project":"Publish to My Gadgets";
			else
				_headerLabel.text = IS_ZAPP?"App Information":"Gadget Information";

			var textIsDefault:Boolean = (!_nameText || _nameText == GadgetDescription.DEFAULT_NAME || _nameText == "" || _nameText == UNTITLED);
			const newName:String = textIsDefault ? UNTITLED : _nameText;
			if (!_nameBox.selectedItem){
				_nameBox.defaultLabel = newName;
            }else{
				_nameBox.textField.rawText = newName;
            }
			_nameBox.alpha = textIsDefault ? 0.5 : 1;
			_nameBox.textField.selectAll();
			_nameBox.textField.italic = textIsDefault ? true : false;
			_descriptionField.text = _descriptionText;

			var i:int;
			var length:int = _categoryMenu.items.length;
			var set:Boolean = false;
			for (i = 0; i < length; i++) {
				if ("id" in _categoryMenu.items[i])
					if (_categoryMenu.items[i].id == _categoryId) {
						_categoryMenu.selectedIndex = i;
						set = true;
					}
			}

			if (!set)
				_categoryMenu.selectedIndex = 0;

			if (!_button) return;

			_nameBox.textField.editable = canPublish;
            _nameBox.textField.addEventListener(KeyboardEvent.KEY_UP, checkPublishedName);
			_descriptionField.editable = canPublish;
            _categoryMenu.enabled = canPublish;

			const pushButton:PushButton = _button as PushButton;
			const cancelButton:PushButton = _cancelButton as PushButton;
			const owned:Boolean = GadgetController.instance.confirmOwnership(gadget);

			_authorLabel.text = canPublish ? GadgetDescription.authorNameForMe() : GadgetDescription.authorNameForGadget(gadget);
            _companyLabel.text = GadgetDescription.companyNameForMe();

            if (_button as PushButton) {
                Style.setStyle(Style.LIGHT);
                cancelButton.enabled = canPublish || !gadget.projectId;
                cancelButton.visible = canPublish || !gadget.projectId;
                updateButtons(null);
                pushButton.active = (canPublish || !gadget.projectId);
                pushButton.enabled = canPublish && !_pendingFindOperation;
                pushButton.visible = (canPublish || !gadget.projectId) || owned;
            } else if (_button as Button) {
                var sparkButton:Button = _button as Button;
                sparkButton.enabled = canPublish && !_pendingFindOperation;
            }
		}

        private function checkPublishedName(event:Event):void{
            _nameBox.addEventListener(Event.ENTER_FRAME, updateButtons);
        }

        private function updateButtons(event:Event):void{
            _nameBox.removeEventListener(Event.ENTER_FRAME, updateButtons);
            var gadget:Project = (Dock.instance[_targetProperty] as Project);
            var zapButton:PushButton = (_button as PushButton);
            var cancelButton:PushButton = (_cancelButton as PushButton);
            if(gadget.publishedName){
                if(_nameBox.textField.text == gadget.publishedName){
                    zapButton.label = "Update App";
                    zapButton.width = 100;
                    zapButton.parent.getChildAt(0).width = 230;
                }else{
                    zapButton.label = "Create New Version";
                    zapButton.width = 175;
                    zapButton.parent.getChildAt(0).width = 155;
                }
            }else{
                zapButton.label = "Zap Project";
            }
            if(StringUtils.isNameSafeForServer(_nameBox.textField.text).valid == false){
                _nameBox.textField.color = 0xFF0000;
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:999, inserts:["project", StringUtils.isNameSafeForServer(_nameBox.textField.text).reason]}));
                _validName = false;
                zapButton.enabled = false;
                zapButton.removeEventListener(MouseEvent.CLICK, handleButtonClick);
            }else{
                _nameBox.textField.color = 0x000000;
                _validName = true;
                zapButton.enabled = true;
                zapButton.addEventListener(MouseEvent.CLICK, handleButtonClick);
            }
        }

        override protected function refreshIcon(event:Event=null, reselect:Boolean=false):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;

			if (!gadget)
				return;

			var operation:GadgetIconOperation = gadget.pendingIconOperation as GadgetIconOperation;
			if (operation && operation.data) {
				var data:ByteArray = operation.data[0];

				// set up icon preview image
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e:Event):void {
					clearIconOverlay();
					setIcon(BitmapUtils.makePreviewRoundedIcon(loader.content as Bitmap, 75, 75 * 0.2), true);
				});
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, function(e:Event):void {
					LogService.error("Error loading bytes for preview icon data");
				});
				loader.loadBytes(data);
				return;
			}

            if(gadget.localIcon){
                setIcon(gadget.localIcon)
                return;
            }

			var iconURL:String;
			if (gadget.icon && gadget.icon.mediumURL){
				iconURL = gadget.icon.mediumURL;
            }else if(_iconMediumUri != null){
                iconURL = _iconMediumUri;
            }else{
				iconURL = GadgetIconReference.DEFAULT_BASE_URL + GadgetIconReference.MEDIUM_SIZE + GadgetIconReference.DEFAULT_EXTENSION;
            }

			if (reselect) {
				Tweener.addTween(_iconContainer, {alpha:0, time:0.15, transition:"easeOutQuart",
					onComplete:function():void {
						_iconContainer.visible = false;
					}});
                try{
                    if (_pendingIconLoader) {
                        _pendingIconLoader.close();
                        _pendingIconLoader = null;
                    }
                }catch (e:Error){}
			}

			_pendingIconLoader = ThumbnailLoadingPipeline.push(iconURL, function(event:Event):void {
				var loaderInfo:LoaderInfo = LoaderInfo(event.target);
				(loaderInfo.loader.content as Bitmap).smoothing = true;
				setIcon(loaderInfo.loader.content);
                clearIconOverlay();
				_pendingIconLoader = null;
			});
		}

        override protected function checkFollowingEditEvent(event:Event=null):void
		{
			if (_pendingFindOperation){
                return;
            }

			var gadget:Project = Dock.instance[_targetProperty] as Project;
			if (!gadget){
                return;
            }

			if (event && event.type == Event.SELECT) { // may be just the ComboBoxes adjusting
				if (!isNaN(_categoryId) && _categoryId != gadget.categoryId)
					gadget.metadataUpToDate = false;
				return;
			}

			if ((_nameText && _nameText != gadget.publishedName) || (_descriptionText && _descriptionText != gadget.description)){
				gadget.metadataUpToDate = false;
            }else{
				gadget.metadataUpToDate = true;
            }

			if (!gadget.metadataUpToDate){
				updateValues();
            }
		}

        override protected function handleButtonClick(e:MouseEvent):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			if (!gadget) return;

			if (PUBLISHING) {
                gadget.hideInStoreList = true;
                gadget.inviteOnly = (Application.instance.customerType == UserService.USER_PRO);
			}

            super.handleButtonClick(e);
		}

        override protected function onZappingSuccess():void{
            /*
             * values in "params" -
             * params.panel:int
             * 		0 = Security Panel
             * 		1 = Share Panel
             * params.privacyLevel:int
             * 		0 = Use what the server gives you
             * 		1 = override with Guest List level
             * 		2 = override with Hidden(NOT in shopp)
             * 		3 = override with Shopp (Public)
             * params.shareDefault:int
             * 		0 = Default
             * 		1 = Guest List
             * 		2 = Embed
             * 		3 = LMS
             * 		4 = Direct Link
             * 		5 = Email
             * 		6 = Facebook
             * 		7 = Linked In
             * 		8 = Twitter
            */

            //TODO: We need to have the user level accounted for here, it will change the default of the privacyLevel
            var params:Object = {panel:0, shareDefault:0, privacyLevel:0};

            if(Application.instance.customerType == UserService.USER_PRO){
                params.privacyLevel = 1;
            }else{
                params.privacyLevel = 2;
            }

            switch(_shareTo){
                case NotificationNamesApplication.SHARE_TO_EMAIL_LIST:
                    params.shareDefault = 5;
                    break;
                case NotificationNamesApplication.SHARE_TO_EMBED:
                    params.shareDefault = 2;
                    break;
                case NotificationNamesApplication.SHARE_TO_FACEBOOK:
                    params.shareDefault = 6;
                    if(Application.instance.customerType == UserService.USER_PRO){
                        params.privacyLevel = 2;
                    }else{
                        params.privacyLevel = 3;
                    }
                    break;
                case NotificationNamesApplication.SHARE_TO_GUEST_LIST:
                    params.shareDefault = 1;
                    if(Application.instance.customerType == UserService.USER_PRO){
                        params.privacyLevel = 1;
                    }
                    break;
                case NotificationNamesApplication.SHARE_TO_LINK:
                    params.shareDefault = 4;
                    params.privacyLevel = 2;
                    break;
                case NotificationNamesApplication.SHARE_TO_LINKED_IN:
                    params.shareDefault = 7;
                    if(Application.instance.customerType == UserService.USER_PRO){
                        params.privacyLevel = 2;
                    }else{
                        params.privacyLevel = 3;
                    }
                    break;
                case NotificationNamesApplication.SHARE_TO_LMS:
                    params.shareDefault = 3;
                    break;
                case NotificationNamesApplication.SHARE_TO_SHOPP:
                    params.privacyLevel = 3;
                    params.shareDefault = 0;
                    break;
                case NotificationNamesApplication.SHARE_TO_TWITTER:
                    params.shareDefault = 8;
                    if(Application.instance.customerType == UserService.USER_PRO){
                        params.privacyLevel = 2;
                    }else{
                        params.privacyLevel = 3;
                    }
                    break;
                default:
                    params.shareDefault = 0;
            }

            params.justZapped = true;

            ProjectController.instance.openPrivacySharePanel(params);
            ZappPublishingDialog.requestHide();
        }
    }
}
