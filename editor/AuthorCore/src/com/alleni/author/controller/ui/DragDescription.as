package com.alleni.author.controller.ui {
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.model.AbstractObject;

import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.Sprite;

import flash.events.TimerEvent;
import flash.utils.Dictionary;
import flash.utils.Timer;

public class DragDescription {
	private static const MESSAGE_CENTER_DELAY:Number = 1000.0 * 0.5; // half a second

	private var _shownMessageCenter:AbstractObject = null; // the most recent MC shown by dragging wire over object

	private var _nextMessageCenter:AbstractObject = null; // the next MC that will be shown

	private var _shownMessageCenterWasVisible:Boolean;

	private var _excludedObjects:Dictionary = new Dictionary(true); // objects deselected by minus key while dragging

	private var _mcSwapTimer:Timer;

	protected var _context:ViewContext;

	public function DragDescription(context:ViewContext) {
        _context = context;
	}

	public function excludeMC():void {
		if (_shownMessageCenter && !_excludedObjects[_shownMessageCenter])
			_excludedObjects[_shownMessageCenter] = true;
	}

	public function get shownMessageCenter():TaconiteModel {
		if (!_shownMessageCenter)
			return null;
		return _shownMessageCenter.model;
	}

	public function set shownMessageCenter(value:TaconiteModel):void {
		if (!value) {
			clearTimer();
			return;
		}
		var ao:AbstractObject = value.value as AbstractObject;
		if (ao == _nextMessageCenter)
			return;
		if (ao == _shownMessageCenter) {
			clearTimer();
			return;
		}
		if (!ao.hasMessageCenter || ao.wiringLocked
				|| _excludedObjects[_shownMessageCenter]) {
			return;
		}
        var ep:EventPage = ao.containingEventPage;
        if (ep && ep.isMaster && !ep.editing) {   // can't wire to Master child unless editing the master
            return;
        }

		_nextMessageCenter = ao;
		if (!_mcSwapTimer) {
			_mcSwapTimer = new Timer(MESSAGE_CENTER_DELAY, 1);
			_mcSwapTimer.addEventListener(TimerEvent.TIMER, swapMC, false, 0, true);
		} else
			_mcSwapTimer.reset();
		_mcSwapTimer.start();
	}

	protected function swapMC(event:TimerEvent):void {
		var next:AbstractObject = _nextMessageCenter;
		if (_shownMessageCenter)
			_shownMessageCenter.messageCenterVisible = _shownMessageCenterWasVisible;
		_shownMessageCenterWasVisible = next.messageCenterVisible;
		next.messageCenterVisible = true;
		ApplicationController.instance.handleMessageCenterBringToFront(getMC(next, _context));
		_shownMessageCenter = next;
		clearTimer();
	}

    private static function getMC(obj:AbstractObject, context:ViewContext):MessageCenterView {
		var view:ObjectView = obj.getView(context);
        var mc:MessageCenterView = null;
        if(view == null) {
            var container:Sprite = WorldContainer(context).feedbackUIView;
            for(var i:int=0; i<container.numChildren; i++) {
                mc = container.getChildAt(i) as MessageCenterView;
                if(mc != null && mc.model.value == obj) {
                    return mc;
                } else {
                    mc = null;
                }
            }
        } else {
            return view.messageCenterView;
        }
        return null;
    }

	private function clearTimer():void {
		_nextMessageCenter = null;
		if (_mcSwapTimer) {
			_mcSwapTimer.removeEventListener(TimerEvent.TIMER, swapMC);
			_mcSwapTimer.stop();
			_mcSwapTimer = null;
		}
	}

	public function containerShouldOpen(container:String):Boolean {
		return true;
	}
}
}
