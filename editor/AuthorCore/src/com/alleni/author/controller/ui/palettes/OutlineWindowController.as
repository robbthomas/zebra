package com.alleni.author.controller.ui.palettes
{
	import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.OutlineView;
	import com.alleni.taconite.controller.ui.SecondaryWindowController;
import com.alleni.taconite.document.ISelection;
import com.alleni.taconite.document.ObjectSelection;
import com.alleni.taconite.event.SelectEvent;
import com.alleni.taconite.factory.ITaconiteWindow;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;

import flash.events.Event;
	import flash.geom.Rectangle;
	
	import mx.core.UIComponent;
	import mx.events.ResizeEvent;
	
	
	public class OutlineWindowController extends SecondaryWindowController
	{
		public static var instance:OutlineWindowController = new OutlineWindowController();
		
		private var _outlineView:OutlineView;
		
		public function OutlineWindowController()
		{
			var ui:EditorUI = EditorUI.instance;
			var bounds:Rectangle = ui.canvasVisibleRect(ui.stage);
			const wd:Number = 200;
			const ht:Number = bounds.height * 2/3;
			var rect:Rectangle = new Rectangle(bounds.right - wd, bounds.bottom - ht -28, wd, ht);

			super(rect, "OUTLINE", true, true);  // resizable=true, utility=true
			_window.isPopup = true;
			_window.addEventListener(Event.CLOSE, handleWindowClose);
		}
		
		public function initializeOutline():void
		{
			_outlineView = new OutlineView();
			_window.addElement(_outlineView);
			_outlineView.updateOutline(world);

            Application.instance.document.addEventListener(SelectEvent.CHANGE_SELECTION, handleSelectionChanged);
		}
		
		override public function setVisible(value:Boolean):void
		{
			super.setVisible(value);
			if (value && !_outlineView)
				initializeOutline();
			
			_outlineView.updateOutline(world);
		}
		
		private function handleWindowClose(event:Event):void
		{
			Application.instance.outlineVisible = false;
		}

        public function showObject(obj:AbstractObject):void
        {
            Application.instance.outlineVisible = true;

            _outlineView.addEventListener(Event.ENTER_FRAME, function(e:Event):void{
                if (_outlineView.stage) {
                    _outlineView.removeEventListener(Event.ENTER_FRAME, arguments.callee);
                    _outlineView.selectObject(obj.uid);
                }
            });
        }

        private function handleSelectionChanged(e:SelectEvent):void
        {

            if(_outlineView == null || _outlineView.visible == false)
                return;

			var objSelection:ISelection = Application.instance.document.selection as ObjectSelection;
			var objects:Vector.<AbstractObject> = new Vector.<AbstractObject>();
			var model:TaconiteModel;
			for each(model in objSelection.selectedModels) {
				objects.push(model.value);
			}

            _outlineView.selectObjects(objects);
        }

		override public function exit():void
		{
			super.exit();
		}
		
		override protected function handleWindowResize(e:ResizeEvent):void
		{	
			trace("handleWindowResize e="+e);
			// implement as needed in subclass
		}

		
		override protected function activate():void
		{
		}
		
		override protected function deactivate():void
		{
		}
		
		private function get world():World
		{
			return Application.instance.document.root.value as World;
		}

	}
}