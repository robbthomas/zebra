package com.alleni.author.controller.ui
{
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.definition.DoubleClickAction;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.model.ui.Dock;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.ui.components.ComboBox;
	import com.alleni.author.view.ui.components.DotPaging;
	import com.alleni.author.view.ui.components.DotPagingItem;
	
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class DockControlsMediator
	{
		public function DockControlsMediator()
		{
		}
		
		public function handleControlEvents(searchField:LightEditableLabel, viewMenu:ComboBox, categoryMenu:ComboBox, sortMenu:ComboBox, dotPaging:DotPaging):DockControlsMediator
		{
			this.searchField = searchField;
			viewMenu.addEventListener(Event.SELECT, handleViewSelect);
			categoryMenu.addEventListener(Event.SELECT, handleCategorySelect);
			sortMenu.addEventListener(Event.SELECT, handleSortSelect);
			dotPaging.addEventListener(MouseEvent.MOUSE_DOWN, handleDotMouseDown);
			
			return this;
		}
		
		private function set searchField(value:LightEditableLabel):void
		{
			value.mediator.maxChars = 40;
			value.mediator.setCallBacks({
				okEditFunc:
				function(e:Event):int{
					return DoubleClickAction.EDIT;
				},
				updateFunc:null,
				keyPressFunc:function(charCode:uint):uint{
					return TextEditMediator.closeOnEnter(charCode);
				},
				closeFunc:
				function(val:String):void{
					Dock.instance.searchField = FormattedText.convertToString(val as String);
					value.text = Dock.instance.searchField;
					value.edit = false;
				},
				hittestFunc:
				function(x:Number, y:Number):Boolean{
					if(value.hitTestPoint(x,y)){
						return true;
					}
					else{
						return false;
					}
				}
			});
		}
		
		private function handleViewSelect(event:Event):void
		{
			handleSelect(event, "selectedView");
		}
		
		private function handleCategorySelect(event:Event):void
		{
			handleSelect(event, "selectedCategoryId");
		}
		
		private function handleSortSelect(event:Event):void
		{
			handleSelect(event, "selectedSort");
		}
		
		private function handleSelect(event:Event, property:String):void
		{
			var menu:ComboBox = (event.target as ComboBox);
			if (!menu)
				return;
			var selectedValue:*;
			if ("id" in menu.selectedItem) selectedValue = menu.selectedItem.id;
			else selectedValue = menu.selectedItem;
			Dock.instance[property] = selectedValue;
		}
		
		private function handleDotMouseDown(event:Event):void
		{
			var item:DotPagingItem = event.target as DotPagingItem;
			
			if (!item) return;
			
			Dock.instance.pageIndex = item.index;
		}
	}
}