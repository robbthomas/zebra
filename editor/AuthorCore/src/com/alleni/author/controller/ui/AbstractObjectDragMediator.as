package com.alleni.author.controller.ui
{
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.taconite.controller.DragMediator;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.TaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class AbstractObjectDragMediator extends DragMediator
	{
		protected var _view:TaconiteView;
		protected var _oldPositions:Array;
		
		public function AbstractObjectDragMediator(context:ViewContext, stopPropagation:Boolean=true)
		{
			super(context, stopPropagation);
		}
		
		public function handleViewEvents(view:ITaconiteView, role:uint):AbstractObjectDragMediator
		{
			// Overridden by subclasses
			return this;
		}
		
		protected function get object():AbstractObject
		{
			return _view.model.value as AbstractObject;
		}
		
		public function updateViewPosition():void
		{
			// Overridden by subclasses
		}
		
		public function clearViewEvents():void
		{
			// Overridden by subclasses
		}
	}
}