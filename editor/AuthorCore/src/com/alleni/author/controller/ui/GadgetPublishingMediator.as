package com.alleni.author.controller.ui
{
	import assets.icons.publishing.gadgetIconWhite;
	import assets.icons.publishing.zappIconWhite;
	
	import caurina.transitions.Tweener;

	import com.alleni.author.application.dialogs.PublishingDialog;
	import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.definition.DoubleClickAction;
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.definition.application.DockIcons;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.definition.text.FormattedText;
    import com.alleni.author.event.GadgetEvent;
	import com.alleni.author.model.gadgets.GadgetIconReference;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.model.ui.Dock;
	import com.alleni.author.service.ThumbnailLoadingPipeline;
	import com.alleni.author.service.gadgets.GadgetIconOperation;
    import com.alleni.author.service.project.ProjectLoadOperation;
    import com.alleni.author.service.project.ProjectSaveOperation;
    import com.alleni.author.util.StringUtils;
	import com.alleni.author.view.objects.CompositeView;
	import com.alleni.author.view.text.LightEditableLabel;
	import com.alleni.author.view.text.LightLabel;
	import com.alleni.author.view.text.RichLabel;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.components.CheckBox;
	import com.alleni.author.view.ui.components.ComboBox;
	import com.alleni.author.view.ui.components.PushButton;
	import com.alleni.author.view.ui.components.RadioButton;
	import com.alleni.author.view.ui.components.SearchComboBox;
	import com.alleni.author.view.ui.components.Style;
	import com.alleni.author.view.ui.controls.Alert;
	import com.alleni.author.view.ui.controls.LoadingSpinner;
	import com.alleni.author.view.ui.controls.RatingContainer;
	import com.alleni.author.view.ui.controls.ScreenshotMiniThumbnailContainer;
	import com.alleni.author.view.ui.palettes.DockDetailsView;
	import com.alleni.author.view.ui.palettes.PopoverView;
	import com.alleni.taconite.dev.BitmapUtils;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.event.OperationEvent;
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.service.IOperation;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.Bitmap;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Rectangle;
	import flash.globalization.CurrencyFormatter;
	import flash.utils.ByteArray;
	
	import mx.collections.IList;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;
	import mx.events.ValidationResultEvent;
	
	import spark.components.Button;
	import spark.validators.CurrencyValidator;

	public class GadgetPublishingMediator extends EventDispatcher
	{
		protected static const PUBLISHING_TARGET_PROPERTY:String = "publishingCandidate";
		
		protected static const UNTITLED:String = "Untitled";
		protected static const PUBLISHING_BUTTON_LABEL:String = "Publish";
		protected static const SELECTING_BUTTON_LABEL:String = "Buy";
		protected static const FREE_PRICE_LABEL:String = "FREE";
		protected static const OWNED_LABEL:String =  "- In Collection -";
		
		protected var _targetProperty:String;
		
		protected var _headerLabel:RichLabel;
		
		protected var _nameBox:SearchComboBox;
		protected var _nameText:String;
		
		protected var _descriptionField:LightEditableLabel;
		protected var _descriptionText:String;
		
		protected var _categoryMenu:ComboBox;
		protected var _categoryId:int;
		
		private var _priceField:RichTextField;
		private var _priceValue:uint;
		
		protected var _authorLabel:LightLabel;
        protected var _validName:Boolean = true;
		protected var _infoIconContainer:DisplayObjectContainer;
		protected var _ratingContainer:RatingContainer;
		protected var _iconContainer:DisplayObjectContainer;
        protected var _screenshotThumbnails:Sprite;
		protected var _iconProgress:DisplayObject;
		protected var _buttonProgress:DisplayObject;
		protected var _button:InteractiveObject;
		protected var _cancelButton:InteractiveObject;
		protected var _detailButton:InteractiveObject;
		protected var _pendingIconLoader:Loader;
		protected var _pendingScreenshotLoader:Loader;
		protected var _pendingFindOperation:IOperation;
		protected var _container:DisplayObjectContainer;
		
		protected var _licenseContainer:DisplayObjectContainer;
		protected var _embedContainer:DisplayObjectContainer;
		protected var _embedCheckBox:CheckBox;
		protected var _republishContainer:DisplayObjectContainer;
		protected var _republishCheckBox:CheckBox;
		protected var _shoppCheckBox:CheckBox;
        protected var _editableCheckBox:CheckBox;
		protected var _freeButton:RadioButton;
		protected var _priceButton:RadioButton;
		protected var _priceContainer:DisplayObject;
		
		protected var _textMediator:TextEditMediator;
		
		public function GadgetPublishingMediator(targetProperty:String, container:DisplayObjectContainer)
		{
			_targetProperty = targetProperty;
			_container = container;
		}
		
		/**
		 * monster method and class. easy candidate for refactor once basic functionality is validated. since functionality has been changing,
		 * we have not yet had the opportunity.
		 * 
		 */
		public function handlePublishingEvents(headerLabel:RichLabel, infoIconContainer:DisplayObjectContainer, nameBox:SearchComboBox, descriptionField:LightEditableLabel, authorLabel:LightLabel, priceField:RichTextField,
												ratingContainer:RatingContainer, iconContainer:DisplayObjectContainer, screenshotThumbnails:Sprite, button:InteractiveObject, categoryMenu:ComboBox,
												detailButton:InteractiveObject, cancelButton:InteractiveObject):GadgetPublishingMediator
		{
			_headerLabel = headerLabel;
			_infoIconContainer = infoIconContainer;
            _screenshotThumbnails = screenshotThumbnails;
            _container.addEventListener(Event.CLOSE, handleContainerClose);
			setNameBox(nameBox);
			setDescriptionField(descriptionField);
			_authorLabel = authorLabel;
			this.priceField = priceField;
			_ratingContainer = ratingContainer;
			_iconContainer = iconContainer;
			_detailButton = detailButton;
			_cancelButton = cancelButton;
			this.button = button;
			_categoryMenu = categoryMenu;
			_categoryMenu.addEventListener(Event.SELECT, handleCategorySelect);
			
			_iconContainer.addEventListener(MouseEvent.CLICK, handleIconClick);
			_iconContainer.addEventListener(MouseEvent.MOUSE_OVER, handleIconOver);
			_iconContainer.addEventListener(MouseEvent.MOUSE_OUT, handleIconOut);
			
			updateValues(true);
			
			GadgetController.instance.model.gadgets.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleCollectionChange);
			Dock.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
			Dock.instance[_targetProperty].addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
			
			_detailButton.addEventListener(MouseEvent.CLICK, handleDetailClick);
			cancelButton.addEventListener(MouseEvent.CLICK, handleCancelClick);

            ApplicationController.addEventListener(NotificationNamesApplication.SCREENSHOT_PANEL_CLOSED, screenshotPublishDialogShow);
            ApplicationController.addEventListener(NotificationNamesApplication.SCREENSHOT_PANEL_SHOWN, screenshotPublishDialogHide);

            ApplicationController.addEventListener(NotificationNamesApplication.OPEN_PUBLISHING_DIALOG, handleCancelClick);
			
			return this;
		}
		
		protected function handleDetailClick(event:Event):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;

			if (!gadget || !gadget.published)
				return;
            var owned:Boolean = gadget.permissions.owned;
			TaconiteFactory.getEnvironmentImplementation().requestDetailsPanel(owned?gadget.apnUUID:gadget.projectId, owned);
		}
		
		protected function handleCancelClick(event:Event):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			if (gadget) {
				gadget.metadataUpToDate = true; // changes in metadata are to be reverted to saved state
				gadget.pendingIconOperation = null;
				gadget.pendingScreenshotOperation = null;
			}
            Application.instance.publishingInProgress = false;
			if (_textMediator)
				_textMediator.cancelEditing();
			_textMediator = null;
			Dock.instance.dispatchEvent(new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE, false, false, null, _targetProperty, Dock.instance[_targetProperty], null));
		}
		
		public function handleLicenseControls(licenseContainer:DisplayObjectContainer, 
											  embedContainer:DisplayObjectContainer, embedCheckBox:CheckBox, republishContainer:DisplayObjectContainer, republishCheckBox:CheckBox,
											  shoppCheckBox:CheckBox, freeButton:RadioButton, priceButton:RadioButton, priceContainer:DisplayObject, editable:CheckBox):void
		{
			_licenseContainer = licenseContainer;
			_embedContainer = embedContainer;
			_embedCheckBox = embedCheckBox;
			_republishContainer = republishContainer;
			_republishCheckBox = republishCheckBox;
			_shoppCheckBox = shoppCheckBox;
			_freeButton = freeButton;
			_priceButton = priceButton;
			_priceContainer = priceContainer;
            _editableCheckBox = editable;
			
			_embedCheckBox.addEventListener(MouseEvent.CLICK, checkFollowingEditEvent);
			_republishCheckBox.addEventListener(MouseEvent.CLICK, checkFollowingEditEvent);
			_shoppCheckBox.addEventListener(MouseEvent.CLICK, checkFollowingEditEvent);
            _editableCheckBox.addEventListener(MouseEvent.CLICK, checkFollowingEditEvent);
			
			_freeButton.addEventListener(MouseEvent.CLICK, handlePriceStateChange);
			_priceButton.addEventListener(MouseEvent.CLICK, handlePriceStateChange);
			
			updateLicenseUI(true);
		}
		
		protected function updateLicenseUI(reselect:Boolean=false):void
		{
			if (!_licenseContainer) return;
			
			while (_licenseContainer.numChildren > 0)
				_licenseContainer.removeChildAt(0);
			_republishCheckBox.enabled = false;
			_embedCheckBox.enabled = false;

			var gadget:Project = Dock.instance[_targetProperty] as Project;
			if (!gadget) return;
			
			// discern which controls should be visible depending on item type and publish priveleges and availability
			const canPublish:Boolean = GadgetController.instance.confirmCanPublish(gadget);
			
			// set up special cases for whether or not the item is free
			const isFree:Boolean = _priceValue == 0;
			const IS_ZAPP:Boolean = getIsZappFor(gadget);
			const EMBED_SUPPORTED:Boolean = getEmbedSupportedFor(gadget);
			const REPUBLISH_SUPPORTED:Boolean = getRepublishSupportedFor(gadget);
			
			trace("isfree: " + isFree);
			trace("IS_ZAPP: " + IS_ZAPP);
			trace("EMBED_SUPPORTED: " + EMBED_SUPPORTED);
			trace("REPUBLISH_SUPPORTED: " + REPUBLISH_SUPPORTED);
			
			if (canPublish) {
				_embedCheckBox.labelText = IS_ZAPP?"Can embed this App in Web pages":"Can embed this Gadget in Web pages";
				_republishCheckBox.labelText = IS_ZAPP?"Can republish within new Apps":"Can republish within new gadgets";
				_shoppCheckBox.labelText = IS_ZAPP?"Also publish to App Shopp":"Also publish to Gadget Shopp";
                _editableCheckBox.labelText = IS_ZAPP?"":"Allow others to edit object";
			} else {
				_embedCheckBox.labelText = IS_ZAPP?"Can embed this App in Web pages":"Can embed this Gadget in Web pages";
				_republishCheckBox.labelText = IS_ZAPP?"Can republish within new Apps":"Can republish within new gadgets";
			}
			
			_freeButton.selected = isFree;
			_priceButton.selected = !isFree;
			
			_embedCheckBox.enabled = EMBED_SUPPORTED && isFree;
			//_shoppCheckBox.enabled = !(IS_ZAPP && isFree); // reserved for upper-tier accounts
			_shoppCheckBox.enabled = !IS_ZAPP; // bottom tier account (currently sole tier) must allow Zapps in shopp PJK 1AUG11
			if (IS_ZAPP) {
				//_shoppCheckBox.selected = isFree; // reserved for upper-tier accounts
				_shoppCheckBox.selected = true; // bottom tier account (currently sole tier) must allow Zapps in shopp PJK 1AUG11
				if (!isFree)
					_embedCheckBox.selected = false;
			}
			// end free special cases
			
			// SHOPP & EDITABLE GADGET
			if (canPublish) {
                if(!IS_ZAPP){
                    _licenseContainer.addChild(_editableCheckBox);
                    if (reselect){
                        if(gadget.published == false){
                            _editableCheckBox.selected = false;
                        } else {
                            _editableCheckBox.selected = gadget.license.edit;
                        }
                    }
                }
				_licenseContainer.addChild(_shoppCheckBox);
				if (reselect)
					_shoppCheckBox.selected = !gadget.hideInStoreList;
				// PRICE
				_licenseContainer.addChild(_priceContainer);
			}
			
			// REPUBLISH
			if (REPUBLISH_SUPPORTED) {
				const republishIsAllowed:Boolean = gadget.license.republish;
				
				if (canPublish || republishIsAllowed) {
					_licenseContainer.addChild(_republishContainer);
				}

                if (reselect)
                    _republishCheckBox.selected = republishIsAllowed;

                _republishCheckBox.enabled = _shoppCheckBox.selected;
                _republishCheckBox.alpha = _shoppCheckBox.selected?1:0.5;
			}
			
			// EMBED
			if (EMBED_SUPPORTED) {
				const embedIsAllowed:Boolean = gadget.license.embed;
				
				if (canPublish && isFree || !canPublish && embedIsAllowed) {
					//_embedCheckBox.enabled = canPublish && isFree;
					_embedCheckBox.enabled = isFree;
					_licenseContainer.addChild(_embedContainer);
					if (reselect)
						_embedCheckBox.selected = embedIsAllowed;
				}
			}
		}
		
		private function handlePriceStateChange(e:Event):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			if (!gadget) return;
			
			if (_freeButton.selected)
				_priceValue = 0;
			else
	 			_priceValue = (gadget.price > 0)?gadget.price:gadget.minPrice;
			checkFollowingEditEvent();
			if (gadget.metadataUpToDate) updateLicenseUI();
		}
		
		private function refreshPriceField(minPrice:int=-1):void
		{
			_priceField.rawText = GadgetDescription.prettyPrice(_priceValue, minPrice);
		}
		
		protected function handleCollectionChange(e:CollectionEvent):void
		{
			switch (e.kind) {
				case CollectionEventKind.ADD:
					var gadget:Project = Dock.instance[_targetProperty] as Project;
					if (!gadget) return;
					// check to see if a gadget added to the stage is our target. if it is, make sure any pending changes save and propagate.
					if (gadget.id == (IList(e.target).getItemAt(e.location) as Project).id)
						cleanUpAndSaveMetadataIfNecessary(gadget);
					break;
				default:
					// no need to do anything
					break;
			}
		}
		
		protected function handleIconClick(e:Event):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			
			if (!gadget || gadget && !GadgetController.instance.confirmCanPublish(gadget))
				return;
			GadgetIconMediator.instance.showImportDialog(gadget);
		}
		
		private var _iconOverlay:Sprite;
		protected function handleIconOver(e:Event):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			
			if (!gadget || gadget && !GadgetController.instance.confirmCanPublish(gadget)
				|| !gadget.icon && !gadget.pendingIconOperation)
				return;
			
			if (applyIconOverlayWithMessage("Change Icon")) {
				_iconOverlay.alpha = 0;
				Tweener.addTween(_iconOverlay, {alpha:1, time:0.15, transition:"easeOutQuart"});
			}
		}
		
		protected function applyIconOverlayWithMessage(message:String):Boolean
		{
			clearIconOverlay();
			_iconOverlay = new Sprite();
			_iconOverlay.mouseEnabled = false;
			_iconOverlay.mouseChildren = false;
			
			const darkener:DisplayObject = BitmapUtils.cacheMe(_iconContainer);
			darkener.blendMode = BlendMode.SUBTRACT;
			darkener.alpha = 0.5;
			_iconOverlay.addChild(darkener);
			_iconContainer.addChild(_iconOverlay);
			
			const label:RichLabel = new RichLabel(75);
			label.multiline = true;
			label.height = 40;
			label.color = 0xffffff;
			label.center = true;
			label.italic = true;
			label.size = 12;
			label.text = message;
			label.filters = [new DropShadowFilter(1, 80, 0, 0.9, 2, 2, 1.0, 3)];
			_iconOverlay.addChild(label);
			return _iconOverlay != null;
		}
		
		protected function clearIconOverlay():void
		{
			if (_iconOverlay && _iconOverlay.parent)
				_iconOverlay.parent.removeChild(_iconOverlay);
			_iconOverlay = null;
		}
		
		protected function handleIconOut(e:Event):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			if (gadget && !gadget.icon && !gadget.pendingIconOperation)
				return;
			
			Tweener.addTween(_iconOverlay, {alpha:0, time:0.15, transition:"easeOutQuart",
				onComplete:clearIconOverlay});
		}
		
		public function handleScreenshotClick(e:Event):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			
			if (!gadget || gadget && !GadgetController.instance.confirmCanPublish(gadget))
				return;
			
			const IS_ZAPP:Boolean = getIsZappFor(Dock.instance[_targetProperty]);
			
            if(_container as DockDetailsView){
                GadgetScreenshotMediator.instance.initializeScreenshotPanelFor(gadget, ((_container as PopoverView).target) as CompositeView, IS_ZAPP);
            }else{
                GadgetScreenshotMediator.instance.initializeScreenshotPanelFor(gadget, null, IS_ZAPP);
            }
		}
		
		public function get PUBLISHING():Boolean
		{
			return _targetProperty == PUBLISHING_TARGET_PROPERTY;
		}
		
		public static function getIsZappFor(gadget:Project):Boolean
		{
			if (!gadget || !Application.instance.document || !Application.instance.document.project)
				return false;
			return gadget === Application.instance.document.project;
		}
		
		public function get targetProperty():String
		{
			return _targetProperty;
		}
		
		protected static function getEmbedSupportedFor(gadget:Project):Boolean
		{
			return getIsZappFor(gadget);
		}

		protected static function getRepublishSupportedFor(gadget:Project):Boolean
		{
			if (!gadget) return false;
			return !getIsZappFor(gadget);
		}
		
		protected function handleContainerClose(event:Event):void
		{
			cleanUpAndSaveMetadataIfNecessary(Dock.instance[_targetProperty] as Project);
			Dock.instance[_targetProperty] = null;
		}
		
		private function cleanUpAndSaveMetadataIfNecessary(gadget:Project):void
		{
			if (!gadget) return;
			if (!GadgetController.instance.confirmCanPublish(gadget)) return;
			
			if (!gadget.published && gadget.pendingIconOperation) gadget.pendingIconOperation = null; // hasn't been published. give up on the icon upload.
			if (!gadget.published && gadget.pendingScreenshotOperation) gadget.pendingScreenshotOperation = null; // hasn't been published. give up on the icon upload.
			
			if (!gadget.published) return; // no need to save metadata as this hasn't been published yet.

			if (!gadget.metadataUpToDate || gadget.pendingIconOperation || gadget.pendingScreenshotOperation) {
				confirmThenSave(gadget, true); // only save metadata here. Don't accidentally publish content because they've not clicked the button!
			}
		}
		
		protected function handleCategorySelect(event:Event):void
		{
			var menu:ComboBox = (event.target as ComboBox);
			if (!menu || !menu.selectedItem)
				return;
			if ("id" in menu.selectedItem)
				if (setValidatedCategory(menu.selectedItem.id))
					checkFollowingEditEvent(event);
		}
		
		private function setValidatedCategory(id:int):Boolean
		{
			for each (var item:Object in Dock.instance.categories) {
				if ("id" in item && item.id == id) {
					_categoryId = id;
					return true;
				}
			}
			return false;
		}
		
		protected function handlePropertyChange(event:PropertyChangeEvent):void
		{
			var publishingDialog:PublishingDialog;
			var gadget:Project;
			
			switch(event.property) {
                //TODO GADGET_PROJECT re-evaluate properties
				case _targetProperty:
					if (event.oldValue != null) event.oldValue.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
					
					const reselect:Boolean = event.oldValue != event.newValue;
					
					if (reselect)
						cleanUpAndSaveMetadataIfNecessary(event.oldValue as Project);
					if (event.newValue != null) {
						gadget = Dock.instance[_targetProperty] as Project;
						if (!gadget) break;
						gadget.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
						
						if (event.newValue === event.oldValue) break;
						updateValues(reselect);
					}
					break;
				case "gadgetID":
					if (event.oldValue == null)
						updateValues(true);
					break;
				case "publishInProgress":
					const inProgress:Boolean = event.newValue != null;
						
					publishingDialog = _container as PublishingDialog;
					if (publishingDialog && publishingDialog.stage) {
						publishingDialog.showProgress = inProgress;
						publishingDialog.okButton.enabled = !inProgress;
					} else {
						var pushButton:PushButton = _button as PushButton;
						showProgress = inProgress;
						if (pushButton) pushButton.enabled = !inProgress;
					}
					break;
				case "lastSuccessfullyPublished":
					if (event.newValue == null || !PUBLISHING) return; // if we have not come upon a completed or successful publish, let's move on.
					
					gadget = Dock.instance[_targetProperty] as Project;
					if (!gadget) break;
					
					if (event.newValue === gadget) {// our target has just been published
						publishingDialog = _container as PublishingDialog;
						if (publishingDialog)
							ApplicationController.instance.removePopup(publishingDialog);
						else
							_container.visible = false;
					}
					break;
				case "pendingIconOperation":
					if (event.newValue == null) break;
					
					var oldOperation:IOperation = event.oldValue as IOperation;
					if (oldOperation)
						oldOperation.removeEventListener(OperationEvent.UPDATE, refreshIcon);
					
					gadget = Dock.instance[_targetProperty] as Project;
					if (!gadget) break;
					
					gadget.pendingIconOperation.addEventListener(OperationEvent.UPDATE, refreshIcon, false, 0, true);
					break;
				default:
					break;
			}
		}
		
		protected function set showProgress(value:Boolean):void
		{
			/*if (_buttonProgress && _container.contains(_buttonProgress))
				_container.removeChild(_buttonProgress);
			
			if (value) {
				if (!_buttonProgress) {
					_buttonProgress = new LoadingSpinner(50, true);
					_buttonProgress.alpha = 0.5;
					_buttonProgress.x = (_container.width - 50) / 2 - 15;
					_buttonProgress.y = (_container.height - 50) / 2 - 15;
				}
				_container.addChild(_buttonProgress);
			} else {
				_buttonProgress = null;
			}*/
		}
		
		/**
		 * 
		 * @param reselect whether or not we're onto a different item
		 * 
		 */
		protected function updateValues(reselect:Boolean=false, fromFindOperation:Boolean=false):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			
			if (!gadget)
				return;
			
			if (reselect) {
				gadget.metadataUpToDate = true;
				showProgress = false;
				if (GadgetController.instance.confirmCanPublish(gadget)) {
					// Ultimately, minprice will be maintained by business rules, so the final safeguard will be the back-end logic 
					//  which will respond with cmd error if this is still too low.
                    if (gadget.minPrice < 25) gadget.minPrice = 25;
				}
				if (!gadget.published && !_pendingFindOperation) {
                    _nameText = GadgetDescription.cleanName(gadget, true);
					_descriptionText = gadget.description != null?gadget.description:"No description found";
					_priceValue = gadget.price;
					_categoryId = gadget.categoryId;
				}
			}
			
			if (reselect && !_pendingFindOperation || fromFindOperation) {
				_nameText = GadgetDescription.cleanName(gadget, true);
				_descriptionText = gadget.description != null?gadget.description:"No description found";
				_priceValue = gadget.price;
				_categoryId = gadget.categoryId;
			}
			
			if (!_pendingFindOperation || fromFindOperation) {
				refreshIcon(null, reselect);
            	refreshScreenshot(reselect);
			}
			updateLicenseUI(reselect);

			_ratingContainer.index = gadget.averageRating;
			_ratingContainer.count = gadget.numRatings;
			
			const canPublish:Boolean = GadgetController.instance.confirmCanPublish(gadget);
			const IS_ZAPP:Boolean = getIsZappFor(Dock.instance[_targetProperty]);
			
			if (PUBLISHING && canPublish)
				_headerLabel.text = IS_ZAPP?"Zap Project":"Publish to My Gadgets";
			else
				_headerLabel.text = IS_ZAPP?"App Information":"Gadget Information";
			
			while (_infoIconContainer.numChildren > 0)
				_infoIconContainer.removeChildAt(0);
			
			var infoIcon:DisplayObject = IS_ZAPP?new zappIconWhite():new gadgetIconWhite();
			_infoIconContainer.addChild(infoIcon);
			
			var textIsDefault:Boolean = (!_nameText || _nameText == GadgetDescription.DEFAULT_NAME || _nameText == "" || _nameText == UNTITLED);
			const newName:String = textIsDefault ? UNTITLED : _nameText;
			if (!_nameBox.selectedItem)
				_nameBox.defaultLabel = newName;
			else 
			_nameBox.textField.rawText = newName;
			_nameBox.alpha = textIsDefault ? 0.5 : 1;
			_nameBox.textField.selectAll();
			_nameBox.textField.italic = textIsDefault ? true : false;
			_descriptionField.text = _descriptionText;
			refreshPriceField(gadget.minPrice);
			
			var i:int;
			var length:int = _categoryMenu.items.length;
			var set:Boolean = false;
			for (i = 0; i < length; i++) {
				if ("id" in _categoryMenu.items[i])
					if (_categoryMenu.items[i].id == _categoryId) {
						_categoryMenu.selectedIndex = i;
						set = true;
					}
			}
			
			if (!set)
				_categoryMenu.selectedIndex = 0;
			
			if (!_button) return;
			
			_nameBox.textField.editable = canPublish;
            _nameBox.textField.addEventListener(KeyboardEvent.KEY_UP, checkPublishedName);
			_descriptionField.editable = canPublish;
            _priceField.editable = canPublish;
			_categoryMenu.enabled = canPublish;
			
			const pushButton:PushButton = _button as PushButton;
			const detailButton:PushButton = _detailButton as PushButton;
			const cancelButton:PushButton = _cancelButton as PushButton;
			const owned:Boolean = GadgetController.instance.confirmOwnership(gadget);
			detailButton.enabled = (gadget.published);
			
			_authorLabel.text = canPublish ? GadgetDescription.authorNameForMe() : GadgetDescription.authorNameForGadget(gadget);
			
			if (PUBLISHING) {
				if (_button as PushButton) {
					Style.setStyle(Style.LIGHT);
					cancelButton.enabled = canPublish || !gadget.projectId;
					cancelButton.visible = canPublish || !gadget.projectId;
					pushButton.label = (canPublish || !gadget.projectId) ? PUBLISHING_BUTTON_LABEL : OWNED_LABEL;
					pushButton.active = (canPublish || !gadget.projectId);
					pushButton.enabled = canPublish && !_pendingFindOperation;
					pushButton.visible = (canPublish || !gadget.projectId) || owned;
				} else if (_button as Button) {
					var sparkButton:Button = _button as Button;
					sparkButton.enabled = canPublish && !_pendingFindOperation;
				}
			} else {
				if (pushButton) {
					Style.setStyle(Style.LIGHT);
					pushButton.label = _priceValue > 0 ? _priceField.text : FREE_PRICE_LABEL;
					if (owned) pushButton.label = OWNED_LABEL;
					pushButton.active = !owned;
				}
			}
		}

        private function checkPublishedName(event:Event):void{
            if(StringUtils.isNameSafeForServer(_nameBox.textField.text).valid == false){
                _nameBox.textField.color = 0xFF0000;
                (_button as PushButton).enabled = false;
                _button.removeEventListener(MouseEvent.CLICK, handleButtonClick);
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:999, inserts:["project", StringUtils.isNameSafeForServer(_nameBox.textField.text).reason]}));
            }else{
                _nameBox.textField.color = 0x000000;
                (_button as PushButton).enabled = true;
                _button.addEventListener(MouseEvent.CLICK, handleButtonClick);
           }
        }

		protected function refreshIcon(event:Event=null, reselect:Boolean=false):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			
			if (!gadget)
				return;

            var operation:GadgetIconOperation = gadget.pendingIconOperation;
			if (operation && operation.data) {
				var data:ByteArray = operation.data[0];
				
				// set up icon preview image
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e:Event):void {
					clearIconOverlay();
					setIcon(BitmapUtils.makePreviewRoundedIcon(loader.content as Bitmap, 75, 75 * 0.2), true);
				});
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, function(e:Event):void {
					LogService.error("Error loading bytes for preview icon data");
				});
				loader.loadBytes(data);
				return;
			}

            if(gadget.localIcon){
                setIcon(gadget.localIcon)
                return;
            }

			var iconURL:String;
			if (gadget.icon && gadget.icon.mediumURL){
				iconURL = gadget.icon.mediumURL;
            }else{
				iconURL = GadgetIconReference.DEFAULT_BASE_URL + GadgetIconReference.MEDIUM_SIZE + GadgetIconReference.DEFAULT_EXTENSION;
            }
			
			if (reselect) {
				Tweener.addTween(_iconContainer, {alpha:0, time:0.15, transition:"easeOutQuart",
					onComplete:function():void {
						_iconContainer.visible = false;
					}});
				if (_pendingIconLoader) {
					_pendingIconLoader.close();
					_pendingIconLoader = null;
				}
			}
			
			_pendingIconLoader = ThumbnailLoadingPipeline.push(iconURL, function(event:Event):void {
				var loaderInfo:LoaderInfo = LoaderInfo(event.target);
				(loaderInfo.loader.content as Bitmap).smoothing = true;
				setIcon(loaderInfo.loader.content);
				_pendingIconLoader = null;
			});
		}
		
		protected function refreshIconProgress():void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			
			if (!gadget)
				return;
	
			if (_iconProgress && _iconContainer.contains(_iconProgress))
				_iconContainer.removeChild(_iconProgress);
		
			if (gadget.pendingIconOperation && !(gadget.pendingIconOperation as GadgetIconOperation).data) {
				if (!_iconProgress) {
					const shadow:DropShadowFilter = new DropShadowFilter(1, 80, 0, 0.9, 2, 2, 1.0, 3);
					_iconProgress = new LoadingSpinner(40, true);
					_iconProgress.alpha = 0.5;
					_iconProgress.filters = [shadow];
					_iconProgress.x = (_iconContainer.width - _iconProgress.width)/2-3;
					_iconProgress.y = (_iconContainer.height - _iconProgress.height)/2-3;
				}
				_iconContainer.addChild(_iconProgress);
			} else {
				_iconProgress = null;
			}
		}
		
		protected function setIcon(value:DisplayObject, createLocalCache:Boolean = false):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			
			if (!gadget)
				return;
			
			const SIZE:Number = DockIcons.SIZE_MEDIUM;
			
			while (_iconContainer.numChildren > 0)
				_iconContainer.removeChildAt(0);
			
			if (value.width > SIZE || value.height > SIZE)
				value = BitmapUtils.bilinearDownscale(value as Bitmap, SIZE);

            if(createLocalCache){
                gadget.localIcon = value;
            }

			_iconContainer.addChild(value);
			refreshIconProgress();
			
			if (!gadget.icon && !gadget.pendingIconOperation)
				applyIconOverlayWithMessage("Click here to\nset icon");
			
			_iconContainer.visible = true;
			Tweener.addTween(_iconContainer, {alpha:1, time:0.3, transition:"easeInOutQuart"});
		}
		
		protected function refreshScreenshot(reselect:Boolean=false):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			
			if (!gadget || gadget.pendingScreenshotOperation) // when there is a pending operation, don't clear out the container[s]
                return;
			while (_screenshotThumbnails.numChildren > 0)
                _screenshotThumbnails.removeChildAt(0);
			
			const IS_ZAPP:Boolean = getIsZappFor(Dock.instance[_targetProperty]);
			
            var screenshotContainer:ScreenshotMiniThumbnailContainer = new ScreenshotMiniThumbnailContainer(_screenshotThumbnails, 0, 0, 5, gadget, null, PUBLISHING, IS_ZAPP);
            screenshotContainer.addEventListener(ScreenshotMiniThumbnailContainer.OPEN_SCREENSHOT_PANEL, handleScreenshotClick);
            _screenshotThumbnails.addChild(screenshotContainer);
        }

		protected function checkFollowingEditEvent(event:Event=null):void
		{
			if (_pendingFindOperation) return;

			var gadget:Project = Dock.instance[_targetProperty] as Project;
			if (!gadget) return;
			
			if (event && event.type == Event.SELECT) { // may be just the ComboBoxes adjusting
				if (!isNaN(_categoryId) && _categoryId != gadget.categoryId)
					gadget.metadataUpToDate = false;
				return;
			}

			const EMBED_SUPPORTED:Boolean = getEmbedSupportedFor(Dock.instance[_targetProperty]);
			const REPUBLISH_SUPPORTED:Boolean = getRepublishSupportedFor(Dock.instance[_targetProperty]);
			
			// check price vs. minimum
			if (_priceValue > 0 && _priceValue < gadget.minPrice) _priceValue = gadget.minPrice;
			
			if ((_nameText && _nameText != gadget.publishedName)
				|| (_descriptionText && _descriptionText != gadget.description)
				|| (!isNaN(_priceValue) && _priceValue != gadget.price)
				|| gadget.hideInStoreList != !_shoppCheckBox.selected
				|| (REPUBLISH_SUPPORTED && gadget.license.republish != _republishCheckBox.selected)
				|| (EMBED_SUPPORTED && gadget.license.embed != _embedCheckBox.selected)
			)
				gadget.metadataUpToDate = false;
			else
				gadget.metadataUpToDate = true;
			
			if (!gadget.metadataUpToDate)
				updateValues();

            _republishCheckBox.enabled = _shoppCheckBox.selected;
            _freeButton.enabled = _shoppCheckBox.selected;
		    _priceButton.enabled = _shoppCheckBox.selected;

			var sprite:Sprite = _priceContainer as Sprite;
			if (sprite) {
				var i:int;
				var length:int = sprite.numChildren;
	            for(i = 0; i < length; i++) {
					var label:LightLabel = sprite.getChildAt(i) as LightLabel;
	                if(label) label.alpha = _shoppCheckBox.selected?1:0.5;
	            }
			}
            _embedCheckBox.alpha = _shoppCheckBox.selected?1:0.5;
            _republishCheckBox.alpha = _shoppCheckBox.selected?1:0.5;
           	_priceField.alpha = _shoppCheckBox.selected?1:0.5;
		}
		
		// todo... clean these up using a centralized single/multi-line form field class.
		protected function setNameBox(value:SearchComboBox, maxChars:int = 30):void
		{
			if (!value) return;
			
			var mediator:TextEditMediator = new TextEditMediator();
			mediator.maxChars = maxChars;
			var okEditFunc:Function = 
				function(e:MouseEvent):int{
					_nameBox.textField.italic = false;
					_textMediator = mediator;
					return DoubleClickAction.EDIT;
				};
			var keyPressFunc:Function = 
				function(charCode:uint):uint{
					var tempText:String = FormattedText.convertToString(value.textField.text as String);
					if (charCode != 8) tempText +=  String.fromCharCode(charCode);
					else tempText = tempText.substr(0, tempText.length-1);
					
					if (!(Dock.instance[_targetProperty] as Project) || tempText.length > 0 || !Dock.instance.userItemList) {
						var gadget:Project = Dock.instance[_targetProperty] as Project;
						var tempItems:Array = new Array();
						
						var item:Project;
						for each (item in Dock.instance.userItemList) {
							if (item.projectTypeId == gadget.projectTypeId 
									&& GadgetController.instance.confirmCanPublish(item)
									&& new RegExp("^" + tempText).test(item.publishedName)) {
								tempItems.push({label:item.publishedName, id:item.projectId});
							}
						}
						
						if (tempItems.length > 0) {
							value.addEventListener(Event.SELECT, handleExistingItemSelect);
							value.items = tempItems;
							if (!value.listShowing)
								value.showOptions = true;
						} else {
							if (value.listShowing)
								value.showOptions = false;
						}
					} else {
						if (value.listShowing)
							value.showOptions = false;
					}
					return TextEditMediator.closeOnEnter(charCode);
				};
			var closeFunc:Function = 
				function(val:String):void{
					var tempText:String = FormattedText.convertToString(val as String);

					StringUtils.stripWhitespace(tempText);
					
					_textMediator = null;
					_nameText = tempText;
					checkFollowingEditEvent();
				};
			var hittestFunc:Function = 
				function(x:Number, y:Number):Boolean{
					if(value.hitTestPoint(x,y)) return true;
					else return false;
				};
			
			mediator.handleTextEditEvents(value.textField, okEditFunc, null, keyPressFunc, closeFunc, hittestFunc, false);
			_nameBox = value;
		}
		
		private function handleExistingItemSelect(event:Event):void
		{
			event.target.removeEventListener(Event.SELECT, handleExistingItemSelect);
			
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			if (!gadget) return;
			
			var menu:ComboBox = (event.target as ComboBox);
			if (!menu || !menu.selectedItem)
				return;
			if ("id" in menu.selectedItem) {
				var loadOperation:ProjectLoadOperation = new ProjectLoadOperation(menu.selectedItem.id, false, null, false /*TODO needs to be apn for true*/);
				loadOperation.addEventListener(Event.COMPLETE, function(event:Event):void {
					var selectedGadget:Project = loadOperation.project;
					_nameText = selectedGadget.publishedName;
					_descriptionText = selectedGadget.description;
					_categoryId = selectedGadget.categoryId;
					_priceValue = selectedGadget.price;
					gadget.publishedName = selectedGadget.publishedName;

					//TODO GADGET_PROJECT gadget.publishedId = selectedGadget.publishedId;
					gadget.icon = selectedGadget.icon;
					gadget.screenshots = selectedGadget.screenshots;
					gadget.permissions = selectedGadget.permissions;
                    if(_shoppCheckBox != null){
					    _shoppCheckBox.selected = selectedGadget.hideInStoreList;
                    }
					checkFollowingEditEvent();
				});
				loadOperation.execute();
			}
		}
		
		protected function setDescriptionField(value:LightEditableLabel, maxChars:int = 300):void
		{
			if (!value) return;
			
			value.mediator.maxChars = maxChars;
			
			value.mediator.setCallBacks({
				okEditFunc:
				function(e:MouseEvent):int{
					_textMediator = ApplicationController.instance.authorController.getTextEditMediator();
					return DoubleClickAction.EDIT;
				},
				updateFunc:null,
				keyPressFunc:null,
				closeFunc:
				function(val:String):void{
					_textMediator = null;
					value.edit = false;
					_descriptionText = FormattedText.convertToString(val as String);
					checkFollowingEditEvent();
				},
				hittestFunc:null
			});
			_descriptionField = value;
		}
		
		private function set priceField(value:RichTextField):void
		{
			if (!value) return;
			var mediator:TextEditMediator = new TextEditMediator();
			
			mediator.maxChars = 17;
			
				var okEditFunc:Function = 
					function(e:MouseEvent):int{
					_textMediator = mediator;
					return DoubleClickAction.EDIT;
				};
				
				var keyPressFunc:Function =
					function(charCode:uint):uint{
					return TextEditMediator.closeOnEnter(charCode);
				};
				
				var closeFunc:Function = 
				function(val:String):void{
					// validate
					_textMediator = null;
					const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
					var formatter:CurrencyFormatter = new CurrencyFormatter(environment.locale);
					var validator:CurrencyValidator = new CurrencyValidator();
					
					var raw:String = FormattedText.convertToString(val);
					if ((raw.substr(0, 1) == '.')) raw = '0' + raw;

					if (raw) {
						var currencyExtraction:Array = raw.match(/[0-9]+(,[0-9]{3})*(\.[0-9]{2})?/);
						if (currencyExtraction && currencyExtraction.length > 0) {
							raw = currencyExtraction[0];
							
							if(raw.length > 5)
								raw = raw.substr(0,5);
							
							var withCurrencySymbol:String = formatter.format(Number(raw), true);
							
							if ((validator.validate(withCurrencySymbol) as ValidationResultEvent).type == ValidationResultEvent.VALID) {
								withCurrencySymbol = withCurrencySymbol.replace(/\./g, "");
								withCurrencySymbol = withCurrencySymbol.replace(/\,/g, "");
								withCurrencySymbol = withCurrencySymbol.replace(formatter.currencySymbol, "");
								
								_priceValue = uint(withCurrencySymbol);
							}
						}
					}
					checkFollowingEditEvent();
				};
				
				var hittestFunc:Function = 
				function(x:Number, y:Number):Boolean{
					if(value.hitTestPoint(x,y)){
						return true;
					}
					else{
						return false;
					}
				};
				
			mediator.handleTextEditEvents(value,okEditFunc,null,keyPressFunc,closeFunc,hittestFunc,false);
			_priceField = value;
		}
		
		protected function set button(value:InteractiveObject):void
		{
			if (!value) return;
			
			_button = value;
			_button.addEventListener(MouseEvent.CLICK, handleButtonClick);
		}
		
		protected function handleButtonClick(e:MouseEvent):void
		{
			var gadget:Project = Dock.instance[_targetProperty] as Project;
			if (!gadget) return;

			if (PUBLISHING) { // PUBLISH
				if (_textMediator) {
					// close out pending text edit before proceeding
					_textMediator.closeEditing(true);
					const maxLoops:int = 5;
					var loopIndex:int = 0;
					_container.addEventListener(Event.ENTER_FRAME, function(frameEvent:Event):void {
						if (loopIndex > maxLoops || _textMediator == null) {
							frameEvent.target.removeEventListener(Event.ENTER_FRAME, arguments.callee);
							if (_textMediator)
								LogService.error("GadgetPublishingMediator was unable to shut down pending text mediator " + _textMediator);
							else
								handleButtonClick(null);
						}
						loopIndex++;
					});
					return;
				}
				var metaOnly:Boolean = false;

				if (gadget.published && gadget.publishedName == _nameBox.textField.text){
					metaOnly = true;
                }
				// allow this to continue if all they want to do is update metadata using the publish button..
				
				confirmThenSave(gadget, metaOnly, gadget.publishedName != _nameBox.textField.text);
			} else { // BUY
				var validateOwnershipOperation:IOperation = CommerceController.instance.validateGadgetPurchase(gadget);
				if (validateOwnershipOperation) {
					validateOwnershipOperation.addEventListener(Event.COMPLETE, handleOwnershipConfirmed);
					validateOwnershipOperation.addEventListener(Event.CANCEL, handleOwnershipValidationCancelled);
				} else
					trace("not sure how we got here, but we already own this!");
			}
		}
		
		static private var _alreadyAgreedToReplace:Boolean = false;
		protected function confirmThenSave(gadget:Project, metaOnly:Boolean, nameChanged:Boolean = false):void
		{
			_alreadyAgreedToReplace = false;
            //TODO GADGET_PROJECT revise
            if (!nameChanged) {
				var message:String;
				if (getIsZappFor(gadget)) {
					if (metaOnly) {
						message = "You have changed this project's app information. Publishing will replace the published information. Do you want to continue?";
					} else {
						if (gadget.metadataUpToDate) {
							message = "You have changed this project. Publishing will replace the published app. Do you want to continue?";
						} else {
							message = "You have changed this project and its app information. Publishing will replace both the published app" +
								" and its information. Do you want to continue?";
						}
					}
				} else {
					if (metaOnly) {
						message = "You have changed this gadget's information. Publishing will replace the published information. Do you want to continue?";
					} else {
						if (gadget.metadataUpToDate) {
							message = "You have changed this gadget. Publishing will replace the published gadget. Do you want to continue?";
						} else {
							message = "You have changed this gadget and its information. Publishing will replace both the published gadget" +
								" and its information. Do you want to continue?";
						}
					}
				}
				Alert.yesLabel = "Replace";
				Alert.cancelLabel = "Cancel";
				//Alert.okLabel = "New " + (getIsZappFor(gadget)?"Zapp":"Gadget");
				Alert.show(message, "Are you sure?",
					Alert.THIRD | Alert.FIRST, handleConfirmUpdate(gadget, metaOnly));
			} else
				save(gadget, metaOnly);
		}
		
		internal function handleConfirmUpdate(gadget:Project, metaOnly:Boolean):Function
		{
			return function(event:CloseEvent):void {
				switch (event.detail) {
					case Alert.THIRD:
						_alreadyAgreedToReplace = true;
                        gadget.copyIconFromProjectId = gadget.projectId;
                        gadget.copyScreenshotsFromProjectId = gadget.projectId;
                        save(gadget, metaOnly);
                        break;
					case Alert.FIRST:
					default:
						break;
				}
			};
		}

        // Simple functions to hide and showing the publish dialog for screenshots w/o removing or adding from stage
//        TODO: Should be refactored after 1.0
        public function screenshotPublishDialogHide(event:Event):void
		{
			_container.scrollRect = new Rectangle();
        }

        public function screenshotPublishDialogShow(event:Event):void
		{
			_container.scrollRect = null;
        }

		protected function handleOwnershipConfirmed(event:Event):void
		{
			// ownership confirmed - possible todo: uncache the item and re-run the list.
		}
		
		private function handleOwnershipValidationCancelled(event:Event):void
		{
			// nothing to do, we have not committed to anything
		}
		
		protected function save(gadget:Project, metaOnly:Boolean, replaceExisting:Boolean=false):void
		{
			const IS_ZAPP:Boolean = getIsZappFor(gadget);
            const type:String = IS_ZAPP?"App":"Gadget";

			if (!gadget || gadget && metaOnly && gadget.metadataUpToDate
					&& !gadget.pendingIconOperation
					&& !gadget.pendingScreenshotOperation) {
                //MESSAGE NOT IN DOCUMENT

				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:660}));
				return;
			}

			if (!_nameText || _nameText == GadgetDescription.DEFAULT_NAME || _nameText == "" || _nameText == UNTITLED) {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:92}));
				return;
			}

			if (gadget.icon.fullURL.indexOf("defaultGadgetIcon") > -1) {
                if(gadget.localIcon == null){
				    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:661, inserts:[type]}));
                    return;
                }
			}

			if (Dock.instance.publishInProgress == gadget)
				return;
			
			var metadataWasUpToDate:Boolean = gadget.metadataUpToDate;
			gadget.metadataUpToDate = true;
            if(gadget.icon != null && gadget.icon.publishedImageId != null){
                gadget.copyIconFromProjectId = gadget.id;
            }
			// use a copy for publish operation. In this way, we make no assumptions as to success of the operation.
			// if it succeeds, we will commit these values to the real gadget.
			const gadgetCopyForPublishing:Project = gadget.clone();
			gadgetCopyForPublishing.copyFromPublished(gadget);
            gadgetCopyForPublishing.copyContentFromProjectId = gadget.id;
			gadgetCopyForPublishing.pendingIconOperation = gadget.pendingIconOperation;
			gadgetCopyForPublishing.pendingScreenshotOperation = gadget.pendingScreenshotOperation;
			gadgetCopyForPublishing.publishedName = _nameText;
			if(_descriptionText == "{Enter Description}"){
                _descriptionText = "";
            }
            if(gadget.copyIconFromProjectId != null){
                gadgetCopyForPublishing.copyIconFromProjectId = gadget.copyIconFromProjectId;
            }
            if(gadget.copyScreenshotsFromProjectId != null){
                gadgetCopyForPublishing.copyScreenshotsFromProjectId = gadget.copyScreenshotsFromProjectId;
            }
            gadgetCopyForPublishing.description = _descriptionText;
			gadgetCopyForPublishing.categoryId = _categoryId;
            gadgetCopyForPublishing.price = _priceValue;
            if(_shoppCheckBox != null){
			    gadgetCopyForPublishing.hideInStoreList = !_shoppCheckBox.selected;
            }else{
                gadgetCopyForPublishing.hideInStoreList = gadget.hideInStoreList;
            }
			
			if (!metaOnly)
				gadgetCopyForPublishing.projectId = gadget.projectId; // this is the act of publishing the current version as the new published version
			
			const EMBED_SUPPORTED:Boolean = getEmbedSupportedFor(gadget);
			const REPUBLISH_SUPPORTED:Boolean = getRepublishSupportedFor(gadget);
			
			if (REPUBLISH_SUPPORTED)
				gadgetCopyForPublishing.license.republish = _republishCheckBox.selected;

            if(!IS_ZAPP){
                gadgetCopyForPublishing.license.edit = _editableCheckBox.selected;
            }else{
                gadgetCopyForPublishing.license.edit = false;
            }

			if (EMBED_SUPPORTED && _embedCheckBox != null){
				gadgetCopyForPublishing.license.embed = _embedCheckBox.selected;
            }else{
				gadgetCopyForPublishing.license.embed = false;
            }

            var operation:ProjectSaveOperation = new ProjectSaveOperation(gadgetCopyForPublishing, true, replaceExisting);
			
			const standardErrorResponse:Function = function(event:GadgetEvent):void {
				resetAfterSave(false, gadget, metadataWasUpToDate, event, gadgetCopyForPublishing);
			};
			operation.addEventListener(GadgetEvent.ERROR, standardErrorResponse);
			operation.addEventListener(GadgetEvent.SAVE_NO_PERMISSION, standardErrorResponse);
			operation.addEventListener(GadgetEvent.SAVE_FAILED, standardErrorResponse);
			operation.addEventListener(GadgetEvent.PUBLISHED_NAME_IN_USE, standardErrorResponse);
            operation.addEventListener(GadgetEvent.NAME_IN_USE, standardErrorResponse);
			operation.addEventListener(GadgetEvent.REPUBLISH_NOT_ALLOWED, standardErrorResponse);
			operation.addEventListener(GadgetEvent.PUBLISHED_PRICE_TOO_LOW, standardErrorResponse);
			operation.addEventListener(Event.COMPLETE, function(event:Event):void {
				trace("PUBLISH COMPLETE. publishId=" + operation.id);
				// icon and screenshot refs are always fresher in the reference gadget. prevent these from being overwritten.
				gadgetCopyForPublishing.icon = gadget.icon;

				gadget.copyFromPublished(gadgetCopyForPublishing); // load values from copy since the operation was successful
				const hadBeenPublishedBefore:Boolean = gadget.published;
				gadget.projectId = operation.id;
                gadget.published = gadgetCopyForPublishing.published;
                gadget.nameIsFromDatabase = gadgetCopyForPublishing.nameIsFromDatabase;
                gadget.copyContentFromProjectId = null;
                gadget.copyIconFromProjectId = null;
				gadget.copyScreenshotsFromProjectId = null;
				gadget.pendingIconOperation = null;
				gadget.pendingScreenshotOperation = null;

                var type:String = (IS_ZAPP?"app":"gadget");

				if (!metaOnly) {
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:70, inserts:[type, gadget.publishedName]}));
				} else {
					if (!metadataWasUpToDate) {
//						notificationText = (IS_ZAPP?"Zapp":"Gadget") + " '"+gadget.publishedName+"' information successfully replaced.";
					}
				}
				
				if (gadget.pendingIconOperation || gadget.pendingScreenshotOperation) {
					var userFriendlyOperationLabel:String;
					if (gadget.pendingIconOperation && gadget.pendingScreenshotOperation)
						userFriendlyOperationLabel = "icon and screenshot";
					else if (gadget.pendingIconOperation && !gadget.pendingScreenshotOperation)
						userFriendlyOperationLabel = "icon";
					else if (!gadget.pendingIconOperation && gadget.pendingScreenshotOperation)
						userFriendlyOperationLabel = "screenshot";
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:72, inserts:[userFriendlyOperationLabel]}));
				}
				resetAfterSave(true, gadget);
			});
			
			Dock.instance.publishInProgress = gadget;
			Dock.instance.lastSuccessfullyPublished = null;
			GadgetController.instance.addToSavingQueue(operation);
            Application.instance.publishingInProgress = false;
		}
		
		protected function resetAfterSave(success:Boolean, gadget:Project, metadataWasUpToDate:Boolean=true, event:Event=null, gadgetCopyForPublishing:Project=null):void
		{
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.REFRESH_MAIN_SITE, null));
            const IS_ZAPP:Boolean = getIsZappFor(gadget);
			gadget.metadataUpToDate = success?true:metadataWasUpToDate;
			
			Dock.instance.publishInProgress = null;
			Dock.instance.lastSuccessfullyPublished = success?gadget:null;

            if(success && IS_ZAPP){
                onZappingSuccess();
            }

			if (!event) return;
            var gadgetType:String = (IS_ZAPP?"app":"gadget");
            switch (event.type) {
				case GadgetEvent.SAVE_NO_PERMISSION:
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:44, inserts:[gadgetType, _nameText]}));
					break;
				case GadgetEvent.REPUBLISH_NOT_ALLOWED:
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:49, inserts:[gadgetType, _nameText]}));
					break;
				case GadgetEvent.PUBLISHED_PRICE_TOO_LOW:
					// TODO: assign the resulting minPrice to our target so we can compare prior to publishing subsequent times.
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:48, inserts:[gadgetType, GadgetDescription.prettyPrice(gadgetCopyForPublishing.minPrice)]}));
					break;
				case GadgetEvent.NAME_IN_USE:
					if (_alreadyAgreedToReplace) {
						// we already know the name is in use, and we already agreed to replace the item.
						confirmPublishOver(gadget)
					} else {
                        var gadgetEvent:GadgetEvent = event as GadgetEvent;
                        if (gadgetEvent && gadgetEvent.id) {
                            if(IS_ZAPP){
                                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:52, inserts:[_nameText], closeFunction:handlePublishOver(gadget, gadgetEvent.id)}));
                            }else{
                                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:54, inserts:[_nameText], closeFunction:handlePublishOver(gadget, gadgetEvent.id)}));
                            }
                        } else {
                            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:45, inserts:[gadgetType, _nameText]}));
                        }
                    }
					break;
                case GadgetEvent.PUBLISHED_NAME_IN_USE:
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:45, inserts:[gadgetType, _nameText]}));
                    break;
				case GadgetEvent.SAVE_FAILED:
				default:
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:46, inserts:[gadgetType, _nameText]}));
					break;
			}
		}

        protected function onZappingSuccess():void{
            //Must be overridden by Zapping Panel
        }

		internal function handlePublishOver(gadget:Project, id:String=null):Function
		{
			return function(event:CloseEvent):void {
				switch (event.detail) {
					case Alert.FIRST:
						confirmPublishOver(gadget);
						break;
					default:
						break;
				}
			};
		}
		
		internal function confirmPublishOver(gadget:Project):void
		{
//			if (id == null) {
//				throw new Error("Publish Over attempted with null id!");
//			}
			save(gadget, false, true);
		}
	}
}
