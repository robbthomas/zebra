package com.alleni.author.controller.ui
{
import caurina.transitions.Tweener;

import com.alleni.author.Navigation.SmartObjectEditingMediator;
import com.alleni.author.application.ApplicationUI;
import com.alleni.author.application.dialogs.AboutDialog;
import com.alleni.author.application.dialogs.HistoryDialog;
import com.alleni.author.application.dialogs.TemplateDialog;
import com.alleni.author.application.dialogs.proBranded.ProBrandedDialog;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.application.ui.PlayerUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ErrorMessagingController;
import com.alleni.author.controller.ObjectDragMediator;
import com.alleni.author.controller.app.ClipboardController;
import com.alleni.author.controller.app.CursorController;
import com.alleni.author.controller.app.KeyMediator;
import com.alleni.author.controller.app.MenuController;
import com.alleni.author.controller.app.ProjectController;
import com.alleni.author.controller.app.ProjectLoadingSequencer;
import com.alleni.author.controller.objects.IUndoMediator;
import com.alleni.author.controller.objects.PathEditingMediator;
import com.alleni.author.controller.test.TestPanelController;
import com.alleni.author.controller.text.TextEditMediator;
import com.alleni.author.controller.ui.palettes.DockController;
import com.alleni.author.controller.ui.palettes.FeedbackController;
import com.alleni.author.controller.ui.palettes.FindWindowController;
import com.alleni.author.controller.ui.palettes.InspectorController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.controller.ui.palettes.LogPaletteController;
import com.alleni.author.controller.ui.palettes.OutlineWindowController;
import com.alleni.author.controller.ui.palettes.ToolboxController;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.definition.application.ApplicationStrings;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.document.Document;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.LMS;
import com.alleni.author.model.objects.ProjectObject;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.SetterWireAnchor;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.service.users.UserService;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.WorldView;
import com.alleni.author.view.feedback.IFeedback;
import com.alleni.author.view.objects.ArenaControlBar;
import com.alleni.author.view.objects.AuthorNoteView;
import com.alleni.author.view.objects.CompositeView;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.author.view.ui.OverlayMessage;
import com.alleni.author.view.ui.ReplacementFeedback;
import com.alleni.author.view.ui.WireboardView;
import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
import com.alleni.author.view.ui.controls.Stats;
import com.alleni.taconite.controller.NotificationController;
import com.alleni.taconite.definition.NotificationNames;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.factory.IEnvironment;
import com.alleni.taconite.factory.ITaconiteWindow;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;
import com.alleni.taconite.view.collision.CollisionDetector;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Stage;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.net.FileReference;
import flash.net.URLRequest;
import flash.net.navigateToURL;
import flash.text.Font;
import flash.utils.Dictionary;
import flash.utils.Timer;
import flash.utils.getTimer;

import mx.core.FlexGlobals;
import mx.core.IFlexDisplayObject;
import mx.events.PropertyChangeEvent;
import mx.managers.PopUpManager;

public class ApplicationController extends EventDispatcher
	{
		public static const TOOLTIP_DELAY:Number = 1 * 1000; // 1 second
		private static const VALID_DIMENSION:uint = 10000;
		private static var _tooltipTimer:Timer;
		
		private static var _instance:ApplicationController;
		private static var _invokedAsEditor:Boolean;
		private static var _notificationController:NotificationController;
		
		private var _viewsToUpdate:Vector.<ObjectView> = new Vector.<ObjectView>();
        private var _enterFrameActions:Vector.<Function> = new Vector.<Function>();
        private var _layoutActions:Vector.<Function> = new Vector.<Function>();
		private var _reorderActions:Vector.<Function> = new Vector.<Function>();
        private var _dragMoveFunction:Function;

		private var _view:ApplicationUI;
		private var _userService:UserService;
		private var _diagramWindowController:DiagramWindowController;
		private var _qaPaletteController:FeedbackController;
		private var _logPaletteController:LogPaletteController;
		private var _testPanelController:TestPanelController;
		private var _outlineWindowController:OutlineWindowController;
        private var _findWindowController:FindWindowController;
		private var _authorController:AuthorController;
		private var _applicationKeyController:KeyMediator;
		private var _clipboardController:ClipboardController;
		private var _wireController:WireController;
		private var _cursorController:CursorController;
        private var _errorMessagingController:ErrorMessagingController
        private var _currentUserMessage:String;
		
		private var _previousWindowArrangement:int; // Window arrangement prior to rearranging the windows
		private var _toolTip:ControlAreaToolTip;
		private var _message:OverlayMessage;

        private var _disabledInput:Boolean = false;
        private var _disableInputTokens:Dictionary = new Dictionary(true);
        private var _forcedEnabledInput:Boolean;

        private var _totalInactiveMillisecs:Number = 0;  // total time menubar is grayed out
        private var _startInactiveTime:Number;  // beginning of the current period of being inactive



//        private var _rightClickMenu:RightClickMenuView;
		
		public function ApplicationController(view:ApplicationUI)
		{
			Utilities.assert(!_instance);
			_instance = this;
			_view = view;
			_notificationController = new NotificationController(_instance)
			initialize();
		}
		
		static public function get instance():ApplicationController
		{
			Utilities.assert(_instance != null);
			return _instance;
		}
		
		private function initialize():void
		{
			ProjectLoadingSequencer.instance;
			ProjectController.instance;
            TaconiteFactory.getEnvironmentImplementation().addExternalCallback("onUpdateEditorProperty", onUpdateEditorProperty);
            _clipboardController = new ClipboardController();
			_wireController = new WireController();
			_authorController = new AuthorController();
			_cursorController = new CursorController(TaconiteFactory.getCursorManagerImplementation());
            _errorMessagingController = new ErrorMessagingController();
            _applicationKeyController = new KeyMediator();
			if (_invokedAsEditor) {
				AssetImportController.instance;
				ToolboxController.instance;
				InspectorController.instance;
                _errorMessagingController.importErrorMessages();
			}
			UserService.start();
			_userService = UserService.instance;
			_userService.addEventListener(NotificationNamesApplication.UPDATE_CUSTOMER_TYPE, handleTemplateDialogUpdate);
			
			Application.instance.window.closeCallback = handleWindowClose;
			Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
			
			// notification listener
			addEventListener(NotificationNames.STOP_OPERATION_PROGRESS, handleStopOperationProgress);
			addEventListener(NotificationNamesApplication.NOTIFY_USER, handleNotifyUser);
			addEventListener(NotificationNamesApplication.RESET_USER_MESSAGE, handleResetUserMessage);
			addEventListener(NotificationNamesApplication.RESET_USER_MESSAGE_WITH_DELAY, handleResetUserMessageWithDelay);
			addEventListener(NotificationNamesApplication.SHOW_ABOUT_WINDOW, handleShowAboutWindow);
			addEventListener(NotificationNamesApplication.PROJECT_LOADING, handleProjectLoading);
			addEventListener(NotificationNamesApplication.PROJECT_CHANGING, handleProjectChanging);
            addEventListener(NotificationNamesApplication.PROJECT_COMPLETE, handleProjectComplete);
            addEventListener(NotificationNamesApplication.PROJECT_UNSAVED_CHANGES, handleProjectUnsavedChanges);
			addEventListener(NotificationNamesApplication.PROJECT_NAME_CHANGED, handleProjectNameChange);
			addEventListener(NotificationNamesApplication.PROJECT_SETTINGS_CHANGED, handleProjectSettingsChanged);
			addEventListener(NotificationNamesApplication.SCALE_CHANGED, handleScaleChanged);
			addEventListener(NotificationNamesApplication.OPEN_TEMPLATE_DIALOG, handleTemplateDialog);
			addEventListener(NotificationNamesApplication.APPLICATION_QUITTING, handleQuitting);
			addEventListener(NotificationNamesApplication.ZOOM_IN, handleZoomIn);
			addEventListener(NotificationNamesApplication.ZOOM_OUT, handleZoomOut);
			addEventListener(NotificationNamesApplication.ZOOM_IN_NO_ADJUST,handleZoomInWithoutAdjust),
			addEventListener(NotificationNamesApplication.ZOOM_OUT_NO_ADJUST,handleZoomOutWithoutAdjust),
			addEventListener(NotificationNamesApplication.ACTUAL_SIZE, handleSetViewActualSize);
			addEventListener(NotificationNamesApplication.FIT_IN_WINDOW, handleSetViewFitEverything);
			addEventListener(NotificationNamesApplication.TOGGLE_LIBRARY, toggleLibrary);
            addEventListener(NotificationNamesApplication.TOGGLE_VARIABLE_LIBRARY, toggleVariableLibrary);
			addEventListener(NotificationNamesApplication.TOGGLE_QA_WINDOW, toggleQA);
			addEventListener(NotificationNamesApplication.TOGGLE_LOG_WINDOW, toggleLog);
			addEventListener(NotificationNamesApplication.TOGGLE_TEST_WINDOW, toggleTest);
			addEventListener(NotificationNamesApplication.TOGGLE_INSPECTOR, handleToggleInspector);
			addEventListener(NotificationNamesApplication.TOGGLE_RIBBON_LIBRARY, handleToggleRibbonLibrary);
            addEventListener(NotificationNamesApplication.SHOW_INSPECTOR, handleShowInspector);
            addEventListener(NotificationNamesApplication.HIDE_INSPECTOR, handleHideInspector);
            addEventListener(NotificationNamesApplication.SHOW_RIBBON_LIBRARY, handleShowRibbonLibrary);
            addEventListener(NotificationNamesApplication.HIDE_RIBBON_LIBRARY, handleHideRibbonLibrary);
			addEventListener(NotificationNamesApplication.HIDE_QA_WINDOW, handleHideQAWindow);
			addEventListener(NotificationNamesApplication.HIDE_LOG_WINDOW, handleHideLogWindow);
			addEventListener(NotificationNamesApplication.TOGGLE_DIAGRAM_WINDOW, handleToggleDiagramWindow);
			addEventListener(NotificationNamesApplication.STACK_WINDOWS, handleSetWindowsStacked);
			addEventListener(NotificationNamesApplication.TILE_WINDOWS, handleSetWindowsTiled);
			addEventListener(NotificationNamesApplication.UI_READY, handleUIReady);
            addEventListener(NotificationNamesApplication.RESET_APPLICATION_STATE, handleResetApplicationState);
            addEventListener(NotificationNamesApplication.SHOW_ALL_TOGGLED, handleShowAllToggled);
            addEventListener(NotificationNamesApplication.PUBLISH_GADGET, handlePublishGadget);
            addEventListener(NotificationNamesApplication.OPEN_WIKI, handleOpenWiki);
			addEventListener(NotificationNamesApplication.SHOW_PRO_BRAND, handleShowProBrandDlg);
			addEventListener(NotificationNamesApplication.UPDATE_CUSTOMER_TYPE, handleTemplateDialogUpdate);
            // Editor Notifications
            addEventListener(NotificationNamesEditor.UNDO, handleUndo);
			addEventListener(NotificationNamesEditor.REDO, handleRedo);
            addEventListener(NotificationNamesEditor.NUDGE_DOWN, handleNudgeDown);
            addEventListener(NotificationNamesEditor.NUDGE_LEFT, handleNudgeLeft);
            addEventListener(NotificationNamesEditor.NUDGE_RIGHT, handleNudgeRight);
            addEventListener(NotificationNamesEditor.NUDGE_UP, handleNudgeUp);
            addEventListener(NotificationNamesEditor.NUDGE_TEN_DOWN, handleNudgeDownTen);
            addEventListener(NotificationNamesEditor.NUDGE_TEN_LEFT, handleNudgeLeftTen);
            addEventListener(NotificationNamesEditor.NUDGE_TEN_RIGHT, handleNudgeRightTen);
            addEventListener(NotificationNamesEditor.NUDGE_TEN_UP, handleNudgeUpTen);
            addEventListener(NotificationNamesEditor.COPY, handleCopy);
			addEventListener(NotificationNamesEditor.CUT, handleCut);
			addEventListener(NotificationNamesEditor.PASTE, paste);
            addEventListener(NotificationNamesEditor.LOCAL_PASTE, paste);
//            addEventListener(NotificationNamesEditor.RIGHT_CLICK_FIRED, handleRightClick);
//            addEventListener(NotificationNamesEditor.HIDE_RIGHT_CLICK_MENU, handleHideRightClickMenu);
			
			dispatchEvent(new ApplicationEvent(NotificationNamesApplication.ACTIVE_WINDOW_CHANGED, Application.instance.window));
		}

        private function onUpdateEditorProperty(payload:Object):void {
            var property:String = payload.property as String;
            var oldValue:Object = payload.oldValue;
            var newValue:Object = payload.newValue;

            switch(property){
                case "projectId":
                    updateProjectId(oldValue as String, newValue as String);
                    break;
            }
        }

        private function updateProjectId(oldValue:String, newValue:String):void {
            var id:String = Application.instance.document.project.projectId;
            if(id == oldValue){
                Application.instance.document.project.projectId = newValue;
            }
        }

		/**
		 * Startup the application with an initial blank project. 
		 * @param window = the window if this is desktop version OR the container if this is web version.
		 * 
		 */
		public static function bootApplication(window:ITaconiteWindow):void
		{			
			// Create the Author Application model.
			new Application();
			Application.instance.window = window;
			
			_invokedAsEditor = TaconiteFactory.getEnvironmentImplementation().invokedAsEditor;
			
			// Create the Author Application view.
			var view:ApplicationUI;
			if (_invokedAsEditor)
				view = new EditorUI();
			else
				view = new PlayerUI();
			view.invokedAsEditor = _invokedAsEditor;
			
			// Create the Application controller.
			_instance = new ApplicationController(view);
			
			MenuController.instance;
			
			window.addElement(view);
			
			if (_invokedAsEditor)
				window.menu = TaconiteFactory.getMenuImplementation().menu;
		}

		private static function checkForInvokedProject():Boolean
		{
			const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();

			if (environment.invokedFile) {
				ProjectController.instance.loadLocalProject(environment.invokedFile);
				return true;
			}
			
			var invokedLoadID:String;
			invokedLoadID = environment.invokedPurchaseID;
			if (invokedLoadID) {
				ProjectLoadingSequencer.instance.requestLoad(invokedLoadID, false, true);
				return true;
			}
			invokedLoadID = environment.invokedProjectID;
			if (invokedLoadID && !environment.showHistory) {
                if(invokedLoadID == "open"){
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent((NotificationNamesApplication.OPEN_PROJECT)));
                    _instance.makeNewUntitledProject();
                    return false;
                }else{
				    ApplicationUI.instance.captureMouseClicks(true);
				    ProjectLoadingSequencer.instance.requestLoad(invokedLoadID);
                    return true;
                }
			}
			
			if (_invokedAsEditor) {
				_instance.makeNewUntitledProject();

                if (environment.showHistory) {
                    Application.instance.showHistory = true;
                    ApplicationController.instance.createPopUp(HistoryDialog, true);
                } else {
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.OPEN_TEMPLATE_DIALOG));
				}
            }

			return false;
		}
		
		public function loadFromLocalInvocation(file:FileReference):void
		{
			if (Application.instance.document) { // if there is a current document, we will handle the invocation event
				if (!file) return;
				ProjectController.instance.loadLocalProject(file);
			}
		}

        /**
         * Removes illegal wires when you upgrade to pro project type
         */
        public function stripIllegalWires(event:Event = null):void{
            var project:ProjectObject = (Application.instance.document.root.value as World).project;
            for each (var wire:Wire in _wireController.getWires(project)){
                if(wire.slaveAnchor is ExternalSmartWireAnchor && wire.slaveAnchor.modifierDescription is InletDescription){
                    WireCreateUtil.unCreate(wire);
                }
            }
        }

        /**
         * Clears the entire undo stack
         */
        public function clearUndo(event:Event = null):void{
            _authorController.currentActionTree.clear();
        }

		public function createPopUp(className:Class, modal:Boolean=false):IFlexDisplayObject
		{
			if(!modal){
                var feedbackWindowShouldOverrideModality:Boolean = Application.instance.qaVisible;
            }
            var mainDisplay:DisplayObject = DisplayObject(FlexGlobals.topLevelApplication);
			var popup:IFlexDisplayObject = PopUpManager.createPopUp(mainDisplay, className, feedbackWindowShouldOverrideModality?false:modal);

            popup.x = (mainDisplay.width - popup.width)/2;
            popup.y = (mainDisplay.height - popup.height)/2;

			return popup;
		}

		public function removePopup(popup:IFlexDisplayObject):void
		{
			PopUpManager.removePopUp(popup);
		}
		
		public function captureMouseClicks(value:Boolean, callback:Function=null):void
		{
			_view.captureMouseClicks(value, callback);
		}

    /**
     * Disable keyboard and mouse input (and all other input someday).
     * Supports multiple clients disabling input with overlapping timing, not necessarily nested in an orderly way.
     * @return integer token that should be passed to enableInput()
     */
        public function disableInput(source:String="Unnamed"):Object
        {
            const token:Object = {name:source};
            _disableInputTokens[token] = true;
//            authorController.log(Utilities.timestamp + " ApplicationController::disableInput source=" + source);

            if (!_disabledInput) {
                disableInputNow();
            }
            return token;
        }

    /**
     * Enable keyboard and mouse input.
     * @param token that was received from disableInput().
     */
        public function enableInput(token:Object):void
        {
            if (token == null) {
                return;
            }
//            authorController.log(Utilities.timestamp + " ApplicationController::enableInput source=" + token.name);
            if (token in _disableInputTokens) {
                delete _disableInputTokens[token];
            } else if (!_forcedEnabledInput) {
                throw new Error("disableInput: unknown token="+token);
            }
//            dumpDisableInputTokens();

            enableInputWhenNeeded();
        }

        private function enableInputWhenNeeded():void
        {
            // this is called in render loop in case a token was garbage-collected
            if (_disabledInput && !disabledInputTokenExists) {
                enableInputNow();
            }
        }

        private function disableInputNow():void
        {
//            authorController.log(Utilities.timestamp + " ApplicationController::disableInput DISABLING");
            _view.captureMouseClicks(true);   // grays the menubar
            _view.mouseChildren = false;     // needed since captureMouseClicks isn't immediate
            dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
            if (!_disabledInput) {
                _disabledInput = true;
                _startInactiveTime = getTimer();
                busyPatternVisible = true;
            }
        }

        private function enableInputNow():void
        {
            _view.captureMouseClicks(false);
            _view.mouseChildren = true;
            dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
            if (_disabledInput) {
                _disabledInput = false;
                var mSecs:Number = (getTimer() - _startInactiveTime);
                _totalInactiveMillisecs += mSecs;
                busyPatternVisible = false;
//                authorController.log(Utilities.timestamp + " ApplicationController::enableInput ENABLING after mSecs="+mSecs);
            }
        }

        public function dumpDisableInputTokens():void
        {
            authorController.log("dumpDisableInputTokens")
            for (var token:Object in _disableInputTokens) {
                authorController.log("  token="+token.name);
            }
        }

        public function forceEnableInput():void
        {
            authorController.log("forceEnableInput")
            _disableInputTokens = new Dictionary(true);
            enableInputNow();
            _forcedEnabledInput = true;
        }

        private function get disabledInputTokenExists():Boolean
        {
            // return true if we have at least one token
            for each (var t:* in _disableInputTokens) {
                return true;
            }
            return false;
        }

        public function disabledTokenInDict(token:Object):Boolean{
            if(_disableInputTokens != null){
                return token in _disableInputTokens;
            }
            return false;
        }

        public function get disabledInput():Boolean
        {
            return _disabledInput;
        }

        private function set busyPatternVisible(value:Boolean):void
        {
            var editorUI:EditorUI = _view as EditorUI;
            if (editorUI && editorUI.busyPattern) {
                editorUI.busyPattern.visible = value;
            }
        }

        public function get totalInactiveSeconds():Number
        {
            return _totalInactiveMillisecs / 1000.0;
        }

		private function handleShowProBrandDlg(event:ApplicationEvent):void {
			trace("calling pro-brand dialog");	
			var dlg:ProBrandedDialog = createPopUp(ProBrandedDialog, true) as ProBrandedDialog;
		}

        private function handlePublishGadget(event:ApplicationEvent):void{
            var selection:Vector.<TaconiteModel> = authorController.cleanSelection;
            if(selection.length == 1){
                if(selection[0].value is Composite){
                    var composite:Composite = selection[0].value as Composite;
                    (composite.getView() as CompositeView).openPublishingDialog();
                }
            }
        }

        private function handleCopy(event:ApplicationEvent):void{
            Application.instance.window.displayObject.stage.dispatchEvent(new Event(Event.COPY));
        }

        private function handleCut(event:ApplicationEvent):void{
            Application.instance.window.displayObject.stage.dispatchEvent(new Event(Event.CUT));
        }

        private function handleShowAllToggled(event:ApplicationEvent):void{
            Application.instance.showAll = !Application.instance.showAll;
			wireController.showWiresForLevel();
        }

		private function handleUndo(event:ApplicationEvent):void
		{
            // undo can be threaded and take a long time, such as for whole-object replace-all
            var mediator:IUndoMediator =_authorController.currentUndoMediator;
			if (mediator.canUndo() && !ObjectDragMediator.draggingNow) {
                var inputToken:Object = disableInput("undo");
                userMessage("Undo "+mediator.undoName(), function():void {
                    AuthorController.instance.editWhileRunningStart();
                    PseudoThread.add(new PseudoThreadSimpleRunnable("undo", mediator.undo));
                    PseudoThread.add(new PseudoThreadSimpleRunnable("finish undo", function():void {
                        LibraryController.instance.updateForAssetChange();
                        AuthorController.instance.editWhileRunningEnd();
                        clearUserMessage();
                        enableInput(inputToken);
                    }));
                });
			}
		}
		
		private function handleRedo(event:ApplicationEvent):void
		{
            var mediator:IUndoMediator =_authorController.currentUndoMediator;
			if (mediator.canRedo() && !ObjectDragMediator.draggingNow) {
                var inputToken:Object = disableInput("redo");
                userMessage("Redo "+mediator.redoName(), function():void {
                    AuthorController.instance.editWhileRunningStart();
                    PseudoThread.add(new PseudoThreadSimpleRunnable("redo", mediator.redo));
                    PseudoThread.add(new PseudoThreadSimpleRunnable("finish redo", function():void {
                        LibraryController.instance.updateForAssetChange();
                        AuthorController.instance.editWhileRunningEnd();
                        clearUserMessage();
                        enableInput(inputToken);
                    }));
                });
			}
		}
		
		private function handleSetViewActualSize(event:ApplicationEvent):void
		{
			handleSetViewScale(1.0);
		}
		
        private function handleNudgeUp(event:ApplicationEvent):void
        {
            _authorController.nudge(new Point(0, -1));
        }

        private function handleNudgeLeft(event:ApplicationEvent):void
        {
            _authorController.nudge(new Point(-1, 0));
        }

        private function handleNudgeRight(event:ApplicationEvent):void
        {
            _authorController.nudge(new Point(1, 0));
        }

        private function handleNudgeDown(event:ApplicationEvent):void
        {
            _authorController.nudge(new Point(0, 1));
        }

        private function handleNudgeUpTen(event:ApplicationEvent):void
        {
            _authorController.nudge(new Point(0, -10));
        }

        private function handleNudgeLeftTen(event:ApplicationEvent):void
        {
            _authorController.nudge(new Point(-10, 0));
        }

        private function handleNudgeRightTen(event:ApplicationEvent):void
        {
            _authorController.nudge(new Point(10, 0));
        }

        private function handleNudgeDownTen(event:ApplicationEvent):void
        {
            _authorController.nudge(new Point(0, 10));
        }

		private function handleStopOperationProgress(event:ApplicationEvent):void
		{
            clearUserMessage();
		}
		
		private function handleNotifyUser(e:ApplicationEvent):void
		{
			if (!_message) {
				_message = new OverlayMessage();
				_message.visible = false;
				_view.stage.addChild(_message);
			}
			var messages:Array = e.data as Array;
			_message.paneX = (_view.width - OverlayMessage.WIDTH)/2;
			_message.paneY = 0.75 * _view.height;
			
			if (messages.length > 0) {
				if (messages[1] == null) messages[1] = "";
				if (messages[2] == null) messages[2] = OverlayMessage.DEFAULT_SKIN;
				_message.show(messages[0] as String, messages[1] as String, messages[2] as String, messages[3] as DisplayObject);
			}
		}

        public function userMessage(message:String, onComplete:Function=null, thisarg:Object=null, args:Array=null):void
        {
            // show a status message and wait for it to appear, then call "onComplete" function
            _currentUserMessage = message;
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, message));
            if (onComplete != null) {
                PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                PseudoThread.add(new PseudoThreadSimpleRunnable("status="+message, onComplete, thisarg, args));
            }
        }

        public function clearUserMessage():void
        {
            userMessage(Application.instance.defaultUserMessage);
        }

        public function setDefaultUserMessage(newDefault:String):void
        {
            // set the default message.  Show it now unless a non-default message was showing
            var showingDefaultMessage:Boolean = (_currentUserMessage == Application.instance.defaultUserMessage);
            Application.instance.defaultUserMessage = newDefault;
            if (showingDefaultMessage) {
                clearUserMessage();  // immediately show new message, eg. "Unsaved Changes"
            }
        }

        public function resetDefaultUserMessage():void
        {
            Application.instance.defaultUserMessage = ApplicationStrings.DEFAULT_STATUS_MESSAGE;
            clearUserMessage();
        }

		private function handleResetUserMessage(event:ApplicationEvent):void
		{
			if (event.data as String)
				setDefaultUserMessage(event.data as String);
            else
                clearUserMessage();
		}
		
		private function handleResetUserMessageWithDelay(event:ApplicationEvent):void
		{
			var timer:Timer = new Timer(8000, 1);
			timer.addEventListener(TimerEvent.TIMER, function():void {
				dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE, event.data));
			});
			timer.start();
		}
		
		private function handleProjectNameChange(event:ApplicationEvent):void
		{
			if (_invokedAsEditor)
				Application.instance.window.title = TaconiteFactory.getAppDescriptorImplementation().longName + " - " + Application.instance.document.name;
			else
				Application.instance.window.title = Application.instance.document.project.publishedName;
		}
		
		private function handleProjectSettingsChanged(e:Event):void
		{
			handleScaleChanged();
		}
		
		private function handleScaleChanged(e:Event=null):void
		{			
			// center the main presentation container
			WorldContainer(Application.instance.viewContext).centerOffset = new Point(0,0);
		}

		private function handleToggleInspector(event:ApplicationEvent):void
		{
			Application.instance.inspectorVisible = !Application.instance.inspectorVisible;
		}
		
		private function handleToggleRibbonLibrary(event:ApplicationEvent):void
		{
			Application.instance.ribbonLibraryVisible = !Application.instance.ribbonLibraryVisible;
		}
		
		private function handleShowInspector(event:ApplicationEvent):void
		{
			Application.instance.inspectorVisible = true;
		}
		
		private function handleShowRibbonLibrary(event:ApplicationEvent):void
		{
			Application.instance.ribbonLibraryVisible = true;
		}
		
		private function handleHideInspector(event:ApplicationEvent):void
		{
			Application.instance.inspectorVisible = false;
		}
		
		private function handleHideRibbonLibrary(event:ApplicationEvent):void
		{
			Application.instance.ribbonLibraryVisible = false;
		}
		
		private function handleHideQAWindow(event:ApplicationEvent):void
		{
			Application.instance.qaVisible = false;
			dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
		}
		
		private function handleHideLogWindow(event:ApplicationEvent):void
		{
			Application.instance.logVisible = false;
			dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
		}
		
		private function handleToggleDiagramWindow(event:ApplicationEvent):void
		{
			var isVisible:Boolean = diagramWindowVisible;
			diagramWindowVisible = !isVisible;
		}
		
		private function handleUIReady(event:ApplicationEvent):void
		{
			if (_invokedAsEditor) {
				var editorUI:EditorUI = _view as EditorUI;
				LibraryController.instance.container = editorUI.leftControlBar.palette;
				DockController.instance.container = editorUI.dockContainer;
			}
			if (!checkForInvokedProject() && _invokedAsEditor)
				refreshViewContext(Application.instance.viewContext);
			Application.instance.window.visible = true;
		}
		
		private function handleResetApplicationState(event:ApplicationEvent):void
		{
			clearUserMessage();
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNames.STOP_OPERATION_PROGRESS));
		}
		
		public function refreshViewContext(viewContext:ViewContext):void
		{
			if (!viewContext) return;
			var application:Application = Application.instance;
			application.viewContext = viewContext;
			dispatchEvent(new ApplicationEvent(NotificationNamesApplication.VIEW_CONTEXT_UPDATED));
			resetKeyboardFocus(true);
			var renderHandler:Function;
			
			if (_invokedAsEditor) {
				EditorUI.instance.projectViewMode = EditorUI.PROJECT_VIEW_PRESENTATION;
				renderHandler = handleEditorRender;
			} else {
				renderHandler = handleRender; // least work possible for the player render loop
			}

			application.viewContext.stage.addEventListener(Event.RENDER, renderHandler); // listen for rendering
			application.viewContext.addEventListener(Event.REMOVED_FROM_STAGE, function(e:Event):void {
				// clean up when this context is no longer needed
				if (e.target.stage)
					e.target.stage.removeEventListener(Event.RENDER, renderHandler);
				e.target.removeEventListener(e.type, arguments.callee);
			});
		}
		
		public function requestRender():void
		{
			var stage:Stage = Application.instance.viewContext.stage;
			if (stage)
				stage.dispatchEvent(new Event(Event.RENDER));
		}
		
		private function handleRender(e:Event):void
		{
			
			if (_dragMoveFunction != null) {
				_dragMoveFunction();
				_dragMoveFunction = null;
			}
			
			var action:Function;
			
			// handle reorder actions
			for each (action in _reorderActions) {
				action();
			}
			_reorderActions.splice(0, _reorderActions.length);
			//end handle reorder actions
			
			// handle layout actions
			for each (action in _layoutActions) {
				action();
			}
			_layoutActions.splice(0, _layoutActions.length);
			// end handle layout actions
			
			_wireController.handleBindings();
			
			// handle view updates
			var view:ObjectView;
			for each (view in _viewsToUpdate)
				if (view.object.parent || view as WorldView)  // skip deleted objects
					view.render();
			_viewsToUpdate.splice(0, _viewsToUpdate.length);			
			// end handle view updates

			// handle selection handle updates - TEMPORARY enabled for player only for runtime rotation handle
			const worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			var container:DisplayObjectContainer;
			var k:int;
			var n:int;
			
			if (!worldContainer) return;
			container = worldContainer.feedbackHandlesView;
			
			k = container.numChildren;
			for (n=0; n < k; n++) {
				var fb:IFeedback = container.getChildAt(n) as IFeedback;
				if (fb)
					if (fb.visible)
						fb.render();
			}			
			// end handle selection handle updates - TEMPORARY enabled for player only for runtime rotation handle

			// TODO: account for runtime rotation grip without having to take on the weight of the entirety of selection handles.
			
			// handle controller updates : adjust location of arena paging control-bars
			container = worldContainer.feedbackControlsView;
			k = container.numChildren;
			for (n=0; n < k; n++) {
				var bar:ArenaControlBar = container.getChildAt(n) as ArenaControlBar;
				if (bar)
					if (bar.visible)
						bar.updatePosition();
			}			
			// end handle controller updates

			var world:World = authorController.world;
			if (world && world.eventFlow && world.eventFlow.currentPage && Application.running){
                handleCollisions(world.eventFlow.currentPage.object);
                for each(var master:AbstractContainer in world.eventFlow.currentPage.masters){
                    if(master != null){
                        handleCollisions(master);
                    }
                }
            }

            // now that we're done, let other renderers run
            dispatchEvent(e);

            if (_disabledInput) {
                enableInputWhenNeeded();
            }
		}
					
		private function handleEditorRender(e:Event):void
		{
			handleRender(e);

			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			var container:DisplayObjectContainer;
			var k:int;
			var n:int;
			
			// handle message center updates
			container = worldContainer.feedbackUIView;
			
			if (container.visible) {
				var area:Rectangle = EditorUI.instance.messageCenterPlacementArea(container);
				
				k = container.numChildren;
				for (n=0; n < k; n++) {
					var mc:MessageCenterView = container.getChildAt(n) as MessageCenterView;
					if (mc) {
						if (mc.visible)
							mc.updatePosition(area);
                    }
				}
			}
			// end handle message center updates
						
			// handle annotation updates : adjust location of author notes
			container = worldContainer.feedbackAnnotationView;
			
			k = container.numChildren;
			for (n=0; n < k; n++) {
				var note:AuthorNoteView = container.getChildAt(n) as AuthorNoteView;
				if (note)
					if (note.visible)
						note.updatePosition();
			}
			// end handle annotation updates
			
			// handle group selection handle updates
			container = worldContainer.feedbackGroupHandlesView;		

			k = container.numChildren;		
			for (n=0; n < k; n++) {		
				var gfb:IFeedback = container.getChildAt(n) as IFeedback;		
				if (gfb)		
					if (gfb.visible)		
						gfb.render();		
			}			            		
			// end handle selection handle updates
			
			// handle replacement feedback positioning
			replacementFeedback.render(); // will punt if not visible
			// end handle replacement feedback positioning
			
			// velum
			var velumMediator:SmartObjectEditingMediator = authorController.currentVelumMediator;
			if (velumMediator)
				velumMediator.updateVelum();
		}

		public function get renderActionsPending():Boolean
		{
			return (_viewsToUpdate.length > 0 || _layoutActions.length > 0 || _reorderActions.length > 0);
		}
		
		private static var _replacementFeedback:ReplacementFeedback;
		private static function get replacementFeedback():ReplacementFeedback
		{
			if (!_replacementFeedback)
				_replacementFeedback = ReplacementFeedback.instance;
			return _replacementFeedback;
		}
		
		public function handleMessageCenterBringToFront(messageCenter:MessageCenterView):void
		{
			if (messageCenter && WorldContainer(messageCenter.context).feedbackUIView.contains(messageCenter))
				WorldContainer(messageCenter.context).feedbackUIView.setChildIndex(messageCenter, WorldContainer(messageCenter.context).feedbackUIView.numChildren-1);		
		}

        public function hideWiring():Boolean
        {
            var wc:WorldContainer = Application.instance.viewContext as WorldContainer;
            var oldWiringVisibility:Boolean = wc.feedbackUIView.visible;
            wc.feedbackUIView.visible = false;
            wc.feedbackWiringView.visible = false;
            return oldWiringVisibility;
        }

        public function restoreWiring(vis:Boolean):void
        {
            var wc:WorldContainer = Application.instance.viewContext as WorldContainer;
            wc.feedbackUIView.visible = vis;
            wc.feedbackWiringView.visible = vis;
        }

		public function requestUpdateView(view:ObjectView):void
		{
            // check _view.stage to avoid crash when closing desktop app
			if (_view.stage && view && _viewsToUpdate.indexOf(view) == -1) {
				_viewsToUpdate.push(view);
				_view.stage.invalidate();
			}
		}
		
        public function requestDragMove(func:Function, event:MouseEvent):void
        {
            var dragPoint:Point = new Point(event.stageX, event.stageY);
            _dragMoveFunction = function():void {
                func(event, dragPoint);
            };
            _view.stage.invalidate();
        }

        // request ENTER_FRAME actions to be executed in order FIFO
        public function requestEnterFrameAction(action:Function):void
        {
            var stage:Stage = _view.stage;
            if (stage) {
                if (_enterFrameActions.length == 0) {
                    _frameCount = 0;
                }
                _enterFrameActions.push(action);
                stage.addEventListener(Event.ENTER_FRAME, processEnterFrameActions);
            }
        }

        private var _frameCount:int;

        private function processEnterFrameActions(e:Event):void
        {
            const FRAMES:int = 2;
            if (++_frameCount < FRAMES) {  // wait an extra frame, else status messages don't show
                return;
            }
            if (_enterFrameActions.length > 0) {
                var action:Function = _enterFrameActions[0];
                _enterFrameActions.splice(0, 1);
                _frameCount = 0;
                action();
            }
            if (_enterFrameActions.length == 0) {
                var stage:Stage = _view.stage;
                if (stage) {
                    stage.removeEventListener(Event.ENTER_FRAME, processEnterFrameActions);
                }
            }
        }

        public function requestLayoutAction(action:Function):void
        {
            var stage:Stage = _view.stage;
            if (stage && _layoutActions.indexOf(action) == -1) {
                _layoutActions.push(action);
                stage.invalidate();
                stage.addEventListener(Event.ENTER_FRAME, function(e:Event):void{   // the above stage.invalidate() may not be adequate, if currently handling a render event
                    stage.removeEventListener(Event.ENTER_FRAME, arguments.callee);
                    stage.invalidate();
                });
            }
        }

		public function requestReorderAction(action:Function):void
		{	
			if (_reorderActions.indexOf(action) == -1) {
				_reorderActions.push(action);
				_view.stage.invalidate();
			}
		}
		
		private function handleCollisions(container:AbstractContainer):void
		{
            if(container.loading) {
                return;
            }
			for each (var obj:AbstractObject in container.children) {
				var view:ObjectView = obj.getView();
				if(view == null || view.visible == false || view.alpha == 0) {
					continue;
				}
				var collisions:Array = [];
				var difference:Boolean = false;
				for each(var target:AbstractObject in obj.collidingPossibilities) {
					var targetView:ObjectView = target.getView();
					if(targetView == null || targetView.visible == false || targetView.alpha == 0) {
						continue;
					}
					if(CollisionDetector.instance.detectOverlap(view, targetView)) {
						collisions.push(target);
						if(obj.collidingWith.indexOf(target) < 0) {
							difference = true;
						}
					}
				}
				if(difference || obj.collidingWith.length != collisions.length) {
					obj.collidingWith = collisions;
				}

				var container:AbstractContainer = obj as AbstractContainer;
                if (container) {
                    if (container is Arena) {
                        container = (container as Arena).currentArenaPage;
                    }
                    handleCollisions(container);  // recurse to scan children of arena and gadget
                }
            }
		}
		
		/**
		 * Called when an Application property changes.  Update the UI accordingly. If the currentEditor property
		 * changes, update our listeners.
		 */
		private function handleApplicationPropertyChange(event:PropertyChangeEvent):void
		{
			var propertyName:String = event.property as String;
			Utilities.assert(propertyName != null);
			
			switch (propertyName) {
				case "libraryVisible":
                    EditorUI.instance.showLeftPalette(Application.instance.libraryVisible);
					break;
				case "variableLibraryVisible":
					EditorUI.instance.showLeftPalette(Application.instance.variableLibraryVisible);
					break;
				case "inspectorVisible":
					if (event.newValue)
						Application.instance.ribbonLibraryVisible = false;
					InspectorController.instance.show(Application.instance.inspectorVisible);
					break;
				case "ribbonLibraryVisible":
					if (event.newValue)
						Application.instance.inspectorVisible = false;
					InspectorController.instance.show(Application.instance.ribbonLibraryVisible);
					break;
				case "qaVisible":
					_qaPaletteController = FeedbackController.instance;
					_qaPaletteController.setVisible(Application.instance.qaVisible);
					break;
				case "logVisible":
					_logPaletteController = LogPaletteController.instance;
					_logPaletteController.setVisible(Application.instance.logVisible);
					break;
				case "testPanelVisible":
					_testPanelController = TestPanelController.instance;
					_testPanelController.setVisible(Application.instance.testPanelVisible);
					break;
				case "outlineVisible":
					_outlineWindowController = OutlineWindowController.instance;
					_outlineWindowController.setVisible(Application.instance.outlineVisible);
					break;
                case "findVisible":
                    _findWindowController = FindWindowController.instance;
                    _findWindowController.setVisible(Application.instance.findVisible);
                    break;
				case "windowArrangement":
					switch (event.newValue) {
						case Application.NONE_ARRANGEMENT:
							break;
						case Application.SINGLE_ARRANGEMENT:
							doSetWindowsSingle();
							break;
						case Application.STACKED_ARRANGEMENT:
							doSetWindowsStacked();
							break;
						case Application.TILED_ARRANGEMENT:
							doSetWindowsTiled();
							break;
					}
					break;
				case "showAll":
					authorController.handleShowAll(event.newValue);
					break;
				case "currentTool":
					if (!Application.instance.inspectorVisible 
							&& [ToolboxController.ARROW, ToolboxController.MOVE, ToolboxController.ZOOM].indexOf(Application.instance.currentTool) < 0)
						dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SHOW_INSPECTOR));
					updateWorldMouseActive();
					break;
                case "flowVisible":
                    dispatchEvent(new ApplicationEvent(NotificationNamesApplication.FLOW_VISIBILITY_CHANGED))
                    dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RECALCULATE_ARENA_CONTROLS_FOR_FLOW_CHANGE, {flowVisible:Application.instance.flowVisible, disableLoadUnload:Application.instance.disableLoadUnload}));
                    break;
                case "disableLoadUnload":
                    dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RECALCULATE_ARENA_CONTROLS_FOR_FLOW_CHANGE, {flowVisible:Application.instance.flowVisible, disableLoadUnload:Application.instance.disableLoadUnload}));
                    break;
			}
		}

		public function updateWorldMouseActive():void
		{
			if (Application.instance.currentTool == ToolboxController.ARROW || ApplicationController.instance.authorController.currentEditingMediator is PathEditingMediator)
				Application.instance.viewContext.mouseChildren = true;
			else
				Application.instance.viewContext.mouseChildren = false;
		}

		
		// Message handlers
		public function handleSetWiringView(index:int):void
		{
			_wireController.setWiringLevel(index);
		}
		
		private static const ZOOM_STEP:Number = 0.9;
		private static const MAXIMUM_ZOOM_FACTOR:Number = 1.1;
		
		public function handleZoomIn(event:ApplicationEvent = null):void
		{
			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			var worldView:WorldView = WorldView(authorController.world.getView());
			var world:World = authorController.world;
			var centerWorld:Point;
			var mouse:Point = new Point(worldContainer.stage.mouseX, worldContainer.stage.mouseY);
			var potentialZoomLevel:Number;
			// grab center of the world and 
			// store a reference to it in 
			// worldView coordinates becase
			// that coordinate system doesnt
			// change when worldContainer is
			// scaled.
			centerWorld = worldContainer.localToGlobal(new Point(world.width/2,world.height/2));
			centerWorld = worldView.globalToLocal(centerWorld);
			// store a reference to center of
			// mouse in worldView coordinates
			// because that coordinate system
			// doesnt change during scaling
			mouse = worldView.globalToLocal(mouse);
			// apply the scale to worldContainer
			potentialZoomLevel = Application.instance.zoomLevel * (1.0 / ZOOM_STEP);
			
			if(worldView.height > VALID_DIMENSION || worldView.width > VALID_DIMENSION){
				potentialZoomLevel *= 1;  //NO CHANGE -- INVALID WORLDVIEW
			}
			
			else{
				potentialZoomLevel *= Math.min(MAXIMUM_ZOOM_FACTOR, 95000000/((potentialZoomLevel*worldView.height)*(potentialZoomLevel*worldView.width)));
			}
	
			trace("potential zoom level",potentialZoomLevel);
			Application.instance.zoomLevel = potentialZoomLevel;
			updateProjectZoom();
			// take the stored world center in worldView
			// coordinates and translate them to 
			// global coordinates and then back to 
			// worldContainer parent's new coordinate
			// system that has been scaled
			centerWorld = worldView.localToGlobal(centerWorld);
			centerWorld = worldContainer.parent.globalToLocal(centerWorld);
			// take the stored mouse center in worldView
			// coordinates and translate them to 
			// global coordinates and then back to 
			// worldContainer parent's new coordinate
			// system that has been scaled
			mouse = worldView.localToGlobal(mouse);
			mouse = worldContainer.parent.globalToLocal(mouse);
			// calculate the offset and apply it
			worldContainer.centerOffset = centerWorld.subtract(mouse);
		}
		
		public function handleZoomInWithoutAdjust(event:ApplicationEvent = null):void
		{
			var worldView:WorldView = WorldView(authorController.world.getView());
			var potentialZoomLevel:Number = Application.instance.zoomLevel * (1.0 / ZOOM_STEP);
			// enforce max zoom
			
			if(worldView.height > VALID_DIMENSION || worldView.width > VALID_DIMENSION){
				potentialZoomLevel *= 1;  //NO CHANGE -- INVALID WORLDVIEW
			}
				
			else{
				potentialZoomLevel *= Math.min(MAXIMUM_ZOOM_FACTOR, 95000000/((potentialZoomLevel*worldView.height)*(potentialZoomLevel*worldView.width)));
			}
			
			Application.instance.zoomLevel = potentialZoomLevel;
			updateProjectZoom();
		}
		
		public function handleZoomInRect(rectangle:Rectangle,event:ApplicationEvent = null):void
		{
			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			var world:World = authorController.world;
			var worldView:WorldView = WorldView(authorController.world.getView());
			var centerWorld:Point;
			var rectCenter:Point = new Point(rectangle.left+(rectangle.width/2),rectangle.top+(rectangle.height/2));
			var stageHeight:Number = EditorUI.instance.leftControlBar.height;
			var stageWidth:Number = EditorUI.instance.topControlBar.width - (EditorUI.CONTROL_AREA_WIDTH * 2);
			var potentialZoomLevel:Number;
			// grab center of the world and 
			// store a reference to it in 
			// worldView coordinates becase
			// that coordinate system doesnt
			// change when worldContainer is
			// scaled.
			centerWorld = worldContainer.localToGlobal(new Point(world.width/2,world.height/2));
			centerWorld = worldView.globalToLocal(centerWorld);
			// store a reference to center of
			// marquee in worldView coordinates
			// because that coordinate system
			// doesnt change during scaling
			rectCenter = worldView.globalToLocal(rectCenter);
			// scale the worldContainer based
			// on the height or width of the
			// marquee in relation to the height
			// or width of the stage
			if(rectangle.height>=rectangle.width)
			{
				// vertically orientated or equal
				potentialZoomLevel = Application.instance.zoomLevel * (stageHeight / rectangle.height);
			}else{
				// horizontally oriented
				potentialZoomLevel = Application.instance.zoomLevel * (stageWidth / rectangle.width);
			}
			// enforce max zoom
			
			if(worldView.height > VALID_DIMENSION || worldView.width > VALID_DIMENSION){
				potentialZoomLevel *= .5;  //INVALID WORLDVIEW
			}
				
			else{
				potentialZoomLevel *= Math.min(.5, 95000000/((potentialZoomLevel*worldView.height)*(potentialZoomLevel*worldView.width)));
			}
			
			Application.instance.zoomLevel = potentialZoomLevel;
			// apply the zoom to the worldContainer
			updateProjectZoom();
			// take the stored world center in worldView
			// coordinates and translate them to 
			// global coordinates and then back to 
			// worldContainer parent's new coordinate
			// system that has been scaled
			centerWorld = worldView.localToGlobal(centerWorld);
			centerWorld = worldContainer.parent.globalToLocal(centerWorld);
			// take the stored marquee center in worldView
			// coordinates and translate them to 
			// global coordinates and then back to 
			// worldContainer parent's new coordinate
			// system that has been scaled
			rectCenter = worldView.localToGlobal(rectCenter);
			rectCenter = worldContainer.parent.globalToLocal(rectCenter);
			// calculate the offset and apply it
			worldContainer.centerOffset = centerWorld.subtract(rectCenter);
		}
		
		public function handleZoomOut(event:ApplicationEvent = null):void
		{
			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			var worldView:WorldView = WorldView(authorController.world.getView());
			var world:World = authorController.world;
			var centerWorld:Point;
			var mouse:Point = new Point(worldContainer.stage.mouseX, worldContainer.stage.mouseY);
			var potentialZoomLevel:Number;
			// grab center of the world and 
			// store a reference to it in 
			// worldView coordinates becase
			// that coordinate system doesnt
			// change when worldContainer is
			// scaled.
			centerWorld = worldContainer.localToGlobal(new Point(world.width/2,world.height/2));
			centerWorld = worldView.globalToLocal(centerWorld);
			// store a reference to center of
			// mouse in worldView coordinates
			// because that coordinate system
			// doesnt change during scaling
			mouse = worldView.globalToLocal(mouse);
			// apply the scale to worldContainer
			potentialZoomLevel = Application.instance.zoomLevel * ZOOM_STEP;
			Application.instance.zoomLevel = (potentialZoomLevel >=.05)?potentialZoomLevel:.05;
			updateProjectZoom();
			// take the stored world center in worldView
			// coordinates and translate them to 
			// global coordinates and then back to 
			// worldContainer parent's new coordinate
			// system that has been scaled
			centerWorld = worldView.localToGlobal(centerWorld);
			centerWorld = worldContainer.parent.globalToLocal(centerWorld);
			// take the stored mouse center in worldView
			// coordinates and translate them to 
			// global coordinates and then back to 
			// worldContainer parent's new coordinate
			// system that has been scaled
			mouse = worldView.localToGlobal(mouse);
			mouse = worldContainer.parent.globalToLocal(mouse);
			// calculate the offset and apply it
			worldContainer.centerOffset = centerWorld.subtract(mouse);
		}
		
		public function handleZoomOutWithoutAdjust(event:ApplicationEvent = null):void
		{
			var potentialZoomLevel:Number = Application.instance.zoomLevel * ZOOM_STEP;
			Application.instance.zoomLevel = (potentialZoomLevel >=.05)?potentialZoomLevel:.05;
			updateProjectZoom();
		}
		
		public function handleSetViewScale(newScale:Number):void
		{
			Application.instance.zoomLevel = newScale;
			updateProjectZoom();
		}
		
		public function handleSetViewFitEverything(event:ApplicationEvent = null):void
		{
			var world:World = authorController.world;
			var stageWidth:Number = EditorUI.instance.topControlBar.width - (EditorUI.CONTROL_AREA_WIDTH * 2);
			var stageHeight:Number = EditorUI.instance.leftControlBar.height;
			
			// Fit the _documentView.width into the editor's width
			var scaleFactor:Number = stageWidth / (world.width + (WireboardView.WIDTH * 2));
			
			// Make sure the scaleFactor is enough to fit the _documentView.height into the editor's height
			var newDocContainerHeight:Number = scaleFactor * world.height;
			if (newDocContainerHeight > stageHeight) {
				// Fit the _documentView.height into the editor's height
				scaleFactor = stageHeight / world.height;
			}
			
			// Fill.
			Application.instance.zoomLevel = scaleFactor;
			updateProjectZoom();
		}
		
		private function updateProjectZoom():void
		{
			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			worldContainer.scaleX = Application.instance.zoomLevel;
			worldContainer.scaleY = Application.instance.zoomLevel;
			trace("ZoomLevel::"+Application.instance.zoomLevel);
			dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SCALE_CHANGED));
		}
		
		// Diagram Window
		
		public function get diagramWindowController():DiagramWindowController
		{
			if (!_diagramWindowController) {
				/*if (Application.instance.currentEditor != null) {
				// TODO: alter this so the diagram window always takes on proper size
				_diagramWindowController = new DiagramWindowController(new Rectangle(10, 10, 800, 700));
				_diagramWindowController.closeCallback = handleDiagramWindowClose;
				}*/
			}
			return _diagramWindowController;
		}
		
		private function handleDiagramWindowClose(event:ApplicationEvent = null):void 
		{
			handleSetWindowsSingle();
		}
		
		public function get diagramWindowVisible():Boolean
		{
			return EditorUI.instance && EditorUI.instance.diagramContainer.stage != null;
		}
		
		public function set diagramWindowVisible(show:Boolean):void
		{
			if (show == false)
				handleDiagramWindowClose();
			else
				handleSetWindowsStacked();
		}
		
		public function handleSetWindowsSingle():void
		{
			// Update the model.
			Application.instance.windowArrangement = Application.SINGLE_ARRANGEMENT;
			_previousWindowArrangement = Application.SINGLE_ARRANGEMENT;
		}
		
		public function doSetWindowsSingle():void
		{
			EditorUI.instance.projectViewMode = EditorUI.PROJECT_VIEW_PRESENTATION;
			
			setDiagramOpen(false);
		}
		
		public function handleSetWindowsStacked():void
		{	
			// Update the model.
			Application.instance.windowArrangement = Application.STACKED_ARRANGEMENT;
			_previousWindowArrangement = Application.STACKED_ARRANGEMENT;
		}
		
		public function doSetWindowsStacked():void
		{
			EditorUI.instance.projectViewMode = EditorUI.PROJECT_VIEW_DIAGRAM;
			
			if (_previousWindowArrangement < 2)
				setDiagramOpen(true);
		}
		
		public function handleSetWindowsTiled():void
		{	
			// Update the model.
			Application.instance.windowArrangement = Application.TILED_ARRANGEMENT;
			_previousWindowArrangement = Application.TILED_ARRANGEMENT;
		}
		
		public function doSetWindowsTiled():void
		{
			EditorUI.instance.projectViewMode = EditorUI.PROJECT_VIEW_SPLIT;
			
			if (_previousWindowArrangement < 2)
				setDiagramOpen(true);
		}
		
		private function setDiagramOpen(diagramOpen:Boolean):void 
		{
			var world:World = authorController.world;
			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			
			wireController.showWiresForLevel();
			if (!Application.running) {
				worldContainer.feedbackWiringView.visible = !diagramOpen;
				worldContainer.feedbackUIView.visible = !diagramOpen;
			}
			var objects:Vector.<AbstractObject> = world.allObjects();
			for each (var obj:Object in objects) {
				// Temporary property to force MC close when switching b/w diagram and presentation windows
				obj.toDiagram = diagramOpen; 
			}
			// update all wires
			wireController.requestRedrawAllWires();
		}
		
		private function toggleLibrary(event:ApplicationEvent = null):void
		{
            Application.instance.variableLibraryVisible = false;
			Application.instance.libraryVisible = !Application.instance.libraryVisible;
		}
		
        private function toggleVariableLibrary(event:ApplicationEvent = null):void
        {
            Application.instance.libraryVisible = false;
            Application.instance.variableLibraryVisible = !Application.instance.variableLibraryVisible;
        }

		private function toggleQA(event:ApplicationEvent = null):void
		{
			ApplicationController.instance.authorController.logStatistics();
			Application.instance.qaVisible = !Application.instance.qaVisible;
			togglePaletteMuting(Application.instance.qaVisible);
		}
		
		private function toggleLog(event:ApplicationEvent = null):void
		{
			Application.instance.logVisible = !Application.instance.logVisible;
			togglePaletteMuting(Application.instance.logVisible);
		}
		
		private function toggleTest(event:ApplicationEvent = null):void
		{
			Application.instance.testPanelVisible = !Application.instance.testPanelVisible;
		}
		
		private function togglePaletteMuting(paletteVisible:Boolean):void
		{
			if (paletteVisible)
				dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
			else
				dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
		}
		
		/**
		 * Request keyboard focus. 
		 * @param objectName = name for debug purposes.  Typically pass this.toString()
		 * @param closingFunction = function for closing text:  closeFunction():Boolean
		 * @return true if focus is successfully changed.  Returns false if previous holder declines to give up focus.
		 * 
		 */
		public function requestKeyboardFocus( mediator:TextEditMediator, stealFocus:Boolean=false ):Boolean
		{
			if (_authorController.currentEditingMediator == mediator)
				return true;
			
			resetKeyboardFocus(stealFocus);
			_authorController.startingEditing(mediator);
			dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
			return true;
		}
		
		/**
		 * Close keyboard focus. 
		 *	This would be called by Run, Save, Close commands...  to close the current editing text.
		 * @return true if successful ... and the Run/Save can proceed
		 * 
		 */
		public function resetKeyboardFocus(stealFocus:Boolean=false):Boolean
		{	
			// Reinstate the keyboard event listener on the application
            if (!_disabledInput) {
                dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
            }

			// TODO is this necessary?
			//_view.stage.focus = _focusLockObj as InteractiveObject;

			var mediator:TextEditMediator = _authorController.currentEditingMediator as TextEditMediator;
			if(mediator != null) {
				mediator.makeObject();
			}
			return true;
		}
		
		// This is called in response to a system window close event from WindowImplementation
		// which we treat as a quit.
		private function handleWindowClose():void 
		{
			dispatchEvent(new ApplicationEvent(NotificationNamesApplication.REQUEST_QUIT_APPLICATION));
		}
		
		private function handleQuitting(event:ApplicationEvent = null):void
		{
			_dlgTemplate = null;
			
            var lms:LMS = World(Application.instance.document.root.value).lms;
            if(lms != null) {
                lms.sendValues();
            }
			// Tell the diagram window to close.
			if (_diagramWindowController != null)
				_diagramWindowController.exit();
			if (_qaPaletteController != null)
				_qaPaletteController.exit();
			if (_logPaletteController != null)
				_logPaletteController.exit();

			// Close the window.
			if (Application.instance.window)
				Application.instance.window.exit();
		}
		
		public function handleNew(event:ApplicationEvent = null):void
		{
            resetDefaultUserMessage();
			ToolboxController.instance.handleNewToolSelection(ToolboxController.ARROW);
		}
		
		public function makeNewUntitledProject(event:ApplicationEvent = null):void
		{
			ProjectController.instance.makeNewUntitledProject();
		}
		
		public static function getSystemFonts():Array
		{
			var storedNames:Array = new Array();
			var fonts:Array = Font.enumerateFonts(false);
			fonts.sortOn("fontName", Array.CASEINSENSITIVE);
			var fontsAsStrings:Array = [];
			var n:int;
			
			var filteredFonts:Array = fonts.filter(function isNameStored(element:*, index:int, newArray:Array):Boolean {
				
				var arrayIndex:Number = 0;
				
			if(element.fontName.search(/^zebra.*/gi) > -1)  //filter out zebra fonts //restore the *
				return false;
				
				if(storedNames.length < 1){
					storedNames.push(element.fontName);
					return true;
				}
				
				while (arrayIndex < storedNames.length){
					if (storedNames[arrayIndex] == element.fontName) {
						return false;
					}
					else{
						arrayIndex++;
					}
				}
				storedNames.push(element.fontName);
				return true;
			        });

			for (n=0; n<filteredFonts.length; n++) {
				if (filteredFonts[n] as Font) {
					var font:Font = filteredFonts[n];
					fontsAsStrings.push({label:font.fontName, data:font.fontName});
				}
			}
			return fontsAsStrings;		
		}
		

		protected var _dlgTemplate:TemplateDialog;

		private function handleTemplateDialogUpdate(event:ApplicationEvent):void {
			if(_dlgTemplate)
				_dlgTemplate.validateVariables();	
		}
		
		private function handleTemplateDialog(event:ApplicationEvent = null):void
		{
            if(UserService.metaDataLoaded == false) {
                UserService.stalledApplicationEvent = event;
                // wait until we have user information.
                return;
            }
//            if(Application.instance.isSpecialFeatureWhiteList == false) {
//                //TODO this disables templates for non admins, remove once templates are desired for all.
//                handleSettingsDialog(event);
//                return;
//            }
			dispatchEvent(new ApplicationEvent(NotificationNamesEditor.DESELECT));
			_dlgTemplate = createPopUp(TemplateDialog, true) as TemplateDialog;
		}
		
		public function get authorController():AuthorController
		{
			return _authorController;
		}
		
		public function get applicationKeyController():KeyMediator
		{
			return _applicationKeyController;
		}
		
		public function get wireController():WireController
		{
			return _wireController;
		}

        public function get errorMessageController():ErrorMessagingController{
            return _errorMessagingController;
        }

		private function handleShowAboutWindow(event:ApplicationEvent = null):void
		{
			this.createPopUp(AboutDialog, true);
		}
		
		/**
		 * current project is guaranteed to close by this point, so we need to get to cleaning. 
		 * @param event
		 * 
		 */
		public function handleProjectLoading(event:ApplicationEvent = null):void
		{
			if(authorController.world == null){
                return
            }
			// Last call for the app to cleanup.
			Tweener.removeAllTweens();

			var ao:AbstractObject;
            for each (ao in authorController.world.allObjects()) {
				if (!ao || !ao.controller) continue;
				ao.controller.warnBeforeDelete(ao);
			}

			// now that everything is cleared out, clean out the model cache
			TaconiteModel.clearCaches();
		}
		
		private function handleProjectChanging(event:ApplicationEvent = null):void
		{
            var document:Document = Application.instance.document;
            wireController.bindingsDisabled = null;
			_view.initializeWithModels(document.root, document.wiringRoot);

            if (document.newDocument) {
                setupNewDocument(document);
            } else {
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_LOAD_SUCCEEDED));
            }
		}

        private function setupNewDocument(document:Document):void
        {
            document.construct(function():void{});

            PseudoThread.add(new PseudoThreadSimpleRunnable("PROJECT_INITIALIZED", function():void{
                if(Application.instance.document.project.accountTypeId < 3) {
                    AuthorController.instance.editCurrentEvent();
                }
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_INITIALIZED));
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_COMPLETE));
                resetDefaultUserMessage();
            }));
        }
		
		private var _initialStartup:Boolean = true;
		
		private function handleProjectComplete(event:ApplicationEvent = null):void
		{
			Application.instance.document.upToDate = true;
            Application.instance.lmsEnabled = (_authorController.world.lms != null);
			
			handleScaleChanged();

			if (_initialStartup) {
				_initialStartup = false;
			}
		}

        private function handleProjectUnsavedChanges(event:ApplicationEvent):void
        {
            var unsaved:Boolean = event.data as Boolean;
            if (unsaved) {
                // change the default message and show it immediately
                setDefaultUserMessage(ApplicationStrings.UNSAVED_CHANGES);
            } else {
                // change default to "Ready" but don't show it ... "Project saved" may be showing
                Application.instance.defaultUserMessage = ApplicationStrings.DEFAULT_STATUS_MESSAGE;
            }
        }
		
		public function isOverStagingArea(e:MouseEvent):Boolean
		{
			if (!_invokedAsEditor) return true;
			
			var editorUI:EditorUI = _view as EditorUI;
			
			if (editorUI.leftControlBar.hitTestPoint(e.stageX, e.stageY, true)
				|| editorUI.rightControlBar.hitTestPoint(e.stageX, e.stageY, true)
				|| editorUI.topControlBar.hitTestPoint(e.stageX, e.stageY, true)
				|| editorUI.bottomControlBar.hitTestPoint(e.stageX, e.stageY, true)
				|| (editorUI.floatingPaletteArea.hitTestPoint(e.stageX, e.stageY, true) && Application.instance.toolboxVisible)
				){
				return false;
			}
			return editorUI.leftBox.hitTestPoint(_view.stage.mouseX, _view.stage.mouseY, true);
		}
		
		public function displayToolTip(location:Point, direction:String, label:String, arrowDisplay:String = "Arrow Show"):void
		{
			if (!Application.instance.viewContext || Application.instance.viewContext.info.userActionInProgress)
				return;
				
			if (!_toolTip) {
				_toolTip = new ControlAreaToolTip();
				_view.stage.addChild(_toolTip);
				_toolTip.visible = false;
			}
			
			_toolTip.displayDirection = direction;
			_toolTip.arrowDisplay = arrowDisplay;
			_toolTip.labelString = label;

			switch (direction) {
                case ControlAreaToolTip.DISPLAY_ABOVE:
                    _toolTip.x = location.x - (_toolTip.containerWidth / 2);
                    _toolTip.y = location.y - _toolTip.containerHeight - 14;
                    break;
                case ControlAreaToolTip.DISPLAY_BELOW:
                    _toolTip.x = location.x - (_toolTip.containerWidth / 2);
                    _toolTip.y = location.y;
                    break;
                case ControlAreaToolTip.DISPLAY_LEFT:
                    _toolTip.x = location.x - _toolTip.containerWidth;
                    _toolTip.y = location.y - _toolTip.containerHeight/2;
                    break;
                default:  // ControlAreaToolTip.DISPLAY_RIGHT:
                    _toolTip.x = location.x;
                    _toolTip.y = location.y;
			}

			// constrain tooltip location to fit in window
			// use entire app space, so tooltips on inspector ribbons will be right below
			if (_toolTip.x < 0)
				_toolTip.x = 10;
			if ((_toolTip.x + _toolTip.containerWidth) > _view.width)
				_toolTip.x = _view.width - _toolTip.containerWidth - 10;
			if ((_toolTip.y + _toolTip.containerHeight) > _view.height) 
				_toolTip.y = location.y - _toolTip.containerHeight - 30;  // 30 is ribbon height.   TODO: pass in height of item getting tooltip 
			
			startToolTipTimer(true);
		}

		private function stopToolTipTimer():void
		{
			if (_tooltipTimer) {
				_tooltipTimer.stop();
				_tooltipTimer.removeEventListener(TimerEvent.TIMER, handleTimedTooltip);
				_tooltipTimer.removeEventListener(TimerEvent.TIMER, handleTimedTooltipEnd);
			}
		}

		private function startToolTipTimer(show:Boolean):void
		{
			stopToolTipTimer();
            if (_tooltipTimer == null) {
			    _tooltipTimer = new Timer(show?TOOLTIP_DELAY:5000,1);
            } else {
                _tooltipTimer.delay = (show?TOOLTIP_DELAY:5000);
                _tooltipTimer.reset();
            }
			_tooltipTimer.addEventListener(TimerEvent.TIMER, show?handleTimedTooltip:handleTimedTooltipEnd);
			_tooltipTimer.start();
		}
		
		private function handleTimedTooltip(event:Event):void
		{
			if(_toolTip) {
				_toolTip.visible = true;
				startToolTipTimer(false);
			}
		}

		private function handleTimedTooltipEnd(event:Event):void
		{
			hideToolTip();
		}
		
		public function hideToolTip():void
		{
			if(_toolTip) {
				stopToolTipTimer();
				_toolTip.visible = false;
			}
		}

        public function showTooltipNow():void
        {
             handleTimedTooltip(null);
        }
		
		public function toggleStats():void
		{
			// intended to be a self-contained method since this is an internal-only, non-production feature.
			var statsFound:Boolean = false;
			for (var i:int = 0 ; i < _view.rawChildren.numChildren ; i++) {
				if (_view.rawChildren.getChildAt(i) as Stats) {
					statsFound = true;
					_view.rawChildren.removeChildAt(i);
				}
			}
			if (!statsFound)
				_view.rawChildren.addChild(new com.alleni.author.view.ui.controls.Stats(_view));
		}

		override public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void
		{
			_notificationController.addEventListener(type, listener, useCapture, priority, useWeakReference);
			super.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}

		override public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void
		{
			_notificationController.removeEventListener(type, listener, useCapture);
			super.removeEventListener(type, listener, useCapture);
		}

		public static function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void
		{
			instance.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}

		public static function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void
		{
			instance.removeEventListener(type, listener, useCapture);
		}

		public static function get currentActionTree():ActionTree
		{
			return instance.authorController.currentActionTree;
		}

		public static function getParentActionTree(object:AbstractObject):ActionTree
		{
			for(var parent:AbstractObject = object.parent; parent != null; parent = parent.parent) {
				if(parent is Composite) {
					return Composite(parent).actionTree;
				}
			}
			return Application.instance.document.actionTree;
		}

		public static function cut(e:Event):void
		{
			instance.authorController.currentClipboardMediator.cut(e);
            LibraryController.instance.updateForAssetChange();
		}

		public static function copy(e:Event):void
		{
			instance.authorController.currentClipboardMediator.copy(e);
		}

		public static function paste(e:Event):void
		{
			instance.authorController.currentClipboardMediator.paste(e);
		}
//
//        public function handleHideRightClickMenu(event:Event):void{
//            if(_rightClickMenu != null){
//                if(EditorUI.instance.rawChildren.contains(_rightClickMenu)){
//                    EditorUI.instance.rawChildren.removeChild(_rightClickMenu);
//                }
//                EditorUI.instance.removeEventListener(MouseEvent.CLICK, handleHideRightClickMenu)
//            }
//        }
//
//        public function handleRightClick(event:ApplicationEvent):void{
//            var rightClickedViews:Vector.<ObjectView> = ApplicationUI.instance.presentationContainer.worldView.findAllViewsUnderPoint(new Point((event.data as Point).x, (event.data as Point).y));
//            handleHideRightClickMenu(null);
//            _rightClickMenu = new RightClickMenuView(rightClickedViews);
//            _rightClickMenu.x = (event.data as Point).x;
//            _rightClickMenu.y = (event.data as Point).y;
//            EditorUI.instance.rawChildren.addChild(_rightClickMenu);
//            EditorUI.instance.addEventListener(MouseEvent.CLICK, handleHideRightClickMenu);
//        }
//
        public function get invokedAsEditor():Boolean{
            return _invokedAsEditor;
        }

        private function handleOpenWiki(event:ApplicationEvent):void{
            var urlRequest:URLRequest = new URLRequest("http://help.zebrazapps.com");
            navigateToURL(urlRequest, '_blank');
        }
	}
}
