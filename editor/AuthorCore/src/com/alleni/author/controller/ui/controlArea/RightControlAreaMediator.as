package com.alleni.author.controller.ui.controlArea
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
	import com.alleni.author.view.ui.controlArea.RightControlArea;
	import com.alleni.author.view.ui.controls.Button;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class RightControlAreaMediator extends ControlAreaMediator
	{	
		private var _view:RightControlArea;
		
		function RightControlAreaMediator():void
		{
			ApplicationController.addEventListener(NotificationNamesApplication.SHOW_INSPECTOR, handleShowInspector);
			ApplicationController.addEventListener(NotificationNamesApplication.HIDE_INSPECTOR, handleHideInspector);
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_INSPECTOR, handleToggleInspector);
			ApplicationController.addEventListener(NotificationNamesApplication.SHOW_RIBBON_LIBRARY, handleShowRibbonLibrary);
			ApplicationController.addEventListener(NotificationNamesApplication.HIDE_RIBBON_LIBRARY, handleHideRibbonLibrary);
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_RIBBON_LIBRARY, handleToggleRibbonLibrary);
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_DOCK, handleToggleDock);		
		}
		
		public function handleViewEvents(view:RightControlArea):void
		{
			_view = view;
			
			_view.inspectorButton.addEventListener(Button.CLICK, buttonClicked);
			_view.inspectorButton.addEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.inspectorButton.addEventListener(MouseEvent.ROLL_OUT, hideTip);
			
			_view.ribbonLibraryButton.addEventListener(Button.CLICK, buttonClicked);
			_view.ribbonLibraryButton.addEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.ribbonLibraryButton.addEventListener(MouseEvent.ROLL_OUT, hideTip);
			
			_view.dockButton.addEventListener(Button.CLICK, buttonClicked);
			_view.dockButton.addEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.dockButton.addEventListener(MouseEvent.ROLL_OUT, hideTip);
		}
		
		public function clearViewEvents():void
		{
			_view.inspectorButton.removeEventListener(Button.CLICK, buttonClicked);
			_view.inspectorButton.removeEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.inspectorButton.removeEventListener(MouseEvent.ROLL_OUT, hideTip);

			_view.ribbonLibraryButton.removeEventListener(Button.CLICK, buttonClicked);
			_view.ribbonLibraryButton.removeEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.ribbonLibraryButton.removeEventListener(MouseEvent.ROLL_OUT, hideTip);

			_view.dockButton.removeEventListener(Button.CLICK, buttonClicked);
			_view.dockButton.removeEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.dockButton.removeEventListener(MouseEvent.ROLL_OUT, hideTip);
		}
		
		private function handleShowInspector(event:ApplicationEvent = null):void
		{
			setButton(_view.inspectorButton, true);
			setButton(_view.ribbonLibraryButton,false);
		}
		
		private function handleHideInspector(event:ApplicationEvent = null):void
		{
			setButton(_view.inspectorButton, false);
		}
		
		private function handleShowRibbonLibrary(event:ApplicationEvent = null):void
		{
			setButton(_view.ribbonLibraryButton, true);
			setButton(_view.inspectorButton,false)
		}
		
		private function handleHideRibbonLibrary(event:ApplicationEvent = null):void
		{
			setButton(_view.ribbonLibraryButton, false);
		}
		
		private function handleToggleInspector(event:ApplicationEvent = null):void
		{
			toggleButton(_view.inspectorButton);
			if (_view.inspectorButton.selected)
				setButton(_view.ribbonLibraryButton,false);
		}
		
		private function handleToggleRibbonLibrary(event:ApplicationEvent = null):void
		{
			toggleButton(_view.ribbonLibraryButton);
			if (_view.inspectorButton.selected)
				setButton(_view.inspectorButton,false);
		}
		
		private function handleToggleDock(event:ApplicationEvent = null):void
		{
			setButton(_view.dockButton, Application.instance.dockVisible);
		}
		
		private function showTip(event:Event):void
		{
			var button:Button = event.currentTarget as Button;
			var point:Point = new Point(-25, button.height/2);
			ApplicationController.instance.displayToolTip(button.localToGlobal(point), ControlAreaToolTip.DISPLAY_LEFT, button.toolTip);
		}
		
		private function buttonClicked(event:Event):void
		{
			var id:String = event.currentTarget.id;
			switch(id){
				case "ribbonLibraryButton":
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_RIBBON_LIBRARY));
					break;
				case "inspectorButton":
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_INSPECTOR));
					break;
				case "dockButton":
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_DOCK));
					break;
			}
		}
	}
}