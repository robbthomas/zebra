/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 10/8/12
 * Time: 10:42 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.ui {
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;

public class CompositeSaveControllerDefaultContext implements CompositeSaveControllerContext {
    private var _world:World = Application.instance.document.root.value as World;
  	private var _authorController:AuthorController = ApplicationController.instance.authorController;


    public function findByUID(uid:String):CompositeSaveControllerTarget {
        return _world.findObjByUID(uid) as CompositeSaveControllerTarget;
    }

    public function selectOuterGadget(outerClosingVellum:CompositeSaveControllerTarget):void {
        // when Event is kept open, select the gadget so MC will show
        var composite:Composite = outerClosingVellum as Composite;
        if (composite && composite.hasParent) {   // empty gadget would be deleted and therefore parent=null
            _authorController.selectSingleModel(composite.model);
        } else {
            _authorController.clearSelection();
        }
    }
    public function get actionTree():ActionTree {
        return _authorController.currentActionTree;
    }

    public function findInstances(gadget:Project, predicate:Function):Vector.<CompositeSaveControllerTarget>
    {
        var list:Vector.<CompositeSaveControllerTarget> = new Vector.<CompositeSaveControllerTarget>();
        if (gadget) {
            var ep:EventPage = _authorController.eventFlow.editingEventPage;
            if (ep) {
                addObjectsToList(gadget, predicate, ep, list);
            } else {  // global backstage
                for each (var obj:AbstractObject in _authorController.world.eventFlow.objects) {
                    addObjectsToList(gadget, predicate, obj, list);
                }
            }
        }
        return list;
    }

    private function addObjectsToList(gadget:Project, predicate:Function, container:AbstractObject, list:Vector.<CompositeSaveControllerTarget>):void
    {
        var objects:Vector.<AbstractObject> =  container.allObjects();
        for each (var obj:AbstractObject in objects) {
            var composite:CompositeSaveControllerTarget = obj as CompositeSaveControllerTarget;
            if (composite && composite.project == gadget && predicate(composite)) {
                list.push(composite);
            }
        }
    }
}
}
