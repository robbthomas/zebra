package com.alleni.author.controller.ui
{
    import com.alleni.author.Navigation.SmartObject;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.definition.action.CustomRibbonAction;
	import com.alleni.author.event.WireEvent;
	import com.alleni.author.model.ui.ExternalSmartWireAnchor;
	import com.alleni.author.model.ui.InternalSmartWireAnchor;
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
    import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.author.view.ui.WireboardButton;
	import com.alleni.author.view.ui.WireboardView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class WireboardMediator
	{
		private var _view:WireboardView;
		private var _context:ViewContext;
		private var _smart:SmartObject;
		private var _hilitedRibbons:Vector.<RibbonView> = new Vector.<RibbonView>(0);
		private var _clickPoint:Point;
		private var _newIntAnchor:InternalSmartWireAnchor;
		
		
		public function WireboardMediator(context:ViewContext, view:WireboardView, smart:SmartObject)
		{
			_view = view;
			_context = context;
			_smart = smart;
			
			_view.addEventListener(MouseEvent.ROLL_OVER, rollListener);
		}
				
		public function handleEventsForButton(button:Sprite):void
		{
			button.addEventListener(MouseEvent.MOUSE_DOWN, buttonMouseDownListener);
		}
				
		private function rollListener(event:MouseEvent):void
		{
			_view.addEventListener(MouseEvent.MOUSE_MOVE, moveListener);
			_view.addEventListener(MouseEvent.ROLL_OUT, rolloutListener);
			var local:Point = _view.globalToLocal(new Point(event.stageX, event.stageY));
			_view.hilitePoint(local);
			hiliteCorrespondingRibbons(local,true);
		}
		
		private function moveListener(event:MouseEvent):void
		{
			var local:Point = _view.globalToLocal(new Point(event.stageX, event.stageY));
			_view.hilitePoint(local);
			hiliteCorrespondingRibbons(local,true);
		}
		
		private function rolloutListener(event:MouseEvent):void
		{
			_view.removeEventListener(MouseEvent.MOUSE_MOVE, moveListener);
			_view.removeEventListener(MouseEvent.ROLL_OUT, rolloutListener);
			_view.removeHilite();
			removeHilite();
		}
		
		
		private function numCustomRibbons(smart:SmartObject):int
		{
			var count:int = 0;
			for each (var anchor:WireAnchor in smart.innerAnchors) {
				++count;
			}
			return count;
		}
		
		private function buttonMouseDownListener(event:MouseEvent):void
		{
			if (event.currentTarget as WireboardButton) {
				event.stopImmediatePropagation();
				var button:WireboardButton = event.currentTarget as WireboardButton;
				_clickPoint = new Point(event.stageX, event.stageY);

                var color:int;
                if(button.type == WireboardView.OUTLET) {
                    color = WireCreateUtil.OUTLET;
                } else if(button.type == WireboardView.INLET) {
                    color = WireCreateUtil.INLET;
                } else {
                    color = WireCreateUtil.PROP;
                }

                var outer:ExternalSmartWireAnchor = WireCreateUtil.makeRawExternalAnchor(_smart, color, WorldContainer(_context).role);
                _newIntAnchor = outer.other;
                _newIntAnchor.portSide = _view.side;
				
				_view.stage.addEventListener(MouseEvent.MOUSE_MOVE, buttonMouseMoveListener);
				_view.stage.addEventListener(MouseEvent.MOUSE_UP, buttonMouseUpListener);
			}
		}
		
		private function buttonMouseMoveListener(event:MouseEvent):void
		{
			var point:Point = new Point(event.stageX, event.stageY);
			if (point.subtract(_clickPoint).length >= 3) {
				_newIntAnchor.model.dispatchEvent(new WireEvent(WireEvent.DRAG_NEW_WIRE,null,false,null,ViewRoles.PRIMARY));
				_view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, buttonMouseMoveListener);
				_view.stage.removeEventListener(MouseEvent.MOUSE_UP, buttonMouseUpListener);
				_newIntAnchor = null;
			}
		}
		
		private function buttonMouseUpListener(event:MouseEvent):void
		{
			_view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, buttonMouseMoveListener);
			_view.stage.removeEventListener(MouseEvent.MOUSE_UP, buttonMouseUpListener);
			_newIntAnchor = null;
		}

        //TODO REMOVE
        [Deprecated]
		public function convertToCustomAnchor(incomplete:InternalSmartWireAnchor, fromAnchor:WireAnchor):PortView
		{
			// convert from an imcomplete inner anchor (not having an ext anchor) to a full custom anchor
			trace("WireboardMediator:convertToCustomAnchor: incomplete="+incomplete,"from="+fromAnchor);
			// if both anchors are properties, clone the fromAnchor to get its data type
			// otherwise, clone the incomplete to get its color (inlet, outlet, prop)
			var cloneThis:WireAnchor = incomplete;
			if (fromAnchor.modifierDescription is PropertyDescription && incomplete.modifierDescription is PropertyDescription)
				cloneThis = fromAnchor;
			var extAnchor:ExternalSmartWireAnchor = _smart.addCustomAnchor(cloneThis);
            if(!extAnchor) {
                return null;
            }
			ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(extAnchor, true));
			var intAnchor:InternalSmartWireAnchor = extAnchor.other;
			intAnchor.portSide = incomplete.portSide;
			_smart.removeInnerAnchor(incomplete);
			return _view.createPort(intAnchor);
		}
				
		private function hiliteCorrespondingRibbons(local:Point, hilite:Boolean):void
		{
			removeHilite();
			if (hilite) {
				var intAnchor:InternalSmartWireAnchor = _view.pointToAnchor(local);
				if (intAnchor) {
					// hilite custom ribbon on gadget MC
					var extAnchor:ExternalSmartWireAnchor = intAnchor.other;
					if (extAnchor) {
						hiliteRibbon(extAnchor);
						// hilite wired ribbons on gadget children
						var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(intAnchor);
						for each (var wire:Wire in wires) {
							var other:WireAnchor = (wire.masterAnchor == intAnchor) ? wire.slaveAnchor : wire.masterAnchor;
							hiliteRibbon(other);
						}
					}
				}
			} 
		}
		
		private function hiliteRibbon(anchor:WireAnchor,tooltip:Boolean = false):void
		{
			var views:Vector.<PortView> = anchor.getViews(_context as WorldContainer);
			for each (var view:PortView in views) {
				if (view.parent is RibbonView) {
					var ribbonView:RibbonView = RibbonView(view.parent);
					ribbonView.roll(true,tooltip);
					_hilitedRibbons.push(ribbonView);
					break;
				}
			}
		}
		
		private function removeHilite():void
		{
			for each (var ribbon:RibbonView in _hilitedRibbons) {
				ribbon.rolled = false;
			}
			_hilitedRibbons.splice(0,_hilitedRibbons.length);
		}
	}
}