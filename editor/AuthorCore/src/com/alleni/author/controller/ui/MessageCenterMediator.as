package com.alleni.author.controller.ui
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.definition.DoubleClickAction;
	import com.alleni.author.definition.InletDescription;
	import com.alleni.author.definition.OutletDescription;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.action.ActionTree;
	import com.alleni.author.definition.action.ModifyObjectProperty;
	import com.alleni.author.definition.action.UserAction;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.event.WireEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.InternalSmartWireAnchor;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.author.view.ui.MessageCenterView;
	import com.alleni.taconite.controller.ITaconiteController;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class MessageCenterMediator extends AbstractObjectDragMediator
	{
		public static const SNAP_DISTANCE:Number = 20.0;
		
		private var _object:AbstractObject;
		private var _oldX:Number;
		private var _oldY:Number;
		private var _titleField:RichTextField;
		private var _textEditMediator:TextEditMediator;
		private var _oldTitle:String;
        private var _objectView:ObjectView;
		
		
		public function MessageCenterMediator(context:ViewContext)
		{
			super(context);		
		}
		
		/**
		 * When asked to work with a ObjectView, take note of the view and add a listener for mouseDown.
		 */
		override public function handleViewEvents(view:ITaconiteView, role:uint):AbstractObjectDragMediator
		{
			_view = MessageCenterView(view);
			_view.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			view.model.addEventListener(WireEvent.COMPLETE, wireCompleteListener);
			view.model.addEventListener(WireEvent.DETACH, wireCompleteListener);
			_object = view.model.value as AbstractObject;
            _objectView = MessageCenterView(view).objectView;
			return this;
		}
		
		
		public function handleTitleEvents(title:RichTextField):void
		{
			_titleField = title;
			_oldTitle = title.text;
			_textEditMediator = new TextEditMediator();
			_textEditMediator.maxChars = 25;
			_textEditMediator.handleTextEditEvents(title,
				function(e:MouseEvent):int{return DoubleClickAction.IGNORE}, // MessageCenterMediator will open editing on mouseUp
				null, // update
				TextEditMediator.closeOnEnter,  // keypress handler
				handleCloseTextEditing);
		}
		
		private function handleCloseTextEditing(value:String):void
		{
			value = FormattedText.convertToString(value);
			
			var obj:AbstractObject = _view.model.value as AbstractObject;
			if (value != obj.title) {
                obj.titleChangedByAuthor = true;
				obj.title = value;

				// log Undo action
				var action:UserAction = ModifyObjectProperty.fromObject(obj, "Retitle object", "title", -1, _oldTitle);
				ApplicationController.currentActionTree.commit(action);

				_oldTitle = value;
			}
		}



		/**
		 * add the stock ribbons to the object from the storage per-class
		 * this only checks by length of the arrays. ideally, we will need to check
		 * all ribbons against what they should/could be to make sure the initial
		 * list is up-to-date. 
		 * @param view
		 * 
		 */
		// TODO: 	The MessageCenterDiagramMediator class now also handles Gadget movement
		//			in the diagram window. This inherited code is unnecessary for gadgets and should
		//			be placed in a message center-only mediator
		public function handleRibbonEvents(view:MessageCenterView):void
		{
			var object:AbstractObject = AbstractObject(view.model.value);
			
			object.inletRibbons.removeAll();
			object.outletRibbons.removeAll();
			object.propertyRibbons.removeAll();
			
			for each(var anchorObj:Object in object.anchors) {
				var anchor:WireAnchor = anchorObj as WireAnchor;
				if(!anchor) {
					continue;
				}
				if(anchor.modifierDescription as InletDescription) {
					object.inletRibbons.addItem(anchor);
				} else if(anchor.modifierDescription as OutletDescription) {
					object.outletRibbons.addItem(anchor);
				} else if(anchor.modifierDescription as PropertyDescription) {
					object.propertyRibbons.addItem(anchor);
				}
			}
		}
		
		override protected function get documentDragDelta():Point
		{
			var point:Point = new Point(WorldContainer(_view.context).mouseX, WorldContainer(_view.context).mouseY);
			return point.subtract(_dragPoint);
		}
		
		override public function handleMouseDown(event:MouseEvent):void
		{
            trace("MC mouseDown",_view);
            event.stopPropagation();
            _dragStarted = false;
            var context:DisplayObjectContainer = _view.context;
            _dragPoint = new Point(context.mouseX, context.mouseY);
            _context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
            _context.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.STAGE_MOUSE_DOWN));
        }
		
				
		override protected function handleMouseMove(e:MouseEvent):void
		{
			var context:DisplayObjectContainer = _view.context;
			if (!_dragStarted && !(object is EventPage)
				&& e.buttonDown
				&& _dragPoint != null
				&& (Math.abs(context.mouseX - _dragPoint.x) >= minimumDrag
					|| Math.abs(context.mouseY - _dragPoint.y) >= minimumDrag))
			{
				_dragStarted = true;
				handleDragStart(e);
				ApplicationController.instance.resetKeyboardFocus(true);  // close editing of MC title
			}
			
			if (_dragStarted)
				handleDragMove(e);
			if(_stopPropagation)
				e.stopPropagation();
		}
		
		override protected function handleMouseUp(e:MouseEvent):void
		{
			if (_dragStarted) {
				handleDragMove(e);
				handleDragEnd(e);
			} else {
				handleClick(e);
			}
			
			_context.stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_context.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			
			if(_stopPropagation)
				e.stopPropagation();
			
			// force focus back to the editor
			//_context.editor.setFocus();
		}
		
		/**
		 * At the start of a drag, capture the positions of all selected objects so that we
		 * can move them all by the same delta later on.
		 */
		override protected function handleDragStart(e:MouseEvent):void
		{			
			Application.instance.draggingMessageCenter = true;

            (_view as MessageCenterView).dragStarted();

			_oldX = _view.x + MessageCenterView.WIDTH/2;
			_oldY = _view.y - MessageCenterView.OBJECT_PADDING;
			var oldPoint:Point = new Point(_oldX, _oldY);
			
			var controller:ITaconiteController = context.controller;
			if (!controller.selection.contains(_view.model) && _view.okToSelect) {
				controller.selectSingleModel(_view.model);
			}
			_oldPositions = [oldPoint];
		}
		
		/**
		 * For each move during the drag, position the models appropriately given the drag distance
		 * from the starting point.
		 */
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
		{
			var obj:AbstractObject = _object;
			var objView:ObjectView = _objectView;
            var location:Point = Point(_oldPositions[0]).add(documentDragDelta);
			if (objView) {
				objView.undockMessageCenter(location);
				
				if (objView.messageCenterNearDockPosition())
					objView.dockMessageCenter();

			} else {
                location.x -= MessageCenterView.WIDTH/2;
                location.y += MessageCenterView.OBJECT_PADDING;
                location = _view.parent.localToGlobal(location);
				object.xMessageCenter = location.x;
				object.yMessageCenter = location.y;
            }
			_view.stage.invalidate();  // trigger the render in ApplicationController, so MC position will update
		}
		
		
		override protected function handleDragEnd(e:MouseEvent) : void
		{
			// just finished dragging the MC
			super.handleDragEnd(e);
			Application.instance.draggingMessageCenter = false;
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_CURSOR));
		}
		
		override protected function handleClick(e:MouseEvent):void
		{
			// clicked MC but didn't drag:  open title for editing
			if (_titleField.hitTestPoint(e.stageX, e.stageY)){
                if(MessageCenterView(_view).titleEditable) {
				    _textEditMediator.openEditing();
                }
			}

            //Bring to Front
            ApplicationController.instance.handleMessageCenterBringToFront(_view as  MessageCenterView);
		}
		
		protected function wireCompleteListener(e:WireEvent):void 
		{
//			trace("MessageCenterMediator::wireCompleteListener",e);
			// avoid closing MC when wire to edge of gadget, since we automatically show the created custom ribbon
			if (e.wire.masterAnchor as InternalSmartWireAnchor || e.wire.slaveAnchor as InternalSmartWireAnchor)
				return;
			
			// let the destination message center collapse its ribbons and redraw wires
			if (e.type == WireEvent.COMPLETE)
				(_view as MessageCenterView).finishWireDrag();
		}
				
		override public function clearViewEvents():void
		{
			_view.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			_view.model.removeEventListener(WireEvent.COMPLETE, wireCompleteListener);
			_view.model.removeEventListener(WireEvent.DETACH, wireCompleteListener);
		}
	}
}
