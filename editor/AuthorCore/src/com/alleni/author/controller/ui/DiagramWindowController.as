package com.alleni.author.controller.ui
{
import com.alleni.author.model.World;
import com.alleni.author.model.World;
	import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.controller.ui.SecondaryWindowController;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.EditorEvent;
	import com.alleni.taconite.event.ModelUpdateEvent;
	
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	import mx.events.PropertyChangeEvent;
	import mx.events.ResizeEvent;

	public class DiagramWindowController extends SecondaryWindowController
	{
		private var _wireController:WireController;
		
		public function DiagramWindowController(bounds:Rectangle)
		{
			super(bounds, "Diagram");
			_wireController = ApplicationController.instance.wireController;
		}
		
		public function recalcWires(toDiagram:Boolean):void
		{	
			trace("\nDiagramWindowController::recalcWires toDiagram="+toDiagram);
			
			// make sure all wires are showing, when open diagram
			// (tbd:  this overlaps work of next function)
			_wireController.showWiresForLevel();
			
			// Show or hide wiring/message centers in presentation window (if we are not running)
			/*if (!(Application.instance.documentData.root.value as World).running)
				editor.showWiringAndMessageCenters(!toDiagram);*/
			
			// close message centers by sending "toDiagram" change
			// (the ContainerController will propagate this change to Arena child objects)
			var objects:Array = World(Application.instance.document.root.value).objects.toArray();
			for each (var obj:Object in objects) {
				// Temporary property to force MC close when switching b/w diagram and presentation windows
				obj.toDiagram = toDiagram; 
			}
			
			// update all wires
			_wireController.requestRedrawAllWires();
			
			if (!toDiagram)
				setVisible(false);
		}
		
		public function handleRun():void
		{
			_window.sendToBack();
		}
		
		override protected function handleWindowResize(e:ResizeEvent):void
		{
			trace("DIAGRAM RESIZE: " + _window.width, _window.height);
			if (_window != null) {
				if (Application.instance.document != null) {
					var world:World = Application.instance.document.root.value as World;
					
					world.diagramWidth = _window.width;
					world.diagramHeight = _window.height;
				}
			}
		}
		
		override protected function handleWindowComplete(e:Event):void
		{
			super.handleWindowComplete(e);
			Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
		}
		
		private function handleApplicationPropertyChange(event:PropertyChangeEvent):void
		{
			var propertyName:String = event.property as String;
			Utilities.assert(propertyName != null);
		}
		
//		private function addEditorListeners(editor:Editor):void
//		{	
//			// Add an event listener on the Editor.
//			editor.addEventListener(EditorEvent.DOCUMENT_CHANGED, handleEditorDocumentChanged);
//		}
//		
//		private function removeEditorListeners(editor:Editor):void
//		{
//			// Remove our listener on the Editor.
//			editor.removeEventListener(EditorEvent.DOCUMENT_CHANGED, handleEditorDocumentChanged);
//		}
//		
//		/**
//		 * Respond to property changes on the Editor.
//		 */
//		private function handleEditorDocumentChanged(event:EditorEvent=null):void
//		{	
//			/*Application.instance.document = Application.instance.document;
//			_editor.configurationService = Application.instance.configurationService;
//			
//			// Let WireController
//			WireController.instance.diagramStage = _editor;*/
//		}
	}
}
