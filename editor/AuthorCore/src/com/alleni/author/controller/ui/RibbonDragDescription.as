/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 3/28/11
 * Time: 12:25 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.ui {
import com.alleni.author.view.ui.RibbonView;

public class RibbonDragDescription extends DragDescription {
	private var _fromRibbonView:RibbonView;

	public function RibbonDragDescription(fromRibbonView:RibbonView) {
        super(fromRibbonView.context);
		_fromRibbonView = fromRibbonView;
	}

	public function get fromRibbonView():RibbonView {
		return _fromRibbonView;
	}

	override public function containerShouldOpen(container:String):Boolean {
		return false;
	}
}
}
