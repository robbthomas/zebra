/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.controller.ui
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.PagerController;
import com.alleni.author.application.dialogs.GadgetReplaceAllDialog;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.definition.GadgetType;
import com.alleni.author.definition.action.ActionGroup;
import com.alleni.author.definition.action.LibraryAddAction;
import com.alleni.author.definition.action.LibraryRemoveAction;
import com.alleni.author.definition.action.UserAction;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.project.Project;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadAsyncRunnable;
import com.alleni.author.util.thread.PseudoThreadContext;
import com.alleni.author.util.thread.PseudoThreadDebugRunnable;
import com.alleni.author.util.thread.PseudoThreadRunnable;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;

import flash.utils.Dictionary;

import mx.collections.ArrayCollection;

/**
	 * A save-controller is created each time a gadget vellum is closed, or for closing multiple vellums at once.
	 * The controller exists only a short time until the closing & saving is done.
	 * But it does exist during the replace-all alert and actually presents the alert.
	 * 
	 * Carryover data:
	 * _allReplacements:  every composite replacement (inside or outside) done by this class, since app startup.
	 * 
	 * Outside replacement:
	 * When editing a nested gadget that has multiple uses, on closing you get replace-all dialog.
	 * Instances outside the outer vellum are considered "outside replacements" and go into _replacementsOutside.
	 * 
	 * The _replacementsOutside data is copied to the vellum kept open, so that undo can be supported at each
	 * level, including the world level after all vellums have been closed.  Without this, undo of a simple
	 * drag-object before editing the gadget would crash, if that outside object were replaced and therefore got
	 * a different uid.
	 * 
	 */
	public class CompositeSaveController
	{

        private var _context:CompositeSaveControllerContext;
        private var _onBegin:Vector.<PseudoThreadRunnable>;
        private var _onComplete:Vector.<PseudoThreadRunnable>;
        private var _onCancel:Vector.<PseudoThreadRunnable>;
        private var _saveChanges:Boolean;

		private var _keepList:Vector.<CompositeSaveControllerTarget> = new Vector.<CompositeSaveControllerTarget>();		// composites to keep open
		private var _keepOpen:CompositeSaveControllerTarget;											// top (innermost) vellum being kept open
		private var _closeList:Vector.<CompositeSaveControllerTarget> = new Vector.<CompositeSaveControllerTarget>();		// composites to be closed
		private var _changedList:Vector.<CompositeSaveControllerTarget> = new Vector.<CompositeSaveControllerTarget>();		// changed gadgets (subset of _closeList)
		private var _outerClosingVellum:CompositeSaveControllerTarget;									// outermost vellum being closed
		private var _instances:Dictionary = new Dictionary();						// key=SmartObject, value=Vector.<SmartObject>
		private var _replacementsOutside:Dictionary = new Dictionary();				// key=oldUID, value=destroyAction:  replacements outside of _keep
        private var _gadgetReplacements:Dictionary = new Dictionary();
		private var _totalOtherInstances:int = 0;
		private static var _allReplacements:Dictionary = new Dictionary();			// key=oldUID, val=newUID  All replacements since app startup. (inside or outside)
		
		
		public function CompositeSaveController(context:CompositeSaveControllerContext)
		{
            _context = context;
		}

        public function saveEditWhileRunChanges(eventPage:EventPage):void
        {
            var oldGadget:Project = eventPage.gadget;
            var newGadget:Project = GadgetController.instance.saveForSmartObject(eventPage);
            swapGadgetWithoutReplacement(eventPage, newGadget);

            // update library: no undoAction, since undo of editWhileRunning will simply create a new revision when this function is called at projectSave or eventUnload
            GadgetController.instance.addToModel(newGadget);
            if (!gadgetStillUsedAnywhere(oldGadget)) {
                GadgetController.instance.removeFromModel(oldGadget);
            }
        }
		
		public function closeComposites(inner:CompositeSaveControllerTarget, keepOpen:CompositeSaveControllerTarget, saveChanges:Boolean, onBegin:Vector.<PseudoThreadRunnable>, onComplete:Vector.<PseudoThreadRunnable>, onCancel:Vector.<PseudoThreadRunnable> = null):void
		{
			if (inner == keepOpen) {  // null range, so do nothing
				if (onComplete != null) {
					PseudoThread.addAll(onComplete);
				}
				return;
			}

            _onBegin = onBegin;
			_onComplete = onComplete;
            _onCancel = onCancel;
            _saveChanges = saveChanges;
			collectAffectedComposites(inner, keepOpen);
//			dump();

            var threadContext:PseudoThreadContext = PseudoThread.instance.context;
            var needsStarted:Boolean = threadContext == null;
            if(needsStarted) {
                threadContext = new PseudoThreadContext();
            }
            if (_totalOtherInstances > 0 && _saveChanges) {
                threadContext.add(new PseudoThreadAsyncRunnable("CompositeSaveController::closeComposites showReplaceAllDialog", function(signalCompletion:Function):void {
				    var dialogCompleteCallback:Function = function(button:String):void{
                        dialogCompletion(button, threadContext);
                        signalCompletion();
                    };
                    GadgetReplaceAllDialog.showReplaceAllDialog(_totalOtherInstances, dialogCompleteCallback, new ArrayCollection());
                }));
			} else if (!_saveChanges) {
                dialogCompletion(GadgetReplaceAllDialog.DISCARD_CHANGES_BUTTON, threadContext);
            } else {
				dialogCompletion(GadgetReplaceAllDialog.REPLACE_THIS_BUTTON, threadContext);
			}
            if(needsStarted) {
                PseudoThread.instance.start(threadContext);
            }
		}
		
		private function dialogCompletion(button:String, threadContext:PseudoThreadContext):void
		{
            var startRunnable:PseudoThreadDebugRunnable = new PseudoThreadDebugRunnable("CompositeSaveController::dialogCompletion " + button, true);
            threadContext.add(startRunnable);
            /**
             * TweenBackFromEdit needs to start immediately and go smoothly to completion, before actually closing the gadgets
             * and unloading the events, saving, replacing the composites/events, etc.
             * The "onBegin" operation will have the tween in it.
             */
            if (button == GadgetReplaceAllDialog.CONTINUE_EDITING_BUTTON) {
                if (_onCancel != null) {
                    PseudoThread.addAll(_onCancel);
                }
                return;
            }

            _context.selectOuterGadget(_outerClosingVellum);


            if (_onBegin) {
                threadContext.addAll(_onBegin);  // probably tweenBackFromEdit
                _onBegin = null;
            }

			switch (button) {
				case GadgetReplaceAllDialog.DISCARD_CHANGES_BUTTON:
                    threadContext.add(new PseudoThreadSimpleRunnable("discardChanges", function():void{
                        discardChanges();
                    }));
					break;
				case GadgetReplaceAllDialog.REPLACE_THIS_BUTTON:
                    threadContext.add(new PseudoThreadSimpleRunnable("closeVellums+saveClosedComposites", function():void{
                        closeVellums();            // closing editing triggers lazy unload
                        saveClosedComposites(false);  // replaceAll = false         // WARNING: this adds more to the queue
                    }));
					break;
				case GadgetReplaceAllDialog.REPLACE_ALL_BUTTON:
                    threadContext.add(new PseudoThreadSimpleRunnable("closeVellums+saveClosedComposites", function():void{
                        closeVellums();
                        saveClosedComposites(true);
                    }));
			}

			if (_onComplete != null) {
                threadContext.addAll(_onComplete);
            }
            threadContext.add(startRunnable.end);
		}
		
		private function collectAffectedComposites(inner:CompositeSaveControllerTarget, keepOpen:CompositeSaveControllerTarget):void
		{
			// to keep open
			_keepOpen = keepOpen;  // top level vellum being kept open
			var composite:CompositeSaveControllerTarget;
			if (keepOpen) {
				composite = keepOpen;
				do {
					_keepList.push(composite);
					composite = composite.savableParent;
				} while (composite != null);
			}
			
			// to be closed
			composite = inner;
			var changesMade:Boolean = false;
            while (composite != null && !inList(composite, _keepList)) {
				_closeList.push(composite);
				_outerClosingVellum = composite;
				changesMade = (changesMade || composite.hasChanges); // after hit a changed one, mark rest changed
				if (changesMade)
					_changedList.push(composite);
				composite = composite.savableParent;
            }

			// _instances
			for each (composite in _changedList) {
				if (composite.canReplaceOtherCopies) {  // not for EventPage, so duplicated page won't give replace-all dialog
					var instances:Vector.<CompositeSaveControllerTarget> = _context.findInstances(composite.project, okToReplace);
					_instances[composite] = instances;
					_totalOtherInstances += instances.length;
				}
			}
		}
		
		private function closeVellums():void
		{
			for each (var smart:CompositeSaveControllerTarget in _closeList) {
                smart.closeVellum(_changedList.indexOf(smart) >= 0);
			}
		}
		
		private function saveClosedComposites(replaceAll:Boolean):void
		{
            var group:PseudoThreadDebugRunnable = new PseudoThreadDebugRunnable("CompositeSaveController::saveClosedComposites " + replaceAll, true);
            PseudoThread.add(group);
			// action to delete the outer composite being closed
			var deletionAction:UserAction = _outerClosingVellum.actionsDestroyOriginal;

			// save each changed composite, and all instances
			for each (var composite:CompositeSaveControllerTarget in _closeList) {
                var groupInner:PseudoThreadDebugRunnable = new PseudoThreadDebugRunnable("for each iteration", true);
                PseudoThread.add(groupInner);
                PseudoThread.add(new PseudoThreadSimpleRunnable("Saving smart object [" + composite.title + "]", function(composite:CompositeSaveControllerTarget):void {
                    assert(composite.hasParent, "saveClosedComposite composite.hasParent");

                    if (_changedList.indexOf(composite) < 0) {
                        // no real changes, so no need to save
                        trace("saveClosedComposites: revert:", composite);
                    } else if (!composite.shouldBeDestroyed) {  // changes need to be saved
                        trace("saveClosedComposites: save:", composite);
                        var newGadget:Project = composite.save();
                        var values:Object = composite.getCurrentValues();
                        swapGadgetWithoutReplacement(composite, newGadget);

                        if (replaceAll) {
                            replaceOtherInstances(composite, newGadget, values);
                        }
                    } else if (composite.actionsDestroyOriginal) {  // destroy empty gadget
                        trace("saveClosedComposites: delete:", composite);
                        composite.actionsDestroyOriginal.perform();
                        _context.actionTree.commit(composite.actionsDestroyOriginal);
                    }
                }, null, [composite]));
                PseudoThread.add(groupInner.end);
			}

            PseudoThread.add(new PseudoThreadSimpleRunnable("CompositeSaveController::saveClosedComposites finish", function():void {
                // log undo actions for outermost saved gadget
                var newComposite:CompositeSaveControllerTarget = _outerClosingVellum;
                if (newComposite && _changedList.length > 0) {
                    if (deletionAction && newComposite.hasParent) {
                        var updateLibrary:UserAction = createLibraryUpdateAction(newComposite.project);
                        updateLibrary.perform();
                        var title:String = (newComposite as Composite) ? "Edit-Gadget" : "Edit-Event"
                        var token:Object = _context.actionTree.openGroup(title);
                        _context.actionTree.commit(deletionAction);
                        _context.actionTree.commit(newComposite.makeCreateAction());
                        _context.actionTree.commit(createActionsReplacingOutside());
                        _context.actionTree.commit(updateLibrary);
                        _context.actionTree.closeGroup(token);
                    }
                }
                LibraryController.instance.updateForAssetChange();  // in case destroyed empty gadget or replaced instance of gadget used in other event
            }));
            PseudoThread.add(group.end);
		}

        private function swapGadgetWithoutReplacement(existing:CompositeSaveControllerTarget, newGadget:Project):CompositeSaveControllerTarget
        {
            assert(existing.hasParent, "replaceComposite existing.hasParent");
            assert(!inList(existing, _keepList), "replaceComposite !inList(existing,_keepList)");
            if (existing.project) {
                _gadgetReplacements[existing.project] = newGadget;
            }

            existing.updateProject(newGadget);
            PseudoThread.add(new PseudoThreadSimpleRunnable("CompositeSaveController::finishReplaceComposite", finishReplaceComposite, this, [existing,  existing]));
            return existing;
        }
		
		private function replaceComposite(existing:CompositeSaveControllerTarget, newGadget:Project, values:Object):CompositeSaveControllerTarget
		{
			assert(existing.hasParent, "replaceComposite existing.parent");
            if (_keepList) {
                assert(!inList(existing, _keepList), "replaceComposite !inList(existing,_keepList)");
            }
            if (existing.project) {
                _gadgetReplacements[existing.project] = newGadget;
            }

            if(existing is EventPage) {
                EventFlow(EventPage(existing).parent).ignorePageListChanges = true;
            }
            var newObj:CompositeSaveControllerTarget = existing.recreateWithProject(newGadget, values);

            PseudoThread.add(new PseudoThreadSimpleRunnable("CompositeSaveController::finishReplaceComposite", finishReplaceComposite, this, [newObj, existing]))

			return newObj;
		}

        private function finishReplaceComposite(newObj:CompositeSaveControllerTarget, existing:CompositeSaveControllerTarget):void
        {
            if(newObj != existing) {
                ReplacementController.performObjectReplacement(AbstractObject(newObj), AbstractObject(existing));
                if(_allReplacements) {
                    _allReplacements[existing.uid] = newObj.uid;
                }  
            }
            var ep:EventPage = newObj as EventPage;
            if(ep) {
                var flow:EventFlow = ep.parent as EventFlow;
                flow.ignorePageListChanges = false;

                // when EventPage was encoded its pageNumber was N+1 since it was added after the original page
                // now we fix the number so the titles won't be messed up
                var index:int = flow.pages.getItemIndex(ep);
                if (index >= 0) {
                    ep.pageNumber = index +1;
                    ep.initialValues.pageNumber = index +1;
                }
                ep.initialValues.title = ep.title;  // this doesn't really matter since title getter computes title unless changed by author
                if (ep.preventUnload) {
                    ep.hasInternals = true;
                    ep.preventUnload = false;
                }
            }
            if(existing == _outerClosingVellum) {
                _outerClosingVellum = newObj;
                PagerController.instance.activateObject(AbstractObject(newObj), true);
            }
            LibraryController.instance.updateForAssetChange();
        }
						
		/**
		 * The outermost container of "composite" or the composite itself.
		 * Stops before the world and before the keep-open vellum. 
		 * @param composite
		 * @return 
		 * 
		 */
		private function outerContainerOf(composite:CompositeSaveControllerTarget):CompositeSaveControllerTarget
		{
			while (true) {
				var container:CompositeSaveControllerTarget = composite.savableParent;
				if (container == null || inList(container, _keepList)) 
					break;
				composite = container;
			}
			return composite;
		}
		
		private function replaceOtherInstances(oldComposite:CompositeSaveControllerTarget, newGadget:Project, values:Object):void
		{
			var instances:Vector.<CompositeSaveControllerTarget> = _instances[oldComposite];
			if (instances) {
				var containers:Vector.<CompositeSaveControllerTarget> = new Vector.<CompositeSaveControllerTarget>();
				for each (var oldInstance:CompositeSaveControllerTarget in instances) {
					if (oldInstance != oldComposite) {
						var instance:CompositeSaveControllerTarget = findReplaced(oldInstance.uid, true);
						if (instance && instance.hasParent) {
							var outer:CompositeSaveControllerTarget = addToContainersList(instance, containers);
							if (outer) {
								trace("replaceOtherInstances OUTSIDE: outer="+outer.title);
								_replacementsOutside[outer.uid] = outer.makeDestroyAction();
							}
							replaceComposite(instance, newGadget, values);
						}
					}
				}
				
				// save containers of nested instances
				containers.sort(compareForSaveComposites);
				var gadgets:Dictionary = new Dictionary();  // key=oldGadget, val=newGadget
				for each (var old:CompositeSaveControllerTarget in containers) {
					var composite:CompositeSaveControllerTarget = findReplaced(old.uid, true);
					if (composite) {
						var oldGadget:Project = composite.project;
						var gadget:Project = gadgets[oldGadget];  // use one already encoded, so we don't orphan multiple instances of gadgets
						if (gadget == null) {
							gadget = composite.save();
							gadgets[oldGadget] = gadget;
						}
						swapGadgetWithoutReplacement(composite, gadget);
					}
				}
			}
		}
				
		private function addToContainersList(composite:CompositeSaveControllerTarget, containers:Vector.<CompositeSaveControllerTarget>):CompositeSaveControllerTarget
		{
			// add all the containers of this instance
			var result:CompositeSaveControllerTarget = composite;
			if (composite) {
				var container:CompositeSaveControllerTarget = composite.savableParent;
				while (container && !inList(container, containers) && !inList(container, _keepList) && !inList(container, _closeList)) {
					containers.push(container);
					result = container;
					container = container.savableParent;
				}
			}
			return result;  // outermost containing composite, or the composite itself (on backstage)
		}
		
		private function mergeOutsideReplacements(additions:Dictionary, list:Dictionary):void
		{
			for (var uid:String in additions) {
				list[uid] = additions[uid];  // value = DestroyAction
			}
		}
		
		private function createActionsReplacingOutside():ActionGroup
		{
			// build list of outer replaced composites
			trace("createActionsReplacingOutside _replacementsOutside.len="+dictionaryLength(_replacementsOutside));
			var list:Vector.<CompositeSaveControllerTarget> = new Vector.<CompositeSaveControllerTarget>();
			var destroyActions:Dictionary = new Dictionary();  // key=composite, value=DestroyObjectAction
			for (var uid:String in _replacementsOutside) {
				var obj:CompositeSaveControllerTarget = findReplaced(uid);
				trace("   uid="+uid, "obj="+obj);
				if (obj) {
					destroyActions[obj] = _replacementsOutside[uid];  // destroyActions is indexed by the current live object
					if (!inList(obj, _keepList)) {
						var outer:CompositeSaveControllerTarget = outerContainerOf(obj);  // may return obj itself
						if (outer && list.indexOf(outer) < 0) {
							list.push(outer);
						}
					}
				}
			}
			
			trace("createActionsReplacingOutside list.len="+list.length);
			for each (var cc:CompositeSaveControllerTarget in list) {
				trace("  cc="+cc);
			}
			
			var group:ActionGroup = new ActionGroup("actionsReplacingOutside");
			for each (var composite:CompositeSaveControllerTarget in list) {
				if (destroyActions[composite]) {
                    group.add(destroyActions[composite]);
                    group.add(composite.makeCreateAction());
                }
			}
			return group;
		}

        private function createLibraryUpdateAction(outer:Project):UserAction
        {
            var group:ActionGroup = new ActionGroup("update gadgets in library");
            var itemsAdded:Dictionary = new Dictionary();
            for (var key:* in _gadgetReplacements) {
                var oldGadget:Project = key as Project;
                var newGadget:Project = _gadgetReplacements[key] as Project;
                if (!gadgetStillUsedAnywhere(oldGadget)) {
                    group.add(LibraryRemoveAction.fromItem(oldGadget));
                }
                group.add(LibraryAddAction.fromItem(newGadget));
                itemsAdded[newGadget] = true;
            }
            if (!(outer in itemsAdded)) {  // in  case this is a brand-new event page
                group.add(LibraryAddAction.fromItem(outer));
            }
            return group;
        }

        private function gadgetStillUsedAnywhere(gadget:Project):Boolean
        {
            var count:int = AuthorController.instance.eventFlow.getItemGlobalUsageCount(gadget);  // scan entire document
            return (count > 0);
        }

		private function discardChanges():void
		{
			// close the first changed vellum
			var composite:CompositeSaveControllerTarget;
			for each (composite in _closeList) {
                composite.discardChanges();
				if (_changedList.indexOf(composite) >= 0)
					break;
			}
		}
		
		private function inList(obj:CompositeSaveControllerTarget, list:Vector.<CompositeSaveControllerTarget>):Boolean
		{
			var target:CompositeSaveControllerTarget = findReplaced(obj.uid, true);
			for each (var item:CompositeSaveControllerTarget in list) {
				var equiv:CompositeSaveControllerTarget = findReplaced(item.uid, true);
				if (equiv == target)
					return true;
			}
			return false;
		}
		
		private function gadgetInList(gadget:Project, list:Vector.<CompositeSaveControllerTarget>):Boolean
		{
			for each (var composite:CompositeSaveControllerTarget in list) {
				if (composite.project == gadget)
					return true;
			}
			return false;
		}
		
		/**
		 * Given the uid of a composite, return the uid of the latest version of that composite.
		 * The dictionary behind this is static, so it knows about all replacements that have occurred.
		 * Thats to support undo of replacements outside the vellum. 
		 * @param uid
		 * @param allowOriginal=true if returning the original composite is ok.
		 * @return 
		 * 
		 */
		private function findReplaced(uid:String, allowOriginal:Boolean=false):CompositeSaveControllerTarget
		{
			var equiv:String = _allReplacements[uid];
			if (equiv == null)
				return allowOriginal ? _context.findByUID(uid) as CompositeSaveControllerTarget : null;
			var latest:String = uid;
			while (true) {  // loop on newer versions
				equiv = _allReplacements[latest];
				if (equiv)
					latest = equiv;
				else
					break;
			}
			var obj:CompositeSaveControllerTarget = _context.findByUID(latest);
			return obj;
		}

		private function okToReplace(composite:CompositeSaveControllerTarget):Boolean
		{
//            trace("CompositeSaveController:okToReplace?", composite);
			if (!GadgetController.instance.confirmCanPublish(composite.project)) {
//				trace("REJECT can't publish",composite);
				return false;
			}

			if (inList(composite, _keepList) || inList(composite, _closeList)) {
//				trace("REJECT keep="+inList(composite, _keepList), "close="+inList(composite, _closeList), "path="+objPath(composite));
				return false;
			}

			// don't replace a nested gadget inside another instance of a gadget being kept open, else the one being kept open will be orphaned from it
			// But if the nested gadget is a vellum being edited, then its ok to replace
			var container:CompositeSaveControllerTarget = composite.savableParent;
			while (container) {
				if (inList(container, _keepList) || inList(container, _closeList))
					return true;
				if (gadgetInList(container.project, _keepList)) {
//					trace("REJECT obj.gadget in keep: path="+objPath(composite), "ID="+container.gadget.projectId);
					return false;
				}
				container = container.savableParent;
			}
			return true;
		}
				
		private function compareForSaveComposites(one:CompositeSaveControllerTarget, two:CompositeSaveControllerTarget):int
		{
			var oneObj:CompositeSaveControllerTarget = findReplaced(one.uid, true);
			var twoObj:CompositeSaveControllerTarget = findReplaced(two.uid, true);
			assert(oneObj.savableParent && twoObj.savableParent, "compareForSaveComposites: oneObj.parent twoObj.parent");
			
			return savableParentDepth(two) - savableParentDepth(one);
		}

        private function savableParentDepth(target:CompositeSaveControllerTarget):int {
            var result:int = 0;
            while(target.savableParent != null) {
                result++;
                target = target.savableParent;
            }
            return result;
        }
		
		private function dictionaryLength(dict:Dictionary):int
		{
			var count:int = 0;
			for each (var thing:* in dict) {
				++count;
			}
			return count;
		}
		
		
		private function assert(condition:*, message:String):void
		{
			if (condition==null || condition==false) {
                dump();
				throw new Error("CompositeSaveController "+message);
            }
		}
		
		private function objPath(obj:CompositeSaveControllerTarget):String
		{
			if (obj is World)
				return "";
			else if (obj.savableParent == null)
				return "DELETED:"+obj.title;
			else
				return objPath(obj.savableParent) + ":" + obj.title;
		}
		
		private function dump():void
		{
			trace("CompositeSaveController dump");
			var ao:AbstractObject;
			var comp:CompositeSaveControllerTarget;
			trace("  _keepList: count="+_keepList.length); 			for each (ao in _keepList) trace("    ", ao);
			trace("  _closeList: count="+_closeList.length); 		for each (ao in _closeList) trace("    ", ao);
			trace("  _changedList: count="+_changedList.length); 	for each (ao in _changedList) trace("    ", ao);
			trace("  _equiv:"); 									for each (var uid:String in _allReplacements) trace("    old="+uid, "new="+_allReplacements[uid]);
			trace("  _instanceList:");
				for each (comp in _closeList) {
					var instances:Vector.<CompositeSaveControllerTarget> = _instances[comp];
					if (instances) {
						trace("     Closing",comp.title, "instance count="+instances.length);
						for each (var ci:CompositeSaveControllerTarget in instances) trace("           ",ci.title, "path="+objPath(ci));
					}
				}

			trace("_totalOtherInstances="+_totalOtherInstances);
		}
	}
}
