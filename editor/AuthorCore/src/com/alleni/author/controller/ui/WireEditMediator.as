package com.alleni.author.controller.ui
{
import com.alleni.author.controller.AuthorController;
import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.action.DestroyWireAction;
	import com.alleni.author.event.WireEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.author.view.ui.WireView;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.lang.Geometry;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class WireEditMediator extends WireDragMediator
	{
		private var _wireView:WireView;
		
		
		public function WireEditMediator(context:ViewContext)
		{
			super(context);
		}

      	/**
         * When asked to work with a ObjectView, take note of the view and add a listener for mouseDown.
         */
		public function handleWireViewEvents(view:WireView, role:uint):void
        {
			_role = role;
			_wireView = view;
	    	_wire = _wireView.model.value as Wire;
            if (_wire.illegal) return;
            _wireView.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
        }

        public function removeMouseListeners(view:WireView):void {
            view.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
        }
		
		override protected function beginWireDrag():Boolean
		{
			// previous drag should have been cleaned up
			Utilities.assert(Application.instance.wireDragDescription == null);
			
			// mouse click location in content coordinates
			var click:Point = new Point(WorldContainer(_context).mouseX, WorldContainer(_context).mouseY);
			ApplicationController.instance.authorController.log("WireEditMediator::beginWireDrag: click="+click+" - beginWireDrag line 55");
            trace();

			// Prevent deleting wire when clicking its end point (over the ports) or if one of the objects is locked
			var obj1:AbstractObject = _wire.masterAnchor.hostObject;
			var obj2:AbstractObject = _wire.slaveAnchor.hostObject;
            if(obj1.selected == false){
                AuthorController.instance.selectSingleModel(obj1.model, true);
            }
            if(obj2.selected == false){
                AuthorController.instance.selectSingleModel(obj2.model, true);
            }
			var objFixedEnd:AbstractObject;
			var anchorFixedEnd:WireAnchor;

			var unattachedAnchor:WireAnchor;
			var unattachedMaster:Boolean;
			
			const HALF:Number = PortView.WIDTH/2;
			var masterEnd:Boolean = Geometry.distanceBetweenPoints(click.x,click.y,_wire.x,_wire.y) < HALF;
			var slaveEnd:Boolean = Geometry.distanceBetweenPoints(click.x,click.y,_wire.x2,_wire.y2) < HALF;
			if (obj1.wiringLocked || obj2.wiringLocked) {
				return false;
			} else if (masterEnd || slaveEnd) {   // prevent dragging existing wire inside port
				trace(" clicked wire-end in a port");
				requestDragNewWire(_wire.masterAnchor);  // author clicked inside port, so create a new wire instead of editing a wire
				return false;  // don't drag the existing wire
			} else {
                // these "effective" anchors are for special chains of wires that hold together, e.g. wires to the project message center in creator projects.
                var effectiveMaster:WireAnchor = ApplicationController.instance.wireController.fixAnchor(_wire.masterAnchor);
                var effectiveSlave:WireAnchor = ApplicationController.instance.wireController.fixAnchor(_wire.slaveAnchor);
                var effectiveNewAnchor:WireAnchor;
				// drag the end nearest to the mouse click
				if (Geometry.distanceBetweenPoints(_wire.x, _wire.y, click.x, click.y) >
					Geometry.distanceBetweenPoints(_wire.x2, _wire.y2, click.x, click.y)) {
					// drag the slave end
					unattachedAnchor = _wire.slaveAnchor;
					unattachedMaster = false;
					_wire.unattachEnd(unattachedMaster);
					objFixedEnd = obj1; // master end is fixed
					anchorFixedEnd = _wire.masterAnchor;
				}
				else {
					// drag the master end
					unattachedAnchor = _wire.masterAnchor;
					unattachedMaster = true;
					_wire.unattachEnd(unattachedMaster);
					objFixedEnd = obj2;  // slave end is fixed
					anchorFixedEnd = _wire.slaveAnchor;
				}

                if(effectiveSlave != _wire.slaveAnchor || effectiveMaster != _wire.masterAnchor) {
                    if (unattachedMaster) {
                        requestDragNewWire(effectiveSlave);
                    } else {
                        requestDragNewWire(effectiveMaster);
                    }
                    WireCreateUtil.unCreate(_wire);
                } else {
                    _wire.unattachEnd(unattachedMaster);
                }
				_wire.visible = true;
				
				// I thought we would need to unregister, re:  WireController.instance.registerWire()
				// but registerWire only sets fields in wire, it doesn't build a database of wires.
			}
			
			// we are really going to drag it
			// setup wireDragDescription in editor
			var model:TaconiteModel = objFixedEnd.model;
			Utilities.assert(model != null);
			var description:WireDragDescription = new WireDragDescription(model, anchorFixedEnd, WorldContainer(_context));  // info about the stationary end
			description.oldAnchor = unattachedAnchor;
			description.oldAnchorWasMaster = unattachedMaster;
			Application.instance.wireDragDescription = description;

			// initially put the wire-end at the click location
			_wire.x2 = click.x;
			_wire.y2 = click.y;
			
			// request update of the wire (will use x2,y2 to decide nearest port on the master obj)
			_applicationController.wireController.requestRedrawWire(_wire);

			return true;  // ok to drag wire
		}
		
		/**
		 * The user clicked on an existing wire-end in a port, which should NOT edit the existing wire...
		 * but it should start dragging a new wire.  But the mouseDown event is not going to bubble to the port
		 * because the wire is not a child of the port.
		 * So we dispatch a custom event on the anchor model, which WireDragMediator listens for.
		 * 
		 */
		private function requestDragNewWire(anchor:WireAnchor):void
		{
			var model:TaconiteModel = anchor.model;

            _context.stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
            _context.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			model.dispatchEvent(new WireEvent(WireEvent.DRAG_NEW_WIRE, null, false, null, _role));  // the WireDragMediator is listening for this, to start dragging new wire
		}

    	override protected function handleClick(e:MouseEvent):void
		{
			trace("Wire edit mediator, click...");
			cancelWire();
		}

        override protected function releaseWire():void {
            return; //Don't null out the wire reference;
        }
		
		override public function handleMouseDown(e:MouseEvent):void
		{
			super.handleMouseDown(e);
			ApplicationController.instance.authorController.log("WireEditMediator line 170");
			_dragStarted = beginWireDrag(); // force wire drag start so wire disconnects and redraws on mouse down.
		}
	}
}