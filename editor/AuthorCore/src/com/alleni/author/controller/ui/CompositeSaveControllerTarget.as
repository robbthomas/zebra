/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 9/26/12
 * Time: 3:45 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.ui {
import com.alleni.author.definition.action.UserAction;
import com.alleni.author.model.project.Project;

public interface CompositeSaveControllerTarget {
    function get savableParent():CompositeSaveControllerTarget;
    function get uid():String;
    function get project():Project;
    function updateProject(newProject:Project):void;
    function recreateWithProject(newProject:Project, values:Object):CompositeSaveControllerTarget;
    function get canReplaceOtherCopies():Boolean;
    function get hasChanges():Boolean;
    function get shouldBeDestroyed():Boolean;
    function get title():String;
    function get hasParent():Boolean;
    function save():Project;
    function get actionsDestroyOriginal():UserAction;
    function set actionsDestroyOriginal(action:UserAction):void;
    function makeDestroyAction():UserAction;
    function makeCreateAction():UserAction;
    function closeVellum(saving:Boolean);
    function discardChanges();
    function getCurrentValues():Object;
}
}
