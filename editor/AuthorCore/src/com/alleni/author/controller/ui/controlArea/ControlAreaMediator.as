package com.alleni.author.controller.ui.controlArea
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.model.World;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ui.controls.Button;
	
	import flash.events.Event;

	public class ControlAreaMediator
	{	
		protected function hideTip(event:Event):void
		{
			ApplicationController.instance.hideToolTip();
		}
		
		protected function setButton(button:Button, selected:Boolean):void
		{
			if (!button) return;
			button.selected = selected;
		}
		
		protected function toggleButton(button:Button):void
		{
			if (!button) return;
			button.selected = !button.selected;
		}
	}
}
