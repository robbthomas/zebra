package com.alleni.author.controller.ui.palettes
{
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.FindModel;
import com.alleni.author.view.FindView;
import com.alleni.author.view.ui.components.PopupMenu;
import com.alleni.taconite.controller.ui.SecondaryWindowController;
import com.alleni.taconite.factory.ISystemClipboard;
import com.alleni.taconite.factory.TaconiteFactory;

import flash.events.Event;
import flash.geom.Rectangle;

import mx.core.UIComponent;

import mx.events.ResizeEvent;

public class FindWindowController extends SecondaryWindowController
	{
		public static var instance:FindWindowController = new FindWindowController();
		
		private var _view:FindView;

		public function FindWindowController()
		{
			var ui:EditorUI = EditorUI.instance;
			var bounds:Rectangle = ui.canvasVisibleRect(ui.stage);
			const wd:Number = 240;
			const ht:Number = bounds.height * 2/3;
			var rect:Rectangle = new Rectangle(bounds.right - wd, bounds.bottom - ht -28, wd, ht);

			super(rect, "FIND", true, true);  // resizable=false, utility=true
			_window.isPopup = true;
			_window.addEventListener(Event.CLOSE, handleWindowClose);
		}
		
		public function findByTitle():void
		{
			Application.instance.findVisible = true;
			_view.model.viewStackPage = FindModel.BY_TITLE_PAGE;
		}
		
		public function findByRibbon():void
		{
			Application.instance.findVisible = true;
			_view.model.viewStackPage = FindModel.BY_RIBBON_PAGE;
		}
		
		public function findByUID():void
		{
			Application.instance.findVisible = true;
			_view.model.viewStackPage = FindModel.BY_ID_PAGE;
			
			var clipboard:ISystemClipboard = TaconiteFactory.getClipboardImplementation(null);
			var uid:String = clipboard.getData(clipboard.TEXT_FORMAT) as String;
			if (uid != null && uid.length != 0) {
				_view.model.targetUID = uid;
				trace("uid.len="+uid.length);
			}
		}
		
		public function initializeFindWindow():void
		{
			_view = new FindView();
			_view.model = new FindModel();
			_window.addElement(_view);
			new FindMediator().handleViewEvents(_view);

			_view.updateView();

            var holder:UIComponent = new UIComponent();
            _window.addElement(holder);
            _view.menuHolder = holder;

		}
		
		override public function setVisible(value:Boolean):void
		{
			super.setVisible(value);
			if (value && !_view) {
				initializeFindWindow();
			} else {
				_view.updateView();
			}
		}
		
		private function handleWindowClose(event:Event):void
		{
			Application.instance.findVisible = false;
		}
		
		override public function exit():void
		{
			super.exit();
		}
		
		override protected function handleWindowResize(e:ResizeEvent):void
		{	
			trace("handleWindowResize e="+e, "width="+_window.width);
			if (_view) {
				_view.handleWindowResize();
			}
		}
		
		override protected function handleWindowComplete(e:Event):void
		{
			trace("handleWindowComplete e="+e, "width="+_window.width);
		}

		
		override protected function activate():void
		{
		}
		
		override protected function deactivate():void
		{
		}
	}
}