package com.alleni.author.controller.ui.palettes
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.dialogs.ZappPublishingDialog;
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.CompositeFeedbackMediator;
	import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.GadgetType;
	import com.alleni.author.definition.application.DockIcons;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.GadgetEvent;
	import com.alleni.author.model.gadgets.Gadgets;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Dock;
	import com.alleni.author.service.gadgets.GadgetCategoryOperation;
    import com.alleni.author.service.project.ProjectListOperation;
    import com.alleni.author.view.ui.palettes.DockView;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.event.OperationFaultEvent;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.service.IOperation;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.BlurFilter;
	import flash.geom.Matrix;
	
	import mx.events.PropertyChangeEvent;
	import mx.events.ResizeEvent;
	
	public class DockController
	{
		public static const ITEM_SPACING_X:Number = 8;
		public static const ITEM_SPACING_Y:Number = 20;
		public static const CONTAINER_PADDING:Number = 40;
		
		private static const WIDTH:Number = 800;
		private static const HEIGHT:Number = 200;
		private static const MAX_BLUR:Number = 50.0;
		private static const ANIMATION_DURATION:Number = 0.3;
		private static const ANIMATION_TRANSITION:String = "easeOutQuart";
		private static const PAGE_LENGTH:int = 34;
		
		private static var _instance:DockController;
		
		private var _model:Dock;
		private var _view:DockView;
		private var _container:DisplayObjectContainer;

        private var restoreDock:Boolean = false;
		
		private static var _pendingCategoryOperation:IOperation;
		
		
		public function DockController()
		{
			Utilities.assert(_instance == null);
			
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_DOCK, handleToggleDock, false, 10); // controllers get priority as they affect models
			ApplicationController.addEventListener(NotificationNamesApplication.SHOW_DOCK, handleShowDock);
			ApplicationController.addEventListener(NotificationNamesApplication.HIDE_DOCK, handleHideDock);
			ApplicationController.addEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED, handleRunningStateChanged, false);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOADING, handleProjectLoading);
			model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);
		}

		private function handleRunningStateChanged(event:ApplicationEvent = null):void
		{
			if(Application.running && Application.instance.dockVisible){
                handleToggleDock(null);
                restoreDock = true;
            }else if(Application.running == false && restoreDock && Application.instance.dockVisible == false){
                handleToggleDock(null);
                restoreDock = false;
            }
		}
		
		private function handleProjectLoading(event:ApplicationEvent):void
		{
			model.publishingCandidate = null; // clear out pub candidate so dialog dismisses itself and things can reset
		}
		
		public static function get instance():DockController
		{
			if (!_instance)
				_instance = new DockController();
			return _instance;
		}
		
		private function get model():Dock
		{
			if (!_model)
				_model = Dock.instance;
			return _model;
		}
		
		public function getViewIfOver(e:MouseEvent):DisplayObject
		{
			if (!_view || !_view.stage)
				return null;
			
			if (_view.hitTestPoint(e.stageX, e.stageY, true))
				return _view;
			
			return null;
		}
		
		private function handlePropertyChange(event:PropertyChangeEvent):void
		{
			switch(event.property) {
				case "lastSuccessfullyPublished":
					if (event.newValue == null // a publish operation may be is in progress. no sense in updating the list until we know whether it worked or not.
						|| !model.pageList)  // the page list is null. the dock is not yet open.
						break;

					var gadgets:Gadgets = model.pageList;
					if (gadgets) {
						var gadget:Project = gadgets.getById((event.newValue as Project).id);
						// no need to refresh list if we're already on the view we need and the gadget's already in the current page list
						if (model.selectedView == Dock.VIEW_MY_GADGETS && gadget) break;
					}
					
					model.selectedView = Dock.VIEW_MY_GADGETS;
					
					if (event.newValue === model.publishingCandidate) // our target has just been published
						hidePublishingPanels();
				case "selectedView":
				case "selectedCategoryId":
					model.pageIndex = 0;
				case "lastSuccessfullyPurchased":
				case "pageIndex":
				case "searchField":
				case "selectedSort":
					handleUpdateQuery();
					break;
				case "publishingCandidate":
					if (event.newValue == null)
						hidePublishingPanels();
				case "selectedItem":
					ensureCategoriesAreLoaded();
					break;
				default:
					break;
			}
		}
		
		private function hidePublishingPanels():void
		{
			CompositeFeedbackMediator.requestHidePublishingPanel();
			ZappPublishingDialog.requestHide();
		}
		
		private function ensureCategoriesAreLoaded():void
		{
			if (model.categories || _pendingCategoryOperation)
				return;
			
			_pendingCategoryOperation = new GadgetCategoryOperation();
			_pendingCategoryOperation.addEventListener(OperationFaultEvent.FAULT, handleOperationFault);
			_pendingCategoryOperation.addEventListener(Event.COMPLETE, handleCategoriesComplete);
			_pendingCategoryOperation.execute();
		}
		
		private function handleCategoriesComplete(e:Event):void
		{
			var operation:GadgetCategoryOperation = e.target as GadgetCategoryOperation;
			_pendingCategoryOperation = null;
			
			if (operation)
				model.categories = operation.categories;
			else
				LogService.error("DockController was unable to load category list.");
		}
		
		private function handleOperationFault(e:Event):void
		{
			if (e.target === _pendingCategoryOperation) _pendingCategoryOperation = null;
		}
		
		public function set container(value:DisplayObjectContainer):void
		{
			_container = value;
			_container.addEventListener(Event.RESIZE, handleContainerResize, false, 0, true);
		}
		
		private function handleContainerResize(event:ResizeEvent):void
		{
			if (_view)
				_view.x = (_container.width - WIDTH)/2;
		}
		
		private function handleUpdateQuery():void
		{
			const oneItemSizeX:Number = DockIcons.SIZE_SMALL + ITEM_SPACING_X;
			const oneItemSizeY:Number = DockIcons.SIZE_SMALL + ITEM_SPACING_Y;
			const itemsPerRow:int = WIDTH / oneItemSizeX;
			const maxRows:int = Math.round((HEIGHT - CONTAINER_PADDING * 1.5 + ITEM_SPACING_Y) / oneItemSizeY);
			const maxItems:int = maxRows * itemsPerRow;
			
			model.selectedItem = null;
			model.pageSize = maxItems;

            //TODO this used to ensure categories were loaded
			var listOperation:ProjectListOperation = new ProjectListOperation(true, GadgetType.GENERIC);
            listOperation.size = model.pageSize;
            listOperation.offset = model.pageSize * model.pageIndex;
            listOperation.sort = model.selectedSort;
            listOperation.inStore = model.selectedView == 0?false:true;
            listOperation.useMini = !(listOperation.inStore);
            listOperation.isForDock = true;
            listOperation.categoryId = model.selectedCategoryId;
            if (model.searchField != null && model.searchField.length > 0) listOperation.searchField = model.searchField;
			listOperation.displayName = "Loading cloud";
			listOperation.addEventListener(GadgetEvent.ERROR, handleError);
			listOperation.addEventListener(Event.COMPLETE, handleGadgetListComplete(maxItems));
			listOperation.execute();
			
			model.listInProgress = true;
		}
		
		private function handleGadgetListComplete(maxItems:int):Function
		{
			return function(e:Event):void
			{
				var listOperation:ProjectListOperation = e.target as ProjectListOperation;
				if (!listOperation)
					return;
				
//				if (model.categories == null) // has not been populated yet
//					model.categories = listOperation.categories;

				var resultGadgets:Vector.<Project> = listOperation.projects;
				if (!resultGadgets) return;

                var newPageList:Gadgets = new Gadgets();
                var numItems:int = Math.min(resultGadgets.length, maxItems);
                for(var i:int=0; i<numItems; i++) {
                    newPageList.gadgets.addItem(resultGadgets[i]);
                }

                model.pageList = newPageList;
				model.totalLength = listOperation.size;
				model.listInProgress = false;
			}
		}
		
		private function handleError(e:Event):void
		{
			model.listInProgress = false;
		}
		
		private function handleToggleDock(event:ApplicationEvent):void
		{
			if (Application.instance.dockVisible)
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.HIDE_DOCK));
			else
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SHOW_DOCK));
			
			Application.instance.dockVisible = !Application.instance.dockVisible;
		}
		
		private function handleShowDock(event:ApplicationEvent):void
		{
			if (!_view) {
				_view = new DockView(WIDTH, HEIGHT);
				_view.x = (_container.width - WIDTH)/2;
			}
			_view.visible = true;
			_view.alpha = 0;
			_view.y = EditorUI.TOP_BAR_HEIGHT;
			_view.z = -200;
			_view.cacheAsBitmap = true;
			
			var blurFilter:BlurFilter = new BlurFilter(MAX_BLUR, MAX_BLUR, 3);
			Tweener.addTween(blurFilter,{blurX:0, blurY:0, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION,onUpdate:function():void {
				_view.filters = [blurFilter];
			}});
			Tweener.addTween(_view,{z:0, alpha:1, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION,
				onComplete:function():void {
					_view.filters = [];
					if (!_view.transform.matrix3D) return;
	
					var raw:Vector.<Number> = _view.transform.matrix3D.rawData;
					var m:Matrix = new Matrix();
					m.a = raw[0];
					m.c = raw[1];
					m.tx = _view.x;
					m.b = raw[4];
					m.d = raw[5];
					m.ty = _view.y;
					_view.transform.matrix3D = null;
					_view.transform.matrix = m;
				}});
			if (!_container.contains(_view))
				_container.addChild(_view);

			if (!model.pageList)
				handleUpdateQuery();
		}
		
		private function handleHideDock(event:ApplicationEvent = null):void
		{
			if (!Application.instance.dockVisible) return;

			if (_view) {
				if (_container.contains(_view)) {
					var blurFilter:BlurFilter = new BlurFilter(0, 0, 3);
					Tweener.addTween(blurFilter,{blurX:MAX_BLUR, blurY:MAX_BLUR, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION,onUpdate:function():void {
						_view.filters = [blurFilter];
					}});
					Tweener.addTween(_view,{z:100, alpha:0, time:ANIMATION_DURATION, transition:ANIMATION_TRANSITION,
						onComplete:function():void {
							_view.visible = false; // keep it on stage to prevent dimension trouble with containers.
							_view.y = -_view.height*2; // on stage but out of the way
							//_container.removeChild(_view); 
						}});
				}
			}
		}
	}
}
