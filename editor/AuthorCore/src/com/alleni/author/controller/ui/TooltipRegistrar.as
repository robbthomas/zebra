/**
 * Created with IntelliJ IDEA.
 * User: mfitch
 * Date: 9/11/12
 * Time: 4:31 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.ui {
import flash.display.InteractiveObject;
import flash.geom.Point;
import flash.utils.Dictionary;

public class TooltipRegistrar {
    //* Use this class similar to how you'd use TooltipMediator... except with as many views as you'd like!

    private var _mediators:Dictionary;
    public function TooltipRegistrar() {
        _mediators = new Dictionary();
    }

    public function registerTooltip(view:InteractiveObject, tooltip:String, location:Point=null, direction:String=null, arrow:Boolean=false):void {
        var mediator:TooltipMediator = _mediators[view] as TooltipMediator;
        if (mediator == null) mediator = new TooltipMediator();
        _mediators[view] = mediator;

        mediator.handleTooltip(view, tooltip, location, direction, arrow);
    }

    public function takedownTooltip(view:InteractiveObject):void {
        var mediator:TooltipMediator = _mediators[view] as TooltipMediator;
        if (mediator != null) {
            mediator.removeListeners();
            delete _mediators[view];
        }
        }
    }
}
