package com.alleni.author.controller.ui
{
    import com.alleni.author.Navigation.EventPage;
    import com.alleni.author.Navigation.IPage;
    import com.alleni.author.Navigation.Pager;
    import com.alleni.author.Navigation.PagerController;
    import com.alleni.author.Navigation.SmartObject;
    import com.alleni.author.controller.AuthorController;
    import com.alleni.author.controller.objects.CompositeVelumMediator;
    import com.alleni.author.controller.objects.IReplaceableReceiver;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.action.CreateObjectAction;
	import com.alleni.author.definition.action.DestroyObjectAction;
    import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractExternalObject;
import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.Arena;
	import com.alleni.author.model.objects.ArenaPage;
	import com.alleni.author.model.objects.Composite;
    import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.EventFlowController;
import com.alleni.author.model.objects.PathObject;
	import com.alleni.author.model.objects.ToggleButton;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.CalcPropertyWireAnchor;
	import com.alleni.author.model.ui.IReplaceable;
	import com.alleni.author.model.ui.SetterWireAnchor;
	import com.alleni.author.model.ui.TriggerWireAnchor;
import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.objects.tables.AbstractTableView;
	import com.alleni.author.view.objects.tables.StateTableCell;
	import com.alleni.author.view.objects.tables.TableCell;
	import com.alleni.author.view.ui.MessageCenterView;
	import com.alleni.author.view.ui.ReplacementFeedback;
	import com.alleni.author.view.ui.RibbonView;
	import com.alleni.author.view.ui.controls.Alert;
    import com.alleni.taconite.dev.Utilities;
    import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;

	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
    import flash.utils.Dictionary;
    import flash.utils.Timer;

import mx.events.CloseEvent;
	import mx.utils.ObjectUtil;

public class ReplacementController
	{
		private static var _instance:ReplacementController;
		
		private static const FEEDBACK_DELAY:Number = 1 * 1000; // 1 second
		
		// _pendingReplacement.candidate:
		// WireAnchor - drag asset over an Asset ribbon
		//		OR drag asset over a gadget child that is wired to the edge
		//		OR drag over obj having exactly one asset of matching type (eg. Drawing or Audio)
		// AbstractObject - object with asset but no ribbon/anchor, such as a Video or Sketch
		//		OR objects with no matching asset, getting whole-object replacement, such as Oval
		// StateTableCell - drag asset over a table cell
		//
		private static var _pendingReplacement:ReplacementDescription;  
		private static var _timer:Timer;
		private static var _feedback:ReplacementFeedback;
		
		
		public function ReplacementController()
		{
			Utilities.assert(!_instance);
		}
		
		public static function get instance():ReplacementController
		{
			if (!_instance)
				_instance = new ReplacementController();
			return _instance;
		}
		
		private static function get feedback():ReplacementFeedback
		{
			if (!_feedback)
				_feedback = ReplacementFeedback.instance;
			return _feedback;
		}
		
		public function testForReplacement(e:MouseEvent):void
		{
			var rep:ReplacementDescription = replacementCandidate;
			if (rep && !rep.sameCandidate(_pendingReplacement)) {
				reset();
				_pendingReplacement = rep;
				_timer = new Timer(FEEDBACK_DELAY, 1);
				_timer.addEventListener(TimerEvent.TIMER, checkReplacementCandidate, false, 0, true);
				_timer.start();
			} else if (!rep) {
				reset();
			}
		}
		
		public function get isPending():Boolean
		{
			return ReplacementFeedback.instance.visible;
		}
		
		private static function checkReplacementCandidate(e:Event):void
		{
			var rep:ReplacementDescription = replacementCandidate;
			if (rep && rep.sameCandidate(_pendingReplacement)) {
				applyReplacementFeedback(rep.rawObject != null);
				_timer.delay = FEEDBACK_DELAY*2;
				_timer.addEventListener(TimerEvent.TIMER, function():void {
					_instance.reset();
				}, false, 0, true);
				_timer.removeEventListener(TimerEvent.TIMER, checkReplacementCandidate);
				_timer.reset();
				_timer.start();
			} else
				_instance.reset();
		}
		
		private static function get replacementCandidate():ReplacementDescription
		{
			var context:ViewContext = Application.instance.viewContext;
			var replaceable:IReplaceable = replaceableBeingDragged;
			var receiver:IReplaceableReceiver;

			var ribbonView:RibbonView = _instance.ribbonUnderPoint(new Point(context.stage.mouseX, context.stage.mouseY));
			if (ribbonView && ribbonView.ribbon.acceptsReplaceableDrop(replaceable))
				return new ReplacementDescription(ribbonView.ribbon, ribbonView);  // target the anchor
			
			var model:TaconiteModel = WorldContainer(context).modelUnderMouse;
			if (model == null)
				return null;
			var objUnderMouse:AbstractObject = model.value as AbstractObject;
			if (CompositeVelumMediator.isSpecialObject(objUnderMouse) || (objUnderMouse is SmartObject && objUnderMouse.editing))
				return null;
            if (objUnderMouse.childOfClosedMasterPage) {
                return null;
            }
            if (objUnderMouse is World) {
                return null;
            }
			
			if (objUnderMouse as AbstractTable) {  // drag to table cell
				var table:AbstractTable = AbstractTable(objUnderMouse);
				var view:AbstractTableView = table.getView(context) as AbstractTableView;
				var cell:TableCell = view.getCellUnderMouse();
				if (cell as StateTableCell && StateTableCell(cell).acceptsReplaceableDrop(replaceable))
					return new ReplacementDescription(cell as StateTableCell, cell);
			}
			
			// drag over object having asset ribbons
			var anchors:Array = findReplaceableRibbons(objUnderMouse, replaceable);
			var ribbonObj:AbstractObject = objUnderMouse;
			var candidateObj:AbstractObject = objUnderMouse;

			// for gadget child, follow wires to custom ribbons
			var accessible:AbstractObject = objUnderMouse.accessibleObject;
			if (accessible as SmartObject) {  // dragging onto a gadget child
				var composite:SmartObject = accessible as SmartObject;
				candidateObj = accessible;
				var custom:Array = null;
				if (anchors.length > 0)
					custom = composite.findCustomAnchors(anchors);
				if (custom && custom.length > 0) {
					anchors = custom;  // use the custom ribbons tied to obj under mouse
					candidateObj = objUnderMouse;  // feedback around the specific object, not whole gadget
				} else {
					anchors = findReplaceableRibbons(accessible, replaceable);  // no custom ribbons for objUnderMouse; treat gadget as ordinary obj
				}
				ribbonObj = accessible;
			}
			
			// gadget or other object having asset ribbon(s)
			if (anchors.length == 1) {
				receiver = anchors[0] as IReplaceableReceiver;
				if (receiver && receiver.acceptsReplaceableDrop(replaceable))
					return new ReplacementDescription(receiver, candidateObj.getView(context));
				else
					return null;
			} else if (anchors.length > 1) {  // multiple asset ribbons:  show the ribbons
				var drag:AssetDragDescription = Application.instance.assetDragDescription;
				if (drag) {
					drag.shownMessageCenter = ribbonObj.model;
					drag.assetAnchor = anchors[0];  // request opening this category
					return null;  // no feedback until over the ribbon itself
				}
			}
			
			// whole-object replacement:
			// cases to ignore: no feedback
			if (objUnderMouse as Arena || objUnderMouse as ArenaPage || objUnderMouse as EventFlow || objUnderMouse as EventPage)
				return null;  // allow object to drop into arena
			if (objUnderMouse.editingLocked || objUnderMouse.editing)
				return null;
			
			receiver = candidateObj as IReplaceableReceiver;
			if (!receiver || (receiver && receiver.acceptsReplaceableDrop(replaceable))) { // higher-level replaceable switch where possible
				return new ReplacementDescription(receiver, candidateObj.getView(), candidateObj);
			} else { // not directly replaceable, needs full-object replacement
				return new ReplacementDescription(null, candidateObj.getView(), candidateObj); // receiver does not accept the drop for higher-level replace. 
			}
		}
		
		private static function get replaceableBeingDragged():IReplaceable
		{
			var objectBeingDragged:Object = DragDropMediator.relatedObject;
			
			if (objectBeingDragged && objectBeingDragged as IReplaceable)
				return objectBeingDragged as IReplaceable;
			return null;
		}
		
		public function executeIfPending(replaceableData:IReplaceable=null):Boolean
		{	
			clearTimer();
			
			if (isPending) {
				// If the replacement object is the only one with this content, replace the object with a new object for the incoming replaceable
				if (usageCountForPendingObject <= 1) {
				    performReplacement(replaceableData);
				} else // Otherwise, show the multiple replacement dialog
					showMultipleReplacementDialog(replaceableData);
				return true;
			}
			return false;
		}

		private static function applyReplacementFeedback(wholeObj:Boolean):void
		{
			// construction tape
			if (wholeObj)
				feedback.showPendingReplacement(_pendingReplacement.viewForOutline);
			else
				feedback.showPendingAsset(_pendingReplacement.viewForOutline);
		}
		
		private static function get usageCountForPendingObject():int
		{
            if (_pendingReplacement == null) {
                return 0;
            }
			if (_pendingReplacement.candidate) {
                var anchor:WireAnchor = _pendingReplacement.candidate as WireAnchor;
                if (anchor) {
                    var item:IReplaceable = anchor.replaceable;
                    if (item) {
                        if (anchor.hostObject is ToggleButton || anchor is SetterWireAnchor) {
                            return 1;   // don't give replace-all dialog for dropping graphic into button or setter ribbon, it would be a nuisance
                        } else {
                            var count:int = getUsageCount(item);
                            count -= countObjectsWiredToAsset(anchor);  // don't count other Drawings wired to this anchor
                            return count;
                        }
                    }
                }
                var cell:StateTableCell = _pendingReplacement.candidate as StateTableCell;
                if (cell && cell.replaceable) {
                    return getUsageCount(cell.replaceable);
                }
			}
            var receiver:IReplaceableReceiver = _pendingReplacement.candidate as IReplaceableReceiver;
            if (receiver == null) {
                receiver = _pendingReplacement.rawObject as IReplaceableReceiver;    // rollover code decided on whole-obj replacement
            }
            if (receiver && receiver.replaceable) {
                return getUsageCount(receiver.replaceable);
            }
			return 0;
		}

        private static function getUsageCount(item:IReplaceable):int
        {
            // counts instances in whole-objects, table cells, setters & triggers.  In the current page or backstage.
            var count:int = AuthorController.instance.eventFlow.getItemLocalUsageCount(item)
            AuthorController.instance.log("Replacement: getUsageCount="+count + " item="+item.name);
            return count;
        }

        /*
            Count occurances of asset that are not wired to anchor.
        */
        private static function countObjectsWiredToAsset(anchor:WireAnchor):int
        {
            var count:int = 0;
            var wiredObjs:Dictionary = new Dictionary();
            addConnectedObjsToList(anchor,  wiredObjs);  // recursively find all objects wired to this asset
            for each (var obj:AbstractObject in wiredObjs) {
                if (obj != anchor.hostObject) {
                    ++count;
                }
            }
            AuthorController.instance.log("Replacement: countObjectsWiredToAsset="+count);
            return count;
        }

        private static function addConnectedObjsToList(anchor:WireAnchor, wiredObjs:Dictionary):void
        {
            var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
            for each (var wire:Wire in wires) {
                var other:WireAnchor = (wire.masterAnchor == anchor) ? wire.slaveAnchor : wire.masterAnchor;
                if (other.replaceable is Asset) {  // in case the asset was wired to a different type of ribbon (eg. text)
                    var obj:AbstractObject = other.hostObject;
                    if (obj.uid in wiredObjs == false) {
                        wiredObjs[obj.uid] = obj;
                        addConnectedObjsToList(other, wiredObjs);
                    }
                }
            }
        }

		private static function showMultipleReplacementDialog(replaceableData:IReplaceable=null):void
		{
            var completion:Function = function(closeEvent:CloseEvent):void {
                            handleMultipleReplacementSelection(closeEvent, replaceableData);
                        };

			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE,
                    {id:50, closeFunction:completion, inserts:[replaceableData.name, usageCountForPendingObject]}));
		}
		
		private static function handleMultipleReplacementSelection(closeEvent:CloseEvent, replaceableData:IReplaceable=null):void
		{
            switch (closeEvent.detail) {
                case Alert.FIRST:	// replace this one
                    performReplacement(replaceableData);
                    break;
                case Alert.SECOND:	// replace all
                    replaceAllWith(replaceableData);
                    break;
                case Alert.THIRD:	// let's not. ok?
                    _instance.reset();
                    break;
            }
		}

        private static function replaceAllWith(newReplaceable:IReplaceable):void
        {
            if (newReplaceable && _pendingReplacement) {
                ApplicationController.instance.userMessage("Replacing all", replaceAllNow, null, [newReplaceable]);
            }
        }

		private static function replaceAllNow(newReplaceable:IReplaceable):void
		{
            var wholeObjectReplacement:Boolean = false;
            var oldReplaceable:IReplaceable;
            if (_pendingReplacement.oldReplaceable) {
                oldReplaceable = _pendingReplacement.oldReplaceable;
            } else if (_pendingReplacement.candidate) {
                oldReplaceable = _pendingReplacement.candidate.replaceable;
            } else if (_pendingReplacement.rawObject as IReplaceableReceiver) {
                oldReplaceable = IReplaceableReceiver(_pendingReplacement.rawObject).replaceable;
                wholeObjectReplacement = true;  // rollover code already decided on whole-obj replacement
            }
            var title:String = (newReplaceable is Asset) ? "Replace all assets" : "Replace all gadgets";
            var inputToken:Object = ApplicationController.instance.disableInput("replaceAllWith");
            var wasRunning:Boolean = AuthorController.instance.editWhileRunningStart();
            var token:Object = ApplicationController.currentActionTree.openGroup(title);
            if (wholeObjectReplacement) {
                EventFlowController.instance.assetReplaceAllWhole(AuthorController.instance.eventFlow, oldReplaceable, newReplaceable);  // uses PseudoThread
            } else {
                EventFlowController.instance.assetReplaceAll(AuthorController.instance.eventFlow, oldReplaceable, newReplaceable);
            }
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            PseudoThread.add(new PseudoThreadSimpleRunnable("replaceAllNow finish", function():void {
                ApplicationController.currentActionTree.closeGroup(token);
                if (wasRunning) AuthorController.instance.editWhileRunningEnd();
                LibraryController.instance.updateForAssetChange();
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE));
                ApplicationController.instance.enableInput(inputToken);
            }));
			_instance.reset();
		}
		
		private static function performReplacement(replaceable:IReplaceable):void
		{
            if (_pendingReplacement) {
                var candidate:IReplaceableReceiver = _pendingReplacement.candidate;

                if (replaceable as Asset && candidate && candidate.acceptsReplaceableDrop(replaceable)) {
                    // assign to ribbon, table-cell or AbstractExternalObject
                    assignReplaceableToPendingObject(replaceable, candidate);
                } else {
                    // whole-object replacement: for example, a bitmap dropped on a gadget, or vice versa
                    var target:AbstractObject = candidate as AbstractObject;
                    if (target == null)
                        target = _pendingReplacement.rawObject;
                    if (target) {
                        if (replaceable as Asset) {
                            var newAssetObj:AbstractExternalObject = AssetController.instance.configureContainerAndDisplay(replaceable as Asset);
                            replaceObjectWithUndo(newAssetObj, target);
                        } else if (replaceable as Project) {   // dragged gadget onto any object
                            var newComposite:Composite = ApplicationController.instance.authorController.addObjectForName("Composite") as Composite;
                            GadgetController.instance.configureContainerAndDisplay(replaceable as Project, newComposite, NaN, NaN, null, function():void {
                                replaceObjectWithUndo(newComposite, target);
                            });
                        }
                    }
                }
            }
			_instance.reset();
		}
		
		private static function assignReplaceableToPendingObject(replaceable:IReplaceable, target:IReplaceableReceiver):void
		{
            // target may be AbstractObject or WireAnchor or StateTableCell
            var anchor:WireAnchor = target as WireAnchor;
            var obj:AbstractObject = target as AbstractObject;
            var cell:StateTableCell = target as StateTableCell;
            var asset:Asset = replaceable as Asset;  // cannot currently drop anything other than Asset (can't drop Gadget into existing Composite)

            if (asset) {
                var wasRunning:Boolean = AuthorController.instance.editWhileRunningStart();
                if (anchor) {
                    obj = anchor.hostObject;
                    obj.controller.assetDroppedOnRibbon(obj, anchor, asset);  // this commits Undo
                    AuthorController.instance.log("drop asset=\""+asset.name+"\" onto ribbon="+anchor.logStr);
                } else if (cell) {
                    cell.replaceable = asset;
                    AuthorController.instance.log("drop asset=\""+asset.name+"\" onto table cell="+cell);
                } else if (obj) {
                    obj.controller.assetDroppedOnObject(obj, asset);  // drag over object instead of over ribbon (eg. Audio which has no ribbon)
                    AuthorController.instance.log("drop asset=\""+asset.name+"\" onto object="+obj.logStr);
                }
                if (wasRunning) AuthorController.instance.editWhileRunningEnd();
			}
		}
		
		public static function replaceObjectWithUndo(newObject:AbstractObject, oldObject:AbstractObject):void
		{
            trace("replaceObjectWithUndo old="+oldObject.logStr, "new="+newObject.logStr);
			if (oldObject.parent == null) {
				throw new Error("replaceObjectWithUndo: null parent");
			}
			var authorController:AuthorController = ApplicationController.instance.authorController;
			
			var token:* = ApplicationController.currentActionTree.openGroup("Replace " + oldObject.title);
			ApplicationController.currentActionTree.commit(DestroyObjectAction.fromObject(oldObject));
			
			var wasSelected:Boolean = authorController.selection.contains(oldObject.model);
			if (wasSelected)
				authorController.deselectSingleModel(oldObject.model);

			performObjectReplacement(newObject, oldObject);

			ApplicationController.currentActionTree.commit(CreateObjectAction.fromObject(newObject));
			ApplicationController.currentActionTree.closeGroup(token);
			
			if(Application.running) {
                newObject.onResume();
            }

			if (wasSelected)
				authorController.selectSingleModel(newObject.model, true);
		}
		
		public static function performObjectReplacement(newObject:AbstractObject, oldObject:AbstractObject):void
		{
            var candidate:Object;
            trace("performObjectReplacement: old="+oldObject, "new="+newObject);
			if (oldObject.parent == null && newObject.parent == null) {
				throw new Error("performObjectReplacement: null parent");
			}
			var authorController:AuthorController = ApplicationController.instance.authorController;

            // this is an author action
            var wasRunning:Boolean = authorController.editWhileRunningStart();
			
			// Position the new object
			newObject.x = oldObject.x;
			newObject.y = oldObject.y;
			newObject.anchorPoint = oldObject.anchorPoint;
            
            // copy message center title and position
            if (oldObject.titleChangedByAuthor) {  // preserve title only if set by author
                newObject.title = oldObject.title;
            }
            newObject.messageCenterRepositioned = oldObject.messageCenterRepositioned;
            newObject.xMessageCenter = oldObject.xMessageCenter;
            newObject.yMessageCenter = oldObject.yMessageCenter;

			// Give the new object the same parent & index as the old object
            var oldParent:TaconiteModel = oldObject.model.parent;
            var pager:Pager = oldParent.value as Pager;
            var wasEditingEvent:int = -1;
            if (pager && oldObject is IPage) {
                if(pager is EventFlow) {
                    wasEditingEvent = EventFlow(pager).wasEditingEvent;
                }
                PagerController(pager.controller).replacePage(pager, newObject as IPage, oldObject as IPage);
            } else {
                var z:int = oldParent.getChildIndex(oldObject.model);
                Utilities.assert(z >= 0);
                if (oldParent != newObject.model.parent) {
                    newObject.model.changeParent(oldParent);
                }
                newObject.model.parent.setChildIndex(newObject, z);
                newObject.initialValues.parent = AbstractObject(newObject.model.parent.value).referenceJSON;
                newObject.initialValues.zIndex = z;
            }

			// Reattach the old object's wires to the new object (if possible)
			for each (var thing:* in oldObject.anchors) {
				if (thing is WireAnchor == false)
					continue;  // probably an array of anchors, for a table
				var anchor:WireAnchor = thing as WireAnchor;
				
				var key:String = anchor.modifierDescription.key;

				if(anchor as SetterWireAnchor) {
					var setter:SetterWireAnchor = anchor as SetterWireAnchor;
					if(newObject.anchors[setter.setterProperty]) {
						var newSetter:SetterWireAnchor = null;
						if(setter.customModifier != null) {
							// reuse existing custom setter (show, hide) if it exists
							// since these are created automatically at object creation
							for each(candidate in newObject.anchors) {
								if(candidate is SetterWireAnchor && SetterWireAnchor(candidate).setterProperty == setter.setterProperty && SetterWireAnchor(candidate).customModifier.key == setter.customModifier.key && SetterWireAnchor(candidate).inValue == setter.inValue) {
									newSetter = candidate as SetterWireAnchor;
									break;
								}
							}
						} else if((setter.setterProperty == "currentPage") && setter.inValue == null) {
					        // reuse existing null inValue ribbon if it exists
							// since these are created automatically all the time
							for each(candidate in newObject.anchors) {
								if(candidate is SetterWireAnchor && SetterWireAnchor(candidate).setterProperty == setter.setterProperty && SetterWireAnchor(candidate).inValue == null) {
									newSetter = candidate as SetterWireAnchor;
									break;
								}
							}
                        }
						if(newSetter == null) {
							var customKey:String = (setter.customModifier==null) ? null : setter.customModifier.key;
							newSetter = new SetterWireAnchor(newObject.anchors[setter.setterProperty], setter.messageCenterVisible, null, true, setter.inValue, customKey);
						}
                        newSetter.setCurrentValues(setter.getCurrentValues());
                        newSetter.initialValues = ObjectUtil.copy(setter.initialValues);
						moveWires(setter.child("inValue"), newSetter.child("inValue"));
						moveWires(setter.child("tweenType"), newSetter.child("tweenType"));
						moveWires(setter.child("tweenTime"), newSetter.child("tweenTime"));
						moveWires(setter.child("tweenDelay"), newSetter.child("tweenDelay"));
						moveWires(setter.child("objectTransitionSecs"), newSetter.child("objectTransitionSecs"));
						moveWires(setter.child("objectTransTweenType"), newSetter.child("objectTransTweenType"));
						moveWires(setter.child("objectTransition"), newSetter.child("objectTransition"));
						moveWires(setter.child("keyPress"), newSetter.child("keyPress"));
						moveWires(setter, newSetter);
					}
				} else if(anchor as TriggerWireAnchor) {
					var trigger:TriggerWireAnchor = anchor as TriggerWireAnchor;
					if(newObject.anchors[trigger.triggerProperty]) {
						var newTrigger:TriggerWireAnchor = null;
						if((trigger.triggerProperty == "collidingWith" || trigger.triggerProperty == "droppedOn") && trigger.condition == null) {
							// reuse existing null collision target or null drop target if it exists
							// since these are created automatically all the time
							for each(candidate in newObject.anchors) {
								if(candidate is TriggerWireAnchor && TriggerWireAnchor(candidate).triggerProperty == trigger.triggerProperty && TriggerWireAnchor(candidate).condition == null) {
									newTrigger = candidate as TriggerWireAnchor;
									break;
								}
							}
						}
						if(newTrigger == null) {
							newTrigger = new TriggerWireAnchor(newObject.anchors[trigger.triggerProperty], trigger.messageCenterVisible, null, true);
						}
                        newTrigger.setCurrentValues(trigger.getCurrentValues());
                        newTrigger.initialValues = ObjectUtil.copy(trigger.initialValues);
						moveWires(trigger.child("condition"), newTrigger.child("condition"));
                        moveWires(trigger.child("onFalse"), newTrigger.child("onFalse"));
						moveWires(trigger, newTrigger);
					}
				} else if(anchor as CalcPropertyWireAnchor) {
					var calc:CalcPropertyWireAnchor = anchor as CalcPropertyWireAnchor;
					if(newObject.anchors[calc.calcProperty]) {
						var newCalc:CalcPropertyWireAnchor = new CalcPropertyWireAnchor(newObject.anchors[calc.calcProperty], calc.messageCenterVisible, null, true);
                        newCalc.setCurrentValues(calc.getCurrentValues());
                        newCalc.initialValues = ObjectUtil.copy(calc.initialValues);
						moveWires(calc.child("expression"), newCalc.child("expression"));
						moveWires(calc, newCalc);
					}
				} else {
					moveWires(anchor, newObject.anchors[key]);
				}

                var newAnchor:WireAnchor = newObject.anchors[key];

                if (newAnchor) {
                    if(anchor.keyPress != null && anchor.keyPress.length > 0) {
                        //TODO fix leak so that this nulling isn't necessary
                        var keyPress:String = anchor.keyPress;
                        anchor.keyPress = null; // clear the old keypress first, just to make sure it's not hanging around (if we're reusing this anchor, don't worry, it'll get its keypress back in a moment...)
                        newAnchor.keyPress = keyPress;
                    }
                    if(anchor.messageCenterVisible) {
                        newAnchor.messageCenterVisible = true;
                    }
                    if(anchor.propertyInspectorVisible) {
                        newAnchor.propertyInspectorVisible = true;
                    }
                }
			}

			// all possible properties are copied from old to new
			var copiedProps:Array = new Array();
			for each(var thingy:* in oldObject.anchors)
			{
				var a:WireAnchor = thingy as WireAnchor;
				if (a) {
					if(a.modifierDescription as PropertyDescription) {
						if (a.hostProperty == "replaceable" || a.hostProperty == "width" || a.hostProperty == "height")
							continue;
	
						copiedProps.push(a.hostProperty);
					}
				}
			}

			// preserve certain important properties
			for each(var prop:String in copiedProps) {
				if(prop in newObject) {
					if(prop in oldObject) {
						newObject[prop] = oldObject[prop];
					}
					if(prop in oldObject.initialValues) {
						newObject.initialValues[prop] = oldObject.initialValues[prop];
					}
				}
			}

			newObject.updateCollidingPossibilities();
			newObject.updateDroppedPossibilities();

			authorController.handleObjectRemoval(oldObject, newObject);
			
			// Delete the old object
			var oldModel:TaconiteModel = oldObject.model;
			if (oldModel.parent) {
                for each (var ao:AbstractObject in oldObject.allObjects()) {
                    if (ao && ao.controller) {
                        ao.controller.warnBeforeDelete(ao);
                    }
                }
                oldModel.parent.removeValueChildAndDescendants(oldModel.value);
            }

            if(wasEditingEvent >= 0) {
                EventFlow(pager).wasEditingEvent = wasEditingEvent;
            }

			// inform the current mediator that this has happened
			authorController.currentEditingMediator.switchedObjects(oldObject, newObject);

            if (wasRunning) ApplicationController.instance.authorController.editWhileRunningEnd();
		}

		private static function moveWires(fromAnchor:WireAnchor, toAnchor:WireAnchor):void
		{
			if(fromAnchor == null || toAnchor == null || fromAnchor.wired == false) {
				return;
			}
			var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(fromAnchor);
			for each (var wire:Wire in wires) {
				wire.unattachEnd(fromAnchor==wire.masterAnchor);
				wire.finish(toAnchor);
			}
		}
		
		public function reset():void
		{
			clearTimer();
			if (_pendingReplacement && _pendingReplacement.candidate as AbstractObject) {
				var object:AbstractObject = _pendingReplacement.candidate as AbstractObject;
				object.pendingReplacement = false;
				object.pendingData = false;
			}
			
			_pendingReplacement = null;
			feedback.clear();
		}
		
		private static function clearTimer():void
		{
			if (_timer) {
				_timer.stop();
				_timer = null;
			}
		}
		
		public function ribbonUnderPoint(stagePoint:Point):RibbonView
		{
			var worldContainer:WorldContainer = Application.instance.viewContext as WorldContainer;
			var container:DisplayObjectContainer = worldContainer.feedbackUIView;
			
			if (container.visible) {
				var n:int = container.numChildren-1;
				for (n; n >= 0; n--) {
					var mc:MessageCenterView = container.getChildAt(n) as MessageCenterView;
					if (mc && mc.visible && mc.hitTestPoint(stagePoint.x, stagePoint.y)) {
						var ribbon:RibbonView = mc.ribbonUnderPoint(stagePoint);
						if (ribbon)
							return ribbon;
					}
				}
			}
			return null;
		}
				
		private static function findReplaceableRibbons(obj:AbstractObject, replaceable:IReplaceable):Array
		{
			var anchors:Array = [];
			var type:String = replaceable.propertyType;
			
			if (obj is ToggleButton && replaceable is Asset && Asset(replaceable).content is XML)
				return anchors;  // SVG cannot be used in buttons
			if (obj is PathObject)
				return anchors;  // prevent replacement of the bitmapFill, so a whole-obj replacement can be done
			
			if (obj.hasMessageCenter) {
				for each (var thing:* in obj.anchors) {
					if (thing as WireAnchor) {
						var anchor:WireAnchor = thing;
						if (anchor.modifierDescription as PropertyDescription) {
							var prop:PropertyDescription = anchor.modifierDescription as PropertyDescription;
							if (prop.type == type)
								anchors.push(anchor);
						}
					}
				}
			}
			return anchors;
		}
	}
}

import com.alleni.author.controller.objects.IReplaceableReceiver;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ui.IReplaceable;

import flash.display.DisplayObject;

class ReplacementDescription
{
	public var candidate:IReplaceableReceiver;
	public var viewForOutline:DisplayObject;
	public var rawObject:AbstractObject;
	private var _oldReplaceable:IReplaceable;
	
	public function ReplacementDescription(candidate:IReplaceableReceiver, viewForOutline:DisplayObject, rawObject:AbstractObject=null, oldReplaceable:IReplaceable=null)
	{
		super();
		this.candidate = candidate;
		this.viewForOutline = viewForOutline;
		this.rawObject = (candidate == null)?rawObject:null;
		_oldReplaceable = oldReplaceable;
	}
	
	public function sameCandidate(rep:ReplacementDescription):Boolean
	{
		return rep && this.candidate == rep.candidate && this.rawObject == rep.rawObject;
	}
	
	public function get oldReplaceable():IReplaceable
	{
		return _oldReplaceable;
	}
}
