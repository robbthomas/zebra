package com.alleni.author.controller.ui
{
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;

	public class WireDragDescription extends DragDescription {
		
		public var fromObj:TaconiteModel;
		public var fromAnchor:WireAnchor;
		public var completed:Boolean = false;

		public var oldAnchor:WireAnchor = null;
		public var oldAnchorWasMaster:Boolean = false;
		
		public function WireDragDescription(obj:TaconiteModel, anchor:WireAnchor, context:ViewContext)
		{
            super(context);
			fromObj = obj;
			fromAnchor = anchor;
		}

	}
}
