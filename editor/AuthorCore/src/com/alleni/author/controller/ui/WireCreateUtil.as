/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 8/17/12
 * Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.ui {
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.action.CustomRibbonAction;
import com.alleni.author.definition.action.DestroyWireAction;
import com.alleni.author.document.Document;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.Calculator;
import com.alleni.author.model.objects.LogicObject;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.InternalSmartWireAnchor;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.ui.PortView;
import com.alleni.author.view.ui.WireboardView;
import com.alleni.taconite.service.LogService;

import flash.display.DisplayObject;

/**
 * Encapsulate the process of creating wires
 */
public class WireCreateUtil {
    public static const INLET:int = 0;
    public static const OUTLET:int = 1;
    public static const PROP:int = 2;

    public function WireCreateUtil() {
    }

    /**
     * Destroy the given wire as a result of user action. Take any implicit actions such as deleting unnecessary anchors
     * and log the action on the action tree.
     * @param wire the undesirable wire
     * @param undo true if undo steps should be added to the action tree
     * @param oldAnchor old anchor the wire was dragged from (probably should only be passed when undo=true)
     * @param oldAnchorWasMaster true if the old anchor was the master anchor of the wire
     */
    public static function unCreate(wire:Wire, undo:Boolean = false, oldAnchor:WireAnchor=null, oldAnchorWasMaster:Boolean=false):void {
        var token:Object;
        if(undo) {
            token = ApplicationController.currentActionTree.openGroup("Delete wire");
        }
        ApplicationController.instance.wireController.deleteWire(wire);
        var anchors:Array;
        if(oldAnchor != null) {
            anchors = [wire.masterAnchor, oldAnchor];
            if(undo) {
                ApplicationController.currentActionTree.commit(DestroyWireAction.fromDraggedWire(wire, oldAnchorWasMaster, oldAnchor));
            }
        } else {
            anchors = [wire.masterAnchor, wire.slaveAnchor];
            if(undo) {
                ApplicationController.currentActionTree.commit(DestroyWireAction.fromWire(wire));
            }
        }
        for each(var anchor:WireAnchor in anchors) {
            if(Document(ApplicationController.instance.authorController.document).project.accountTypeId < 3) {
                if(anchor.hostObject is EventPage) {
                    if(anchor is InternalSmartWireAnchor) {
                        anchor = InternalSmartWireAnchor(anchor).other;
                    }
                    if(anchor is ExternalSmartWireAnchor) {
                        var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
                        if(wires.length < 1) {
                            LogService.error("Missing second wire for creator edge anchor" + anchor.logStr);
                        } else {
                            var otherWire:Wire = wires[0];
                            ApplicationController.instance.wireController.deleteWire(otherWire);
                            if(undo) {
                                ApplicationController.currentActionTree.commit(DestroyWireAction.fromWire(otherWire));
                            }
                        }
                        var anchorAction:CustomRibbonAction = CustomRibbonAction.fromAnchor(anchor, false);
                        anchorAction.perform();
                        if(undo) {
                            ApplicationController.currentActionTree.commit(anchorAction);
                        }
                    }
                }
            }
            anchor.highlight = false;
        	anchor.wired = ApplicationController.instance.wireController.getWiresForAnchor(anchor).length>0;
        }
        ApplicationController.currentActionTree.closeGroup(token);
    }

    public static function checkFinish(wire:Wire, dropPoint:DisplayObject):WireCreateRecipe {
        var master:AnchorCreateStep = new SeedAnchorCreateStep(wire.masterAnchor);
        var slave:AnchorCreateStep;
        if(dropPoint is PortView) {
            var slaveAnchor:WireAnchor = PortView(dropPoint).anchor;
            if(slaveAnchor == wire.masterAnchor) {
                return null;
            }
            if(ApplicationController.instance.wireController.wireExists(wire.masterAnchor,  slaveAnchor)) {
                return null; // this is a basic case where the wire between the endpoints exists so another is disallowed
            }
            slave = new SeedAnchorCreateStep(slaveAnchor);
        } else if(dropPoint is WireboardView) {
            // first assume the step is in the slave list even if it will be put in the master list later
            // so assuming wire direction is from master to slave, the slave will be inward
            var wireType:WireType = master.wireType;
            var outward:Boolean = false;
            slave = new ExternalAnchorCreateStep(WireboardView(dropPoint).object, outward, wireType);
        }
        return generateSteps(master, slave);
    }

    public static function actuallyFinish(recipe:WireCreateRecipe, originalWire:Wire, oldAnchor:WireAnchor, oldAnchorWasMaster:Boolean, role:uint):void {
        var actionTitle:String = (oldAnchor == null) ? "Create wire" : "Move wire";
     	var token:Object = ApplicationController.currentActionTree.openGroup(actionTitle);
        unCreate(originalWire, oldAnchor != null, oldAnchor, oldAnchorWasMaster);
        recipe.execute(role);

        ApplicationController.currentActionTree.closeGroup(token);
        ApplicationController.instance.wireController.showWiresForLevel();
    }

    private static function generateSteps(originalMaster:AnchorCreateStep, originalSlave:AnchorCreateStep):WireCreateRecipe {

        // a special rule prevents self wiring for Calculators
        if(originalMaster.hostObject is Calculator && originalMaster.hostObject == originalSlave.hostObject) {
            return null;
        }

        var isUnsafeForObjectWiring:Boolean = false;
        if((originalMaster.type == Modifiers.OBJECT_TYPE || originalSlave.type == Modifiers.OBJECT_TYPE)){
            if(!(originalMaster.type == Modifiers.STRING_TYPE || originalSlave.type == Modifiers.STRING_TYPE || originalMaster.type == Modifiers.FORMATTED_TEXT_TYPE || originalSlave.type == Modifiers.FORMATTED_TEXT_TYPE)){
                isUnsafeForObjectWiring = true;
            }
        }

        // a special rule prevents wiring object references to or from wireboards
        if(((originalMaster.isEdge() && originalMaster.color == PROP) || (originalSlave.isEdge() && originalSlave.color == PROP))
            && (isUnsafeForObjectWiring)) {
                return null;
        }

        // first collect some information on what kind of wire we want to make based on which anchor we dragged from
        // this will affect what sort of intermediary wires we make
        var desired:WireType = originalMaster.wireType;
        if(originalMaster.color != PROP && originalSlave.color == PROP) {
            desired.type = originalSlave.type;
            desired.constraints = originalSlave.constraints;
        }

        // collect all the scopes between the master and slave anchors
        // this may be the two host objects and may also include gadgets/events that must be crossed
        // this will internally store the anchors which may have been changed for example from an InternalSmartWireAnchor to the external one
        var scopes:AnchorScopes = generateScopes(originalMaster, originalSlave);
        if(scopes == null) {
            // couldn't make this work for some reason
            return null;
        }

        // check to see if any wire can be made in the direction we dragged
        // if it cant be we will try the other direction writable
        if(scopes.masterAnchor) {
            if(scopes.slaveAnchor) {
                if(!(scopes.masterAnchor.readable && scopes.slaveAnchor.writable)) {
                    // the desired direction of the wire is not going to be possible
                    if(scopes.slaveAnchor.readable && scopes.masterAnchor.writable) {
                        // it can, however, work in the opposite direction. So flip it.
                        scopes.flip();
                    } else {
                        // it can't work in either direction so punt
                        return null;
                    }
                } else if(scopes.masterAnchor.value is EventPage && !(scopes.slaveAnchor.value is EventPage) && !(scopes.slaveAnchor.hostObject is LogicObject)){
                    scopes.flip();
                } else {
                    // all good
                }
            } else {
                if(!scopes.masterAnchor.readable) {
                    // we have only one end that is write only, so force it slave
                    scopes.flip();
                } else {
                    // fine
                }
            }
        } else {
            if(scopes.slaveAnchor) {
                if(!scopes.slaveAnchor.writable) {
                    // we have on ly one end that is read only so force it master
                    scopes.flip();
                } else {
                    // fine
                }
            } else {
                // nothing? Shouldn't be able to happen
            }
        }

        // for the desired value, we calculate after any flipping
        // in the case that both sides are properties, the master will only be the desired slave
        // if it is readonly, so we want that value
        if(scopes.masterAnchor && scopes.masterAnchor.hasValue) {
            if(scopes.masterAnchor.value is EventPage && !(scopes.slaveAnchor.value is EventPage) && !(scopes.slaveAnchor.hostObject is LogicObject)){
                desired.value = scopes.slaveAnchor.value;
            } else {
                desired.value = scopes.masterAnchor.value;
            }
        } else if(scopes.slaveAnchor && scopes.slaveAnchor.hasValue) {
            desired.value = scopes.slaveAnchor.value;
        }

        // Make all the steps to get each anchor out to the common scope including any setter/trigger anchors that may be necessary
        var masterSteps:Vector.<StepList> = generateAllSteps(scopes.masterAnchor, scopes.masterScopes, true, desired);
        var slaveSteps:Vector.<StepList> = generateAllSteps(scopes.slaveAnchor, scopes.slaveScopes, false, desired);

        // combine all possibilities from both sides that are workable into one final set of possible wire chains
        var possibilities:Vector.<WireCreateRecipe> = mergeSteps(masterSteps, slaveSteps);

        // return out the recipe with the lowest score
        var bestRecipe:WireCreateRecipe = null;
        trace("<><><><><><><><> All Possibilities");
        for each(var recipe:WireCreateRecipe in possibilities) {
            trace("Possibility", recipe.describe());
            if(bestRecipe == null || recipe.score < bestRecipe.score) {
                bestRecipe = recipe;
            }
        }
        return bestRecipe;
    }

    /**
     * Take all possible step lists from the master to the outside scope, and combine with all possible steps from
     * the slave to the outside scope. Only allow legal combinations
     * @param master set of possible step lists from the master anchor
     * @param slave set of possible step lists from the slave anchor
     * @return list of all possible recipes for connecting the master anchor to the slave anchor
     */
    private static function mergeSteps(master:Vector.<StepList>, slave:Vector.<StepList>):Vector.<WireCreateRecipe> {
        var result:Vector.<WireCreateRecipe> = new Vector.<WireCreateRecipe>();
        var recipe:WireCreateRecipe;
        if(master.length == 0) {
            var emptyMaster:StepList = StepList.empty();
            for each(var soloSlave:StepList in slave) {
                recipe = new WireCreateRecipe(emptyMaster, soloSlave);
                result.push(recipe);
            }
        } else if(slave.length == 0) {
            var emptySlave:StepList = StepList.empty();
            for each(var soloMaster:StepList in master) {
                recipe = new WireCreateRecipe(soloMaster, emptySlave);
                result.push(recipe);
            }
        } else {
            for each(var thisMaster:StepList in master) {
                for each(var thisSlave:StepList in slave) {
                    var m:AnchorCreateStep = thisMaster.last();
                    var s:AnchorCreateStep = thisSlave.last();
                    if(m.color == PROP && s.color != PROP) {
                        continue; // skip it, unworkable
                    }
                    if(m.color != PROP && s.color == PROP) {
                        continue; // skip it, unworkable
                    }
                    if(m.readable == false || s.writable == false) {
                        continue; // skip it, unworkable
                    }
                    recipe = new WireCreateRecipe(thisMaster, thisSlave);
                    result.push(recipe);
                }
            }
        }
        return result;
    }

    /**
     * Generates a list of objects starting with the host of the master anchor, containing SmartObject instances that
     * a wire must cross the boarder of and ending in the host of the slave anchor.
     * @param master The wire anchor that was dragged from
     * @param slave The wire anchor that was dragged to
     * @return Vector of objects
     */
    private static function generateScopes(master:AnchorCreateStep, slave:AnchorCreateStep):AnchorScopes {
        var scopes:AnchorScopes = new AnchorScopes(master, slave);

        var currentSide:GenerateScopeWorkingSide = GenerateScopeWorkingSide.master(scopes);

        // hop back and forth between lists, each time going up a scope in one or the other until they meet
        while(true) {
            currentSide.top = currentSide.top.wireScopeContainer;
            var otherIndex:int = currentSide.other.scopes.indexOf(currentSide.top);
            if(otherIndex >= 0) {
                // this path has crossed the other path at this index
                // everything above this index on that list is meaningless so cut it out
                currentSide.other.scopes.splice(otherIndex, currentSide.other.scopes.length-otherIndex);
                scopes.capScope = currentSide.top;
                break;
            } else if(currentSide.top != null) {
                // we haven't crossed but we do have a valid additional scope
                currentSide.scopes.push(currentSide.top);
            }
            if(currentSide.other.top != null) {
                // the other side has not ended
                // toggle to the other side
                currentSide = currentSide.other;
            } else if(currentSide.top == null) {
                // the other side has ended, and this side has too now
                // neither side has anywhere to go

                // this should not be able to happen normally, so error rather than fail silently
                LogService.error("There is no common scope for these wire anchors " + scopes.masterAnchor + " : " + scopes.slaveAnchor);
                return null;
            }
        }
        // check for wiring from or to an edge anchor. Make sure this anchor goes to the same side
        // of the vellum as the other anchor

        // At this point we are guaranteed that at least one scope list is non empty
        // In the case of wiring to an edge anchor, one list or the other will be empty
        // In the case of wiring between two edge anchors on the same object, both lists
        // will contain that one scope
        if(scopes.masterScopes.length == 0) {
            // master end is at the very top scope
            if(scopes.masterAnchor.isExternalEdge()) {
                // master end is just outside the boundary, so move it in
                if(scopes.masterAnchor is ExternalAnchorCreateStep) {
                    // let slave end punch out
                    scopes.slaveScopes.push(scopes.masterAnchor.hostObject);
                    scopes.masterAnchor = null;
                } else {
                    scopes.masterAnchor.flipEdge();
                }
            } else if(!scopes.masterAnchor.isInternalEdge()) {
                // this end isn't on the inside edge, and can't be pushed inside, so we'll have to push the scopes out
                // this is where we may use an internal version of the masterAnchor if it existed
                scopes.masterScopes.push(scopes.capScope);
                scopes.slaveScopes.push(scopes.capScope);
                scopes.capScope = scopes.capScope.wireScopeContainer;
            }
            if(scopes.slaveAnchor.isInternalEdge()) {
                // slave end is just inside a necessarily deeper boundary, so move it out
                scopes.slaveAnchor.flipEdge();
            }
        } else if(scopes.slaveScopes.length == 0) {
            // slave end is at the very top scope
            if(scopes.slaveAnchor.isExternalEdge()) {
                // master end is just outside the boundary, so move it in
                if(scopes.slaveAnchor is ExternalAnchorCreateStep) {
                    // let master end punch out
                    scopes.masterScopes.push(scopes.slaveAnchor.hostObject);
                    scopes.slaveAnchor = null;
                } else {
                    scopes.slaveAnchor.flipEdge();
                }
            } else if(!scopes.slaveAnchor.isInternalEdge()) {
                // this end isn't on the inside edge, and can't be pushed inside, so we'll have to push the scopes out
                // this is where we may use an internal version of the slaveAnchor if it existed
                scopes.masterScopes.push(scopes.capScope);
                scopes.slaveScopes.push(scopes.capScope);
                scopes.capScope = scopes.capScope.wireScopeContainer;
            }
            if(scopes.masterAnchor.isInternalEdge()) {
                // master end is just inside a necessarily deeper boundary, so move it out
                scopes.masterAnchor.flipEdge();
            }
        } else if(scopes.masterScopes.length == scopes.slaveScopes.length == 1 && scopes.masterScopes[0] == scopes.slaveScopes[0]) {
            // check for wiring from an edge anchor to edge anchor on the same gadget
            // force both sides to be inside or outside the vellum

            // Note, right now we will forbid inner-inner wires, so we'll force the inner side outward
            // if both sides are inner, we will forbid instead of moving both sides as that could be unexpected and confusing
            if(scopes.masterAnchor.isInternalEdge() && scopes.slaveAnchor.isExternalEdge()) {
                scopes.masterAnchor.flipEdge();
            }
            if(scopes.masterAnchor.isExternalEdge() && scopes.slaveAnchor.isInternalEdge()) {
                scopes.slaveAnchor.flipEdge();
            }
            if(scopes.masterAnchor.isInternalEdge() && scopes.slaveAnchor.isInternalEdge()) {
                LogService.message("Refusing to wire between two InternalSmartWireAnchors " + scopes.masterAnchor + " : " + scopes.slaveAnchor);
                return null;
            }
        }
        return scopes;
    }

    /**
     * Generate a set of step lists, where each is a list of steps from the start anchor punching through each scope barrier
     * Each list will contain at least one step per scope, and possibly more for making setters or triggers
     * @param startAnchor either the desired master or slave of the wire chain
     * @param scopes List of object scopes starting with the startAnchor.hostObject. Each subsequent object will be a SmartObject that we need to wire outside through the edge
     * @param outward the direction of the wires to be made. True if information flows away from the start anchor, false if it flows toward
     * @param desired
     * @return all possible lists of steps from the start anchor all the way to the outside of all scopes listed
     */
    private static function generateAllSteps(startAnchor:AnchorCreateStep, scopes:Vector.<AbstractObject>, outward:Boolean, desired:WireType):Vector.<StepList> {
        if(startAnchor == null) {
            return new Vector.<StepList>();
        }
        if(startAnchor is SeedAnchorCreateStep) {
            SeedAnchorCreateStep(startAnchor).value = desired.value;
        }
        var possibilities:Vector.<StepList> = new <StepList>[StepList.of(startAnchor, desired)];

        var nextPossibilities:Vector.<StepList> = new Vector.<StepList>();
        // each iteration of this loop will punch through the next scope
        for each(var scope:AbstractObject in scopes) {
            for each(var soFar:StepList in possibilities) {
                // take one of the current possibilities adn get it to punch through this scope
                // this may yield multiple new possibilities
                nextSteps(soFar, scope, outward, nextPossibilities);
            }
            possibilities = nextPossibilities;
            nextPossibilities = new Vector.<StepList>();
        }
        return possibilities;
    }

    private static function nextSteps(soFar:StepList, nextObject:AbstractObject, outward:Boolean, resultStore:Vector.<StepList>):void {
        var last:AnchorCreateStep = soFar.last();
        var succeeded:StepList = null;

        var proEvent:Boolean = nextObject is EventPage && Document(ApplicationController.instance.authorController.document).project.accountTypeId >= 3;
        var creatorEvent:Boolean = nextObject is EventPage && Document(ApplicationController.instance.authorController.document).project.accountTypeId < 3;

        if(last.hostObject == nextObject) {
            // what we have already can be good enough, we are already there
            succeeded = soFar;
        } else if(!(last.color == INLET && proEvent)){
            // we can't wire out of an event page if we're an inlet, but this restriction only applies to projects with an event flow (pro or above)
            var outerType:WireType = last.wireType;
            if(last.color == PROP && soFar.desired.blue) {
                // we are going from prop to prop, so make sure we use the desired type if it is different
                outerType = soFar.desired;
            }

            // we can get out with this anchor
            // make a copy of the step vector and add our new last item
            last = new ExternalAnchorCreateStep(nextObject as SmartObject, outward, outerType);
            if (last.type != Modifiers.OBJECT_TYPE) // disable object references through edges.
                succeeded = soFar.plus(last);
        }

        if(succeeded != null) {
            resultStore.push(succeeded);

            // consider color changes on the outside of this scope
            // we can only change from property to inlet/outlet on the outside of an object
            // we also will disallow changes on an event page if we are less than pro as the message center will never be visible
            if(last.color == PROP && !(nextObject is LogicObject) && !creatorEvent && last.isChildAnchor() == false) {
                if(last.readable && outward) {
                    resultStore.push(succeeded.plus(new TriggerAnchorCreateStep(last.hostObject)));
                }
                if(last.writable && !outward) {
                    resultStore.push(succeeded.plus(new SetterAnchorCreateStep(last.hostObject)));
                }
            }
        }
    }

    /**
     * Make a single unconnected external or custom anchor on a smart object.
     * This is intended for use when clicking the wire board plus ports.
     * @param smart the object on which to create the anchor
     * @param color one of WireCreateUtil.INLET, WireCreateUtil.OUTLET, WireCreateUtil.PROP
     * @param role the view role where the request originates
     */
    public static function makeRawExternalAnchor(smart:SmartObject, color:int, role:uint):ExternalSmartWireAnchor {
        var outward:Boolean = color != INLET;
        var wireType:WireType = new WireType();
        wireType.blue = color == PROP;
        wireType.bidirectional = color == PROP;
        wireType.type = Modifiers.STRING_TYPE;
        wireType.constraints = null;
        wireType.value = '';

        var step:ExternalAnchorCreateStep = new ExternalAnchorCreateStep(smart, outward,  wireType);
        var result:WireAnchor = step.perform(null, role);
        return result as ExternalSmartWireAnchor;
    }

    public static function nextLabelFromPrevious(previous:WireAnchor):String {
        if (previous != null) {
            return previous.hostObject.title + ': ' + previous.label;
        } else {
            return ExternalAnchorCreateStep.DEFAULT_LABEL;
        }
    }
}
}
/*
 * Types below this point merely aggregate data. There are some accessors to make reading data a bit easier.
 * There is and should be no significant logic below this point. The reader can assume any constructors or getters
 * do the obvious thing and no more.
 *
 * The one three exceptions are
 *   WireCreateRecipe#execute This function does the meat of following the recipe
 *   SeedAnchorCreateStep#perform This function takes extra steps with removing old wires and setting values
 *   ExternalAnchorCreateStep#perform This function makes and wires custom ribbons.
 */
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.WireCreateUtil;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.OutletDescription;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.action.CreateWireAction;
import com.alleni.author.definition.action.CustomRibbonAction;
import com.alleni.author.definition.action.ModifyRibbonAction;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.LogicObject;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.HelicopterWireAnchor;
import com.alleni.author.model.ui.InternalSmartWireAnchor;
import com.alleni.author.model.ui.SetterWireAnchor;
import com.alleni.author.model.ui.TriggerWireAnchor;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;

import mx.collections.IList;

class WireType {
    public var blue:Boolean;
    public var bidirectional:Boolean;
    public var type:String;
    public var constraints:Object;
    public var value:Object;

    public function clone():WireType {
        var result:WireType = new WireType();
        result.blue = this.blue;
        result.bidirectional = this.bidirectional;
        result.type = this.type;
        result.constraints = this.constraints;
        result.value = this.value;
        return result;
    }
}

class WireCreateRecipe {
    private var master:StepList;
    private var slave:StepList;

    public function WireCreateRecipe(master:StepList, slave:StepList) {
        this.master = master;
        this.slave = slave;
    }

    public function execute(role:uint):void {
        var step:AnchorCreateStep;
        var masterAnchor:WireAnchor = null;
        var slaveAnchor:WireAnchor = null;
        for each(step in master.steps) {
            masterAnchor = step.perform(masterAnchor, role);
        }
        for each(step in slave.steps) {
            slaveAnchor = step.perform(slaveAnchor, role);
        }
        if(masterAnchor != null && slaveAnchor != null && !ApplicationController.instance.wireController.wireExists(masterAnchor,  slaveAnchor)) {
            var capWire:Wire = ApplicationController.instance.wireController.createWire(masterAnchor);
            capWire.finish(slaveAnchor);
            capWire.notifyAttached(role);
            ApplicationController.instance.wireController.handlePropertyWireManuallyPropagated(capWire, true);
            ApplicationController.currentActionTree.commit(CreateWireAction.fromWire(capWire));
            ApplicationController.instance.authorController.log("createWire " + capWire.logStr);
        }
    }

    public function get score():int {
        return master.score + slave.score;
    }

    public function describe():String {
        return "Master: " + master.steps.join() + " Slave: " + slave.steps.join() + " Score: " + score;
    }
}

class GenerateScopeWorkingSide {
    public var top:AbstractObject;
    public var scopes:Vector.<AbstractObject>;
    public var other:GenerateScopeWorkingSide;

    public function GenerateScopeWorkingSide(scopes:Vector.<AbstractObject>) {
        this.top = scopes[scopes.length-1];
        this.scopes = scopes;
    }

    public static function master(scopes:AnchorScopes):GenerateScopeWorkingSide {
        var result:GenerateScopeWorkingSide = new GenerateScopeWorkingSide(scopes.masterScopes);
        result.other = new GenerateScopeWorkingSide(scopes.slaveScopes);
        result.other.other = result;
        return result;
    }
}

class AnchorScopes {
    public var masterAnchor:AnchorCreateStep;
    public var masterScopes:Vector.<AbstractObject>;
    public var slaveAnchor:AnchorCreateStep;
    public var slaveScopes:Vector.<AbstractObject>;
    public var capScope:AbstractObject;

    public function AnchorScopes(masterAnchor:AnchorCreateStep, slaveAnchor:AnchorCreateStep) {
        this.masterAnchor = masterAnchor;
        this.slaveAnchor = slaveAnchor;
        this.masterScopes = new <AbstractObject>[masterAnchor.hostObject];
        this.slaveScopes = new <AbstractObject>[slaveAnchor.hostObject];
        this.capScope = null;
    }

    public function flip():void {
        var tempStep:AnchorCreateStep = slaveAnchor;
        var tempScopes:Vector.<AbstractObject> = slaveScopes;
        slaveAnchor = masterAnchor;
        slaveScopes = masterScopes;
        masterAnchor = tempStep;
        masterScopes = tempScopes;
    }
}

class StepList {
    public var desired:WireType;
    public var steps:Vector.<AnchorCreateStep>;
    public var score:int;
    public static function empty():StepList {
        var result:StepList = new StepList();
        result.desired = null;
        result.steps = new <AnchorCreateStep>[];
        result.score = 0;
        return result;
    }
    public static function of(seed:AnchorCreateStep, desired:WireType):StepList {
        var result:StepList = new StepList();
        result.desired = desired;
        result.steps = new <AnchorCreateStep>[seed];
        result.score = seed.score(desired);
        return result;
    }
    public function plus(step:AnchorCreateStep):StepList {
        var result:StepList = new StepList();
        result.desired = desired;
        result.steps = steps.slice();
        result.steps.push(step);
        result.score = score + step.score(desired);
        return result;
    }
    public function last():AnchorCreateStep {
        return steps[steps.length-1];
    }
}

class AnchorCreateStep {
    public function perform(previous:WireAnchor, role:uint):WireAnchor {
        return null;
    }
    public function get hostObject():AbstractObject {
        return null;
    }
    public function get color():int {
        return -1;
    }

    public function get type():String {
        return null;
    }

    public function get constraints():Object {
        return null;
    }

    public function get value():Object {
        return null;
    }

    public function get readable():Boolean {
        return false;
    }

    public function get writable():Boolean {
        return false;
    }

    public function get hasValue():Boolean {
        return (color == WireCreateUtil.PROP) && readable;
    }

    public function get wireType():WireType {
        var result:WireType = new WireType();
        result.blue = color == WireCreateUtil.PROP;
        result.bidirectional = readable && writable;
        result.type = type;
        result.constraints = constraints;
        result.value = value;
        return result;
    }

    public function score(desired:WireType):int {
        if(desired.blue) {
            if(color != WireCreateUtil.PROP || (readable && writable) != desired.bidirectional || type != desired.type || constraints != desired.constraints) {
                return 2; // wasn't blue, or wrong direction, type, or constraints
            }
        } else {
            if(color == WireCreateUtil.PROP) {
                return 2; // we didn't want blue but it was
            }
        }
        return 1;
    }

    public function isInternalEdge():Boolean {
        return false;
    }

    public function isExternalEdge():Boolean {
        return false;
    }

    public function isChildAnchor():Boolean {
        return false;
    }

    public function isEdge():Boolean {
        return isExternalEdge() || isInternalEdge();
    }

    public function flipEdge():void {
        throw new Error("Cannot flip edge on non edge wire anchor: " + desc);
    }

    public function get desc():String {
        return (['I','O','P'][color]) + (writable?'<':'-') + (readable?'>':'-') + ((color == WireCreateUtil.PROP)?(type+'['+value+']'):'');
    }
}

class SeedAnchorCreateStep extends AnchorCreateStep {
    public var seedAnchor:WireAnchor;
    private var _desiredValue:Object = null;

    public function SeedAnchorCreateStep(seedAnchor:WireAnchor) {
        this.seedAnchor = seedAnchor;
    }

    override public function perform(previous:WireAnchor, role:uint):WireAnchor {
        if(seedAnchor.modifierDescription is PropertyDescription) {
            if(seedAnchor.isAdditionAnchor) {
                // if we are wiring to the add anchor port (last anchor when there are more anchors than property values)
                // then add a property value at this time.
                IList(seedAnchor.hostObject[seedAnchor.hostProperty]).addItem(value);
            } else {
                if(writable && !readable) {
                    // write only anchors only allow one wire connecting them, so we have to remove any existing wires
                    for each(var other:Wire in ApplicationController.instance.wireController.getWiresForAnchor(seedAnchor)) {
                        WireCreateUtil.unCreate(other,  true);
                    }
                }
                if(writable) {
                    var oldValue:* = seedAnchor.value;
                    var newValue:* = value;
                    seedAnchor.value = newValue;
                    ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(seedAnchor.hostObject, seedAnchor.path, oldValue));
                }
            }
        }
        return seedAnchor;
    }

    override public function get hostObject():AbstractObject {
        return seedAnchor.hostObject;
    }

    override public function get color():int {
        if(seedAnchor.modifierDescription is InletDescription) {
            return WireCreateUtil.INLET;
        } else if(seedAnchor.modifierDescription is OutletDescription) {
            return WireCreateUtil.OUTLET;
        } else {
            return WireCreateUtil.PROP;
        }
    }

    override public function get type():String {
        if(seedAnchor.modifierDescription is PropertyDescription) {
            return PropertyDescription(seedAnchor.modifierDescription).type;
        } else {
            return null;
        }
    }

    override public function get constraints():Object {
        if(seedAnchor.modifierDescription is PropertyDescription) {
            return PropertyDescription(seedAnchor.modifierDescription).constraints;
        } else {
            return null;
        }
    }

    public function set value(value:Object):void {
        this._desiredValue = value;
    }

    override public function get value():Object {
        if(_desiredValue != null) {
            return _desiredValue;
        }
        return seedAnchor.value;
    }

    override public function get readable():Boolean {
        if(seedAnchor is InternalSmartWireAnchor) {
            return seedAnchor.modifierDescription.writeable;
        }
        return seedAnchor.modifierDescription.readable;
    }

    override public function get writable():Boolean {
        if(seedAnchor is InternalSmartWireAnchor) {
            return seedAnchor.modifierDescription.readable;
        }
        return seedAnchor.modifierDescription.writeable;
    }

    override public function get hasValue():Boolean {
        return super.hasValue && !seedAnchor.isAdditionAnchor;
    }

    override public function isInternalEdge():Boolean {
        return seedAnchor is InternalSmartWireAnchor;
    }

    override public function isExternalEdge():Boolean {
        return seedAnchor is ExternalSmartWireAnchor;
    }

    override public function isChildAnchor():Boolean{
        return seedAnchor.host is WireAnchor;
    }

    override public function flipEdge():void {
        if(seedAnchor is InternalSmartWireAnchor) {
            seedAnchor = InternalSmartWireAnchor(seedAnchor).other;
        } else if(seedAnchor is ExternalSmartWireAnchor) {
            seedAnchor = ExternalSmartWireAnchor(seedAnchor).other;
        } else {
            throw new Error("Cannot flip edge on non edge wire anchor: " + seedAnchor.describe());
        }
    }

    public function toString():String {
        return "Start from " + seedAnchor.hostObject.title+'.\''+seedAnchor.label+'\' ' + desc;
    }
}

class ExternalAnchorCreateStep extends AnchorCreateStep {
    private var _smart:SmartObject;
    private var _outward:Boolean;
    private var _outerType:WireType;
    public static const DEFAULT_LABEL:String = 'Custom Ribbon';
    private static const DEFAULT_DESCRIPTION:String = '';


    public function ExternalAnchorCreateStep(smart:SmartObject, outward:Boolean, outerType:WireType) {
        _smart = smart;
        _outward = outward;
        _outerType = outerType;
    }

    override public function get hostObject():AbstractObject {
        return _smart;
    }

    override public function perform(previous:WireAnchor, role:uint):WireAnchor {


        // setup internal anchor
        var inner:InternalSmartWireAnchor = new InternalSmartWireAnchor();
        inner.hostObject = _smart;
        inner.other.hostObject = _smart;

        var label:String = WireCreateUtil.nextLabelFromPrevious(previous);
        var description:String = DEFAULT_DESCRIPTION;

        if(_outerType.blue) {
            var type:String = _outerType.type;
            if(type == "eventPage" && previous != null && !(previous.hostObject is LogicObject)){
                type = (previous.modifierDescription as PropertyDescription).type;
            }
            var constraints:Object = _outerType.constraints;
            //var readable:Boolean = _outerType.bidirectional || _outward;
            var readable:Boolean = true; // temporary override as we do not support write only external wires
            var writable:Boolean = _outerType.bidirectional || !_outward;

            inner.modifierDescription = new PropertyDescription("internal-"+inner.internalID,"",label,description,"CUSTOM", 0, false, -1, type, constraints);
            inner.other.modifierDescription = new PropertyDescription("external-"+inner.other.externalID,"",label,description,"CUSTOM",0, false, -1, type, constraints, writable, readable);
            inner.value = _outerType.value;
        } else {
            if(_outward) {
                inner.modifierDescription = new OutletDescription("internal-"+inner.internalID,"",label,description,"CUSTOM");
                inner.other.modifierDescription = new OutletDescription("external-"+inner.other.externalID,"",label,description,"CUSTOM");
            } else {
                inner.modifierDescription = new InletDescription("internal-"+inner.internalID,"",label,description,"CUSTOM");
                inner.other.modifierDescription = new InletDescription("external-"+inner.other.externalID,"",label,description,"CUSTOM");
            }
        }

        _smart.addAnchor(inner.other, true);
        ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(inner.other, true));

        if(previous != null) {
            var wire:Wire = ApplicationController.instance.wireController.createWire(previous);
            wire.finish(inner);
            wire.notifyAttached(role);
            ApplicationController.instance.wireController.handlePropertyWireManuallyPropagated(wire, true);
            ApplicationController.currentActionTree.commit(CreateWireAction.fromWire(wire));
            ApplicationController.instance.authorController.log("createWire " + wire.logStr);
        }
        return inner.other;
    }

    override public function get color():int {
        return _outerType.blue ? WireCreateUtil.PROP : (_outward ? WireCreateUtil.OUTLET : WireCreateUtil.INLET);
    }

    override public function get type():String {
        return _outerType.type;
    }

    override public function get constraints():Object {
        return _outerType.constraints;
    }

    override public function get value():Object {
        return _outerType.value;
    }

    override public function get readable():Boolean {
        return _outward || (_outerType.blue && _outerType.bidirectional);
    }

    override public function get writable():Boolean {
        return !_outward || (_outerType.blue && _outerType.bidirectional);
    }

    override public function get wireType():WireType {
        return _outerType;
    }

    override public function isInternalEdge():Boolean {
        return false;
    }

    override public function isExternalEdge():Boolean {
        return true;
    }

    override public function flipEdge():void {
        // do nothing, cant be flipped
        // where a flip would happen it will get moved
        // between master and slave lists
    }

    public function toString():String {
        return "Wire to edge " + desc;
    }
}

class SetterAnchorCreateStep extends AnchorCreateStep {
    private var _hostObject:AbstractObject;

    public function SetterAnchorCreateStep(hostObject:AbstractObject) {
        _hostObject = hostObject;
    }

    override public function perform(previous:WireAnchor, role:uint):WireAnchor {
        var result:SetterWireAnchor = new SetterWireAnchor(previous, true);
        ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(result, true));
        return result;
    }

    override public function get hostObject():AbstractObject {
        return _hostObject;
    }

    override public function get color():int {
        return WireCreateUtil.INLET;
    }

    override public function get writable():Boolean {
        return true;
    }

    public function toString():String {
        return "Create setter " + desc;
    }
}

class TriggerAnchorCreateStep extends AnchorCreateStep {
    private var _hostObject:AbstractObject;

    public function TriggerAnchorCreateStep(hostObject:AbstractObject) {
        _hostObject = hostObject;
    }

    override public function perform(previous:WireAnchor, role:uint):WireAnchor {
        var result:TriggerWireAnchor = new TriggerWireAnchor(previous, true);
        ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(result, true));
        return result;
    }

    override public function get hostObject():AbstractObject {
        return _hostObject;
    }

    override public function get color():int {
        return WireCreateUtil.OUTLET;
    }

    override public function get readable():Boolean {
        return true;
    }

    public function toString():String {
        return "Create trigger " + desc;
    }
}