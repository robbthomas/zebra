package com.alleni.author.controller.ui
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.ObjectView;
import com.alleni.taconite.view.ViewContext;

import flash.events.Event;
	import flash.events.TimerEvent;

	public class AssetDragDescription extends DragDescription
	{
		public var assetAnchor:WireAnchor;
		
		public function AssetDragDescription(context:ViewContext)
		{
            super(context);
		}
		
		override protected function swapMC(event:TimerEvent):void
		{
			super.swapMC(event);
			
			// open MC category, but wait for it to initialize (in render event)
			if (assetAnchor && shownMessageCenter) {
				var obj:AbstractObject = shownMessageCenter.value as AbstractObject;
				if (obj) {
					obj.messageCenterVisible = true;
					if (EditorUI.instance.stage) {
						EditorUI.instance.stage.addEventListener(Event.ENTER_FRAME, handleEnterEvent);
					}
				}
			}
		}
		
		private function handleEnterEvent(event:Event):void
		{
			if (EditorUI.instance.stage) {
				EditorUI.instance.stage.removeEventListener(Event.ENTER_FRAME, handleEnterEvent);
				EditorUI.instance.stage.invalidate();
			}

			if (assetAnchor && shownMessageCenter) {
				var obj:AbstractObject = shownMessageCenter.value as AbstractObject;
				var view:ObjectView = obj.getView();
				if (view && view.messageCenterView) {
					view.messageCenterView.openCategory(assetAnchor.modifierDescription.category);
				}
			}
		}
	}
}
