/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 3/28/11
 * Time: 11:51 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.ui {
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.app.KeyMediator;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.action.CustomRibbonAction;
import com.alleni.author.event.AnchorEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.CalcPropertyWireAnchor;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.SetterWireAnchor;
import com.alleni.author.model.ui.TriggerWireAnchor;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.ui.MenuButton;
import com.alleni.author.view.ui.MessageCenterMenuBar;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.author.view.ui.RibbonSlider;
import com.alleni.author.view.ui.RibbonView;
import com.alleni.taconite.controller.DragMediator;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Point;

public class RibbonDragMediator extends DragMediator {

	private var _view:RibbonView;
	private var _snapshot:Sprite = null;
	private var _ribbonDragDescription:RibbonDragDescription = null;
	private var _mouseOffset:Point = null;

	public function RibbonDragMediator(view:RibbonView, valueSlider:RibbonSlider) {
		super(view.context);
		_view = view;
		_view.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
        if (valueSlider)
            valueSlider.addEventListener(MouseEvent.MOUSE_DOWN,handleMouseDown);
	}

	override protected function handleDragStart(e:MouseEvent):void {
		super.handleDragStart(e);
		_mouseOffset = new Point(_view.mouseX, _view.mouseY);
		_mouseOffset.y = -1;
		_snapshot = _view.makeDragSnapShot()
		positionSnapshot();
		_ribbonDragDescription = new RibbonDragDescription(_view);
		Application.instance.ribbonDragDescription = _ribbonDragDescription;
	}

	override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean = false):void {
		super.handleDragMove(e, doneDragging);
		positionSnapshot();
	}

	override protected function handleDragEnd(e:MouseEvent):void {
		super.handleDragEnd(e);
        var setter:SetterWireAnchor;
        var trigger:TriggerWireAnchor;
        var needMore:Boolean;
        var a:Object;
        var anchor:WireAnchor;
		var overMC:Boolean = false;
		for(var target:DisplayObject = e.target as DisplayObject; target != null; target = target.parent) {
			if(target is MenuButton && target.parent.parent is MessageCenterMenuBar) {
				overMC = true;
				var bar:MessageCenterMenuBar = target.parent.parent as MessageCenterMenuBar;
				var obj:AbstractObject = MessageCenterView(bar.parent.parent).model.value as AbstractObject;
				if(_view.ribbon.modifierDescription is PropertyDescription && _view.ribbon.hostObject == obj && _view.ribbon.host is WireAnchor == false) {
					if(bar.inBtn == target) {
						setter = new SetterWireAnchor(_view.ribbon, true);
						ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(setter, true));
					} else if(bar.outBtn == target) {
						trigger = new TriggerWireAnchor(_view.ribbon, true);
						ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(trigger, true));
					} else if(bar.propsBtn == target) {
						var calc:CalcPropertyWireAnchor = new CalcPropertyWireAnchor(_view.ribbon, true);
						ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(calc, true));
					}
				}
				break;
			} else if(target is MessageCenterView) {
				overMC = true;
				break;
			}
		}
		if(overMC == false) {
			if(_view.ribbon is SetterWireAnchor || _view.ribbon is TriggerWireAnchor || _view.ribbon is CalcPropertyWireAnchor || (_view.ribbon is ExternalSmartWireAnchor && SmartObject(_view.ribbon.hostObject).editing)) {
				var group:Object = null;
                var showHide:Boolean = false;
                if(_view.ribbon is SetterWireAnchor
                        && SetterWireAnchor(_view.ribbon).customModifier != null
                        && (SetterWireAnchor(_view.ribbon).customModifier.key == "show" || SetterWireAnchor(_view.ribbon).customModifier.key == "hide")) {
                    group = ApplicationController.currentActionTree.openGroup("Clear " + SetterWireAnchor(_view.ribbon).customModifier.key + " ribbon");
                    showHide = true;
                } else if(_view.ribbon is SetterWireAnchor
                        && SetterWireAnchor(_view.ribbon).setterProperty == "currentPage") {
                    var setterProp:String = SetterWireAnchor(_view.ribbon).setterProperty;
                    needMore = true;
                    for each(a in _view.ribbon.hostObject.anchors) {
                        if(a is SetterWireAnchor && SetterWireAnchor(a).setterProperty == setterProp && a != _view.ribbon && SetterWireAnchor(a).inValue == null) {
                            needMore = false;
                            break;
                        }
                    }
                    if(needMore) {
                        group = ApplicationController.currentActionTree.openGroup("Clear " + SetterWireAnchor(_view.ribbon).setterProperty + " setter");
                    }
                } else if(_view.ribbon is TriggerWireAnchor
                        && (TriggerWireAnchor(_view.ribbon).triggerProperty == "droppedOn" || TriggerWireAnchor(_view.ribbon).triggerProperty == "collidingWith")) {
                    var triggerProp:String = TriggerWireAnchor(_view.ribbon).triggerProperty;
                    needMore = true;
                    for each(a in _view.ribbon.hostObject.anchors) {
                        if(a is TriggerWireAnchor && TriggerWireAnchor(a).triggerProperty == triggerProp && a != _view.ribbon && TriggerWireAnchor(a).condition == null) {
                            needMore = false;
                            break;
                        }
                    }
                    if(needMore) {
                        group = ApplicationController.currentActionTree.openGroup("Clear " + TriggerWireAnchor(_view.ribbon).triggerProperty + " trigger");
                    }
                }
                var action:CustomRibbonAction = CustomRibbonAction.fromAnchor(_view.ribbon, false);
				action.perform();
				ApplicationController.currentActionTree.commit(action);
                if(group != null) {
                    if(showHide) {
                        var val:Object;
                        var key:String;
                        if(SetterWireAnchor(_view.ribbon).customModifier.key == "show") {
                            val = true;
                            key = "show"
                        } else {
                            val = false;
                            key = "hide";
                        }
						setter = new SetterWireAnchor(_view.ribbon.hostObject.anchors["visible"], false, null, false, val, key);
						ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(setter, true));
                    } else if(_view.ribbon is SetterWireAnchor) {
                        anchor = _view.ribbon.hostObject.anchors[SetterWireAnchor(_view.ribbon).setterProperty];
                        setter = new SetterWireAnchor(anchor, false);
                        ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(setter, true));
                    } else {
                        anchor = _view.ribbon.hostObject.anchors[TriggerWireAnchor(_view.ribbon).triggerProperty];
                        trigger = new TriggerWireAnchor(anchor, false);
                        ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(trigger, true));
                    }
                    ApplicationController.currentActionTree.closeGroup(group);
                }
			} else {
				_view.ribbon.messageCenterVisible = false;
				_view.ribbon.hostObject.dispatchEvent(new AnchorEvent(AnchorEvent.DRAGGED_OUT, _view.ribbon, false));
			}
		}
		cleanup();
	}


	override protected function handleClick(e:MouseEvent):void {
		super.handleClick(e);
		if(KeyMediator.heldKey == "o") {
			if(_view.ribbon.modifierDescription is PropertyDescription && _view.ribbon.host is WireAnchor == false) {
				var trigger:TriggerWireAnchor = new TriggerWireAnchor(_view.ribbon, true);
				ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(trigger, true));
			}
		} else if(KeyMediator.heldKey == "i") {
			if(_view.ribbon.modifierDescription is PropertyDescription && _view.ribbon.host is WireAnchor == false) {
				var setter:SetterWireAnchor = new SetterWireAnchor(_view.ribbon, true);
				ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(setter, true));
			}
		} else if(KeyMediator.heldKey == "p") {
			if(_view.ribbon.modifierDescription is PropertyDescription && _view.ribbon.host is WireAnchor == false) {
				var calc:CalcPropertyWireAnchor = new CalcPropertyWireAnchor(_view.ribbon, true);
				ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(calc, true));
			}
		} else if(KeyMediator.heldKey == "k") {
			if(_view.ribbon.modifierDescription is InletDescription && _view.ribbon.host is WireAnchor == false) {
//				var key:KeyHandlerWireAnchor = new KeyHandlerWireAnchor(_view.ribbon);
				//ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(key, true));
			}
		}
	}

	private function positionSnapshot():void {
		_snapshot.x = EditorUI.instance.rawChildren.mouseX - _mouseOffset.x;
		_snapshot.y = EditorUI.instance.rawChildren.mouseY - _mouseOffset.y;
	}

	private function cleanup():void {
		_view.highlight(false);
		if(_snapshot != null) {
			if(_snapshot.parent != null) {
				_snapshot.parent.removeChild(_snapshot);
			}
			_snapshot = null;
		}
		if(_ribbonDragDescription != null) {
			if(Application.instance.ribbonDragDescription == _ribbonDragDescription) {
				Application.instance.ribbonDragDescription = null;
			}
			_ribbonDragDescription = null;
		}
	}
}
}
