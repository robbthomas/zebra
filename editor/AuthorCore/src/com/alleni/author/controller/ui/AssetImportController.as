package com.alleni.author.controller.ui
{
import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.definition.AssetType;
import com.alleni.author.definition.action.LibraryAddAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.AssetEvent;
import com.alleni.author.model.ui.Asset;
import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.service.LogService;
	
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.FileReference;
	import flash.net.FileReferenceList;

	
	public class AssetImportController
	{
		static private var _instance:AssetImportController = new AssetImportController();
		
		private var _fileReferenceList:FileReferenceList;
		private var _currentImportCount:int = 0;
		private var _pendingFiles:Vector.<FileReference> = new Vector.<FileReference>;

		public function AssetImportController()
		{
			ApplicationController.addEventListener(NotificationNamesApplication.IMPORT_ASSET, handleImportAsset);
		}
		
		static public function get instance():AssetImportController
		{
			Utilities.assert(_instance != null);
			return _instance;
		}
		
		private function handleImportAsset(event:ApplicationEvent = null):void
		{
			_fileReferenceList = new FileReferenceList();
			_fileReferenceList.addEventListener(Event.SELECT, handleFileSelected, false, 0, true);
			_fileReferenceList.addEventListener(Event.CANCEL, handleFileCancel, false, 0, true);
			// FileReferenceList seems to inadvertently trigger IllegalOperationError as if there were multiple sessions
			// with no rhyme or reason, so we need to catch the resulting exception.
			try { 
				_fileReferenceList.browse(typeFilter);
			} catch(e:IllegalOperationError) {
				LogService.error("AssetImportController attempted a new browsing session before the previous had completed.");
			}
		}
		
		private function handleFileSelected(event:Event):void
		{
			var fileList:Array = (event.currentTarget as FileReferenceList).fileList;

			if (fileList.length == 1)
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, "Importing " + (fileList[0] as FileReference).name + "..."));
			else
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, "Importing " + fileList.length + " assets..."));
			var pendingFiles:Array = new Array();
			var file:FileReference;
			for (var i:uint = 0; i < fileList.length; i++) {
				file = FileReference(fileList[i]);
				addPendingFile(file);
			}
			_currentImportCount = fileList.length;
			_fileReferenceList = null;
		}
		
		private function handleFileCancel(event:Event):void
		{
			_fileReferenceList = null;
		}
		
		private function addPendingFile(file:FileReference):void
		{
			_pendingFiles.push(file);
			file.addEventListener(Event.COMPLETE, completeHandler, false, 0, true);
			file.addEventListener(ProgressEvent.PROGRESS, progressHandler, false, 0, true);
			file.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler, false, 0, true);
			file.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler, false, 0, true);
			file.load();
		}
		
		private function removePendingFile(file:FileReference):void 
		{
			for (var i:uint; i < _pendingFiles.length; i++) {
				if (_pendingFiles[i].name == file.name) {
					_pendingFiles.splice(i, 1);
					file = null;
					return;
				}
			}
		}
		
		private function completeHandler(event:Event):void 
		{
			var file:FileReference = FileReference(event.target);
			var fileName:String = Utilities.FILENAME_FROM_URL_REGEX.exec(file.name);
			
			switch(AssetDescription.validateFileType(fileName)){
				case AssetType.BITMAP:
				case AssetType.AUDIO:
				//case AssetType.VIDEO:
				//case AssetType.SWF:
				case AssetType.FONT:
				case AssetType.TEXT:
				case AssetType.CSS:
				case AssetType.SVG:
					buildAsset(file);
					break;
				default:
					removePendingFile(file);
					if (_pendingFiles.length == 0) 
						resetMessage("Sorry, " + fileName + " is not a supported file for import.");
					break;
			}
		}
		
		private function progressHandler(event:ProgressEvent):void 
		{
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void 
		{
			var file:FileReference = FileReference(event.target);
			LogService.error("IO error importing " + file.name + ": " + event.text);
			removePendingFile(file);
			if (_pendingFiles.length == 0) 
				resetMessage("IO error importing " + file.name + "...");
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void 
		{
			var file:FileReference = FileReference(event.target);
			LogService.error("Security error importing " + file.name + ": " + event.text);
			removePendingFile(file);
			if (_pendingFiles.length == 0) 
				resetMessage("Security error importing " + file.name + "...");
		}
		
		private function buildAsset(file:FileReference):void
		{
			var name:String = Utilities.FILENAME_FROM_URL_REGEX.exec(file.name);
			
			AssetController.instance.importAndReportBackOnComplete(name, file.data, assetCompleteClosure(file));
		}

		private function assetCompleteClosure(file:FileReference):Function
		{
			return function(event:AssetEvent):void {
				removePendingFile(file);
				addAssetToLibrary(event.asset);
                if (_pendingFiles.length == 0)
                    resetMessage();
                else {
                    var message:String = "Importing " + _pendingFiles.length + " asset";
                    if (_pendingFiles.length > 1)
                        message += "s";
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, message + "..."));
                }
			}
		}

        private function addAssetToLibrary(asset:Asset):void
        {
            var libraryAdd:LibraryAddAction = LibraryAddAction.fromItem(asset);
            libraryAdd.perform();
            ApplicationController.currentActionTree.commit(libraryAdd);
        }
		
		private function resetMessage(message:String=""):void
		{
			if (message.length == 0) message = "Asset Import Completed";
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, message));
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE_WITH_DELAY));
		}
		
		private function get typeFilter():Array 
		{
			return [
					AssetDescription.BITMAP_FILE_FILTER, 
					AssetDescription.SVG_FILE_FILTER,
					AssetDescription.AUDIO_FILE_FILTER, 
					//AssetDescription.VIDEO_FILE_FILTER,  no video until streaming implementation is enabled.
					//AssetDescription.SWF_FILE_FILTER,
					//AssetDescription.FONT_FILE_FILTER,
					//AssetDescription.TEXT_FILE_FILTER,  //disabled until text is added to the supported media list
					//AssetDescription.CSS_FILE_FILTER
			];
		}
	}
}