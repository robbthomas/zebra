package com.alleni.author.controller.ui.palettes
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.ArenaPage;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.FindModel;
	import com.alleni.author.view.FindView;
import com.alleni.taconite.service.LogService;

import flash.events.Event;
import flash.utils.Dictionary;

import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;

	public class FindMediator
	{
		private var _view:FindView;
		private var _model:FindModel;

        private static const WILDCARD:String = "*";
		
		public function FindMediator()
		{
		}
		
		public function handleViewEvents(view:FindView):void
		{
			_view = view;
			_model = view.model;
			view.mediator = this;
			_model.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, modelListener);
		}
		
		private function modelListener(event:PropertyChangeEvent):void
		{
			trace("FindMediator:modelListener prop="+event.property, "val="+event.newValue);
			switch (event.property) {
				case "targetTitle":
                    if (event.newValue != "") {
                        if (_model.viewStackPage == FindModel.BY_TITLE_PAGE)
                            performFind();
                        else if (_model.viewStackPage == FindModel.BY_RIBBON_PAGE)
                            findRibbonValue();
                    }
                    break;
				case "targetUID":
					if (event.newValue != "") {
						performFindUID();
					}
					break;
                case "viewStackPage":
                    _view.addEventListener(Event.ENTER_FRAME, afterPageChange);
                    break;
                case "selectedCategory":
                    setupRibbonNames();
                    break;
                case "selectedKey":
                case "targetValue":
                    findRibbonValue();
                    break;

			}
		}

        private function afterPageChange(e:Event):void
        {
            _view.removeEventListener(Event.ENTER_FRAME, afterPageChange);

            if (_model.viewStackPage == FindModel.BY_RIBBON_PAGE) {
                setupRibbonCategories();
            }
        }
		
		private function performFind():void
		{
			if (_model.viewStackPage == FindModel.BY_ID_PAGE) {
				performFindUID();
			} else {
				performFindTitle();
			}
		}
		
		private function performFindTitle():void
		{
			var list:Array = [];
			if (_model.targetTitle.length > 0) {
                var wildTitle:Boolean = (_model.targetTitle == WILDCARD);
				var target:RegExp = new RegExp(_model.targetTitle, "i");
				var objects:Vector.<AbstractObject> = world.allObjects();
				for each (var obj:AbstractObject in objects) {
					if (obj is ArenaPage == false) {
						if (wildTitle || obj.title.search(target) >= 0) {
							list.push(obj);
						}
					}
				}
			}
			
			_model.foundObjects.source = list;
			trace("title found count="+list.length);
		}

        private function findRibbonValue():void
        {
            var list:Array = [];
            var key:String = _model.selectionRibbonKey;
            if (key != null && _model.targetValue.length > 0) {
                var titlePattern:RegExp = new RegExp(_model.targetTitle, "i");
                var wildTitle:Boolean = _model.targetTitle == WILDCARD || _model.targetTitle == "";
                var valuePattern:RegExp = new RegExp(_model.targetValue, "i");
                var wildValue:Boolean = (_model.targetValue == "*");

                var objects:Vector.<AbstractObject> = world.allObjects();
                for each (var obj:AbstractObject in objects) {
                    if (obj is ArenaPage == false) {
                        if (key in obj && (wildTitle || obj.title.search(titlePattern) >= 0)) {
                            var actual:* = obj[key];
                            trace("  actual="+actual, obj);
                            if (actual != null) {
                                var actualStr:String = actual.toString();
                                if (wildValue || actualStr.search(valuePattern) >= 0) {
                                    list.push(obj);
                                }
                            }
                        }
                    }
                }
            }

            _model.foundObjects.source = list;
            trace("value found count="+list.length);
        }

		private function performFindUID():void
		{
			var list:Array = [];
			var target:String = _model.targetUID;
			
			if (target.length > 0) {
				var objects:Vector.<AbstractObject> = world.allObjects();
				for each (var obj:AbstractObject in objects) {
					if (obj.uid == target) {
						list.push(obj);
					}
				}
			}
			
			_model.foundObjects.source = list;
			trace("UID found count="+list.length);
		}

        private function setupRibbonCategories():void
        {
            var reg:Dictionary = Modifiers.instance.modifiersRegistry;
            if (reg && _model.allCategories.length == 0) {
                var cats:Array = [];
                var have:Dictionary = new Dictionary();
                for each (var thing:* in reg) {
                    var prop:PropertyDescription = thing as PropertyDescription;
                    if (prop) {
                        var cat:String = prop.category;
                        if (cat != "PORT" && cat in have == false) {
                             have[cat] = prop;
                             cats.push( {label:cat, data:cat} );
                        }
                    }
                }
                cats.sort(compareCats)
                _model.allCategories.source = cats;
                _model.selectedCategory = cats[0];
            }
        }

        private function compareCats(a:Object,  b:Object):int
        {
            var aWeight:int = Modifiers.instance.categoryWeight(a.data as String);
            var bWeight:int = Modifiers.instance.categoryWeight(b.data as String);
            if (aWeight <= 0) aWeight += 200;  // put wierd stuff at bottom of list
            if (bWeight <= 0) bWeight += 200;
            return aWeight - bWeight;
        }

        private function setupRibbonNames():void
        {
            var reg:Dictionary = Modifiers.instance.modifiersRegistry;
            if (reg) {
                var ribbons:Array = [];
                for each (var thing:* in reg) {
                    var prop:PropertyDescription = thing as PropertyDescription;
                    if (prop && prop.category == _model.selectedCategoryName) {
                        ribbons.push( {label:prop.label, data:prop.key} );
                    }
                }
                ribbons.sortOn("label");
                _model.ribbonsInCategory.source = ribbons;

                _model.selectedRibbon = ribbons[0];
                if (_model.selectedCategory && _model.selectedCategory.data == Modifiers.VISIBILITY) {  // select "visible" as a nice example
                    for (var n:int = 0; n<ribbons.length; n++){
                        var item:Object = ribbons[n];
                        if (item.data == "visible") {
                            _model.selectedRibbon = item;
                        }
                    }
                }
            }
        }

		
		private function get world():World
		{
			return Application.instance.document.root.value as World;
		}

	}
}