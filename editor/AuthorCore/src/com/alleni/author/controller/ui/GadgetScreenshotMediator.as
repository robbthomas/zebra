package com.alleni.author.controller.ui
{
	import com.alleni.author.application.ui.EditorUI;
    import com.alleni.author.definition.GadgetDescription;
    import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.definition.application.NotificationNamesEditor;
	import com.alleni.author.event.GadgetEvent;
	import com.alleni.author.model.World;
	import com.alleni.author.model.gadgets.GadgetScreenshot;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.service.gadgets.GadgetScreenshotOperation;
	import com.alleni.author.service.gadgets.GadgetScreenshotsOperation;
	import com.alleni.author.util.ViewUtils;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.controls.ScreenshotCaptureFrame;
	import com.alleni.author.view.ui.palettes.ScreenshotPanel;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.event.OperationEvent;
	import com.alleni.taconite.service.IOperation;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.net.FileReference;
	import flash.net.FileReferenceList;
	import flash.utils.Dictionary;
	
	public class GadgetScreenshotMediator extends GadgetImageMediator
	{
        public static const NEW_SHOT_CAPTURED:String = "New Shot Captured";

        public var cachedGadgetScreenShot:BitmapData;
        public var isZapp:Boolean;

        private var _compositeView:ObjectView;

        private static const DELETE:int = 0;
		private static var _instance:GadgetScreenshotMediator;
        private var _screenshotPanel:ScreenshotPanel;
        private var _currentScreenshotIndex:int = 0;
        private var _pendingUploads:Dictionary;
        private var _pendingDelete:Dictionary;

        private var _sessionUploads:Dictionary;
        private var _sessionDeletes:Dictionary;

        private var _frame:ScreenshotCaptureFrame;

        public var pendingScreenshotOperation:GadgetScreenshotOperation;

		public function GadgetScreenshotMediator()
		{
			Utilities.assert(!_instance);
		}
		
		public static function get instance():GadgetScreenshotMediator
		{
			if (!_instance)
				_instance = new GadgetScreenshotMediator();
			return _instance;
		}

        public function initializeScreenshotPanelFor(gadget:Project, compositeView:ObjectView = null, isZapp:Boolean = false):void
		{
            _gadget = gadget;
            _compositeView = compositeView;
            this.isZapp = isZapp;

            updateScreenShot();

            var op:GadgetScreenshotsOperation = _gadget.pendingScreenshotOperation as GadgetScreenshotsOperation;
            if(op) {
                _pendingUploads = op.pendingUploads;
                _pendingDelete = op.pendingDelete;
            } else {
                _pendingUploads = new Dictionary();
                _pendingDelete = new Dictionary();
            }
            if(_pendingUploads == null) {
                _pendingUploads = new Dictionary();
            }
            if(_pendingDelete == null) {
                _pendingDelete = new Dictionary();
            }
            _sessionDeletes = new Dictionary();
            _sessionUploads = new Dictionary();

            _screenshotPanel = new ScreenshotPanel(this, gadget);
            _screenshotPanel.y = -_screenshotPanel.height;
            _screenshotPanel.setupForGadget(_gadget);
            _screenshotPanel.visible = true;
            EditorUI.instance.addToBottomPalette(_screenshotPanel);
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SCREENSHOT_PANEL_SHOWN));

            ApplicationController.addEventListener(NotificationNamesEditor.DELETE_SELECTED, killScreenshotBar);
            ApplicationController.addEventListener(NotificationNamesApplication.CLOSE_PROJECT, killScreenshotBar);
            ApplicationController.addEventListener(NotificationNamesApplication.CREATE_NEW_PROJECT, killScreenshotBar);
            ApplicationController.addEventListener(NotificationNamesApplication.OPEN_LOCAL_PROJECT, killScreenshotBar);
            ApplicationController.addEventListener(NotificationNamesApplication.OPEN_PROJECT, killScreenshotBar);

            ApplicationController.addEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED, updateObjectCache);
        }

        public function get screenshotPanel():ScreenshotPanel
		{
            return _screenshotPanel;
        }

        private function updateScreenShot(event:ApplicationEvent = null):void{
            if(_compositeView != null){
                cachedGadgetScreenShot = captureObject(_compositeView);
            }
        }

        public function storeGadgetScreenshot():void
		{
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NEW_SHOT_CAPTURED, {
                        screenshot: new GadgetScreenshot(cachedGadgetScreenShot.clone(), false),
                        index:_screenshotPanel.thumbnails.activeScreenshot
                    }));
        }

        public function captureStage():void
		{
			hideFrame();
            const world:World = Application.instance.document.root.value as World;
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NEW_SHOT_CAPTURED, {
                        screenshot: new GadgetScreenshot(ViewUtils.instance.takeScreenshot(WorldContainer(Application.instance.viewContext).worldView, world.width, world.height), false),
                        index:_screenshotPanel.thumbnails.activeScreenshot
                    }));
        }


        override protected function handleOperationUpdate(event:Event):void {
            const name:String = _gadget.publishedName?_gadget.publishedName:GadgetDescription.cleanName(_gadget);
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:651}));
        }

        public function captureObject(view:ObjectView):BitmapData
		{
            var rotation:Number = view.rotation;
            view.rotation = -(rotation);
            var viewBitmapData:BitmapData = ViewUtils.instance.takeObjectScreenshot(view);
            view.rotation = rotation;
            return viewBitmapData;
        }

        public function deleteScreenshot():void
		{
            var index:int = _screenshotPanel.thumbnails.activeScreenshot;
            if(index in _pendingUploads){
                delete _pendingUploads[index];
                _screenshotPanel.thumbnails.removeThumbnail(index);
            }else if(_gadget.screenshots != null){
                for each(var screenshot:GadgetScreenshot in _gadget.screenshots.screenShots){
                    if(screenshot.visibleIndex == index){
                        _pendingDelete[screenshot.screenShotId] = screenshot;
                        _sessionDeletes[screenshot.screenShotId] = screenshot;
                        _screenshotPanel.thumbnails.removeThumbnail(index);
                        break;
                    }
                }
            }
        }

        private function updateObjectCache(event:Event):void{
            if (!_compositeView || !cachedGadgetScreenShot) return;

            cachedGadgetScreenShot = captureObject(_compositeView);
        }

        public function displayGadgetFrame():void
		{
			if (!_compositeView || !cachedGadgetScreenShot) return;

            cachedGadgetScreenShot = captureObject(_compositeView);
			
            _frame = new ScreenshotCaptureFrame(cachedGadgetScreenShot.width, cachedGadgetScreenShot.height, true);
            _frame.addChild(new Bitmap(cachedGadgetScreenShot.clone()));

            var location:Point = EditorUI.instance.projectContainer.globalToLocal(_compositeView.localToGlobal(new Point(_compositeView.object.left, _compositeView.object.top)));
            _frame.x = location.x;
            _frame.y = location.y;

            EditorUI.instance.projectContainer.addChild(_frame);
        }

        public function displayStageFrame():void
		{
			if (!Application.instance.document) return;
			const world:World = Application.instance.document.root.value as World;
			if (!_compositeView || !_compositeView.context.stage) {
				_compositeView = world.getView();
				if (!_compositeView || !_compositeView.context.stage) return;
			}
			const worldContainer:WorldContainer = _compositeView.context as WorldContainer;
			if (!worldContainer || !world) return;

			_frame = new ScreenshotCaptureFrame(world.width, world.height);
			var location:Point = worldContainer.worldView.localToGlobal(new Point(0, 0));
			_frame.x = location.x;
			_frame.y = location.y;
			_compositeView.context.stage.addChild(_frame);
        }

        public function hideFrame():void
		{
            if (!_frame || !_frame.parent) return;
			_frame.parent.removeChild(_frame);
			_frame = null;
        }

        public function killScreenshotBar(event:Event = null):void
		{
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SCREENSHOT_PANEL_CLOSED, this));
            if(_pendingUploads != null){
                _pendingUploads = null;
            }
            if(_pendingDelete != null){
                _pendingDelete = null;
            }
            if(_screenshotPanel != null){
                EditorUI.instance.removeFromBottomPalette(_screenshotPanel);
                _screenshotPanel = null;
            }
        }

        public function cancelScreenShotBar(event:Event = null):void{

            var shot:GadgetScreenshot;
            for each(shot in _sessionUploads){
                if(_pendingUploads[shot.visibleIndex] != null){
                    delete _pendingUploads[shot.visibleIndex];
                }
            }
            for each(shot in _sessionDeletes){
                if(_pendingDelete[shot.visibleIndex] != null){
                    delete _pendingUploads[shot.visibleIndex];
                }
            }
            if(_screenshotPanel != null){
                EditorUI.instance.removeFromBottomPalette(_screenshotPanel);
                _screenshotPanel = null;
            }
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SCREENSHOT_PANEL_CLOSED, this));
        }

        public function prepForPublishing():void
		{
            var operation:GadgetScreenshotsOperation = new GadgetScreenshotsOperation(_pendingDelete, _pendingUploads, _gadget);
            _gadget.pendingScreenshotOperation = operation;
            _screenshotPanel.visible = false;
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SCREENSHOT_PANEL_CLOSED, this));
        }

        override protected function handleFileSelected(event:Event=null):void
		{
			var fileList:Array = (event.currentTarget as FileReferenceList).fileList;
			var file:FileReference = fileList[0];

			var operation:IOperation = OperationFactory(file, null); // don't give it an ID yet so the data is only preloaded
			operation.addEventListener(GadgetEvent.ERROR, handleOperationError);
			operation.addEventListener(OperationEvent.UPDATE, handleOperationUpdate);
			operation.addEventListener(Event.COMPLETE, handleOperationComplete);
			operation.execute();
		}


        public function get sessionUploads():Dictionary{
            return _sessionUploads;
        }

        public function get pendingUploads():Dictionary
		{
            return _pendingUploads;
        }

        public function get pendingDeletes():Dictionary
		{
            return _pendingDelete
        }


        override protected function OperationFactory(file:FileReference, id:String):IOperation
		{
			pendingScreenshotOperation = new GadgetScreenshotOperation(file, id);
            return pendingScreenshotOperation;
		}

		override protected function handleOperationComplete(event:Event):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NEW_SHOT_CAPTURED, {
                   screenshot: new GadgetScreenshot(((event.target.loader as Loader).content as Bitmap).bitmapData, false),
                   index:_screenshotPanel.thumbnails.activeScreenshot
            }));
		}

		override protected function handleOperationError(event:Event):void
		{
            trace("screenshot ingestion operation Error")
		}
	}
}
