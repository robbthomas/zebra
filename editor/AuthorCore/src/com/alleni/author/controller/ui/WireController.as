package com.alleni.author.controller.ui
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.application.ApplicationUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.definition.CalcWireDescription;
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.OutletDescription;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.TriggeredWire;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.document.Document;
import com.alleni.author.event.WireEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.AbstractTable;
import com.alleni.author.model.objects.Calc;
import com.alleni.author.model.objects.LMS;
import com.alleni.author.model.objects.LogicObject;
import com.alleni.author.model.objects.ProjectObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.InternalSmartWireAnchor;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.model.ui.Wiring;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.objects.LogicObjectView;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.author.view.ui.PortView;
import com.alleni.author.view.ui.WireView;
import com.alleni.taconite.controller.ITaconiteController;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.service.LogService;

import flash.display.Stage;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.geom.Point;

import mx.collections.ArrayCollection;
import mx.collections.IList;

public class WireController extends EventDispatcher
	{

        public function get bindingsEnabled():Boolean {
            return bindingsDisabled != Application.instance.document.root.value;
        }

        public function set bindingsEnabled(value:Boolean):void {
            if(value) {
                bindingsDisabled = null;
            } else {
                bindingsDisabled = Application.instance.document.root.value as World;
            }
        }

        private var _bindingsDisabled:AbstractObject = null;

        public function get bindingsDisabled():AbstractObject {
            return _bindingsDisabled;
        }

        public function set bindingsDisabled(value:AbstractObject):void {
            _bindingsDisabled = value;
        }

        private var _ghostAnchor:WireAnchor = new WireAnchor();
        private var _requestUpdateVisibility:Boolean;
        private var _requestUpdateGeometry:Boolean;
		private var _redrawAll:Boolean;

//		private var _triggeredBlueAnchors:Vector.<WireAnchor> = new Vector.<WireAnchor>();
//		private var _triggeredBlueWires:Vector.<TriggeredWire> = new Vector.<TriggeredWire>();
//		private var _triggeredOrangeAnchors:Vector.<WireAnchor> = new Vector.<WireAnchor>();
//		private var _triggeredOrangeWires:Vector.<TriggeredWire> = new Vector.<TriggeredWire>();

        private var _triggeredAnchorsByDepth:Vector.<Vector.<WireAnchor>> = new Vector.<Vector.<WireAnchor>>();
        private var _triggeredWiresByDepth:Vector.<Vector.<TriggeredWire>> = new Vector.<Vector.<TriggeredWire>>();

		private var _doingBindings:Boolean;
		
		private static var _application:Application; // pointer local to this class for a little performance efficiency
		private static var _applicationController:ApplicationController; // pointer local to this class for a little performance efficiency

		public function WireController():void
		{
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_INITIALIZED, showWiresForLevel);
			ApplicationController.addEventListener(NotificationNamesApplication.MORE_WIRES, handleMoreWires);
			ApplicationController.addEventListener(NotificationNamesApplication.FEWER_WIRES, handleFewerWires);

			if (!_application)
				_application = Application.instance;
			if (!_applicationController)
				_applicationController = ApplicationController.instance;
		}
		
		private function handleMoreWires(event:ApplicationEvent = null):void
		{
			changeWiringLevel(false, ApplicationController.instance.authorController);
		}
		
		private function handleFewerWires(event:ApplicationEvent = null):void
		{
			changeWiringLevel(true, ApplicationController.instance.authorController);
		}

		public function get ghostAnchor():WireAnchor
		{
			return _ghostAnchor;
		}

		private function get document():Document
		{
			return _application.document;
		}

        private function get wiringModel():Wiring
        {
            return document.wiringRoot.value as Wiring;
        }

        private function get world():World
        {
            return document.root.value as World;
        }

        public function getWiringLevel():int{
            return Application.instance.projectWiringLevel;
        }

		public function createWire(anchor:WireAnchor):Wire
		{
			var wire:Wire = new Wire(anchor);
			//	wire.masterDescription = modifierDescription;
			wiringModel.wires.addItem(wire);
			wire.visible = true;
			return wire;
		}
		
		/*
		public function registerBinding(wire:Wire, master:Object, slave:Object, masterProperty:PropertyDescription, slaveProperty:PropertyDescription, fromCalc:Boolean=false):void
		{
			registerWire(wire, master.serial, slave.serial, masterProperty, slaveProperty);

			// initial binding
			var p1:String = ObjectProperties.instance.getPropertyForObject(master, masterProperty);
			var p2:String = ObjectProperties.instance.getPropertyForObject(slave, slaveProperty);
			if (!fromCalc && !slaveProperty.readOnly) // This works for initial binding, but model updates ultimately set the calc property to the slave property before outlet fires
				slave[p2] = master[p1];
		}
		*/

		public function deleteWire(wire:Wire):void
		{
			if(wiringModel.wires.getItemIndex(wire) >= 0){
                wiringModel.wires.removeItemAt(wiringModel.wires.getItemIndex(wire));
            }
			wire.masterAnchor.wired = getWiresForAnchor(wire.masterAnchor).length > 0;
			wire.slaveAnchor.wired = getWiresForAnchor(wire.slaveAnchor).length > 0;
			if (wire.slaveAnchor != null && wire.slaveAnchor != ghostAnchor) {
				wire.slaveAnchor.hostObject.model.dispatchEvent(new WireEvent(WireEvent.DETACH, wire, true, wire.slaveAnchor));
			}
			wire.masterAnchor.hostObject.model.dispatchEvent(new WireEvent(WireEvent.DETACH, wire, true, wire.masterAnchor));
			
			for (var depth:int = 0; depth < _triggeredWiresByDepth.length; depth++) {
				var triggeredWires:Vector.<TriggeredWire> = _triggeredWiresByDepth[depth];
				for (var n:int = triggeredWires.length-1; n >= 0; n--) {
					if (triggeredWires[n].wire == wire) {
						delete triggeredWires[n];
					}
				}
			}
		}
		
		public function  handleObjectDeleted(obj:AbstractObject):void
		{
			for each (var wire:Wire in getWires(obj)) {
				WireCreateUtil.unCreate(wire);
            }

			for (var depth:int = 0; depth < _triggeredAnchorsByDepth.length; depth++) {
				var triggeredAnchors:Vector.<WireAnchor> = _triggeredAnchorsByDepth[depth];
				for (var n:int = triggeredAnchors.length-1; n >= 0; n--) {
					if (triggeredAnchors[n].hostObject == obj) {
						delete triggeredAnchors[n];
					}
				}
			}
		}

		public function getBindings(object:ITaconiteObject):Array
		{
			return ArrayCollection(wiringModel.wires).source.filter(byBindings(object));
		}

		public function getTriggers(object:ITaconiteObject):Array
		{
			return ArrayCollection(wiringModel.wires).source.filter(byTriggers(object));
		}

		public function getWires(object:AbstractObject):Array
		{
			return ArrayCollection(wiringModel.wires).source.filter(byObject(object));
		}

        public function getWiresInScope(wireScopeContainer:AbstractContainer):Array
        {
            return ArrayCollection(wiringModel.wires).source.filter(function (item:Wire, index:int, arr:Array):Boolean {
                return wireScopeContainer.isEventualScopeContainer(item.masterAnchor.wiringScope) && wireScopeContainer.isEventualScopeContainer(item.slaveAnchor.wiringScope);
            });
        }

		public function getWiresExclusive(objects:Array):Array
		{
			return ArrayCollection(wiringModel.wires).source.filter(function (item:Wire, index:int, arr:Array):Boolean {
				return (objects.indexOf(item.masterAnchor.hostObject) >= 0) && (objects.indexOf(item.slaveAnchor.hostObject) >= 0);
			});
		}

		public function getWiresInclusive(objects:Array):Array
		{
			return ArrayCollection(wiringModel.wires).source.filter(function (item:Wire, index:int, arr:Array):Boolean {
				return ((objects.indexOf(item.masterAnchor.hostObject) >= 0 && ((item.masterAnchor is InternalSmartWireAnchor) == false))
						|| (objects.indexOf(item.slaveAnchor.hostObject) >= 0 && ((item.slaveAnchor is InternalSmartWireAnchor) == false)));
			});
		}

		public function getWiresForAnchor(anchor:WireAnchor):Array
		{
			return ArrayCollection(wiringModel.wires).source.filter(byAnchor(anchor));
		}

		public function objectHasWires(object:ITaconiteObject, modifierDescription:IModifier=null):Boolean
		{
			var hasWires:Boolean;

			if (modifierDescription != null) {
				hasWires = (ArrayCollection(wiringModel.wires).source.filter(byModifier(object, modifierDescription)).length > 0);
			} else {
				hasWires = (ArrayCollection(wiringModel.wires).source.filter(byMaster(object)).length > 0
					|| ArrayCollection(wiringModel.wires).source.filter(bySlave(object)).length > 0);
			}
			return hasWires;
		}

        private function getDepth(anchor:WireAnchor):int {
            var obj:AbstractObject = anchor.hostObject.parent;
            var result:int = 0; // start a depth 0 and go up by 2 as we go deeper
            if(anchor is InternalSmartWireAnchor) {
                result += 2; // internal anchors are inside so at a lower depth
            }
            while(obj != null) {
                if(obj is SmartObject) {
                    result += 2; // one depth lower for each composite
                }
                obj = obj.parent;
            }
            if(anchor.modifierDescription is PropertyDescription) {
                result += 1; // blue wires are one half depth lower to make them faster
            }
            return result;
        }

		public function anchorTriggered(anchor:WireAnchor):void
		{
            if(bindingsEnabled == false) {
                return;
            }
            if(anchor == null) {
                return;
            }
            if(anchor.modifierDescription is InletDescription && !(anchor as ExternalSmartWireAnchor || anchor as InternalSmartWireAnchor)) {
//                trace("Ignoring trigger for inlet", anchor.label);
                return;
            }
            if(anchor.modifierDescription is PropertyDescription && PropertyDescription(anchor.modifierDescription).writeOnly) {
//                trace("Ignoring trigger for writeonly property", anchor.label);
                return;
            }
            if(anchor.modifierDescription is OutletDescription && anchor.hostObject && !anchor.hostObject.enabled) {
                return; // Enabled- outs = F, so ignore this trigger request.
            }


            if(anchor.modifierDescription is PropertyDescription && (anchor is InternalSmartWireAnchor) && (anchor.host is EventPage) && EventPage(anchor.host).hasInternals) {
                // This anchor has passed the gauntlet and is being propagated inside
                // to a populated event.
                // So clear out the flag reminding us to propagate it.
                InternalSmartWireAnchor(anchor).other.valueNewerThanInside = false;
            }
            // PLAYERTODO: double check this
			var stage:Stage = _application.viewContext.stage;
			if (stage)
				stage.invalidate();

			var triggeredAnchors:Vector.<WireAnchor>;
			var triggeredWires:Vector.<TriggeredWire>;
            var depth:int = getDepth(anchor);
            while(depth >= _triggeredAnchorsByDepth.length) {
                _triggeredAnchorsByDepth.push(new Vector.<WireAnchor>());
                _triggeredWiresByDepth.push(new Vector.<TriggeredWire>());
            }
            triggeredAnchors = _triggeredAnchorsByDepth[depth];
            triggeredWires = _triggeredWiresByDepth[depth];

			var other:TriggeredWire;
//			trace("Trigger received for", anchor.hostObject, type(anchor), anchor.modifierDescription.label);
			if (triggeredAnchors.indexOf(anchor) == -1) {
				triggeredAnchors.push(anchor);
				var wires:Array = getWiresForAnchor(anchor);
//                if(wires.length > 0) {
//			        trace("Trigger received for", anchor.describe());
//                }
				var found:Boolean = false;
				for each (var w:Wire in wires) {
					var master:Boolean = w.masterAnchor == anchor;
//					trace("Testing", w.describe());
					if(master) {
						// remove any entry that would be setting the value through this wire from the slave end
						found = false;
						for each(other in triggeredWires) {
							if(other.wire == w) {
								if(other.propagated) {
//									trace("Found",other.describe());
									found = true;
									break;
								} else if(other.toSlave == false) {
//									trace("Removing",other.describe());
									other.removed = true;
									break;
								}
							}
						}
					} else {
						// dont add if the master end is a readonly proprty
						if(w.masterAnchor.modifierDescription as PropertyDescription && PropertyDescription(w.masterAnchor.modifierDescription).readOnly) {
//							trace("Other anchor is readonly");
							continue;
						}
						// dont add if the master end is already firing
						found = false;
						for each(other in triggeredWires) {
							if(other.wire == w && (other.toSlave || other.propagated)) {
//								trace("Found",other.describe());
								found = true;
								break;
							}
						}
					}
					if(found) {
						continue;
					}
					triggeredWires.push(new TriggeredWire(w, master, false, false));
//					trace("  Adding");
				}
			}
		}

        private function printStack(current:TriggeredWire):void {
            for(var depth:int = 0; depth < _triggeredWiresByDepth.length; depth++) {
                var triggeredWires:Vector.<TriggeredWire> = _triggeredWiresByDepth[depth];
                trace("   >>["+depth+"]");
                for(var index:int = 0; index < triggeredWires.length; index++) {
                    var triggeredWire:TriggeredWire = triggeredWires[index];
                    var isCurrent:String = triggeredWire == current ? "*" : " ";
                    trace("    >>["+index+"]", isCurrent, triggeredWires[index].describe());
                }
            }
        }

		public function handleBindings():void
		{
			if (bindingsEnabled) {
                var didBinding:Boolean = false;
//				if(_triggeredWiresByDepth.length > 0) {
//					trace(" >> Starting binding propagation");
//                    didBinding = true;
//				}
				_doingBindings = true;
                outerLoop: while(_triggeredWiresByDepth.length > 0) {
                    var currentDepth:int = _triggeredWiresByDepth.length - 1;
                    var triggeredWires:Vector.<TriggeredWire> = _triggeredWiresByDepth[currentDepth];
                    for each (var trigger:TriggeredWire in triggeredWires) {
                        if (trigger.removed || trigger.propagated || trigger.wire.masterAnchor == this.ghostAnchor || trigger.wire.slaveAnchor == this.ghostAnchor)  {
                            continue;
                        }
//                        printStack(trigger);
//                        trace("  >> Propagating " + trigger.describe());
                        trigger.propagated = true;
                        var anchor:WireAnchor = trigger.toSlave ? trigger.wire.masterAnchor : trigger.wire.slaveAnchor;
                        var otherAnchor:WireAnchor = trigger.toSlave ? trigger.wire.slaveAnchor : trigger.wire.masterAnchor;
                        if (trigger.wire.masterAnchor.modifierDescription as PropertyDescription) {
                            otherAnchor.value = anchor.value;
                        } else {
                            otherAnchor.handleTrigger();
                        }
                        var maxDepth:int = _triggeredWiresByDepth.length - 1;
                        if(maxDepth > currentDepth) {
                            continue outerLoop; // go back and get a new triggeredWires at a deeper level
                        }
                    }
                    _triggeredAnchorsByDepth.pop();
                    _triggeredWiresByDepth.pop();
                }
				_doingBindings = false;
//                if(didBinding) {
//                    trace(" >> Finished binding propagation");
//                }
                if(Application.running == false && Application.instance.editWhileRunningInProgress == false && Application.instance.tempEditComplete){
                    Application.instance.tempEditComplete = false;
                    Application.instance.document.running = true;
                    var world:World = document.root.value as World;
                    world.onResume();
                }
			}
		}

		public function handlePropertyWireManuallyPropagated(wire:Wire, toSlave:Boolean):void {
            var depth:int = getDepth(wire.masterAnchor);
            while(depth >= _triggeredAnchorsByDepth.length) {
                _triggeredAnchorsByDepth.push(new Vector.<WireAnchor>());
                _triggeredWiresByDepth.push(new Vector.<TriggeredWire>());
            }
            var triggeredWires:Vector.<TriggeredWire> = _triggeredWiresByDepth[depth];
			for each (var trigger:TriggeredWire in triggeredWires) {
				if(trigger.wire == wire) {
					if(trigger.toSlave == toSlave) {
						// found one about to be propagated, mark it already done
						trigger.propagated = true;
						return;
					} else {
						// found one about to be propagated from the other direction
						// mark it as unnecessary
						trigger.removed = true;
					}
				}
			}
			// add one in for this direction as already propagated
			triggeredWires.push(new TriggeredWire(wire, toSlave, false, true));
		}

		public function wireExists(anchor1:WireAnchor, anchor2:WireAnchor):Boolean
		{
			return getExistingWire(anchor1, anchor2) != null;
		}

        public function getExistingWire(anchor1:WireAnchor, anchor2:WireAnchor):Wire{
            if(anchor1 == anchor2)
				return null;
			var temp:Array = ArrayCollection(wiringModel.wires).source.filter(byAnchor(anchor1));
            var temp2:Array = temp.filter(byAnchor(anchor2))
            if(temp2.length > 0){
			    return temp2[0]
            } else {
                return null;
            }
        }

        public function findWireToObject(anchor:WireAnchor,  object:AbstractObject):Wire {
            var found:Array = ArrayCollection(wiringModel.wires).source.filter(byAnchorAndObject(anchor, object));
            if(found.length > 0) {
                return found[0];
            } else {
                return null;
            }
        }

		public function sendTrigger(object:ITaconiteObject, modifierDescription:IModifier):Boolean
		{
			var key:String = Modifiers.instance.getKeyFor(modifierDescription);
			var anchor:WireAnchor = (object as AbstractObject).anchors[key];
//			trace("sendTrigger: key="+key, "anchor="+anchor);
			anchorTriggered(anchor);
			return anchor && anchor.wired && anchor.hostObject && (anchor.hostObject.enabled || !(anchor.modifierDescription is OutletDescription));
		}

		private function byAnchor(searchAnchor:WireAnchor):Function
		{
			return function (item:Wire, index:int, arr:Array):Boolean {
				return (item.masterAnchor == searchAnchor || item.slaveAnchor == searchAnchor);
			}
		}

		private function byAnchorAndObject(searchAnchor:WireAnchor, object:AbstractObject):Function
		{
			return function (item:Wire, index:int, arr:Array):Boolean {
				return (item.masterAnchor == searchAnchor  && item.slaveAnchor.hostObject == object
                        || item.slaveAnchor == searchAnchor && item.masterAnchor.hostObject == object);
			}
		}

		private function byObject(searchObject:AbstractObject):Function
		{
			return function (item:Wire, index:int, arr:Array):Boolean {
				return (item.masterAnchor.hostObject == searchObject || item.slaveAnchor.hostObject == searchObject);
			}
		}

		private function byMaster(searchObject:ITaconiteObject):Function
		{
			return function (item:Wire, index:int, arr:Array):Boolean {
				return (item.masterAnchor.host == searchObject);
			}
		}

		private function bySlave(searchObject:ITaconiteObject):Function
		{
			return function (item:Wire, index:int, arr:Array):Boolean {
				return (item.slaveAnchor.host == searchObject);
			}
		}

		private function byBindings(searchObject:ITaconiteObject):Function
		{
			return function (item:Wire, index:int, arr:Array):Boolean {
				return ((item.masterAnchor.host == searchObject || item.slaveAnchor.host == searchObject) &&
					(item.masterAnchor.modifierDescription is PropertyDescription && item.slaveAnchor.modifierDescription is PropertyDescription));
			}
		}

		private function byTriggers(searchObject:ITaconiteObject):Function
		{
			return function (item:Wire, index:int, arr:Array):Boolean {
				return ((item.masterAnchor.host == searchObject || item.slaveAnchor.host == searchObject) &&
					(item.masterAnchor.modifierDescription is InletDescription && item.slaveAnchor.modifierDescription is OutletDescription
						|| item.masterAnchor.modifierDescription is OutletDescription && item.slaveAnchor.modifierDescription is InletDescription));
			}
		}

		private function byModifier(searchObject:ITaconiteObject, searchModifier:IModifier):Function
		{
			return function (item:Wire, index:int, arr:Array):Boolean {
				return (item.masterAnchor.host == searchObject && item.masterAnchor.modifierDescription == searchModifier)
				|| (item.slaveAnchor.host == searchObject && item.slaveAnchor.modifierDescription == searchModifier);
			}
		}

		/** Set the wiring level to the specified newLevel. */
		public function setWiringLevel(newLevel:int, statusMessage:Boolean = true):void
		{
			// Set wiring level
			Application.instance.projectWiringLevel = newLevel;
			showWiresForLevel();
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.WIRING_LEVEL_SET, newLevel));
            if (statusMessage) {
                wireLevelNotify(newLevel);
            }
		}
		
		private function wireLevelNotify(level:int) : void
		{
			var e:ApplicationEvent;
			switch (level) {
				case 0:
					e = new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:711}); break;
				case 1:
					e = new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:712}); break;
				case 2:
					e = new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:713}); break;
				case 3:
					e = new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:714}); break;
				case 4:
					e = new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:715}); break;
			}
			
			ApplicationController.instance.dispatchEvent(e);
		}

		private function get wiringLevel():int
		{
		    return Application.instance.projectWiringLevel;
		}


		/** Set the wiring level up or down one level depending on whether the shift key is true or false. */
		public function changeWiringLevel(shiftKey:Boolean, controller:ITaconiteController):void
		{
			// Set wiring level
			var newLevel:int = wiringLevel;
			if (shiftKey)
				newLevel -= (newLevel==0?0:1);
			else
				newLevel += (newLevel==4?0:1);

			setWiringLevel(newLevel);
		}

        public function messageCenterCreatedAndReady(mc:MessageCenterView):void
        {
            showWiresForLevel();
        }

		public function showWiresForLevel(event:Event=null): void
		{
            _requestUpdateVisibility = true;
            requestRenderEvent();
        }

        private function updateVisibility():void
        {
			switch (wiringLevel)
			{
				case 0: // Hide all wires and message centers
					// hide all message centers
					showAllMessageCenters(false);
					// hide all wires
					showAllWires(false);
					break;

				case 1: // Show message centers for selected objects
					// show message centers
					showMessageCentersForSelectedObjects();
					showAllWires(false);
					break;

				case 2: // Show wires between selected objects
					showMessageCentersForSelectedObjects();
					ApplicationUI.instance.addEventListener(Event.EXIT_FRAME, callShowSelectedWires);
					break;

				case 3: // Show all wires for selected objects
					showMessageCentersForSelectedObjects();
                    ApplicationUI.instance.addEventListener(Event.EXIT_FRAME, callShowRelatedWires);
					break;

				case 4:
					showAllMessageCenters(true);
					showAllWires(true);
					break;
			}
		}

        private function callShowRelatedWires(event:Event):void{
            ApplicationUI.instance.removeEventListener(Event.EXIT_FRAME, callShowRelatedWires);
            showRelatedWires();
        }

        private function callShowSelectedWires(event:Event):void{
            ApplicationUI.instance.removeEventListener(Event.EXIT_FRAME, callShowSelectedWires);
            showSelectedWires();
        }

		private function setVisibleIfSelected(obj:AbstractObject):Boolean
		{
            var vis:Boolean;
            var page:EventPage = obj as EventPage;
            if(page) {
                vis = page.needMessageCenter;
                obj.messageCenterVisible = vis;  // EventPageController also copies value from needMessageCenter to messageCenterVisible
                return vis && isSelected(obj);   // wires don't show unless obj selected, even if Event MC is showing
            } else if (obj.editing) {
                return false;  // leave text/path/composite MC as-is
            } else {
                vis = isSelected(obj) && !obj.childOfClosedComposite;
                obj.messageCenterVisible = vis;
                return vis;
            }
		}

		/**
		 * Show or hide all message centers
		 * @param objects The current list of objects
		 * @param show When true, show. When false, hide
		 *
		 */
		private function showAllMessageCenters(showAll:Boolean):void
		{
            var list:Vector.<AbstractObject> = world.allObjects();
            for each (var object:AbstractObject in list) {
                if(object is World) {
                    continue;
                }
                if(object.isInEventPage && Application.instance.flowVisible){
                    object.messageCenterVisible = false;
                    continue;
                }

                if((object.containingEventPage != AuthorController.instance.eventFlow.editingPage) && !(object is EventPage) && !(object is ProjectObject)){
                    object.messageCenterVisible = false;
                    continue;
                }
                var show:Boolean;
                var page:EventPage = object as EventPage;
                if (page) {
                    show = showAll && page.needMessageCenter;
                } else {
                    show = showAll && !object.childOfClosedComposite;
                }
                object.messageCenterVisible = show;
			}
		}

		/**
		 * Show only the message centers for selected objects. Hide the message centers for deselected objects
		 * @param objects The current list of objects
		 *
		 */
		private function showMessageCentersForSelectedObjects():void
		{
			var list:Vector.<AbstractObject> = world.allObjects();

			for each (var object:AbstractObject in list) {
                if(object is World) {
                    continue;
                }
				setVisibleIfSelected(object);
			}
		}

		/**
		 * Show or hide all the wires
		 * @param show When true, show. When false, hide.
		 *
		 */
		private function showAllWires(show:Boolean):void
		{
			//trace("show all wires",show);
			var wire:Wire;
			for each (wire in wiringModel.wires) {
                var master:WireAnchor = fixAnchor(wire.masterAnchor);
                var slave:WireAnchor = fixAnchor(wire.slaveAnchor);
                if(master && slave && AuthorController.instance.eventFlow.editingPage){
                    if(master.hostObject.containingEventPage != AuthorController.instance.eventFlow.editingPage && slave.hostObject.containingEventPage != AuthorController.instance.eventFlow.editingPage){
                        if((master.hostObject is ProjectObject && slave.hostObject is EventPage) ||  (slave.hostObject is ProjectObject && master.hostObject is EventPage) || (slave.hostObject is EventPage && master.hostObject is EventPage)){
                            wire.visible = true;
                        }else{
                            wire.visible = false;
                            continue;
                        }
                    }
                }
                if(master == null || slave == null) {
                    wire.visible = false;
                    continue;
                }
				var wireboardClosed:Boolean = (master is InternalSmartWireAnchor && !SmartObject(master.hostObject).activeVelum
					|| slave is InternalSmartWireAnchor && !SmartObject(slave.hostObject).activeVelum);
				
				var internalWiring:Boolean = isInternalWire(master, slave);

                function visibleLogicObject(obj:AbstractObject):Boolean {
                    return obj is LogicObject && obj.visible;
                }

				var vis:Boolean = (!wireboardClosed && !internalWiring) && show;
				vis &&= (master.hostObject.hasMessageCenter || visibleLogicObject(master.hostObject)) && (slave == ghostAnchor || slave.hostObject.hasMessageCenter || visibleLogicObject(slave.hostObject));
                vis &&= viewShowing(master.hostObject);  // detect offstage view, such as obj on hidden page of arena
                vis &&= viewShowing(slave.hostObject);

                var page:EventPage = master.hostObject as EventPage;
                if (page && (!page.needMessageCenter)) {
                    vis = false;
                }
                page = slave.hostObject as EventPage;
                if (page && (!page.needMessageCenter)) {
                    vis = false;
                }
                if((slave.hostObject && slave.hostObject.messageCenterVisible == false) || (master.hostObject && master.hostObject.messageCenterVisible == false)) {
                    vis = false;
                }
                wire.visible = vis;
			}
		}

        private function isInternalWire(masterAnchor:WireAnchor,  slaveAnchor:WireAnchor):Boolean
        {
            var master:AbstractObject = masterAnchor.hostObject;
            var slave:AbstractObject = slaveAnchor.hostObject;
            if ((master && master.childOfClosedComposite) || (slave && slave.childOfClosedComposite)) {
                return true;
            } else {
                return false;
            }
        }

		/**
		 * Show the wires for each selected object. Also show the message center at the other end of the wire
		 *
		 */
		private function showRelatedWires():void
		{
			for each (var wire:Wire in wiringModel.wires) {
                var master:WireAnchor = fixAnchor(wire.masterAnchor);
                var slave:WireAnchor = fixAnchor(wire.slaveAnchor);
                if(master == null || slave == null) {
                    wire.visible = false;
                    continue;
                }

                var wireboardClosed:Boolean = (master is InternalSmartWireAnchor && !master.hostObject.editing
											|| slave is InternalSmartWireAnchor && !slave.hostObject.editing);
								
                var internalWiring:Boolean = isInternalWire(master,  slave);
                if(slave.isGhost()){
                    wire.visible = true;
                    continue;
                }

                var vis:Boolean = (!wireboardClosed && !internalWiring && (isSelected(master.hostObject) || isSelected(slave.hostObject)));

                vis &&= master.hostObject.complete;
                vis &&= slave.hostObject.complete;

                var slaveHasMC:Boolean = slave.hostObject && slave.hostObject.hasMessageCenter;
                vis &&= (master.hostObject.hasMessageCenter || master.hostObject is LogicObject) && (slaveHasMC || slave.hostObject is LogicObject);
				
				if (vis && wire.attached) {
                    if (!(master.hostObject is EventPage) && !(master.hostObject is LogicObject))  master.hostObject.messageCenterVisible = true;
                    if (!(slave.hostObject is EventPage) && !(slave.hostObject is LogicObject))   slave.hostObject.messageCenterVisible = true;
				}

                if(master && slave){
                    if(master.hostObject.containingEventPage != AuthorController.instance.eventFlow.editingPage && slave.hostObject.containingEventPage != AuthorController.instance.eventFlow.editingPage){
                        if((master.hostObject is ProjectObject && slave.hostObject is EventPage) ||  (slave.hostObject is ProjectObject && master.hostObject is EventPage)  || (slave.hostObject is EventPage && master.hostObject is EventPage)){
                            //Vis should not need to change here;
                        }else{
                            wire.visible = false;
                            continue;
                        }
                    }
                }

				// Handling wires to objects that are not visible.
				//
				// Note: if objects are not visible, we will still show the wires if
				// the above conditions warrant showing the message center.  The point
				// is that we never show wires out to nothing.
				vis &&= viewShowing(master.hostObject);
                vis &&= viewShowing(slave.hostObject);
                wire.visible = vis;
			}
		}

        private function viewShowing(obj:AbstractObject):Boolean
        {
            if (obj == null) {   // normal for slave anchor when dragging out a new wire
                return false;
            }
            if (obj is ProjectObject || obj is LMS) {
                return true;
            }
            var page:EventPage = obj as EventPage;
            if (page) {
                if(Document(ApplicationController.instance.authorController.document).project.accountTypeId < 3) {
                    return true;
                }
                return page.needMessageCenter;
            } else {
                var view:ObjectView = obj.getView() as ObjectView;   // null if offstage, such as obj on hidden arena page
                if (view as LogicObjectView) {
                    return view.visible;  // tables have no MC
                } else if (view) {
                    var mc:MessageCenterView = view.messageCenterView;
                    if (mc && mc.visible && mc.readyForWires) {
                        return true;
                    }
                }
                return false;
            }
        }

		/**
		 * Show the wires between selected objects.
		 *
		 */
		private function showSelectedWires():void
		{
			for each (var wire:Wire in wiringModel.wires) {
                var master:WireAnchor = fixAnchor(wire.masterAnchor);
            	var slave:WireAnchor = fixAnchor(wire.slaveAnchor);
                if(master == null || slave == null) {
                    wire.visible = false;
                    continue;
                }
                if(master.hostObject != null && slave.hostObject != null){
				    wire.visible = setVisibleIfSelected(master.hostObject) && setVisibleIfSelected(slave.hostObject);
                }
			}
		}

		/**
		 * Create and return a list of CalcWireDescription objects, which allow treating the two-wire+calc combination
		 * as a single wire for the purposes of the show/hide wiring levels above.
		 * @return the list of CalcWireDescription objects
		 *
		 */
		private function get calcWires():IList
		{
			var calcWires:IList = new ArrayCollection();
			for each (var wire:Wire in wiringModel.wires) {
				if (wire.slaveAnchor.hostObject is Calc) {
					for each (var w:Wire in this.getWires(wire.slaveAnchor.hostObject))
						if (w.masterAnchor.hostObject is Calc)
							calcWires.addItem(new CalcWireDescription(wire, w));
				}
			}

			return calcWires;
		}

		private function isSelected(obj:ITaconiteObject=null):Boolean
		{
			if (!obj) return false;

			var result:Boolean = false;
			var model:TaconiteModel = obj.model;
			if (document.selection)
				result = document.selection.includes(model);

			return result;
		}


		/**
		 * Functions to request redraw of wires.
		 *
		 * The actual redraw is done later at a render event,
		 * and will apply to the currently active view (primary v/s presentation window).
		 * Both endpoints of the wire will be recalculated from model data.
		 * @param wire = the wire to be redrawn.
		 *
		 */
		public function requestRedrawWire(wire:Wire):void
		{
			wire.redraw = true;
            _requestUpdateGeometry = true;
			requestRenderEvent();
		}

		public function requestRedrawForObject(obj:AbstractObject):void
		{
			obj.redrawWires = true;
            _requestUpdateGeometry = true;
			requestRenderEvent();
		}

		public function requestRedrawForObjects(objModels:Vector.<TaconiteModel>):void
		{
			// mark each wire as needing redraw
			for each (var model:TaconiteModel in objModels) {
				var obj:AbstractObject = model.value as AbstractObject;
				obj.redrawWires = true;
			}
            _requestUpdateGeometry = true;
			requestRenderEvent();
		}

		public function requestRedrawAllWires():void
		{
			_redrawAll = true;
            _requestUpdateGeometry = true;
			requestRenderEvent();
		}


		private function requestRenderEvent():void
		{
            if(!_applicationController.invokedAsEditor) {
                return;
            }

			var stage:Stage = _application.viewContext.stage;
			if (stage) {
                stage.addEventListener(Event.RENDER, handleRender);
                stage.invalidate();
			}
		}


        private function handleRender(e:Event):void
        {
            if (_requestUpdateVisibility) {
                updateVisibility();
                _requestUpdateVisibility = false;
            }
            if (_requestUpdateGeometry) {
                updateGeometry();
                _requestUpdateGeometry = false;
            }
        }


		// for updateGeometry
		private var _container:WorldContainer;
		private var _objsReq:Vector.<AbstractObject> = new Vector.<AbstractObject>();  // list of objects that requested redraw, so we can clear redraw flags afterward
		private var _renderObj1:AbstractObject = null;
		private var _renderObj2:AbstractObject = null;
		
		private function updateGeometry():void
		{
			_container = WorldContainer(_application.viewContext);
			_renderObj2 = null;
			
			// scan all wires and process the affected ones
			for each (var wire:Wire in wiringModel.wires) {
				_renderObj1 = wire.masterAnchor.hostObject;
				if (wire.attached) {
					_renderObj2 = wire.slaveAnchor.hostObject;
				}
				// redraw if requested
				if (_redrawAll || wire.redraw || _renderObj1.redrawWires || (_renderObj2 && _renderObj2.redrawWires)) {
					if (wire.attached)
						setWireEndpoints(wire, _container);
					else
						setMasterEndpointOnDraggingWire(wire, _container);

					for each(var wireView:WireView in wire.getViews(_container)) {
						wireView.render();
					}

					// clear redraw requests
					wire.redraw = false;

					// defer clearing requests in objects, to ensure we update all the wires for each obj
					if (_renderObj1.redrawWires)
						_objsReq.push(_renderObj1);
					if (_renderObj2 && _renderObj2.redrawWires)
						_objsReq.push(_renderObj2);
				}
			}
			_redrawAll = false;

			// clear requests in objs
			for each (var ao:AbstractObject in _objsReq)
				ao.redrawWires = false;
				
			_objsReq.splice(0, _objsReq.length); // clear out request list
		}


		/**
		 * Set the endpoints of the wire, based on the object serials & modifiers in the wire.
		 * This function is also used for positioning Calc objects.
		 * @param wire
		 * @param role: indicates which view the wire is currently showing in.
		 *
		 */
		public function setWireEndpoints(wire:Wire, container:WorldContainer):void
		{
			// info about both ends
			var anchor1:WireAnchor = fixAnchor(wire.masterAnchor);
			var anchor2:WireAnchor = fixAnchor(wire.slaveAnchor);

			// in case we are called during file load, some structures may not yet exist
			if (_renderObj1 == null || _renderObj2 == null || anchor1 == null || anchor2 == null) {
				return;
			}

			var anchor1Ports:Vector.<PortView> = anchor1.getViews(container);
			var anchor2Ports:Vector.<PortView> = anchor2.getViews(container);

			if (anchor1Ports.length == 0 || anchor2Ports.length == 0) {
				return;
			}
			var anchor1Port:PortView;
			var anchor1Pos:Point;
			var anchor2Port:PortView;
			var anchor2Pos:Point;
			var distance:Number = -1;
			for each(var p1:PortView in anchor1Ports) {
				for each(var p2:PortView in anchor2Ports) {
					var pos1:Point = p1.localToGlobal(p1.wireEndpoint);
					var pos2:Point = p2.localToGlobal(p2.wireEndpoint);
					var d:Number = pos2.subtract(pos1).length;
					if(distance < 0 || d < distance) {
						anchor1Port = p1;
						anchor1Pos = pos1;
						anchor2Port = p2;
						anchor2Pos = pos2;
						distance = d;
					}
				}
			}

			anchor1Pos = container.globalToLocal(anchor1Pos);
			anchor2Pos = container.globalToLocal(anchor2Pos);

			wire.x = anchor1Pos.x;
			wire.y = anchor1Pos.y;
			wire.angle = anchor1Port.angle;

			wire.x2 = anchor2Pos.x;
			wire.y2 = anchor2Pos.y;
			wire.angle2 = anchor2Port.angle;
		}

		private function setMasterEndpointOnDraggingWire(wire:Wire, container:WorldContainer):void
		{
			// info about master end
			var anchor1:WireAnchor = wire.masterAnchor;

			anchor1.wired = true;
			// in case we are called during file load, some structures may not yet exist
			if (_renderObj1 == null || anchor1 == null) {
				return;
			}

			var anchor1Ports:Vector.<PortView> = anchor1.getViews(container);

			if (anchor1Ports.length == 0) {
				return;
			}
			var anchor1Port:PortView;
			var anchor1Pos:Point;
			var pos2:Point = container.localToGlobal(new Point(wire.x2, wire.y2));
			var distance:Number = -1;
			for each(var p1:PortView in anchor1Ports) {
				var pos1:Point = p1.localToGlobal(p1.wireEndpoint);
				var d:Number = pos2.subtract(pos1).length;
				if (distance < 0 || d < distance) {
					anchor1Port = p1;
					anchor1Pos = pos1;
					distance = d;
				}
			}

			anchor1Pos = container.globalToLocal(anchor1Pos);

			wire.x = anchor1Pos.x;
			wire.y = anchor1Pos.y;
			wire.angle = anchor1Port.angle;
		}
		
		public function get doingBindings():Boolean
		{
			return _doingBindings;
		}
		
		public function get triggersPending():Boolean
		{
			return (_triggeredAnchorsByDepth.length > 0 || _triggeredWiresByDepth.length > 0);
		}
		
		public function dumpWires():void
		{
			log("Wires:");
			for each (var wire:Wire in wiringModel.wires) {
				log("   "+wire);
			}
		}
		
		private function log(str:String):void
		{
			ApplicationController.instance.authorController.log(str);
		}



        public function fixAnchor(anchor:WireAnchor):WireAnchor {
            if(Document(ApplicationController.instance.authorController.document).project.accountTypeId < 3) {
                // non pro so no event flow
                // draw wires out of the one event as a single wire and not split at the event boundary
                var other:InternalSmartWireAnchor;
                var wires:Array;
                var otherWire:Wire;
                if(anchor is InternalSmartWireAnchor && anchor.hostObject is EventPage) {
                    return null;
                }
                if(anchor is ExternalSmartWireAnchor && anchor.hostObject is EventPage) {
                    other = ExternalSmartWireAnchor(anchor).other;
                    wires = getWiresForAnchor(other);
                    if(wires.length < 1) {
//                        LogService.error("Missing internal wire for creator edge anchor" + anchor.logStr);
                        return null;
                    }
                    otherWire = wires[0];
                    if(otherWire.masterAnchor == other) {
                        return otherWire.slaveAnchor;
                    } else if(otherWire.slaveAnchor == other) {
                        return otherWire.masterAnchor;
                    }
                }
            }
            return anchor;
        }
	}
}
