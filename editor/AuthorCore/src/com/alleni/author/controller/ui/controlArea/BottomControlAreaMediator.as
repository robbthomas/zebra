package com.alleni.author.controller.ui.controlArea
{
	import com.alleni.author.controller.app.CursorController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.CursorNames;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ui.controlArea.BottomControlArea;
	import com.alleni.author.view.ui.controls.PopOutMenu;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.events.PropertyChangeEvent;

	public class BottomControlAreaMediator
	{
		private var _view:BottomControlArea;
		
		public function BottomControlAreaMediator()
		{	
		}
		
		public function handleViewEvents(view:BottomControlArea):void
		{
			_view = view;
			_view.zoomPopOutMenu.addEventListener(PopOutMenu.ITEM_CHANGED, updateZoom);
			_view.addEventListener(MouseEvent.MOUSE_OVER, handleMouseOver);
			_view.addEventListener(MouseEvent.MOUSE_OVER, handleMouseOut);
			Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, applicationPropertyChangeListener)
			//ApplicationUI.instance.projectContainer.addEventListener(EditorEvent.DISPLAY_SCALE_CHANGE, _view.scaleChanged);
		}

		private function applicationPropertyChangeListener(event:PropertyChangeEvent):void {
			if(event.property == "zoomLevel") {
				var values:Array = _view.zoomPopOutMenu.dataProvider;
				if(event.newValue <= values[0].data) {
					_view.zoomPopOutMenu.updateSelectedItemByData(values[0].data);
					return;
				}
                var newZoom:Number = Application.instance.zoomLevel;
			    _view.zoomPopOutMenu.baseLabelOverrideString = "" + Math.floor(newZoom * 100) + "%";
			    _view.zoomPopOutMenu.updateSelectedItemByData(newZoom);
			}
		}
		
		public function clearViewEvents():void
		{
			_view.zoomPopOutMenu.removeEventListener(PopOutMenu.ITEM_CHANGED, updateZoom);
			//ApplicationUI.instance.projectContainer.removeEventListener(EditorEvent.DISPLAY_SCALE_CHANGE, _view.scaleChanged);
		}
		
		private function updateZoom(event:Event):void
		{
			ApplicationController.instance.handleSetViewScale(_view.zoomPopOutMenu.selectedItem.data as Number);
		}
		
		private function handleMouseOver(event:MouseEvent):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.CHANGE_CURSOR_TO, {name:CursorNames.ARROW_SELECT, type:CursorController.CURSOR_TYPE_STANDARD}));
		}
		
		private function handleMouseOut(event:MouseEvent):void{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_CURSOR));
		}
		
		//GETTERS and SETTERS
		public function get view():BottomControlArea
		{
			return _view;
		}
	}
}