package com.alleni.author.controller.ui
{
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.GadgetEvent;
    import com.alleni.author.model.project.Project;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.event.OperationEvent;
	import com.alleni.taconite.service.IOperation;
	import com.alleni.taconite.service.LogService;
	
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.FileReferenceList;

	public class GadgetImageMediator
	{
		private var _fileReferenceList:FileReferenceList;
		protected var _gadget:Project;
		
		protected var _targetOperationProperty:String;
		protected var _targetProperty:String;
		protected var _targetUserFacingName:String;
		
		public function GadgetImageMediator()
		{
		}
		
		public function showImportDialog(gadget:Project = null):void
		{
            if (gadget != null)
			    _gadget = gadget;

            if (gadget == null) return;
			
			_fileReferenceList = new FileReferenceList();
			_fileReferenceList.addEventListener(Event.SELECT, handleFileSelected);
			_fileReferenceList.addEventListener(Event.CANCEL, handleFileCancel);
			// FileReferenceList seems to inadvertently trigger IllegalOperationError as if there were multiple sessions
			// with no rhyme or reason, so we need to catch the resulting exception.
			try {
				_fileReferenceList.browse([new FileFilter("Images (*.jpg, *.jpeg, *.gif, *.png)", "*.jpg;*.jpeg;*.gif;*.png")]);
			} catch(e:IllegalOperationError) {
				LogService.error("GadgetImageMediator attempted a new browsing session before the previous had completed.");
			}
		}
		
		protected function handleFileSelected(event:Event=null):void
		{
			var fileList:Array = (event.currentTarget as FileReferenceList).fileList;
			var file:FileReference = fileList[0];
			
			var operation:IOperation = OperationFactory(file, null); // don't give it an ID yet so the data is only preloaded
			if (!operation) return;
			operation.addEventListener(GadgetEvent.ERROR, handleOperationError);
			operation.addEventListener(OperationEvent.UPDATE, handleOperationUpdate);
			operation.addEventListener(Event.COMPLETE, handleOperationComplete);
			
			_gadget[_targetOperationProperty] = operation;
			operation.execute();
		}
		
		protected function handleOperationUpdate(event:Event):void
		{
			const name:String = _gadget.publishedName?_gadget.publishedName:GadgetDescription.cleanName(_gadget);
			//ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:650}));
		}
		
		protected function OperationFactory(file:FileReference, id:String):IOperation
		{
			return null;
		}
		
		protected function handleOperationComplete(event:Event):void
		{
		}
		
		protected function handleOperationError(event:Event):void
		{
		}
		
		protected function handleFileCancel(event:Event):void
		{
			// nothing to do
		}
	}
}
