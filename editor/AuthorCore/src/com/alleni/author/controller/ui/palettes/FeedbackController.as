package com.alleni.author.controller.ui.palettes
{
	import com.alleni.author.application.palettes.FeedbackPanel;
	import com.alleni.author.application.palettes.QAPanel;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Feedback;
	import com.alleni.author.model.ui.QAVellumSettings;
	import com.alleni.author.service.FeedbackOperation;
	import com.alleni.taconite.controller.ui.SecondaryWindowController;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	import mx.core.IVisualElement;
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;
	
	public class FeedbackController extends SecondaryWindowController
	{
		private static const FEEDBACK_X:Number = 0;
		private static const FEEDBACK_Y:Number = 25;
		
		public static var instance:FeedbackController = new FeedbackController();
		
		private var _vellumWasVisible:Boolean = false;
		private var view:DisplayObject;
		
		public function FeedbackController()
		{
			Utilities.assert(!instance);
			super(new Rectangle(0, 0, 100, 100), "FEEDBACK", false, true);
			QAVellumSettings.vellum = TaconiteFactory.getQAVellumImplementation(_window);
			_window.addEventListener(Event.CLOSE, handleWindowClose);
			Feedback.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange, false, 0, true);
		}
		
		private function handlePropertyChange(event:PropertyChangeEvent):void
		{
			switch (event.property) {
				case "toggleVellum":
					QAVellumSettings.vellum.visible = Feedback.instance.toggleVellum;
					break;
				case "pending":
					if(event.newValue == false)
						reset();
					break;
				case "enabled":
				case "toggleAttachScreenshot":
					FeedbackOperation.instance.refreshScreenshot();
					break;
			}
		}
		
		private function reset():void
		{	
			var panel:FeedbackPanel = view as FeedbackPanel;
			if (panel)
				panel.resetForm();
		}
		
		private function handleWindowClose(event:Event=null):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.HIDE_QA_WINDOW));	
		}
		
		override public function setVisible(value:Boolean):void
		{
			super.setVisible(value);
			if (!value && QAVellumSettings.vellum.visible) {
				QAVellumSettings.vellum.visible = false;
				_vellumWasVisible = true;
			} else {
				QAVellumSettings.vellum.visible = _vellumWasVisible;
				_vellumWasVisible = false;
			}
			if (value) {
				// initialize the operation
				FeedbackOperation.instance.initialize();
			}
		}
		
		override public function exit():void
		{
			QAVellumSettings.vellum.close();
			super.exit();
		}
		
		override protected function handleWindowComplete(e:Event):void
		{
			if (TaconiteFactory.getEnvironmentImplementation().isDev)
				//view = new QAPanel();
				view = new FeedbackPanel();
			else
				view = new FeedbackPanel();
			view.addEventListener(FlexEvent.CREATION_COMPLETE, handleViewComplete);
			_window.addElement(view as IVisualElement);
		}
		
		private function handleViewComplete(event:Event):void
		{
			var view:DisplayObject = event.target as DisplayObject;
			_window.bounds = new Rectangle(FEEDBACK_X, FEEDBACK_Y, view.width, view.height);
		}
		
		override protected function activate():void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
		}
		
		override protected function deactivate():void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
		}
	}
}