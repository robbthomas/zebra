package com.alleni.author.controller.ui.palettes
{
	import com.alleni.author.application.palettes.PathEditingPanel;
	import com.alleni.author.application.palettes.PolygonEditingPanel;
	import com.alleni.author.application.palettes.ToolboxPanel;
	import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.objects.PathEditingMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.World;
import com.alleni.author.model.objects.EventFlowController;
import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.events.Event;
	
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;

	public class ToolboxController
	{
		private static var _instance:ToolboxController = null;

		public static var ARROW:int = 0;
		public static var ZOOM:int = 1;
		public static var MOVE:int = 2;
		public static var PENCIL:int = 3;
		public static var RECTANGLE:int = 4;
		public static var ELLIPSE:int = 5;
		public static var LINE:int = 6;
		public static var TEXT:int = 7;
		public static var AUDIO:int = 8;
		public static var INPUTTEXT:int = 9;
		public static var ARENA:int = 10;
		public static var BUTTON:int = 11;
		public static var RADIOBUTTON:int = 12;
		public static var CHECKBOX:int = 13;
		public static var SLIDER:int = 14;
		public static var CLOCK:int = 15;
		public static var TRUTHTABLE:int = 16;
		public static var ANSWERTABLE:int = 17;
		public static var STATETABLE:int = 18;
		public static var ANIMATION_PATH:int = 19;
		public static var VIDEO:int = 20;
		public static var MAP:int = 21;
		public static var GOTOURL:int = 22;
		public static var QUEUE:int = 23;
		public static var BROADCAST_CONNECTION:int = 24;
		public static var METRICS_CONNECTION:int = 25;
		public static var CALCULATOR:int = 26;
		public static var POLYGON:int = 27;

		public var currentToolEnabled:Boolean;
		
		public var drawingTools:Array = [ARROW, PENCIL];//[ARROW, PENCIL,RECTANGLE,ELLIPSE,LINE];
		
		private var _toolNameArray:Array = ["Arrow", "Zoom", "Move", "VectorDrawing", "RectangleObject", "Oval", "Line", 
			"TextObject", "Audio", "InputText", "Arena", "PushButton", "RadioButton", "Checkbox", "Slider", "Clock", "TruthTable", "AnswerTable", "StateTable",
			"PathObject", "VideoObject", "MapObject", "GotoURL", "QueueConnection", "BroadcastConnection", "MetricsConnection", "Calculator", "PathObject"];
		private var _previousVisibilityState:Boolean;
		private var _view:ToolboxPanel;
		
		public function ToolboxController()
		{
			if (_instance != null) throw new Error("Singleton, do not instantiate");
			
			ApplicationController.addEventListener(NotificationNamesApplication.UI_READY, handleUIReady);
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_TOOL_BOX, handleToggleToolBox);
            ApplicationController.addEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED, handleRunningStateChanged);

			_view = new ToolboxPanel();
		}
		
		public static function get instance():ToolboxController
		{
			if (_instance == null)
				_instance = new ToolboxController();
			return _instance;
		}
		
		public function getShortClassNameForTool(tool:int):String
		{
			return _toolNameArray[tool];
		}
		
		private function handleUIReady(event:ApplicationEvent = null):void{
			_previousVisibilityState = false;

			// Create the view
			EditorUI.instance.addFloatingPalette(_view);
            _view.visible = Application.instance.toolboxVisible;
			
			Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
		}

		private function handleToggleToolBox(event:ApplicationEvent):void{
            // this is responding directly to an author gesture
			Application.instance.toolboxVisible = !Application.instance.toolboxVisible;
            Application.instance.toolboxVisibleWhenPaused = Application.instance.toolboxVisible;
            if(Application.instance.toolboxVisible){
                ApplicationController.instance.authorController.toolboxToggledOn();
            }
		}

        private function handleRunningStateChanged(event:ApplicationEvent = null):void{
            if (Application.uiRunning) {
                Application.instance.toolboxVisible = false;
            } else {
                Application.instance.toolboxVisible = Application.instance.toolboxVisibleWhenPaused;
            }
        }

		/**
		 * Called when an Application property changes.  Update the UI accordingly. If the currentEditor property
		 * changes, update our listeners.
		 */
		private function handleApplicationPropertyChange(event:PropertyChangeEvent):void
		{
			var propertyName:String = event.property as String;
		}
		
		public function handleHideShowEvent(event:FlexEvent):void
		{
			// Propagate state from the view to the model.
			Application.instance.toolboxVisible = _view.visible;
		}
		
		public function handleXYChangedEvent(event:Event):void
		{
			// Propagate state from the view to the model.
			if (event.type == "xChanged") {
				Application.instance.toolboxX = _view.x;
			}
			else if (event.type == "yChanged") {
				Application.instance.toolboxY = _view.y;
			}
		}

		public function handleNewToolSelection(newTool:int):void
		{
			Application.instance.currentTool = newTool;
			
			if (newTool == ToolboxController.ANIMATION_PATH) {
				if (ApplicationController.instance.authorController.currentEditingMediator is PathEditingMediator == false) {
					// when author selects path tool, immediately show the path editing panel, so they can adjust smoothing
					ToolboxPanel.pathPanel.addChild(new PathEditingPanel());
				}
			} else if (newTool == ToolboxController.POLYGON && ToolboxPanel.pathPanel.numChildren==0) {
				ToolboxPanel.pathPanel.addChild(new PolygonEditingPanel());
			}
		}
	}
}
