package com.alleni.author.controller.ui
{
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Dock;
	import com.alleni.author.model.gadgets.Gadgets;
	import com.alleni.author.service.gadgets.GadgetIconOperation;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.service.IOperation;
	
	import flash.events.Event;
	import flash.net.FileReference;

	public class GadgetIconMediator extends GadgetImageMediator
	{
		private static var _instance:GadgetIconMediator;
		
		public function GadgetIconMediator()
		{
			super();
			Utilities.assert(!_instance);
			_targetOperationProperty = "pendingIconOperation";
			_targetProperty = "icon";
			_targetUserFacingName = "icon";
		}
		
		public static function get instance():GadgetIconMediator
		{
			if (!_instance)
				_instance = new GadgetIconMediator();
			return _instance;
		}
		
		override protected function OperationFactory(file:FileReference, id:String):IOperation
		{
			return new GadgetIconOperation(file, id);
		}

        override protected function handleOperationUpdate(event:Event):void {
            super.handleOperationUpdate(event);
        }

        override protected function handleOperationComplete(event:Event):void
		{
			var operation:GadgetIconOperation = event.target as GadgetIconOperation;
			if (!operation || !operation.gadget || !operation.id) return;
			
			const targetValue:* = operation.gadget[_targetProperty];
			_gadget[_targetProperty] = targetValue;
			_gadget[_targetOperationProperty] = null;
			const publishedId:String = operation.id;
			
			//ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:650}));
			
			// better way to do this? yup! this works for now to validate the functionality.
			var gadgets:Gadgets;
			var gadget:Project;
			gadgets = Dock.instance.pageList;
			if (gadgets) {
				gadget = gadgets.getById(publishedId);
				if (gadget) {
					gadget[_targetProperty] = targetValue;
					gadget[_targetOperationProperty] = null;
				}
			}
			gadgets = GadgetController.instance.model;
			if (gadgets) {
				gadget = gadgets.getById(publishedId);
				if (gadget) {
					gadget[_targetProperty] = targetValue;
					gadget[_targetOperationProperty] = null;
				}
			}
		}
		
		override protected function handleOperationError(event:Event):void
		{
			var operation:GadgetIconOperation = event.target as GadgetIconOperation;
			if (!operation || !operation.gadget || !operation.id) return;
			
			_gadget[_targetOperationProperty] = null;
			const publishedId:String = operation.id;
			
			// better way to do this? yup! this works for now to validate the functionality.
			var gadgets:Gadgets;
			var gadget:Project;
			gadgets = Dock.instance.pageList;
			if (gadgets) {
				gadget = gadgets.getById(publishedId);
				if (gadget) {
					gadget[_targetOperationProperty] = null;
				}
			}
			gadgets = GadgetController.instance.model;
			if (gadgets) {
				gadget = gadgets.getById(publishedId);
				if (gadget) {
					gadget[_targetOperationProperty] = null;
				}
			}
		}
	}
}
