package com.alleni.author.controller.ui
{
import com.alleni.author.Navigation.EmptyPageView;
import com.alleni.author.Navigation.PagerView;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.OutletDescription;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.definition.action.CustomRibbonAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.event.WireEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.LogicObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.InternalSmartWireAnchor;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.feedback.NotAllowedIcon;
import com.alleni.author.view.objects.ArenaPageView;
import com.alleni.author.view.objects.ArenaView;
import com.alleni.author.view.ui.PortView;
import com.alleni.author.view.ui.RibbonView;
import com.alleni.author.view.ui.WireView;
import com.alleni.author.view.ui.WireboardButton;
import com.alleni.author.view.ui.WireboardPortView;
import com.alleni.author.view.ui.WireboardView;
import com.alleni.taconite.controller.DragMediator;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObject;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Point;

import mx.collections.IList;

import flash.filters.DropShadowFilter;

public class WireDragMediator extends DragMediator
	{
		protected static var _applicationController:ApplicationController;
		
		// these fields are setup when mediator is created, or in handleViewEvents
		protected var _role:uint;
		
		// only for the WireDragMediator; the WireEditMediator does not use these
		private var _portView:PortView;
		
		// WireDragMediator sets these on handleViewEvents;  WireEditMediator sets them on mouseDown
		protected var _modifierDescription:IModifier;  // property or inlet/outlet this mediator is attached to
		protected var _hostObject:AbstractObject;  // object this mediator is attached to
		
		// WireDragMediator sets this on mouseDown;  WireEditMediator sets it on handleViewEvents
		protected var _wire:Wire;
		
		private var _origCenter:Point;
		
		public function WireDragMediator(context:ViewContext)
		{
			super(context);
			if (!_applicationController)
				_applicationController = ApplicationController.instance;
			this._stopPropagation = true;
		}
		
		/**
		 * When asked to work with a ObjectView, take note of the view and add a listener for mouseDown.
		 */
		public function handlePortEvents(view:PortView, role:uint):void 
		{
			_role = role;
			_portView = view;
			_portView.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			_modifierDescription = WireAnchor(_portView.model.value).modifierDescription;
			_hostObject = WireAnchor(_portView.model.value).hostObject as AbstractObject;
			
			// custom event to catch clicks on existing wires
			_portView.model.addEventListener(WireEvent.DRAG_NEW_WIRE,wireDragListener);
		}
		
		/**
		 * Listener for WireEvent.DRAG_NEW_WIRE:
		 * Dragging a new wire can begin by clicking on a port, which goes to super.handleMouseDown.
		 * Or, the user can click on an existing wire inside the port.
		 * In that case, WireEditMediator dispatches a custom event on the anchor model, received here.
		 * @param e
		 * 
		 */
		private function wireDragListener(e:WireEvent):void
		{
			if (e.role == _role) {
	ApplicationController.instance.authorController.log("WireDragMediator::wireDragListener  _hostObject= "+_hostObject+" _portView="+_portView);
				
				// ensure previous drag is cleaned up
    Utilities.assert(Application.instance.wireDragDescription == null);
    Utilities.assert(_portView != null, "Port View is null");
    Utilities.assert(_context != null, "Context is null");
    Utilities.assert(_context.stage != null, "Stage is null");

    ApplicationController.instance.authorController.log("WireDragMediator Line 103, WireDragListener")
				if (beginWireDrag()) {
    ApplicationController.instance.authorController.log("WireDragMediator Line 105, WireDragListener")
					_dragPoint = new Point(0, 0);
    ApplicationController.instance.authorController.log("WireDragMediator Line 107, WireDragListener")
					_dragStarted = true;
    ApplicationController.instance.authorController.log("WireDragMediator Line 109, WireDragListener")
					_context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
    ApplicationController.instance.authorController.log("WireDragMediator Line 111, WireDragListener")
					_context.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
    ApplicationController.instance.authorController.log("WireDragMediator Line 113, WireDragListener")
				}  // this function doesn't care about coordinates in the event
				e.stopImmediatePropagation();
    ApplicationController.instance.authorController.log("WireDragMediator Line 116, WireDragListener")
			}
		}
		
		
		override public function handleMouseDown(e:MouseEvent):void
		{
	ApplicationController.instance.authorController.log("WireDragMediator line 117");
			// ensure previous drag is cleaned up
			Utilities.assert(Application.instance.wireDragDescription == null);

			super.handleMouseDown(e);
			_dragPoint = new Point(WorldContainer(_context).mouseX, WorldContainer(_context).mouseY);
			_context.info.userActionInProgress = false;
		}
		
		override protected function get documentDragDelta():Point
		{
			var point:Point = new Point(WorldContainer(_context).mouseX, WorldContainer(_context).mouseY);
			return point.subtract(_dragPoint);
		}
		
		override protected function handleMouseMove(e:MouseEvent):void
		{
			if (!_dragStarted
				&& e.buttonDown
				&& _dragPoint != null
				&& (Math.abs(WorldContainer(_context).mouseX -_dragPoint.x) >= minimumDrag
					|| Math.abs(WorldContainer(_context).mouseY -_dragPoint.y) >= minimumDrag))
			{
				_dragStarted = beginWireDrag();
			}
			
			if (_dragStarted)
				handleDragMove(e);
			if (_stopPropagation)
				e.stopPropagation();
		}

		override protected function handleDragStart(e:MouseEvent):void
		{
			// decide whether its ok to drag a wire, and if so, create the wire
			// if not ok, then completely ignore this mouse click (eg. locked MC)
			beginWireDrag();
		}

		protected function beginWireDrag():Boolean
		{	

            _origCenter = WorldContainer(_context).centerOffset;
			// can't drag wire if MC is locked
			if (_hostObject ? _hostObject.wiringLocked : false)
				return false;
			var wireController:WireController = ApplicationController.instance.wireController;
    		var anchor:WireAnchor = _portView.anchor;
    		// create the wire
			_wire = wireController.createWire(anchor);
    		_wire.x2 = WorldContainer(_context).mouseX;
	        _wire.y2 = WorldContainer(_context).mouseY;
    		// request it to draw
			wireController.requestRedrawWire(_wire);
			// Set the description of the wire we are dragging
			var model:TaconiteModel = anchor.host.model;
	        Application.instance.wireDragDescription = new WireDragDescription(model, anchor, WorldContainer(_context));
    		// listener to get the minus key, to exclude msg ctr while dragging wire
			ApplicationController.addEventListener(NotificationNamesApplication.EXCLUDE_MSG_CTR, excludeMsgCtr);
			return true;
		}
		
		
		
		/**
		 * For each move during the drag, position the wire
		 */
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
		{
			//trace("WireDragMediator::handleDragMove:documentDragDelta: " + documentDragDelta);
			if (!_wire)
				return;
			
			// place wire at position just above cursor so we can drop it and drop detection works
			_wire.x2 = dragPoint.x + documentDragDelta.x - 1 / context.info.displayScale;
			_wire.y2 = dragPoint.y + documentDragDelta.y - 3 / context.info.displayScale;
			
			_wire.visible = true; // Wires should always be visible during wire drag, regardless of wiring level
			_applicationController.wireController.requestRedrawWire(_wire);

			// playing with scrollable world during wiring.
			// var current:Point = WorldContainer(_context).globalToLocal(new Point(e.stageX, e.stageY));
			//	WorldContainer(_context).centerOffset = _origCenter.subtract(current.subtract(_dragPoint));
			//WorldContainer(_context).reposition();
			
			
		}
		
		/**
		 * complete the wire or clear
		 * 
		 */
		override protected function handleDragEnd(e:MouseEvent) : void
		{
			ApplicationController.instance.authorController.log("\nWireDragMediator::handleDragEnd "+e.target);
			if (!_wire)
				return;

			// let everyone know the drag is completed (so don't open ribbons for us)
			if (Application.instance.wireDragDescription)
				Application.instance.wireDragDescription.completed = true;


			var dropPoint:DisplayObject;
			
			// Handle dropping wire on a ribbon ...
			var rb:RibbonView = null;
			var target:Object = e.target;
			while (!(target as RibbonView) && target != null) {
				target = target.parent;
			}


            rb = target as RibbonView;

			// ... or on a port (or child of a port (rollover hilite))
			var arena:Arena = null;
			if (rb) {
				dropPoint = rb.handleWireDrop(true);
				//var rbPt:Point = context.editor.contentPointFor(rb, new Point(0, 0));
				//dropPoint = rb.handleWireDrop(_wire.x<rbPt.x?true:false);  // returns a PortView
			}
			else if (e.target as PortView) {
				dropPoint = e.target as DisplayObject;
			} else if (e.target as DisplayObject && DisplayObject(e.target).parent as PortView) {
				dropPoint = DisplayObject(e.target).parent;
			} else if (e.target as ArenaView) {
				dropPoint = new PortView(Arena(ArenaView(e.target).model.value).anchors["pageNumber"], _context as WorldContainer);
				arena = Arena(ArenaView(e.target).model.value);
			} else if (e.target as Sprite && Sprite(e.target).parent as ArenaPageView) {
				dropPoint = new PortView((e.target.parent as ArenaPageView).arena.anchors["pageNumber"], _context as WorldContainer);
				arena = (e.target.parent as ArenaPageView).arena;
			} else if (e.target as ArenaPageView) {
                dropPoint = new PortView((e.target as ArenaPageView).arena.anchors["pageNumber"], _context as WorldContainer);
                arena = (e.target as ArenaPageView).arena;
			} else if (e.target as WireboardView) {
				dropPoint = e.target as WireboardView;
			} else if (e.target as WireboardButton) {
				dropPoint = WireboardButton(e.target).wireboard;
			} else if (e.target as WireView) {
				var wv:WireView = WireView(e.target);
				if (wv.model.value != _wire) {
					var pt1:Point = new Point(wv.wire.x, wv.wire.y);
					var pt2:Point = new Point(wv.wire.x2, wv.wire.y2);
					var current:Point = new Point(wv.parent.mouseX, wv.parent.mouseY);
					
					// If dropping on top of another wire end, set the drop point as that wire end's port view.
					if (Math.abs(pt1.subtract(current).length)<20) {
						dropPoint = wv.wire.masterAnchor.getViews(context as WorldContainer)[0];
					}
					else if (Math.abs(pt2.subtract(current).length)<20) {
						dropPoint = wv.wire.slaveAnchor.getViews(context as WorldContainer)[0];
					}
				}
			}

            if (arena != null && !(_wire.masterAnchor.modifierDescription is OutletDescription)) {
                dropPoint = null;
                arena = null;
            }
			if (_wire.masterAnchor is InternalSmartWireAnchor) {
				if (dropPoint is WireboardPortView || dropPoint is WireboardButton || dropPoint is WireboardView)
					dropPoint = null;  // prevent wiring from peg to peg (and prevent creation of custom ribbon)
			}


            if (dropPoint is PortView || dropPoint is WireboardView) {
            	finishWireDrag(dropPoint); // actually connect the wire
            } else { // not dragged to a port
            	cancelWire();
			}
            releaseWire();
			
			// playing with scrolling World
			///ApplicationUI.instance.presentationContainer.scrollToCenterOffset(_origCenter);
			
			// No longer dragging a wire - reset the wireDragDescription
			Application.instance.wireDragDescription = null;
			ApplicationController.removeEventListener(NotificationNamesApplication.EXCLUDE_MSG_CTR, excludeMsgCtr);
		}
		
		/**
		 * Clicked in a port and released mouse without moving 3-pixels. 
		 * @param e
		 * 
		 */
		override protected function handleClick(e:MouseEvent):void
		{
			trace("WireDragMediator::handleclick");
			if (_portView) {
				if(_portView.anchor.isAdditionAnchor) {
						IList(_portView.anchor.hostObject[_portView.anchor.hostProperty]).addItem("");
				} else {
					if(_portView.anchor.modifierDescription as InletDescription && _portView.anchor.hostObject as LogicObject) {
						_portView.anchor.handleTrigger();
					}
				}
			}
		}

        protected function releaseWire():void {
            ApplicationController.instance.authorController.log("releaseWire line 324");
            _wire = null;
        }

		private function finishWireDrag(dropPoint:DisplayObject):Boolean 
		{
			var drag:WireDragDescription = Application.instance.wireDragDescription;
            if(dropPoint == null) {
                cancelWire();
                }
            var recipe:* = WireCreateUtil.checkFinish(_wire,  dropPoint);
            if(recipe) {
                WireCreateUtil.actuallyFinish(recipe, _wire, drag.oldAnchor, drag.oldAnchorWasMaster, _role);
                return true;
            } else {
                if(drag.oldAnchor) {
                    _wire.finish(drag.oldAnchor);
                } else {
                    cancelWire(true);
                }
                return false;
            }

		}

		
		private function addCustomAnchor(fromAnchor:WireAnchor, wireboard:WireboardView):PortView
		{
			var smart:SmartObject = wireboard.object;
			var custom:ExternalSmartWireAnchor = smart.addCustomAnchor(fromAnchor);
            if(!custom) {
                return null;
            }
			var inner:InternalSmartWireAnchor = custom.other;
			var portView:PortView = wireboard.createPort(inner);  // sets inner.portSide
			ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(custom, true));
			return portView;
		}
		
		protected function cancelWire(illegal:Boolean = false):void
		{
            var slaveLocked:Boolean = (_wire.slaveAnchor.hostObject? _wire.slaveAnchor.hostObject.wiringLocked : false);
			var masterLocked:Boolean = (_wire.masterAnchor.hostObject? _wire.masterAnchor.hostObject.wiringLocked : false);
            if(!slaveLocked && !masterLocked) {
			    var description:WireDragDescription = Application.instance.wireDragDescription;
                WireCreateUtil.unCreate(_wire, description.oldAnchor != null, description.oldAnchor, description.oldAnchorWasMaster);
				Application.instance.wireDragDescription = null;
                _wire.illegal = true;
                if (illegal) {
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER,
                            ["This connection is not supported.","",null,noWiresAllowedFeedback(_wire)]));
                }
                releaseWire();
                ApplicationController.instance.authorController.log("releaseWire line 382");
			}
		}

        private function noWiresAllowedFeedback(wire:Wire):DisplayObject {
            var shadow:DropShadowFilter = new DropShadowFilter(2, 80, 0, 0.8, 6, 6, 1, 3);
            var wireView:WireView = new WireView(Application.instance.viewContext, wire.model,ViewRoles.PRIMARY);
            wireView.render();
            wireView.transform.matrix = Application.instance.viewContext.transform.concatenatedMatrix;
            var noIcon:Sprite = new NotAllowedIcon();
            noIcon.x = wire.x2;
            noIcon.y = wire.y2;

            wireView.addChild(noIcon);

            return wireView;
        }

		private function sameIPO_class(one:WireAnchor, two:WireAnchor):Boolean
		{
			if (one.modifierDescription as PropertyDescription && two.modifierDescription as PropertyDescription)
				return true;
			if (one.modifierDescription as InletDescription && two.modifierDescription as InletDescription)
				return true;
			if (one.modifierDescription as OutletDescription && two.modifierDescription as OutletDescription)
				return true;
			return false;
		}
				
		/**
		 * The minus key can be used during wire-drag to hide the most recently
		 * shown message center, so you can wire to other nearby objects.
		 * Once the minus key has been used on an object, that object is excluded from
		 * showing its MC until mouseUp. 
		 * 
		 */
		private function excludeMsgCtr(event:ApplicationEvent = null):void
		{
			trace("EXCLUDE MSG CTR");
			var drag:WireDragDescription = Application.instance.wireDragDescription;
			if (drag) {
				drag.excludeMC();
			}
		}
	}
}
