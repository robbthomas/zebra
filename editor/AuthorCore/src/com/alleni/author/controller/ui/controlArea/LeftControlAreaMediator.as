package com.alleni.author.controller.ui.controlArea
{
	import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
	import com.alleni.author.view.ui.controlArea.LeftControlArea;
	import com.alleni.author.view.ui.controls.Button;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;

import mx.events.PropertyChangeEvent;

public class LeftControlAreaMediator extends ControlAreaMediator
	{
		private var _view:LeftControlArea;
		
		function LeftControlAreaMediator():void
		{
            Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_LIBRARY, handleToggleLibrary);
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_VARIABLE_LIBRARY, handleToggleVariableLibrary);
			ApplicationController.addEventListener(NotificationNamesApplication.SHOW_QA_WINDOW, handleShowQAWindow);
			ApplicationController.addEventListener(NotificationNamesApplication.HIDE_QA_WINDOW, handleHideQAWindow);
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_QA_WINDOW, handleToggleQAWindow);
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_DOCK, handleToggleDock);
		}
		
		public function handleViewEvents(view:LeftControlArea):void
		{
			_view = view;
			
			_view.toolBoxButton.addEventListener(Button.CLICK, buttonClicked);
			_view.toolBoxButton.addEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.toolBoxButton.addEventListener(MouseEvent.ROLL_OUT, hideTip);
			
			_view.dockButton.addEventListener(Button.CLICK, buttonClicked);
			_view.dockButton.addEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.dockButton.addEventListener(MouseEvent.ROLL_OUT, hideTip);
			
			_view.libraryButton.addEventListener(Button.CLICK, buttonClicked);
			_view.libraryButton.addEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.libraryButton.addEventListener(MouseEvent.ROLL_OUT, hideTip);
			
            _view.variableLibraryButton.addEventListener(Button.CLICK, buttonClicked);
            _view.variableLibraryButton.addEventListener(MouseEvent.ROLL_OVER, showTip);
            _view.variableLibraryButton.addEventListener(MouseEvent.ROLL_OUT, hideTip);

            /*_view.qaButton.addEventListener(Button.CLICK, buttonClicked);
			_view.qaButton.addEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.qaButton.addEventListener(MouseEvent.ROLL_OUT, hideTip);*/
            setButton(_view.toolBoxButton, Application.instance.toolboxVisible);
		}
		
		public function clearViewEvents():void
		{
			_view.toolBoxButton.removeEventListener(Button.CLICK, buttonClicked);
			_view.toolBoxButton.removeEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.toolBoxButton.removeEventListener(MouseEvent.ROLL_OUT, hideTip);
			
			_view.dockButton.removeEventListener(Button.CLICK, buttonClicked);
			_view.dockButton.removeEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.dockButton.removeEventListener(MouseEvent.ROLL_OUT, hideTip);
			
			_view.libraryButton.removeEventListener(Button.CLICK, buttonClicked);
			_view.libraryButton.removeEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.libraryButton.removeEventListener(MouseEvent.ROLL_OUT, hideTip);
			
			_view.variableLibraryButton.removeEventListener(Button.CLICK, buttonClicked);
			_view.variableLibraryButton.removeEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.variableLibraryButton.removeEventListener(MouseEvent.ROLL_OUT, hideTip);
			
			/*_view.qaButton.removeEventListener(Button.CLICK, buttonClicked);
			_view.qaButton.removeEventListener(MouseEvent.ROLL_OVER, showTip);
			_view.qaButton.removeEventListener(MouseEvent.ROLL_OUT, hideTip);*/
		}

        private function handleApplicationPropertyChange(event:PropertyChangeEvent):void
        {
            var property:String = event.property as String;
            var newValue:* = event.newValue;
            switch (property) {
                case "toolboxVisible":
                    setButton(_view.toolBoxButton, newValue as Boolean);
                    break;
            }
        }

		private function handleToggleLibrary(event:ApplicationEvent = null):void
		{
			toggleButton(_view.libraryButton);
            if (!(_view.libraryButton.selected && !_view.variableLibraryButton.selected))
                setButton(_view.variableLibraryButton, false);
		}
		
		private function handleToggleVariableLibrary(event:ApplicationEvent = null):void
		{
			toggleButton(_view.variableLibraryButton);
            if (!(_view.variableLibraryButton.selected && !_view.libraryButton.selected))
                setButton(_view.libraryButton, false);
		}
		
		
		private function handleShowQAWindow(event:ApplicationEvent = null):void
		{
			setButton(_view.qaButton, true);
		}
		
		private function handleHideQAWindow(event:ApplicationEvent = null):void
		{
			setButton(_view.qaButton, false);
		}
		
		private function handleToggleQAWindow(event:ApplicationEvent = null):void
		{
			toggleButton(_view.qaButton);
		}
		
		private function handleToggleDock(event:ApplicationEvent = null):void
		{
			setButton(_view.dockButton, Application.instance.dockVisible);
		}
		
		private function showTip(event:MouseEvent):void
		{
			var button:Button = event.currentTarget as Button;
			var point:Point = new Point(25, -3);
			ApplicationController.instance.displayToolTip(button.localToGlobal(point), ControlAreaToolTip.DISPLAY_RIGHT, button.toolTip, "Arrow Show");
		}
		
		private function buttonClicked(event:Event):void
		{
			var id:String = event.currentTarget.id;
			switch(id){
				case "toolBoxButton":
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_TOOL_BOX));
					break;
                case "libraryButton":
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_LIBRARY));
					break;
                case "variableLibraryButton":
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_VARIABLE_LIBRARY));
                    break;
				case "qaButton":
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_QA_WINDOW));
					break;
				case "dockButton":
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_DOCK));
					break;
			}
		}
	}
}
