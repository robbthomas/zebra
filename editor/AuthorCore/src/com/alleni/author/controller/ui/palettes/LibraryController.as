package com.alleni.author.controller.ui.palettes
{
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.definition.action.ActionGroup;
import com.alleni.author.definition.action.DestroyObjectAction;
import com.alleni.author.definition.action.LibraryRemoveAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.IListable;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.Library;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.view.ui.palettes.LibraryView;
	import com.alleni.author.view.ui.palettes.VariableLibraryView;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.TaconiteModel;

import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
import flash.events.Event;
import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
	import mx.collections.IList;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
import mx.events.PropertyChangeEvent;

public class LibraryController
	{
		private static var _instance:LibraryController;
		
		private var _model:Library;
		private var _view:LibraryView;  
		private var _variableLibraryView:VariableLibraryView;  
		private var _container:DisplayObjectContainer;
		
		public function LibraryController()
		{
			Utilities.assert(!_instance);
			
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_LIBRARY, handleToggleLibrary);
            Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
			ApplicationController.addEventListener(NotificationNamesApplication.TOGGLE_VARIABLE_LIBRARY, handleToggleVariableLibrary);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_CLOSING, handleProjectClosing);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_CHANGING, handleProjectChanging);
		}
		
		public static function get instance():LibraryController
		{
			if (!_instance)
				_instance = new LibraryController();
			return _instance;
		}
		
		private function get model():Library
		{
			if (!_model)
				_model = Library.instance;
			return _model;
		}
		
		public function set container(value:DisplayObjectContainer):void
		{
			_container = value;
		}
		
		public function getViewIfOver(e:MouseEvent):DisplayObject
		{
			if (!_view || !_view.stage)
				return null;
			
			if (_view.hitTestPoint(e.stageX, e.stageY, true))
				return _view;
			
			return null;
		}
		
		private function handleToggleLibrary(event:ApplicationEvent):void
		{
            if(_variableLibraryView) {
                _variableLibraryView.setVisible(false);    // Library & Variables hide each other
            }

            // toggling is done by ApplicationController listening to Application.libraryVisible and telling EditorUI to show/hide the left panel
			if (!_view) {			
				_view = new LibraryView();
				_container.addChild(_view);
                updateList();
			}
            _view.visible = true;
		}

		private function handleToggleVariableLibrary(event:ApplicationEvent):void
		{
			if(_view) {
                _view.visible = false;   // Library & Variables hide each other
            }

			if (!_variableLibraryView) {			
				_variableLibraryView = new VariableLibraryView(); 
				_container.addChild(_variableLibraryView);
			}
            _variableLibraryView.setVisible(true);
		}
		
		private function handleProjectClosing(event:ApplicationEvent):void
		{
			removeListeners();
			updateList();
		}
		
        private function handleProjectChanging(event:ApplicationEvent):void
        {
            addListeners();
            updateList();
        }

        public function updateForEventGadgetToggle():void
        {
            updateList();
        }

        private function updateList():void
        {
            var pageList:ArrayCollection = new ArrayCollection();
            pageList.addAll(AssetController.instance.availableModel);
            pageList.addAll(GadgetController.instance.availableModel);

            model.pageList = pageList;

            if (model.selectedItem && model.pageList.getItemIndex(model.selectedItem) < 0)
                model.selectedItem = null;
            updateForAssetChange();
        }

        /**
         * Update reference-counts in the library, for Assets and Gadgets.
         * This is optimized to not execute while library is not visible.
         * ... since it scans all objects and all wire-anchors in the entire document.
         */
        public function updateForAssetChange(force:Boolean=false):void
        {
            var flow:EventFlow = AuthorController.instance.eventFlow;
            if (flow && _view && Application.instance.libraryVisible && (force || (!flow.lazyLoading))) {
                Application.instance.viewContext.stage.addEventListener(Event.RENDER, updateForAssetChangeNow);
            }
        }

        private function updateForAssetChangeNow(event:Event=null):void
        {
            Application.instance.viewContext.stage.removeEventListener(Event.RENDER, updateForAssetChangeNow);
            var usages:UsageCounts = new UsageCounts();
            AuthorController.instance.eventFlow.getUsageCounts(usages);
            model.usageCounts = usages;
        }

        private function handleApplicationPropertyChange(event:PropertyChangeEvent):void
        {
            switch (event.property) {
                case "libraryVisible":
                    if (event.newValue) {
                        updateForAssetChange(true);
                    }
                    break;
            }
        }

		private function addListeners():void
		{
			AssetController.instance.model.assets.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleCollectionChange);
			GadgetController.instance.model.gadgets.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleCollectionChange);
		}
		
		private function removeListeners():void
		{
			AssetController.instance.model.assets.removeEventListener(CollectionEvent.COLLECTION_CHANGE, handleCollectionChange);
			GadgetController.instance.model.gadgets.removeEventListener(CollectionEvent.COLLECTION_CHANGE, handleCollectionChange);
		}
		
		public function alreadyHasItem(id:String):Boolean
		{
			return AssetController.instance.model.getById(id) || GadgetController.instance.model.getById(id);
		}
		
		private function handleCollectionChange(e:CollectionEvent):void
		{
			switch (e.kind) {
				case CollectionEventKind.ADD:
                    updateList();
					model.selectedItem = IList(e.target).getItemAt(e.location) as IListable;
                    break;
				case CollectionEventKind.REMOVE:
					updateList();
					break;
			}
		}

        public function deleteFromProject(item:IReplaceable):void
        {
            var group:ActionGroup = new ActionGroup("Delete Instances of Resource");

            var objects:Vector.<TaconiteModel> = AuthorController.instance.getObjectModelsForReplaceable(item);
            var objDeletion:DestroyObjectAction = DestroyObjectAction.fromSelection(objects);
            objDeletion.perform();
            group.add(objDeletion);

            var usages:UsageCounts = new UsageCounts();
            AuthorController.instance.eventFlow.getUsageCounts(usages); // scan entire project
            if (usages.getReplaceableUsageCount(item) == 0) {
                var libraryDeletion:LibraryRemoveAction = LibraryRemoveAction.fromItem(item);
                libraryDeletion.perform();
                group.add(libraryDeletion);
            }
            ApplicationController.instance.authorController.currentActionTree.commit(group);
            updateForAssetChange();
        }

	}
}