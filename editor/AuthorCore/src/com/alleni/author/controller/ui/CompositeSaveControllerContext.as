/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 10/8/12
 * Time: 10:41 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.ui {
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.model.project.Project;

public interface CompositeSaveControllerContext {
    function findByUID(uid:String):CompositeSaveControllerTarget;
    function selectOuterGadget(outerClosingVellum:CompositeSaveControllerTarget):void;
    function get actionTree():ActionTree;
    function findInstances(gadget:Project,  predicate:Function):Vector.<CompositeSaveControllerTarget>;
}
}
