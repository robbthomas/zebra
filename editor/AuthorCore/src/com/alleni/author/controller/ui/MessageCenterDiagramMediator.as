package com.alleni.author.controller.ui
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.controller.app.CursorController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.WireEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractContainer;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.objects.GadgetView;
	import com.alleni.author.view.ui.MessageCenterView;
	import com.alleni.taconite.controller.TaconiteController;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.MouseCursor;
	
	import mx.collections.IList;
	
	public class MessageCenterDiagramMediator extends MessageCenterMediator
	{
		private var _gadget:Boolean; // True if this is a gadget (since gadgets behave like MCs in the diagram window)
		
		public function MessageCenterDiagramMediator(context:ViewContext, gadget:Boolean=false)
		{
			super(context);
			
			_gadget = gadget;
		}
		
		override public function handleViewEvents(view:ITaconiteView, role:uint):AbstractObjectDragMediator
		{
			if (_gadget) {
				_view = GadgetView(view);
			} else {
				_view = MessageCenterView(view);
				view.model.addEventListener(WireEvent.COMPLETE, wireCompleteListener);
			}
			
			_view.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			
			return this;			
		}
		
		/**
		 * At the start of a drag, capture the positions of all selected objects so that we
		 * can move them all by the same delta later on.
		 */
		override protected function handleDragStart(e:MouseEvent):void
		{
			trace("MessageCenterDiagramMediator::handleDragStart", _view, e);
			
			// Move object
			Application.instance.draggingMessageCenter = true;

			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.CHANGE_CURSOR_TO, {name:MouseCursor.HAND, type:CursorController.CURSOR_TYPE_STANDARD}));
			
			if (!context.controller.selection.contains(_view.model))
			{
				context.controller.selectSingleModel(_view.model);
			}
			
			_oldPositions = [];
			for each (var m:TaconiteModel in context.controller.selection.selectedModels) {
				_oldPositions.push(new Point(m.value["xDiagram"], m.value["yDiagram"]));
			}
		}
		
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
		{
			var i:int = 0;
			for each (var m:TaconiteModel in context.controller.selection.selectedModels) {
				var obj:AbstractObject = m.value as AbstractObject;
				var view:ObjectView = obj.getView(context);
				if (view)
					view.updateDiagramPosition(Point(_oldPositions[i]).add(documentDragDelta));
				++i;
			}
		}
		
		override public function updateViewPosition():void
		{
			_view.visible = object.active; // active page of flipbook: implies showing the MC
			if (object.active) {
				if (isNaN(object.xDiagram)) // If object is being positioned initially
					positionMC(object);  // compute and store xDiagram, yDiagram
				_view.x = object.xDiagram;
				_view.y = object.yDiagram;// + MessageCenterView.yOffset;
				
				if (_view is MessageCenterView)  // MCs are placed downward to leave room for thumbnail
					_view.y += MessageCenterView.yOffset;
				
				ApplicationController.instance.wireController.requestRedrawForObject(object);
			}
		}
		
		/**
		 * Initially position a message center on the diagram.
		 * Formerly called clearSpot(). 
		 * @param model = the object owning the MC being placed.
		 * 
		 */
		private function positionMC(obj:AbstractObject):void {
			
			const H_MARGIN:Number = 20;
			
			// get the bounds of all the existing MCs
			var boundsList:Array = [];  // Array of Rectangle
			getBoundsOfObjects(context.document.root.valueChildren, boundsList);
			
			// form a default rect based on the obj.y  (designers want MC placed at same Y as object)
			var rectangle:Rectangle = new Rectangle(H_MARGIN,obj.y, MessageCenterView.WIDTH, MessageCenterView.HEIGHT);
			
			// loop on columns across the view, until a clear spot is found
			while (1) { 
				var anotherRectangle:Rectangle = overlappingRect(rectangle, boundsList);
				if (anotherRectangle) {
					// move across to next column, 
					// from the one we hit ... otherwise we waste space with irregular spacing
					rectangle.x = anotherRectangle.right + H_MARGIN;  
				} else {
					obj.xDiagram = rectangle.x;
					obj.yDiagram = rectangle.y;
					return;  // all done
				}
			}
			
			ApplicationController.instance.wireController.requestRedrawForObjects(context.controller.selection.selectedModels);
		}
		
		private function getBoundsOfObjects(objects:IList, boundsList:Array):void
		{
			for each (var obj:AbstractObject in objects)
			{
				// put only valid coordinates of active objects onto the list
				if (!isNaN(obj.xDiagram) && obj.active) {
					var rect:Rectangle = new Rectangle(obj.xDiagram, obj.yDiagram, MessageCenterView.WIDTH, MessageCenterView.HEIGHT);
					boundsList.push(rect);
				}
				
				// recurse to get children's bounds
				if (obj is AbstractContainer) {
					var children:IList = AbstractContainer(obj).objects;
					getBoundsOfObjects(children,boundsList);
				}
			}
		}
		
		private function overlappingRect(rect:Rectangle, boundsList:Array):Rectangle
		{
			for each (var bounds:Rectangle in boundsList) {
				if (bounds.intersects(rect))
					return bounds;
			}
			return null;  // nothing intersected
		}
	}
}