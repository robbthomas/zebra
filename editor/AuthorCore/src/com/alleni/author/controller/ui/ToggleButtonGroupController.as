package com.alleni.author.controller.ui
{
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.ToggleButton;
	import com.alleni.author.model.objects.ToggleButtonGroup;
	import com.alleni.author.model.ui.Application;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.TaconiteModel;
	
	import flash.utils.Dictionary;
	
	import mx.utils.UIDUtil;

	public class ToggleButtonGroupController
	{
		private static var _instance:ToggleButtonGroupController = null;
		
		public static function get instance():ToggleButtonGroupController
		{
			if (_instance == null)
				_instance = new ToggleButtonGroupController();
			return _instance;
		}
		
		public function ToggleButtonGroupController()
		{
		}
		
		public function makeToggleButtonGroup(list:Vector.<ToggleButton>):void
		{
			var buttonGroup:ToggleButtonGroup = new ToggleButtonGroup();
			var firstOne:ToggleButton = null;
			for each (var btn:ToggleButton in list) {
				btn.toggleButtonGroup = buttonGroup;
				btn.showingGroupIndicator = true;
                btn.toggleButtonValue = "";
				if (firstOne == null) {
					firstOne = btn;
				}
			}
            if (makeButtonsNonMomentary(firstOne)) {
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, ["Momentary button", "Changed to non-momentary"]));
            }
			resolveToggleButtonGroupValues(firstOne);
			firstOne.checked = true;
			ApplicationController.instance.authorController.clearSelection();
		}
		
		private function resolveToggleButtonGroupValues(example:ToggleButton):void
		{
			trace("resolveToggleButtonGroupValues: example="+example);
            var list:Vector.<ToggleButton> = buttonListFromExample(example);

			trace("resolveToggleButtonGroupValues:");
			var btn:ToggleButton, val:String;
			var values:Dictionary = new Dictionary();
			var highest:int = 0;
			var duplicates:Vector.<ToggleButton> = new Vector.<ToggleButton>();
			for each (btn in list) {
				val = btn.toggleButtonValue;
				trace("   label="+btn.label, "val="+val);
				if (val != null && val != "") {
					if (values[val] != null) {
						duplicates.push(btn);
					} else {
						values[val] = btn;
						var num:int = int(val);
						if (num > highest)
							highest = num;
					}
				}
			}
			
			trace("resolveToggleButtonGroupValues: highest="+highest);
			for each (btn in list) {
				val = btn.toggleButtonValue;
				if (val == null || val == "" || duplicates.indexOf(btn) >= 0) {
					btn.toggleButtonValue = ToggleButton.DEFAULT_VALUE_PREPEND+(++highest).toString(); // adding "B" for button, to make it more clear that this is a string (per discussion with Dr. Allen) - SBJ
					trace("   val="+btn.toggleButtonValue, "init="+btn.initialValues.toggleButtonValue);
				}
				/*if (btn.label.indexOf(ToggleButton.DEFAULT_TEXT) == 0) { // begins with "Untitled"
					btn.label = ToggleButton.DEFAULT_TEXT + " " + btn.toggleButtonValue;
					trace("   label="+btn.label, "init="+btn.initialValues.label, "text="+btn.initialValues["text"]);
				}*/
			}
		}

        private function makeButtonsNonMomentary(example:ToggleButton):Boolean
        {
            trace("makeButtonsMomentary: example="+example);
            var list:Vector.<ToggleButton> = buttonListFromExample(example);
            var btn:ToggleButton, val:String;
            var result:Boolean = false;
            for each (btn in list) {
                if (btn.momentary) {
                    result = true;
                    btn.momentary = false;
                }
            }
            return result;
        }


        private function buttonListFromExample(example:ToggleButton):Vector.<ToggleButton>
        {
            var group:ToggleButtonGroup = example.toggleButtonGroup;
            var scope:AbstractContainer = example.wireScopeContainer;
            var list:Vector.<ToggleButton> = new Vector.<ToggleButton>();
            scope.allObjectsInScope(function(obj:AbstractObject):Boolean{
                var tb:ToggleButton = obj as ToggleButton;
                if (tb && tb.toggleButtonGroup == group)
                    list.push(tb);
                return true;
            });
            return list;
        }
		
		public function unmakeToggleButtonGroup(list:Vector.<ToggleButton>):void
		{
			for each(var btn:ToggleButton in list) {
				btn.toggleButtonGroup = null;
				btn.toggleButtonGroupValue = "";
                btn.toggleButtonGroupNullValue = "";
			}
			ApplicationController.instance.authorController.clearSelection();
		}
		
		public function finishPastingToggleButtonGroups(buttons:Vector.<ToggleButton>):void
		{
			var toBeProcessed:Vector.<ToggleButton> = buttons.concat();
			trace("finishPastingToggleButtonGroups count="+buttons.length, "toBeProcessed="+toBeProcessed.length);
			while (toBeProcessed.length > 0) {
				var btn:ToggleButton = toBeProcessed[0];
				finishOneButtonGroup(btn, toBeProcessed);  // removes buttons from toBeProcessed list
			}
		}
		
		private function finishOneButtonGroup(button:ToggleButton, toBeProcessed:Vector.<ToggleButton>):void
		{
			// survey the world/gadget to find all buttons of this group
			var group:ToggleButtonGroup = button.toggleButtonGroup;  // group created by pasting
			trace("finishOneButtonGroup: button="+button, "id="+group.id);
			var existing:ToggleButton = null;  // existing button with same ID but a different group
			var originalButtons:int = 0;  // number of buttons in existing group
			var pastedButtons:int = 0;  // number of buttons pasted from that group
			var scope:AbstractContainer = button.wireScopeContainer;
			scope.allObjectsInScope(function(obj:AbstractObject):Boolean{
				var tb:ToggleButton = obj as ToggleButton;
				if (tb && tb.toggleButtonGroup && tb.toggleButtonGroup.id == group.id) {  // either original or pasted button of group
					if (tb.toggleButtonGroup == group) {  // pasted
						++pastedButtons;
					} else {
						++originalButtons;
						existing = tb;
					}
				}
				return true; 
			});
			
			// if copy/paste a subset of a group, then expand the original group;  otherwise leave the pasted buttons as a new group
			var referenceOriginalGroup:Boolean = (existing != null && pastedButtons < originalButtons);
			trace(" existing="+existing, "original="+originalButtons, "pasted="+pastedButtons, "referenceOriginal="+referenceOriginalGroup);
			
			// process all pasted buttons in this group
			var btn:ToggleButton;
			for (var n:int=toBeProcessed.length-1; n >= 0; n--) {
				btn = toBeProcessed[n];
				if (btn.toggleButtonGroup == group) {
					if (referenceOriginalGroup) {
						btn.toggleButtonGroup = existing.toggleButtonGroup;  // make the pasted button reference existing group
					}
					toBeProcessed.splice(n, 1);
				}
			}
			
			if (referenceOriginalGroup) {  
				resolveToggleButtonGroupValues(button); // ensure a duplicated button gets a unique value
			} else {
				group.id = UIDUtil.createUID();  // ensure the pasted group is independent of the original group
			}
		}
		
		public function buttonGroupingToJson(list:Vector.<ToggleButton>):Object
		{
			var json:Array = [];
			for each(var btn:ToggleButton in list) {
				var group:ToggleButtonGroup = btn.toggleButtonGroup;
				var groupID:String = (group != null) ? group.id : null;
				json.push( { button:btn.uid, group:groupID, value:btn.toggleButtonValue, nullValue:btn.toggleButtonGroupNullValue, checked:btn.checked, label:btn.label, momentary:btn.momentary } );
			}
			return json;
		}
		
		public function applyButtonGroupingFromJson(json:Object):void
		{
			var groups:Dictionary = new Dictionary();   // groups[scope][groupID] -> ToggleButtonGroup
			for each(var entry:Object in json) {
				var buttonID:String = entry.button;
				var groupID:String = entry.group;
				var value:String = entry.value;
                var nullValue:String = entry.nullValue;
				var btn:ToggleButton = world.findObjByUID(buttonID) as ToggleButton;
				if (groupID == null) {
					btn.toggleButtonGroup = null;
					btn.toggleButtonValue = "";
                    btn.toggleButtonGroupNullValue = "";
				} else {
					var scope:AbstractContainer = btn.wireScopeContainer;
					if (groups[scope] == null) {
						groups[scope] = findGroupsInScope(scope);
					}
					var group:ToggleButtonGroup = groups[scope][groupID] as ToggleButtonGroup;
					if (group == null) {  // didn't find any existing buttons with this group: user probably did unmake of entire group
						group = new ToggleButtonGroup();
						group.id = groupID;
						groups[scope][groupID] = group;
						trace("created NEW GROUP");
					}
					btn.toggleButtonGroup = group;
					btn.toggleButtonValue = value;
                    btn.toggleButtonGroupNullValue = nullValue;
				}
				btn.checked = entry.checked;
				btn.label = entry.label;
                btn.momentary = entry.momentary;
			}
		}
		
		private function findGroupsInScope(scope:AbstractContainer):Dictionary
		{
			var groups:Dictionary = new Dictionary();
			scope.allObjectsInScope(function(obj:AbstractObject):Boolean{
				var tb:ToggleButton = obj as ToggleButton;
				if (tb) {
					var group:ToggleButtonGroup = tb.toggleButtonGroup;
					if (group && groups[group.id] == null) {
						groups[group.id] = group;
					}
				}
				return true;
			});
			return groups;
		}
		
		private function get world():World
		{
			return Application.instance.document.root.value as World;
		}

	}
}
