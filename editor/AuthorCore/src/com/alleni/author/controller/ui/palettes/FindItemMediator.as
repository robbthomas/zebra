/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 11/23/11
 * Time: 12:36 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.ui.palettes {
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Objects;
import com.alleni.author.definition.ViewRoles;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.FindItemView;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldView;
import com.alleni.author.view.ui.components.PopupMenu;
import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
import com.alleni.taconite.definition.ObjectDefinition;
import com.alleni.taconite.view.ViewContext;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.PixelSnapping;
import flash.display.Shape;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;

public class FindItemMediator {

    private var _obj:AbstractObject;
    private var _view:FindItemView;
    private var _showingCapture:Boolean;
    private var _capture:DisplayObject;
    private var _capturingOffStage:Boolean;
    private static var _popupMenu:PopupMenu;

    private static const SELECT_OBJ:String = "selectObj";
    private static const SELECT_AND_GOTO_PAGE:String = "selectAndGotoPage";
    private static const SHOW_IN_OUTLINE:String = "showInOutline";


    public function FindItemMediator() {
    }

    public function handleViewEvents(view:FindItemView):void
    {
        _view = view;
        _obj = view.object;
        _view.addEventListener(MouseEvent.ROLL_OVER, rollOver);
        _view.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);

        if (_popupMenu == null) {   // the popup is shared between all items
            createPopupMenu();
        }
    }

    public function clearViewEvents():void
    {
        _view.removeEventListener(MouseEvent.ROLL_OVER, rollOver);
        _view.removeEventListener(MouseEvent.ROLL_OUT, rollOut);
        _view.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
        _view.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
        feedback(false);
    }

    private function rollOver(event:MouseEvent):void
    {
        _view.addEventListener(MouseEvent.ROLL_OUT, rollOut);
        _view.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
        feedback(true);
    }

    private function rollOut(event:MouseEvent):void
    {
        _view.removeEventListener(MouseEvent.ROLL_OUT, rollOut);
        _view.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
        feedback(false);
    }

    private function mouseMove(event:MouseEvent):void
    {
        _view.addEventListener(MouseEvent.ROLL_OUT, rollOut);
        _view.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
        feedback(true);
    }

    private function mouseDown(event:MouseEvent):void
    {
        trace("mouseDown findItem", _obj);
        feedback(false);


        var pm:PopupMenu = _popupMenu;
        var local:Point = pm.parent.globalToLocal(_view.localToGlobal(new Point(_view.mouseX, 3)));
        pm.x = local.x - pm.width/2;
        pm.y = local.y -3;
        pm.requestOpenMenu(menuActionCallback);  // menu has been clicked ... listen for mouse-move & up
    }

    private function createPopupMenu():void
    {
        var data:Array = [
            {label:"Select object", id:SELECT_OBJ},
            {label:"Select and goto page", id:SELECT_AND_GOTO_PAGE},
            {label:"Show in outline", id:SHOW_IN_OUTLINE}
            ];

        var menu:PopupMenu = new PopupMenu(_view.menuHolder, 0,0, data);
        menu.selectedColor = 0x6666ff;
        menu.visible = false;
        _popupMenu = menu;
    }

    private function get menuPoppedUp():Boolean
    {
        if (_popupMenu == null)
            return false;
        else
            return _popupMenu.visible;
    }

    private function menuActionCallback(actionID:String):void
    {
        switch (actionID) {
            case SELECT_OBJ:
                ApplicationController.instance.authorController.selectSingleModel(_obj.model);
                break;
            case SELECT_AND_GOTO_PAGE:
                showContainingPage(_obj);
                ApplicationController.instance.authorController.selectSingleModel(_obj.model);
                break;
            case SHOW_IN_OUTLINE:
                OutlineWindowController.instance.showObject(_obj);
                ApplicationController.instance.authorController.selectSingleModel(_obj.model);
                break;
        }
    }

    private function showContainingPage(obj:AbstractObject):void
    {
        var child:AbstractObject = obj;
        var parent:AbstractObject = child.parent;
        while (parent is World == false) {
            if (parent is Arena) {
                var arena:Arena = parent as Arena;
                var index:int = arena.pages.getItemIndex(child);
                arena.pageNumber = index +1;
            }
            child = parent;
            parent = child.parent;
        }
    }


    private function feedback(show:Boolean):void
    {
        if (show && !menuPoppedUp) {
            if (_view.mouseX < FindItemView.TEXT_X) {
                showCapture();
                hideTooltip();
            } else {
                showTooltip();
                hideCapture();
            }

        } else {
            hideCapture();
            hideTooltip();
        }
    }

    private function showCapture():void
    {
        if (_capture == null) {
            _capture = captureView();
        }
        if (_capture != null){
            showCapturedView();
        }
        _showingCapture = true;
    }

    private function showCapturedView():void
    {
        var pt:Point = _view.localToGlobal(new Point(0 - _capture.width - 40, _view.height/2 - _capture.height/2 ));
        _capture.x = pt.x;
        _capture.y = pt.y;
        feedbackContainer.addChild(_capture);
    }

    private function hideCapture():void
    {
        if (_capture && feedbackContainer.contains(_capture)) {
            feedbackContainer.removeChild(_capture);
        }
        _showingCapture = false;
    }

    private function showTooltip():void
    {
        var point:Point = _view.localToGlobal(new Point(FindItemView.TEXT_X+20, _view.height + 0));
        ApplicationController.instance.displayToolTip(point, ControlAreaToolTip.DISPLAY_BELOW, tooltipText);
        ApplicationController.instance.showTooltipNow();
    }

    private function hideTooltip():void
    {
        ApplicationController.instance.hideToolTip();
    }

    private function get tooltipText():String
    {
        var result:String = objString(_obj);
        var obj:AbstractObject = _obj.parent;
        while (obj && !(obj is World)) {
            var objStr:String = objString(obj);
            if (objStr != null) {
                result += "\n" + "in " + objString(obj);
            }
            obj = obj.parent;
        }
        return result;
    }

    private function objString(obj:AbstractObject):String
    {
        // return string such as Arena "title" or Page 6
        // return null to omit this object, such as for single arena page
        var result:String;
        if (obj is ArenaPage) {
            var arena:Arena = obj.parent as Arena;
            if (arena) {
                if (arena.pages.length <= 1) {
                    result = null;  // single-page arena: omit the page from listing
                } else {
                    result = "Page ";
                    var index:int = arena.pages.getItemIndex(obj);
                    result += (index+1);
                }
            }
        } else { // not arena page
            var desc:ObjectDefinition = Objects.descriptionForShortClassname(obj.shortClassName);
            result = (desc ? desc.defaultTitle : obj.shortClassName) + (obj.titleChangedByAuthor ? " " + '"'+obj.title+'"' : "");
        }
        return result;
    }

    private function get feedbackContainer():DisplayObjectContainer
    {
        return EditorUI.instance.stage;
    }

    private function get worldView():WorldView
    {
        return EditorUI.instance.presentationContainer.worldView;
    }


    private static const MAX_WIDTH:Number = 100;
    private static const MAX_HEIGHT:Number = 100;

    private function captureView():DisplayObject
    {
        if (!_capturingOffStage) {
            var view:ObjectView = _obj.getView();
            if (view && view.stage){
                return captureOnStageView(view);
            } else {
                return captureOffStageView(_obj);
            }
        }
        return null;
    }

    private function captureOnStageView(view:ObjectView):DisplayObject
    {
        var bounds:Rectangle = view.getBounds(view);
        if (bounds.width > 0) {
            var rect:Rectangle = bounds.clone();
            rect.width = Math.min(rect.width,  MAX_WIDTH);
            rect.height = Math.min(rect.height, MAX_HEIGHT);
            var bmd:BitmapData = new BitmapData(rect.width, rect.height, true);  // transparent=true
            var result:DisplayObject = new Bitmap(bmd, PixelSnapping.AUTO, true);
            var sx:Number = rect.width / bounds.width;
            var sy:Number = rect.height / bounds.height;
            rect.x *= sx;
            rect.y *= sy;

            var oldFilters:Array = view.filters;
            var oldVis:Boolean = view.visible;
            var oldAlpha:Number = view.alpha;
            view.filters = null;
            view.visible = true;
            view.alpha = 1;

            var matrix:Matrix = new Matrix();
            matrix.createBox(sx, sy, 0, -rect.x, -rect.y);
            try {
                bmd.draw(view, matrix, null, null, null, true); // smoothing helps with subpixel shifting
            } catch (e:Error) {
                result = createBlueRect(rect.width, rect.height);
            }

            view.filters = oldFilters;
            view.visible = oldVis;
            view.alpha = oldAlpha;

            return result;
        }
        return null;
    }

    private function createBlueRect(ww:Number, hh:Number):DisplayObject
    {
        var disp:Shape = new Shape();
        disp.graphics.beginFill(0x0000ff);
        disp.graphics.drawRect(0,0, ww, hh);
        disp.graphics.endFill();
        return disp;
    }

    private function captureOffStageView(obj:AbstractObject):DisplayObject
    {
        _capturingOffStage = true;
        var view:ObjectView = createView(obj);
        worldView.addChild(view);

        var frameCounter:int = 0;
        worldView.stage.addEventListener(Event.ENTER_FRAME, function(e:Event):void {
            if (++frameCounter > 1) {
                worldView.stage.removeEventListener(Event.ENTER_FRAME, arguments.callee);
                _capturingOffStage = false;

                if (_showingCapture && _capture == null) {
                    var capture:DisplayObject = captureOnStageView(view);
                    if (capture) {
                        _capture = capture;
                        showCapturedView();
                    }
                }
                view.warnViewToDelete();  // remove the temporary view instance
                worldView.removeChild(view);
            }
        });
        return null;
    }

    private function createView(obj:AbstractObject):ObjectView
    {
        var context:ViewContext = Application.instance.viewContext;
        var desc:ObjectDefinition = Objects.descriptionForShortClassname(obj.shortClassName);
        var viewClass:Class = desc.view;
        var view:ObjectView = new viewClass(context, obj.model, ViewRoles.PRIMARY) as ObjectView;
        view.preventMessageCenter = true;
        view.initialize();
        view.x = -10000; // prevent it showing on stage
        return view as ObjectView;
    }

}
}
