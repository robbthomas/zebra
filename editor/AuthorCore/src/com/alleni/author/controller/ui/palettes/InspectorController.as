package com.alleni.author.controller.ui.palettes
{
    import assets.icons.object.GroupObjIcon;
    
    import com.alleni.author.application.ApplicationUI;
    import com.alleni.author.application.ui.EditorUI;
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.InletDescription;
    import com.alleni.author.definition.InspectorPresets;
    import com.alleni.author.definition.ObjectIcons;
    import com.alleni.author.definition.Objects;
    import com.alleni.author.definition.OutletDescription;
    import com.alleni.author.definition.PropertyDescription;
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.definition.text.TextSelection;
    import com.alleni.author.event.AnchorEvent;
    import com.alleni.author.model.AbstractObject;
    import com.alleni.author.model.World;
    import com.alleni.author.model.objects.AbstractTable;
    import com.alleni.author.model.objects.Calc;
    import com.alleni.author.model.objects.CalcExp;
    import com.alleni.author.model.objects.CalcTween;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.model.ui.ForwardingWireAnchor;
    import com.alleni.author.model.ui.InspectorValues;
    import com.alleni.author.model.ui.Wire;
    import com.alleni.author.view.ui.palettes.InspectorView;
    import com.alleni.taconite.dev.Utilities;
    import com.alleni.taconite.document.ISelection;
    import com.alleni.taconite.document.ObjectSelection;
    import com.alleni.taconite.event.ApplicationEvent;
    import com.alleni.taconite.event.ModelUpdateEvent;
    import com.alleni.taconite.event.SelectEvent;
    import com.alleni.taconite.model.ITaconiteObject;
    import com.alleni.taconite.model.TaconiteModel;
    
    import flash.display.Sprite;
    import flash.events.EventDispatcher;
    import flash.utils.Dictionary;
    
    import mx.events.PropertyChangeEvent;

	public class InspectorController
	{
		private static var _instance:InspectorController;
		
		private var _view:InspectorView;
		private var _inspectorHolder:Sprite;
		private var _selectedObjects:Vector.<ITaconiteObject> = new Vector.<ITaconiteObject>();
		private var _utilities:Utilities;
        private var _listeningToSelection:Boolean = true;
        private var _wasShowingRibbonLibrary:Boolean = false;
        private var _wasShowingInspector:Boolean = false;

        public function get listeningToSelection():Boolean
		{
            return _listeningToSelection;
        }

        public function set listeningToSelection(value:Boolean):void
		{
            var callChanged:Boolean = !_listeningToSelection && value;

            _listeningToSelection = value;

            if(callChanged)
                handleSelectionChanged()
        }

        public function InspectorController()
		{	
			if (_instance != null) throw new Error("Singleton, do not instantiate");
			
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_CLOSING, handleProjectClosing);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_INITIALIZED, handleProjectInitialized);
			//ApplicationController.addEventListener(NotificationNamesApplication.WORLD_REPOSITIONED, handleWorldRepositioned);
            ApplicationController.addEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED,handleInOutFlowRunResume);
            ApplicationController.addEventListener(NotificationNamesApplication.FLOW_VISIBILITY_CHANGED,handleInOutFlowRunResume);
            _utilities = new Utilities();
		}

		public static function get instance():InspectorController
		{
			if (_instance == null)
				_instance = new InspectorController();
			return _instance;
		}

        private function handleInOutFlowRunResume(event:ApplicationEvent):void
		{

            if (!Application.running && !Application.instance.flowVisible) {
                if ( _wasShowingRibbonLibrary ) ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SHOW_RIBBON_LIBRARY));
                if ( _wasShowingInspector ) ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SHOW_INSPECTOR));
            } else {
                _wasShowingRibbonLibrary = Application.instance.ribbonLibraryVisible;
                _wasShowingInspector = Application.instance.inspectorVisible;
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.HIDE_INSPECTOR));
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.HIDE_RIBBON_LIBRARY));
            }
        }
		
		private function handleProjectClosing(event:ApplicationEvent = null):void
		{
			removeListeners();
		}
		
		private function handleProjectInitialized(event:ApplicationEvent = null):void
		{
			addListeners();
			handleSelectionChanged();
		}
		
		/*private function handleWorldRepositioned(event:ApplicationEvent = null):void
		{
			updateView();  // immediately update inspector
		}*/
		
		private function addListeners():void
		{
			if (!ApplicationUI.instance.invokedAsEditor) return;
			InspectorValues.createNew();
			
			// prepare a holder, so the inspector can be replaced when switching to another project
			if (!_inspectorHolder) {
				_inspectorHolder = new Sprite();
				EditorUI.instance.addToRightPalette(_inspectorHolder);
			} else {
				while (_inspectorHolder.numChildren > 0)
					_inspectorHolder.removeChildAt(0);
			}
			
			_view = new InspectorView();
			_inspectorHolder.addChild(_view);

			Application.instance.document.root.addEventListener(ModelUpdateEvent.MODEL_UPDATE, modelUpdateListener, false, 3);
			Application.instance.document.addEventListener(SelectEvent.CHANGE_SELECTION, handleSelectionChanged);
			Application.instance.document.addEventListener(SelectEvent.ADD_SELECTION, handleSelectionChanged);
			Application.instance.document.addEventListener(SelectEvent.REMOVE_SELECTION, handleSelectionChanged);
			Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
            ApplicationController.instance.addEventListener(NotificationNamesApplication.RUNNING_STATE_CHANGED, handleRunningChanged);
		}
		
		private function removeListeners():void
		{
			Application.instance.document.root.removeEventListener(ModelUpdateEvent.MODEL_UPDATE, modelUpdateListener);
			Application.instance.document.removeEventListener(SelectEvent.CHANGE_SELECTION, handleSelectionChanged);
			Application.instance.document.removeEventListener(SelectEvent.ADD_SELECTION, handleSelectionChanged);
			Application.instance.document.removeEventListener(SelectEvent.REMOVE_SELECTION, handleSelectionChanged);
			Application.instance.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
		}
		
		private function selectedPropertyChangeListener(e:PropertyChangeEvent):void
		{
			var anchor:ForwardingWireAnchor = InspectorValues.instance.anchors[e.property.toString()] as ForwardingWireAnchor;			
			if(anchor)
				anchor.valueChanged();
		}

		public function set selectedTextElements(objects:Vector.<ITaconiteObject>):void
		{
			if(!objects)
				return;
			
            if(objects.length == 0) {
                handleSelectionChanged();
            } else {
			    setSelection(objects);
            }
		}
		
		public function set selectedDrawings(objects:Vector.<ITaconiteObject>):void
		{
			setSelection(objects, true);
		}

        private function handleRunningChanged(e:ApplicationEvent):void {
            if(Application.running) {
                show(false, true);
            } else {
                show(Application.instance.inspectorVisible);
            }
        }
		
		private function modelUpdateListener(e:ModelUpdateEvent):void
		{
            if (e.property == "complete") {
				var obj:Object;
				var complete:Boolean = true;
				
				for each(obj in _selectedObjects) {
					if(obj is AbstractTable || obj is Calc || obj is CalcExp || obj is CalcTween)
						continue;
					if(obj is AbstractObject) {
						complete &&= AbstractObject(obj).complete;
					} else {
						complete = false;
					}
				}
				if (complete) {
					for each(obj in _selectedObjects) {
						if (_selectedObjects.length == 1) {
							_view.updateSelectedObjectName(obj.title);
	                        _view.setSelectedIcon(ObjectIcons.getIconFor(obj as AbstractObject));
	                    } else if (_selectedObjects.length > 1) {
							_view.updateSelectedObjectName("[multiple selection]");
	                        _view.setSelectedIcon(new GroupObjIcon());
	                    }
					}
					buildAnchors();
					updateView();
				}
			}
		}

        public function handleTitleChange(object:AbstractObject):void {
            if(_selectedObjects.length == 1 && _selectedObjects[0] == object) {
                _view.updateSelectedObjectName(object.title);
            }
        }
		
		public function handleSelectionChanged(e:SelectEvent=null):void 
		{
            if(!listeningToSelection)
                return;

			var objSelection:ISelection = Application.instance.document.selection as ObjectSelection;
			var objects:Vector.<ITaconiteObject> = new Vector.<ITaconiteObject>();
			var model:TaconiteModel;
			for each(model in objSelection.selectedModels) {
				objects.push(model.value);
			}
			setSelection(objects);
		}

		private function setSelection(objects:Vector.<ITaconiteObject>, force:Boolean = false):void
		{
            if(_view == null || _selectedObjects == null)
                return;
            _view.resetScrollPosition();

			if (!force) {
				if(objects.length == _selectedObjects.length) {
					if(		objects.every(function(item:ITaconiteObject, index:int, vector:Vector.<ITaconiteObject>):Boolean{
								return _selectedObjects.indexOf(item) != -1;
							})
						&&	_selectedObjects.every(function(item:ITaconiteObject, index:int, vector:Vector.<ITaconiteObject>):Boolean{
								return objects.indexOf(item) != -1;
							})) {
						return;
					}
				}
			}

			var objectHasMessageCenter:Boolean = false;
			var messageCenterButtonEnabled:Boolean = true;
			var inspector:InspectorValues = InspectorValues.instance;

			inspector.clearModifiers();

			if(_selectedObjects.length > 0) {
				var o:ITaconiteObject;
				for each(o in _selectedObjects) {
					if(o is EventDispatcher) {
						EventDispatcher(o).removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, selectedPropertyChangeListener);
						EventDispatcher(o).removeEventListener(AnchorEvent.TYPE,handleAnchorChange,true);
					}
				}
			}

			_selectedObjects = new Vector.<ITaconiteObject>();
			var preset:Object = {};

			if (objects != null && objects.length > 0) {				
				var n:int;
				var complete:Boolean = true;
				var lastTitle:String = "Selected Object";
				for (n = 0; n < objects.length; n++) {
					if(!(objects[n] is Wire)){
						var obj:ITaconiteObject = objects[n];
						_selectedObjects.push(obj);
						var modifiers:Dictionary = new Dictionary();
						if(obj is AbstractObject) {
							var ao:AbstractObject = obj as AbstractObject;
							objectHasMessageCenter = ao.messageCenter;
							messageCenterButtonEnabled = !(obj as AbstractTable || obj as Calc || obj as CalcExp || obj as CalcTween); // MCs cannot be toggled for Logic Objects
							complete &&= ao.complete;
							preset = inspector.getPreset(ao.shortClassName);
							modifiers = ao.modifiers;
							lastTitle = ao.title;
						} else if(obj is TextSelection) {
							preset = inspector.getPreset("TextSelection");
							modifiers = TextSelection(obj).modifiers;
							lastTitle = "Text Selection";
						} else if(obj is InspectorPresets) {
							modifiers = (obj as InspectorPresets).modifiers;
                            if (Application.instance.currentTool) {
                                lastTitle = Objects.descriptionForShortClassname(ToolboxController.instance.getShortClassNameForTool(Application.instance.currentTool)).defaultTitle+" presets";
                                _view.setSelectedIcon(ObjectIcons.getIconFor(null,ToolboxController.instance.getShortClassNameForTool(Application.instance.currentTool)));
                            }
						}
						const eventDispatcher:EventDispatcher = obj as EventDispatcher;
						if(eventDispatcher) {
							eventDispatcher.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, selectedPropertyChangeListener);
							eventDispatcher.addEventListener(AnchorEvent.TYPE,handleAnchorChange);
						}

						var key:String;
						if(inspector.modifiers == null) {
							inspector.modifiers = new Dictionary();
							inspector.inletModifiers = new Dictionary();
							inspector.outletModifiers = new Dictionary();

							for(key in modifiers) {
								if(modifiers[key] is PropertyDescription && modifiers[key].indexed == false /* && preset["propertyInspectorVisible"].indexOf(key) >= 0*/) {
									inspector.modifiers[key] = modifiers[key];
								} else if (modifiers[key] is InletDescription && modifiers[key].indexed == false ) {
									inspector.inletModifiers[key] = modifiers[key];
								} else if (modifiers[key] is OutletDescription && modifiers[key].indexed == false ) {
									inspector.outletModifiers[key] = modifiers[key];
								}
							}
						} else {
							for(key in inspector.modifiers) {
								if(modifiers[key] /* && preset["propertyInspectorVisible"].indexOf(key) >= 0 */) {
//									trace("keeping " + key);
									// keep it
								} else {
//									trace("Removing", key, "blaming", obj);
									delete inspector.modifiers[key];
								}
							}
							for (key in inspector.inletModifiers) {
								if(modifiers[key] /* && preset["propertyInspectorVisible"].indexOf(key) >= 0 */) {
									// keep it
								} else {
									//trace("Removing", key, "blaming", obj);
									delete inspector.inletModifiers[key];
								}
							}
							for (key in inspector.outletModifiers) {
								if(modifiers[key] /* && preset["propertyInspectorVisible"].indexOf(key) >= 0 */) {
									// keep it
								} else {
									//trace("Removing", key, "blaming", obj);
									delete inspector.outletModifiers[key];
								}
							}
						}
					}
				}
				buildAnchors(preset as InspectorPresets);

				if(complete) {
					if (objects.length == 1){
						if(!(objects[0] is Wire)){
							_view.updateSelectedObjectName(lastTitle);
                            if (objects[0] as AbstractObject != null)
                                _view.setSelectedIcon(ObjectIcons.getIconFor(objects[0] as AbstractObject));
						}
					}else if (objects.length > 1){
						_view.updateSelectedObjectName("[multiple selection]");
                        _view.setSelectedIcon(new GroupObjIcon());
					}
					buildAnchors();
				}
                _view.setMessageCenterToggleVisibility(true);
			} else {
                var blankSelectionTitle:String = (Application.instance.inspectorVisible && !Application.instance.ribbonLibraryVisible? "Inspector":"Ribbon Library")
				_view.updateSelectedObjectName(blankSelectionTitle);
                _view.setMessageCenterToggleVisibility(false);
                _view.setSelectedIcon(null);
			}
			updateView();
			_view.updateMessageCenterToggle(objectHasMessageCenter, messageCenterButtonEnabled);
		}
		
		private function handleAnchorChange(e:AnchorEvent):void
		{
			var forwardingAnchor:ForwardingWireAnchor = InspectorValues.instance.anchors[e.anchor.modifierDescription.key];
            if (forwardingAnchor)
                forwardingAnchor.dispatchEvent(PropertyChangeEvent.createUpdateEvent(forwardingAnchor, "messageCenterVisible", null, forwardingAnchor.messageCenterVisible));
		}
		
		private function buildAnchors(presets:InspectorPresets = null):void
		{
			for(var property:String in InspectorValues.instance.modifiers) {
				var anchor:ForwardingWireAnchor = InspectorValues.instance.anchors[property] as ForwardingWireAnchor;
				if(anchor != null) {
					anchor.selectedObjects = _selectedObjects;
					continue;
				}
				var prop:PropertyDescription = InspectorValues.instance.modifiers[property] as PropertyDescription;
				anchor = new ForwardingWireAnchor(_selectedObjects);
				anchor.modifierDescription = prop;
				anchor.hostObject = null;//maybe inspector
				anchor.hostProperty = property;
				InspectorValues.instance.anchors[property] = anchor;
			}
			for(var inletModifier:String in InspectorValues.instance.inletModifiers) {
				anchor = InspectorValues.instance.anchors[inletModifier] as ForwardingWireAnchor;
				if(anchor != null) {
					anchor.selectedObjects = _selectedObjects;
					continue;
				}
				var inlet:InletDescription = InspectorValues.instance.inletModifiers[inletModifier] as InletDescription;
				anchor = new ForwardingWireAnchor(_selectedObjects);
				anchor.modifierDescription = inlet;
				anchor.hostObject = null;//maybe inspector
				anchor.hostProperty = inletModifier;
				InspectorValues.instance.anchors[inletModifier] = anchor;
			}
			for(var outletModifier:String in InspectorValues.instance.outletModifiers) {
				anchor = InspectorValues.instance.anchors[outletModifier] as ForwardingWireAnchor;
				if(anchor != null) {
					anchor.selectedObjects = _selectedObjects;
					continue;
				}
				var outlet:OutletDescription = InspectorValues.instance.outletModifiers[outletModifier] as OutletDescription;
				anchor = new ForwardingWireAnchor(_selectedObjects);
				anchor.modifierDescription = outlet;
				anchor.hostObject = null;//maybe inspector
				anchor.hostProperty = outletModifier;
				InspectorValues.instance.anchors[outletModifier] = anchor;
			}
		}
		
		/**
		 * TODO: RELATING THE TOOLS TO THE SHORTCLASSNAMES OF THE OBJECTS THEY CREATE IS A TEMPORARY FIX
		 * FOR THE NEEDED BEHAVIOR FOR INSPECTOR TO RESPOND TO SELECTED TOOLS. TOOLS WHICH CREATE OBJECTS
		 * SHOULD BE RELATED DIRECTLY TO THE OBJECT IDENTIFIERS. POSSIBLY OBJECT CREATION TOOLS SHOULD BE 
		 * DEFINED IN THE OBJECTS DEFINITIONS. 
		 * 
		 */
		private function handleToolChange():void
		{
			var currentTool:String = ToolboxController.instance.getShortClassNameForTool(Application.instance.currentTool);
			
//			if (currentTool == "Arrow" || currentTool == "Zoom" || currentTool == "Move")
//				return;
			
			if (_selectedObjects.length == 1) {
				if (_selectedObjects[0] as InspectorPresets) {
					_selectedObjects.pop();
				}
			}

			// load presets and update Inspector title if there are no objects already
			// selected (tool change is always trumped by object selection)
			if (currentTool != null &&  currentTool != "Arrow" && currentTool != "Zoom" && currentTool != "Move" ) {
				// load presets for the object type
				//_view.updateSelectedObjectName("Set Default: " + currentTool);
				_view.updateSelectedObjectName("Default Settings");
				buildAnchors(loadPresets(currentTool));
				updateView();
			} else {
                handleSelectionChanged();
            }
		}
		
		private function loadPresets(shortClassName:String):InspectorPresets
		{
			InspectorValues.instance.modifiers = new Dictionary();
			var inspectorPresets:Vector.<ITaconiteObject> = new Vector.<ITaconiteObject>();
			inspectorPresets.push(InspectorValues.instance.getPreset(shortClassName));
			setSelection(inspectorPresets);
			updateView();
			return inspectorPresets[0] as InspectorPresets;
		}
		
		
		private function handleApplicationPropertyChange(event:PropertyChangeEvent):void
		{
			// The application model changed, so update the UI accordingly.
			var propertyName:String = event.property as String;
			Utilities.assert(propertyName != null);
			
			switch (propertyName) {
				case "currentTool":
					handleToolChange();
					break;
			}
		}
		
		// Call to update the model.
		public function show(show:Boolean, run:Boolean=false):void
		{
			if (!ApplicationUI.instance.invokedAsEditor) return;
			if (_view == null)
				return;
            if (_selectedObjects.length == 0) {
                var blankSelectionTitle:String = (Application.instance.inspectorVisible && !Application.instance.ribbonLibraryVisible? "Inspector":"Ribbon Library")
                _view.updateSelectedObjectName(blankSelectionTitle);
            }

            _view.saveScrollBarPosition();
			var showPalette:Boolean = Application.instance.inspectorVisible || Application.instance.ribbonLibraryVisible;
			EditorUI.instance.showRightPalette(showPalette);

			if (show) {
				if(show)
					updateView();
				
				if (!Application.instance.inspectorVisible && !Application.instance.ribbonLibraryVisible)
					handleToolChange();
			}		
			/*if (!run) // Entering run should not change the visibility of inspector when we pause
				Application.instance.inspectorVisible = show;*/
		}
		
		public function toggleMessageCenters(on:Boolean):void
		{
			if (_selectedObjects.length == 0)
				return;
			
			var obj:Object;
			for each (obj in _selectedObjects) {
				const abstractObject:AbstractObject = obj as AbstractObject;
				if(abstractObject)
					abstractObject.messageCenter = on;
			}
		}
		
		private function updateView():void
		{
			_view.updateRibbons();
		}
		
		public function setTitle(title:String):void
		{
			_view.updateSelectedObjectName(title);
		}
	}
}
