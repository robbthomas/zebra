/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller
{
    import caurina.transitions.Tweener;

import com.alleni.author.model.ui.Application;

import com.alleni.author.view.WorldContainer;
    import com.alleni.author.view.WorldView;
    import com.alleni.taconite.controller.DragMediator;
    import com.alleni.taconite.view.ViewContext;
    
    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    /**
     * Mediator for the WorldView that draws a marquee rectangle that selects enclosed objects.
     */
    public class MarqueeMediator extends DragMediator
    {
        protected var _worldView:WorldView;
        protected var _marquee:Shape;
        protected var _worldDragRect:Rectangle;
		private var _worldStart:Rectangle;
		private var _callback:Function;   //  func(resultRect:Rectangle):void
		private var _clickCallback:Function;
        
        public function MarqueeMediator(context:ViewContext, worldView:WorldView, callback:Function, clickCallback:Function=null)
        {
            super(context);
            _worldView = worldView; 
			_callback = callback;
			_clickCallback = clickCallback;
        }
        
		override public function handleMouseDown(e:MouseEvent):void
		{
			if (_context.stage == null || Application.running)
				return;
			_dragStarted = false;
			
			_dragPoint = new Point(WorldContainer(_context).mouseX, WorldContainer(_context).mouseY);
			_context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_context.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			if (_stopPropagation)
				e.stopPropagation();
		}
		
        /**
         * At the start of a drag, capture the sizes of all selected objects so that we
         * can resize them all by the same delta later on.
         */
        override protected function handleDragStart(e:MouseEvent):void
        {
            _marquee = new Shape();
			WorldContainer(_context).feedbackHandlesView.addChild(_marquee);
            
            _worldStart = dragEndpointRect;
        }
        
        /**
         * For each move during the drag, resize the models appropriately.
         */
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
        {
            _worldDragRect = _worldStart.union(dragEndpointRect);
            
			if (_marquee) {
            	_marquee.graphics.clear();
	            _marquee.graphics.beginFill(0xeeeeee,0.2);
	            _marquee.graphics.lineStyle(1,0x999999,0.5);
	            _marquee.graphics.drawRect(_worldDragRect.x, _worldDragRect.y, _worldDragRect.width, _worldDragRect.height);
			}
        }
		
		/**
		 * Handle mouse motion during the drag by initiating it if necessary when the mouse
		 * exceeds the distance threshold, and then calling the move-handling function. 
		 */
		override protected function handleMouseMove(e:MouseEvent):void
		{
			if (!_dragStarted
				&& e.buttonDown
				&& _dragPoint != null
				&& (Math.abs(WorldContainer(_context).mouseX -_dragPoint.x) >= minimumDrag
				|| Math.abs(WorldContainer(_context).mouseY -_dragPoint.y) >= minimumDrag))
				{
				_dragStarted = true;
				handleDragStart(e);
			}
			
			if (_dragStarted)
			{
				handleDragMove(e);
			}
			if(_stopPropagation) {
				e.stopPropagation();
			}
		}
		
		override protected function handleClick(e:MouseEvent) : void
		{
			super.handleClick(e);
			if(_clickCallback != null) _clickCallback();
		}
		
        override protected function handleDragEnd(e:MouseEvent):void
        {
			if (_marquee) {
				Tweener.addTween(_marquee,{alpha:0, time:.25, transition:"easeOutCubic", onComplete:function():void{
					WorldContainer(_context).feedbackHandlesView.removeChild(_marquee);
					_marquee = null;
				}}); 
			
				var resultRect:Rectangle = _marquee.getBounds(_context.stage);
				_callback(resultRect);
			}
        }

        private function get dragEndpointRect():Rectangle
        {
            return new Rectangle(_worldView.mouseX, _worldView.mouseY, 1, 1)
        }
   }
}
