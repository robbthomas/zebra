/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 11/10/11
 * Time: 11:26 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.ErrorMessage;
import com.alleni.author.view.ui.OverlayMessage;
import com.alleni.author.view.ui.controls.Alert;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.service.LogService;

import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.utils.Dictionary;

public class ErrorMessagingController {
        public static const ALERT_STANDARD:String = "Editor Alert - Standard";
        public static const TEMPORAL_CLOUD:String = "Editor Temporal - Cloud style";
        public static const TEMPORAL_STANDARD:String = "Editor Temporal - Standard";
        public static const PLAYER_MESSAGE:String = "Player Message";

        public static const INSERT_ARRAY:Array = [ new RegExp("{INSERT1}"),
                    new RegExp("{INSERT2}"),
                    new RegExp("{INSERT3}"),
                    new RegExp("{INSERT4}")];

        private static const INSERT_REGEXP:RegExp = /{INSERT(\d+)}/g;

        private var _errorMessages:Dictionary = new Dictionary();

        private var _xmlLoader:URLLoader;

        public function ErrorMessagingController() {
            ApplicationController.addEventListener(NotificationNamesApplication.DISPLAY_MESSAGE, displayError);
        }

        public function importErrorMessages():void{
            var urlRequest:URLRequest = new URLRequest("assets/externalMessages/errorMessages.xml");
            _xmlLoader = new URLLoader(urlRequest);
            _xmlLoader.addEventListener(Event.COMPLETE, handleMessagesLoaded);
            _xmlLoader.addEventListener(IOErrorEvent.IO_ERROR, handleErrors);
            _xmlLoader.load(urlRequest);
        }

        private function handleErrors(event:IOErrorEvent):void{
            trace("Error Loading Messages", event.toString());
            _xmlLoader.removeEventListener(Event.COMPLETE, handleMessagesLoaded);
            _xmlLoader.removeEventListener(IOErrorEvent.IO_ERROR, handleErrors);
        }

        private function handleMessagesLoaded(event:Event):void{
            _xmlLoader.removeEventListener(Event.COMPLETE, handleMessagesLoaded);
            _xmlLoader.removeEventListener(IOErrorEvent.IO_ERROR, handleErrors);
            var xml:XML = new XML(event.currentTarget.data);
            var ns:Namespace = new Namespace("http://www.filemaker.com/fmpdsoresult");
            for each(var row:XML in xml.ns::ROW){
                var errorMessage:ErrorMessage = new ErrorMessage();
                errorMessage.messageId = row.ns::msgNum;
                errorMessage.messageTitle = row.ns::title2;
                errorMessage.messageBody = row.ns::body2
                errorMessage.buildMessageButtonList(row.ns::choices2);
                errorMessage.messageContainerType = row.ns::container2;
                _errorMessages[errorMessage.messageId] = errorMessage;
            }
        }

        private function displayError(event:ApplicationEvent):void{
            var errorMessage:ErrorMessage = _errorMessages[event.data.id] as ErrorMessage;
            if(errorMessage == null){
                errorMessage = new ErrorMessage();
                errorMessage.messageTitle = "Error Message " + event.data.id + " Not Found";
                errorMessage.messageBody = "Sorry, an unknown error has occurred.";
                errorMessage.messageContainerType = TEMPORAL_STANDARD;
            }
            switch(errorMessage.messageContainerType){
                case ALERT_STANDARD:
                    displayErrorAlertBox(errorMessage, event.data.closeFunction, event.data.inserts);
                    break;
                case TEMPORAL_STANDARD:
                    displayErrorTemporalStandard(errorMessage, event.data.inserts);
                    break;
                case TEMPORAL_CLOUD:
                    displayErrorTemporalCloud(errorMessage, event.data.inserts);
                    break;
            }
        }

        private function displayErrorAlertBox(errorMessage:ErrorMessage, closeFunction:Function = null, inserts:Array = null):void{
            var numButtons:int = errorMessage.messageButtonList.length;
            var buttons:uint;
            var processedBody:String = errorMessage.messageBody;
            var processedTitle:String = errorMessage.messageTitle;

            switch(numButtons){
                case 4:
                    Alert.noLabel = errorMessage.messageButtonList[3];
                case 3:
                    Alert.yesLabel = errorMessage.messageButtonList[2];
                case 2:
                    Alert.okLabel = errorMessage.messageButtonList[1];
                case 1:
                    Alert.cancelLabel = errorMessage.messageButtonList[0];
                    break;
            }

            switch(numButtons){
                case 4:
                    buttons = Alert.FOURTH | Alert.THIRD | Alert.SECOND | Alert.FIRST;
                    break;
                case 3:
                    buttons = Alert.THIRD | Alert.SECOND | Alert.FIRST;
                    break;
                case 2:
                    buttons = Alert.SECOND | Alert.FIRST;
                    break;
                case 1:
                    buttons = Alert.FIRST;
                    break;
                default:
                    buttons = 0;
            }

            processedBody = processInserts(processedBody, inserts);
            processedTitle = processInserts(processedTitle, inserts);

            Alert.show(processedBody,
                    processedTitle,
                    buttons,
                    closeFunction
                );
        }

        private function displayErrorTemporalStandard(errorMessage:ErrorMessage, inserts:Array = null):void{
            var processedBody:String = errorMessage.messageBody;
            var processedTitle:String = errorMessage.messageTitle;

            processedBody = processInserts(processedBody, inserts);
            processedTitle = processInserts(processedTitle, inserts);

            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, [processedTitle, processedBody]));
        }

        private function displayErrorTemporalCloud(errorMessage:ErrorMessage, inserts:Array = null):void{
            var processedBody:String = errorMessage.messageBody;
            var processedTitle:String = errorMessage.messageTitle;

            processedBody = processInserts(processedBody, inserts);
            processedTitle = processInserts(processedTitle, inserts);

            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, [processedTitle, processedBody, OverlayMessage.CLOUD_SKIN]));
        }

        public static function processInserts(src:String, inserts:Array = null):String {
            var result:String = "";
            var match:Object = INSERT_REGEXP.exec(src);
            var error:Boolean = false;
            var lastEnd:int = 0;
            while(match != null) {
                result += src.substring(lastEnd, match.index);
                lastEnd = match.index + match[0].length;
                var index:int = int(match[1])-1;
                if(inserts != null && index >= 0 && index < inserts.length) {
                    result += inserts[index];
                } else {
                    result += match[0];
                    error = true;
                }
                match = INSERT_REGEXP.exec(src);
            }
            result += src.substring(lastEnd);
            if(error) {
                LogService.error("Error processing message ["+src+"] yielding ["+result+"]" + (inserts?("inserts ["+inserts.join(",")+"]"):"Null Inserts"));
            }
            return result;
        }
    }
}
