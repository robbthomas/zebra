package com.alleni.author.controller {
	import caurina.transitions.Tweener;
	
	import com.alleni.author.application.palettes.ToolboxPanel;
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.objects.PathEditingMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.action.PathEditAction;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.event.PathEvent;
	import com.alleni.author.model.objects.Path;
	import com.alleni.author.model.objects.PathNode;
	import com.alleni.author.model.objects.PathObject;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.feedback.PathNodeHandle;
	import com.alleni.author.view.objects.PathObjectView;
	import com.alleni.taconite.controller.DragMediator;
	import com.alleni.taconite.definition.HandleDescription;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.lang.Geometry;
	import com.alleni.taconite.persistence.json.JSON;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	
	public class PathHandleDragMediator {
		
		// this class does NOT extend DragMediator, because it has unusual needs to continue dragging
		// to extend the path after mouse-up occurs.  The mouseUp means to make a straight line.
	
		private var _view:PathObjectView;
		private var _curve:Sprite;  // the sprite containing path graphic, offset from _view by anchorPoint
		private var _context:ViewContext;
		private var _dragPoint:Point;
		private var _dragStarted:Boolean;
		private var _handle:PathNodeHandle;
		private var _path:PathObject;
		private var _oldPath:Path;
		private var _oldPosition:Point;
		private var _oldAnchor:Point;
		private var _movePoint:Point;
		private var _wasSelected:Boolean;
		private var _mouseIsDown:Boolean;
		
		// timer used for two things:  hold-mouse to extend-path, and pause-extend to drop-round-point
		private var _pausePoint:Point;
		private var _pauseTime:uint;
		private var _pauseFired:Boolean;
		
		private var _requestRound:Boolean;
		private var _snapping:Boolean;  // snap to round/sharp depending on angle
		
		private var _keysHeldDown:Array;
		private var _clickTime:int = 0;
		
		private static const DOUBLE_CLICK_TIME:int = 200;  // mSecs
		
		
		public static var minimumDrag:Number = 3;
		private static const PAUSE_TIME:int = 400;
		private static const HOLD_TIME:int = 400;
	
		public function PathHandleDragMediator(context:ViewContext)
		{
			_context = context;
		}
	
		public function handleViewEvents(handle:DisplayObject, view:PathObjectView):void
		{
			_handle = handle as PathNodeHandle;
			_view = view;
			_curve = view.curve;
			_path = view.model.value as PathObject;
			handle.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		}
		
		public function clearViewEvents():void
		{
			_handle.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			clearDragEvents();
		}
		
		public function clearDragEvents():void
		{
			_context.stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_context.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			_context.stage.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDownExtend, true);
			_context.stage.removeEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			_context.stage.removeEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
			_context.stage.removeEventListener(Event.ENTER_FRAME, enterFrame);
			_path.removeEventListener(PathEvent.FINISH_EXTENDING, finishExtendingListener);
		}
			
		public function handleMouseDown(event:MouseEvent):void
		{
			if(_context.stage == null) {
				return;
			}
			_mouseIsDown = true;
			event.stopImmediatePropagation();
			
			if (_path.nodeCount > 1) {
				_oldPath = _path.path.clone();
				_oldPosition = new Point(_path.x, _path.y);
				_oldAnchor = _path.anchorPoint.clone();
			} else {
				_oldPath = null;  // don't allow undo to the original single point
			}
			if (_path.nodeCount == 1) {
				_path.extendingPath = true;
				_handle.visible = false;
			} else if (!_path.extendingPath) {
				var selected:Boolean = _handle.node.selected;
				_wasSelected = selected;
				if (event.shiftKey)
					_path.selectNode(nodeIndex, !selected, true);  // select=!sel, extend=true
				else
					_path.selectNode(nodeIndex, true, selected);  // select=true, extend=sel
			}
			setDragPoint(event);
			_context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_context.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			_context.stage.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDownExtend, true, 1);  // priority above background objects
			
			_context.stage.focus = null;
			_context.stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			_context.stage.addEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
			_keysHeldDown = [];
			_context.stage.addEventListener(Event.ENTER_FRAME, enterFrame);
			_path.addEventListener(PathEvent.FINISH_EXTENDING, finishExtendingListener);
			_pausePoint = null;
		}
		
		private function setDragPoint(event:MouseEvent):void
		{
			// called by mouseDown & handleMouseDownExtend
			_dragStarted = false;
			_dragPoint = new Point(event.stageX, event.stageY);
			_movePoint = _dragPoint.clone();
		}
		
		private function handleMouseDownExtend(event:MouseEvent):void
		{
			_mouseIsDown = true;
			event.stopImmediatePropagation();
			setDragPoint(event);
			
			// had trouble getting double-click to end path extending
			var time:int = getTimer();
			trace("click time="+(time - _clickTime));
			if (time - _clickTime < DOUBLE_CLICK_TIME) {
				trace("double-click timer caught");
				_view.dispatchEvent(new MouseEvent(MouseEvent.DOUBLE_CLICK));
				return;
			}
			_clickTime = time;
			
			// had released mouse to get a straight line section; now back down for a curve section
			var pos:Point = _curve.globalToLocal(new Point(event.stageX, event.stageY));
			_path.setSharp(nodeIndex, false);  // round the point under the mouse (which doesn't draw a handle)
			_path.extendPath(pos, true, nodeIndex);  // sharp point where mouse went down
			_pausePoint = null;
		}
	
		private function handleMouseMove(event:MouseEvent):void
		{
			if (!_dragStarted
				&& _dragPoint != null
				&& (Math.abs(event.stageX -_dragPoint.x) >= minimumDrag || Math.abs(event.stageY -_dragPoint.y) >= minimumDrag))
			{
				_dragStarted = true;
			}
	
			if (_dragStarted)
			{
				handleDragMove(event);
			}
		}
		
		private function handleDragMove(event:MouseEvent):void
		{
			var newPos:Point = _curve.globalToLocal(new Point(event.stageX, event.stageY));
			if (_requestRound) {
				_requestRound = false;
				roundAction(newPos);
			} if (_path.extendingPath) {
				if (_mouseIsDown) {
					var added:Boolean = _path.extendPath(newPos, false, nodeIndex);  // sharp=false
					if (added)
						_snapping = false;
					else if (_snapping)
						doSnap();
				} else {  // mouse up: rubber-banding a straight-line 
					// stop extending if rollover toolbox or out of the gray canvas area
					if (PathEditingMediator.hitTestToolbox(event.stageX, event.stageY)
							||  !EditorUI.instance.canvasVisibleRect(_view.stage).contains(event.stageX, event.stageY))
						finishExtendingPath();
					else
						_path.setPoint(nodeIndex, newPos);
				}
			} else if (_handle.node.selected) {   // dragging one or more selected nodes
				var global:Point = new Point(event.stageX, event.stageY);
				if (event.shiftKey) {
					var base:Point;  // when dragging last point constraint base = next-to-last; otherwise base is click-point
					if (_path.path.selectedNodeCount == 1 && _path.nodeCount > 1 && nodeIndex == _path.nodeCount-1)
						base = _curve.localToGlobal(_path.path.getPoint(_path.nodeCount-2));
					else
						base = _dragPoint;
					global = Geometry.snapToDirection(base, global, true);
				}
				var local:Point = _curve.globalToLocal(global);
				var delta:Point = local.subtract(_curve.globalToLocal(_movePoint));  // delta of local coordinates
				_path.moveSelectedNodes(delta.x, delta.y);
				_movePoint = global; // affected by constrain
			}
		}
				
		private function handleMouseUp(event:MouseEvent):void
		{
			_mouseIsDown = false;
			if (_path.extendingPath) {
				if (_path.nodeCount == 1 && !_dragStarted)
					_path.setSharp(0, true);
				if (_dragStarted || _path.nodeCount == 1) {
					var pos:Point = _curve.globalToLocal(new Point(event.stageX, event.stageY));
					_path.setSharp(nodeIndex, true);
					_path.extendPath(pos, true, nodeIndex);  // sharp=true
				}
				_requestRound = false;
				_snapping = false;
			} else {
				finishDrag();
			}
		}
		
		private function finishExtendingListener(event:PathEvent):void
		{
			finishExtendingPath();
		}
		
		private function finishExtendingPath():void
		{
			if (_path.nodeCount > 1 && nodeIndex == _path.nodeCount-1)
				_path.deletePoint(_path.nodeCount-1);  // when extending from end, drop the last point on pressing Enter
			finishDrag();
		}
		
		private function finishDrag():void
		{
			clearDragEvents();
			_handle.visible = true;  // was false during extend-path
			_pausePoint = null;
			_path.finish();
			
			if (!_path.extendingPath && _wasSelected && _handle.node.selected && !_dragStarted) {
				var index:int = nodeIndex;
				_path.setSharp(index, !_path.path.getSharp(index));  // toggle sharp
			}
			_path.extendingPath = false;
			
			if (_oldPath)
				ApplicationController.currentActionTree.commit(PathEditAction.fromObject(_path, _oldPath, _oldPosition, _oldAnchor));
			_oldPath = null;
			_oldPosition = null;
			_oldAnchor = null;
		}
		
		private function handleKeyDown(event:KeyboardEvent):void
		{
			if (nodeIndex < 0)
				return;
			if (_keysHeldDown[event.keyCode]) {
				trace("IGNORE REPEAT key="+event.keyCode);
				return;  // that key was already down ... its repeating
			}
			
			switch (event.keyCode) {
				case Keyboard.PERIOD:
					_path.addPoint(nodeIndex, new Point(_curve.mouseX, _curve.mouseY), _handle.node.sharp);
					break;
				case Keyboard.DELETE:
				case Keyboard.BACKSPACE:
					if (_path.extendingPath && nodeIndex > 0 && _path.nodeCount > 2)
						_path.deletePoint(nodeIndex-1);  // delete the point before the one being dragged
					break;
			}
			_keysHeldDown[event.keyCode] = true;
		}
		
		private function handleKeyUp(event:KeyboardEvent):void
		{
			if (nodeIndex < 0)
				return;
			if (_keysHeldDown[event.keyCode])
				delete _keysHeldDown[event.keyCode];
		}
		
		private function enterFrame(event:Event):void
		{
			var point:Point = new Point(_handle.stage.mouseX, _handle.stage.mouseY);
			if (_pausePoint == null && _mouseIsDown) {
				_pausePoint = point;
				_pauseTime = getTimer();
				_pauseFired = false;
			} else if (_pausePoint && _pausePoint.equals(point)) {
				var waitTime:Number = (_path.extendingPath) ? PAUSE_TIME : HOLD_TIME;
				if (!_pauseFired && getTimer() - _pauseTime > waitTime) {
					if (_path.extendingPath) {
						if (_dragStarted)
							pauseAction();
					} else {
						holdAction();
					}
					_pauseFired = true;
				}
			} else {
				_pausePoint = null;
			}
		}
		
		private function pauseAction():void
		{
			if (_path.nodeCount > 1) {
				var event:PathEvent = new PathEvent(PathEvent.REQUEST_FLASH);  // request circles around it
				event.index = nodeIndex;
				_path.dispatchEvent(event);
				_requestRound = true;
				_snapping = false;
			}
		}
		
		private function roundAction(pos:Point):void
		{
//			trace("roundAction");
			var node:PathNode = _handle.node;
			var index:int = nodeIndex;
			node.sharp = false;  // this will still be the last node
			var point:Point = _path.path.getPoint(index);
			node.x = pos.x;
			node.y = pos.y;
			_path.addPoint(index, point, false);  // insert before last node: round point
			_snapping = true;
		}
		
		private function doSnap():void
		{
			var node:PathNode = _handle.node;
			var index:int = nodeIndex;
//			trace("doSnap index="+index);
			if (index > 1) {
				var prev:Point = _path.path.getPoint(index-1);
				var prevPrev:Point = _path.path.getPoint(index-2);
				var prevDelta:Point = prev.subtract(prevPrev);
				var prevAngle:Number = Math.atan2(prevDelta.y, prevDelta.x);
				var point:Point = _path.path.getPoint(index);
				var delta:Point = point.subtract(prev);
				var angle:Number = Math.atan2(delta.y, delta.x);
				if (angle-Math.PI > prevAngle)
					angle -= 2*Math.PI;
				if (angle+Math.PI < prevAngle)
					angle += 2*Math.PI;
				var degrees:Number = (angle - prevAngle) * 180 / Math.PI;
//				trace("degrees="+degrees);
				var sharp:Boolean = Math.abs(degrees) > 45;
				_path.setSharp(index-1, sharp);
			}
		}
		
		private function holdAction():void
		{
			if (!_dragStarted && _path.path.selectedNodeCount == 1) {
				var event:PathEvent = new PathEvent(PathEvent.REQUEST_FLASH);  // request circles around it
				event.index = nodeIndex;
				_path.dispatchEvent(event);
				_path.extendingPath = true;
				
				if (_path.path.getSharp(nodeIndex)) {
					_path.setSharp(nodeIndex, false);
					_path.extendPath(_path.path.getPoint(nodeIndex), true, nodeIndex);
				}
			}
		}
		
		private function get nodeIndex():int
		{
			return _handle.nodeIndex;
		}
		
	}
}