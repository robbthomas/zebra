package com.alleni.author.controller.objects
{
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.view.objects.tables.AbstractTableView;
import com.alleni.taconite.factory.SerializerImplementation;
import com.alleni.taconite.model.TaconiteModel;

import mx.collections.IList;
import mx.containers.TabNavigator;

public class AnswerTableController extends TableController
	{
		public static var instance:AnswerTableController = new AnswerTableController();

		public function AnswerTableController()
		{
			super();
		}

		override protected function addColumns(table:AbstractTable):void
		{
			if(table.rearranging){
				return;
			}
			super.addColumns(table);
			var t:AnswerTable = AnswerTable(table);
			var added:int = table.numColumns - t.allLogicValues.length / table.port.length;
			var row:int;
			var col:int;
			for(row=0; row < table.port.length; row++) {
				for(col=table.numColumns-added; col < table.numColumns; col++) {
					t.allLogicValues.addItemAt("", row*table.numColumns + col);
					t.allLogicFulfilled.addItemAt(false, row*table.numColumns + col);
				}
			}
			added = t.numColumns - t.logicColumns.length;
			for(col=table.numColumns-added; col < table.numColumns; col++) {
				var ast:Object = t.parse(col);
				t.logicColumns.addItem(ast);
			}
            t.copySectionToInitialValues(TableHeaderDragMediator.COLUMNS);
		}
		
		override protected function judgeNow(table:AbstractTable):void
		{
            if(!table.poweredOnRun) {
                return;
            }
            table.column = 0;
			var t:AnswerTable = table as AnswerTable;
			var newCol:int = t.firstFulfilledColumn();
            if(newCol >= -1) {
                table.column = newCol +1;
            }
		}
		
		override protected function resizeLogic(table:AbstractTable):void
		{
			super.resizeLogic(table);
			var t:AnswerTable = table as AnswerTable;
			while(t.port.length * t.numColumns > t.allLogicValues.length) {
				// add one row's worth of cells
				for(var i:int=0; i<table.numColumns; i++) {
					t.allLogicValues.addItem("");
					t.allLogicFulfilled.addItem(false);
				}
			}
		}

		override public function handleLogicTruthChanged(table:AbstractTable, row:int, truth:Boolean, startingColumn:int = 0):void
		{
			var newCol:int = -1;
			var t:AnswerTable = table as AnswerTable;
			for(var col:int = startingColumn; col < table.numColumns; col++) {
				if(t.fulfill(row, col, truth) && newCol < 0) {
					newCol = col;
				}
			}
			if(newCol >= -1 && table.continuallyJudging) {
				table.column = newCol + 1;
			}
		}

        public function handleContiuneBranch(table:AbstractTable, row:int, truth:Boolean, startingColumn:int = 0):void
		{
			var newCol:int = -1;
			var t:AnswerTable = table as AnswerTable;
			for(var col:int = startingColumn; col < table.numColumns; col++) {
				if(t.fulfill(row, col, truth) && newCol < 0) {
					newCol = col;
				}
			}
			if(newCol >= -1) {
				table.column = newCol + 1;
			}
		}

        public function makeValueRowsSafe(table:AnswerTable):void{
            /*This method cleans table value row data that have become corrupt for an unknown reason.  We have been looking for a solution
              for quite some time and have not found a pattern.  This will make a project or gadget safe to restore to initial values and/or
              run properly in the player.

              This is not an ideal solution, but it does get appear to make projects safe.
              Fixes ticket ZZX-1445 as well as others that we have not been able to reproduce over the last year or so.

              rthomas 12-12-12
             */
            var rows:int = Math.floor(table.allStateValues.length/table.numColumns);
            while(table.stateValueExpressions.length > rows){
                table.stateValueExpressions.removeItemAt(table.stateValueExpressions.length - 1);
                ApplicationController.instance.authorController.log("removed a corrupt piece of data from table.stateValueExpressions");
            }
            while(table.stateValueBound.length > rows){
                table.stateValueBound.removeItemAt(table.stateValueBound.length - 1);
                ApplicationController.instance.authorController.log("removed a corrupt piece of data from table.stateValueBound");
            }
            while(table.stateValueName.length > rows){
                table.stateValueName.removeItemAt(table.stateValueName.length - 1);
                ApplicationController.instance.authorController.log("removed a corrupt piece of data from table.stateValueName");
            }
            while(table.stateValueAst.length > rows){
                table.stateValueAst.removeItemAt(table.stateValueAst.length - 1);
                ApplicationController.instance.authorController.log("removed a corrupt piece of data from table.stateValueAst");
            }
            while(table.stateValueType.length > rows){
                table.stateValueType.removeItemAt(table.stateValueType.length - 1);
                ApplicationController.instance.authorController.log("removed a corrupt piece of data from table.stateValueType");
            }
            while(table.stateValueTypeConstraints.length > rows){
                table.stateValueTypeConstraints.removeItemAt(table.stateValueTypeConstraints.length - 1);
                ApplicationController.instance.authorController.log("removed a corrupt piece of data from table.stateValueTypeConstraints");
            }
        }

		override public function reset(table:AbstractTable):void
		{
			var t:AnswerTable = table as AnswerTable;
			if (!t) return;
		    t.clearFulfilled();
            t.currentStateValues = SerializerImplementation.deserializeValue(t.initialValues.currentStateValues, t.currentStateValues) as IList;
		}
	}
}
