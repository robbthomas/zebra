/**
 * © Copyright 2011 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: ObjectView.as 9948 2011-04-23 16:20:48Z pkrekelberg $
 */
package com.alleni.author.controller.objects
{
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.objects.layout.FreeLayout;
import com.alleni.author.controller.objects.layout.GridLayout;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.feedback.ArenaInsertionFeedback;
import com.alleni.author.view.objects.ArenaPageView;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObjectContainer;
import flash.geom.Point;
import flash.geom.Rectangle;

import mx.collections.IList;

public class ArenaLayoutManager
	{
		public static const NONE:uint				= 0;
		public static const GRID:uint 				= 1;
		public static const HBOX:uint				= 2;
		public static const VBOX:uint				= 3;

		public static var instance:ArenaLayoutManager = new ArenaLayoutManager();
		
		private static var feedBack:ArenaInsertionFeedback;

		/**
		 * Constructor 
		 * 
		 */
		public function ArenaLayoutManager()
		{
		}
		
		/**
		 * Creates a reusable feedback sprite for layout feedback;
		 *  
		 * @param arena
		 * @param target
		 * @param worldView
		 * 
		 */
		public function drawInsertFeedBack(page:ArenaPage, index:int, selected:ObjectView):void
		{
            if (page.transientDraggingCells && page.transientDraggingCells.length > 0) {
                var bounds:Rectangle = GridLayout.instance.getCellBounds(page, index);

                if(!feedBack) feedBack = new ArenaInsertionFeedback(feedbackContainer);
                feedBack.clear();
                if(!feedbackContainer.contains(feedBack))
                {
                    feedbackContainer.addChild(feedBack);
                }
                feedBack.insertIndicatorColor = page.insertIndicatorColor;
                feedBack.insertIndicatorWeight = page.insertIndicatorWeight;
                var pageView:ArenaPageView = page.getView() as ArenaPageView;
                var rotate:Number = pageView.computeNetRotation();
                var horizontal:Boolean = (page.columns == 1 && !page.growHorizontal);
                var p1:Point = bounds.topLeft;
                var p2:Point;
                if (horizontal) {
                    p2 = new Point(bounds.right,bounds.top);
                } else {
                    p2 = new Point(bounds.left,bounds.bottom);
                }
                var srcContainer:DisplayObjectContainer = pageView.childViewContainer;
                p1 = feedbackContainer.globalToLocal(srcContainer.localToGlobal(p1));
                p2 = feedbackContainer.globalToLocal(srcContainer.localToGlobal(p2));
                feedBack.drawInsertFeedback(p1, p2, rotate, horizontal);
            }
		}

        private function get feedbackContainer():DisplayObjectContainer
        {
            return WorldContainer(Application.instance.viewContext).feedbackControlsView;
        }

		/**
		 * Removes the feedBack sprite from presentation container 
		 * but doesnt destroy it.
		 * 
		 */
		public function removeInsertFeedBack():void
		{
			if(feedBack) 
			{
				feedBack.clear();
				if(feedbackContainer.contains(feedBack))
				{
                    feedbackContainer.removeChild(feedBack);
				}
			}
			
		}
		
		public function requestRecalcLayout(pageView:ArenaPageView, animate:Boolean=true, force:Boolean=false):void
		{
            var page:ArenaPage = pageView.object as ArenaPage;
            var context:ViewContext = pageView.context;
			if(force) {
				recalcLayout(pageView, animate, force);
			} else if (!page.transientRequestRecalc && !page.transientDoingRecalc) {

                page.transientRequestRecalc = true;  // prevents redundant requests

				ApplicationController.instance.requestLayoutAction(function():void{
					recalcLayout(pageView,animate);
				});
			}
		}

		private function recalcLayout(pageView:ArenaPageView,animate:Boolean=true,force:Boolean=false):void
		{
            var page:ArenaPage = pageView.object as ArenaPage;
			if (!page.transientDoingRecalc || force)
			{
                page.transientDoingRecalc = true;
                if (pageView.stage) {
                    recalcAlignedObjects(pageView, animate);
                }
                page.transientDoingRecalc = false;

                page.transientRequestRecalc = false;
            }
		}
		
		private function recalcAlignedObjects(pageView:ArenaPageView,animate:Boolean=true):void {

            var page:ArenaPage = pageView.object as ArenaPage;
            var context:ViewContext = pageView.context;
			var objects:IList = page.objects;
			  // true if arena has alignment set
            if (page.objects.length == 0) {
                page.contentWidth = 0;
                page.contentHeight = 0;
            } else {

                switch(page.layout)
                {
                    case NONE:
                        page.defaultColumnsApplied = false;
                        FreeLayout.instance.position(pageView,animate);
                        break;
                    case HBOX:
                        page.growHorizontal = true;
                        page.columns = 1;
                        page.rows = 1;
                        page.defaultColumnsApplied = false;
                        GridLayout.instance.position(pageView,animate);
                        break;
                    case VBOX:
                        page.growHorizontal = false;
                        page.columns = 1;
                        page.rows = 1;
                        page.defaultColumnsApplied = false;
                        GridLayout.instance.position(pageView,animate);
                        break;
                    case GRID:
                        if(!page.defaultColumnsApplied && page.columns == 1)
                        {
                            page.defaultColumnsApplied = true;
                            page.columns = 4;
                        }
                        page.growHorizontal = false;
                        GridLayout.instance.position(pageView,animate);
                        break;
                }
            }
            page.transientAuthorDrag = false;
		}


	}
}
