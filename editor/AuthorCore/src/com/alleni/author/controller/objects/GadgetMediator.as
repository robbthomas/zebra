package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.controller.ObjectDragMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.DropIntoContainerEvent;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.InletDescription;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.OutletDescription;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.ViewRoles;
	import com.alleni.author.event.WireEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;

	public class GadgetMediator
	{
		protected var _context:ViewContext;
		private var _view:ObjectView;
		private var _role:uint;
		private var _model:TaconiteModel;
		private var _object:AbstractObject;
		private var _portsArray:Array = [];
		
		public function GadgetMediator(context:ViewContext)
		{
			_context = context;
		}
		
		public function handleViewEvents(view:ObjectView, role:uint):void
		{
			_view = view;
			_role = role;
			_model = view.model;
			_object = _model.value as AbstractObject;
			
			_model.addEventListener(WireEvent.COMPLETE,wireCompleteListener);
			if(_role == ViewRoles.PRIMARY) {
				_view.model.addEventListener(DropIntoContainerEvent.DROP_INTO_CONTAINER, handleDropIntoContainer);
			}
		}
		
		public function clearViewEvents() : void
		{
			_model.removeEventListener(WireEvent.COMPLETE,wireCompleteListener);
			if(_role == ViewRoles.PRIMARY) {
				_view.model.removeEventListener(DropIntoContainerEvent.DROP_INTO_CONTAINER, handleDropIntoContainer);
			}
		}
		
		private function handleDropIntoContainer(evt:DropIntoContainerEvent):void
		{
			ObjectDragMediator.dropIntoContainer(_view, evt.location);
		}
		
		/**
		 * Create an anchor and add it to the appropriate list in the object.
		 * The type of anchor (property / inlet / outlet) is determined by the key name.
		 * Returns the created anchor. 
		 */
		protected function createAnchor(key:String):WireAnchor
		{
			var anchor:WireAnchor = new WireAnchor();
			anchor.hostObject = _object;
			anchor.hostProperty = key;
			
			trace("GadgetMediator::createAnchor key="+key, "model="+_model, 
				"inletCount="+_object.inletRibbons.length,
				"propCount="+_object.propertyRibbons.length);

			
			var modifier:IModifier = Modifiers.instance.fetch(key);
			if (modifier is PropertyDescription) {
				var prop:PropertyDescription = modifier as PropertyDescription;
				anchor.modifierDescription = prop;
				_object.propertyRibbons.addItem(anchor);  // permanently add this anchor to the object (creates model)
			} else if (modifier is InletDescription) {
				var inlet:InletDescription = modifier as InletDescription;
				anchor.modifierDescription = inlet;
				_object.inletRibbons.addItem(anchor);  
			} else {
				Utilities.assert(modifier is OutletDescription);
				var outlet:OutletDescription = modifier as OutletDescription;
				anchor.modifierDescription = outlet;
				_object.outletRibbons.addItem(anchor); 
			}

			anchor.wired = ApplicationController.instance.wireController.objectHasWires(_object,modifier);
			return anchor;			
		}
		
		
		private function wireCompleteListener(e:WireEvent):void {
			
			trace("GadgetMediator::wireCompleteListener",_model);
			if (e.role == _role) {
				handleNewWire(e.wire);
				e.stopImmediatePropagation();
			}
		}
		
		protected function handleNewWire(wire:Wire):void
		{
			// this is overridden by truth table
		}
		
	}
}