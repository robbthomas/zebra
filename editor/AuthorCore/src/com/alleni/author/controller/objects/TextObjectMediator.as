/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */


package com.alleni.author.controller.objects
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.application.controlArea.RightControlBar;
	import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.controller.ui.palettes.InspectorController;
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.DoubleClickAction;
	import com.alleni.author.definition.action.ModifyRibbonAction;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.definition.text.StyleMediator;
	import com.alleni.author.definition.text.TLFStyleSheet;
	import com.alleni.author.definition.text.TextSelection;
	import com.alleni.author.definition.text.TransformedStyleSheet;
	import com.alleni.author.event.TextObjectEvent;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.TextObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.util.CSSFormatResolver;
	import com.alleni.author.view.objects.TextObjectView;
	import com.alleni.author.view.text.RichTextField;
	import com.alleni.taconite.model.ITaconiteObject;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.StyleSheet;
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	import flash.utils.Dictionary;
	
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.events.SelectionEvent;
	import flashx.textLayout.formats.TextDecoration;
	import flashx.textLayout.formats.TextLayoutFormat;

	
	public class TextObjectMediator
	{
		private var _view:TextObjectView;
		private var _textObj:TextObject;
		private var _role:uint;
		private var _textEditMediator:TextEditMediator;
		private var _textField:RichTextField;
		private var _oldTextFlow:TextFlow;
		private var _textSelection:TextSelection;
		private var _selectedTextElements:Vector.<ITaconiteObject>;
		private var _TLF2CSS:Dictionary = new Dictionary();
		private var _styleMediator:StyleMediator;
		private var _title:String = "Text Selection";
		private var _lastTitle:String = "";
		
		
		public function TextObjectMediator()
		{
			
			//seedStyleSheets();  //remove this after testing  -- this creates some dummy css assets
			this.mapTLF2CSS();
		}
		
		

		public function handleViewEvents(view:TextObjectView, textField:RichTextField):void
		{
			_view = view;
			_textObj = view.model.value as TextObject;
			_role = view.role;
			_textField = textField;
			_oldTextFlow = _textField.textFlow;
			
			_textSelection = new TextSelection(_textObj,_textField);
			_selectedTextElements = Vector.<ITaconiteObject>([_textSelection]);
			
			_textEditMediator = new TextEditMediator();
			
			// we give the text-editor a hittest function, so clicks anywhere in bounds of text object will continue editing...
			// (otherwise a click on the right side of a small or empty text will close editing)
			
			_textEditMediator.handleTextEditEvents(_textField,
					function(e:MouseEvent):int{return DoubleClickAction.IGNORE},  // opened in doubleClick listener, below 
					function(tx:RichTextField,val:String):void{}, 
					null, handleCloseEditing, closeEditingHitTest, true, true);
			
			_view.addEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
			
			// listen for event on mouseUp with tool, telling us to open for editing
			_view.addEventListener(TextObjectEvent.OPEN_EDITING, handleOpenEditing);
			
			_styleMediator = new StyleMediator(_textObj,"zebradefault");
			
		}
		
		public function get styleMediator():StyleMediator
		{
			return _styleMediator;
		}
		
		
		public function handleTextFlowChange():void
		{
			if (_oldTextFlow)
				_oldTextFlow.removeEventListener(SelectionEvent.SELECTION_CHANGE, selectionChanged);
			
			_textField.textFlow.addEventListener(SelectionEvent.SELECTION_CHANGE, selectionChanged);
			_oldTextFlow = _textField.textFlow;
			this.handleStyleSheetOverride();  //added 05-11-11
		}

		public function closeEditingHitTest(x:Number,y:Number,shapeFlag:Boolean = false):Boolean
		{
			// check to see if the click was in the inspector area
			// if so then treat this as clicking in the text area itself
			// which will not stop editing
			// todo: remove this asap for something which does not depend on ui elements which only exist in the current incarnation of the editor ui.
			var rightBox:RightControlBar = EditorUI.instance.rightControlBar;
			var topLeft:Point = rightBox.localToGlobal(new Point(0,0));
			if (x > topLeft.x) {
				return true;
			}
			return _view.hitTestPoint(x, y, shapeFlag);
		}
		
		public function clearViewEvents():void
		{
			_view.removeEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
			_view.addEventListener(TextObjectEvent.OPEN_EDITING, handleOpenEditing);
		}
		
		
		private function handleOpenEditing(event:TextObjectEvent):void
		{
			if (_textEditMediator && !Application.runningLocked && !_textObj.childOfClosedMasterPage)
				_textEditMediator.openEditing();  // open editing for an object just drawn by the author, or for single-click at runtime
            if (!Application.runningLocked)
			    InspectorController.instance.selectedTextElements = _selectedTextElements;
			
		}
		
		private function handleCloseEditing(text:String):void
		{
			TextObjectController.instance.makeObject(_view.model, text);
			
			var tlfMarkup:String = _textField.exportMarkup();
			trace("tlf:  ",tlfMarkup);
			var oldValue:FormattedText = _textObj.source;
			
			var formattedText:FormattedText =  new FormattedText(tlfMarkup); 
			formattedText.cssObject =  _textObj.css;  //source needs to hold a definition of ZebraDefault, which comes from css
			_textObj.source = formattedText;
			_textObj.initialValues["source"] =formattedText.toJson();
			ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(_textObj, "source", oldValue, "Edit text on " + _textObj.title));
			
			_textObj.initialValues["fontColor"]  = _textObj.fontColor;
			_textObj.initialValues["fontSize"]  = _textObj.fontSize;
			_textObj.initialValues["fontFamily"]  = _textObj.fontFamily;
			_textObj.initialValues["fontAlpha"]  = _textObj.fontAlpha;
			_textObj.initialValues["italic"]  = _textObj.italic;
			_textObj.initialValues["bold"]  = _textObj.bold;
			_textObj.initialValues["underline"]  = _textObj.underline;
			
		}
		
		
		private function handleDoubleClick(event:MouseEvent):void
		{
			// PLAYERTODO
			if (Application.runningLocked  || _textObj.locked || _textObj.childOfClosedMasterPage)
				return;
			var action:int = _view.doubleClickAction;
			trace("button dbl action="+action);
			switch (action) {
				case DoubleClickAction.EDIT:
					_textEditMediator.openEditing();
					event.stopImmediatePropagation();  // don't go into Pause mode
					break;
				case DoubleClickAction.STOP:
                case DoubleClickAction.IGNORE:
					event.stopImmediatePropagation();
					break;
			}
		}

        public function closeEditingIfDeselected():Boolean /* did we actually close editing? */ {
             if (_textObj.editing && !_view.selected) {
//                 clearViewEvents();
                 _textEditMediator.closeEditing(true);
                 return true;
             }
            return false;
        }
	
		private function selectionChanged(selectionEvent:SelectionEvent):void
		{
//			trace("TextObjectMediator>SelectionChanged");
			_textField.setFocus(); //?
			
			updateTextSelectionFormat();

			if(_textSelection.format){

				if(!Application.runningLocked)
				    InspectorController.instance.selectedTextElements = _selectedTextElements;
				
				if(_textSelection.isAllSelected()){
					_title = "Text";
					}
					else{
					_title = "Text Selection";
					}
				
				if(_title != _lastTitle && !Application.runningLocked){
					InspectorController.instance.setTitle(_title);
					_lastTitle = _title;
				}
			}
			
			else{
				trace("TextObjectMediator > selectionChanged reports a problem");
			}
		}
		
		public function seedStyleSheets():void{  //testing purposes only
		
			var cssText:String = ".heading {font-size:14pt;font-family:Arial; color:#00FF00;text-decoration:none}" + 
				".footer {font-size:12pt;font-name:Arial;color:#AA0000;text-decoration:none}";
			
			var cssText2:String = ".heading {font-size:30pt;font-family:Comic Sans; color:#FF0000;text-decoration:underline}" + 
				".footer {font-size:15pt;font-family:Comic Sans;color:#0000AA;text-decoration:underline}";
			
			var defaultCSSText:String = ".heading {font-weight:bold;font-style:italic;font-size:25pt;font-family:sans-serif; color:#CCCCCC;text-decoration:underline}" + 
				".footer {font-size:12pt;font-name:Arial;color:#AA0000;text-decoration:none;font-style:italic;font-weight:bold}";
			
			var asset1:Asset = new Asset("1");
			asset1.content = cssText;
			asset1.name = "Corporate";
			asset1.type = AssetType.CSS;
			AssetController.instance.addToModel(asset1); //just add to model -- don't import yet
		
			var asset2:Asset = new Asset("2");
			asset2.content = cssText2;
			asset2.name = "Circus";
			asset2.type = AssetType.CSS;
			AssetController.instance.addToModel(asset2);
			
			var asset3:Asset = new Asset("3");
			asset3.content = defaultCSSText;
			asset3.name = "Default";
			asset3.type = AssetType.CSS;
			AssetController.instance.addToModel(asset3);
		}
		
		public function handleStyleSheetOverride():void{
			
			var styleSheet:StyleSheet = new TLFStyleSheet();
			var css:String = _textObj.css.makeCSS();
			styleSheet.parseCSS(css);
			var wasAllTextSelected:Boolean = _textSelection.isAllSelected();
			updateCSSFormatResolver(styleSheet);
			
			if(wasAllTextSelected)
				refreshSelection();
		}
		
		//not currently being used -- used for handling stylesheets as assets
		public function handleStyleSheets(styles:String):void{
			
			var styleSheet:StyleSheet = new TLFStyleSheet();
			var validator:TransformedStyleSheet = new TransformedStyleSheet();
			
			 var assets:Array = styles.split(";");
			 
			 for each(var asset:String in assets){
				 if (asset == null || asset == "")
					 continue;
				 
				 var css:String = AssetController.instance.model.getById(asset).content as String;
				 validator.parseCSS(css);
				 if(!validator.validCSS)
					 continue;  //there is a bad style sheet -- don't include
		
				 styleSheet.parseCSS(css);
			 }
			 
			 	updateCSSFormatResolver(styleSheet);
				addStylesToObject(styleSheet);
			
			 
		}
		
		public function updateCSSFormatResolver(styleSheet:StyleSheet):void{
			
			var cssFormatResolver:CSSFormatResolver = new CSSFormatResolver(styleSheet);
			_textField.formatResolver = cssFormatResolver;
			_textField.textFlow.flowComposer.updateAllControllers();
			//addStyleSelectors(styleSheet);
		}
		
		/*
		This function is incomplete -- waiting for a special property type that results in the creation of a combobox whose dataprovider is the stylesArray
		*/
		
		private function addStyleSelectors(styles:StyleSheet):void{
			
			var stylesArray:Array = new Array();
			
			for each(var style:String in styles.styleNames){
				
				stylesArray.push({label:style, data:style});
			}
			

		
		}
		
		/*
		This function is incomplete
		*/
		
		private function addStylesToObject(styles:StyleSheet):void{
			
			for each(var style:String in styles.styleNames){
				
				var styleObject:Object = styles.getStyle(style);
				
				for (var prop:String in _TLF2CSS){
					if(styleObject.hasOwnProperty(prop))
						_textObj.css.addProperty(style,_TLF2CSS[prop],styleObject[prop]);
				}
			
			}
		}
		
		private function mapTLF2CSS():void{
			
			_TLF2CSS["fontName"] = "font-family";
			_TLF2CSS["fontSize"] = "font-size";
			_TLF2CSS["color"]    = "color";
			_TLF2CSS["fontStyle"] = "font-style";
			_TLF2CSS["fontWeight"] = "font-weight";
			_TLF2CSS["textDecoration"] = "text-decoration";
			
		}
		
		private function updateTextSelectionFormat(event:Event = null):void{
			var format:TextLayoutFormat;
			
			if(_textSelection.isAllSelected()){
				format = this.getTextObjectFormat();
			}
			
			else{
				if(_textField && _textField.textFlow && _textField.textFlow.interactionManager){
					format = _textField.textFlow.interactionManager.getCommonCharacterFormat() as TextLayoutFormat;
				}
			else{
				format = this.getTextObjectFormat();
				}
			}
			
			if(format != null) {
				_textSelection.format = format;
			}
			else{
				trace("!!!!!!!!");
			}
		}
		
		private function clearStylesFromFirstTextElement():void{
			if(_textField)
				_textField.clearAllStylesFromFirstElement();
		}
		
		
		/*
		This captures the current settings of the textobject so that selectedText and InspectorController reflect the 
		accurate textObject level fontsize, color, etc. when all is selected
		
		*/
		private function getTextObjectFormat():TextLayoutFormat{
			var format:TextLayoutFormat;
			
			format = new TextLayoutFormat();
			format.fontSize = _textObj.fontSize>0?_textObj.fontSize:1;
			format.fontFamily = _textObj.fontFamily;
			format.textAlpha = _textObj.fontAlpha/100;
			format.fontWeight = _textObj.bold?FontWeight.BOLD:FontWeight.NORMAL;
			format.fontStyle = _textObj.italic?FontPosture.ITALIC:FontPosture.NORMAL;
			format.textDecoration = _textObj.underline?TextDecoration.UNDERLINE:TextDecoration.NONE;
			format.color = "#" + _textObj.fontColor.toString(16);
			
			trace("TextObjectMediator>updateTextSelectionFormat>getting text style format");
			return format;
		}
		
		/*
		More aggressive attempt at keeping selection when the textflow and interaction manager have changed
		from underneath us as a result of making and assigning a new CSSResolver to textflow.
		*/
		
		private function refreshSelection():void{
			if (_textField.stage)
				_textField.addEventListener(Event.ENTER_FRAME, refreshSelectionListener);
		}
		
		private function refreshSelectionListener(event:Event):void
		{
			if (_textField.stage)
				_textField.removeEventListener(Event.ENTER_FRAME, refreshSelectionListener);
			
			_textField.setFocus();
			_textField.selectAll();
			updateTextSelectionFormat();
			
		}
		
	}
}
