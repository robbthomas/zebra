package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.model.objects.MapObject;
import com.alleni.author.view.objects.MapView;
import com.alleni.taconite.model.TaconiteModel;
import com.google.maps.LatLng;
import com.google.maps.Map;

import flash.utils.Dictionary;

	public class MapController extends ObjectController
	{
		public static var instance:MapController = new MapController();
		
		public function MapController():void
		{
		}

        override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void {
            super.handleModelUpdate(model, property, newValue, oldValue, parent);
            var mapObject:MapObject = model.value as MapObject;
			switch (property){
				case "zoom":
				case "latitude":
				case "longitude":
					updateMap(mapObject);
					break;
				case "mapType":
					updateMapType(mapObject);
			}
        }

		private function updateMap(mapObject:MapObject):void
		{
			var map:Map = mapObject.map;

            if(mapObject.latitude < -85){
                mapObject.latitude = -85;
            }

            if(mapObject.latitude > 85){
                mapObject.latitude = 85;
            }

            if(mapObject.longitude < -180){
                mapObject.longitude = -180;
            }

            if(mapObject.longitude > 180){
                mapObject.longitude = 180;
            }

			if(map)
				map.setCenter(new LatLng(mapObject.latitude,mapObject.longitude), mapObject.zoom,MapView.setMapType(mapObject));
		}


		private function updateMapType(mapObject:MapObject):void
		{
			var map:Map = mapObject.map;

			if(map)
				map.setMapType(MapView.setMapType(mapObject));
		}
	}
}