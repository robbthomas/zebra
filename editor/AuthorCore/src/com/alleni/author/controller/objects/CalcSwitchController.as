package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.model.objects.CalcSwitch;
	import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.model.TaconiteModel;
	
	import mx.collections.IList;

	public class CalcSwitchController extends ObjectController
	{	
		public static var instance:CalcSwitchController = new CalcSwitchController();

		public function CalcSwitchController()
		{
		}
		
		override protected function handleModelCollectionUpdate(model:TaconiteModel, property:String, collection:IList, kind:String, index:int, item:Object, oldItem:Object):void {
			super.handleModelCollectionUpdate(model, property, collection, kind, index, item, oldItem);
			var c:CalcSwitch = model.value as CalcSwitch;
			switch(kind) {
				case ModelEvent.ADD_ARRAY_ITEM:
					break;
				case ModelEvent.CHANGE_ARRAY_ITEM:
					break;
			}
		}
		
		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
		{
			var s:CalcSwitch = model.value as CalcSwitch;
			switch (property) {
				case "value":
					var matched:Boolean = false;
					for(var i:int=0; i<s.condition.length; i++) {
						if(String(newValue) == String(s.condition[i])) {
							matched = true;
							_wiring.anchorTriggered(IList(s.anchors["output"])[i]);
						}
					}
					if(!matched) {
						_wiring.anchorTriggered(s.anchors["outputDefault"]);
					}
					break;
			}
		}
	}
}