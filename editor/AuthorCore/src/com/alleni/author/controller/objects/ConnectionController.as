/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/20/11
 * Time: 10:56 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.objects {
import com.alleni.author.controller.ObjectController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.model.objects.MetricsConnection;
import com.alleni.taconite.model.ITaconiteObject;

public class ConnectionController extends ObjectController {
	public static var instance:ConnectionController = new ConnectionController();
	override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void {
		super.handleTrigger(object, modifier);
		var r:MetricsConnection = object as MetricsConnection;
		switch(modifier.key) {
			case "send":
				r.send();
				break;
			case "receive":
				r.receive();
				break;
		}
	}
}
}
