/**
 * © Copyright 2011 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */
package com.alleni.author.controller.objects
{
import com.alleni.author.Navigation.IPage;
import com.alleni.author.Navigation.Pager;
import com.alleni.author.Navigation.PagerController;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ObjectDragMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.taconite.lang.TaconiteTimer;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;

import flash.events.Event;

import mx.events.PropertyChangeEvent;

public class ArenaController extends PagerController
	{
		public static var instance:ArenaController = new ArenaController();
		// animated flipbook paging
		private static const REPORT_PERIOD:Number = 1.5;  // seconds between updates to ActualRate
		private static var _taconiteTimer:TaconiteTimer;
		
		public function ArenaController()
		{
            super();
			_taconiteTimer = TaconiteTimer.instance;
		}

		static private function getArena(model:TaconiteModel):Arena
		{
			return model.value as Arena;
		}
		
		override public function initializeObject(model:TaconiteModel) : void
		{
			var arena:Arena = getArena(model);
			super.initializeObject(model);
		}
		
		override public function warnBeforeDelete(object:AbstractObject):void
		{
			super.warnBeforeDelete(object);
			if (object.tickListener != null)
				_taconiteTimer.unregisterListener(object.tickListener);
		}

		private function copyingMyself(arena:Arena, child:AbstractObject):Boolean
		{
			// when drag-copy a blank arena, ensure that the original keeps its gray fill, even tho the copy is dragged across & falls into it for a moment
			return (ObjectDragMediator.copyingNow && child is Arena && child.width == arena.width && child.height == arena.height);
		}

		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
		{	
			super.handleModelUpdate(model,property,newValue,oldValue,parent);
            var arena:Arena = model.value as Arena;
			if (arena && !arena.bindingsDisabled && !arena.loading) {
//              trace("ArenaController::handleModelUpdate: prop="+property, "val="+newValue, "doing="+arena.transientDoingRecalc, "arena="+arena.title);
                var page:ArenaPage = arena.currentArenaPage;
                mirrorValueToPage(arena, page, property, newValue);
                switch (property) {
                    case "currentPage":
                        var newPage:IPage = newValue as IPage;   // similar code in EventFlowController
                        var oldPage:IPage = oldValue as IPage;
                        if (arena.transientTransitionInProgress) {
                            arena.changePageAfterTransition = true;   // wait till after transition, to actually change pages
                            arena.newPage = newPage;
                            arena.oldPage = oldPage;
                            activateForPageTransitionEffect(arena, newPage, oldPage);  // activate only masters common to new & old pages
                        } else {
                            handlePageChange(arena, newPage, oldPage);   // immediate change-page
                        }
                        break;
                    case "transientTransitionInProgress":
                        if (newValue == false) {  // transition just finished
                            if (arena.changePageAfterTransition) {
                                handlePageChange(arena, arena.newPage, arena.oldPage);
                                arena.changePageAfterTransition = false;
                                arena.newPage = null;
                                arena.oldPage = null;
                            }
                        }
                        break;

                    case "pageRate":
                        if (arena.pageRate!=0 && Application.running)
                            startClock(arena);
                        break;
                    case "width":
                    case "height":
                        recalcAllPages(arena);
                        break;
                    case "currentPage":
                        if(newValue != oldValue){
                            ArenaPageController.instance.requestRecalcLayout(page);
                        }
                        break;
                }
			}

            if (model.value is ArenaPage) {  // page itself is changing
                var page:ArenaPage = model.value as ArenaPage;
                arena = getArena(model.parent);
                if (!arena.bindingsDisabled && !page.loading){
                    if (page == arena.currentArenaPage) {  // mirror page values to arena, only if value is from current page
                        mirrorValueFromPage(arena, property, newValue);
                        switch (property) {
                            case "contentWidth":
                                arena.contentWidth = page.contentWidth;
                                break;
                            case "contentHeight":
                                arena.contentHeight = page.contentHeight;
                                break;
                        }
                    }
                }
            }
		}

        override protected function afterLoad(pager:Pager):void
        {
            super.afterLoad(pager);
            var arena:Arena = pager as Arena;
            var page:ArenaPage = arena.currentArenaPage;
            arena.contentWidth = page.contentWidth;
            arena.contentHeight = page.contentHeight;
        }

        /**
         * The new page has been added already: compute the Arena pageNumber value.
         * @param pager
         * @param additionPageNumber = one-based number of new page
         * @return = pageNumber value for the Pager.
         */
        override protected function computePagerNumberAfterAddition(oldPageNumber:int, additionPageNumber:int):int
        {
            // for Arena show the new page.  (different from EventFlow)
            if (additionPageNumber == oldPageNumber+1) {
                return oldPageNumber + 1;
            }
            return oldPageNumber;
        }

        private function copyArenaPropsToPages(arena:Arena):void
        {
            for each (var page:ArenaPage in arena.pages) {
                mirrorAllToPage(arena, page);
            }
        }

        private function setupInspectorProps(arena:Arena):void
        {
            for each (var page:ArenaPage in arena.pages) {
                page.setupInspectorRibbons();
            }
        }

        protected function recalcAllPages(arena:Arena):void
        {
            if (!arena.loading) {
                for each (var page:ArenaPage in arena.pages) {
                    page.width = arena.width;
                    page.height = arena.height;
                    ArenaPageController.instance.requestRecalcLayout(page);
                }
            }
        }

		private function handlePageChange(arena:Arena, newPage:IPage, oldPage:IPage):void
        {
            updatePagesActive(arena);
            AuthorController.instance.deselectDescendantsOf(arena);

            var page:ArenaPage = newPage as ArenaPage;
            mirrorAllFromPage(page, arena);
            arena.contentWidth = page.contentWidth;
            arena.contentHeight = page.contentHeight;

			// keep track of time, for animated page flipping
			arena.startTime = _taconiteTimer.milliseconds;
        }

		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			super.handleTrigger(object,modifier);
			
			var model:TaconiteModel = object.model;
			var arena:Arena = object as Arena;
            if (arena.pages.length == 0) {  // probably in process of deleting arena
                return;
            }

			// Handle dynamically created page ribbons
			var key:String = modifier.key;
//			trace("Arena:handleTrigger",object, "key="+key);
			if (key && key.search("goToPage")!=-1) {
				arena.pageNumber = new Number(modifier.label.substr(9, modifier.label.length));
			}
			else {
				switch (modifier.key)
				{
					case "goCurrentPage":
						arena.pageNumber = parseCurrentPageNumber(model, Modifiers.instance.fetch("goCurrentPage"));
						break;
					case "start":
						this.startClock(arena);
						break;
					case "pause":
						this.stopClock(arena);
						break;
					case "randomizeZIndex":
						ArenaPageController.instance.handleRandomizeZIndex(arena.currentArenaPage);
						break;
				}
			}
		}
		

		public function parseCurrentPageNumber(model:TaconiteModel, modifier:IModifier):Number
		{
			var value:Number = 1; // default
			
			for each (var rb:WireAnchor in AbstractObject(model.value).inletRibbons) {
				if (rb.modifierDescription == modifier) {
					value = new Number(rb.label.substr(17, rb.label.length-1));
				}	
			}
			
			return value;
		}
		


		/**
		 * Request the view to update the current page, even tho the pageNumber didn't change. 
		 * @param arena
		 * 
		 */
		override public function requestUpdatePageNumber(pager:Pager):void
		{
			pager.dispatchEvent(PropertyChangeEvent.createUpdateEvent(pager,  "pageNumber", -1, pager.pageNumber));
		}

		
		public function startClock(arena:Arena):void
		{
			if (!arena.ticking) {
				arena.tickListener = tick(arena);
				_taconiteTimer.registerListener(arena.tickListener, true);
				arena.startTime = _taconiteTimer.milliseconds;  // time on current page = time in milliseconds since 1970
				arena.ticking = true;
				arena.lastReportTime = arena.startTime;
				arena.clockCycles = 0;
			}
		}
		
		public function stopClock(arena:Arena):void
		{
			if (arena.ticking) {
				if (arena.tickListener != null)
					_taconiteTimer.unregisterListener(arena.tickListener);
				arena.ticking = false;
			}
		}
		
		private function tick(arena:Arena):Function
		{
			return function(event:Event):void
			{
				var time:Number = _taconiteTimer.milliseconds;
				if (arena.active == false) {
					arena.startTime = time;
					return;  // do nothing while this arena is on a hidden page
				}
                if (arena.transientPauseWhileDragging) {
                    if (ObjectDragMediator.draggingNow) {
                        arena.startTime = time;
                        return;  // pause after dropping child into arena, even if not moved
                    } else {
                        arena.transientPauseWhileDragging = false;
                        // continue, since mouse is up now
                    }
                }

				var elapsed:Number = (time - arena.startTime) / 1000;  // seconds spent on this page
				
				// set n = number of pages to advance from current page
				// (if negative: num pages to back up)
				var n:int = arena.pageRate * elapsed;
				if (n == 0)
					return;
				if (arena.noSkip) {
					if (n > 1)			n = 1;
					else if (n < -1)	n = -1;
				}
				//				trace("arenaTick",arena,"elapsed="+elapsed, "n="+n);
				
				// advance "n" pages ... but wrap when reach either end
				var page:int = (arena.pageNumber-1 + n) % arena.pageCount;  // zero-based number
				page = int(page);  // kludge:  without this line, page had values such as 3.53
				if (page < 0)  // deal with negative rate
					page += arena.pageCount;
				++page;  // one-based page number
				
				// are we really changing pages?
				if (page != arena.pageNumber && page <= arena.pageCount) {  // (never add a page)
					//					trace("arenaTick setting page="+page);
					arena.pageNumber = page;  // change the page  (sets arena.startTime)
					reportActualRate(arena,time);
				}
			}
		}
		
		private function reportActualRate( arena:Arena, time:Number ):void
		{
			// reporting:  ActualRate property
			++arena.clockCycles;
			var sinceReport:Number = (time - arena.lastReportTime) / 1000;  // seconds since last report
			if (sinceReport >= REPORT_PERIOD) {
				arena.actualRate = arena.clockCycles / sinceReport;
				arena.lastReportTime = time;  // reset for next reporting period
				arena.clockCycles = 0;
			}
		}
	}
}
