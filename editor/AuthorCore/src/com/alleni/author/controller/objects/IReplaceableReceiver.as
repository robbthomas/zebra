package com.alleni.author.controller.objects
{
	import com.alleni.author.model.ui.IReplaceable;
	
	public interface IReplaceableReceiver
	{
		function acceptsReplaceableDrop(newReplaceable:IReplaceable):Boolean;
		function get replaceable():IReplaceable;
		function set replaceable(value:IReplaceable):void;
	}
}
