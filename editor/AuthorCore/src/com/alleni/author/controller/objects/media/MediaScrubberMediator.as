package com.alleni.author.controller.objects.media
{
	import com.alleni.author.model.objects.MediaObject;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.ui.controls.MediaScrubber;
	import com.alleni.taconite.event.ModelUpdateEvent;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;

	public class MediaScrubberMediator
	{
		private var _view:ObjectView;
		private var _scrubber:MediaScrubber;
		
		public function MediaScrubberMediator()
		{
		}
		
		public function handleScrubber(view:ObjectView, withAnimation:Boolean=true, expandable:Boolean=true, container:DisplayObjectContainer=null):MediaScrubberMediator
		{
			_view = view;
			if (!container)
				container = _view;
			
			_scrubber = new MediaScrubber(expandable);
			_scrubber.alpha = 0.8;
			container.addChild(_scrubber);
			
			addListeners();
			return this;
		}
		
		private function addListeners():void
		{
			_view.model.addEventListener(ModelUpdateEvent.MODEL_UPDATE, handleModelUpdate);
			_scrubber.addEventListener(MouseEvent.ROLL_OVER, handleRollOver);
			_scrubber.addEventListener(MouseEvent.ROLL_OUT, handleRollOut);
		}
		
		private function removeListeners():void
		{
			_view.model.removeEventListener(ModelUpdateEvent.MODEL_UPDATE, handleModelUpdate);
			_scrubber.removeEventListener(MouseEvent.ROLL_OVER, handleRollOver);
			_scrubber.removeEventListener(MouseEvent.ROLL_OUT, handleRollOut);
			_scrubber.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		}
		
		private function handleModelUpdate(event:ModelUpdateEvent):void
		{
			switch (event.property) {
				case "fractionLoaded":
					_scrubber.fractionLoaded = Number(event.newValue);
					break;
				case "percentElapsed":
					_scrubber.playProgress = Number(event.newValue)/100;
					break;
				default:
					break;
			}
		}
		
		public function get scrubber():DisplayObject
		{
			return _scrubber;
		}
		
		public function warnBeforeDelete():void
		{
			removeListeners();
		}
		
		private function handleRollOver(event:MouseEvent):void
		{
			_scrubber.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		}
		
		private function handleRollOut(event:MouseEvent):void
		{
			_scrubber.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		}
		
		private function handleMouseDown(event:MouseEvent):void
		{
			var fraction:Number = event.localX/_scrubber.width*_scrubber.scaleX;
			
			if (_view.model) {
				var mediaObject:MediaObject = _view.model.value as MediaObject;
				if (mediaObject && mediaObject.duration > 0)
					mediaObject.percentElapsed = fraction*100;
			}
		}
	}
}