package com.alleni.author.controller.objects
{
	import caurina.transitions.Tweener;

	import com.alleni.author.controller.TweenController;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.objects.CalcTween;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;


	public class CalcTweenController extends CalcController
	{
		public static var instance:CalcTweenController = new CalcTweenController();
		
		
		protected static function getCalcTween(model:TaconiteModel):CalcTween
		{
			return model.value as CalcTween;
		}

		
		// Tweens are also removed by ObjectController.stopTweenMotion, when the target object is dragged during tween
		
		
		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
		{	
			var index:int = -1;
			
			super.handleModelUpdate(model,property,newValue,oldValue,parent);
			
			var calc:CalcTween = getCalcTween(model);
			if (!calc.loading) {
				
				switch (property) {
					case "calcTweenValue":
						// previously used this to adjust a motion tween
						break;
				}
			}
		}

		
		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			var calc:CalcTween = object as CalcTween;
			
			var start:* = TweenController.getStartValue(calc, "calcTweenValue", -1);  // get start value from wired object
			var end:* = calc.controlValue;
			if (start == null) {
				if (end is GraphicFill)  // TODO_FILL ... fill values should never be null ... so start shouldn't be null
					start = new GraphicFill();
				else
					start = end;
			}
			
			var twc:TweenController = new TweenController(start, end, calc.type);
			calc.frac = 0;
			
			Tweener.addTween(calc, {frac:1.0, time:calc.tweenTime, transition:calc.tweenType, onUpdate:updateTween, onUpdateParams:[twc,calc]});
		}
		
		private function updateTween(twc:TweenController, calc:CalcTween):void
		{
			calc.calcTweenValue = twc.computeTween(calc.frac);
			trace("CalcTweenController: updateTween:",calc.calcTweenValue);
		}
	}
}
