package com.alleni.author.controller.objects.media
{
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.service.LogService;
	
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.system.Security;

	public class YouTubeVideoMediator extends VideoMediator implements IMediaMediator
	{
		private var _player:Object;
		private var _loader:Loader;
		private var _startPosition:Number = 0;

		override public function handleMediaEvents(object:ITaconiteObject):IMediaMediator
		{
			try {
				Security.allowDomain("www.youtube.com");
				Security.allowDomain("youtu.be");
			} catch (e:Error) {
				// catch just in case this triggers sandbox errors. This is nonessential.
				LogService.info("YouTubeMediator tried to allowDomain but caught an error: " + e);
			}
			super.handleMediaEvents(object);
			// we use alpha to reflect that the necessary metadata are loaded.
			_videoObject.alpha = 100; // possibly temporary, in case we can detect metadata
			return this;
		}
		
		private function addPlayerListeners():void
		{
			if (!_loader || !_loader.content) return;
			_loader.content.addEventListener("onReady", onPlayerReady, false, 0, true);
			_loader.content.addEventListener("onError", onPlayerError, false, 0, true);
			_loader.content.addEventListener("onStateChange", onPlayerStateChange, false, 0, true);
			_loader.content.addEventListener("onPlaybackQualityChange", onVideoPlaybackQualityChange, false, 0, true);
		}
		
		private function clearPlayerListeners():void
		{
			if (!_loader || !_loader.content) return;
			_loader.content.removeEventListener("onReady", onPlayerReady);
			_loader.content.removeEventListener("onError", onPlayerError);
			_loader.content.removeEventListener("onStateChange", onPlayerStateChange);
			_loader.content.removeEventListener("onPlaybackQualityChange", onVideoPlaybackQualityChange);
		}
		
		private function load():void
		{
            if (_loader == null) {
				unsetPlayerStatus(ALL_PLAYER_STATUS_FLAGS);

				var url:String = "http://www.youtube.com/apiplayer?version=3";
				_loader = new Loader();
				_loader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit, false, 0, true);
				_loader.load(new URLRequest(url));
			}
		}
		
		private function onLoaderInit(event:Event):void
		{
			addPlayerListeners();
			_loader.contentLoaderInfo.removeEventListener(Event.INIT, onLoaderInit);
		}
		
		override public function start(position:Number=0, restart:Boolean=false):void
		{
			if (!playerStatusIs(PLAYER_LOADED)) {
				_startPosition = position;
				load();
			} else {
				if (!playerStatusIs(PLAYER_PLAYING) 
						|| playerStatusIs(PLAYER_PAUSED)
						|| playerStatusIs(PLAYER_PLAYING) && restart) {
					play(position);
					unsetPlayerStatus(PLAYER_PAUSED);
					setPlayerStatus(PLAYER_STARTED);
				}
			}
		}
		
		override protected function play(position:Number=0):void
		{
			addListeners();
			_player.seekTo(position, true);
			_player.playVideo();
			setPlayerStatus(PLAYER_PLAYING);
		}
		
		override public function pause():void
		{
			if (playerStatusIs(PLAYER_LOADED)) {
				_player.pauseVideo();
				setPlayerStatus(PLAYER_PAUSED);
                unsetPlayerStatus(PLAYER_PLAYING);
				clearListeners();
			}
		}
		
		override public function stop():void
		{
			super.stop();
			if (playerStatusIs(PLAYER_LOADED)) {
				try {
					_player.stopVideo();
				} catch(e:Error) {
					// do nothing. since the youtube player is their own code and we have no control over it, we can't control what happens when we stop it.
					// bugs in their code have triggered unhandled exceptions, here.
				}
			}
		}
		
		override protected function setTime(time:Number):void
		{
			super.setTime(time);
			if (playerStatusIs(PLAYER_LOADED)) {
				_player.seekTo(time);
			}
			if (!playing)
				pause();
		}
		
		override protected function setVolume():void
		{
			if (Application.instance.audioEnabled)
				_player.setVolume(_videoObject.volume);
			else
				_player.setVolume(0);
		}
		
		override protected function setOffset(offset:Number):void
		{
			if (safeToUseOffset(offset, _player.getCurrentTime())) {
				_player.seekTo(offset/100 * _duration);
			}
		}
		
		private function get videoIDfromURI():String
		{
			var tokens:Array = AssetDescription.YOUTUBE_ID_REGEX.exec(_videoObject.uri);
			if (tokens != null && tokens.length > 1) {
				return tokens[tokens.length-1];
			} else {
				LogService.error("YouTube URL '"+_videoObject.uri+"' does not specify a video ID. Sorry, no support for playlists yet.");
			}
			return "";
		}
		
		private function onPlayerReady(event:Event):void
		{
			_player = _loader.content;
			if (videoIDfromURI.length == 0 || _player == null) return;
			watchLoading = true;
			_player.loadVideoById(videoIDfromURI, 0);  // 0 is startseconds
			
			_videoObject.contentViewer.visible = false;
			invalidateViewWith(_player as DisplayObject);
			_videoObject.contentViewer.addEventListener(Event.ENTER_FRAME, confirmProperSize);
			
			_player.setVolume(_videoObject.volume);
			
			_duration = _player.getDuration();
			_mediaObject.duration = Math.floor(_duration*100)/100;
			setPlayerStatus(PLAYER_LOADED);
			this.start(_startPosition);
		}
		
		override protected function handleWatchLoading(event:Event):void
		{
			updateFractionLoaded(_player.getVideoBytesLoaded(), _player.getVideoBytesTotal());
		}
		
		private function confirmProperSize(event:Event):void
		{
			_videoObject.contentViewer.removeEventListener(Event.ENTER_FRAME, confirmProperSize);
			_videoObject.contentViewer.visible = true;
			if (_videoObject.maintainAspect) { // adjust width to aspect of content, with height priority
				_videoObject.width = _videoObject.height * _player.width / _player.height;
			}
			_videoObject.sourceDimensions = new Point(_player.width, _player.height);
		}
		
		private function onPlayerError(event:Event):void
		{
            var data:* = (event.hasOwnProperty("data")) ? Object(event).data : null;
			LogService.error("player error: event="+event + " data="+data);
		}
		
		private function onPlayerStateChange(event:Event):void
		{
			LogService.debug("player state:" + Object(event).data);
			if (Object(event).data == 1) {
				if (_startPosition > 0) {
					_player.seekTo(_startPosition, true);
					_startPosition = 0;
				}
			}
		}
		
		private function onVideoPlaybackQualityChange(event:Event):void
		{
			LogService.debug("video quality:" + Object(event).data);
		}
		
		override protected function updateTimingTick(event:Event):void
		{
			updateTiming(_player.getCurrentTime(), _player.getDuration());
		}
		
		override public function unload():void
		{
			super.unload();
			
			if (_loader)
				clearPlayerListeners();
			if (_player) {
				try {
					_player.destroy();
				} catch(e:Error) {
					// do nothing. since the youtube player is their own code and we have no control over it, we can't control what happens when we destroy it.
					// in some cases, this triggers exceptions when being killed, particularly in com.google.ads.templates.overlay::OverlayFlashTemplate/destroy() .
					// bugs in their code have triggered unhandled exceptions, here.
				}
			}
			_player = null;
			_loader = null;
		}
	}
}