package com.alleni.author.controller.objects.media
{
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractExternalObject;
	import com.alleni.author.model.objects.VideoObject;
	import com.alleni.author.model.ui.Asset;
	
	public class VideoController extends MediaController
	{
		public static var instance:VideoController = new VideoController();

		override protected function validateMediator(object:AbstractObject):void
		{
			var videoObject:VideoObject = object as VideoObject;
			
			if (videoObject.uri != AssetDescription.HOSTED_MEDIA_IDENTIFIER
					&& (videoObject.asset && videoObject.asset.assetID != null)) {
//				AssetController.instance.confirmDetachment(videoObject.asset);    // previously this decremented asset.count
			}
			
			if (AssetDescription.YOUTUBE_REGEX.exec(videoObject.uri)) {
				replaceMediatorForWith(object, new YouTubeVideoMediator().handleMediaEvents(videoObject));
				return;
					
			/*} else if (AssetDescription.RTMP_REGEX.exec(videoObject.uri)) {
				replaceMediatorForWith(object, new StreamVideoMediator().handleMediaEvents(videoObject));
				return;*/ // if we support streaming from non-Zebra servers, we may not
			
			} else if (AssetDescription.FLASH_VIDEO_REGEX.exec(videoObject.uri)) {
				replaceMediatorForWith(object, new StreamVideoMediator().handleMediaEvents(videoObject));
				return;
			
			}/* else
				replaceMediatorForWith(object, new StreamVideoMediator().handleMediaEvents(videoObject));*/ // default streamer
			
			if (_mediators[object])
				IMediaMediator(_mediators[object]).handleMediaEvents(videoObject);
		}
		
		override protected function handleAssetUpdatedFor(object:AbstractExternalObject, oldAsset:Asset):void
		{
			super.handleAssetUpdatedFor(object, oldAsset);
			var videoObject:VideoObject = object as VideoObject;
			
			if (videoObject.asset && videoObject.asset.assetID)
				videoObject.uri = AssetDescription.HOSTED_MEDIA_IDENTIFIER;
		}
		
		override protected function setTitleWithURI(object:AbstractObject, uri:String):void
		{
			if (AssetDescription.YOUTUBE_REGEX.exec(uri)) {
				// not ideal, but it will work fine for now.	
				var tokens:Array = AssetDescription.YOUTUBE_ID_REGEX.exec(uri);
				if (tokens && tokens.length > 1)
					object.title = AssetDescription.YOUTUBE_TITLE + " " + tokens[tokens.length-1];
			} else
				super.setTitleWithURI(object, uri);
		}
	}
}