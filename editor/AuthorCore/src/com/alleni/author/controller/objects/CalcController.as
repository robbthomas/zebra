package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.model.objects.Calc;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;

	public class CalcController extends ObjectController
	{
		public static var instance:CalcController = new CalcController();
		
		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier) : void
		{
			var calc:Calc = object as Calc;
			calc.calcValue = calc.controlValue
		}
	}
}