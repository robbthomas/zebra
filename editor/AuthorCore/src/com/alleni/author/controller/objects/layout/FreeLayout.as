package com.alleni.author.controller.objects.layout
{
    import com.alleni.author.model.AbstractObject;
    import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.objects.PathObject;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.objects.ArenaPageView;
import com.alleni.taconite.view.ViewContext;

import flash.geom.Rectangle;

    import mx.collections.IList;

    public class FreeLayout extends Layout
	{
		public static var instance:FreeLayout = new FreeLayout();
		
		
		override protected function calculatePlacements(pageView:ArenaPageView, animate:Boolean=true):void
		{
            var page:ArenaPage = pageView.object as ArenaPage;
            var context:ViewContext = pageView.context;
            var objects:IList = page.objects;
			var aligningAll:Boolean = page.alignObjsHorz!="" || page.alignObjsVert!="";
			var contentWidth:Number = 0;
			var contentHeight:Number = 0;
			var obj:AbstractObject;
			for each (obj in objects) {
				if (!(obj.editing && obj is PathObject) && !obj.dragging && (aligningAll || obj.alignAreaHorz!="" || obj.alignAreaVert!="")) {
                    var bounds:Rectangle = getObjBounds(page, obj, context);
                    var view:ObjectView = getView(obj, context);
					alignObjHorz(page, obj, view, bounds);
					alignObjVert(page, obj, view, bounds);
				}
			}
			for each (obj in objects)
			{
                if(!(obj.editing && !obj.dragging  && obj is PathObject)){
				    bounds = getObjBounds(page,obj, context);
                    if (bounds.x > Layout.MAX_COORD || bounds.y > Layout.MAX_COORD || bounds.width > Layout.MAX_COORD || bounds.height > Layout.MAX_COORD) {
                        return;  // don't update contentSize while a child view has invalid coordinates
                    }
				    if((bounds.x +bounds.width+page.marginRight) > contentWidth) contentWidth = (bounds.x +bounds.width+page.marginRight+page.hGap);
				    if((bounds.y +bounds.height+page.marginBottom) > contentHeight) contentHeight = (bounds.y +bounds.height+page.marginBottom+page.vGap);
                }
            }
			// transfer content dimensions to model
			page.contentWidth = (contentWidth)?contentWidth:page.width;    // note early return above
			page.contentHeight = (contentHeight)?contentHeight:page.height;
		}
		
		/**
		 * Align object horizontally, in the space within the margins of the page.
		 * @param arena
		 * @param obj
		 *
		 */
		public function alignObjHorz(page:ArenaPage, obj:AbstractObject, view:ObjectView, bounds:Rectangle):void
		{
			var horz:String = obj.alignAreaHorz;
			if (horz == "")
				horz = page.alignObjsHorz;

			switch (horz) {
				case Layout.ALIGN_CENTER:	obj.x = alignCenter(page) - (bounds.x + bounds.width/2 - view.x);		break;
				case Layout.ALIGN_RIGHT:	obj.x = alignRight(page) - (bounds.right - view.x);		            break;
				case Layout.ALIGN_LEFT: 	obj.x = alignLeft(page) - (bounds.left - view.x);		                break;
			}
		}
		
		
		/**
		 * Align object vertically, in the space within the margins of the page.
		 * @param arena
		 * @param obj
		 *
		 */
		public function alignObjVert(page:ArenaPage, obj:AbstractObject, view:ObjectView, bounds:Rectangle):void
		{
			var vert:String = obj.alignAreaVert;
			if (vert == "")
				vert = page.alignObjsVert;
			
			switch (vert) {
				case Layout.ALIGN_MIDDLE:	obj.y = alignMiddle(page) - (bounds.y + bounds.height/2 - view.y);		break;
				case Layout.ALIGN_TOP:		obj.y = alignTop(page) - (bounds.top - view.y);		                break;
				case Layout.ALIGN_BOTTOM: 	obj.y = alignBottom(page) - (bounds.bottom - view.y);		            break;
			}
		}
		
		
		
		
		private function alignLeft(page:ArenaPage):Number
		{
			return page.marginAll + page.marginLeft;
		}
		
		private function alignRight(page:ArenaPage):Number
		{
			return page.width - page.marginAll - page.marginLeft;
		}
		
		private function alignCenter(page:ArenaPage):Number
		{
			return (page.width + page.marginLeft - page.marginRight)/2;
		}
		
		private function alignTop(page:ArenaPage):Number
		{
			return page.marginAll + page.marginTop;
		}
		
		private function alignBottom(page:ArenaPage):Number
		{
			return page.height - page.marginAll - page.marginBottom
		}
		
		private function alignMiddle(page:ArenaPage):Number
		{
			return (page.height + page.marginTop - page.marginBottom)/2;
		}
		
		
	}
}