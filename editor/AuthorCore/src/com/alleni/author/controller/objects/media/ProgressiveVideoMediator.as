package com.alleni.author.controller.objects.media
{
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.model.BorealToken;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.service.BorealTokenOperation;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.service.IOperation;
	import com.alleni.taconite.service.LogService;
	
	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.NetStatusEvent;
	import flash.geom.Point;
	import flash.media.SoundTransform;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;

	public class ProgressiveVideoMediator extends VideoMediator implements IMediaMediator
	{
		private var _video:Video;
		private var _netStream:NetStream;
		private var _netStreamURI:String;
		private var _soundTransform:SoundTransform;

		override public function handleMediaEvents(object:ITaconiteObject):IMediaMediator
		{
			super.handleMediaEvents(object);

			unsetPlayerStatus(METADATA_LOADED);
			if (_videoObject.asset && _videoObject.asset.assetID || _videoObject.uri) {
				start();
				unsetPlayerStatus(PLAYER_STARTED); // this is a bootstrapped start just to get the metadata, not a real start/autostart
			}
			return this;
		}
		
		override protected function initializeState():void
		{
			super.initializeState();
			
			if (_netStream)
				_netStream.close();
		}
		
		override protected function addListeners():void
		{
			super.addListeners();
			_netStream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, handleAsynchError, false, 0, true);
			_netStream.addEventListener(IOErrorEvent.IO_ERROR, handleIOError, false, 0, true);
			_netStream.addEventListener(NetStatusEvent.NET_STATUS, handleNetStatus, false, 0, true);
		}
		
		override protected function clearListeners():void
		{
			super.clearListeners();
			_netStream.removeEventListener(AsyncErrorEvent.ASYNC_ERROR, handleAsynchError);
			_netStream.removeEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			_netStream.removeEventListener(NetStatusEvent.NET_STATUS, handleNetStatus);
		}
		
		private function load():void
		{
			var nc:NetConnection = new NetConnection();
			nc.addEventListener(NetStatusEvent.NET_STATUS, handleNetStatus, false, 0, true);
			nc.connect(null);
			_netStream = new NetStream(nc);
			var dataClient:Object = new Object();
			dataClient.onMetaData = onMetaData;
			dataClient.onXMPData = onXMPData;
			_netStream.client = dataClient;
			_soundTransform = new SoundTransform(1);
			
			_video = new Video(_videoObject.width, _videoObject.height);
			_video.attachNetStream(_netStream);

			setPlayerStatus(PLAYER_LOADED);
		}
		
		override public function start(position:Number = 0, restart:Boolean=false):void
		{
			if (!playerStatusIs(METADATA_LOADED) && playerStatusIs(PLAYER_PLAYING)) {
				setPlayerStatus(PLAYER_STARTED);
				return;
			}
			if (!playerStatusIs(PLAYER_PLAYING) || playerStatusIs(PLAYER_PAUSED)) {
				unsetPlayerStatus(PLAYER_PAUSED);
				setPlayerStatus(PLAYER_STARTED);
				
				if (_videoObject.uri == AssetDescription.HOSTED_MEDIA_IDENTIFIER
						&& _videoObject.asset && Utilities.UUID_REGEX.test(_videoObject.asset.assetID)
						&& !playerStatusIs(PLAYER_LOADED)
						|| _accessToken && !_accessToken.valid) { // play from our back-end
					var tokenOperation:BorealTokenOperation = new BorealTokenOperation();
					tokenOperation.displayName = "Fetch video playback token";
					tokenOperation.addEventListener(Event.COMPLETE, function(e:Event):void {
						tokenOperation.removeEventListener(Event.COMPLETE, arguments.callee);
						_accessToken = (e.target as IOperation).result as BorealToken;
						_netStreamURI = TaconiteFactory.getEnvironmentImplementation().apiURI + "auth/" + _accessToken.content + "/asset/" + _videoObject.asset.assetID + "/data";
						play(position);
					});
					tokenOperation.execute();
					return;
					
					//the code in the following else if block is temporary and only meant to accomodate a specific project
				}else {
					if (!_videoObject.asset || !Utilities.UUID_REGEX.test(_videoObject.asset.assetID)) {
						LogService.error("Request to play video '" + _videoObject + "' cannot be fulfilled: need non-null asset ID");
						return;
					}
				}
				play(position);
			} else {
				if (restart) {
					_netStream.seek(0);
					play(position);
				}
			}
		}
		
		override protected function play(position:Number=0):void
		{
			var uri:String = _netStreamURI?_netStreamURI:_videoObject.uri;
			LogService.debug("Playing video from URL: " + uri);
			
			if (!playerStatusIs(PLAYER_LOADED))
				load();

			if (position != _netStream.time)
				_netStream.seek(position);

			_netStream.play(uri);
			addListeners();

			setVolume();
			_cuePointsHandler.clear();
			addCuePoints();
			_videoObject.buffering = true;
			watchLoading = true;
			setPlayerStatus(PLAYER_PLAYING);
		}
		
		override protected function handleWatchLoading(event:Event):void
		{
			updateFractionLoaded(_netStream.bytesLoaded, _netStream.bytesTotal);
		}
		
		override public function pause():void
		{	
			if (playerStatusIs(METADATA_LOADED)) {
				_netStream.pause();
				clearListeners();
			}
			setPlayerStatus(PLAYER_PAUSED);
            unsetPlayerStatus(PLAYER_PLAYING);
		}
		
		override public function resume(fromActivation:Boolean=false):Boolean
		{	
			if (playerStatusIs(PLAYER_PAUSED)) {
				if (playerStatusIs(PLAYER_LOADED)) {
					addListeners();
					_netStream.resume();
				} else
					start();
				unsetPlayerStatus(PLAYER_PAUSED);
				return true;
			}
			return false;
		}
		
		override protected function setTime(time:Number):void
		{
			_netStream.seek(time);
		}
		
		override protected function setVolume():void
		{
			if (Application.instance.audioEnabled)
				_soundTransform.volume = _videoObject.volume/100.0;
			else
				_soundTransform.volume = 0;
				
			_netStream.soundTransform = _soundTransform;
		}
		
		override protected function setOffset(offset:Number):void
		{
			if (safeToUseOffset(offset, _netStream.time))
				_netStream.seek(offset/100 * _duration);
		}
		
		override protected function onMetaData(data:Object):void
		{
			super.onMetaData(data);
			// data.framerate
			_duration = data.duration;
			_mediaObject.duration = Math.floor(_duration*100)/100;
			if (!playerStatusIs(METADATA_LOADED)) {
				setPlayerStatus(METADATA_LOADED);
				_video.width = data.width;
				_video.height = data.height;
				invalidateViewWith(_video);
				_videoObject.sourceDimensions = new Point(data.width, data.height);
				
				if (!playerStatusIs(PLAYER_STARTED) 
						|| playerStatusIs(PLAYER_STARTED) && playerStatusIs(PLAYER_PAUSED)) {
					pause();
					if (!_videoObject.preload)
						initializeState();
					unsetPlayerStatus(PLAYER_PAUSED);
					unsetPlayerStatus(PLAYER_STARTED);
					unsetPlayerStatus(PLAYER_PLAYING);
				}
			}
		}
		
		override protected function updateTimingTick(event:Event):void
		{
			updateTiming(_netStream.time, _duration);
		}
		
		override public function unload():void
		{
			super.unload();
			
			if (_netStream)
				_netStream.pause();
			_netStream = null;
			_soundTransform = null;
			_video = null;
		}
	}
}