package com.alleni.author.controller.objects
{
    import com.alleni.author.definition.DragInfo;
    import com.alleni.author.definition.action.ActionTree;
    import com.alleni.taconite.model.ITaconiteObject;

	public interface IEditingMediator
	{
        function getSelection(getGlobal:Boolean, strict:Boolean=true, omitLocked:Boolean=true):Vector.<DragInfo>;
		function cancelEditing():Boolean;
		function deleteEditing():Boolean;
		function selectAll(includeLocked:Boolean):void;
		function makeObject():Boolean;
		function switchedObjects(oldObj:ITaconiteObject, newObj:ITaconiteObject):void;
		
		function get editingObject():ITaconiteObject;
		function get actionTree():ActionTree;
		function get hasActiveState():Boolean;
        function get MENULABEL_TYPE():String;
		function activeMediatorChanged(editor:IEditingMediator):void;
	}
}
