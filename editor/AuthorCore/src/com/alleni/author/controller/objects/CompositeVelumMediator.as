package com.alleni.author.controller.objects
{
import com.alleni.author.Navigation.SmartObjectEditingMediator;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.MarqueeSelectionMediator;
import com.alleni.author.controller.ObjectDragMediator;
import com.alleni.author.controller.ObjectFactory;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.event.CompositeEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.Oval;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.objects.ArenaControlBar;
import com.alleni.author.view.objects.ArenaView;
import com.alleni.author.view.objects.CompositeTrailView;
import com.alleni.author.view.objects.CompositeVelumView;
import com.alleni.author.view.objects.CompositeView;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;

public class CompositeVelumMediator extends SmartObjectEditingMediator implements IEditingMediator, IUndoMediator, IClipboardMediator
	{
        public const _MENULABEL_TYPE:String = "Gadget";

		private var _view:CompositeVelumView;
		private var _trail:CompositeTrailView;
		private var _hilitedObj:AbstractObject;

		private var _compositeView:CompositeView;
		private var _composite:Composite;

		private var _compositeMediator:CompositeMediator;
		
        public static const VELUM_TITLE:String = "$velum$";
        public static const PLACEHOLDER_TITLE:String = "$placeholder$";  // used for two different placeholders


		public function CompositeVelumMediator(context:ViewContext, velum:CompositeVelumView, compositeView:CompositeView, compositeMediator:CompositeMediator)
		{
            super(context, velum, compositeView, compositeMediator);

			_compositeMediator = compositeMediator;
			_view = velum;
			_trail = velum.trail;
			
			_compositeView = compositeView;
			_composite = _compositeView.model.value as Composite;

            startEditing();     // will cause activate()
		}
		
		override public function clearViewEvents() : void
		{
            restoreToLayer();  // if an object was temp brought to front ... restore it now
            super.clearViewEvents();
		}

		override public function makeObject():Boolean
		{
			if (_composite.editing) {
				_compositeMediator.closeEditing();
			}
			return true;
		}

        override public function get MENULABEL_TYPE():String{
            return _MENULABEL_TYPE;
        }

        override public function switchedObjects(oldObj:ITaconiteObject, newObj:ITaconiteObject):void
        {
            trace("switched old="+oldObj, "new="+newObj);
            if (_tempObjFront == oldObj)
                _tempObjFront = newObj as AbstractObject;
            updateTrail();
        }

		override protected function activate():void
		{
            trace("CompositeVelum: activate editing="+_composite.editing);
            super.activate();
			_trail.doubleClickEnabled = true;
			_trail.addEventListener(MouseEvent.MOUSE_DOWN, trailMouseDownListener);
			_trail.addEventListener(MouseEvent.DOUBLE_CLICK, trailDoubleClickListener);
			_context.stage.addEventListener(MouseEvent.MOUSE_MOVE, trailMouseMoveListener, true);
			_composite.addEventListener(CompositeEvent.REMOVE_HILITE, trailRemoveHiliteListener);
			updateTrail();
		}
		
		override protected function deactivate():void
		{
            super.deactivate();
			_trail.removeEventListener(MouseEvent.MOUSE_DOWN, trailMouseDownListener);
			_trail.removeEventListener(MouseEvent.DOUBLE_CLICK, trailDoubleClickListener);
			_context.stage.removeEventListener(MouseEvent.MOUSE_MOVE, trailMouseMoveListener, true);
			_composite.removeEventListener(CompositeEvent.REMOVE_HILITE, trailRemoveHiliteListener);
			removeHilite();
		}

		private function trailMouseDownListener(evt:MouseEvent):void
		{
			// check for click on the breadcrumb trail
			var trailBounds:Rectangle = trailStageBounds();
			if (trailBounds.contains(evt.stageX,evt.stageY)) {
				var local:Point = _trail.globalToLocal(new Point(evt.stageX,evt.stageY));
				selectObjectFromTrail(local.x);
				evt.stopImmediatePropagation();
			}
		}
		
		
		private function trailMouseMoveListener(evt:MouseEvent):void
		{
			// the target may be trail or the header text ... we don't care
			var trailBounds:Rectangle = trailStageBounds();
			if (trailBounds.contains(evt.stageX,evt.stageY)) {
				var local:Point = _trail.globalToLocal(new Point(evt.stageX,evt.stageY));
				updateHeaderFromTrail(local.x);
			} else {
				childRolloverAffectsTrail();
			}
		}

		private function childRolloverAffectsTrail():void
		{
			// check for mouse rolling over a child of this gadget
			var wc:WorldContainer = WorldContainer(_context);
			if (wc.viewsUnderMouse.length > 0) {
				var view:ObjectView = wc.viewsUnderMouse[0];
				if (view.model.parent == _model && view.model.value != _hilitedObj) {
					forceHilite(view.model.value as AbstractObject);
					updateTrail();
				}
			}
		}

		private function trailDoubleClickListener(evt:MouseEvent):void
		{
			// the target may be trail or the header text ... we don't care
			var trailBounds:Rectangle = trailStageBounds();
			if (trailBounds.contains(evt.stageX,evt.stageY)) {
				var local:Point = _trail.globalToLocal(new Point(evt.stageX,evt.stageY));
				var model:TaconiteModel = _trail.modelUnderX(local.x);
				if (model && model.value is Composite && model != _trail.model) {
					var otherComposite:Composite = model.value as Composite;
					CompositeMediator.openEditing(otherComposite); 
				}
			} 
			
			evt.stopImmediatePropagation();  // prevent ObjectController of the parent composite from getting this double click on the velum
		}
		
		private function updateHeaderFromTrail(xx:Number):void
		{
			// find the object represented by the box in the trail, under xx
			var model:TaconiteModel = _trail.modelUnderX(xx);
			if (model) {
				
				// remove hilite from previous obj, then hilite this one
				forceHilite(model.value as AbstractObject);
			}
		}
		
		private function selectObjectFromTrail(xx:Number):void
		{
			// find the object represented by the box in the trail, under xx
			var model:TaconiteModel = _trail.modelUnderX(xx);
			if (model && model.parent == _trail.model) {  // only ok to select immediate children of this composite
				tempBringFront(model.value as AbstractObject);
				_authorController.selectSingleModel(model);
			}
		}
		
		private var _layerPlaceholder:Oval;
		private var _tempObjFront:AbstractObject;
		private var _wasVisible:Boolean;
		private var _origAlpha:Number;
		
		private function tempBringFront(obj:AbstractObject):void
		{
			if (_layerPlaceholder) // restore other obj first
				restoreToLayer();
			
			var container:AbstractContainer = obj.model.parent.value as AbstractContainer;  // container where it is now (possibly in arena)
			var layer:int = container.objects.getItemIndex(obj);
			var placeholder:Oval = ObjectFactory.createForName("Oval", container, layer,0,0,0,0) as Oval;
			placeholder.title = PLACEHOLDER_TITLE;
			placeholder.visible = false;
			placeholder.locked = true;  // prevent showing via Show all
			ObjectDragMediator.changeParentObject(obj,_composite);
			_layerPlaceholder = placeholder;
			_tempObjFront = obj;
			var view:ObjectView = obj.getView();
			_wasVisible = obj.visible;
			_origAlpha = obj.alpha;
			if (!view.visible) {
				obj.visible = true;
				obj.alpha = 80;
			}
			updateTrail();
		}
		
		private function restoreToLayer():void
		{
			if (_layerPlaceholder) {
				var container:AbstractContainer = _layerPlaceholder.model.parent.value as AbstractContainer;
				var layer:int = container.objects.getItemIndex(_layerPlaceholder);
				container.objects.removeItemAt(layer);  // remove placeholder
				var view:ObjectView = _tempObjFront.getView();
				if (view) {  // obj has not been deleted by user
					ObjectDragMediator.changeParentObject(_tempObjFront, container);
					container.model.setChildIndex(_tempObjFront, layer);
					_tempObjFront.visible = _wasVisible;
					_tempObjFront.alpha = _origAlpha;
				}
			}
			_tempObjFront = null;
			_layerPlaceholder = null;
			updateTrail();
		}

		private function forceHilite(obj:AbstractObject):void
		{
			// author has rolled mouse over item in breadcrumb trail:  so we hilite the object as if mouse rolled over it
			removeHilite();
			
			obj.forceHilite = true;
			_hilitedObj = obj;
			updateTrail();  // the tile in the trail will change color when rolled over
		}
		
		private function removeHilite():void
		{
			if (_hilitedObj) {
				_hilitedObj.forceHilite = false;
				_hilitedObj = null;
				updateTrail();
			}
		}
		
		private function trailStageBounds():Rectangle
		{
			// this code will work while view is zoomed to larger size
			var stagePoint:Point = _trail.localToGlobal(new Point(0,0));
			var widthHeight:Point = _trail.localToGlobal(new Point(_trail.width, _trail.height+6));
			return new Rectangle(stagePoint.x, stagePoint.y, widthHeight.x - stagePoint.x, widthHeight.y - stagePoint.y);
		}
		
		private var _dragClickStagePoint:Point;
		private var _dragOldStagePoint:Point;
		
		override protected function mouseDownVelumListener(evt:MouseEvent):void
		{
			var composite:Composite = _trail.model.value as Composite;
			var trailBounds:Rectangle = trailStageBounds();
			if (evt.stageY < trailBounds.bottom) {  // drag the whole gadget, since author clicked the velum header
				_view.stage.addEventListener(MouseEvent.MOUSE_MOVE, velumMoveListener);
				_view.stage.addEventListener(MouseEvent.MOUSE_UP, velumUpListener);
				_dragClickStagePoint = new Point(_view.stage.mouseX, _view.stage.mouseY);
				_dragOldStagePoint = composite.getView(_context).stagePosition;
				
			} else {  // drag out a selection marquee
				restoreToLayer();
                new MarqueeSelectionMediator(_context, worldView, _composite.objects).handleMouseDown(evt);
			}
			evt.stopImmediatePropagation();  
		}
		
		private function velumMoveListener(event:MouseEvent):void
		{
			if (_composite.children.length > 0) {
				var mouse:Point = new Point(_context.stage.mouseX, _context.stage.mouseY);
				var delta:Point = mouse.subtract(_dragClickStagePoint);
				var stagePoint:Point = _dragOldStagePoint.add(delta);
				var composite:Composite = _trail.model.value as Composite;
				var view:ObjectView = composite.getView(_context);
				ObjectDragMediator.setStagePosition(view,stagePoint);
			}
		}
		
		private function velumUpListener(event:MouseEvent):void
		{
			_context.stage.removeEventListener(MouseEvent.MOUSE_MOVE, velumMoveListener);
			_context.stage.removeEventListener(MouseEvent.MOUSE_UP, velumUpListener);
		}
		
		override public function updateVelum():void
		{
			// called in render loop
			if (_view.perimeterMode) {
				updatePerimeterBox();
			} else if (_model.numChildren > 0) {
				// velum will not be rotated, so we must compute bounds in non-rotated context
				var rect:Rectangle = computeVelumBounds(velumHolderView.parent);
				if (rect == null)  // velum has no objects... that's ok
					rect = new Rectangle();
								
				// round the coordinates to minimize jitter of the velum frame
				rect.x = Math.round(rect.x);
				rect.y = Math.round(rect.y);
								
				// when obj-drag causes topLeft of velum to move, wires inside gadget must redraw since wireboard has moved
				if (rect.x != velumHolder.x || rect.y != velumHolder.y) {
					velumHolder.x = rect.x;
					velumHolder.y = rect.y;
					ApplicationController.instance.wireController.requestRedrawForObject(_composite);
				}
				
				_view.drawVelum(rect.width, rect.height);  // pass width & height for velum
				
				// update the composite MC, if its docked
				if (!_composite.messageCenterRepositioned && _compositeView.messageCenterView) {
					var mc:MessageCenterView = _compositeView.messageCenterView;
					var area:Rectangle = EditorUI.instance.messageCenterPlacementArea(mc.parent);
					mc.updatePosition(area);
				}

				updateWireboardsIfRequested(rect.width);
			}
			
			// find parent composite mediator, if any, and let it draw perimeter
			var myLevel:int = _authorController.editingMediatorIndex(this);
			var ed:IEditingMediator = _authorController.editingMediatorForIndex(myLevel-1);
			if (ed is CompositeVelumMediator)
				CompositeVelumMediator(ed).updateVelum();
		}
		
		private function updatePerimeterBox():void
		{
			var topEditor:CompositeVelumMediator = _authorController.currentVelumMediator as CompositeVelumMediator;
			if (topEditor && topEditor != this) {
				var topRect:Rectangle = topEditor._compositeView.velumView.getBounds(velumHolderView.parent);
				var myRect:Rectangle = computeVelumBounds(velumHolderView.parent);
				var sumRect:Rectangle = (myRect) ? topRect.union(myRect) : topRect;
				if (sumRect.width > topRect.width || sumRect.height > topRect.height) {
					_view.visible = true;
					sumRect.inflate(3,3);
					_view.drawVelum(sumRect.width, sumRect.height);  // pass width & height for velum
					velumHolder.x = sumRect.x;
					velumHolder.y = sumRect.y;
				} else {
					_view.visible = false;  // the dotted line would be no larger than the velum
				}
			}
		}

        private function get velumHolder():AbstractObject
        {
            return _view.velumHolder;
        }

        private function get velumHolderView():DisplayObjectContainer
        {
            return _view.velumHolder.getView(_context);
        }

		private function computeVelumBounds(frame:DisplayObject):Rectangle
		{
			// bounds in the coordinate system of "frame"
			var rect:Rectangle;
//			trace("computeVelumBounds frame="+frame);

			var count:int = _compositeView.childViewContainer.numChildren;
			for (var n:int=0; n < count; n++) {
				var objView:ObjectView = _compositeView.childViewContainer.getChildAt(n) as ObjectView;
				if (!isSpecialObject(objView.object)) {  // ignore placeholder of nested gadget being edited
					var objRect:Rectangle = objView.getBounds(frame);
					if (rect == null) {
						rect = objRect;
					} else {
						rect = rect.union(objRect);
					}
				}
			}		
			if (rect == null)
				return null;
			
			// include message-centers in bounds ... not just immediate children but arena children also
			var children:Array = GadgetEncoder.collectChildren(_compositeView.model.value as AbstractObject, null, true, false);
			for each (var obj:AbstractObject in children) {
				if (obj != _composite) {
					objView = obj.getView();
					if (objView && objView.messageCenterView && objView.messageCenterView.visible) {
						objRect = objView.messageCenterView.getBounds(frame);
						rect = rect.union(objRect);
					}
					if (objView as ArenaView) {
						var controlBar:ArenaControlBar = ArenaView(objView).controlBar;
						if (controlBar && controlBar.visible) {
							objRect = controlBar.getBounds(frame);
							rect = rect.union(objRect);
						}
					}
				}
			}
			return rect;
		}

        public static function isSpecialObject(obj:AbstractObject):Boolean
        {
            var title:String = obj.title;
            return (title == PLACEHOLDER_TITLE || title == VELUM_TITLE);
        }
		
		private function trailRemoveHiliteListener(event:CompositeEvent):void
		{
			// an event has been dispatched to remove the hilite caused by rolling over a breadcrumb
			removeHilite();
			
			childRolloverAffectsTrail();  // clicked obj to select:  we are probably still rolled over the object
			updateTrail();  // even if no previous hilite ... selection may have changed
		}
		
		private function updateTrail():void
		{
			// update the trail, and let it know which box in the trail has been rolled over, if any
			var models:Array = modelsForTrail;
			_trail.updateView(_hilitedObj, models);
			
			if (_hilitedObj)
				_view.heading = _hilitedObj.title;
			else
				_view.heading = "Making Object";
		}
		
		private function get modelsForTrail():Array
		{
			var array:Array = [];
			array.push(_model);
			var childCount:int = _model.numChildren;
			for (var childNum:int=0; childNum < childCount; childNum++) {
				var child:TaconiteModel = _model.getChildAt(childNum);
				child = filterTrailObjects(child);
				if (child)
					array.push(child);
			}
			return array;
		}
		
		private function filterTrailObjects(child:TaconiteModel):TaconiteModel
		{
			// show the trail order as if temp-bring-front had not been done
			if (child.value == _tempObjFront)
				return null;  // skip the one brought to front
			if (child.value == _layerPlaceholder)
				return _tempObjFront.model;  // where placeholder appears in trail, show the real object
			return child;
		}

	}
}
