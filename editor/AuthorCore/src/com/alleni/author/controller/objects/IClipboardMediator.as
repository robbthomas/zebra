package com.alleni.author.controller.objects
{
	import flash.events.Event;

	public interface IClipboardMediator
	{
		function cut(e:Event):void;
		function copy(e:Event):void;
		function paste(e:Event):void;
	}
}
