package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.model.objects.Text;
	import com.alleni.taconite.model.TaconiteModel;

	/**
	 * Controller especially for the Text object
	 * 
	 */
	public class TextController extends ObjectController
	{	
		public static var instance:TextController = new TextController();

		static protected function getText(model:TaconiteModel):Text
		{
			return model.value as Text;
		}
	}
}
