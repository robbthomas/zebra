/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller.objects
{
import com.alleni.author.controller.ObjectController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;

import flash.events.MouseEvent;

import mx.collections.IList;

public class ContainerController extends ObjectController
	{	
		public static var instance:ContainerController = new ContainerController();

		static private function getContainer(model:TaconiteModel):AbstractContainer
		{
			return model.value as AbstractContainer;
		}
		
		override protected function handleModelChange(kind:String, parent:TaconiteModel, child:TaconiteModel, index:int):void
		{
			super.handleModelChange(kind,parent,child,index);
			switch(kind)
			{
				case ModelEvent.ADD_CHILD_MODEL:
				case ModelEvent.REMOVE_CHILD_MODEL:
					var obj:AbstractObject = child.value as AbstractObject;
					// do not update zIndex while reordering for layers
					// because reordering physically moves objects in and out
					// of the display which send uneeded model updates - these
					// uneeded updates can cause trigger updates for events to
					// pile up in one render and lead to a false positive
					if(!(parent.value as AbstractContainer).doingLayerReorder && !obj.dragging)
					{
						obj.zIndex = parent.getChildIndex(obj.model);
					}
					
					break;
			}
		}
		
		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
		{	
			super.handleModelUpdate(model,property,newValue,oldValue,parent);

			switch (property) {
				case "layer":
					requestLayerReorder(model.parent.value as AbstractContainer);
					break;
				case "showAll":
					var count:int = model.numChildren;
					for (var i:int = count-1; i >= 0; i--) {
						var m:TaconiteModel = model.getChildAt(i);
						var obj:AbstractObject = m.value as AbstractObject;
						obj.showAll = newValue;
					}
					break;
				case "toDiagram":
					// propagate the new value to all children, so the Arena children will close their MCs
//					var count:int = model.numChildren;
//					for (var i:int = count-1; i >= 0; i--) {
//						var m:TaconiteModel = model.getChildAt(i);
//						var obj:AbstractObject = m.value as AbstractObject;
//						obj.toDiagram = newValue;
//					}
					break;
                case "width":
                case "height":
                    if (model.value is World) {
                        var world:World = model.value as World;
                        var flow:EventFlow = world.eventFlow;
                        flow.width = world.width;
                        flow.height = world.height;
                    }
                    break;
			}
		}
		
		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			super.handleTrigger(object, modifier);
		}
		
		protected function requestLayerReorder(container:AbstractContainer):void
		{
			if(!container.doingLayerReorder && !container.loading)
			{
				container.doingLayerReorder = true;
				ApplicationController.instance.requestReorderAction(function ():void { handleLayerReorder(container) });
			}
		}

		public function handleLayerReorder(container:AbstractContainer):void
		{
            container.doingLayerReorder = true;
			var objs:Array = container.objects.toArray();
			var layers:Array = new Array();
			var sorted:Array = new Array();
			var i:int = 0;
			
			
			
			var m1:AbstractObject;
			for each(m1 in objs) {
				container.model.moveChildOutofList(m1.model);
				if(layers.indexOf(m1.layer) < 0) layers.push(m1.layer); 
			}
			layers.sort(Array.NUMERIC);
			var j:int;
			for each(j in layers) {
				var m2:AbstractObject;
				for each(m2 in objs) {
					if(m2.layer == j) sorted.push(m2);
				}
			}
			
			var m3:AbstractObject;
			for each(m3 in sorted) {
				container.model.moveChildIntoList(m3.model,i++);
			}
			for each(m3 in sorted) {
				// we are now setting zIndex because we suspend
				// updating the model while reordering is occuring.
				if(m3.dragging)
				{
					m3.zIndex = -1;
				}else{
					m3.zIndex = m3.model.parent.getChildIndex(m3.model);
				}
			}
			
			container.doingLayerReorder = false;
			container.setInitialZIndex(false);
		}

		/**
		 * 
		 * Overrideable methods for managing container specific
		 * behavior of dragging children.
		 * 
		 */		
		public function childStartDrag(e:MouseEvent, selectedChild:ObjectView):void {}
		public function childMoveDrag(e:MouseEvent, selectedChild:ObjectView):void {}

		public function childEndDrag(e:MouseEvent, selectedChild:ObjectView):void {
            var obj:AbstractObject = selectedChild.object;
        }
	}
}
