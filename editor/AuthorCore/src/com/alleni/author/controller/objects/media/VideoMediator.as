package com.alleni.author.controller.objects.media
{
	import com.alleni.author.model.objects.VideoObject;
	import com.alleni.taconite.model.ITaconiteObject;
	
	import flash.display.DisplayObject;
	
	public class VideoMediator extends AbstractMediaMediator implements IMediaMediator
	{
		protected var _videoObject:VideoObject;
		
		
		override public function handleMediaEvents(object:ITaconiteObject):IMediaMediator
		{
			super.handleMediaEvents(object);
			
			_videoObject = VideoObject(_mediaObject);
			
			return this;
		}
		
		protected function invalidateViewWith(newChild:DisplayObject):void
		{
			while (_videoObject.contentViewer.numChildren > 0)
				_videoObject.contentViewer.removeChildAt(0);
			
			if (newChild)
				_videoObject.contentViewer.addChild(newChild);
		}
		
		override public function unload():void
		{
			super.unload();
			invalidateViewWith(null);
		}
	}
}