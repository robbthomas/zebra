package com.alleni.author.controller.objects
{
	import com.alleni.author.application.ApplicationUI;
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.action.TableRearrangeAction;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.view.objects.tables.AbstractTableView;
import com.alleni.author.view.objects.tables.TableCell;
import com.alleni.author.view.objects.tables.TruthTableCell;
import com.alleni.author.view.objects.tables.TruthTableGrid;
import com.alleni.author.view.objects.tables.TruthTableView;
	import com.alleni.taconite.controller.DragMediator;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class TableHeaderDragMediator extends DragMediator
	{
		public static const COLUMNS:String="COLUMNS";
		public static const LOGIC_ROWS:String="LOGIC_ROWS";
		public static const VALUE_ROWS:String="VALUE_ROWS";
	
		private var _view:AbstractTableView;
		private var _section:String;
		private var _index:int;
		private var _clickHandler:Function;
		private var _draggableGhost:Sprite;
	
		private var _maxIndex:int = -1;
		private var _newIndex:int = -1;

		private var _copying:Boolean = false;
	
		private function setMaxIndex():void
		{
			switch(_section) {
				case COLUMNS:
					_maxIndex = _view.table.numColumns;
					break;
				case LOGIC_ROWS:
					_maxIndex = _view.table.port.length;
					break;
				case VALUE_ROWS:
					_maxIndex = _view.table.currentStateValues.length;
					break;
			}
		}
	
		private function setNewIndex():void
		{
			var proposed:int = _view.getGridMousePosition(_section);
			if(_index < 0) {
				if(proposed < 0 || proposed > _maxIndex) {
					_view.hideInsertHighlight();
					_newIndex = -1; // same place
					//_view.setTitle("No change to " + _section + " " + _index);
				} else {
					_view.drawInsertHighlight(_section, proposed);
					_newIndex = proposed;
					//_view.setTitle("Move " + _section + " " + _index + " to position " + _newIndex);
				}
			} else if(proposed < 0 || proposed > _maxIndex || _view.mouseY > _view.height + 2 || 
						_view.mouseY < -2 || _view.mouseX > _view.width + 2 || _view.mouseX < -2) {
				_view.drawDeleteHighlight();
				_newIndex = -2; // out of section
				//_view.setTitle("Delete " + _section + " " + _index);
			} else if(proposed == _index || proposed == _index+1) {
				if(_copying) {
					_view.drawInsertHighlight(_section, proposed);
					_newIndex = proposed;
				} else {
					_view.hideInsertHighlight();
					_newIndex = -1; // same place
				}
				//_view.setTitle("No change to " + _section + " " + _index);
			} else if(proposed > _index) {
				_view.drawInsertHighlight(_section, proposed);
				if(_copying) {
					_newIndex = proposed;
				} else {
					_newIndex = proposed-1; // account for shifting later rows/columns back
				}
				//_view.setTitle("Move " + _section + " " + _index + " to position " + _newIndex);
			} else {
				_view.drawInsertHighlight(_section, proposed);
				_newIndex = proposed;
				//_view.setTitle("Move " + _section + " " + _index + " to position " + _newIndex);
			}
		}
	
		public function TableHeaderDragMediator(hotspot:DisplayObject, view:AbstractTableView, section:String, index:int, clickHandler:Function=null)
		{
			super(view.context, true);
			
			_view = view;
			_section = section;
			_index = index;
			_clickHandler = clickHandler;
			hotspot.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		}

        override public function handleMouseDown(e:MouseEvent):void{
            setMaxIndex();
            if(_maxIndex > 1){
                super.handleMouseDown(e);
            }
        }

		override protected function handleClick(e:MouseEvent):void
		{
			super.handleClick(e);
			if(_clickHandler != null) {
				_clickHandler(e);
			}
		}
	
		override protected function handleDragStart(e:MouseEvent):void
		{
			if(_view is TruthTableView) {
				return; // this section disabled at least for now.
			}
			super.handleDragStart(e);
			setMaxIndex();
			setNewIndex();

			_copying = e.altKey;
			
			_draggableGhost = _view.makeDragSnapShot(_index, _section);
			var localLocation:Point = _view.getDragableLocation(_index, _section);
			var globalLocation:Point = _view.localToGlobal(localLocation);
			_draggableGhost.x = globalLocation.x;
			_draggableGhost.y = globalLocation.y - 20;
			_draggableGhost.alpha = .70;
			EditorUI.instance.rawChildren.addChild(_draggableGhost);
			_draggableGhost.startDrag(false, new Rectangle(0,0, EditorUI.instance.width, EditorUI.instance.height));
			
			_view.muteDragArea(_index, _section);
            _view.muteHeadingRollovers();
		}
		
		override protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
		{
			if(_view is TruthTableView) {
				return; // this section disabled at least for now.
			}
			super.handleDragMove(e);
			setNewIndex();
			if((_view.mouseX > _view.width - 22) && (_section == COLUMNS)){
				_view.addEventListener(Event.ENTER_FRAME, moveColumnsLeft);
			}else if((_view.mouseX < _view.nowColumnRightEdge - 5) && (_section == COLUMNS)){
				_view.addEventListener(Event.ENTER_FRAME, moveColumnsRight);
			}else{
				_view.removeEventListener(Event.ENTER_FRAME, moveColumnsLeft);
				_view.removeEventListener(Event.ENTER_FRAME, moveColumnsRight);
			}
		}
	
		private function moveColumnsLeft(event:Event):void{
			var scrollSpeed:Number = (_view.width - 18) - _view.mouseX;
			_view.updateScrollLocation(scrollSpeed);
		}
		
		private function moveColumnsRight(event:Event):void{
			var scrollSpeed:Number = _view.nowColumnRightEdge - _view.mouseX;
			_view.updateScrollLocation(scrollSpeed);
		}
		
		override protected function handleDragEnd(e:MouseEvent):void
		{
			if(_view is TruthTableView) {
				return; // this section disabled at least for now.
			}
			EditorUI.instance.rawChildren.removeChild(_draggableGhost);
			_draggableGhost = null;
			_view.unmuteDragArea();
			if(_newIndex == -1) {
				return;
			}
			_view.table.fireBranches = false;
			super.handleDragEnd(e);
			
			_view.removeEventListener(Event.ENTER_FRAME, moveColumnsLeft);
			_view.removeEventListener(Event.ENTER_FRAME, moveColumnsRight);
			
			_view.hideInsertHighlight();
			var action:TableRearrangeAction = TableRearrangeAction.fromObject(_view.table, _section, _copying, _index, _newIndex);
			action.perform();
			ApplicationController.currentActionTree.commit(action);
            _view.restoreHeadingRollovers();
		}
	}
}