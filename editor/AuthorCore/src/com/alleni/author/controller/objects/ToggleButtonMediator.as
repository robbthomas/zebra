/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.controller.objects
{
	import caurina.transitions.Tweener;

import com.alleni.author.controller.AuthorController;

import com.alleni.author.controller.text.TextEditMediator;
	import com.alleni.author.definition.DoubleClickAction;
import com.alleni.author.definition.action.ActionGroup;
import com.alleni.author.definition.action.ModifyObjectProperty;
import com.alleni.author.definition.action.ModifyRibbonAction;
import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.model.objects.ToggleButton;
	import com.alleni.author.model.objects.ToggleButtonGroup;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.model.ui.Asset;
	import com.alleni.author.view.objects.ToggleButtonView;
	import com.alleni.author.view.text.RichTextField;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mx.events.PropertyChangeEvent;


	public class ToggleButtonMediator
	{
		private var _view:ToggleButtonView;
		private var _btn:ToggleButton;
		private var _textEditMediator:TextEditMediator;
		private var _groupIndicator:Sprite;
		private var _currentGraphicAsset:Asset;
		
		public function ToggleButtonMediator(toggleButtonView:ToggleButtonView)
		{
			_view = toggleButtonView;
			_btn = _view.model.value as ToggleButton;
			_textEditMediator = new TextEditMediator();
			_textEditMediator.handleTextEditEvents(_view.textField, okToEditText,
				updateText,  // update=null
				TextEditMediator.closeOnEnter,
				handleCloseEditLabel,null,false);
			_groupIndicator = new Sprite();
		}
		
		private function updateText(field:RichTextField, value:String):void
		{
			_btn.labelHeight = field.containerHeight;
		}
		
		public function showGroupIndicator():void
		{
			// similar code in ToggleButtonMediator, LineView, PathObjectView, 
			_view.addChild(_groupIndicator);
			_groupIndicator.graphics.clear();
			_groupIndicator.graphics.lineStyle(3,0x990000,1);
			_groupIndicator.graphics.moveTo(3,3);
			_groupIndicator.graphics.lineTo(_view.currentDisplayedGraphic.width-3,3);
			_groupIndicator.graphics.lineTo(_view.currentDisplayedGraphic.width-3,_view.currentDisplayedGraphic.height-3);
			_groupIndicator.graphics.lineTo(3,_view.currentDisplayedGraphic.height-3);
			_groupIndicator.graphics.lineTo(3,3);
			_groupIndicator.alpha = 0;
            _groupIndicator.x = _btn.left;
            _groupIndicator.y = _btn.top;
			Tweener.addTween(_groupIndicator,{alpha:0.8, time:1, transition:"easeOutQuart",
					onComplete:function():void {
						Tweener.addTween(_groupIndicator,{alpha:0, time:0.5, transition:"easeOutQuart", onComplete:function():void {
						_view.removeChild(_groupIndicator);
						_btn.showingGroupIndicator= false;
						}
					});
				}});
			
		}
		
		public function monitorToggleButtonGroup(group:ToggleButtonGroup):void
		{
			if (!group) return;
			group.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
		}

        private function handlePropertyChange(event:PropertyChangeEvent):void
        {
            if(event.property == "toggleButtonGroupValue") {
                _btn.toggleButtonGroupValue = event.newValue as String;
            }
            if(event.property == "toggleButtonGroupNullValue") {
                if(_btn.toggleButtonGroupValue == "" || _btn.toggleButtonGroupValue == event.oldValue as String){
                    _btn.toggleButtonGroupValue = event.newValue as String;
                }
                _btn.toggleButtonGroupNullValue = event.newValue as String;
            }
        }

		public function unmonitorToggleButtonGroup(group:ToggleButtonGroup):void
		{
			if (!group) return;
			group.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
		}

		public function warnViewToDelete():void
		{
			if (!_currentGraphicAsset) return;
			_currentGraphicAsset.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleAssetPropertyChange);
			_currentGraphicAsset = null;
		}
		
		/**
		 * Handles an asset change and attaches/dettaches listeners
		 * @param newValue
		 * @param oldValue
		 * 
		 */
		public function acceptNewAsset(newValue:Asset, oldValue:Asset=null):void
		{
			if (newValue && newValue != oldValue) {
				_currentGraphicAsset = newValue;
				newValue.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleAssetPropertyChange);
			}	
			if (oldValue && oldValue != newValue) {
				oldValue.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleAssetPropertyChange);
			}
            _view.adjustButtonSizeForNewAsset();   // immediately adjust width,height for benefit of Undo
		}
		
		public function informOfNeedForContent(asset:Asset):void
		{
			if (_currentGraphicAsset)
				_currentGraphicAsset.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleAssetPropertyChange);
			_currentGraphicAsset = asset;
			_currentGraphicAsset.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleAssetPropertyChange);
		}
		
		/**
		 * Handles updates to an assets content (i.e. loading).
		 * @param event
		 * 
		 */
		private function handleAssetPropertyChange(event:PropertyChangeEvent):void
		{
			const asset:Asset = event.target as Asset;
			if (!asset) return;
			if (asset.content) {
				asset.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleAssetPropertyChange);
				_btn.assetsLoaded++;
			}
		}
		
		private function okToEditText(event:MouseEvent):int
		{
			if (Application.running || _btn.childOfClosedMasterPage || event.type != MouseEvent.DOUBLE_CLICK) { // when paused, double click required to edit
				return DoubleClickAction.IGNORE;
			} else {
				_btn.editingText = true;
				return DoubleClickAction.EDIT;
			}	
		}
		
		private function handleCloseEditLabel(val:String):void
		{
			val = FormattedText.convertToString(val);
            var oldLabel:String = _btn.label;
            var oldLabelDown:String = _btn.labelDown;
            var oldTitle:String = _btn.title;
			
			if (_btn.editingText) {
				if(_btn.checked && _btn.labelDown != ""){  // when labelDown="", all changes go to "label" regardless of button state
					_btn.labelDown = val;
				}else {
					_btn.label = val;
					
					if (!_btn.titleChangedByAuthor)
						_btn.title = val;  // only <label> is copied to MC title
				}
				
				_btn.initialValues["label"] = _btn.label;
				_btn.initialValues["labelDown"] = _btn.labelDown;

                var group:ActionGroup = new ActionGroup("Rename button");
                group.add(ModifyObjectProperty.fromObject(_btn, "set label", "label", -1, oldLabel));
                group.add(ModifyObjectProperty.fromObject(_btn, "set downState", "labelDown", -1, oldLabelDown));
                group.add(ModifyObjectProperty.fromObject(_btn, "set title", "title", -1, oldTitle));
                AuthorController.instance.currentActionTree.commit(group);
			}
			_btn.editingText = false;
		}
	}
}