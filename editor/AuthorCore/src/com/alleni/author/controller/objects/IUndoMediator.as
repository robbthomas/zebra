/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 1/30/11
 * Time: 4:55 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.objects {
public interface IUndoMediator {
	function canUndo():Boolean;
	function canRedo():Boolean;
	function undoName():String;
	function redoName():String;
	function undo():void;
	function redo():void;
}
}
