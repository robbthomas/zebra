package com.alleni.author.controller.objects.layout
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
import com.alleni.author.view.objects.ArenaPageView;
import com.alleni.taconite.view.ViewContext;
	
	import flash.display.DisplayObjectContainer;
	import flash.geom.Rectangle;
	
	import mx.collections.IList;

	public class Layout
	{
		public static const ALIGN_LEFT:String 		= "left";
		public static const ALIGN_CENTER:String 	= "center";
		public static const ALIGN_RIGHT:String 		= "right";
		
		public static const ALIGN_TOP:String 		= "top";
		public static const ALIGN_MIDDLE:String 	= "middle";
		public static const ALIGN_BOTTOM:String 	= "bottom";

        // invalid child-view coordinates can crash arena rollover glow, if contentHeight is wired to arena.height
        public static const MAX_COORD:Number = 5000000;  // five million, since typical invalid Sprite coordinate is 671106


		public function position(pageView:ArenaPageView, animate:Boolean=true):void
		{
			calculatePlacements(pageView, animate);
		}
		
		protected function calculatePlacements(pageView:ArenaPageView,animate:Boolean=true):void
		{
		}

        protected function getView(obj:AbstractObject, context:ViewContext):ObjectView
        {
            return obj.getView(context);
        }
		
		final protected function getObjBounds(page:ArenaPage, obj:AbstractObject, context:ViewContext):Rectangle
		{
            var view:ObjectView = getView(obj, context);
			var container:DisplayObjectContainer = view.parent;
			return view.getBounds(container);
		}
	}
}