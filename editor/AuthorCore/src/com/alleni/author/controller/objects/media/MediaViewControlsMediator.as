package com.alleni.author.controller.objects.media
{
	import assets.icons.media.capsuleInner;
	import assets.icons.media.capsuleOuter;
	import assets.icons.media.controlPauseBlue;
	import assets.icons.media.controlPauseGray;
	import assets.icons.media.controlPauseWhite;
	import assets.icons.media.controlPlayBlue;
	import assets.icons.media.controlPlayGray;
	import assets.icons.media.controlPlayWhite;
	import assets.icons.media.controlResumeBlue;
	import assets.icons.media.controlResumeGray;
	import assets.icons.media.controlResumeWhite;
	
	import caurina.transitions.Tweener;
	
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.application.ObjectIcons;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.MediaObject;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.ui.components.HBox;
	import com.alleni.author.view.ui.components.VBox;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.filters.GradientBevelFilter;
	import flash.geom.Point;
	import flash.external.ExternalInterface;

	public class MediaViewControlsMediator
	{
		private static var _dropShadow:DropShadowFilter;
		
		private var _view:ObjectView;
		private var _controls:Sprite;
		private var _containerBox:VBox;
		private var _scrubberMediator:MediaScrubberMediator;
		private var _position:Point;

		public function MediaViewControlsMediator()
		{
			if (!_dropShadow)
				_dropShadow = new DropShadowFilter(2.0, 90, 0, 0.5, 4.0, 4.0, 0.8, 3);
			_position = new Point(0, 0);
		}
		
		public function handleViewControls(view:ObjectView):MediaViewControlsMediator
		{
			_view = view;
			_view.addEventListener(MouseEvent.ROLL_OVER, handleShowControls);
			
			var controller:MediaController = (_view.model.value as MediaObject).controller as MediaController;
			
			var outerFrame:DisplayObject = new capsuleOuter();
			outerFrame.alpha = 0.7;
			outerFrame.filters = [new GradientBevelFilter(2, 80, [0xffffff, 0x222222], [1, 0.2], [0, 0xff], 2, 2, 1, 2)];
			var innerFrame:DisplayObject = new capsuleInner();
			innerFrame.alpha = 0.8;
			innerFrame.filters = [new GlowFilter(0, 0.7, 4, 4, 1, 2, true)];
			innerFrame.x = (outerFrame.width - innerFrame.width)/2;
			innerFrame.y = (outerFrame.height - innerFrame.height)/2;
			
			_controls = new Sprite();
			_controls.graphics.beginFill(0, 0);
			_controls.graphics.drawRect(0, outerFrame.height, outerFrame.width, 5);
			_controls.alpha = 0.9;
			_controls.addChild(outerFrame);
			_controls.addChild(innerFrame);
			_controls.filters = [new DropShadowFilter(2, 80, 0, 0.8, 3, 3, 0.7, 2)];
			_controls.visible = false;
			
			updatePosition();
			
			var hitArea:Shape = new Shape();
			hitArea.graphics.beginFill(0, 1);
			hitArea.graphics.drawRect(0, 0, 20, 14);
			
			_containerBox = new VBox(_controls, 0, 0, 2);
			_containerBox.alignment = VBox.CENTER;
			var controlBox:HBox = new HBox(_containerBox, 0, 0, 0);
			controlBox.alignment = HBox.MIDDLE;
			
			var playButton:SimpleButton = new SimpleButton(new controlPlayGray(), new controlPlayWhite(), new controlPlayBlue(), hitArea);
			playButton.addEventListener(MouseEvent.CLICK, function():void {
				controller.handleTrigger(_view.model.value, Modifiers.instance.fetch("start"));
			});
			controlBox.addChild(playButton);
			var pauseButton:SimpleButton = new SimpleButton(new controlPauseGray(), new controlPauseWhite(), new controlPauseBlue(), hitArea);
			pauseButton.addEventListener(MouseEvent.CLICK, function():void {
				controller.handleTrigger(_view.model.value, Modifiers.instance.fetch("pause"));
			});
			controlBox.addChild(pauseButton);
			var resumeButton:SimpleButton = new SimpleButton(new controlResumeGray(), new controlResumeWhite(), new controlResumeBlue(), hitArea);
			resumeButton.addEventListener(MouseEvent.CLICK, function():void {
				controller.handleTrigger(_view.model.value, Modifiers.instance.fetch("resume"));
			});
			controlBox.addChild(resumeButton);
			/*recordLabel.addEventListener(MouseEvent.CLICK, function():void {
				controller.handleTrigger(_view.model.value, Modifiers.instance.fetch("record"));
			});*/
			
			var controlFilter:DropShadowFilter = new DropShadowFilter(1, 80, 0, 0.8, 2, 2, 0.9, 2);
			controlBox.draw();
			controlBox.filters = [controlFilter];

			_scrubberMediator = new MediaScrubberMediator().handleScrubber(_view, false, false, _containerBox);
			_scrubberMediator.scrubber.alpha = 1;
			_scrubberMediator.scrubber.filters = [controlFilter];
			
			_containerBox.draw();
			
			_containerBox.x = (outerFrame.width - _containerBox.width)/2;
			_containerBox.y = (outerFrame.height - _containerBox.height)/2-3;
			return this;	
		}
		
		public function updatePosition():void
		{
			if (!_view || !_view.object) return;
			const object:AbstractObject = _view.object;
			_position.x = object.left + object.width/2 - (_controls.width)/2;
			_position.y = object.top + object.height/2 - (ObjectIcons.SIZE/2 + _controls.height);
			if (_controls.stage != null) {
				calculateControlPosition();
			}
		}

		private function calculateControlPosition():void
		{
			var stagePosition:Point = _view.localToGlobal(_position);
			_controls.x = stagePosition.x;
			_controls.y = stagePosition.y;
		}
		
		private function handleShowControls(event:Event):void
		{
			if (_controls.stage == null && !Application.running) {
				calculateControlPosition();
				_view.stage.addChild(_controls);
				_containerBox.draw();

				_controls.alpha = 0;
				_controls.visible = true;
				Tweener.addTween(_controls,{alpha:0.8, time:0.2, transition:"easeOutQuart" ,
					onComplete:function():void {
						_view.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleHideControls);
					}});
			}
		}

		private function handleHideControls(event:MouseEvent):void
		{
			if (_view == null) {
				if (_controls != null && _controls.parent != null)
					_controls.parent.removeChild(_controls);
			}

			if ((_view.model == null || _view.model.value == null || !(_view.model.value as AbstractObject).dragging)
					&& (event != null && !_view.hitTestPoint(event.stageX, event.stageY, false)
						&& !_controls.hitTestPoint(event.stageX, event.stageY, false)
						|| event == null)) {
				if (_view.stage != null)
					_view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleHideControls);

				Tweener.addTween(_controls,{alpha:0, time:0.2, transition:"easeOutQuart",
					onComplete:function():void {
						_controls.visible = false;
						if (_controls.parent != null) {
							_controls.parent.removeChild(_controls);
						}
					}});
			}
		}
		
		public function warnBeforeDelete():void
		{
			handleHideControls(null);
			_view.removeEventListener(MouseEvent.ROLL_OVER, handleShowControls);
			_scrubberMediator.warnBeforeDelete();
		}
	}
}
