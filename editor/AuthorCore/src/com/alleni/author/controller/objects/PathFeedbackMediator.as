/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.controller.objects
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.event.PathEvent;
	import com.alleni.author.model.objects.PathNode;
	import com.alleni.author.model.objects.PathObject;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.feedback.PathFeedback;
	
	import flash.display.Shape;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.events.PropertyChangeEvent;

	public class PathFeedbackMediator
	{
		private var _path:PathObject;
		private var _feedback:PathFeedback;
		
		public function PathFeedbackMediator(path:PathObject, feedback:PathFeedback)
		{
			_path = path;
			_feedback = feedback;
			
			_path.addEventListener(PathEvent.ADD_POINT, addPointListener);
			_path.addEventListener(PathEvent.DELETE_POINT, deletePointListener);
			_path.addEventListener(PathEvent.REQUEST_FLASH, flashListener);
			_path.addEventListener(PathEvent.HILITE_ANCHOR, anchorListener);
			_path.addEventListener(PathEvent.INITIAL_CLICK, initialDrawListener);  // initial creation: mouse event on model
		}
		
		public function clearViewEvents():void
		{
			_path.removeEventListener(PathEvent.ADD_POINT, addPointListener);
			_path.removeEventListener(PathEvent.DELETE_POINT, deletePointListener);
			_path.removeEventListener(PathEvent.REQUEST_FLASH, flashListener);
			_path.removeEventListener(PathEvent.HILITE_ANCHOR, anchorListener);
			_path.removeEventListener(PathEvent.INITIAL_CLICK, initialDrawListener);  // initial creation: mouse event on model
		}
		
		private function addPointListener(event:PathEvent):void
		{
			_feedback.addHandle(event.node);
		}
		
		private function deletePointListener(event:PathEvent):void
		{
			_feedback.removeHandle(event.node);
		}
		
		private function initialDrawListener(event:PathEvent):void
		{
//			event.stopImmediatePropagation();
			trace("PathFeedback: initial point="+event.point);
			_feedback.initialDrawPath(event.point);
		}
		
		private function anchorListener(event:PathEvent):void
		{
			_feedback.hiliteAnchor(event.index, event.yes);
		}

		private function flashListener(event:PathEvent):void
		{
			var point:Point = _path.path.getPoint(event.index);
			var flash:Shape = _feedback.setupFlashGraphic(point);
			flash.alpha = 1.0;
			flash.scaleX = 0.2;
			flash.scaleY = 0.2;
			Tweener.addTween(flash, {scaleX:1, scaleY:1, time:0.5, transition:"linear"});
			Tweener.addTween(flash, {alpha:0, delay:0.4, time:0.1, transition:"linear"});
		}
	}
}