package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.Clock;
	import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.lang.TaconiteTimer;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;

	import flash.events.Event;

	/**
	 * Controller especially for the Clock object
	 * 
	 * Notes:
	 * clock.startTime = value of TaconiteTimer.milliseconds while ticking, else elapsed mSec.
	 * clock.ticking = low-level control of clock mode.  Value = clock.started && clock.running
	 * clock.started = high level control, driven by autoStart and the Start/Stop/Pause/Resume inlets
	 * clock.running = true when zebra is running
	 * clock.
	 * 
	 */
	public class ClockController extends ObjectController
	{	
		public static var instance:ClockController = new ClockController();
		
		private static var _tickModifier:IModifier;
		private static var _cycleFinishedModifier:IModifier;
		private static var _cyclesFinishedModifier:IModifier;
		private static var _taconiteTimer:TaconiteTimer;
		
		public function ClockController():void
		{
			super();
			_tickModifier = Modifiers.instance.fetch("tick");
			_cycleFinishedModifier = Modifiers.instance.fetch("cycleFinished");
			_cyclesFinishedModifier = Modifiers.instance.fetch("cyclesFinished");
			_taconiteTimer = TaconiteTimer.instance;
		}
		
		override protected function activate(model:TaconiteModel):void
		{
			super.activate(model);
			var clock:Clock = getClock(model);

            if (Application.running && clock.resumeTicking) {
            	clock.started = true;
            	switchModes(clock, true);
            }
		}
		
		override protected function deactivate(model:TaconiteModel):void
		{
			super.activate(model);

			var clock:Clock = getClock(model);
            clock.resumeTicking = clock.ticking;
            if (clock.ticking) {
				switchModes(clock, false);
            }
		}
		
		override public function warnBeforeDelete(object:AbstractObject):void
		{
			super.warnBeforeDelete(object);
			if (object.tickListener != null)
				_taconiteTimer.unregisterListener(object.tickListener);
		}
		
		static private function getClock(model:TaconiteModel):Clock
		{
			return model.value as Clock;
		}

        override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
		{
			super.handleModelUpdate(model,property,newValue,oldValue,parent);
			var clock:Clock = getClock(model);

            if (clock.loading) {
                return;
            }
			
			switch (property) {
				case "started":
					switchModes(clock, Application.running);
					break;
				case "ticking":
					if (clock.ticking) {
						clock.tickListener = tick(clock);
						_taconiteTimer.registerListener(clock.tickListener, true);
					} else
						if (clock.tickListener != null)
							_taconiteTimer.unregisterListener(clock.tickListener);
					break;
			}
			
			// a value is being changed by a binding; adjust our values accordingly
			// _wiring.doingBindings removed from conditional below
			if (newValue is Number) {
				var val:Number = newValue as Number;
				switch (property) {
					case "secondsElapsed":
						adjustElapsed(clock, val);
						break;
					case "secondsRemaining":
						adjustElapsed(clock, clock.duration - val);
						break;
					case "percentElapsed":
						adjustElapsed(clock, val * clock.duration / 100)
						break;
					case "percentRemaining":
						adjustElapsed(clock, clock.duration * (1 - val/100))
						break;
				}
			}
		}

		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			super.handleTrigger(object, modifier);
			var clock:Clock = object as Clock;
			switch (modifier.key)
			{
				/*case Modifiers.instance.fetch("resetTiming"): for future use
					reset(clock);
					break;*/
				case "pause":
					clock.started = false; 
					break;
				case "togglePause":
					clock.started = !clock.started; 
					break;
				case "stop":
					clock.started = false;
					reset(clock);
					break;
				case "start":
					if (clock.ticking)
						break;
				case "restart":
					reset(clock);
					clock.started = true;
					break;
				case "resume":
					clock.started = true;
					break;
			}
		}
		
		public function reset(clock:Clock):void
		{
			// initialize
			clock.secondsElapsed = 0;
			clock.startTime = _taconiteTimer.milliseconds;
			clock.started = false;
			clock.ticking = false;
			clock.cycleNumber = 1;
			clock.ticksFired = 0;

			updateElapsedTime(clock);
		}
		
		public function switchModes(clock:Clock, running:Boolean):void
		{
			var ticking:Boolean = clock.started && running;
			if (ticking != clock.ticking) {
				if (ticking) {
					if (clock.secondsElapsed == clock.duration)
						clock.secondsElapsed = 0;
					
					clock.startTime = _taconiteTimer.milliseconds - clock.secondsElapsed*1000;
				}
				clock.ticking = ticking;
			}
		}
		
		private function tick(clock:Clock):Function
		{
			return function(event:Event):void {
				updateElapsedTime(clock);

				if (clock.duration > 0) {
					var tickNum:uint = clock.ticksPerCycle * clock.secondsElapsed / clock.duration;
					if (tickNum > clock.ticksFired) {
						_wiring.sendTrigger(clock, _tickModifier);
						++clock.ticksFired;
					}
				}
				if (clock.secondsElapsed >= clock.duration) { // time is up
					cycleEnd(clock);
				}

				updateValues(clock);
			}
	    }

	    private function updateElapsedTime(clock:Clock):void
	    {
			clock.secondsElapsed = (_taconiteTimer.milliseconds - clock.startTime) / 1000;
	    }
		
		private function cycleEnd(clock:Clock):void
		{
			_wiring.sendTrigger(clock, _cycleFinishedModifier);
			if (clock.cycleNumber < clock.cycles) {  // need another cycle
				clock.startTime += clock.duration * 1000;  // next cycle starts exactly "duration" after previous cycle
				clock.secondsElapsed = 0;
				++clock.cycleNumber;
				clock.ticksFired = 0;
			} else {  // done cycling
				clock.started = false;
				clock.ticking = false;
				clock.secondsElapsed = clock.duration;  // limit to max ... stay there until reset
				_wiring.sendTrigger(clock, _cyclesFinishedModifier);
			}
		}
		
		private function adjustElapsed(clock:Clock, seconds:Number):void
		{
			if (!(seconds != seconds)) { // faster than isNaN. numeric types which are NaN don't even equal themselves!
				if (seconds < 0)  seconds = 0;
				if (seconds > clock.duration)  seconds = clock.duration;
				
				if (clock.ticking)	
					clock.startTime = _taconiteTimer.milliseconds - seconds*1000;
				else
					clock.startTime = seconds*1000;
				clock.secondsElapsed = seconds;
				updateValues(clock);
			}
		}
		
		private function updateValues(clock:Clock):void
		{
			clock.secondsRemaining = clock.duration - clock.secondsElapsed;
			
			if (clock.duration > 0) { // avoid div 0
				clock.percentRemaining = 100 * clock.secondsRemaining / clock.duration;
				clock.percentElapsed = 	100 * clock.secondsElapsed / clock.duration;
			} else {
				clock.percentRemaining = 0;
				clock.percentElapsed = 100;
			}
		}
	}
}
