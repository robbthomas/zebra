/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.controller.objects
{
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ObjectController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.WireCreateUtil;
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.action.ModifyRibbonAction;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ObjectProperties;
import com.alleni.author.model.objects.Checkbox;
import com.alleni.author.model.objects.ToggleButton;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.model.TaconiteObject;
import com.alleni.taconite.view.ITaconiteView;

import flash.events.MouseEvent;

/**
	 * 
	 * @author Shannon Jackson
	 * 
	 */
	public class ToggleButtonController extends ObjectController
	{
		public static var instance:ToggleButtonController = new ToggleButtonController();
		
		/**
		 * Constructor 
		 * 
		 */
		public function ToggleButtonController()
		{
			super();
		}
		
		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel) : void
		{
			super.handleModelUpdate(model,property,newValue,oldValue,parent);
			var btn:ToggleButton = ToggleButton(model.value);
			//trace('>>>>>> btn.x:'+btn.x+' btn.y:'+btn.y);
			switch (property) {
				case "down":
					if(newValue == false && (!btn.dragging || btn.movable || Application.runningLocked || (btn.movable && btn.actLikeRunning)))
					{
						if(btn.over)_wiring.sendTrigger(btn, Modifiers.instance.fetch("buttonPressed"));
					}
					break;
				case "checked":
					_wiring.sendTrigger(btn, Modifiers.instance.fetch((newValue)?"checkedBox":"uncheckedBox"));
					if(btn.toggleButtonGroup)
					{
						if(newValue)
						{
							btn.toggleButtonGroup.toggleButtonGroupValue = btn.toggleButtonValue;
						}
					}
					break;
				case "toggleButtonValue":
					btn.checked = (newValue == btn.toggleButtonGroupValue);
					break;
				case "toggleButtonGroupValue":
					if(btn.toggleButtonGroup) 
					{
                        btn.toggleButtonGroup.toggleButtonGroupValue = newValue as String;
						btn.checked = (newValue == btn.toggleButtonValue);
					}
					break;
                case "toggleButtonGroupNullValue":
                    if(btn.toggleButtonGroup)
                    {
                        btn.toggleButtonGroup.toggleButtonGroupNullValue = newValue as String;
                    }
                    break;
				case "isGraphicMode":
                    adjustForGraphicMode(btn, newValue);
					break;
				case "toggleButtonNormalUp":
				case "toggleButtonNormalDown":
				case "toggleButtonNormalOver":
				case "toggleButtonNormalDisabled":
				case "toggleButtonCheckedUp":
				case "toggleButtonCheckedDown":
				case "toggleButtonCheckedOver":
				case "toggleButtonCheckedDisabled":
					// if this is the first asset being
					// added to the ToggleButton, then 
					// propagate it to all the button 
					// states as defaults.
					if(!btn.isGraphicMode && newValue != null) 
					{
						// toggle tracking variable
						btn.isGraphicMode = true;
						// set all bitmaps to first provided bitmap
						btn.toggleButtonNormalUp = 
							btn.toggleButtonNormalDown =
							btn.toggleButtonNormalOver =
							btn.toggleButtonNormalDisabled =
							btn.toggleButtonCheckedUp =
							btn.toggleButtonCheckedDown =
							btn.toggleButtonCheckedOver =
							btn.toggleButtonCheckedDisabled = newValue as Asset;
					}else if(btn.isGraphicMode && newValue == null){
						btn.toggleButtonNormalUp = 
							btn.toggleButtonNormalDown =
							btn.toggleButtonNormalOver =
							btn.toggleButtonNormalDisabled =
							btn.toggleButtonCheckedUp =
							btn.toggleButtonCheckedDown =
							btn.toggleButtonCheckedOver =
							btn.toggleButtonCheckedDisabled = null;
						btn.isGraphicMode = false;
					} else if(btn.isGraphicMode && property == "toggleButtonCheckedUp" &&
						oldValue == btn.toggleButtonCheckedDown &&
						oldValue == btn.toggleButtonCheckedOver &&
						oldValue == btn.toggleButtonCheckedDisabled) {
						btn.toggleButtonCheckedDown =
							btn.toggleButtonCheckedOver =
							btn.toggleButtonCheckedDisabled = btn.toggleButtonCheckedUp;
					}
					break;
				
			}
		}

        private function adjustForGraphicMode(btn:ToggleButton, graphicMode:Boolean):void
        {
            if(graphicMode) {
                // remove anchors that no longer apply
                removeAppearanceWires(btn);  // normally wires are already removed by assetDroppedOnRibbon, to prevent undo actions being committed here
                btn.removeAnchor(btn.anchors["fillSpec"]);
                btn.removeAnchor(btn.anchors["fillAlpha"]);
                btn.removeAnchor(btn.anchors["lineColor"]);
                btn.removeAnchor(btn.anchors["lineThickness"]);
                btn.removeAnchor(btn.anchors["lineAlpha"]);
                btn.removeAnchor(btn.anchors["downColor"]);
            }else{
                // add anchors that now apply
                ObjectProperties.instance.registerProperty(btn, Modifiers.instance.fetch("fillSpec"), "fillSpec");
                ObjectProperties.instance.registerProperty(btn, Modifiers.instance.fetch("fillAlpha"), "fillAlpha");
                ObjectProperties.instance.registerProperty(btn, Modifiers.instance.fetch("lineColor"), "lineColor");
                ObjectProperties.instance.registerProperty(btn, Modifiers.instance.fetch("lineThickness"), "lineThickness");
                ObjectProperties.instance.registerProperty(btn, Modifiers.instance.fetch("lineAlpha"), "lineAlpha");
                ObjectProperties.instance.registerProperty(btn, Modifiers.instance.fetch("downColor"), "downColor");
                btn.addAnchor(btn.anchors["fillSpec"], false);
                btn.addAnchor(btn.anchors["fillAlpha"], false);
                btn.addAnchor(btn.anchors["lineColor"], false);
                btn.addAnchor(btn.anchors["lineThickness"], false);
                btn.addAnchor(btn.anchors["lineAlpha"], false);
                btn.addAnchor(btn.anchors["downColor"], false);
            }
        }

        private function removeAppearanceWires(btn:ToggleButton):void
        {
            // find all the wires to the anchors we are removing and delete them
            var wires:Array = [];
            wires = wires.concat(ApplicationController.instance.wireController.getWiresForAnchor(btn.anchors["fillSpec"]));
            wires = wires.concat(ApplicationController.instance.wireController.getWiresForAnchor(btn.anchors["fillAlpha"]));
            wires = wires.concat(ApplicationController.instance.wireController.getWiresForAnchor(btn.anchors["lineColor"]));
            wires = wires.concat(ApplicationController.instance.wireController.getWiresForAnchor(btn.anchors["lineThickness"]));
            wires = wires.concat(ApplicationController.instance.wireController.getWiresForAnchor(btn.anchors["lineAlpha"]));
            wires = wires.concat(ApplicationController.instance.wireController.getWiresForAnchor(btn.anchors["downColor"]));
            if (wires.length > 0) {
                var grp:Object = ApplicationController.currentActionTree.openGroup("Change to bitmap mode for "+ btn.title);
                while(wires.length)
                {
                    WireCreateUtil.unCreate(wires.pop(), true);  // commits Undo actions
                }
                ApplicationController.currentActionTree.closeGroup(grp);
            }
        }

		/**
		 * 
		 * @param object
		 * @param modifier
		 * 
		 */
		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			super.handleTrigger(object, modifier);
			var btn:ToggleButton = object as ToggleButton;

			switch (modifier.key)
			{
				case "press":
					_wiring.sendTrigger(btn, Modifiers.instance.fetch("buttonPressed"));
					break;
				case "toggle":
					btn.checked = !btn.checked;
					break;
				case "check":
					btn.checked = true;
					break;
				case "uncheck":
					btn.checked = false;
					break;
				case "enableButton":
					btn.interactive = true;
					break;
				case "disableButton":
					btn.interactive = false;
					break;
			}
		}

		private function hintGroupedButtons(btn:ToggleButton):void
		{
			btn.wireScopeContainer.allObjectsInScope(
				function(obj:TaconiteObject):Boolean
				{
					drawIndicatorIfApplicable(obj,btn);
					return true;
				}
			);
		}
		private function drawIndicatorIfApplicable(obj:TaconiteObject, btn:ToggleButton):void
		{
			var object:TaconiteObject = obj.model.value;
			if(object is ToggleButton && ToggleButton(object).toggleButtonGroup == btn.toggleButtonGroup)
			{
				ToggleButton(object).showingGroupIndicator = true;
			}
		}
		
		/**
		 * 
		 * @param objectView
		 * @param e
		 * 
		 */
		override public function handleMouseOver(objectView:ITaconiteView, e:MouseEvent):void
		{
			var btn:ToggleButton = ToggleButton(objectView.model.value);
			if(btn.interactive && !btn.editingText && !btnInClosedMaster(btn))
			{
				btn.over = true;
				if(btn.inClickCycle)
				{
					btn.down = true;
				}
				if(btn.toggleButtonGroup && !Application.running) hintGroupedButtons(btn);
			}
            if(!btn.editingText) {
                super.handleMouseOver(objectView,  e);
            }
		}
		
		/**
		 * 
		 * @param objectView
		 * @param e
		 * 
		 */
		override public function handleMouseOut(objectView:ITaconiteView, e:MouseEvent):void
		{
			var btn:ToggleButton = ToggleButton(objectView.model.value);
			if(btn.interactive)
			{
				btn.over = false;
				btn.down = false;
			}
            super.handleMouseOut(objectView,  e);
		}
		
		/**
		 * 
		 * @param objectView
		 * @param e
		 * @return 
		 * 
		 */
		override public function handleMouseDown(objectView:ITaconiteView, e:MouseEvent):Boolean
		{
			var btn:ToggleButton = ToggleButton(objectView.model.value);
            var childOfClosed:Boolean = btn.childOfClosed;
			if((btn.interactive && !btn.editingText && ObjectView(objectView).selected) || ((Application.running || btn.actLikeRunning) && btn.interactive  && !btnInClosedMaster(btn)))
			{
				if(!(e && e.altKey) && (Application.running || btn.actLikeRunning))btn.inClickCycle = true;
				btn.down = true;
			}
            super.handleMouseDown(objectView,  e);
			return (Application.runningLocked && btn.movable) || !Application.runningLocked;
		}
		
		/**
		 * 
		 * @param objectView
		 * @param e
		 * 
		 */
		override public function handleMouseUp(objectView:ITaconiteView, e:MouseEvent):void
		{
			var btn:ToggleButton = ToggleButton(objectView.model.value);
            var childOfClosed:Boolean = btn.childOfClosed;
			var oldChecked:Boolean = btn.checked;
            trace("UIRunning:",Application.uiRunning, "Running:", Application.running);
			if((btn.over && btn.interactive && !btn.editingText && ObjectView(objectView).selected && (!btn.dragging || btn.movable))
				|| 
				(btn.over && (Application.running || btn.actLikeRunning) && btn.interactive && (!btn.dragging || btn.movable)))
			{
				
				if(!btn.momentary) btn.checked = 	(	btn.toggleButtonValue != btn.toggleButtonGroupValue ||
														btn as Checkbox || 
														btn.toggleButtonGroup == null
													)?!btn.checked:true;
			}
			if(btn.checked && btn.toggleButtonGroup) btn.toggleButtonGroup.toggleButtonGroupValue = btn.toggleButtonValue;
			btn.down = false;
			btn.inClickCycle = false;
            super.handleMouseUp(objectView,  e);
			
			if (btn.checked != oldChecked) {
				logEvent("toggled", btn);
			}
		}

        private function btnInClosedMaster(btn:AbstractObject):Boolean
        {
            return (AuthorController.instance.eventFlow.editingEventPage && btn.childOfClosedMasterPage);
        }

        override public function assetDroppedOnRibbon(obj:AbstractObject, anchor:WireAnchor, newAsset:IReplaceable):void
        {
            var btn:ToggleButton = obj as ToggleButton;
            var oldX:Number = btn.x;
            var oldY:Number = btn.y;
            var oldWidth:Number = btn.width;
            var oldHeight:Number = btn.height;
            var oldAsset:Asset = anchor.value as Asset;

            var title:String = "Set button graphic on " + btn.title;
            var token:Object = ApplicationController.currentActionTree.openGroup(title);

            var newGraphicMode:Boolean = (newAsset != null);
            if (newGraphicMode && !btn.isGraphicMode) {
                removeAppearanceWires(btn);  // commits undo actions
            }

            // setting anchor.value causes isGraphicMode to be changed, causing appearanceAnchors to be created or deleted
            // ToggleButtonMediator forces immediate update, so btn.width,height,x,y will be adjusted now, captured by ModifyRibbonAction below
            anchor.value = newAsset;

            ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(btn, anchor.path, oldAsset));
            ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(btn, "x", oldX));
            ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(btn, "y", oldY));
            ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(btn, "width", oldWidth));
            ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(btn, "height", oldHeight));
            ApplicationController.currentActionTree.closeGroup(token);
        }

	}
}
