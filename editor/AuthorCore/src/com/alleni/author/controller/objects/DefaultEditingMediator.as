/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 1/30/11
 * Time: 8:46 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.objects {
	import com.alleni.author.controller.AuthorController;
	import com.alleni.author.controller.app.ClipboardController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.DragInfo;
	import com.alleni.author.definition.action.ActionTree;
	import com.alleni.author.definition.action.DestroyObjectAction;
	import com.alleni.author.model.World;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.document.ObjectSelection;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.events.Event;
	
	public class DefaultEditingMediator implements IEditingMediator, IUndoMediator, IClipboardMediator
	{		
        private const _MENULABEL_TYPE:String = " ";

        public function getSelection(getGlobal:Boolean, strict:Boolean=true, omitLocked:Boolean=true):Vector.<DragInfo> {
			return new Vector.<DragInfo>();
        }

        public function cancelEditing():Boolean {
			return false;
		}
	
		public function deleteEditing():Boolean {
			return false;
		}
	
		public function selectAll(includeLocked:Boolean):void
		{
		}
	
		public function makeObject():Boolean {
			return false;
		}
		
		public function get hasActiveState():Boolean
		{
			return true;
		}
		
		public function activeMediatorChanged(mediator:IEditingMediator):void
		{
		}
		
		public function switchedObjects(oldObj:ITaconiteObject, newObj:ITaconiteObject):void {
		}
	
		public function get editingObject():ITaconiteObject {
			return null;
		}
		
		public function get actionTree():ActionTree
		{
			return null;//Application.instance.document.actionTree;
		}

        public function get MENULABEL_TYPE():String{
            return _MENULABEL_TYPE;
        }

		public function canUndo():Boolean {
			return false;
		}
	
		public function canRedo():Boolean {
			return false;
		}
	
		public function undo():void {
		}
	
		public function redo():void {
		}
	
		public function undoName():String {
			return "";
		}
	
		public function redoName():String {
			return "";
		}
	
		public function cut(e:Event):void {
		}
	
		public function copy(e:Event):void {
		}
	
		public function paste(e:Event):void {
		}
	}
}
