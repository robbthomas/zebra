package com.alleni.author.controller.objects
{
	import com.alleni.author.definition.text.ActionMap;
	
	import flash.text.engine.FontLookup;
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	
	import flashx.textLayout.edit.EditManager;
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.ParagraphElement;
	import flashx.textLayout.elements.SpanElement;
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.events.SelectionEvent;
	import flashx.textLayout.formats.TextAlign;
	import flashx.textLayout.formats.TextDecoration;
	import flashx.textLayout.formats.TextLayoutFormat;
	import flashx.textLayout.formats.VerticalAlign;
	
	public class RichTextController
	{
		private var _textFlow:TextFlow;
		private var _actionMap:ActionMap = new ActionMap();
		private var _selectedFormat:TextLayoutFormat;
		private var _outerSpanReadOnly:Boolean = false;
		
		public function RichTextController(textFlow:TextFlow)
		{
			
			_textFlow = textFlow;
			_textFlow.addEventListener(SelectionEvent.SELECTION_CHANGE, selectionChanged);
			
			createDefaultActionMap();
			
		}
		
		public function setTextFlow(textFlow:TextFlow):void{
			clearListeners();
			_textFlow = textFlow;
			_textFlow.addEventListener(SelectionEvent.SELECTION_CHANGE, selectionChanged);
		}
		
		public function makeOuterSpanReadOnly(value:Boolean):void{
			
			_outerSpanReadOnly = value;
		}
		
		
		public function get actions():ActionMap
		{
			return _actionMap;
		}

		public function set actions(value:ActionMap):void
		{
			_actionMap = value;
		}
		
		public function perform(action:String, argument:* = null):void{
			
			var func:Function = _actionMap.get(action);
			func(action, argument);
		}
		

		private function applyFormat(action:String, value:*):void{
			var charStyle:TextLayoutFormat =  new TextLayoutFormat();
			performAction(action, value, charStyle);
		}
		
		
		
		//span related functions
		
		/*
		Selecting an action from inspector will produce a command string
		Selecting an action through a short-cut key will produce null and then gets treated as a toggle
		*/
		
		
		private function performAction(action:String,value:*, charStyle:TextLayoutFormat):void{
			
			switch(action) {
				case ActionMap.FOREGROUNDCOLOR_ACTION:
					
					if(value is uint)
						charStyle.color = value as uint;
					
					trace("colorvalue", value);
					
					break;
				
				case ActionMap.ITALIC_ACTION:
					
					if(value as String){  
						charStyle.fontStyle = value == FontPosture.ITALIC?FontPosture.ITALIC:FontPosture.NORMAL;
					}
					else{
						charStyle.fontStyle = _selectedFormat.fontStyle == FontPosture.ITALIC?FontPosture.NORMAL:FontPosture.ITALIC;
					}
					
					break;
				
				case ActionMap.BOLD_ACTION:
					
					if(value as String){  
						charStyle.fontWeight = value == FontWeight.BOLD?FontWeight.BOLD:FontWeight.NORMAL;
					}
					else{
						charStyle.fontWeight = _selectedFormat.fontWeight == FontWeight.BOLD?FontWeight.NORMAL:FontWeight.BOLD;
					}
					
					
					break;
				
				case ActionMap.UNDERLINE_ACTION:
					
					if(value as String){  
						charStyle.textDecoration = value == TextDecoration.UNDERLINE?TextDecoration.UNDERLINE:TextDecoration.NONE;
					}
					else{
						charStyle.textDecoration = _selectedFormat.textDecoration == TextDecoration.UNDERLINE?TextDecoration.NONE:TextDecoration.UNDERLINE;
					}
					
					break;
				
				
				case ActionMap.FONTSIZE_ACTION:
					
					var newSize:uint = value as uint;
					if (newSize > 0)
						charStyle.fontSize = newSize;
					
					break;
				
				case ActionMap.FONTALPHA_ACTION:
					
					if(value as Number)
						charStyle.textAlpha= (value as Number);
					
					break;
				
				case ActionMap.FONTNAME_ACTION:
					
					if(value as String){
						charStyle.fontFamily = value as String;
						charStyle.fontLookup = selectLookupType(charStyle.fontFamily);
					}
					
					break;
				
				case ActionMap.VERTICAL_ALIGN_ACTION:
					
					if(value is String){
						charStyle.verticalAlign = getAlignmentValue(value);
					} // if value is string
					
					break;
				
				case ActionMap.TEXT_ALIGN_ACTION:
					if(value is String){
						charStyle.textAlign = getAlignmentValue(value);
					}
	
					break;
			}//switch
			
			if(_textFlow && _textFlow.interactionManager){
				var selectionState:SelectionState = _textFlow.interactionManager.getSelectionState();
				selectionState.pointFormat = charStyle;
				
				if(this.isSelected){
					(_textFlow.interactionManager as EditManager).applyLeafFormat(charStyle);
				}
				else{
					_textFlow.format = charStyle;
				}
				
				_textFlow.flowComposer.updateAllControllers();
			}
		}
		
		public function applyStyleToAllElements(action:String, value:*, targetTextFlow:TextFlow):void{
			
			            var leaf:SpanElement = new SpanElement(); 
			            leaf = SpanElement(targetTextFlow.getFirstLeaf()); 
						
						if(!_outerSpanReadOnly)  //text objects should never write to the outermost span -- dictated by style
			         	 	applyStyle(action, value, leaf);  
			           
			            while(leaf = SpanElement(leaf.getNextLeaf())) { 
								applyStyle(action, value, leaf);
			            } 
			
				targetTextFlow.flowComposer.updateAllControllers();
			        
		}
		
		public function isLeafInSelectionRange(leaf:SpanElement):Boolean{
			
			return true;  //to do
		}
		
		private function applyStyle(action:String,value:*, leaf:SpanElement):void{
			
			switch(action) {
				
				case ActionMap.STYLENAME_ACTION:
					
					if(value is String)
						leaf.styleName = value;
					break;
				
				case ActionMap.FOREGROUNDCOLOR_ACTION:
					value = uint(value);
					leaf.color = value as uint;
					break;
				
				case ActionMap.ITALIC_ACTION:
					value = String(value);
					leaf.fontStyle = value == FontPosture.ITALIC||value == "true"?FontPosture.ITALIC:FontPosture.NORMAL;
					break;
				
				case ActionMap.BOLD_ACTION:
					value = String(value);
					leaf.fontWeight = value == FontWeight.BOLD||value == "true"?FontWeight.BOLD:FontWeight.NORMAL;
					break;
				
				case ActionMap.UNDERLINE_ACTION:
					value = String(value);
					leaf.textDecoration = value == TextDecoration.UNDERLINE||value == "true"?TextDecoration.UNDERLINE:TextDecoration.NONE;
					break;
				
				case ActionMap.FONTSIZE_ACTION:
					value = uint(value);
					leaf.fontSize = (Math.max(1,Math.min(value as Number,720)));
					break;
					
				case ActionMap.FONTALPHA_ACTION:
					value = Number(value);
					leaf.textAlpha = (Math.max(0,Math.min(value as Number,1)));
					break;
				
				case ActionMap.FONTNAME_ACTION:
					value = String(value);
					leaf.fontFamily =  value as String;
					leaf.fontLookup = selectLookupType(leaf.fontFamily);
					break;
				
				case ActionMap.VERTICAL_ALIGN_ACTION:
					
					if(value is String){
						leaf.verticalAlign = getAlignmentValue(value);
					}
					
					break;
				
				case ActionMap.TEXT_ALIGN_ACTION:
					
					if(value is String){
						leaf.textAlign = getAlignmentValue(value);
					}
					
					break;
				
			}
			
		}
		
		
		public function clearStyleFromAllElements(action:String, targetTextFlow:TextFlow):void{
			
			            var leaf:SpanElement;
			            leaf = SpanElement(targetTextFlow.getFirstLeaf()); 
			
			            clearStyle(action, leaf);
			           
			            while(leaf = SpanElement(leaf.getNextLeaf())) { 
				clearStyle(action, leaf);
			            } 
			
			targetTextFlow.flowComposer.updateAllControllers();
			        
		}
		
		private function clearStyle(action:String,leaf:SpanElement):void{
			
			switch(action) {
				
				case ActionMap.FOREGROUNDCOLOR_ACTION:
					leaf.clearStyle("color");
					break;
				
				case ActionMap.ITALIC_ACTION:
					leaf.clearStyle("fontStyle");
					break;
				
				case ActionMap.BOLD_ACTION:
					leaf.clearStyle("fontWeight");
					break;
				
				case ActionMap.UNDERLINE_ACTION:
					leaf.clearStyle("textDecoration");
					break;
				
				case ActionMap.FONTSIZE_ACTION:
					leaf.clearStyle("fontSize");
					break;
				
				case ActionMap.FONTALPHA_ACTION:
					leaf.clearStyle("textAlpha");
					break;
				
				case ActionMap.FONTNAME_ACTION:
					leaf.clearStyle("fontFamily");
					break;
				
			}
			
		}
		
		/*
		The default structure is <textFlow><p><span>The race to space</span></p></textFlow>
		*/
		
		public function clearAllStylesFromFirstElement(targetTextFlow:TextFlow):void{
			
			    var leaf:SpanElement;
			    leaf = SpanElement(targetTextFlow.getFirstLeaf()); 
				clearStyle(ActionMap.BOLD_ACTION, leaf);
				clearStyle(ActionMap.FONTALPHA_ACTION, leaf);
				clearStyle(ActionMap.FONTNAME_ACTION, leaf);
				clearStyle(ActionMap.FONTSIZE_ACTION, leaf);
				clearStyle(ActionMap.FOREGROUNDCOLOR_ACTION, leaf);
				clearStyle(ActionMap.ITALIC_ACTION, leaf);
				clearStyle(ActionMap.UNDERLINE_ACTION, leaf);
			
			targetTextFlow.flowComposer.updateAllControllers();
			        
		}
		
		public function clearAllStylesFromAllElements(targetTextFlow:TextFlow):void{
			
			clearStyleFromAllElements(ActionMap.BOLD_ACTION,targetTextFlow);
			clearStyleFromAllElements(ActionMap.FONTALPHA_ACTION, targetTextFlow);
			clearStyleFromAllElements(ActionMap.FONTNAME_ACTION, targetTextFlow);
			clearStyleFromAllElements(ActionMap.FONTSIZE_ACTION, targetTextFlow);
			clearStyleFromAllElements(ActionMap.FOREGROUNDCOLOR_ACTION, targetTextFlow);
			clearStyleFromAllElements(ActionMap.ITALIC_ACTION, targetTextFlow);
			clearStyleFromAllElements(ActionMap.UNDERLINE_ACTION, targetTextFlow);
			
			targetTextFlow.flowComposer.updateAllControllers();
			        
		}
		
		
		/*
		The paragraph level functions are currently not in use, but will be needed in the future
		to apply paragraph level attributes as opposed to span level
		
		the functions are not tested
		
		*/
		
		//paragraph related functions
		
		private function applyFormatToParagraphs(action:String, value:*):void{
			var paragraphStyle:TextLayoutFormat = new TextLayoutFormat();
			performActionOnParagraphs(action, value, paragraphStyle);
		}
		
		private function performActionOnParagraphs(action:String,value:*, paragraphStyle:TextLayoutFormat):void{
			
			switch(action) {
				
				case ActionMap.VERTICAL_ALIGN_ACTION:
					
					if(value is String){
						paragraphStyle.verticalAlign = getAlignmentValue(value);
					}
					
					break;
				
				case ActionMap.TEXT_ALIGN_ACTION:

					if(value is String){
						paragraphStyle.textAlign = getAlignmentValue(value);
					}
					
					break;
		}//outer switch
		}
		
		public function applyStyleToAllParagraphs(action:String, value:*, targetTextFlow:TextFlow):void{
			
			   var paragraphElement:ParagraphElement = new ParagraphElement(); 
			   paragraphElement = ParagraphElement(targetTextFlow.getParagraph()); 
			
				if(paragraphElement == null){ //could be empty text box
					return;
				}
			   
			   applyParagraphStyle(action, value, paragraphElement);
			           
			   while(paragraphElement = ParagraphElement(paragraphElement.getNextParagraph())){ 
					applyParagraphStyle(action, value,paragraphElement);
			        } 
			
			targetTextFlow.flowComposer.updateAllControllers();
			        
		}
		
		private function applyParagraphStyle(action:String,value:*, paragraph:ParagraphElement):void{
		
			
			switch(action) {
				
				case ActionMap.VERTICAL_ALIGN_ACTION:
					value = uint(value);
					paragraph.verticalAlign = value as uint;
					break;
				
				case ActionMap.TEXT_ALIGN_ACTION:
					value = uint(value);
					paragraph.textAlign = value as uint;
					break;
			}
		}
		
		public function applyStyleName(styleName:String):void{
			
			//Step One:  Force a new span with a tag
			
			var charStyle:TextLayoutFormat =  new TextLayoutFormat();
				charStyle.locale = "tag";
				
				if(_textFlow && _textFlow.interactionManager){
					var selectionState:SelectionState = _textFlow.interactionManager.getSelectionState();
					selectionState.pointFormat = charStyle;
					
					if(this.isSelected){
						(_textFlow.interactionManager as EditManager).applyLeafFormat(charStyle);
					}
					
					
					
				//Step Two: Now find the tagged span and apply a new stylename
					
					 var leaf:SpanElement = new SpanElement(); 
					 leaf = SpanElement(_textFlow.getFirstLeaf()); 
					 if (leaf.locale == "tag"){
						leaf.styleName = styleName;
						leaf.locale = "en";
					 }
					           
					 while(leaf = SpanElement(leaf.getNextLeaf())) { 
						if (leaf.locale == "tag"){
							leaf.styleName = styleName;
							leaf.locale = "en";
						}
					  } 
					
					//let richtextfield do the flow update
				}
		}
			
		
		
		private function createDefaultActionMap():void{
			
			_actionMap.put(ActionMap.FOREGROUNDCOLOR_ACTION,applyFormat);
			_actionMap.put(ActionMap.ITALIC_ACTION,applyFormat);
			_actionMap.put(ActionMap.BOLD_ACTION, applyFormat);
			_actionMap.put(ActionMap.UNDERLINE_ACTION, applyFormat);
			_actionMap.put(ActionMap.STRIKETHROUGH_ACTION, applyFormat);
			_actionMap.put(ActionMap.FONTSIZE_ACTION, applyFormat);
			_actionMap.put(ActionMap.FONTALPHA_ACTION, applyFormat);
			_actionMap.put(ActionMap.FONTNAME_ACTION, applyFormat);
			_actionMap.put(ActionMap.TEXT_ALIGN_ACTION, applyFormatToParagraphs);
			_actionMap.put(ActionMap.VERTICAL_ALIGN_ACTION, applyFormatToParagraphs);
			_actionMap.put(ActionMap.STYLENAME_ACTION,applyFormat);
			
		}
		
		public function get isSelected():Boolean{
			
			if(_textFlow && _textFlow.interactionManager){
				var selectionState:SelectionState = _textFlow.interactionManager.getSelectionState();
				return selectionState ? true:false;
			}
				
			else{
				return false;
			}
		}
		
		public function get isRangeSelected():Boolean{
			
			if(_textFlow && _textFlow.interactionManager){
				var selectionState:SelectionState = _textFlow.interactionManager.getSelectionState();
				return selectionState.absoluteStart != selectionState.absoluteEnd ? true:false;
			}
			else{
				return false;
			}
		}
		
		private function selectionChanged(selectionEvent:SelectionEvent):void
		{
			_selectedFormat= _textFlow.interactionManager.getCommonCharacterFormat() as TextLayoutFormat;
			
		}
		
		public function clearListeners():void{
			
			_textFlow.removeEventListener(SelectionEvent.SELECTION_CHANGE, selectionChanged);
		}
		
		private function getAlignmentValue(value:String):String{
			
			switch (value){
				
				case "left":
					return TextAlign.LEFT;
					break;
				
				case "center":
					return TextAlign.CENTER;
					break;
				
				case "right":
					return TextAlign.RIGHT;
					break;
				
				case "top":
					return VerticalAlign.TOP;
					break;
				
				case "middle":
					return VerticalAlign.MIDDLE;
					break;
				
				case "bottom":
					return VerticalAlign.BOTTOM;
					break;
				
			}// inner switch
			
			return "";
		}
		
		private function selectLookupType(fontfamily:String):String{
			
			//all fonts are now embedded
			return FontLookup.EMBEDDED_CFF;
			/*
			if(fontfamily == Fonts.DEFAULT_FAMILY || fontfamily == Fonts.DEFAULT_FAMILY_CONDENSED || fontfamily == Fonts.DEFAULT_SANS_FONT || Fonts.DEFAULT_SERIF_FONT){
				return FontLookup.EMBEDDED_CFF;
			}
			else{
				return FontLookup.DEVICE;
			}
			*/
			
		}
		
	}
}