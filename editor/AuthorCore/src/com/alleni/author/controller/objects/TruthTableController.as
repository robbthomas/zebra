package com.alleni.author.controller.objects
{
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.TruthTable;

public class TruthTableController extends TableController
	{          
		public static var instance:TruthTableController = new TruthTableController();

		public function TruthTableController()
		{
			super();
		}

		override protected function resizeLogic(table:AbstractTable):void {
			super.resizeLogic(table);
			// number of columns is 2 ** N (2 raised to N power, where N = number of ports)
			table.numColumns = 1 << table.port.length;
            table.recordInitialValue("numColumns");
            table.updateForAssetChange();  // enlarged table by wiring to next truth port
		}

		override public function handleLogicTruthChanged(table:AbstractTable, row:int, truth:Boolean, startingColumn:int = 0):void {
			setColumn(table as TruthTable);
		}

        public function setColumn(table:TruthTable):void {
			var newCol:int = 0;
			for(var portNum:int = 0; portNum < table.truthPort.length; portNum++) {
				if(table.truthPort[portNum]) {
					newCol |= (1 << portNum);
				}
			}
			table.column = newCol + 1;
        }
	}
}
