package com.alleni.author.controller.objects
{
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.action.ActionGroup;
import com.alleni.author.definition.action.ModifyRibbonAction;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.AbstractExternalObject;
	import com.alleni.author.model.objects.Drawing;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.taconite.model.TaconiteModel;

	public class DrawingController extends ExternalObjectController
	{
		public static var instance:DrawingController = new DrawingController();
		
		protected static function getDrawing(model:TaconiteModel):Drawing
		{
			return model.value as Drawing;
		}
		
		override protected function resizeForNewAsset(object:AbstractExternalObject, assetWidth:Number, assetHeight:Number):void
		{
			var drawing:Drawing = object as Drawing;
			
			if (drawing.resizeImage == "native") {
				drawing.width = assetWidth;
				drawing.height = assetHeight;
			}
		}

        override public function assetDroppedOnRibbon(obj:AbstractObject, anchor:WireAnchor, asset:IReplaceable):void
        {
            if (anchor.path == "replaceable") {
                var object:AbstractExternalObject = obj as AbstractExternalObject;
                var oldWidth:Number = object.width;
                var oldHeight:Number = object.height;
                var oldAsset:Asset = anchor.value as Asset;

                anchor.value = asset;   // resizeForNewAsset will adjust height & width: undo will capture values below

                var title:String = "Set graphic on " + obj.title;
                var group:ActionGroup = new ActionGroup(title);
                group.add(ModifyRibbonAction.fromObject(object, "width", oldWidth));
                group.add(ModifyRibbonAction.fromObject(object, "height", oldHeight));
                group.add(ModifyRibbonAction.fromObject(object, anchor.path, oldAsset));
                ApplicationController.currentActionTree.commit(group);
            } else {
                super.assetDroppedOnRibbon(obj, anchor, asset); // in case of drop into setter ribbon
            }
        }

	}
}