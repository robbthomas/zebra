package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.model.objects.Slider;
	import com.alleni.author.view.objects.SliderView;
	import com.alleni.taconite.event.ModelUpdateEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class SliderMediator
	{
		protected var _context:ViewContext;
		private var _view:SliderView;
		private var _knob:Sprite;
		private var _knobOriginalPoint:Point = new Point(0,0);  // original mouse loc, before sliding it
		private var _clickPoint:Point = new Point(0,0);
		private var _role:uint;
		private var _model:TaconiteModel;
		private var _slider:Slider;

		
		public function SliderMediator(context:ViewContext)
		{
			_context = context;
		}
		
		public function handleViewEvents(view:SliderView, knob:Sprite, role:uint):void
		{
			_view = view;
			_knob = knob;
			_role = role;
			_model = view.model;
			_slider = _model.value as Slider;
			
			_knob.doubleClickEnabled = true;
			_knob.addEventListener(MouseEvent.MOUSE_DOWN,mouseDownListener);
			_knob.addEventListener(MouseEvent.DOUBLE_CLICK, doubleClickListener);
			_model.addEventListener(ModelUpdateEvent.MODEL_UPDATE, modelUpdateListener, false, 3);
		}
		
		public function clearViewEvents() : void
		{
			_knob.removeEventListener(MouseEvent.MOUSE_DOWN,mouseDownListener);
			_knob.removeEventListener(MouseEvent.DOUBLE_CLICK, doubleClickListener);
			_model.removeEventListener(ModelUpdateEvent.MODEL_UPDATE, modelUpdateListener);
		}
		
		private function mouseDownListener(e:MouseEvent):void
		{
            if(!_slider.interactive) return;
			_knobOriginalPoint.x = _knob.x;  // local coordinates of knob
			_knobOriginalPoint.y = _knob.y;
			var global:Point = new Point(e.stageX,e.stageY);
			_clickPoint = _view.globalToLocal(global);  // click location, relative to slider body
			
			_view.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
			_view.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);
		}
		
		
		private function mouseMoveListener(e:MouseEvent):void
		{
            if (_view.stage) {
                var global:Point = new Point(e.stageX,e.stageY);
                var local:Point = _view.globalToLocal(global);  // point local to the slider view
                var delta:Point = local.subtract(_clickPoint);
                var newY:Number = _knobOriginalPoint.y + delta.y;
                var knobY:Number = newY + _knob.height / 2;  // center of knob, relative to anchor point
    //			trace("global="+global, "local="+local, "delta="+delta, "newY="+newY, "knobY="+knobY);
                setKnobY(_slider, knobY, true);
            }
		}
		
		private function mouseUpListener(e:MouseEvent):void
		{
			// finish the movement
			mouseMoveListener(e);
			
			if (!_slider.childOfClosed)
				ApplicationController.instance.authorController.selectSingleModel(_model);
			ApplicationController.instance.authorController.log("Slider set value="+_slider.sliderValue + " " + _slider.logStr);
			
			// this view may not be on stage anymore, if changing value wired to arena page-change
			_view.context.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
			_view.context.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);
		}
		
		private function doubleClickListener(event:MouseEvent):void
		{
			_view.dispatchEvent(event);  // bubble to the slider body, so a gadget can be opened by double-click on knob
		}
		
		private function modelUpdateListener(e:ModelUpdateEvent):void
		{
			handleModelUpdate(e.source, e.property as String, e.newValue, e.parent);
		}
		
		protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, parent:TaconiteModel):void
		{	
			if (model == _model && !_slider.loading) {
				switch (property) {
					case "sliderValue":
					case "width":
					case "height":
					case "x":  // because moving the whole slider moves the knob also
					case "y":
					case "rotation":
						updateKnobPositionProperties(model);
						break;
					case "knobX":
					case "knobY":
						if (!_slider.doingKnobPropertyUpdate)  // if we aren't updating knobX ourselves
							handleKnobPropertyChange(model);
						break;
				}
			}
		}

		
		/**
		 * Update the knob position properties knobX and knobY.
		 * These coordinates are relative to the container the slider is in.
		 * @param model
		 * 
		 */
		private function updateKnobPositionProperties(model:TaconiteModel):void
		{
			var slider:Slider = _slider;
			var local:Point = new Point(slider.left + slider.width/2, getKnobY(slider));
			var global:Point = _view.localToGlobal(local);
			var position:Point = _view.parent.globalToLocal(global);  // relative to parent
//			trace("updateKnob: values for ribbon",position,  "getKnobY="+getKnobY(slider));
			
			slider.doingKnobPropertyUpdate = true;  // suppress binding action from bringing it back in to affect "sliderValue" property
			slider.knobX = position.x;
			slider.knobY = position.y;
			slider.doingKnobPropertyUpdate = false; 
		}
		
		
		/**
		 * the knobX or knobY property has changed:  change "sliderValue" to match. 
		 * @param slider
		 * 
		 */
		private function handleKnobPropertyChange(model:TaconiteModel):void
		{
			var slider:Slider = _slider;
			var position:Point = new Point(slider.knobX,slider.knobY);  // relative to parent container
			var global:Point = _view.parent.localToGlobal(position);
			var local:Point = _view.globalToLocal(global);
//			trace("knobPropChange: position="+position, "local="+local);
			setKnobY(slider,local.y);  // change "sliderValue" which eventually causes the knob to move, contrained by max, units, etc
		}
		
		
		/**
		 * get and set the position of the knob: actual position used by the view, relative to anchor point.
		 * (not the value displayed to the user in the ribbon) 
		 * @param slider
		 * @return 
		 * 
		 */
		public function getKnobY(slider:Slider):Number {
			
			var range:Number = slider.sliderMaximum - slider.sliderMinimum;
			var pixelRange:Number = slider.height * (1 - 2 * SliderView.INDENT_FRAC);
			var yBase:Number = computeYBase();  // location of bottom of scale
			if (range == 0) {
				return yBase;
			} else {
				var yOffset:Number = (slider.sliderValue - slider.sliderMinimum) * pixelRange / range;  // positive going up screen
				//trace("SliderController::getKnobY: y="+(yBase-yOffset),"yOffset="+yOffset,"value="+slider.value);
				return yBase - yOffset;
			}
		}
		
		public function setKnobY(slider:Slider,yy:Number,limit:Boolean=false):void
		{
			var range:Number = slider.sliderMaximum - slider.sliderMinimum;
			var pixelRange:Number = slider.height * (1 - 2 * SliderView.INDENT_FRAC);
			if (pixelRange == 0) {
				slider.sliderValue = slider.sliderMinimum;
			} else {
				var yBase:Number = computeYBase();  // location of bottom of scale
				var yOffset:Number = yBase - yy;  // positive going up screen
                var newSliderValue:Number = slider.sliderMinimum +  yOffset * range / pixelRange;
                if (limit) { // should we limit the slider's value to within its range??
                    if (slider.sliderMinimum < slider.sliderMaximum) {
                        newSliderValue = Math.max(slider.sliderMinimum, newSliderValue);
                        newSliderValue = Math.min(slider.sliderMaximum, newSliderValue);
                    } else if (slider.sliderMinimum > slider.sliderMaximum) {
                        newSliderValue = Math.max(slider.sliderMaximum, newSliderValue);
                        newSliderValue = Math.min(slider.sliderMinimum, newSliderValue);
                    } else {
                        newSliderValue = slider.sliderMinimum;
                    }
                }
				slider.sliderValue = newSliderValue;
			}
			//trace("SliderController::setKnobY: y="+yy,"yOffset="+yOffset,"value="+slider.value);
		}
		
		private function computeYBase():Number
		{
			return _slider.bottom - _slider.height * SliderView.INDENT_FRAC;  // location of bottom of scale
		}
	}
}