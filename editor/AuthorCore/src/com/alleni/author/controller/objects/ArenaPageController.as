/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/7/12
 * Time: 11:16 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.objects {
import com.alleni.author.application.ApplicationUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ObjectDragMediator;
import com.alleni.author.controller.objects.layout.GridLayout;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.ui.Application;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.objects.ArenaPageView;
import com.alleni.author.view.objects.ArenaView;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.ModelCollectionEvent;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.model.TaconiteModel;

import flash.display.Stage;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.utils.Dictionary;

public class ArenaPageController extends ContainerController {

    public static var instance:ArenaPageController = new ArenaPageController();


    public function ArenaPageController() {
    }

    override public function initializeObject(model:TaconiteModel) : void
    {
        var page:ArenaPage = getPage(model);
        if (!page.controlled) {
            model.addEventListener(ModelEvent.MODEL_CHANGE, modelChangeListener, false, 0, true);
        }
        super.initializeObject(model);
    }

    override public function warnBeforeDelete(object:AbstractObject):void
    {
        super.warnBeforeDelete(object);
        object.model.removeEventListener(ModelEvent.MODEL_CHANGE, modelChangeListener, false);
    }

    private function getPage(model:TaconiteModel):ArenaPage
    {
        return model.value as ArenaPage;
    }

    private function modelChangeListener(e:ModelEvent):void
    {
        if (!(e is ModelCollectionEvent)) { // simply a ModelEvent
            var model:TaconiteModel = e.parent as TaconiteModel;
            var par:AbstractObject = model.value as AbstractObject;
            if (par.loading == false) {
                if (par as ArenaPage) {
                    handlePageModelChange(par as ArenaPage, e.child.value as AbstractObject, e.kind);
                }
            }
        }
    }

    private function handlePageModelChange(page:ArenaPage, child:AbstractObject, kind:String):void
    {
        if (!page.doingLayerReorder && !page.bindingsDisabled) {
            switch (kind) {
                case ModelEvent.ADD_CHILD_MODEL:
                    acceptChildToPage(page, child);
                    break;
                case ModelEvent.REMOVE_CHILD_MODEL:
                    releaseChildFromPage(page,  child);
                    break;
            }
        } else if (page.hadChildren == false && page.objects.length > 0 && !child.dragging) {
            page.hadChildren = true;    // notice obj pasted into arena, and remove the empty-page graphic
        }
    }

    private function acceptChildToPage(page:ArenaPage, obj:AbstractObject):void
    {
        if (page.loading)
            return;
        if (obj.parent == page) {
            // NOTE: this is called multiple times when an obj is dragged in
//                trace("ACCEPT child="+obj.title, "arena="+arena.title);
//            testStuff(page,  obj);

            if (obj.dragging) {
                if (page.transientObjsDraggedIn == null)
                    page.transientObjsDraggedIn = new Dictionary();
                page.transientObjsDraggedIn[obj.uid] = true;
                var arena:Arena = page.parent as Arena;
                if (arena) {
                    arena.transientPauseWhileDragging = true;  // pause running flipbook until mouseUp
                }
            }

            page.objCount = page.objects.length;
            sendArenaOutletMessage(page, "objAdded");

            if (!page.hadChildren && !arenaCopyingMyself(page, obj)) {
                page.hadChildren = true;
            }

            // schedule recalc for render event, since child view may not exist yet
            if(!page.loading && page.getView()) {
                requestLayerReorder(page);
                    requestDelayedRecalcLayout(page, true); // animate=true
            }
        }
    }

    private function releaseChildFromPage(page:ArenaPage, obj:AbstractObject):void
    {
        if (page.isMaster) {
            return;
        }

        page.objCount = page.objects.length;
        setArenaDragHilite(page, false);
        sendArenaOutletMessage(page, "objRemoved");

        // schedule recalc for render event, since child view may not exist yet
        ArenaLayoutManager.instance.removeInsertFeedBack();
        requestRecalcLayout(page);
    }


    override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
    {
        super.handleModelUpdate(model,property,newValue,oldValue,parent);

        if (model.value is ArenaPage) {  // page itself is changing
            var page:ArenaPage = model.value as ArenaPage;
            if (!page.bindingsDisabled && !page.loading){
                switch (property) {
                    case "marginAll":
                    case "marginLeft":
                    case "marginRight":
                    case "marginTop":
                    case "marginBottom":
                    case "hGap":
                    case "vGap":
                    case "width":
                    case "height":
                        requestRecalcLayout(page,false); // doing without animation for performance
                        break;
                    case "alignCellsHorz":
                    case "alignCellsVert":
                    case "alignObjsHorz":
                    case "alignObjsVert":
                    case "layout":
                        requestRecalcLayout(page);
                        break;
                    case "doingLayerReorder":
                        if (newValue == false) {
                            requestRecalcLayout(page);
                        }
                        break;
                    case "columns":
                        if(!page.growHorizontal) {
                            requestRecalcLayout(page);
                        }
                        break;
                }
            }
        }

        if (model.parent && model.parent.value is ArenaPage) {  // a page child is changing
            var obj:AbstractObject = model.value as AbstractObject;
            var page:ArenaPage = obj.parent as ArenaPage;
            if (!page.bindingsDisabled){
                  //trace("Arena child change: prop="+property, "val="+newValue, "child="+obj.title);
                switch (property) {
                    case "visible":
                    case "alpha":
                        if (newValue != oldValue) { // resumeRunningState fires a change event with same value
                            requestRecalcLayout(page,true);
                        }
                        break;
                    case "alignAreaHorz":
                    case "alignAreaVert":
                        requestRecalcLayout(page,true,true);
                        break;
                    case "width":  // do not watch for x,y here -- wait for childEndDrag.
                    case "height":
                    case "rotation":
                    case "textAlignV":
                    case "textAlignH":
                    case "label":
                    case "textWidth":
                    case "labelHeight":
                    case "labelPosition":
                    case "cloakedNow":
                        requestDelayedRecalcLayout(page,false); // only the model has changed: delay recalc until child view is updated
                        break;
                    case "draggingHandles":
                    case "editingText":
                        if(!newValue) requestRecalcLayout(page,false); // doing without animation for performance
                        break;
                    case "scale":
                    case "contentUpdated":  // bitmap or SVG
                    case "assetsLoaded":  // ToggleButton
                        requestRecalcLayout(page,false);
                        break;
                    case "excludeFromContainerLayouts": // while drawing a path
                        if (newValue == false) {
                            requestRecalcLayout(page);
                        }
                        break;
                    case "dragging":
                        if (newValue == false && page.transientObjsDraggedIn && obj.uid in page.transientObjsDraggedIn) {
                            trace("DRAGGING FALSE",obj);
                            page.transientObjsDraggedIn = null;  // also done elsewhere
                        }
                        break;
                }
            }
        }
    }

    private function requestDelayedRecalcLayout(page:ArenaPage, animate:Boolean):void
    {
        // test cases:
        // change child width via inspector, or by MC ribbon.
        // option-drag object from outside and drop in grid.
        if (!page.loading) {
            var counter:int = 0;
            ApplicationUI.instance.stage.addEventListener(Event.ENTER_FRAME, function(e:Event):void{
                if (++counter >= 2) {
                    ApplicationUI.instance.stage.removeEventListener(Event.ENTER_FRAME, arguments.callee);
                    requestRecalcLayout(page, animate);
                }
            });
        }
    }

    public function requestRecalcLayout(obj:AbstractContainer, animate:Boolean=true, force:Boolean=false):void
    {
        var page:ArenaPage = obj as ArenaPage;
        if(!page.loading && !page.transientDraggingChild)
        {
            var pageView:ArenaPageView = page.getView() as ArenaPageView;
            if (pageView) {
                ArenaLayoutManager.instance.requestRecalcLayout(pageView, animate, force);
            }
        }
    }



    public function handleRandomizeZIndex(page:ArenaPage):void
    {
        var objs:Array = page.objects.toArray();
        var i:int = 0;
        for each(var m1:AbstractObject in objs)
        {
            page.model.moveChildOutofList(m1.model);
        }
        objs.sort(randomSort);
        for each(var m3:AbstractObject in objs)
        {
            page.model.moveChildIntoList(m3.model,i++);
        }
        page.setInitialZIndex(false);

        requestRecalcLayout(page,true,true);
    }

    private function randomSort(elementA:Object, elementB:Object):Number {
        return Math.round(Math.random());
    }


		/**
		 * Overriding to provide startDrag features for Grid Layouts.
		 * @param e
		 * @param selectedChild
		 *
		 */
		override public function childStartDrag(e:MouseEvent, selectedChild:ObjectView):void
		{
            var selected:AbstractObject = selectedChild.object;
            trace("childStartDrag --- obj="+selected);
            var page:ArenaPage = selected.parent as ArenaPage;
			var arena:Arena = page.parent as Arena;
            arena.transientPauseWhileDragging = true;
            if (page.layout == ArenaLayoutManager.NONE || selected.snapTo)
                return;

            childDragBegin(page, selected);
        }

        private function childDragBegin(page:ArenaPage, selected:AbstractObject):void
        {
            if (!page.transientDraggingChild) {
                // force an immediate recalc & save cells array
                var pageView:ArenaPageView = page.getView() as ArenaPageView;
                GridLayout.instance.prepareDragging(pageView);
                page.transientDraggingChild = true;  // prevent recalcLayout during drag
                page.transientAuthorDrag = !selected.movable && Application.uiRunning;  // GridLayout wants this for controlling inits
                page.transientOverIndex = -1;
                if (!ObjectDragMediator.copyingNow) {  // copied object will naturally be at front
                    AuthorController.instance.bringFrontWithoutUndo();
                }

                // listen for mouseUp in case user drags obj outside this arena
                var stage:Stage = ApplicationUI.instance.stage;
                stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void{
                    stage.removeEventListener(MouseEvent.MOUSE_UP, arguments.callee);
                    finishChildDrag(page);
                });
            }
		}

		/**
		 * Overriding to provide moveDrag features for Grid Layouts.
		 * @param e
		 * @param selectedChild
		 *
		 */
		override public function childMoveDrag(e:MouseEvent, selectedChild:ObjectView):void
		{
            var selected:AbstractObject = selectedChild.object;
            var page:ArenaPage = selected.parent as ArenaPage;

            setArenaDragHilite(page, true);
            if (page.layout == ArenaLayoutManager.NONE || selected.snapTo)
                return;

            var pageView:ArenaPageView = page.getView() as ArenaPageView;
            childDragBegin(page, selected);  // when dragging an object from outside into arena, we don't get a "childStartDrag" call

            trace("childMoveDrag ---");
            var point:Point = pageView.childViewContainer.globalToLocal(new Point(e.stageX, e.stageY));
            var n:int = GridLayout.instance.cellIndexForInsertion(page, point);
            if(n >= 0) {
                ArenaLayoutManager.instance.drawInsertFeedBack(page, n, selectedChild);
                page.transientOverIndex = n;
            } else {
                ArenaLayoutManager.instance.removeInsertFeedBack();
            }

		}

		/**
		 * Overriding to provide endDrag features for Grid Layouts.
		 * @param e
		 * @param selectedChild
		 *
		 */
		override public function childEndDrag(e:MouseEvent, selectedChild:ObjectView):void
		{
            super.childEndDrag(e, selectedChild);

            trace("childEndDrag ---");
            var selected:AbstractObject = selectedChild.object;
            var page:ArenaPage = selected.parent as ArenaPage;
            if (page == null)
                return;
            setArenaDragHilite(page, false);
            if (page.layout == ArenaLayoutManager.NONE || selected.snapTo) {
                finishChildDrag(page);
                return;
            }

			if (page.transientOverIndex >= 0) {
                var index:int = GridLayout.instance.cellIndexToChildIndex(page, page.transientOverIndex);
                selected.parent.setChildIndexWithinLayer(selected, index);
                selected.parent.setChildrenZPositions(!Application.running || !selected.movable);
			}

			ArenaLayoutManager.instance.removeInsertFeedBack();
            finishChildDrag(page);
		}

        private function finishChildDrag(page:ArenaPage):void
        {
            trace("finishChildDrag ---", page.transientDraggingChild)
            if (page.transientDraggingChild) {
                page.transientDraggingChild = false;
                page.transientObjsDraggedIn = null;
                if (page.layout == ArenaLayoutManager.GRID)
                    GridLayout.instance.finishDragging(page);
            }
            requestRecalcLayout(page);
        }


        // ******* Arena functions **********

        private function setArenaDragHilite(page:ArenaPage, value:Boolean):void
        {
            var arena:Arena = page.parent as Arena;
            if (arena) {
                var view:ArenaView = arena.getView() as ArenaView;
                if (view && !Application.uiRunning) {
                    view.dragHilite = value;
                }
            }

        }

        private function sendArenaOutletMessage(page:ArenaPage, key:String):void
        {
            var arena:Arena = page.parent as Arena;
            if (arena.editingLocked || !(arena.visible || Application.instance.showAll))
                return;

            if (arena && arena.locked == false && (arena.visible || Application.instance.showAll)) {
                _wiring.sendTrigger(arena, Modifiers.instance.fetch(key));
            }
        }

        private function arenaCopyingMyself(page:ArenaPage, child:AbstractObject):Boolean
        {
            // when drag-copy a blank arena, ensure that the original keeps its gray fill, even tho the copy is dragged across & falls into it for a moment
            var arena:Arena = page.parent as Arena;
            if (arena) {
                return (ObjectDragMediator.copyingNow && child is Arena && child.width == arena.width && child.height == arena.height);
            }
            return false;
        }

        private function testStuff(page:ArenaPage, obj:AbstractObject):void
        {
            trace("-");
            trace("testStuff")
            var path:Array = world.objectPathArray(obj);
            var pp:AbstractObject = world.resolveObjectPathArray(path);
            trace("path: obj="+obj, "parent="+obj.parent, "path="+path.join(","), "match="+(pp == obj));
            var scope:AbstractContainer = obj.wireScopeContainer;
            trace("scope="+scope);

            var ao:AbstractObject;
            var all:Vector.<AbstractObject> = world.allObjects();
            trace("ALL:  count="+all.length);
            for each (ao in all) {
                path = world.objectPathArray(ao);
                trace("   ",ao, "path="+path.join(","));
            }

            trace("IN SCOPE:",scope);
            var counter:int = 0;
            scope.allObjectsInScope(function(one:AbstractObject):Boolean{
                path = world.objectPathArray(one);
                trace("   ["+(counter++)+"]", one, "path="+path.join(","));
                return true;
            });

            trace("IN WORLD SCOPE:");
            var counter:int = 0;
            world.allObjectsInScope(function(one:AbstractObject):Boolean{
                path = world.objectPathArray(one);
                trace("   ["+(counter++)+"]", one, "path="+path.join(","));
                return true;
            });
        }


        private function get world():World
        {
            return Application.instance.document.root.value as World;
        }

    }
}
