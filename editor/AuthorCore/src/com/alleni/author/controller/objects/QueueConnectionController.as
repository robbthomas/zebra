/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/5/11
 * Time: 9:50 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.objects {
import com.alleni.author.controller.ObjectController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.model.objects.QueueConnection;
import com.alleni.taconite.model.ITaconiteObject;

public class QueueConnectionController extends ObjectController {
	public static var instance:QueueConnectionController = new QueueConnectionController();
	public function QueueConnectionController() {
	}

	override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void {
		super.handleTrigger(object, modifier);
		var q:QueueConnection = object as QueueConnection;
		switch(modifier.key) {
			case "send":
				q.send();
				break;
			case "receive":
				q.receive();
				break;
		}
	}
}
}
