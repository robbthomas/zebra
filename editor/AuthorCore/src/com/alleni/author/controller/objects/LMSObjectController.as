package com.alleni.author.controller.objects
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.model.objects.LMS;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.document.Document;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.taconite.event.ApplicationEvent;
    import com.alleni.taconite.service.LogService;
    import com.alleni.author.model.ui.WireAnchor;

	import flash.utils.Dictionary;
	import flash.events.Event;

	public class LMSObjectController extends ObjectController
	{
		public static var instance:LMSObjectController = new LMSObjectController();

		private static var _environment:IEnvironment;
		private static var _metadataUpdateHandlers:Dictionary; 		// of Function
		private static var _lrsInitializeHandlers:Dictionary; 		// of Function
		private static var _lrsRequestSucceededHandlers:Dictionary; // of Function
		private static var _lrsRequestFailedHandlers:Dictionary; 	// of Function

	    public function LMSObjectController() {
       		_metadataUpdateHandlers = new Dictionary(false);
       		_lrsInitializeHandlers = new Dictionary(false);
       		_lrsRequestSucceededHandlers = new Dictionary(false);
       		_lrsRequestFailedHandlers = new Dictionary(false);
	    }

        override public function initializeObject(model:TaconiteModel):void
        {
        	super.initializeObject(model);
        	var l:LMS = model.value as LMS;
        	updateEnvironmentMetadata(l);

        	_metadataUpdateHandlers[l] = handleNewEnvironmentMetadata(l);
            ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_SETTINGS_CHANGED, _metadataUpdateHandlers[l]);

            _lrsInitializeHandlers[l] = handleLRSInitialized(l);
            ApplicationController.addEventListener(NotificationNamesApplication.LRS_INITIALIZED, _lrsInitializeHandlers[l]);

            _lrsRequestSucceededHandlers[l] = handleLRSRequestSucceeded(l);
            ApplicationController.addEventListener(NotificationNamesApplication.LRS_REQUEST_SUCCEEDED, _lrsRequestSucceededHandlers[l]);

            _lrsRequestFailedHandlers[l] = handleLRSRequestFailed(l);
            ApplicationController.addEventListener(NotificationNamesApplication.LRS_REQUEST_FAILED, _lrsRequestFailedHandlers[l]);
        }

		override public function warnBeforeDelete(object:AbstractObject):void
		{
			super.warnBeforeDelete(object);
			if (object in _metadataUpdateHandlers) {
	            ApplicationController.removeEventListener(NotificationNamesApplication.PROJECT_SETTINGS_CHANGED, _metadataUpdateHandlers[object]);
				delete _metadataUpdateHandlers[object];
	        }
			if (object in _lrsInitializeHandlers) {
	            ApplicationController.removeEventListener(NotificationNamesApplication.LRS_INITIALIZED, _lrsInitializeHandlers[object]);
				delete _lrsInitializeHandlers[object];
	        }
			if (object in _lrsRequestSucceededHandlers) {
	            ApplicationController.removeEventListener(NotificationNamesApplication.LRS_REQUEST_SUCCEEDED, _lrsRequestSucceededHandlers[object]);
				delete _lrsRequestSucceededHandlers[object];
	        }
			if (object in _lrsRequestFailedHandlers) {
	            ApplicationController.removeEventListener(NotificationNamesApplication.LRS_REQUEST_FAILED, _lrsRequestFailedHandlers[object]);
				delete _lrsRequestFailedHandlers[object];
	        }
		}

        private function handleNewEnvironmentMetadata(l:LMS):Function {
        	return function (event:Event=null):void {
        		updateEnvironmentMetadata(l);
	        }
        }

        private function updateEnvironmentMetadata(l:LMS):void {
        	l.activity_id = defaultActivityId;
        	l.activity_name = defaultActivityName;
        	l.activity_description = defaultActivityDescription;
        	l.actor_name = defaultActorName;
        }

        private function handleLRSInitialized(l:LMS):Function {
        	return function (event:ApplicationEvent):void {
	            WireAnchor(l.anchors["statementSuccess"]).trigger();

	        	var responseObj = event.data;
	        	if ("initialized" in responseObj) {
	        		if (responseObj.initialized && "lrs" in responseObj && "endpoint" in responseObj.lrs) {
		        		LogService.info("Initialization succeeded for LRS: " + responseObj.lrs.endpoint);
		        		if (responseObj.lrs.endpoint == l.lrs_endpoint) {
		        			l.lrs_active = true;
		        		}
        			} else if (!responseObj.initialized && "lrs" in responseObj && "endpoint" in responseObj.lrs) {
		        		LogService.info("Initialization failed for LRS: " + responseObj.lrs.endpoint);
        			} else {
		        		LogService.info("handleLRSInitialized general failure. No LRS ID in response object.");
        			}
	        	}
	        	if ("lrs" in responseObj && "apiVersion" in responseObj.lrs) {
	        		LogService.info("Initialization setting LRS apiVersion: " + responseObj.lrs.apiVersion);
	        		l.lrs_api_version = responseObj.lrs.apiVersion;
	        	}
	        }
        }

        private function handleLRSRequestSucceeded(l:LMS):Function {
        	return function (event:ApplicationEvent):void {
	            WireAnchor(l.anchors["statementSuccess"]).trigger();

	        	var responseObj = event.data;
	        	if (responseObj != null) {
		        	if ("lrs" in responseObj && "endpoint" in responseObj.lrs) {
		        		LogService.info("handleLRSRequestSucceeded LRS endpoint: " + responseObj.lrs.endpoint);
		        	}
		        	if ("url" in responseObj) {
		        		LogService.info("handleLRSRequestSucceeded LRS url: " + responseObj.url);
		        	}
	        	}
	        }
	    }

        private function handleLRSRequestFailed(l:LMS):Function {
        	return function (event:ApplicationEvent):void {
	            WireAnchor(l.anchors["statementFailure"]).trigger();

	        	var responseObj = event.data;
	        	if (responseObj != null) {
		        	if ("lrs" in responseObj && "endpoint" in responseObj.lrs) {
		        		LogService.info("handleLRSRequestFailed LRS endpoint: " + responseObj.lrs.endpoint);
		        	}
		        	if ("url" in responseObj) {
		        		LogService.info("handleLRSRequestFailed LRS url: " + responseObj.url);
		        	}
		        }
	        }
	    }

		private static function get environment():IEnvironment {
			if (_environment == null) {
				_environment = TaconiteFactory.getEnvironmentImplementation();
			}
			return _environment;
		}

	    override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void {
	        super.handleModelUpdate(model, property, newValue, oldValue, parent);
	        var l:LMS = model.value as LMS;
	        switch(property) {
	            case "loading":
	                if (newValue == false) {
	                    l.active = true;
	                }
	                break;
	            case "activity_name":
	            case "activity_type":
	            case "actor_name":
	            case "statement_verb":
	            case "statement_object":
	            case "statement_include_module":
	            case "statement_include_score":
	            case "statement_include_success":
	            case "statement_include_completion":
	            case "result_success":
	            case "result_completion":
	            case "module_index":
	            case "score":
	            case "score_percent":
	            case "score_sendAs":
	            	l.updateStatementPreview();
	                break;
	            default:
	            	// nothing to do
	            	break;
	        }
	    }

		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void {
			super.handleTrigger(object, modifier);
			var l:LMS = object as LMS;
			switch(modifier.key) {
				case "initiateActivity":
					l.initiateExperienceActivity();
					break;
				case "sendStatement":
					l.sendExperienceStatement();
					break;
				case "updateModule":
					l.sendModuleExperienceStatement();
					break;
				case "sendValues":
					l.sendValues();
					break;
				case "score_nudgeUp":
					l.nudgeScoreUp();
					break;
				case "score_nudgeDown":
					l.nudgeScoreDown();
					break;
			}
		}

		private static function get defaultActivityId():String {
			var activityId = environment.apiURI;
			var document:Document = Application.instance.document;
			if (document != null && document.documentId != null) {
				activityId += document.documentId;
			}
			return activityId;
		}

		private static function get defaultActivityName():String {
			var document:Document = Application.instance.document;
			if (document != null && document.project != null)
				return GadgetDescription.cleanName(document.project, true);
			return "";
		}

		private static function get defaultActivityDescription():String {
			var document:Document = Application.instance.document;
			if (document != null && document.project != null)
				return document.project.description;
			return "";
		}

		private static function get defaultActorName():String {
			return environment.userFullName;
		}
	}
}
