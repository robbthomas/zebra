/**
 * ¬¨¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 1/30/11
 * Time: 8:46 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.objects {
import com.alleni.author.application.ApplicationUI;
import com.alleni.author.application.palettes.PathEditingPanel;
import com.alleni.author.application.palettes.PolygonEditingPanel;
import com.alleni.author.application.palettes.ToolboxPanel;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.MarqueeMediator;
import com.alleni.author.controller.app.ClipboardController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.palettes.InspectorController;
import com.alleni.author.controller.ui.palettes.ToolboxController;
import com.alleni.author.definition.DoubleClickAction;
import com.alleni.author.definition.DragInfo;
import com.alleni.author.definition.DropIntoContainerEvent;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.definition.action.CreateObjectAction;
import com.alleni.author.definition.action.CustomRibbonAction;
import com.alleni.author.definition.action.DestroyObjectAction;
import com.alleni.author.definition.action.ObjectExistenceAction;
import com.alleni.author.definition.action.PathEditAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.event.AnchorEvent;
import com.alleni.author.event.PathEvent;
import com.alleni.author.event.ToolEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.Path;
import com.alleni.author.model.objects.PathNodeInfo;
import com.alleni.author.model.objects.PathObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.PathNodeWireAnchor;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.WorldView;
import com.alleni.author.view.feedback.PathFeedback;
import com.alleni.author.view.feedback.PathNodeHandle;
import com.alleni.author.view.objects.PathObjectView;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.author.view.ui.PortView;
import com.alleni.author.view.ui.RibbonView;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.ui.Keyboard;
import flash.utils.Dictionary;

import mx.core.UIComponent;
import mx.events.PropertyChangeEvent;

public class PathEditingMediator implements IEditingMediator, IUndoMediator, IClipboardMediator
	{
		private const _MENULABEL_TYPE:String = "Path";

        private var _path:PathObject;
		private var _view:PathObjectView;
		private static var _context:ViewContext;
		private var _feedback:PathFeedback;
		private var _authorController:AuthorController;
		private var _actionTree:ActionTree;
		private var _oldPath:Path;
		private var _oldPosition:Point;
		private var _oldAnchor:Point;
		private var _destroyObjectAction:ObjectExistenceAction;
        private var _nodeAnchorsOriginal:Dictionary;  // key=anchor.path
        private var _nodeAnchorActions:Dictionary;  // key=anchor.path, value=CustomRibbonAction
		private var _toolboxPanel:PathEditingPanel;
		private var _showingCreationPanel:Boolean;
		private var _initialDragDraw:Boolean;
		private var _creationSession:Boolean;

		private static const polygonRadius:Number = 300;

		
		public static function mouseDownWithTool(stageX:Number, stageY:Number):AbstractObject
		{
			var controller:AuthorController = ApplicationController.instance.authorController;
			var obj:PathObject = null;
			if (controller.currentEditingMediator is PathEditingMediator == false) {
				obj = controller.addObjectForName("PathObject", 0, 0, 0, 0, false) as PathObject;
                obj.excludeFromContainerLayouts = true;
				obj.model.dispatchEvent(new DropIntoContainerEvent(new Point(stageX, stageY)));
				obj.setInitialZIndex();

				var clickEvent:PathEvent = new PathEvent(PathEvent.INITIAL_CLICK);
				clickEvent.point = new Point(stageX, stageY);
				trace("mouseDownWithTool point="+clickEvent.point);
				obj.dispatchEvent(clickEvent);  // on model, to signal "initialDrawListener"
			}
			return obj;
		}
		
		public static function setupPolygon(obj:AbstractObject):void
		{
			if (ApplicationController.instance.authorController.currentEditingMediator is PathEditingMediator)
				ApplicationController.instance.authorController.makeObject();

			var path:PathObject = obj as PathObject;
			path.visible = false;  // revealed below
			path.polygonCreation = true;
			path.title = "Polygon";
			path.fillAlpha = 100;
			path.buildPolygon(polygonRadius, PolygonEditingPanel.numSides);
			path.width = 1;  // user will drag it out from this tiny size (ObjectController will enforce min size, if this is less than 3)
			path.height = 1;
			path.anchorPoint = new Point(0.5,0.5);
			
			// avoid flash of full-size graphic
			// initial scaling will be overridden by setting width/height on mouseMove
			path.path.scaleX = 0;
			path.path.scaleY = 0;
			path.getView().render();
			path.visible = true;  // this has a side-effect of de-selecting the object
		}

		public function PathEditingMediator(view:PathObjectView)
		{
			_context = view.context;
			_view = view;
			_path = view.object as PathObject;
			_path.addEventListener(PathEvent.INITIAL_CLICK, initialDrawListener);
			_path.addEventListener(PathEvent.OPEN_EDITING, openEditingListener);
			_view.addEventListener(MouseEvent.DOUBLE_CLICK, doubleClickListener);
			_path.addEventListener(AnchorEvent.TYPE, anchorChangeListener);
			_authorController = ApplicationController.instance.authorController;
			_actionTree = new ActionTree();
		}
		
		public function clearViewEvents():void
		{
			if (_path.editing) {
				_path.editing = false;
				_authorController.closingEditing(this, true);
			}
			_path.removeEventListener(PathEvent.OPEN_EDITING, openEditingListener);
			_context.stage.removeEventListener(MouseEvent.MOUSE_DOWN, mouseCaptureListener, true);
			_path.removeEventListener(AnchorEvent.TYPE, anchorChangeListener);
			removeEditListeners();
			removeFeedback();
			if (_path.bitmapFill)
				_path.bitmapFill.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, assetPropertyListener);
		}
		
		private function openEditing():void
		{
			if (_path.editing)
				return;
			_creationSession = (_path.nodeCount == 0);
			trace("openEditing",_path, "creation="+_creationSession);
			_oldPath = _path.path.clone();
			_oldPosition = new Point(_path.x, _path.y);
			_oldAnchor = _path.anchorPoint.clone();
			_destroyObjectAction = DestroyObjectAction.fromObject(_path);
			_authorController.clearSelection();
            _actionTree = new ActionTree();
            _nodeAnchorsOriginal = collectNodeAnchors();
            _nodeAnchorActions = new Dictionary();
			ApplicationController.instance.wireController.showWiresForLevel();
            _path.excludeFromContainerLayouts = true;

			_path.editing = true;
			_path.selectAll(false);
			_authorController.startingEditing(this);
			addFeedback();
			WorldContainer(_context).feedbackUIView.mouseChildren = false;  // prevent clicks on MC
			WorldContainer(_context).feedbackWiringView.mouseChildren = false; 
			WorldContainer(_context).feedbackControlsView.mouseChildren = false;  // prevent clicks on arena paging controls
			
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
			_context.stage.focus = null;
			addEditListeners();
						
			showToolboxPanel(_initialDragDraw);
			Application.instance.currentTool = ToolboxController.ANIMATION_PATH;
			ApplicationController.instance.updateWorldMouseActive();  // enable mouse events on the path

			// let inspector know about this object
			var drawings:Vector.<ITaconiteObject> = new Vector.<ITaconiteObject>();
			drawings.push(_path);
			InspectorController.instance.listeningToSelection = false;  // restored on closeEditing
			InspectorController.instance.selectedDrawings = drawings;
		}
		
		private function closeEditing():void
		{
			if (!_path.editing)
				return;
			
			finishExtendingPath();
			_path.complete = true; // may or may not be a new created obj
            _path.active = true;
			removeAnchorHilite();
			_path.finish();
			_path.editing = false;  // after finish, so snapTo can be disabled when recalcBounds
			_path.snapObjectToPath();
            _path.excludeFromContainerLayouts = false;
			_path.draggingHandle = false; // this was occasionally getting left true
			_authorController.closingEditing(this, true);
			removeFeedback();
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
			removeEditListeners();
			InspectorController.instance.listeningToSelection = true;
			_authorController.selectSingleModel(_path.model);
			WorldContainer(_context).feedbackUIView.mouseChildren = true; 
			WorldContainer(_context).feedbackWiringView.mouseChildren = true; 
			WorldContainer(_context).feedbackControlsView.mouseChildren = true;
			
			if (_path.nodeCount > 1) { 
				if (_creationSession) {
					ApplicationController.currentActionTree.commit(CreateObjectAction.fromObject(_path));
                } else {
                    var groupToken:Object = ApplicationController.currentActionTree.openGroup("Edit path");
					ApplicationController.currentActionTree.commit(PathEditAction.fromObject(_path, _oldPath, _oldPosition, _oldAnchor));
                    commitNodeAnchorActions();
                    ApplicationController.currentActionTree.closeGroup(groupToken);
                }
				ApplicationController.instance.authorController.log("editPathObject " + _path.logStr);
			} else {  // null path:  delete it 
				// don't log undo action for creation of null path (since Redo would then create a null path)
				if (!_creationSession)  
					ApplicationController.currentActionTree.commit(_destroyObjectAction);
				_destroyObjectAction.perform();
			}
			_oldPath = null;
			_oldPosition = null;
			_oldAnchor = null;
			_destroyObjectAction = null;
            _nodeAnchorsOriginal = null;
            _nodeAnchorActions = null;
						
			// in case it wasn't already cleared by mouseUp
			_initialDragDraw = false;  
						
			if (_toolboxPanel)
				_toolboxPanel.editingClosed();  // panel may remain, if path tool was locked in
			
			// switch back to arrow tool (unless author selected another tool already)
			if (Application.instance.currentTool == ToolboxController.ANIMATION_PATH) {
				var toolEvent:ToolEvent = new ToolEvent(ToolEvent.TOOL_APPLIED);
				ApplicationController.instance.dispatchEvent(toolEvent);
			} else {
				ApplicationController.instance.updateWorldMouseActive();  // if switch to polygon or magnifier, objects should not select when clickede
			}
		}
		
		private function addEditListeners():void
		{
			_context.stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseCaptureListener, true, 1);
			_context.stage.addEventListener(MouseEvent.DOUBLE_CLICK, doubleCaptureListener, true, 1);
			ApplicationUI.instance.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);
			_context.stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			_context.stage.addEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
		}
		
		private function removeEditListeners():void
		{
			_context.stage.removeEventListener(MouseEvent.MOUSE_DOWN, mouseCaptureListener, true);
			_context.stage.removeEventListener(MouseEvent.DOUBLE_CLICK, doubleCaptureListener, true);
			ApplicationUI.instance.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);
			_context.stage.removeEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			_context.stage.removeEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
		}
		
		private function showToolboxPanel(creation:Boolean):void
		{
			removeToolboxPanel();
			var holder:UIComponent = ToolboxPanel.pathPanel;
			if (holder) {
				_showingCreationPanel = creation;
				_toolboxPanel = new PathEditingPanel(_showingCreationPanel, _path);
				holder.addChild(_toolboxPanel);
			}
		}
		
		private function removeToolboxPanel():void
		{
			if (_toolboxPanel) {
				var holder:UIComponent = ToolboxPanel.pathPanel;
				while (holder.numChildren > 0)
					holder.removeChildAt(0);  // be sure to remove panel created selecting the path tool, before any PathObject existed
				_toolboxPanel = null;
			}
		}

		
		private function makeAnotherPath():void
		{
			trace("makeAnotherPath");
			ApplicationController.instance.authorController.makeObject();
			ToolboxController.instance.handleNewToolSelection(ToolboxController.ANIMATION_PATH);
			_authorController.clearSelection();
		}
		
		private function handleKeyDown(event:KeyboardEvent):void
		{
			trace("KEY="+event.keyCode);
			switch (event.keyCode) {
				case Keyboard.ENTER:
				case Keyboard.ESCAPE:
					if (_path.extendingPath)
						finishExtendingPath();
					else
						makeObject();
					break;
				case Keyboard.DELETE:
				case Keyboard.BACKSPACE:
					deleteEditing();
					break;
				case Keyboard.A:
					if (!_path.extendingPath)
						_path.selectAll();
					break;
				case Keyboard.SLASH:
				case Keyboard.SPACE:
					finishExtendingPath();
					makeAnotherPath();
					break;
				case Keyboard.R:
					if (!_path.extendingPath)
						addRibbonToSelectedNode();
					break;
				case Keyboard.Z:
					undo();
					break;
				case Keyboard.Y:
					redo();
					break;
				case Keyboard.W:
					_path.complete = true;
                    _path.active = true;
					_path.messageCenter = true;  
					_path.messageCenterVisible = !event.shiftKey;
					break;
				case Keyboard.LEFT:
					_path.moveSelectedNodes(-1, 0);
					break;
				case Keyboard.RIGHT:
					_path.moveSelectedNodes(+1, 0);
					break;
				case Keyboard.UP:
					_path.moveSelectedNodes(0, -1);
					break;
				case Keyboard.DOWN:
					_path.moveSelectedNodes(0, +1);
					break;
			}
		}
		
		private function handleKeyUp(event:KeyboardEvent):void
		{
			switch (event.keyCode) {
				case Keyboard.SHIFT:
					break;
			}
		}
		
		private function openEditingListener(event:PathEvent):void
		{
			if (!_path.editing && !Application.running && !Application.locked && !_path.childOfClosed) {
				openEditing();
				event.stopImmediatePropagation(); // don't open other gadgets
			}
		}

		private function initialDrawListener(event:PathEvent):void
		{
			// on creation, the worldMediator dispatches a mouseDown onto the PathObject (not on the view)
			_path.removeEventListener(PathEvent.INITIAL_CLICK, initialDrawListener);
			_initialDragDraw = true;
			
			// grab smoothing value from path-panel created by selecting path tool
			if (ToolboxPanel.pathPanel && ToolboxPanel.pathPanel.numChildren > 0) {  
				var panel:DisplayObject = ToolboxPanel.pathPanel.getChildAt(0);
				if (panel is PathEditingPanel)
					_path.pathSpacing = PathEditingPanel(panel).smoothing;  
			}
			openEditing();
			_path.dispatchEvent(event);  // feedback mediator will get this
			
			_view.stage.addEventListener(MouseEvent.MOUSE_UP, initialMouseUpListener, false, -1);
		}
		
		private function initialMouseUpListener(event:MouseEvent):void
		{
			// even tho drawing the initial path may be continuing ....
			_view.stage.removeEventListener(MouseEvent.MOUSE_UP, initialMouseUpListener);
			_initialDragDraw = false;
		}
		
		private function mouseCaptureListener(event:MouseEvent):void
		{
			trace("Path mouseCapture");
			// while extendingPath, the handle drag mediator needs to catch mouseDown on stage
			if (hitTestToolbox(event.stageX, event.stageY))
				return;  // allow clicks on toolbox tools
			var canvas:Rectangle = EditorUI.instance.canvasVisibleRect(_view.stage);
			if (!canvas.contains(event.stageX, event.stageY))
				return;  // allow clicks on inspector controls
			
			var onHandle:Boolean = (event.target is PathNodeHandle);
			if (!_path.extendingPath && !onHandle)
				event.stopImmediatePropagation();
			
			// add new point along existing line or curve
			if (event.target == _view.curve && !_path.extendingPath) {
				removeAnchorHilite();
				var pathBeforeAdd:Path = _path.path.clone();
				var positionBeforeAdd:Point = new Point(_path.x, _path.y);
				var anchorBeforeAdd:Point = _path.anchorPoint.clone();
				var local:Point = _view.curve.globalToLocal(new Point(event.stageX, event.stageY));
				var pointOnCurve:Point = new Point();
				var frac:Number = _path.path.closestPoint(local, pointOnCurve);
				if (Point.distance(pointOnCurve, local) <= _path.lineThickness+2) {
					var n:int = _path.path.nodeIndexFromFrac(frac);
					var sharp:Boolean = _path.path.getSharp(n);
					if (n+1 < _path.nodeCount)
						sharp &&= _path.path.getSharp(n+1);
					_path.addPoint(n+1, pointOnCurve, sharp);
					ApplicationController.currentActionTree.commit(PathEditAction.fromObject(_path, pathBeforeAdd, positionBeforeAdd, anchorBeforeAdd));
					return;
				}
			}
			
			if (!onHandle && !_path.extendingPath) {
				doMarquee(event);
			}
		}
		
		public function doMarquee(event:MouseEvent):void
		{
			trace(" path marquee");
			if (!event.shiftKey)
				_path.selectAll(false);
			new MarqueeMediator(_context, worldView, marqueeCallback).handleMouseDown(event);
		}
		
		private function marqueeCallback(globalRect:Rectangle):void
		{
			var count:int = _path.nodeCount;
			for (var n:int=0; n < count; n++) {
				var local:Point = _path.path.getPoint(n);
				var global:Point = _view.curve.localToGlobal(local);
				if (globalRect.containsPoint(global))
					_path.selectNode(n, true, true);  // select=true, extend=true
			}
		}
		
		private function doubleCaptureListener(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
			finishExtendingPath();
			trimPointsAtEnd();
		}
		
		private function mouseMoveListener(event:MouseEvent):void
		{
			// we are using a global mouseMove listener, since mouseChildren=false on the message-centers layer

			removeAnchorHilite();
			var global:Point = new Point(event.stageX, event.stageY);
			var n:int = anchorUnderPoint(global);
			if (n >= 0) {
				hiliteAnchor(n, true);
			}
		}
		
		private function anchorUnderPoint(global:Point):int
		{
			// gray squares on the path nodes
			var n:int = _feedback.anchorUnderPoint(global);
			if (n >= 0)
				return n;
			
			// ribbons
			var mc:MessageCenterView = _view.messageCenterView;
			if (mc && mc.visible && mc.getBounds(mc.stage).containsPoint(global)) {
				var ribbon:RibbonView = _view.messageCenterView.ribbonUnderPoint(global);
				if (ribbon) {
					var key:String = ribbon.ribbon.modifierDescription.key;
					return _path.nodeForKey(key);
				}
			}
			return -1;
		}
		
		private var _hilitedAnchor:int = -1;

		private function hiliteAnchor(n:int, hilite:Boolean):void
		{
			if (n >= _path.nodeCount)
				return;
			
			_feedback.hiliteAnchor(n, hilite);
			
			// hilite the corresponding ribbon, if the MC is showing
			var info:PathNodeInfo = _path.path.getAnchorInfo(n);
			if (info) {
				var anchor:WireAnchor = _path.anchors[info.key];
				if (anchor) {
					var views:Vector.<PortView> = anchor.getViews(WorldContainer(_context));
					if (views.length > 0) {
						var port:PortView = views[0];
						if (port) {
							var ribbon:RibbonView = port.parent as RibbonView;
							if (ribbon) {
								ribbon.rolled = hilite;
							}
						}
					}
				}
			}
			_hilitedAnchor = (hilite) ? n : -1;
		}
		
		private function removeAnchorHilite():void
		{
			if (_hilitedAnchor >= 0) {
				hiliteAnchor(_hilitedAnchor, false);
			}
		}

		private function doubleClickListener(event:MouseEvent):void
		{
			if (_path.editing) {
				finishExtendingPath();
				trimPointsAtEnd();
			} else if (!_path.locked) {
				switch (_view.doubleClickAction) {
					case DoubleClickAction.EDIT:
						if (!Application.runningLocked && !_path.childOfClosed) {
							if (Application.running){
								ApplicationController.instance.authorController.pause();
                            }
							openEditing();
							event.stopImmediatePropagation(); // don't open other gadgets
						}
						break;
					case DoubleClickAction.STOP:
						event.stopImmediatePropagation();
						break;
				}
			}
		}
		
		private function finishExtendingPath():void
		{
			if (_path.extendingPath) {
				_path.dispatchEvent(new PathEvent(PathEvent.FINISH_EXTENDING));
			}
			if (_showingCreationPanel) {
				showToolboxPanel(false); // creation=false;   show the editing panel, now that the initial stroke is completed
			}
		}
		
		private function trimPointsAtEnd():void
		{
			const TOL:Number = 6;
			var endPoint:Point = _path.path.getPoint(_path.nodeCount-1);
			while (_path.nodeCount > 2 && Point.distance(endPoint, _path.path.getPoint(_path.nodeCount-2)) < TOL) {
				_path.deletePoint(_path.nodeCount-2);
				trace("trimmedPoint");
			}
		}
		
		private function anchorChangeListener(e:AnchorEvent):void
		{
			// detect author dragging ribbon out of the message-center (can only be done when not editing)
//			trace("anchorChangeListener: kind="+e.kind, "anchor="+e.anchor);
			if (e.kind == AnchorEvent.DRAGGED_OUT) {
				var anchor:PathNodeWireAnchor = e.anchor as PathNodeWireAnchor;
				if (anchor) {
					var key:String = anchor.hostProperty;
					var n:int = _path.nodeForKey(key);
					if (n >= 0) {
						_path.path.setAnchorInfo(n, null);
					}
				}
			}
		}
		
		public function pathReplaced():void
		{
//			trace("pathMediator: pathReplaced");
			if (_path.editing) {
				removeFeedback();  // rebuild feedback since each handle has a reference to a node within the path array
				addFeedback();
			}
		}
		
		public function bitmapFillChanged(oldAsset:Asset, newAsset:Asset):void
		{
			if (oldAsset) {
				oldAsset.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, assetPropertyListener);
			}
			if (newAsset) {
				newAsset.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, assetPropertyListener);
			}
			_path.requestRedraw();
		}
		
		private function assetPropertyListener(event:PropertyChangeEvent):void
		{
			if (event.property == "content")
				_path.requestRedraw();
		}
		
		public function objCompleted():void
		{
			if (_path.polygonCreation) {
				trace("poly complete");
				_path.polygonCreation = false;
                _path.bornAsPolygon = true;
				_path.finish();  // ensure initialValues.path is set
				_authorController.selectSingleModel(_path.model);  // polygon was deselected by setting visible=false during setup
			}
		}
		
		private function addFeedback():void
		{
			_feedback = new PathFeedback(_context, _view);
			_feedback.initialize();
			EditorUI.instance.presentationContainer.feedbackHandlesView.addChild(_feedback);
		}
		
		private function removeFeedback():void
		{
			if (_feedback) {
				_feedback.warnViewToDelete();
				EditorUI.instance.presentationContainer.feedbackHandlesView.removeChild(_feedback);
			}
			_feedback = null;
		}
		
		public function recalcBounds():void
		{
			// update drawing so getBounds will give correct results
			_view.render();
			
			if (_path.nodeCount <= 1)
				return;  // don't recalc bounds when only one node, since getBounds won't have any graphics to measure
			
			// similar to CompositeMediator:recalcBounds()
			var bounds:Rectangle = _view.curve.getBounds(_view.curve);
			
			// part 1: set the new location of the path object
			var oldGlobalTopLeft:Point = _view.curve.localToGlobal(new Point(0, 0));
			setBounds(bounds);
			var oldTopLeft:Point = _view.curve.globalToLocal(oldGlobalTopLeft);
			var dx:Number = oldTopLeft.x;
			var dy:Number = oldTopLeft.y;
			
			// part 2: adjust the points to keep them at same global positions
			_path.path.applyOffset(dx,dy);
			_path.resnapAll(false);  // nearest=false
			
			// immediate update to prevent image bounce
			_view.render();
		}
		
		private function setBounds(bounds:Rectangle):void
		{
			// similar code exists in PathObject.restorePath()
			var oldWidthScale:Number = _path.scaleX;
			var oldHeightScale:Number = _path.scaleY;
						
			// adjust gadget position: different handling of anchorPoint depending on whether its custom, v/s at corner or center
			if (_path.hasCustomAnchor) {
				// custom anchorpoint: adjust it now to preserve its absolute position
				var oldX:Number = _path.x;
				var oldY:Number = _path.y;
				var anchor:Point = _view.curve.globalToLocal(_view.localToGlobal(new Point(0,0)));
				_path.anchorPoint = new Point((anchor.x - bounds.left)/bounds.width, (anchor.y - bounds.top)/bounds.height);
				_path.x = oldX;  // setting anchorPoint affects obj.x & y
				_path.y = oldY;
			} else {
				// adjust object position to keep gadget contents at same position, while width & height change
				// in this case the anchorPoint moves, to stay at same percentage
				var newAnchor:Point = new Point(bounds.left + _path.anchorPoint.x * bounds.width, bounds.top + _path.anchorPoint.y * bounds.height);
				var newPosition:Point = _view.parent.globalToLocal(_view.curve.localToGlobal(newAnchor));  // parent coordinates
				_path.x = newPosition.x;
				_path.y = newPosition.y;
			}
			
			_path.viewBox = new Rectangle(0,0, bounds.width / oldWidthScale, bounds.height / oldHeightScale);
			_path.width = bounds.width;
			_path.height = bounds.height;
			_path.requestRedraw();
		}
		
		private function get worldView():WorldView
		{
			return WorldContainer(_context).worldView;
		}
		
		public static function hitTestToolbox(stageX:Number, stageY:Number):Boolean
		{
			if (ApplicationController.instance.authorController.currentEditingMediator is PathEditingMediator) {
				var mediator:PathEditingMediator  = ApplicationController.instance.authorController.currentEditingMediator as PathEditingMediator;
				if (mediator._toolboxPanel) {
					return mediator._toolboxPanel.hitTestWholeToolbox(stageX, stageY);
				}
			}
			return false;
		}

        public function addRibbonToSelectedNode():void
        {
            for (var n:int=0; n < _path.nodeCount; n++) {
                if (_path.path.getSelected(n) && _path.path.getAnchorInfo(n)==null) {
                    addNodeRibbon(n);
                    return;
                }
            }
        }

        private function addNodeRibbon(index:int):void
        {
            var pathBefore:Path = _path.path.clone();
            var positionBefore:Point = new Point(_path.x, _path.y);
            var anchorBefore:Point = _path.anchorPoint.clone();

            var anchor:PathNodeWireAnchor = _path.addNodeRibbon(index);
            var action:CustomRibbonAction = CustomRibbonAction.fromAnchor(anchor, true);

            var grp:Object = ApplicationController.currentActionTree.openGroup("Add node ribbon");
            ApplicationController.currentActionTree.commit(PathEditAction.fromObject(_path, pathBefore, positionBefore, anchorBefore));
            ApplicationController.currentActionTree.commit(action);
            ApplicationController.currentActionTree.closeGroup(grp);
            logNodeAnchorAction(anchor, action);

            // show the MC, so the ribbon can be seen
             _path.complete = true;
             _path.active = true;
             _path.messageCenter = true;
             _path.messageCenterVisible = true;
        }

        public function deleteSelected():void
        {
            var pathBefore:Path = _path.path.clone();
            var positionBefore:Point = new Point(_path.x, _path.y);
            var anchorBefore:Point = _path.anchorPoint.clone();

            for (var n:int=_path.nodeCount-1; n >= 0; n--) {
                if (_path.path.getSelected(n) && _path.nodeCount>1) {  // always leave one node
                    _path.deletePoint(n);
                }
            }
            var grp:Object = ApplicationController.currentActionTree.openGroup("Delete path nodes");
            removeExcessAnchorsAndWires(_path);
            ApplicationController.currentActionTree.commit(PathEditAction.fromObject(_path, pathBefore, positionBefore, anchorBefore));
            ApplicationController.currentActionTree.closeGroup(grp);
        }

        private function removeExcessAnchorsAndWires(obj:PathObject):void
        {
            // find anchors for path nodes that no longer exist   Commits undo actions
            var anchorsToRemove:Vector.<PathNodeWireAnchor> = new <PathNodeWireAnchor>[];
            for each (var thing:* in obj.anchors) {
                var nodeAnchor:PathNodeWireAnchor = thing as PathNodeWireAnchor;
                if (nodeAnchor) {
                    if (obj.nodeForKey(nodeAnchor.modifierDescription.key) < 0) {
                        anchorsToRemove.push(nodeAnchor);
                        trace("  deleting anchor="+nodeAnchor);
                    }
                }
            }

            // remove the anchors
            for each (var anchor:PathNodeWireAnchor in anchorsToRemove) {
                var action:CustomRibbonAction = CustomRibbonAction.fromAnchor(anchor, false);
                action.perform();
                ApplicationController.currentActionTree.commit(action);
                logNodeAnchorAction(anchor, action);
            }
        }

        private function logNodeAnchorAction(anchor:PathNodeWireAnchor, action:CustomRibbonAction):void
        {
            var anchorPath:String = anchor.path;
            if (!(anchorPath in _nodeAnchorActions)) {
                _nodeAnchorActions[anchorPath] = action;
            }
        }

        private function collectNodeAnchors():Dictionary
        {
            var result:Dictionary = new Dictionary();
            for each (var thing:* in _path.anchors) {
                var nodeAnchor:PathNodeWireAnchor = thing as PathNodeWireAnchor;
                if (nodeAnchor) {
                    result[nodeAnchor.path] = true;
                }
            }
            return result;
        }

        private function commitNodeAnchorActions():void
        {
            // on closing editing, commit actions to create & delete path node ribbons as needed
            for (var anchorPath:String in _nodeAnchorActions) {
                var originallyExisted:Boolean = (anchorPath in _nodeAnchorsOriginal);
                var stillExists:Boolean = WireAnchor.findByPath(_path, anchorPath);
                if (originallyExisted != stillExists) {
                    var action:CustomRibbonAction = _nodeAnchorActions[anchorPath];
                    ApplicationController.currentActionTree.commit(action);
                }
            }
        }

		public function cancelEditing():Boolean {
			return false;
		}

        public function getSelection(getGlobal:Boolean, strict:Boolean=true, omitLocked:Boolean=true):Vector.<DragInfo> {
            return new Vector.<DragInfo>();
        }

        public function deleteEditing():Boolean {
			if (!_path.extendingPath) {
				removeAnchorHilite();
				deleteSelected();
			}
			return true;
		}
		
		public function selectAll(includeLocked:Boolean):void {
			_path.selectAll();
		}
		
		public function makeObject():Boolean {
			closeEditing();
			return true;
		}
		
		public function get hasActiveState():Boolean
		{
			return true;
		}
		
		public function activeMediatorChanged(mediator:IEditingMediator):void
		{
		}
		
		public function switchedObjects(oldObj:ITaconiteObject, newObj:ITaconiteObject):void {
		}
		
		public function get editingObject():ITaconiteObject {
			return _path;
		}
		
		public function get actionTree():ActionTree
		{
			return _actionTree;
		}
		
		public function canUndo():Boolean {
			return _actionTree.canUndo;
		}
		
		public function canRedo():Boolean {
			return _actionTree.canRedo;
		}
		
		public function undo():void {
			finishExtendingPath();
			_actionTree.undo();
		}
		
		public function redo():void {
			_actionTree.redo();
		}
		
		public function undoName():String {
			return _actionTree.undoName;
		}
		
		public function redoName():String {
			return _actionTree.redoName;
		}

        public function get MENULABEL_TYPE():String{
            return _MENULABEL_TYPE;
        }

		public function cut(e:Event):void {
			ClipboardController.instance.cutClipboard(e);
		}
		
		public function copy(e:Event):void {
			ClipboardController.instance.copyClipboard(e);
		}
		
		public function paste(e:Event):void {
			ClipboardController.instance.pasteClipboard(e);
		}
	}
}
