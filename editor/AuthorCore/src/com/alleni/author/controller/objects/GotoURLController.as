package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.GotoURL;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.net.sendToURL;

	public class GotoURLController extends ObjectController
	{
		public static var instance:GotoURLController = new GotoURLController();
		
		public function GotoURLController()
		{
			super();
		}
		
		override public function initializeObject(model:TaconiteModel):void
		{
			super.initializeObject(model);
		}
		
		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			var gtUrlObject:GotoURL = GotoURL(object);
			switch (modifier.key)
			{
				case "gotoURL":
					gotoNewURL(gtUrlObject);
					break;
				case "powerOn":
					gtUrlObject.poweredOnRun = true;
					break;
				case "powerOff":
					gtUrlObject.poweredOnRun = false;
					break;
			}
		}
		
		private function gotoNewURL(gtUrlObject:GotoURL):void{
			if(!gtUrlObject.poweredOnRun){
				trace("Weblink's power was shut off");
				return;
			}
			var gotoUrl:String = gtUrlObject.theURL;
			if(gotoUrl.substr(0,7) != "http://" && gotoUrl.substr(0,8) != "https://"){
				gotoUrl = "http://"+gotoUrl;
			}
			if(gtUrlObject.queryString != ""){
				gotoUrl = gtUrlObject.theURL + "?" + gtUrlObject.queryString;
			}
			
			var request:URLRequest = new URLRequest(gotoUrl);
			if(gtUrlObject.jump){
				if(gtUrlObject.newWindow){
					navigateToURL(request, '_blank');
				}else{
					navigateToURL(request, '_self');
				}
			}else{
				sendToURL(request);
			}
		}
	}
}