/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/14/11
 * Time: 12:46 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.controller.objects {
import com.alleni.author.controller.ObjectController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.model.objects.BroadcastConnection;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;

public class BroadcastConnectionController extends ObjectController{
	public static var instance:BroadcastConnectionController = new BroadcastConnectionController();
	public function BroadcastConnectionController() {
	}

	override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void {
		super.handleTrigger(object, modifier);
		var conn:BroadcastConnection = object as BroadcastConnection;
		switch(modifier.key) {
			case "send":
				conn.send();
				break;
		}
	}
}
}
