package com.alleni.author.controller.objects
{
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.Navigation.SmartObjectController;
import com.alleni.author.Navigation.SmartObjectEditingMediator;
import com.alleni.author.Navigation.SmartObjectMediator;
import com.alleni.author.controller.ObjectDragMediator;
import com.alleni.author.controller.ObjectFactory;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.controller.ui.GroupSelectionController;
import com.alleni.author.definition.GadgetDescription;
import com.alleni.author.definition.PositionAndAngle;
import com.alleni.author.definition.action.DestroyObjectAction;
import com.alleni.author.event.CompositeEvent;
import com.alleni.author.event.WireEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.AbstractTable;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.LogicObject;
import com.alleni.author.model.objects.Oval;
import com.alleni.author.model.objects.RectangleObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.SetterWireAnchor;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.objects.ArenaControlBar;
import com.alleni.author.view.objects.CompositeVelumView;
import com.alleni.author.view.objects.CompositeView;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ModelCollectionEvent;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.event.SelectEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Dictionary;

import mx.collections.IList;
import mx.events.PropertyChangeEvent;

public class CompositeMediator extends SmartObjectMediator
	{		
		private var _view:CompositeView;
		private var _composite:Composite;
        private var _velum:CompositeVelumView;   // the velum under the objects
        private var _velumHolder:RectangleObject;
		private var _model:TaconiteModel;
		private var _ignoreChildrenMoving:int = 0;  // increment to ignore
		private var _layerPlaceholder:Oval;  // marker left at same layer/parentage before editing brought us to front

		public function CompositeMediator(context:ViewContext, view:CompositeView)
		{
            super(context, view);
			_view = view;
			_model = view.model;
			_composite = _model.value as Composite;

			_model.addEventListener(ModelEvent.MODEL_CHANGE, modelChangeListener, false, 3);
			_model.addEventListener(CompositeEvent.UNMAKE_OBJ, unmakeObjListener); 
			_model.addEventListener(CompositeEvent.ABSORB_OBJECTS, absorbObjectsListener);
			Application.instance.document.addEventListener(SelectEvent.CHANGE_SELECTION, selectionChangeListener);
		}
				
		override public function clearViewEvents() : void
		{
            super.clearViewEvents();
			_model.removeEventListener(ModelEvent.MODEL_CHANGE, modelChangeListener);
			_model.removeEventListener(CompositeEvent.UNMAKE_OBJ, unmakeObjListener); 
			_model.removeEventListener(CompositeEvent.ABSORB_OBJECTS, absorbObjectsListener); 
			Application.instance.document.removeEventListener(SelectEvent.CHANGE_SELECTION, selectionChangeListener);
		}

        private function get currentContainer():AbstractContainer
        {
            return ApplicationController.instance.authorController.currentContainer;
        }

		private function get cleanSelection():Vector.<TaconiteModel>
		{
			return ApplicationController.instance.authorController.cleanSelection;
		}
				
		private function modelChangeListener(e:ModelEvent):void
		{
			if ( !(e is ModelCollectionEvent) && e.parent == _model)
				handleModelChange(e.kind, e.parent, e.child, e.index);
		}
		
		protected function handleModelChange(kind:String, parent:TaconiteModel, child:TaconiteModel, index:int):void
		{
			//trace("handleModelChange: kind="+kind, "child="+child);
			switch (kind) {
				case ModelEvent.ADD_CHILD_MODEL:
				case ModelEvent.REMOVE_CHILD_MODEL:
					if (_composite.editing) {
						removeHiliteFromRolledBreadcrumb();
					}
					break;
			}
		}

        override protected function handleDoubleClick(event:MouseEvent):void
        {
            if (!Application.runningLocked && SmartObjectController.okToEdit(_composite)) {
                if (Application.running)
                    ApplicationController.instance.authorController.pause();
                var container:AbstractObject = _composite.wireScopeContainer;
                const composite:SmartObject = container as SmartObject;
                if (container as World || (composite && okToEditNestedGadget(composite))) {
                    openEditingThis();
                    event.stopImmediatePropagation(); // don't open other gadgets
                }
            }
        }

        private function okToEditNestedGadget(smart:SmartObject):Boolean
        {
            // only allow editing a nested gadget if the parent currently has the velum
            if (!smart.editing) {
                return false;
            } else {
                var editor:SmartObjectEditingMediator = ApplicationController.instance.authorController.currentVelumMediator;
                return editor && editor.editingObject == smart;
            }
        }

		public static function openEditing(composite:Composite):void
		{
			if (SmartObjectController.okToEdit(composite)) {
				var view:CompositeView = composite.getView() as CompositeView;
				if (view)
					view.openEditing();
			}
		}

		override public function openEditingThis():void
		{
			// close editors that aren't ancestors of this composite
			// find the topmost ancestor being edited
			var keepOpen:IEditingMediator = editorToKeepOpen(_composite);  // null if nothing to keep open
			ApplicationController.instance.authorController.closeEditors(null, null, null, keepOpen);

            // wait for gadget to build-out, else vellum could see invalid viewBox
//            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            PseudoThread.add(new PseudoThreadSimpleRunnable("CompositeMediator:openEditingNow", openEditingNow));
		}

		private function openEditingNow():void
		{
			if (_composite.editing)
				return;
			if (_composite.parent == null)  // probably replace-all just replaced this gadget
				return;
            trace("openEditingNow",_composite);
			ApplicationController.instance.authorController.clearSelection();
			
			// create deletion action for use in GadgetSaveController
			_composite.actionsDestroyOriginal = DestroyObjectAction.fromObject(_composite)
			
			_composite.editing = true;
						
			// bring to front, but leave placeholder so we can go back there
			var layer:int = AbstractContainer(_composite.model.parent.value).objects.getItemIndex(_composite);
			_layerPlaceholder = ObjectFactory.createForName("Oval", _composite.model.parent.value, layer,0,0,0,0) as Oval;
			_layerPlaceholder.title = CompositeVelumMediator.PLACEHOLDER_TITLE;
			_layerPlaceholder.visible = false;
			_layerPlaceholder.locked = true;  // prevent showing via Show-all
			var scopeParent:AbstractContainer = _composite.parent;
            ObjectDragMediator.changeParentObject(_composite, currentContainer);

			_composite.scopeParent = scopeParent;
			
			// create the velum and place it behind the composite (initial position is at mouse click)
			createVelumView();
						
			// enable objects to be added to the composite
			_composite.accepting = true;  

			// set editing and show composite message center
			_composite.complete = true;
			_composite.messageCenterVisible = true;
			
			// in case showing-all-wiring, show it now
			ApplicationController.instance.wireController.showWiresForLevel();
		
			removeHiliteFromRolledBreadcrumb();
			
			// show truth tables
			notifyChildrenOfStatusChange();
			ArenaControlBar.updateAllControlbarVisibility();
			
			_model.addEventListener(WireEvent.COMPLETE, wireCompleteListener);
		}

        private function notifyChildrenOfStatusChange():void
        {
            // gadget just opened or closed, so childOfClosedComposite changed...  truth-tables need to hide or show
            var children:Array = GadgetEncoder.collectChildren(_composite, null, true, false);
            notifyArrayOfStatusChange(children);
        }

        private function notifyArrayOfStatusChange(array:Array):void
        {
            var obj:AbstractObject;
            for each (obj in array) {
                var event:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(obj, "childOfClosedComposite", null, obj.childOfClosedComposite);
                obj.dispatchEvent(event);
            }
        }

		private function createVelumView():void
		{
			var container:AbstractContainer = currentContainer;
			var layer:int = container.objects.getItemIndex(_composite);
			_velumHolder = ObjectFactory.createForName("RectangleObject", container, layer) as RectangleObject;
            _velumHolder.title = CompositeVelumMediator.VELUM_TITLE;
            _velumHolder.fillAlpha = 0;
            _velumHolder.lineAlpha = 0;
            _velumHolder.locked = true;  // prevent selection
            var holderView:ObjectView = _velumHolder.getView(_context);
			_velum = new CompositeVelumView(_context, _composite, _velumHolder, this);
            holderView.addChild(_velum);
            _velum.velumHolder = _velumHolder;
			_view.velumView = _velum;
		}
		
		public function deleteVelumView():void
		{
			if (_velumHolder) {
                _velum.warnViewToDelete();  // _velum is not an ObjectView so it doesn't get this automatically
				var container:AbstractContainer = _velumHolder.parent as AbstractContainer;
				var index:int = container.objects.getItemIndex(_velumHolder);
				container.objects.removeItemAt(index);
				_view.velumView = null;
				_velum = null;
                _velumHolder = null;
			}
		}
				
		override public function closeEditing():void
		{
			trace("CompositeMediator:closeEditing", _composite, "parent="+_composite.parent, "objects="+_composite.objects.length);
			if (_composite.editing == false)
				return;  // already closed
			deleteVelumView();  // closes editing mediator
			_composite.messageCenterVisible = false;
			_model.removeEventListener(WireEvent.COMPLETE, wireCompleteListener);
			
			ApplicationController.instance.wireController.showWiresForLevel();
			var oldPos:Dictionary = new Dictionary();
			var oldRot:Dictionary = new Dictionary();
			var existing:Vector.<AbstractObject> = new Vector.<AbstractObject>();
			var n:int;
			var count:int = _model.numChildren;
			var child:TaconiteModel;
			var obj:AbstractObject;
			for (n = count - 1; n >= 0; n--) {
				child = _model.getChildAt(n);
				obj = child.value as AbstractObject;
				oldPos[obj] = new Point(obj.x, obj.y);
				oldRot[obj] = obj.rotation;
				existing.push(obj);
				
				if (obj.messageCenterRepositioned && !obj.messageCenterRelative) {  // switch to relative MC coordinates (in case of pasted obj)
					var view:ObjectView = obj.getView();
					view.undockMessageCenter(view.computeMessageCenterPosition());
				}
			}
			
			recalcBounds();
			fixParentChange(existing, oldPos, oldRot);
			_composite.editing = false;  // (enables snapToPath, so must be after recalcBounds)
			_composite.accepting = false;
			_composite.complete = true;  // show MC, there will be one
			
			// restore to former layer
			if (_layerPlaceholder) {
				var container:AbstractContainer = _layerPlaceholder.model.parent.value as AbstractContainer;
				var layer:int = container.objects.getItemIndex(_layerPlaceholder);
				container.objects.removeItemAt(layer);  // remove placeholder
				ObjectDragMediator.changeParentObject(_composite, container);
				container.model.setChildIndex(_composite, layer);
				_composite.scopeParent = null;
			}
            notifyChildrenOfStatusChange();
			ArenaControlBar.updateAllControlbarVisibility();
		}

		private function computeBounds(frame:DisplayObject):Rectangle
		{
			// bounds in the coordinate system of "frame"
			var rect:Rectangle;
			var obj:AbstractObject;
			for each (obj in _composite.objects) {
				if (!isLogicObject(obj) && obj.visible && !CompositeVelumMediator.isSpecialObject(obj)) {
					var view:ObjectView = obj.getView();
					var objRect:Rectangle = view.getBounds(frame);
					if (rect == null) {
						rect = objRect;
					} else {
						rect = rect.union(objRect);
					}
				}
			}
			if (rect)
				return rect;
			else
				return computeDefaultRect(frame);
		}
		
		private function computeDefaultRect(frame:DisplayObject):Rectangle
		{
			var obj:AbstractObject;
			for each (obj in _composite.objects) {
				if (!CompositeVelumMediator.isSpecialObject(obj)) {
					var view:ObjectView = obj.getView();
					var objRect:Rectangle = view.getBounds(frame);
					objRect.width = 25;
					objRect.height = 25;
					return objRect;
				}
			}
			return null;  // should never happen
		}
		
		private function isLogicObject(obj:AbstractObject):Boolean
		{
			return (obj as LogicObject);
		}
		
		private function recalcBounds():void
		{
			if (_composite.loading)  // since child coordinates will be incomplete (some NaN)
				throw new Error("CompositeMediator:recalcBounds: called while loading");
			
			// part 1: place the composite
            var container:DisplayObjectContainer = _view.childViewContainer;
			var bounds:Rectangle = computeBounds(container);
			if (bounds == null)
				return;
			var oldGlobalTopLeft:Point = container.localToGlobal(new Point(0, 0));
			setBounds(bounds);
			var oldTopLeft:Point = container.globalToLocal(oldGlobalTopLeft);
			var dx:Number = oldTopLeft.x;
			var dy:Number = oldTopLeft.y;

			// part 2: adjust the children to keep them at same global positions
			++_ignoreChildrenMoving;
			if (dx != 0 || dy != 0) {
				moveChildren(dx,dy);
			}
			--_ignoreChildrenMoving;
		}
		
		
		
		private function setBounds(bounds:Rectangle):void
		{
			var oldWidthScale:Number = 1;
			var oldHeightScale:Number = 1;
			if (_composite.viewBox) {
				oldWidthScale = _composite.width / _composite.viewBox.width;
				oldHeightScale = _composite.height / _composite.viewBox.height;
			}

            if(oldHeightScale == Infinity && oldWidthScale != Infinity){
                oldHeightScale = oldWidthScale
            }else if(oldHeightScale != Infinity && oldWidthScale == Infinity){
                oldWidthScale = oldHeightScale;
            }else if(oldHeightScale == Infinity && oldWidthScale == Infinity){
                oldWidthScale = 1;
                oldHeightScale = 1;
            }

			// adjust gadget position: different handling of anchorPoint depending on whether its custom, v/s at corner or center
			if (_composite.hasCustomAnchor) {
				// custom anchorpoint: adjust it now to preserve its absolute position
				var oldX:Number = _composite.x;
				var oldY:Number = _composite.y;
				var anchor:Point = _view.childViewContainer.globalToLocal(_view.localToGlobal(new Point(0,0)));
				_composite.anchorPoint = new Point((anchor.x - bounds.left)/bounds.width, (anchor.y - bounds.top)/bounds.height);
				_composite.x = oldX;  // setting anchorPoint affects obj.x & y
				_composite.y = oldY;
			} else {
				// adjust object position to keep gadget contents at same position, while width & height change
				// in this case the anchorPoint moves, to stay at same percentage
				var newAnchor:Point = new Point(bounds.left + _composite.anchorPoint.x * bounds.width, bounds.top + _composite.anchorPoint.y * bounds.height);
				var newPosition:Point = _view.parent.globalToLocal(_view.childViewContainer.localToGlobal(newAnchor));  // parent coordinates
				_composite.x = newPosition.x;
				_composite.y = newPosition.y;
			}
            if(bounds.width <= 0){
                bounds.width = .1;
            }
            if(bounds.height <= 0){
                bounds.height = .1;
            }

            if (bounds.width > 0 && bounds.height > 0) {
                _composite.viewBox = new Rectangle(0,0, bounds.width, bounds.height);
            } else {
                _composite.viewBox = null;
            }
			_composite.width = bounds.width * oldWidthScale;
			_composite.height = bounds.height * oldHeightScale;
		}
		
		private function moveChildren(dx:Number, dy:Number):void
		{
			var n:int = 0;
			var count:int = _model.numChildren;
			for (n = 0; n < count; n++) {
				var obj:AbstractObject = _composite.objects.getItemAt(n) as AbstractObject;
                if (obj.snapTo == null || obj.pathOn == false) {
                    obj.x += dx;
                    obj.y += dy;
                }

                if (obj.messageCenterRelative) {
                    obj.xMessageCenter += dx;
                    obj.yMessageCenter += dy;
                }
			}
		}

		private function unmakeObjListener(evt:CompositeEvent):void
		{
			if (evt.target == _model) {
				unmakeObj();
			}
		}
		
		private function unmakeObj():void
		{
			// prepare to notify children & arena children, after deed is done
			var children:Array = GadgetEncoder.collectChildren(_composite, null, true, false);

			var oldPos:Dictionary = new Dictionary();
			var oldRot:Dictionary = new Dictionary();
			var removed:Vector.<AbstractObject> = new Vector.<AbstractObject>();
			
			CompositeController.instance.rerouteCustomRibbons(_composite);

			// move the composite children to the parent of the composite
			var newParentModel:TaconiteModel = _model.parent;
			var compositeIndex:int = newParentModel.getChildIndex(_model);
			var newParentObj:AbstractObject = newParentModel.value as AbstractObject;
			var offsetX:int = _composite.x + _composite.left;
            var offsetY:int = _composite.y + _composite.top;
			var n:int;
			var count:int = _model.numChildren;
			for (n = count - 1; n >= 0; n--) {
				var child:TaconiteModel = _model.getChildAt(n);
                if (child.value.group == null && child.value.groupID != null) {
                    GroupSelectionController.makeGroupFromObjectGroupID(child.value);
                }
				var obj:AbstractObject = child.value as AbstractObject;
				oldPos[obj] = new Point(obj.x, obj.y);
				oldRot[obj] = obj.rotation;
				removed.push(obj);
				var view:ObjectView = obj.getView();
				var mcPos:Point = view.computeMessageCenterPosition();
                // update parent and apply offset for scaling
                view.model.changeParent(newParentObj.model);
                if (obj.snapTo == null || obj.pathOn == false) {
                    obj.x += offsetX;
                    obj.y += offsetY;
                }
                //
				newParentModel.setChildIndex(obj, compositeIndex);				
				if (obj.messageCenterRepositioned) {
                    view.undockMessageCenter(mcPos);  // use coordinates appropriate to new parent
                }
				ApplicationController.instance.authorController.selectSingleModel(child,true);
			}

			// delete the composite itself
			newParentModel.removeValueChild(_composite);
			
			notifyArrayOfStatusChange(children);

			fixParentChange(removed, oldPos, oldRot);
		}
		
		private function absorbObjectsListener(event:CompositeEvent):void
		{
			if (event.target == _model)
				absorbObjects(event.models);
		}
		
		private function absorbObjects(models:Vector.<TaconiteModel>):void
		{
			trace("absorbObjects, into",_composite);
			Utilities.assert(_composite.editing == false);   // recalcBounds is only done when closed
			++_ignoreChildrenMoving;

			var parentModel:TaconiteModel = TaconiteModel(models[0]).parent;
			var objects:IList = AbstractContainer(parentModel.value).objects;

			var oldPos:Dictionary = new Dictionary();
			var oldRot:Dictionary = new Dictionary();
			var absorbed:Vector.<AbstractObject> = new Vector.<AbstractObject>();
			var oldSnapTo:AbstractObject = null;
			var oldPathPercent:Number;

			// scan the parent so we keep objects in same Z order
			var n:int = 0;
			while (n < objects.length) {
				var obj:AbstractObject = objects[n];
				if (obj != _composite && obj.parent != _composite && models.indexOf(obj.model) != -1) {
                    trace("  absorb",obj, "parent="+obj.parent);
                    Utilities.assert(_composite.parent);
					if (obj.snapTo && models.indexOf(obj.snapTo.model) < 0) {  // on a path thats not selected
						oldSnapTo = obj.snapTo;
						oldPathPercent = obj.pathPercent;
						obj.snapTo = null;
					}
					oldPos[obj] = new Point(obj.x, obj.y);
					oldRot[obj] = obj.rotation;
					absorbed.push(obj);
					var view:ObjectView = obj.getView();
					var mcPos:Point = view.computeMessageCenterPosition();
                    ObjectDragMediator.changeParentObject(obj, _composite);
					if (obj.messageCenterRepositioned)
						view.undockMessageCenter(mcPos);  // switch to relative coordinates
				} else {
					++n;  // skip this object -- its not selected
				}
			}
			notifyChildrenOfStatusChange();
			recalcBounds();

			AbstractContainer(parentModel.value).setChildrenZPositions(true);
			fixParentChange(absorbed, oldPos, oldRot);
			
			--_ignoreChildrenMoving;
			
			if (oldSnapTo && _composite.snapTo==null) {
				_composite.snapTo = oldSnapTo;
				_composite.pathPercent = oldPathPercent;
			}
		}

		private function fixParentChange(objects:Vector.<AbstractObject>, oldPos:Dictionary, oldRot:Dictionary):void
		{
			var obj:AbstractObject;
			for each(obj in objects) {
                var posChange:Point = new Point(obj.x, obj.y).subtract(oldPos[obj]);
                var rotChange:Number = obj.rotation - oldRot[obj];
                updateValues(obj, posChange, rotChange, "locationXYR");
                updateValues(obj, posChange, rotChange, "x");
                updateValues(obj, posChange, rotChange, "y");
                updateValues(obj, posChange, rotChange, "rotation");
				obj.setInitialZIndex();
			}
		}

		private function updateValues(obj:Object, posChange:Point, rotChange:Number, property:String):void
		{
			var w:Wire;
			for each(w in ApplicationController.instance.wireController.getWiresForAnchor(obj.anchors[property])) {
				var table:AbstractTable = null;
				var row:int;
				if(w.masterAnchor.hostObject == obj && w.slaveAnchor.hostProperty == "currentStateValues") {
					table = w.slaveAnchor.hostObject as AbstractTable;
					row = w.slaveAnchor.hostPropertyIndex;
				} else if(w.slaveAnchor.hostObject == obj && w.masterAnchor.hostProperty == "currentStateValues") {
					table = w.masterAnchor.hostObject as AbstractTable;
					row = w.masterAnchor.hostPropertyIndex;
				}
				if(table) {
					for(var column:int=0; column<table.numColumns; column++) {
						// the current table cell will not be updated by wire propagation due to delays in triggering
//						if(column+1 == table.column) {
//							continue; // current column should already have gone over the wire
//						}
						var tableValue:Object = table.getStateValue(row, column);
						tableValue = updateValue(tableValue, posChange, rotChange, property);
						table.setStateValue(row, column, tableValue);
					}
				}
			}
			
			var anchor:Object;
			for each(anchor in obj.anchors) {
				const setterWireAnchor:SetterWireAnchor = anchor as SetterWireAnchor;
				if(setterWireAnchor && setterWireAnchor.setterProperty == property) {
					var setterValue:Object = setterWireAnchor.inValue;
					setterValue = updateValue(setterValue, posChange, rotChange, property);
					setterWireAnchor.inValue = setterValue;
				}
			}
		}

		private function updateValue(value:Object, posChange:Point, rotChange:Number, property:String):Object
		{
			switch(property) {
				case "locationXYR":
						var xyr:PositionAndAngle = (value as PositionAndAngle).clone();
						xyr.x += posChange.x;
						xyr.y += posChange.y;
						xyr.angle += rotChange;
						return xyr;
				case "x":
						var x:Number = Number(value);
						if(isNaN(x)) {
							return value;
						}
						x += posChange.x;
						return x;
				case "y":
						var y:Number = Number(value);
						if(isNaN(y)) {
							return value;
						}
						y += posChange.y;
						return y;
				case "rotation":
						var rotation:Number = Number(value);
						if(isNaN(rotation)) {
							return value;
						}
						rotation += rotChange;
						return rotation;
			}
			throw new Error("Unknown property to update", property);
		}
		
		private function selectionChangeListener(event:SelectEvent):void
		{
			removeHiliteFromRolledBreadcrumb();
		}
		
		
		private function get objectToReportOn():TaconiteModel
		{
			if (_model.numChildren == 0)
				return null;
			
			var selection:Vector.<TaconiteModel> = cleanSelection;
			
			// scan composite, front front to back, return first one selected
			var n:int;
			for (n = _model.numChildren-1; n >= 0; n--) {
				var model:TaconiteModel = _model.getChildAt(n);
				if (selection.indexOf(model) != -1)
					return model;
			}
			
			// none of the composite children are selected;  just use the frontmost obj
			return _model.getChildAt(_model.numChildren-1);
		}
		
		private function removeHiliteFromRolledBreadcrumb():void
		{
			// remove hilite from breadcrumb previously rolled over
			if (_velum)
				_composite.dispatchEvent(new CompositeEvent(CompositeEvent.REMOVE_HILITE));
		}

		private function wireCompleteListener(event:WireEvent):void
		{
			// re-create custom ribbon, if internal wire is moved to a different property
			var wire:Wire = event.wire;
			trace("wireComplete:",wire);
			CompositeController.instance.handleNewCompositeWire(_composite, wire);
		}

	}
}

