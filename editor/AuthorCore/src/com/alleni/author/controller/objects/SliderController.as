/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: AuthorEditor.as 1955 2009-09-03 16:56:04Z pkrekelberg $  */

package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.Slider;
	import com.alleni.author.view.objects.SliderView;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.geom.Point;

	/**
	 * Controller the Slider control object
	 * 
	 */
	public class SliderController extends ObjectController
	{
		public static var instance:SliderController = new SliderController();


		static private function getSlider(model:TaconiteModel):Slider
		{
			return model.value as Slider;
		}
		
		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel) : void
		{
			super.handleModelUpdate(model,property,newValue,oldValue,parent);
			
			var slider:Slider = getSlider(model);
			
			switch (property) {
				case "atMinimum":
					if (newValue == true)  // just now reached the bottom
						_wiring.sendTrigger(slider, Modifiers.instance.fetch("hitBottom"));
					break;
				case "atMaximum":
					if (newValue == true)  // just now reached the top
						_wiring.sendTrigger(slider, Modifiers.instance.fetch("hitTop"));
					break;
				case "sliderMinimum":
				case "sliderMaximum":
				case "sliderUnit":
                    slider.forceSetSliderValue(slider.sliderValue);
					break;
				case "complete":
					if (slider.sliderValue == 0) {
						slider.sliderValue = 1;   // force the creation of an initial value, on initial creation of the slider
						slider.sliderValue = 0; 	// WorldMediator will notice this change and create the init
					}
					break;
			}
		}
		
		override public function propertyNeedsInit(property:String, obj:AbstractObject):Boolean
		{
			switch (property) {
				case "knobX": 
				case "knobY":
				case "sliderPercent":
					return false;  // only init sliderValue, not knob position
					break;
			}
			return super.propertyNeedsInit(property, obj);
		}

				
		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			super.handleTrigger(object, modifier);
			var slider:Slider = object as Slider;
			switch (modifier.key)
			{
				case "nudgeDown":
					nudgeDown(slider);
					break;
				
				case "nudgeUp":
					nudgeUp(slider);
					break;
				
				case "setRandom":
					setRandom(slider);
					break;
				
				case "invertNudge":
					invertNudge(slider);
					break;
				
				case "invertAndNudge":
					invertNudge(slider);
					nudgeUp(slider);
					break;

                case "enableButton":
                    slider.interactive = true;
                    break;

                case "disableButton":
                    slider.interactive = false;
                    break;
			}
		}
		
		private function nudgeDown(slider:Slider):void {
			slider.sliderValue = slider.sliderValue - slider.sliderNudgeAmount;
		}
		
		private function nudgeUp(slider:Slider):void {
			slider.sliderValue = slider.sliderValue + slider.sliderNudgeAmount;
		}
		
		private function setRandom(slider:Slider):void {
			
			var range:Number = slider.sliderMaximum - slider.sliderMinimum;
			slider.sliderValue = slider.sliderMinimum + range * Math.random();
		}
		
		private function invertNudge(slider:Slider):void {
			slider.sliderNudgeAmount = -slider.sliderNudgeAmount;
		}

	}
}
