

package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.action.ModifyObjectProperty;
import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractExternalObject;
	import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.model.TaconiteModel;

import mx.events.PropertyChangeEvent;


public class ExternalObjectController extends ObjectController
	{
		public static var instance:ExternalObjectController = new ExternalObjectController();
		
		public function ExternalObjectController()
		{
			super();
		}
		
		protected static function getExternalObject(model:TaconiteModel):AbstractExternalObject
		{
			return model.value as AbstractExternalObject;
		}
		
		override public function initializeObject(model:TaconiteModel) : void
		{
			super.initializeObject(model);
			var obj:AbstractExternalObject = getExternalObject(model);
			
			// if loading the project & we don't have the asset now ... it must the in the process of loading
			if (obj.loading)
				obj.loadingAsset = true;  // prevent resizing the obj when load completes
		}
		
		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
		{		
			super.handleModelUpdate(model,property,newValue,oldValue,parent);
			var externalObject:AbstractExternalObject = getExternalObject(model);
			
			// check to see if our asset has been reset
			switch (property) {
				case "asset":
					handleAssetUpdatedFor(externalObject, oldValue as Asset);
                    // if asset is wired, the anchor will be "replaceable" not "asset"
                    externalObject.dispatchEvent(PropertyChangeEvent.createUpdateEvent(externalObject, "replaceable", oldValue, newValue));
					break;
				case "uri":
					refreshSource(externalObject);
					break;
			}
		}
		
		protected function handleAssetUpdatedFor(object:AbstractExternalObject, oldAsset:Asset):void
		{
			var asset:Asset = object.asset;
			if (asset) {
				if (!object.titleChangedByAuthor)
					setTitleWithURI(object, asset.name);
				
				if (asset.content && object.content != asset.content) {
					
					if (!object.loading && !object.loadingAsset) {
						var assetWidth:Number = 0;
						var assetHeight:Number;
						if(asset.content is XML) { // extract width from svg
							assetWidth = XML(asset.content).@width;
							assetHeight = XML(asset.content).@height;
						} else if ("width" in asset.content && "height" in asset.content) {
							assetWidth = asset.content.width;
							assetHeight = asset.content.height;
						}
												
						if (assetWidth > 0) {
							if (object.initialLoadDimensionsApplied) {  // object previously had a different asset, probably a different width/height
								resizeForNewAsset(object, assetWidth, assetHeight);
							} else {
								object.width = assetWidth;
								object.height = assetHeight;
								object.initialLoadDimensionsApplied = true;   // also done in AuthorController make-graphic-obj
								object.initialValues.initialLoadDimensionsApplied = true;  // ensure it gets into gadget data
							}
						}
					}
					object.content = asset.content;
					object.loadingAsset = false;
				}
			} else {
				object.content = null;
			}
		}

        override public function assetDroppedOnObject(obj:AbstractObject, asset:IReplaceable):void
        {
            var anchor:WireAnchor = findAnchorForObj(obj, Modifiers.instance.fetch("replaceable"), -1);
            if (anchor) {
                assetDroppedOnRibbon(obj, anchor, asset);
            } else {
                replaceTheAssetWithUndo(obj, asset);
            }
        }

        private function replaceTheAssetWithUndo(obj:AbstractObject, asset:IReplaceable):void
        {
            var object:AbstractExternalObject = obj as AbstractExternalObject;   // eg. Audio doesn't have asset ribbon
            var oldAsset:IReplaceable = object.replaceable;
            object.replaceable = asset;
            var title:String = "Set asset in " + obj.title;
            ApplicationController.currentActionTree.commit(ModifyObjectProperty.fromObject(object, title, "replaceable", -1, oldAsset));
        }

        override public function assetReplaceAll(obj:AbstractObject, oldAsset:IReplaceable, newAsset:IReplaceable):void
        {
            super.assetReplaceAll(obj, oldAsset, newAsset);
            var extObj:AbstractExternalObject = obj as AbstractExternalObject;
            if (extObj.replaceable == oldAsset || initialAsset(extObj) == oldAsset.id) {
                replaceTheAssetWithUndo(obj, newAsset);
            }
        }

        private function initialAsset(obj:AbstractExternalObject):String
        {
            if ("replaceable" in obj.initialValues) {
                return Asset.idFromJson(obj.initialValues.replaceable);  // id or null
            }
            return null;
        }

		protected function setTitleWithURI(object:AbstractObject, uri:String):void
		{
			var newTitle:String = Utilities.FILENAME_FROM_URL_REGEX.exec(uri);
			if (newTitle.length > 0) {
				try {
					object.title = decodeURI(newTitle); // decodeURI will die if the URI has a real % symbol, as opposed to a uri-encoded character.
				} catch(e:Error) {
					object.title = newTitle;
				}
			}
		}
		
		protected function refreshSource(externalObject:AbstractExternalObject):void
		{
			// override in subclass for specific functionality
		}
		
		protected function resizeForNewAsset(obj:AbstractExternalObject, w:Number, h:Number):void
		{
			// this function is overridden in DrawingController
			obj.width = w;
			obj.height = h;
		}

        override public function propertyNeedsInit(property:String, obj:AbstractObject):Boolean
        {
            switch (property) {
                case "initialLoadDimensionsApplied":
                    return true;
                    break;
            }
            return super.propertyNeedsInit(property, obj);
        }

	}
}