package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.objects.Calculator;
	import com.alleni.savana.Ast;
	import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;
	
	import mx.collections.IList;


	public class CalculatorController extends ObjectController
	{
		public static var instance:CalculatorController = new CalculatorController();
		
		public function CalculatorController()
		{
			super();
		}
		
		static private function getCalculator(model:TaconiteModel):Calculator
		{
			return model.value as Calculator;
		}
		
		override public function initializeObject(model:TaconiteModel):void
		{
			super.initializeObject(model);
			
			var calculator:Calculator = getCalculator(model);

			if(calculator.loading == false) {
				calculator.calcInputValues.addItem("0");
				calculator.calcOutputExpressions.addItem("");
			}
		}

		public function rebuildAst(calculator:Calculator, row:int):void {
			try {
                var exp:String = calculator.calcOutputExpressions[row];
                if(/^\s*[\+\-\*\/]/.test(exp)) {
                    exp = "#" + exp;
                }
				var ast:Ast = new Ast(exp);

				calculator.calcOutputAsts[row] = ast;
			} catch(error:Error) {
				trace(error.message);
				if(row < calculator.calcOutputAsts.length){
					calculator.calcOutputAsts[row] = null;
				}
			}
		}
		
		public function judgeExpressions(calculator:Calculator):void{
			
			for(var row:int=0; row < calculator.calcOutputExpressions.length; row++) {
				try {
					var ast:Ast = calculator.calcOutputAsts[row];
                    var env:Object = calculator.getDefaultEnvironment();
                    var typeEnv:Object = calculator.getDefaultTypeEnvironment();
                    if(row > 0) {
                        env["#"] = calculator.calcOutputValues[row-1];
                        typeEnv["#"] = calculator.calcOutputTypes[row-1];
                    }
					var type:String = ast.type(typeEnv);
					var result:Object = ast.evaluate(env);
					if(type == null || type == Modifiers.ENUMERATED_TYPE || type == Modifiers.POINT_TYPE) {
						type = Modifiers.STRING_TYPE;
					}
					if(calculator.calcOutputTypes[row] != type){
						calculator.calcOutputTypes[row] = type;
					}
					calculator.calcOutputValues[row] = result;
				} catch(error:Error) {
					if(calculator.calcOutputTypes[row] != Modifiers.STRING_TYPE){
						calculator.calcOutputTypes[row] = Modifiers.STRING_TYPE;
					}
					calculator.calcOutputValues[row] = "?";
				}
			}
		}
		
		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			var calculator:Calculator = object as Calculator; 
			switch (modifier.key)
			{
				case "judgeNow":
					judgeExpressions(calculator);
					break;
			}
		}

		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void {
			super.handleModelUpdate(model, property, newValue, oldValue, parent);

			var calculator:Calculator = getCalculator(model);
			switch(property) {
				case "loading":
					if(newValue == false) {
						for(var row:int=0; row<calculator.calcOutputExpressions.length; row++) {
							if(calculator.calcOutputAsts.length < row+1) {
								calculator.calcOutputAsts.addItem(null);
							}
							rebuildAst(calculator, row);
						}
//                        judgeExpressions(calculator);
					}
			}
		}

		override protected function handleModelCollectionUpdate(model:TaconiteModel, property:String, collection:IList, kind:String, index:int, item:Object, oldItem:Object):void
		{
			super.handleModelCollectionUpdate(model, property, collection, kind, index, item, oldItem);
			
			var calculator:Calculator = getCalculator(model);
			
			if(calculator.rearranging || calculator.loading)
				return;
			switch(kind) {
				case ModelEvent.ADD_ARRAY_ITEM:
					switch(property) {
						case "calcInputValues":
							var defaultName:String = calculator.calcInputDefaultNames.length == 0? "Attach Property..." : "Local Variable";
							calculator.calcInputDefaultNames.addItem(defaultName);
							calculator.calcInputNames.addItem("");
							calculator.calcInputTypes.addItem(Modifiers.STRING_TYPE);
							calculator.calcInputConstraints.addItem(null);
                            if(calculator.continuallyJudging) {
                                judgeExpressions(calculator);
                            }
							break;
						case "calcOutputExpressions":
							calculator.calcOutputValues.addItem("");
							calculator.calcOutputAsts.addItem(null);
							calculator.calcOutputTypes.addItem(Modifiers.STRING_TYPE);
							calculator.calcOutputConstraints.addItem(null);
							break;
					}
					break;
				case ModelEvent.CHANGE_ARRAY_ITEM:
					switch(property) {
						case "calcInputValues":
						case "calcInputNames":
							if(calculator.continuallyJudging){
								judgeExpressions(calculator);
							}
							break;
						case "calcOutputExpressions":
							rebuildAst(calculator, index);
							judgeExpressions(calculator);
							break;
					}
					break;
			}
		}
	}
}