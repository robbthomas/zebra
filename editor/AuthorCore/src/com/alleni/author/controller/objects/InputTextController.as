package com.alleni.author.controller.objects
{
import com.alleni.author.definition.IModifier;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.InputText;
import com.alleni.author.view.objects.InputTextView;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;

import mx.events.PropertyChangeEvent;

public class InputTextController extends TextController
	{
		public static var instance:InputTextController = new InputTextController();

		public function InputTextController()
		{
			super();
		}

        override public function propertyNeedsInit(property:String, obj:AbstractObject):Boolean
        {
            switch (property) {
                case "text": // prevent temporal field value from colliding with prompt
                    return false;
                default:
                    return super.propertyNeedsInit(property, obj);
            }
        }

		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void {
			super.handleModelUpdate(model, property, newValue, oldValue, parent);
			
			var theText:InputText = model.value as InputText;
			if(property == "label") {
				theText.dispatchEvent(PropertyChangeEvent.createUpdateEvent(theText, "text", oldValue, newValue));
			}

            switch(property){
                case "promptText":
                    theText.text = (newValue as String);
                    break;
                case "interactive":
                    if (!newValue) {
                        setFocus(theText, false);
                    }
                    break;
            }
		}

        override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
        {
            super.handleTrigger(object, modifier);
            var obj:AbstractObject = AbstractObject(object);
            switch (modifier.key) {
                // Hide & Show are now done via SetterWireAnchor
                case "giveTextFocus":
                    setFocus(object,true,true);
                    break;
                case "takeTextFocus":
                    setFocus(object, false,true);
                    break;
                case "enableButton":
                    obj.interactive = true;
                    break;
                case "disableButton":
                    obj.interactive = false;
                    break;
            }
        }

        public function textFocusChanged(obj:InputText,nowHasFocus:Boolean):void {
            if (!obj.enabled) return;
            if (nowHasFocus) {
                _wiring.anchorTriggered(obj.anchors["gotTextFocus"]);
            } else {
                _wiring.anchorTriggered(obj.anchors["lostTextFocus"]);
            }
        }


        private function setFocus(object:ITaconiteObject,value:Boolean,asTrigger:Boolean=false):void {
            var input:InputText = object as InputText;
            if (input) {
                if (value && !input.interactive) return;
                var view:InputTextView = input.getView() as InputTextView;
            }
            if (view)
                view.setTextFocus(value);
        }

}
}
