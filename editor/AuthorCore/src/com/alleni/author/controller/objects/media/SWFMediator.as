package com.alleni.author.controller.objects.media
{
	import com.alleni.author.model.objects.SWFObject;
	import com.alleni.taconite.model.ITaconiteObject;
	
	import flash.display.MovieClip;

	public class SWFMediator implements IMediaMediator
	{
		private var _swf:SWFObject;
        private var _playing:Boolean = false;
		
		public function handleMediaEvents(object:ITaconiteObject):IMediaMediator
		{
			_swf = SWFObject(object);
			return this;
		}
		
		public function initialize(fromActivation:Boolean=false):void
		{
			stop();
			
			if (_swf.autorun) 
				start();
		}
		
		public function pauseResume(running:Boolean):void
		{
			if (running) {
				resume();
			} else {
				pause();
			}
		}
		
		public function start(position:Number=0, restart:Boolean=false):void
		{
			MovieClip(_swf.content).gotoAndPlay(position);
            _playing = true;
		}
		
		public function pause():void
		{
			var mc:MovieClip = MovieClip(_swf.content);
			_swf.currentFrame = mc.currentFrame;
			mc.stop();
            _playing = false;
		}
		
		public function togglePause():void
		{
            pauseResume(!playing);
		}
		
		public function resume(fromActivation:Boolean=false):Boolean
		{
			this.start(_swf.currentFrame);
            _playing = true;
			return true;
		}
		
		public function stop():void
		{
			MovieClip(_swf.content).stop();
            _playing = false;
			_swf.currentFrame = 0;
		}
		
		public function get playing():Boolean
		{
			return _playing;
		}
		
		public function record():void
		{
			// nothing to do for SWF media
		}
		
		public function unload():void
		{
			
		}
	}
}