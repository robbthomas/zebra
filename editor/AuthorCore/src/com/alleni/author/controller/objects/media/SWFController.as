package com.alleni.author.controller.objects.media
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractExternalObject;
	import com.alleni.author.model.objects.SWFObject;
	import com.alleni.author.model.ui.Asset;
	
	import flash.display.MovieClip;

	public class SWFController extends MediaController
	{
		public static var instance:SWFController = new SWFController();
		
		override protected function validateMediator(object:AbstractObject):void
		{
			if (!_mediators[object]) _mediators[object] = new SWFMediator().handleMediaEvents(object);
		}
		
		override protected function handleAssetUpdatedFor(object:AbstractExternalObject, oldAsset:Asset):void
		{
			super.handleAssetUpdatedFor(object, oldAsset);
			if(object.content is MovieClip) {
				SWFObject(object).setupContent();
			}
		}
	}
}