package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ComputeMessageCenterPosition;
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.Line;
	import com.alleni.taconite.lang.Geometry;
	import com.alleni.taconite.lang.Trigonometry;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.geom.Point;
	
	public class LineController extends ObjectController
	{
		public static var instance:LineController = new LineController();
		
		
		static private function getLine(model:TaconiteModel):Line
		{
			return model.value as Line;
		}
		
		
	}
}