package com.alleni.author.controller.objects
{
	import caurina.transitions.Tweener;
	
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.OutletDescription;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.model.objects.StateTable;
	import com.alleni.author.model.objects.TruthTable;
	import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.objects.tables.AbstractTableView;
import com.alleni.savana.Ast;
	import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.view.ITaconiteView;
	
	import flash.events.MouseEvent;

import mx.collections.IList;

	/**
	 * Controller especially for the Table family of objects
	 *
	 */
	public class TableController extends ObjectController
	{
		public static var instance:TableController = new TableController();

		static private function getTable(model:TaconiteModel):AbstractTable
		{
			return model.value as AbstractTable;
		}

        override public function initializeObject(model:TaconiteModel):void
        {
			super.initializeObject(model);

			// reset this internal flag
			var table:AbstractTable = getTable(model);
			table.ignorePortChanges = false;
			addColumns(table);

	        if(!table.loading) {
				if((table as TruthTable || table as AnswerTable) && table.port.length < 1) {
					table.port.addItem(""); // ui design dictates one visible at start.
				} else if (table as StateTable) {
					table.currentStateValues.addItem("");
				}

				if(table.numColumns < 1) {
					table.numColumns = 1;
				}
	        }

        	// recalc the "Value" property
		    handleLogicTruthChanged(table, -1, false);
        }

		internal function different(lhs:*,  rhs:*):Boolean
		{
			return lhs  != rhs && !(rhs != rhs);
		}

		protected function handleNewLogicValue(table:AbstractTable, row:int, force:Boolean=false):void
		{
			if(table.poweredOnRun){
				if (!(row in table.truthPort)) {
					LogService.error("TableController found undefined table.truthPort["+row+"]");
					//////////return; // removing punt to explore if this caused table issues
				}
				var oldTruth:Boolean = table.truthPort[row];
				var newTruth:Boolean = table.portBoolean(row);
				
				table.truthPort[row] = newTruth;
				
				if(force || oldTruth != newTruth) {
					handleLogicTruthChanged(table, row, newTruth);
					
					/*if(table is AnswerTable && (table as AnswerTable).getView() as AnswerTableView != null){
						((table as AnswerTable).getView() as AnswerTableView).statusLabel.text = ((table as AnswerTable).getView() as AnswerTableView).statusLabel.text + "- mem";
					}*/
				}
			}
		}
		
		protected function storeTruths(row:int, table:AbstractTable):void
		{
			//Overridden by the Answer Judging Table
		}
		
		override protected function handleModelCollectionUpdate(model:TaconiteModel, property:String, collection:IList, kind:String, index:int, item:Object, oldItem:Object):void
		{
			super.handleModelCollectionUpdate(model, property, collection, kind, index, item, oldItem);
			
			var table:AbstractTable = getTable(model);
			var col:int;
			var i:int;
			var length:int;

			if(table.rearranging || table.loading)
				return;
			switch(kind) {
				case ModelEvent.ADD_ARRAY_ITEM:
					if(property == "port") {
						resizeLogic(table);
						handleNewLogicValue(table, index, true);
					} else if(property == "currentStateValues") {
						length = table.numColumns;
						for(i = 0; i < length; i++) {
							table.allStateValues.addItem("");
						}
						table.stateValueExpressions.addItem("");
						table.stateValueBound.addItem(false);
						table.stateValueName.addItem("");
						table.stateValueAst.addItem(null);
						table.stateValueType.addItem(Modifiers.STRING_TYPE);
						table.stateValueTypeConstraints.addItem(null);
					}
					break;
				case ModelEvent.CHANGE_ARRAY_ITEM:
					var row:int = index / (table.numColumns);
					var column:int = index % (table.numColumns);
					if(property == "port" || property == "portValue") {
						handleNewLogicValue(table, index);
					} else if(property == "currentStateValues") {
						if(table.column > 0 && !Tweener.isTweening(table) && different(table.getStateValue(index, table.column-1), table.currentStateValues[index]) && table.poweredOnRun) {
							table.setStateValue(index, table.column-1, table.currentStateValues[index]);
						}
					} else if(property == "allStateValues") {
						if(table.column > 0 && column == table.column-1 && different(table.currentStateValues[row], table.getStateValue(row, column))) {
							table.currentStateValues[row] = table.getStateValue(row, column);
						}
					} else if(table.column > 0 && !table.ignorePortChanges && property == "currentTweenValues") {
						if(different(table.getTweenValue(index, table.column-1), table.currentTweenValues[index])) {
							table.setTweenValue(index, table.column-1, table.currentTweenValues[index]);
						}
//					} else if(table.column > 0 && property == "allTweenValues") {   // updating currentTweenValues is currently not needed (per Jesse 4/4/11)
//						if(column == table.column-1 && different(table.currentTweenValues[row], table.getTweenValue(row, column))) {
//							table.currentTweenValues[row] = table.getTweenValue(row, column);
//						}
					} else if(property == "stateValueExpressions") {
						var exp:String = table.stateValueExpressions[index] as String;
						if(exp == null || exp == "") {
							table.stateValueAst[index] = null;
							table.stateValueBound[index] = false;
						} else {
							var ast:Ast = null;
							var type:String = null;
							try {
								ast = new Ast(exp);
								var env:Object = Ast.defaultTypeEnvironment();
								for(i = 0; i<index; i++) {
									env[table.stateValueName[i]] = table.stateValueType[i];
								}
								type = ast.type(env);
							} catch(e:Error) {
								LogService.error("AST Exception in TableController: " + e);
								ast = null;
							}
							table.stateValueAst[index] = ast;
							if(ast != null) {
								table.stateValueBound[index] = true;
								if(type != null && different(table.stateValueType[index], type)) {
									table.stateValueType[index] = type;
									table.stateValueTypeConstraints[index] = null; // types from an expression wont have constraints
								}
								length = table.numColumns;
								for(col = 0; col < length; col++) {
									table.recalcValues(index, col);
								}
							} else {
								table.stateValueBound[index] = false;
							}
						}
					} else if(property == "stateValueType") {
						var valueType:String = table.stateValueType[index] as String;
						length = table.numColumns;
						for(col = 0; col < length; col++) {
							var val:* = table.getStateValue(index, col);
							var newVal:* = Modifiers.convertValue(valueType, val);
							table.setStateValue(index, col, newVal);
						}
					}
					break;
			}
		}

		public function handleLogicTruthChanged(table:AbstractTable, row:int, truth:Boolean, startingColumn:int = 0):void
		{
		}

		protected function addColumns(table:AbstractTable):void
		{
			if(table.rearranging)
				return;
			var added:int = table.numColumns - table.allStateValues.length / table.currentStateValues.length;
			var row:int;
			var col:int;
			for(row=0; row < table.currentStateValues.length; row++) {
				for(col=table.numColumns-added; col < table.numColumns; col++) {
					table.allStateValues.addItemAt(table.currentStateValues[row], row*table.numColumns + col);
				}
			}
			added = table.numColumns - table.allTweenValues.length / 3;
			for(row=0; row < 3; row++) {
                var val:String = "0"
                if(row == 0){
                    val = "linear";
                }
                if(table.allTweenValues.length - 1 > row*table.numColumns + col){
                    val = (table.getTweenValue(row, table.activeColumn-1) as String);
                }
                for(col=table.numColumns-added; col < table.numColumns; col++) {
					table.allTweenValues.addItemAt(val, row*table.numColumns + col);
				}
			}
			addOutlets(table, table.numColumns);
		}

		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel) : void
		{
			super.handleModelUpdate(model,property,newValue,oldValue,parent);
			var table:AbstractTable = getTable(model);
			var index:int;

			switch (property) {
				case "rearranging":
					if(table.rearranging == false) {
						handleRearranged(table);
					}
					break;
				case "numColumns":
					if (!table.loading)
						addColumns(table);
					//recalcAllResults(table);
					break;
				case "column":
					if (!table.loading) {
						handleColumnChange(table);
                    }
					if(!Application.running) {
						table.activeColumn = table.column; 
					}
					break;
				case "activeColumn":
					if (!table.loading) {
					    table.column = table.activeColumn;
                    }
					break;
				case "continuallyJudging":
					if (!table.loading) {
						if (table.continuallyJudging) {
							judgeNow(table);
						} else {
							reset(table);
						}
					}
					break;
				case "loading":
					if (!table.loading) {
						addColumns(table);

						for(index=0; index<table.currentStateValues.length; index++) {
							while(table.port.length > table.truthPort.length) {
								table.truthPort.addItem(table.portBoolean(table.truthPort.length));
							}
						}

						for(index=0; index<table.currentStateValues.length; index++) {
							var type:String = table.stateValueType[index] as String;
							if(table.stateValueTypeConstraints.length <= index) {
								table.stateValueTypeConstraints.addItem(null);
								// hack for tables created before this array existed
							}
							var exp:String = table.stateValueExpressions[index] as String;
							if(exp == null || exp == "") {
								table.stateValueAst.addItem(null);
								table.stateValueBound[index] = false;
							} else {
								var ast:Ast = null;
								try {
									ast = new Ast(exp)
								} catch(e:Error) {
									LogService.error("AST Exception in TableController: " + e);
									ast = null;
								}
								table.stateValueAst.addItem(ast);
								table.stateValueBound[index] = true;
							}
						}

                        table.activeColumn = table.column;
                        if(table is AnswerTable) {
                            for(index = 0; index < table.numColumns; index++) {
                                AnswerTable(table).logicColumns[index] =  AnswerTable(table).parse(index);
                            }
                        }
					}
					break;
				case "x":
				case "y":
					ApplicationController.instance.wireController.requestRedrawForObject(table);
					break;
			}
		}

		private function handleRearranged(table:AbstractTable):void
		{
			setCurrentValues(table);
		}

		protected function judgeNow(table:AbstractTable):void
		{
			//Overridden in Answer Judging Controller
		}

		public function reset(table:AbstractTable):void
		{
		}

		private function setCurrentValues(table:AbstractTable):void
		{
			var row:int;
			var length:int = table.currentTweenValues.length;
			for(row = 0; row < length; row++) {
				table.currentTweenValues[row] = table.getTweenValue(row, table.column-1);
			}
			table.handleTween();
		}

		protected function valueOfPorts(table:AbstractTable):int
		{
			var portNum:int;
			var length:int = table.port.length;
			for(portNum = 0; portNum < length; portNum++) {
				if(table.portBoolean(portNum)) {
					table.truthPort[portNum] = true;
				} else {
					table.truthPort[portNum] = false;
				}
			}
			return 0;
		}

		protected function resizeLogic(table:AbstractTable):void
		{
			while(table.port.length > table.portValue.length) {
				var val:Object = table.port[table.portValue.length];
				table.portValue.addItem(val);
			}
			
			while(table.port.length > table.logicRowName.length) {
				table.logicRowName.addItem("");
			}
			
			while(table.port.length > table.truthPort.length) {
				table.truthPort.addItem(true);
			}
		}

		private function handleColumnChange(table:AbstractTable):void
		{
			// if the column number we are changing to has an outlet wire, send the trigger
			if(table.column == 0) {
				return;
			}
			setCurrentValues(table);
		}

		private function addOutlets(table:AbstractTable, numToShow:Number):void
		{
			var list:IList = table.anchors["colPort"];
			var description:OutletDescription = table.modifiers["colPort"];
			var current:Number = list.length;

			for(var i:int = current; i<numToShow; i++) {
				var anchor:WireAnchor = OutletDescription(description).createAnchor(table, i);
				list.addItem(anchor);
			}
		}

		override public function handleMouseDown( view:ITaconiteView, e:MouseEvent) : Boolean
		{
			var table:AbstractTable = getTable(view.model);
			return true;
		}
		
		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			var table:AbstractTable = AbstractTable(object);
			var stateTable:StateTable = table as StateTable;
			var answerTable:AnswerTable = table as AnswerTable;
			switch (modifier.key)
			{
				case "judgeNow":
					judgeNow(table);
					break;
				case "reset":
					reset(table);
					break;
				case "first":
					table.column = 1;
					break;
				case "previous":
					if(table.column > 1) {
						table.column--;
					}
					break;
				case "togglePause":
					if(stateTable) {
						stateTable.ticking = !stateTable.ticking;
						if(stateTable.ticking) {
							stateTable.column++;
						}
					}
					break;
				case "start":
					if(stateTable) {
						stateTable.ticking = true;
						if(stateTable.column == 1) {
							stateTable.column++;
						} else {
							stateTable.column = 1;
						}
                        stateTable.fireBranches = true;
					}
					break;
				case "stop":
					if(stateTable) {
						stateTable.ticking = false;
						stateTable.column = 1;
					}
					break;
				case "pause":
					if(stateTable) {
						stateTable.ticking = false;
					}
					break;
				case "resume":
					if(stateTable) {
						if(stateTable.ticking == false) {
							stateTable.ticking = true;
							stateTable.column++;
						}
					}
					if(answerTable){
						AnswerTableController.instance.handleContiuneBranch(answerTable, answerTable.lastTriggeredRow, answerTable.truthPort[answerTable.lastTriggeredRow], answerTable.column);
					}
					break;
				case "next":
					table.column++;
					break;
				case "last":
					table.column = table.numColumns;
					break;
				case "powerOn":
					table.poweredOnRun = true;
					break;
				case "powerOff":
					table.poweredOnRun = false;
					break;
				default:
					super.handleTrigger(object, modifier);
			}
		}

        override public function assetReplaceAll(obj:AbstractObject, oldAsset:IReplaceable, newAsset:IReplaceable):void
        {
            var view:AbstractTableView = obj.getView(Application.instance.viewContext) as AbstractTableView;
            if (view) {
                view.assetReplaceAll(oldAsset, newAsset);
            }
        }

        override public function propertyNeedsInit(property:String, obj:AbstractObject):Boolean
        {
            switch (property) {
                case "column":
                    return true;
                    break;
            }
            return super.propertyNeedsInit(property, obj);
        }
    }
}
