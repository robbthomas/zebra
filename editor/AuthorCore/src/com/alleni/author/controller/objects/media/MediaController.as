package com.alleni.author.controller.objects.media
{
	import com.alleni.author.controller.objects.ExternalObjectController;
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractExternalObject;
	import com.alleni.author.model.objects.MediaObject;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.util.StringUtils;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.utils.Dictionary;

	public class MediaController extends ExternalObjectController
	{
		protected var _mediators:Dictionary = new Dictionary(true); // key is AbstractObject, a hash of IMediaMediators
		
		override public function initializeObject(model:TaconiteModel) : void
		{
			super.initializeObject(model);
			validateMediator(getObject(model));
		}
		
		override public function warnBeforeDelete(object:AbstractObject):void
		{
            super.warnBeforeDelete(object);
			if (_mediators[object]) {
				IMediaMediator(_mediators[object]).unload();
				_mediators[object] = null;
			}
		}

        override public function propertyNeedsInit(property:String, obj:AbstractObject):Boolean
        {
            switch (property) {
                case "percentElapsed":
                case "percentRemaining":
                    return true;
                default:
                    return super.propertyNeedsInit(property, obj);
            }
        }

        public function mediatorFor(object:MediaObject):IMediaMediator {
            if (_mediators[object]) {
				return IMediaMediator(_mediators[object]);
            }
            return null;
        }
		
		protected function validateMediator(object:AbstractObject):void
		{
			// override in subclass for particular mediator
		}
		
		protected function replaceMediatorForWith(object:AbstractObject, mediator:IMediaMediator):void
		{
			if (_mediators[object]) {
				IMediaMediator(_mediators[object]).unload();
				_mediators[object] = null;
			}
			_mediators[object] = mediator;
		}
		
		override protected function activate(model:TaconiteModel):void
		{
			super.activate(model);

			var mediator:IMediaMediator = IMediaMediator(_mediators[getObject(model)]);
			if (Application.running && mediator) {
				if (!mediator.resume(true)) {
					mediator.initialize(true);
				}
			}
		}
		
		override protected function deactivate(model:TaconiteModel):void
		{
			super.activate(model);

			var mediator:IMediaMediator = IMediaMediator(_mediators[getObject(model)]);
			if (mediator) {
				if (Application.running && mediator.playing)
					mediator.pause();
				else
					mediator.unload();
			}
		}
		
		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			super.handleTrigger(object, modifier);
			
			var mediator:IMediaMediator = _mediators[object];
			
			if (mediator) {
				switch (modifier.key) {
					case "start":
						mediator.start();
						break;
					case "pause":
						mediator.pause();
						break;
					case "togglePause":
						mediator.togglePause();
						break;
					case "resume":
						if (!mediator.resume())
							mediator.start();
						break;
					case "restart":
						mediator.start(0, true);
						break;
					case "stop":
						mediator.stop();
						break;
					case "record":
						mediator.record();
						break;
					default:
						break;
				}
			}
		}
		
		override protected function refreshSource(externalObject:AbstractExternalObject):void
		{
			StringUtils.stripWhitespace(externalObject.uri);
			
			validateMediator(externalObject);
			if (!externalObject.loading && externalObject.uri != AssetDescription.HOSTED_MEDIA_IDENTIFIER && externalObject.uri !="")
				setTitleWithURI(externalObject, externalObject.uri);
		}
	}
}
