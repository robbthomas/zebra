package com.alleni.author.controller.objects
{
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.event.TextObjectEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.TextObject;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.model.ui.VariableWireAnchor;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	
	import flash.events.MouseEvent;

import mx.events.PropertyChangeEvent;


public class TextObjectController extends ExternalObjectController
	{
		public static var instance:TextObjectController = new TextObjectController();
		
		static protected function getText(model:TaconiteModel):TextObject
		{
			return model.value as TextObject;
		}

		override public function initializeObject(model:TaconiteModel):void {
			super.initializeObject(model);
			if(getText(model).loading == false) {
				getText(model).addAnchor(new VariableWireAnchor(getText(model), "x", "0"), false);
			}
		}

		public function makeObject(model:TaconiteModel, content:String):void
		{
			var textObject:TextObject = getText(model);
			
//			if (!textObject.asset) {
//				var asset:Asset = new Asset();
//				asset.type = AssetType.TEXT;
//				asset.mimeType = AssetDescription.getMimeType(".txt");
//				asset.content = content;
//				AssetController.instance.attachAssetFor(textObject, asset);
//			}
//
//			AssetController.instance.informIntentToReplaceAssetFor(model, null, true);
//			AssetController.instance.insertFromText(content, textObject.x, textObject.y);
			textObject.complete = true;
            textObject.active = true;
		}
		
		override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void
		{
			super.handleTrigger(object, modifier);
			
			switch (modifier.key)
			{
				case "reEval":
					(object as TextObject).valid = false;
					break;
				
				default:
					break;
			}
		}

        override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void {
            super.handleModelUpdate(model, property, newValue, oldValue, parent);
            switch (property) {
                case "loading":
                    if (newValue == false) {
                        getText(model).variablesChanged();
                    }
                    break;
                case "lineThickness":  // these properties affect arena layout, since a transparent background affects text bounds
                case "lineAlpha":
                case "fillAlpha":
                    var obj:TextObject = model.value as TextObject;
                    layoutSizeChange(obj);
                    break;
            }
        }

        public function layoutSizeChange(obj:TextObject):void
        {
            // change affecting arena layout
            obj.dispatchEvent(PropertyChangeEvent.createUpdateEvent(obj, "width", obj.width, obj.width));
        }

        override public function handleClick(objectView:ITaconiteView, e:MouseEvent):void
		{
            super.handleClick(objectView, e);

			// single-click to edit text while running, if text not in a gadget, and clickRelease not wired
            var obj:TextObject = objectView.model.value as TextObject;
            var singleClickableAtRuntime:Boolean = obj.sourceTextIsAccessible && !obj.childOfClosedMasterPage
                    && !anchorWired(obj,"click") && !anchorWired(obj,"clickStart") && !anchorWired(obj,"clickRelease");
			if (!Application.uiRunning || singleClickableAtRuntime) {
                if (!objectView.context.info.testView)  // prevent edit-text while recording script, to allow click on text to create a verify step
                    objectView.dispatchEvent(new TextObjectEvent(TextObjectEvent.OPEN_EDITING));
			}
		}

        override protected function triggerable(obj:AbstractObject, key:String):Boolean {
            switch (key) {
                case "click":
                case "clickStart":
                case "clickRelease":
                    return Application.running;
                    break;
                default:
                    return true;
                    break;
            }
        }

		private function anchorWired(obj:AbstractObject, key:String):Boolean
		{
			var anchor:WireAnchor = obj.anchors[key];
			return (anchor && anchor.wired);
		}
	}
}
