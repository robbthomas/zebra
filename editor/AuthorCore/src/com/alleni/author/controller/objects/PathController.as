/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.PathObject;
	import com.alleni.author.view.objects.PathObjectView;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class PathController extends ObjectController
	{
		public static var instance:PathController = new PathController();

		public function PathController()
		{
			super();
		}
		
		static private function getPath(model:TaconiteModel):PathObject
		{
			return model.value as PathObject;
		}

		
		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
		{	
			super.handleModelUpdate(model,property,newValue,oldValue,parent);
			var path:PathObject = getPath(model);
			
			switch (property) {
				case "loading":
					if (newValue == false) {
						if (path.viewBox == null) {
							path.viewBox = new Rectangle(0,0, path.width, path.height);  // LEGACY:  early paths didn't have viewBox
						}
						path.updateScaling();
						path.loadPathNodeAnchors();  // LEGACY: this code is for projects before 6/15/11  anchors are now loaded by gadgetDecoder
					}
					break;
			}
		}
		
		override public function handleClick(objectView:ITaconiteView, e:MouseEvent):void
		{
			PathObjectView(objectView).object.active = true;  // must not be on an inactive page, if got clicked on
			
			super.handleClick(objectView, e);
		}
		
		override public function handleCreationComplete(object:AbstractObject, location:Point):void
		{
			var path:PathObject = object as PathObject;
			if (path.polygonCreation) {
				super.handleCreationComplete(object, location);  // like any other object
			} else {  // path editing now in progress..
				// prevent DropIntoContainerEvent() and other actions of ObjectController
			}
		}
	}
}
