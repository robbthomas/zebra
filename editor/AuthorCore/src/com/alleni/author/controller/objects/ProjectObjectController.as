package com.alleni.author.controller.objects {
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ObjectController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.EventFlowController;
import com.alleni.author.model.objects.ProjectObject;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;

public class ProjectObjectController extends ObjectController {
	public static var instance:ProjectObjectController = new ProjectObjectController();
    public static var pageChanging:Boolean = false;
    public function ProjectObjectController() {
    }

	override public function handleTrigger(object:ITaconiteObject, modifier:IModifier):void {
		super.handleTrigger(object, modifier);
		var p:ProjectObject = object as ProjectObject;
		switch(modifier.key) {
			case "restart":
                if (!ApplicationController.instance.authorController.world.eventFlow.editingPage) {  // ignore if author invokes while editing (wire a button)
                    ApplicationController.instance.authorController.run();
                }
				break;
			case "quit":
                if(EditorUI.instance) {
                    return; // dont quit the editor
                }
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.REQUEST_QUIT_APPLICATION));
				break;
            case "goStartPage":
            case "goNextPage":
            case "goPrevPage":
            case "goEndPage":
            case "goBack":
            case "goForward":
                var world:World = ApplicationController.instance.authorController.world;
                var flow:EventFlow = world.eventFlow;
                EventFlowController(flow.controller).handleTrigger(flow, modifier);
                break;
		}
	}

    override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void {
        super.handleModelUpdate(model, property, newValue, oldValue, parent);
        var p:ProjectObject = model.value as ProjectObject;
        switch(property) {
            case "loading":
                if (newValue == false) {
                    p.active = true;
                }
                break;
            case "nextEventExists":
                if (oldValue != newValue && p.anchors["nextEventExists"]) {
                    (p.anchors["nextEventExists"] as WireAnchor).valueChanged(oldValue);
                }
                break;
            case "prevEventExists":
                if (oldValue != newValue && p.anchors["prevEventExists"]) {
                    (p.anchors["prevEventExists"] as WireAnchor).valueChanged(oldValue);
                }
                break;
            case "currentPage":
                if(newValue != oldValue && Application.uiRunning == false && pageChanging == false){
                    pageChanging = true;
                    AuthorController.instance.editEvent(newValue as EventPage, true);
                    pageChanging = false;
                }
                break;
            case "pageNumber":
                if((newValue is Number || newValue is int) && (newValue > 0 && newValue <= AuthorController.instance.eventFlow.pages.length) && newValue != oldValue && Application.uiRunning == false && pageChanging == false){
                    p.currentPage = (AuthorController.instance.eventFlow.pages.getItemAt((newValue as Number)-1) as EventPage);
                }
                break;
        }
    }

    override public function propertyNeedsInit(property:String, obj:AbstractObject):Boolean
    {
        switch (property) {
            case "currentPage":
            case "pageNumber": // these ribbons are forwards to _flow, so no inits needed here
            case "projectTransition":
            case "initialPage":
            case "initialPageNumber":
            case "transTweenType":
            case "transitionSecs":
                return false;
        }
        return super.propertyNeedsInit(property, obj);
    }
}
}
