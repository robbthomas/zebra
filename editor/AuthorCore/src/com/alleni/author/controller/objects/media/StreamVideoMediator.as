package com.alleni.author.controller.objects.media
{
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.service.LogService;
	
	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.NetStatusEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.media.Camera;
	import flash.media.Microphone;
	import flash.media.SoundCodec;
	import flash.media.SoundTransform;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.utils.Timer;
	
	public class StreamVideoMediator extends VideoMediator implements IMediaMediator
	{
		private static const CONNECT_REATTEMPT_DELAY:int = 2 * 1000; // 2 seconds
		private static const MAX_CONNECT_ATTEMPTS:int = 4;
		private var _attempts:int = 0;
		
		private var _video:Video;
		private var _netStream:NetStream;
		private var _netStreamRecorder:NetStream;
		private var _netStreamURI:String;
		private var _soundTransform:SoundTransform;
		
		override public function handleMediaEvents(object:ITaconiteObject):IMediaMediator
		{
			super.handleMediaEvents(object);
			
			unsetPlayerStatus(METADATA_LOADED);
			if (_videoObject.asset && _videoObject.asset.assetID || _videoObject.uri) {
				start();
				unsetPlayerStatus(PLAYER_STARTED); // this is a bootstrapped start just to get the metadata, not a real start/autostart
			}
			return this;
		}
		
		override protected function initializeState():void
		{
			super.initializeState();
			
			if (_netStream)
				_netStream.close();
		}
		
		override protected function addListeners():void
		{
			super.addListeners();
			_netStream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, handleAsynchError, false, 0, true);
			_netStream.addEventListener(IOErrorEvent.IO_ERROR, handleIOError, false, 0, true);
			_netStream.addEventListener(NetStatusEvent.NET_STATUS, handleNetStatus, false, 0, true);
		}
		
		override protected function clearListeners():void
		{
			super.clearListeners();
			_netStream.removeEventListener(AsyncErrorEvent.ASYNC_ERROR, handleAsynchError);
			_netStream.removeEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			_netStream.removeEventListener(NetStatusEvent.NET_STATUS, handleNetStatus);
		}
		
		private static const CAMERA_SIZE:Point = new Point(360, 240);
		private static const TESTING_STREAM_ID:String = "zebra";
		override public function record():void
		{
			if (playerStatusIs(PLAYER_STARTED))
				unload(); // be assertive about unloading whatever was here to start from scratch
			
			setPlayerStatus(RECORDING);
			
			if (!playerStatusIs(PLAYER_LOADED)) {
				load();
				return;
			}
			
			// set up camera
			var camera:Camera = Camera.getCamera();
			var microphone:Microphone = Microphone.getMicrophone();
			
			camera.setMode(CAMERA_SIZE.x, CAMERA_SIZE.y, 30, false);
			camera.setQuality(10000, 0);
			camera.setKeyFrameInterval(30);
			microphone.codec = SoundCodec.SPEEX;
			microphone.setSilenceLevel(0);
			
			_video.clear();
			_video.attachCamera(camera);
			_video.width = CAMERA_SIZE.x;
			_video.height = CAMERA_SIZE.y;
			_video.attachCamera(camera);
			invalidateViewWith(_video);
			_videoObject.sourceDimensions = CAMERA_SIZE;
			
			// publish the stream by unique identifier
			_netStream.publish(TESTING_STREAM_ID, "record"); // accepts "record" or "append"
			
			// add custom metadata to the header of the .flv file
			var metaData:Object = new Object();
			metaData.width = CAMERA_SIZE.x;
			metaData.height = CAMERA_SIZE.y;
			metaData.description = "Recorded using Zebra"
			_netStream.send("@setDataFrame", "onMetaData", metaData);
			
			// attach the camera and microphone to the server
			_netStream.attachCamera(camera);
			_netStream.attachAudio(microphone);
			
			// set the buffer time to 20 seconds to buffer 20 seconds of video
			// data for better performance and higher quality video
			_netStream.bufferTime = 20;
		}
		
		private function load():void
		{
			var nc:NetConnection = new NetConnection();
			var dataClient:Object = new Object();
			dataClient.onBWDone   = function():void{}
			dataClient.onMetaData = onMetaData;
			dataClient.onCuePoint = function():void{}
			nc.client = dataClient;
			nc.addEventListener(NetStatusEvent.NET_STATUS, handleNetStatus, false, 0, true);
			if (_videoObject.stream || playerStatusIs(RECORDING)) {
				var protocol:String;
				
				if (!playerStatusIs(CONNECT_REJECTED))
					protocol = "rtmp://"; // try RTMP first
				else // then fall through to RTMPT (RTMP tunneled over HTTP, slightly more overhead)
					protocol = "rtmpt://";
				
				var streamerURI:String = protocol + TaconiteFactory.getEnvironmentImplementation().mediaStreamURI;
				
				if (!playerStatusIs(RECORDING))
					streamerURI += "/borealvod"; // video on demand from S3 app
				else
					streamerURI += "/borealrecorder"; // recording app
			
				LogService.debug("Streamer URI: " + streamerURI);
				nc.connect(streamerURI);
			} else
				nc.connect(null);
			
			_attempts++;
		}
		
		override protected function handleConnectSuccess(event:NetStatusEvent):void
		{
			unsetPlayerStatus(CONNECT_REJECTED); // we're either successfully connected, or trying again
			
			var nc:NetConnection = event.target as NetConnection;
			_netStream = new NetStream(nc);
			var dataClient:Object = new Object();
			dataClient.onMetaData = onMetaData;
			dataClient.onXMPData = onXMPData;
			_netStream.client = dataClient;
			_soundTransform = new SoundTransform(1);
			_netStream.inBufferSeek = true;
			
			_video = new Video(_videoObject.width, _videoObject.height);
			_video.attachNetStream(_netStream);
			
			setHighQuality(_videoObject.highQuality);
			setPlayerStatus(PLAYER_LOADED);
			
			if (playerStatusIs(PLAYER_STARTED))
				play();
			else if (playerStatusIs(RECORDING))
				record();
			else // grabbing metadata, not a real start/play request
				play();
		}
		
		override protected function handleConnectFailed():void
		{
			super.handleConnectFailed();
			if (_attempts < MAX_CONNECT_ATTEMPTS) {
				if (_attempts == 1)
					load();
				else {
					var reattemptTimer:Timer = new Timer(CONNECT_REATTEMPT_DELAY, 1);
					reattemptTimer.addEventListener(TimerEvent.TIMER, function():void {
						LogService.debug("Reattempting connection for '" + _videoObject.title + "'");
						load();
					});
					reattemptTimer.start();
				}
			} else
				handleUnrecoverableError();
		}
		
		override public function start(position:Number = 0, restart:Boolean=false):void
		{
			if (!playerStatusIs(METADATA_LOADED) && playerStatusIs(PLAYER_PLAYING)) {
				setPlayerStatus(PLAYER_STARTED);
				return;
			}
			if (!playerStatusIs(PLAYER_PLAYING) || playerStatusIs(PLAYER_PAUSED)) {
				unsetPlayerStatus(PLAYER_PAUSED);
				setPlayerStatus(PLAYER_STARTED);
			
				if (_videoObject.uri == AssetDescription.HOSTED_MEDIA_IDENTIFIER
						&& _videoObject.asset
						&& !playerStatusIs(PLAYER_LOADED)) {
					
					if (_videoObject.stream && Utilities.UUID_REGEX.test(_videoObject.asset.assetID)) {
						_netStreamURI = _videoObject.asset.fileID;
						
						// _netStreamURI = "amazons3/com.alleni.zebra.assets/" + _netStreamURI; // for the wowza vod streamer to access s3 securely
						
						if (_videoObject.asset.mimeType == "video/x-flv")
							_netStreamURI = _netStreamURI.split(".flv")[0];
						else
							_netStreamURI = "mp4:" + _netStreamURI;
					} else if (!Utilities.UUID_REGEX.test(_videoObject.asset.assetID)) {
						LogService.error("Request to play video '" + _videoObject + "' cannot be fulfilled: need non-null asset ID");
					} else {
						/*_netStreamURI = TaconiteFactory.getEnvironmentImplementation().apiURI + "/asset/stream/" + _videoObject.asset.assetID;
						play(position);*/
						LogService.error("Request to play video '" + _videoObject + "' cannot be fulfilled: progressive media playback from Zebra asset not supported.");
						return;
					}
				} else {
					if (!AssetDescription.FLASH_VIDEO_REGEX.exec(_videoObject.uri)) {
						LogService.error("Request to play video '" + _videoObject + "' cannot be fulfilled: unsupported URL '"+_videoObject.uri+"'");
						return;
					}
				}
				play(position);
			} else {
				if (restart)
					play(0);
			}
		}
		
		override protected function play(position:Number=0):void
		{
			if (!playerStatusIs(PLAYER_LOADED)) {
				load();
				return;
			}

			if (position != _netStream.time) {
				_netStream.resume();
				_netStream.seek(position);
			} else {
				var uri:String = _netStreamURI?_netStreamURI:_videoObject.uri;
				LogService.debug("Playing video from: " + uri);
				_netStream.play(uri);
			}
			addListeners();

			setVolume();
			_cuePointsHandler.clear();
			addCuePoints();
			_videoObject.buffering = true;
			watchLoading = true;
			setPlayerStatus(PLAYER_PLAYING);
		}
		
		override protected function handleWatchLoading(event:Event):void
		{
			updateFractionLoaded(_netStream.bytesLoaded, _netStream.bytesTotal);
		}
		
		override public function pause():void
		{	
			if (playerStatusIs(METADATA_LOADED)) {
				_netStream.pause();
				clearListeners();
			}
			setPlayerStatus(PLAYER_PAUSED);
            unsetPlayerStatus(PLAYER_PLAYING);
		}
		
		override public function resume(fromActivation:Boolean=false):Boolean
		{	
			if (playerStatusIs(PLAYER_PAUSED)) {
				if (playerStatusIs(PLAYER_LOADED)) {
					addListeners();
					_netStream.resume();
				} else
					start();
				unsetPlayerStatus(PLAYER_PAUSED);
				return true;
			}
			return false;
		}
		
		override protected function setTime(time:Number):void
		{
			super.setTime(time);
			_netStream.seek(time);
		}
		
		override protected function setVolume():void
		{
			if (Application.instance.audioEnabled)
				_soundTransform.volume = _videoObject.volume/100.0;
			else
				_soundTransform.volume = 0;
				
			_netStream.soundTransform = _soundTransform;
		}
		
		override protected function setOffset(offset:Number):void
		{
			if (safeToUseOffset(offset, _netStream.time))
				_netStream.seek(offset/100 * _duration);
		}
		
		override protected function setHighQuality(value:Boolean):void
		{
			if (playerStatusIs(METADATA_LOADED)) {
				if (value) { 
					_video.smoothing = true; 	// interpolation will be applied on scaled video
					_video.deblocking = 0; 		// decompressor will apply filters as needed to manage artifacts
				} else { 
					_video.smoothing = false; 	// no expensive interpolation
					_video.deblocking = 1; 		// no filters will be applied, cheaper
				}
			}
		}
		
		override protected function onMetaData(data:Object):void
		{
			// data.framerate
			_duration = data.duration;
			_mediaObject.duration = Math.floor(_duration*100)/100;
			if (!playerStatusIs(METADATA_LOADED)) {
				setPlayerStatus(METADATA_LOADED);
				var dimensions:Point;

				if (data != null && "width" in data) { // if the metadata contain dimensions
					_video.width = data.width;
					_video.height = data.height;
					dimensions = new Point(data.width, data.height); // assign dimensions based on the metadata, what it says its dimensions are
				} else
					dimensions = new Point(_video.width, _video.height); // didn't come in the metadata, let the stream dictate its dimensions
				invalidateViewWith(_video);
				_videoObject.sourceDimensions = dimensions;
				
				if (!playerStatusIs(PLAYER_STARTED) 
					|| playerStatusIs(PLAYER_STARTED) && playerStatusIs(PLAYER_PAUSED)) {
					pause();
					if (!_videoObject.preload)
						initializeState();
					unsetPlayerStatus(PLAYER_PAUSED);
					unsetPlayerStatus(PLAYER_STARTED);
					unsetPlayerStatus(PLAYER_PLAYING);
				}
			}
			super.onMetaData(data);
		}
		
		override protected function updateTimingTick(event:Event):void
		{
			updateTiming(_netStream.time, _duration);
		}
		
		override public function unload():void
		{
			super.unload();
			
			_attempts = 0;
			if (_netStream)
				_netStream.pause();
			_netStream = null;
			_soundTransform = null;
			_video = null;
		}
	}
}