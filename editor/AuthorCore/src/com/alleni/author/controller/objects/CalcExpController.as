package com.alleni.author.controller.objects
{
	import com.alleni.author.controller.ObjectController;
	import com.alleni.author.model.objects.CalcExp;
	import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.model.TaconiteModel;
	
	import mx.collections.IList;
	
	public class CalcExpController extends ObjectController
	{	
		public static var instance:CalcExpController = new CalcExpController();
		
		public function CalcExpController()
		{
		}
		
		override protected function handleModelCollectionUpdate(model:TaconiteModel, property:String, collection:IList, kind:String, index:int, item:Object, oldItem:Object):void {
			super.handleModelCollectionUpdate(model, property, collection, kind, index, item, oldItem);
			var c:CalcExp = model.value as CalcExp;
			switch(kind) {
				case ModelEvent.ADD_ARRAY_ITEM:
					if(property == "input" && c.inputName.length-1 < index) {
						var code:Number = 64; // one before A
						for each(var name:String in c.inputName) {
							if(name.length == 1) {
								code = Math.max(code, name.charCodeAt(0));
							}
						}
						c.inputName.addItemAt(String.fromCharCode(code+1), index);
					}
				case ModelEvent.CHANGE_ARRAY_ITEM:
					c.recalc();
					break;
			}
		}
		
		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
		{
			super.handleModelUpdate(model,property,newValue,oldValue,parent);
			var c:CalcExp = model.value as CalcExp;
			switch (property) {
				case "expression":
					c.reparse();
			}
		}
	}
}