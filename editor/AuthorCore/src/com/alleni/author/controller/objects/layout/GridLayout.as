/**
 * © Copyright 2011 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: ObjectView.as 9948 2011-04-23 16:20:48Z pkrekelberg $
 */
package com.alleni.author.controller.objects.layout
{
import caurina.transitions.Tweener;

import com.alleni.author.controller.objects.ArenaLayoutManager;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.objects.ArenaPageView;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObjectContainer;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Dictionary;

import mx.collections.IList;

public class GridLayout extends Layout
	{
		public static var instance:GridLayout = new GridLayout();
		
		
		/**
		 * Calculates object placement in advance, then 
		 * applies results to objects.
		 * 
		 * @param arena
		 * @param objects
		 * 
		 */
		override protected function calculatePlacements(pageView:ArenaPageView, animate:Boolean=true):void
		{
            var page:ArenaPage = pageView.object as ArenaPage;
//            trace("calculatePlacements", page.title, "rows="+page.rows,  "cols="+page.columns, "anim="+animate);
            var context:ViewContext = pageView.context;
            if (page.columns && page.objects.length) {
                var cells:Vector.<Cell> = buildCellArray(page, context);

                for each (var cell:Cell in cells){
                    if (cell.obj && cell.obj.draggingHandle)
                        animate = false;
                }

                applyCoordsToObjects(page, context, cells, animate);

                if(!animate) {
                    // if we are tweening, this gets added as the end tween
                    calculateContentSize(page, context, cells);
                }
            }
        }

        private function buildCellArray(page:ArenaPage, context:ViewContext):Vector.<Cell>
        {
            var objects:IList = page.objects;
			var rows:Number = page.rows;
			var columns:Number = page.columns;
			var cells:Vector.<Cell> = new Vector.<Cell>();
			var rowDescriptions:Vector.<Description> = new Vector.<Description>();
			var columnDescriptions:Vector.<Description> = new Vector.<Description>();
			//
			var currentRow:Number = -1;
			var currentCol:Number = 0;
			var previousX:Number = 0;
			var previousY:Number = 0;
			var xOffsetForAlign:Number; // defaults isNaN
			var yOffsetForAlign:Number; // defaults isNaN
			var obj:AbstractObject;
			var bounds:Rectangle;
			var ct:uint;
			var i:uint;
			
            /**
             * creating cells & storing height and width
             */
            for (ct = 0; ct<objects.length; ct++)
            {
                obj = objects.getItemAt(ct) as AbstractObject;

                // create a cell for each visible obj, but not obj on path or obj dragged in during this drag
                if(obj.visible && obj.snapTo == null && !(page.transientObjsDraggedIn && obj.uid in page.transientObjsDraggedIn))
                {
                    var cell:Cell = new Cell();
                    bounds = getObjBounds(page, obj, context);
                    cell.width = bounds.width;
                    cell.height = bounds.height;
                    cell.obj = obj;
                    cells.push(cell);
                }
            }
            if(page.growHorizontal)
            {
                page.rows = rows = 1;
                page.columns = columns = Math.ceil(cells.length/rows);
            }else{
                page.rows = rows = Math.ceil(cells.length/columns);
            }
            /**
             * generating descriptions for all rows/columns
             */
            for (ct = 0; ct<rows; ct++)
            {
                rowDescriptions.push(gatherRowDescription(ct,cells,columns));
            }
            for (ct = 0; ct<columns; ct++)
            {
                columnDescriptions.push(gatherColumnDescription(ct,cells,columns));
            }
            // update the model in case these values
            // changed from growHorizontal condition
            if(page.growHorizontal) {
                page.columns = columns = columnDescriptions.length;
            }
            page.rows = rows = rowDescriptions.length;
            /**
             * calculating x/y
             */
            //trace("LAYOUT:: calculating x/y first pass -------------------");
            for (ct = 0; ct<cells.length; ct++)
            {

                var prevCell:Cell = (ct==0)?null:cells[ct-1];

                if(ct%columns)
                {
                    ++currentCol;
                }else{
                    currentCol = 0;
                    ++currentRow;
                    if(prevCell)
                    {
                        previousY = prevCell.y +prevCell.height;
                    }else{
                        previousY = cells[ct].y +cells[ct].height;
                    }
                    if(previousY > rowDescriptions[currentRow].maxY)
                    {
                        rowDescriptions[currentRow].maxY = previousY;
                    }

                }
                // store maxX on column details
                if(prevCell && currentCol) columnDescriptions[currentCol].maxX = previousX+ prevCell.width;
                // set x (including marginLeft if is first column)
                cells[ct].x = columnDescriptions[currentCol].maxX;

                // set width
                cells[ct].width = columnDescriptions[currentCol].maxWidth + page.hGap*2;
                // set height
                cells[ct].height = rowDescriptions[currentRow].maxHeight + page.vGap*2;
                // store x location for next cell(s)
                previousX = cells[ct].x;
                // set y (including marginTop if is first row)
                cells[ct].y = (currentRow == 0)?0:rowDescriptions[currentRow].maxY;

                //trace("LAYOUT:: cell "+ct+" - x:"+cells[ct].x);
                //trace("LAYOUT:: cell "+ct+" - y:"+cells[ct].y);

            }

            /**
             * Apply Arena Alignment
             *
             */
            currentCol = 0;
            currentRow = -1;
            previousX = 0;
            previousY = 0;
            //trace("LAYOUT:: calculating x/y arena alignment pass -------------------");
            for (ct = 0; ct<cells.length; ct++)
            {

                var prevCell2:Cell = (ct==0)?null:cells[ct-1];

                if(ct%columns)
                {
                    ++currentCol;
                }else{
                    currentCol = 0;
                    ++currentRow;
                    if(prevCell2)
                    {
                        previousY = prevCell2.y +prevCell2.height;
                    }else{
                        previousY = cells[ct].y +cells[ct].height;
                    }
                    if(previousY > rowDescriptions[currentRow].maxY)
                    {
                        rowDescriptions[currentRow].maxY = previousY;
                    }

                }
                // store maxX on column details
                if(prevCell2 && currentCol) columnDescriptions[currentCol].maxX = previousX+ prevCell2.width;


                // apply horizontal arena alignment & margins if is first column
                if(currentCol == 0)
                {
                    if(isNaN(xOffsetForAlign))
                    {
                        switch(page.alignObjsHorz)
                        {
                            case Layout.ALIGN_LEFT:
                                // do nothing
                                break;
                            case Layout.ALIGN_RIGHT:
                                xOffsetForAlign = cells[ct].x +=
                                (page.width-(page.marginLeft+page.marginRight+(page.marginAll*2)))
                                -rowDescriptions[currentRow].totalCellsWidth;
                                break;
                            case Layout.ALIGN_CENTER:
                                xOffsetForAlign = cells[ct].x +=
                                ((page.width-(page.marginLeft+page.marginRight+(page.marginAll*2)))/2)
                                -(rowDescriptions[currentRow].totalCellsWidth/2);
                                break;
                        }
                    }else{
                        cells[ct].x += (!isNaN(xOffsetForAlign)?xOffsetForAlign:0);
                    }
                    cells[ct].x += page.marginLeft+page.marginAll-page.hGap;
                }else{
                    cells[ct].x = columnDescriptions[currentCol].maxX;
                }

                // apply vertical arena alignment & margins if is first column
                if(currentRow == 0)
                {
                    if(isNaN(yOffsetForAlign))
                    {
                        if(columnDescriptions[currentCol].totalCellsHeight < page.height){
                            switch(page.alignObjsVert)
                            {
                                case Layout.ALIGN_TOP:
                                    // do nothing
                                    break;
                                case Layout.ALIGN_BOTTOM:
                                    yOffsetForAlign = cells[ct].y +=
                                            (page.height-(page.marginTop+page.marginBottom+(page.marginAll*2)))
                                                    -columnDescriptions[currentCol].totalCellsHeight;
                                    break;
                                case Layout.ALIGN_MIDDLE:
                                    yOffsetForAlign = cells[ct].y +=
                                            ((page.height-(page.marginTop+page.marginBottom+(page.marginAll*2)))/2)
                                                    -(columnDescriptions[currentCol].totalCellsHeight/2);
                                    break;
                            }
                            /*
                            trace('-------------------------------------');
                            trace('cells[ct].y='+cells[ct].y);
                            trace('page.height='+page.height);
                            trace('page.marginTop='+page.marginTop);
                            trace('page.marginBottom='+page.marginBottom);
                            trace('page.marginAll='+page.marginAll);
                            trace('columnDescriptions[currentCol].totalCellsHeight='+columnDescriptions[currentCol].totalCellsHeight);
                            trace('[ yOffsetForAlign => '+yOffsetForAlign+' ]');
                             */
                        }
                    }else{
                        cells[ct].y += (!isNaN(yOffsetForAlign)?yOffsetForAlign:0);
                    }
                    cells[ct].y += page.marginTop+page.marginAll - page.vGap;
                }else{
                    cells[ct].y = rowDescriptions[currentRow].maxY - page.vGap;
                }

                previousX = cells[ct].x;
                //trace("LAYOUT:: currentrow: "+currentRow+" currentcolumn:"+currentCol);
                //trace("LAYOUT:: cell "+ct+" - x:"+cells[ct].x);
                //trace("LAYOUT:: cell "+ct+" - y:"+cells[ct].y);
            }
            return cells;
		}

        private function applyCoordsToObjects(page:ArenaPage, context:ViewContext, cells:Vector.<Cell>, animate:Boolean):void
        {
            var ct:uint;

            for (ct = 0; ct<cells.length; ct++)
            {
                var obj:AbstractObject = cells[ct].obj;
                var view:ObjectView = getView(obj, context);
                if(view) // was null when page animated transition was occuring
                {
                    // stop object from being draggable
                    // so we dont get recursive resizing
                    view.preventDrag();
                    // skip dragging & draggingHandle so we dont
                    // get recursive resizing while
                    if(!obj.draggingHandle && !obj.dragging && !obj.excludeFromContainerLayouts)
                    {
                            // checking to see if any objects are being resized to stop all animations
                            // except for straight x/y changes
                            positionObjectInCell(obj,view,cells[ct],page, context, cells, animate);
                    } else {
//                        trace("-  draggingHandle="+obj.draggingHandle, "dragging="+obj.dragging, "excludeFromContainerLayouts="+obj.excludeFromContainerLayouts);
                    }
                    // re-enable dragging for the object
                    view.preventDrag(false);
                }
            }
        }
		
		private function calculateContentSize(page:ArenaPage, context:ViewContext, cells:Vector.<Cell>):void
		{
			var bounds:Rectangle;
			var contentWidth:Number = 0;
			var contentHeight:Number = 0;
			var obj:AbstractObject;
			for (var ct:uint = 0; ct<cells.length; ct++)
			{
				obj = cells[ct].obj;
				bounds = getObjBounds(page, obj, context);
                if (bounds.x > Layout.MAX_COORD || bounds.y > Layout.MAX_COORD || bounds.width > Layout.MAX_COORD || bounds.height > Layout.MAX_COORD) {
                    return;  // don't update contentSize while a child view has invalid coordinates
                }
				if((bounds.x +bounds.width+page.marginRight) > contentWidth) contentWidth = (bounds.x +bounds.width+page.marginRight+page.hGap);
				if((bounds.y +bounds.height+page.marginBottom) > contentHeight) contentHeight = (bounds.y +bounds.height+page.marginBottom+page.vGap);
			}
			page.contentWidth = contentWidth + page.marginAll;     // note early return above
			page.contentHeight = contentHeight + page.marginAll;
		}
		
		/**
		 * Adjusts objects x/y values based
		 * on row/column calculations, alignment,
		 * VGap and HGap.
		 * 
		 * @param obj
		 * @param cell
		 * @param arena
		 * 
		 */
		private function positionObjectInCell(obj:AbstractObject, view:ObjectView, cell:Rectangle, page:ArenaPage, context:ViewContext, cells:Vector.<Cell>, animate:Boolean=true):void
		{
//			trace("LAYOUT:: positioning pass -------------------");
			var pos:Point = computeObjXY(obj, cell, page, context);
            cancelTween(page, obj,  "x");
            cancelTween(page, obj,  "y");
			if(animate && view.stage)
			{
				if(obj.x != pos.x) {
                    startTween(page, obj, context, cells, "x", pos.x);
                }
                if (obj.y != pos.y) {
                    startTween(page, obj, context, cells, "y", pos.y);
                }
			}else{
				obj.x = pos.x;
				obj.y = pos.y;
			}
//			trace("LAYOUT:: cell - x:"+obj.x);
//			trace("LAYOUT:: cell - y:"+obj.y);
		}

        private function computeObjXY(obj:AbstractObject, cell:Rectangle, page:ArenaPage, context:ViewContext):Point
        {
            var bounds:Rectangle = getObjBounds(page, obj, context);
            var view:ObjectView = getView(obj, context);
            var x:Number = cell.x + (view.x - bounds.x) + page.hGap;
            var y:Number = cell.y + (view.y - bounds.y) + page.vGap;
            // set vertical alignment
            switch(page.alignCellsVert)
            {
                case Layout.ALIGN_TOP:
                    // do nothing
                    break;
                case Layout.ALIGN_BOTTOM:
                    y += cell.height-bounds.height;
                    break;
                case Layout.ALIGN_MIDDLE:
                default:
                    y += (cell.height-bounds.height)/2;
                    break;
            }
            // set horizontal alignment
            switch(page.alignCellsHorz)
            {
                case Layout.ALIGN_LEFT:
                    // do nothing
                    break;
                case Layout.ALIGN_RIGHT:
                    x += cell.width-bounds.width;
                    break;
                case Layout.ALIGN_CENTER:
                default:
                    x += (cell.width-bounds.width)/2;
                    break;
            }
            return new Point(x, y);
        }

        private function cancelTween(page:ArenaPage, obj:AbstractObject, prop:String):void
        {
            var key:String = keyForProperty(prop,  obj);
            if (key in page.transientTweens) {
                delete page.transientTweens[key];
                Tweener.removeTweens(obj,  prop);
            }
        }

        private function startTween(page:ArenaPage, obj:AbstractObject, context:ViewContext, cells:Vector.<Cell>, prop:String, dest:Number):void
        {
            var key:String = keyForProperty(prop,  obj);
            var args:Object = {transition:page.layoutTweenType, time:page.layoutTweenTime, onComplete:function():void {monitorTweenComplete(page,context,key,cells)}};
            args[prop] = dest;
            Tweener.addTween(obj, args);
            page.transientTweens[key] = true;  // register tween

            // set initial value when author adjusts obj position while running
            // (since running=false now, but it will be true when tween completes)
            if (page.transientAuthorDrag) {
                obj.initialValues[prop] = dest;
            }
        }

        /**
         * Handles tween completion and determines whether or not the
         * arena is really doing doing the layout recalcuation.
         * @param arena
         * @param obj
         * @param cells
         *
         */
        private function monitorTweenComplete(page:ArenaPage, context:ViewContext, key:String, cells:Vector.<Cell>=null):void
        {
            if (key in page.transientTweens) {
                delete page.transientTweens[key];
            }
            if(tweensActive(page)) {
//                trace("- MORE TWEENS")
            } else {
                if (cells != null) {
                    calculateContentSize(page, context, cells);
                }
//                trace("-- ALL DONE TWEENS")
            }
        }

        private function keyForProperty(prop:String, obj:AbstractObject):String
        {
            return prop + "." + obj.uid;
        }

        private function tweensActive(page:ArenaPage):Boolean
        {
            for each (var thing:* in page.transientTweens) {
                return true;  // at least one tween is still going
            }
            return false;
        }

		/**
		 * Retrieves a single row of cells.
		 *  
		 * @param rowNum
		 * @param cells
		 * @param columns
		 * @return 
		 * 
		 */
		private function getRow(rowNum:Number,cells:Vector.<Cell>, columns:Number):Vector.<Cell>
		{
			var currentRow:Number = -1;	
			var row:Vector.<Cell> = new Vector.<Cell>();
			var i:uint;

			for (i = 0; i<cells.length; i++)
			{
				if(!(i%columns))
				{
					++currentRow;
				}
				if(rowNum == currentRow)
				{
					row.push(cells[i]);
				}
			}
			
			return row;
		}
		/**
		 * Retrieves a single column of cells.
		 *  
		 * @param columnNum
		 * @param cells
		 * @param columns
		 * @return 
		 * 
		 */		
		private function getColumn(columnNum:Number, cells:Vector.<Cell>, columns:Number):Vector.<Cell>
		{
			var currentCol:Number = 0;
			var column:Vector.<Cell> = new Vector.<Cell>();
			var i:uint;
			for (i = 0; i<cells.length; i++)
			{
				if(i%columns) 
				{
					++currentCol;
				}else{
					currentCol = 0;
				}
				if(columnNum == currentCol)
				{
					column.push(cells[i]);
				}
			}
			
			return column;
		}
		
		/**
		 * Loops through the cells and calculates size
		 * for a specific row.
		 *  
		 * @param row
		 * @param cells
		 * @param columns
		 * @return 
		 * 
		 */
		private function gatherRowDescription(rowNum:Number,cells:Vector.<Cell>,columns:Number):Description
		{
			var currentRow:Number = -1;
			var currentCol:Number = 0;		
			var row:Vector.<Cell> = getRow(rowNum,cells,columns);
			var i:uint;
			var config:Description = new Description(row);

			if(row.length)
			{
				for(i = 0; i<row.length; i++)
				{
					if(row[i].width > config.maxWidth) config.maxWidth = row[i].width;
					if(row[i].height > config.maxHeight) config.maxHeight = row[i].height;
					if(row[i].width < config.minWidth) config.minWidth = row[i].width;
					if(row[i].height < config.minHeight) config.minHeight = row[i].height;
				}
			}
			
			return config;
		}
		
		/**
		 * Loops through the cells and calculates size
		 * for a specific column.
		 *  
		 * @param column
		 * @param cells
		 * @param columns
		 * @return 
		 * 
		 */
		private function gatherColumnDescription(columnNum:Number,cells:Vector.<Cell>,columns:Number):Description
		{
			var currentRow:Number = -1;
			var currentCol:Number = 0;
			var column:Vector.<Cell> = getColumn(columnNum,cells,columns);
			var i:uint;
			var config:Description = new Description(column);

			if(column.length)
			{
				for(i = 0; i<column.length; i++)
				{
					if(column[i].width > config.maxWidth) config.maxWidth = column[i].width;
					if(column[i].height > config.maxHeight) config.maxHeight = column[i].height;
					if(column[i].width < config.minWidth) config.minWidth = column[i].width;
					if(column[i].height < config.minHeight) config.minHeight = column[i].height;
				}
			}

			return config;
		}

        public function prepareDragging(pageView:ArenaPageView):void
        {
            var page:ArenaPage = pageView.object as ArenaPage;
            var context:ViewContext = pageView.context;
            var objects:IList = page.objects;
            finishAllTweens(page, objects);
            ArenaLayoutManager.instance.requestRecalcLayout(pageView, false, true);  // animate=false, force=true
            var cells:Vector.<Cell> = buildCellArray(page, context);
            page.transientDraggingCells = cells;

            // update the cells array for dragging:  cell.objBounds, cell.objDragging
            var container:DisplayObjectContainer = pageView.childViewContainer;
            for (var n:int = 0; n < cells.length; n++) {
                var cell:Cell = cells[n];
                var obj:AbstractObject = cell.obj;
                var view:ObjectView = getView(obj, context);
                var bounds:Rectangle = view.getBounds(container);
                var pos:Point = computeObjXY(obj, cell,  page, context);
                var dx:Number = pos.x - view.x;
                var dy:Number = pos.y - view.y;
                bounds.x += dx;  // difference between where layout would place obj, to where it actually is now (due to obj.dragging preventing obj placement)
                bounds.y += dy;
                cell.objBounds = bounds;
                cell.objDragging = obj.dragging;
            }
        }

        private function finishAllTweens(page:ArenaPage,  objects:IList):void
        {
            for each (var obj:AbstractObject in objects) {
                obj.cancelLocationTweens();
            }
            page.transientTweens = new Dictionary();
        }

        public function finishDragging(page:ArenaPage):void
        {
            page.transientDraggingCells = null;
        }

        public function cellIndexForInsertion(page:ArenaPage, local:Point):int
        {
            var cells:Vector.<Cell> = page.transientDraggingCells as Vector.<Cell>;
            if (cells == null)
                return -1;
            if (cells.length == 0)
                return 0;

            // test individual cells within the grid
            var result:int = 0;
            for (var n:int = 0; n < cells.length; n++){
                var cell:Cell = cells[n];
                if (cell && cell.containsPoint(local)) {
//                  trace("INS grid result="+result);
                    return result;
                } else if (!cell.objDragging) {  // skip over cells vacated by objects being dragged
                    result = n+1;
                }
            }
            if (result == 0) // every cell is being dragged
                return result;
            // result is used below

            // test margin areas
            var cellCount:int = result;  // cell-count that omits dragging-cells at end
            var colCount:int = Math.max(1, page.columns);
            var rowCount:int = (cellCount+colCount-1) / colCount; // rowCount includes a partial row at the bottom
            var col:int = colForX(local.x, cells, colCount);
            var row:int = rowForY(local.y, cells,  colCount);
//          trace("INS: col="+col, "row="+row, "cols="+colCount, "rows="+rowCount, "cells="+cellCount);
            if (local.y < cells[0].y) { // top side
                result = col;
            } else if (local.x < cells[0].x) {  // left side
                result = row * colCount;
            } else if (local.y > cells[cellCount-1].bottom && colCount > 1) { // bottom side
                result = Math.min(cellCount, col + (rowCount-1) * colCount);
            } else { // right side
                result = Math.min(cellCount, (row+1) * colCount);
            }

            // backup if right after a vacancy
            while (result > 0 && (result > cells.length || cells[result-1].objDragging)) {
                --result;
            }
//            trace("INS: result="+result);
            return result;
        }

        private function colForX(x:Number, cells:Vector.<Cell>, colCount:int):int
        {
            var limit:int = Math.min(cells.length, colCount);
            for (var n:int = 0; n < limit; n++){
                var cell:Cell = cells[n];
                if (x < cell.right)
                    return n;
            }
            return limit;
        }

        private function rowForY(y:Number, cells:Vector.<Cell>, colCount:int):int
        {
            var limit:int = cells.length;
            for (var n:int = 0; n < limit; n+=colCount){
                var cell:Cell = cells[n];
                if (y < cell.bottom)
                    break;
            }
            return n / colCount;
        }

        public function cellIndexToChildIndex(page:ArenaPage, cellIndex:int):int
        {
            var objects:IList = page.objects;
            var cells:Vector.<Cell> = page.transientDraggingCells as Vector.<Cell>;
            if (cells == null)
                return cellIndex;
            var result:int = cellIndex;  // value must be reduced by the number of dragging-cells below cellIndex
            var limit:int = Math.min(cellIndex, cells.length-1);
            for (var n:int = 0; n < limit; n++){
                var cell:Cell = cells[n];
                if (cell.objDragging) {
                    --result;
                }
            }
            for (var k:int = 0; k < result && k < objects.length; k++){
                var obj:AbstractObject = objects[k];
                if (obj.visible == false || obj.snapTo != null) {
                    ++result;
                }
            }
//            trace("cellIndexToChildIndex cell="+cellIndex, "result="+result);
            return result;
        }


        public function getCellBounds(page:ArenaPage,  index:int):Rectangle
        {
            var cells:Vector.<Cell> = page.transientDraggingCells as Vector.<Cell>;
            var bounds:Rectangle;
            if (index < cells.length && (index == 0 || !cells[index].objDragging)) {
                bounds = cells[index].objBounds;
            } else {
                index = Math.min(index-1,  cells.length-1); // beyond last cell: create a rect based on far edge of previous cell

//                trace("getCellBounds: cols="+page.columns, "rows="+page.rows, "cells="+cells.length, "index="+index);
                var vert:Boolean = (page.columns == 1 && page.growHorizontal == false);
                var downShift:Boolean = vert;
                if (page.columns > 1 && !page.growHorizontal && (index+1 == page.rows * page.columns)) {  // for grid, if bottom row is full, put marker below start of row
                    index = index - page.columns + 1;
                    downShift = true;
                }
                var cell:Cell = cells[index];
                bounds = cell.objBounds.clone();
                if (downShift) {
                    bounds.y += cell.height;
                } else {
                    bounds.x += cell.width;
                }
            }
            return bounds;
        }
	}
}


import com.alleni.author.model.AbstractObject;

import flash.geom.Rectangle;

class Cell extends Rectangle
{
	public var obj:AbstractObject;
    public var objDragging:Boolean;
    public var objBounds:Rectangle;
}

class Description
{
	public function Description(cells:Vector.<Cell>)
	{
		_cells = cells;
	}
	
	public var maxX:Number = 0;
	public var maxY:Number = 0;
	public var maxWidth:Number = 0;
	public var maxHeight:Number = 0;
	public var minWidth:Number = Number.MAX_VALUE;
	public var minHeight:Number = Number.MAX_VALUE;
	
	private var _cells:Vector.<Cell>;
	
	public function get totalCellsHeight():Number
	{
		return (_cells.length>1)?_cells[_cells.length-1].y+_cells[_cells.length-1].height - _cells[0].y:_cells[0].height;
	}
	public function get totalCellsWidth():Number
	{
		return (_cells.length>1)?_cells[_cells.length-1].x+_cells[_cells.length-1].width - _cells[0].x:_cells[0].width;
	}
	
}

