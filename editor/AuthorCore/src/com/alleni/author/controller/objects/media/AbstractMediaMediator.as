package com.alleni.author.controller.objects.media
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.WireController;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.BorealToken;
	import com.alleni.author.model.objects.MediaObject;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.util.CuePointsHandler;
	import com.alleni.author.view.ui.WarningWindow;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.lang.TaconiteTimer;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.service.LogService;
	
	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.NetStatusEvent;
	import flash.geom.Point;

	import mx.events.CloseEvent;
	import mx.events.PropertyChangeEvent;

	public class AbstractMediaMediator implements IMediaMediator
	{
		protected static const PLAYER_PLAYING:uint 	= 1 << 1;
		protected static const PLAYER_ENDED:uint 	= 1 << 2;
		protected static const PLAYER_PAUSED:uint 	= 1 << 3;
		protected static const PLAYER_STARTED:uint 	= 1 << 4;
		protected static const PLAYER_LOADED:uint 	= 1 << 5;
		protected static const METADATA_LOADED:uint = 1 << 6;
		protected static const PLAY_HEAD_HUNG:uint 	= 1 << 7;
		protected static const CONNECT_REJECTED:uint= 1 << 8;
		protected static const RECORDING:uint 		= 1 << 9;
		
		protected static const ALL_PLAYER_STATUS_FLAGS:uint =
			PLAYER_PLAYING |
			PLAYER_ENDED |
			PLAYER_PAUSED |
			PLAYER_STARTED |
			PLAYER_LOADED |
			METADATA_LOADED |
			PLAY_HEAD_HUNG |
			RECORDING;
		
		protected var _mediaObject:MediaObject;
		protected var _wireController:WireController;
		protected var _duration:Number = 0;
		protected var _segmentsPerSecond:Number = 1;
		protected var _playerStatus:uint = 0;
		
		protected var _oldTime:Number = 0;
		protected var _currentSecondsElapsed:Number = 0;
		protected var _currentOffset:Number = 0;
		protected var _cuePointList:Array;
		protected var _cuePointsHandler:CuePointsHandler;
		protected var _accessToken:BorealToken;
		private var _assetId:String;
		
		private var _taconiteTimer:TaconiteTimer;
		
		public function AbstractMediaMediator()
		{
			_wireController = ApplicationController.instance.wireController;
			_cuePointList = [];
			_taconiteTimer = TaconiteTimer.instance;
		}
		
		public function handleMediaEvents(object:ITaconiteObject):IMediaMediator
		{
			_mediaObject = object as MediaObject;
			_mediaObject.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
			Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
			return this;
		}
		
		private function handleApplicationPropertyChange(e:PropertyChangeEvent):void
		{
			if (e.property == "audioEnabled")
				if (playerStatusIs(PLAYER_LOADED))
					setVolume();
		}
			
		protected function handlePropertyChange(e:PropertyChangeEvent):void
		{
			var value:Number;
			
			switch(e.property) {
				case "autorun":
					// just in case we want to do something when autorun is disabled
					break;
				case "volume":
					if (playerStatusIs(PLAYER_LOADED))
						setVolume();
					break;
				case "percentElapsed":
					if (playerStatusIs(PLAYER_LOADED)) {
						value = e.newValue as Number;
						
						if (value < 0 || value > 100) {
							_mediaObject.percentElapsed = _currentOffset;
							break;
						}
						
						if (value != _currentOffset)
							setOffset(value);
						_mediaObject.percentRemaining = 100 - value;
					}
					break;
				case "secondsElapsed":
					if (playerStatusIs(PLAYER_LOADED)) {
						setSecondsElapsed(e.newValue as Number);
					}
					break;
				case "highQuality":
					setHighQuality(e.newValue as Boolean);
					break;
				case "asset":
					if (validateAssetId() && !_mediaObject.loading) {
						handleAssetChange();
					}
					break;
			}
		}
		
		protected function handleAssetChange():void
		{
		}
		
		public function initialize(fromActivation:Boolean=false):void
		{
			validateAssetId();
			if (!fromActivation) {
				initializeState();
			}
		}
		
		protected function initializeState():void
		{
			if (playerStatusIs(PLAYER_PLAYING))
				pause();
			unsetPlayerStatus(PLAYER_STARTED);
			unsetPlayerStatus(PLAYER_PAUSED);
			unsetPlayerStatus(PLAYER_PLAYING);
			_mediaObject.buffering = false;
		}
		
		protected function addListeners():void
		{
			_taconiteTimer.registerListener(updateTimingTick);
			_cuePointsHandler = new CuePointsHandler(cuePointCallBack);
		}
		
		protected function clearListeners():void
		{
			_taconiteTimer.unregisterListener(updateTimingTick);
			_cuePointsHandler = null;
		}
		
		public function pauseResume(running:Boolean):void
		{
			if (running) {
				if (playerStatusIs(PLAYER_PLAYING))
					this.resume();
			} else {
				this.pause();
			}
		}
		
		public function unload():void
		{
			_mediaObject.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);
			_mediaObject.fractionLoaded = 0;
			_mediaObject.duration = 0;
			_mediaObject.secondsElapsed = 0;
			_mediaObject.secondsRemaining = 0;
			_mediaObject.percentElapsed = 0;
			_mediaObject.percentRemaining = 0;
			_accessToken = null;
			_mediaObject.buffering = false;
			watchLoading = false;
			if (playerStatusIs(PLAYER_LOADED))
				clearListeners();
			unsetPlayerStatus(ALL_PLAYER_STATUS_FLAGS);
		}

        public function get playing():Boolean
		{
            return playerStatusIs(PLAYER_PLAYING) && !playerStatusIs(PLAYER_PAUSED);
        }

		protected function playerStatusIs(mask:uint):Boolean
		{
			return (_playerStatus & mask) != 0;
		}
		
		protected function setPlayerStatus(mask:uint):void
		{
			_playerStatus |= mask;
		}
		
		protected function unsetPlayerStatus(mask:uint):void
		{
			_playerStatus &= ~mask;
		}
		
		public function pause():void
		{
		}
		
		public function stop():void
		{
			this.pause();
			initializeState();
		}
		
		public function record():void
		{
		}
		
		public function start(position:Number=0, restart:Boolean=false):void
		{
		}
		
		protected function play(position:Number=0):void
		{
		}
		
		public function togglePause():void
		{
			if (playerStatusIs(PLAYER_PAUSED))
				resume();
			else if (playerStatusIs(PLAYER_PLAYING))
				pause();
			else
				start();
		}
		
		public function resume(fromActivation:Boolean=false):Boolean
		{
			if (fromActivation && !_mediaObject.resumeTicking && !playerStatusIs(PLAYER_PAUSED)) return false;

			validateAssetId();
			if (!playerStatusIs(PLAYER_PLAYING)) {
				if (fromActivation) {
					_oldTime = _mediaObject.secondsElapsed * _segmentsPerSecond;
				}
				if (playerStatusIs(PLAYER_LOADED))
					play(_oldTime);
				else
					start(_oldTime);		
				unsetPlayerStatus(PLAYER_PAUSED);
				return true;
			}
			return false;
		}

		private function validateAssetId():Boolean
		{
			if (_mediaObject.asset != null && _mediaObject.asset.id != _assetId) {
				_assetId = _mediaObject.asset.id;
				return true;
			}
			return false; 		
		}
		
		protected function setTime(time:Number):void
		{
			_oldTime = time;
		}
		
		protected function setSecondsElapsed(value:Number):void
		{
			if (value < 0 || value > _duration) {
				_mediaObject.secondsElapsed = _currentSecondsElapsed;
				return;
			}
			
			if (value != _currentSecondsElapsed)
				setTime(value);
			_mediaObject.secondsRemaining = _mediaObject.duration - value;
		}
		
		protected function setOffset(offset:Number):void
		{
		}
		
		protected function setVolume():void
		{
		}
		
		protected function setHighQuality(value:Boolean):void
		{	
		}
		
		protected function onMetaData(data:Object):void
		{
			if (!playerStatusIs(METADATA_LOADED)) {
				setPlayerStatus(METADATA_LOADED);
			}
		}
		
		protected function onXMPData(info:Object):void 
		{ 
			trace("on XMPData", info);
		}
		
		protected function handleNetStatus(event:NetStatusEvent):void
		{
			//trace(event.info.code);
			switch (event.info.code) {
				case "NetConnection.Connect.Rejected":
				case 'NetConnection.Connect.Failed':
					handleConnectFailed();
					break;
				case "NetConnection.Connect.Success":
					handleConnectSuccess(event);
					break;
				case "NetStream.Failed":
				case "NetStream.Play.StreamNotFound":
					handleStreamNotFound();
					break;
				case "NetStream.Buffer.Full":
					handleBufferFull();
					break;
				case "NetStream.Buffer.Empty":
					handleBufferEmpty();
					break;
				case "NetStream.Buffer.Flush":
					// something
					break;
				case "NetStream.Play.Start":
					// something
					break;
				case "NetStream.Play.Stop":
					///handlePlayComplete(); this was causing trouble with vod streams prematurely triggering complete
					break;
				default :
					// nothing to do, since this occurs in cases of both success and failure.
					break;
			}
		}
		
		protected function handleConnectFailed():void
		{
			// in the case of RTMP streams, we will try again using RTMPT as this may be a firewall issue.
			setPlayerStatus(CONNECT_REJECTED);
			LogService.error("Connect rejected for '" + _mediaObject.title + "'");
		}
		
		protected function handleConnectSuccess(event:NetStatusEvent):void
		{
		}
		
		protected function handleBufferFull(event:Event=null):void
		{
			_mediaObject.buffering = false;
		}
		
		protected function handleBufferEmpty(event:Event=null):void
		{
			_mediaObject.buffering = true;
		}
		
		protected function handlePlayComplete(event:Event=null):void
		{
			if (playerStatusIs(PLAYER_PLAYING)) {
				_wireController.sendTrigger(_mediaObject, Modifiers.instance.fetch("finished"));
				if (!_mediaObject.loop)
					stop();
				else
					start(0, true); // restart to loop
			}
		}
		
		protected function handleStreamNotFound():void
		{
			// TODO this is most likely unrecoverable so we should inform the author and give them the option to try something else.
			LogService.error("Media stream not found for '" + _mediaObject.title + "'");
			handleUnrecoverableError();
		}
		
		protected function handleIOError(event:IOErrorEvent):void
		{	
			// TODO this is most likely unrecoverable so we should inform the author and give them the option to try something else.
			LogService.error("IO Error for media '" + _mediaObject.title + "': " + event.text);
			handleUnrecoverableError();
		}
		
		protected function handleAsynchError(event:AsyncErrorEvent):void
		{	
			LogService.error("Asynch Error for media '" + _mediaObject.title + "': " + event.text);
		}
		
		protected function handleUnrecoverableError():void
		{
			LogService.error("Encountered unrecoverable error attempting to play '" + _mediaObject.title + "'");
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:31, inserts:[_mediaObject.title]}));
            this.unload();
		}
		
		protected function handleCloseAlertSelection(event:CloseEvent):void
		{	
			// override as needed
		}
		
		protected function cuePointCallBack(id:Number):void
		{	
			_mediaObject.cuePoint = id;
		}
		
		protected function addCuePoints():void
		{	
			parseCuePointList();
			
			for (var i:uint = 0; i < _cuePointList.length; i++)
				_cuePointsHandler.addCuePoint(new Point(_cuePointList[i][0], _cuePointList[i][1]));
		}
		
		protected function parseCuePointList():void
		{
			_cuePointList = [];
			
			var tokens:Array = _mediaObject.cuePointList.split(";");
			
			for each (var token:String in tokens) {
				var parts:Array = token.split(":");
				_cuePointList.push([parts[0],parts[1]]);
			}
		}
		
		// cue point support not yet built
		private function onCuePoint(infoObject:Object):void 
		{ 
		}
		
		protected function updateTimingTick(event:Event):void
		{
		}
		
		protected function updateTiming(currentTime:Number, duration:Number):void
		{
			// ZZX-1764: if we're within 100ms of completion, ensure complete trigger.
			// this prevents lower resolution audio from completing without trigger at 80-99%.
			if (duration > 0.0 && (((duration - currentTime) / _segmentsPerSecond) < 0.1)) {
				currentTime = duration;
			}

			_currentSecondsElapsed = Math.floor(currentTime/_segmentsPerSecond * 100)/100;
			_mediaObject.secondsElapsed = _currentSecondsElapsed;
			_duration = duration;
			_mediaObject.duration = Math.floor(duration/_segmentsPerSecond*100)/100;

			if (_cuePointsHandler && _oldTime != currentTime)
				_cuePointsHandler.findCuePoint(currentTime);
			
			if ((_duration != _duration) || !isFinite(_duration) || _duration < 1)
				return;
			
			var roundedOffset:Number = Math.round(currentTime / _duration  * 100);
			_currentOffset = roundedOffset;
			if (roundedOffset <= 100)
				_mediaObject.percentElapsed = roundedOffset;
			
			if (roundedOffset >= 100)
				handlePlayComplete();
			
			if (_oldTime > 0 && _oldTime == currentTime)
				setPlayerStatus(PLAY_HEAD_HUNG);
			else
				unsetPlayerStatus(PLAY_HEAD_HUNG);
			_oldTime = currentTime;
		}
		
		protected function set watchLoading(value:Boolean):void
		{
			if (value)
				_taconiteTimer.registerListener(handleWatchLoading);
			else
				_taconiteTimer.unregisterListener(handleWatchLoading);
		}
		
		protected function handleWatchLoading(event:Event):void
		{
		}
		
		protected function updateFractionLoaded(bytesLoaded:Number=0, bytesTotal:Number=0):void
		{
			_mediaObject.fractionLoaded = bytesLoaded /  bytesTotal;
			if (bytesTotal > 0 && _mediaObject.fractionLoaded == 1) {
				watchLoading = false;
				_mediaObject.buffering = false;
			}
		}
		
		protected function safeToUseOffset(offset:Number, time:Number):Boolean
		{
			if (!(offset != offset) && isFinite(_duration)  && _duration > 0) {
				var currentTime:Number = time / _duration  * 100;
				var timeDiff:Number = Math.abs(offset - currentTime);
				
				if (timeDiff > 2)
					return true;
			}
			return false;
		}
	}
}
