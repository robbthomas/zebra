package com.alleni.author.controller.objects
{
	import com.alleni.author.model.objects.CalcGate;
	import com.alleni.taconite.model.TaconiteModel;


	public class CalcGateController extends CalcController
	{
		public static var instance:CalcGateController = new CalcGateController();
		
		protected static function getCalcGate(model:TaconiteModel):CalcGate
		{
			return model.value as CalcGate;
		}
		
		override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel) : void
		{
			var calcGate:CalcGate = getCalcGate(model);
			
			if (calcGate && calcGate.gateValue) {
				switch (property) {
					case "gateValue":
					case "calcValue":
						if (calcGate.calcValue)
							calcGate.calcValue2 = calcGate.calcValue;
						break;
					case "calcValue2":
						calcGate.calcValue = calcGate.calcValue2;
				}
			}
		}
	}
}