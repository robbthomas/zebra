package com.alleni.author.controller.objects.media
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.taconite.model.ITaconiteObject;

	public interface IMediaMediator
	{
		function handleMediaEvents(object:ITaconiteObject):IMediaMediator
		function initialize(fromActivation:Boolean=false):void;
		function pauseResume(running:Boolean):void;
		function get playing():Boolean;
		function start(position:Number=0, restart:Boolean=false):void;
		function pause():void;
		function togglePause():void;
		function resume(fromActivation:Boolean=false):Boolean;
		function stop():void;
		function record():void;
		function unload():void;
	}
}