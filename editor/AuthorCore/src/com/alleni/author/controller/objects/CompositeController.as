package com.alleni.author.controller.objects
{
import com.alleni.author.Navigation.PagerController;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.Navigation.SmartObjectController;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.DropIntoContainerEvent;
	import com.alleni.author.definition.GadgetDescription;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.action.ActionGroup;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.definition.action.CreateObjectAction;
	import com.alleni.author.definition.action.CustomRibbonAction;
	import com.alleni.author.definition.action.DestroyObjectAction;
import com.alleni.author.definition.action.LibraryAddAction;
import com.alleni.author.definition.action.ModifyRibbonAction;
	import com.alleni.author.definition.action.MoveWireAction;
import com.alleni.author.definition.action.UserAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.event.CompositeEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.AbstractContainer;
	import com.alleni.author.model.objects.ArenaPage;
	import com.alleni.author.model.objects.Composite;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.ExternalSmartWireAnchor;
	import com.alleni.author.model.ui.InternalSmartWireAnchor;
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.util.Set;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.ui.WireboardView;
	import com.alleni.author.view.ui.controls.Alert;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;

import flash.events.Event;

import flash.events.MouseEvent;
	
	import mx.events.CloseEvent;
	
	public class CompositeController extends SmartObjectController
	{
		public static var instance:CompositeController = new CompositeController();
		
		public function CompositeController()
		{
			super();
		}

		public function handleMakeObject(openIt:Boolean=false):void
		{
			// close current composite, nothing is selected
			var models:Vector.<TaconiteModel> = cleanSelectionIncludingLocked;
			var compositeMediator:CompositeVelumMediator = ApplicationController.instance.authorController.currentVelumMediator as CompositeVelumMediator;
			if (compositeMediator && !openIt && models.length < 1) {
				compositeMediator.makeObject();
			} else if (models.length == 0) {
				// nothing selected: ignore the request and give no alert
			} else if (models.length == 1 && models[0].value is Composite) {
				// avoid making gadget of a single gadget object
			} else if (models.length > 0) {
				var outerParent:TaconiteModel = findOuterParent(models);
				if (commonParent(models, outerParent)) {
					models = cleanArenaChildren(models, outerParent);
					makeComposite(models, openIt);
					ApplicationController.instance.wireController.showWiresForLevel();
				}else{
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:38}));
				}
			}
		}
		
		private function makeComposite(models:Vector.<TaconiteModel>, openIt:Boolean):void
		{
			ApplicationController.instance.authorController.log("makeGadget openIt="+openIt, models);
			// sorted by layer, so the objects will remain in same layering order
			models.sort(compareLayers);

			// undo actions are Cut outside refs, Delete children, and Add composite
			var group:ActionGroup = new ActionGroup("Make gadget");
			var prefixGroup:ActionGroup = new ActionGroup("makeCompositePrefixActions");  // remove links from outside before delete objs
			group.add(prefixGroup);
			group.add(DestroyObjectAction.fromSelection(models));

			// create the composite object
			var composite:Composite = ApplicationController.instance.authorController.addObjectForName("Composite", 0, 0, 100, 100, false) as Composite;
			var compositeModel:TaconiteModel = composite.model;
			composite.title = GadgetDescription.DEFAULT_NAME;

			// move the composite to the proper layer
			var frontModel:TaconiteModel = models[models.length-1];
			var compositeParent:TaconiteModel = frontModel.parent;
			var compositeIndex:int = compositeParent.getChildIndex(frontModel);
			compositeModel.changeParent(compositeParent);
			compositeParent.setChildIndex(composite, compositeIndex);
			composite.rotation = 0;
			
			// add selected objects to the composite
			compositeModel.dispatchEvent(new CompositeEvent(CompositeEvent.ABSORB_OBJECTS, models));
			
			if (!(compositeParent.value as Composite)) {
				compositeModel.dispatchEvent(new DropIntoContainerEvent());  // into arena, etc
				composite.setInitialZIndex();
			}
			
			fixLinksToOutside(composite, prefixGroup);  // cut links between outside objects and the composite children & arena children
			
			rerouteWiresToOutside(composite);  // create custom ribbons as needed for outside connections

            // wait for render, so we can get bounds accurately
            var token:Object = ApplicationController.instance.disableInput("makeComposite");
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            PseudoThread.add(new PseudoThreadSimpleRunnable("makeComposite/saveNewComposite", saveNewComposite, this, [composite, group, token, openIt]));
		}

        private function saveNewComposite(composite:Composite, group:ActionGroup, token:Object, openIt:Boolean):void
        {
            Utilities.assert(composite.viewBox.height > 0);
            composite.updateProject(composite.save());
            var libraryUpdate:UserAction = LibraryAddAction.fromItem(composite.gadget);
            libraryUpdate.perform();
            group.add(libraryUpdate);
            group.add(CreateObjectAction.fromObject(composite));
            AuthorController.instance.currentActionTree.commit(group);
            PagerController.instance.activateObject(composite, true);
            composite.controller.objectCompleted(composite);  // create a message center
            ApplicationController.instance.wireController.showWiresForLevel();
            if (openIt) {
                CompositeMediator.openEditing(composite);
            } else {
                ApplicationController.instance.authorController.selectSingleModel(composite.model);
            }
            ApplicationController.instance.enableInput(token);
        }
		
		private function commonParent(models:Vector.<TaconiteModel>, outerParent:TaconiteModel):Boolean
		{
			// true if all models in selection are direct children of "outerParent" or are descendents of an arena in the selection
			if (models.length == 0)
				return false;
			else if (models.length == 1)
				return true;
			
			for(var i:int=0; i<models.length; i++){
				var m:TaconiteModel = models[i] as TaconiteModel;
				while (m.parent && m.parent != outerParent) {
					if (models.indexOf(m.parent) < 0 && !(m.parent.value is ArenaPage && models.indexOf(m.parent.parent) >= 0))
						return false;
					m = m.parent;
				}
			}
			return true;
		}
		
		private function cleanArenaChildren(selection:Vector.<TaconiteModel>, outerParent:TaconiteModel):Vector.<TaconiteModel>
		{
			var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
			for each (var m:TaconiteModel in selection) {
				if (m.parent == outerParent)
					models.push(m);
			}
			return models;
		}
		
		private function findOuterParent(models:Vector.<TaconiteModel>):TaconiteModel
		{
			// return the outermost parent of selected objects
			var result:TaconiteModel = models[0].parent;
			var i:uint;
			for(i = 1; i < models.length; i++){
				var m:TaconiteModel = models[i];
				while (m.parent) {
					if (models.indexOf(m.parent) < 0) {
						if (!m.isDescendant(result))
							result = m.parent;  // parent of the outermost selected object
						break;
					}
					m = m.parent;
				}
			}
			return result;
		}
		
		
		override public function handleMouseDown(objectView:ITaconiteView, e:MouseEvent):Boolean
		{
			var obj:AbstractObject = objectView.model.value as AbstractObject;
			if (Application.running && obj.enabled)
				_wiring.sendTrigger(obj, Modifiers.instance.fetch("clickStart"));
			
			return !obj.editingLocked;
		}
		
		
		private function compareLayers(model1:TaconiteModel, model2:TaconiteModel):int
		{
			var world:World = Application.instance.document.root.value as World; 
			var p1:Array = world.objectPathArray(model1.value as AbstractObject);
			var p2:Array = world.objectPathArray(model2.value as AbstractObject);
			var l1:int = p1.length;
			var l2:int = p2.length;
			var compare:Boolean = false;
			var longestPath:int = ( l1 > l2 ? l1 : l2 );
			
			for (var i:int=0; i < longestPath; i++) {
				var one:int = p1[i];
				var two:int = p2[i];
				if (one == two) {
					continue;
				} else {
					compare = (one > two);
					break;
				}
			}
			return (compare) ? 1 : -1;
		}
		
		public function handleUnmakeObject():void
		{
            var ribbonName:String = null;
			var selection:Vector.<TaconiteModel> = cleanSelection;
			var count:int = selection.length;
            var n:int;
            var model:TaconiteModel;

            for (n=0; n < count; n++) {
				model= selection[n];
				var composite:Composite = model.value as Composite;
                if(composite == null || !okToEdit(composite)){
                    return;
                }
            }
			// check for custom ribbons with too many wires, give alert if needed
			for (n=0; n < count; n++) {
				model= selection[n];
				var composite:Composite = model.value as Composite;
				if (composite) {
					var errRibbonName:String = cannotRerouteCustomRibbons(composite);
					if (errRibbonName != null)
						ribbonName = errRibbonName;
				}
			}
			if (ribbonName == null)
				doUnmakeObject();
			else
				alertCustomRibbonProblem(ribbonName);
		}
		
		public function doUnmakeObject():void
		{
			// if several composites are selected, there will be a separate Undo step for each one
			var selection:Vector.<TaconiteModel> = cleanSelection;
			ApplicationController.instance.authorController.log("unmakeGadget", selection);
			var count:int = selection.length;
			for (var n:int=0; n < count; n++) {
				var model:TaconiteModel = selection[n];
				const composite:Composite = model.value as Composite;
				if (composite) {
					unmakeComposite(composite);
				}
			}
		}
		
		private function unmakeComposite(composite:Composite):void
		{
			// undo actions are deletion of the composite and addition of the children
			var group:ActionGroup = new ActionGroup("Unmake gadget");
			var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
			models.push(composite.model);
			group.add(DestroyObjectAction.fromSelection(models));
			models = new Vector.<TaconiteModel>();
			var obj:AbstractObject;
			for each (obj in composite.children) {
				models.push(obj.model);
			}
			
			composite.model.dispatchEvent(new CompositeEvent(CompositeEvent.UNMAKE_OBJ));
			
			group.add(CreateObjectAction.fromSelection(models));
			ApplicationController.instance.authorController.currentActionTree.commit(group);
		}
		
		private function alertCustomRibbonProblem(ribbonName:String):void
		{
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:39, closeFunction:alertCloseListener, inserts:[ribbonName]}));
		}
		
		private function alertCloseListener(e:CloseEvent):void
		{
			if (e.detail == Alert.SECOND)
				doUnmakeObject();
		}
		
		/**
		 * Find the child or nearest descendant of "scope", that contains "slave".
		 * Move out the scope chain one step closer to "slave", further from world.
		 * @param obj
		 * @return 
		 * 
		 */
		private function nextWiringScope(scope:AbstractObject, slave:WireAnchor):SmartObject
		{
			trace("nextWiringScope inputScope="+scope, "slave="+slave);
			var composite:SmartObject;
			var test:AbstractObject = slave.wiringScope;
			while (test != scope && !(test as World)) {
				composite = test as SmartObject;
				if (composite)
					break;
				test = test.wireScopeContainer;
			}
			trace("  resultScope=" + composite);
			return composite;
		}
		
		private function rerouteWiresToOutside(composite:SmartObject):void
		{
			var token:Object = ApplicationController.currentActionTree.openGroup("reroute wires"); // discard actions
			
			var objects:Array = GadgetEncoder.collectChildren(composite, null, true, false);
			var oldWiresArray:Array = ApplicationController.instance.wireController.getWiresInclusive(objects);
			var oldWires:Vector.<Wire> = new Vector.<Wire>();
			var wire:Wire;
			for each (wire in oldWiresArray) {
				oldWires.push(wire);
			}
			
			var newWires:Vector.<Wire> = new Vector.<Wire>();
			for each(wire in oldWires) {
				if(wire.masterAnchor.wiringScope != composite || wire.slaveAnchor.wiringScope != composite) {
					rerouteWireToOutside(composite, wire, newWires, oldWires);
				}
			}
			
			for each (wire in oldWires)
				wire.fireCompleteEvent(true);  // existing=true
			
			ApplicationController.currentActionTree.cancelGroup(token);
		}
		
		private function rerouteWireToOutside(composite:SmartObject, wire:Wire, newWires:Vector.<Wire>, oldWires:Vector.<Wire>):void
		{
			var outward:Boolean = (wire.slaveAnchor.wiringScope != composite);
			if (!finishWireThruExistingRibbon(composite, wire, wire.slaveAnchor, newWires, oldWires, outward))
				finishWireThruEdge(composite, wire, wire.slaveAnchor, newWires, outward);
		}
		
		private function finishWireThruExistingRibbon(composite:SmartObject, wire:Wire, slave:WireAnchor, newWires:Vector.<Wire>, oldWires:Vector.<Wire>, outward:Boolean):Boolean
		{
			// look for a custom anchor already created, with the same slave or anchor
			var found:WireAnchor = findSmartAnchor(slave, composite, true, newWires);  // targetSlave=true, search new created wires
			if (found) {
				return wire.finish(found);
			} else if (oldWires) {
				found = findSmartAnchor(wire.masterAnchor, composite, false, oldWires);  // targetSlave=false, search old wires redirected to custom ribbons
				if (found) {
					trace("FOUND MASTER",found);
					wire.masterAnchor = found;
					wire.slaveAnchor = null;
					return wire.finish(slave);
				}
			}
			return false;
		}
		
		private function findSmartAnchor(target:WireAnchor, composite:SmartObject, targetSlave:Boolean, wires:Vector.<Wire>):WireAnchor
		{
			for each (var wire:Wire in wires) {
				var anchor:WireAnchor = (targetSlave) ? wire.slaveAnchor : wire.masterAnchor;
				if (anchor == target) {
					// we found the new wire:  now look at other end of wire, and get anchor from other side of gadget interface
					var otherEnd:WireAnchor = (targetSlave) ? wire.masterAnchor : wire.slaveAnchor;
					if (otherEnd.hostObject == composite) {
						var internalAnchor:InternalSmartWireAnchor = otherEnd as InternalSmartWireAnchor;
						if (internalAnchor)
							return internalAnchor.other;
						var externalAnchor:ExternalSmartWireAnchor = otherEnd as ExternalSmartWireAnchor;
						if (externalAnchor)
							return externalAnchor.other;
					}
				}
			}
			return null;
		}

		
		public function cannotRerouteCustomRibbons(composite:SmartObject):String
		{
			// if any custom ribbon has multiple wires inside & multiple wires outside, then return the name of that ribbon
			var inner:InternalSmartWireAnchor;
			for each (inner in composite.innerAnchors) {
				if (inner.other && ApplicationController.instance.wireController.getWiresForAnchor(inner).length > 1) {
					if (ApplicationController.instance.wireController.getWiresForAnchor(inner.other).length > 1) {
						return inner.other.label;  // this one is a problem
					}
				}
			}
			return null;  // no problem rerouting these anchors
		}
		
		public function rerouteCustomRibbons(composite:SmartObject):void
		{
			var inner:InternalSmartWireAnchor;
			for each (inner in composite.innerAnchors) {
				if (inner.other) {
					var innerWires:Array = ApplicationController.instance.wireController.getWiresForAnchor(inner);
					var outerWires:Array = ApplicationController.instance.wireController.getWiresForAnchor(inner.other);
					var wire:Wire;
					if (innerWires.length == 1) {
						for each (wire in outerWires)
							rerouteOuterWire(inner, wire, innerWires[0]);
					} else if (outerWires.length == 1) {
						for each (wire in innerWires)
							rerouteInnerWire(inner, wire, outerWires[0]);
					}
				}
			}
		}
		
		private function rerouteOuterWire(inner:InternalSmartWireAnchor, wire:Wire, innerWire:Wire):void
		{
			var outer:ExternalSmartWireAnchor = inner.other;
			var innerTarget:WireAnchor = (innerWire.masterAnchor == inner) ? innerWire.slaveAnchor : innerWire.masterAnchor;
			if (wire.masterAnchor == outer)
				wire.masterAnchor = innerTarget;
			else
				wire.slaveAnchor = innerTarget;
			wire.fireCompleteEvent(true); // existing=true
			trace("rerouted custom ribbon wire:",wire);
		}
		
		private function rerouteInnerWire(inner:InternalSmartWireAnchor, wire:Wire, outerWire:Wire):void
		{
			var outer:ExternalSmartWireAnchor = inner.other;
			var outerTarget:WireAnchor = (outerWire.masterAnchor == outer) ? outerWire.slaveAnchor : outerWire.masterAnchor;
			if (wire.masterAnchor == inner)
				wire.masterAnchor = outerTarget;
			else
				wire.slaveAnchor = outerTarget;
			wire.fireCompleteEvent(true); // existing=true
		}
		
		public function finishWireOutside(wire:Wire, slave:WireAnchor, wires:Vector.<Wire>):Boolean
		{
			var masterAnchor:WireAnchor = wire.masterAnchor;
			
			var outward:Boolean;
			if (masterAnchor as InternalSmartWireAnchor)
				outward = !withinScope(slave.hostObject, masterAnchor.hostObject);  // from wireboard peg
			else if (slave as InternalSmartWireAnchor)
				outward = !withinScope(slave.hostObject, masterAnchor.hostObject);  // to wireboard peg  (wiring from outside to a peg is disallowed)
			else
				outward = !withinScope(slave.hostObject, masterAnchor.hostObject.wireScopeContainer);  // eg. true = wiring to world child from gadget child 
			trace("finishWireOutside: master="+masterAnchor.hostObject.title, "slave="+slave.hostObject.title, "outward="+outward);
			
			var internalSmartWireAnchor:InternalSmartWireAnchor;
			
			// check for drag from a wireboard peg to the outside, and a custom ribbon already exists
			internalSmartWireAnchor = masterAnchor as InternalSmartWireAnchor;
			if (internalSmartWireAnchor && internalSmartWireAnchor.other) {
				if (outward) {  // slave not inside the composite
					masterAnchor = internalSmartWireAnchor.other;  // use the custom ribbon
					wire.masterAnchor = masterAnchor;
					trace("  wire from peg; use existing custom ribbon: master="+masterAnchor);
				}
			}
			
			// check for wire from outside to a wireboard peg, and a custom ribbon exists
			internalSmartWireAnchor = slave as InternalSmartWireAnchor;
			if (internalSmartWireAnchor && internalSmartWireAnchor.other) {
				if (!outward) {  // master not inside the composite
					slave = internalSmartWireAnchor.other;  // use the custom ribbon
					trace("  wiring to a peg; use custom ribbon: slave="+slave);
				}
			}
			
			var masterScope:AbstractObject = masterAnchor.wiringScope;
			var slaveScope:AbstractObject = slave.wiringScope;
			trace(" finishWireOutside: masterScope="+masterScope.title, "slaveScope="+slaveScope.title, "master="+masterAnchor.hostObject.title, "slave="+slave.hostObject.title);
			if (masterScope == slaveScope)
				return wire.finish(slave);
			
			var composite:SmartObject;
			if (outward) {
				composite = masterScope as SmartObject;  // create custom ribbon here
			} else {  // move up one level from masterScope: don't need a custom ribbon on masterScope itself (eg. the world)
				composite = nextWiringScope(masterScope, slave);
			}

            // composite==null when wire from outside object to a wireboard peg
            if (composite){
			    return finishWireThruEdge(composite, wire, slave, wires, outward);
            } else {
                return false;
            }
		}
		
		public function finishWireThruEdge(composite:SmartObject, wire:Wire, slave:WireAnchor, wires:Vector.<Wire>, outward:Boolean):Boolean
		{
			trace("finishWireThruEdge: composite="+composite.title, "master="+wire.masterAnchor.hostObject.title, "slave="+slave.hostObject.title, "outward="+outward)
			var masterAnchor:WireAnchor = wire.masterAnchor;
			var baseAnchor:WireAnchor = (outward) ? masterAnchor : slave;  // custom anchor is based on the inside anchor
			var extAnchor:ExternalSmartWireAnchor = composite.addCustomAnchor(baseAnchor);
            if(!extAnchor) {
                return false; // wasn't able to make the custom anchor
            }
			var intAnchor:InternalSmartWireAnchor = extAnchor.other;
			intAnchor.portSide = WireboardView.RIGHT;  // recalc will switch to nearest side
			
			if (masterAnchor as InternalSmartWireAnchor && masterAnchor.hostObject == composite && outward) {
				wire.masterAnchor = extAnchor;  // use the new custom ribbon
				return finishWireOutside(wire, slave, wires);
			}
			var nextAnchor:WireAnchor;
			assert(intAnchor.hostObject, "intAnchor");
			assert(extAnchor.hostObject, "extAnchor");
			if (outward) {
				var ok:Boolean = wire.finish(intAnchor);
				assert(ok, "outward: master="+wire.masterAnchor + " slave="+intAnchor);
				nextAnchor = extAnchor;
			} else {
				ok = wire.finish(extAnchor);
				assert(ok, "inward: master="+wire.masterAnchor + " slave="+extAnchor);
				nextAnchor = intAnchor;
			}
			assert(wire.masterAnchor.hostObject, "wire.master="+wire.masterAnchor);
			assert(wire.slaveAnchor.hostObject, "wire.slave="+wire.slaveAnchor);
			assert(nextAnchor.hostObject, "nextAnchor="+nextAnchor);
			trace(" finishWireThruEdge: nextAnchor="+nextAnchor, "outward="+outward);
			
			var nextWire:Wire = ApplicationController.instance.wireController.createWire(nextAnchor);
			var success:Boolean = finishWireOutside(nextWire, slave, wires);
			if (success) {
				ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(extAnchor, true));
				wires.push(nextWire);
			}
			return success;
		}

		override public function childStartDrag(e:MouseEvent, selectedChild:ObjectView):void
		{
			const composite:SmartObject = selectedChild.object.parent as SmartObject;
			if (composite) {
				composite.draggingChild = true;
			}
		}
		
		override public function childEndDrag(e:MouseEvent, selectedChild:ObjectView):void
		{
			const composite:Composite = selectedChild.object.parent as Composite;
			if (composite) {
				composite.draggingChild = false;
                preventDragGadgetIntoItself(composite);
			}
		}

        private function preventDragGadgetIntoItself(composite:Composite):void
        {
            for each (var m:TaconiteModel in AuthorController.instance.selection.selectedModels) {
                var obj:AbstractObject = m.value as AbstractObject;
                if (recursiveReference(composite, obj)) {
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.UNDO));  // undo the drag that brought gadget into itself
                    recursiveGadgetAlert();
                    break;
                }
            }
        }

        private function recursiveReference(composite:Composite, child:AbstractObject):Boolean
        {
            var draggedGadgets:Set = new Set(false);
            var test:Composite;
            for each (var ao:AbstractObject in child.allObjects()) {   // watch for arena full of gadgets being dragged
                test = ao as Composite;
                if (test && test.gadget) {
                    draggedGadgets.add(test.gadget);
                }
            }
            var container:AbstractContainer = composite;
            while (true) {
                var test:Composite = container as Composite;
                if (test && test.gadget && draggedGadgets.hasItem(test.gadget)) {
                    return true;
                }
                // nested gadgets are brought to front while editing, but wireScopeContainer accounts for that
                container = container.wireScopeContainer;
                if (container == null || container is World) {
                    break;
                }
            }
            return false;

        }

        public function recursiveGadgetAlert():void
        {
            Alert.cancelLabel = "Cancel";
            Alert.show("That would create a recursive structure.", "Cannot drag a gadget into itself", Alert.FIRST, null, null, Alert.FIRST);
        }

		public static function withinScope(child:AbstractObject, container:AbstractObject):Boolean
		{
			// this function must be used instead of TaconiteModel.isDescendant, because the current editor is reparented to the front for layering
			var scope:AbstractObject = child.wireScopeContainer;
			while (true) {
				if (scope == container)
					return true;
				else if (scope as World)
					return false;
				else
					scope = scope.wireScopeContainer;  // follow chain of nested gadget editors
			}
			return false;
		}
		
		public function handleNewCompositeWire(obj:SmartObject, wire:Wire):void
		{
			// check for wire-to-edge moved to a different object port
			var inner:InternalSmartWireAnchor;
			var anchor:WireAnchor;
			if (wire.masterAnchor as InternalSmartWireAnchor) {
				inner = wire.masterAnchor as InternalSmartWireAnchor;
				anchor = wire.slaveAnchor;
			} else if (wire.slaveAnchor as InternalSmartWireAnchor) {
				inner = wire.slaveAnchor as InternalSmartWireAnchor;
				anchor = wire.masterAnchor;
			} else {
				return;  // not a wire to edge
			}
		}
		
		private function moveWires(wires:Array, oldAnchor:WireAnchor, newAnchor:WireAnchor):void
		{
			var wire:Wire;
			for each (wire in wires) {
				var oldAnchorWasMaster:Boolean = (wire.masterAnchor == oldAnchor);
				var action:MoveWireAction = MoveWireAction.fromWire(wire, oldAnchorWasMaster, oldAnchor, newAnchor);
				action.perform();
				ApplicationController.currentActionTree.commit(action);
			}
		}
		
		private function fixLinksToOutside(composite:SmartObject, group:ActionGroup):void
		{
			// cut links between outside objects and the composite children & arena children
			var inside:Vector.<AbstractObject> = findInsideObjects(composite);
			var outside:Vector.<AbstractObject> = findOutsideObjects(composite);
			fixLinksBetween(composite, true, inside, outside);  // outward
			fixLinksBetween(composite, false, outside, inside, group); // inward: undo actions recorded for changes to outside objects
		}
		
		private function findInsideObjects(composite:SmartObject):Vector.<AbstractObject>
		{
			var inside:Vector.<AbstractObject> = new Vector.<AbstractObject>();
			composite.allObjectsInScope( function(one:AbstractObject):Boolean{
				if (one != composite && inside.indexOf(one) == -1) {
					inside.push(one);
				}
				return true;
			} );
			return inside;
		}
		
		private function findOutsideObjects(composite:SmartObject):Vector.<AbstractObject>
		{
			// collect all objects in world or in open composites, not in the specified composite
			var ao:AbstractObject;
			var child:AbstractObject;
			var outside:Vector.<AbstractObject> = new Vector.<AbstractObject>();
			var world:World = Application.instance.document.root.value as World;
			outside.push(world);
			for each (ao in outside) {
				const container:AbstractContainer = ao as AbstractContainer;
				if(container && ao != composite && !(ao as SmartObject && ao.editing==false)) {
					for each(child in container.objects) {
						outside.push(child);
					}
				}
			}
			return outside;
		}
		
		private function fixLinksBetween(composite:SmartObject, outward:Boolean, fromList:Vector.<AbstractObject>, toList:Vector.<AbstractObject>, group:ActionGroup=null):void
		{
			var obj:AbstractObject;
			for each (obj in fromList) {
				var thing:*;
				for each(thing in obj.anchors) {
					var anchor:WireAnchor = thing as WireAnchor;
					if (anchor) {
						if (anchor.value as AbstractObject && toList.indexOf(anchor.value) >= 0) {
							removeAnchorLink(obj, anchor, group);
						}
						var child:WireAnchor;
						for each (child in anchor.childAnchors) {
							if (child.value as AbstractObject && toList.indexOf(child.value) >= 0) {
								removeAnchorLink(obj, child, group);
							}
						}
					}
				}
			}
		}
		
		private function removeAnchorLink(obj:AbstractObject, anchor:WireAnchor, group:ActionGroup):void
		{
			var oldValue:AbstractObject = anchor.value as AbstractObject;
			anchor.value = null;
			if (group) {
				var action:ModifyRibbonAction = ModifyRibbonAction.fromObject(obj, anchor.path, oldValue);
				group.add(action);
			}
		}

		private static function assert(condition:*, message:String):void
		{
			if (condition==null || condition==false)
				throw new Error(message);
		}
	}
}
