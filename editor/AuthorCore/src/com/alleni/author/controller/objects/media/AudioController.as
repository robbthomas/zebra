package com.alleni.author.controller.objects.media
{
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractExternalObject;
	import com.alleni.author.model.objects.Audio;
	import com.alleni.author.model.ui.Asset;
	
	public class AudioController extends MediaController
	{
		public static var instance:AudioController = new AudioController();

		override protected function validateMediator(object:AbstractObject):void
		{
			var audio:Audio = object as Audio;
			
			if (audio.uri != AssetDescription.HOSTED_MEDIA_IDENTIFIER
					&& (audio.asset && audio.asset.assetID != null)) {
//				AssetController.instance.confirmDetachment(audio.asset);   // previously this decremented asset.count
			}
			
			if (AssetDescription.RTMP_REGEX.exec(audio.uri)) {
					//&& !(_mediators[object] as StreamAudioMediator)) {
				replaceMediatorForWith(object, new StreamAudioMediator().handleMediaEvents(audio));
				return;
			} else if (AssetDescription.FLASH_AUDIO_REGEX.exec(audio.uri)) {
					//&& !(_mediators[object] as ProgressiveAudioMediator)) {
				replaceMediatorForWith(object, new ProgressiveAudioMediator().handleMediaEvents(audio));
				return;
			}
			if (_mediators[object])
				IMediaMediator(_mediators[object]).handleMediaEvents(audio);
		}
		
		override protected function handleAssetUpdatedFor(object:AbstractExternalObject, oldAsset:Asset):void
		{
			super.handleAssetUpdatedFor(object, oldAsset);
			var audio:Audio = object as Audio;
			if (audio.asset && audio.asset.assetID)
				audio.uri = AssetDescription.HOSTED_MEDIA_IDENTIFIER;
		}
	}
}