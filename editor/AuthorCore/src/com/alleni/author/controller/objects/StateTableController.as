package com.alleni.author.controller.objects {
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.AbstractTable;
import com.alleni.author.model.objects.StateTable;
import com.alleni.author.view.objects.tables.AbstractTableView;
import com.alleni.taconite.model.TaconiteModel;

public class StateTableController extends TableController {
	public static var instance:StateTableController = new StateTableController();

    override public function warnBeforeDelete(object:AbstractObject):void {
        super.warnBeforeDelete(object);
        StateTable(object).stopAll();
    }
}
}
