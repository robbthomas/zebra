package com.alleni.author.controller.objects.media
{
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.model.objects.Audio;
	import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.media.ID3Info;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	public class AudioMediator extends AbstractMediaMediator implements IMediaMediator
	{
		protected var _audioObject:Audio;
		
		private var _soundTransform:SoundTransform;
		private var _soundChannel:SoundChannel;
		private var _sound:Sound;
		private var _soundURI:String;
		private var _audioIntervalID:uint = 0;

		override public function handleMediaEvents(object:ITaconiteObject):IMediaMediator
		{
			super.handleMediaEvents(object);
			_audioObject = Audio(_mediaObject);
			_segmentsPerSecond = 1000;
			// bootstrap values where availble (such as for buffered media)
			load();
			updateTimingTick(null);
			if (_duration > 0) {
				setSecondsElapsed(0);
			}
			return this;
		}
		
		override protected function addListeners():void
		{
			super.addListeners();
			_sound.addEventListener(IOErrorEvent.IO_ERROR, handleIOError, false, 0, true);
			_sound.addEventListener(Event.ID3, onID3, false, 0, true);
			_sound.addEventListener(Event.COMPLETE, handleBufferFull, false, 0, true);
			_soundChannel.addEventListener(Event.SOUND_COMPLETE, handlePlayComplete, false, 0, true);
		}
		
		override protected function clearListeners():void
		{
			super.clearListeners();
			_sound.removeEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			_sound.removeEventListener(Event.ID3, onID3);
			_sound.removeEventListener(Event.COMPLETE, handleBufferFull);
			_soundChannel.removeEventListener(Event.SOUND_COMPLETE, handlePlayComplete);
		}
		
		protected function onID3(event:Event):void 
		{
			try {
				var id3:ID3Info = event.target.id3; 
				if ("TLEN" in id3)
					_duration = id3["TLEN"];
				for (var propName:String in id3) 
					trace(propName + " = " + id3[propName]); 
			} catch(e:Error) {
				LogService.error("AudioMediator Caught Security Exception attempting to access ID3 metadata");
			}
		}
		
		override protected function handleAssetChange():void
		{
			pause();
			unsetPlayerStatus(PLAYER_LOADED);
			initialize();
		}
		
		private function load():void
		{
			_soundTransform = new SoundTransform(1);
			_soundChannel = new SoundChannel();
			if (!_audioObject.content) {
				var urlRequest:URLRequest = new URLRequest(_soundURI?_soundURI:_audioObject.uri);
				if (_soundURI) {
					var variables:URLVariables = new URLVariables;
					variables._method = RestHttpOperation.GET;
					urlRequest.url += "?" + variables;
					urlRequest.data = RestHttpOperation.postInsurance;
					urlRequest.method = URLRequestMethod.POST;
					urlRequest.requestHeaders.push(RestHttpOperation.basicAuthenticationHeader);
				}
				_sound = new Sound(urlRequest);
			} else {
				_sound = Sound(_audioObject.content);
			}
			if (_sound != null) {
				setPlayerStatus(PLAYER_LOADED);
			}
		}
		
		override public function start(position:Number=0, restart:Boolean=false):void
		{
			if (playerStatusIs(PLAY_HEAD_HUNG)) {
				unsetPlayerStatus(PLAY_HEAD_HUNG);
				stop();
			}
			if (!playerStatusIs(PLAYER_PLAYING) || playerStatusIs(PLAYER_PAUSED)) {
				unsetPlayerStatus(PLAYER_PAUSED);
				setPlayerStatus(PLAYER_STARTED);

				if (!_audioObject.content) {
					if (_audioObject.uri == AssetDescription.HOSTED_MEDIA_IDENTIFIER) {
						if (_audioObject.asset && Utilities.UUID_REGEX.test(_audioObject.asset.assetID)
								&& !playerStatusIs(PLAYER_LOADED)
								|| _accessToken && !_accessToken.valid) { // play from our back-end
							_soundURI = TaconiteFactory.getEnvironmentImplementation().apiURI + "asset/stream/" + _audioObject.asset.assetID;
							LogService.debug("Accessing audio uri " + _soundURI);
							play(position);
							return;
						} else {
							if (!_audioObject.asset || !Utilities.UUID_REGEX.test(_audioObject.asset.assetID)) {
								LogService.error("Request to play audio '" + _audioObject + "' cannot be fulfilled: need non-null asset ID");
								return;
							}
						}
					}
				}
				play(position);
				return;
			} else {
				if (playerStatusIs(PLAYER_PLAYING)) {
					if (restart) {
						_soundChannel.stop();
						play(0);
						return;
					}
					if (_audioObject.repeater) {
						/*
						separate from restart in case this might do something different such as 
						multiple plays composited as opposed to a single track restarted
						*/
						play(0);
						return;
					}
				}
			}
		}
		
		override protected function play(position:Number=0):void
		{
			if (!playerStatusIs(PLAYER_LOADED))
				load();
			else
				_soundChannel.stop();

			addListeners();
			_soundChannel = _sound.play(position, 1, _soundTransform);
			setVolume();
			_cuePointsHandler.clear();
			addCuePoints();
			if (!_audioObject.content)
				_audioObject.buffering = true;
			watchLoading = true;
			setPlayerStatus(PLAYER_PLAYING);
		}
		
		override protected function handleWatchLoading(event:Event):void
		{
			updateFractionLoaded(_sound.bytesLoaded, _sound.bytesTotal);
		}
		
		override public function pause():void
		{
			if (playerStatusIs(PLAYER_PLAYING)) { // soundChannel will be available if we've been playing.
				_soundChannel.stop();
				clearListeners();
			}
			setPlayerStatus(PLAYER_PAUSED);
            unsetPlayerStatus(PLAYER_PLAYING);
		}
		
		override protected function setTime(time:Number):void
		{
			super.setTime(time);
			if (playing)
				play(time);
			else
				pause();
		}
		
		override protected function setOffset(offset:Number):void
		{
			if (safeToUseOffset(offset, _soundChannel.position))
				setTime(offset/100 * _duration)
		}
		
		override protected function setVolume():void
		{
			if (Application.instance.audioEnabled)
				_soundTransform.volume = _audioObject.volume/100.0;
			else
				_soundTransform.volume = 0;
				
			_soundChannel.soundTransform = _soundTransform;
		}
		
		override protected function updateTimingTick(event:Event):void
		{
			if (_sound.length > _duration)
				_duration = _sound.length;
			updateTiming(_soundChannel.position, _duration);
		}
		
		override public function unload():void
		{
			super.unload();
			
			if (_soundChannel)
				_soundChannel.stop();
			_soundChannel = null;
			_sound = null;
		}
	}
}