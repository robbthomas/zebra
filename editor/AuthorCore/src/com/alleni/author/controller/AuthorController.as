/**
 * ¬¨¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller
{
import caurina.transitions.Tweener;

import com.alleni.author.Navigation.EventImageView;
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.EventPageEditingMediator;
import com.alleni.author.Navigation.EventPageView;
import com.alleni.author.Navigation.FlowEditingMediator;
import com.alleni.author.Navigation.FlowView;
import com.alleni.author.Navigation.IPage;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.Navigation.SmartObjectEditingMediator;
import com.alleni.author.application.ApplicationUI;
import com.alleni.author.application.dialogs.SettingsDialog;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.app.ClipboardController;
import com.alleni.author.controller.app.MenuController;
import com.alleni.author.controller.app.ProjectController;
import com.alleni.author.controller.objects.ArenaController;
import com.alleni.author.controller.objects.CompositeController;
import com.alleni.author.controller.objects.CompositeMediator;
import com.alleni.author.controller.objects.DefaultEditingMediator;
import com.alleni.author.controller.objects.IClipboardMediator;
import com.alleni.author.controller.objects.IEditingMediator;
import com.alleni.author.controller.objects.IReplaceableReceiver;
import com.alleni.author.controller.objects.IUndoMediator;
import com.alleni.author.controller.text.TextEditMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.controller.ui.CompositeSaveController;
import com.alleni.author.controller.ui.CompositeSaveControllerDefaultContext;
import com.alleni.author.controller.ui.GroupSelectionController;
import com.alleni.author.controller.ui.ReplacementController;
import com.alleni.author.controller.ui.ToggleButtonGroupController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.controller.ui.palettes.ToolboxController;
import com.alleni.author.definition.AssetType;
import com.alleni.author.definition.DragInfo;
import com.alleni.author.definition.InspectorPresets;
import com.alleni.author.definition.action.ActionGroup;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.definition.action.ActionTreeEvent;
import com.alleni.author.definition.action.CreateObjectAction;
import com.alleni.author.definition.action.DestroyObjectAction;
import com.alleni.author.definition.action.GroupCreateAction;
import com.alleni.author.definition.action.GroupDestroyAction;
import com.alleni.author.definition.action.ObjectDragAction;
import com.alleni.author.definition.action.ObjectLocation;
import com.alleni.author.definition.action.ObjectOrderAction;
import com.alleni.author.definition.action.ToggleButtonGroupAction;
import com.alleni.author.definition.action.UserAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.definition.text.FormattedText;
import com.alleni.author.document.Document;
import com.alleni.author.event.AssetEvent;
import com.alleni.author.event.PathEvent;
import com.alleni.author.event.TextObjectEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.AbstractTable;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.Drawing;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.EventFlowController;
import com.alleni.author.model.objects.PathObject;
import com.alleni.author.model.objects.TextObject;
import com.alleni.author.model.objects.ToggleButton;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.InspectorValues;
import com.alleni.author.model.ui.SetterWireAnchor;
import com.alleni.author.model.ui.TriggerWireAnchor;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.model.ui.Wiring;
import com.alleni.author.persistence.SVGEncoder;
import com.alleni.author.service.assets.AssetSaveOperation;
import com.alleni.author.util.ViewUtils;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadAsyncRunnable;
import com.alleni.author.util.thread.PseudoThreadRunnable;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.objects.DrawingView;
import com.alleni.author.view.text.IRichEditor;
import com.alleni.author.view.text.LightTextEditor;
import com.alleni.author.view.text.RichTextField;
import com.alleni.author.view.ui.GroupSelectionFeedback;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.author.view.ui.WireboardView;
import com.alleni.taconite.controller.TaconiteController;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.document.ObjectSelection;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.SelectEvent;
import com.alleni.taconite.factory.IEnvironment;
import com.alleni.taconite.factory.ISystemClipboard;
import com.alleni.taconite.factory.SerializerImplementation;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.model.GroupSelectionObject;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.service.LogService;
import com.alleni.taconite.view.ViewContext;

import flash.display.DisplayObjectContainer;
import flash.display.Sprite;

import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Dictionary;
import flash.utils.getTimer;

import flashx.textLayout.edit.TextScrap;
import flashx.textLayout.tlf_internal;

use namespace tlf_internal;

    /**
     * Application specific subclass of TaconiteController.  This class is responsible
     * for applying modifications to the application model on behalf of the presentation layer. 
     */
    public class AuthorController extends TaconiteController
	{
        private static var _instance:AuthorController;
	    private static const compileSerializer:SerializerImplementation = null;
		public var assetController:AssetController = AssetController.instance;
		private var _sessionStartTime:Number;
		private var _logEntryCount:int = 0;

		private static var _world:World;
		private static var _wiring:Wiring;
		
	    private var _editingMediators:Vector.<IEditingMediator> = new Vector.<IEditingMediator>();
	    private var _defaultEditingMediator:DefaultEditingMediator = new DefaultEditingMediator();
        private var _wasRunningWhenEditedEvent:Boolean;
        private var _wasRunningWhenShowedFlow:Boolean;

		private var _groupSelectionController:GroupSelectionController;
		private var _groupSelectionFeedback:GroupSelectionFeedback;

		private var _richTextEditor:IRichEditor;
		private var _secondaryTextEditor:IRichEditor;
		private var _lightTextEditor:LightTextEditor;
		private var _secondaryLightTextEditor:LightTextEditor;
		private var _textEditMediator:TextEditMediator;
		private var _secondaryTextEditMediator:TextEditMediator;

		
		public function AuthorController()
		{
            Utilities.assert(!_instance);
            _instance = this;
			super(Application.instance.document);
			assetController.controller = this;
			_sessionStartTime = getTimer();
            ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_CHANGING, handleProjectChanging);
            ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_INITIALIZED, handleProjectInitialized);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_CLOSING, handleProjectClosing);
            ApplicationController.addEventListener(NotificationNamesApplication.OPEN_SETTINGS_DIALOG, handleSettingsDialog);
			ApplicationController.addEventListener(NotificationNamesEditor.BRING_FORWARD, handleBringForward);
			ApplicationController.addEventListener(NotificationNamesEditor.BRING_TO_FRONT, handleBringToFront);
			ApplicationController.addEventListener(NotificationNamesEditor.SEND_BACK, handleSendBackward);
			ApplicationController.addEventListener(NotificationNamesEditor.SEND_TO_BACK, handleSendToBack);
			
			ApplicationController.addEventListener(NotificationNamesEditor.ALIGN_EDGES_LEFT, handleAlignEdgesLeft);
			ApplicationController.addEventListener(NotificationNamesEditor.ALIGN_EDGES_RIGHT, handleAlignEdgesRight);
			ApplicationController.addEventListener(NotificationNamesEditor.ALIGN_EDGES_TOP, handleAlignEdgesTop);
			ApplicationController.addEventListener(NotificationNamesEditor.ALIGN_EDGES_BOTTOM, handleAlignEdgesBottom);
			ApplicationController.addEventListener(NotificationNamesEditor.ALIGN_CENTERS_VERTICAL, handleAlignCentersVert);
			ApplicationController.addEventListener(NotificationNamesEditor.ALIGN_CENTERS_HORIZONTAL, handleAlignCentersHorz);
			ApplicationController.addEventListener(NotificationNamesEditor.DISTRIBUTE_VERTICAL, handleDistributeVert);
			ApplicationController.addEventListener(NotificationNamesEditor.DISTRIBUTE_HORIZONTAL, handleDistributeHorz);
			
            ApplicationController.addEventListener(NotificationNamesEditor.MAKE_OBJECT_OPEN, handleMakeObjectOpen);
            ApplicationController.addEventListener(NotificationNamesEditor.MAKE_OBJECT_CLOSE, handleMakeObjectClose);
			ApplicationController.addEventListener(NotificationNamesEditor.UNMAKE_OBJECT, handleUnMakeObject);
			ApplicationController.addEventListener(NotificationNamesEditor.MAKE_TOGGLEBUTTON_GROUP, handleMakeToggleButtonGroup);
			ApplicationController.addEventListener(NotificationNamesEditor.UNMAKE_TOGGLEBUTTON_GROUP, handleUnMakeToggleButtonGroup);
			ApplicationController.addEventListener(NotificationNamesEditor.MAKE_GRAPHIC, handleMakeGraphic);
			ApplicationController.addEventListener(NotificationNamesEditor.MAKE_ARENA, handleMakeArena);
			ApplicationController.addEventListener(NotificationNamesApplication.SELECT_ASSET, handleSelectReplaceable);
			ApplicationController.addEventListener(NotificationNamesApplication.SELECT_GADGET, handleSelectReplaceable);
			ApplicationController.addEventListener(NotificationNamesEditor.SELECT_OBJECT, handleSelectObject);
			ApplicationController.addEventListener(NotificationNamesEditor.DESELECT_OBJECT, handleDeselectObject);
			ApplicationController.addEventListener(NotificationNamesEditor.SELECT_ALL, handleSelectAll);
            ApplicationController.addEventListener(NotificationNamesEditor.SELECT_ALL_WITH_LOCKED, handleSelectAllWithLocked);
			ApplicationController.addEventListener(NotificationNamesEditor.DESELECT, handleDeselect);
			ApplicationController.addEventListener(NotificationNamesEditor.DELETE_SELECTED, handleDeleteSelected);
            ApplicationController.addEventListener(NotificationNamesEditor.GROUP_ITEMS,makeGroupHandler);
            ApplicationController.addEventListener(NotificationNamesEditor.UNGROUP_ITEMS,unmakeGroupHandler);
            ApplicationController.addEventListener(NotificationNamesEditor.EDIT_SELECTED_OBJECT, editSelectedObject);
            ApplicationController.addEventListener(NotificationNamesEditor.CLOSE_ONE_EDITOR, closeOneEditor);
        }

        public static function get instance():AuthorController
        {
            return _instance;
        }

        // high-level operations invoked directly from the UI

        public function runCommand():void
        {
            log("run");
            run();
        }

        public function runEventCommand(pageNumber:int, resume:Boolean):void
        {
            log("runEvent num="+pageNumber + " resume="+resume);
            closeEditors(null,
                function():void{  // onComplete
                    eventFlow.pageNumber = pageNumber;  // lazyload
                    PseudoThread.add(new PseudoThreadSimpleRunnable("runEventCommand", function():void {
                        if (!resume) {
                            var page:EventPage = eventFlow.currentEventPage;
                            page.resetObject(world);
                            for each (var master:EventPage in page.masters) {
                                if (master) {
                                    master.updateImage();
                                }
                            }
                        }
                        handleRunProject(true, pageNumber);  // resume=true
                    }))
                }
            );
        }

        public function resumeCommand():void
        {
            log("resume");
            // if currently editing an eventPage, resume that page
            var flow:EventFlow = world.eventFlow;
            var editedEvent:IPage = flow.editingPage;
            var pageNum:int = 0;
            if (editedEvent && !editedEvent.isMaster && !Application.instance.flowVisible) {
                pageNum = editedEvent.pageNumber;
            }
            handleRunProject(true, pageNum); // resume=true
        }

        public function pauseCommand():void
        {
            log("pause");
            eventFlow.titleBarRunning = false;
            closeEditors(null, function():void{
                world.eventFlow.disableLoadUnload = false;
                Application.instance.flowVisible = false;

                PseudoThread.add(new PseudoThreadSimpleRunnable("editResetCommand2", function():void {
                    if (!world.eventFlow.editingPage) {
                        editEvent(world.eventFlow.currentPage as EventPage);
                    }
                }));
            }, null, currentEventPageEditingMediator);
        }

        public function editResetCommand():void
        {
            log("editReset");
            eventFlow.titleBarRunning = false;
            closeEditors(null, function():void{
                if (Application.instance.flowVisible) {
                    toggleFlowVisible(false);
                }
                // may be editing an Event or could be running:  this will pause but not open editing
                PseudoThread.add(new PseudoThreadSimpleRunnable("editResetCommand", resetNow));

                PseudoThread.add(new PseudoThreadSimpleRunnable("editResetCommand2", function():void {
                    if (!world.eventFlow.editingPage) {
                        editEvent(world.eventFlow.currentPage as EventPage);
                    }
                }));
            }, null, currentEventPageEditingMediator);  // close path/text/gadget; keep the Event open for editing
        }

        public function toggleRunLock():void
        {
            setLocked(!Application.locked);
        }

        public function setLocked(locked:Boolean):void {
            Application.instance.document.locked = locked;
            if (Application.running) {
                //Closes text editor if editing while running.
                while (editing && !(currentEditingMediator is SmartObjectEditingMediator || currentEditingMediator is FlowEditingMediator)) {
                    makeObject();
                }
                ladiesAndGentlemen(true);
            }
           ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RUN_LOCK_TOGGLED));
        }

        private function resetNow():void {
            worldContainer.toggleRun(false);
            if(Application.instance.document.running == true){
                world.onPause();
                Application.instance.document.running = false;
            }
            world.resetObject(world);
            			// Clear the keyboard focus.  This has the side effect of setting any RichTextFields to not editing, which
			// keeps us from crashing.
			// TODO: remove once we figure out why how not to crash in RTF.
			ApplicationController.instance.resetKeyboardFocus(true);
				ApplicationController.instance.wireController.requestRedrawAllWires();
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RUNNING_STATE_CHANGED));
        }

        public function pause():void {
            worldContainer.toggleRun(false);
            if(Application.instance.document.running == true){
                world.onPause();
                Application.instance.document.running = false;
            }
            			// Clear the keyboard focus.  This has the side effect of setting any RichTextFields to not editing, which
			// keeps us from crashing.
			// TODO: remove once we figure out why how not to crash in RTF.
			ApplicationController.instance.resetKeyboardFocus(true);

		    ApplicationController.instance.wireController.requestRedrawAllWires();
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RUNNING_STATE_CHANGED));
        }

        public function run():void {
            handleRunProject(false);
        }

        public function resume():void {
            handleRunProject(true);
        }

        private function handleRunProject(resuming:Boolean, startPageNumber:int = 0):void
        {
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RUNNING_STATE_PENDING));
            clearSelection();
            var flow:EventFlow = world.eventFlow;
            flow.titleBarRunning = true;
            var pageNum:int;
            if (startPageNumber > 0) pageNum = startPageNumber;
            else if (resuming)  pageNum = flow.pageNumber;
            else  pageNum = flow.initialPageNumber;
            var page:EventPage = flow.pages[pageNum-1];
            ladiesAndGentlemen(true);
            var inputToken:Object = ApplicationController.instance.disableInput("handleRunProject");
            if (flow.editingPage) {
                saveEventImageAfterEdit(flow.editingPage);
            }

            var overlayToken:Object = null;

            // before running, close any active editing mediators and watch out for cancel button
            closeEditors(
                  // onBegin  (runs after gadget-replace-all dialog if not cancelled)
                  new PseudoThreadSimpleRunnable("handleRunProject/beginningOperation", function():void{
                      Application.instance.flowVisible = false;
                      hideAllVelumAndMC();  // before showOverlayImage, else vellum is captured
                      overlayToken = EventFlowController.instance.showOverlayImage(flow, page, "handleRunProject");
                      flow.ignorePageChanges = true;  // prevent extra load when closeEditors clears editingPage
                      hideLayer(worldContainer.feedbackControlsView);
                      hideLayer(worldContainer.feedbackAnnotationView);
                      PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                 }),
                 function():void{  // onComplete
                     flow.setPageNumberWithoutTransitionEffect(pageNum);
                     flow.ignorePageChanges = false;
                     flow.disableLoadUnload = false;  // triggers lazyLoad to be queued
                     PseudoThread.add(new PseudoThreadWaitFrameRunnable());  // let the views get ready for handleRunningChanged
                     PseudoThread.add(new PseudoThreadSimpleRunnable("handleRunProjectNow", handleRunProjectNow, this, [resuming]));
                     PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                     PseudoThread.add(new PseudoThreadSimpleRunnable("handleRunProject:finishing", function():void{
                        showLayer(worldContainer.feedbackControlsView);
                        // don't show feedbackAnnotationView since we're running
                        EventFlowController.instance.removeOverlayImage(flow, overlayToken);
                        ApplicationController.instance.enableInput(inputToken);
                     }));
                },
                function():void{   // onCancel
                        flow.ignorePageChanges = false;
                        ApplicationController.instance.enableInput(inputToken);
                    },
                    null); // keepOpen
        }

        private function handleRunProjectNow(resuming:Boolean):void
        {
            if(resuming) {
                resumeNow();
            } else {
                runNow();
            }
            Application.instance.showAll = false;
            if(EditorUI.instance != null){
                EditorUI.instance.topControlBar.showAllButton.selected = false;
            }
            Application.instance.currentTool = ToolboxController.ARROW;
        }

        private function runNow():void {
            worldContainer.toggleRun(true);
            world.resetObject(world);
            Application.instance.document.running = true;
            Application.instance.showAll = false;
            if(EditorUI.instance != null){
                EditorUI.instance.topControlBar.showAllButton.selected = false;
            }
            world.onResume();
            if(world.lms) {
                world.lms.initializeLms();
            }
            			// Clear the keyboard focus.  This has the side effect of setting any RichTextFields to not editing, which
			// keeps us from crashing.
			// TODO: remove once we figure out why how not to crash in RTF.
			ApplicationController.instance.resetKeyboardFocus(true);
            				// clear selection before going into run to prevent confusion with dragging objects during run
				clearSelection();
				document.selection = null;

            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RUNNING_STATE_CHANGED));
        }

        private function resumeNow():void {
            worldContainer.toggleRun(true);
            Application.instance.document.running = true;
            Application.instance.showAll = false;
            if(EditorUI.instance != null){
                EditorUI.instance.topControlBar.showAllButton.selected = false;
            }
            world.onResume();
            if(world.lms) {
                world.lms.initializeLms();
            }
            			// Clear the keyboard focus.  This has the side effect of setting any RichTextFields to not editing, which
			// keeps us from crashing.
			// TODO: remove once we figure out why how not to crash in RTF.
			ApplicationController.instance.resetKeyboardFocus(true);
            				// clear selection before going into run to prevent confusion with dragging objects during run
				clearSelection();
				document.selection = null;

            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RUNNING_STATE_CHANGED));
        }

        public function editWhileRunningStart():Boolean {
            if(Application.running == false) {
                return false;
            }
            Application.instance.editWhileRunningInProgress = true;
            Application.instance.document.running = false;
            world.onPause();
            return true;
        }

        public function editWhileRunningEnd():void {
            if(Application.instance.editWhileRunningInProgress) {
                Application.instance.editWhileRunningInProgress = false;
                var page:EventPage = world.eventFlow.currentPage as EventPage;
                if (page) {
                    page.logEditWhileRunChanges();  // remember to save changes just made in the page gadget
                }
            }
        }

        public function ladiesAndGentlemen(running:Boolean):void
        {
            if (EditorUI.instance) {
               EditorUI.instance.ladiesAndGentlemen(running);
            }
        }

        private function get worldContainer():WorldContainer
        {
            return Application.instance.viewContext as WorldContainer;
        }

        private function hideLayer(layer:DisplayObjectContainer):void
        {
            if (layer) {
                layer.visible = false;
            }
        }

        private function showLayer(layer:DisplayObjectContainer):void
        {
            if (layer) {
                layer.visible = true;
            }
        }

		public function get cleanSelection():Vector.<TaconiteModel>
		{
			return DragInfo.toModels(currentEditingMediator.getSelection(false));  // omitLocked=true
		}

		private function processUndoForAlignOrDistribute(action:Function,description:String):void
		{
            if (Application.uiRunning) {
                return;
            }
			var models:Vector.<TaconiteModel> = cleanSelection;
			var group:ActionGroup = new ActionGroup(description);
			var actions:Vector.<ObjectDragAction> = new Vector.<ObjectDragAction>();
			var oldLocations:Vector.<ObjectLocation> = new Vector.<ObjectLocation>();
			var newLocations:Vector.<ObjectLocation> = new Vector.<ObjectLocation>();
			var i:uint = 0;
			var ao:AbstractObject;
			
			for each(var m:TaconiteModel in models) {
				ao = m.value as AbstractObject;
				oldLocations.push(ObjectLocation.fromObject(ao));
			}
			
			action(models);
			
			for each(var m2:TaconiteModel in models) {
				ao = m2.value as AbstractObject;
				newLocations.push(ObjectLocation.fromObject(ao));
			}
			
			for each(var m3:TaconiteModel in models) {
				ao = m3.value as AbstractObject;
				group.add(new ObjectDragAction(ao.uid, ao.title, oldLocations[i], newLocations[i]));
				i++;
			}
			ApplicationController.instance.authorController.currentActionTree.commit(group);
		}
		
		private function handleAlignEdgesLeft(e:ApplicationEvent=null):void
		{
			processUndoForAlignOrDistribute(ObjectAlignmentManager.objAlignLeft,NotificationNamesEditor.ALIGN_EDGES_LEFT);
		}
		
		private function handleAlignEdgesRight(e:ApplicationEvent=null):void
		{
			processUndoForAlignOrDistribute(ObjectAlignmentManager.objAlignRight,NotificationNamesEditor.ALIGN_EDGES_RIGHT);
		}
		
		private function handleAlignEdgesTop(e:ApplicationEvent=null):void
		{
			processUndoForAlignOrDistribute(ObjectAlignmentManager.objAlignTop,NotificationNamesEditor.ALIGN_EDGES_TOP);
		}
		
		private function handleAlignEdgesBottom(e:ApplicationEvent=null):void
		{
			processUndoForAlignOrDistribute(ObjectAlignmentManager.objAlignBottom,NotificationNamesEditor.ALIGN_EDGES_BOTTOM);
		}
		
		private function handleAlignCentersVert(e:ApplicationEvent=null):void
		{
			processUndoForAlignOrDistribute(ObjectAlignmentManager.objAlignVertical,NotificationNamesEditor.ALIGN_CENTERS_VERTICAL);
		}
		
		private function handleAlignCentersHorz(e:ApplicationEvent=null):void
		{
			processUndoForAlignOrDistribute(ObjectAlignmentManager.objAlignHorizontal,NotificationNamesEditor.ALIGN_CENTERS_HORIZONTAL);	
		}
		
		private function handleDistributeVert(e:ApplicationEvent=null):void
		{
			processUndoForAlignOrDistribute(ObjectAlignmentManager.objDistributeVertical,NotificationNamesEditor.DISTRIBUTE_VERTICAL);
		}
		
		private function handleDistributeHorz(e:ApplicationEvent=null):void
		{
			processUndoForAlignOrDistribute(ObjectAlignmentManager.objDistributeHorizontal,NotificationNamesEditor.DISTRIBUTE_HORIZONTAL);
		}
		
		public function editSelectedObject(event:ApplicationEvent = null):void
		{
            if (Application.uiRunning) {
                return;
            }
			var selection:Vector.<TaconiteModel> = cleanSelection;
			if (selection.length == 1) {
				const obj:AbstractObject = selection[0].value as AbstractObject;
				if (obj as Composite) {
					CompositeMediator.openEditing(obj as Composite);
				} else if (obj as PathObject) {
					PathObject(obj).dispatchEvent(new PathEvent(PathEvent.OPEN_EDITING));
				} else if (obj is TextObject) {
                    if (!(event.data)){
                        (obj as TextObject).getView().dispatchEvent(new TextObjectEvent(TextObjectEvent.OPEN_EDITING));
                    }
                }
			}
		}

        public function toggleRunEdit():void
        {
            if (Application.uiRunning) {
                pauseCommand();
            } else {
                resumeCommand();
            }
        }

		public function handleEnterKey(shift:Boolean=false):void
		{
            if(ApplicationUI.instance.mouseIsDown){
                return;
            }
            if (!shift) {
                closeOneEditor();
            } else if (isPro && shift && eventFlow.editingPage) {  // Creator should press j to resume, not Enter key
                _wasRunningWhenShowedFlow = false;
                closeEventEditingAndTweenToFlow();
            }
        }

        private function closeOneEditor(event:Event = null):void
        {
            // the Close Event or Close gadget or Close path menu-item has been selected
            // or Enter-key pressed.   Close one vellum or path obj.  Does not close EventPage
            var editor:IEditingMediator = currentEditingMediator;
            if (editor && !(editor is EventPageEditingMediator)) {
                if (editor is SmartObjectEditingMediator) {
                    closeEditingCurrentSmartObject();   // gadget
                } else {
                    makeObject();    // close Path object
                }
            }
		}

        private function closeEventEditingAndTweenToFlow():void
        {
            ladiesAndGentlemen(false);
            var token:Object = ApplicationController.instance.disableInput("closeEventEditingAndTweenToFlow");  // the tween also disables input, but thats ok
            var oldWireLevel:int = wiring.wiringLevel;
            var flow:EventFlow = world.eventFlow;
            var wasEditing:EventPage = flow.editingPage as EventPage;
            // operation to be run after gadget replace-all dialog (if not cancelled), before closing & saving all the gadgets
            var beginningOperation:PseudoThreadRunnable = new PseudoThreadAsyncRunnable("tweenBackFromEdit", function(signalCompletion:Function):void{
                rememberOpenRibbonCategories(wasEditing);
                hideLayer(worldContainer.feedbackAnnotationView);
                tweenBackFromEdit(wasEditing, signalCompletion);  // sets Application.instance.flowVisible = true
            });

            closeEditors(beginningOperation,
                function():void{  // onComplete
                    world.eventFlow.disableLoadUnload = true;  // after saving gadgets & taking snapshots, unload events
                    PseudoThread.add(new PseudoThreadSimpleRunnable("restoreWiringLevel after unload", function():void{
                        restoreWiringLevel(oldWireLevel);
                        showLayer(worldContainer.feedbackAnnotationView);
                        ApplicationController.instance.enableInput(token);
                    }));
                    },
                function():void{  // onCancel
                    restoreWiringLevel(oldWireLevel);
                    ApplicationController.instance.enableInput(token);
                    });
        }

        private function rememberOpenRibbonCategories(page:EventPage):void
        {
            var view:EventImageView = eventFlow.flowView.getModelView(page);
            if (view && view.messageCenterView) {
                var mc:MessageCenterView = view.messageCenterView;
                page.ribbonCategoriesToOpen = mc.arrayOfOpenCategories;
            }
        }

        private function hideAllVelumAndMC():void
        {
            for(var n:int=_editingMediators.length-1; n >= 0; n--) {
                var ed:SmartObjectEditingMediator = _editingMediators[n] as SmartObjectEditingMediator;
                if (ed) {
                    ed.hideVelum();
                    var composite:Composite = ed.editingObject as Composite;
                    if (composite) {
                        composite.messageCenterVisible = false;
                    }
                }
            }
        }

        private function afterCloseEventEditing():void
        {
            if (!ProjectController.instance.doingProjectSave) {
                if (_wasRunningWhenEditedEvent) {
                    resume();
                }
            }
        }

		public function closeEditingCurrentSmartObject():void
		{
			var inner:SmartObject = currentEditingMediator.editingObject as SmartObject;
			if (inner is Composite) {  // not EventPage
				var keepOpen:SmartObject = inner.wireScopeContainer as SmartObject;
				closeSmartObjects(inner, keepOpen);
			}
		}

		
		/**
		 * Close all text editing, path editing, gadget velum editing
         * @param onBegin  operation to perform after the replace-all dialog, but before saving the gadgets
         * @param onComplete  function callback():void
		 * @param keepOpen  close editors down to this level, then stop.
         * @param saveChanges  when this is false we are abandoning editor, so may not properly revert changes.
		 * 
		 */
		public function closeEditors(onBegin:PseudoThreadRunnable=null, onComplete:Function=null, onCancel:Function=null, keepOpen:IEditingMediator=null, saveChanges:Boolean=true):void
		{
			// close text editing & path editing  
			while (editing && !(currentEditingMediator is SmartObjectEditingMediator || currentEditingMediator is FlowEditingMediator)) {
				makeObject();
			}
			if (!editing) {
                if (onBegin != null) {
                    PseudoThread.add(onBegin);
                }
                if (onComplete != null) {
                    PseudoThread.add(new PseudoThreadSimpleRunnable("closeEditors:onComplete", onComplete));  // all done now
                }
			} else if (currentVelumMediator) {
				// close gadget editing
				var closeObj:SmartObject = currentVelumMediator.editingObject as SmartObject;
                if (closeObj) {
                    var keepObj:SmartObject = null;
                    if (keepOpen) {
                        keepObj = keepOpen.editingObject as SmartObject;
                    }
                    var beginActions:Vector.<PseudoThreadRunnable> = new <PseudoThreadRunnable>[];
                    var ep:EventPage = eventFlow.editingEventPage;
                    if (ep != null && ep != keepObj) {
                        beginActions.push(new PseudoThreadSimpleRunnable("closeEditors:onBegin", function():void {
                            ep.currentlyEditing = false;
                        }));
                    }
                    if (onBegin != null) {
                        beginActions.push(new PseudoThreadWaitFrameRunnable());  // give objects time to respond to currentlyEditing=false before eventPage.updateImage is done
                        beginActions.push(onBegin);
                    }
                    closeSmartObjects(closeObj, keepObj, saveChanges, beginActions, onComplete, onCancel);
                }
			}
		}

		private function closeSmartObjects(inner:SmartObject, keepOpen:SmartObject, saveChanges:Boolean=true, beginActions:Vector.<PseudoThreadRunnable>=null, onComplete:Function=null, onCancel:Function=null):void
		{
			log("closeGadget " + inner + " keepOpen=" + (keepOpen ? keepOpen.logStr : ""));
			clearSelection();
            var token:Object = ApplicationController.instance.disableInput("closeSmartObjects");
            var reEnable:PseudoThreadSimpleRunnable = new PseudoThreadSimpleRunnable("Enable Input", ApplicationController.instance.enableInput, null,  [token])
            var completeActions:Vector.<PseudoThreadRunnable> = new <PseudoThreadRunnable>[reEnable];
            var cancelActions:Vector.<PseudoThreadRunnable> = new <PseudoThreadRunnable>[reEnable];

            if(onComplete != null) {
                completeActions.unshift(new PseudoThreadSimpleRunnable("closeSmartObjects onComplete", onComplete));
                completeActions.unshift(new PseudoThreadWaitFrameRunnable());
            }
            if(onCancel != null) {
                cancelActions.unshift(new PseudoThreadSimpleRunnable("closeSmartObjects onCancel", onCancel));
                //cancelActions.unshift(new PseudoThreadWaitFrameRunnable());
            }
			new CompositeSaveController(new CompositeSaveControllerDefaultContext()).closeComposites(inner, keepOpen, saveChanges, beginActions, completeActions, cancelActions);
		}

        public function abandonEditors():void
        {
            if (Application.instance.document == null) return;

            // this will leave the project in an undefined state, since gadgets are not properly reverted, and ignorePageChanges is left true
            clearSelection();
            world.eventFlow.ignorePageChanges = true;
            world.eventFlow.abandoned = true;
//            closeEditors(null, null, null, null, false);   // saveChanges=false
        }

		public function makeObject():void
		{
			// makeObject no longer saves the current editing gadget
			currentEditingMediator.makeObject();
			
			///// TODO: pass this to ToolboxController to check if the tool is locked or not
			if (ToolboxController.instance.drawingTools.indexOf(Application.instance.currentTool) != -1) // && ToolboxController.instance.currentToolEnabled)
				Application.instance.currentTool = ToolboxController.ARROW;
		}

        public function toolboxToggledOn():void
        {
            if (Application.instance.flowVisible) {
                // let author edit the backstage
                if (Application.uiRunning) {
                    pause();
                }
            } else {
                // running, so edit the current event
                editCurrentEvent();
            }
        }

        public function toggleFlowVisible(tween:Boolean):void
        {
            if(Application.instance.document.project.accountTypeId < 3) {
                return;
            }
            var vis:Boolean = !Application.instance.flowVisible;
            var flow:EventFlow = world.eventFlow;
            var flowView:FlowView = flow.flowView;
            var inputToken:Object = null;
            log("toggleFlowVisible newVisible="+vis + " tween="+tween);
            clearSelection();
            if (vis) {  // going to show the flowView
                _wasRunningWhenShowedFlow = Application.uiRunning;
                ladiesAndGentlemen(false);
                if (flow.editingPage) {
                    if (tween) {
                        closeEventEditingAndTweenToFlow();
                    } else {
                        closeEditors(
                            // beginningOperation
                            new PseudoThreadSimpleRunnable("toggleFlowVisible/beginningOperation", function():void{
                                flow.ignorePageChanges = true; // prevents lazyLoad when editingPage goes null by closeEditors
                                saveEventImageAfterEdit(flow.editingPage);
                                rememberOpenRibbonCategories(flow.editingPage as EventPage);
                                hideLayer(worldContainer.feedbackAnnotationView);
                                Application.instance.flowVisible = true;
                                PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                                PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                            }),
                            function():void{  // onComplete
                                PseudoThread.add(new PseudoThreadSimpleRunnable("finishJumpToFlow", finishJumpToFlow, this, [flow, inputToken]));
                            }
                        );
                    }
                } else { // must have been running
                    inputToken = ApplicationController.instance.disableInput("toggleFlowVisible/fromRunning");
                    if (tween) {
                        // tweenBackFromEdit sets Application.instance.flowVisible = true
                        var oldWireLevel:int = tweenBackFromEdit(flow.currentPage as EventPage, function():void{
                            if (Application.uiRunning) {
                                pause();  // do this after tween, else it would show extra Arena controlbars
                                hideLayer(worldContainer.feedbackAnnotationView);  // pause just showed this
                            }
                            restoreWiringLevel(oldWireLevel);
                            PseudoThread.add(new PseudoThreadSimpleRunnable("finishJumpToFlow", finishJumpToFlow, this, [flow, inputToken]));
                        });
                    } else {
                        Application.instance.flowVisible = true;
                        if (Application.uiRunning) {
                            pause();
                            hideLayer(worldContainer.feedbackAnnotationView);  // pause just showed this
                        }
                        PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                        PseudoThread.add(new PseudoThreadSimpleRunnable("finishJumpToFlow", finishJumpToFlow, this, [flow, inputToken]));
                    }
                }

            } else { // hiding the flowView, going to the stage
                var edit:Boolean = true;
                var page:EventPage = flow.wasEditingEventPage;
                if (page == null || _wasRunningWhenShowedFlow) {
                    page = flow.currentPage as EventPage;
                    edit = false;
                }
                Utilities.assert(page.object.parent);
                if (edit) {
                    editEvent(page as EventPage, tween);
                } else {  // start running
                    flow.titleBarRunning = true;
                    inputToken = ApplicationController.instance.disableInput("toggleFlowVisible/resume");
                    var overlayToken:Object = EventFlowController.instance.showOverlayImage(flow, page, "toggleFlowVisible");
                    hideLayer(worldContainer.feedbackControlsView);
                    hideLayer(worldContainer.feedbackAnnotationView);  // leave this hidden since going to Run
                    if (tween) {
                        flowView.tweenForEditEvent(page as EventPage, function():void{
                            finishHideFlowAndRun(flow, inputToken, overlayToken);
                        });
                    } else {
                        finishHideFlowAndRun(flow, inputToken, overlayToken);
                    }
                }
            }
        }

        private function finishJumpToFlow(flow:EventFlow, inputToken:Object):void
        {
            flow.disableLoadUnload = true;
            flow.ignorePageChanges = false;  // enqueue unloading

            PseudoThread.add(new PseudoThreadSimpleRunnable("finishJumpToFlow", function():void{
                showLayer(worldContainer.feedbackAnnotationView);
                ApplicationController.instance.enableInput(inputToken);
            }));
        }

        private function finishHideFlowAndRun(flow:EventFlow, inputToken:Object, overlayToken:Object):void
        {
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());  // let overlayImage show
            Application.instance.flowVisible = false;
            flow.disableLoadUnload = false;  // triggers enqueue lazyLoad
            PseudoThread.add(new PseudoThreadSimpleRunnable("finishHideFlowAndRun1", function():void{
                if (!Application.uiRunning) {
                    resumeNow();
                }
            }));
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            PseudoThread.add(new PseudoThreadSimpleRunnable("finishHideFlowAndRun2", function():void{
                showLayer(worldContainer.feedbackControlsView);
                // do not show feedbackAnnotationView since we're running
                EventFlowController.instance.removeOverlayImage(flow, overlayToken);
                ApplicationController.instance.enableInput(inputToken);
            }));
        }

        private function tweenBackFromEdit(page:EventPage, onComplete:Function):int
        {
            clearSelection();
            var oldWireLevel:int = wiring.wiringLevel;
            if (oldWireLevel == 4) {
                wiring.wiringLevel = 3;  // hide wiring of Event child objects, since Event remains open during tween
            }
            hideAllVelumAndMC();
            saveEventImageAfterEdit(page);
            world.eventFlow.flowView.tweenBackFromEdit(page as EventPage, onComplete);   // sets Application.instance.flowVisible
            return oldWireLevel;
        }

        private function restoreWiringLevel(oldLevel:int):void
        {
            if (oldLevel != wiring.wiringLevel) {
                wiring.wiringLevel = oldLevel;
                ApplicationController.instance.wireController.showWiresForLevel();
            }
        }

        public function saveEventImageAfterEdit(page:IPage):void
        {
            var ep:EventPage = page as EventPage;
            ep.hadChildren = true;  // immediately hide empty-page in flow
            ep.updateImage(true);
            for each (var master:EventPage in ep.masters) {
                if (master) {
                    master.updateImage();
                }
            }
        }

        public function editCurrentEvent():void
        {
            if (!world.eventFlow.editingPage) {
                editEvent(world.eventFlow.currentPage as EventPage, false);
            }
        }

        public function editEvent(page:EventPage, tween:Boolean=false):void
        {
            if (!Application.instance.flowVisible) {
                tween = false;
            }
            if(page == null){
                return;
            }
            log("editEvent: tween="+tween + " page="+page.logStr);

            Utilities.assert(page.parent);
            if (page.editing) {
                return;
            }
            if (!ProjectController.instance.doingProjectSave) {
                _wasRunningWhenEditedEvent = Application.uiRunning;
            }
            if (Application.uiRunning) {
                pause();
            }
            clearSelection();

            var flow:EventFlow = world.eventFlow;
            flow.titleBarRunning = false;
            var overlayToken:Object = null;
            var inputToken:Object = null;  // set this inside beginningOperation so won't be done if cancel the replace-all dialog
            var beginningOperation:PseudoThreadRunnable;
            if (tween) {
                beginningOperation = new PseudoThreadAsyncRunnable("editEvent/beginningOperation/tween", function(signalCompletion:Function):void{
                    inputToken = ApplicationController.instance.disableInput("editEvent");
                    overlayToken = EventFlowController.instance.showOverlayImage(flow, page, "editEvent");
                    flow.disableLoadUnload = true;
                    flow.flowView.tweenForEditEvent(page, function():void{
                        flow.pageHolderVisible = false; // hide backstage objects
                        Application.instance.flowVisible = false;
                        signalCompletion();  // tell PseudoThreadAsyncRunnable that our work is done
                    });
                });
            } else {
                beginningOperation = new PseudoThreadSimpleRunnable("editEvent/beginningOperation/noTween", function():void{
                    inputToken = ApplicationController.instance.disableInput("editEvent");
                    Application.instance.flowVisible = false;
                    page.dockMessageCenterToStage = true;
                    overlayToken = EventFlowController.instance.showOverlayImage(flow, page, "editEvent");
                });
            }

            var pageIsTheOldOne:Boolean = (page == flow.wasEditingEventPage);  // closeEditors may change wasEditingEventPage
            closeEditors(beginningOperation,
                function():void{  // onComplete
                    if (flow.wasEditingEventPage) {
                        saveEventImageAfterEdit(flow.wasEditingEventPage);
                    }
                    if (pageIsTheOldOne) {
                        page = flow.wasEditingEventPage;
                    }
                    PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                    PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                    var view:EventPageView = null;
                    PseudoThread.add(new PseudoThreadSimpleRunnable("editEvent:openEventEditingNow", function():void{
                        view = openEventEditingNow(page);
                        flow.ignorePageChanges = false;
                        flow.disableLoadUnload = false;  // enqueue lazyLoad
                    }));
                    PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                    PseudoThread.add(new PseudoThreadSimpleRunnable("editEvent:finish", function():void{
                        if (view) {
                            view.restoreSelection();
                        }
                        flow.pageHolderVisible = true;
                        page.currentlyEditing = true;   // after lazyLoad
                        EventFlowController.instance.removeOverlayImage(flow, overlayToken);
                        ApplicationController.instance.enableInput(inputToken);
                    }));
                });
        }

        private function openEventEditingNow(page:EventPage):EventPageView
        {
            if (page.editing) {
                return null;
            }
            if (page.parent == null) {  // probably replace-all just replaced this gadget
                return null;
            }

            var flow:EventFlow = page.parent as EventFlow;
            var view:EventPageView = flow.eventPagerView.getViewForModel(page) as EventPageView;
            view.openEditing();

            var index:int = flow.pages.getItemIndex(page);
            if (index >= 0) {
                flow.pageNumber = index+1;  // Project MC should show the most recently viewed Event
            }
            return view;
        }

        /**
         * Go to neighboring event-page:
         * @param incr: when incr = +1 go to next page, when incr = -1 go to prev page.
         * @param tween
         */
        public function gotoNextPage(incr:int, tween:Boolean=false):void
        {
            var flow:EventFlow = eventFlow;
            if (!flow.editingEventPage) {
                flow.pageNumber += incr;    // running
                return;
            }
            var newPage:EventPage = null;
            if (flow.editingEventPage) {  //  allow moving between masters & events
                var oldIndex:int = flow.getIndexForPage(flow.editingEventPage);
                if (oldIndex >= 0) {
                    var newIndex:int = oldIndex + incr;
                    newPage = flow.getPageForIndex(newIndex);  // null if beyond legal values
                }
            }
            if (newPage) {
                if (tween) {
                    tweenStageToNextEditingPage(newPage, incr);
                } else {
                    editEvent(newPage, tween);
                }
            }
        }

        private function tweenStageToNextEditingPage(newPage:EventPage, incr:int):void
        {
            var oldWiringLevel:int = ApplicationController.instance.wireController.getWiringLevel();
            ApplicationController.instance.wireController.setWiringLevel(1, false);
            var token:Object = ApplicationController.instance.disableInput("tweenStageToNextEditingPage");
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            PseudoThread.add(new PseudoThreadSimpleRunnable("prepareTweenToNextPage", prepareTweenToNextPage, this, [newPage, incr, token, oldWiringLevel]));
        }

        private function prepareTweenToNextPage(newPage:EventPage, incr:int, token:Object, oldWiringLevel:int):void
        {
            _holderForTweenNextPage = new Sprite();
            EditorUI.instance.leftBox.rawChildren.addChild(_holderForTweenNextPage);
            var holderPoint:Point = _holderForTweenNextPage.globalToLocal(new Point(0,0));
            _holderForTweenNextPage.x = holderPoint.x;
            _holderForTweenNextPage.y = holderPoint.y;

            var oldPageImage:Sprite = ViewUtils.instance.captureEditingWorld();
            _holderForTweenNextPage.addChild(oldPageImage);
            worldContainer.visible = false;
            eventFlow.doingTweenToNextPage = true;  // ensure the progress-bar doesn't stop prematurely
            AuthorController.instance.editEvent(newPage);  // close editing, lazyload, buildout
            PseudoThread.add(new PseudoThreadSimpleRunnable("tweenToNextPage", tweenToNextPage, this, [oldPageImage, newPage, incr, token, oldWiringLevel]));
        }

        private var _holderForTweenNextPage:Sprite;

        public var tweenFrac:Number;

        private function tweenToNextPage(oldPageImage:Sprite, newPage:EventPage, incr:int, token:Object, oldWiringLevel:int):void
        {
            var flow:EventFlow = eventFlow;
            flow.doingTweenToNextPage = false;
            ProgressBarController.instance.stop();
            if (flow.editingEventPage != newPage) {    // cancelled out of replace-all
                finishTweenToNextPage(oldPageImage, newPageImage, token, oldWiringLevel);
                return;
            }

            var topLeft:Point = worldContainer.localToGlobal(new Point(0,0));
            var botRight:Point = worldContainer.localToGlobal(new Point(flow.width,flow.height));
            var moveX:Number;
            if (incr > 0) { // forward
                moveX = topLeft.x - WireboardView.WIDTH - EditorUI.instance.width;  // start with left edge of page at right edge of window
            } else {
                moveX = botRight.x + WireboardView.WIDTH;  // start with right edge of page at left edge of window
            }

            flow.eventPagerView.updateTitleBarImmediately();  // ensure titlebar is correct color in capture (master v/s event)
            var newPageImage:Sprite = ViewUtils.instance.captureEditingWorld();
            _holderForTweenNextPage.addChild(newPageImage);
            newPageImage.x = -moveX;

            tweenFrac = 0;
            var oldStartX:Number = oldPageImage.x;
            var newStartX:Number = newPageImage.x;
            Tweener.addTween(this, {tweenFrac:1, time:FlowView.tweenTimeEdit, transition:FlowView.tweenTypeEdit,
                onUpdate:function():void{
                    oldPageImage.x = oldStartX + tweenFrac * moveX;
                    newPageImage.x = newStartX + tweenFrac * moveX;
                },
                onComplete:function():void{
                    finishTweenToNextPage(oldPageImage, newPageImage, token, oldWiringLevel);
                }
            });
        }

        private function finishTweenToNextPage(oldPageImage:Sprite, newPageImage:Sprite, token:Object, oldWiringLevel:int):void
        {
            worldContainer.visible = true;
            _holderForTweenNextPage.parent.removeChild(_holderForTweenNextPage);
            _holderForTweenNextPage = null;
            ApplicationController.instance.enableInput(token);
            ApplicationController.instance.wireController.setWiringLevel(oldWiringLevel, false);

            // kludge to ensure the "Editing" ribbon updates in the Event MC
            var view:EventImageView = eventFlow.flowView.getModelView(eventFlow.editingEventPage);
            if (view) {
                var mc:MessageCenterView = view.messageCenterView;
                if (mc) {
                    mc.alpha += 0.01;
                }
            }
        }

        public function showEditingWorldOverlay():Sprite
        {
            // create a shield to prevent flashing during project-save.  Similar to prepareTweenToNextPage().
            var holder:Sprite = new Sprite();
            EditorUI.instance.leftBox.rawChildren.addChild(holder);
            var holderPoint:Point = holder.globalToLocal(new Point(0,0));
            holder.x = holderPoint.x;
            holder.y = holderPoint.y;
            var image:Sprite = ViewUtils.instance.captureEditingWorld();
            holder.addChild(image);
            worldContainer.visible = false;
            return holder;
        }

        public function removeEditingWorldOverlay(holder:Sprite):void
        {
            worldContainer.visible = true;
            holder.parent.removeChild(holder);
        }


        public function addEventPageBy(page:IPage, after:Boolean, duplicate:Boolean):void
        {
            trace("addEventPageBy page="+page.object.title, "after="+after, "duplicate="+duplicate);
            var wasEditing:Boolean = page.object.editing;
            var flowIndex:int = eventFlow.getIndexForPage(page);
            var token:Object;
            AuthorController.instance.closeEditors(
                new PseudoThreadSimpleRunnable("addPage", function():void{   // onBegin
                        token = ApplicationController.instance.disableInput("addPage");
                        PseudoThread.add(new PseudoThreadWaitFrameRunnable());
                    }),
                function():void {   // closeEditors onComplete
                    page = eventFlow.getPageForIndex(flowIndex);  // since page was replaced by closing editing
                    trace("afterCloseEdit: page="+page, "parent="+page.object.parent);
                    addPageNow(page, after, duplicate);
                    PseudoThread.add(new PseudoThreadSimpleRunnable("gotoNewPage", function():void{
                        gotoNewPage(flowIndex, after, wasEditing);
                    }));
                    PseudoThread.add(new PseudoThreadSimpleRunnable("addPage", function():void{
                        LibraryController.instance.updateForAssetChange(true);
                        ApplicationController.instance.enableInput(token);
                    }));
            });
        }

        private function addPageNow(page:IPage, after:Boolean, duplicate:Boolean):void
        {
            editWhileRunningStart();
            EventFlowController.instance.addPageBy(eventFlow, page,  after, duplicate);
            editWhileRunningEnd();
        }

        private function gotoNewPage(existingFlowIndex:int, after:Boolean, edit:Boolean):void
        {
            if (Application.instance.flowVisible) {
                return;
            }
            // at stage and adjustPageNumbersForAddition has already set flow.pageNumber to the new page, unless its a master
            if (edit) {
                // in case we were editing a master-page we still need to compute the new page
                var flowIndex:int = existingFlowIndex;
                if (after) {
                    ++flowIndex;  // index of the new page
                }
                var page:EventPage = eventFlow.getPageForIndex(flowIndex) as EventPage;
                trace("gotoNewPage existing="+existingFlowIndex, "index="+flowIndex, "page="+page, "flow.overlay="+eventFlow.overlayImage);
                editEvent(page as EventPage);
            } else {
                resumeNow();
            }
        }

        private function handleSettingsDialog(event:ApplicationEvent):void
        {
            ApplicationController.instance.createPopUp(SettingsDialog, true);
        }

        private function makeGroupHandler(e:ApplicationEvent):void
		{
            if (Application.uiRunning) {
                return;
            }
			var token:Object = currentActionTree.openGroup("Group");
			unmakeGroupHandler(null);  // ensure that previous groups will be restored, if this Group action is undone

       		var group:GroupSelectionObject = GroupSelectionController.makeGroup(new ObjectSelection(world.modelRoot, cleanSelection));
			var action:UserAction = GroupCreateAction.fromGroup(group);
			currentActionTree.commit(action);
			currentActionTree.closeGroup(token);
			log("makeGroup id="+group.uid, cleanSelection);
       }

        private function unmakeGroupHandler(e:ApplicationEvent):void
		{
            if (Application.uiRunning) {
                return;
            }
        	var list:Vector.<GroupSelectionObject> = GroupSelectionController.dischargeSelection(new ObjectSelection(world.modelRoot, cleanSelection));
			var token:Object = currentActionTree.openGroup("Ungroup");
			for each (var group:GroupSelectionObject in list) {
				currentActionTree.commit(GroupDestroyAction.fromGroup(group));
				log("unmakeGroup " + group.uid);
			}
			currentActionTree.closeGroup(token);
        }

		public function isCurrentlyEditingObject(models:Vector.<TaconiteModel>):Boolean
		{
			for each (var model:TaconiteModel in models) {
				var obj:AbstractObject = model.value as AbstractObject;
                if (obj.editing) {
                    return true;
                }
			}
			return false;
		}
		
	    public function get editing():Boolean
		{
		    return _editingMediators.length > 1;  // FlowEditingMediator is always active
	    }

        public function get indexOfEventBeingEdited():int
        {
            var result:int = -1;
            var flow:EventFlow = world.eventFlow;
            if (flow.editingPage) {
                result = flow.getModelIndex(flow.editingPage);
            }
            return result;
        }

	    public function startingEditing(mediator:IEditingMediator,concurrent:Boolean = false):void
		{
		    if(currentEditingMediator == mediator) {
			    return;
		    }
		    if(editing) {
				if(mediator as SmartObjectEditingMediator) {
					while(editing && currentEditingMediator is SmartObjectEditingMediator == false) {
						makeObject();
					}
				} else if(mediator as TextEditMediator && !concurrent) {
					while(editing && currentEditingMediator is TextEditMediator) {
						makeObject();
					}
				}
		    }
			_editingMediators.push(mediator);
			if (mediator.hasActiveState) {
				for(var n:int=_editingMediators.length-1; n >= 0; n--) {
					var ed:IEditingMediator = _editingMediators[n];
					ed.activeMediatorChanged(mediator);
				}
			}
            MenuController.instance.syncTopMenuBarItems();
			MenuController.instance.setupUndoListener();
			MenuController.instance.updateUndoItems();
	    }

	    public function closingEditing(mediator:IEditingMediator, saveChanges:Boolean):void
		{
		    var idx:int = _editingMediators.indexOf(mediator);

		    if(idx < 0)
			    return; // this mediator isn't one of the editing ones.
			
			var i:int;
			var length:int = _editingMediators.length;
		    for(i = length; i > idx + 1; i--) {
			    var child:IEditingMediator = _editingMediators.pop();
			    if(saveChanges) {
				    child.makeObject();
			    } else {
				    child.cancelEditing();
			    }
		    }
		    _editingMediators.pop();
		    if(idx != _editingMediators.length) {
			    LogService.error("Wrong number of editing mediators in closingEditing");
		    }
			if (_editingMediators.length > 0) {
				var activeMediator:IEditingMediator = _editingMediators[_editingMediators.length-1];
				for(var n:int=_editingMediators.length-1; n >= 0; n--) {
					var ed:IEditingMediator = _editingMediators[n];
					ed.activeMediatorChanged(activeMediator);
				}
                MenuController.instance.setupUndoListener();
             	MenuController.instance.updateUndoItems();
			}
	    }

        public function get currentContainer():AbstractContainer
        {
            if (eventFlow.editingEventPage) {    // when editing a gadget, currentContainer is still the EventPage
                return eventFlow.editingEventPage;
            } else if (!Application.instance.flowVisible) {
                return eventFlow.currentEventPage;
            } else {
                return eventFlow;  // global backstage
            }
        }

        /**
         * @return null if there is no editing mediator other than flow and event page mediators
         */
        public function get currentNonFlowEditingMediator():IEditingMediator
        {
            var result:IEditingMediator = currentEditingMediator;
            if(result is FlowEditingMediator) {
                return null;
            }
            return result;
        }

	    public function get currentEditingMediator():IEditingMediator
		{
            if(_editingMediators.length == 0) {
                return _defaultEditingMediator;
            }
		    return _editingMediators[_editingMediators.length-1];
	    }

        public function get currentEventPageEditingMediator():EventPageEditingMediator
        {
            if(_editingMediators.length < 2) {
                return null;
            }
            var result:EventPageEditingMediator = _editingMediators[1] as EventPageEditingMediator;
            return result; // just return null if the cast fails
        }

        public function get currentFlowEditingMediator():FlowEditingMediator
        {
            if(_editingMediators.length < 1) {
                return null;
            }
            var result:FlowEditingMediator = _editingMediators[0] as FlowEditingMediator;
            return result; // just return null if the cast fails
        }

	    public function get currentUndoMediator():IUndoMediator
		{
		    for(var i:int = _editingMediators.length-1; i >= 0; i--) {
			    var result:IUndoMediator = _editingMediators[i] as IUndoMediator;
			    if(result != null) {
				    return result;
			    }
		    }
		    return _defaultEditingMediator;
	    }

	    public function get currentClipboardMediator():IClipboardMediator
		{
		    for(var i:int = _editingMediators.length-1; i >= 0; i--) {
			    var result:IClipboardMediator = _editingMediators[i] as IClipboardMediator;
			    if(result != null) {
				    return result;
			    }
		    }
		    return _defaultEditingMediator;
	    }
		
		public function editingMediatorIndex(mediator:IEditingMediator):int
		{
			return _editingMediators.indexOf(mediator);
		}
		
		public function editingMediatorForIndex(index:int):IEditingMediator
		{
			if (index >= 0 && index < _editingMediators.length) 
				return _editingMediators[index];
			return null;
		}
		
		public function editingMediatorForObject(obj:ITaconiteObject):IEditingMediator
		{
			for(var i:int = _editingMediators.length-1; i >= 0; i--) {
				var ed:IEditingMediator = _editingMediators[i];
				if(ed.editingObject == obj) {
					return ed;
				}
			}
			return null;
		}

        public function editorNames(m:Vector.<IEditingMediator>):Array {
            var result:Array = [];
            for each(var ed:IEditingMediator in m) {
                result.push(ed.editingObject is AbstractObject ? AbstractObject(ed.editingObject).title : "NONE");
            }
            return result;
        }
		
		public function get currentVelumMediator():SmartObjectEditingMediator
		{
			for(var i:int = _editingMediators.length-1; i >= 0; i--) {
				var result:SmartObjectEditingMediator = _editingMediators[i] as SmartObjectEditingMediator;
				if(result != null) {
					return result;
				}
			}
			return null;
		}


	    public function get currentActionTree():ActionTree
		{
		    for(var i:int = _editingMediators.length-1; i >= 0; i--) {
			    var mediator:IEditingMediator = _editingMediators[i];
			    if(mediator != null && mediator.actionTree != null) {
				    return mediator.actionTree;
			    }
		    }
		    return Application.instance.document.actionTree;
	    }
		
		private function handleProjectChanging(event:ApplicationEvent = null):void
		{
			trace("handleProjectChanging doc="+document);
			this.document = Application.instance.document;
			var doc:Document = this.document as Document;
			_world = doc.root.value as World;
			_wiring = doc.wiringRoot.value as Wiring;
			doc.addEventListener(SelectEvent.CHANGE_SELECTION,handleSelectionChange);

		}

        private function handleProjectInitialized(event:ApplicationEvent = null):void
        {
            if (ApplicationController.instance.invokedAsEditor) {
                world.eventFlow.actionTree.addEventListener(ActionTreeEvent.TYPE, handleMainEdit);
            }
		}

		private function handleProjectClosing(event:ApplicationEvent = null):void
		{
			trace("handleProjectClosing doc="+document);
			// the ProjectController will have already closed as needed for saving
			// now anything left open just needs to close down
			while (editing)
				currentEditingMediator.makeObject();
			
			var doc:Document = this.document as Document;
			if (doc) {
				doc.removeEventListener(SelectEvent.CHANGE_SELECTION, handleSelectionChange);
				world.eventFlow.actionTree.removeEventListener(ActionTreeEvent.TYPE, handleMainEdit);
			}
		}
		
		/**
		 * The World that is the root value object of our document model.
		 */
        public function get world():World
        {
            return _world;
        }

        public function get eventFlow():EventFlow
        {
            return _world.eventFlow;
        }

		/**
		 * The Wiring which forms the interactivity
		 */
		public function get wiring():Wiring
		{
			return _wiring;
		}

        private function handleMakeObjectOpen(event:ApplicationEvent):void{
            if (!Application.uiRunning) {
                CompositeController.instance.handleMakeObject(true);
            }
        }
		
        private function handleMakeObjectClose(event:ApplicationEvent):void{
            if (!Application.uiRunning) {
                CompositeController.instance.handleMakeObject(false);
            }
        }
		
		private function handleUnMakeObject(event:ApplicationEvent = null):void
		{
            if (!Application.uiRunning) {
			    CompositeController.instance.handleUnmakeObject();
            }
		}
		
		private function handleMakeToggleButtonGroup(event:ApplicationEvent = null):void
		{
            if (Application.uiRunning) {
                return;
            }
			var models:Vector.<TaconiteModel> = cleanSelection;
			var list:Vector.<ToggleButton> = new Vector.<ToggleButton>();
			log("makeButtonSet", models);
			for each(var m:TaconiteModel in cleanSelection) {
				var btn:ToggleButton = m.value as ToggleButton;
				if (btn == null) {
                    //MESSAGE NOT IN DOCUMENT
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, ["Not a button","Must be radio-button, checkbox or button"]));
					return;
				} else {
					list.push(btn);
				}
			}
			if (list.length > 0) {  // maybe the buttons were locked, so cleanSelection was empty
				var oldGrouping:Object = ToggleButtonGroupController.instance.buttonGroupingToJson(list);
				ToggleButtonGroupController.instance.makeToggleButtonGroup(list);  
				ApplicationController.currentActionTree.commit(ToggleButtonGroupAction.fromButtons(list, "Make Button Set", oldGrouping));
			}
		}
		
		private function handleUnMakeToggleButtonGroup(event:ApplicationEvent = null):void
		{
            if (Application.uiRunning) {
                return;
            }
			var models:Vector.<TaconiteModel> = cleanSelection;
			log("unMakeButtonSet", models);
			
			var list:Vector.<ToggleButton> = new Vector.<ToggleButton>();
			for each(var m:TaconiteModel in models) {
				var btn:ToggleButton = m.value as ToggleButton;
				if (btn) {
					list.push(btn);
				}
			}
			if (list.length > 0) {
				var oldGrouping:Object = ToggleButtonGroupController.instance.buttonGroupingToJson(list);
				ToggleButtonGroupController.instance.unmakeToggleButtonGroup(list);
				ApplicationController.currentActionTree.commit(ToggleButtonGroupAction.fromButtons(list, "Unmake Button Set", oldGrouping));
			}
		}

		private function handleMakeArena(event:ApplicationEvent = null):void
		{
            if (Application.uiRunning) {
                return;
            }
			var viewBox:Rectangle = selectionBounds;
			if (!viewBox) return;
			var savedSelection:Vector.<TaconiteModel> = cleanSelection;
			log("makeArena",savedSelection);
			var group:ActionGroup = new ActionGroup("Make Arena");
            // (wait to add CreateObjectAction for the arena until reparenting is finished; see below...)
            group.add(DestroyObjectAction.fromSelection(savedSelection));

			var arena:Arena = ObjectFactory.createForName("Arena",
                    currentContainer, 0, viewBox.left + viewBox.width/2, viewBox.top + viewBox.height/2, viewBox.width, viewBox.height) as Arena;
			var controller:ArenaController = arena.controller as ArenaController;

			selectSingleModel(arena.model);
			// initialize the arena by giving it a page
			controller.createFirstPageIfNeeded(arena);
			var ao:AbstractObject;
			// reparent the objects
			for each(var m:TaconiteModel in savedSelection) {
				ao = m.value as AbstractObject;
				ObjectDragMediator.changeParentObject(ao, arena.currentArenaPage);
				ao.setInitialZIndex(true);
			}
			
			// set up presets
			var values:InspectorPresets = InspectorValues.instance.getPreset(arena.shortClassName) as InspectorPresets;
			for (var property:String in values) {
				if (arena.hasOwnProperty(property)) arena[property] = values[property];
			}
			for each (property in (values.messageCenterVisible as Array)) {
				if (arena.anchors.hasOwnProperty(property)) (arena.anchors[property] as WireAnchor).messageCenterVisible = true;
			}
			for each (property in (values.propertyInspectorVisible as Array)) {
				if (arena.anchors.hasOwnProperty(property)) (arena.anchors[property] as WireAnchor).propertyInspectorVisible = true;
			}
			arena.layer = int.MAX_VALUE;  // ensure it drops into front arena.  changed below.
			arena.parent.model.setChildIndex(arena, arena.parent.model.numChildren-1);
			dropIntoContainer(arena);
			
			// the new arena has now dropped into the appropriate container
			// now we fix the layer to match the front sibling
			var highest:int = 0;
			var arenaParent:AbstractContainer = arena.parent as AbstractContainer;
			for each (ao in arenaParent.objects) {
				if (ao != arena && ao.layer > highest)
					highest = ao.layer;
			}
			arena.layer = highest;  // changing from the temporary MAX_VALUE down to the sibling value ... but still stays in front
			
			// update z index values of arena siblings
			for each (ao in arenaParent.objects) {
				ao.setInitialZIndex(true);  // changedParent=false
			}

            // the arena is now prepared for adding a CreateObjectAction to the undo action group
            group.add(CreateObjectAction.fromObject(arena));
            ApplicationController.instance.authorController.currentActionTree.commit(group);

			// show the message center
			arena.controller.objectCompleted(arena);
		}

		private var _tokenForMakeGraphic:Object;

		private function handleMakeGraphic(event:ApplicationEvent = null):void
		{
            if (Application.uiRunning) {
                return;
            }
			// create target graphic to hold new vectors
			var bounds:Rectangle = selectionBounds;
			if (!bounds) return;
			var xx:Number = bounds.left + bounds.width/2;  // center point
			var yy:Number = bounds.top + bounds.height/2;
			var drawing:Drawing = ObjectFactory.createForName("Drawing", currentContainer,-1, xx, yy, bounds.width, bounds.height) as Drawing;
			dropIntoContainer(drawing);

			// encode selected objects into SVG XML
			var savedAnchor:Point = drawing.anchorPoint;
			drawing.anchorPoint = new Point(0,0);  // temporarily set anchor point to topLeft, needed by encoder
			var encoder:SVGEncoder = new SVGEncoder(drawing);
			var savedSelection:Vector.<TaconiteModel> = cleanSelection;
			log("makeGraphic", savedSelection);
			for each (var m:TaconiteModel in savedSelection)
			{
				encoder.addView((m.value as AbstractObject).getView());
			}
			
			// build undo actions to delete old graphics
			var group:ActionGroup = new ActionGroup("Make Graphic");
			group.add(DestroyObjectAction.fromSelection(savedSelection));
			ApplicationController.instance.authorController.currentActionTree.commit(group);
			// store a reference to the undo action so we can append to it when the save operation is complete
			drawing.pendingMakeGraphicAction = group;
			
			// wait for SVG to render before saving asset, to prevent a flash, by ensuring the drawing shows before the save-complete function deletes old objs
			drawing.addEventListener(DrawingView.SVG_VIEWER_COMPLETE, handleSVGViewerComplete);
			drawing.content = encoder.xml;  // initiates building the svg viewer
			drawing.anchorPoint = savedAnchor;
			
			// prevent dragging object or closing gadget (close gadget will crash if drawing assetID is not yet back from server)
			// dragging the drawing could reveal the original objects underneath, which looks like a bug
            _tokenForMakeGraphic = ApplicationController.instance.disableInput("handleMakeGraphic");
		}
		
		private function handleSVGViewerComplete(event:Event):void
		{
			var drawing:Drawing = event.target as Drawing;
			drawing.removeEventListener(DrawingView.SVG_VIEWER_COMPLETE, handleSVGViewerComplete);
//			trace("handleSVGViewerComplete",drawing);

			// kick-off save operation
			var assetSaveOperation:AssetSaveOperation = new AssetSaveOperation(null, drawing.title, drawing.content, AssetType.SVG);
			assetSaveOperation.addEventListener(AssetEvent.COMPLETE, function(e:AssetEvent):void{handleMakeGraphicSaveComplete(e, drawing)});
			assetSaveOperation.execute();
		}
		
		private function handleMakeGraphicSaveComplete(e:AssetEvent,drawing:Drawing):void
		{
//			trace("handleMakeGraphicSaveComplete");
			AssetController.instance.attachFor(drawing, e.asset);
			// delete the old objects
			drawing.pendingMakeGraphicAction.perform();
			// add action to create drawing, for Redo command
			drawing.pendingMakeGraphicAction.add(CreateObjectAction.fromObject(drawing));
			
			// select the newly created graphic
			selectSingleModel(drawing.model);
			drawing.controller.objectCompleted(drawing);  // create message center
            drawing.initialLoadDimensionsApplied = true;  // so that Shrink-to-fit will function later when a bitmap is dropped in
            drawing.initialValues.initialLoadDimensionsApplied = true;  // ensure it gets into gadget data
            ApplicationController.instance.enableInput(_tokenForMakeGraphic);
            _tokenForMakeGraphic = null;
		}
		
		public function get selectionBounds():Rectangle
		{
			var bounds:Rectangle = null;
			for each(var m:TaconiteModel in cleanSelection)
			{
				var obj:AbstractObject = m.value as AbstractObject;
				var view:ObjectView = obj.getView();
				var rect:Rectangle = view.getBounds(world.getView());
				if(bounds)
					bounds = bounds.union(rect);
				else
					bounds = rect;
			}
			return bounds;
		}

		private function handleSelectReplaceable(event:ApplicationEvent = null):void
		{
			var replaceable:IReplaceable = event.data as IReplaceable;
			if (replaceable)
				selectModels(getObjectModelsForReplaceable(replaceable, true));  // lookInClosedGadgets=true
		}
        private function handleSelectionChange(event:SelectEvent):void
        {
            if (_groupSelectionFeedback && worldContainer.feedbackGroupHandlesView.contains(_groupSelectionFeedback)) {
                worldContainer.feedbackGroupHandlesView.removeChild(_groupSelectionFeedback);
                _groupSelectionFeedback.warnViewToDelete();
            }
            GroupSelectionController.showFeedbackFor(event.selection);
        }
		
        public function get groupSelectionController():GroupSelectionController {
            return _groupSelectionController;
        }

		private function handleSelectObject(event:ApplicationEvent = null):void
		{
			if(event.data is TaconiteModel) {
				selectSingleModel(event.data as TaconiteModel);
			} else {
				selectSingleModel(event.data.model as TaconiteModel, event.data.extend as Boolean);
			}
		}
		
		private function handleDeselectObject(event:ApplicationEvent = null):void
		{
			deselectSingleModel(event.data as TaconiteModel);
		}
		
		private function handleSelectAll(event:ApplicationEvent):void
		{
			this.currentEditingMediator.selectAll(false);  // includeLocked=boolean
		}

        private function handleSelectAllWithLocked(event:ApplicationEvent):void
		{
			this.currentEditingMediator.selectAll(true);  // includeLocked=boolean
		}

		private function handleDeselect(event:ApplicationEvent):void
		{
			clearSelection();
		}
		
		private function handleDeleteSelected(event:ApplicationEvent):void
		{
			if(!Application.uiRunning && !Application.instance.wireDragDescription){
				currentEditingMediator.deleteEditing();
                LibraryController.instance.updateForAssetChange();
			}
		}

        public function deselectDescendantsOf(obj:AbstractObject, reverse:Boolean = false):void
        {
            var deselect:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
            var selection:Vector.<TaconiteModel> = ApplicationController.instance.authorController.selection.selectedModels;
            for each (var m:TaconiteModel in selection) {
                var d = m.isDescendant(obj.model)
                if ((reverse)?!d:d) {
                    deselect.push(m);
                }
            }
            ApplicationController.instance.authorController.modifyMultiSelection(deselect);
        }
		
		override protected function filterDroppedFile(e:MouseEvent):void
		{
			var localX:Number = 0;
			var localY:Number = 0;
			if (e as MouseEvent) {
				var dropPointLocal:Point = Application.instance.viewContext.globalToLocal(new Point(MouseEvent(e).localX, MouseEvent(e).localY));
				localX = dropPointLocal.x;
				localY = dropPointLocal.y;
			} 
			if (e as MouseEvent && e.relatedObject != null)
				handlePastedObject(e.relatedObject, localX, localY);  // e.relatedObject is an AssetBox dragged from palette
			else
				pasteFromSystemClipboard(localX,localY,e);
		}

        public function addObjectForName(name:String, x:Number=0, y:Number=0, width:Number=25, height:Number=25, select:Boolean=true, parent:AbstractObject=null, index:Object = -1):AbstractObject
        {
			if (parent == null) {
                if (parent == null) {
                    parent = currentContainer;
                }
            }

			var newObject:AbstractObject = ObjectFactory.createForName(
						name,
						parent, index,
            			x, y,
            			width, height);

			log("addObject  " + newObject.logStr);
	        if(select) selectSingleModel(newObject.model);
			return newObject;
        }
		
		public function dropIntoContainer(obj:AbstractObject):void
		{
			var velumMediator:SmartObjectEditingMediator = currentVelumMediator;
			if (velumMediator != null) {
				// ensure pasted object will go into the Gadget vellum
				ObjectDragMediator.changeParentObject(obj, velumMediator.editingObject as SmartObject);
			}
            if (!(obj as SmartObject && obj.editing)) {
				var view:ObjectView = obj.getView();
				if (view)
					ObjectDragMediator.dropIntoContainer(view);
			}
			obj.setInitialZIndex();
			obj.initialValues["x"] = obj.x;
			obj.initialValues["y"] = obj.y;
		}
        
		
		override protected function handleDragOver(e:MouseEvent):void
		{
			// test mouse point for potential replacement candidates
			ReplacementController.instance.testForReplacement(e);
		}
		
		override protected function handlePastedObjects(obj:Object, localX:Number=0, localY:Number=0, event:Event=null):void
		{
            if (Application.uiRunning) {
                return;
            }
			var clipboard:ISystemClipboard = TaconiteFactory.getClipboardImplementation(event);
            ClipboardController.instance.pasteClipboardObjects(obj, localX, localY, event);
		}
		
		override protected function handlePastedText(text:String, localX:Number=0, localY:Number=0, event:Event=null):void
		{
            if (Application.uiRunning) {
                return;
            }
			log("handlePastedText tx="+text);
			var textObject:TextObject = addObjectForName("TextObject", localX, localY, 100, 20, true, ClipboardController.instance.parentForPaste(null)) as TextObject;
			textObject.source = FormattedText.fromString(text);
            textObject.controller.handleCreationInitialize(textObject);
            textObject.controller.objectCompleted(textObject);
		}
		
		override protected function handlePastedTextScrap(scrap:TextScrap, localX:Number=0, localY:Number=0, event:Event=null):void
		{
            if (Application.uiRunning) {
                return;
            }
			log("handlePastedTextScrap scrap="+scrap);
			var textObject:TextObject = addObjectForName("TextObject", localX, localY, 100, 20, true, ClipboardController.instance.parentForPaste(null)) as TextObject;
			textObject.source = FormattedText.fromTextFlow(scrap.textFlow);
            textObject.controller.handleCreationInitialize(textObject);
            textObject.controller.objectCompleted(textObject);
		}
		
		override protected function handlePastedBitmap(data:*, localX:Number=0, localY:Number=0, event:Event=null):void
		{
            if (Application.uiRunning) {
                return;
            }
			AssetController.instance.insertFromBitmap(data, localX, localY);
		}
		
		override protected function handlePastedFiles(fileList:Array, localX:Number=0, localY:Number=0, event:Event=null):void
		{
			// TODO: pass the list onto the AssetImportController
		}
		
		override protected function handlePastedURL(url:String, localX:Number=0, localY:Number=0, event:Event=null) : void
		{
            if (Application.uiRunning) {
                return;
            }
			AssetController.instance.insertFromURL(url, localX, localY);
		}
		
		override protected function handlePastedObject(relatedObject:Object, localX:Number=0, localY:Number=0, event:Event=null):void
		{
			//if (relatedObject.toString().match(AssetDescription.ASSET_BOX_REGEXP))
				// if we ever need to handle native drags from other windows, this is an example of how to handle it.
		}
        
        public static function get nextSerial():uint
        {
            if(_world == null) {
                return -1;
            }
            return _world.currentSerial++;
        }
        
        override protected function handleSelectEvent(e:SelectEvent) : void
        {
        	super.handleSelectEvent(e);
        	
			// PLAYERTODO
			if (!Application.runningLocked)
				ApplicationController.instance.wireController.showWiresForLevel();
        }
		
		private function handleBringForward(event:ApplicationEvent = null):void
		{
			// advance each model by the number of selected models in the same parent ("count").
			// so if you select 3 objects and BringForward, they each move up 3 places.
			// but if you also select other objects in a different arena, those 3 still move by 3.
			renumberSelection("Bring forward", NotificationNamesEditor.BRING_FORWARD, true);
		}
		
		private function handleBringToFront(event:ApplicationEvent = null):void
		{
            renumberSelection("Bring to front", NotificationNamesEditor.BRING_TO_FRONT, true);
		}

        public function bringFrontWithoutUndo():void
        {
            renumberSelection("Bring to front", NotificationNamesEditor.BRING_TO_FRONT, true, false);
        }

		private function handleSendBackward(event:ApplicationEvent = null):void
		{
            renumberSelection("Send backward", NotificationNamesEditor.SEND_BACK, false);
		}
		
		private function handleSendToBack(event:ApplicationEvent = null):void
		{
            renumberSelection("Send to back", NotificationNamesEditor.SEND_TO_BACK, false);
		}

        private function renumberSelection(actionTitle:String, op:String, backward:Boolean, commitUndoAction:Boolean = true):void
        {
            if (Application.uiRunning) {
                return;
            }
            var models:Vector.<TaconiteModel> = cleanSelection;
            if (models.length>0) {
                if (commitUndoAction) {
                    log(op, cleanSelection);
                }
                clearSelection();  // unclearSelection is done below

                var indexes:Vector.<int> = AbstractContainer.computeReorderIndexes(op, models);
                var group:ActionGroup = new ActionGroup(actionTitle);
                // loop through models to change indices to match new position
                var count:int = models.length;
                for (var n:int=0; n < count; n++) {
                    var nn:int = backward ? count-n-1 : n;  // reverse the order when moving backward
                    var m:TaconiteModel = models[nn];
                    var obj:AbstractObject = m.value as AbstractObject;
                    var newIndex:int = indexes[nn];
                    var action:ObjectOrderAction = ObjectOrderAction.fromObject(obj, "", newIndex);
                    group.add(action);
                }
                group.perform();
                if (commitUndoAction) {
                    currentActionTree.commit(group);
                }

                // bring back selection from cache
                unclearSelection();
            }
        }

		public function handleShowAll(show:Boolean):void
		{
            world.showAllObjects(show);
		}

        public function getObjectModelsForReplaceable(target:IReplaceable, lookInClosed:Boolean=false):Vector.<TaconiteModel>
        {
            var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
            findReplaceableInObjects(target, eventFlow.objectsCurrentPageOuterScope(), lookInClosed, models);
            return models;
        }

        public function findReplaceableInObjects(target:IReplaceable, objects:Vector.<AbstractObject>, lookInClosed:Boolean, models:Vector.<TaconiteModel>):void
        {
            for each (var object:AbstractObject in objects) {
                var receiver:IReplaceableReceiver = object as IReplaceableReceiver;
                if (receiver && sameItem(receiver.replaceable, target)) {
                    models.push(object.model);
                }
                var composite:Composite = object as Composite;
                if (composite && (lookInClosed || composite.editing)) {
                    findReplaceableInObjects(target, composite.allObjectsInWiringScope(), lookInClosed, models);
                }
            }
        }

        private function sameItem(one:IReplaceable, two:IReplaceable):Boolean
        {
            // when drag from cloud you get same ID but different Project object
            if (one == null) {
                return false;
            } else if (one == two) {
                return true;
            } else {
                return (one && two && one.id != null && one.id == two.id);
            }
        }

		override public function removeSelection():void
		{
			// should not be called, since it could delete a composite being edited
			throw new Error("removeSelection");
		}

	    public function getObjectByTitle(title:String, obj:AbstractObject = null):AbstractObject
		{
		    if (obj == null) {
                obj = AuthorController.instance.currentContainer as AbstractContainer;
		    }
		    if (obj.title.match(new RegExp("([\\w\\s]*"+title+"[\\w\\s]*)"))) {
				return obj;
		    } else if (obj is AbstractContainer && !(obj is Composite)) {
			    for each (var child:AbstractObject in AbstractContainer(obj).objects) {
					var result:AbstractObject = getObjectByTitle(title, child);
				    if (result != null) {
					    return result;
				    }
			    }
		    }
		    return null;
	    }

	    public function handleObjectRemoval(removing:AbstractObject, replacement:AbstractObject=null):void
		{
			if (removing.parent == null)
				throw new Error("handleObjectRemoval");
		    if (removing as AbstractContainer && removing is SmartObject == false) {
			    for each(var child:AbstractObject in AbstractContainer(removing).objects) {
				    handleObjectRemoval(child);
			    }
		    }
		    removing.wireScopeContainer.allObjectsInScope(function(obj:AbstractObject):Boolean {
			    if(obj.snapTo == removing) {
				    obj.snapTo = replacement;
			    }
				for each(var anchor:Object in obj.anchors) {
					var trigger:TriggerWireAnchor = anchor as TriggerWireAnchor;
					if(trigger && trigger.condition == removing) {
						trigger.condition = replacement;
					}
                    var setter:SetterWireAnchor = anchor as SetterWireAnchor;
                    if(setter && setter.inValue == removing) {
                        setter.inValue = replacement;
                    }
				}
                var page:IPage = obj as IPage;
                if(page) {
                    var masterIndex:int = page.masters.getItemIndex(removing);
                    if(masterIndex >= 0) {
                        page.masters.removeItemAt(masterIndex);
                        page.masters.addItem(replacement);
                    }
                }
                var table:AbstractTable = obj as AbstractTable;
                if(table) {
                    for(var stateIndex:int = 0; stateIndex < table.allStateValues.length; stateIndex++) {
                        if(table.allStateValues.getItemAt(stateIndex) == removing) {
                            table.allStateValues.setItemAt(replacement, stateIndex);
                        }
                    }
                    for(var portValueIndex:int = 0; portValueIndex < table.portValue.length; portValueIndex++) {
                        if(table.portValue.getItemAt(portValueIndex) == removing) {
                            table.portValue.setItemAt(replacement, portValueIndex);
                        }
                    }
                }
			    return true;
		    });
	    }
		
		public function getRichTextEditor():IRichEditor
		{	
			if(!_richTextEditor){
				_richTextEditor = new RichTextField(38,15);
				_richTextEditor.formattable = false;
				(_richTextEditor as RichTextField).alwaysUseTextFlow = false;
			}
			
			return _richTextEditor;
		}
		
		public function getSecondaryRichTextEditor():IRichEditor
		{	
			if(!_secondaryTextEditor){
				_secondaryTextEditor = new RichTextField(38,15);
				_secondaryTextEditor.formattable = false;
				(_secondaryTextEditor as RichTextField).alwaysUseTextFlow = false;
			}
			
			return _secondaryTextEditor;
		}
		
		public function getLightTextEditor():LightTextEditor
		{	
			if(!_lightTextEditor){
				_lightTextEditor = new LightTextEditor();
			}
			
			return _lightTextEditor;
		}
		public function getSecondaryLightTextEditor():LightTextEditor
		{	
			if(!_secondaryLightTextEditor){
				_secondaryLightTextEditor = new LightTextEditor();
			}
			
			return _secondaryLightTextEditor;
		}
		
		public function getTextEditMediator():TextEditMediator
		{	
			if(!_textEditMediator){
				_textEditMediator = new TextEditMediator();
			}
			
			return _textEditMediator;
		}
		public function getSecondaryTextEditMediator():TextEditMediator
		{	
			if(!_secondaryTextEditMediator){
				_secondaryTextEditMediator = new TextEditMediator();
			}
			
			return _secondaryTextEditMediator;
		}
		
		public function repositionLightTextEditor():void{
			if(_lightTextEditor)
			_lightTextEditor.reposition();
		}
		
		/**
		 * Apply some function to all selected objects.
		 */
		private function applyToTopLayerSelection(topLevelModels:Vector.<TaconiteModel>, f:Function):void
		{
			for each (var m:TaconiteModel in topLevelModels)
			{
				f(m);
			}
		}
				
		public function nudge(delta:Point):void
		{
            var selection:Vector.<TaconiteModel> = cleanSelection;
            if (selection.length > 0) {
                editWhileRunningStart();
                var token:* = ApplicationController.instance.authorController.currentActionTree.openGroup("Nudge");
                applyToTopLayerSelection(selection, function(m:TaconiteModel):void {
                    var obj:AbstractObject = m.value as AbstractObject;
                    if(!obj.editingLocked) {
                        nudgeObject(obj, delta);
                    }
                });
                ApplicationController.instance.authorController.currentActionTree.closeGroup(token);
                editWhileRunningEnd();
            }
		}
		
		private function nudgeObject(obj:AbstractObject, delta:Point):void
		{
			var oldLocation:ObjectLocation = ObjectLocation.fromObject(obj);
			obj.x += delta.x;
			obj.y += delta.y;
			var action:ObjectDragAction = ObjectDragAction.fromObject(obj, oldLocation);
			ApplicationController.instance.authorController.currentActionTree.commit(action);
		}
		
		public function handleMainEdit(e:ActionTreeEvent):void
		{
			// an edit has been done to the document actionTree 
			// (edits to gadgets eventually commit to this actionTree)
            var eventEditMediator:EventPageEditingMediator = currentEventPageEditingMediator;
            var hasEdit:Boolean = false;
            if(eventEditMediator != null) {
                hasEdit = hasEdit || eventEditMediator.canUndo();
            }
            hasEdit ||= currentFlowEditingMediator.canUndo();
			Application.instance.document.upToDate = !hasEdit;   // EventPageEditingMediator also does this
		}
		
		public function log(str:String, models:Vector.<TaconiteModel>=null):void
		{
			// log an author action, including edits and runtime clicks and drags
			// to help tech-support re-create a bug situation
			var prefix:String = "";
			if (world) {
				prefix = Application.running ? "RUN:" : "EDIT:";  // aways prefix with run state
			}
			logOne(prefix + str);
			if (models) {
				logSelection(models);
			}
		}
		
		public function logSelection(models:Vector.<TaconiteModel>):void
		{
			if (models && models.length > 0) {
				for each (var m:TaconiteModel in models) {
					var obj:AbstractObject = m.value as AbstractObject;
					if (obj) {
						logOne("   " + obj.logStr);
					}
				}
			} else {
				logOne("   Empty selection");
			}
		}
		
		private function logOne(str:String):void
		{
			LogService.info(str);
			++_logEntryCount;
		}
		
		public function logStatistics():void
		{
			logOne("STATS:  " + sessionInfo);
		}
		
		public function get sessionInfo():String
		{
			var minutes:Number = int((getTimer() - _sessionStartTime) / (1000 * 60))+1;
			var user:String;
			var account:String;
			var server:String;
			var projID:String;
			var projName:String;

			var env:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			if (env) {
				user = env.username;
				account = env.accountName;
				server = env.apiURI;
			}
			var doc:Document = Application.instance.document;
			if (doc) {
				projID = doc.documentId;
				projName = doc.name;
			}
			return 'projName="'+projName+'"' + " projID="+projID + " user="+user + " server="+server + " minutes="+minutes + " logEntryCount="+_logEntryCount;
		}

        public function getLmsEnabled():Boolean {
            return world != null && world.lms != null;
        }

        public function setLmsEnabled(enabled:Boolean):void {
            if(enabled) {
                if(world.lms == null) {
                    ObjectFactory.createForName("LMS", world, "lms");
                    world.lms.complete = true;
                    world.lms.active = true;
                    var createAction:CreateObjectAction = CreateObjectAction.fromObject(world.lms);
                    ApplicationController.currentActionTree.commit(createAction);
                }
            } else {
                if(world.lms != null) {
                    var destroyAction:DestroyObjectAction = DestroyObjectAction.fromObject(world.lms);
                    ApplicationController.currentActionTree.commit(destroyAction);
                    destroyAction.perform();
                }
            }
        }

        private function get isPro():Boolean {
            return Application.instance.document.project.accountTypeId >= 3;
        }

    }
}
