package com.alleni.author.controller
{
import com.alleni.author.definition.AssetType;
import com.alleni.author.definition.Objects;
import com.alleni.author.definition.application.ObjectIcons;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.ui.Application;
import com.alleni.taconite.definition.ObjectDefinition;
import com.alleni.taconite.dev.Utilities;

import flash.display.DisplayObject;

import mx.collections.IList;

public class ObjectFactory
	{
		/**
		 * create with loading flag
		 * The client code MUST call obj.initializeModel() after setting properties on the object.
		 * And must clear the loading flag, which enables bindings to function.
		 * 
		 */
		public static function loadForName(shortClassName:String, parent:*, index:Object):AbstractObject
		{
			return createObject(shortClassName, parent, index, 0, 0, 0, 0, true);
		}
		
		
		/**
		 * Create an object of the specified class, as a child of the "parent". 
		 * @param shortClassName = name of class, which must be a subclass of AbstractObject.
		 * @param parent = World or AbstractContainer object where object should be added.
		 * @param x = local coordinate of left of object.
		 * @param y = local coord of top of object.
		 * @param width = object width.
		 * @param height
		 * @return 
		 * 
		 */
		public static function createForName(shortClassName:String,
									  parent:*,
									  index:Object=-1,
									  x:Number=0, y:Number=0,
									  width:Number=25, height:Number=25):AbstractObject
		{
			var obj:AbstractObject = createObject(shortClassName, parent, index, x, y, width, height, false);
			obj.initializeModel(false);  // loaded=false
			return obj;
		}

		
		/**
		 * create a new instance of an object using the class referenced in the ClassFactory object
		 * give the new object a unique "serial number" based on the root model (world) reference
		 * 
		 * NOTE: This same technique could be used to create objects by model, view, or controller
		 * reference. Anything that could filter the object definitions table can be used to get
		 * a supported model class to instantiate and, therefor, serve as a factory method.
		 * 
		 * @param shortClassName named reference to the object class to instantiate
		 * @param world the root model
		 * @param x the initial x position of the created object
		 * @param y the initial y position of the created object
		 * @return a reference to the new object instance
		 * 
		 */
        private static function createObject(shortClassName:String,
									parent:*, index:Object,
									x:Number, y:Number,
									width:Number, height:Number, loading:Boolean):AbstractObject
        {
        	var obj:AbstractObject;
        	
        	var descriptionPointer:ObjectDefinition = Objects.descriptionForShortClassname(shortClassName);
        	if (descriptionPointer && descriptionPointer.model != null) {
        		obj = new descriptionPointer.model();

				var assetTypeToContain:int = AssetType.getTypeForName(shortClassName);
				if (assetTypeToContain > -1) {
					var tmpIcon:DisplayObject = ObjectIcons.getByType(assetTypeToContain);
					width = width > tmpIcon.width?width:tmpIcon.width;
					height = height > tmpIcon.height?height:tmpIcon.height;
					tmpIcon = null;
				}
				obj.shortClassName = shortClassName;
				obj.title = descriptionPointer.defaultTitle;
          	  	setupObject(obj, parent, index, x, y, width, height, loading);
        		return obj;
        	} else {
        		throw new Error("Unrecognized object type: " + shortClassName);
        	}
        	return null;
       	}
		
		
		private static function setupObject(obj:AbstractObject, parent:Object, index:Object, x:Number, y:Number, width:Number, height:Number, loading:Boolean):void
		{
            obj.serial = AuthorController.nextSerial;
            obj.width = width;
            obj.height = height;
       		obj.loading = loading;
			
			obj.controller.initializeObject(obj.model);
			
            /* add this object instance to the parent model's children property.
            this will trigger the parent model view's createChildView method
            to create a view for our new object. */
            if(parent != null) {
                insertChild(parent,  index, obj);
                Utilities.assert(obj.parent);

                if(!loading) {
                    obj.initialValues.parent = parent.referenceJSON;
                    if(index is Number) {
                        parent.setChildrenZPositions(true);
                    }
                }
            }
			
			// set X/Y after the object is added, so the initialValues subsystem logs the change
			obj.x = x;
			obj.y = y;
			obj.rawRotation = 0;
			obj.visible = true;
			obj.showAll = Application.instance.showAll;
			
			if (obj.shortClassName == "PathObject")  // make sure animation path names are unique
				obj.title += " " + obj.serial.toString();
		}

        private static function insertChild(parent:Object, index:Object, obj:AbstractObject):void {
            var nextIndex:Array = null;
            var list:IList;
            if(index is Array) {
                if(index.length < 1) {
                    return;
                }
                if(index.length > 1) {
                    nextIndex = index.slice(1); // stow the remaining for subsequent iterations
                }
                index = index[0]; // grab the first one for this iteration
            }
            if(parent != null) {
                if(index is Number) {
                    list = (parent is AbstractContainer) ? AbstractContainer(parent).objects : parent as IList;
                    if(list) {
                        if(index < 0 || index >= list.length) {
                            if(nextIndex) {
                                return; // invalid index, no next parent
                            } else {
                                list.addItem(obj);
                            }
                        } else {
                            if(nextIndex) {
                                insertChild(list.getItemAt(int(index)), nextIndex, obj);
                            } else {
                                list.addItemAt(obj, int(index));
                            }
                        }
                    }
                } else if(parent is AbstractObject) {
                    if(parent.model.getAllValueProperties().indexOf(String(index)) >= 0) {
                        if(nextIndex) {
                            insertChild(parent[String(index)], nextIndex,  obj);
                        } else {
                            parent[String(index)] = obj;
                        }
                    } else if(parent.model.isValueListProperty(String(index))) {
                        list = parent[String(index)] as IList;
                        if (list) {
                            if(nextIndex) {
                                insertChild(list, nextIndex, obj);
                            } else {
                                list.addItem(obj);
                            }
                        }
                    }
                }
            }
        }
	}
}
