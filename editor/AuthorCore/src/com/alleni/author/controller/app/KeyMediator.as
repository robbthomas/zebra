package com.alleni.author.controller.app
{
import com.alleni.author.application.ApplicationUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.test.TestPanelController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.controller.ui.palettes.ToolboxController;
	import com.alleni.author.definition.CursorNames;
	import com.alleni.author.definition.UserKeyEvent;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.TextObject;
import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.WorldView;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.lang.TaconiteTimer;
import com.alleni.taconite.model.TaconiteModel;

import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;

public class KeyMediator
	{
		private const PRESSED_HELD_MIN_MILLIS:Number = 300;
		private const PRESSED_HELD_MAX_MILLIS:Number = 1000;
		
		private static var _lastKeyMillis:Number = 0;
		private static var _lastKeyPressMillis:Number = 0;
		private static var _lastKeyChar:String = "";
		private static var _pressedAndHeld:Boolean = false;
		
		
		private var _mutedKey:String= "";
		private var _hotkeysActive:Boolean = false;
		
		public function KeyMediator()
		{
			ApplicationController.addEventListener(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS, hideHotKeys);
			ApplicationController.addEventListener(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS, activateHotKeys);
		}
		
		public static function get heldKey():String
		{
			if (_pressedAndHeld) return _lastKeyChar.toLocaleLowerCase();
			else return "";
		}
		
		//TURN ON AND OFF HOT KEYS
		
		private function activateHotKeys(event:ApplicationEvent = null):void
		{
			// guard against adding key listener twice
            if(Application.instance.customerType != null && Application.instance.document != null){
                if(!(Application.instance.customerType == "CREATOR" && (Application.instance.document.project.accountType == "PRO" || Application.instance.document.project.accountType == "ENTERPRISE"))){
                    if (!_hotkeysActive) {
                        var stage:Stage = Application.instance.window.displayObject.stage;
                        if (stage) {
                            stage.addEventListener(Event.CUT, handleCut);
                            stage.addEventListener(Event.COPY, handleCopy);
                            stage.addEventListener(Event.PASTE, handlePaste);
                            stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown, false, 99);
                            stage.addEventListener(KeyboardEvent.KEY_UP, handleKeyUp, false, 99);
                            stage.focus = stage;
                            _hotkeysActive = true;
                        }
                    }
                }
            }
		}
		
		private function hideHotKeys(event:ApplicationEvent = null):void
		{
			var stage:Stage = Application.instance.window.displayObject.stage;
			if (stage) {
				stage.removeEventListener(Event.CUT, handleCut);
				stage.removeEventListener(Event.COPY, handleCopy);
				stage.removeEventListener(Event.PASTE, handlePaste);
				stage.removeEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
				stage.removeEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
			}
			_hotkeysActive = false;
		}
		
		public function muteHotKey(mutedKey:String):void{
			_mutedKey = mutedKey;
		}
		
		private function clearMutedKey():void{
			_mutedKey = "";
		}
		
		private function handleCut(event:Event):void
		{
			ApplicationController.cut(event);
		}
		
		private function handleCopy(event:Event):void
		{
			ApplicationController.copy(event);
		}
		
		private function handlePaste(event:Event):void
		{
			ApplicationController.paste(event);
		}
		
		//HANDLE KEY MODIFERS
		private function handleKeyDown(event:KeyboardEvent):void
		{
			if(ApplicationUI.instance.mouseIsDown == true){
                return;
            }
            event.stopImmediatePropagation();
			event.stopPropagation();
			event.preventDefault();
			
			ApplicationController.instance.dispatchEvent(new UserKeyEvent(String.fromCharCode(event.charCode),true, false,event.keyCode,event.ctrlKey,event.altKey,event.shiftKey));

            if(ApplicationController.instance.invokedAsEditor){
    //			trace("key="+event.keyCode, "char="+String.fromCharCode(event.charCode), "charCode="+event.charCode);
                if (event.ctrlKey && event.altKey && event.shiftKey) {
                    handleAltControlShift(event);
                } else if (event.ctrlKey && event.altKey) {
                    handleAltControl(event);
                } else if (event.ctrlKey && event.shiftKey) {
                    handleControlShift(event);
                } else if (event.altKey && event.shiftKey) {
                    handleAltShift(event);
                } else if (event.ctrlKey) {
                    handleControlOnly(event);
                } else if (event.altKey) {
                    handleAltOnly(event);
                } else if (event.shiftKey) {
                    handleShiftOnly(event);
                } else {
                    handlePlainKey(event);
                }
            }
		}
		
		//HANDLE KEY UP
		private function handleKeyUp(event:KeyboardEvent):void
		{
 			event.stopImmediatePropagation();
			event.stopPropagation();
			event.preventDefault();
			
			_pressedAndHeld = false;
		
			//ApplicationController.instance.dispatchEvent(new UserKeyEvent(String.fromCharCode(event.charCode), false, false,event.keyCode,event.ctrlKey,event.altKey,event.shiftKey));
			
			switch(event.keyCode){
				case 16:
					if(Application.instance.currentTool == ToolboxController.ZOOM){
						ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.CHANGE_CURSOR_TO, {name:CursorNames.TOOL_ZOOMIN, type:CursorController.CURSOR_TYPE_TOOL}));
					}
					break;
			}
		}

        private function get editingKeysAllowed():Boolean
        {
            if (TestPanelController.testRunning) {
                return true;
            }
            return !Application.uiRunning || (Application.uiRunning && !Application.runningLocked);
        }
		
		private function get debugKeysEnabled():Boolean
		{
			return TaconiteFactory.getEnvironmentImplementation().isAdmin;
		}
	
		
		//HANDLE KEY VALUES 
		private function handlePlainKey(event:KeyboardEvent):void
		{
			var ch:String = (String.fromCharCode(event.charCode)).toLowerCase();
			
			/*
			get current time in milliseconds and compare against last key down event to find whether we've got a key
			pressed and held or not. Standard OS configuration will trigger Key Down event repeatedly at some
			arbitrary delay set in system preferences, so we can only use our own internal time to decide whether
			the key is held. 
			we decide the key is held down if the current Key Down is for the same key character as the last, and this
			same key has been held down for more than PRESSED_HELD_MIN_MILLIS milliseconds. We also ensure they don't
			just happen to be pressing the same key again as was last held down by ensuring the delay since 
			_lastKeyMillis was set is less than PRESSED_HELD_MAX_MILLIS.
			*/
			var now:Number = TaconiteTimer.instance.milliseconds;
			_pressedAndHeld = (ch == _lastKeyChar) && (now - _lastKeyPressMillis) > PRESSED_HELD_MIN_MILLIS 
					&& (now - _lastKeyMillis) <= PRESSED_HELD_MAX_MILLIS;
			
			if (ch != _lastKeyChar || (now - _lastKeyMillis) > PRESSED_HELD_MAX_MILLIS) {
				_lastKeyPressMillis = now;
			}
			_lastKeyMillis = now;
			_lastKeyChar = ch;
			
			/*
			when running, user keys takes precedence over hot keys
			a wire anchor will temporarily mute the hot key that matches the user key
			*/
			
			if(ch == _mutedKey){
				clearMutedKey();
				return;
			}
			else{
				clearMutedKey();
			}

            if(editingKeysAllowed){
                switch (ch)
                {
                    case "[":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.ALIGN_EDGES_LEFT));
                        break;
                    case "]":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.ALIGN_EDGES_RIGHT));
                        break;
                    case "\\":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.ALIGN_CENTERS_HORIZONTAL));
                        break;
                    case "'":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.DISTRIBUTE_HORIZONTAL));
                        break;
                    case " ":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.DESELECT));
                        break;
                    case "-":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.ZOOM_OUT_NO_ADJUST));
                        break
                    case "=":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.ZOOM_IN_NO_ADJUST));
                        break;
                    case "a":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.SELECT_ALL));
                        break;
                    case "b":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.SEND_BACK));
                        break;
                    case "c":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.COPY));
                        break;
                    case "d":
                        if(Application.instance.document.project.accountTypeId > 2 && ApplicationUI.instance.mouseIsDown == false){
                            ApplicationController.instance.authorController.toggleFlowVisible(false); // tween=false
                        }
                        break;
                    case "e":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.EDIT_SELECTED_OBJECT, "keyMediator"));
                        break;
                    case "f":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.BRING_FORWARD));
                        break;
                    case "g":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.GROUP_ITEMS));
                        break;
                    case "i":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.IMPORT_ASSET));
                        break;
                    case "k":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.MAKE_TOGGLEBUTTON_GROUP));
                        break;
                    case "m":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.MAKE_OBJECT_OPEN));
                        break;
                    case "n":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.CREATE_NEW_PROJECT));
                        break;
                    case "o":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.OPEN_PROJECT));
                        break;
                    case "q":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.REQUEST_QUIT_APPLICATION));
                        break;
                    case "s":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SAVE_PROJECT));
                        break;
                    case "t":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SHOW_ALL_TOGGLED));
                        break;
                    case "u":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.UNMAKE_OBJECT));
                        break;
                    case "v":
                        // this paste is not from a Flash PASTE event so it reads from a private clipboard.
                        // To paste from another environ, use cmd-v to invoke system paste
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.LOCAL_PASTE, {shift:false}));
                        break;
                    case "w":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MORE_WIRES));
                        break;
                    case "x":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.CUT));
                        break;
                    case "y":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.REDO));
                        break;
                    case "z":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.UNDO));
                        break;
                    case "1":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.ACTUAL_SIZE));
                        break;
                    case "2":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.FIT_IN_WINDOW));
                        break;
                    case String.fromCharCode(0x08):  // Delete/backspace
                    case String.fromCharCode(0x7F):
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.DELETE_SELECTED));
                        break;
                }

                switch (event.keyCode) {
                    case Keyboard.ENTER:
                        ApplicationController.instance.authorController.handleEnterKey(false);
                        break;
                    case Keyboard.ESCAPE:
                        ApplicationController.instance.authorController.handleEscKey();
                        break;
                    case Keyboard.SPACE:
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.DESELECT));
                        break;
                    case Keyboard.LEFT:
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.NUDGE_LEFT));
                        break;
                    case Keyboard.RIGHT:
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.NUDGE_RIGHT));
                        break;
                    case Keyboard.UP:
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.NUDGE_UP));
                        break;
                    case Keyboard.DOWN:
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.NUDGE_DOWN));
                        break;
                }
            }
            //Hot Keys for ALL run modes
            switch (ch){
                case "j":
                    ApplicationController.instance.authorController.toggleRunEdit();
                    break;
                case "l":
                    ApplicationController.instance.authorController.toggleRunLock();
                    break;
                case "r":
                    ApplicationController.instance.authorController.runCommand();
                    break;
            }
		}
		
		private function handleControlOnly(event:KeyboardEvent):void
		{
            switch (event.keyCode) {
                case Keyboard.LEFT:
                    AuthorController.instance.gotoNextPage(-1);
                    break;
                case Keyboard.RIGHT:
                    AuthorController.instance.gotoNextPage(+1);
                    break;
            }
		}
		
		private function handleAltOnly(event:KeyboardEvent):void
		{
		}
		
		private function handleShiftOnly(event:KeyboardEvent):void
		{
			var ch:String = String.fromCharCode(event.charCode).toLowerCase();
			
			if(ch == _mutedKey){
				clearMutedKey();
				return;
			}
			else{
				clearMutedKey();
			}
			if(editingKeysAllowed){
                switch (ch)
                {
                    case "a":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.SELECT_ALL_WITH_LOCKED));
                        break;
                    case "b":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.SEND_TO_BACK));
                        break;
                    case "f":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.BRING_TO_FRONT));
                        break;
                    case "g":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.UNGROUP_ITEMS));
                        break;
                    case "k":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.UNMAKE_TOGGLEBUTTON_GROUP));
                        break;
                    case "m":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.MAKE_OBJECT_CLOSE));
                        break;
                    case "s":
                        event.stopImmediatePropagation();
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SAVE_AS_PROJECT));
                        break;
                    case "v":
                        // this paste is not from a Flash PASTE event so it reads from a private clipboard.
                        // To paste from another environ, use cmd-v to invoke system paste
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.PASTE, {shift:true}));
                        break;
                    case "w":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.FEWER_WIRES));
                        return;
                    case "{":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.ALIGN_EDGES_TOP));
                        return;
                    case "}":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.ALIGN_EDGES_BOTTOM));
                        return;
                    case "|":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.ALIGN_CENTERS_VERTICAL));
                        return;
                    case '"':
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.DISTRIBUTE_VERTICAL));
                        break;
                    case "@":
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.MAKE_ARENA));
                        break;
                    case "#":
                        if (debugKeysEnabled) {
                            AuthorController.instance.eventFlow.validateForKeypress();
                        }
                        break;
                    default:
                        null;
                }
                switch (event.keyCode) {
                    case Keyboard.LEFT:
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.NUDGE_TEN_LEFT));
                        return;
                    case Keyboard.RIGHT:
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.NUDGE_TEN_RIGHT));
                        return;
                    case Keyboard.UP:
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.NUDGE_TEN_UP));
                        return;
                    case Keyboard.DOWN:
                        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.NUDGE_TEN_DOWN));
                        return;
                    default:
                        null;
                }
                if(Application.instance.currentTool == ToolboxController.ZOOM){
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.CHANGE_CURSOR_TO, {name:CursorNames.TOOL_ZOOMOUT, type:CursorController.CURSOR_TYPE_TOOL}));
                }
            }
            switch (ch){
                case "r":
                    ApplicationController.instance.authorController.editResetCommand();
                    break;
            }
		}
		
		private function handleAltControl(event:KeyboardEvent):void
		{
			var ch:String = String.fromCharCode(event.charCode);
			if (debugKeysEnabled) {
				switch (ch)
				{
					case "2":
						ApplicationController.currentActionTree.dump();
						break;
                    case "3":
                        pasteGadgetFromSelectedText();
//                        GadgetController.instance.loadAndPlace(_item as Project, x, y, null, withContainer);
                        break;
					case "4":
						trace("");
						trace("server="+TaconiteFactory.getEnvironmentImplementation().serverName);
						trace("user="+TaconiteFactory.getEnvironmentImplementation().username);
						trace("project="+Application.instance.document.name);
						if (Application.instance.document.project)
							trace("id="+Application.instance.document.project.projectId);
						break;
					case "7":
						WorldContainer(Application.instance.viewContext).worldView.dumpViews();
						Utilities.dumpDisplayObjectContainer(Application.instance.viewContext.parent,"ApplicationUI");
						break;
				}
			}
		}
		
		private function handleControlShift(event:KeyboardEvent):void
		{
		}
		
		private function handleAltShift(event:KeyboardEvent):void
		{
		}
		
		private function handleAltControlShift(event:KeyboardEvent):void
		{
		}

		
	//add a function here and call isKeyMatched -- iterate through the anchorkeypresses split on |
		
		public function isKeyMatched(anchorKeyPress:String, userKeyEvent:UserKeyEvent):Boolean{
			
			var keyPressTokens:Array = anchorKeyPress.split("|");
			
			for each (var token:String in keyPressTokens){
			
				if(processKey(token.replace(/[ \t]+/g, ""), userKeyEvent)){
					return true;
				}
			}
			
			return false;
		}
		
		private function processKey(anchorKeyPress:String, userKeyEvent:UserKeyEvent):Boolean{
			
			
			switch (anchorKeyPress.length){
			
			//no keypress
			case 0:  					
				_mutedKey = "";
				return false;
			//if we only have a single letter, does it match the anchor key.  If not, return.
			case 1:    					
			if(anchorKeyPress != userKeyEvent.key){
				return false;
			}
			_mutedKey = anchorKeyPress.toLowerCase();
			break;
			
			//combination of modifiers and/or keywords with keys
			default:
			var tokens:Array = anchorKeyPress.split(/[+-]/);
			
			for each (var token:String in tokens){
				
				if(token.length == 1){
					 if(processSingleKey(token,userKeyEvent,false) == false){
						return false;
					 }
					 
					 _mutedKey = token.toLowerCase();
				}
				else{
					
					if(isModifier(token)){
						if(processKeyModifiers(token,userKeyEvent) == false){
							return false;
						}
					}
					else{
						if(processKeyConstants(token, userKeyEvent) == false){
							return false;
						}
					}
				}
			}//for
		}//switch
			
				
			return true;
		}
	
		private function processKeyConstants(keyword:String, userKeyEvent:UserKeyEvent):Boolean{
			
			
			switch(keyword.toUpperCase()){
				
				
				case "BACKSPACE":
					
					if(userKeyEvent.keyCode == Keyboard.BACKSPACE)
						return true;
					
					break;
				
				case "BACKSLASH":
					
					if(userKeyEvent.keyCode == Keyboard.BACKSLASH)
						return true;
					
					break;
				
				case "CAPSLOCK":
					
					if(userKeyEvent.keyCode == Keyboard.CAPS_LOCK)
						return true;
					
					break;
				
				case "DELETE":
					
					if(userKeyEvent.keyCode == Keyboard.DELETE)
						return true;
					
					break;
				
				case "DOWN":
					if(userKeyEvent.keyCode == Keyboard.DOWN)
						return true;
					
					break;
				
				case "END":
					
					if(userKeyEvent.keyCode == Keyboard.END)
						return true;
					
					break;
				
				
				case "ENTER":
					
					if(userKeyEvent.keyCode == Keyboard.ENTER)
						return true;
					
					break;
				
				case "ESCAPE":
					
					if(userKeyEvent.keyCode == Keyboard.ESCAPE)
						return true;
					
					break;
				
				case "HOME":
					
					if(userKeyEvent.keyCode == Keyboard.HOME)
						return true;
					
					break;
				
				
				case "LEFT":
					
					if(userKeyEvent.keyCode == Keyboard.LEFT)
						return true;
					
					break;
				
				case "PAGEDOWN":
					
					if(userKeyEvent.keyCode == Keyboard.PAGE_DOWN)
						return true;
					
					break;
				
				case "PAGEUP":
					
					if(userKeyEvent.keyCode == Keyboard.PAGE_UP)
						return true;
					
					break;
				
				
				case "RIGHT":
					if(userKeyEvent.keyCode == Keyboard.RIGHT)
						return true;
					
					break;
				
				case "SPACE":
					
					if(userKeyEvent.keyCode == Keyboard.SPACE){
						var _worldView:WorldView = WorldContainer(Application.instance.viewContext).worldView;
						_worldView.stage.focus = _worldView.stage; 
						return true;
					}
					
					break;
				
				case "TAB":
					
					if(userKeyEvent.keyCode == Keyboard.TAB)
						return true;
					
					break;
				
				
				case "UP":
					if(userKeyEvent.keyCode == Keyboard.UP)
						return true;
					
					break;
				
				case "F1":
					
					if(userKeyEvent.keyCode == Keyboard.F1)
						return true;
					
					break;
				
				case "F2":
					
					if(userKeyEvent.keyCode == Keyboard.F2)
						return true;
					
					break;
				
				case "F3":
					
					if(userKeyEvent.keyCode == Keyboard.F3)
						return true;
					
					break;
				
				case "F4":
					
					if(userKeyEvent.keyCode == Keyboard.F4)
						return true;
					
					break;
				
				case "F5":
					
					if(userKeyEvent.keyCode == Keyboard.F5)
						return true;
					
					break;
				
				case "F6":
					
					if(userKeyEvent.keyCode == Keyboard.F6)
						return true;
					
					break;
				
				case "F7":
					
					if(userKeyEvent.keyCode == Keyboard.F7)
						return true;
					
					break;
				
				case "F8":
					
					if(userKeyEvent.keyCode == Keyboard.F8)
						return true;
					
					break;
				
				case "F9":
					
					if(userKeyEvent.keyCode == Keyboard.F9)
						return true;
					
					break;
				
				case "F10":
					
					if(userKeyEvent.keyCode == Keyboard.F10)
						return true;
					
					break;
				
				case "F11":
					
					if(userKeyEvent.keyCode == Keyboard.F11)
						return true;
					
					break;
				
				case "F12":
					
					if(userKeyEvent.keyCode == Keyboard.F12)
						return true;
					
					break;
				
			}
			
			return false;
		}
		
		private function processKeyModifiers(keyword:String, userKeyEvent:UserKeyEvent):Boolean{
			
			trace("userKeyEvent->control ", userKeyEvent.ctrlKey);
			trace("userKeyEvent->shift ", userKeyEvent.shiftKey);
				
			switch(keyword.toUpperCase()){
				
				case "CONTROL":
					
					if(userKeyEvent.ctrlKey)
						return true;
					
					break;
				
				case "CTRL":
					
					if(userKeyEvent.ctrlKey)
						return true;
					
					break;
				
				case "SHIFT":
					
					if(userKeyEvent.shiftKey)
						return true;
					
					break;
				
				
			}
			
			return false;
		}
		
		private function processSingleKey(key:String, userKeyEvent:UserKeyEvent, caseSensitive:Boolean = false):Boolean{
			
			if(caseSensitive){
				if(key != userKeyEvent.key)
					return false;
			}
			else{
				if(key.toUpperCase().charCodeAt(0) != userKeyEvent.keyCode)
					return false;
			}
			
			return true;
			
		}
		
		private function isModifier(token:String):Boolean{
			
			var modifiers:Array = new Array("CONTROL", "CTRL", "SHIFT");
			return modifiers.indexOf(token.toUpperCase()) > -1?true:false;
		}

        private function pasteGadgetFromSelectedText():void
        {
            var selection:Vector.<TaconiteModel> = Application.instance.document.selection.selectedModels;
            if (selection.length == 1) {
                var text:TextObject = selection[0].value as TextObject;
                if (text) {
                    var id:String = text.plainText;
                    trace("pasteGadgetFromSelectedText id="+id, "length="+id.length);
                    if (id.length == 32) {
                        var composite:Composite = ApplicationController.instance.authorController.addObjectForName("Composite") as Composite;
                        GadgetController.instance.requestForID(id, -1, false, composite);
                        trace("  composite="+composite);
                        return;
                    }
                }
            }
            trace("  ** Must have a single text obj selected, having exactly 32 chars of text (the gadgetID) **");
        }

		
	}
}
