package com.alleni.author.controller.app
{
    import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.application.dialogs.LoadProjByIdDialog;
import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.Objects;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.document.Document;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.AbstractContainer;
	import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.model.ui.Wiring;
	import com.alleni.author.persistence.GadgetEncoder;
	import com.alleni.taconite.definition.ObjectDefinition;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.factory.ISystemClipboard;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.persistence.json.JSON;
	import com.alleni.taconite.persistence.json.JSONEncoder;
    import com.alleni.taconite.service.LogService;

	import flash.desktop.ClipboardFormats;
import flash.events.MouseEvent;
import flash.utils.Dictionary;
	
	import mx.collections.IList;

	public class SupportUtilities
	{
		
		private static var _objectCounter:int;

        private static const INDENT:String = "  ";

        public static function dumpObjectsToLog():void
        {
            log(dumpObjects());
        }

		public static function dumpObjects():String
		{
			_objectCounter = 0;
			var buffer:String = "\nDump document objects: proj=\""+document.name+"\"\n";
			buffer += dumpObject(world, INDENT) + "\n";
            buffer += "total="+_objectCounter+" objects\n";
			return buffer;
		}

        private static function dumpObject(obj:AbstractObject, indent:String = ""):String
        {
            var buffer:String = indent + formatObj(obj) + "\n";
            ++_objectCounter;
            indent += INDENT;

            var cont:AbstractContainer = obj as AbstractContainer;
            if (cont) {
                buffer += indent + "Child objects:\n";
                buffer += dumpObjList(cont.objects, indent+INDENT);
            }

            for each(var prop:String in obj.model.getAllValueProperties()) {   // eventFlow, LMS, project
                var propObj:AbstractObject = obj[prop] as AbstractObject;
                if (propObj) {
                    buffer += indent + "Property \"" + prop + "\":\n";
                    buffer += dumpObject(propObj, indent+INDENT);
                }
            }

            var lists:Vector.<String> = obj.model.getAllValueListProperties();  // masterPages, pages
            for each (var name:String in lists) {
                var list:IList = obj[name] as IList;
                buffer += indent + "List \"" + name + "\":\n";
                buffer += dumpObjList(list, indent+INDENT);
            }
            return buffer;
        }
		
		private static function dumpObjList( objects:IList, indent:String="" ):String
		{
			var buffer:String = "";
			for each (var ao:AbstractObject in objects) {
				buffer += dumpObject(ao, indent);
			}
			return buffer;
		}

        private static function formatObj(obj:AbstractObject):String
        {
            var buffer:String = obj.toString();
            if (!hasUniqueUID(obj)) {
                buffer += " " + obj.uid;
            }
            return buffer;
        }

        private static function hasUniqueUID(obj:AbstractObject):Boolean
        {
            var all:Vector.<AbstractObject> = world.allObjects();
            for each (var ao:AbstractObject in all) {
                if (ao != obj && ao.uid == obj.uid) {
                    return false;
                }
            }
            return true;
        }
		
		public static function dumpModels( title:String="" ):String
		{
			var buffer:String = "";
			buffer += Utilities.dumpModelList(world.model, "ObjectModels");
			buffer += Utilities.dumpModelList(Document(document).wiringRoot, "WiringModels");
			buffer += Utilities.dumpModelList(Document(document).assetsRoot, "AssetModels");
			return buffer;
		}
		
		
		public static function dumpSelection(title:String=""):String
		{
			var buffer:String = "";
			if (document.selection)
				buffer += document.selection.dumpSelection(title);
			else
				buffer += trace("Document selection=null");
			log(buffer);
			return buffer;
		}
		
		public static function copyProjectJson():void
		{
			var json:Object;
			if (ApplicationController.instance.authorController.currentVelumMediator) {
				log("encoding Gadget to clipboard");
				var smart:SmartObject = ApplicationController.instance.authorController.currentVelumMediator.editingObject as SmartObject;
				json = new GadgetEncoder().encode(new Project(), smart);
			} else {
				log("encoding Project to clipboard");
				json = GadgetEncoder.getState(ApplicationController.instance.authorController.world, GadgetEncoder.SERIALIZED_LOCATION_EXISTING);
			}
			var text:String = new JSONEncoder(json).getString();
			copyTextToClipboard(text);
			log("  json string len="+text.length);
		}
		
		private static function copyTextToClipboard(text:String):void
		{
			var clipboard:ISystemClipboard = TaconiteFactory.getClipboardImplementation();
			clipboard.clear();
			clipboard.setData(ClipboardFormats.TEXT_FORMAT, text);
		}
		
		public static function copyProjectXML():void
		{
			var xml:XML = buildObjectXML(world);
			var text:String = xml.toXMLString();
			copyTextToClipboard(text);
			log("  XML text len="+text.length);
		}
		
        public static function copyProjectDump():void
        {
            var text:String = dumpObjects();
            copyTextToClipboard(text);
            log("  Project dump: text len="+text.length);
        }

        public static function copyLog():void
        {
            var text:String = LogService.dump;
            copyTextToClipboard(text);
            log("  Copy log: text len="+text.length);
        }

		private static function buildContainerXML(container:AbstractContainer):XMLList
		{
			trace("buildContainerXML: contain="+container);
			var list:XMLList = new XMLList();
			for each (var obj:AbstractObject in container.objects) {
				var xml:XML = buildObjectXML(obj);
				
				list[list.length()] = xml;
			}
			return list;
		}
		
		public static function buildObjectXML(obj:AbstractObject):XML
		{
			var label:String;
			if (obj is ArenaPage) {
				label = "Page " + (obj.model.parent.getChildIndex(obj.model)+1);
			} else {
				var desc:ObjectDefinition = Objects.descriptionForShortClassname(obj.shortClassName);
				label = (desc ? desc.defaultTitle : obj.shortClassName) + (obj.titleChangedByAuthor ? " " + '"'+obj.title+'"' : "") + " " + obj.uid;
			}
			
			var xml:XML = <obj/>;
			xml.@label =  label;
			xml.@id = obj.uid;

            if (obj is ArenaPage == false && obj is World == false && obj is EventFlow == false) {
                var ribbons:XMLList = buildRibbonsXML(obj);
                xml.appendChild(ribbons);
            }

            if (obj is AbstractContainer) {
                var children:XMLList = buildContainerXML(obj as AbstractContainer);
                xml.appendChild(children);
            }

            var child:AbstractObject;
            var prop:XML;
            for each (var valueListProp:String in obj.model.getAllValueListProperties()) {
                prop = new XML("<valueListProp label=\""+valueListProp+"\"/>");
                var propList:XMLList = new XMLList();
                var valueList:IList = obj[valueListProp];
                if(valueList == null) {
                    continue;
                }
                for each(child in valueList) {
                    propList[propList.length()] = buildObjectXML(child);
                }
                prop.appendChild(propList);
                xml.appendChild(prop);
            }
            for each(var valueProp:String in obj.model.getAllValueProperties()) {
                prop = new XML("<valueProp label=\"" + valueProp + "\"/>");
                child = obj[valueProp] as AbstractObject;
                if(child == null) {
                    continue;
                }
                prop.appendChild(buildObjectXML(child));
                xml.appendChild(prop);
            }
			return xml;
		}
		
		private static function buildRibbonsXML(obj:AbstractObject):XMLList
		{
			var ribbons:XMLList = new XMLList(<ribbons label="ribbons"/>);
			
			var ignore:Array = ["zIndex", "contentWidth", "contentHeight", "objCount"];
			var defaults:ValueDetector = ValueDetector.instance;
			var dict:Dictionary = defaults.getDefaultValues(obj.shortClassName);
			
			for each (var thing:* in obj.anchors) {
				var anchor:WireAnchor = thing as WireAnchor;
				if (anchor) {
					var property:PropertyDescription = anchor.modifierDescription as PropertyDescription;
					if (property && !property.readOnly && ignore.indexOf(property.key) < 0) {
						var prop:String = property.key;
						if (prop in obj) {
							var value:* = obj[prop];
							if (defaults.differsFromDefault(value, prop, dict)) {
								var valStr:String = com.alleni.taconite.persistence.json.JSON.encode(value);
								var ribbon:XML = <ribbon/>;
								ribbon.@label = prop + " = " + valStr;
								ribbon.@value = valStr;
								ribbons.appendChild(ribbon);
							}
						}
					}
				}
			}
			return ribbons;
		}
		
		
        public static function teleport():void
        {
            ProjectController.instance.makeNewUntitledProject();  // abandon the current project
            new LoginMediator().handleLoginEvents();
        }

        public static function loadProjectByID():void
        {
            var dlg:LoadProjByIdDialog = ApplicationController.instance.createPopUp(LoadProjByIdDialog, false) as LoadProjByIdDialog;
            dlg.usernameInput.text = TaconiteFactory.getEnvironmentImplementation().username;
            dlg.projectIDInput.text = Application.instance.document.documentId;
            dlg.okButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void{
                ApplicationController.instance.removePopup(dlg);
                var id:String = dlg.projectIDInput.text;
                if (Utilities.UUID_REGEX.test(id)) {
                    Application.instance.projectFromOtherUser = true;  // assume its from other user, so Save will be Save-as
                    ProjectLoadingSequencer.instance.requestLoad(id);
                } else {
                    ApplicationController.instance.userMessage("Project ID must be 32 characters, all numbers & letters.");
                }
            });
        }

		public static function copySelectedObjectUID():void
		{
			var models:Vector.<TaconiteModel> = ApplicationController.instance.authorController.document.selection.selectedModels;
			if (models.length == 1) {
				var obj:AbstractObject = models[0].value as AbstractObject;
				copyTextToClipboard(obj.uid);
			}
		}
		
		private static function log(str:String):void
		{
			ApplicationController.instance.authorController.log(str);
		}
		
		private static function get document():Document
		{
			return Application.instance.document;
		}
		
		private static function get world():World
		{
			return document.root.value as World;
		}
		
		public static function copyProjectDOT():void
		{
			_dotText = "";
			
			dotLine("digraph {");
			dotLine("node [label=\"\\N\", shape=record, width=\".1\", height=\".1\"];");
			dotLine("graph [nodesep=\".25\", rankdir=LR];");
			printObjects(document.root.value as World);
			printWires(document.wiringRoot.value as Wiring);
			dotLine("}");
			
			copyTextToClipboard(_dotText);
			log("  DOT string len="+_dotText.length);
			_dotText = null;
		}
		
		private static var _dotText:String;
		
		private static function dotLine(str:String):void
		{
			_dotText += str + "\n";
		}
		
		private static function printObjects(root:AbstractContainer):void {
			dotLine("subgraph \"cluster_" + root.uid + "\" { label=\"" + root.title + "\"");
			printObject(root);
			for each(var child:Object in root.objects) {
				if(child is AbstractContainer) {
					printObjects(child as AbstractContainer);
				} else {
					printObject(child as AbstractObject);
				}
			}
			dotLine("}")
		}
		
		private static function printObject(object:AbstractObject):void {
			var node:String = "\"" + object.uid + "\" [label=\" <self> " + object.title + " | ";
			var nodes:Array = [];
			for each(var child:Object in object.anchors) {
				if(child is IList) {
					for each(var child2:Object in IList(child)) {
						nodes = nodes.concat(printRibbon(child2 as WireAnchor));
					}
				} else if(child is WireAnchor) {
					nodes = nodes.concat(printRibbon(child as WireAnchor));
				}
			}
			node += nodes.join(" | ") + "\"]";
			dotLine(node);
		}
		
		private static function printRibbon(anchor:WireAnchor):Array {
			if(anchor.wired == false) {
				return [];
			}
			var result:Array = ["<" + anchor.path + "> " + anchor.modifierDescription.label + anchorIndex(anchor)];
			for each(var child:WireAnchor in anchor.childAnchors) {
				result = result.concat(printRibbon(child));
			}
			return result;
		}
		
		private static function anchorIndex(anchor:WireAnchor):String {
			return (anchor.modifierDescription.indexed?" [" + anchor.hostPropertyIndex + "]":"");
		}
		
		private static function printWires(wiring:Wiring):void {
			for each(var wire:Wire in wiring.children) {
				printWire(wire);
			}
		}
		
		private static function printWire(wire:Wire):void {
			dotLine(printName(wire.masterAnchor) + " -> " + printName(wire.slaveAnchor)  +
				((wire.masterAnchor.modifierDescription is PropertyDescription)?" [dir=both color=\"blue:blue\"]":" [dir=forward color=\"red:orange\"]"));
		}
		
		private static function printName(anchor:WireAnchor):String {
			return "\"" + anchor.hostObject.uid + "\":\"" + anchor.path + "\"";
		}

	}
}