package com.alleni.author.controller.app
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.IPage;
import com.alleni.author.Navigation.Pager;
import com.alleni.author.Navigation.PagerController;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.Navigation.SmartObjectController;
import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.AuthorController;
	import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.controller.ui.ToggleButtonGroupController;
	import com.alleni.author.controller.ui.WireController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.definition.DragInfo;
	import com.alleni.author.definition.action.CreateObjectAction;
import com.alleni.author.definition.action.LibraryAddAction;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.document.Document;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.AbstractContainer;
	import com.alleni.author.model.objects.Arena;
	import com.alleni.author.model.objects.ArenaPage;
	import com.alleni.author.model.objects.ToggleButton;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.persistence.GadgetDecoder;
	import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.factory.ISystemClipboard;
import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	
	public class ClipboardController extends EventDispatcher
	{
		private var _application:Application;
		private static var _instance:ClipboardController;
		private var _wireController:WireController;
		private var _pasteCount:int = 0;
		private var _objectCut:Boolean = false;

		
		public function ClipboardController()
		{
			Utilities.assert(_instance == null);
			
			_application = Application.instance;
			_wireController = ApplicationController.instance.wireController;
			_instance = this;
		}
		
		public static function get instance():ClipboardController
		{
			return _instance;
		}
		
		
		/////  FROM TACONITE CONTROLLER
		
		private function get _document():Document
		{
			return Application.instance.document;
		}
		
		private function get _controller():AuthorController
		{
			return ApplicationController.instance.authorController;
		}
		
		private function get world():World
		{
			return Application.instance.document.root.value as World;
		}
		
		///////////// high-level functions operate on clipboard and log Undo operations
		
		
		public function duplicateSelection(setComplete:Boolean, parent:AbstractObject):void
		{
			trace("DUPLICATE");
			_objectCut = false;
			var buffer:Object = copyToBuffer();
			pasteFromBuffer(buffer, setComplete, parent, false, true);  // logAction=false, since ObjectDragMediator does it
		}
		
		public function cutClipboard(event:Event):void
		{
			trace("CUT");
			_objectCut = true;
            var objCount:String = currentSelection.length.toString();
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:605, inserts:[objCount]}));
			var json:Object = copyToBuffer();
			var format:String = TaconiteFactory.getClipboardImplementation().OBJECTS_FORMAT;
			_controller.copyFormatToClipboard(format, json, event);
			_controller.currentEditingMediator.deleteEditing();
		}
		
		public function copyClipboard(event:Event):void
		{
			trace("COPY");
			_objectCut = false;
            var objCount:String = currentSelection.length.toString();
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:604, inserts:[objCount]}));
			var json:Object = copyToBuffer();
			var format:String = TaconiteFactory.getClipboardImplementation().OBJECTS_FORMAT;
			_controller.copyFormatToClipboard(format, json, event);
		}

		/**
		 * This is the highest-level paste command.
		 * The clipboard may be Zebra objects, or text, bitmap, etc
		 * If its zebra objects, we get a call to pasteClipboardObjects() 
		 * 
		 */
		public function pasteClipboard(e:Event):void
		{
			// provide a default location for content such as text & bitmap that don't have location
			// (zebra objects will paste at their original location)
			trace("pasteClipboard e="+e);
            if(e.type == NotificationNamesEditor.PASTE){
			    _controller.pasteFromSystemClipboard(10, 10, e);
            }else{
                var clipboard:ISystemClipboard = TaconiteFactory.getClipboardImplementation(e);
                pasteClipboardObjects(clipboard.getData(clipboard.OBJECTS_FORMAT), 10, 10, e);
            }
		}
		
		/**
		 * Paste zebra objects that were copied to internal clipboard.
		 * (does not use the contents of the system clipboard) 
		 * 
		 */
		public function pasteClipboardObjects(buffer:Object, localX:Number=0, localY:Number=0, event:Event = null):void
		{
			if (buffer) {
                // msg 603 - Insert1 = number of objects
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:603}));
				EditorUI.instance.stage.addEventListener(Event.ENTER_FRAME, function(e:Event):void{
					EditorUI.instance.stage.removeEventListener(Event.ENTER_FRAME, arguments.callee);
                    var shift:Boolean = false;
                    if(event is ApplicationEvent && ApplicationEvent(event).data != null && "shift" in ApplicationEvent(event).data) {
                        shift = ApplicationEvent(event).data.shift;
                    }
					pasteFromBuffer(buffer, true, parentForPaste(buffer.commonParent), true, !shift);  // complete=true, logAction=true
				});
			}
		}
		
		public function parentForPaste(commonParentUID:String):AbstractObject
		{
			// use the commonParent from clipboard if it still exists, so the object will paste into arena it was copied from
            var arena:Arena;
			var commonParent:AbstractObject = null;
			if (commonParentUID != null) {
				var page:ArenaPage = world.findObjByUID(commonParentUID) as ArenaPage;  // not EventPage because of currentPage v/s editingPage issue
				if (page) {
					arena = page.parent as Arena;
					if (arena && arena.active && arena.currentPage && !arena.childOfClosed) {
						commonParent = arena.currentPage.object;  // in case flipped to another page since copied this stuff
					}
				}
			}

			// parent= selected arena page, original parent or gadget being edited, else world
			var parent:AbstractObject;
			var selection:Vector.<TaconiteModel> = _controller.selection.selectedModels;
			if (selection.length == 1 && selection[0].value is Arena) {
				arena = selection[0].value as Arena;
				parent = arena.currentArenaPage;
			} else if (commonParent) {
				parent = commonParent;
			} else if (_controller.currentVelumMediator) {
				parent = _controller.currentVelumMediator.editingObject as AbstractContainer;
			} else {
				parent = world.eventFlow;  // backstage
			}
            trace("parentForPaste="+parent);
			return parent;
		}
		
		
		///////////////  low-level functions to perform copy / paste to buffer
		
		/**
		 * Copy the current selection and return as JSON.
		 */
		public function copyToBuffer():Object
		{
			_pasteCount = 0;
			
			// its ok to copy a null selection ... just gives empty clipboard
			// getSelection strict=false to allow drag-copy objects outside the current vellum
            var selection:Vector.<TaconiteModel> = currentSelection;
			var buffer:Object = GadgetEncoder.buildCopyContentJson(selection, false);  // globalPositions=false
			ApplicationController.instance.authorController.log("copy", selection);
			return buffer;
		}

        private function get currentSelection():Vector.<TaconiteModel>
        {
            var diList:Vector.<DragInfo> = ApplicationController.instance.authorController.currentEditingMediator.getSelection(true, false, false);  // omitLocked=false
            return DragInfo.toModels(diList);
        }
		
		/**
		 * Paste the clipboard contents into the document. 
		 */
		private function pasteFromBuffer(buffer:Object, setComplete:Boolean, parent:AbstractObject, logAction:Boolean, withWires:Boolean):void
		{
			var bindingsState:Boolean = ApplicationController.instance.wireController.bindingsEnabled;
			//ApplicationController.instance.wireController.bindingsEnabled = false;
			_controller.clearSelection();

            if (logAction) {
                var token:Object = _controller.currentActionTree.openGroup("Paste");

                // add assets & gadgets to library, in case we are pasting in a different project
                var assets:Vector.<Asset> = GadgetDecoder.getClipboardAssets(buffer);
                for each (var asset:Asset in assets) {
                    if (!AssetController.instance.existsInModel(asset)) {
                        var assetAction:LibraryAddAction =  new LibraryAddAction(asset);
                        assetAction.perform();
                        _controller.currentActionTree.commit(assetAction);
                    }
                }
                var gadgets:Vector.<Project> = GadgetDecoder.getClipboardGadgets(buffer);
                for each (var gadget:Project in gadgets) {
                    if (!GadgetController.instance.existsInModel(gadget)) {
                        var gadgetAction:LibraryAddAction =  new LibraryAddAction(gadget);
                        gadgetAction.perform();
                        _controller.currentActionTree.commit(gadgetAction);
                    }
                }
            }

            trace("makePasteContent parent="+parent);
			GadgetDecoder.makePasteContent(buffer, parent as AbstractContainer, null, setComplete, withWires, function (objects:Vector.<AbstractObject>):void
                {
                    var obj:AbstractObject;
                    var buttons:Vector.<ToggleButton> = new Vector.<ToggleButton>();
                    for each(obj in objects) {
                        // force backstage objects to be above the flow view (don't affect arena or gadget children)
                        if(Application.instance.flowVisible && obj.y > 0 && obj.parent == world.eventFlow){
                            obj.y = -15;
                        }
                        // differentiate new groups from old ones;
                        if (obj.groupID != null) {
                            obj.groupID = "c"+obj.groupID;
                        }
                        var tb:ToggleButton = obj as ToggleButton;
                        if (tb && tb.toggleButtonGroup) {
                            buttons.push(tb);
                        }
                    }

                    for each(obj in objects)
                        transformPastedObj(obj);

                    ApplicationController.instance.wireController.bindingsEnabled = bindingsState;

                    ToggleButtonGroupController.instance.finishPastingToggleButtonGroups(buttons);
                    LibraryController.instance.updateForAssetChange();

                    // trim stray wires that now cross the velum boundary
                    if (ApplicationController.instance.authorController.currentVelumMediator)
                        SmartObjectController.trimStrayWires(ApplicationController.instance.authorController.currentVelumMediator.editingObject as SmartObject);

                    // undo
                    if (logAction) {
                        var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
                        for each(obj in objects) {
                            models.push(obj.model);
                        }
                        _controller.currentActionTree.commit(CreateObjectAction.fromSelection(models));
                        _controller.currentActionTree.closeGroup(token);
                    }

                    _finishWaitCounter = 2;
                    EditorUI.instance.stage.addEventListener(Event.ENTER_FRAME, finishPasteAfterRender);
                });
        }
		
		private function transformPastedObj(obj:AbstractObject):void
		{
            // this function is called only for the top-level pasted objects, not their children
			_pasteCount++;

			// adjust title:  Copy of XXXX 5
			if(!_objectCut){
				obj.title = Utilities.nameForCopy(obj.title, _pasteCount);
                obj.recordInitialValue("title");
			}

            if (!(obj is EventPage)) {
                PagerController.instance.activateObject(obj, true);
            }
			var model:TaconiteModel = obj.model;
            obj.messageCenterRepositioned = false;  // dock the MC
            _controller.selectSingleModel(model,true);  // extend = true
		}
		
		
		private var _finishWaitCounter:int;
		
		private function finishPasteAfterRender(e:Event):void
		{
			// if the pasted objects landed outside visible arena bounds, move them to topLeft of arena
			if (--_finishWaitCounter > 0)
				return;
			EditorUI.instance.stage.removeEventListener(Event.ENTER_FRAME, finishPasteAfterRender);

			var selection:Vector.<TaconiteModel> = _controller.selection.selectedModels;
			if (selection.length == 0)
				return;
			ApplicationController.instance.authorController.log("paste", selection);

			var parent:AbstractContainer = AbstractObject(selection[0].value).parent;
            parent.setChildrenZPositions(true); // setInits=true  (and with bindings=true in case StackOrder is wired to something)

            var page:ArenaPage = parent as ArenaPage;
			if (page == null)
				return;

			// ensure pasted objects are not hidden by arena clipping

			// get bounds of all pasted objects together
			var pasteBounds:Rectangle = new Rectangle();
			var stage:Stage = EditorUI.instance.stage;
			var m:TaconiteModel;
			var obj:AbstractObject;
			var view:ObjectView;
			for each (m in selection) {
				obj = m.value as AbstractObject;
				view = obj.getView();
				if (view) {
					pasteBounds = pasteBounds.union(view.getBounds(stage));
				}
			}

			// shift objects to topLeft of page if necessary
            view = page.getView();
            if (view) {
                var pageBounds:Rectangle = view.getBounds(stage);
                var sect:Rectangle = pageBounds.intersection(pasteBounds);
                if ((sect.width * sect.height > 50) == false) {  // small intersection, or none or NaN
                    // shift the objects
                    var dx:Number = pageBounds.x - pasteBounds.x;
                    var dy:Number = pageBounds.y - pasteBounds.y;
                    if (Math.abs(dx) < 1000000) {
                        for each (m in selection) {
                            obj = m.value as AbstractObject;
                            obj.x += dx;
                            obj.y += dy;
                        }
                    }
                }
            }
		}

        public function get pasteCount():int{
            return _pasteCount;
        }
	}
}
