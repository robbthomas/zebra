package com.alleni.author.controller.app
{
	import com.alleni.author.application.dialogs.LoginDialog;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.Environments;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.service.users.LoginOperation;
	import com.alleni.author.view.ui.controls.Alert;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.service.IOperation;
	import com.alleni.taconite.service.LogService;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.events.CloseEvent;

	public class LoginMediator
	{
		private static const MAX_LOGIN_ATTEMPTS:int = 4;
		private static const MAX_OPERATION_ATTEMPTS:int = 10;
		
		private static var _loginDialog:LoginDialog;
		private static var _loginOperation:LoginOperation;
		private static var _pendingOperation:IOperation;
		
		private static var _sessionLoginAttempts:int = 0;
		
		public function LoginMediator()
		{
		}
		
		public function handleLoginEvents(pendingOperation:IOperation=null):void
		{
			if (pendingOperation)
				_pendingOperation = pendingOperation;
			
			if (TaconiteFactory.getEnvironmentImplementation().guestUsername) {
				executeLogin(TaconiteFactory.getEnvironmentImplementation().guestUsername, TaconiteFactory.getEnvironmentImplementation().guestPassword, TaconiteFactory.getEnvironmentImplementation().serverName);
			} else {
				if (_loginDialog) return;
				_loginDialog = ApplicationController.instance.createPopUp(LoginDialog, false) as LoginDialog;
				
				_loginDialog.environInput.dataProvider = new ArrayCollection(Environments.environmentList);
				var server:String = TaconiteFactory.getEnvironmentImplementation().serverName;
				var environ:String = Environments.environForServerName(server);
				var index:int = getEnvIndex(Environments.environmentList, environ);
				trace("environ="+environ, "index="+index);
				_loginDialog.environInput.selectedIndex = index;

				if (TaconiteFactory.getEnvironmentImplementation().username.length > 0) {
					_loginDialog.usernameInput.text = TaconiteFactory.getEnvironmentImplementation().username;
					_loginDialog.passwordInput.text = TaconiteFactory.getEnvironmentImplementation().password;
					_loginDialog.usernameInput.selectRange(_loginDialog.usernameInput.text.length, _loginDialog.usernameInput.text.length+1);
				}
				_loginDialog.okButton.addEventListener(MouseEvent.CLICK, handleOKClicked);
			}
		}
		
		private function getEnvIndex(list:Array, target:String):int
		{
			var count:int = list.length;
			for (var n:int=0; n < count; n++) {
				var item:String = list[n];
				if (item == target)
					return n;
			}
			return -1;
		}
		
		private function handleOKClicked(event:Event):void
		{
			if (!_loginDialog) return;
			_loginDialog.usernameInput.errorString = "";
			_loginDialog.passwordInput.errorString = "";
			var env:String = _loginDialog.environInput.selectedItem as String;
			var server:String = Environments.serverForEnvironName(env);
			executeLogin(_loginDialog.usernameInput.text, _loginDialog.passwordInput.text, server);
		}
		
		private function executeLogin(username:String, password:String, server:String):void
		{
			trace("executeLogin user="+username, "pw="+password, "server="+server);
			if (!_loginOperation) {
				_loginOperation = new LoginOperation();
				_loginOperation.addEventListener(Event.COMPLETE, handleComplete);
				_loginOperation.addEventListener(OperationFaultEvent.FAULT, handleError);
			}
			_loginOperation.setCredentials(username, password, server);
			_loginOperation.execute();
			_sessionLoginAttempts++;
		}
		
		private function handleComplete(event:Event):void
		{
			trace("Login:complete");
			_sessionLoginAttempts = 0;
			reset();
			if (_pendingOperation && _pendingOperation.attempts < MAX_OPERATION_ATTEMPTS) {
				_pendingOperation.execute();
				return;
			}
			if (_pendingOperation && _pendingOperation.attempts >= MAX_OPERATION_ATTEMPTS)
				LogService.error("Operation '"+_pendingOperation.displayName+"' has been attempted by LoginMediator and looped back to login too many times.");
			
			if (_pendingOperation == null) {
                //MESSAGE NOT IN DOCUMENT
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, ["Successful login"]));
			}
		}
		
		private function handleError(event:Event):void
		{
			trace("Login:ERROR api="+TaconiteFactory.getEnvironmentImplementation().apiURI);
			LogService.error("Login:ERROR api="+TaconiteFactory.getEnvironmentImplementation().apiURI);
			if (_loginDialog) {
				var loginOperation:LoginOperation = event.target as LoginOperation;
				if (_sessionLoginAttempts > MAX_LOGIN_ATTEMPTS) {
					reset();

                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:33, closeFunction:handleErrorAlertDecision}));
				} else {
					_loginDialog.usernameInput.errorString = "Please verify your username.";
					_loginDialog.passwordInput.errorString = "Please verify your password.";
					_loginDialog.usernameInput.selectAll();
					_loginDialog.focusManager.setFocus(_loginDialog.usernameInput);
				}
			} else
				LogService.error("LoginMediator::handleError tried to dispatch an error to the view, but the user may have successfully logged in since");
		}
		
		private function handleErrorAlertDecision(event:CloseEvent):void
		{
			switch (event.detail) {
				case Alert.FIRST:
					LogService.instance.sendAsEmail("Error logging in");
					break;
				default:
					break;
			}
		}
		
		private function reset():void
		{
			// Restore application hot keys
			ApplicationController.instance.removePopup(_loginDialog);
			_loginOperation.removeEventListener(Event.COMPLETE, handleComplete);
			_loginOperation.removeEventListener(OperationFaultEvent.FAULT, handleError);
			_loginOperation = null;
			_loginDialog = null;
		}
	}
}
