package com.alleni.author.controller.app
{
	import com.alleni.author.definition.Objects;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.taconite.definition.ObjectDefinition;
	import com.alleni.taconite.persistence.json.JSON;
	
	import flash.utils.Dictionary;
	import flash.utils.describeType;
	
	public class ValueDetector
	{
		private static var _instance:ValueDetector;
		
		private var classExamples:Dictionary = new Dictionary();  // shortClassname -> Dictionary{ prop -> defaultValueAsString }
		
		
		public function ValueDetector()
		{
			if (_instance != null)
				throw new Error("ValueDetector");
		}
		
		public static function get instance():ValueDetector
		{
			if (_instance == null)
				_instance = new ValueDetector();
			return _instance;
		}
		
		public function differsFromDefault(value:*, prop:String, dict:Dictionary):Boolean
		{
			var defStr:String = dict[prop];
			if (defStr != null) {
				var valStr:String = com.alleni.taconite.persistence.json.JSON.encode(value);
				var differ:Boolean = valStr != defStr;
				//				if (differ)
				//					trace("  differ: prop="+prop, "valStr="+valStr, "defStr="+defStr);
				return differ;
			}
			return false;  // NOTE: could be read-only or Transient as noted in classDescription
		}
		
		public function getDefaultValues(className:String):Dictionary
		{
			var values:Dictionary = classExamples[className];
			if (values == null) {
				values = buildDefaultValues(className);
				classExamples[className] = values;
			}
			return  values;
		}
		
		private function buildDefaultValues(className:String):Dictionary
		{
			var dict:Dictionary = new Dictionary();
			var def:ObjectDefinition = Objects.descriptionForShortClassname(className);
			var obj:* = new def.model();
			var classInfo:XML = describeType(obj);
			for each (var v:XML in classInfo..*.(name() == "variable" || (name() == "accessor" && attribute("access").charAt( 0 ) == "r" ))) {
				if (v.metadata && v.metadata.( @name == "Transient" ).length() > 0)
					continue;
				var prop:String = v.@name;
				var value:* = obj[prop];
				var valueStr:String = com.alleni.taconite.persistence.json.JSON.encode(value);
				dict[prop] = valueStr;
			}
			return dict;
		}
		
	}
}