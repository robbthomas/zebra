package com.alleni.author.controller.app
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.EventPageEditingMediator;
import com.alleni.author.Navigation.FlowEditingMediator;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.application.ApplicationUI;
import com.alleni.author.application.dialogs.HistoryDialog;
import com.alleni.author.application.dialogs.OpenDialog;
import com.alleni.author.application.dialogs.SaveAsDialog;
import com.alleni.author.application.dialogs.ZappPublishingDialog;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.objects.CompositeVelumMediator;
import com.alleni.author.controller.objects.PathEditingMediator;
import com.alleni.author.controller.test.TestPanelController;
import com.alleni.author.controller.text.TextEditMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.definition.Environments;
import com.alleni.author.definition.GadgetType;
import com.alleni.author.definition.application.ApplicationStrings;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.document.Document;
import com.alleni.author.event.GadgetEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.gadgets.GadgetState;
import com.alleni.author.model.gadgets.Gadgets;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.EventFlowController;
import com.alleni.author.model.objects.LMS;
import com.alleni.author.model.objects.TemplateValueObject;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Assets;
import com.alleni.author.persistence.GadgetData;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.service.gadgets.GadgetHistoryOperation;
import com.alleni.author.service.gadgets.GadgetStateSaveOperation;
import com.alleni.author.service.project.ProjectListDialogOperation;
import com.alleni.author.service.project.ProjectListOperation;
import com.alleni.author.service.project.ProjectLoadOperation;
import com.alleni.author.service.project.PublishedProjectListOperation;
import com.alleni.author.util.StringUtils;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.view.ui.OverlayMessage;
import com.alleni.author.view.ui.controls.Alert;
import com.alleni.author.view.ui.controls.TransformationEditorPanel;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.factory.IEnvironment;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.persistence.json.JSON;
import com.alleni.taconite.persistence.json.JSONEncoder;
import com.alleni.taconite.service.LogService;
import com.alleni.taconite.service.RestHttpOperation;

import flash.display.Sprite;

import flash.errors.IllegalOperationError;
import flash.events.Event;
import flash.events.TimerEvent;
import flash.external.ExternalInterface;
import flash.net.FileFilter;
import flash.net.FileReference;
import flash.utils.Dictionary;
import flash.utils.Timer;
import flash.utils.getTimer;

import mx.events.CloseEvent;
import mx.formatters.DateFormatter;

public class ProjectController extends GadgetController
	{
		public static const DEFAULT_EXTENSION:String = "zzx";

		private static const STATE_NORMAL:String 		= "normal";
		private static const STATE_NEWDOC:String 		= "newDoc";
		private static const STATE_SETDOC:String 		= "setDoc";
		private static const STATE_CLOSING:String 		= "closing";
		private static const STATE_QUITTING:String 		= "quitting";
		private static const STATE_INVOKING:String		= "invoking";
		private static const STATE_OPENING:String		= "opening";
		private static const STATE_OPENING_CLOUD:String	= "openingCloud";
		private static const STATE_PUBLISHING:String	= "publishing";
		
		private static var _instance:ProjectController = null;
		
		private var _appState:String = STATE_NORMAL;
		private var _previousState:String = STATE_NORMAL;
        private var _wasEditingWhenSaved:Boolean;
        private var _overlaySprite:Sprite;

		private var _invokedId:String;
		
		public static const AUTOSAVE_MIN_MINUTES:Number = 1;
		public static const AUTOSAVE_MAX_MINUTES:Number = 120;
		public static const AUTOSAVE_DEFAULT_MINUTES:Number = 20;

		private var _autosaveTimer:Timer;
		private var _autosaveTriggered:Boolean;
		private var _autosaveStartTime:Number = 0;
		private var _nowLoading:Boolean;
		private var _loadVersion:Boolean;  // loading a specific version from history log (prevents the alert that appears when loading autosave version)
        private var _doingProjectSave:Boolean;
        private var _newWindowType:String = "new";

        public var isTemplateDialogSave:Boolean = false;
        public var isDrawingPath:Boolean = false;
		
		public function ProjectController()
		{
			Utilities.assert(!_instance);
			ApplicationController.addEventListener(NotificationNamesApplication.CREATE_NEW_PROJECT, createNewProject);
			ApplicationController.addEventListener(NotificationNamesApplication.INVOKE_PROJECT, handleOpenProject);
			ApplicationController.addEventListener(NotificationNamesApplication.OPEN_PROJECT, handleOpenProject);
			ApplicationController.addEventListener(NotificationNamesApplication.OPEN_LOCAL_PROJECT, handleOpenLocalProject);
			ApplicationController.addEventListener(NotificationNamesApplication.SAVE_PROJECT, handleSaveProject);
			ApplicationController.addEventListener(NotificationNamesApplication.SAVE_AS_PROJECT, handleSaveAsProject);

            //ZAPPING and SHARING LISTENERS
            ApplicationController.addEventListener(NotificationNamesApplication.OPEN_PUBLISHING_DIALOG, handlePublishingDialog);
            ApplicationController.addEventListener(NotificationNamesApplication.UPDATE_PRIVACY_LEVEL, goToPrivacyLevelPanel);

            ApplicationController.addEventListener(NotificationNamesApplication.SHARE_TO_EMAIL_LIST, handleShareDialog);
            ApplicationController.addEventListener(NotificationNamesApplication.SHARE_TO_EMBED, handleShareDialog);
            ApplicationController.addEventListener(NotificationNamesApplication.SHARE_TO_FACEBOOK, handleShareDialog);
            ApplicationController.addEventListener(NotificationNamesApplication.SHARE_TO_LINK, handleShareDialog);
            ApplicationController.addEventListener(NotificationNamesApplication.SHARE_TO_LINKED_IN, handleShareDialog);
            ApplicationController.addEventListener(NotificationNamesApplication.SHARE_TO_LMS, handleShareDialog);
            ApplicationController.addEventListener(NotificationNamesApplication.SHARE_TO_SHOPP, handleShareDialog);
            ApplicationController.addEventListener(NotificationNamesApplication.SHARE_TO_TWITTER, handleShareDialog);
            ApplicationController.addEventListener(NotificationNamesApplication.SHARE_TO_GUEST_LIST, handleShareDialog);

            // add more share notification listeners here ^^^^^^

			ApplicationController.addEventListener(NotificationNamesApplication.CLOSE_PROJECT, handleCloseProject);
			ApplicationController.addEventListener(NotificationNamesApplication.REQUEST_QUIT_APPLICATION, handleRequestQuit);
			
			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
				_autosaveTimer = new Timer(20*1000); // polling cycle  (should be shorter than the shortest auto-save time interval)
				_autosaveTimer.addEventListener(TimerEvent.TIMER, autosaveChecker);
				_autosaveTimer.start();
            }
        }

		static public function get instance():ProjectController
		{
			if (!_instance)
				_instance = new ProjectController();
			return _instance;
		}
		
		private function handleOpenProject(event:ApplicationEvent = null):void
		{
            _invokedId = null;
            if (event && event.type == NotificationNamesApplication.INVOKE_PROJECT && event.data != null) {
                _invokedId = event.data as String;
                _previousState = _appState;
                _appState = STATE_INVOKING;
                alertSave();
            } else {
                openProject(true);
            }
		}
		
		private function handleOpenLocalProject(event:ApplicationEvent = null):void
		{
		    openProject(false);
		}
		
		private function handleSaveProject(event:ApplicationEvent = null):void
		{
            if (Application.instance.projectFromOtherUser) {
                handleSaveAsProject();
            } else {
                saveProject();
            }
		}

		
		private function handleSaveAsProject(event:ApplicationEvent = null):void
		{
		    saveAsCloudProject();
		}


		private function get authorController():AuthorController
		{
			return ApplicationController.instance.authorController;
		}
		
		private function autosaveReset():void
		{
			// a Save completed, or autosave completed, or load completed
			_autosaveTriggered = false;
		}
		
		private function autosaveChecker(e:Event):void
		{
            var doc:Document = Application.instance.document;
            if (doc == null || doc.upToDate || !autosaveAllowed) {
                return;
            }
            if (_autosaveTriggered == false) {
                _autosaveTriggered = true;
                _autosaveStartTime = getTimer();
                autosaveLog("autosave Triggered.  If save is allowed it will occur in minutes="+world.autosaveMinutes);
            } else {  // triggered already
                var elapsedMinutes:Number = (getTimer() - _autosaveStartTime)/(60*1000);
                autosaveLog("autosave:  time elapsed since trigger: minutes="+elapsedMinutes);
                if (elapsedMinutes > world.autosaveMinutes) {
                    showAutosavingSign();
                    authorController.log("autosave:  now saving project="+doc.name);
                    saveProject(null, true);  // autosave=true
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, "Automatically saving your work..."));
                    // autosaveReset() is called on completion of the save
                    autosaveReset(); // but also call it now, to prevent a double-save
                }
            }
		}

        private function get autosaveAllowed():Boolean
        {
            if (Application.instance.document == null || world == null || world.eventFlow == null) {
                autosaveLog("autosave dissallowed by null var");
                return false;
            }
            if (ApplicationController.instance.disabledInput) {    // disabledInput is true while any dialog popup is showing, and during lazyLoad & event transitions, etc
                autosaveLog("autosave dissallowed by disabledInput (dialog is active, or lazyloading, or doing event transition, etc)");
                return false;
            }
            if (ApplicationUI.instance.mouseIsDown) {
                autosaveLog("autosave dissallowed: mouseIsDown");
                return false;
            }
            if (Application.instance.projectFromOtherUser || _doingProjectSave || _nowLoading) {
                autosaveLog("autosave dissallowed:  projectFromOtherUser="+Application.instance.projectFromOtherUser + " nowSaving="+_doingProjectSave + " nowLoading="+_nowLoading);
                return false;
            }
            if (!(world.autosaveMinutes > 0)) {
                autosaveLog("autosave dissallowed:  autosaveMinutes="+world.autosaveMinutes);
                return false;
            }
            if (authorController.currentVelumMediator is CompositeVelumMediator) {
                autosaveLog("autosave dissallowed by gadget editing");
                return false;
            }
            if (authorController.currentEditingMediator is TextEditMediator){
                autosaveLog("autosave dissallowed by text editing");
                return false;
            }
            if (authorController.currentEditingMediator is PathEditingMediator){
                autosaveLog("autosave dissallowed by path editing");
                return false;
            }
            if (isDrawingPath) {
                autosaveLog("autosave dissallowed by path creation");
                return false;
            }
            if (RestHttpOperation.networkAvailable == false){
                autosaveLog("autosave dissallowed because network is not available");
                return false;
            }
            if (Application.instance.publishingInProgress == true){
                autosaveLog("autosave dissallowed because publishing is in progress");
                return false;
            }
            if(Application.instance.customerType == "CREATOR" && (Application.instance.document.project.accountType == "PRO" || Application.instance.document.project.accountType == "ENTERPRISE")){
                autosaveLog("autosave dissallowed because project is pro or above and user is creator");
                return false;
            }
            if (TransformationEditorPanel.isActive) {
                autosaveLog("autosave dissallowed because XYR panel is showing");
                return false;
            }
            return true;
        }

        private function autosaveLog(message:String):void
        {
            if (world.autosaveMinutes == 1) { // when minutes=1 its probably an engineer or tester and this is useful.  Otherwise don't fill up the log
                authorController.log(message);
            }
        }

        private function showAutosavingSign():void
        {
            const AUTOSAVE_SIGN_MSECS:Number = 4000;  // fixed amount of time for sign; regardless of how long the Save actually takes
            EditorUI.instance.bottomControlBar.showAutosavingSign();

            var timer:Timer = new Timer(AUTOSAVE_SIGN_MSECS);
            timer.addEventListener(TimerEvent.TIMER, function (e:TimerEvent):void {
                timer.removeEventListener(TimerEvent.TIMER, arguments.callee);
                timer.stop();
                EditorUI.instance.bottomControlBar.removeAutosavingSign();
            });
            timer.start();
        }

		private function get world():World
		{
			return Application.instance.document.root.value as World;
		}
		
		private function handlePublishingDialog(event:ApplicationEvent = null):void
		{
			const document:Document = Application.instance.document;
			if (!document) return;
			if (authorController.editing || !document.upToDate // have changes been made since last save?
				|| !document.project || !document.project.projectId || Application.instance.projectFromOtherUser) { // does the document have a valid root gadget?
                //MESSAGE NOT IN DOCUMENT
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, ["Preparing to publish",
					"Saving in preparation for publishing.", OverlayMessage.CLOUD_SKIN]));

                _previousState = STATE_PUBLISHING;

                handleSaveProject();

                return;
			}
            Application.instance.publishingInProgress = true;
			prepPublishNameData();
		}

        private function prepPublishNameData(projectType:uint = 2):void{
            var publishedProjectList:PublishedProjectListOperation = new PublishedProjectListOperation(projectType);
            publishedProjectList.addEventListener(Event.COMPLETE, doPublish);
            publishedProjectList.execute();
        }

        private function doPublish(event:Event):void
        {
            if (ZappPublishingDialog.instance) return;
            ZappPublishingDialog.shareMessage = false;
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.DESELECT));
            ApplicationController.instance.createPopUp(ZappPublishingDialog, false);
        }

		private function handleShareDialog(event:ApplicationEvent = null):void{
            if(Application.instance.document.project.published == false) {
                ZappPublishingDialog.howToShare = event.type;
                handlePublishingDialog();

            }else{
                var params:Object = {panel:1, shareDefault:0, privacyLevel:0};
                if(Application.instance.document.project.inviteOnly){
                    params.privacyLevel = 1;
                }else if(Application.instance.document.project.hideInStoreList == false){
                    params.privacyLevel = 3;
                }else{
                    params.privacyLevel = 2;
                }

                switch(event.type){
                    case NotificationNamesApplication.SHARE_TO_EMAIL_LIST:
                        params.shareDefault = 5;
                        break;
                    case NotificationNamesApplication.SHARE_TO_EMBED:
                        params.shareDefault = 2;
                        break;
                    case NotificationNamesApplication.SHARE_TO_FACEBOOK:
                        params.shareDefault = 6;
                        break;
                    case NotificationNamesApplication.SHARE_TO_GUEST_LIST:
                        params.shareDefault = 1;
                        break;
                    case NotificationNamesApplication.SHARE_TO_LINK:
                        params.shareDefault = 4;
                        break;
                    case NotificationNamesApplication.SHARE_TO_LINKED_IN:
                        params.shareDefault = 7;
                        break;
                    case NotificationNamesApplication.SHARE_TO_LMS:
                        params.shareDefault = 3;
                        break;
                    case NotificationNamesApplication.SHARE_TO_SHOPP:
                        params.privacyLevel = 3;
                        params.shareDefault = 0;
                        params.panel = 0;
                        break;
                    case NotificationNamesApplication.SHARE_TO_TWITTER:
                        params.shareDefault = 8;
                        break;
                    default:
                        params.shareDefault = 0;
                }

                params.justZapped = false;

                openPrivacySharePanel(params);
            }
        }

        private function goToPrivacyLevelPanel(event:ApplicationEvent):void{
            openPrivacySharePanel({panel:0, privacyLevel:0, justZapped:false});
        }

        public function openPrivacySharePanel(params:Object):void {
            /*
             * values in "params" -
             * params.panel:int
             * 		0 = Security Panel
             * 		1 = Share Panel
             * params.privacyLevel:int
             * 		0 = Use what the server gives you
             * 		1 = override with Guest List level
             * 		2 = override with Hidden(NOT in shopp)
             * 		3 = override with Shopp (Public)
             * params.shareDefault:int
             * 		0 = Default
             * 		1 = Guest List
             * 		2 = Embed
             * 		3 = LMS
             * 		4 = Direct Link
             * 		5 = Email
             * 		6 = Facebook
             * 		7 = Linked In
             * 		8 = Twitter
             * params.publishId
            */
            params.projectId = Application.instance.document.project.projectId;
            var payload:Object = params;
            ExternalInterface.call("openDialog", payload);
        }

		private function handleCloseProject(event:ApplicationEvent = null):void
		{
			alertSave();
		}
		
		private function actOnState():void
		{
			// pop the current state and use the previous one
			switch (_previousState) {
				case STATE_QUITTING:
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.APPLICATION_QUITTING));
					break;
				case STATE_NEWDOC:
                    PseudoThread.add(new PseudoThreadWaitFrameRunnable());  // let menu redraw
                    PseudoThread.add(new PseudoThreadSimpleRunnable("abandonEditors", authorController.abandonEditors));
                    PseudoThread.add(new PseudoThreadSimpleRunnable("makeNewUntitledProject", makeNewUntitledProject));
                    PseudoThread.add(new PseudoThreadSimpleRunnable("open template dialog", ApplicationController.instance.dispatchEvent, null, [new ApplicationEvent(NotificationNamesApplication.OPEN_TEMPLATE_DIALOG)]));
					break;
				case STATE_SETDOC:
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.OPEN_SETTINGS_DIALOG));
                    break;
				case STATE_INVOKING:
					ProjectLoadingSequencer.instance.requestLoad(_invokedId, false, false);
					_invokedId = null;
					break;
				case STATE_OPENING:
					showLocalBrowseDialog();
					break;
				case STATE_OPENING_CLOUD:
					showCloudBrowseDialog();
					break;
				case STATE_PUBLISHING:
					prepPublishNameData();
					break;
				case STATE_NORMAL:
					// nothing to do
					break;
				default:
					LogService.error("ProjectController was asked to act on a state '"+_previousState+"' which is not accounted for.");
					break;
			}
			
			_previousState = STATE_NORMAL;
			_appState = STATE_NORMAL;
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE_WITH_DELAY));
		}
		
		private function createNewProject(event:ApplicationEvent = null):void
		{
            _previousState = _appState;
            _appState = STATE_NEWDOC;
            alertSave();
		}
		
		public function makeNewUntitledProject():void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_LOADING));
			var document:Document = new Document();
            document.newDocument = true;
			document.name = Application.instance.nextUntitledProjectName;
            ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_INITIALIZED, function(event:Event):void {
				event.currentTarget.removeEventListener(event.type, arguments.callee);
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE, ApplicationStrings.DEFAULT_STATUS_MESSAGE));
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_COMPLETE)); // to ensure new project is marked clean
				authorController.log("newProject");
			});
			Application.instance.document = document;
		}
		
		private function handleRequestQuit(event:Event):void
		{
			// move to the quitting state
			_previousState = STATE_NORMAL;
			_appState = STATE_QUITTING;
			
			// Give the user the chance to save open documents.  If they don't cancel,
			// close will do the final exit.
			alertSave();
		}
		
		private function openProject(fromCloud:Boolean=true):void
		{
			_previousState = _appState;
			if (fromCloud)
				_appState = STATE_OPENING_CLOUD;
			else
				_appState = STATE_OPENING;
			alertSave("open");
		}
		
		private function showLocalBrowseDialog():void
		{
			var fileReference:FileReference = new FileReference();
			fileReference.addEventListener(Event.SELECT, handleFileSelected);
			fileReference.addEventListener(Event.CANCEL, handleFileCancel);
			// FileReference seems to inadvertently trigger IllegalOperationError as if there were multiple sessions
			// with no rhyme or reason, so we need to catch the resulting exception.
			try {
				fileReference.browse([new FileFilter("ZebraZapps Projects (*."+DEFAULT_EXTENSION+")", "*."+DEFAULT_EXTENSION)]);
			} catch(e:IllegalOperationError) {
				LogService.error("ProjectController attempted a new browsing session before the previous had completed.");
			}
		}
		
		private function handleFileSelected(e:Event):void
		{
			loadLocalProject(e.currentTarget as FileReference);
			
			_previousState = _appState;
			_appState = STATE_NORMAL;
		}
		
		private function handleFileCancel(e:Event):void
		{
			_previousState = _appState;
			_appState = STATE_NORMAL;
		}
		
		public function loadLocalProject(file:FileReference):void
		{	
			_appState = STATE_NORMAL;
			_previousState = STATE_NORMAL;
			
			if (!file) return;
			
//			var operation:GadgetLocalLoadOperation = new GadgetLocalLoadOperation(file, false);
//			operation.displayName = "Loading...";
//			operation.addEventListener(GadgetEvent.LOAD_FAILED, handleLoadFailed);
//			operation.addEventListener(GadgetEvent.ERROR, handleError);
//			operation.addEventListener(Event.COMPLETE, handleLoadComplete());
//			operation.execute();
//
//			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE_WITH_INDICATOR, operation.displayName));
//			authorController.log("   loadedProject name=\""+Application.instance.document.name+"\"" + " id="+Application.instance.document.documentId);
		}
		
		/**
		 * Open a file from the cloud 
		 * 
		 */
		private function showCloudBrowseDialog():void
		{
			ApplicationController.instance.createPopUp(OpenDialog, true);
		}

        public function cancelledOpenDialog():void
        {
        }
		
		public function loadProjectList(callback:Function, userName:String=null):void
		{
            var op:ProjectListDialogOperation = new ProjectListDialogOperation(false, GadgetType.PROJECT);
            op.userName = userName;
            op.useMini = true;
            op.displayName = "Loading project list";
			op.addEventListener(Event.COMPLETE, handleLoadProjectsComplete(callback));
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE_WITH_INDICATOR, op.displayName));
			op.execute();

		}
		
		private function handleLoadProjectsComplete(callback:Function):Function
		{
			return function(event:Event):void
			{
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE));
				
				var op:ProjectListDialogOperation = event.target as ProjectListDialogOperation;
				if (!op)
					return;
                op.removeEventListener(event.type, arguments.callee);
				callback(op.projects);
			}
		}
		
		public function loadGadgetHistory(name:String, callback:Function):void
		{
			var historyOp:GadgetHistoryOperation = new GadgetHistoryOperation(name);
			historyOp.displayName = "Loading history";
			historyOp.addEventListener(Event.COMPLETE, historyLoadComplete(callback));
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE_WITH_INDICATOR, historyOp.displayName));
			historyOp.execute();
		}
		
		private function historyLoadComplete(callback:Function):Function
		{
			return function(event:Event):void
			{
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE));
				
				var historyOp:GadgetHistoryOperation = event.target as GadgetHistoryOperation;
				if (!historyOp)
					return;
				
				callback(historyOp.listForUI);
			}
		}
		
		/**
		 * Select a file from the cloud 
		 * Only ProjectLoadingSequencer should be calling.
		 * @param id
		 * 
		 */
		public function loadCloudProject(id:String, asACopy:Boolean=false, purchased:Boolean=false, loadVersion:Boolean=false, templateVO:TemplateValueObject=null):void
		{
            var env:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			var server:String = Environments.environForServerName(env.serverName);

			authorController.log("loadProject id="+id + " user="+OpenDialog.selectedUsername + " server="+server);
            _appState = STATE_OPENING_CLOUD;
            _nowLoading = true;
            _loadVersion = loadVersion;
            if(!purchased) {
                load(id, asACopy, true, null, null, NaN, NaN, null, false, null, loadVersion, templateVO);
            } else if(env.fromLMS) {
                var values:Object = env.lmsValues;
                var externalUserId:String = null;
                if(values.hasOwnProperty('user_id')) {
                    externalUserId = values.user_id;
                }
                if(values.hasOwnProperty('suspendData')) {
                    try {
                        var suspendData:Object = com.alleni.taconite.persistence.json.JSON.decode(values.suspendData, false);
//                        if('onNextOpen' in suspendData && suspendData.onNextOpen == 'menu') {
//                            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:1050, closeFunction:loadStateCloseFunction(id, asACopy,  externalUserId), inserts:[]}));
//                        } else {
                            loadPurchased(id, asACopy, false, externalUserId);
//                        }
                    } catch (error:Error) {
                        loadPurchased(id, asACopy, false, null);
                    }
                } else {
                    loadPurchased(id, asACopy, false, null);
                }
            } else {
                loadPurchased(id, asACopy, false, null);
            }
		}
		
		override protected function handleLoadComplete(asACopy:Boolean=false, composite:SmartObject=null, dropX:Number=NaN, dropY:Number=NaN,
													   gadgetData:GadgetData=null, createOnNullContainer:Boolean=false, published:Project=null, templateVO:TemplateValueObject=null):Function
		{
			return function(e:Event):void
			{

				const project:Project = ProjectLoadOperation(e.target).project;

                //TODO GADGET_PROJECT prevent embedding?
//				const purchasedLoadOperation:PurchasedLoadOperation = e.target as PurchasedLoadOperation;
//				if (purchasedLoadOperation) {
//					if (project) {
//						const hasDomainCapabilities:Boolean = TaconiteFactory.getEnvironmentImplementation().hasDomainCapabilities;
//						rootGadget.userAwarePurchaseId = purchasedLoadOperation.id;
//
//						if (!rootGadget.userAccessPrivs.embed && !hasDomainCapabilities) {
//							LogService.error("Project '" + rootGadget.publishedName + "' " +
//								"[Gadget ID:" + rootGadget.gadgetID + "][Published ID:" + rootGadget.publishedId + "] is not allowed to run embedded out of domain. Aborting.");
//							ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_NO_EMBED_PERMISSION));
//							return;
//						}
//					} else {
//						LogService.error("Unrecoverable: ProjectController::handleLoadComplete encountered a PurchasedLoadOperation with no root gadget.");
//						return;
//					}
//				}
				
				Application.instance.document = new Document(project);
				
				if (Application.instance.projectFromOtherUser) {
                    World(Application.instance.document.root.value).autosaveMinutes = 0;  // turn off autosave, for project loaded from other account
				}
				
				// We're not done until we're done. Let the higher-level project loading sequencer determine when we are back to normal.
				ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_COMPLETE, function(event:Event):void {
					event.currentTarget.removeEventListener(NotificationNamesApplication.PROJECT_COMPLETE, arguments.callee);
					actOnState();
					var env:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
					autosaveReset();
					_nowLoading = false;
					authorController.log("   loadedProject name=\""+Application.instance.document.name+"\"" + " id="+Application.instance.document.documentId);

					if (project.state == null) {
						handleMissingProjectState();
					}

					if(templateVO) {
						Application.instance.document.documentId = null;
                        templateVO.apply(Application.instance.document, true);
						saveCloudProject(templateVO.projectName, null, false, true);
						
						ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_SETTINGS_CHANGED));
					}
				});
				if (asACopy) // if project was loaded as a copy
					saveCurrentAsACopy();
			}
		}

		override protected function handleLoadFailed(event:Event):void
		{
			super.handleLoadFailed(event);
			logError("LOAD FAILED");
			handleErrorDefault();
		}
		
		override protected function handleLoadNoPermission(event:Event):void
		{
			super.handleLoadNoPermission(event);
			logError("LOADING: NO-PERMISSION");
			handleErrorDefault();
		}
		
		override protected function handleError(event:GadgetEvent):void
		{
			super.handleError(event);
			logError("GADGET LOADING ERROR id="+event.id);
			handleErrorDefault();
		}
		
		private function handleErrorDefault():void
		{
			_previousState = STATE_NORMAL;
			actOnState();
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_LOAD_FAILED));
		}

		//SAVING DOCUMENT FILES


        private function saveProject(filename:String = null, autosave:Boolean=false):void
        {
            authorController.log("saveProject name="+filename + " autosave="+autosave);
            postSavingMessage();
            _doingProjectSave = true;  // setting this early for greater protection ... its also set later
            _wasEditingWhenSaved = false;
            _overlaySprite = null;
            var flow:EventFlow = world.eventFlow;
            if (flow.editingPage) {
                flow.ignorePageChanges = true;
                _wasEditingWhenSaved = true;
                authorController.saveEventImageAfterEdit(flow.editingPage);
                _overlaySprite = authorController.showEditingWorldOverlay();
            } else if (Application.running) {
                flow.wasEditingEvent = -1;  // ensure it loads in running state
                EventFlowController.instance.saveResumeData(flow);
                EventFlowController.instance.saveEditWhileRunChanges();  // uses PseudoThread
            }

            PseudoThread.add(new PseudoThreadWaitFrameRunnable());  // wait for event overlay to appear, and status msg also
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            PseudoThread.add(new PseudoThreadSimpleRunnable("saveProjectNow", saveProjectNow, this, [filename, autosave]));
        }

        private function saveProjectNow(filename:String = null, autosave:Boolean=false):void
        {
            authorController.closeEditors(null,
                function():void {  // onComplete
                    saveCloudProject(filename, null, autosave);
                },
                function():void {  // onCancel of the gadget replaceAll dialog
                    saveProjectComplete();
                    _doingProjectSave = false;
                    _appState = STATE_NORMAL;   // may have cancelled replaceAll in process of closing file via alertSave
                    _previousState = STATE_NORMAL;
                });
        }

        private function postSavingMessage():void
        {
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE,
                    ApplicationStrings.SAVING + ' "' + Application.instance.document.project.name + '"'));
        }

        private function saveProjectComplete():void
        {
            // this is done even when cancel the gadget replace-all, and Save was never attempted
            if (_wasEditingWhenSaved) {
                _wasEditingWhenSaved = false;
                var flow:EventFlow = world.eventFlow;
                PseudoThread.add(new PseudoThreadSimpleRunnable("ProjectController::saveProjectComplete1", function():void{
                    if (flow.wasEditingEventPage) {
                        authorController.editEvent(flow.wasEditingEventPage);  // clears ignorePageChanges
                    }
                }));
                PseudoThread.add(new PseudoThreadWaitFrameRunnable());  // let loaded objects render
                PseudoThread.add(new PseudoThreadSimpleRunnable("ProjectController::saveProjectComplete2", function():void{
                    flow.ignorePageChanges = false; // start lazyload, if not already done by editEvent
                    authorController.removeEditingWorldOverlay(_overlaySprite);
                }));
            }
            PseudoThread.add(new PseudoThreadSimpleRunnable("ProjectController::saveProjectComplete3", function():void{
                if(_tokenForSaving) {
                    ApplicationController.instance.enableInput(_tokenForSaving);
                    _tokenForSaving = null;
                }
                if(_tokenForSaveAs) {
                    ApplicationController.instance.enableInput(_tokenForSaveAs);
                    _tokenForSaveAs = null;
                }
            }));
        }

		private function saveCurrentAsACopy():void    // no longer used
		{
			// TODO: duplicate gadgets and assets within for a true copy
			var document:Document = Application.instance.document;
			document.documentId = null;
			document.name = Utilities.nameForCopy(document.name);
			saveCloudProject();
		}

        private var _tokenForSaveAs:Object;

		private function saveAsCloudProject():void
		{
			// Open Save to Cloud Dialog
			_tokenForSaveAs = ApplicationController.instance.disableInput("saveAsCloudProject");
            var saveProjectDialog:SaveAsDialog = ApplicationController.instance.createPopUp(SaveAsDialog, true) as SaveAsDialog;
			saveProjectDialog.filename = Application.instance.document.name;
		}

        public function saveAsOkClicked(filename:String):void
        {
            Application.instance.document.project.nameIsFromDatabase = false;
            saveProject(filename);
        }

        public function cancelledSaveAsDialog():void
        {
            if(_tokenForSaveAs) {
                ApplicationController.instance.enableInput(_tokenForSaveAs);
                _tokenForSaveAs = null;
            }
        }

        private var _tokenForSaving:Object;

		private function saveCloudProject(name:String=null, id:String=null, autosave:Boolean=false, templateSave:Boolean = false, replaceExisting:Boolean = false):void
		{
			trace("SAVE auto="+autosave);
            _doingProjectSave = true;
			var document:Document = Application.instance.document;
			name = StringUtils.stripWhitespace(name); // strip whitespace

			if (name != null) document.name = name; // will be validated in validateFileName
			if (id != null) {
                document.documentId = id;
                // we are knowingly overwriting
                // so we'll say we know the name is from the database
                document.project.nameIsFromDatabase = true;
                document.project.publishedNameIsFromDatabase = true;
            }
			
			if (validateFileName(document.name)) {
				// we will now finishing saving any pending gadgets, and save the project gadget;  Prevent user interruptions
                postSavingMessage();
				_tokenForSaving = ApplicationController.instance.disableInput("saveCloudProject");
				ApplicationController.instance.addEventListener(NotificationNamesApplication.PROJECT_STATE_SAVED, saveCompletionHandler);
                var gadgets:Vector.<Project> = GadgetController.instance.gadgetsListForProjectSave();
                save(document.project, world, true, false, gadgets, document.assetsRoot.value as Assets, autosave, isTemplateDialogSave, null, replaceExisting);
                saveProjectComplete();  // enqueues operations to re-open editing
			}
		}

        public function saveUserState(lms:LMS):void {
			trace("SAVE user state");
			var document:Document = Application.instance.document;
            var externalUserId:String = null;
            if(ApplicationController.instance.authorController.world.lms) {
                externalUserId = ApplicationController.instance.authorController.world.lms.user_id;
                if(externalUserId != null && externalUserId.length == 0) {
                    externalUserId = null;
                }
            }

			ApplicationController.instance.addEventListener(NotificationNamesApplication.PROJECT_STATE_SAVED, stateSaveCompletionHandler);
            var gadgetStateOperation:GadgetStateSaveOperation = new GadgetStateSaveOperation(document.documentId, externalUserId, null, document.root.value as World, document.gadgetsRoot.value as Gadgets, document.assetsRoot.value as Assets, document.project.scripts);
            gadgetStateOperation.addEventListener(GadgetEvent.STATE_ERROR, function(event:Event):void {
                if(lms) {
                    lms.handleStateComplete(true, null);
                }
            });
            gadgetStateOperation.addEventListener(Event.COMPLETE, function(event:Event):void {
                if(lms) {
                    lms.handleStateComplete(false, gadgetStateOperation.stateId);
                }
            });
            gadgetStateOperation.execute();
        }

        public function stateSaveCompletionHandler(event:ApplicationEvent):void {
            var error:String = event.data as String;
            handleUserStateComplete(error == null, error);
        }

        public function handleUserStateComplete(success:Boolean, error:String = null):void {
			if (success) {
				actOnState();  // go ahead and quit or load, whatever the user wanted to do
			} else {
				// failure:  just stop here
				_previousState = STATE_NORMAL;
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, "Failed to save user state."));
				actOnState();
			}
        }
		
		private function saveCompletionHandler(event:ApplicationEvent):void
		{
            Application.instance.document.project.published = false;
            var error:String = event.data as String;
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_NAME_CHANGED));
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_SAVED, Application.instance.document.documentId));
            Application.instance.projectFromOtherUser = false;
			authorController.log("savedProject name=\""+Application.instance.document.name+"\" id="+Application.instance.document.documentId);

            PseudoThread.add(new PseudoThreadSimpleRunnable("ProjectController::saveCompletionHandler", function():void{
                Application.instance.document.upToDate = true;
                Application.instance.defaultUserMessage = ApplicationStrings.DEFAULT_STATUS_MESSAGE;
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE,
                        (error == null) ? "Saved " + Utilities.prettyDate : "Error saving: "+error));
                saveComplete(true);
                _doingProjectSave = false;
                isTemplateDialogSave = false;
            }));
		}

        override protected function handleNameInUse(name:String, templateSave:Boolean):Function
  		{
  			return function(event:GadgetEvent):void
  			{
                  ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:42, closeFunction:handleSaveOver(name, event.id, isTemplateDialogSave), inserts:[name]}));
                  isTemplateDialogSave = false;
  			}
  		}
		
		override protected function handleSaveOver(name:String, id:String, templateSave:Boolean):Function
		{
			return function(event:CloseEvent):void {
				trace("Alert Switch");
                switch (event.detail) {
					case Alert.FIRST:
                        trace("Alert First");
						saveCloudProject(name, id, false, templateSave, true);
						break;
					case Alert.SECOND:
                        trace("Alert Second");
                        if(templateSave) {
                            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.OPEN_TEMPLATE_DIALOG));
                        } else {
                            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.OPEN_SETTINGS_DIALOG));
                        }
						break;
					default:
                        trace("Alert Default");
						break;
				}
				actOnState(); 
			};
		}
		
		private function saveComplete(success:Boolean):void
		{
			if (success) {
				actOnState();  // go ahead and quit or load, whatever the user wanted to do
			} else {
				// failure:  just stop here
				_previousState = STATE_NORMAL;
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SET_USER_MESSAGE, "Operation failed."));
				actOnState();
			}
			autosaveReset();
            if(_tokenForSaving) {
                ApplicationController.instance.enableInput(_tokenForSaving);
                _tokenForSaving = null;
            }
		}
		
		override protected function handleSaveFault(event:Event):void
		{
			super.handleSaveFault(event);
			saveComplete(false);  // success = false
		}
		
		private function errorOnInvalidName():void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:34}));
		}
		
		private function errorOnNameLength():void
		{
			Alert.okLabel = "OK";
			Alert.buttonWidth = 90;
			Alert.show("Your work cannot be saved unless you shorten your project name.",
				"Shorten project name, please.", Alert.FIRST, null, null, Alert.FIRST);
		}
		
        private function errorOnName():void
  		{
  			Alert.okLabel = "OK";
  			Alert.buttonWidth = 90;
  			Alert.show("Valid Project Names must be between 1 and 254 characters in length",
  				"Invalid Project Name", Alert.FIRST, null, null, Alert.FIRST);
  		}

		private function warnAboutNoProjectChangePermission():void
		{
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:35}));
		}
		
		private function warnAboutNoSavePermission():void
		{
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:36}));
		}

		private function alertSave(type:String = "new"):void
		{
			const document:Document = Application.instance.document;
			_newWindowType = type;

            if(authorController.editing && !(authorController.currentEditingMediator is EventPageEditingMediator || authorController.currentEditingMediator is FlowEditingMediator)){
                authorController.closeEditors();
                document.upToDate = false;
            }

			// have changes been made since last save?
			if (!document || (document && document.upToDate)) {
				// push closing state
				_previousState = _appState;
				_appState = STATE_CLOSING;
				
				// no changes, so close project and act on the current state: close, quit, etc
				actOnState();
			} else {
                //TODO: NEW USER MESSAGE SHOULD GO HERE.  37 and 331
				// Save changes?  yes/no/cancel
				Alert.cancelLabel = "Cancel";
				Alert.okLabel = "New Editor";
                if(!(Application.instance.customerType == "CREATOR" && (Application.instance.document.project.accountType == "PRO" || Application.instance.document.project.accountType == "ENTERPRISE"))){
                    Alert.noLabel = "Don't Save";
                    Alert.yesLabel = "Save Changes";
                }
				Alert.buttonWidth = 90;
				
				var flags:uint = Alert.FIRST | Alert.THIRD | Alert.FOURTH | Alert.SECOND;
                if(!(Application.instance.customerType == "CREATOR" && (Application.instance.document.project.accountType == "PRO" || Application.instance.document.project.accountType == "ENTERPRISE"))){
                    var text:String = "You have unsaved changes. Would you like to save changes to "+document.name+"?";
                }else{
                    var text:String = "would you like to open a new Editor window to keep this project open?";
                }
                if (_appState == STATE_OPENING || _appState == STATE_OPENING_CLOUD || _appState == STATE_NEWDOC || _appState == STATE_INVOKING) {
                    if(!(Application.instance.customerType == "CREATOR" && (Application.instance.document.project.accountType == "PRO" || Application.instance.document.project.accountType == "ENTERPRISE"))){
                    	flags = Alert.THIRD | Alert.FOURTH | Alert.FIRST | Alert.SECOND;
                        text += " Or would you like to open a new Editor window to keep this project open?";
                    }else{
                        flags = Alert.FIRST | Alert.SECOND;
                    }
				}
				Alert.show(text, "Just a moment...", flags, handleCloseAlertSelection, null, Alert.THIRD);
			}
		}
		
		private function handleCloseAlertSelection(event:CloseEvent):void
		{
			switch(event.detail) {
				case Alert.SECOND: // If we are creating new or opening, open in a new Editor
					TaconiteFactory.getWindowImplementation().requestNewWindowAndInvokeId(_newWindowType);
					_appState = STATE_NORMAL;
					_previousState = STATE_NORMAL;
					break;
				case Alert.THIRD: // Yes: Save.
					_previousState = _appState;
					_appState = STATE_CLOSING;
					saveProject();
					break;
				case Alert.FOURTH: // No: Do not save.
					_previousState = _appState;
					_appState = STATE_CLOSING;
					actOnState();
					break;
				default: // Cancel
					_appState = STATE_NORMAL;
					_previousState = STATE_NORMAL;
					break;
			}
		}

		private function showAutosaveLoadedAlert():void
		{
			const document:Document = Application.instance.document;

//			Alert.cancelLabel = "Project History";
//			Alert.okLabel = "Last Manual Save";
//			Alert.noLabel = "Loaded Version";
//			Alert.buttonWidth = 90;
			
			var date:Number = document.project.createdDateTime;
			var df:DateFormatter = new DateFormatter();
			df.formatString = "EEEEEEE MMM D L:NNA";
			var dateStr:String = df.format(new Date(date));
//			var text:String =
//				"The project version just loaded was auto-saved on " + dateStr + "."
//				+ "  You can switch to the last manually-saved version or select any version in the project history.";
//			var flags:uint =  Alert.NO | Alert.OK | Alert.CANCEL;
//
//			Alert.show(text, "Would you prefer a different version?", flags, handleAutosaveAlertSelection, null, Alert.NO);

            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.DISPLAY_MESSAGE, {id:337, closeFunction:handleAutosaveAlertSelection, inserts:[dateStr]}));
		}
		
		private function handleAutosaveAlertSelection(event:CloseEvent):void
		{
			switch(event.detail) {
				case Alert.FIRST: // History
					showHistory();
					break;
				case Alert.SECOND: // Saved version
					loadLastManuallySavedVersion();
					break;
				default: // No:  stay with autosave version
			}
		}
		
		private function handleMissingProjectState():void
		{
			var env:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			if (env.isAdmin && env.invokedAsEditor && !TestPanelController.testRunning) {
				Alert.okLabel = "OK"
				Alert.show("Failed to load project state.", "Error loading", Alert.FIRST);
				authorController.log("   Missing gadgetState: projName=\""+Application.instance.document.name+"\"");
			}
			logError("MISSING PROJECT STATE proj="+Application.instance.document.name);
		}
		
		private function logError(str:String):void
		{
			if (TestPanelController.testRunning) {
				TestPanelController.instance.logSummary("ERROR LOADING PROJ: " + str);  // don't know the project name at this point
			}
			LogService.error(str);
		}
		
		private function showHistory():void
		{
			ApplicationController.instance.createPopUp(HistoryDialog, true);
           //MESSAGE NOT IN DOCUMENT
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, ["Loading history"]));
		}
		
		private function loadLastManuallySavedVersion():void
		{
			// load most recent manually saved version
			const document:Document = Application.instance.document;
			if (document.documentId != null) {
				loadGadgetHistory(document.documentId, manualCallback);
                //MESSAGE NOT IN DOCUMENT
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, ["Loading other version"]));
			}
		}
		
		private function manualCallback(list:Array):void
		{
			for (var n:int=0; n < list.length; n++) {
				var item:Object = list[n];
				if (item.autosave == false) {
					var id:String = item.id;
					ProjectLoadingSequencer.instance.requestLoad(id,false,false, true);  // loadVersion=true
					break;
				}
			}
		}
		
		public function get projectDump():String
		{
            try {
    			const document:Document = Application.instance.document;
    			var dumpList:Array = [];
    			dumpList.push(new GadgetEncoder().encode(document.project, document.root.value as AbstractContainer));
    			
    			var models:Vector.<TaconiteModel>;
    			for each (var gadget:Project in model.gadgets) {
    				models = ApplicationController.instance.authorController.getObjectModelsForReplaceable(gadget, true);
    				if (models.length > 0)
    					dumpList.push(new GadgetEncoder().encode(gadget, models[0].value as AbstractContainer));
    			}
    			
    			var abstractObject:AbstractObject = document.root.value as AbstractObject;
    			if (abstractObject) {
    				var state:GadgetState = GadgetEncoder.getState(abstractObject, GadgetEncoder.SERIALIZED_LOCATION_EXISTING);
    				state.running = document.running;
    				state.locked = document.locked;
    				state.scripts = (document.project.scripts == null) ? null : document.project.scripts.toJson();
    				
    				var stateObject:Object = {rootGadget:document.project.id, content:state, authorMode:true};
    			}
    			
    			var dump:Object = {atoms:dumpList, state:stateObject};
    			return new JSONEncoder(dump).getString();
            } catch(e:Error) {
                LogService.error("Error dumping project contents: " + e);
            }
            return null;
		}
		
		//called when saving to cloud and provides specific feedback on fileName problems
		public function validateFileName(fileName:String):Boolean
		{	
			if (fileName == null || fileName.length == 0) {
				errorOnInvalidName(); // this should never happen, here as a last-minute safeguard
				return false;
			} 
			else if (fileName.length > 254){
				errorOnNameLength();
				return false;
			}
            else {
            
                var str:String = fileName.toLocaleLowerCase();
                
                while(str.indexOf(' ')>-1)
                    str = str.replace(' ', '');

                if (str == "untitledproject") {
                    errorOnInvalidName();
                    return false;
                }
			    return true;
            }
		}

        public function get doingProjectSave():Boolean
        {
            return _doingProjectSave;
        }

	}
}
