/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.controller.app
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.Pager;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.objects.IEditingMediator;
import com.alleni.author.controller.objects.IUndoMediator;
import com.alleni.author.controller.text.TextEditMediator;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.controller.ui.palettes.FindWindowController;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.definition.action.ActionTreeEvent;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.EventFlowController;
import com.alleni.author.model.ui.Application;
import com.alleni.author.service.users.UserService;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.view.ui.controls.Alert;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.SelectEvent;
import com.alleni.taconite.factory.IMenu;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.service.LogService;

import flash.events.Event;
import flash.events.MouseEvent;

import mx.collections.XMLListCollection;
import mx.events.CloseEvent;
import mx.events.PropertyChangeEvent;

/**
	 * Application specific menu bar.
	 */
	public class MenuController
	{	
		private static var _instance:MenuController = null;
		
		private var _menuBarDefinition:XMLList;
		private var _menuBarProvider:XMLListCollection;
		private var _menu:IMenu;
		private var _actionTree:ActionTree = null;
        private var _enabled:Boolean = true;
        public var savedThisSession:Boolean = false;
		
        private static const RUN_PROJECT:String = "Run Project";
		private static const PAUSE_RESUME_PROJECT:String = "Pause/Resume Project";
		private static const RESET_PROJECT:String = "Reset Project";
        private static const TOGGLE_RUN_LOCK:String = "Toggle Run Lock";
		private static const MENU_ITEM_PRO_BRAND:String = "showProBrand";

        private static const SHOW_LMS:String = "showLMS";
        private static const EVENT_FLOW_TOGGLE:String = "event flow toggle";

		public function MenuController()
		{
			if (_instance != null) throw new Error("Singleton, do not instantiate");
			
			initializeMenuItems();
			// set up menu from abstract factory.
			// this allows for specific desktop- or web-specific menus to be implemented in the subapp, 
			// but this should only be done after the dataProvider has been assigned.
			_menu = TaconiteFactory.getMenuImplementation();
			_menu.dataProvider = _menuBarProvider;
			_menu.labelField = "@label";
			_menu.keyEquivalentField = "@keyEquivalent";
			_menu.callback = handleCommand;

			setItemEnabled("arrange", NotificationNamesEditor.MAKE_OBJECT_CLOSE, false);
            setItemEnabled("arrange", NotificationNamesEditor.MAKE_OBJECT_OPEN, false);
			setItemEnabled("arrange", NotificationNamesEditor.UNMAKE_OBJECT, false);
			setItemEnabled("arrange", NotificationNamesEditor.MAKE_TOGGLEBUTTON_GROUP, false);
			setItemEnabled("arrange", NotificationNamesEditor.UNMAKE_TOGGLEBUTTON_GROUP, false);
			setItemEnabled("project", NotificationNamesEditor.MAKE_GRAPHIC, false);
			setItemEnabled("arrange", NotificationNamesEditor.MAKE_ARENA, false);
            setItemEnabled("arrange", NotificationNamesEditor.EDIT_SELECTED_OBJECT, false);
            setItemEnabled("arrange", NotificationNamesEditor.CLOSE_ONE_EDITOR, false);
            setItemEnabled("arrange", NotificationNamesEditor.UNMAKE_OBJECT, false);

			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_INITIALIZED, handleProjectInitialized);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_CLOSING, handleProjectClosing);
			ApplicationController.addEventListener(NotificationNamesApplication.SHOW_ALL_TOGGLED, handleShowAllToggled);
            ApplicationController.addEventListener(NotificationNamesApplication.RUN_LOCK_TOGGLED, handleRunLockToggle);
            ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_SETTINGS_CHANGED, handleProjectSettingsChange);
            ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_SAVED, updateSavedBoolean);

            UserService.instance.addEventListener(NotificationNamesApplication.UPDATE_CUSTOMER_TYPE, handleCustomerTypeChange);

			Application.instance.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleApplicationPropertyChange);
		}

        private function updateSavedBoolean(event:ApplicationEvent):void{
            savedThisSession = true;
            syncTopMenuBarItems();
        }

		private function handleProjectInitialized(event:ApplicationEvent = null):void
		{
			addDocListeners();
			syncTopMenuBarItems();
		}
		
		private function handleProjectClosing(event:ApplicationEvent = null):void
		{
			removeDocListeners();
		}
		
		private function handleShowAllToggled(event:ApplicationEvent = null):void
		{
			getMenuItem("view", NotificationNamesApplication.SHOW_ALL_TOGGLED).@toggled = Application.instance.showAll;
		}

        private function handleRunLockToggle(event:ApplicationEvent):void{
            getMenuItem("run", TOGGLE_RUN_LOCK).@toggled = Application.locked;
        }

		public static function get instance():MenuController
		{
			if (_instance == null)
				_instance = new MenuController();
			return _instance;
		}

        public function set enabled(value:Boolean):void
        {
            // mouseCapture / disableInput() sets this false.  We avoid updating menu items while enabled==false
            _menu.enabled = value;
            _enabled = value;

            if (value) {
                updateMenuItems();
                syncTopMenuBarItems();
            }
        }


		protected function initializeMenuItems():void
		{
			var aboutText:String = "About " + TaconiteFactory.getAppDescriptorImplementation().name;

            _menuBarDefinition =
                    <>
                        <menuitem id="app" label={TaconiteFactory.getAppDescriptorImplementation().name} >
                            <menuitem id={NotificationNamesApplication.SHOW_ABOUT_WINDOW} label="About ZebraZapps" />
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesApplication.REQUEST_QUIT_APPLICATION} label="Quit Editor" keyEquivalent="q"/>
                        </menuitem>
                        <menuitem id="project" label="Project">
                            <menuitem id={NotificationNamesApplication.CREATE_NEW_PROJECT} label="New" keyEquivalent="n"/>
                            <menuitem id={NotificationNamesApplication.OPEN_PROJECT} label="Open…" keyEquivalent="o"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesApplication.SAVE_PROJECT} label="Save" keyEquivalent="s"/>
                            <menuitem id={NotificationNamesApplication.SAVE_AS_PROJECT} label="Save As..." keyEquivalent="s" shiftKey="true"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesApplication.OPEN_SETTINGS_DIALOG} label="Project Settings…"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesApplication.PREP_TO_SHARE_PROJECTS} label="Share Projects"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesApplication.IMPORT_ASSET} label="Import Asset..." keyEquivalent="i"/>
                            <menuitem id={NotificationNamesEditor.MAKE_GRAPHIC} label="Make Graphic Asset" />
                        </menuitem>
                        <menuitem id="edit" label="Edit">
                            <menuitem id={NotificationNamesEditor.UNDO} label="Undo" keyEquivalent="z" />
                            <menuitem id={NotificationNamesEditor.REDO} label="Redo" keyEquivalent="y" />
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.CUT} label="Cut" keyEquivalent="x"/>
                            <menuitem id={NotificationNamesEditor.COPY} label="Copy" keyEquivalent="c"/>
                            <menuitem id="paste" label="Paste" keyEquivalent="v"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.DELETE_SELECTED} label="Delete" keyEquivalent="delete" />
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.SELECT_ALL} label="Select All" keyEquivalent="a" />
                            <menuitem id={NotificationNamesEditor.SELECT_ALL_WITH_LOCKED} label="Select All + Locked" keyEquivalent="a" shiftKey="true"/>
                            <menuitem id={NotificationNamesEditor.DESELECT} label="Deselect" keyEquivalent="space" />
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.NUDGE_LEFT} label="Nudge- Left" keyEquivalent="←"/>
                            <menuitem id={NotificationNamesEditor.NUDGE_RIGHT} label="Nudge- Right" keyEquivalent="→"/>
                            <menuitem id={NotificationNamesEditor.NUDGE_UP} label="Nudge- Up" keyEquivalent="↑"/>
                            <menuitem id={NotificationNamesEditor.NUDGE_DOWN} label="Nudge- Down" keyEquivalent="↓"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.NUDGE_TEN_LEFT} label="Nudge 10- Left" keyEquivalent="←" shiftKey="true"/>
                            <menuitem id={NotificationNamesEditor.NUDGE_TEN_RIGHT} label="Nudge 10- Right" keyEquivalent="→" shiftKey="true"/>
                            <menuitem id={NotificationNamesEditor.NUDGE_TEN_UP} label="Nudge 10- Up" keyEquivalent="↑" shiftKey="true"/>
                            <menuitem id={NotificationNamesEditor.NUDGE_TEN_DOWN} label="Nudge 10- Down" keyEquivalent="↓" shiftKey="true"/>
                        </menuitem>
                        <menuitem id="arrange" label="Arrange">
                            <menuitem id={NotificationNamesEditor.BRING_TO_FRONT} label="Bring to Front"  keyEquivalent="f" shiftKey="true"/>
                            <menuitem id={NotificationNamesEditor.BRING_FORWARD} label="Bring Forward"  keyEquivalent="f"/>
                            <menuitem id={NotificationNamesEditor.SEND_BACK} label="Send Backward"  keyEquivalent="b" />
                            <menuitem id={NotificationNamesEditor.SEND_TO_BACK} label="Send to Back" keyEquivalent="b" shiftKey="true"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.GROUP_ITEMS} label="Group" keyEquivalent="g" />
                            <menuitem id={NotificationNamesEditor.UNGROUP_ITEMS} label="Ungroup"  keyEquivalent="g" shiftKey="true" />
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.MAKE_OBJECT_OPEN} label="Make Gadget- Opened" keyEquivalent="m" />
                            <menuitem id={NotificationNamesEditor.MAKE_OBJECT_CLOSE} label="Make Gadget- Closed" keyEquivalent="m" shiftKey="true" />
                            <menuitem id={NotificationNamesEditor.EDIT_SELECTED_OBJECT} label="Edit..." keyEquivalent="e"/>
                            <menuitem id={NotificationNamesEditor.CLOSE_ONE_EDITOR} label="Close..." keyEquivalent="enter"/>
                            <menuitem id={NotificationNamesEditor.UNMAKE_OBJECT} label="Unmake Gadget"  keyEquivalent="u" />
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.MAKE_TOGGLEBUTTON_GROUP} label="Make Button Set" keyEquivalent="k" />
                            <menuitem id={NotificationNamesEditor.UNMAKE_TOGGLEBUTTON_GROUP} label="Unmake Button Set" keyEquivalent="k" shiftKey="true"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.MAKE_ARENA} label="Make Arena" keyEquivalent="@"/>
                        </menuitem>
                        <menuitem id="align" label="Align">
                            <menuitem id={NotificationNamesEditor.ALIGN_EDGES_LEFT} label="Align Edges- Left" keyEquivalent="["/>
                            <menuitem id={NotificationNamesEditor.ALIGN_EDGES_RIGHT} label="Align Edges- Right" keyEquivalent="]"/>
                            <menuitem id={NotificationNamesEditor.ALIGN_EDGES_TOP} label="Align Edges- Top" keyEquivalent="{" shiftKey="true"/>
                            <menuitem id={NotificationNamesEditor.ALIGN_EDGES_BOTTOM} label="Align Edges- Bottom" keyEquivalent="}" shiftKey="true"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.ALIGN_CENTERS_HORIZONTAL} label="Align Centers- Horizontal" keyEquivalent="\\"/>
                            <menuitem id={NotificationNamesEditor.ALIGN_CENTERS_VERTICAL} label="Align Centers- Vertical" keyEquivalent="|" shiftKey="true"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesEditor.DISTRIBUTE_HORIZONTAL} label="Distribute Center- Horizontal" keyEquivalent="'"/>
                            <menuitem id={NotificationNamesEditor.DISTRIBUTE_VERTICAL} label="Distribute Center- Vertical" keyEquivalent='"' shiftKey="true"/>
                        </menuitem>
                        <menuitem id="run" label="Run">
                            <menuitem id={EVENT_FLOW_TOGGLE} label="Go to Event Flow/Stage" keyEquivalent="d"/>
                            <menuitem type="separator"/>
                            <menuitem id={RESET_PROJECT} label="Edit- Reset" keyEquivalent="r" shiftKey="true"/>
                            <menuitem id={RUN_PROJECT} label="Run- Restart" keyEquivalent="r" />
                            <menuitem id={PAUSE_RESUME_PROJECT} label="Pause/Resume" keyEquivalent="j" />
                            <menuitem type="separator"/>
                            <menuitem id={TOGGLE_RUN_LOCK} type="check" label="Run- Lock" keyEquivalent="l"/>
                        </menuitem>
                        <menuitem id="view" label="View">
                            <menuitem id={NotificationNamesApplication.ZOOM_IN_NO_ADJUST} label="Zoom In" keyEquivalent="="/>
                            <menuitem id={NotificationNamesApplication.ZOOM_OUT_NO_ADJUST} label="Zoom Out" keyEquivalent="-"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesApplication.ACTUAL_SIZE} label="View Actual Size" keyEquivalent="1" />
                            <menuitem id={NotificationNamesApplication.FIT_IN_WINDOW} label="Fit Stage to Window" keyEquivalent="2"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesApplication.MORE_WIRES} label="Show More Wires" keyEquivalent="w" />
                            <menuitem id={NotificationNamesApplication.FEWER_WIRES} label="Show Fewer Wires" keyEquivalent="w" shiftKey="true" />
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesApplication.SHOW_ALL_TOGGLED} label="Show All" type="check" keyEquivalent="t"/>
                        </menuitem>
                        <menuitem id="window" label="Window">
                            <menuitem id={NotificationNamesApplication.TOGGLE_TOOL_BOX} type="check" label="Toolbox"/>
                            <menuitem id={NotificationNamesApplication.TOGGLE_LIBRARY} type="check" label="Resource Library"/>
                            <menuitem id={NotificationNamesApplication.TOGGLE_VARIABLE_LIBRARY} type="check" label="Variables and Functions"/>
                            <menuitem id={NotificationNamesApplication.TOGGLE_INSPECTOR} type="check" label="Inspector"/>
                            <menuitem id={NotificationNamesApplication.TOGGLE_RIBBON_LIBRARY} type="check" label="Ribbon Library"/>
                            <menuitem id={NotificationNamesApplication.TOGGLE_DOCK} type="check" label="Cloud"/>
                        </menuitem>
                        <menuitem id="publish" label="Publish">
                            <menuitem id={NotificationNamesApplication.OPEN_PUBLISHING_DIALOG} label="Zap Project..."/>
                            <menuitem id={NotificationNamesApplication.UPDATE_PRIVACY_LEVEL} label="Set Privacy Level..."/>
                            <menuitem type="separator"/>
                            <menuitem id={SHOW_LMS} type="check" label="Show LMS Options"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_SHOPP} label="Share App to Shopp..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_LINK} label="- to direct link..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_EMBED} label="- to embed..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_EMAIL_LIST} label="- to email list..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_GUEST_LIST} label="- to guest list..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_LMS} label="- to LMS package..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_FACEBOOK} label="- to Facebook..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_TWITTER} label="- to Twitter..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_LINKED_IN} label="- to Linked In..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_ARTICULATE} label="- to Articulate..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_LECTORA} label="- to Lectora..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_CAPTIVATE} label="- to Captivate..."/>
                            <menuitem id={NotificationNamesApplication.SHARE_TO_POWERPOINT} label="- to Power Point..."/>
                        </menuitem>
                        <menuitem id="help" label="Help">
                            <menuitem id={NotificationNamesApplication.OPEN_WIKI} label="ZebraZapps Help"/>
                            <menuitem type="separator"/>
                            <menuitem id={NotificationNamesApplication.TOGGLE_QA_WINDOW} type="check" label="Provide Feedback..." />
                        </menuitem>
                    </>;

			_menuBarProvider = new XMLListCollection(_menuBarDefinition);
		}
		
		private function addAdminMenu():void
		{
			var adminMenu:XMLList =
				<>
					<menuitem id="admin" label="Admin">
						<menuitem id={NotificationNamesApplication.TOGGLE_LOG_WINDOW} type="check" label="Log panel"/>
						<menuitem id={NotificationNamesApplication.TOGGLE_TEST_WINDOW} type="check" label="Test panel"/>
						<menuitem id={NotificationNamesApplication.TOGGLE_OUTLINE_VIEW} type="check" label="Outline view"/>
                        <menuitem id={NotificationNamesApplication.ADMIN_TELEPORT} label="Teleport to other server/user"/>
                        <menuitem id={NotificationNamesApplication.ADMIN_LOAD_PROJ_BY_ID} label="Load project by ID"/>
						<menuitem type="separator"/>
						<menuitem id={NotificationNamesApplication.ADMIN_FIND_TITLE} label="Find object by title"/>
						<menuitem id={NotificationNamesApplication.ADMIN_FIND_RIBBON} label="Find by ribbon value"/>
						<menuitem id={NotificationNamesApplication.ADMIN_FIND_UID} label="Find object by ID"/>
						<menuitem id={NotificationNamesApplication.ADMIN_COPY_UID} label="Copy ID of selected object"/>
						<menuitem type="separator"/>
						<menuitem id={NotificationNamesApplication.ADMIN_COPY_JSON} label="Copy project JSON"/>
						<menuitem id={NotificationNamesApplication.ADMIN_COPY_DUMP} label="Copy project Dump"/>
                        <menuitem type="separator"/>
                        <menuitem id={NotificationNamesApplication.ADMIN_LOG_CLEAR} label="Clear log"/>
                        <menuitem id={NotificationNamesApplication.ADMIN_LOG_SELECTION} label="Log selected objects"/>
                        <menuitem id={NotificationNamesApplication.ADMIN_LOG_OBJECTS} label="Log all objects"/>
                        <menuitem id={NotificationNamesApplication.ADMIN_LOG_WIRES} label="Log wires"/>
                        <menuitem id="DumpLoadedEvents" label="Log Loaded Events"/>
                        <menuitem id={NotificationNamesApplication.ADMIN_COPY_LOG} label="Copy log"/>
                        <menuitem type="separator"/>
                        <menuitem id={"showCache"} label="Show Event-flow cache"/>
                        <menuitem id="EventsInLibrary" type="check" label="Show Events in library"/>
                        <menuitem id="EventValidation" type="check" label="Event validation"/>
                        <menuitem id="PseudoThreadSlow" type="check" label="Slow threading"/>
					</menuitem>
			</>;
			
			_menuBarDefinition = _menuBarDefinition + adminMenu;
			
			_menuBarProvider.source = _menuBarDefinition;

            _menu.menu.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownToEnabledInput);
		}

        private function mouseDownToEnabledInput(e:MouseEvent):void
        {
            if (_enabled == false && e.altKey) {
                ApplicationController.instance.dumpDisableInputTokens();
                ApplicationController.instance.forceEnableInput();
            }
        }


		private function updateMenuItems():void
		{
            if (_enabled) {
                // Sync to model
                setItemToggled("window", NotificationNamesApplication.TOGGLE_TOOL_BOX, Application.instance.toolboxVisible);
                setItemToggled("window", NotificationNamesApplication.TOGGLE_LIBRARY, Application.instance.libraryVisible);
                setItemToggled("window", NotificationNamesApplication.TOGGLE_VARIABLE_LIBRARY, Application.instance.variableLibraryVisible);
                setItemToggled("window", NotificationNamesApplication.TOGGLE_INSPECTOR, Application.instance.inspectorVisible);
                setItemToggled("window", NotificationNamesApplication.TOGGLE_DOCK, Application.instance.dockVisible);
                setItemToggled("window", NotificationNamesApplication.TOGGLE_RIBBON_LIBRARY, Application.instance.ribbonLibraryVisible);
                setItemToggled("window", NotificationNamesApplication.TOGGLE_VARIABLE_LIBRARY, Application.instance.variableLibraryVisible);
            }
		}
		
		private function handleUndoChange(event:ActionTreeEvent):void
		{
			syncTopMenuBarItems();
		}

        private function handleCustomerTypeChange(event:ApplicationEvent):void {
            syncTopMenuBarItems();
        }

        private function handleProjectSettingsChange(event:ApplicationEvent):void {
            syncTopMenuBarItems();
        }

        private function enableAll():void{
            for each(var menuitem:XML in _menuBarDefinition){
                var menu:String = menuitem.attribute("id");
                for each(var item:XML in menuitem.children()){
                    setItemEnabled(menu, item.attribute("id"), true);
                }
            }
        }

        private function disableMenuItem(id:String):void{
             for each(var menuitem:XML in _menuBarDefinition){
                var menu:String = menuitem.attribute("id");
                for each(var item:XML in menuitem.children()){
                    if(item.attribute("id") == id){
                        setItemEnabled(menu, item.attribute("id"), false);
                    }
                }
            }
        }

        private function enableMenuItem(id:String):void{
            for each(var menuitem:XML in _menuBarDefinition){
                var menu:String = menuitem.attribute("id");
                for each(var item:XML in menuitem.children()){
                    if(item.attribute("id") == id){
                        setItemEnabled(menu, item.attribute("id"), true);
                    }
                }
            }
        }

        public function syncTopMenuBarItems():void{

            if (_enabled) {
                enableAll();
                var masterList:Array = [];
                masterList = masterList.concat(checkSelection());
                masterList = masterList.concat(updateUndoItems());
                masterList = masterList.concat(checkEditing());
                masterList = masterList.concat(checkClipboard());
                masterList = masterList.concat(checkPublished());
                masterList = masterList.concat(checkPermissions());
                masterList = masterList.concat(checkRunningState());
                masterList = masterList.concat(checkProjectAccountType());
                for each(var item:String in masterList){
                    disableMenuItem(item);
                }
                setItemToggled("publish", SHOW_LMS, Application.instance.lmsEnabled);
            }
        }

        // Check Status of the application and Sync the menu bar
        public function checkProjectAccountType():Array{
            var disableList:Array = [];
            // this trace crashes sometimes:
//            trace(Application.instance.customerType,"-", Application.instance.document.project.accountType);
            if(Application.instance.customerType == "CREATOR" && Application.instance.document && (Application.instance.document.project.accountType == "PRO" || Application.instance.document.project.accountType == "ENTERPRISE")){
                 disableList.push(NotificationNamesApplication.OPEN_PUBLISHING_DIALOG,
                         NotificationNamesApplication.UPDATE_PRIVACY_LEVEL,
                         NotificationNamesApplication.SHARE_TO_SHOPP,
                         NotificationNamesApplication.SHARE_TO_LINK,
                         NotificationNamesApplication.SHARE_TO_EMBED,
                         NotificationNamesApplication.SHARE_TO_EMAIL_LIST,
                         NotificationNamesApplication.SHARE_TO_GUEST_LIST,
                         NotificationNamesApplication.SHARE_TO_LMS,
                         NotificationNamesApplication.SHARE_TO_FACEBOOK,
                         NotificationNamesApplication.SHARE_TO_LINKED_IN,
                         NotificationNamesApplication.SHARE_TO_TWITTER,
                         NotificationNamesApplication.SHARE_TO_ARTICULATE,
                         NotificationNamesApplication.SHARE_TO_LECTORA,
                         NotificationNamesApplication.SHARE_TO_CAPTIVATE,
                         NotificationNamesApplication.SHARE_TO_POWERPOINT,
                         NotificationNamesEditor.MAKE_OBJECT_OPEN,
                         NotificationNamesEditor.MAKE_OBJECT_CLOSE,
                         NotificationNamesEditor.EDIT_SELECTED_OBJECT,
                         NotificationNamesEditor.CLOSE_ONE_EDITOR,
                         NotificationNamesEditor.UNMAKE_OBJECT,
                         NotificationNamesEditor.CUT,
                         NotificationNamesEditor.COPY,
                         "paste",
                         NotificationNamesApplication.SAVE_PROJECT,
                         NotificationNamesApplication.SAVE_AS_PROJECT,
                         NotificationNamesApplication.OPEN_SETTINGS_DIALOG,
                         "showLMS"
                         );
            }

            return disableList;
        }

        public function checkPermissions():Array{
            var disableList:Array = [];

            if(Application.instance.customerType.toLowerCase() == UserService.USER_CREATOR){
                if(Application.instance.document && Application.instance.document.project.permissions.canShare == false){
                    disableList.push(NotificationNamesApplication.SHARE_TO_FACEBOOK,
                        NotificationNamesApplication.SHARE_TO_LINKED_IN,
                        NotificationNamesApplication.SHARE_TO_TWITTER,
                        NotificationNamesApplication.SHARE_TO_EMBED);
                }
            }
            if(Application.instance.document && Application.instance.document.project.accountTypeId < 3) {
                disableList.push(SHOW_LMS, NotificationNamesApplication.SHARE_TO_LMS,
                        NotificationNamesApplication.SHARE_TO_GUEST_LIST,
                        EVENT_FLOW_TOGGLE);
            }

            return disableList;
        }

        public function checkClipboard():Array{
            var disableList:Array = [];
            if(ClipboardController.instance.pasteCount == 0){
                //disableList = ["paste"];
            }
            return disableList;
        }

        public function checkPublished():Array{
            var disableList:Array = [];

            disableList.push(NotificationNamesApplication.SHARE_TO_ARTICULATE,
                        NotificationNamesApplication.SHARE_TO_LECTORA,
                        NotificationNamesApplication.SHARE_TO_CAPTIVATE,
                        NotificationNamesApplication.SHARE_TO_POWERPOINT,
                        NotificationNamesApplication.SHARE_TO_LINKED_IN);

            if((savedThisSession)||(Application.instance.document == null)||(Application.instance.document.upToDate == false) || (Application.instance.document.project.published == false)){
                disableList.push(NotificationNamesApplication.UPDATE_PRIVACY_LEVEL);
            }

            return disableList;
        }

        public function checkSelection():Array{
            var models:Vector.<TaconiteModel> = ApplicationController.instance.authorController.document == null ? new Vector.<TaconiteModel>() : ApplicationController.instance.authorController.cleanSelection;
            var textEditMediator:TextEditMediator = ApplicationController.instance.authorController.currentEditingMediator as TextEditMediator;
            var disableList:Array = [];
            if(models.length == 0 || Application.uiRunning){
                disableList = [NotificationNamesEditor.MAKE_GRAPHIC,   // nudge & deselect are listed below
                        NotificationNamesApplication.PUBLISH_GADGET,
                        NotificationNamesEditor.DELETE_SELECTED,
                        NotificationNamesEditor.BRING_TO_FRONT,
						NotificationNamesEditor.BRING_FORWARD,
						NotificationNamesEditor.SEND_BACK,
						NotificationNamesEditor.SEND_TO_BACK,
						NotificationNamesEditor.ALIGN_EDGES_LEFT,
						NotificationNamesEditor.ALIGN_EDGES_RIGHT,
						NotificationNamesEditor.ALIGN_EDGES_TOP,
						NotificationNamesEditor.ALIGN_EDGES_BOTTOM,
						NotificationNamesEditor.ALIGN_CENTERS_HORIZONTAL,
						NotificationNamesEditor.ALIGN_CENTERS_VERTICAL,
						NotificationNamesEditor.DISTRIBUTE_HORIZONTAL,
                        NotificationNamesEditor.DISTRIBUTE_VERTICAL,
                        NotificationNamesEditor.UNMAKE_OBJECT,
                        NotificationNamesEditor.GROUP_ITEMS,
                        NotificationNamesEditor.UNGROUP_ITEMS,
                        NotificationNamesEditor.MAKE_OBJECT_OPEN,
                        NotificationNamesEditor.MAKE_OBJECT_CLOSE,
                        NotificationNamesEditor.EDIT_SELECTED_OBJECT,
						NotificationNamesEditor.UNMAKE_OBJECT,
						NotificationNamesEditor.MAKE_TOGGLEBUTTON_GROUP,
						NotificationNamesEditor.UNMAKE_TOGGLEBUTTON_GROUP,
						NotificationNamesEditor.MAKE_ARENA
                ];
                if (textEditMediator == null) {
                    disableList.push( NotificationNamesEditor.COPY,
                                      NotificationNamesEditor.CUT);
                }
            } else if (models.length == 1) {
                var type:String = models[0].value.shortClassName;
                switch (type){
                    case "Drawing":
                        disableList = [NotificationNamesEditor.MAKE_GRAPHIC,
                                NotificationNamesEditor.UNMAKE_OBJECT,
                                NotificationNamesApplication.PUBLISH_GADGET,
                                NotificationNamesEditor.EDIT_SELECTED_OBJECT];
                        break;
                    case "Composite":
                        setItemLabel("arrange", NotificationNamesEditor.EDIT_SELECTED_OBJECT, "Edit Gadget");
                        setItemKey("arrange",  NotificationNamesEditor.EDIT_SELECTED_OBJECT, "e")
                        disableList = [NotificationNamesEditor.MAKE_GRAPHIC];
                        break;
                    case "TextObject":
                        setItemLabel("arrange", NotificationNamesEditor.EDIT_SELECTED_OBJECT, "Edit Text");
                        setItemKey("arrange",  NotificationNamesEditor.EDIT_SELECTED_OBJECT, "");
                        disableList = [NotificationNamesApplication.PUBLISH_GADGET,
                                NotificationNamesEditor.UNMAKE_OBJECT,
                                NotificationNamesEditor.MAKE_GRAPHIC];
                        break;
                    case "PathObject":
                        setItemLabel("arrange", NotificationNamesEditor.EDIT_SELECTED_OBJECT, "Edit Path");
                        setItemKey("arrange",  NotificationNamesEditor.EDIT_SELECTED_OBJECT, "e");
                        disableList = [NotificationNamesApplication.PUBLISH_GADGET,
                                NotificationNamesEditor.UNMAKE_OBJECT,
                                NotificationNamesEditor.CLOSE_ONE_EDITOR];
                        break;
                    case "Oval":
                    case "RectangleObject":
                    case "Line":
                        disableList = [NotificationNamesApplication.PUBLISH_GADGET,
                                NotificationNamesEditor.UNMAKE_OBJECT,
                                NotificationNamesEditor.EDIT_SELECTED_OBJECT];
                        break;
                    default:
                        disableList = [NotificationNamesApplication.PUBLISH_GADGET,
                                NotificationNamesEditor.UNMAKE_OBJECT,
                                NotificationNamesEditor.MAKE_GRAPHIC,
                                NotificationNamesEditor.EDIT_SELECTED_OBJECT];

                }
            } else if (models.length >= 2){
                for each(var model:TaconiteModel in models){
                    if(model.value.shortClassName != "PathObject" &&
                            model.value.shortClassName != "Oval" &&
                            model.value.shortClassName != "RectangleObject" &&
                            model.value.shortClassName != "Line"){
                        disableList = [NotificationNamesEditor.MAKE_GRAPHIC];
                        break;
                    }else{
                        disableList = [];
                    }
                }
                disableList.push(NotificationNamesApplication.PUBLISH_GADGET,
                        NotificationNamesEditor.UNMAKE_OBJECT);
            }

            if(models.length == 0) {
                disableList = disableList.concat([NotificationNamesEditor.NUDGE_LEFT,   // nudge is allowed while running
                        NotificationNamesEditor.NUDGE_RIGHT,
                        NotificationNamesEditor.NUDGE_UP,
                        NotificationNamesEditor.NUDGE_DOWN,
						NotificationNamesEditor.NUDGE_TEN_LEFT,
                        NotificationNamesEditor.NUDGE_TEN_RIGHT,
                        NotificationNamesEditor.NUDGE_TEN_UP,
                        NotificationNamesEditor.NUDGE_TEN_DOWN,
                        NotificationNamesEditor.DESELECT
                    ]);
            }

            return disableList;
        }

        public function updateUndoItems():Array
		{
			var disableList:Array = [];
            if(ApplicationController.instance.authorController.document == null) {
                return [];
            }
            var mediator:IUndoMediator = ApplicationController.instance.authorController.currentUndoMediator;
			if(mediator.canUndo() == false){
                disableList.push(NotificationNamesEditor.UNDO);
            }
			setItemLabel("edit", NotificationNamesEditor.UNDO, "Undo " + handleLabel(mediator.undoName()));

            if(mediator.canRedo() == false){
			     disableList.push(NotificationNamesEditor.REDO);
            }
			setItemLabel("edit", NotificationNamesEditor.REDO, "Redo " + handleLabel(mediator.redoName()));
            return disableList
		}

        public function checkEditing():Array{
            var mediator:IEditingMediator = ApplicationController.instance.authorController.currentNonFlowEditingMediator;
            var disableList:Array = [];
            var closeLabel:String = "Close ";
            if(mediator != null && (mediator.MENULABEL_TYPE.indexOf("Event") == -1 || mediator.MENULABEL_TYPE.indexOf("Master") == -1)){
                disableList.push(NotificationNamesEditor.EDIT_SELECTED_OBJECT);
                enableMenuItem(NotificationNamesEditor.CLOSE_ONE_EDITOR);
                closeLabel = closeLabel + mediator.MENULABEL_TYPE;
                if(mediator != null && mediator.MENULABEL_TYPE == "Text"){
                    setItemKey("arrange", NotificationNamesEditor.CLOSE_ONE_EDITOR, "");
                } else {
                    setItemKey("arrange", NotificationNamesEditor.CLOSE_ONE_EDITOR, "enter");
                }
            }else {
                disableList.push(NotificationNamesEditor.CLOSE_ONE_EDITOR);
                enableMenuItem(NotificationNamesEditor.EDIT_SELECTED_OBJECT);
            }
            setItemLabel("arrange", NotificationNamesEditor.CLOSE_ONE_EDITOR, closeLabel);
            return disableList;
        }

        public function checkRunningState():Array{
            var disableList:Array = [];
            if(Application.instance.document && Application.instance.document.running){
                disableList.push(NotificationNamesApplication.OPEN_SETTINGS_DIALOG);
            }
            return disableList;
        }
        // End Status Checking and Syncing



		public function handleSelectionChange(event:SelectEvent):void
		{
			if (World(Application.instance.document.root.value).getView() == null)
				return;
			
			var allToggleButtons:Boolean = true;
            var allVectorObjects:Boolean = true;
            var groupSelected:Boolean = false;
            var models:Vector.<TaconiteModel> = ApplicationController.instance.authorController.cleanSelection;
			syncTopMenuBarItems();
		}

		private function getMenuItem(menuName:String, itemName:String):XML
		{
			for each (var child:XML in _menuBarDefinition)
			{
				if (child.@id.toString() == menuName)
				{
					for each (var item:XML in child.menuitem)
					{
						if (item.@id.toString() == itemName)
						{
							return item;
						} 
					}
				}
			}
			return null;
		}
		
		private function setItemEnabled(menuName:String, itemName:String, enabled:Boolean):void
		{
			getMenuItem(menuName, itemName).@enabled = enabled ? "true" : "false";
		}

		private function getItemToggled(menuName:String, itemName:String):Boolean
		{
			var item:XML = getMenuItem(menuName, itemName);
			if (item) {
				if(item.@toggled == "true") {
                    return true;
                } else if(item.@toggled == "false") {
                    return false;
                }
            }
            return false;
		}

		private function setItemToggled(menuName:String, itemName:String, toggled:Boolean):void
		{
			var item:XML = getMenuItem(menuName, itemName);
			if (item)
				item.@toggled = toggled ? "true" : "false";
		}
		
		private function setItemLabel(menuName:String, itemName:String, label:String):void
		{
			getMenuItem(menuName, itemName).@label = label;
		}

        private function setItemKey(menuName:String, itemName:String, key:String):void{
            getMenuItem(menuName,  itemName).@keyEquivalent = key;
        }
		
		private function handleLabel(label:String):String{
			
			if (label.length > 50){
				return label.substr(0,49) + "...";
			}
			
			return label;
		}
		
		public function addDocListeners():void
		{
			if (Application.instance.document)
				Application.instance.document.addEventListener(SelectEvent.CHANGE_SELECTION, handleSelectionChange);
			setupUndoListener();
		}
		
		private function removeDocListeners():void
		{
			if (_actionTree)
				_actionTree.removeEventListener(ActionTreeEvent.TYPE, handleUndoChange);
			_actionTree = null;
		}
		
		public function setupUndoListener():void
		{
			// opening a gadget changes the current action tree, so listener must be changed
			if (_actionTree)
				_actionTree.removeEventListener(ActionTreeEvent.TYPE, handleUndoChange);
			_actionTree = ApplicationController.currentActionTree;
			_actionTree.addEventListener(ActionTreeEvent.TYPE, handleUndoChange);
		}

		/**
		 * Called when an Application property changes.  Update the UI accordingly.
		 */
		private function handleApplicationPropertyChange(event:PropertyChangeEvent):void
		{
			var propertyName:String = event.property as String;
			Utilities.assert(propertyName != null);
			switch (propertyName) {
				case "toolboxVisible":
					setItemToggled("window", NotificationNamesApplication.TOGGLE_TOOL_BOX, Application.instance.toolboxVisible);
					break;
				case "libraryVisible":
					setItemToggled("window", NotificationNamesApplication.TOGGLE_LIBRARY, Application.instance.libraryVisible);
					break;
				case "variableLibraryVisible":
					setItemToggled("window", NotificationNamesApplication.TOGGLE_VARIABLE_LIBRARY, Application.instance.variableLibraryVisible);
					break;
				case "inspectorVisible":
					setItemToggled("window", NotificationNamesApplication.TOGGLE_INSPECTOR, Application.instance.inspectorVisible);
					break;
				case "dockVisible":
					setItemToggled("window", NotificationNamesApplication.TOGGLE_DOCK, Application.instance.dockVisible);
					break;
				case "qaVisible":
					setItemToggled("app", NotificationNamesApplication.TOGGLE_QA_WINDOW, Application.instance.qaVisible);
					break;
				case "logVisible":
					setItemToggled("admin", NotificationNamesApplication.TOGGLE_LOG_WINDOW, Application.instance.logVisible);
					break;
				case "testPanelVisible":
					setItemToggled("admin", NotificationNamesApplication.TOGGLE_TEST_WINDOW, Application.instance.testPanelVisible);
					break;
				case "outlineVisible":
					setItemToggled("admin", NotificationNamesApplication.TOGGLE_OUTLINE_VIEW, Application.instance.outlineVisible);
					break;
				case "showAll":
					setItemToggled("view", "showAll", Application.instance.showAll);
					break;
				case "lmsEnabled":
					setItemToggled("publish", SHOW_LMS, Application.instance.lmsEnabled);
					break;
				case "isAdmin":
					if (event.newValue == true && TaconiteFactory.getEnvironmentImplementation().invokedAsEditor)
						addAdminMenu();
					break;
				default:
					// not bloody likely
					break;
//				case Application.instance.diagramWindow:
//					break;
			}
		}
		
		private function handleCommand(name:String):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(name));
			
			switch(name)
			{
				// View Menu
				case "showAll":
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SHOW_ALL_TOGGLED));
					break;
				case "cut":
					Application.instance.window.displayObject.stage.dispatchEvent(new Event(Event.CUT));
					break;
                case EVENT_FLOW_TOGGLE:
                    ApplicationController.instance.authorController.toggleFlowVisible(false);
                    break;
//				case "copy":
//					Application.instance.window.displayObject.stage.dispatchEvent(new Event(Event.COPY));
//					break;
				case "paste":
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.LOCAL_PASTE));
					break;
				case "enableUndo":
					//Application.instance.document.undoHistory.hackEnabled = getMenuItem("edit", "enableUndo").@toggled == "true";
					break;
                case SHOW_LMS:
                    ApplicationController.instance.authorController.setLmsEnabled(getItemToggled("publish", SHOW_LMS));
                    break;
				case RESET_PROJECT:
                    ApplicationController.instance.authorController.editResetCommand();
					break;   
				case RUN_PROJECT:
                    ApplicationController.instance.authorController.runCommand();
					break;
				case PAUSE_RESUME_PROJECT:
                    ApplicationController.instance.authorController.toggleRunEdit();
					break;
                case TOGGLE_RUN_LOCK:
                    ApplicationController.instance.authorController.toggleRunLock();
                    break;


                case "showCache":
                    EditorUI.instance.bottomControlBar.showCache = !EditorUI.instance.bottomControlBar.showCache;
                    break;
				case NotificationNamesApplication.TOGGLE_OUTLINE_VIEW:
					Application.instance.outlineVisible = !Application.instance.outlineVisible;
					break;
                case NotificationNamesApplication.ADMIN_TELEPORT:
                    SupportUtilities.teleport();
                    break;
                case NotificationNamesApplication.ADMIN_LOAD_PROJ_BY_ID:
                    SupportUtilities.loadProjectByID();
                    break;
				case NotificationNamesApplication.ADMIN_FIND_TITLE:
					FindWindowController.instance.findByTitle();
					break;
				case NotificationNamesApplication.ADMIN_FIND_RIBBON:
					FindWindowController.instance.findByRibbon();
					break;
				case NotificationNamesApplication.ADMIN_FIND_UID:
					FindWindowController.instance.findByUID();
					break;
				case NotificationNamesApplication.ADMIN_COPY_UID:
					SupportUtilities.copySelectedObjectUID();
					break;
				case NotificationNamesApplication.ADMIN_COPY_JSON:
					SupportUtilities.copyProjectJson();
					break;
				case NotificationNamesApplication.ADMIN_COPY_XML:
					SupportUtilities.copyProjectXML();
					break;
				case NotificationNamesApplication.ADMIN_COPY_DUMP:
					SupportUtilities.copyProjectDump();
					break;
                case NotificationNamesApplication.ADMIN_COPY_DOT:
                    SupportUtilities.copyProjectDOT();
                    break;
                case NotificationNamesApplication.ADMIN_LOG_CLEAR:
                    LogService.clear();
                    break;
                case NotificationNamesApplication.ADMIN_LOG_SELECTION:
                    SupportUtilities.dumpSelection();
                    break;
                case NotificationNamesApplication.ADMIN_LOG_OBJECTS:
                    GadgetController.instance.dump();
                    SupportUtilities.dumpObjectsToLog();
                    break;
                case NotificationNamesApplication.ADMIN_LOG_WIRES:
                    ApplicationController.instance.wireController.dumpWires();
                    break;
                case NotificationNamesApplication.ADMIN_COPY_LOG:
                    SupportUtilities.copyLog();
                    break;
                case NotificationNamesApplication.ADMIN_CRASH:
					crashAlert();
                    break;
                case "DumpLoadedEvents":
                    EventFlowController.instance.dumpLoadStatus();
                    break;
                case "PseudoThreadSlow":
                    PseudoThread.DEBUG_ONE_RUNNABLE_PER_FRAME = getItemToggled("admin", "PseudoThreadSlow");
                    break;
                case "EventValidation":
                    Application.instance.validationEnabled = getItemToggled("admin", "EventValidation");
                    break;
                case "EventsInLibrary":
                    Application.instance.showEventGadgetsInLibrary = getItemToggled("admin", "EventsInLibrary");
                    break;
			}
		}
		
		private function crashAlert():void
		{
			Alert.cancelLabel = "Cancel";
			Alert.okLabel = "Crash now";
			Alert.buttonWidth = 90;
			Alert.show("Crash will not save your changes.  Are you sure?", "Crash now", Alert.FIRST|Alert.SECOND, handleCrashNowSelection, null, Alert.SECOND);
		}
		
		private function handleCrashNowSelection(event:CloseEvent):void
		{
			switch(event.detail) {
				case Alert.SECOND:
					var n:AbstractObject = null;
					n.title = "Crash";
					break;
			}
		}

        private function get world():World
        {
            return Application.instance.document.root.value as World;
        }


	}
}
