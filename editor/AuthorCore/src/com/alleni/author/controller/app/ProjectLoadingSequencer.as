package com.alleni.author.controller.app
{
    import com.alleni.author.Navigation.EventPage;
    import com.alleni.author.application.ApplicationUI;
    import com.alleni.author.application.ui.EditorUI;
    import com.alleni.author.controller.AuthorController;
    import com.alleni.author.controller.ProgressBarController;
import com.alleni.author.controller.test.TestPanelController;
import com.alleni.author.controller.text.FontLoader;
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.application.ApplicationStrings;
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.model.World;
    import com.alleni.author.model.objects.EventFlow;
    import com.alleni.author.model.objects.EventFlowController;
    import com.alleni.author.model.objects.TemplateValueObject;
import com.alleni.author.model.test.TestPanel;
import com.alleni.author.model.ui.Application;
    import com.alleni.author.util.thread.PseudoThread;
    import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
    import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
    import com.alleni.taconite.dev.Utilities;
    import com.alleni.taconite.event.ApplicationEvent;
    import com.alleni.taconite.factory.IEnvironment;
    import com.alleni.taconite.factory.TaconiteFactory;
    import com.alleni.taconite.lang.TaconiteTimer;
    import com.alleni.taconite.persistence.json.JSON;
    import com.alleni.taconite.service.LogService;
    import com.alleni.author.controller.ui.AssetController;
    import com.alleni.author.model.ui.Asset;
    import com.alleni.author.service.assets.AssetLoadOperation;
    import com.alleni.author.definition.AssetLoadingReport;
    import com.alleni.author.util.Set;

    import flash.events.Event;

    public class ProjectLoadingSequencer
	{
		private static var _instance:ProjectLoadingSequencer;
		
		private static const LOAD_REQUESTED:uint          = 1 << 1;
		private static const PROJECT_LOADED:uint          = 1 << 2;
        private static const PROJECT_CONSTRUCTED:uint     = 1 << 3;
        private static const PROJECT_COMPLETE:uint        = 1 << 4;
		
		private static const ALL_PROGRESS_FLAGS:uint =
			LOAD_REQUESTED |
			PROJECT_LOADED |
            PROJECT_CONSTRUCTED |
            PROJECT_COMPLETE;
		
		private var _status:uint = 0;
		private var _loadBeginTimestamp:Number;
        private var _assetHoldToken:Object;
        private var _inputToken:Object;

		public function ProjectLoadingSequencer()
		{
			Utilities.assert(!_instance);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOADING, handleProjectLoading);
		}
		
		static public function get instance():ProjectLoadingSequencer
		{
			if (!_instance)
				_instance = new ProjectLoadingSequencer();
			return _instance;
		}
		
        public function get loading():Boolean
        {
            return statusIs(LOAD_REQUESTED);
        }

        public function get complete():Boolean
        {
            return statusIs(PROJECT_COMPLETE);
        }

		public function requestLoad(id:String, 
									asACopy:Boolean=false, 
									purchased:Boolean=false, 
									loadVersion:Boolean=false, 
									templateVO:TemplateValueObject=null):void
		{
			//PerformanceTimer.begin("loadProject");
            ApplicationController.instance.authorController.abandonEditors();

            PseudoThread.add(new PseudoThreadSimpleRunnable("startLoading", startLoading, this, 
                [id, asACopy, purchased, loadVersion, templateVO]));
        }

        private function startLoading(id:String, asACopy:Boolean=false, purchased:Boolean=false, loadVersion:Boolean=false, templateVO:TemplateValueObject=null):void
        {
            unsetStatus(ALL_PROGRESS_FLAGS);

            _assetHoldToken = AssetLoadOperation.hold("ProjectLoadingSequencer::startLoading");
            _inputToken = ApplicationController.instance.disableInput("ProjectLoadingSequencer::startLoading");
            addListeners();
			setStatus(LOAD_REQUESTED);
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_LOADING, id));
			ProjectController.instance.loadCloudProject(id, asACopy, purchased, loadVersion, templateVO);

            ProgressBarController.instance.startProjectLoadProgress();
            Application.instance.defaultUserMessage = ApplicationStrings.LOADING_PROJECT;
            ProgressBarController.instance.enqueuePhaseWithName(ApplicationStrings.LOADING_PROJECT);

            // after this, ApplicationController will dispatch PROJECT_LOAD_SUCCEEDED
		}
		
		private function addListeners():void
		{
            ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOAD_SUCCEEDED, handleProjectLoadSucceeded);
            ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOAD_FAILED, handleProjectLoadFailed);
            ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_LOAD_PROGRESS, handleProjectLoadProgress);
            ApplicationController.addEventListener(NotificationNamesApplication.FONTS_PROGRESS, handleFontProgress);
		}
		
		private function removeListeners():void
		{
            ApplicationController.removeEventListener(NotificationNamesApplication.PROJECT_LOAD_SUCCEEDED, handleProjectLoadSucceeded);
			ApplicationController.removeEventListener(NotificationNamesApplication.PROJECT_LOAD_FAILED, handleProjectLoadFailed);
            ApplicationController.removeEventListener(NotificationNamesApplication.PROJECT_LOAD_PROGRESS, handleProjectLoadProgress);
            ApplicationController.removeEventListener(NotificationNamesApplication.FONTS_PROGRESS, handleFontProgress);
		}

		
		private function handleProjectLoading(event:Event):void
		{
			_loadBeginTimestamp = TaconiteTimer.instance.milliseconds;
		}
        
        private function handleProjectLoadFailed(event:Event):void
        {
            unsetStatus(ALL_PROGRESS_FLAGS);
            removeListeners();
            if (_inputToken != null) {
                ApplicationController.instance.enableInput(_inputToken);
                _inputToken = null;
            }
            AssetLoadOperation.unhold(_assetHoldToken);
        }
        
        private function handleProjectLoadProgress(event:ApplicationEvent):void
        {
            var progressFraction:Number = Number(event.data);
            ProgressBarController.instance.projectLoadFraction = progressFraction;
        }

        private function handleAssetProgress(requestedAssets:Set):Function {
            return function(event:ApplicationEvent):void {
                var assetReport:AssetLoadingReport = AssetController.instance.getLoadProgressForSet(requestedAssets);

                AssetController.instance.showLoadProgressFor(event, assetReport.remainingCount, assetReport.progressFraction);

                if (assetReport.remainingCount == 0) {
                    ApplicationController.removeEventListener(NotificationNamesApplication.ASSET_REQUEST_PROGRESS, arguments.callee);
                }
            }
        }

        private function handleFontProgress(event:Event):void
        {
            if (!statusIs(PROJECT_LOADED)) {
                handleProjectLoadSucceeded()
            }
        }

        private function handleProjectLoadSucceeded(event:ApplicationEvent=null):void
        {
            if (FontLoader.instance.requestedCount > 0) return;

            var document = Application.instance.document;
            if (document == null) return;

            setStatus(PROJECT_LOADED);
            var population:int = document.project.estimateBuildoutCount;
            trace("backstage population="+population);
            var frameCount:int = EventFlowController.instance.getFrameCountForLoadUnload(population);
            ProgressBarController.instance.startBackstageBuildProgress(frameCount);

            PseudoThread.add(new PseudoThreadSimpleRunnable("document.construct", document.construct));
            PseudoThread.add(new PseudoThreadSimpleRunnable("handleProjectConstructComplete", handleProjectConstructComplete));
        }

        private function handleProjectConstructComplete():void
        {
            if (statusIs(PROJECT_CONSTRUCTED)) return;
            setStatus(PROJECT_CONSTRUCTED);

            // let everyone know the project is initialized (but we haven't yet shown an EventPage)
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_INITIALIZED));

            var document = Application.instance.document;
            var world:World = document.root.value as World;
            var project = document.project;
            var flow:EventFlow = AuthorController.instance.eventFlow;
            var env:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
            var page:EventPage = null;
            var running:Boolean;
            var reset:Boolean;
            if (env.invokedAsEditor) {
                if (project.accountTypeId < 3) {
                    flow.flowVisible = false;  // lock to a specific page
                    page = flow.pages[0];
                } else {  // professional
                    flow.titleBarVisible = true;
                    if (flow.flowVisible) {     // show flow
                        watchAssetLoading();
                        page = null;
                    } else {                    // show a specific event page
                        page = flow.wasEditingEventPage;
                    }
                }
                flow.disableLoadUnload = flow.flowVisible;
                Application.instance.flowVisible = flow.flowVisible;
                running = (document.running || page == null) && !flow.flowVisible;
                reset = false;  // editor always resumes
                AuthorController.instance.setLocked(document.locked);
            } else {  // Player
                watchAssetLoading();
                Application.instance.flowVisible = false;
                AuthorController.instance.setLocked(true);
                running = true;

                reset = (world.project.onNextOpen == "restart" || !haveValidResumeDataFromLMS);
            }

            if (running) {
                page = (reset ? world.project.initialPage : flow.currentPage) as EventPage;
            }
            if (page != null) {
                enqueueRunOrEdit(flow, page, running, reset);
            }
            AssetLoadOperation.unhold(_assetHoldToken);
            PseudoThread.add(new PseudoThreadSimpleRunnable("handleProjectInitialized", handleProjectInitialized));

            if (_inputToken != null) {
                PseudoThread.add(new PseudoThreadSimpleRunnable("ProjectLoadingSequencer:enableInput", function():void {
                    var scripts:TestPanel = Application.instance.document.scripts;
                    if (scripts && scripts.panelVisible && !Application.instance.testPanelVisible) {
                        Application.instance.testPanelVisible = true;
                    }
                    ApplicationController.instance.enableInput(_inputToken);
                    _inputToken = null;
                }));
            }
        }

        private function watchAssetLoading():void
        {
            var assetSet:Set = AssetController.instance.getQueueAsSet();
            if (assetSet.size > 0) {
                ApplicationController.addEventListener(NotificationNamesApplication.ASSET_REQUEST_PROGRESS, 
                    handleAssetProgress(assetSet));
            } else {
                ProgressBarController.instance.assetLoadFraction = 1;
            }
        }

        private function enqueueRunOrEdit(flow:EventFlow, page:EventPage, running:Boolean, reset:Boolean):void
        {
            var overlayToken:Object = null;
            EventFlowController.instance.waitForPageImages(flow,  page);
            PseudoThread.add(new PseudoThreadSimpleRunnable("enqueueRunOrEdit/overlayImage",
                function():void {
                    overlayToken = EventFlowController.instance.showOverlayImage(flow, page, "enqueueRunOrEdit");
                }
            ));
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            PseudoThread.add(new PseudoThreadSimpleRunnable("runOrEditNow", runOrEditNow, this, [flow, page, running, reset]));
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            PseudoThread.add(new PseudoThreadSimpleRunnable("enqueueRunOrEdit/removeOverlayImage",
                function():void {
                    EventFlowController.instance.removeOverlayImage(flow, overlayToken);
                }
            ));
        }

        private function runOrEditNow(flow:EventFlow, page:EventPage, running:Boolean, reset:Boolean):void
        {
            if (running && reset) {
                AuthorController.instance.run();
            } else if (running) {
                AuthorController.instance.resume();
            } else {
                AuthorController.instance.editEvent(page);
            }
        }

        private function handleProjectInitialized():void
        {
            unsetStatus(ALL_PROGRESS_FLAGS);
            setStatus(PROJECT_COMPLETE);

            ProgressBarController.instance.stop();

            var secondsToLoad:Number = (TaconiteTimer.instance.milliseconds - _loadBeginTimestamp)/1000;
            ApplicationController.instance.dispatchEvent(
                new ApplicationEvent(NotificationNamesApplication.PROJECT_COMPLETE,
                secondsToLoad));
            ApplicationController.instance.dispatchEvent(
                new ApplicationEvent(NotificationNamesApplication.RESET_USER_MESSAGE, 
                ApplicationStrings.DEFAULT_STATUS_MESSAGE));
            removeListeners();

            trace("ALL DONE in " + secondsToLoad + " seconds.");
        }

        private function get haveValidResumeDataFromLMS():Boolean
        {
            var env:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
            if(env.fromLMS) {
                var values:Object = env.lmsValues;
                if(values.hasOwnProperty('suspendData')) {
                    try {
                        var suspendData:Object = com.alleni.taconite.persistence.json.JSON.decode(values.suspendData, false);
                        return true;
                    } catch(error:Error) {
                        LogService.error("suspendData found in LMS values but decode failed: suspendData="+values.suspendData);
                    }
                }
            }
            return false;
        }

        private function get world():World
        {
            return AuthorController.instance.world;
        }
		
		private function statusIs(mask:uint):Boolean
		{
			return (_status & mask) != 0;
		}
		
		private function setStatus(mask:uint):void
		{
			_status |= mask;
		}
		
		private function unsetStatus(mask:uint):void
		{
			_status &= ~mask;
		}
	}
}
