package com.alleni.author.controller.app
{
	import com.alleni.author.controller.text.FontLoader;
	import com.alleni.author.definition.text.Fonts;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.factory.TaconiteFactory;
	
	import flash.events.EventDispatcher;
	import flash.text.Font;
	
	public class FontController extends EventDispatcher
	{
		private static var _instance:FontController;
		private static const IS_EDITOR:Boolean = TaconiteFactory.getEnvironmentImplementation().invokedAsEditor;
		
		/*
		* This is to be replaced by an api call
		*/
		private static const HOUSE_FONTS:Vector.<String> = Vector.<String>([
			"Crimson",
			"Droid_Serif",
			"Josefin_Sans",
			"Ubuntu",
			"Cantarell",
			"Josefin_Slab",
			"Tinos",
			"Amaranth" 
		]);
		
		/*
		Font Loader loads in SWFs and registers them.  The fonts encapsulated in the swf are successfully grouped in a family.
		*/
		private var _fontLoader:FontLoader;
		
		public function FontController()
		{
			Utilities.assert(!_instance);
			_fontLoader = FontLoader.instance;
			if (IS_EDITOR)
				loadHouseFonts();
		}
		
		static public function get instance():FontController
		{
			if (!_instance)
				_instance = new FontController();
			return _instance;
		}
		
		private function loadHouseFonts():void
		{
			var font:String;
			for each(font in HOUSE_FONTS)
				requestByName(font);
		}
		
		public function requestByName(font:String):void
		{
			// replace spaces with underscores and add .swf
			var fontName:String = font.replace(/ /g,"_"); // replace spaces with underscores
			var fontFile:String = fontName + ".swf";
			
			if (!isFontRegistered(fontName))
				_fontLoader._loadFontByData(fontFile, fontName);
		}
		
		private function isFontRegistered(font:String):Boolean
		{
			/* all house fonts with spaces like Droid Sans are registed as Droid_Sans. 
			Their filenames look like this:  Droid_Sans.swf.  
			Their menu labels look like this:  Droid Sans
			*/
			if(Fonts.EMBEDDED_FONT_NAMES.indexOf(font) > -1)
				return true;
			
			var fonts:Array = Font.enumerateFonts(false);
			var fontObj:Object;
			for each(fontObj in fonts) {
				if (("fontName" in fontObj) && (fontObj.fontName == font))
					return true;
			}
			return false;
		}
		
		public function get queueProgressFraction():uint
		{
			return _fontLoader.queueProgressFraction;
		}
		
		public function get requestedCount():uint
		{
			return _fontLoader.requestedCount;
		}
	}
}