package com.alleni.author.controller.app
{
	import com.alleni.taconite.controller.IWindowListener;
	import com.alleni.taconite.factory.ITaconiteWindow;
	
	import flash.events.EventDispatcher;
	import flash.ui.MouseCursor;
	
	import mx.managers.ICursorManager;
	
	public class CursorController extends EventDispatcher
	{
		public static const CURSOR_TYPE_STANDARD:String = "Standard";
		public static const CURSOR_TYPE_TOOL:String = "Tool";
		public static const CURSOR_TYPE_LOCKED_TOOL:String = "Locked Tool";
		public static const CURSOR_TYPE_CONTROLS:String = "Controls";
		
		public var defaultCursor:String = MouseCursor.ARROW;
		
		/*[Embed(source="/assets/cursors/eyedrop.png",compression="true",quality="80")] 
		private var Eyedropper:Class;
		
		[Embed(source="/assets/cursors/arrow-copy.png",compression="true",quality="80")]
		private var ArrowCopy:Class;
		
		[Embed(source="/assets/cursors/arrow-select.png",compression="true",quality="80")]
		private var ArrowSelect:Class;
		
		[Embed(source="/assets/cursors/tool-arena.png",compression="true",quality="80")]
		private var ToolArena:Class;
		
		[Embed(source="/assets/cursors/tool-button.png",compression="true",quality="80")]
		private var ToolButton:Class;
		
		[Embed(source="/assets/cursors/tool-checkbox.png",compression="true",quality="80")]
		private var ToolCheckbox:Class;
		
		[Embed(source="/assets/cursors/tool-clock.png",compression="true",quality="80")]
		private var ToolClock:Class;
		
		[Embed(source="/assets/cursors/tool-ellipse.png",compression="true",quality="80")]
		private var ToolEllipse:Class;
		
		[Embed(source="/assets/cursors/tool-line.png",compression="true",quality="80")]
		private var ToolLine:Class;
		
		[Embed(source="/assets/cursors/tool-pencil.png",compression="true",quality="80")]
		private var ToolPencil:Class;
		
		[Embed(source="/assets/cursors/tool-rectangle.png",compression="true",quality="80")]
		private var ToolRectangle:Class;
		
		[Embed(source="/assets/cursors/tool-slider.png",compression="true",quality="80")]
		private var ToolSlider:Class;
		
		[Embed(source="/assets/cursors/tool-text.png",compression="true",quality="80")]
		private var ToolText:Class;
		
		[Embed(source="/assets/cursors/tool-textinput.png",compression="true",quality="80")]
		private var ToolTextInput:Class;
		
		[Embed(source="/assets/cursors/tool-truthtable.png",compression="true",quality="80")]
		private var ToolTruthTable:Class;
		
		[Embed(source="/assets/cursors/zoomIn.png",compression="true",quality="80")]
		private var ZoomIn:Class;
		
		[Embed(source="/assets/cursors/zoomOut.png",compression="true",quality="80")]
		private var ZoomOut:Class;
		
		[Embed(source="/assets/cursors/finger.png",compression="true",quality="80")]
		private var Finger:Class;
		
		[Embed(source="/assets/cursors/hand.png",compression="true",quality="80")]
		private var Hand:Class;
		
		[Embed(source="/assets/cursors/ibeam.png",compression="true",quality="80")]
		private var IBeam:Class;
		
		[Embed(source="/assets/cursors/scale-corner1.png",compression="true",quality="80")]
		private var Scale_Corner1:Class;
		
		[Embed(source="/assets/cursors/scale-corner2.png",compression="true",quality="80")]
		private var Scale_Corner2:Class;
		
		[Embed(source="/assets/cursors/scale-horz.png",compression="true",quality="80")]
		private var Scale_Horz:Class;
		
		[Embed(source="/assets/cursors/scale-vert.png",compression="true",quality="80")]
		private var Scale_Vert:Class;*/
		
		private var _cursorManager:ICursorManager;
		private var _cursorID:int;
		private var _window:ITaconiteWindow;
		private var _customCursor:Object;
		private var _previousCursor:Object;
		private static var _lock:Boolean;
		private var _previousType:String;
		private var _currentType:String = CURSOR_TYPE_STANDARD;
		
		public function CursorController(cursorManager:ICursorManager):void
		{
			_cursorManager = cursorManager;
		}
		
		private function changeWindow(window:ITaconiteWindow):void
		{
			_window = window;
			if ((_cursorManager as IWindowListener) != null)
				(_cursorManager as IWindowListener).window = window;
		}
		
		public static function unlockCursor():void
		{
			_lock = false;
		}
		
		public static function lockCursor():void
		{
			_lock = true;
		}
		
		private function changeCursorTo(name:String, type:String):void{	
			_previousType = _currentType;
			_previousCursor = _customCursor;
			
/*			switch(_currentType){
				case CURSOR_TYPE_STANDARD:
					setCustom(name);
					_currentType = type;
					break;
				case CURSOR_TYPE_CONTROLS:
					if(_cursorID){
						_cursorManager.removeCursor(_cursorID);
					}
					_cursorManager.hideCursor();
					_cursorID = _cursorManager.setCursor(_previousCursor.theClass, _previousCursor.priority, _previousCursor.xOffset, _previousCursor.yOffset);
					_currentType = type;
					break;
				case CURSOR_TYPE_TOOL:
					// Odd empty conditional left with only a semicolon so I commented it out; should this be enabled? -PJK
					//if(name == CursorNames.ARROW_SELECT || type == CURSOR_TYPE_LOCKED_TOOL);
						setCustom(name);
						_previousCursor = _customCursor;
						_currentType = type;
					break;
				case CURSOR_TYPE_LOCKED_TOOL:
					_lock = true;
					_currentType = type;
			}
*/			
		}
		
		public function setCustom(cursor:String):void
		{
/*			switch(cursor){
				case MouseCursor.AUTO:
					if(!_lock && _previousCursor != null){
						drawCustomCursor(_previousCursor);
					}
					break;
				case CursorNames.ARROW_SELECT:
					drawCustomCursor({theClass:ArrowSelect, priority:2, xOffset:-2, yOffset:-3});
					break;
				case MouseCursor.BUTTON:
					drawCustomCursor({theClass:Finger, priority:2, xOffset:-7, yOffset:-2});
					break;
				case MouseCursor.HAND:
					drawCustomCursor({theClass:Hand, priority:2, xOffset:-7, yOffset:-2});
					break;
				case MouseCursor.IBEAM:
					drawCustomCursor({theClass:IBeam, priority:2, xOffset:-8, yOffset:-10});
					break;
				case CursorNames.EYEDROPPER:
					drawCustomCursor({theClass:Eyedropper, priority:3, xOffset:-2, yOffset:-15});
					break;
				case CursorNames.ARROW_COPY:
					drawCustomCursor({theClass:ArrowCopy, priority:2, xOffset:-3, yOffset:-3});
					break;
				case CursorNames.TOOL_ARENA:
					drawCustomCursor({theClass:ToolArena, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_BUTTON:
					drawCustomCursor({theClass:ToolButton, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_CHECKBOX:
					drawCustomCursor({theClass:ToolCheckbox, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_CLOCK:
					drawCustomCursor({theClass:ToolClock, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_ELLIPSE:
					drawCustomCursor({theClass:ToolEllipse, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_LINE:
					drawCustomCursor({theClass:ToolLine, priority:2, xOffset:-3, yOffset:-5});
					break;
				case CursorNames.TOOL_PENCIL:
					drawCustomCursor({theClass:ToolPencil, priority:2, xOffset:-3, yOffset:-16});
					break;
				case CursorNames.TOOL_RECTANGLE:
					drawCustomCursor({theClass:ToolRectangle, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_SLIDER:
					drawCustomCursor({theClass:ToolSlider, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_TEXT:
					drawCustomCursor({theClass:ToolText, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_TEXT_INPUT:
					drawCustomCursor({theClass:ToolTextInput, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_TRUTH_TABLE:
					drawCustomCursor({theClass:ToolTruthTable, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_ZOOMIN:
					drawCustomCursor({theClass:ZoomIn, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.TOOL_ZOOMOUT:
					drawCustomCursor({theClass:ZoomOut, priority:2, xOffset:-5, yOffset:-5});
					break;
				case CursorNames.SCALE_CORNER1:
					drawCustomCursor({theClass:Scale_Corner1, priority:2, xOffset:-9, yOffset:-9});
					break;
				case CursorNames.SCALE_CORNER2:
					drawCustomCursor({theClass:Scale_Corner2, priority:2, xOffset:-9, yOffset:-9});
					break;
				case CursorNames.SCALE_HORZ:
					drawCustomCursor({theClass:Scale_Horz, priority:2, xOffset:-9, yOffset:-9});
					break;
				case CursorNames.SCALE_VERT:
					drawCustomCursor({theClass:Scale_Vert, priority:2, xOffset:-9, yOffset:-9});
					break;
			}
*/		}
		
		public function resetCursor():void
		{
/*			switch(Application.instance.currentTool){
				case ToolboxController.ARROW:
					drawCustomCursor({theClass:ArrowSelect, priority:2, xOffset:-2, yOffset:-3});
					break;
				case ToolboxController.MOVE:
					drawCustomCursor({theClass:Hand, priority:2, xOffset:-7, yOffset:-2});
					break;
				case ToolboxController.ARENA:
					drawCustomCursor({theClass:ToolArena, priority:2, xOffset:-5, yOffset:-5});
					break;
				case ToolboxController.BUTTON:
					drawCustomCursor({theClass:ToolButton, priority:2, xOffset:-5, yOffset:-5});
					break;
				case ToolboxController.CHECKBOX:
					drawCustomCursor({theClass:ToolCheckbox, priority:2, xOffset:-5, yOffset:-5});
					break;
				case ToolboxController.CLOCK:
					drawCustomCursor({theClass:ToolClock, priority:2, xOffset:-5, yOffset:-5});
					break;
				case ToolboxController.ELLIPSE:
					drawCustomCursor({theClass:ToolEllipse, priority:2, xOffset:-5, yOffset:-5});
					break;
				case ToolboxController.LINE:
					drawCustomCursor({theClass:ToolLine, priority:2, xOffset:-3, yOffset:-5});
					break;
				case ToolboxController.PENCIL:
					drawCustomCursor({theClass:ToolPencil, priority:2, xOffset:-3, yOffset:-16});
					break;
				case ToolboxController.RECTANGLE:
					drawCustomCursor({theClass:ToolRectangle, priority:2, xOffset:-5, yOffset:-5});
					break;
				case ToolboxController.SLIDER:
					drawCustomCursor({theClass:ToolSlider, priority:2, xOffset:-5, yOffset:-5});
					break;
				case ToolboxController.TEXT:
					drawCustomCursor({theClass:ToolText, priority:2, xOffset:-5, yOffset:-5});
					break;
				case ToolboxController.INPUTTEXT:
					drawCustomCursor({theClass:ToolTextInput, priority:2, xOffset:-5, yOffset:-5});
					break;
				case ToolboxController.TRUTHTABLE:
					drawCustomCursor({theClass:ToolTruthTable, priority:2, xOffset:-5, yOffset:-5});
					break;
				case ToolboxController.ZOOM:
					drawCustomCursor({theClass:ZoomIn, priority:2, xOffset:-5, yOffset:-5});
					break;
			}*/
		}
		
		private function drawCustomCursor(cursor:Object):void
		{
			if(_cursorID){
				_cursorManager.removeCursor(_cursorID);
			}
			_cursorID = _cursorManager.setCursor(cursor.theClass, cursor.priority, cursor.xOffset, cursor.yOffset);
		}
		
		public function showFlashCursor():void
		{
			_cursorManager.removeCursor(_cursorID);
		}
		
		public function hideFlashCursor():void
		{
			_cursorManager.hideCursor();
			resetCursor();
		}
	}
}