package com.alleni.author.definition
{
	public class Parser
	{
		public function Parser()
		{
		}
		
		public static function parse(tokenQueue:Object):AST
		{
			var rslt:Object = parseExpression(tokenQueue);
			var t:Object = rslt.tokens.obj;
			if(t.kind != "eof") {
				throw new Error("Expected eof at position " + t.position + " but found " + t.kind + " [" + t.text + "] instead");
			} else {
				return new AST(rslt.value);
			}
		}
		
		private static function parseArgumentList(tokenQueue:Object):Object
		{
			var t:Object = tokenQueue.obj;
			var arg:Object;
			var rslt:Array = new Array();
			try {
				arg = parseExpression(tokenQueue);
			} catch(e:Object) {
				return {value:new Array(), tokens:tokenQueue}; // no arguments
			}
			rslt.push(arg.value);
			tokenQueue = arg.tokens;
			while(tokenQueue.obj.kind == "comma") {
				tokenQueue = tokenQueue.next;
				arg = parseExpression(tokenQueue);
				rslt.push(arg.value);
				tokenQueue = arg.tokens;
			}
			return {value:{kind: "argumentList", list:rslt}, tokens:tokenQueue};
		}
		
		private static function parseExpression1(tokenQueue:Object):Object
		{
			var t:Object = tokenQueue.obj;
			if(t.kind == "number") {
				tokenQueue = tokenQueue.next;
				return {value:{kind:"number", token:t}, tokens:tokenQueue};
			} else if(t.kind == "identifier") {
				tokenQueue = tokenQueue.next;
				if(tokenQueue.obj.kind == "open paren") { // function call
					tokenQueue = tokenQueue.next;
					var arguments:Object = parseArgumentList(tokenQueue);
					tokenQueue = arguments.tokens;
					var nameToken:Object = t;
					t = tokenQueue.obj;
					if(t.kind == "close paren") {
						tokenQueue = tokenQueue.next;
						return {value:{kind:"functionCall", token:nameToken, args:arguments.value}, tokens:tokenQueue};
					} else {
						throw new Error("Expected close paren at position " + t.position + " but found " + t.kind + " [" + t.text + "] instead");
					}
				} else {
					return {value:{kind:"variable", token:t}, tokens:tokenQueue};
				}
			} else if(t.kind == "open paren") {
				tokenQueue = tokenQueue.next;
				var inner:Object = parseExpression(tokenQueue);
				tokenQueue = inner.tokens;
				t = tokenQueue.obj;
				if(t.kind == "close paren") {
					inner.tokens = tokenQueue.next;
					return inner;
				} else {
					throw new Error("Expected close paren at position " + t.position + " but found " + t.kind + " [" + t.text + "] instead");
				}
			} else if(t.kind == "operator" && t.text == "!" || t.text == "-") {
				tokenQueue = tokenQueue.next;
				var rhs:Object = parseExpression1(tokenQueue);
				tokenQueue = rhs.tokens;
				return {value:{kind:"unary operator", rhs:rhs.value, token:t}, tokens:tokenQueue}
			} else {
				throw new Error("Expected number or identifier at position " + t.position + " but found " + t.kind + " [" + t.text + "] instead");
			}
		}
		
		private static function parseExpression2(tokenQueue:Object, level:int=4):Object
		{
			var newLeft:Object;
			var appendPoint:Object;
			var andToken:Object;
			if(level == 0) {
				return parseExpression1(tokenQueue);
			}
			var left:Object = parseExpression2(tokenQueue, level-1);
			tokenQueue = left.tokens;
			var t:Object = tokenQueue.obj;
			if(t.kind == "operator" && t.level == level) {
				tokenQueue = tokenQueue.next; // consume
				left.tokens = null;
				var right:Object = parseExpression2(tokenQueue, level);
				tokenQueue = right.tokens;
				if(right.value.kind == "operator" && right.value.token.level == 3 && level == 3) {
					// chaining comparisons
					newLeft = {kind:"operator", lhs:left.value, rhs:right.value.lhs, token:t};
					andToken = {kind:"operator", text:"&", position:t.position, level:4};
					switch(t.text) {
						case "<": case "<=":
							switch(right.value.token.text) {
								case "<": case "<=": andToken.comparisonDirection = -1; break;
								case ">": case ">=":
								throw new Error("Direction of comparison at position " + t.position + " [" + t.text + "] does not match comparisons to the right");
								default: andToken.comparisonDirection = -1;
							}
							break;
						case ">": case ">=":
							switch(right.value.token.text) {
								case "<": case "<=": throw new Error("Direction of comparison at position " + t.position + " [" + t.text + "] does not match comparisons to the right");
								case ">": case ">=": andToken.comparisonDirection = 1; break;
								default: andToken.comparisonDirection = 1;
							}
							break;
						default:
							switch(right.value.token.text) {
								case "<": case "<=": andToken.comparisonDirection = -1; break;
								case ">": case ">=": andToken.comparisonDirection = 1; break;
								default: andToken.comparisonDirection = 0;
							}
					}
					return {value:{kind:"operator", lhs:newLeft, rhs:right.value, token:andToken}, tokens:tokenQueue};
				} else if(right.value.kind == "operator" && right.value.token.level == 4 && level == 3) {
					// rhs was already a chained comparison
					appendPoint = right.value;
					while(appendPoint.lhs.kind == "operator" && appendPoint.lhs.token.level == 4) {
						appendPoint = appendPoint.lhs;
					}
					if(appendPoint.lhs.kind == "operator" && appendPoint.lhs.token.level == 3) {
						newLeft = {kind:"operator", lhs:left.value, rhs:appendPoint.lhs.lhs, token:t};
						andToken = {kind:"operator", text:"&", position:t.position, level:4};
						switch(t.text) {
							case "<": case "<=":
								if(right.value.token.comparisonDirection == 0) {
									andToken.comparisonDirection = -1;
								} else if(right.value.token.comparisonDirection > 0) {
									throw new Error("Direction of comparison at position " + t.position + " [" + t.text + "] does not match comparisons to the right");
								}
							    break;
							case ">": case ">=":
								if(right.value.token.comparisonDirection == 0) {
									andToken.comparisonDirection = 1;
								} else if(right.value.token.comparisonDirection < 0) {
									throw new Error("Direction of comparison at position " + t.position + " [" + t.text + "] does not match comparisons to the right");
								}
								break;
							// no default
						}
						appendPoint.lhs = {kind:"operator", lhs:newLeft, rhs:appendPoint.lhs, token:andToken};
						return right;
					} else {
						throw new Error("Expected comparison operator at position " + appendPoint.lhs.position + " but found " + appendPoint.lhs.kind + " [" + appendPoint.lhs.text + "] instead")
					}
				} else if(right.value.kind == "operator" && right.value.token.level == level) {
					// chaining other operators
					appendPoint = right.value;
					while(appendPoint.lhs.kind == "operator" && appendPoint.lhs.token.level == level) {
						appendPoint = appendPoint.lhs;
					}
					appendPoint.lhs = {kind:"operator", lhs:left.value, rhs:appendPoint.lhs, token:t};
					return right;
				}
				return {value:{kind:"operator", lhs:left.value, rhs:right.value, token:t}, tokens:tokenQueue};
			} else {
				return left;
			}
		}
		
		private static function parseExpression(tokenQueue:Object):Object
		{
			return parseExpression2(tokenQueue);
		}
	}
}