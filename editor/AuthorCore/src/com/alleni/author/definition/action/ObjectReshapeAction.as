package com.alleni.author.definition.action {
import com.alleni.author.model.AbstractObject;

public class ObjectReshapeAction extends UserAction {
	private var _objectUID:String;
	private var _objectName:String;

	private var _oldShape:ObjectShape;
	private var _newShape:ObjectShape;


	public function ObjectReshapeAction(objectUID:String, objectName:String, oldShape:ObjectShape, newShape:ObjectShape) {
		_objectUID = objectUID;
		_objectName = objectName;
		_oldShape = oldShape;
		_newShape = newShape;
	}

	override public function get title():String {
		return "Resize " + _objectName;
	}

	public static function fromObject(object:AbstractObject, oldShape:ObjectShape):ObjectReshapeAction {
		return new ObjectReshapeAction(object.uid, object.title, oldShape, ObjectShape.fromObject(object));
	}

	override public function perform():void {
		_newShape.apply(world.findObjByUID(_objectUID));
	}

	override public function rollback():void {
		_oldShape.apply(world.findObjByUID(_objectUID));
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "ObjectReshapeAction";
		result.objectUID = _objectUID;
		result.objectName = _objectName;
		result.oldShape = _oldShape.toJSON();
		result.newShape = _newShape.toJSON();
		return result;
	}

	public static function fromJson(json:Object):ObjectReshapeAction {
		return new ObjectReshapeAction(json.objectUID, json.objectName, ObjectShape.fromJson(json.oldShape), ObjectShape.fromJson(json.newShape));
	}
}
}