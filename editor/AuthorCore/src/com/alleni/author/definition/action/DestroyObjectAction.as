package com.alleni.author.definition.action {
import com.alleni.author.model.AbstractObject;
import com.alleni.taconite.model.TaconiteModel;

public class DestroyObjectAction extends ObjectExistenceAction {
	public function DestroyObjectAction(serialized:Object) {
		super(serialized);
	}

	override public function get title():String {
		return "Delete " + name;
	}

	override public function perform():void {
		destroy();
	}

	override public function rollback():void {
		create();
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "DestroyObjectAction";
		result.object = serialized;
		return result;
	}

	public static function fromSelection(models:Vector.<TaconiteModel>):DestroyObjectAction {
		return new DestroyObjectAction(createSerialized(models));
	}

	public static function fromObject(obj:AbstractObject):DestroyObjectAction {
		var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
		models.push(obj.model);
		return new DestroyObjectAction(createSerialized(models));
	}

	public static function fromJson(json:Object):DestroyObjectAction {
		return new DestroyObjectAction(json.object);
	}
}
}