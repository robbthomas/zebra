/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 8/28/12
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.definition.PositionAndAngle;
import com.alleni.author.definition.text.FormattedText;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.GraphicFill;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.ToggleButtonGroup;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.taconite.service.LogService;

import flash.geom.Point;
import flash.geom.Rectangle;

/**
 * Action which sets a property for example through a ribbon control.
 */
public class ModifyValueAction extends UserAction {

	protected var _objectUID:String;
	protected var _oldValue:Object;
	protected var _newValue:Object;
	protected var _title:String;


	public function ModifyValueAction(objectUID:String, oldValue:Object, newValue:Object, title:String, jsonify:Boolean) {
		_objectUID = objectUID;
		_oldValue = jsonify?ModifyValueAction.valueToJson(oldValue):oldValue;
		_newValue = jsonify?ModifyValueAction.valueToJson(newValue):newValue;
		_title = title;
	}

	override public function get title():String {
		return _title;
	}

	public static function valueToJson(value:Object):Object {
		if(value == null || value is Number || value is String || value is Boolean) {
			return value;
		}
        if(value is Array){
            var result:Array = [];
            for each(var obj:Object in value){
                result.push(valueToJson(obj));
            }
            return {type:"Array", values:result};
        }
		if(value is Rectangle) {
			return {type:"Rectangle", x:value.x, y:value.y, width:value.width, height:value.height};
		}
		if(value is Point) {
            return {type:"Point", x:value.x, y:value.y};
		}
		if(value is AbstractObject) {
			return AbstractObject(value).referenceJSON;
		}
		if("toJson" in value) {
			return value.toJson();
		}
		return null;
	}

	public static function valueFromJson(json:Object):Object {
		if(json == null || json is Number || json is String || json is Boolean) {
			return json;
		}
		if(json.hasOwnProperty("type")) {
			switch(json.type) {
				case "Rectangle":
					return new Rectangle(json.x, json.y, json.width, json.height);
				case "Point":
					return new Point(json.x, json.y);
				case "AbstractObjectReference":
					return (Application.instance.document.root.value as World).findObjByRef(json);
				case "PositionAndAngle":
					return PositionAndAngle.fromJson(json);
				case "Gradient Fill":
				case "Jellybean Fill":
				case "Solid Fill":
					return GraphicFill.fromJson(json);
				case "FormattedText":
					return FormattedText.fromJson(json);
				case "Asset":
                case "AssetReference":
					return Asset.fromJson(json);
				case "ToggleButtonGroup":
					return ToggleButtonGroup.fromJson(json);
                case "Array":
                    var result:Array = [];
                    for each(var obj:Object in json.values){
                        result.push(valueFromJson(obj));
                    }
                    return result;
			}
			LogService.error("Unable to deserialize value of type [" + json.type + "] in ModifyValueAction::valueFromJson");
		}
		return null;
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "ModifyValueAction";
		result.objectUID = _objectUID;
		result.oldValue = _oldValue;
		result.newValue = _newValue;
		result.title = _title;
		return result;
	}
}
}