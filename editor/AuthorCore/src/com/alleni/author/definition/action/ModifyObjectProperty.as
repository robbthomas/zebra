/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.definition.action
{
	import com.alleni.author.model.AbstractObject;

    /**
     * Action which sets a property, for example messageCenterLocked.
     * Changes do not propagate on wires, which is why ModifyRibbonAction is better when an anchor exists.
     */
	public class ModifyObjectProperty extends ModifyValueAction
	{
		protected var _propertyName:String;
		protected var _index:int;
		
		public function ModifyObjectProperty(objectUID:String, title:String, propertyName:String, index:int, oldValue:*, newValue:*, jsonify:Boolean) {
			super(objectUID, oldValue, newValue, title, jsonify);
			_propertyName = propertyName;
			_index = index;
		}
		
		override public function get title():String {
			return _title;
		}
		
		public static function fromObject(object:AbstractObject, title:String, propertyName:String, index:int, oldValue:*):ModifyObjectProperty {
			var newValue:Object = object[propertyName];
            if(index >= 0) {
                newValue = newValue[index];
            }
            return new ModifyObjectProperty(object.uid, title, propertyName, index, oldValue, newValue, true);
		}
		
		override public function perform():void {
			setValue(_newValue);
		}
		
		override public function rollback():void {
            setValue(_oldValue);
		}

        private function setValue(valueJson:Object):void
        {
            var val:* =  ModifyValueAction.valueFromJson(valueJson);

            var obj:AbstractObject = world.findObjByUID(_objectUID);
            if(_index >= 0) {
                obj[_propertyName][_index] = val;
                if (_propertyName in obj.initialValues && _index in obj.initialValues[_propertyName]) {
                    obj.initialValues[_propertyName][_index] = val;
                }
            } else {
                obj[_propertyName] = val;
                if (_propertyName in obj.initialValues) {
                    obj.initialValues[_propertyName] = val;
                }
            }
        }
		
		override public function toJSON():Object {
			var result:Object =  super.toJSON();
			result.kind = "ModifyObjectProperty";
			result.propertyName = _propertyName;
            result.index = _index;
			return result;
		}
		
		public static function fromJson(json:Object):ModifyObjectProperty {
			return new ModifyObjectProperty(json.objectUID, json.title, json.propertyName, json.index, json.oldValue, json.newValue, false);
		}
	}
}