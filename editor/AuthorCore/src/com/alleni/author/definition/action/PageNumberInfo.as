/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/24/12
 * Time: 9:31 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.Navigation.Pager;

public class PageNumberInfo {

    public var pageNumber:int;
    public var initialPageNumber:int;
    public var leftPageNumber:int;
    public var rightPageNumber:int;

    public function PageNumberInfo(pageNumber:int, initialPageNumber:int, leftPageNumber:int, rightPageNumber:int)
    {
        this.pageNumber = pageNumber;
        this.initialPageNumber = initialPageNumber;
        this.leftPageNumber = leftPageNumber;
        this.rightPageNumber = rightPageNumber;
    }

    public function applyToPager(pager:Pager):void
    {
        pager.pageNumber = pageNumber;
        pager.initialPageNumber = initialPageNumber;
        pager.leftPageNumber = leftPageNumber;
        pager.rightPageNumber = rightPageNumber;
    }

    public function toJSON():Object {
        return {pageNumber:pageNumber, initialPageNumber:initialPageNumber, leftPageNumber:leftPageNumber, rightPageNumber:rightPageNumber};
    }

    public static function fromJSON(json:Object):PageNumberInfo {
        return new PageNumberInfo(json.pageNumber, json.initialPageNumber, json.leftPageNumber, json.rightPageNumber);
    }

    public static function fromObject(pager:Pager):PageNumberInfo {
        return new PageNumberInfo(pager.pageNumber, pager.initialPageNumber, pager.leftPageNumber, pager.rightPageNumber);
    }

    public function toString():String
    {
        return "[PageNumberInfo p="+pageNumber + " init="+initialPageNumber + " left="+leftPageNumber + " right="+rightPageNumber+"]";
    }

}
}
