package com.alleni.author.definition.action {
import com.alleni.author.controller.ui.GroupSelectionController;
import com.alleni.author.model.AbstractObject;
import com.alleni.taconite.model.GroupSelectionObject;
import com.alleni.taconite.model.TaconiteModel;

public class GroupExistenceAction extends UserAction { 

	protected var _serialized:Object;

	public function GroupExistenceAction(serialized:Object) {
		super();
		_serialized = serialized;  // array of uid
	}

	protected static function createSerialized(group:GroupSelectionObject):Object {
		var list:Array = [];
		for each (var m:TaconiteModel in group.selection.selectedModels) {
			var obj:AbstractObject = m.value as AbstractObject;
			list.push(obj.uid);
		}
		return list;
	}
	
	private function get objectsList():Vector.<TaconiteModel> {
		var result:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
		for each (var uid:String in _serialized) {
			var obj:AbstractObject = world.findObjByUID(uid);
			if (obj)
				result.push(obj.model);
		}
		return result;
	}
		
	protected function create():void {
		GroupSelectionController.makeGroupFromModels(objectsList);
	}

	protected function destroy():void {
		var firstObject:AbstractObject = objectsList[0].value as AbstractObject;
		var group:GroupSelectionObject = firstObject.group;
		if (group)
			GroupSelectionController.dischargeGroup(group);
	}
}
}