/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 11/26/12
 * Time: 3:41 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.model.ui.IReplaceable;

public class LibraryRemoveAction extends LibraryExistenceAction {
    public function LibraryRemoveAction(item:IReplaceable) {
        super(item);
    }

    override public function perform():void {
        destroy();
    }

    override public function rollback():void {
        create();
    }

    override public function get title():String {
        return "Remove from library: " + _name;
    }

    override public function toJSON():Object {
        var result:Object = super.toJSON();
        result.kind = "LibraryRemoveAction";
        result.object = _serialized;
        return result;
    }

    public static function fromItem(item:IReplaceable):LibraryRemoveAction {
        return new LibraryRemoveAction(item);
    }

    public static function fromJson(json:Object):LibraryRemoveAction {
        return new LibraryRemoveAction(itemFromJson(json.object));
    }

}
}
