/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 2/28/11
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.model.objects.AbstractTable;
import com.alleni.taconite.service.LogService;

public class ModifyTableAction extends ModifyRibbonAction {
	private var _row:int;
	private var _column:int;

	public function ModifyTableAction(objectUID:String, ribbon:String, oldValue:Object, newValue:Object, title:String, row:int, column:int, jsonify:Boolean) {
		super(objectUID, ribbon, oldValue, newValue, title, jsonify);
		_row = row;
		_column = column;
	}

	protected static function setValue(table:AbstractTable, prop:String, row:int, column:int, value:Object):void {
        // check table size since there is currently not an undo action for resizing tables
        if (column < table.numColumns && (row * table.numColumns) < table.allStateValues.length) {
            switch(prop) {
                case "allStateValues":
                    table.setStateValue(row, column, value, true);
                    break;
                case "allTweenValues":
                    table.setTweenValue(row, column, value, true);
                    break;
                default:
                    LogService.error("Unknown table property [" + prop + "] in ModifyTableAction setValue");
            }
        }
	}

	protected static function getValue(table:AbstractTable, prop:String, row:int, column:int):Object {
		switch(prop) {
			case "allStateValues":
				return table.getStateValue(row, column);
				break;
			case "allTweenValues":
				return table.getTweenValue(row, column);
				break;
			default:
				LogService.error("Unknown table property [" + prop + "] in ModifyTableAction getValue");
				return null;
		}
	}

	protected static function getName(table:AbstractTable, prop:String, row:int, column:int):Object {
		switch(prop) {
			case "allStateValues":
				return "State Value[" + (row+1) + "][" + (column+1) + "]";
				break;
			case "allTweenValues":
					switch(row) {
						case 0:
							return "Tween type[" + (column+1) + "]";
						case 1:
							return "Tween time[" + (column+1) + "]";
						case 2:
							return "Tween hold time[" + (column+1) + "]";
						default:
							LogService.error("Unknown tween row [" + row + "] in ModifyTableAction getName");
							return "Tween Value[" + (row+1) + "][" + (column+1) + "]";
					}
				break;
			default:
				LogService.error("Unknown table property [" + prop + "] in ModifyTableAction getName");
				return null;
		}
	}

	public static function fromObject(table:AbstractTable, prop:String, row:int, column:int, oldValue:Object, title:String=null):ModifyTableAction {
		var newValue:Object = getValue(table, prop, row, column);
		return new ModifyTableAction(table.uid, prop, oldValue, newValue, (title != null)?title:("Set " + getName(table, prop, row, column) + " on " + table.title), row, column, true);
	}

	override public function perform():void {
		var table:AbstractTable = world.findObjByUID(_objectUID) as AbstractTable;
		setValue(table, _ribbon, _row, _column, ModifyValueAction.valueFromJson(_newValue));
	}

	override public function rollback():void {
		var table:AbstractTable = world.findObjByUID(_objectUID) as AbstractTable;
		setValue(table, _ribbon, _row, _column, ModifyValueAction.valueFromJson(_oldValue));
	}

	override public function merge(action:UserAction):Boolean {
		if(action is ModifyTableAction) {
			var other:ModifyTableAction = action as ModifyTableAction;
			if(this._objectUID == other._objectUID && this._ribbon == other._ribbon && this._row == other._row && this._column == other._column) {
				this._newValue = other._newValue;
				return true;
			}
		}
		return false;
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "ModifyTableAction";
		result.row = _row;
		result.column = _column;
		return result;
	}

	public static function fromJson(json:Object):ModifyTableAction {
		return new ModifyTableAction(json.objectUID, json.ribbon, json.oldValue, json.newValue, json.title, json.row, json.column, false);
	}
}
}
