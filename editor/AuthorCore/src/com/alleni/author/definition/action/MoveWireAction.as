package com.alleni.author.definition.action {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;

public class MoveWireAction extends UserAction {

	protected var wireUID:String;
	protected var unattachedMaster:Boolean;
	protected var oldAnchorHostUID:String;
	protected var oldAnchorPath:String;
	protected var newAnchorHostUID:String
	protected var newAnchorPath:String;

	public function MoveWireAction(wireUID:String, unattachedMaster:Boolean, oldAnchorHostUID:String, oldAnchorPath:String, newAnchorHostUID:String, newAnchorPath:String) {
		this.wireUID = wireUID;
		this.unattachedMaster = unattachedMaster;
		this.oldAnchorHostUID = oldAnchorHostUID;
		this.oldAnchorPath = oldAnchorPath;
		this.newAnchorHostUID = newAnchorHostUID;
		this.newAnchorPath = newAnchorPath;
	}

	public static function fromWire(wire:Wire, unattachedMaster:Boolean, oldAnchor:WireAnchor, newSlave:WireAnchor):MoveWireAction {
		return new MoveWireAction(wire.uid, unattachedMaster, oldAnchor.hostObject.uid, oldAnchor.path, newSlave.hostObject.uid, newSlave.path);
	}

	override public function get title():String {
		return "Move wire";
	}

	override public function perform():void {
		var wire:Wire = wiring.findWireByUID(wireUID);
		var host:AbstractObject = world.findObjByUID(newAnchorHostUID);
		var anchor:WireAnchor = WireAnchor.findByPath(host, newAnchorPath);
		if(wire == null || host == null || anchor == null) {
			return;
		}

		wire.unattachEnd(unattachedMaster);
		wire.finish(anchor);
		ApplicationController.instance.wireController.requestRedrawWire(wire);
	}

	override public function rollback():void {
		var wire:Wire = wiring.findWireByUID(wireUID);
		var host:AbstractObject = world.findObjByUID(oldAnchorHostUID);
		var anchor:WireAnchor = WireAnchor.findByPath(host, oldAnchorPath);
		if(wire == null || host == null || anchor == null) {
			return;
		}
		if(ApplicationController.instance.wireController.wireExists(wire.masterAnchor, anchor as WireAnchor)) {
			// the wire we are trying to undo to already exists, we can't make another
			return;
		}

		var newIsMaster:Boolean = newAnchorHostUID == wire.masterAnchor.hostObject.uid && newAnchorPath == wire.masterAnchor.path;

		// null out and check wired on the anchor that is going away
		var old:WireAnchor;
		if(newIsMaster) {
			old = wire.masterAnchor;
			wire.masterAnchor = null;
		} else {
			old = wire.slaveAnchor;
			wire.slaveAnchor = null;
		}
		old.wired = ApplicationController.instance.wireController.getWiresForAnchor(old).length>0;

		// put the anchor that was not just nulled in the correct spot if it isn't
		// then put the just found anchor in the other spot.
		if(unattachedMaster) {
			if(!newIsMaster) {
				wire.slaveAnchor = wire.masterAnchor;
			}
			wire.masterAnchor = anchor;
		} else {
			if(newIsMaster) {
				wire.masterAnchor = wire.slaveAnchor;
			}
			wire.slaveAnchor = anchor;
		}
		ApplicationController.instance.wireController.requestRedrawWire(wire);
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "MoveWireAction";
		result.wireUID = wireUID;
		result.unattachedMaster = unattachedMaster;
		result.oldAnchorHostUID = oldAnchorHostUID;
		result.oldAnchorPath = oldAnchorPath;
		result.newAnchorHostUID = newAnchorHostUID;
		result.newAnchorPath = newAnchorPath;
		return result;
	}

	public static function fromJson(json:Object):ModifyRibbonAction {
		return new ModifyRibbonAction(json.wireUID, json.unattachedMaster, json.oldAnchorHostUID, json.oldAnchorPath, json.newAnchorHostUID, json.newAnchorPath);
	}
}
}