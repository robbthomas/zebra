package com.alleni.author.definition.action {
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.AbstractContainer;

public class ObjectLocation {

	public var x:Number;
	public var y:Number;
	public var rotation:Number;
	public var parentID:String;
	public var zIndex:int;


	public function ObjectLocation(x:Number, y:Number, rotation:Number, parentID:String, zIndex:int) {
		this.x = x;
		this.y = y;
		this.rotation = rotation;
		this.parentID = parentID;
		this.zIndex = zIndex;
	}

	public function apply(world:World, obj:AbstractObject):void {
		var parent:AbstractContainer = world.findObjByUID(parentID) as AbstractContainer;
		var oldParent:AbstractContainer = obj.parent as AbstractContainer;
        if(parent != oldParent) {
//            trace("ObjectLocation:apply parent was="+obj.parent, "newParent="+parent);
            obj.model.changeParent(parent.model);
        }
        var newIndex:int = Math.min(zIndex, parent.children.length-1);
		if(parent.children.getItemIndex(obj) != newIndex) {
            parent.model.setChildIndex(obj, newIndex);
		}
		obj.x = x;
		obj.y = y;
		obj.rotation = rotation;
        obj.setInitialZIndex();
        oldParent.setChildrenZPositions(true);
        parent.setChildrenZPositions(true);
//        trace("ObjectLocation applied: obj="+obj, "parentID="+parentID, "parent="+obj.parent, "parentUID="+obj.parent.uid);
	}

	public static function fromObject(obj:AbstractObject):ObjectLocation {
		var parentID:String = null;
		var zIndex:int = 0;
		var parent:AbstractContainer = obj.parent as AbstractContainer;
		if(parent) {
			parentID = parent.uid;
			zIndex = parent.children.getItemIndex(obj);
		}
		return new ObjectLocation(obj.x, obj.y, obj.rotation, parentID, zIndex);
	}

	public function toJSON():Object {
		return {
			type:"ObjectLocation",
			x:x,
			y:y,
			rotation:rotation,
			parentID:parentID,
			zIndex:zIndex
		}
	}

	public static function fromJson(json:Object):ObjectLocation {
		return new ObjectLocation(json.x, json.y, json.rotation, json.parentID, json.zIndex);
	}
}
}