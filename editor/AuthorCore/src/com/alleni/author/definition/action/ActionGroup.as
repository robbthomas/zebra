package com.alleni.author.definition.action {

/**
 * Action specifying some ordered grouping of sub actions.
 */
public class ActionGroup extends UserAction {

	private var _children:Vector.<UserAction> = new Vector.<UserAction>();

	private var _cursor:int = -1;

	private var _title:String;

	public function ActionGroup(title:String) {
		super();
		_title = title;
	}


	public override function get title():String {
		return _title;
	}

	private function truncate():void {
		if(_cursor >= 0) {
			// boo stop doing this and implement branching undo
			_children.splice(_cursor, _children.length - _cursor);
			_cursor = -1;
		}
	}

	public function add(action:UserAction):void {
		truncate();
		var prev:UserAction = (_children.length > 0) ? (_children[_children.length-1]) : null;
		// attempt to merge this action with the previous one
		// if it works then we don't need this action added
		if(parent == null || prev == null || !prev.merge(action)) {
			_children.push(action);
			action.parent = this;
		}
	}

	public function removeOne():void {
		if(canRollbackOne()) {
			_cursor--;
			if(_cursor < 0) {
				_cursor = _children.length-1;
			}
		}
		truncate();
	}

	public function get length():int {
		return _children.length;
	}

	public function canPerformOne():Boolean {
		return _cursor >= 0 && _cursor < _children.length;
	}

	public function canRollbackOne():Boolean {
		return (_cursor > 0 && _cursor <= _children.length) || (_cursor < 0 && _children.length > 0);
	}

	public function performOne():void {
		if(canPerformOne()) {
			_children[_cursor].perform();
			_cursor++;
		}
	}

	public function performOneName():String {
		if(canPerformOne()) {
			return _children[_cursor].title;
		} else {
			return "";
		}
	}

	public function rollbackOne():void {
		if(canRollbackOne()) {
			_cursor--;
			if(_cursor < 0) {
				_cursor = _children.length-1;
			}
			_children[_cursor].rollback();
		}
	}

	public function rollbackOneName():String {
		if(canRollbackOne()) {
			var prev:int = _cursor-1;
			if(prev < 0) {
				prev = _children.length-1;
			}
			return _children[prev].title;
		} else {
			return "";
		}
	}

	override public function perform():void {
		for(var i:int=0; i<_children.length; i++) {
			_children[i].perform();
		}
	}

	override public function rollback():void {
		for(var i:int=_children.length-1; i>=0; i--) {
			_children[i].rollback();
		}
	}

	override public function toJSON():Object {
		var result:Object = super.toJSON();
		result.kind = "ActionGroup";
		result.children = [];
		for(var i:int=0; i<_children.length; i++) {
			result.children.push(_children[i].toJSON());
		}
		return result;
	}

	public static function fromJson(json:Object):ActionGroup {
		var result:ActionGroup = new ActionGroup(json.title);
		for each(var child:Object in json.children) {
			var action:UserAction = UserAction.fromJson(child);
			action.parent = result;
			result._children.push(action);
		}
		return result;
	}
}
}