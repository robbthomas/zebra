package com.alleni.author.definition.action {

import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ui.WireAnchor;

/**
 * Action which sets a property for example through a ribbon control.
 * Changes will propagate on wires.
 */
public class ModifyRibbonAction extends ModifyValueAction {

	protected var _ribbon:String;


	public function ModifyRibbonAction(objectUID:String, ribbon:String, oldValue:Object, newValue:Object, title:String, jsonify:Boolean) {
		super(objectUID, oldValue,  newValue, title,  jsonify);
		_ribbon = ribbon;
	}

	public static function fromObject(object:AbstractObject, ribbon:String, oldValue:Object, title:String=null):ModifyRibbonAction {
		var anchor:WireAnchor = WireAnchor.findByPath(object, ribbon);
		return new ModifyRibbonAction(object.uid, ribbon, oldValue, anchor.value, (title != null)?title:("Set " + anchor.modifierDescription.label + " on " + object.title), true);
	}

    public static function fromAnchor(anchor:WireAnchor, oldValue:Object, newValue:Object = null, title:String=null):ModifyRibbonAction {
        if(newValue == null) {
            newValue = anchor.value;
        }
   		return new ModifyRibbonAction(anchor.hostObject.uid, anchor.path, oldValue, newValue, (title != null)?title:("Set " + anchor.modifierDescription.label + " on " + anchor.hostObject.title), true);
   	}

	override public function perform():void {
		var anchor:WireAnchor = WireAnchor.findByPath(world.findObjByUID(_objectUID), _ribbon);
		anchor.value = ModifyValueAction.valueFromJson(_newValue);
	}

	override public function rollback():void {
		var anchor:WireAnchor = WireAnchor.findByPath(world.findObjByUID(_objectUID), _ribbon);
        if (anchor) {    // because of green-threading of undelete, obj may not yet exist
		    anchor.value = ModifyValueAction.valueFromJson(_oldValue);
        }
	}

	override public function merge(action:UserAction):Boolean {
		if(action is ModifyRibbonAction) {
			var other:ModifyRibbonAction = action as ModifyRibbonAction;
			if(this._objectUID == other._objectUID && this._ribbon == other._ribbon) {
				this._newValue = other._newValue;
				return true;
			}
		}
		return super.merge(action);
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "ModifyRibbonAction";
		result.ribbon = _ribbon;
		return result;
	}

	public static function fromJson(json:Object):ModifyRibbonAction {
		return new ModifyRibbonAction(json.objectUID, json.ribbon, json.oldValue, json.newValue, json.title, false);
	}
}
}