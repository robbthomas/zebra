package com.alleni.author.definition.action
{
	import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.AbstractContainer;
	import com.alleni.author.persistence.GadgetDecoder;
	import com.alleni.author.persistence.GadgetEncoder;
	import com.alleni.taconite.model.TaconiteModel;
	
	public class ObjectExistenceAction extends UserAction
	{
		protected var numObjects:int;
	
		protected var serialized:Object;
	
		public function ObjectExistenceAction(serialized:Object)
		{
			super();
			this.serialized = serialized;
			numObjects = serialized.children.length;
		}
	
		protected function get name():String
		{
			if(numObjects==1) {
				return serialized.currentValues.children[0].values.title;
			}
			return serialized.children.length + " objects";
		}
	
		protected static function createSerialized(models:Vector.<TaconiteModel>):Object {
			return GadgetEncoder.buildDeleteContentJson(models);
		}
	
		protected function create():void
		{
			GadgetDecoder.makeUndeleteContent(serialized, function():void {
                LibraryController.instance.updateForAssetChange(true);     // onComplete update reference counts
            });
		}
	
		protected function destroy():void
		{
			var w:World = world;
			for each(var id:String in serialized.ids) {
				var obj:AbstractObject = w.findObjByUID(id);
				ApplicationController.instance.authorController.handleObjectRemoval(obj);
				obj.controller.warnBeforeDelete(obj);
				var parent:AbstractContainer = obj.model.parent.value as AbstractContainer;
				obj.model.parent.removeValueChildAndDescendants(obj);
				parent.setChildrenZPositions(true);
			}
		}
	}
}