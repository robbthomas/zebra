package com.alleni.author.definition.action {
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;

public class ObjectShape {

	public var x:Number;
	public var y:Number;
	public var width:Number;
	public var height:Number;


	public function ObjectShape(x:Number, y:Number, width:Number, height:Number) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public function apply(obj:AbstractObject):void {
		obj.x = x;
		obj.y = y;
		obj.width = width;
		obj.height = height;
	}

	public static function fromObject(obj:AbstractObject):ObjectShape {
		return new ObjectShape(obj.x, obj.y, obj.width, obj.height);
	}

	public function toJSON():Object {
		return {
			type:"ObjectShape",
			x:x,
			y:y,
			width:width,
			height:height
		}
	}

	public static function fromJson(json:Object):ObjectShape {
		return new ObjectShape(json.x, json.y, json.width, json.height);
	}
}
}