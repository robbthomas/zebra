package com.alleni.author.definition.action
{
	import com.alleni.author.controller.objects.TableController;
	import com.alleni.author.controller.objects.TableHeaderDragMediator;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.AnswerTable;
	import com.alleni.author.view.objects.tables.AbstractTableView;

	public class TableRearrangeAction extends UserAction
	{
		private var _tableUid:String;
		private var _oldIndex:int;
		private var _newIndex:int;
		private var _values:Object;
		private var _section:String;
		private var _copying:Boolean;
		private var _title:String;
		
		/**
		 * if the index is:
		 * > 0 new row or column location
		 * = -1 "now" column
		 * = -2 row or column deletion
		 **/
		
		public function TableRearrangeAction(tableUid:String, section:String, copying:Boolean, oldIndex:int, newIndex:int, values:Object)
		{
			super();
			_tableUid = tableUid;
			_section = section;
			_copying = copying;
			_oldIndex = oldIndex;
			_newIndex = newIndex;
			_values = values;
			_title = "Table Rearrange";
		}
		
		override public function get title():String {
			return _title;
		}
		
		public override function perform():void{
			//New is the new location of the row or column
			//Old is the old location of the row or column
			rearrange(_newIndex, _oldIndex);
		}
		
		public override function rollback():void{
			rearrange(_oldIndex, _newIndex);
		}
		
		private function rearrange(toIndex:int, fromIndex:int):void
		{
			var table:AbstractTable = world.findObjByUID(_tableUid) as AbstractTable;;
			if(!table){
				return;
			}
			table.fireBranches = false;
			
			var prevColumn:int = table.column -1;

			table.rearranging = true;
			
			if(fromIndex >= 0 && !_copying) {
				switch(_section) {
					case TableHeaderDragMediator.COLUMNS:
						table.removeColumn(fromIndex);
						break;
					case TableHeaderDragMediator.LOGIC_ROWS:
						(table as AnswerTable).removeLogicRow(fromIndex);
						break;
					case TableHeaderDragMediator.VALUE_ROWS:
						table.removeValueRow(fromIndex);
						break;
				}
			}
			
			if(toIndex >= 0) { //&& fromIndex != -2
				switch(_section) {
					case TableHeaderDragMediator.COLUMNS:
						table.insertColumn(toIndex, _values);
						break;
					case TableHeaderDragMediator.LOGIC_ROWS:
						(table as AnswerTable).insertLogicRow(toIndex, _values);
						break;
					case TableHeaderDragMediator.VALUE_ROWS:
						table.insertValueRow(toIndex, _values);
						break;
				}
			}
			
			if(_section == TableHeaderDragMediator.COLUMNS) {
				if(prevColumn == fromIndex) {
					if(toIndex >= 0) {
						table.column = toIndex+1;
					} else {
						if(!_copying) {
							table.column = 0;
						}
					}
				} else if(fromIndex >= 0) {
					// table is shifting, maybe current column will too
					
					if(toIndex < 0) {
						// removed a column
						if(prevColumn > fromIndex && !_copying) {
							// removed a column that was before the previously selected column
							table.column--;
						}
					} else if(prevColumn > fromIndex && prevColumn <= toIndex) {
						// moved column left to right across previously selected column
						if(!_copying) {
							table.column = prevColumn-1 + 1;
						}
					} else if(prevColumn < fromIndex && prevColumn >= toIndex) {
						// moved column right to left across previously selected column
						table.column = prevColumn+1 + 1;
					}
				} else {
					// dragging now column
					if(toIndex <= prevColumn) {
						// inserted column before the previously selected column
						table.column++
					}
				}
			}
			
			table.copySectionToInitialValues(_section);
			
			table.rearranging = false;
		}
		
		public static function fromObject(table:AbstractTable, section:String, copying:Boolean, oldIndex:int, newIndex:int):TableRearrangeAction{
			var values:Object = null;
			
			if(newIndex >= 0 || newIndex == -2) {
				switch(section) {
					case TableHeaderDragMediator.COLUMNS:
						values = table.copyColumn(oldIndex);
						break;
					case TableHeaderDragMediator.LOGIC_ROWS:
						values = (table as AnswerTable).copyLogicRow(oldIndex);
						break;
					case TableHeaderDragMediator.VALUE_ROWS:
						values = table.copyValueRow(oldIndex);
						break;
				}
			}
			return new TableRearrangeAction(table.uid, section, copying, oldIndex, newIndex, values);
		}
		
		override public function toJSON():Object {
			var result:Object =  super.toJSON();
			result.kind = "TableRearrangeAction";
			result.tableUid = _tableUid;
			result.section = _section;
			result.copying = _copying;
			result.oldIndex = _oldIndex;
			result.newIndex = _newIndex;
			result.values = _values;
			return result;
		}
		
		public static function fromJson(json:Object):TableRearrangeAction {
			return new TableRearrangeAction(json.tableUid, json.section, json.copying, json.oldIndex, json.newIndex, json.values);
		}
	}
}