package com.alleni.author.definition.action {
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;

public class DestroyWireAction extends WireExistenceAction {
	public function DestroyWireAction(serialized:Object) {
		super(serialized);
	}

	override public function get title():String {
		return "Delete Wire";
	}

	override public function perform():void {
		destroy();
	}

	override public function rollback():void {
		create();
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "DestroyWireAction";
		result.object = serialized;
		return result;
	}

	public static function fromDraggedWire(wire:Wire, unattachedMaster:Boolean, oldAnchor:WireAnchor):DestroyWireAction {
		if(unattachedMaster) {
			wire.slaveAnchor = wire.masterAnchor;
			wire.masterAnchor = oldAnchor;
		} else {
			wire.slaveAnchor = oldAnchor;
		}
		return fromWire(wire);
	}

    public static function fromWire(wire:Wire):DestroyWireAction {
   		return new DestroyWireAction(createSerialized(wire));
   	}

	public static function fromJson(json:Object):DestroyWireAction {
		return new DestroyWireAction(json.object);
	}
}
}