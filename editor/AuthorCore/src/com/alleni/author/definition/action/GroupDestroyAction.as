package com.alleni.author.definition.action {
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.taconite.model.GroupSelectionObject;

public class GroupDestroyAction extends GroupExistenceAction {
	public function GroupDestroyAction(serialized:Object) {
		super(serialized);
	}

	override public function get title():String {
		return "Ungroup";
	}

	override public function perform():void {
		destroy();
	}

	override public function rollback():void {
		create();
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "GroupDestroyAction";
		result.object = _serialized;
		return result;
	}

	public static function fromGroup(group:GroupSelectionObject):GroupDestroyAction {
		return new GroupDestroyAction(createSerialized(group));
	}

	public static function fromJson(json:Object):GroupDestroyAction {
		return new GroupDestroyAction(json.object);
	}
}
}