/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/23/12
 * Time: 8:07 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.ui.Application;


public class ObjectOrderAction extends UserAction {

    private var _objectUID:String;
    private var _title:String;

    private var _oldValue:int;
    private var _newValue:int;

		
    public function ObjectOrderAction(objectUID:String, title:String, oldValue:int, newValue:int) {
        _objectUID = objectUID;
        _title = title;
        _oldValue = oldValue;
        _newValue = newValue;
    }
		
    override public function get title():String {
        return _title;
    }
		
    public static function fromObject(obj:AbstractObject, title:String, newValue:int):ObjectOrderAction {
        var parent:AbstractContainer = obj.parent;
        var currentIndex:int = parent.objects.getItemIndex(obj);
        return new ObjectOrderAction(obj.uid, title, currentIndex, newValue);
    }
		
    override public function perform():void {
        setIndex(_newValue);
    }
		
    override public function rollback():void {
        setIndex(_oldValue);
    }

    private function setIndex(index:int):void
    {
        var obj:AbstractObject = world.findObjByUID(_objectUID) as AbstractObject;
        trace("ObjectOrderAction: set index="+index, obj);
        obj.parent.model.setChildIndex(obj, index);
        obj.parent.setChildrenZPositions(!Application.running);
    }

    override public function toJSON():Object {
        var result:Object =  super.toJSON();
        result.kind = "ObjectOrderAction";
        result.objectUID = _objectUID;
        result.title = _title;
        result.oldValue = _oldValue;
        result.newValue = _newValue;
        return result;
    }
		
    public static function fromJson(json:Object):ObjectOrderAction {
        return new ObjectOrderAction(json.objectUID, json.title, json.oldValue as int, json.newValue as int);
    }

}
}
