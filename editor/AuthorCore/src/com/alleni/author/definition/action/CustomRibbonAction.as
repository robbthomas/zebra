package com.alleni.author.definition.action {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ui.ExternalSmartWireAnchor;
import com.alleni.author.model.ui.IDependentAnchor;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.taconite.factory.SerializerImplementation;

import mx.utils.ObjectUtil;

public class CustomRibbonAction extends UserAction {
	protected var _objectUID:String;
    protected var _anchorPath:String;
    protected var _anchorKind:String;
	protected var _anchorsJson:Object;
    protected var _valuesJson:Object;
	protected var _creation:Boolean;
	protected var _wiresJson:Array;

	public function CustomRibbonAction(objectUID:String, anchorPath:String, anchorKind:String, anchorsJson:Array, valuesJson:Array, creation:Boolean, wiresJson:Array) {
		_objectUID = objectUID;
        _anchorPath = anchorPath;
        _anchorKind = anchorKind;
		_anchorsJson = anchorsJson;
        _valuesJson = valuesJson;
		_creation = creation;
		_wiresJson = wiresJson;
	}

	public static function fromAnchor(anchor:WireAnchor, creation:Boolean):CustomRibbonAction {
		var s:SerializerImplementation = new SerializerImplementation();
        var anchorsJson:Array = [], valuesJson:Array = [];
		var wires:Vector.<Wire> = new <Wire>[];
        serializeAnchor(anchor, anchorsJson, valuesJson, wires, s);
        var kind:String = anchorsJson[0].kind;
        for each (var thing:* in anchor.hostObject.anchors) {    // save any setters & triggers based on this ribbon
            var dep:IDependentAnchor = thing as IDependentAnchor;
            if (dep && dep.prototypeAnchor == anchor) {
                serializeAnchor(dep as WireAnchor, anchorsJson, valuesJson, wires, s);
            }
        }
        var wiresJson:Array = [];
        for each(var wire:Wire in wires) {
            wiresJson.push(s.serializeWire(wire));
        }
		return new CustomRibbonAction(anchor.hostObject.uid, anchor.path, kind, anchorsJson, valuesJson, creation, wiresJson);
	}

    private static function serializeAnchor(anchor:WireAnchor, anchorsJson:Array, valuesJson:Array, wires:Vector.<Wire>, s:SerializerImplementation):void
    {
        anchorsJson.push(s.serializeWireAnchor(anchor));
        valuesJson.push(anchor.getCurrentValues());

        // get all wires attached to this anchor, child anchors and "other" anchor
        var anchors:Vector.<WireAnchor> = anchor.childAnchors.slice();
        anchors.push(anchor);
        if(anchor is ExternalSmartWireAnchor) {
            anchors.push(ExternalSmartWireAnchor(anchor).other);
        }
        for each(var a:WireAnchor in anchors) {
            for each(var wire:Wire in ApplicationController.instance.wireController.getWiresForAnchor(a)) {
                if (wires.indexOf(wire) < 0) {
                    wires.push(wire);
                }
            }
        }
    }

    private function deserializeAnchor(anchorJson:Object, valuesJson:Object, s:SerializerImplementation):void {
        var obj:AbstractObject = world.findObjByUID(_objectUID);
        if(obj) {
            var anchor:WireAnchor = s.deserializeWireAnchor(anchorJson, obj, -1, false).value as WireAnchor;
            anchor.setCurrentValues(valuesJson);
            anchor.initialValues = ObjectUtil.copy(valuesJson);
            if(anchor.modifierDescription.key in obj.anchors == false) {
                obj.addAnchor(anchor, true);
            }
        }
    }


	override public function get title():String {
		return (_creation?"Create":"Delete") + " custom " + _anchorKind + " ribbon";
	}

	protected function create():void {
		var s:SerializerImplementation = new SerializerImplementation();
        for (var n:int = 0; n < _anchorsJson.length; n++) {
            deserializeAnchor(_anchorsJson[n], _valuesJson[n], s);
        }
        for each(var wire:Object in _wiresJson) {
            s.deserializeWire(wire, wiring, -1);
        }
        ApplicationController.instance.wireController.showWiresForLevel();
	}

	protected function destroy():void {
		var obj:AbstractObject = world.findObjByUID(_objectUID);
		if(obj) {

			var anchor:WireAnchor = WireAnchor.findByPath(obj, _anchorPath);
			if(anchor) {
                for each (var thing:* in obj.anchors) {    // first destroy any setters & triggers based on this ribbon
                    var dep:IDependentAnchor = thing as IDependentAnchor;
                    if (dep && dep.prototypeAnchor == anchor) {
                        destroyAnchor(obj, dep as WireAnchor);
                    }
                }
                destroyAnchor(obj,  anchor);
			}
		}
	}

    private function destroyAnchor(obj:AbstractObject, anchor:WireAnchor):void {
        var anchors:Vector.<WireAnchor> = anchor.childAnchors.slice();
        anchors.push(anchor);
        if(anchor is ExternalSmartWireAnchor) {
            anchors.push(ExternalSmartWireAnchor(anchor).other);
        }
        for each(var a:WireAnchor in anchors) {
            for each(var wire:Wire in ApplicationController.instance.wireController.getWiresForAnchor(a)) {
                ApplicationController.instance.wireController.deleteWire(wire);
            }
        }
        anchor.handleDeleteAnchor();
        obj.removeAnchor(anchor);
    }

	override public function perform():void {
		if(_creation) {
			create();
		} else {
			destroy();
		}
	}

	override public function rollback():void {
		if(_creation) {
			destroy();
		} else {
			create();
		}
	}

	override public function toJSON():Object {
		var result:Object = super.toJSON();
		result.kind = "CustomRibbonAction";
		result.objectUID = _objectUID;
        result.anchorPath = _anchorPath;
        result.anchorKind = _anchorKind;
		result.anchors = _anchorsJson;
        result.values = _valuesJson;
		result.creation = _creation;
        result.wires = _wiresJson;
		return result;
	}

	public static function fromJson(json:Object):CustomRibbonAction {
		return new CustomRibbonAction(json.objectUID, json.anchorPath, json.anchorKind, json.anchors, json.values, json.creation, json.wires);
	}

    public function get creation():Boolean {
        return _creation;
    }
}
}
