/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 11/26/12
 * Time: 3:41 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.model.ui.IReplaceable;

public class LibraryAddAction extends LibraryExistenceAction {
    public function LibraryAddAction(item:IReplaceable) {
        super(item);
    }

    override public function perform():void {
        create();
    }

    override public function rollback():void {
        destroy();
    }

    override public function get title():String {
        return "Add to library: " + _name;
    }

    override public function toJSON():Object {
        var result:Object = super.toJSON();
        result.kind = "LibraryAddAction";
        result.object = _serialized;
        return result;
    }

    public static function fromItem(item:IReplaceable):LibraryAddAction {
        return new LibraryAddAction(item);
    }

    public static function fromJson(json:Object):LibraryAddAction {
        return new LibraryAddAction(itemFromJson(json.object));
    }

}
}
