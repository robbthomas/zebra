/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.definition.action
{
	import com.alleni.author.controller.ui.ToggleButtonGroupController;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.ToggleButton;
	import com.alleni.taconite.persistence.json.JSON;

	public class ToggleButtonGroupAction extends UserAction
	{
		private var _title:String;
		
		private var _oldValue:Object;
		private var _newValue:Object;
		
		
		public function ToggleButtonGroupAction(title:String, oldJson:Object, newJson:Object) {
			_title = title;
			_oldValue = oldJson;
			_newValue = newJson;
		}
		
		override public function get title():String {
			return _title;
		}
		
		public static function fromButtons(list:Vector.<ToggleButton>, title:String, oldJson:Object):ToggleButtonGroupAction {
			return new ToggleButtonGroupAction(title, oldJson, ToggleButtonGroupController.instance.buttonGroupingToJson(list));
		}
		
		override public function perform():void {
			ToggleButtonGroupController.instance.applyButtonGroupingFromJson(_newValue);
		}
		
		override public function rollback():void {
			ToggleButtonGroupController.instance.applyButtonGroupingFromJson(_oldValue);
		}
		
		override public function toJSON():Object {
			var result:Object =  super.toJSON();
			result.kind = "ToggleButtonGroupAction";
			result.title = _title;
			result.oldValue = com.alleni.taconite.persistence.json.JSON.encode(_oldValue);
			result.newValue = com.alleni.taconite.persistence.json.JSON.encode(_newValue);
			return result;
		}
		
		public static function fromJson(json:Object):ToggleButtonGroupAction {
			return new ToggleButtonGroupAction(json.title, com.alleni.taconite.persistence.json.JSON.decode(json.oldValue), com.alleni.taconite.persistence.json.JSON.decode(json.newValue));
		}
	}
}