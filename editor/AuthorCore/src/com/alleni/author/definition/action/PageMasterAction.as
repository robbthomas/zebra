/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 8/28/12
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.Navigation.IPage;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;

/**
 * Assign a master to a page.
 */
public class PageMasterAction extends UserAction {

    protected var _pageUID:String;
    protected var _masterUID:String;
	protected var _assign:Boolean;  // otherwise unassign


	public function PageMasterAction(pageUID:String, masterUID:String, assign:Boolean) {
		_pageUID = pageUID;
        _masterUID = masterUID;
        _assign = assign;
	}

    public static function fromObject(page:IPage, master:IPage, assign:Boolean):PageMasterAction {
        return new PageMasterAction(page.object.uid, master.object.uid, assign);
    }


    override public function perform():void {
        setAssignment(_assign);
    }

    override public function rollback():void {
        setAssignment(!_assign);
    }

    private function setAssignment(value:Boolean):void {
        if (value) {
        // wait for master to be un-deleted
            PseudoThread.add(new PseudoThreadSimpleRunnable("PageMasterAction.setAssignment", setAssignmentNow, this, [value]));
        } else {
            setAssignmentNow(value);  // do it immediately for benefit of FlowControlbarMediator.hideRolloverMaster()
        }
    }

    private function setAssignmentNow(value:Boolean):void {
        var page:IPage = world.findObjByUID(_pageUID) as IPage;
        var master:IPage = world.findObjByUID(_masterUID) as IPage;

        if (value) {
            if (page.masters.getItemIndex(master) < 0) {
                page.masters.addItem(master);
            }
        } else {  // unassign
            var n:Number = page.masters.getItemIndex(master);
            if (n >= 0) {
                page.masters.removeItemAt(n);
            }
        }
    }

	override public function get title():String {
		return _assign ? "Assign master" : "Unassign master";
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "PageMasterAction";
		result.pageUID = _pageUID;
        result.masterUID = _masterUID;
        result.assign = _assign;
		return result;
	}


    public static function fromJson(json:Object):PageMasterAction {
        return new PageMasterAction(json.pageUID, json.masterUID, json.assign);
    }

}
}