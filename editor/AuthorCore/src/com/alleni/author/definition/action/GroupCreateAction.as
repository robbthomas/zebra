package com.alleni.author.definition.action {
import com.alleni.taconite.model.GroupSelectionObject;

public class GroupCreateAction extends GroupExistenceAction {

	public function GroupCreateAction(serialized:Object) {
		super(serialized);
	}

	override public function get title():String {
		return "Create group";
	}

	override public function perform():void {
		create();
	}

	override public function rollback():void {
		destroy();
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "GroupCreateAction";
		result.object = _serialized;
		return result;
	}

	public static function fromGroup(group:GroupSelectionObject):GroupCreateAction {
		return new GroupCreateAction(createSerialized(group));
	}

	public static function fromJson(json:Object):GroupCreateAction {
		return new GroupCreateAction(json.object);
	}
}
}