package com.alleni.author.definition.action {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.WireController;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.event.WireEvent;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.taconite.factory.SerializerImplementation;
import com.alleni.taconite.model.Notification;
import com.alleni.taconite.model.TaconiteModel;

public class WireExistenceAction extends UserAction { 

	protected var serialized:Object;

	public function WireExistenceAction(serialized:Object) {
		super();
		this.serialized = serialized;
	}

	protected static function createSerialized(wire:Wire):Object {
		return new SerializerImplementation().serializeWire(wire);
	}

	protected function create():void {
		var model:TaconiteModel = new SerializerImplementation().deserializeWire(serialized, wiring, -1);
		if(model != null && Wire(model.value).masterAnchor.modifierDescription is PropertyDescription) {
			if(Wire(model.value).masterAnchor.value != Wire(model.value).slaveAnchor.value) {
				Wire(model.value).slaveAnchor.value = Wire(model.value).masterAnchor.value
			}
		}
		
		Wire(model.value).masterAnchor.host.model.dispatchEvent(new WireEvent(WireEvent.COMPLETE,Wire(model.value)));
		Wire(model.value).slaveAnchor.host.model.dispatchEvent(new WireEvent(WireEvent.COMPLETE,Wire(model.value)));

		ApplicationController.instance.wireController.showWiresForLevel();
	}

	protected function destroy():void {
		var wire:Wire = wiring.findWireByUID(serialized.id);
        if(wire == null) {
            var master:WireAnchor = SerializerImplementation.findAnchor(serialized.masterAnchor, null);
            var slave:WireAnchor = SerializerImplementation.findAnchor(serialized.slaveAnchor, null);

            if(master != null && slave != null){
                wire = ApplicationController.instance.wireController.getExistingWire(master,  slave);
            }
        }
		if(wire) {
			ApplicationController.instance.wireController.deleteWire(wire);
		}
		ApplicationController.instance.wireController.showWiresForLevel();
	}
}
}