package com.alleni.author.definition.action {
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.taconite.model.TaconiteModel;

import mx.events.PropertyChangeEvent;

public class LibraryExistenceAction extends UserAction {

	protected var _serialized:Object;    // {id:id, type:"Asset" or "Gadget"}
    protected var _gadget:Project;
    protected var _asset:Asset;
    protected var _name:String = "";

	public function LibraryExistenceAction(item:IReplaceable) {
		super();
        _gadget = item as Project;   // useful when gadget.id is null because its new, or when GadgetController doesn't yet know about a gadget dragged from dock
        _asset = item as Asset;
        _serialized = createSerialized(item);
        _name = (_gadget) ? _gadget.name : Asset(item).name;
        if (_gadget && _gadget.id == null) {
            _gadget.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, gadgetIdListener);
        }
	}

    private function gadgetIdListener(event:PropertyChangeEvent):void {
        switch (event.property) {
            case "projectId":
                _gadget.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, gadgetIdListener);
                _serialized = createSerialized(_gadget);
                break;
        }
    }

	protected function create():void {
        if (_asset || (_serialized && _serialized.type == "Asset")) {
            var asset:Asset = _asset;
            if (asset == null && _serialized != null) {
                asset = AssetController.instance.getExistingAsset(_serialized.id);
            }
            AssetController.instance.addToModel(asset);
        } else {
            var gadget:Project = _gadget;
            if (gadget == null && _serialized != null) {
                gadget = GadgetController.instance.getExistingGadget(_serialized.id);
            }
            GadgetController.instance.addToModel(gadget);
        }
	}

	protected function destroy():void {
        if (_serialized != null && _serialized.type == "Asset") {
            var asset:Asset = AssetController.instance.getExistingAsset(_serialized.id);
            AssetController.instance.removeFromModel(asset);
        } else {
            var gadget:Project = _gadget;
            if (gadget == null && _serialized != null) {
                gadget = GadgetController.instance.getExistingGadget(_serialized.id);
            }
            GadgetController.instance.removeFromModel(gadget);
        }
	}

    protected static function createSerialized(item:IReplaceable):Object {
        var asset:Asset = item as Asset;
        var gadget:Project = item as Project;
        if (asset) {
            return {type:"Asset", id:asset.id};
        } else if (gadget.projectId != null) {
            return {type:"Gadget", id:gadget.id};
        } else {
            return null;
        }
    }

    protected static function itemFromJson(json:Object):IReplaceable {
        switch (json.type) {
            case "Asset":
                return AssetController.instance.getExistingAsset(json.id);
            case "Gadget":
                return GadgetController.instance.getExistingGadget(json.id);
            default:
                return null;
        }
    }
}
}