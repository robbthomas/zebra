package com.alleni.author.definition.action {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.WireController;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.taconite.model.TaconiteModel;

/**
 * Encapsulates all location changes due to dragging which can also include parent changes and local rotation changes
 */
public class ObjectDragAction extends UserAction {

	private var _objectUID:String;
	private var _objectName:String;

	private var _oldLocation:ObjectLocation;
	private var _newLocation:ObjectLocation;


	public function ObjectDragAction(objectUID:String, objectName:String, oldLocation:ObjectLocation, newLocation:ObjectLocation) {
		_objectUID = objectUID;
		_objectName = objectName;
		_oldLocation = oldLocation;
		_newLocation = newLocation;
	}

	override public function get title():String {
		return "Drag " + _objectName;
	}

	override public function perform():void {
		var w:World = world;
		_newLocation.apply(w, w.findObjByUID(_objectUID));
	}

	override public function rollback():void {
		var w:World = world;
		_oldLocation.apply(w, w.findObjByUID(_objectUID));
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "ObjectDragAction";
		result.objectUID = _objectUID;
		result.objectName = _objectName;
		result.oldLocation = _oldLocation.toJSON();
		result.newLocation = _newLocation.toJSON();
		return result;
	}

	public static function fromObject(object:AbstractObject, oldLocation:ObjectLocation):ObjectDragAction {
		return new ObjectDragAction(object.uid, object.title, oldLocation, ObjectLocation.fromObject(object));
	}

	public static function fromJson(json:Object):ObjectDragAction {
		return new ObjectDragAction(json.objectUID, json.objectName, ObjectLocation.fromJson(json.oldLocation), ObjectLocation.fromJson(json.newLocation));
	}
}
}