/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.definition.action
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.Path;
	import com.alleni.author.model.objects.PathObject;
	import com.alleni.taconite.persistence.json.JSON;
	
	import flash.geom.Point;

	public class PathEditAction extends UserAction
	{
		private var _objectUID:String;
		
		private var _oldValue:String;
		private var _newValue:String;
		private var _oldPosition:String;
		private var _newPosition:String;
		private var _oldAnchor:String;
		private var _newAnchor:String;
		
		
		public function PathEditAction(objectUID:String, oldValue:Path, newValue:Path, oldPosition:Point, newPosition:Point, oldAnchor:Point, newAnchor:Point) {
			_objectUID = objectUID;
			_oldValue = com.alleni.taconite.persistence.json.JSON.encode(oldValue.toJson());
			_newValue = com.alleni.taconite.persistence.json.JSON.encode(newValue.toJson());
			_oldPosition = com.alleni.taconite.persistence.json.JSON.encode(oldPosition);
			_newPosition = com.alleni.taconite.persistence.json.JSON.encode(newPosition);
			_oldAnchor = com.alleni.taconite.persistence.json.JSON.encode(oldAnchor);  // with custom anchorPoint, every path edit adjusts the anchor
			_newAnchor = com.alleni.taconite.persistence.json.JSON.encode(newAnchor);
		}
		
		override public function get title():String {
			return "Edit path";
		}
		
		public static function fromObject(object:PathObject, oldValue:Path, oldPosition:Point, oldAnchor:Point):PathEditAction {
			return new PathEditAction(object.uid, oldValue, object.path, 
					oldPosition, new Point(object.x, object.y),
					oldAnchor, object.anchorPoint.clone());
		}
		
		override public function perform():void {
			var obj:PathObject = world.findObjByUID(_objectUID) as PathObject;
			var path:Path = Path.fromJson(com.alleni.taconite.persistence.json.JSON.decode(_newValue));
			var pos:Point = decodePoint(_newPosition);
			var anchor:Point = decodePoint(_newAnchor);
			obj.restorePath(path, pos, anchor);
		}
		
		override public function rollback():void {
			var obj:PathObject = world.findObjByUID(_objectUID) as PathObject;
			var path:Path = Path.fromJson(com.alleni.taconite.persistence.json.JSON.decode(_oldValue));
			var pos:Point = decodePoint(_oldPosition);
			var anchor:Point = decodePoint(_oldAnchor);
			obj.restorePath(path, pos, anchor);
		}
		
		override public function toJSON():Object {
			var result:Object =  super.toJSON();
			result.kind = "PathEditAction";
			result.objectUID = _objectUID;
			result.oldValue = _oldValue;
			result.newValue = _newValue;
			result.oldPosition = _oldPosition;
			result.newPosition = _newPosition;
			result.oldAnchor = _oldAnchor;
			result.newAnchor = _newAnchor;
			return result;
		}
		
		public static function fromJson(json:Object):PathEditAction {
			return new PathEditAction(json.objectUID, 
				Path.fromJson(com.alleni.taconite.persistence.json.JSON.decode(json.oldValue)), Path.fromJson(com.alleni.taconite.persistence.json.JSON.decode(json.newValue)),
				decodePoint(json.oldPosition), decodePoint(json.newPosition),
				decodePoint(json.oldAnchor), decodePoint(json.newAnchor));
		}
		
		private static function decodePoint(str:String):Point
		{
			var json:Object = com.alleni.taconite.persistence.json.JSON.decode(str);
			return new Point(json.x, json.y);
		}
	}
}