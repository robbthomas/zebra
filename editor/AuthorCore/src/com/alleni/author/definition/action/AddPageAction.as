/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 2/22/11
 * Time: 7:13 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.Navigation.IPage;
import com.alleni.author.Navigation.Pager;
import com.alleni.taconite.model.TaconiteModel;

public class AddPageAction extends PageExistenceAction {
	public function AddPageAction(serialized:Object, pagerUID:String, oldNumbers:PageNumberInfo, newNumbers:PageNumberInfo) {
		super(serialized, pagerUID, oldNumbers, newNumbers);
	}

    override public function perform():void {
        create();
    }

    override protected function creationCompletionCallback():void
    {
        super.creationCompletionCallback();
        applyPageNumbers(_newPageNumbers);
    }

    override public function rollback():void {
        destroy();
        applyPageNumbers(_oldPageNumbers);
    }

	override public function get title():String {
		return "Add "+pageKind;
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "AddPageAction";
		return result;
	}

    public static function fromObject(page:IPage, pager:Pager, oldPageNumbers:PageNumberInfo):AddPageAction {
        var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
        models.push(page.object.model);
        return new AddPageAction(createSerialized(models), pager.uid, oldPageNumbers, PageNumberInfo.fromObject(pager));
    }

    public static function fromSelection(models:Vector.<TaconiteModel>, pager:Pager, oldPageNumbers:PageNumberInfo):AddPageAction {
        return new AddPageAction(createSerialized(models), pager.uid, oldPageNumbers, PageNumberInfo.fromObject(pager));
    }

	public static function fromJson(json:Object):AddPageAction {
		return new AddPageAction(json.object, json.pagerUID, PageNumberInfo.fromJSON(json.oldPageNumbers), PageNumberInfo.fromJSON(json.newPageNumbers));
	}
}
}
