/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/23/12
 * Time: 8:07 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.Navigation.Pager;
import com.alleni.author.persistence.GadgetDecoder;

public class PageExistenceAction extends ObjectExistenceAction {
    private var _serialized:Object;
    private var _pagerUID:String;
    protected var _oldPageNumbers:PageNumberInfo;
    protected var _newPageNumbers:PageNumberInfo;

    public function PageExistenceAction(serialized:Object, pagerUID:String, oldNumbers:PageNumberInfo, newNumbers:PageNumberInfo) {

        super(serialized);
        _serialized = serialized;
        _pagerUID = pagerUID;
        _oldPageNumbers = oldNumbers;
        _newPageNumbers = newNumbers;
    }

    override protected function create():void
    {
        GadgetDecoder.makeUndeleteContent(serialized, creationCompletionCallback);
    }

    protected function get pageKind():String {
        var kind:String;
        if (_serialized.hasOwnProperty("children") && _serialized["children"][0] != null) {
            var firstChild:Object = _serialized["children"][0];
            if (firstChild.className == "EventPage") {
                kind = "Event";
            } else if (firstChild.className == "ArenaPage") {
                kind = "Page";
            } else {
                kind = "some kind of Page"; // shouldn't happen as far as I can tell.
            }
        }
        return kind;
    }

    protected function creationCompletionCallback():void
    {
	}

    protected function applyPageNumbers(numbers:PageNumberInfo):void
    {
        trace("applyPageNumbers", numbers);
        var pager:Pager = world.findObjByUID(_pagerUID) as Pager;
        numbers.applyToPager(pager);
        trace("  applied: p="+pager.pageNumber, "init="+pager.initialPageNumber, "left="+pager.leftPageNumber, "right="+pager.rightPageNumber);
    }

    override public function toJSON():Object {
        var result:Object =  super.toJSON();
        result.pagerUID = _pagerUID;
        result.oldPageNumbers = _oldPageNumbers.toJSON();
        result.newPageNumbers = _newPageNumbers.toJSON();
        return result;
    }


}
}
