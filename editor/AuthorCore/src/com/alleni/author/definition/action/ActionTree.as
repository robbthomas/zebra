package com.alleni.author.definition.action
{
	import com.alleni.taconite.controller.NotificationController;
	import com.alleni.taconite.persistence.json.JSON;
	
	import flash.events.EventDispatcher;
	
	public class ActionTree extends EventDispatcher
	{
		private var _root:ActionGroup;
		private var _currentGroup:ActionGroup;
	
		protected var notificationController:NotificationController;
	
		public function ActionTree()
		{
            clear();
			notificationController = new NotificationController(this);
		}

        public function clear():void {
            _root = new ActionGroup("Root");
          	_currentGroup = _root;
            dispatchEvent(new ActionTreeEvent(ActionTreeEvent.OTHER));
        }
	
		public function openGroup(name:String):Object
		{
			var next:ActionGroup = new ActionGroup(name);
			_currentGroup.add(next);
			_currentGroup = next;
			return next;
		}
	
		public function closeGroup(token:Object):void
		{
			for(var group:ActionGroup = _currentGroup; group != null; group = group.parent) {
				if(group == token) {
					// If in the future groups require explicit closing for any cleanup
					// each group in the chain would need to be closed at this point.
					_currentGroup = group.parent;
					dispatchEvent(new ActionTreeEvent(ActionTreeEvent.COMMIT));
					return;
				}
			}
		}
	
		public function cancelGroup(token:Object):void
		{
			for(var group:ActionGroup = _currentGroup; group != null; group = group.parent) {
				if(group == token) {
					// If in the future groups require explicit closing for any cleanup
					// each group in the chain would need to be closed at this point.
					_currentGroup = group.parent;
					_currentGroup.removeOne();
					dispatchEvent(new ActionTreeEvent(ActionTreeEvent.OTHER));
					return;
				}
			}
		}
		
		public function get currentGroupLength():int
		{
			if (_currentGroup)
				return _currentGroup.length;
			else
				return 0;
		}
	
		public function cancelAction():void
		{
			_currentGroup.removeOne();
		}
	
		public function commit(action:UserAction):void
		{
			_currentGroup.add(action);
			dispatchEvent(new ActionTreeEvent(ActionTreeEvent.COMMIT));
		}
	
		public function undo():void
		{
			_root.rollbackOne();
			dispatchEvent(new ActionTreeEvent(ActionTreeEvent.UNDO));
		}
	
		public function get canUndo():Boolean
		{
			return _root.canRollbackOne();
		}
	
		public function get undoName():String
		{
			return _root.rollbackOneName();
		}
	
		public function redo():void
		{
			_root.performOne();
			dispatchEvent(new ActionTreeEvent(ActionTreeEvent.REDO));
		}
	
		public function get canRedo():Boolean
		{
			return _root.canPerformOne();
		}
	
		public function get redoName():String
		{
			return _root.performOneName();
		}
	
		public function dump():void
		{
			trace(com.alleni.taconite.persistence.json.JSON.encode(_root.toJSON()));
		}
	
		override public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void
		{
			notificationController.addEventListener(type, listener, useCapture, priority, useWeakReference);
			super.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
	
		override public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void
		{
			notificationController.removeEventListener(type, listener, useCapture);            
			super.removeEventListener(type, listener, useCapture);
		}
	}
}