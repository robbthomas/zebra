package com.alleni.author.definition.action {
import com.alleni.author.model.World;
import com.alleni.author.model.ui.Application;

import com.alleni.author.model.ui.Wiring;

import mx.effects.SetPropertyAction;

/**
 * Superclass for all performable user actions on the document including moving objects and setting values.
 */
public class UserAction {

	public var parent:ActionGroup = null;

	public function get title():String {
		return "Unknown Action";
	}

	public function UserAction() {
	}

	public function perform():void {

	}

	public function rollback():void {

	}

	public function merge(action:UserAction):Boolean {
		return false;
	}

	public static function get world():World {
		return Application.instance.document.root.value as World;
	}

	public static function get wiring():Wiring {
		return Application.instance.document.wiringRoot.value as Wiring;
	}

	public function toJSON():Object {
		return {type:"UserAction", kind:"Unknown", title:title};
	}

	public static function fromJson(json:Object):UserAction {
		switch(json.kind) {
			case "ModifyPropertyAction":
				return ModifyRibbonAction.fromJson(json);
		}
		return null;
	}
}
}