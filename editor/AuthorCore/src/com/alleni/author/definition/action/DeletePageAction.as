/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 2/22/11
 * Time: 7:05 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.Navigation.Pager;
import com.alleni.author.model.AbstractObject;
import com.alleni.taconite.model.TaconiteModel;

public class DeletePageAction extends PageExistenceAction {

	public function DeletePageAction(serialized:Object, pagerUID:String, oldNumbers:PageNumberInfo, newNumbers:PageNumberInfo) {
		super(serialized, pagerUID, oldNumbers, newNumbers);
	}

    override public function perform():void {
        destroy();
        applyPageNumbers(_newPageNumbers);
    }

    override public function rollback():void {
        create();
    }

    override protected function creationCompletionCallback():void
    {
        super.creationCompletionCallback();
        applyPageNumbers(_oldPageNumbers);
    }

	override public function get title():String {
		return "Delete "+pageKind;
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "DeletePageAction";
		return result;
	}

	public static function fromObject(page:AbstractObject, pager:Pager, newNumbers:PageNumberInfo):DeletePageAction {
		var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
		models.push(page.model);
		return new DeletePageAction(createSerialized(models), pager.uid, PageNumberInfo.fromObject(pager), newNumbers);
	}

    public static function fromSelection(models:Vector.<TaconiteModel>, pager:Pager, newNumbers:PageNumberInfo):DeletePageAction {
        return new DeletePageAction(createSerialized(models), pager.uid, PageNumberInfo.fromObject(pager), newNumbers);
    }

	public static function fromJson(json:Object):DeletePageAction {
		return new DeletePageAction(json.object, json.pagerUID, PageNumberInfo.fromJSON(json.oldPageNumbers), PageNumberInfo.fromJSON(json.newPageNumbers));
	}
}
}
