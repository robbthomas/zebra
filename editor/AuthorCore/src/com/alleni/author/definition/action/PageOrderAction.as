/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 3/23/12
 * Time: 8:07 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.action {
import com.alleni.author.Navigation.IPage;
import com.alleni.author.Navigation.Pager;
import com.alleni.author.model.AbstractObject;

import mx.collections.IList;

public class PageOrderAction extends UserAction {

    private var _objectUID:String;
    private var _title:String;

    private var _oldValue:int;
    private var _newValue:int;
    private var _oldPageNumbers:PageNumberInfo;
    private var _newPageNumbers:PageNumberInfo;

		
    public function PageOrderAction(objectUID:String, title:String, oldValue:int, newValue:int, oldNumbers:PageNumberInfo, newNumbers:PageNumberInfo) {
        _objectUID = objectUID;
        _title = title;
        _oldValue = oldValue;
        _newValue = newValue;
        _oldPageNumbers = oldNumbers;
        _newPageNumbers = newNumbers;
    }
		
    override public function get title():String {
        return _title;
    }
		
    public static function fromObject(page:IPage, title:String, newValue:int, newPageNumbers:PageNumberInfo):PageOrderAction {
        var obj:AbstractObject = page.object;
        var pager:Pager = page.object.parent as Pager;
        var list:IList = obj.parent.model.findListContainingValueObj(obj);
        var currentIndex:int = list.getItemIndex(obj);
        return new PageOrderAction(obj.uid, title, currentIndex, newValue, PageNumberInfo.fromObject(pager), newPageNumbers);
    }
		
    override public function perform():void {
        trace("PageOrderAction.perform");
        setIndex(_newValue);
        applyPageNumbers(_newPageNumbers);
    }
		
    override public function rollback():void {
        trace("PageOrderAction.rollback");
        setIndex(_oldValue);
        applyPageNumbers(_oldPageNumbers);
    }

    private function setIndex(index:int):void
    {
        var page:IPage = world.findObjByUID(_objectUID) as IPage;
        var pager:Pager = page.object.parent as Pager;
        trace("PageOrderAction: set index="+index, page);
        pager.rearranging = true;
        pager.model.setChildIndex(page.object, index);
        pager.rearranging = false;
    }

    protected function applyPageNumbers(numbers:PageNumberInfo):void
    {
        trace("applyPageNumbers", numbers);
        var page:IPage = world.findObjByUID(_objectUID) as IPage;
        var pager:Pager = page.object.parent as Pager;
        numbers.applyToPager(pager);
        trace("  applied: p="+pager.pageNumber, "init="+pager.initialPageNumber, "left="+pager.leftPageNumber, "right="+pager.rightPageNumber);
    }
		
    override public function toJSON():Object {
        var result:Object =  super.toJSON();
        result.kind = "PageOrderAction";
        result.objectUID = _objectUID;
        result.title = _title;
        result.oldValue = _oldValue;
        result.newValue = _newValue;
        result.oldPageNumbers = _oldPageNumbers.toJSON();
        result.newPageNumbers = _newPageNumbers.toJSON();
        return result;
    }
		
    public static function fromJson(json:Object):PageOrderAction {
        return new PageOrderAction(json.objectUID, json.title, json.oldValue as int, json.newValue as int, PageNumberInfo.fromJSON(json.oldPageNumbers), PageNumberInfo.fromJSON(json.newPageNumbers));
    }

}
}
