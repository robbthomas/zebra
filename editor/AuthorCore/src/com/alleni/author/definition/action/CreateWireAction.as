package com.alleni.author.definition.action {
import com.alleni.author.model.ui.Wire;

public class CreateWireAction extends WireExistenceAction {

	public function CreateWireAction(serialized:Object) {
		super(serialized);
	}

	override public function get title():String {
		return "Create wire";
	}

	override public function perform():void {
		create();
	}

	override public function rollback():void {
		destroy();
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "CreateWireAction";
		result.object = serialized;
		return result;
	}

	public static function fromWire(wire:Wire):CreateWireAction {
		return new CreateWireAction(createSerialized(wire));
	}

	public static function fromJson(json:Object):CreateWireAction {
		return new CreateWireAction(json.object);
	}
}
}