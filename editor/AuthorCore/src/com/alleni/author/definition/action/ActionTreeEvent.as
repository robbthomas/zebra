package com.alleni.author.definition.action {
import flash.events.Event;

public class ActionTreeEvent extends Event {
	public static const TYPE:String = "ActionTreeEvent";
	public static const UNDO:String = "Undo";
	public static const REDO:String = "Redo";
	public static const COMMIT:String = "Commit";
	public static const OTHER:String = "Other";

	public var kind:String;

	public function ActionTreeEvent(kind:String) {
		super(TYPE);
		this.kind = kind;
	}
}
}