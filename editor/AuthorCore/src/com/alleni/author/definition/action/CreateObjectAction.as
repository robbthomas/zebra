package com.alleni.author.definition.action {
import com.alleni.author.model.AbstractObject;
import com.alleni.taconite.model.TaconiteModel;

public class CreateObjectAction extends ObjectExistenceAction {

	public function CreateObjectAction(serialized:Object) {
		super(serialized);
	}

	override public function get title():String {
		return "Create " + name;
	}

	override public function perform():void {
		create();
	}

	override public function rollback():void {
		destroy();
	}

	override public function toJSON():Object {
		var result:Object =  super.toJSON();
		result.kind = "CreateObjectAction";
		result.object = serialized;
		return result;
	}

	public static function fromSelection(models:Vector.<TaconiteModel>):CreateObjectAction {
		return new CreateObjectAction(createSerialized(models));
	}

	public static function fromObject(obj:AbstractObject):CreateObjectAction {
		var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
		models.push(obj.model);
		return new CreateObjectAction(createSerialized(models));
	}

	public static function fromJson(json:Object):CreateObjectAction {
		return new CreateObjectAction(json.object);
	}
}
}