package com.alleni.author.definition
{
	public interface IModifier
	{
		function get key():String;
		function get id():String;
		function get label():String;
		function set label(value:String):void;
		function get description():String;
		function get category():String;
		function set category(value:String):void;
		function get weight():int;
		function get readable():Boolean;
		function get writeable():Boolean;
		function get readOnly():Boolean;
        function get writeOnly():Boolean;
		function get indexed():Boolean;
		function get autoAddUpTo():Number;
	}
}