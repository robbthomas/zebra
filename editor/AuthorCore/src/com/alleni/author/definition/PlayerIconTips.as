package com.alleni.author.definition
{
	public class PlayerIconTips
	{
	    public static const AUDIO:String = "Turn audio on / off";
	    public static const EMBED:String = "Copy code to embed this app";
	    public static const LINK:String = "Copy direct link to this app";
	    public static const EMAIL:String = "Send an email to share this app";
	    public static const POPUP_PLAYER:String = "Open in new window";
	    public static const FULL_SCREEN:String = "Expand to full screen";
	    public static const FULL_SCREEN_KEYBOARD_INACTIVE:String = FULL_SCREEN + " (keyboard is inactive)";
	    public static const NORMAL_SCREEN:String = "Reduce screen";
	}
}
