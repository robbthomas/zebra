package com.alleni.author.definition
{
	public class PositionAndAngle
	{
		public var x:Number;
		public var y:Number;
		public var angle:Number;
		
		public function PositionAndAngle(xx:Number=0, yy:Number=0, aa:Number=0)
		{
			x = xx;
			y = yy;
			angle = aa;
		}
		
		public function isEqualTo(other:PositionAndAngle):Boolean
		{
			// angle=NaN treated as wildcard.  Its produced by path node ribbon.
			return (x == other.x && y == other.y && (angle == other.angle || (isNaN(angle) || isNaN(other.angle))));
		}
		
		public function clone():PositionAndAngle
		{
			return new PositionAndAngle(x,y,angle);
		}
		
		public function toString():String
		{
			return formatNumber(x) + "," + formatNumber(y) + "," + formatNumber(angle);
		}
		
		public function setFromString(string:String):PositionAndAngle
		{
			var comma1:int = string.search("\\,");
			if (comma1 > 0) {
				var remainder:String = string.substr(comma1+1);
				var comma2:int = remainder.search("\\,");
				if (comma2 > 0) {
					var xStr:String = string.substr(0,comma1);
					var yStr:String = remainder.substr(0,comma2);
					var aStr:String = remainder.substr(comma2+1);
					x = Number(xStr);
					y = Number(yStr);
					angle = Number(aStr);
				}
			}
			return this;
		}
		
		private function formatNumber(value:Number):String
		{
			var text:String = value.toString();
			if (text.indexOf(".") != -1) {  // has decimal point
				// limit to one decimal place
				return text.replace(/(\.[0-9])[0-9]*/,"$1");
			} else {
				return text;
			}
		}

		public function toJson():Object
		{
			return {
				type: "PositionAndAngle",
				x: x,
				y: y,
				angle:angle
			};
		}

		public static function fromJson(json:Object):PositionAndAngle {
			return new PositionAndAngle(json.x, json.y, json.angle);
		}

	}
}