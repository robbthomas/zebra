/**
 * Created by IntelliJ IDEA.
 * User: jessecoyle
 * Date: 9/14/11
 * Time: 11:48 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition {
import com.alleni.author.model.ui.VariableContainer;
import com.alleni.author.model.ui.VariableWireAnchor;
import com.alleni.savana.Ast;
import com.alleni.taconite.factory.SerializerImplementation;
import com.alleni.taconite.persistence.json.JSON;

import flash.utils.Dictionary;

public class VariableStore {
    private var variables:Dictionary = new Dictionary();
    private var usedVariables:Dictionary = new Dictionary();
    public var parent:VariableContainer = null;

    public function VariableStore(parent:VariableContainer=null) {
        this.parent = parent;
    }

    private function mark(name:String):void {
        usedVariables[name] = true;
    }

    public function removeVariable(variable:String):void
    {
        var anchor:VariableWireAnchor = parent.getVariableAnchor(variable);
        if(anchor && anchor.wired == false) {
            parent.removeVariableAnchor(anchor);
            delete variables[variable];
            delete usedVariables[variable];
        }
    }

    public function sweep():void {
            var removeXNeeded:Boolean = false;
			for(var variable:String in variables) {
				if(!usedVariables[variable]) {
                    if(variable == 'x') {
                        removeXNeeded = true;
                    } else {
					    removeVariable(variable);
                    }
				}
			}
			var usingAnyNonX:Boolean = false;
            var haveX:Boolean = false;
			for(var variable1:String in variables) {
                if(variable1 == 'x') {
                    haveX = true;
                } else {
                    usingAnyNonX = true;
                    break;
                }
			}
            if(usingAnyNonX) {
                if(removeXNeeded) {
                    removeVariable('x');
                }
            } else {
                if(!haveX) {
                    parent.addVariableAnchor(new VariableWireAnchor(parent, "x", "0"));
                }
            }
    }

    public function setVariableValue(name:String, value:Object):void {
        variables[name] = value;
        if(parent) {
            parent.variableChanged(name);
        }
    }

    public function getVariableValue(name:String):Object {
        if(name in variables) {
            return variables[name];
        } else {
            return name;
        }
    }

    public function registerVariables(vars:Array):void
    {
        for each(var variable:String in vars) {
            if(variable in variables == false) {
                parent.addVariableAnchor(new VariableWireAnchor(parent, variable, variable));
            }
            usedVariables[variable] = true;
        }
    }

    public function unmarkVariables():void
    {
        for(var variable:String in variables) {
            usedVariables[variable] = false;
        }
    }

    public function environment():Object
    {
        var result:Object = Ast.defaultEnvironment();
        for(var key:String in variables) {
            result[key] = variables[key];
        }
        return result;
    }

    public function toJson():Object {
        var vars:Object = {};
        for(var name:String in variables) {
            vars[name] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(variables[name]));
        }
        return {
            type: "VariableStore",
            vars: vars
        };
    }

    public static function fromJson(json:Object, objectPropStorage:Object=null, host:Object=null):VariableStore {
        var result:VariableStore = new VariableStore(host as VariableContainer);
        for(var name:String in json.vars) {
            var value:Object = SerializerImplementation.deserializeValue(json.vars[name], null, objectPropStorage, result.variables,  name);
            result.variables[name] = value;
        }
        return result;
    }

}
}
