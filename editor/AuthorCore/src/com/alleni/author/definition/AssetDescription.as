package com.alleni.author.definition
{
	import com.alleni.author.definition.application.AssetIcons;
	
	import flash.geom.Point;
	import flash.net.FileFilter;

	public class AssetDescription
	{		
		public static const DEFAULT_NAME:String = "Asset";
		public static const ASSET_BOX_REGEXP:RegExp 	= /[\\w\\s]*AssetPaletteItem[\\w\\s]*/;
		public static const DEFAULT_VIDEO_SIZE:Point = new Point(576, 360);
		
		public static const TYPES:Vector.<AssetType> = Vector.<AssetType>([
			 new AssetType(/jpe?g$/i,						AssetType.BITMAP,	"JPG Image",	"image/jpeg")
			,new AssetType(/png$/i, 						AssetType.BITMAP, 	"PNG Image", 	"image/png")
			,new AssetType(/gif$/i,							AssetType.BITMAP, 	"GIF Image", 	"image/gif")
			,new AssetType(/((svg)|(fxg))$/i, 				AssetType.SVG, 		"SVG Image", 	"image/svg+xml")
			,new AssetType(/mp3$/i,							AssetType.AUDIO, 	"MP3 Audio",	"audio/mpeg")
			/*,new AssetType(/((f4a)|(m4a))$/i, 			AssetType.AUDIO, 	"MP4 Audio",	"audio/mp4")*/ // no mp4 for now, OSMF bug
			,new AssetType(/xml$/i, 						AssetType.XML, 		"XML Text", 	"text/xml")
			,new AssetType(/txt$/i, 						AssetType.TEXT, 	"Plain Text", 	"text/plain")
			,new AssetType(/flv$/i,							AssetType.VIDEO, 	"FLV Video", 	"video/x-flv")
			,new AssetType(/((f4v)|(m4v)|(mp4)|(mov))$/i, 	AssetType.VIDEO, 	"MP4 Video", 	"video/mp4") // these containers are supported provided they contain h.264 encoded video
			,new AssetType(/swf$/i, 						AssetType.SWF, 		"SWF Media", 	"application/x-shockwave-flash")
			,new AssetType(/ttf$/i,							AssetType.FONT,		"TrueType Font","application/x-font-truetype")
			,new AssetType(/otf$/i, 						AssetType.FONT, 	"OpenType Font","application/x-font-opentype")
			,new AssetType(/css$/i, 						AssetType.CSS, 		"Cascading Style Sheet","text/css")
		]);
		
		// this should maybe have its own static definition class, but this is better than a string
		// literal in code for now.
		public static const HOSTED_MEDIA_IDENTIFIER:String = "Zebra Server";
		
		public static const YOUTUBE_TITLE:String = "YouTube";
		public static const YOUTUBE_REGEX:RegExp = /.+((youtube)|(youtu.be)).+/i;
		public static const YOUTUBE_ID_REGEX:RegExp = /((youtube\.com\/((watch\?v=)|(embed\/)))|(youtu.be\/))([A-Za-z0-9._%-]*)[\"&\w;=\+_\-]*/i;
		public static const RTMP_REGEX:RegExp = /^rtmp.+/i;
		public static const FLASH_VIDEO_REGEX:RegExp = new RegExp("^((.+\\.((flv)|(f4v)|(m4v)|(mp4)|(mov)))|("+HOSTED_MEDIA_IDENTIFIER+"))", "i");
		
		//public static const FLASH_AUDIO_REGEX:RegExp = new RegExp("^((.+\\.((mp3)|(f4a)|(m4a)))|("+HOSTED_MEDIA_IDENTIFIER+"))", "i"); // no mp4 for now, OSMF bug
		public static const FLASH_AUDIO_REGEX:RegExp = new RegExp("^((.+\\.((mp3)))|("+HOSTED_MEDIA_IDENTIFIER+"))", "i");

		public static const BITMAP_FILE_FILTER:FileFilter 	= new FileFilter("Images (*.jpg, *.jpeg, *.gif, *.png)", "*.jpg;*.jpeg;*.gif;*.png");
		public static const SVG_FILE_FILTER:FileFilter		= new FileFilter("SVG (*.svg)", "*.svg");
		/*public static const AUDIO_FILE_FILTER:FileFilter 	= new FileFilter("Audio (*.mp3, *.f4a, *.m4a)", "*.mp3;*.f4a;*.m4a");*/ // no mp4 for now, OSMF bug
		public static const AUDIO_FILE_FILTER:FileFilter 	= new FileFilter("Audio (*.mp3)", "*.mp3");
		public static const VIDEO_FILE_FILTER:FileFilter 	= new FileFilter("Video (*.flv, *.f4v, *.m4v, *.mp4, *.mov)", "*.flv;*.f4v;*.m4v;*.mp4;*.mov");
		public static const SWF_FILE_FILTER:FileFilter 		= new FileFilter("Flash (*.swf)", "*.swf");
		public static const TEXT_FILE_FILTER:FileFilter 	= new FileFilter("Text (*.txt)", "*.txt");
		public static const CSS_FILE_FILTER:FileFilter 	    = new FileFilter("CSS (*.css)", "*.css");
		//public static const FONT_FILE_FILTER:FileFilter 	= new FileFilter("Fonts (*.ttf, *.otf)", "*.ttf;*.otf"); // disable ttf until ttf->ccf path is in place
		public static const FONT_FILE_FILTER:FileFilter 	= new FileFilter("Fonts (*.otf)", "*.otf");
			
		private static const DEFAULT_MIME_TYPE:String = "application/octet-stream";
		
		/*
		These are things which don't necessarily belong here. These lists were created to be used
		as data providers for UI controls which allow selection by asset type. This was done in order
		to avoid the string literals which were found in a number of places in the UI, which are a
		maintainability problem and a major issue for future internationalization of the UI.
		
		Eventually, these should be moved somewhere else which makes more sense, such as a definition
		class to contain lists and other centralized data for the UI. This class should be reserved
		only for describing assets, not data tailored for consumption by UI elements.
		*/
		private static const _assetSelectionTypes:Array = [AssetType.ANY, AssetType.BITMAP, AssetType.SVG, AssetType.TEXT, AssetType.VIDEO, AssetType.AUDIO];
		private static const _otherSelectionTypes:Array = [AssetType.GADGET];
		
		
		static public function get assetSelectionData():Array
		{
			var newArray:Array = [];
			for each (var type:int in _assetSelectionTypes)
				newArray.push({label:AssetType.getTitle(type), 	data:type,	icon:AssetIcons.getByType(type, false)});
			return newArray;
		}
		
		static private function get otherSelectionData():Array
		{
			var newArray:Array = [];
			for each (var type:int in _otherSelectionTypes)
				newArray.push({label:AssetType.getTitle(type), 	data:type,	icon:AssetIcons.getByType(type, false)});
			return newArray;
		}
		
		static public function get fullSelectionData():Array
		{
			return assetSelectionData.concat(otherSelectionData);
		}
		/*
		end of things which don't necessarily belong here
		*/
		
		// convenience functions
		static public function validateFileType(extension:String):int
		{
			for (var i:int ; i < TYPES.length ; i++) {
				if (TYPES[i].extensionRegExp.exec(extension))
					return TYPES[i].type;
			}
			return -1;	
		}

		static public function validateMimeType(mimeType:String):int
		{
			for (var i:int ; i < TYPES.length ; i++){
				if (mimeType == TYPES[i].mimeType)
					return TYPES[i].type;
			}
			return -1;
		}
		
		static public function getMimeTypeForExtension(extension:String):String
		{
			for (var i:int = 0; i < TYPES.length; i++) {
				if (TYPES[i].extensionRegExp.exec(extension))
					return TYPES[i].mimeType;
			}
			return null;
		}
		
		static public function guessMimeByType(type:int):String
		{
			for (var i:int = 0; i < TYPES.length; i++) {
				if (TYPES[i].type == type)
					return TYPES[i].mimeType;
			}
			return null;
		}
	}
}