package com.alleni.author.definition
{
	public class BorealErrorTypes
	{
		public static const BUSINESS:String = "BUSINESS";
		public static const INTERNAL:String = "INTERNAL";
		
		// convenience methods
		public static function isBusiness(error:Object):Boolean
		{
			return typeForErrorObject(error) == BUSINESS;
		}
		
		public static function isInternal(error:Object):Boolean
		{
			return typeForErrorObject(error) == INTERNAL;
		}
		
		// return type attribute of error object, if it exists
		private static function typeForErrorObject(error:Object):String
		{
			if ("type" in error)
				return error["type"];
			else return null;
		}
	}
}