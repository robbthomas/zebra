package com.alleni.author.definition
{
import com.alleni.author.model.objects.Arena;

public class InspectorRibbonDescriptions
	{
		// Global Ribbons
		public static const AUTHOR_NOTES:Array = ["authorNotes"];
		public static const VISIBILITY_RIBBONS:Array = ["layer", "visible", "cloakOnRun", "hideOnRun", "alpha"];
		public static const GEOMETRY_RIBBONS:Array = ["anchorPoint", "x", "y", "rotation", "width", "height"];
		public static const EFFECTS_RIBBONS:Array = ["shadowLength", "glowLength"];
		
		// Line Ribbons in the Inspector
		public static const LINE_APPEARANCE_RIBBONS:Array = ["lineThickness", "lineColor"];
		
		// Rectangle and Oval Ribbons in the Inspector
		public static const SHAPE_GEOMETRY_RIBBONS:Array = ["anchorPoint", "x", "y", "rotation", "width", "height", "cornerRadius"];
		public static const SHAPE_APPEARANCE_RIBBONS:Array = ["fillSpec", "fillAlpha", "lineThickness", "lineColor", "lineAlpha"];
		
		// Arena Ribbons in the Inspector
		public static const ARENA_VISIBILITY_RIBBONS:Array = ["layer", "visible", "cloakOnRun", "cloakFrame", "hideOnRun", "controlsVisible", "controlsCloaked", "alpha"];
		public static const ARENA_APPEARANCE_RIBBONS:Array = ["fillSpec", "fillAlpha", "lineThickness", "lineColor"];
		public static const ARENA_GEOMETRY_RIBBONS:Array = ["anchorPoint", "x", "y", "scale", "width", "height"];
		public static const ARENA_ALIGN_RIBBONS:Array = ["alignObjsHorz", "alignObjsVert","alignCellsHorz","alignCellsVert", "hGap", "vGap", "vertScrollPolicy", "horzScrollPolicy", "layout"];
		public static const ARENA_ACTIONS_RIBBONS:Array = ["accepting", "releasing"];

        // Event Ribbons in the Inspector
        public static const EVENT_ACTIONS_RIBBONS:Array = ["resetOnEnter", "eventTransition", "transTweenType", "transitionSecs", "moveWithTransition"];

        // a Page of an Arena
        public static const PAGE_INSPECTOR_RIBBONS:Array = Arena.PAGE_LAYOUT_PROPERTIES;

		// Text Ribbons in the Inspector
		public static const TEXT_FONT_RIBBONS:Array = ["fontFamily", "bold", "italic", "underline", "fontSize", "fontColor"];
		public static const TEXT_PARAGRAPH_RIBBONS:Array = ["textAlignH", "textAlignV", "columnCount", "columnGap", "autoSize", "vScrollPolicy"];
		public static const TEXT_APPEARANCE_RIBBONS:Array = ["fillSpec", "fillAlpha", "lineThickness", "lineColor"];
		
		// Button Ribbons in the Inspector
		public static const BUTTON_VISIBILITY_RIBBONS:Array = ["layer", "cloakOnRun", "hideOnRun", "alpha"];
		public static const BUTTON_VALUES_RIBBONS:Array = ["checked", "text", "momentary", "downText"];
		public static const BUTTON_PARAGRAPH_RIBBONS:Array = ["textAlignH", "textAlignV"];
		public static const BUTTON_FONT_RIBBONS:Array = ["fontFamily", "bold", "fontSize", "fontColor"];
		public static const BUTTON_GEOMETRY_RIBBONS:Array = ["anchorPoint", "x", "y", "rotation", "width", "height", "cornerRadius"];
		public static const BUTTON_APPEARANCE_RIBBONS:Array = ["fillSpec", "downColor", "alpha"];
		
		// Radio and Checkbox Ribbons in the inspector
		public static const TOGGLE_VISIBILITY_RIBBONS:Array = ["layer", "hideOnRun"];
		public static const TOGGLE_VALUES_RIBBONS:Array = ["checked", "text", "toggleButtonValue"];
		public static const TOGGLE_FONT_RIBBONS:Array = ["fontFamily", "bold", "fontSize", "fontColor"];
		public static const TOGGLE_APPEARANCE_RIBBONS:Array = ["fillSpec", "downColor", "alpha"];
		
		// Input Text Ribbons in Inspector
		public static const INPUT_VISIBILITY_RIBBONS:Array = ["layer", "cloakOnRun", "hideOnRun", "alpha"];
		public static const INPUT_VALUES_RIBBONS:Array = ["promptText", "numbersOnly", "maxCharCount", "submit"];
		public static const INPUT_FONT_RIBBONS:Array = ["fontFamily", "bold", "fontSize", "fontColor"];
		public static const INPUT_PARAGRAPH_RIBBONS:Array = ["textAlignH", "textAlignV"];
		public static const INPUT_APPEARANCE_RIBBONS:Array = ["fillSpec", "fillAlpha", "lineThickness", "lineColor"];
		
		// Slider Ribbons in Inspector
		public static const SLIDER_VISIBILITY_RIBBONS:Array = ["layer", "hideOnRun"];
		public static const SLIDER_VALUES_RIBBONS:Array = ["sliderPercent", "sliderValue", "sliderMaximum", "sliderMinimum", "sliderUnit", "sliderNudgeAmount", "sliderUnit"];
		public static const SLIDER_GEOMETRY_RIBBONS:Array = ["anchorPoint", "x", "y", "rotation"];
		public static const SLIDER_APPEARANCE_RIBBONS:Array = ["knobColor", "knobAlpha", "fillSpec", "lineThickness", "lineColor"];
		
		// Clock Ribbons in Inspector
		public static const CLOCK_VISIBILITY_RIBBONS:Array = ["layer", "hideOnRun"];
		public static const CLOCK_VALUES_RIBBONS:Array = ["duration", "cycles", "ticksPerCycle"];
		public static const CLOCK_ACTIONS_RIBBONS:Array = ["autorun"];
		public static const CLOCK_APPEARANCE_RIBBONS:Array = ["handColor", "handWeight", "handAlpha", "fillSpec", "fillAlpha", "lineAlpha"];
		
		// Audio Ribbons in Inspector
		public static const AUDIO_VISIBILITY_RIBBONS:Array = ["hideOnRun"];
		public static const AUDIO_VALUES_RIBBONS:Array = ["volume", "uri"];
		public static const AUDIO_ACTIONS_RIBBONS:Array = ["autorun", "preload", "loop"];
		
		// Video Ribbons in Inspector
		public static const VIDEO_VISIBILITY_RIBBONS:Array = ["layer", "cloakOnRun", "hideOnRun", "alpha"];
		public static const VIDEO_VALUES_RIBBONS:Array = ["volume", "uri", "secondsElapsed", "secondsRemaining", "percentElapsed", "percentRemaining"];
		public static const VIDEO_ACTIONS_RIBBONS:Array = ["autorun", "loop"];
		public static const VIDEO_GEOMETRY_RIBBONS:Array = ["anchorPoint", "x", "y", "scale", "rotation", "width", "height", "maintainAspect"];
		public static const VIDEO_APPEARANCE_RIBBONS:Array = ["highQuality"];
		
		// Map Ribbons in Inspector
		public static const MAP_VISIBILITY_RIBBONS:Array = ["layer", "cloakOnRun", "hideOnRun", "alpha"];
		public static const MAP_VALUES_RIBBONS:Array = ["mapType", "latitude", "longitude", "zoom"];
		
		// Truth Table Ribbons in Inspector
		public static const TTABLE_VISIBILITY_RIBBONS:Array = ["visible", "hideOnRun"];
		public static const TTABLE_VALUES_RIBBONS:Array = ["column"];
		public static const TTABLE_SHOW_RIBBONS:Array = ["showValueRows", "showTweens", "poweredOnRun"];
		
		// State Table Ribbons in Inspector
		public static const STABLE_VISIBILITY_RIBBONS:Array = ["visible", "hideOnRun"];
		public static const STABLE_VALUES_RIBBONS:Array = ["column"];
		public static const STABLE_SHOW_RIBBONS:Array = ["showControls", "showTweens", "showBranchingRow", "poweredOnRun"];
		
		// Answer Table Ribbons in Inspector
		public static const ANSWER_VISIBILITY_RIBBONS:Array = ["visible", "hideOnRun"];
		public static const ANSWER_VALUES_RIBBONS:Array = ["column"];
		public static const ANSWER_SHOW_RIBBONS:Array = ["showTweens", "showValueRows", "showBranchingRow", "poweredOnRun"];
		
		// Web Link Ribbons in Inspector
		public static const WEB_LINK_VISIBILITY_RIBBONS:Array = ["visible", "hideOnRun"];
		public static const WEB_LINK_SHOW_RIBBONS:Array = ["newWindow", "jump", "poweredOnRun"];
		
		// Calculator Ribbons in Inspector
		public static const CALCULATOR_SHOW_RIBBONS:Array = ["poweredOnRun"];
		
		// Networking Object Ribbons in Inspector
		public static const NETWORK_VALUE_RIBBONS:Array = ["key", "local"];

        // Path Ribbons in Inspector
		public static const PATH_VISIBILITY_RIBBONS:Array = ["layer","cloakOnRun","hideOnRun","alpha"];
		public static const PATH_ACTIONS_RIBBONS:Array = ["movable","rotatable"];
        public static const PATH_GEOMETRY_RIBBONS:Array = ["anchorPoint", "x", "y", "rotation", "scale", "width", "height"];
        public static const PATH_APPEARANCE_RIBBONS:Array = ["fillSpec", "fillAlpha", "lineColor", "lineThickness", "lineAlpha"];

        // Drawing Ribbons in Inspector
        public static const DRAWING_VISIBILITY_RIBBONS:Array = ["visible", "layer","cloakOnRun","hideOnRun","alpha"];
        public static const DRAWING_ACTIONS_RIBBONS:Array = ["movable","rotatable"];
        public static const DRAWING_GEOMETRY_RIBBONS:Array = GEOMETRY_RIBBONS.concat("maintainAspect","scale");
		
		// Gadget
		public static const COMPOSITE_INSPECTOR_RIBBONS:Array = ["layer","cloakOnRun","hideOnRun","alpha",
			"anchorPoint", "x", "y", "rotation", "scale", "width", "height"];
	}
}