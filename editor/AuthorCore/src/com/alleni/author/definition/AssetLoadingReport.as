package com.alleni.author.definition
{
	public class AssetLoadingReport
	{
		public var remainingCount:int;
		public var progressFraction:Number;

		function AssetLoadingReport(remainingCount:int, progressFraction) {
			this.remainingCount = remainingCount;
			this.progressFraction = progressFraction;
		}
	}
}