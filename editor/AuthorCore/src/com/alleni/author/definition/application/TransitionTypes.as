package com.alleni.author.definition.application
{
	public class TransitionTypes
	{
        public static const NONE:String = "none";
        public static const DEFAULT:String = "defaultTransition";

		public static const WIPE_LEFT_RIGHT:String = "wipe left right";
		public static const WIPE_LEFT_RIGHT_IN:String = "wipe left right in";
		public static const WIPE_RIGHT_LEFT:String = "wipe right left";
		public static const WIPE_TOP_BOTTOM:String = "wipe top bottom";
		public static const WIPE_BOTTOM_TOP:String = "wipe bottom top";
		
		public static const SLIDE_LEFT_RIGHT:String = "slide left right";
		public static const SLIDE_RIGHT_LEFT:String = "slide right left";
		public static const SLIDE_TOP_BOTTOM:String = "slide top bottom";
		public static const SLIDE_BOTTOM_TOP:String = "slide bottom top";
		
		public static const FADE_IN:String = "fade in";
		public static const FADE_OUT:String = "fade out";
		public static const FADE_CROSS:String = "fade cross";
		
		public static const BARN_DOOR_OPEN_VERTICAL:String = "barndoor open vertical";
		public static const BARN_DOOR_OPEN_HORIZONTAL:String = "barndoor open horizontal";
		public static const BARN_DOOR_CLOSE_VERTICAL:String = "barndoor close vertical";
		public static const BARN_DOOR_CLOSE_HORIZONTAL:String = "barndoor close horizontal";
		
		public static const ZOOM_IN:String = "zoom in";
		public static const ZOOM_OUT:String = "zoom out";
		
		public static const IRIS_IN_CIRCLE:String = "iris in circle";
		public static const IRIS_OUT_CIRCLE:String = "iris out circle";
		public static const IRIS_IN_SQUARE:String = "iris in square";
		public static const IRIS_OUT_SQUARE:String = "iris out square";
		
		public static const VERTICAL_BLINDS:String = "vertical blinds";
		public static const VENETIAN_BLINDS:String = "venetian blinds";
		
		public static const DISSOLVE_PIXELS:String = "dissolve pixels";
		public static const DISSOLVE_SQUARES:String = "dissolve squares";
		public static const DISSOLVE_CIRCLES:String = "dissolve circles";
		
		public static const CHECKER_BOARD:String = "checkerboard";
	}
}