package com.alleni.author.definition.application
{	
	import assets.icons.media.objectRectInnerGradientLine;
	
	import com.alleni.taconite.dev.BitmapUtils;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.filters.GlowFilter;

	public class Icons
	{
		[Embed(source="/assets/icons/squareIconBase.png")]
		private static const SquareBase:Class;
		
		[Embed(source="/assets/icons/audio.png")]
		private static const Audio:Class;
		
		protected static function get audio():Bitmap
		{
			return new Audio();
		}
		
		[Embed(source="/assets/icons/button.png")]
		private static const ButtonIcon:Class;
		
		protected static function get button():Bitmap
		{
			return new ButtonIcon();
		}
		
		[Embed(source="/assets/icons/calculation.png")]
		private static const Calculation:Class;
		
		protected static function get calculation():Bitmap
		{
			return new Calculation();
		}
		
		[Embed(source="/assets/icons/gadget.png")]
		private static const Gadget:Class;
		
		protected static function get gadget():Bitmap
		{
			return new Gadget();
		}
		
		[Embed(source="/assets/icons/graphic.png")]
		private static const Graphic:Class;
		
		protected static function get graphic():Bitmap
		{
			return new Graphic();
		}
		
		[Embed(source="/assets/icons/interaction.png")]
		private static const Interaction:Class;
		
		protected static function get interaction():Bitmap
		{
			return new Interaction();
		}
		
		[Embed(source="/assets/icons/navigation.png")]
		private static const Navigation:Class;
		
		protected static function get navigation():Bitmap
		{
			return new Navigation();
		}
		
		[Embed(source="/assets/icons/question.png")]
		private static const Question:Class;
		
		protected static function get question():Bitmap
		{
			return new Question();
		}
		
		[Embed(source="/assets/icons/simulation.png")]
		private static const Simulation:Class;
		
		protected static function get simulation():Bitmap
		{
			return new Simulation();
		}
		
		[Embed(source="/assets/icons/sketch.png")]
		private static const Sketch:Class;
		
		protected static function get sketch():Bitmap
		{
			return new Sketch();
		}
		
		[Embed(source="/assets/icons/swf.png")]
		private static const SWF:Class;
		
		protected static function get swf():Bitmap
		{
			return new SWF();
		}
		
		[Embed(source="/assets/icons/quizTest.png")]
		private static const QuizTest:Class;
		
		protected static function get quizTest():Bitmap
		{
			return new QuizTest();
		}
		
		[Embed(source="/assets/icons/text.png")]
		private static const Text:Class;
		
		protected static function get text():Bitmap
		{
			return new Text();
		}
		
		[Embed(source="/assets/icons/video.png")]
		private static const Video:Class;
		
		protected static function get video():Bitmap
		{
			return new Video();
		}
		
		protected static function getMediaBase(icon:DisplayObject):DisplayObject
		{
			var base:Sprite = new objectRectInnerGradientLine();
			base.graphics.lineStyle(0, 0, 0, true);
			base.graphics.beginFill(0xbfbfbf, 0.5);
			base.graphics.drawRoundRect(-(48-base.width)/2, -(48-base.height)/2, 48, 48, 6, 6);
			base.addChild(icon);
			icon.x = (base.width - icon.width)/2
			icon.y = (base.height - icon.height)/2
			return base;
		}
		
		protected static function getSquare(icon:DisplayObject):DisplayObject
		{
			return BitmapUtils.composite(new SquareBase(), icon);
		}
	}
}