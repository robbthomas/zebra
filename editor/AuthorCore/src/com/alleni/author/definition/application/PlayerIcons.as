package com.alleni.author.definition.application
{
	import assets.icons.player.*;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;

	public class PlayerIcons
	{
		public static function get hitArea():DisplayObject
		{
			var shape:Shape = new Shape();
			shape.graphics.beginFill(0, 0);
			shape.graphics.drawRect(-5, -5, 25, 25);
			shape.graphics.endFill();
			return shape;
		}
		
		public static function get logoInBlack():DisplayObject
		{
			return new logoBlack();
		}
		
		// bar name
		public static function get barNameIcon():DisplayObject
		{
			return new barName();
		}
		
		// logo
		public static function get logoIcon():DisplayObject
		{
			return new zebraLogoSel();
		}
		public static function get logoIconOver():DisplayObject
		{
			return new zebraLogoOver();
		}
		public static function get logoIconDown():DisplayObject
		{
			return new zebraLogoDown();
		}
		
		// popup
		public static function get popupIcon():DisplayObject
		{
			return new popupSel();
		}
		public static function get popupIconOver():DisplayObject
		{
			return new popupOver();
		}
		public static function get popupIconDown():DisplayObject
		{
			return new popupDown();
		}
		
		// audio
		// ON
		public static function get audioOnIcon():DisplayObject
		{
			return new audioOnSel();
		}
		public static function get audioOnIconOver():DisplayObject
		{
			return new audioOnOver();
		}
		public static function get audioOnIconDown():DisplayObject
		{
			return new audioOnDown();
		}
		// OFF
		public static function get audioOffIcon():DisplayObject
		{
			return new audioOffSel();
		}
		public static function get audioOffIconOver():DisplayObject
		{
			return new audioOffOver();
		}
		public static function get audioOffIconDown():DisplayObject
		{
			return new audioOffDown();
		}
		
		// embed
		public static function get embedIcon():DisplayObject
		{
			return new embedSel();
		}
		public static function get embedIconOver():DisplayObject
		{
			return new embedOver();
		}
		public static function get embedIconDown():DisplayObject
		{
			return new embedDown();
		}
		
		// full screen
		public static function get fullScreenIcon():DisplayObject
		{
			return new fullScreenSel();
		}
		public static function get fullScreenIconOver():DisplayObject
		{
			return new fullScreenOver();
		}
		public static function get fullScreenIconDown():DisplayObject
		{
			return new fullScreenDown();
		}
		
		// regular screen
		public static function get regScreenIcon():DisplayObject
		{
			return new regScreenSel();
		}
		public static function get regScreenIconOver():DisplayObject
		{
			return new regScreenOver();
		}
		public static function get regScreenIconDown():DisplayObject
		{
			return new regScreenDown();
		}
		
		// share
		public static function get shareIcon():DisplayObject
		{
			return new linkSel();
		}
		public static function get shareIconOver():DisplayObject
		{
			return new linkOver();
		}
		public static function get shareIconDown():DisplayObject
		{
			return new linkUnsel();
		}
		
		public static function get zebraZappsEditor():DisplayObject
		{
			return new preloaderLogoNameEditor();
		}
		
		public static function get zebraZappsPlayer():DisplayObject
		{
			return new preloaderLogoNamePowerZapp();
		}
		
		public static function get dialogEmbedWhiteIcon():DisplayObject
		{
			return new dialogEmbedWhite();
		}
		
		public static function get dialogLinkWhiteIcon():DisplayObject
		{
			return new dialogLinkWhite();
		}
	}
}