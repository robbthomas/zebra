package com.alleni.author.definition.application
{
	public class NotificationNamesEditor
	{
		public static const UNDO:String = "Undo";
		public static const REDO:String = "Redo";
		public static const CUT:String = "Cut";
		public static const COPY:String = "Copy";
		public static const PASTE:String = "Paste";
        public static const LOCAL_PASTE:String = "Local Paste";
		public static const SELECT_ALL:String = "Select All";
        public static const SELECT_ALL_WITH_LOCKED:String = "Select All With Locked";
		public static const DESELECT:String = "Deselect";
		public static const DESELECT_OBJECT:String = "Deselect Object";
		public static const SELECT_OBJECT:String = "Select Object";
		public static const DELETE_SELECTED:String = "Delete Selected Object[s]";
		public static const NUDGE_LEFT:String = "Nudge Left";
		public static const NUDGE_RIGHT:String = "Nudge Right";
		public static const NUDGE_UP:String = "Nudge Up";
		public static const NUDGE_DOWN:String = "Nudge Down";
        public static const NUDGE_TEN_LEFT:String = "Nudge Ten Left";
		public static const NUDGE_TEN_RIGHT:String = "Nudge Ten Right";
		public static const NUDGE_TEN_UP:String = "Nudge Ten Up";
		public static const NUDGE_TEN_DOWN:String = "Nudge Ten Down";
				
		public static const BRING_FORWARD:String = "Bring Forward";
		public static const BRING_TO_FRONT:String = "Bring To Front";
		public static const SEND_BACK:String = "Send Back";
		public static const SEND_TO_BACK:String = "Send To Back";
		public static const GROUP_ITEMS:String = "Group Items";
		public static const UNGROUP_ITEMS:String = "Ungroup Items";
		
		public static const ALIGN_EDGES_LEFT:String = "Align Edges- Left";
		public static const ALIGN_EDGES_RIGHT:String = "Align Edges- Right";
		public static const ALIGN_EDGES_TOP:String = "Align Edges- Top";
		public static const ALIGN_EDGES_BOTTOM:String = "Align Edges- Bottom";
		public static const ALIGN_CENTERS_VERTICAL:String = "Align Centers- Vertical";
		public static const ALIGN_CENTERS_HORIZONTAL:String = "Align Centers- Horizontal";
		public static const DISTRIBUTE_VERTICAL:String = "Distribute- Vertical";
		public static const DISTRIBUTE_HORIZONTAL:String = "Distribute- Horizontal";
		
        public static const MAKE_OBJECT_OPEN:String = "Make Object Open";
        public static const MAKE_OBJECT_CLOSE:String = "Make Object Close";
		public static const UNMAKE_OBJECT:String = "Unmake Object";
		public static const MAKE_TOGGLEBUTTON_GROUP:String = "Make Button Group";
		public static const UNMAKE_TOGGLEBUTTON_GROUP:String = "Unmake Button Group";
		public static const MAKE_GRAPHIC:String = "Make Graphic";
		public static const MAKE_ARENA:String = "Make Arena";
		public static const OBJECT_LOCK_CHILDREN:String = "Object Lock Children";
        public static const EDIT_SELECTED_OBJECT:String = "Edit Selected Object";

        public static const CLOSE_ONE_EDITOR:String = "Enter Key Fired";
		
		public static const CLOSE_UNFOCUSED_RIBBON_CONTAINERS:String = "Opening a different ribbon container";

        public static const RIGHT_CLICK_FIRED:String = "Right Click Fired"; //Passes point with mouseX and mouseY
        public static const HIDE_RIGHT_CLICK_MENU:String = "Hide Right Click Menu";

        public static const UPDATE_NOTE_VISIBILITY:String = "Update Note Visibility";
	}
}
