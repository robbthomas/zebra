package com.alleni.author.definition.application
{
	import assets.icons.publishing.*;
	
	import flash.display.DisplayObject;

	public class PublishIcons
	{
		public static const SIZE:int = 20;
		
		// icon for unpublished gadget feedback
		public static function get feedbackCloudUnpublished():DisplayObject
		{
			var icon:DisplayObject =  new cloudUnpubUp();
			icon.x = -icon.width/2;
			icon.y = -icon.height/2;
			return icon;
		}
		
		public static function get feedbackCloudUnpublishedOver():DisplayObject
		{
			var icon:DisplayObject =  new cloudUnpubOver();
			icon.x = -icon.width/2;
			icon.y = -icon.height/2;
			return icon;
		}
		
		public static function get feedbackCloudUnpublishedDown():DisplayObject
		{
			var icon:DisplayObject =  new cloudUnpubDown();
			icon.x = -icon.width/2;
			icon.y = -icon.height/2;
			return icon;
		}
		
		// icon for published gadget feedback
		public static function get feedbackCloudPublished():DisplayObject
		{
			var icon:DisplayObject =  new cloudPubUp();
			icon.x = -icon.width/2;
			icon.y = -icon.height/2;
			return icon;
		}
		
		public static function get feedbackCloudPublishedOver():DisplayObject
		{
			var icon:DisplayObject =  new cloudPubOver();
			icon.x = -icon.width/2;
			icon.y = -icon.height/2;
			return icon;
		}
		
		public static function get feedbackCloudPublishedDown():DisplayObject
		{
			var icon:DisplayObject =  new cloudPubDown();
			icon.x = -icon.width/2;
			icon.y = -icon.height/2;
			return icon;
		}
		
		public static function get dialogDecoration():DisplayObject
		{
			return new cornerCloud();
		}
		
		public static function get screenshotIconUp():DisplayObject
		{
			return new cameraUp();	
		}
		
		public static function get screenshotIconOver():DisplayObject
		{
			return new cameraOver();	
		}
		
		public static function get screenshotIconDown():DisplayObject
		{
			return new cameraDown();	
		}
		
		public static function get ratings0():DisplayObject
		{
			return new ratingStars0();	
		}
		
		public static function get ratings1():DisplayObject
		{
			return new ratingStars1();	
		}
		
		public static function get ratings2():DisplayObject
		{
			return new ratingStars2();	
		}
		
		public static function get ratings3():DisplayObject
		{
			return new ratingStars3();	
		}
		
		public static function get ratings4():DisplayObject
		{
			return new ratingStars4();	
		}
		
		public static function get ratings5():DisplayObject
		{
			return new ratingStars5();	
		}
	}
}