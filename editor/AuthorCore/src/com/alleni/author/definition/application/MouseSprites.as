package com.alleni.author.definition.application
{
	import flash.display.DisplayObject;
	import flash.filters.DropShadowFilter;

	public class MouseSprites
	{
		[Embed(source="/assets/cursors/decoration/copyOKgreen1.png",compression="true",quality="90")]
		private static var PlusDecoration:Class;
		
		public static function get plus():DisplayObject
		{
			return new PlusDecoration();
		}
	}
}