package com.alleni.author.definition.application
{
	import com.alleni.author.definition.AssetType;
	import com.alleni.taconite.dev.BitmapUtils;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;

	public class AssetIcons extends Icons
	{	
		public static const SIZE:Number = 25.0;
		
		public static const ICONCHIP_FILL_COLOR_1:uint = 0x2F577B;
		public static const ICONCHIP_FILL_COLOR_2:uint = 0xB9DAF4;
		public static const ICONCHIP_SIZE:Number = 50.0;
		
		private static function get allBlack():DisplayObject
		{
			var allIcon:Sprite = new Sprite();
			allIcon.graphics.beginFill(0x000000);
			allIcon.graphics.drawCircle(2.5,2.5,2.5);
			allIcon.graphics.drawCircle(9,2.5,2.5);
			allIcon.graphics.drawCircle(2.5,9,2.5);
			allIcon.graphics.drawCircle(9,9,2.5);
			return allIcon;
		}
		
		private static function get allWhite():DisplayObject
		{
			var allIcon:Sprite = new Sprite();
			allIcon.graphics.beginFill(0xFFFFFF);
			allIcon.graphics.drawCircle(2.5,2.5,2.5);
			allIcon.graphics.drawCircle(9,2.5,2.5);
			allIcon.graphics.drawCircle(2.5,9,2.5);
			allIcon.graphics.drawCircle(9,9,2.5);
			return allIcon;
		}
		
		private static function getScaled(icon:Bitmap):DisplayObject
		{
			return BitmapUtils.bilinearDownscale(icon, SIZE);
		}
		
		private static function getTinted(icon:DisplayObject, color:uint):DisplayObject
		{
			return BitmapUtils.tint(Bitmap(icon), color);
		}
		
		public static function get audio():DisplayObject
		{
			return getScaled(Icons.audio);
		}
		
		public static function get graphic():DisplayObject
		{
			return getScaled(Icons.graphic);
		}
		
		public static function get sketch():DisplayObject
		{
			return getScaled(Icons.sketch);
		}
		
		public static function get swf():DisplayObject
		{
			return getScaled(Icons.swf);
		}
		
		public static function get text():DisplayObject
		{
			return getScaled(Icons.text);
		}
		
		public static function get video():DisplayObject
		{
			return getScaled(Icons.video);
		}
		
		public static function get gadget():DisplayObject
		{
			return getScaled(Icons.gadget);
		}
		
		// if you need to request the inverted version, add that as a param rather than reverting to "icon types" which was a maintainability problem
		// given assets are already indexed.
		public static function getByType(type:int, isDarkUI:Boolean=true):DisplayObject
		{
			switch (type) {
				case AssetType.AUDIO:
					return audio;
					break;
				case AssetType.BITMAP:
					return graphic;
					break;
				case AssetType.SVG:
					return sketch;
					break;
				case AssetType.SWF:
					return swf;
					break;
				case AssetType.VIDEO:
					return video;
					break;
				case AssetType.TEXT:
					if (isDarkUI)
						return getTinted(text, 0xffffff);
					else
						return text;
					break;
				case AssetType.GADGET:
					if (isDarkUI)
						return getTinted(gadget, 0xffffff);
					else
						return gadget;
					break;
				case AssetType.ANY:
				default:
					if (isDarkUI)
						return allWhite;
					else
						return allBlack;
					break;
			}
			/*
			}else if (type == THIS_PROJECT_ICON){
				return AssetIcons.thisProjectIconLight;
			}else if (type == MY_ASSETS_ICON){
				return AssetIcons.myAssetsIconLight;
			}*/
			return null;
		}		
		
		public static function get addAssetButtonUp():DisplayObject
		{
			var shape:Shape = new Shape();
			with (shape.graphics) {
				beginFill(0x000000);
				drawCircle(0,0,7);
				endFill();
				lineStyle(2,0x646464);
				moveTo(-.5,-4);
				lineTo(-.5,4);
				moveTo(-4,-.5);
				lineTo(4,-.5);
			}
			return shape;
		}
		
		public static function get addAssetButtonOver():DisplayObject
		{
			var shape:Shape = new Shape();
			with (shape.graphics) {
				beginFill(0x484848);
				drawCircle(0,0,7);
				endFill();
				lineStyle(2,0xC8C8C8);
				moveTo(-.5,-4);
				lineTo(-.5,4);
				moveTo(-4,-.5);
				lineTo(4,-.5);
			}
			return shape;
		}
		
		public static function get addAssetButtonDown():DisplayObject
		{
			var shape:Shape = new Shape();
			with (shape.graphics) {
				beginFill(0xFFFFFF);
				drawCircle(0,0,7);
				endFill();
				lineStyle(2,0x000000);
				moveTo(-.5,-4);
				lineTo(-.5,4);
				moveTo(-4,-.5);
				lineTo(4,-.5);
			}
			return shape;
		}
		
		public static function get deleteAssetButtonUp():DisplayObject
		{
			var shape:Shape = new Shape();
			with (shape.graphics) {
				beginFill(0x000000);
				drawCircle(0,0,7);
				endFill();
				lineStyle(2,0x646464);
				moveTo(-4,-.5);
				lineTo(4,-.5);
			}
			return shape;
		}
		
		public static function get deleteAssetButtonOver():DisplayObject
		{
			var shape:Shape = new Shape();
			with (shape.graphics) {
				beginFill(0x484848);
				drawCircle(0,0,7);
				endFill();
				lineStyle(2,0xC8C8C8);
				moveTo(-4,-.5);
				lineTo(4,-.5);
			}
			return shape;
		}
		
		public static function get deleteAssetButtonDown():DisplayObject
		{
			var shape:Shape = new Shape();
			with (shape.graphics) {
				beginFill(0xFFFFFF);
				drawCircle(0,0,7);
				endFill();
				lineStyle(2,0x000000);
				moveTo(-4,-.5);
				lineTo(4,-.5);
			}
			return shape;
		}
		
		public static function get thisProjectLight():DisplayObject
		{
			var shape:Shape = new Shape();
			with (shape.graphics) {
				beginFill(0xFFFFFF);
				drawRect(0, 0, 12, 12);
				endFill();
				beginFill(0x000000);
				drawRect(4,4,4,4);
				endFill();
			}
			return shape;
		}
		
		public static function get thisProjectDark():DisplayObject
		{
			var shape:Shape = new Shape();
			with (shape.graphics) {
				beginFill(0x000000);
				drawRect(0, 0, 12, 12);
				endFill();
				beginFill(0xC3C3C3);
				drawRect(4,4,4,4);
				endFill();
			}
			return shape;
		}
		
		public static function get myAssetsDark():DisplayObject
		{
			var shape:Shape = new Shape();
			with (shape.graphics) {
				beginFill(0x000000);
				drawRect(0, 0, 12, 12);
				endFill();
				beginFill(0xC3C3C3);
				drawCircle(3.5,3.5,2);
				drawCircle(8,3.5,2);
				drawCircle(3.5,8,2);
				drawCircle(8,8,2);
			}
			return shape;
		}
		
		public static function get myAssetsLight():DisplayObject
		{
			var shape:Shape = new Shape();
			with (shape.graphics) {
				beginFill(0xFFFFFF);
				drawRect(0, 0, 12, 12);
				endFill();
				beginFill(0x000000);
				drawCircle(3.5,3.5,2);
				drawCircle(8,3.5,2);
				drawCircle(3.5,8,2);
				drawCircle(8,8,2);
			}
			return shape;
		}
		
		/* kept for posterity and reference
		public static function iconChip(width:Number = 0, height:Number = 0, lineColor:uint = 0xffffff):DisplayObject
		{
			if (width == 0) {// default
				width = ICONCHIP_SIZE;
				height = ICONCHIP_SIZE;
			}
			var chipSprite:Sprite = new Sprite();
			var fillModel:GraphicFill = new GraphicFill();
			fillModel.linear = true;
			fillModel.angle = -45;
			fillModel.colors = Vector.<uint>([ICONCHIP_FILL_COLOR_1, ICONCHIP_FILL_COLOR_2]);
			beginGradientFill(chipSprite.graphics, fillModel, width, height,0, 0, 1);
			chipSprite.graphics.lineStyle(1, lineColor, 0.2, true, LineScaleMode.NONE);
			
			chipSprite.graphics.drawRoundRect(0, 0, width, height, 15, 15);
			chipSprite.graphics.endFill();
			
			return chipSprite;
		}
		
		public static function beginGradientFill(graphics:Graphics, fillModel:GraphicFill, fillWidth:Number, fillHeight:Number, tx:Number, ty:Number, fillAlpha:Number ):void
		{
			var ratios:Array = [];
			var alphas:Array = [];
			var gradientType:String = fillModel.linear ? GradientType.LINEAR : GradientType.RADIAL;
			var band:Number = 255 / (fillModel.colors.length-1);  // 3 colors = bands 128 wide
			
			for (var n:int=0; n < fillModel.colors.length; n++) {
				ratios[n] = n * band;
				alphas[n] = fillAlpha;
			}
			
			var angle:Number = 0; 
			if (fillModel.linear) {
				angle = fillModel.angle * Math.PI/180;  // angle in radians
			} else {  // radial gradient
				tx += fillModel.xOffset * fillWidth / 200;  // 100% takes you from center to edge
				ty += fillModel.yOffset * fillHeight / 200;
			}
			
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(fillWidth, fillHeight, angle, tx, ty);
			
			var colorArray:Array = new Array();
			for(var i:int; i < fillModel.colors.length; ++i)
				colorArray[i] = fillModel.colors[i];
			graphics.beginGradientFill(gradientType, colorArray, alphas, ratios, matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
		}*/
	}
}