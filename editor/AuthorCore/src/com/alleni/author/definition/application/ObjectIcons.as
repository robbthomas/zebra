package com.alleni.author.definition.application
{
	import assets.icons.media.objectIconAudio;
	import assets.icons.media.objectIconVideo;
	
	import com.alleni.author.definition.AssetType;
	
	import flash.display.DisplayObject;

	public class ObjectIcons extends Icons
	{
		public static const SIZE:Number = 48.0;
		
		public static function getByType(type:int):DisplayObject
		{
			switch (type) {
				case AssetType.AUDIO:
					return getMediaBase(new objectIconAudio());
					break;
				case AssetType.BITMAP:
					return getMediaBase(graphic);
					break;
				case AssetType.SVG:
					return getMediaBase(sketch);
					break;
				case AssetType.VIDEO:
					return getMediaBase(new objectIconVideo());
					break;
				case AssetType.TEXT:
					return getMediaBase(text);
					break;
				case AssetType.GADGET:
					return getMediaBase(gadget);
					break;
			}
			return null;
		}		
	}
}