package com.alleni.author.definition.application
{
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.definition.GadgetType;
	import com.alleni.taconite.dev.BitmapUtils;
	
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.filters.BevelFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	
	public class DockIcons extends Icons
	{
		public static const SIZE_SMALL:Number = 50.0;
		public static const SIZE_MEDIUM:Number = 75.0;
		private static const CORNER_RADIUS_FRACTION:Number = 0.2;
		
		private static var _iconBevelFilter:BevelFilter;
		private static var _iconGlowFilter:GlowFilter;
		
		private static function get iconBevelFilter():BevelFilter
		{
			if (!_iconBevelFilter)
				_iconBevelFilter = new BevelFilter(2, 80, 0xffffff, 0.5, 0, 0.15, 2, 2, 1, 3);
			return _iconBevelFilter;
		}
		
		private static function get iconGlowFilter():GlowFilter
		{
			if (!_iconGlowFilter)
				_iconGlowFilter = new GlowFilter(0xffffff, 0.5, 10, 10, 1, 3);
			return _iconGlowFilter;
		}
		
		private static function getScaledSquare(icon:DisplayObject, size:Number=SIZE_SMALL):DisplayObject
		{
			const SIZE:Number = size;
			const RADIUS:Number = size*CORNER_RADIUS_FRACTION;
			
			var squareIconBackground:Shape = new Shape();
			squareIconBackground.graphics.lineStyle(1, 0, 0, true);
			
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(size, size, (Math.PI/2), 0, 0);
			squareIconBackground.graphics.beginGradientFill(GradientType.LINEAR, [0xffffff, 0x6b91ba], [1, 1], [0x00, 0x99], matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
			squareIconBackground.graphics.drawRoundRect(0, 0, SIZE, SIZE, RADIUS, RADIUS);
			squareIconBackground.graphics.endFill();
			squareIconBackground.filters = [iconBevelFilter];
			
			icon.filters = [iconGlowFilter];
			
			return BitmapUtils.compositeVectorBase(squareIconBackground, icon);
		}
		
		/*private static function getScaledRound(icon:DisplayObject, size:Number=SIZE_SMALL):DisplayObject
		{
			const SIZE:Number = size;
			return BitmapUtils.bilinearDownscale(getRound(icon), SIZE);
		}*/
		
		public static function getButton(size:Number):DisplayObject
		{
			return getScaledSquare(Icons.button, size);
		}
		
		public static function getCalculation(size:Number):DisplayObject
		{
			return getScaledSquare(Icons.calculation, size);
		}
		
		public static function getInteraction(size:Number):DisplayObject
		{
			return getScaledSquare(Icons.interaction, size);
		}
		
		public static function getNavigation(size:Number):DisplayObject
		{
			return getScaledSquare(Icons.navigation, size);
		}
		
		public static function getQuestion(size:Number):DisplayObject
		{
			return getScaledSquare(Icons.question, size);
		}
		
		public static function getQuizTest(size:Number):DisplayObject
		{
			return getScaledSquare(Icons.quizTest, size);
		}
		
		public static function getSimulation(size:Number):DisplayObject
		{
			return getScaledSquare(Icons.simulation, size);
		}
		
		/*public static function getAudio(size:Number):DisplayObject
		{
			return getScaledRound(Icons.audio, size);
		}
		
		public static function getGraphic(size:Number):DisplayObject
		{
			return getScaledRound(Icons.graphic, size);
		}
		
		public static function getSketch(size:Number):DisplayObject
		{
			return getScaledRound(Icons.sketch, size);
		}
		
		public static function getSwf(size:Number):DisplayObject
		{
			return getScaledRound(Icons.swf, size);
		}
		
		public static function getText(size:Number):DisplayObject
		{
			return getScaledRound(Icons.text, size);
		}
		
		public static function getVideo(size:Number):DisplayObject
		{
			return getScaledRound(Icons.video, size);
		}*/
		
		public static function getUnknown(size:Number):DisplayObject
		{
			return getScaledSquare(PlayerIcons.logoIcon, size);
		}
		
		public static function getByType(type:int, size:Number=SIZE_SMALL):DisplayObject
		{
			switch (type) {
				case GadgetType.BUTTONS:
					return getButton(size);
					break;
				case GadgetType.CALCULATION:
					return getCalculation(size);
					break;
				case GadgetType.INTERACTION:
					return getInteraction(size);
					break;
				case GadgetType.NAVIGATION:
					return getNavigation(size);
					break;
				case GadgetType.QUESTION:
					return getQuestion(size);
					break;
				case GadgetType.QUIZTEST:
					return getQuizTest(size);
					break;
				case GadgetType.SIMULATION:
					return getSimulation(size);
					break;
				/*case AssetType.BITMAP:
					return getGraphic(size);
					break;
				case AssetType.AUDIO:
					return getAudio(size);
					break;
				case AssetType.SVG:
					return getSketch(size);
					break;
				case AssetType.TEXT:
					return getText(size);
					break;
				case AssetType.VIDEO:
					return getVideo(size);
					break;
				case AssetType.SWF:
					return getSwf(size);
					break;*/
			}
			return getUnknown(size);
		}
	}
}