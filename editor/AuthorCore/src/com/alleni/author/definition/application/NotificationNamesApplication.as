package com.alleni.author.definition.application
{
	public class NotificationNamesApplication
	{
		public static const APPLICATION_DID_ENTER_BACKGROUND:String = "Application Did Enter Background";
		public static const APPLICATION_WILL_ENTER_FOREGROUND:String = "Application Will Enter Foreground";
		
		//PROJECT RELATED
		public static const INVOKE_PROJECT:String = "Invoke Project";
		public static const OPEN_PROJECT:String = "Open Project";
		public static const OPEN_LOCAL_PROJECT:String = "Open Local Project";
		public static const SAVE_PROJECT:String = "Save Project";
		public static const SAVE_AS_PROJECT:String = "Save As Project";
		public static const CREATE_NEW_PROJECT:String = "Create New Project";
		public static const CLOSE_PROJECT:String = "Close Project";
		
		public static const PROJECT_CLOSING:String = "Project Closing";
		public static const PROJECT_LOADING:String = "Project Loading";
		public static const PROJECT_LOAD_PROGRESS:String = "Project Load Progress";
		public static const PROJECT_CHANGING:String = "Project Changing";
		public static const PROJECT_INITIALIZED:String = "Project Initialized";
		public static const PROJECT_COMPLETE:String = "Project Complete";
		public static const PROJECT_LOAD_SUCCEEDED:String = "Project Load Succeeded";
		public static const PROJECT_LOAD_FAILED:String = "Project Load Failed";
		public static const PROJECT_SETTINGS_CHANGED:String = "Project Settings Changed";
		public static const PROJECT_UNSAVED_CHANGES:String = "Project Unsaved Changes";
		public static const PROJECT_SAVED:String = "Project Saved";
		public static const PROJECT_STATE_SAVED:String = "Project State Saved";
		
		public static const OPEN_PUBLISHING_DIALOG:String = "Open Publishing Dialog";
        public static const OPEN_UPDATE_ZAPP:String = "Open Update App";
		public static const OPEN_SETTINGS_DIALOG:String = "Open Settings Dialog";
		public static const OPEN_TEMPLATE_DIALOG:String = "Open Template Dialog";
		public static const IMPORT_ASSET:String = "Import Asset";
		public static const IMPORT_GADGET:String = "Import Gadget";
		public static const SELECT_ASSET:String = "Select Asset"; 
		public static const SELECT_GADGET:String = "Select Gadget";
		public static const PROJECT_NAME_CHANGED:String = "Project Name Changed";
        public static const PUBLISH_GADGET:String = "Publish Gadget";
        public static const PREP_TO_SHARE_PROJECTS:String = "Prep To Share Projects";
        public static const SHARE_PROJECTS:String = "Share Projects";
		
		public static const ASSET_REQUESTED:String = "Asset Requested";
		public static const ASSET_REQUEST_PROGRESS:String = "Asset Request Progress";
		
		public static const OBJECT_BUILD_OUT_START:String = "Object build out start";
		public static const OBJECT_BUILD_OUT_UPDATE:String = "Object build out update";
		
		public static const GADGET_REQUESTED:String = "Gadget Requested";
		public static const GADGET_REQUEST_FULFILLED:String = "Gadget Request Fulfilled";

		public static const RESET_APPLICATION_STATE:String = "Reset Application State"; // when operations are cancelled
		public static const VIEW_CONTEXT_UPDATED:String = "View Context Updated"; // main application view context is being reassigned
		
		//RUN RELATED
        public static const RUNNING_STATE_PENDING:String = "Running State Pending";
        public static const RUNNING_STATE_CHANGED:String = "Running State Changed";
		public static const RUN_LOCK_TOGGLED:String = "Toggled Run Locking";
		
		//ZOOM RELATED
		public static const SCALE_CHANGED:String = "Scale Changed";
		public static const ZOOM_IN:String = "Zoom In";
		public static const ZOOM_OUT:String = "Zoom Out";
		public static const ZOOM_IN_NO_ADJUST:String = "Zoom In No Adjust";
		public static const ZOOM_OUT_NO_ADJUST:String = "Zoom Out No Adjust";
		public static const ACTUAL_SIZE:String = "Actual Size";
		public static const FIT_IN_WINDOW:String = "Fit In Window";
		
		//WIRE RELATED
		public static const MORE_WIRES:String = "More Wires";
		public static const FEWER_WIRES:String = "Fewer Wires";
		public static const WIRING_LEVEL_SET:String = "Wire Level Set";
		public static const EXCLUDE_MSG_CTR:String = "Exclude Msg Ctr";
		
		//TOOLS RELATED
		public static const TOGGLE_TOOL_BOX:String = "Toggle Tool Box";
		public static const HIDE_TOOLBOX:String = "Hide Tool Box";
		public static const SHOW_TOOLBOX:String = "Show Tool Box";
		
		public static const TOGGLE_LIBRARY:String = "Toggle Library";
		public static const TOGGLE_VARIABLE_LIBRARY:String = "Toggle Variable Library";
		
		public static const HIDE_DOCK:String = "Hide Dock";
		public static const SHOW_DOCK:String = "Show Dock";
		public static const TOGGLE_DOCK:String = "Toggle Dock";
		
		public static const SHOW_INSPECTOR:String = "Show Inspector";
		public static const HIDE_INSPECTOR:String = "Hide Inspector";
		public static const TOGGLE_INSPECTOR:String = "Toggle Inspector";

		public static const SHOW_RIBBON_LIBRARY:String = "Show Ribbon Library";
		public static const HIDE_RIBBON_LIBRARY:String = "Hide Ribbon Library";
		public static const TOGGLE_RIBBON_LIBRARY:String = "Toggle Ribbon Library";

		//APPLICATION RELATED
		public static const UI_READY:String = "UI Ready";
		public static const STAGE_RESIZED:String = "Stage Resized";
		public static const TOGGLE_FULL_SCREEN:String = "Toggle Full Screen";
		public static const NOTIFY_NO_EMBED_PERMISSION:String = "No Embed Permission";
		public static const TOGGLE_AUDIO:String = "Toggle Audio";
		public static const REQUEST_QUIT_APPLICATION:String = "Request Quit Application";
		public static const APPLICATION_QUITTING:String = "Application Quitting";
		public static const MUTE_APPLICATION_HOT_KEYS:String = "Mute Hot Keys";
		public static const UNMUTE_APPLICATION_HOT_KEYS:String = "Unmute Hot Keys";
		public static const SHOW_ALL_TOGGLED:String = "Show All Toggle";
		public static const STAGE_MOUSE_DOWN:String = "Stage Mouse Down";
		public static const FONTS_PROGRESS:String = "Fonts Progress";
		public static const SHOW_PRO_BRAND:String = "Show Pro-branded player options";
        public static const UPDATE_CUSTOMER_TYPE:String = "Update Customer Type";
        public static const FLOW_VISIBILITY_CHANGED:String = "Flow Visibility Changed";
        public static const RECALCULATE_ARENA_CONTROLS_FOR_FLOW_CHANGE:String = "Recalculate Arena Controls For Flow Visibility Change";

        //USER MESSAGE
		public static const SET_USER_MESSAGE:String = "Set User Message";
		public static const SET_USER_MESSAGE_WITH_INDICATOR:String = "Set User Message with Indicator";
		public static const CLEAR_USER_MESSAGE:String = "Clear User Message";
		public static const RESET_USER_MESSAGE:String = "Reset User Message";
		public static const RESET_USER_MESSAGE_WITH_DELAY:String = "Reset User Message with Delay";
		
		//NOTIFY USER
		public static const NOTIFY_USER:String = "Display Message in Notification Box";
		
		//WINDOW RELATED
		public static const SHOW_ABOUT_WINDOW:String = "Show About Window";
		public static const TOGGLE_QA_WINDOW:String = "Toggle QA Window";
		public static const SHOW_QA_WINDOW:String = "Show QA Window";
		public static const HIDE_QA_WINDOW:String = "Hide QA Window";
		public static const TOGGLE_DIAGRAM_WINDOW:String = "Toggle Diagram Window";
		public static const STACK_WINDOWS:String = "Stack Windows";
		public static const TILE_WINDOWS:String = "Tile Windows";
		public static const ACTIVE_WINDOW_CHANGED:String = "Active Window Changed"; //Data is ITaconiteWindow
		public static const WORLD_REPOSITIONED:String = "World Container Repositioned";
        public static const SCREENSHOT_PANEL_CLOSED:String = "Screenshot Panel Closed";
        public static const SCREENSHOT_PANEL_SHOWN:String = "Screenshot Panel Shown";
        public static const OPEN_WIKI:String = "Open Wiki";
        public static const REFRESH_MAIN_SITE:String = "Refresh Main Site";
        public static const UPDATE_COMPOSITE_FEEBACK:String = "Update Composite Feedback";
		
		public static const SHOW_EMBED:String = "Show Embed";
		public static const HIDE_EMBED:String = "Hide Embed";
		
		//CURSORS
		public static const CHANGE_CURSOR_TO:String = "Change Cursor To"; //Data is String
		public static const RESET_CURSOR:String = "Reset Cursor"; //Data is String
		public static const SHOW_NATIVE_CURSOR:String = "Show Native Cursor";
		public static const HIDE_NATIVE_CURSOR:String = "Hide Native Cursor";

        //ERROR MESSAGES
        public static const DISPLAY_MESSAGE:String = "Display Error Message"; // {id(String), closeHandler(Function)}

		//TEMP
		public static const MOUSE_ON_OBJECT:String = "Mouse On Object"; //Data is ObjectView
		public static const GET_OBJECT_UNDER_MOUSE:String = "Get Object Under Mouse"; //Data is Point
		
		public static const PATH_SELECTED:String = "Path Selected";
        //Data is path
		
		// ADMIN
		public static const TOGGLE_LOG_WINDOW:String = "Toggle Log Window";
		public static const HIDE_LOG_WINDOW:String = "Hide Log Window";
		public static const TOGGLE_TEST_WINDOW:String = "Toggle Test Window";
        public static const TOGGLE_OUTLINE_VIEW:String = "adminOutlineView";
        public static const ADMIN_TELEPORT:String = "adminTeleport";
        public static const ADMIN_LOAD_PROJ_BY_ID:String = "adminLoadByID";
        public static const ADMIN_FIND_TITLE:String = "adminFindTitle";
        public static const ADMIN_FIND_RIBBON:String = "adminFindRibbon";
        public static const ADMIN_FIND_UID:String = "adminFindUID";
        public static const ADMIN_COPY_UID:String = "adminCopyUID";
        public static const ADMIN_COPY_JSON:String = "adminCopyJson";
        public static const ADMIN_COPY_XML:String = "adminCopyXML";
        public static const ADMIN_COPY_DUMP:String = "adminCopyDump";
        public static const ADMIN_COPY_DOT:String = "adminCopyDOT";
        public static const ADMIN_COPY_LOG:String = "adminCopyLog";
        public static const ADMIN_LOG_CLEAR:String = "adminLogClear";
        public static const ADMIN_LOG_SELECTION:String = "adminLogSelection";
		public static const ADMIN_LOG_OBJECTS:String = "adminLogObjects";
		public static const ADMIN_LOG_WIRES:String = "adminLogWires";
        public static const ADMIN_CRASH:String = "adminCrashNow";

        // SHARE TO...
        public static const SHARE_TO_SHOPP:String = "ShareToShopp";
        public static const SHARE_TO_LMS:String = "ShareToLMS";
        public static const SHARE_TO_EMAIL_LIST:String = "ShareToEmailList";
        public static const SHARE_TO_GUEST_LIST:String = "ShareToGuestList";
        public static const SHARE_TO_FACEBOOK:String = "ShareToFacebook";
        public static const SHARE_TO_LINKED_IN:String = "ShareToLinkedIn";
        public static const SHARE_TO_TWITTER:String = "ShareToTwitter";
        public static const SHARE_TO_EMBED:String = "ShareToEmbed";
        public static const SHARE_TO_LINK:String = "ShareToLink";

        public static const SHARE_TO_LECTORA:String = "ShareToLectora";
        public static const SHARE_TO_CAPTIVATE:String = "ShareToCaptivate";
        public static const SHARE_TO_ARTICULATE:String = "ShareToArticulate";
        public static const SHARE_TO_POWERPOINT:String = "ShareToPowerPoint";

        public static const UPDATE_PRIVACY_LEVEL:String = "Update Privacy Level";

        // LMS
		public static const LMS_SET_VALUES:String = "LMS Set Values";
		public static const LMS_GET_VALUES:String = "LMS Get Values";

		public static const LRS_REQUEST_SUCCEEDED:String = "LRS Request Succeeded";
		public static const LRS_REQUEST_FAILED:String = "LRS Request Failed";
		public static const LRS_INITIALIZED:String = "LRS Initialize";
	}
}
