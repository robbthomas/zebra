package com.alleni.author.definition.application
{
	// in lieu of i18n. See Zebra X to see a technique for runtime-defined XML-driven internationalization of strings.
	public class ApplicationStrings
	{
		public static const DEFAULT_STATUS_MESSAGE:String 	= "Ready";
		public static const SAVING:String 					= "Saving";
		public static const LOADING:String 					= "Loading";
		public static const UNSAVED_CHANGES:String 			= "Unsaved changes";
		public static const LOADING_PROJECT:String 			= "Loading project";
		public static const BUILDING:String 				= "Building";
		public static const BUILDING_OBJECTS:String 		= "Building objects";
		public static const LOADING_ASSETS:String 			= "Loading assets";
		public static const UNLOADING:String 				= "Unloading";
	}
}