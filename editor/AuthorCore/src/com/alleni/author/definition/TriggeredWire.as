package com.alleni.author.definition
{
	import com.alleni.author.model.ui.Wire;

import mx.utils.ArrayUtil;

public class TriggeredWire
	{
		public var wire:Wire;
		public var toSlave:Boolean;
		public var removed:Boolean;
		public var propagated:Boolean;
		
		public function TriggeredWire(wire:Wire, toSlave:Boolean, removed:Boolean, propagated:Boolean)
		{
			this.wire = wire;
			this.toSlave = toSlave;
			this.removed = removed;
			this.propagated = propagated;
		}

        public function describe():String
        {
            var wireStr:String = propagated ? "===" : "---";
            if(removed) {
                wireStr[1] = "/";
            }
            if(toSlave) {
                wireStr = wireStr + ">";
            } else {
                wireStr = "<" + wireStr;
            }
            return wireStr + " " + wire.describe();
        }
	}
}