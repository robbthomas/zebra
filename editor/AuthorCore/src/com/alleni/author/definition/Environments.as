package com.alleni.author.definition
{
	import mx.collections.ArrayCollection;
	import mx.collections.IList;

	public class Environments
	{
		// these names appear in popup dialog
		public static const INTEGRATION:String = "Integration";
		public static const QA:String = "QA";
        public static const PRO:String = "Pro";
		public static const PRODUCTION:String = "Production";
        public static const DEMO:String = "Demo";
		public static const PROTOTYPE:String = "Prototype";
		
		private static const nameToEnviron:Object = {
				Integration:"integration.zebrazapps.com",
				QA:"qa.zebrazapps.com",
                Pro:"pro.zebrazapps.com",
                Production:"zebrazapps.com",
				Demo:"demo.zebrazapps.com",
				Prototype:"prototype.zebrazapps.com"
			};
		
		public static function get environmentList():Array
		{
			return [QA, PRO, PRODUCTION, DEMO, PROTOTYPE, INTEGRATION];
		}
		
		public static function serverForEnvironName(name:String):String
		{
			return nameToEnviron[name];
		}
		
		public static function environForServerName(server:String):String
		{
			for (var environ:String in nameToEnviron) {
				if (nameToEnviron[environ] == server)
					return environ;
			}
			return null;
		}
	}
}