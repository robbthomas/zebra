package com.alleni.author.definition
{
	public class GadgetType
	{

		public static const NAVIGATION				:int = 6000;
		public static const BUTTONS					:int = 6001;
		public static const QUESTION				:int = 6002;
		public static const CALCULATION				:int = 6003;
		public static const SIMULATION				:int = 6004;
		public static const INTERACTION				:int = 6005;
		public static const QUIZTEST				:int = 8000;
		
		public static const GENERIC					:int = 1;
        public static const PROJECT					:int = 2;
        public static const EVENT					:int = 3;
		
	}
}