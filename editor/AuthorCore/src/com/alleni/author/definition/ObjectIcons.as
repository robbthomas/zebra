package com.alleni.author.definition
{
	import assets.icons.object.*;
	
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.*;
	import com.alleni.taconite.document.ObjectSelection;
	
	import flash.display.DisplayObject;
	
	public class ObjectIcons
	{
	    static public function getIconFor(obj:AbstractObject = null, shortClassName:String = null):DisplayObject
		{
            var className:String;
            if (obj != null)
                className = obj.shortClassName;
            if (shortClassName != null)
                className = shortClassName;
	        switch (className) {
	            case Objects.shortClassNameForClass(Composite):
	                return new GadgetObjIcon();
	            break;
	            case Objects.shortClassNameForClass(Arena):
	                return new ArenaObjIcon();
	            break;
	            case Objects.shortClassNameForClass(PushButton):
	                return new PushbuttonObjIcon();
	            break;
	            case Objects.shortClassNameForClass(Calc):
	                return new CalcObjIcon();
	            break;
	            case Objects.shortClassNameForClass(Checkbox):
	                return new CheckboxObjIcon();
	            break;
	            case Objects.shortClassNameForClass(Clock):
	                return new ClockObjIcon();
	            break;
	            case Objects.shortClassNameForClass(VideoObject):
	                return new VideoObjIcon();
	            break;
	            case Objects.shortClassNameForClass(Audio):
	                return new AudioObjIcon();
	            break;
	            case Objects.shortClassNameForClass(MapObject):
	                return new MapObjIcon();
	            break;
	            case Objects.shortClassNameForClass(Oval):
	                return new OvalObjIcon();
	            break;
	            case Objects.shortClassNameForClass(RectangleObject):
	                return new RectangleObjIcon();
	            break;
	            case Objects.shortClassNameForClass(PathObject):
	                if (obj && (obj as PathObject).bornAsPolygon)
	                    return new PolygonObjIcon();
	                else
	                    return new SketchObjIcon();
	            break;
	            case Objects.shortClassNameForClass(Line):
	                return new LineObjIcon();
	            break;
	            case Objects.shortClassNameForClass(RadioButton):
	                return new RadiobuttonObjIcon();
	            break;
                case Objects.shortClassNameForClass(Slider):
                    return new SliderObjIcon();
                break;
                case Objects.shortClassNameForClass(Drawing):
                    return new GraphicObjIcon();
                break;
	            case Objects.shortClassNameForClass(TextObject):
	                return new TextObjIcon();
	            break;
	            case Objects.shortClassNameForClass(InputText):
	                return new TextinputObjIcon();
	            break;
	            case Objects.shortClassNameForClass(GotoURL):
	                return new JumpObjIcon();
	            break;
	            case Objects.shortClassNameForClass(ObjectSelection):
	                return new GroupObjIcon();
	            break;
	            case Objects.shortClassNameForClass(ProjectObject):
	                return new ProjectObjIcon();
	            break;
	            case Objects.shortClassNameForClass(LMS):
	                return new LmsObjIcon();
	            break;
	        }
	        return null;
	    }
	}
}
