package com.alleni.author.definition
{
	

	public class AssetTypeInfo
	{
		public var type:int;
		public var propertyType:String;
		public var title:String;
		public var defaultContainerName:String;
		public var supportsChanges:Boolean; // whether changes to this asset constitutes a content update as opposed to a replacement
		
		public function AssetTypeInfo(type:int, propertyType:String, title:String, defaultContainerName:String, supportsChanges:Boolean=false)
		{
			this.type = type;
			this.propertyType = propertyType;
			this.title = title;
			this.defaultContainerName = defaultContainerName;
			this.supportsChanges = supportsChanges;
		}
	}
}