package com.alleni.author.definition
{
	

	public class AssetType
	{
		public static const BITMAP		:int = 0;
		public static const AUDIO		:int = 1;
		public static const XML			:int = 2;
		public static const VIDEO		:int = 3;
		public static const SWF			:int = 4;
		public static const SVG			:int = 5;
		public static const TEXT		:int = 6;
		public static const FONT		:int = 7;
		public static const GADGET		:int = 8;
		public static const CSS			:int = 9;
		public static const ANY			:int = 99;
		
		public static const INFO:Vector.<AssetTypeInfo> = Vector.<AssetTypeInfo>([
			 new AssetTypeInfo(BITMAP,		Modifiers.IMAGE_ASSET_TYPE,	"Bitmap Graphics",		"Drawing")
			,new AssetTypeInfo(SVG,			Modifiers.IMAGE_ASSET_TYPE, "Vector Graphics",		"Drawing", 			true)
			,new AssetTypeInfo(AUDIO,		Modifiers.AUDIO_ASSET_TYPE,	"Audio",				"Audio")
			,new AssetTypeInfo(XML,			"",							"XML",					null,				true)
			,new AssetTypeInfo(TEXT,		Modifiers.TEXT_ASSET_TYPE,	"Text",					"TextObject",		true)
			,new AssetTypeInfo(CSS,			Modifiers.CSS_SELECTOR_TYPE,"CSS",					null,				true)
			,new AssetTypeInfo(VIDEO,		Modifiers.VIDEO_ASSET_TYPE,	"Video",				"VideoObject")
			,new AssetTypeInfo(SWF,			"",							"Flash",				"SWFObject")
			,new AssetTypeInfo(FONT,		Modifiers.FONT_ASSET_TYPE,	"Font",					"TextObject")
			,new AssetTypeInfo(GADGET,		Modifiers.GADGET_TYPE,		"Gadget",				"Composite")
			
			,new AssetTypeInfo(ANY,			"",							"All",					null)
		]);
		
		public var extensionRegExp:RegExp;
		public var type:int;
		public var description:String;
		public var mimeType:String;
		
		public function AssetType(extensionRegExp:RegExp, type:int, description:String, mimeType:String)
		{
			this.extensionRegExp = extensionRegExp;
			this.type = type;
			this.description = description;
			this.mimeType = mimeType;
		}
		
		public static function getTitle(type:int):String
		{
			for (var i:int = 0; i < INFO.length; i++) {
				if (INFO[i].type == type)
					return INFO[i].title;
			}
			return "";
		}
		
		public static function getPropertyType(type:int):String
		{
			for (var i:int = 0; i < INFO.length; i++) {
				if (INFO[i].type == type)
					return INFO[i].propertyType;
			}
			return "";
		}
		
		public static function getContainerName(type:int):String
		{
			for (var i:int = 0; i < INFO.length; i++) {
				if (INFO[i].type == type)
					return INFO[i].defaultContainerName;
			}
			return "";
		}
		
		public static function getSupportsChanges(type:int):Boolean
		{
			for (var i:int = 0; i < INFO.length; i++) {
				if (INFO[i].type == type)
					return INFO[i].supportsChanges;
			}
			return false;
		}
		
		public static function getTypeForName(name:String):int
		{
			for (var i:int = 0; i < INFO.length; i++) {
				if (INFO[i].defaultContainerName == name)
					return INFO[i].type;
			}
			return -1;
		}
	}
}