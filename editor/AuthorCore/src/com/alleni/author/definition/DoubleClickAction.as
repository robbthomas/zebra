/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.definition
{
	public class DoubleClickAction
	{
		public static const IGNORE:int = 0;
		public static const EDIT:int = 1;
		public static const STOP:int = 2;
	}
}