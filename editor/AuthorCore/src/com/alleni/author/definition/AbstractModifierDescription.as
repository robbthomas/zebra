package com.alleni.author.definition
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.WireAnchor;

	public class AbstractModifierDescription implements IModifier
	{
		private var _key:String
		private var _label:String;
		private var _description:String;
		private var _category:String;
		private var _weight:int;
		private var _indexed:Boolean;
		private var _autoAdd:Number;
		private var _id:String;
		
		public function AbstractModifierDescription(key:String, id:String, label:String, description:String, category:String, weight:int, indexed:Boolean, autoAdd:Number)
		{
			_key = key;
			_label = label;
			_description = description;
			_category = category;
			_weight = weight;
			_indexed = indexed;
			_autoAdd = autoAdd;
			_id = id;
		}
		
		public function get key():String
		{
			return _key;
		}
		
		public function get label():String
		{
			return _label;
		}
		
		public function set label(value:String):void
		{
			_label = value;
		}
		
		public function get description():String
		{
			return _description;
		}
        public function set description(value:String):void{
            _description = value;
        }
		
		public function get category():String
		{
			return _category;
		}
		
		public function set category(value:String):void
		{
			_category = value;
		}
		
		public function get weight():int
		{
			return _weight;
		}
		
		public function get readable():Boolean
		{
			return false;
		}
		
		public function get writeable():Boolean
		{
			return false;
		}
		
		public function get readOnly():Boolean
		{
			return readable && !writeable;
		}

		public function get writeOnly():Boolean
		{
			return writeable && !readable;
		}
		
		public function get indexed():Boolean
		{
			return _indexed;
		}

		// TODO: Remove this unused property
		public function get id():String
		{
			return _id;
		}
		
		protected function createAnchorInternal(host:AbstractObject, index:uint, x:Number=NaN, y:Number=NaN, angle:Number=NaN):WireAnchor
		{
			var anchor:WireAnchor = new WireAnchor();
			anchor.modifierDescription = this;
			anchor.hostObject = host;
			if(indexed) {
				anchor.hostPropertyIndex = index;
			}
			return anchor;
		}
		
		public function get autoAddUpTo():Number
		{
			return _autoAdd;
		}
	}
}