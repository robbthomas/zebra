package com.alleni.author.definition
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;

	public class Thumbnails
	{
		public static const THUMB_NOMINAL_SIZE:Number = 50;
		
		public static function generate(view:DisplayObject, width:Number, height:Number, scale:Number, nominalSize:Number=0):DisplayObject
		{
		/*	// get real display bounds of the view
			var bounds:Rectangle = view.getBounds(view);
			// scale the nominal size to suit current thumbnail scale
			var size:Number = (nominalSize>0?nominalSize:THUMB_NOMINAL_SIZE) * scale;
			// set up matrix to translate view to accommodate differences between view and object bounds
			//  (this accounts for "negative width" as well as line thickness)
			var matrix:Matrix = new Matrix(1, 0, 0, 1,  - bounds.x,  - bounds.y);
			// un-rotate object for thumbnail
			view.rotation = 0;
			
			// draw object to a bitmap
			if (view.width > 0 && view.height > 0) {
				var clipRect:flash.geom.Rectangle = new Rectangle(0, 0, view.width, view.height);
				var bitmapData:BitmapData = new BitmapData(view.width, view.height, true, 0xffffff);
				bitmapData.draw(view, matrix, null, null, clipRect, true);
				
				var bitmap:Bitmap = new Bitmap(bitmapData);
				bitmap.smoothing = true;
				
				// adjust size of bitmap by aspect ratio to nominal size
				var ratio:Number = view.height / view.width;
				if (ratio < 1) {
					bitmap.width = size;
					bitmap.height = size*ratio;
				} else {
					bitmap.height = size;
					bitmap.width = size/ratio;
				}
				return bitmap as DisplayObject;
			}
			*/
			return null;
		}
	}
}