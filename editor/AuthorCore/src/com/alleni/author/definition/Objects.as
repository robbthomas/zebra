/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: Handles.as 1840 2009-08-28 22:33:50Z pkrekelberg $  */

package com.alleni.author.definition
{
import com.alleni.author.Navigation.EventFlowView;
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.EventPageController;
import com.alleni.author.Navigation.EventPageView;
import com.alleni.author.controller.ObjectController;
import com.alleni.author.controller.objects.*;
import com.alleni.author.controller.objects.media.*;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.*;
import com.alleni.author.view.WorldView;
import com.alleni.author.view.objects.*;
import com.alleni.author.view.objects.tables.StateTableView;
import com.alleni.author.view.objects.tables.TruthTableView;
import com.alleni.taconite.definition.ObjectDefinition;

public class Objects
	{
		// definition of object models, views and controllers for runtime as-needed instantiation
		public static const DEFINITION:Vector.<ObjectDefinition> = Vector.<ObjectDefinition>([
			/* ,new ObjectDefinition("<MY_OBJECT_CLASS_NAME>", MyModelClass, MyObjectViewClass, MyObjectControllerClass, myObjectIconClass) */
			
			// graphics
			 new ObjectDefinition("Line", 				"Line",				Line, 				LineView,			LineController)
			,new ObjectDefinition("RectangleObject",	"Rectangle", 		RectangleObject,	RectangleView, 		ObjectController)
			,new ObjectDefinition("Oval", 				"Oval",				Oval,				OvalView, 			ObjectController)
			,new ObjectDefinition("Drawing", 			"Drawing",			Drawing,			DrawingView, 		DrawingController)
			,new ObjectDefinition("PathObject",			"Path",				PathObject,			PathObjectView,		PathController)
			// controls
			,new ObjectDefinition("Checkbox", 			"Check box",		Checkbox,			CheckboxView, 		ToggleButtonController)
			,new ObjectDefinition("InputText", 			"Input text",		InputText,			InputTextView, 		InputTextController)
			,new ObjectDefinition("PushButton",			"Push button",		PushButton,			PushButtonView, 	ToggleButtonController)
			,new ObjectDefinition("RadioButton",		"Radio Button",		RadioButton,		RadioButtonView, 	ToggleButtonController)
			,new ObjectDefinition("Slider", 			"Slider",			Slider,				SliderView, 		SliderController)
			,new ObjectDefinition("Clock", 				"Clock",			Clock,				ClockView, 			ClockController)
            ,new ObjectDefinition("MapObject", 			"Map",				MapObject,			MapView, 			MapController)

			// text
			,new ObjectDefinition("TextObject", 		"Text",				TextObject,			TextObjectView, 	TextObjectController)
			
			// tables
			,new ObjectDefinition("TruthTable", 		"Truth table",		TruthTable,			TruthTableView, 	TruthTableController)
			,new ObjectDefinition("AnswerTable",		"Answer table",		AnswerTable,		AnswerTableView,	AnswerTableController)
			,new ObjectDefinition("StateTable",			"State table",		StateTable,			StateTableView,		StateTableController)
			
			//Logic Objects
			,new ObjectDefinition("GotoURL",			"Weblink",			GotoURL,			GotoURLView,		GotoURLController)
			,new ObjectDefinition("Calculator",			"Calculation",		Calculator,			CalculatorView,		CalculatorController)
			
			// calculation
			,new ObjectDefinition("Calc",				"Calc",				Calc,				CalcView,			CalcController)
			,new ObjectDefinition("CalcTween",			"Tween calc",		CalcTween,			CalcTweenView,		CalcTweenController)
			,new ObjectDefinition("CalcGate",			"Gate calc",		CalcGate,			CalcGateView,		CalcGateController)
			,new ObjectDefinition("CalcExp",			"Expression calc",	CalcExp,			CalcExpView,		CalcExpController)
			,new ObjectDefinition("CalcSwitch",			"Switch calc",		CalcSwitch,			CalcSwitchView,		CalcSwitchController)
			
			// media
			,new ObjectDefinition("Audio", 				"Audio",			Audio,				AudioView, 			AudioController)
			,new ObjectDefinition("VideoObject", 		"Video",			VideoObject,		VideoView, 			VideoController)
			,new ObjectDefinition("SWFObject",			"SWF object",		SWFObject,			SWFView, 			SWFController)
		
			// containers
			,new ObjectDefinition("Composite",			"Gadget",			Composite,			CompositeView,		CompositeController)
			,new ObjectDefinition("World",				"World",			World,				WorldView,			ContainerController)
			,new ObjectDefinition("Arena", 				"Arena",			Arena,				ArenaView,	 		ArenaController)
            ,new ObjectDefinition("ArenaPage", 			"Arena page",		ArenaPage,			ArenaPageView,	 	ArenaPageController)
            ,new ObjectDefinition("EventFlow", 			"Event Flow",		EventFlow,			EventFlowView,	 	EventFlowController)
            ,new ObjectDefinition("EventPage", 			"Event",		    EventPage,			EventPageView,	 	EventPageController)

			// collaboration
			,new ObjectDefinition("QueueConnection", 	"Queue connection",	QueueConnection,	QueueConnectionView,QueueConnectionController)
			,new ObjectDefinition("BroadcastConnection", 	"Broadcast connection",	BroadcastConnection,	BroadcastConnectionView,BroadcastConnectionController)
			,new ObjectDefinition("MetricsConnection", 	"Metrics connection",	MetricsConnection,	ConnectionView,ConnectionController)
			,new ObjectDefinition("DistributionConnection", 	"Distribution connection",	DistributionConnection,	ConnectionView,ConnectionController)
			,new ObjectDefinition("TopConnection", 	"Top connection",	TopConnection,	ConnectionView,ConnectionController)
			,new ObjectDefinition("StatsConnection", 	"Stats connection",	StatsConnection,	ConnectionView,ConnectionController)

            // project
			,new ObjectDefinition("ProjectObject", 	"Project",	ProjectObject,	null,ProjectObjectController)
			,new ObjectDefinition("LMS", 	"",	LMS,	null,LMSObjectController)
		]);


		public static function descriptionForShortClassname(classname:String):ObjectDefinition
		{
			var subset:Vector.<ObjectDefinition> = Objects.DEFINITION.filter(
					function (item:ObjectDefinition, index:int, vector:Vector.<ObjectDefinition>):Boolean
							{return item.name==classname} );
			if (subset.length > 0)
				return subset[0];
			else
				return null;
		}


		public static function shortClassNameForClass(cls:Class):String
		{
			var subset:Vector.<ObjectDefinition> = Objects.DEFINITION.filter(
					function (item:ObjectDefinition, index:int, vector:Vector.<ObjectDefinition>):Boolean
							{return item.model==cls} );
			if (subset.length > 0)
				return subset[0].name;
			else
				return null;
		}
	}
}
