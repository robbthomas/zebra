package com.alleni.author.definition
{
	public class ViewRoles
	{
		public static const PRIMARY 		: uint = 1;
		public static const DIAGRAM		 	: uint = 2;
		public static const HUD			 	: uint = 4;
	}
}