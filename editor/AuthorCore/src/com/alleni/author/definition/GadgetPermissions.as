package com.alleni.author.definition
{
	public class GadgetPermissions
	{
		public static const LOCKED:String			= "locked";
		public static const EDITABLE:String			= "editable";
		public static const VIEWABLE:String			= "viewable";
	}
}