package com.alleni.author.definition
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.LMS;
import com.alleni.author.model.objects.ProjectObject;
import com.alleni.author.model.ui.Application;
	import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.geom.Point;
	
	public class DragInfo
	{
		public var global:Point;
		public var model:TaconiteModel;
		public var zArray:Vector.<int> = new Vector.<int>;
		
		public static function fromModel(m:TaconiteModel, getGlobal:Boolean=true, parentInfo:DragInfo = null):DragInfo
		{
			if (!m) return null;
			var info:DragInfo = new DragInfo();
			info.model = m;
			
			const object:AbstractObject = m.value as AbstractObject;
            if(object is LMS) {
                return null; // no DragInfos from LMSs (for now)
            }
            if(object is ProjectObject) {
                return null; // no DragInfos from Projects (for now)
            }
            if(object is EventPage || object is EventFlow) {
                return null; // no event objects for now
            }
			if(getGlobal && object != null) {
				var view:ObjectView = object.getView(Application.instance.viewContext);
				if (!view) return null;
				info.global = view.stagePosition;
			}
			
			while(m.parent != null) {
				var container:AbstractContainer = m.parent.value as AbstractContainer;
				if (container) {
                    var z:int = container.objects.getItemIndex(m.value);  // TaconiteModel.getChildIndex is not always correct
                    info.zArray.unshift(z);
                    if(parentInfo != null && container == parentInfo.model.value) {
                        // we encountered the desired parent, so we know the rest of the route
                        info.zArray = parentInfo.zArray.concat(info.zArray);
                        return info;
                    } else if(container is SmartObject && parentInfo != null) {
                        // don't allow children of composites unless that composite is the named parent
                        return null;
                    }
					m = m.parent;
				} else {
					break;
				}
			}
            if(parentInfo != null) {
                // we never found the desired parent
                return null;
            }
			return info;
		}
		
		/**
		 * Returns the subset of objects that can be moved or deleted.
		 * Cleans a selection list of:
		 *     children of closed composites,
		 *     objects that are not a child of parent,
		 *     objects which have some parent also in the list
		 * @param parent the world or editing composite that encompasses all objects that are allowed to be in the result
		 * @param models the initial list which is usually the selection list
		 * @param getGlobal passed on to the second parameter of DragInfo::fromModel
		 * @param strict: true to omit objects outside the vellum, false to allow them.
		 * @return a list of DragInfo sorted in z order with children of models cleared, and non children of parent cleared
		 */
		public static function fromModelList(parent:AbstractContainer, models:Vector.<TaconiteModel>, getGlobal:Boolean=true, strict:Boolean=true, omitLocked:Boolean=true):Vector.<DragInfo>
		{
			var i:int;
			var dragged:TaconiteModel = null;
			var oldPositions:Vector.<DragInfo> = new Vector.<DragInfo>;
			var parentInfo:DragInfo = DragInfo.fromModel(parent.model);  // will be null in certain cases
			nextChild: for each(var m:TaconiteModel in models){
				if(m.value != parent) {
					var info:DragInfo = DragInfo.fromModel(m, getGlobal, parentInfo);
                    if(info == null) {
                        continue nextChild;
                        // likely didn't have a view like the LMS object
                        // also may not have been a child of parent
                    }
					if(strict && parentInfo && info.zArray.length <= parentInfo.zArray.length) {
						continue nextChild; // cant possibly be inside parent if it is less deep or at same depth in the tree
					}
                    if(info.model.value is SmartObject && SmartObject(info.model.value).editing) {
                        continue nextChild; // skip a composite being edited
                    }
					if(omitLocked && AbstractObject(info.model.value).locked) {
						continue nextChild; // skip a locked object
					}
					if (strict && parentInfo) {  // omit objects outside the vellum
                        var length:int = parentInfo.zArray.length;
						for(i = 0; i < length; i++) {
							if(info.zArray[i] != parentInfo.zArray[i]) {
								continue nextChild; // not within parent
							}
						}
					}
					oldPositions.push(info);
				}
			}
			oldPositions.sort(function(a:DragInfo, b:DragInfo):Number {
				var bLen:int = b.zArray.length;
				for(i=0; i<a.zArray.length; i++) {
					if(i >= bLen) {
						// a is some child of b
						return 1;
					} else if(a.zArray[i] > b.zArray[i]) {
						return 1;
					} else if(a.zArray[i] < b.zArray[i]) {
						return -1;
					}
					// continue to check next position
				}
				// b is some child of a
				return -1;
			});
			// filter out objects that are not a child of the parent
			oldPositions = oldPositions.filter(function(element:DragInfo, index:int, arr:Vector.<DragInfo>):Boolean {
				for each(var info:DragInfo in oldPositions) {
					//				        trace("Testing: ", element.model.value, obj.model.value);
					var p:TaconiteModel = element.model.parent;
					while(p != null) {
						if(p == info.model) {
							//						        trace(" obj is parent");
							if(element.model == dragged) {
								dragged = p;
								// we are filtering out element because p is an ancestor
								// so we'll assume we're dragging p instead of element
							}
							return false;
						}
						p = p.parent;
					}
					//						trace(" obj is NOT parent");
				}
				//					trace("no parent");
				return true;
			});
			// if we are already moving/reparenting an ancestor, there is no need to move/reparent the child
			// in fact this has produced buggy behavior
			oldPositions = oldPositions.filter(function(element:DragInfo, index:int, arr:Vector.<DragInfo>):Boolean {
				for each(var info:DragInfo in oldPositions) {
					//				        trace("Testing: ", element.model.value, obj.model.value);
					var p:TaconiteModel = element.model.parent;
					while(p != null) {
						if(p == info.model) {
							//						        trace(" obj is parent");
							if(element.model == dragged) {
								dragged = p;
								// we are filtering out element because p is an ancestor
								// so we'll assume we're dragging p instead of element
							}
							return false;
						}
						p = p.parent;
					}
					//						trace(" obj is NOT parent");
				}
				//					trace("no parent");
				return true;
			});
			return oldPositions;
		}
		
		public static function toModels(source:Vector.<DragInfo>):Vector.<TaconiteModel>
		{
			var result:Vector.<TaconiteModel> = new Vector.<TaconiteModel>;
			for each(var info:DragInfo in source) {
				result.push(info.model);
			}
			return result;
		}
	}
}