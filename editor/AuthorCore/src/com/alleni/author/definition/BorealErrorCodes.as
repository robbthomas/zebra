package com.alleni.author.definition
{
	// ultimately, these codes need to be localized, so we will likely simply take the integer
	// code and translate on the front-end.
	public class BorealErrorCodes
	{
		private static const ERROR_CODE_REGEXP:RegExp = /^(\d+):\s*(.*)/;
		
		public static const SESSION_EXPIRED:int 		= 400;
		public static const NOT_ALLOWED:int 			= 401;
		
		public static const UNSUPPORTED_MIME:int		= 408;
		public static const THUMBNAIL_NOT_READY:int		= 410;
		public static const ASSET_DATA_NOT_FOUND:int	= 411;
		
		public static const PUBLISHED_NOT_FOUND:int 	= 710;
		public static const PUBLISH_FAILED:int			= 711;
		public static const PUBLISHED_NAME_IN_USE:int 	= 712;
		public static const REPUBLISH_NOT_ALLOWED:int 	= 714;
		public static const PUBLISHED_PRICE_TOO_LOW:int = 715;
		
		public static const PURCHASE_FAILED:int 		= 722;
		public static const BAD_ACCOUNT:int				= 723;
		
		public static const GADGET_STATE_NOT_FOUND:int 	= 810;
		public static const UPDATE_FAILED:int 			= 900;
		
		public static const ASSET_SAVE_FAILED:int 		= 901;
		public static const ASSET_LOAD_FAILED:int		= 902;
		
		public static const SAVE_FAILED:int 			= 913;
		public static const NAME_IN_USE:int 			= 914;
		public static const LOAD_FAILED:int 			= 915;
		public static const BRANCH_DETECTED:int 		= 916;
		public static const GADGET_NOT_FOUND:int 		= 917;
		
		public static const UNKNOWN:int 				= 999;
		
		// convenience methods
		public static function isNotAllowed(error:Object):Boolean
		{
			return testForCode(error, NOT_ALLOWED);
		}
		
		public static function isNameInUse(error:Object):Boolean
		{
			return testForCode(error, NAME_IN_USE);
		}
		
		public static function isPublishedNotFound(error:Object):Boolean
		{
			return testForCode(error, PUBLISHED_NOT_FOUND);
		}
		
		public static function isPublishFailed(error:Object):Boolean
		{
			return testForCode(error, PUBLISH_FAILED);
		}
		
		public static function isPublishedNameInUse(error:Object):Boolean
		{
			return testForCode(error, PUBLISHED_NAME_IN_USE);
		}
		
		public static function isRepublishNotAllowed(error:Object):Boolean
		{
			return testForCode(error, REPUBLISH_NOT_ALLOWED);
		}
		
		public static function isPublishedPriceTooLow(error:Object):Boolean
		{
			return testForCode(error, PUBLISHED_PRICE_TOO_LOW);
		}
		
		public static function isPurchaseFailed(error:Object):Boolean
		{
			return testForCode(error, PURCHASE_FAILED);
		}
		
		public static function isBadAccount(error:Object):Boolean
		{
			return testForCode(error, BAD_ACCOUNT);
		}
		
		public static function isLoadFailed(error:Object):Boolean
		{
			return testForCode(error, LOAD_FAILED) || testForCode(error, GADGET_NOT_FOUND);
		}
		
		public static function isSaveFailed(error:Object):Boolean
		{
			return testForCode(error, SAVE_FAILED);
		}
		
		public static function isGadgetStateNotFound(error:Object):Boolean
		{
			return testForCode(error, GADGET_STATE_NOT_FOUND);
		}
		
		public static function isBranchDetected(error:Object):Boolean
		{
			return testForCode(error, BRANCH_DETECTED);
		}
		
		private static function testForCode(error:Object, code:int):Boolean
		{
			return getCode(error) == code;
		}
		
		// return string portion of code attribute of error object, if it exists
		public static function getDescription(error:Object):String
		{
			if (error != null && "message" in error)
				return error["message"];
			else return null;
		}
		
		// return integer portion of code attribute of error object, if it exists
		public static function getCode(error:Object):int
		{
			if (error != null && "code" in error)
				return int(error["code"]);
			else return -1;
		}
	}
}