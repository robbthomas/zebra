package com.alleni.author.definition
{
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.ModelRoot;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.utils.Dictionary;
	
	/*
	 * This class exists only to comply with new Vector.<ITaconiteObject> that can no longer hold arbitrary objects
	 */
	dynamic public class InspectorPresets implements ITaconiteObject
	{
		public var modifiers:Dictionary;
		
		public function InspectorPresets()
		{
			this["preset"] = true;
		}
	
		public function get model():TaconiteModel
		{
			return null;
		}
	
		public function get modelRoot():ModelRoot
		{
			return null;
		}
	}
}