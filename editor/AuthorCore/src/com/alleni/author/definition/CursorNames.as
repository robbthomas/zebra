package com.alleni.author.definition
{
	public class CursorNames
	{
		//ARROWS
		public static const ARROW_SELECT:String = "Arrow Select";
		public static const ARROW_COPY:String = "Arrow Copy";
		
		//TOOLS
		public static const TOOL_ARENA:String = "Tool Arena";
		public static const TOOL_BUTTON:String = "Tool Button";
		public static const TOOL_SLIDER:String = "Tool Slider";
		public static const TOOL_CLOCK:String = "TOOL CLOCK";
		public static const TOOL_TRUTH_TABLE:String = "Tool Truth Table";
		public static const TOOL_PENCIL:String = "Tool Pencil";
		public static const TOOL_ZOOMIN:String 	= "Tool Zoom In";
		public static const TOOL_ZOOMOUT:String = "Tool Zoom Out";
		public static const TOOL_RECTANGLE:String = "Tool Rectangle";
		public static const TOOL_ELLIPSE:String = "Tool Ellipse";
		public static const TOOL_TEXT:String = "Tool Text";
		public static const TOOL_TEXT_INPUT:String = "Tool Text Input";
		public static const TOOL_CHECKBOX:String = "Tool Checkbox";
		public static const TOOL_LINE:String = "Tool Line";
		public static const TOOL_AUDIO:String = "Tool Audio";
		public static const TOOL_VIDEO:String = "Tool Video";
		public static const TOOL_MAP:String = "Tool Map";
		
		//SUB_TOOLS
		public static const EYEDROPPER:String = "eyedropper";
		
		//EDITOR CURSORS
		public static const SCALE_CORNER1:String = "Scale Corner 1";
		public static const SCALE_CORNER2:String = "Scale Corner 2";
		public static const SCALE_HORZ:String = "Scale Horz";
		public static const SCALE_VERT:String = "Scale Vert";
	}
}