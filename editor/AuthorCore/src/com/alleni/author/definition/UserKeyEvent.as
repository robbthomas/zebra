/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/27/11
 * Time: 11:15 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition {
import flash.events.Event;
import flash.events.KeyboardEvent;

public class UserKeyEvent extends Event {
	public static var TYPE:String = "USER_KEY_EVENT_TYPE";

	public var key:String;
	public var down:Boolean;
	public var held:Boolean;
	public var keyCode:int;
	public var ctrlKey:Boolean;
	public var altKey:Boolean;
	public var shiftKey:Boolean;

	public function UserKeyEvent(key:String, down:Boolean, held:Boolean, keyCode:int = -1, ctrlKey:Boolean = false, altKey:Boolean = false, shiftKey:Boolean = false) {
		
		super(TYPE);
		this.key = key;
		this.down = down;
		this.held = held;
		this.keyCode = keyCode;
		this.ctrlKey = ctrlKey;
		this.altKey = altKey;
		this.shiftKey = shiftKey;
		
	}
}
}
