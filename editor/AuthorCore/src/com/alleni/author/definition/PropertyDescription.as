package com.alleni.author.definition
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.WireAnchor;
	

	public class PropertyDescription extends AbstractModifierDescription implements IModifier
	{
		private var _type:String;
		private var _constraints:Object;
		private var _writeable:Boolean;
		private var _readable:Boolean;
		
		public function PropertyDescription(key:String="", id:String="", label:String="", description:String="", category:String="", weight:int=0, indexed:Boolean=false, autoAdd:Number=-1, type:String="", constraints:Object=null, writeable:Boolean=true, readable:Boolean=true)
		{
			super(key, id, label, description, category, weight, indexed, autoAdd);
			
			_type = type;
			_constraints = constraints;
			_writeable = writeable;
            _readable = readable;
		}
		
		override public function get writeable():Boolean {
			return _writeable;
		}
		
		override public function get readable():Boolean {
			return _readable;
		}
		
		public function get type():String {
			return _type;
		}
		
		public function get constraints():Object {
			return _constraints;
		}
		
		/**
		 * Create an anchor for this modifier
		 * only host and property are required for a ribbon
		 * for a gadget anchor x, y, and angle must be supplied
		 */
		public function createAnchor(host:AbstractObject, property:String, index:uint=0, x:Number=NaN, y:Number=NaN, angle:Number=NaN):WireAnchor {
			var anchor:WireAnchor = createAnchorInternal(host, index, x, y, angle);
			anchor.hostProperty = property;
			return anchor;
		}

		public function overrideType(type:String):void {
			_type = type;
		}
	}
}