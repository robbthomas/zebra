package com.alleni.author.definition
{
	import com.alleni.author.controller.ui.GadgetController;
	import com.alleni.author.model.objects.IListable;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.ui.ExternalContentObject;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.factory.IEnvironment;
	import com.alleni.taconite.factory.TaconiteFactory;
	
	import flash.globalization.CurrencyFormatter;

	public class GadgetDescription
	{
		public static const PENDING_NAME:String = "Pending...";
		public static const DEFAULT_NAME:String = "Gadget";
		public static const PRICE_FREE_INDICATOR:String = "---";
		
		public static function cleanName(item:IListable, publishedPriority:Boolean=false):String
		{
			const contentObject:ExternalContentObject = item as ExternalContentObject;
			if (!contentObject) return null;
			var name:String = contentObject.name;
			
			if (!publishedPriority) publishedPriority = !GadgetController.instance.confirmCanPublish(contentObject);
			if (publishedPriority && contentObject.publishedName) return contentObject.publishedName;
																						
			if (name == null || name == "" || name == DEFAULT_NAME || Utilities.UID_REGEX.test(name) || Utilities.UUID_REGEX.test(name)) {
				if (contentObject as Project)
					name = contentObject.publishedName ? contentObject.publishedName : DEFAULT_NAME;
				else name = PENDING_NAME;
			}
			return name;
		}
		
		public static function authorNameForGadget(gadget:Project):String
		{
			var authorName:String = "";
			
			if (gadget.authorFullName && gadget.authorFullName != " ")
				authorName += gadget.authorFullName;
			if (gadget.accountName && gadget.accountName.length > 0){
				if (authorName.length > 0)
					authorName += "\n";
			    authorName += gadget.accountName;
            }
			
			return authorName;
		}
		
		public static function authorNameForMe():String
		{
			var authorName:String = "";
			
			const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			
			if (environment.userFullName && environment.userFullName != " ")
				authorName += environment.userFullName;
			
			return authorName;
		}
		
		public static function prettyPrice(price:uint, minPrice:int=-1):String
		{
			if (price == 0)
				return PRICE_FREE_INDICATOR;
			
			var environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
			var formatter:CurrencyFormatter = new CurrencyFormatter(environment.locale);

			var raw:Number = (price as Number)/100;
			var prettyPriceString:String = formatter.format(raw, true) + " " + formatter.currencyISOCode;

			if (minPrice > -1 && price == minPrice)
				prettyPriceString += " (min)";
			return prettyPriceString;
		}

        public static function companyNameForMe():String{
            var companyName:String = "";

			const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();

			companyName = environment.companyName;

			return companyName;
        }
	}	
}
		