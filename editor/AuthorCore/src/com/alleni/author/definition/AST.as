package com.alleni.author.definition
{
	public class AST
	{
		private var _root:Object = null;
		
		public function AST(root:Object = null)
		{
			this._root = root;
		}
		
		public function evaluate(env:Object=null):Object
		{
			if(_root == null) {
				return NaN;
			}
			if(env == null) {
				env = {};
			}
			try {
				return eval(_root, env);
			} catch(e:Error) {
				trace("Error evaluating expression", e);
				trace(e.getStackTrace())
				return NaN;
			}
			throw new Error("Bad Flex compiler");
		}

		public function getType(env:Object=null):String
		{
			if(_root == null) {
				return "";
			}
			if(env == null) {
				env = {};
			}
			try {
				return type(_root, env);
			} catch(e:Error) {
				trace("Error evaluating type of expression", e);
				trace(e.getStackTrace())
				return "";
			}
			throw new Error("Bad Flex compiler");
		}
		
		public function print():void
		{
			trace(">>>>>>>>>> Object printing");
			printObject(_root);
			trace(">>>>>>>>>> Ast printing");
			printAst(_root);
			trace(">>>>>>>>>> Parenthesis printing");
			trace(paren(_root));
		}
		
		public static function defaultEnvironment():Object
		{
			return {
				sin:Math.sin,
				cos:Math.cos,
				tan:Math.tan,
				asin:Math.asin,
				acos:Math.acos,
				atan:Math.atan,
				atan2:Math.atan2,
				abs:Math.abs,
				random:Math.random,
				floor:Math.floor,
				ceil:Math.ceil,
				round:Math.round,
				min:Math.min,
				max:Math.max,
				sqrt:Math.sqrt,
				pow:Math.pow,
				log:Math.log,
				exp:Math.exp,
				pi:Math.PI,
				e:Math.E,
				"true":true,
				"false":false
			}
		}

		public static function defaultTypeEnvironment():Object
		{
			return {
				sin:"number",
				cos:"number",
				tan:"number",
				asin:"number",
				acos:"number",
				atan:"number",
				atan2:"number",
				abs:"number",
				random:"number",
				floor:"number",
				ceil:"number",
				round:"number",
				min:"number",
				max:"number",
				sqrt:"number",
				pow:"number",
				log:"number",
				exp:"number",
				pi:"number",
				e:"number",
				"true":"boolean",
				"false":"boolean"
			}
		}

		public function variables(env:Object = null):Array
		{
			if(env == null) {
				env = {};
			}
			return getVariables(_root, env);
		}

		private static function concatAll(arr:Array, env:Object):Array
		{
			var result:Array = [];
			for each(var obj:Object in Array) {
				result = result.concat(obj);
			}
			return result;
		}

		private static function getVariables(exp:Object, env:Object):Array
		{
			switch(exp.kind) {
				case "number": return [];
				case "variable": return (exp.token.text in env)?[]:[exp.token.text];
				case "unary operator": return getVariables(exp.rhs, env);
				case "operator": return getVariables(exp.lhs, env).concat(getVariables(exp.rhs, env));
				case "functionCall":
					var result:Array = [];
					for each(var arg:Object in exp.args.list) {
						result = result.concat(getVariables(arg, env));
					}
					return result;
			}
			return [];
		}

		private static function printObject(obj:Object, indent:String = ""):void
		{
			for(var prop:String in obj) {
				if(prop == "token") {
					trace(indent,prop,"=",obj[prop].kind,obj[prop].text,obj[prop].position);
				} else if(typeof obj[prop] == 'object' && obj[prop] != null) {
					trace(indent,prop,"=","{");
					printObject(obj[prop], indent+"  ");
					trace(indent,"}");
				} else {
					trace(indent,prop,"=",obj[prop]);
				}
			}
		}
		
		private static function printAst(exp:Object, indent:String = ""):void
		{
			switch(exp.kind) {
				case "number": trace(indent + "number " + exp.token.text); break;
				case "variable": trace(indent + "varible " + exp.token.text); break;
				case "unary operator": trace(indent + "unary operator " + exp.token.text); indent += "  "; printAst(exp.rhs, indent); break;
				case "operator": trace(indent + "operator " + exp.token.text); indent += "  "; printAst(exp.lhs, indent); printAst(exp.rhs, indent); break;
				case "functionCall": trace(indent + "functionCall " + exp.token.text); indent += "  "; for each(var a:Object in exp.args.list) {printAst(a, indent);} break;
			}
		}

		private static function paren(exp:Object):String
		{
			switch(exp.kind) {
				case "number": case "variable": return exp.token.text;
				case "unary operator": return exp.token.text + "(" + paren(exp.rhs) + ")";
				case "operator": return "(" + paren(exp.lhs) + " " + exp.token.text + " " + paren(exp.rhs) + ")";
				case "functionCall": return exp.token.text + "(" + exp.args.list.map(function(element:*, index:int, arr:Array):String{return paren(element);}).join(",")  + ")";
			}
			throw new Error("Unpossible error");
		}

		private static function type(exp:Object, env:Object):String
		{
			switch(exp.kind) {
				case "number": return "number";
				case "variable":
					if(env.hasOwnProperty(exp.token.text)) {
						return env[exp.token.text];
					} else {
						throw new Error("Undefined variable: " + exp.token.text + " at " + exp.token.position);
					}
				case "unary operator":
					switch(exp.token.text) {
						case "!": return "boolean";
						case "-": return "number";
						default: throw("Unknown unary operator " + exp.token.text + " at " + exp.token.position);
					}
				case "operator":
					switch(exp.token.text) {
						case "+": return "number";
						case "-": return "number";
						case "*": return "number";
						case "/": return "number";
						case "<": return "boolean";
						case ">": return "boolean";
						case "=": return "boolean";
						case "<=": return "boolean";
						case ">=": return "boolean";
						case "<>": return "boolean";
						case "&": return "boolean";
						case "|": return "boolean";
						default: throw("Unknown operator " + exp.token.text + " at " + exp.token.position);
					}
				case "functionCall":
					if(env.hasOwnProperty(exp.token.text)) {
						return env[exp.token.text];
					} else {
						throw new Error("Undefined function named: " + exp.token.text + " at " + exp.token.position);
					}
			}
			throw new Error("Unpossible error");
		}
		
		private static function eval(exp:Object, env:Object):Object
		{
			switch(exp.kind) {
				case "number":
					return Number(exp.token.text);
				case "variable":
					if(env.hasOwnProperty(exp.token.text)) {
						return env[exp.token.text];
					} else {
						throw new Error("Undefined variable: " + exp.token.text + " at " + exp.token.position);
					}
				case "unary operator":
					var u_rhs:Number = Number(eval(exp.rhs, env));
					var u_rhs_b:Boolean = Boolean(eval(exp.rhs, env));
					switch(exp.token.text) {
						case "!": return ! u_rhs_b;
						case "-": return - u_rhs;
						default: throw("Unknown unary operator " + exp.token.text + " at " + exp.token.position);
					}
				case "operator":
					var lhs:Number = Number(eval(exp.lhs, env));
					var rhs:Number = Number(eval(exp.rhs, env));
					var lhs_b:Boolean = Boolean(eval(exp.lhs, env));
					var rhs_b:Boolean = Boolean(eval(exp.rhs, env));
					switch(exp.token.text) {
						case "+": return lhs + rhs;
						case "-": return lhs - rhs;
						case "*": return lhs * rhs;
						case "/": return lhs / rhs;
						case "<": return lhs < rhs;
						case ">": return lhs > rhs;
						case "=": return lhs == rhs;
						case "<=": return lhs <= rhs;
						case ">=": return lhs >= rhs;
						case "<>": return lhs != rhs;
//						case "%": return lhs % rhs;
						case "&": return lhs_b && rhs_b;
						case "|": return lhs_b || rhs_b;
						default: throw("Unknown operator " + exp.token.text + " at " + exp.token.position);
					}
				case "functionCall":
					var arguments:Array = new Array();
					for each(var a:Object in exp.args.list) {
					arguments.push(eval(a, env));
					}
					if(env.hasOwnProperty(exp.token.text)) {
						return env[exp.token.text].apply(null, arguments);
					} else {
						throw new Error("Unknown function named: " + exp.token.text + " at " + exp.token.position);
					}
			}
			throw new Error("Unpossible error");
		}
	}
}