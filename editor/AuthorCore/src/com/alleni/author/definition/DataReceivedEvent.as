/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/14/11
 * Time: 12:23 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition {
import flash.events.Event;

public class DataReceivedEvent extends Event{
	private var _data:Object;
	public function get data():Object {
		return _data;
	}
	public function DataReceivedEvent(type:String, data:Object) {
		super(type);
		_data = data;
	}
}
}
