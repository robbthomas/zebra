package com.alleni.author.definition {
	import flash.events.Event;
	import flash.geom.Point;
	
	public class DropIntoContainerEvent extends Event
	{
		public static const DROP_INTO_CONTAINER:String = "dropIntoContainerEvent";
		public var location:Point;
		
		public function DropIntoContainerEvent(location:Point=null)
		{
			super(DROP_INTO_CONTAINER);
			this.location = location;
		}
	}
}