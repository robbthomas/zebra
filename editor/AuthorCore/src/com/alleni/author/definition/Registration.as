package com.alleni.author.definition
{
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.WireAnchor;
	
	import flash.net.registerClassAlias;

	public class Registration
	{
		static public function registerClassesForCloning():void {
        	registerClassAlias("com.alleni.author.definition.PropertyDescription", PropertyDescription);
        	registerClassAlias("com.alleni.author.definition.InletDescription", InletDescription);
        	registerClassAlias("com.alleni.author.definition.OutletDescription", OutletDescription);
			registerClassAlias("com.alleni.author.model.ui.WireAnchor", WireAnchor);
			registerClassAlias("com.alleni.author.model.ui.Asset", Asset);
		}
	}
}