package com.alleni.author.definition.text{
	
	import flash.utils.Dictionary;
	
	public class ActionMap {
		
		public static const STYLENAME_ACTION:String = "style";
		public static const BOLD_ACTION:String = "bold";
		public static const ITALIC_ACTION:String = "italic";
		public static const UNDERLINE_ACTION:String = "underline";
		public static const FOREGROUNDCOLOR_ACTION:String = "fontColor";
		public static const STRIKETHROUGH_ACTION:String = "strikeThrough";
		public static const FONTSIZE_ACTION:String = "fontSize";
		public static const FONTNAME_ACTION:String = "fontFamily";
		public static const FONTALPHA_ACTION:String = "fontAlpha";
		public static const TEXT_ALIGN_ACTION:String = "textAlign";
		public static const VERTICAL_ALIGN_ACTION:String = "verticalAlign";
		
		private var _map:Dictionary = new Dictionary();
		
		public function ActionMap(){
			
			
		}
		
		public function put(key:String, func:Function):void{
			
			_map[key] = func;
		}
		
		public function get(key:String):Function{
			
			return _map[key];
		}
		
		public function deleteKey(key:String):void{
			
			delete _map[key];
		}
		
		public function createMap(xml:XML):void{
			
			//generate this map from XML
		}
		
		
		
	}
	
	
}

