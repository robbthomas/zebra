/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 3/21/11
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.text {
	import flash.text.StyleSheet;
	import flash.utils.Dictionary;

	public class CSSObject {

		private var _selectors:Dictionary = new Dictionary();

		public function CSSObject()
		{

		}

		public function get selectors():Dictionary
		{
			return _selectors;
		}

		public function addSelector(selector:String):void{

			if(_selectors[selector] == null){

				_selectors[selector] = new Dictionary();
			}
		}

		private function getSelector(selector:String):Dictionary {
			var result:Dictionary = _selectors[selector];
			if(result == null) {
				result = new Dictionary();
				_selectors[selector] = result;
			}
			return result;
		}

		public function addProperty(selector:String, property:String, value:String):void{

			var props:Dictionary = getSelector(selector);
			props[property] = value;

		}

		public function getProperty(selector:String, property:String):String {
			var props:Dictionary = getSelector(selector);
			if(property in props) {
				return props[property];
			} else {
				return null;
			}
		}

		private function makePropertyValuePair(property:String, value:Object):String{
			var line:String  = property;
			line =  line.concat(": ");
			line =  line.concat(value);
			line =  line.concat(";");
			return line;
		}

		public function makeCSS():String{

			var css:String = "";

			for(var skey:* in _selectors){

				css = css.concat(skey);
				css = css.concat(" { ");
				var _properties:Dictionary = _selectors[skey];


				for(var key:* in _properties){
					css = css.concat(makePropertyValuePair(key,_properties[key]));
				}

				css = css.concat(" }");

			}

			return css;
		}

		public function toJson():Object {
			
			return {
				type:"CSSObject", css:makeCSS()
			}
		}

		public static function fromJson(json:Object):CSSObject {
			var cssObject:CSSObject = new CSSObject();
			var styleSheet:StyleSheet = new StyleSheet();
			
			if (!json.hasOwnProperty("css"))
				return cssObject;
				
			styleSheet.parseCSS(json.css);
			
			for each(var style:String in styleSheet.styleNames){
				var styleObject:Object = styleSheet.getStyle(style);
				
					for (var prop:String in styleObject){
						cssObject.addProperty(style,prop,styleObject[prop]);
					}
			}
			
			return cssObject;
		}
		
		


		public function test():void{

			/*
			body {
			color: navy;
			font-family: "arial"; }
			.title {  text-decoration: bold;
			color: green;
			}

			*/

			addSelector("body");
			addProperty("body","text-decoration", "none");
			addProperty("body","color", "navy");

			addSelector(".title");
			addProperty(".title","text-decoration", "bold");
			addProperty(".title","color", "green");

			trace("CSS:     ", makeCSS());

		}
	}
}
