package com.alleni.author.definition.text
{
	public class Fonts
	{
		public static const DEFAULT_FAMILY:String = "Zebra";
		public static const DEFAULT_SANS_FONT:String = "sans serif";
		public static const DEFAULT_SERIF_FONT:String = "serif";
		public static const EMBEDDED_FONT_NAMES:Vector.<String> = Vector.<String>(["sans_serif", "serif", "Zebra"]); //underscore in sans_serif is required


		// Myriad Pro
		[Embed(mimeType="application/x-font", fontFamily="Zebra", fontWeight="normal", fontStyle="normal",
			embedAsCFF="true", source="/assets/fonts/MyriadPro-Regular.otf", unicodeRange="U+0020-007E")]
		private static var myMyriadPro:Class;
		
		[Embed(mimeType="application/x-font", fontFamily="Zebra", fontWeight="normal", fontStyle="italic", 
			embedAsCFF="true", source="/assets/fonts/MyriadPro-It.otf", unicodeRange="U+0020-007E")]
		private static var myMyriadProItalic:Class;
		[Embed(mimeType="application/x-font", fontFamily="Zebra", fontWeight="bold", fontStyle="normal",
			embedAsCFF="true", source="/assets/fonts/MyriadPro-Bold.otf", unicodeRange="U+0020-007E")]
		private static var myMyriadProBold:Class;
		[Embed(mimeType="application/x-font", fontFamily="Zebra", fontWeight="bold", fontStyle="italic",
			embedAsCFF="true", source="/assets/fonts/MyriadPro-BoldIt.otf", unicodeRange="U+0020-007E")]
		private static var myMyriadProBoldItalic:Class;
		
		
		// Default sans serif -- licensed OFL
		[Embed(mimeType="application/x-font", fontFamily="sans serif", fontWeight="normal", fontStyle="normal",
			embedAsCFF="true", source="/assets/fonts/Cabin-Regular.otf", unicodeRange="U+0020-007E")]
		private static var CabinRegular:Class;
		
		[Embed(mimeType="application/x-font", fontFamily="sans serif", fontWeight="normal", fontStyle="italic", 
			embedAsCFF="true", source="/assets/fonts/Cabin-Italic.otf", unicodeRange="U+0020-007E")]
		private static var CabinItalic:Class;
		
		[Embed(mimeType="application/x-font", fontFamily="sans serif", fontWeight="bold", fontStyle="italic",
			embedAsCFF="true", source="/assets/fonts/Cabin-BoldItalic.otf", unicodeRange="U+0020-007E")]
		private static var CabinBoldItalic:Class;

		[Embed(mimeType="application/x-font", fontFamily="sans serif", fontWeight="bold", fontStyle="normal",
			embedAsCFF="true", source="/assets/fonts/Cabin-Bold.otf", unicodeRange="U+0020-007E")]
		private static var CabinBold:Class;
		
		// Default serif -- licensed OFL
		[Embed(mimeType="application/x-font", fontFamily="serif", fontWeight="normal", fontStyle="normal",
			embedAsCFF="true", source="/assets/fonts/pt_serifwebregular.otf", unicodeRange="U+0020-007E")]
		private static var PTRegular:Class;
		
		[Embed(mimeType="application/x-font", fontFamily="serif", fontWeight="normal", fontStyle="italic", 
			embedAsCFF="true", source="/assets/fonts/pt_serifwebitalic.otf", unicodeRange="U+0020-007E")]
		private static var PTItalic:Class;
		
		[Embed(mimeType="application/x-font", fontFamily="serif", fontWeight="bold", fontStyle="italic",
			embedAsCFF="true", source="/assets/fonts/pt_serifwebbolditalic.otf", unicodeRange="U+0020-007E")]
		private static var PTBoldItalic:Class;
		
		[Embed(mimeType="application/x-font", fontFamily="serif", fontWeight="bold", fontStyle="normal",
			embedAsCFF="true", source="/assets/fonts/pt_serifwebbold.otf", unicodeRange="U+0020-007E")]
		private static var PTBold:Class;
	}
}