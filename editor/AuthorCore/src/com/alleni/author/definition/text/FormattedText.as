package com.alleni.author.definition.text
{
	import com.alleni.author.controller.app.FontController;
	
	import flashx.textLayout.conversion.ConversionType;
	import flashx.textLayout.conversion.TextConverter;
	import flashx.textLayout.elements.Configuration;
	import flashx.textLayout.elements.TextFlow;
	
	public class FormattedText	
	{
		public static const DEFAULT_TEXT:String = TextConverter.export(
				TextConverter.importToFlow("", TextConverter.PLAIN_TEXT_FORMAT),
				TextConverter.TEXT_LAYOUT_FORMAT,
				ConversionType.STRING_TYPE).toString();
	
		private var _text:String;
		private var _cssObject:CSSObject;
	
		public function FormattedText(text:String = null)
		{
			if(text == null) {
				_text = DEFAULT_TEXT;
			} else {
				_text = text;
			}
		}
	
		public function get cssObject():CSSObject
		{
			return _cssObject;
		}

		public function set cssObject(value:CSSObject):void
		{
			_cssObject = value;
		}

		public function toTextFlow(config:Configuration = null):TextFlow
		{
			return TextConverter.importToFlow(_text, TextConverter.TEXT_LAYOUT_FORMAT,config);
		}
	
		public function toString():String
		{
			var _textFlow:TextFlow = toTextFlow();
			
			if(_textFlow) {
					return TextConverter.export(_textFlow, TextConverter.PLAIN_TEXT_FORMAT, ConversionType.STRING_TYPE).toString();
			} else {
				return _text;
			}
		}
		
		public function get isEmpty():Boolean
		{
			var str:String = toString();
			return str == " " || str == "";
		}
		
		public function toMarkUpAsString():String
		{
			return TextConverter.export(toTextFlow(), TextConverter.TEXT_LAYOUT_FORMAT, ConversionType.STRING_TYPE).toString();
		}
		
		//don't use - adds extra whitespace -- this needs to be looked at
		public function toMarkUpAsXML():XML
		{
			XML.ignoreWhitespace;
			return TextConverter.export(toTextFlow(), TextConverter.TEXT_LAYOUT_FORMAT, ConversionType.XML_TYPE) as XML;
		}
	
		public function toJson():Object
		{
			return {
				type:"FormattedText",
				format: TextConverter.TEXT_LAYOUT_FORMAT,
				text: _text
			}
		}
	
		public static function fromTextFlow(flow:TextFlow):FormattedText
		{
			var text:String = TextConverter.export(flow, TextConverter.TEXT_LAYOUT_FORMAT, ConversionType.STRING_TYPE).toString();
			return new FormattedText(text);
		}
	
		public static function fromJson(json:Object):FormattedText
		{
			getFonts(json.text);
			return new FormattedText(json.text);
		}
		
		private static function getFonts(source:String):void
		{
			var fonts:Array = new Array();
			var xml:XML = new XML(source);
			var namespace:Namespace = xml.namespace();
			
			var paragraphs:XMLList = xml.children();
			var paragraph:XML;
			for each (paragraph in paragraphs)
				parseParagraph(paragraph, namespace, fonts);
			
			var font:String;
			var fontController:FontController = FontController.instance;
			for each(font in fonts)
				fontController.requestByName(font);  //replace spaces with underscores and add .swf
		}
		
		private static function parseParagraph(paragraph:XML, namespace:Namespace,fonts:Array):void
		{
			var spans:XMLList = paragraph.namespace::span;
			var span:XML;
			for each(span in spans){
				var fontName:String = span.@fontFamily;
				
				if (fontName != null && fontName != "" && fonts.indexOf(fontName) == -1)
					fonts.push(fontName);
			}
		}
	
		public static function fromString(str:String):FormattedText
		{
			var flow:TextFlow;
			
			if(str == null || str == ""){  ////an empty string crashes ContainerController at "if (flowComposer.getLineAt(lineIdx).controller == this)"  lineIdx is -1
				str =" ";
			}
				
		   	flow = TextConverter.importToFlow(str, TextConverter.PLAIN_TEXT_FORMAT);
			
			var text:String = TextConverter.export(flow, TextConverter.TEXT_LAYOUT_FORMAT, ConversionType.STRING_TYPE).toString();
			return new FormattedText(text);
		}
		
		public function setColumnCount(value:Number):void
		{
			var tf:TextFlow = toTextFlow();
			tf.columnCount = value;
			_text = TextConverter.export(tf, TextConverter.TEXT_LAYOUT_FORMAT, ConversionType.STRING_TYPE).toString();
		}
		
		public function setColumnGap(value:Number):void
		{
			var tf:TextFlow = toTextFlow();
			tf.columnGap = value;
			_text = TextConverter.export(tf, TextConverter.TEXT_LAYOUT_FORMAT, ConversionType.STRING_TYPE).toString();
		}
		
		public static function convertToString(value:String):String
		{
			if(value.search(/^<TextFlow/) < 0) //not a textflow, pass through
				return value;
			
			var flow:TextFlow = TextConverter.importToFlow(value, TextConverter.TEXT_LAYOUT_FORMAT);
			var string:String = TextConverter.export(flow,TextConverter.PLAIN_TEXT_FORMAT, ConversionType.STRING_TYPE).toString();
			return string;
		}
	}
}
