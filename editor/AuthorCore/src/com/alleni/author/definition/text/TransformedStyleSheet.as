package com.alleni.author.definition.text
{
	import flash.text.FontStyle;
	import flash.text.StyleSheet;
	import flash.text.TextFormat;
	
	import flashx.textLayout.formats.TextDecoration;

	public class TransformedStyleSheet extends StyleSheet
	{
		private var _validCSS:Boolean = true;
		
		public function TransformedStyleSheet()
		{
		}
		
		public function get validCSS():Boolean
		{
			return _validCSS;
		}

		public function set validCSS(value:Boolean):void
		{
			if(_validCSS)  //once validCSS is false, it stays false until reset
				_validCSS = value;
		}
		
		public function resetValidCSS():void{
			_validCSS = true;
		}

		override public function transform(formatObject:Object):TextFormat{
			
			var textFormat:TextFormat = new TextFormat();
			
			trace("formatObject", formatObject);
			
			for (var property:String in formatObject){
				
				var value:String = formatObject[property];
				
				switch(property){
					
					case "fontSize":
						if(validateFontSize(value) == false){
							this.validCSS = false;
							return null;
						}
						break;
					case "color":
						if(validateColor(value) == false){
							this.validCSS = false;
							return null;
						}
						break;
					case "fontName":
						if(validateFontFamily(value) == false){
							this.validCSS = false;
							return null;
						}
						break;
					case "fontStyle":
						if(validateFontStyle(value) == false){
							this.validCSS = false;
							return null;
						}
						break;
					case "fontWeight":
						if(validateFontWeight(value) == false){
							this.validCSS = false;
							return null;
						}
						break;
					case "textDecoration":
						if(validateTextDecoration(value) == false){
							this.validCSS = false;
							return null;
						}
						break;
						
				}
				
				
				
			}
			return textFormat;
		}
		
		private function validateFontSize(property:String):Boolean{
				property = property.replace(/pt|px/i, "");
				return isNumber(property);
		}
		private function validateColor(property:String):Boolean{
			property = property.replace("#","0x")
			return isNumber(property);
		}
		private function validateFontFamily(property:String):Boolean{
			return true; //assume true for now
		}
		private function validateFontStyle(property:String):Boolean{
			
			if(property == FontStyle.REGULAR || property == FontStyle.ITALIC){  //do the conversion from oblique
				return true;
			}
			else{
				return false;
			}
		}
		private function validateFontWeight(property:String):Boolean{
			if(property == FontStyle.BOLD || property == FontStyle.REGULAR){  //do the conversion
				return true;
			}
			else{
				return false;
			}
		}
		private function validateTextDecoration(property:String):Boolean{
			if(property == TextDecoration.UNDERLINE || property == TextDecoration.NONE){  //do the conversion
				return true;
			}
			else{
				return false;
			}
		}
		
		private function isNumber(val:String):Boolean {
			if (!isNaN(Number(val))) {
				return true;
			}
			return false;
		}
		
		
	}
}