package com.alleni.author.definition.text{
	
		import com.alleni.author.definition.Modifiers;
		import com.alleni.author.definition.PropertyDescription;
		import com.alleni.author.definition.text.TextSelection;
		import com.alleni.author.model.AbstractObject;
		import com.alleni.author.model.objects.TextObject;
		
		import flash.text.engine.FontPosture;
		import flash.text.engine.FontWeight;
		import flash.utils.Dictionary;
		
		import flashx.textLayout.formats.TextDecoration;
		
		public class StyleMediator {
			
			private var _styleName:String;
			private var _TLF2CSS:Dictionary = new Dictionary();
			private var _TLF2Props:Dictionary = new Dictionary();
			private var _textSelection:TextSelection;
			private var _textObject:TextObject;
			
			
			public function StyleMediator(textObject:TextObject, styleName:String)
			{
				
				_textObject = textObject;
				_styleName = styleName;
			}

			
			public function setFontProperty(name:String, value:Object):void {
				_textObject.css.addProperty("." + _styleName, name, String(value));
				_textObject.stylesValid = false;
			}
			
			public function getFontProperty(name:String):Object {
				return _textObject.css.getProperty("." + _styleName, name);
			}
			
			public function set fontFamily(value:String):void {
				setFontProperty("font-family", value);
			}
			
			public function get fontFamily():String {
				return getFontProperty("font-family") as String;
			}
			
			public function set fontSize(value:Number):void {
				if (value < 1)return; //fontsize must be > 0 or textflow crashes
				var size:String = String(value) + "pt";
				setFontProperty("font-size", size);
			}
			
			public function get fontSize():Number {
				var size:String = getFontProperty("font-size") as String;
				size = size.replace(/pt|px/i, "");
				var tempFontSize:Number = Number(size);  //fontsize must be > 0 or textflow crashes
				return tempFontSize > 0? tempFontSize:1;  
			}
			
			public function set fontColor(value:uint):void {
				var colorString:String = "#" + value.toString(16);
				setFontProperty("color", colorString);
			}
			
			public function get fontColor():uint {
				var value:String =  (getFontProperty("color")).replace("#","0x");
				var color:uint = uint(value);
				return color;
			}
			
			public function set textAlpha(value:Number):void {
				setFontProperty("text-alpha", value);
			}
			
			public function get textAlpha():Number {
				return Number(getFontProperty("text-alpha"));
			}
			
			public function set bold(value:Boolean):void {
				setFontProperty("font-weight", value == true?FontWeight.BOLD:FontWeight.NORMAL);
			}
			
			public function get bold():Boolean {
				var prop:String = getFontProperty("font-weight") as String;
				return prop == FontWeight.BOLD?true:false;
			}
			
			public function set italic(value:Boolean):void {
				setFontProperty("font-style", value == true?FontPosture.ITALIC:FontPosture.NORMAL);
			}
			
			public function get italic():Boolean {
				var prop:String = getFontProperty("font-style") as String;
				return prop == FontPosture.ITALIC?true:false;
			}
			
			public function set underline(value:Boolean):void {
				setFontProperty("text-decoration", value == true?TextDecoration.UNDERLINE:TextDecoration.NONE);
			}
			
			public function get underline():Boolean {
				var prop:String = getFontProperty("text-decoration") as String;
				return prop == TextDecoration.UNDERLINE?true:false;
			}
			
			//this does not currently work
			public function set columnCount(value:Number):void {
				setFontProperty("column-count", value);
			}
			
			public function get columnCount():Number {
				return Number(getFontProperty("column-count"));
			}
			
			//this does not currently work
			public function set columnGap(value:Number):void {
				setFontProperty("column-gap", value);
			}
			
			public function get columnGap():Number {
				return Number(getFontProperty("column-gap"));
			}
			
			private function mapTLF2CSS():void{
				
				_TLF2CSS["fontName"] = "font-family";
				_TLF2CSS["fontSize"] = "font-size";
				_TLF2CSS["color"]    = "color";
				_TLF2CSS["fontStyle"] = "font-style";
				_TLF2CSS["fontWeight"] = "font-weight";
				_TLF2CSS["textDecoration"] = "text-decoration";
				_TLF2CSS["textAlpha"] = "text-alpha";
				_TLF2CSS["columnCount"] = "column-count";
				
			}
			
			private function mapTLF2Props():void{
				
				_TLF2Props["fontName"] = "fontFamily";
				_TLF2Props["fontSize"] = "fontSize";
				_TLF2Props["color"]    = "fontColor";
				_TLF2Props["fontStyle"] = "italic";
				_TLF2Props["fontWeight"] = "bold";
				_TLF2Props["textDecoration"] = "underline";
				_TLF2Props["textAlpha"] = "fontAlpha";
				
			}
		}
	}
