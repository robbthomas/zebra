/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 2/11/11
 * Time: 4:26 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition.text {
import com.alleni.author.controller.objects.TextObjectMediator;
import com.alleni.author.controller.ui.palettes.InspectorController;
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.objects.TextObject;
import com.alleni.author.model.ui.StyleAnchor;
import com.alleni.author.view.text.IRichEditor;
import com.alleni.author.view.text.RichTextField;
import com.alleni.taconite.model.ITaconiteObject;
import com.alleni.taconite.model.ModelRoot;
import com.alleni.taconite.model.TaconiteModel;

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.text.engine.FontPosture;
import flash.text.engine.FontWeight;
import flash.utils.Dictionary;

import flashx.textLayout.formats.TextDecoration;
import flashx.textLayout.formats.TextLayoutFormat;

import mx.events.PropertyChangeEvent;


public class TextSelection extends EventDispatcher implements ITaconiteObject {

	private var _format:TextLayoutFormat;
	private var _textField:RichTextField;
	private var _textObject:TextObject;
	private var _styleName:String = "";
    public var propertyInspector:Dictionary = new Dictionary();
	

	
	public var modifiers:Dictionary = new Dictionary();
	public var anchors:Dictionary = new Dictionary();


	public function TextSelection(textObject:TextObject,textField:RichTextField) {
		_textObject = textObject;
		_textField = textField;
		
		//styles:  reinstate styles by uncommenting next line
		//var styleAnchor:StyleAnchor = new StyleAnchor(_textObject,this);
		
		//styles:  reinstate styles by uncommenting next line
		//modifiers[styleAnchor.modifierDescription.key] = styleAnchor.modifierDescription;
		
		//styles:  reinstate styles by uncommenting next line
		//anchors[styleAnchor.modifierDescription.key] = styleAnchor;
		
		modifiers["fontFamily"] = Modifiers.instance.fetch("fontFamily");
		modifiers["fontSize"] = Modifiers.instance.fetch("fontSize");
		modifiers["bold"] = Modifiers.instance.fetch("bold");
		modifiers["italic"] = Modifiers.instance.fetch("italic");
		modifiers["underline"] = Modifiers.instance.fetch("underline");
		modifiers["fontColor"] = Modifiers.instance.fetch("fontColor");
		modifiers["fontAlpha"] = Modifiers.instance.fetch("fontAlpha");

        for each (var modifier:IModifier in modifiers) {
            propertyInspector[modifier.key] = true;
        }
	}
	

	public function get format():TextLayoutFormat
	{
		return _format;
	}

	//use these dispatches to update the inspector
	public function set format(value:TextLayoutFormat):void
	{
		_format = value;
		//dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "styleName", null, styleName));
		dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "fontFamily", null, fontFamily));
		dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "fontSize", null, fontSize));
		dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "bold", null, bold));
		dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "italic", null, italic));
		dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "underline", null, underline));
		dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "fontColor", null, fontColor));
		dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "fontAlpha", null, fontAlpha));
	}
	
	
	
	public function set styleName(value:String):void {
		_styleName = value;
		_textField.styleName = value;
	}
	public function get styleName():String{
		return _styleName;
	}

	public function get fontFamily():String {
		return _format.fontFamily;
	}
	

	public function set fontFamily(value:String):void {
		
		if (isAllSelected()){
			_textField.clearStyleFromAllElements(ActionMap.FONTNAME_ACTION);
			_textObject.fontFamily= value;
		}
		else{
			_textField.fontFamily = value;
		}
	}

	public function get fontSize():uint {
		return _format.fontSize;
	}

	public function set fontSize(value:uint):void {
		if (isAllSelected()){
			//trace("TextSelection>setFontSize>isAllSelected");
			_textField.clearStyleFromAllElements(ActionMap.FONTSIZE_ACTION);
			_textObject.fontSize = value;
		}
		else{
			trace("TextSelection>setFontSize>_textField.size=value");
			_textField.size = value;
		}
		
		refreshSelection();
	}

	public function get bold():Boolean {
		return _format.fontWeight == FontWeight.BOLD?true:false;
	}
	
	public function set bold(value:Boolean):void{
		if (isAllSelected()){
			_textField.clearStyleFromAllElements(ActionMap.BOLD_ACTION);
			_textObject.bold = value;
		}
		else{
			_textField.bold = value;
		}
		refreshSelection();
	}

	public function get italic():Boolean {
		return _format.fontStyle == FontPosture.ITALIC?true:false;
	}

	public function set italic(value:Boolean):void {
		if (isAllSelected()){
			_textField.clearStyleFromAllElements(ActionMap.ITALIC_ACTION);
			_textObject.italic = value;
		}
		else{
			_textField.italic = value;
		}
		refreshSelection();
	}

	public function get underline():Boolean {
		return _format.textDecoration == TextDecoration.UNDERLINE?true:false;
	}

	public function set underline(value:Boolean):void {
		if (isAllSelected()){
			_textField.clearStyleFromAllElements(ActionMap.UNDERLINE_ACTION);
			_textObject.underline = value;
		}
		else{
			_textField.underline = value;
		}
		refreshSelection();
	}

	public function get fontColor():uint {
		return _format.color;
	}

	public function set fontColor(value:uint):void {
		if (isAllSelected()){
			_textField.clearStyleFromAllElements(ActionMap.FOREGROUNDCOLOR_ACTION);
			_textObject.fontColor = value;
		}
		else{
			_textField.color = value;
		}
		refreshSelection();
	}

	public function get fontAlpha():Number {
		return _format.textAlpha * 100;
	}

	public function set fontAlpha(value:Number):void {
		
		if (isAllSelected()){
			_textField.clearStyleFromAllElements(ActionMap.FONTALPHA_ACTION);
			_textObject.fontAlpha = value;
		}
		else{
			_textField.fontAlpha = value/100;
		}
		refreshSelection();
	}

	public function get model():TaconiteModel {
		return null;
	}

	public function get modelRoot():ModelRoot {
		return null;
	}
	
	public function isAllSelected():Boolean{
		
		if(!_textObject)
			return false;
		
		if(_textField.textFlow && _textField.textFlow.interactionManager){
			var begin:uint = _textField.textFlow.interactionManager.absoluteStart;
			var end:uint = _textField.textFlow.interactionManager.absoluteEnd;
			var length:uint = _textField.text.length;
			
			if (begin == 0 && end == length){
				return true;
			}
			else{
				return false;
			}
		}
		
		return false;
	}
	
	private function refreshSelection():void{
		if (_textField.stage)
			_textField.addEventListener(Event.ENTER_FRAME, refreshSelectionListener);
	}
	
	private function refreshSelectionListener(event:Event):void
	{
		if (_textField.stage)
			_textField.removeEventListener(Event.ENTER_FRAME, refreshSelectionListener);
		
		trace("TextSelection > refreshSelectionListener");
		var start:int = _textField.selectionStart;
		var end:int = _textField.selectionEnd;
		_textField.select(-1, -1);
		_textField.select(start, end);
	}
	
	
}
}
