/**
 * Created with IntelliJ IDEA.
 * User: mfitch
 * Date: 9/6/12
 * Time: 2:07 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.definition {
public class FlowTooltips {
    public static const ADD_MASTER_BEFORE:String = "Add master before this master";
    public static const BACKGROUND_MASTER:String = "Background master- appears behind event objects \n(click to move to foreground)";
    public static const FOREGROUND_MASTER:String = "Foreground master- appears in front of event objects \n(click to move to background)";
    public static const EDIT_THIS_MASTER:String = "Edit this master";
    public static const DELETE_THIS_MASTER:String = "Delete this master";
    public static const ADD_MASTER_AFTER:String = "Add master after this master";

    public static const ADD_BEFORE_EVENT:String = "Add event before this event";
    public static const MASTERS:String = "Masters (12 maximum)\n(click to show or hide)";
    public static const EDIT_THIS_EVENT:String = "Edit this event";
    public static const RUN_FROM_THIS_EVENT_FORWARD:String = "Run from this event forward";
    public static const RESUME_FROM_THIS_EVENT:String = "Resume from this event";
    public static const DELETE_THIS_EVENT:String = "Delete this event";
    public static const ADD_EVENT_AFTER:String = "Add event after this event";
    public static const PREV_PAGE:String = "Go to previous event (Ctrl(Cmd)-Left-Arrow key)";
    public static const NEXT_PAGE:String = "Go to next event (Ctrl(Cmd)-Right-Arrow key)";

    public static const HOVER_OVER_MASTER:String = "Drag master to scroll bar to reorder";
    public static const HOVER_OVER_EVENT:String = "Drag event to scroll bar to reorder";

    public static const EVENT_FLOW_SLIDER:String = "Event Flow Slider\n(drag to view event sequence)";
    public static const LEFT_STACK_SLIDER:String = "Left Stack Slider \n(drag to view a master or event on the left side)";
    public static const RIGHT_STACK_SLIDER:String = "Right Stack Slider \n(drag to view a master or event on the right side)";
}
}
