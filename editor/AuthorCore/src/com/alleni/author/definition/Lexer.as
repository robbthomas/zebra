package com.alleni.author.definition
{
	public class Lexer
	{
		public function Lexer()
		{
		}
		
		
		public static function lex(exp:String):Object
		{
			var rslt:Object = {obj:null, next:null};
			var last:Object = rslt;
			var start:int = 0;
			var state:String = null;
			var state2:String = null;
			for(var i:int=0; i<=exp.length; i++) {
				var c:String = i<exp.length?exp.charAt(i):"";
				switch(state) {
					case "number":
						if(c.search(/^\d$/) == 0) {
							if(state2 == "exponent-sign") {
								state2 = "exponent";
							}
							continue;
						} else if(c.search(/^\.$/) == 0) {
							if(state2 == "decimal") {
								throw new Error("Error at position " + start + " second decimal found in number literal");
							} else if(state2 == "exponent" || state2 == "exponent-sign") {
								throw new Error("Error at position " + start + " decimal not allowed in exponent");
							} else {
								state2 = "decimal";
								continue;
							}
						} else if(c.search(/^[eE]$/) == 0) {
							if(state2 == "exponent" || state2 == "exponent-sign") {
								throw new Error("Error at position " + start + " second exponent in number literal");
							} else {
								state2 = "exponent-sign";
								continue;
							}
						} else if(c.search(/^[+-]$/) == 0) {
							if(state2 == "exponent-sign") {
								state2 = "exponent";
								continue;
							}
							// fall through
						}
						if(state2 == "exponent-sign") {
							throw new Error("Error at position " + start + " exponent value required but [" + c + "] found");
						}

						last.obj = {kind:"number", text:exp.substring(start,i), position:start}
						last.next = {obj:null, next:null};
						last = last.next;
						start = i;
						state = null;
						state2 = null;
						break;
					case "identifier":
						if(c.search(/^\w$/) == 0) {
							continue;
						} else {
							var word:String = exp.substring(start,i).toLowerCase();
							switch(word) {
								case "and": last.obj = {kind:"operator", text:"&", position:start, level:4}; break;
								case "or": last.obj = {kind:"operator", text:"|", position:start, level:4}; break;
								case "not": last.obj = {kind:"operator", text:"!", position:start, level:-1}; break;
								default: last.obj = {kind:"identifier", text:word, position:start}; break;
							}
							last.next = {obj:null, next:null};
							last = last.next;
							start = i;
							state = null;
						}
						break;
					default:
						break;
				}
				if(i == exp.length) {
					break;
				}
				// start a new token
				if(c.search(/^\s$/) == 0) {
					start = i+1; // ignore
				} else if(c.search(/^\d$/) == 0) {
					state = "number";
				} else if(c.search(/^\.$/) == 0) {
					state = "number";
					state2 = "decimal";
				} else if(c.search(/^[a-zA-Z]$/) == 0) {
					state = "identifier"
				} else if(c.search(/^[\<\>\=]$/) == 0) {
					var c2:String = (i+1)<exp.length?exp.charAt(i+1):"";
					if((c+c2).search(/^(\<\=|\>\=|\<\>)$/) == 0) {
						last.obj = {kind:"operator", text:c+c2, position:start, level:3}
						last.next = {obj:null, next:null};
						last = last.next;
						start = i+2;
						i++;
					} else {
						last.obj = {kind:"operator", text:c, position:start, level:3}
						last.next = {obj:null, next:null};
						last = last.next;
						start = i+1;
					}
				} else if(c.search(/^[\*\/]$/) == 0) {
					last.obj = {kind:"operator", text:c, position:start, level:1}
					last.next = {obj:null, next:null};
					last = last.next;
					start = i+1;
//				} else if(c == "%") {
//					last.obj = {kind:"operator", text:c, position:start, level:2}
//					last.next = {obj:null, next:null};
//					last = last.next;
//					start = i+1;
				} else if(c.search(/^[\+\-]$/) == 0) {
					last.obj = {kind:"operator", text:c, position:start, level:2}
					last.next = {obj:null, next:null};
					last = last.next;
					start = i+1;
				} else if(c.search(/^[\&\|]$/) == 0) {
					last.obj = {kind:"operator", text:c, position:start, level:4}
					last.next = {obj:null, next:null};
					last = last.next;
					start = i+1;
				} else if(c.search(/^[\!]$/) == 0) {
					last.obj = {kind:"operator", text:c, position:start, level:-1}
					last.next = {obj:null, next:null};
					last = last.next;
					start = i+1;
				} else if(c == "(") {
					last.obj = {kind:"open paren", text:c, position:start}
					last.next = {obj:null, next:null};
					last = last.next;
					start = i+1;
				} else if(c == ")") {
					last.obj = {kind:"close paren", text:c, position:start}
					last.next = {obj:null, next:null};
					last = last.next;
					start = i+1;
				} else if(c == ",") {
					last.obj = {kind:"comma", text:c, position:start}
					last.next = {obj:null, next:null};
					last = last.next;
					start = i+1;
				} else if(c == "#") {
					last.obj = {kind:"identifier", text:"#", position:start}
					last.next = {obj:null, next:null};
					last = last.next;
					start = i+1;
				} else {
					throw new Error("Error at position " + start + " unknown symbol [" + c + "]");
				}
			}
			last.obj = {kind:"eof", text:"[EOF]", position:exp.length};
			return rslt;
		}
	}
}