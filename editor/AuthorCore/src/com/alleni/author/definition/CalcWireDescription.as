package com.alleni.author.definition
{
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;

	public class CalcWireDescription
	{
		public var attached:Boolean = true;
		
		private var _visible:Boolean;
		private var _wire1:Wire;
		private var _wire2:Wire;
		
		/**
		 * A CalcWireDescription provides a way to treat the two-wire + calc combination
	 	 * as a single wire for the purposes of showing and hiding wires in WireController
		 * (e.g., showWiresForSelectedObjects())
		 * 
		 * @param wire1: The wire going TO the calc inlet port
		 * @param wire2: The wire going FROM the calc outlet port
		 * 
		 */
		public function CalcWireDescription(wire1:Wire, wire2:Wire)
		{
			_wire1 = wire1;
			_wire2 = wire2;
		}
		
		public function set visible(value:Boolean):void
		{
			_visible = value;
			
			_wire1.visible = value;
			_wire2.visible = value;
		}
		
		public function get visible():Boolean
		{
			return _visible;
		}
		
		public function get masterAnchor():WireAnchor
		{
				return _wire1.masterAnchor;
		}
		
		public function get slaveAnchor():WireAnchor
		{
				return _wire2.slaveAnchor;
		}
	}
}