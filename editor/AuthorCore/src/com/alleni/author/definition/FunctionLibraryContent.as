package com.alleni.author.definition {
import com.alleni.savana.Ast;

public class FunctionLibraryContent {
    public static function getVariableFunctionDescriptions():Object{
        return {
    //CALCULATOR
        //GENERAL
            calculatorVariables:{
                label:"Calculator Variables",
                description:"NAME:\n" +
                    "Calculator Variables\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "When properties are wired into the calculator, representative variables labeled A-Z are automatically created in the upper section of the calculator. These letter variables can be used in the lower half of the calculator in expressions.\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      A = 15\n" +
                    "      B = 20\n" +
                    "   Expression:\n" +
                    "      A + B\n" +
                    "   Result:\n" +
                    "      35\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      B = 10\n" +
                    "      D = 5\n" +
                    "   Expression:\n" +
                    "      B > D\n" +
                    "   Result:\n" +
                    "      True\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      A = 1.34\n" +
                    "   Expression:\n" +
                    "      ceil(A)\n" +
                    "   Result:\n" +
                    "      2.0",
                code:"",
                category:Ast.CALCULATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            calculatorLocalVariables:{
                label:"Calculator Local Variables",
                description:"NAME:\n" +
                    "Calculator Local Variables\n" +
                    "(Custom Name)\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "In the top half of the calculator you can rename wired-in properties to use in your expressions below. The variable A can be labeled any string like \"Score\" or \"Zebras\".\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      A = 10\n" +
                    "      B = 6\n" +
                    "      Var A Label = Zebras\n" +
                    "      Var B Label = Toads\n" +
                    "   Expression:\n" +
                    "      Zebras + Toads\n" +
                    "   Result:\n" +
                    "      16",
                code:"",
                category:Ast.CALCULATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            calculatorSequence:{
                label:"Calculator Sequence",
                description:"NAME:\n" +
                    "Calculator Sequences\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Rows of expression in the lower half of the calculator can be used in succession on multiple lines to create a cumulative value. When an expression row begins with an operator, it builds on the value of the previous row.\n" +
                    "\n" +
                    "When an expression row begins with something that is not an operator (Variable, expression, constant, etc), it will start a new sequence.\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      A = 2\n" +
                    "   Expression Row 1:\n" +
                    "      A + 2\n" +
                    "   Expression Row 2:\n" +
                    "      -1\n" +
                    "   Expression Row 3:\n" +
                    "      +4\n" +
                    "   Final Result:\n" +
                    "      7\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      A = 3\n" +
                    "      B = 7\n" +
                    "   Expression Row 1:\n" +
                    "      A * B\n" +
                    "   Expression Row 2:\n" +
                    "      -B\n" +
                    "   Expression Row 3:\n" +
                    "      +4\n" +
                    "   Final Result:\n" +
                    "      18",
                code:"",
                category:Ast.CALCULATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            addExpressionRow:{
                label:"Add Expression Row",
                description:"NAME:\n" +
                    "Add Expression Row\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "To create a new expression row in the bottom half of the calculator, select the bottom expression row of the calculator (or where it says \"Enter Expression\") and hit enter.",
                code:"",
                category:Ast.CALCULATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            addVariables:{
                label:"Add Variables",
                description:"NAME:\n" +
                    "Add Variables\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "There are two ways to create new variables in the top half of the calculator.\n" +
                    "\n" +
                    "The first option is to wire a property to the '+' on the left of the blue bar on the calculator. This will create a new variable, which is wired in from the property.\n" +
                    "\n" +
                    "The second option is to simply click the '+' on the left of the blue bar. This will create a new variable which is not wired to any properties (a local variable).",
                code:"",
                category:Ast.CALCULATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            judgeNow:{
                label:"Judge Now",
                description:"NAME:\n" +
                    "Judge Now\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "The judge now gavel is located in the top left of the calculator. A trigger can be wired to the judge now in order to judge the calculator's expressions only when that trigger is fired.",
                code:"",
                category:Ast.CALCULATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},
    //SYSTEM
        //Math
            abs:{
                label:"abs(x)",
                description:"NAME:\n" +
                    "Absolute\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns an absolute (positive) value of the input.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   abs(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      abs(-25)\n" +
                    "   Result:\n" +
                    "      25",
                code:"abs(-2)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            acos:{
                label:"acos(ratio)",
                description:"NAME:\n" +
                    "Arc Cosine\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the arc cosine of the input (range -1 to 1). Result is in radians (range -pi / 2 to pi / 2).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   acos(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      acos(1)\n" +
                    "   Result:\n" +
                    "      0",
                code:"acos(-1)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            asin:{
                label:"asin(ratio)",
                description:"NAME:\n" +
                    "Arc Sine\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the arc sine of the input (range -1 to 1). Result is in radians (range -pi / 2 to pi / 2).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   asin(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      asin(1)\n" +
                    "   Result:\n" +
                    "      1.57079",
                code:"asin(-1)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            atan:{
                label:"atan(ratio)",
                description:"NAME:\n" +
                    "Arc Tangent\n" +
                    "\n" +
                    "Used in math. Returns the angle of the tangent specified in the input (range -1 to 1). Results is in radians (range -pi / 2 to pi / 2).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   atan(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      atan(1)\n" +
                    "   Result:\n" +
                    "      0.78539",
                code:"atan(-1)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            atan2:{
                label:"atan2(y, x)",
                description:"NAME:\n" +
                    "Arc Tangent 2\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the angle of the point for the two inputs (y, x), when measured counterclockwise from a circle's x axis (where [0,0] represents the center of the circle. Result is in radians (range -pi to pi).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   atan2(y, x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      atan2(1, 1)\n" +
                    "   Result:\n" +
                    "      0.78539",
                code:"atan2(0, -1)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            ceil:{
                label:"ceil(x, digits)",
                description:"NAME:\n" +
                    "Ceiling\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the input value rounded up to the nearest whole value in the decimal place specified by the digits value (optional).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   ceil(x)\n" +
                    "   ceil(x, digits)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      ceil(5.3)\n" +
                    "   Result:\n" +
                    "      6\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      ceil(7.231, 2)\n" +
                    "   Result:\n" +
                    "      7.24",
                code:"ceil(4.234, 2)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            cos:{label:"cos(angle)",
                description:"NAME:\n" +
                    "Cosine\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the cosine of the input angle. Result is in radians (range -1 to 1).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   cos(a)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      cos(1)\n" +
                    "   Result:\n" +
                    "      0.5403\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      cos(2 * pi)\n" +
                    "   Result:\n" +
                    "      1",
                code:"cos(2*pi)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            exp:{
                label:"exp(x)",
                description:"NAME:\n" +
                    "Exponent\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Returns the base of the natural logarithm (e), to the power of the input exponent.\n" +
                    "\n" +
                    "exp(x) is e to the power of x\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   exp(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      exp(2)\n" +
                    "   Result:\n" +
                    "      7.389",
                code:"exp",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            floor:{
                label:"floor(x, digits)",
                description:"NAME:\n" +
                    "Floor\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the input value rounded down to the nearest whole value in the decimal place specified by the digits value (optional).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   floor(x)\n" +
                    "   floor(x, digits)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      floor(5.8)\n" +
                    "   Result:\n" +
                    "      5\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      floor(7.739, 2)\n" +
                    "   Result:\n" +
                    "      7.73",
                code:"floor(4.234, 2)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            isnan:{
                label:"isNaN",
                description:"NAME:\n" +
                    "Is Not a Number\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns true if the input value is not a number. Returns false if the input is a number.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   isNaN(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      isNaN(\"word\")\n" +
                    "   Result:\n" +
                    "      True\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      isNaN(7)\n" +
                    "   Result:\n" +
                    "      False",
                code:"",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            log:{
                label:"log(x)",
                description:"NAME:\n" +
                    "Natural Logarithm\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the natural logarithm of the input.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   log(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      log(e)\n" +
                    "   Result:\n" +
                    "      1\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      log(1)\n" +
                    "   Result:\n" +
                    "      0",
                code:"log(2*e)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            max:{
                label:"max(x, y, z, ..., n)",
                description:"NAME:\n" +
                    "Maximum\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math comparisons. Returns the largest value from a list of input values, or +Infinity if no values are given.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   max()\n" +
                    "   max(x)\n" +
                    "   max(x, y, z, ..., n)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      max()\n" +
                    "   Result:\n" +
                    "      +Infinity\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      max(2, -42.5, 0)\n" +
                    "   Result:\n" +
                    "      2",
                code:"max()\nmax(2)\nmax(2, -42.5, 0)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            min:{
                label:"min(x, y, z, ..., n)",
                description:"NAME:\n" +
                    "Minimum\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math comparisons. Returns the smallest value from a list of input values, or -Infinity if no values are given.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   min()\n" +
                    "   min(x)\n" +
                    "   min(x, y, z, ..., n)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      min()\n" +
                    "   Result:\n" +
                    "      -Infinity\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      min(2, -42.5, 0)\n" +
                    "   Result:\n" +
                    "      -42.5",
                code:"min()\nmin(2)\nmin(2, -42.5, 0)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            pow:{
                label:"pow(x, y)",
                description:"NAME:\n" +
                    "Power\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the first input to the power of the second.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   pow(x, y)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      pow(2, 4)\n" +
                    "   Result:\n" +
                    "      16",
                code:"pow(2,8)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            random:{
                label:"random()",
                description:"NAME:\n" +
                    "Random\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns a pseudo-random number between 0.0 and 1.0.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   random()\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      random()\n" +
                    "   Result:\n" +
                    "      0.43537\n" +
                    "      (random number between 0.0 and 1.0)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      round(random() * 100))\n" +
                    "   Result:\n" +
                    "      73\n" +
                    "(whole random number between 0 and 100)",
                code:"random()*6 + 1",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            round:{
                label:"round(x, digits)",
                description:"NAME:\n" +
                    "Round\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the input value rounded to the nearest whole value in the decimal place specified by the digits value (optional).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   round(x)\n" +
                    "   round(x, digits)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      round(5.3)\n" +
                    "   Result:\n" +
                    "      5\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      round(7.85)\n" +
                    "   Result:\n" +
                    "      8\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      round(7.231, 2)\n" +
                    "   Result:\n" +
                    "      7.23",
                code:"round(4.234, 2)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            sin:{
                label:"sin(angle)",
                description:"NAME:\n" +
                    "Sine\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the sine of the input (range -1 to 1).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   sin(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      sin(1)\n" +
                    "   Result:\n" +
                    "      0.84147",
                code:"sin(2*pi)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            sqrt:{
                label:"sqrt(x)",
                description:"NAME:\n" +
                    "Square Root\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Returns the square root of the input.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   sqrt(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      sqrt(16)\n" +
                    "   Result:\n" +
                    "      4",
                code:"sqrt(2)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            tan:{
                label:"tan(angle)",
                description:"NAME:\n" +
                    "Tangent\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns the tangent of the input angle (range -1 to 1).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   tan(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      tan(1)\n" +
                    "   Result:\n" +
                    "      1.5574",
                code:"cos(2*pi)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            toexponential:{
                label:"toExponential(x, digits)",
                description:"NAME:\n" +
                    "To Exponential\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns a string of the input in exponential notation out to the decimal place specified by the digits value (optional).\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   toExponential(x)\n" +
                    "   toExponential(x, digits)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toExponential(5.467)\n" +
                    "   Result:\n" +
                    "      5\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toExponential(400.23, 4)\n" +
                    "   Result:\n" +
                    "      4.0023e+2",
                code:"toExponential(4.234, 2)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            tofixed:{
                label:"toFixed(x, digits)",
                description:"NAME:\n" +
                    "To Fixed\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns a string representation of the input in fixed-point notation to the decimal place specified out to the digits value (optional). Digits value represents the amount of decimal places to read out to.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   toFixed(x)\n" +
                    "   toFixed(x, digits)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toFixed(5.467)\n" +
                    "   Result:\n" +
                    "      5\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toFixed(400.23, 4)\n" +
                    "   Result:\n" +
                    "      400.2300\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toFixed(pi, 3)\n" +
                    "   Result:\n" +
                    "      3.142",
                code:"toFixed(4.234, 2)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

            toprecision:{
                label:"toPrecision(x, digits)",
                description:"NAME:\n" +
                    "To Precision\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math. Returns a string representation of the input in either exponential or fixed-point notation depending on the case out to the digits value (optional). The digits value represents the amount of significant digits to read out to, which can include whole numbers and decimal places.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   toPrecision(x)\n" +
                    "   toPrecision(x, digits)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toPrecision(5.45)\n" +
                    "   Result:\n" +
                    "      5.45\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toPrecision(400.23, 4)\n" +
                    "   Result:\n" +
                    "      400.2\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toPrecision(pi, 3)\n" +
                    "   Result:\n" +
                    "      3.14\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toPrecision(4300000,2)\n" +
                    "   Result:\n" +
                    "      4.3e+6",
                code:"toPrecision(4.234, 2)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.MATH_TYPE},

        //SYSTEM
            //Character

            charcount:{
                label:"charCount(str)",
                description:"NAME:\n" +
                    "Character Count\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in string operations. Returns the number of characters in the input string.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   charCount(str)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      charCount(\"zebra\")\n" +
                    "   Result:\n" +
                    "      5",
                code:"charCount('ABBA')",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.STRING_TYPE},

            substr:{
                label:"substr(str, index, len)",
                description:"NAME:\n" +
                    "Substring\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in string operations. Returns a series of characters as a substring of the input string, beginning at index and with a length of len.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   substr(str)\n" +
                    "   substr(str, index)\n" +
                    "   substr(str, index, len)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      substr(\"interaction\")\n" +
                    "   Result:\n" +
                    "      \"interaction\"\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      substr(\"interaction\", 5)\n" +
                    "   Result:\n" +
                    "      \"action\"\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      substr(\"interaction\", 5, 3)\n" +
                    "   Result\n" +
                    "      \"act\"",
                code:"substr('ABBA',1,2)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.STRING_TYPE},

            tolowercase:{
                label:"toLowerCase(str)",
                description:"NAME:\n" +
                    "To Lower Case\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in string operations. Returns the input string in all lower case characters.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   toLowerCase(\"str\")\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toLowerCase(\"ZeBrA\")\n" +
                    "   Result:\n" +
                    "      \"zebra\"\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toLowerCase(\"ALLEN\")\n" +
                    "   Result:\n" +
                    "      \"allen\"",
                code:"toLowerCase('JESSE COYLE')",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.STRING_TYPE},

            touppercase:{
                label:"toUpperCase(str)",
                description:"NAME:\n" +
                    "To Upper Case\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Used in string operations. Returns the input string in all upper case characters.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   toUpperCase(\"str\")\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toUpperCase(\"ZeBrA\")\n" +
                    "   Result:\n" +
                    "      \"ZEBRA\"\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toUpperCase(\"allen\")\n" +
                    "   Result:\n" +
                    "      \"ALLEN\"",
                code:"toUpperCase('Jesse Coyle')",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.STRING_TYPE},

        //SYSTEM
            //General

            blue:{
                label:"blue(c)",
                description:"NAME:\n" +
                    "Blue\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Returns the value of blue in the input color. (0-255)\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   blue(c)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      blue(rgb(128, 0, 255))\n" +
                    "   Result:\n" +
                    "      255\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      blue(rgb(64, 10, 128))\n" +
                    "   Result:\n" +
                    "      128",
                code:"blue(rgb(128, 0, 255))",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            boolean:{
                label:"boolean(value)",
                description:"NAME:\n" +
                    "Boolean\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Converts the input to a boolean value (True/False).\n" +
                    "\n" +
                    "The results that convert to false are:\n" +
                    "   0\n" +
                    "   \"\"\n" +
                    "   \"false\"\n" +
                    "   false\n" +
                    "\n" +
                    "Everything else converts to true, including:\n" +
                    "   \"False\"\n" +
                    "   1\n" +
                    "   -1\n" +
                    "   \"True\"\n" +
                    "   \"true\"\n" +
                    "   true\n" +
                    "   \"Any Other String\"\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   boolean(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      boolean(1)\n" +
                    "   Result:\n" +
                    "      True\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      boolean(0)\n" +
                    "   Result:\n" +
                    "      False\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      boolean(\"Mary\")\n" +
                    "   Result:\n" +
                    "      True\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      boolean(\"false\")\n" +
                    "   Result:\n" +
                    "      False",
                code:"",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            color:{
                label:"color(value)",
                description:"NAME:\n" +
                    "Color\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Returns a color chip for the input (must be a decimal color value). Using the function FromBase() can convert a hexidecimal color value into the required decimal color value.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   color(value)\n" +
                    "   color(\"0xFFFFFF\")\n" +
                    "color(frombase(\"FFFFFF\",16))\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      color(\"0xFF0000\")\n" +
                    "   Result:\n" +
                    "      Red color chip\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      color(\"0xFFFF00\")\n" +
                    "   Result:\n" +
                    "      Yellow color chip\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "color(frombase(\"FF0000\",16))\n" +
                    "   Result:\n" +
                    "      Red color chip",
                code:"",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            contains:{
                label:"contains(list, item)",
                description:"NAME:\n" +
                    "Contains\n" +
                    "\n" +
                    "DESCIPTION:\n" +
                    "Returns true if the list contains the item.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   contains([list], item)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      contains([1, 2, 3], 3)\n" +
                    "   Result:\n" +
                    "      True\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      contains([14, 73], 3)\n" +
                    "   Result:\n" +
                    "      False\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "    contains([\"a\", \"an\"], \"an\")\n" +
                    "   Result:\n" +
                    "      True",
                code:"contains([2,3,4], 3)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            empty:{
                label:"empty(list)",
                description:"NAME:\n" +
                    "Empty\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Returns true if a list is empty.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   empty([list])\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      empty([])\n" +
                    "   Result:\n" +
                    "      True\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      empty([5,9])\n" +
                    "   Result:\n" +
                    "      False",
                code:"empty([])",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            frombase:{
                label:"fromBase(str, base)",
                description:"NAME:\n" +
                    "From Base\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Converts the input string to a number using the second input as the numeric base.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   fromBase(str, base)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      fromBase(\"A\", 16)\n" +
                    "   Result:\n" +
                    "      10\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      fromBase(\"F6\", 16)\n" +
                    "   Result:\n" +
                    "      246",
                code:"fromBase('F6', 16)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            green:{
                label:"green(c)",
                description:"NAME:\n" +
                    "Green\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Returns the value of green in the input color. (0-255)\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   green(c)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      green(rgb(0, 128, 255))\n" +
                    "   Result:\n" +
                    "      128\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      green(rgb(64, 10, 128))\n" +
                    "   Result:\n" +
                    "      10",
                code:"green(rgb(128, 0, 255))",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            length:{
                label:"length(list)",
                description:"NAME:\n" +
                    "Length\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Returns the length of the input list.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   length([x])\n" +
                    "   length([x, y, z, .., n])\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      length([1, 20, 15])\n" +
                    "   Result:\n" +
                    "      3\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      length([1, 4, 12, 5, 125])\n" +
                    "   Result:\n" +
                    "      5\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      length([\"a\", \"an\", \"and\"])\n" +
                    "   Result:\n" +
                    "      3",
                code:"length([1, 2, 3])",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            map:{
                label:"map(list, fun)",
                description:"NAME:\n" +
                    "Map\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Applies a function to each item in the list.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   map(list, fun)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      map([7, -5, 3], abs)\n" +
                    "   Result:\n" +
                    "      7, 5, 3\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      map([\"an\",\"a\",\"and\"],\n" +
                    "      charcount)\n" +
                    "   Result:\n" +
                    "      2, 1, 3\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "    map([1.7, 4.2, 6.1], round)\n" +
                    "   Result:\n" +
                    "      2, 4, 6",
                code:"map([1,-2,3], abs)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            number:{
                label:"number(value)",
                description:"NAME:\n" +
                    "Number\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Converts an input string to a number.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   number(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      number(\"150\")\n" +
                    "   Result:\n" +
                    "      150",
                code:"",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            red:{
                label:"red(c)",
                description:"NAME:\n" +
                    "Red\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Returns the value of red in the input color. (0-255)\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   red(c)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      red(rgb(128, 0, 255))\n" +
                    "   Result:\n" +
                    "      128\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      red(rgb(64, 10, 128))\n" +
                    "   Result:\n" +
                    "      64",
                code:"red(rgb(128, 0, 255))",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            reduce:{
                label:"reduce(list, fun, id)",
                description:"NAME:\n" +
                    "Reduce\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Using the id (optional) as initial value, applies a function (which requires 2 inputs) over a list and builds up a return value. \n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   reduce([list],fun)\n" +
                    "   reduce([list], fun, id)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      reduce([2, 3], pow, 4)\n" +
                    "   Result:\n" +
                    "      4096\n" +
                    "      (4 to the power of 2 to\n" +
                    "      the power of 3)",
                code:"",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            rgb:{
                label:"rgb(r, g, b)",
                description:"NAME:\n" +
                    "RGB\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Returns a color using the red, green, and blue input values.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   rgb(r,g,b)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      rgb(255, 255, 0)\n" +
                    "   Result:\n" +
                    "      Yellow color chip\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      rgb(255, 128, 0)\n" +
                    "   Result:\n" +
                    "      Orange color chip",
                code:"rgb(0, 0, 255)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            string:{
                label:"string(value)",
                description:"NAME:\n" +
                    "String\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Converts the input to a string.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   string(x)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      string(150)\n" +
                    "   Result:\n" +
                    "      \"150\"",
                code:"",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

            tobase:{
                label:"toBase(x, base)",
                description:"NAME:\n" +
                    "To Base\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Returns a converted input number of the numeric base (2 to 36) as a string.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   toBase(x, base)\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toBase(246, 16)\n" +
                    "   Result:\n" +
                    "      \"f6\"\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      toBase(24, 2)\n" +
                    "   Result:\n" +
                    "      11000",
                code:"toBase(245, 16)",
                category:Ast.FUNCTION_CATEGORY,
                type:Ast.GENERAL_TYPE},

        //System Variables
            //General

            dayname:{
                label:"dayname",
                description:"NAME:\n" +
                    "Day Name\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Result is a string for the name of the current day of the week.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   dayname\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      dayname\n" +
                    "   Result:\n" +
                    "      \"Wednesday\"",
                code:"Wednesday",
                category:Ast.VARIABLE_CATEGORY,
                type:Ast.GENERAL_TYPE},

            dayofmonth:{
                label:"dayofmonth",
                description:"NAME:\n" +
                    "Day of the Month\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Result is the current number of the day of the month.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   dayofmonth\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      dayofmonth\n" +
                    "   Result:\n" +
                    "      31",
                code:"31",
                category:Ast.VARIABLE_CATEGORY,
                type:Ast.GENERAL_TYPE},

            dayofweek:{
                label:"dayofweek",
                description:"NAME:\n" +
                    "Day of the Week\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Result is the current number of the day of the week.\n" +
                    "0 = Sunday\n" +
                    "1 = Monday\n" +
                    "2 = Tuesday\n" +
                    "...\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   dayofweek\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      dayofweek\n" +
                    "   Result:\n" +
                    "      3",
                code:"3",
                category:Ast.VARIABLE_CATEGORY,
                type:Ast.GENERAL_TYPE},

            fulldate:{
                label:"fulldate",
                description:"NAME:\n" +
                    "Full Date\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Result is the current full date.\n" +
                    "(Day, Month, Date, Year)\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   fulldate\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      fulldate\n" +
                    "   Result:\n" +
                    "      \"Friday, November 30,\n" +
                    "      2012\"",
                code:"Wednesday, August 10, 2011",
                category:Ast.VARIABLE_CATEGORY,
                type:Ast.GENERAL_TYPE},

            monthname:{
                label:"monthname",
                description:"NAME:\n" +
                    "Month Name\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Result is the name of the current month.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   monthname\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      monthname\n" +
                    "   Result:\n" +
                    "      \"August\"",
                code:"August",
                category:Ast.VARIABLE_CATEGORY,
                type:Ast.GENERAL_TYPE},

            monthofyear:{
                label:"monthofyear",
                description:"NAME:\n" +
                    "Month of the Year\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Result is the current number of the month of the year.\n" +
                    "0 = January\n" +
                    "1 = February\n" +
                    "2 = March\n" +
                    "...\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   monthofyear\n" +
                    "\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      monthofyear\n" +
                    "   Result:\n" +
                    "      7",
                code:"7",
                category:Ast.VARIABLE_CATEGORY,
                type:Ast.GENERAL_TYPE},

            sessiontime:{
                label:"sessiontime",
                description:"NAME:\n" +
                    "Session Time\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Duration of the current session since the user started the project or app. Result is in milliseconds.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   sessiontime\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      sessiontime\n" +
                    "   Result:\n" +
                    "      828625\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "  floor(sessiontime/60000)\n" +
                    "   Result:\n" +
                    "      13\n" +
                    "      (13 minutes)",
                code:"",
                category:Ast.VARIABLE_CATEGORY,
                type:Ast.GENERAL_TYPE},

            username:{
                label:"username",
                description:"NAME:\n" +
                    "User Name\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Name of the user who is logged in.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   username\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      username\n" +
                    "   Result:\n" +
                    "      \"JohnDoe47\"",
                code:"",
                category:Ast.VARIABLE_CATEGORY,
                type:Ast.GENERAL_TYPE},

            year:{
                label:"year",
                description:"NAME:\n" +
                    "Year\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Year number.\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      year\n" +
                    "   Result:\n" +
                    "      2012",
                code:"2011",
                category:Ast.VARIABLE_CATEGORY,
                type:Ast.GENERAL_TYPE},

    //SYSTEM CONSTANTS
        //MATH

            hashSymbol:{
                label:"#",
                description:"NAME:\n" +
                    "This Value\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "A symbol representing the current value of the ribbon or field.\n" +
                    "\n" +
                    "FORMAT:\n" +
                    "   #\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      # = 13\n" +
                    "   Expression:\n" +
                    "      #+1\n" +
                    "   Result:\n" +
                    "      14\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      # = True\n" +
                    "   Expression:\n" +
                    "      ~#\n" +
                    "   Result:\n" +
                    "      False\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      # = 15\n" +
                    "   Expression:\n" +
                    "      # > 10\n" +
                    "   Result:\n" +
                    "      True\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      # = 11.73\n" +
                    "   Expression:\n" +
                    "      floor(#)\n" +
                    "   Result:\n" +
                    "      11\n" +
                    "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      # = \"Zebra\"\n" +
                    "   Expression:\n" +
                    "      charCount(#)\n" +
                    "   Result:\n" +
                    "      5",
                code:"",
                category:Ast.CONSTANT_CATEGORY,
                type:Ast.MATH_TYPE},

            e:{
                label:"e",
                description:"NAME:\n" +
                    "e\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "A math constant for the base of natural\n" +
                    "logarithms, expressed as e.\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      e\n" +
                    "   Result:\n" +
                    "      2.71828...",
                code:"",
                category:Ast.CONSTANT_CATEGORY,
                type:Ast.MATH_TYPE},

            infinity:{
                label:"Infinity",
                description:"NAME:\n" +
                    "Infinity\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "A math constant value representing infinity.\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      9 / 0\n" +
                    "   Result:\n" +
                    "      Infinity",
                code:"",
                category:Ast.CONSTANT_CATEGORY,
                type:Ast.MATH_TYPE},

            nan:{
                label:"NaN",
                description:"NAME:\n" +
                    "Not a Number\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "The value representing Not a Number.\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      Infinity / Infinity\n" +
                    "   Result:\n" +
                    "      NaN",
                code:"",
                category:Ast.CONSTANT_CATEGORY,
                type:Ast.MATH_TYPE},

            pi:{
                label:"PI",
                description:"NAME:\n" +
                    "PI\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "A math constant for the ratio of the\n" +
                    "circumference of a circle to its diameter.\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      pi\n" +
                    "   Result:\n" +
                    "      3.145926...",
                code:"",
                category:Ast.CONSTANT_CATEGORY,
                type:Ast.MATH_TYPE},

    //SYSTEM Operators
        //GENERAL

            addition:{
                label:"+",
                description:"NAME:\n" +
                    "Addition\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math operations. Outputs a number\n" +
                    "that is the sum of the two sides of the\n" +
                    "operation.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a + b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      3 + 4\n" +
                    "   Result:\n" +
                    "      7",
                code:"a + b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            concatenate:{
                label:"^",
                description:"NAME:\n" +
                    "Concatenate\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in string operations. Takes two inputs,\n" +
                    "converts them to strings and combines them\n" +
                    "into one string of characters.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a ^ b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      \"Zebra\" ^ \"Zapps\"\n" +
                    "   Result:\n" +
                    "      \"ZebraZapps\"\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      1 ^ 2\n" +
                    "   Result:\n" +
                    "      \"12\"",
                code:"tom ^ cat",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            conditional:{
                label:"expression?value:value",
                description:"NAME:\n" +
                    "Conditional Operator\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in conditional operations. Takes a\n" +
                    "boolean expression (resulting in True/False),\n" +
                    "and uses it to determine the output. If the\n" +
                    "expression is judged True, then the result is\n" +
                    "the left side of the ' : '. If the expression is\n" +
                    "judged False, then the result is the right side of\n" +
                    "the ' : '.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a ? b : c\n" +
                    "   expression ? true : false\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      2 = 2 ? True : False\n" +
                    "   Result:\n" +
                    "      True\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      3 > 7 ? \"Yes\" : \"No\"\n" +
                    "   Result:\n" +
                    "      \"No\"\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      Checked = True\n" +
                    "   Expression:\n" +
                    "      Checked ? 1 : 0\n" +
                    "   Result:\n" +
                    "      1",
                code:"A<0?'A is negative':'A is positive'\n\nx=25?'correct':'incorrect'",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            division:{
                label:"/",
                description:"NAME:\n" +
                    "Division\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math operations. Outputs a number\n" +
                    "that is the quotient of the two sides of the\n" +
                    "operation.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a / b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      6 / 2\n" +
                    "   Result:\n" +
                    "      3",
                code:"a / b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            equalto:{
                label:"=",
                description:"NAME:\n" +
                    "Equal To\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in comparisons. Outputs a boolean value\n" +
                    "(True/False). When both inputs are equal, the\n" +
                    "result is True. When both inputs are not equal,\n" +
                    "the result is False.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a = b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      2 = 2\n" +
                    "   Result:\n" +
                    "      True\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      3 = 6\n" +
                    "   Result:\n" +
                    "      False",
                code:"a = b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            exponentiate:{
                label:"**",
                description:"NAME:\n" +
                    "Exponentiate\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math operations. Outputs a number\n" +
                    "value that is the result of a to the power of b.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a**b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      2**4\n" +
                    "   Result:\n" +
                    "      16",
                code:"a**b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            greaterthan:{
                label:">",
                description:"NAME:\n" +
                    "Greater Than\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in comparisons. Outputs a boolean value\n" +
                    "(True/False). When the left input is greater\n" +
                    "than the right input, the result is True. When\n" +
                    "the left input is less than or equal to the right\n" +
                    "input, the result is False.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a > b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      6 > 5\n" +
                    "   Result:\n" +
                    "      True\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      3 > 3\n" +
                    "   Result:\n" +
                    "      False\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      2 > 4\n" +
                    "   Result:\n" +
                    "      False",
                code:"a > b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            greaterthanorequalto:{
                label:">=",
                description:"NAME:\n" +
                    "Greater Than or Equal To\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in comparisons. Outputs a boolean value\n" +
                    "(True/False). When the left input is greater\n" +
                    "than or equal to the right input, the result is\n" +
                    "True. When the left input is less than the right\n" +
                    "input, the result is False.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a >= b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      4 >= 2\n" +
                    "   Result:\n" +
                    "      True\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      7 >= 7\n" +
                    "   Result:\n" +
                    "      True\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      3 >= 6\n" +
                    "   Result:\n" +
                    "      False",
                code:"a >= b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            lessthan:{
                label:"<",
                description:"NAME:\n" +
                    "Less Than\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in comparisons. Outputs a boolean value\n" +
                    "(True/False). When the left input is less than\n" +
                    "the right input, the result is True. When the left\n" +
                    "input is greater than or equal to the right input,\n" +
                    "the result is False.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a < b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      4 < 7\n" +
                    "   Result:\n" +
                    "      True\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      6 < 6\n" +
                    "   Result:\n" +
                    "      False\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      8 < 4\n" +
                    "   Result:\n" +
                    "      False",
                code:"a < b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            lessthanorequalto:{
                label:"<=",
                description:"NAME:\n" +
                    "Less Than or Equal To\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in comparisons. Outputs a boolean value\n" +
                    "(True/False). When the left input is less than or\n" +
                    "equal to the right input, the result is True.\n" +
                    "When the left input is greater than the right\n" +
                    "input, the result is False.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a <= b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      3 <= 5\n" +
                    "   Result:\n" +
                    "      True\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      7 <= 7\n" +
                    "   Result:\n" +
                    "      True\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      5 <= 2\n" +
                    "   Result:\n" +
                    "      False",
                code:"a <= b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            logicaland:{
                label:"&",
                description:"NAME:\n" +
                    "Logical AND\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in comparisons. Outputs a boolean value\n" +
                    "(True/False). When both sides of the\n" +
                    "comparison are True, the result is True. When\n" +
                    "either or both sides are False, the result is\n" +
                    "False.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a & b\n" +
                    "   a and b\n" +
                    "   a AND b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      a = 6\n" +
                    "   Expression:\n" +
                    "      a > 5 & a > 2\n" +
                    "   Result:\n" +
                    "      True\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      a = 5\n" +
                    "   Expression:\n" +
                    "      a >3 and a < 5\n" +
                    "   Result:\n" +
                    "      False",
                code:"a & b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            logicalnot:{
                label:"~",
                description:"NAME:\n" +
                    "Logical NOT\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in logical operations. Outputs a boolean\n" +
                    "value (True/False). Result is the inverse of the\n" +
                    "input. When the input is True, the result is\n" +
                    "False. When the input is False, the input is\n" +
                    "True.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   ~a\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      ~True\n" +
                    "   Result:\n" +
                    "      False\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      ~False\n" +
                    "   Result:\n" +
                    "      True",
                code:"~a",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            logicalor:{
                label:"|",
                description:"NAME:\n" +
                    "Logical OR\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in comparisons. Outputs a boolean value\n" +
                    "(True/False). When either or both sides of the\n" +
                    "comparison are True, the result is True. When\n" +
                    "both sides are False, the result is False.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a | b\n" +
                    "   a or b\n" +
                    "   a OR b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      a = 4\n" +
                    "   Expression:\n" +
                    "      a > 3 | a < 1\n" +
                    "   Result:\n" +
                    "      True\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      a = 10\n" +
                    "   Expression:\n" +
                    "      a >=0 or a < 10\n" +
                    "   Result:\n" +
                    "      False",
                code:"a | b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            multiplication:{
                label:"*",
                description:"NAME:\n" +
                    "Multiplication\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math operations. Outputs a number\n" +
                    "that is the product of the two sides of the\n" +
                    "operation.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a * b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      4 * 5\n" +
                    "   Result:\n" +
                    "      20",
                code:"a * b ",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            negate:{
                label:"-",
                description:"NAME:\n" +
                    "Negate\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math operations. Outputs a number\n" +
                    "that is inverse of the input. Subtracts the input\n" +
                    "from zero (0-a).\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   -a\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      a = 3\n" +
                    "   Expression:\n" +
                    "      -a\n" +
                    "   Result:\n" +
                    "      -3",
                code:"-a",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            notequalto:{
                label:"<>",
                description:"NAME:\n" +
                    "Not Equal To\n" +
                    "(less than or greater than)\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in comparisons. Outputs a boolean value\n" +
                    "(true/false). When the two inputs aren't equal,\n" +
                    "the result is True. When the two inputs are\n" +
                    "equal, the result is False.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   a <> b\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Condition:\n" +
                    "      a=5\n" +
                    "   Expression\n" +
                    "      a <> 5\n" +
                    "   Result:\n" +
                    "      False",
                code:"a <> b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            positive:{
                label:"+",
                description:"NAME:\n" +
                    "Positive\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math operations. Outputs a number\n" +
                    "that is the result of zero plus the input (0+a).\n" +
                    "Can be used to show obvious difference\n" +
                    "between positive and negative numbers. (+a is\n" +
                    "same as a).\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   Expression:\n" +
                    "      +3\n" +
                    "   Result:\n" +
                    "      3",
                code:"+a",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

            subtraction:{
                label:"-",
                description:"NAME:\n" +
                    "Subtraction\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Used in math operations. Outputs a number\n" +
                    "that is the difference of the two sides of the\n" +
                    "operation.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   Expression:\n" +
                    "      8 - 3\n" +
                    "   Result:\n" +
                    "      5",
                code:"a - b",
                category:Ast.OPERATOR_CATEGORY,
                type:Ast.GENERAL_TYPE},

    //KEY CONSTANTS
        //KEYS

            backslash:{
                label:"backslash",
                description:"NAME:\n" +
                    "Backslash\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the \\ key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   backslash",
                code:"backslash",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            backspace:{
                label:"backspace",
                description:"NAME:\n" +
                    "Backspace\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Backspace key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   backspace",
                code:"shift-backspace",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            capslock:{
                label:"capslock",
                description:"NAME:\n" +
                    "Caps Lock\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Caps Lock key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   capslock",
                code:"capslock",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            delete_key:{
                label:"delete",
                description:"NAME:\n" +
                    "Delete\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Delete key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   delete",
                code:"shift-delete",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            down:{
                label:"down",
                description:"NAME:\n" +
                    "Down\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Down Arrow key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   down",
                code:"shift-down",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            end:{
                label:"end",
                description:"NAME:\n" +
                    "End\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the End key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   end",
                code:"end",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            enter:{
                label:"enter",
                description:"NAME:\n" +
                    "Enter\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Enter key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   enter",
                code:"shift-enter",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            escape:{
                label:"escape",
                description:"NAME:\n" +
                    "Escape\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Escape key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   escape",
                code:"escape",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            left:{
                label:"left",
                description:"NAME:\n" +
                    "Left\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Left Arrow key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   left",
                code:"left",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            right:{
                label:"right",
                description:"NAME:\n" +
                    "Right\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Right Arrow key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   right",
                code:"right",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            space:{
                label:"space",
                description:"NAME:\n" +
                    "Space\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Spacebar key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   space",
                code:"ctrl-space",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            tab:{
                label:"tab",
                description:"NAME:\n" +
                    "Tab\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the tab key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   tab",
                code:"tab",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

            up:{
                label:"up",
                description:"NAME:\n" +
                    "Up\n" +

                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Up Arrow key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   up",
                code:"up",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.KEY_TYPE},

    //KEY CONSTANTS
        //MODIFIERS

            control:{
                label:"control",
                description:"NAME:\n" +
                    "Control\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Control (Ctrl) key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   control\n" +
                    "   control+keyname\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      control+a",
                code:"control+a",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.MODIFIER_TYPE},

            ctrl:{
                label:"ctrl",
                description:"NAME:\n" +
                    "Ctrl\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Ctrl (Control) key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   ctrl\n" +
                    "   ctrl+keyname\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      ctrl+a",
                code:"ctrl+a",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.MODIFIER_TYPE},

            shift:{
                label:"shift",
                description:"NAME:\n" +
                    "Shift\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Constant for the Shift key.\n" +
                        "\n" +
                    "FORMAT:\n" +
                    "   shift\n" +
                    "   shift+keyname\n" +
                        "\n" +
                    "EXAMPLE:\n" +
                    "   Expression:\n" +
                    "      shift+a",
                code:"shift+a",
                category:Ast.KEYPRESS_CATEGORY,
                type:Ast.MODIFIER_TYPE},

    //TIPS
        //GENERAL

            copyandpaste:{
                label:"Copy and Paste",
                description:"NAME:\n" +
                    "Copy and Paste Variables\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Double-click an item in the Variables &\n" +
                    "Functions Library to select that item's text.\n" +
                    "You can copy the variable, function, constant,\n" +
                    "or operator. You can then paste it into a text\n" +
                    "field, table or ribbon.",
                code:"",
                category:Ast.TIPS_CATEGORY,
                type:Ast.GENERAL_TYPE},

            expressions:{
                label:"Embed in expressions",
                description:"NAME:\n" +
                    "Embed in Expressions\n" +
                        "\n" +
                    "DESCRIPTION:\n" +
                    "Variables and functions can be used to create\n" +
                    "expressions in tables and calculations. Some\n" +
                    "types of ribbons can also accept expressions,\n" +
                    "such as:\n" +
                    "   Set property ribbons\n" +
                    "   Calculated ribbons\n" +
                    "   Conditional ribbons\n" +
                    "In ribbon fields, you do not need to use the ' { '\n" +
                    "and ' } ' characters to contain the item.",
                code:"",
                category:Ast.TIPS_CATEGORY,
                type:Ast.GENERAL_TYPE},

            textembed:{
                label:"Embed in text",
                description:"NAME:\n" +
                    "Embed in Text\n" +
                    "\n" +
                    "DESCRIPTION:\n" +
                    "Variables and functions can be embedded in\n" +
                    "text. You can embed an expression into a text\n" +
                    "object by typing the ' { ' character, then the\n" +
                    "expression, followed by the ' } ' to end the\n" +
                    "expression.\n" +
                    "For example, you can create a text block that\n" +
                    "says \"Today is October 23, 2012\" by typing in '\n" +
                    "Today is {fulldate} ' in the text object to embed\n" +
                    "the 'fulldate' variable.",
                code:"", category:Ast.TIPS_CATEGORY,
                type:Ast.GENERAL_TYPE}
        }
    }
}
}
