package com.alleni.author.definition
{
import com.alleni.author.controller.objects.ArenaLayoutManager;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.definition.application.TransitionTypes;
import com.alleni.author.definition.text.FormattedText;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.GraphicFill;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.NumberValueControlConstraint;
import com.alleni.author.service.project.PublishedProjectListOperation;
import com.alleni.author.util.CustomPoint;
import com.alleni.author.util.PresetPoint;
import com.alleni.savana.Ast;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.service.LogService;

import flash.display.BlendMode;
import flash.geom.Point;
import flash.utils.Dictionary;

import flashx.textLayout.container.ScrollPolicy;

public class Modifiers
	{
		public static var instance:Modifiers = new Modifiers();
		
		// property types
		public static const BOOLEAN_TYPE:String					=	"boolean";
		public static const STRING_TYPE:String					= 	"string";
		public static const FORMATTED_TEXT_TYPE:String 			=   "formattedText";
		public static const COLOR_TYPE:String					= 	"color";
		public static const HORIZONTAL_ALIGN_TYPE:String		= 	"horizontalAlign";
		public static const VERTICAL_ALIGN_TYPE:String			= 	"verticalAlign";
		public static const UNSIGNED_NUMBER_TYPE:String			= 	"unsignedNumber";
		public static const NUMBER_TYPE:String					=	"number";
		public static const INT_TYPE:String						= 	"int";
		public static const ENUMERATED_TYPE:String				=	"enumerated";
		public static const FORM_TEXT_FIELD:String				=	"formTextField";
		public static const FORM_TEXT_BOX:String				=	"formTextBox";
		public static const FONT_ENUMERATED_TYPE:String			=	"fontEnumerated";
		public static const FILL_TYPE:String					=	"fill";
		public static const POSITION_TYPE:String				=	"position";
		public static const POINT_TYPE:String					=	"point";
		public static const OBJECT_TYPE:String					=	"object";
		public static const IMAGE_ASSET_TYPE:String				=	"imageAsset";
		public static const AUDIO_ASSET_TYPE:String				=	"audioAsset";
		public static const VIDEO_ASSET_TYPE:String				=	"videoAsset";
		public static const TEXT_ASSET_TYPE:String				=	"textAsset";
		public static const FONT_ASSET_TYPE:String				=	"fontAsset";
		public static const GADGET_TYPE:String					=	"gadget";
		public static const STYLE_TYPE:String					=	"style";
		public static const CSS_SELECTOR_TYPE:String			=	"cssSelector";
		public static const LIST_TYPE:String					=	"list";
        public static const KEY_TYPE:String						=	"key";
        public static const EVENTPAGE_TYPE:String				=	"eventPage";
        public static const ARENAPAGE_TYPE:String				=	"arenaPage";

        public static function getDefaultValue(type:String):Object
		{
			switch(type) {
				case BOOLEAN_TYPE: return true;
				case STRING_TYPE: return "";
				case COLOR_TYPE: return 0xFFFFFF;
				case UNSIGNED_NUMBER_TYPE: return 0;
				case POSITION_TYPE: return new PositionAndAngle(0, 0, 0);
				case POINT_TYPE:	return new Point(0,0);
				case IMAGE_ASSET_TYPE:	return null;
				case AUDIO_ASSET_TYPE:	return null;
				case VIDEO_ASSET_TYPE:	return null;
				case TEXT_ASSET_TYPE:	return null;
				case FONT_ASSET_TYPE:	return null;
				case LIST_TYPE:         return [];
				default: return 0;
			}
		}

        public static function isNumericType(type:String):Boolean
        {
            return type == NUMBER_TYPE
            || type == INT_TYPE
            || type == UNSIGNED_NUMBER_TYPE;
        }

		
		// property heading categories: should be entered in weight function, below
		public static const GEOMETRY:String		=	"GEOMETRY";
		public static const VISIBILITY:String	=	"VISIBILITY";
		public static const APPEARANCE:String	=	"APPEARANCE";
		public static const BUTTONSTATES:String = 	"GRAPHIC BUTTON";
		public static const NOTES:String		=	"NOTES";
		public static const PARAGRAPH:String	=	"PARAGRAPH";
		public static const FONT:String			=	"FONT";
		public static const STYLE:String		=	"STYLE";
		public static const VALUE:String		=	"VALUES";
		public static const EXPERIENCE_API:String		=	"TINCAN";
		public static const SCORM:String		=	"SCORM";
		public static const ALIGN:String		=	"ALIGN";
		public static const PORT:String 		=	"PORT";
		public static const ACTIONS:String		=	"ACTIONS";
		public static const EFFECTS:String		=	"EFFECTS";
		public static const TWEENS:String		=	"TWEENS";
		public static const TABLES:String 		=	"TABLES";
		public static const CALCULATED:String 	=	"CALCULATED";
        public static const LAYOUT:String 	    =	"LAYOUT";
        public static const EVENTFLOW:String 	=	"EVENT FLOW";

		// inlet heading categories
		public static const DISPLAY:String 		=	"DISPLAY";
		public static const DO:String			=	"DO";
		public static const SETTERS:String		=	"SET PROPERTY";
		
		// outlet heading categories
		public static const GESTURES:String 	=	"POINT & CLICK";
		public static const STATES:String 		=	"TRIGGERS";
		public static const TRIGGERS:String		=	"CONDITIONAL";
		public static const COLLIDE:String		=	"COLLISION";
		public static const DRAG_N_DROP:String	=	"DRAG & DROP";
		
		private var _registry:Dictionary = new Dictionary(true);  /* of InletDescriptions, PropertyDescriptions, and OutletDescriptions */


		//  AS3 does not allow const declarations of type Array or Object. Also, apparently static var doesn't work either, so normal var it is.
		private var TWEEN_ENUMERATION_CONSTRAINTS:Array = [
					{label:"linear", data:"linear"},					{label:"easeInSine", data:"easeInSine"},
					{label:"easeOutSine", data:"easeOutSine"},			{label:"easeInOutSine", data:"easeInOutSine"},			{label:"easeInQuad", data:"easeInQuad"},
					{label:"easeOutQuad", data:"easeOutQuad"},			{label:"easeInOutQuad", data:"easeInOutQuad"},		{label:"easeInCubic", data:"easeInCubic"},
					{label:"easeOutCubic", data:"easeOutCubic"},		{label:"easeInOutCubic", data:"easeInOutCubic"},	{label:"easeInQuart", data:"easeInQuart"},
					{label:"easeOutQuart", data:"easeOutQuart"},		{label:"easeInOutQuart", data:"easeInOutQuart"},	{label:"easeInQuint", data:"easeInQuint"},
					{label:"easeOutQuint", data:"easeOutQuint"},			{label:"easeInOutQuint", data:"easeInOutQuint"},	{label:"easeInExpo", data:"easeInExpo"},
					{label:"easeOutExpo", data:"easeOutExpo"},			{label:"easeInOutExpo", data:"easeInOutExpo"},		{label:"easeInCirc", data:"easeInCirc"},
					{label:"easeOutCirc", data:"easeOutCirc"},			{label:"easeInOutCirc", data:"easeInOutCirc"},		{label:"easeInElastic", data:"easeInElastic"},
					{label:"easeOutElastic", data:"easeOutElastic"},	{label:"easeInOutElastic", data:"easeInOutElastic"},{label:"easeInBack", data:"easeInBack"},
					{label:"easeOutBack", data:"easeOutBack"},			{label:"easeInOutBack", data:"easeInOutBack"},		{label:"easeInBounce", data:"easeInBounce"},
					{label:"easeOutBounce", data:"easeOutBounce"},		{label:"easeInOutBounce", data:"easeInOutBounce"},	{label:"none", data:"none"}
				];
		private var SCROLL_POLICY_ENUMERATION_CONSTRAINTS:Array = [{label:"Off", data:ScrollPolicy.OFF}, {label:"On", data:ScrollPolicy.ON}, {label:"Auto", data:ScrollPolicy.AUTO}];

		public function Modifiers()
		{
		// Inlets - id prefix = 1
			
			// DISPLAY - id prefix = 1
			register(new InletDescription("show",			"1-1-00", 	"Show", 				"Show object", 													DISPLAY,		0		));
			register(new InletDescription("hide", 			"1-1-01",	"Hide", 				"Hide object", 													DISPLAY,		1		));
			register(new InletDescription("cloak", 			"1-1-01",	"Cloak", 				"Cloak object\n(hidden- responds to clicks & rollovers)", 													DISPLAY,		2		));
			
			register(new InletDescription("goCurrentPage",	"1-1-02",	"Go- current page(1)",	"Go to the currently showing page of the arena", DISPLAY, 	3		));

			register(new InletDescription("bringFront",		"1-1-07",	"Bring- to front",		"Bring to front", 										DISPLAY,	 7		));
			register(new InletDescription("bringForward",	"1-1-08", 	"Bring- forward", 		"Bring forward", 										DISPLAY,	 8		));
			register(new InletDescription("sendBackward",	"1-1-09", 	"Send- backward", 		"Send backward", 										DISPLAY,	 9		));
			register(new InletDescription("sendBack",		"1-1-10",	"Send- to back", 		"Send to back", 											DISPLAY,	 10	));

			
			
			// DO - id prefix = 2
			register(new InletDescription("start",			"1-2-08",	"Start", 				"Start", 									DO,		8   ));
			register(new InletDescription("pause",			"1-2-09",	"Pause", 				"Pause", 									DO,		9   ));
			register(new InletDescription("resume",			"1-2-10",	"Resume", 				"Resume", 									DO,		10  ));
			register(new InletDescription("restart",		"1-2-11",	"Restart", 				"Restart this app\n(reset all properties to initial values)", 									DO,		11  ));
			register(new InletDescription("stop", 			"1-2-12",	"Stop", 				"Stop", 									DO,		12  ));
			register(new InletDescription("togglePause", 	"1-2-24",	"Toggle- pause", 		"Toggle (pause | resume)", 							DO,		24   ));
			register(new InletDescription("resetTiming", 	"1-2-24",	"Reset", 				"Reset timing", 							DO,		25   ));
			
			register(new InletDescription("goStartPage",	"1-1-03", 	"Go- first page", 		"First page", 									DO,		26		));
			register(new InletDescription("goNextPage",		"1-1-04", 	"Go- next page", 		"Next page", 									DO,		27		));
			register(new InletDescription("goPrevPage",		"1-1-05",	"Go- prev page", 		"Previous page", 								DO,	28		));
            register(new InletDescription("goEndPage",		"1-1-06",	"Go- last page", 		"Last page", 								 DO,		29		));
            register(new InletDescription("goThisPage",		"1-1-06",	"Go- this page", 		"This page", 								 DO,		30		));

			register(new InletDescription("first", 			"1-4-00",	"first", "First", DO, 30));
			register(new InletDescription("last", 			"1-4-00",	"last", "Last", DO, 31));
			register(new InletDescription("next", 			"1-4-00",	"next", "Next", DO, 32));
			register(new InletDescription("previous", 		"1-4-00",	"previous", "Previous", DO, 33));
			register(new InletDescription("resetObject", 	"1-4-00",	"Reset properties", "Reset properties to initial values", DO, 34));

			// ToggleButton
			register(new InletDescription("enableButton", 	"1-2-17",	"Power- on", 		"Enable button", 			DO,		1  ));
			register(new InletDescription("disableButton", 	"1-2-17",	"Power- off", 		"Disable button", 			DO,		2  ));
			register(new InletDescription("press", 			"1-2-13",	"Press", 				"Press this button", 	DO,		13  ));
			register(new InletDescription("release", 		"1-2-14",	"Release", 				"Release button",					DO,		14  ));
			register(new InletDescription("over",	 		"1-2-14",	"Over", 				"Over",						DO,		15  ));
			register(new InletDescription("check", 			"1-2-16",	"Check", 				"Check this button", 					DO,		16  ));
			register(new InletDescription("uncheck", 		"1-2-17",	"Uncheck", 				"Uncheck this button", 					DO,		17  ));
			register(new InletDescription("toggle", 		"1-2-18",	"Toggle", 				"Toggle (on|off)", 			DO,		19  ));
			
			register(new InletDescription("nudgeUp", 		"1-2-19",	"Nudge- up", 			"Increase value by 1 nudge amount", 						DO,		19  ));
			register(new InletDescription("nudgeDown", 		"1-2-20",	"Nudge- down", 			"Decrease value by 1 nudge amount", 					DO,		20  ));
			
			register(new InletDescription("setRandom", 		"1-2-21",	"Set random value", 		"Set to random value", 			DO,		21  ));
			register(new InletDescription("invertNudge", 	"1-2-22",	"Invert- nudge v", 		"Invert nudge value", 					DO,		22  ));
			register(new InletDescription("invertAndNudge", "1-2-23",	"Invert- & nudge", 			"Invert nudge value, and then nudge", 				DO,		23  ));
			
			register(new InletDescription("reEval", 		"1-2-25",	"Update {exp}", 		"Updates all {expression or value}s embedded in text", 			DO,		25   ));

			register(new InletDescription("record", 		"1-2-12",	"Record", 				"", 									DO,		12  ));//***
			
			register(new InletDescription("randomizeZIndex", "",		"Randomize order", 	"Randomize stack order of objects \n(see \"Geometry: Stack order\")", 			DO,		33   ));
			register(new InletDescription("putBack", 		"1-2-26",	"Put back", 			"Put object back to its initial position", 			DO,		34   ));
			register(new InletDescription("snapToValue", 	"1-2-26",	"Path- snap value", 	"Snap object to current value of path", 	DO,		35   ));
			register(new InletDescription("snapToNearest", 	"1-2-26",	"Path- snap near", 		"Snap object to nearest point of path", 	DO,		36   ));
			
			
			// PORT
			// Calc  - id prefix = 3
			register(new InletDescription("calcInlet", 		"1-3-00",	"Calc inlet", 			"Inlet for calculation", PORT));
			
			// Table Inlets = id prefix = 4
			register(new InletDescription("judgeNow", 		"1-4-00",	"judgeNow", "Judge now", PORT));
			register(new InletDescription("reset", 			"1-4-00",	"reset", "Reset table", PORT));
			register(new InletDescription("powerOff",			"1-4-00",	"powerOff", "Power- off", PORT));
			register(new InletDescription("powerOn",			"1-4-00",	"powerOn", "Power- on", PORT));
			
			
			
			
		// Properties - id prefix = 3
			
		// VISIBILITY - id prefix = 1
			register(new PropertyDescription("visible",			"3-1-00",	"Visible", 			"Object is visible", 								VISIBILITY, 0, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("alpha", 			"3-7-17",	"Opacity", 		"Opacity percent", 				VISIBILITY, 1, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("disabledAlpha", 	"3-2-00",	"Opacity- pwr off", 	'Opacity when "Actions: Power" = F', 	VISIBILITY, 2, false, -1, UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
            register(new PropertyDescription("hideOnRun", 		"3-1-02",	"Hide on run", 	"Hide on run\n(hidden- ignores clicks & rollover)", 			VISIBILITY, 3, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("cloakedNow", 		"3-1-01",	"Cloaked", 			"Object is currently cloaked", 			VISIBILITY, 4, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("cloakOnRun", 		"3-1-01",	"Cloak on run", 	"Cloak on run\n(hidden- responds to clicks & rollover)", 			VISIBILITY, 5, false, -1, BOOLEAN_TYPE));

			register(new PropertyDescription("cloakFrame", 		"3-1-03",	"Cloak- bkg on run", 	"Cloak arena background (fill & line) on run", 	VISIBILITY, 6, false, -1, BOOLEAN_TYPE));

			register(new PropertyDescription("controlsVisible",	"3-1-04",	"Control- visible",	"Control- visible", 				VISIBILITY, 7, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("controlsCloaked",	"3-1-05",	"Control- hide..", 	"Control- hide on run\n(hidden- ignores clicks & rollover)",	VISIBILITY, 8, false, -1, BOOLEAN_TYPE));

		

		// VALUES - id prefix = 2
			register(new PropertyDescription("text", 			"3-2-00",	"Text- input", 			"Unformatted text entered by user \n(no style properties)",    VALUE, 0, false, -1, STRING_TYPE));
			register(new PropertyDescription("source", 			"3-2-40",	"Text- formatted", 	"Formatted text \n(with style properties: size, weight, italic...)", 					VALUE, 1, false, -1, FORMATTED_TEXT_TYPE));
			register(new PropertyDescription("plainText", 		"3-2-41",	"Text- unformatted", 	"Unformatted text \n(no style properties)", 				VALUE, 2, false, -1, STRING_TYPE,null,false));
			
			// InputText
            register(new PropertyDescription("promptText", 			"3-2-00",	"Text- prompt", "Initial text to prompt user for input", 	VALUE, 2, false, -1, STRING_TYPE));
			register(new PropertyDescription("numbersOnly", 	"3-2-27",	"Numbers only",		"Only numbers allowed", 									VALUE, 3, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("maxCharCount", 	"3-2-28",	"Max char count", 	"Maximum character count",							VALUE, 4, false, -1, UNSIGNED_NUMBER_TYPE));
			// Checkbox/Radio Button
			register(new PropertyDescription("checked", 		"", "Checked",					"Button is checked",								VALUE,	5, false, -1,	BOOLEAN_TYPE));
			register(new PropertyDescription("down", 			"3-2-18",	"Down", 			"Button is down", 								VALUE, 6, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("toggleButtonValue", 				"", "Value- this btn", 				"Current value of this button", VALUE, 7, false, -1, STRING_TYPE));
			register(new PropertyDescription("toggleButtonGroupValue", 			"", "Value- btn set", 			"Current value of associated button set\n(if this button belongs to a button set)", VALUE, 8, false, -1, STRING_TYPE));
            register(new PropertyDescription("toggleButtonGroupNullValue", 			"", "Value- none", 			"Value returned if none selected in set\n(if this button belongs to a button set)", VALUE, 9, false, -1, STRING_TYPE));
            register(new PropertyDescription("label", 			"3-2-00",	"Label- unchecked", 			"Button label when unchecked", 						VALUE, 10, false, -1, STRING_TYPE));
			register(new PropertyDescription("labelDown", 		"3-2-00",	"Label- checked", 		"Button label when checked", 						VALUE, 11, false, -1, STRING_TYPE));
			// Arena
			register(new PropertyDescription("pageCount", 		"3-2-06",	"Page- total", 		"Total pages in arena", 						VALUE, 	12, false, -1, 	UNSIGNED_NUMBER_TYPE, null, false));
			register(new PropertyDescription("pageNumber", 		"3-2-05",	"Page- current #", 	"Current page number", 					VALUE, 	13, false, -1, 	UNSIGNED_NUMBER_TYPE));
            register(new PropertyDescription("objCount", 		"3-2-12",	"Obj- total", 		"Total objects in arena", 			VALUE, 	14, false, -1, 	UNSIGNED_NUMBER_TYPE, null, false));
            register(new PropertyDescription("notLastPage", 		"3-2-12",	"Page- next?", 		"Is there a next page? \n(common use: wire to a Next button to activate)", 	VALUE, 	15, false, -1, 	BOOLEAN_TYPE, null, false));
            register(new PropertyDescription("notFirstPage", 		"3-2-12",	"Page- prev?", 		"Is there a previous page? \n(common use: wire to a Previous or Back button to activate)", 	VALUE, 	16, false, -1, 	BOOLEAN_TYPE, null, false));
            // Audio
			register(new PropertyDescription("volume", 			"3-2-15",	"Volume", 			"Volume (1-100)",										VALUE,	16, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("uri",				"3-2-01",	"URL", "URL (web address)", VALUE, 17, false, 0, STRING_TYPE));
			// Clock
			register(new PropertyDescription("duration", 		"3-2-31",	"Secs- per cycle", 	"Seconds per cycle (revolution)",					VALUE, 18, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("secondsElapsed", 	"3-2-32",	"Secs- elapsed", 	"Seconds (time) elapsed", 					VALUE,	19, false, -1,	UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("secondsRemaining","3-2-33",	"Secs- remaining",	"Seconds (time) remaining", 					VALUE, 20, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("percentElapsed",	"3-2-34",	"%- elapsed", 		"Seconds percent elapsed",		VALUE, 21, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("percentRemaining","3-2-35",	"%- remaining", 	"Seconds percent remaining",	VALUE, 22, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("cycles", 			"3-2-37",	"Cycles- total", 			"Total clock cycles (revolutions)", 						VALUE, 23, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("cycleNumber", 	"3-2-38",	"Cycle- current", 	"Current cycle (revolution)", 																	VALUE, 24, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("ticksPerCycle", 	"3-2-36",	"Ticks per cycle", 	"How many times per cycle to fire\nthe \"Triggers: Tick\" OUT ribbon", 									VALUE, 25, false, -1, UNSIGNED_NUMBER_TYPE));
			// Slider
			register(new PropertyDescription("sliderPercent",	"3-2-22",	"Slider- %", 	"Slider- current value as percent", 				VALUE,26, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("sliderValue",		"3-2-21",	"Slider- value", 	"Slider- current value", 						VALUE, 27, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("sliderMaximum",	"3-2-23",	"Slider- end value", 	"Slider- end value",					VALUE, 28, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("sliderMinimum",	"3-2-24",	"Slider- start value", 	"Slider- start value", 				VALUE, 29, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("sliderUnit",		"3-2-25",	"Unit", 		"Unit used to move slider (division of scale)", 					VALUE, 30, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("sliderNudgeAmount","3-2-26",	"Nudge amount",	"Nudge slider amount\n(value used to nudge value up or down)", 					VALUE, 31, false, -1, UNSIGNED_NUMBER_TYPE));
			// Map
			register(new PropertyDescription("mapType", 		"3-2-53",	"Map- type", 	"Map type",		VALUE, 	32, false, -1, ENUMERATED_TYPE,
				[{label:"Normal", data:0},{label:"Satellite", data:1},{label:"Hybrid", data:2},{label:"Physical", data:3}]));
			register(new PropertyDescription("latitude", 		"3-2-50",	"Map- latitude", 	"Map latitude", 	VALUE, 33, false, -1, 	NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, -85, 85)));
			register(new PropertyDescription("longitude", 		"3-2-51",	"Map- longitude", 	"Map longitude", 	VALUE, 34, false, -1, 	NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, -180, 180)));
			register(new PropertyDescription("zoom", 			"3-2-52",	"Map- zoom level", 	"Map zoom level (0-17)", VALUE, 35, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 17)));
			// Collaboration
			register(new PropertyDescription("key","","Key", "Key (channel or activity name)", VALUE, 36, false, -1, STRING_TYPE));
			register(new PropertyDescription("sendData","","Send data", "Data to be sent from this client", VALUE, 37, false, -1, STRING_TYPE));
			register(new PropertyDescription("receiveData","","Receive data", "Data that has been received by this client", VALUE, 38, false, -1, STRING_TYPE, null, false));
			register(new PropertyDescription("local","","Local", "Data that sent and received is local to this application and does not utilize the network", VALUE, 39, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("subscribed","","Subscribed", "This connection is subscribed and will continuously receive data", VALUE, 40, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("descending","","Descending", "Results are sorted in reverse", VALUE, 41, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("resultCount","","Result count", "Number of results to retrieve", VALUE, 42, false, -1, NUMBER_TYPE));

			
		// ACTIONS - id prefix = 3
			register(new PropertyDescription("resetOnEnter", 		"3-2-17",	"Reset- on enter",		"Reset properties on enter",			ACTIONS, 1, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("momentary", 		"3-2-17",	"Momentary",		"Momentary (pops back after press)",					ACTIONS, 1, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("submit", 			"3-2-29",	"Submit text", 			"When text is sent", 								ACTIONS, 2, false, -1, ENUMERATED_TYPE,
				[{label:"Enter key pressed", data:"onEnter"}, {label:"Tab key pressed", data:"onTab"}, {label:"Continuously", data:"continuously"}]));
			register(new PropertyDescription("autorun", 		"3-3-00",	"Auto start", 		"Auto start on run",	ACTIONS, 3, false, -1, BOOLEAN_TYPE));
			// Arena
			register(new PropertyDescription("pageRate", 		"3-2-09",	"Play- p/sec",	"Play speed- pages per second", 				ACTIONS, 	4, false, -1, 	UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("actualRate", 		"3-2-11",	"Play- actual","Actual play speed- pages per second delivered",	ACTIONS, 	5, false, -1, 	UNSIGNED_NUMBER_TYPE,null,false));
			register(new PropertyDescription("noSkip", 			"3-2-10",	"Play- no skip", 	"Play without skipping pages", 			ACTIONS, 	6, false, -1, 	BOOLEAN_TYPE));
            register(new PropertyDescription("transition", 		"3-2-07",	"Trans- type", 		"Transition type- style of change", 					ACTIONS, 	7, false, -1, 	ENUMERATED_TYPE,
                [{label:"NONE", data:"none"},{label:"F-IN   fade- in", data:TransitionTypes.FADE_IN}, {label:"F-OUT   fade- out", data:TransitionTypes.FADE_OUT}, {label:"FADE   cross-fade", data:TransitionTypes.FADE_CROSS},
                    {label:"W-LR   wipe- left to right", data:TransitionTypes.WIPE_LEFT_RIGHT}, {label:"W-RL   wipe- right to left", data:TransitionTypes.WIPE_RIGHT_LEFT}, {label:"W-TB   wipe- top to bottom", data:TransitionTypes.WIPE_TOP_BOTTOM}, {label:"W-BT   wipe- bottom to top", data:TransitionTypes.WIPE_BOTTOM_TOP},
                    {label:"S-LR   slide- left to right", data:TransitionTypes.SLIDE_LEFT_RIGHT}, {label:"S-RL   slide- right to left", data:TransitionTypes.SLIDE_RIGHT_LEFT}, {label:"S-TB   slide- top to bottom", data:TransitionTypes.SLIDE_TOP_BOTTOM}, {label:"S-BT   slide- bottom to top", data:TransitionTypes.SLIDE_BOTTOM_TOP},
                    {label:"DO-V   door open- vertical", data:TransitionTypes.BARN_DOOR_OPEN_VERTICAL}, {label:"DC-V   door close- vertical", data:TransitionTypes.BARN_DOOR_CLOSE_VERTICAL},
                    {label:"DO-H   door open- horizontal", data:TransitionTypes.BARN_DOOR_OPEN_HORIZONTAL}, {label:"DC-H   door close- horizontal", data:TransitionTypes.BARN_DOOR_CLOSE_HORIZONTAL},
                    {label:"IRI-C   iris in- circle", data:TransitionTypes.IRIS_IN_CIRCLE}, {label:"IRO-C   iris out- circle", data:TransitionTypes.IRIS_OUT_CIRCLE},
                    {label:"IRI-R   iris in- rectangle", data:TransitionTypes.IRIS_IN_SQUARE}, {label:"IRO-R   iris out- rectangle", data:TransitionTypes.IRIS_OUT_SQUARE},
                    {label:"ZM-I   zoom in", data:TransitionTypes.ZOOM_IN}, {label:"ZM-O   zoom out", data:TransitionTypes.ZOOM_OUT},
                    {label:"BLI-H   blinds- horizontal", data:TransitionTypes.VENETIAN_BLINDS}, {label:"BLI-V   blinds- vertical", data:TransitionTypes.VERTICAL_BLINDS}
				/*	{label:"Ken Burns", data:"kenBurns"} */ ]));
            register(new PropertyDescription("projectTransition",     "3-2-07",	"Trans- type", 		"Transition type- style of change", 					ACTIONS, 	7, false, -1, 	ENUMERATED_TYPE,
                [{label:"NONE", data:"none"},{label:"FADE   cross-fade", data:TransitionTypes.FADE_CROSS},{label:"S-LR   slide- left to right", data:TransitionTypes.SLIDE_LEFT_RIGHT},
                    {label:"S-RL   slide- right to left", data:TransitionTypes.SLIDE_RIGHT_LEFT}, {label:"S-TB   slide- top to bottom", data:TransitionTypes.SLIDE_TOP_BOTTOM}, {label:"S-BT   slide- bottom to top", data:TransitionTypes.SLIDE_BOTTOM_TOP}
				]));
            register(new PropertyDescription("eventTransition",     "3-2-07",	"Trans- type", 		"Transition type- style of change", 					ACTIONS, 	7, false, -1, 	ENUMERATED_TYPE,
                [{label:"PROJ   Use project transition", data:TransitionTypes.DEFAULT},{label:"NONE", data:"none"},{label:"FADE   cross-fade", data:TransitionTypes.FADE_CROSS},{label:"S-LR   slide- left to right", data:TransitionTypes.SLIDE_LEFT_RIGHT},
                    {label:"S-RL   slide- right to left", data:TransitionTypes.SLIDE_RIGHT_LEFT}, {label:"S-TB   slide- top to bottom", data:TransitionTypes.SLIDE_TOP_BOTTOM}, {label:"S-BT   slide- bottom to top", data:TransitionTypes.SLIDE_BOTTOM_TOP}
				]));
			register(new PropertyDescription("transTweenType","","Trans- tween", "Transition tween- tempo or flow of change", ACTIONS, 8, false, -1, ENUMERATED_TYPE, TWEEN_ENUMERATION_CONSTRAINTS));
            register(new PropertyDescription("transitionSecs", 	"3-2-08",	"Trans- secs", 		"Transition time- length in seconds", 	ACTIONS, 	9, false, -1, 	UNSIGNED_NUMBER_TYPE));
            register(new PropertyDescription("moveWithTransition", 	"3-2-08",	"Trans- w/event", "Include this master with event transitions?\n(affects this master when it appears in sequential events)", 	ACTIONS, 	9, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("accepting", 		"3-2-13",	"Obj- accept", 		"Arena will accept objects", 	ACTIONS, 	10, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("releasing", 		"3-2-14",	"Obj- release", 	"Arena will release objects", 		ACTIONS, 	11, false, -1, 	BOOLEAN_TYPE));
            register(new PropertyDescription("acceptIntoOutOfArena",	"",			"Arena- in/out",	"Object can enter or exit an arena",	ACTIONS, 12, false, -1, BOOLEAN_TYPE));
            register(new PropertyDescription("preload", 		"3-3-10",	"Preload", 			"Preload media", 									ACTIONS, 13, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("loop", 			"3-3-10",	"Loop", 			"Auto restart on completion", 					ACTIONS, 14, false, -1, BOOLEAN_TYPE));
            register(new PropertyDescription("interactive", 	"3-3-01.5",	"Power", 	"Object is interactive.", 	ACTIONS, 	15, false, -1, BOOLEAN_TYPE));
            register(new PropertyDescription("enabled", 		"3-3-01",	"Outs- on", 	"Are OUT ribbons active?", 	ACTIONS, 	16, false, -1, BOOLEAN_TYPE));
            register(new PropertyDescription("movable", 		"3-3-02",	"Movable", 			"Movable by user during run",		ACTIONS, 	17, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("inertial", 		"3-3-03",	"Inertia",			"Retains momentum following drag or move", 		ACTIONS, 18, false, -1, BOOLEAN_TYPE)); //***
			register(new PropertyDescription("runtimeRotation", "3-3-04",	"Rotatable", "Rotation allowed by user during run", 		ACTIONS, 19, false, -1, BOOLEAN_TYPE)); //***
			register(new PropertyDescription("collidingWith",	"3-3-05",	"Colliding with",	"Target(s) colliding with this object\n(set collision target(s) in OUT ribbons)", 	ACTIONS, 	20, false, -1, LIST_TYPE, null, false));
			register(new PropertyDescription("droppedOn",	"3-3-05",	"Dropped on",	"Target(s) on which this object is dropped\n(set drop target(s) in OUT ribbons)", 	ACTIONS, 	21, false, -1, LIST_TYPE, null, false));
			register(new PropertyDescription("putBackSecs",		"3-3-08",	"Put back- secs", 	"Seconds to put back (go to previous location)", 	ACTIONS, 	22, false, -1, UNSIGNED_NUMBER_TYPE));
			
			register(new PropertyDescription("snapTo", "3-5-21", "Path- name", "Snap object to targeted path name", ACTIONS, 22, false, -1, OBJECT_TYPE));
			register(new PropertyDescription("pathOn", 			"3-12-06",	"Path- snapped", 	"Object is snapped to path", 	ACTIONS, 23, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("snap", "3-5-22", "Path- jump gaps", "Allow object to jump gaps along path", ACTIONS, 24, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("pathPercent",		"3-2-22",	"Path- %", 			"Current path value as percent", 				ACTIONS, 25, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("pathValue",		"3-2-21",	"Path- value", 	"Current path value", 							ACTIONS, 26, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("pathMaximum",		"3-2-23",	"Path- end value", 		"Path- end value",							ACTIONS, 27, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("pathMinimum",		"3-2-24",	"Path- start value", 		"Path- start value", 							ACTIONS, 28, false, -1, NUMBER_TYPE));

            register(new PropertyDescription("currentlyEditing",		"3-2-24",	"Editing", 		"This Event or Master is being edited", 			ACTIONS, 29, false, -1, BOOLEAN_TYPE, null, false));

			// FONT - id prefix = 4
			register(new PropertyDescription("fontFamily", 		"3-4-00",	"Font- name", 		"", FONT, 0, false, -1, FONT_ENUMERATED_TYPE, ApplicationController.getSystemFonts()));
			register(new PropertyDescription("fontSize", 		"3-4-01",	"Font- size", 		"", FONT, 1, false, -1, UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 5, 72, null, 150)));
			register(new PropertyDescription("fontSpacing", 	"3-4-02",	"Font- ltrspace", 	"Font- letterspace", FONT, 2, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("bold", 			"3-4-03",	"Font- bold", 		"", FONT, 3, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("italic", 			"3-4-04",	"Font- italic", 	"", FONT, 4, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("underline", 		"3-4-05",	"Font- underline",	"", FONT, 5, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("fontColor", 		"3-4-06",	"Font- color", 		"",	FONT, 6, false, -1, COLOR_TYPE));
			register(new PropertyDescription("fontAlpha", 		"3-4-07",	"Font- opacity %", 	"Font- opacity percent", FONT, 7, false, -1, UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));

			// GEOMETRY - id prefix = 5
			register(new PropertyDescription("anchorPoint", 	"3-5-08",	"Anchor Point", 	"Anchor point for zoom or rotate",	GEOMETRY, 	0, false, -1, POINT_TYPE,
				[{label:"TL- top left", data:new PresetPoint(0,0)},
					{label:"TC- top center", data:new PresetPoint(0.5,0)},
					{label:"TR- top right", data:new PresetPoint(1,0)},
					{label:"CL- center left", data:new PresetPoint(0,0.5)},
					{label:"CC- center center", data:new PresetPoint(0.5,0.5)},
					{label:"CR- center right", data:new PresetPoint(1,0.5)},
					{label:"BL- bottom left", data:new PresetPoint(0,1)},
					{label:"BC- bottom center", data:new PresetPoint(0.5,1)},
					{label:"BR- bottom right", data:new PresetPoint(1,1)},
					{label:"XY- custom position",data:new CustomPoint(0.5,0.5),isCustomAnchorPosition:true}]));

			register(new PropertyDescription("x", 		"3-5-00",	"Location- X (H)",	"X (horz) location of object anchor point\n(from left edge of window or arena)",	GEOMETRY, 1, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("y", 		"3-5-01",	"Location- Y (V)",	"Y (vert) location of object anchor point\n(from top edge of window or arena)",				GEOMETRY, 2, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("locationXYR", 	"3-5-01-1",	"Location- XYR",	"Combined XY location and rotation degrees",		GEOMETRY, 3, false, -1, POSITION_TYPE));
			register(new PropertyDescription("layer",			"",			"Layer",			"Layer\n(each layer has an independent \"Stack order\"",								GEOMETRY, 4, false, -1, INT_TYPE));
			register(new PropertyDescription("zIndex",			"",	"Stack order", 		"Stack order- the order (0, 1, 2...) \nin which objects are arranged back (0) to front\n(affected by the order of objects in arena \"Layout\")",								GEOMETRY, 5, false, -1, INT_TYPE,null,false));
			register(new PropertyDescription("rotation", 		"3-5-10",	"Rotate degrees", 	"Rotation degrees (positive clockwise)",		GEOMETRY, 	6, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("scale", 			"3-5-06",	"Zoom- %", 	"Object zoom (scale) percent",GEOMETRY, 	7, false, -1, NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 200, null, 10000)));

			register(new PropertyDescription("width", 			"3-5-04",	"Size- width", 		"Object width", 			GEOMETRY, 	8, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("height", 			"3-5-05",	"Size- height", 	"Object height", 			GEOMETRY, 	9, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("cornerRadius", 	"3-5-07",	"Corner- radius", 	"Corner- radius", 						GEOMETRY, 	10, false, -1, UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
            register(new PropertyDescription("maintainAspect", 	"3-5-06",	"Keep aspect ratio", 	"Keep current aspect ratio when resizing", 			GEOMETRY, 	11, false, -1, 	BOOLEAN_TYPE));

			register(new PropertyDescription("knobX",			"3-5-02",	"Knob loc- X (H)", 	"Knob X (horz) location\n(from left edge of window or arena)", 	GEOMETRY,	12, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("knobY",			"3-5-03",	"Knob loc- Y (V)", 	"Knob Y (vert) location\n(from top edge of window or arena)", 	GEOMETRY, 	13, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("contentWidth",	"",			"Content- width", 	"Total width of arena content",	GEOMETRY, 14, false, -1, NUMBER_TYPE,null,false));
			register(new PropertyDescription("contentHeight",	"",			"Content- height", 	"Total height of arena content",GEOMETRY, 15, false, -1, NUMBER_TYPE,null,false));
			register(new PropertyDescription("clippedWidth",	"",			"Clipped- width", 	"Total width of clipped arena content",	GEOMETRY, 16, false, -1, NUMBER_TYPE,null,false));
			register(new PropertyDescription("clippedHeight",	"",			"Clipped- height", 	"Total height of clipped arena content",GEOMETRY, 17, false, -1, NUMBER_TYPE,null,false));

			// LAYOUT
			register(new PropertyDescription("autoSize",    	"3-7-03",	"Auto size", 	"Auto size as text changes\n(set to F to allow scroll bar in \"Layout: Scroll\")", 					LAYOUT, 0, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("margin",			"3-5-20",	"Margin- all",	"Margin- all sides",			LAYOUT,	1, false, -1,	UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("marginAll", 		"3-5-11",	"Margin- all", 	"Margin- all sides",	        LAYOUT, 2, false, -1, 	UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("marginLeft", 		"3-5-12",	"Margin- L", 		"Margin- left", 		LAYOUT, 	3, false, -1,	UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("marginRight", 	"3-5-15",	"Margin- R", 		"Margin- right", 		LAYOUT, 	4, false, -1, 	UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("marginTop", 		"3-5-13",	"Margin- T", 		"Margin- top", 			LAYOUT, 	5, false, -1,	UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("marginBottom", 	"3-5-14",	"Margin- B", 		"Margin- bottom", 		LAYOUT, 	6, false, -1, 	UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("layout", 							"",	"Layout", 				"Arena layout\n(the order of objects in Grid and Box\nreflects their \"Geometry: Stack order\")", 	LAYOUT, 			7, false, -1, 	ENUMERATED_TYPE,
				[
					{label:"Free", data:ArenaLayoutManager.NONE},
					{label:"Grid", data:ArenaLayoutManager.GRID},
					{label:"H-Box", data:ArenaLayoutManager.HBOX},
					{label:"V-Box", data:ArenaLayoutManager.VBOX}
				]));
			register(new PropertyDescription("columns",			"",			"Columns", 			"Number of columns",				LAYOUT, 8, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("columnCount",     "3-7-01",	"Columns", 	        "Number of Columns", 				LAYOUT, 8, false, -1, NUMBER_TYPE,[1,50]));
			register(new PropertyDescription("hGap",			"",			"Columns- pad", 	"Space between columns",			LAYOUT, 9, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("columnGap",       "3-7-02",	"Columns- pad", 	"Space between columns", 			LAYOUT, 9, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("rows",			"",			"Rows", 			"Number of rows in a Grid \"Layout\" \n(automatically created as objects are added)",					LAYOUT, 9, false, -1, NUMBER_TYPE,null,false));
			register(new PropertyDescription("vGap",			"",			"Rows- pad", 		"Space between rows",				LAYOUT, 10, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("layoutTweenType","","Change- tween", "Change of layout or object position-\ntempo or rhythm of change", LAYOUT, 11, false, -1, ENUMERATED_TYPE, TWEEN_ENUMERATION_CONSTRAINTS));
			register(new PropertyDescription("layoutTweenTime","","Change- secs", "Change of layout or object position-\nlength in seconds", LAYOUT, 12, false, -1, NUMBER_TYPE));

			register(new PropertyDescription("vScrollPolicy",    "3-7-04",	"Scroll- V", "Use scroll bar- vertical\n(set \"Layout: Auto size\" to F to allow scroll bar)", 				LAYOUT, 13, false, -1, ENUMERATED_TYPE, SCROLL_POLICY_ENUMERATION_CONSTRAINTS));
			register(new PropertyDescription("vertScrollPolicy",    "3-7-04",	"Scroll- V", "Use scroll bar- vertical", 			LAYOUT, 14, false, -1, ENUMERATED_TYPE, SCROLL_POLICY_ENUMERATION_CONSTRAINTS));
			register(new PropertyDescription("hScrollPolicy",    "3-7-04",	"Scroll- H", "Use scroll bar- horizontal", 			LAYOUT, 15, false, -1, ENUMERATED_TYPE, SCROLL_POLICY_ENUMERATION_CONSTRAINTS));
			register(new PropertyDescription("horzScrollPolicy",    "3-7-04",	"Scroll- H", "Use scroll bar- horizontal", 		LAYOUT, 16, false, -1, ENUMERATED_TYPE, SCROLL_POLICY_ENUMERATION_CONSTRAINTS));


			register(new PropertyDescription("verticalScrollPosition",     "3-7-04",	"Scroll pos- V %", 	"Vertical scroll position as percent", 	LAYOUT, 17, false, -1, UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("vertScrollPosition",     "",	"Scroll pos- V %", 	"Vertical scroll position as percent", 				LAYOUT, 18, false, -1, UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("horzScrollPosition",     "",	"Scroll pos- H %", 	"Horizontal scroll position as percent", 			LAYOUT, 19, false, -1, UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("textLength",     "3-7-05",	"Scroll- txt height", 	"Text height in pixels\n(use to create custom scrolling)", 				LAYOUT, 20, false, -1, NUMBER_TYPE,null, false));
	
			register(new PropertyDescription("labelPosition", 					"",	"Label- position", 		"Location of label\nin relationship to button",	LAYOUT, 	21, false, -1, POINT_TYPE,
				[
					{label:"TC- top center", data:new Point(0.5,0)},
					{label:"CL- center left", data:new Point(0,0.5)},
					{label:"CR- center right", data:new Point(1,0.5)},
					{label:"BC- bottom center", data:new Point(0.5,1)}
				]));
			register(new PropertyDescription("labelPadding", 					"",	"Label- gap", 		"Space between label and button",	LAYOUT, 	22, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("textWidth",		"",			"Label- width",		"Width of button label",				LAYOUT, 23, false, -1, NUMBER_TYPE));

			
			// ALIGN - id prefix = 6
			register(new PropertyDescription("textAlignH", 		"3-6-00",	"Align text- H", 	"Align text- horizontal", 									ALIGN, 0, false, -1, HORIZONTAL_ALIGN_TYPE));
			register(new PropertyDescription("textAlignV", 		"3-6-01",	"Align text- V", 	"Align text- vertical", 									ALIGN, 1, false, -1, VERTICAL_ALIGN_TYPE));
			register(new PropertyDescription("alignObjsHorz", 	"3-6-04",	"Align objs- H", 	"Align objects inside an arena- horizontal",	ALIGN, 2, false, -1, HORIZONTAL_ALIGN_TYPE));
			register(new PropertyDescription("alignObjsVert", 	"3-6-05",	"Align objs- V", 	"Align objects inside an arena- vertical", 		ALIGN, 3, false, -1, VERTICAL_ALIGN_TYPE));
			register(new PropertyDescription("alignCellsHorz", 	"",		"Align cells- H", 	"Align objects within layout cells- horizontal\n(set \"Layout\" to Grid or Box to see effect)",	ALIGN, 4, false, -1, HORIZONTAL_ALIGN_TYPE));
			register(new PropertyDescription("alignCellsVert", 	"",		"Align cells- V", 	"Align objects within layout cells- vertical\n(set \"Layout\" to Grid or Box to see effect)", 		ALIGN, 5, false, -1, VERTICAL_ALIGN_TYPE));
			register(new PropertyDescription("alignAreaHorz", 	"3-6-02",	"Align H to arena", 		"Align this object horizontally to arena margins\n(must be inside an arena to see effect)", 			ALIGN, 6, false, -1, HORIZONTAL_ALIGN_TYPE));
			register(new PropertyDescription("alignAreaVert", 	"3-6-03",	"Align V to arena", 		"Align this object vertically to arena margins\n(must be inside an arena to see effect)", 			ALIGN, 7, false, -1, VERTICAL_ALIGN_TYPE));



			// APPEARANCE - id prefix = 7
			register(new PropertyDescription("pathSpacing",		 	"3-7-11","Point spacing", 	"Space between control points (10-200)", 			APPEARANCE, 0, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 10, 200)));
			register(new PropertyDescription("fillSpec", 		"3-7-11",	"Fill- color", 	"Fill- solid color, gradient or jellybean", 			APPEARANCE, 1, false, -1, 	FILL_TYPE));
			register(new PropertyDescription("downColor",	 	"3-7-12",	"Fill ✓- color", 'Fill when checked-\nsolid color, gradient or jellybean', 					APPEARANCE, 2, false, -1, 	FILL_TYPE));
			register(new PropertyDescription("fillAlpha", 		"3-7-13",	"Fill- opacity %",	"Fill- opacity percent", 		APPEARANCE, 3, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("replaceable",	 	"3-7-11",	"Fill- graphic", 	"Fill- graphic image", 			APPEARANCE, 4, false, -1, 	IMAGE_ASSET_TYPE));
			register(new PropertyDescription("bitmapFill",	 	"3-7-11",	"Fill- graphic", 	"Fill- graphic image", 			APPEARANCE, 4, false, -1, 	IMAGE_ASSET_TYPE));
			register(new PropertyDescription("resizeImage", 	"3-2-07",	"Replace fit", 		"How to fit a graphic that replaces this image",		APPEARANCE, 	5, false, -1, 	ENUMERATED_TYPE,
					[{label:"Do not resize", data:"native"},{label:"Enlarge to current graphic size", data:"fill"}, {label:"Shrink to current graphic size", data:"shrink"}, {label:"Shrink or enlarge to current graphic size", data:"enlarge"},] ));
			register(new PropertyDescription("lineColor", 		"3-7-14",	"Line- color", 		"Line- color", 		            APPEARANCE, 6, false, -1, 	COLOR_TYPE,		null));
			register(new PropertyDescription("lineThickness", 	"3-7-15",	"Line- weight", 	"Line- weight", 	            APPEARANCE, 7, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 30)));
			register(new PropertyDescription("lineAlpha", 		"3-7-16",	"Line- opacity %", 	"Line- opacity percent", 		APPEARANCE, 8, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("checkMark", 						"",	"Use ✓ mark", 		"Use ✓, not X, as mark", 			APPEARANCE, 9, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("checkColor", 						"",	"✓ mark- color", 		"✓ mark- line color", 			APPEARANCE, 10, false, -1, 	COLOR_TYPE));
			register(new PropertyDescription("checkWeight", 					"",	"✓ mark- weight", 		"✓ mark- line weight", 			APPEARANCE, 11, false, -1, 	UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("checkOpacity", 					"",	"✓ mark- opac %", 	"✓ mark- opacity percent", 		APPEARANCE, 12, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("knobFill", 		"3-2-26-1",	"Knob fill- color", 	"Knob fill- solid color, gradient or jellybean", 					APPEARANCE, 13, false, -1, 	FILL_TYPE,null));
			register(new PropertyDescription("knobAlpha",		"3-7-07",	"Knob fill- opac %", 	"Knob fill- opacity percent", 	APPEARANCE, 14, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("sliderKnobGraphic","3-2-26",	"Knob fill- graphic",	"Graphic to replace knob", 					APPEARANCE, 15, false, -1, IMAGE_ASSET_TYPE));
			// Clock
			register(new PropertyDescription("handColor", 		"3-7-00",	"Hand- color", 		"Clock hand- color", 	APPEARANCE, 16, false, -1, 	COLOR_TYPE));
			register(new PropertyDescription("handWeight", 		"3-7-01",	"Hand- weight", 	"Clock hand- weight", 	APPEARANCE, 17, false, -1, 	UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("handAlpha", 		"3-7-02",	"Hand- opacity %",	"Clock hand- opacity percent", 	APPEARANCE, 18, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));

			register(new PropertyDescription("scrollThumbColor","3-7-17",	"Knob- fill col", "Scroll bar knob- fill color\n(set scroll bar in \"Layout: Scroll\")", 		APPEARANCE, 19, false, -1, 	COLOR_TYPE, null));
			register(new PropertyDescription("scrollThumbOverColor","3-7-18","Knob- over col","Scroll bar knob- over color\n(set scroll bar in \"Layout: Scroll\")", 		APPEARANCE, 20, false, -1, 	COLOR_TYPE, null));
			register(new PropertyDescription("scrollThumbAlpha","3-7-19",	"Knob- opac %","Scroll bar knob- opacity percent\n(set scroll bar in \"Layout: Scroll\")", 		APPEARANCE, 21, false, -1, 	NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("scrollThumbLineColor","3-7-20","Knob- line col","Scroll bar knob- line color\n(set scroll bar in \"Layout: Scroll\")", 		APPEARANCE, 22, false, -1, 	COLOR_TYPE, null));

			register(new PropertyDescription("scrollTrackColor","3-7-21",	"Track- fill col","Scroll bar track- fill color\n(set scroll bar in \"Layout: Scroll\")", 		APPEARANCE, 23, false, -1, 	COLOR_TYPE, null));
			register(new PropertyDescription("scrollTrackAlpha","3-7-22",	"Track- opac %","Scroll bar track- opacity percent\n(set scroll bar in \"Layout: Scroll\")", 		APPEARANCE, 24, false, -1, 	NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("scrollTrackLineColor","3-7-23",	"Track- line col","Scroll bar track- line color\n(set scroll bar in \"Layout: Scroll\")", 		APPEARANCE, 25, false, -1, 	COLOR_TYPE, null));
			register(new PropertyDescription("insertIndicatorActive", 	"", "Insert bar- on", 		"Display a bar to show where an object\nwill be inserted into a Grid or Box", APPEARANCE, 26, false, -1, BOOLEAN_TYPE));
			register(new PropertyDescription("insertIndicatorColor",	"", "Insert bar- col",	"Insertion bar- color", APPEARANCE, 27, false, -1, COLOR_TYPE));
			register(new PropertyDescription("insertIndicatorWeight",	"", "Insert bar- wgt",	"Insertion bar- line weight", APPEARANCE, 28, false, -1, UNSIGNED_NUMBER_TYPE, [0,30]));
			register(new PropertyDescription("highQuality", 	"3-7-00",	"High quality", 	"Play at high quality (may be slower)", 									APPEARANCE, 29, false, -1, BOOLEAN_TYPE));



			// BUTTONSTATES
			register(new PropertyDescription("toggleButtonNormalUp", 			"", "Up", 					"Graphic to show when button is up", BUTTONSTATES, 0, false, -1, IMAGE_ASSET_TYPE));
			register(new PropertyDescription("toggleButtonNormalOver", 			"", "Over", 				"Graphic to show when pointer is over button", BUTTONSTATES, 1, false, -1, IMAGE_ASSET_TYPE));
			register(new PropertyDescription("toggleButtonNormalDown", 			"", "Down", 				"Graphic to show when button is down", BUTTONSTATES, 2, false, -1, IMAGE_ASSET_TYPE));
			register(new PropertyDescription("toggleButtonNormalDisabled", 		"", "Power off", 			'Graphic to show when "Actions: Power" = F \n("Visibility: Opacity- pwr off" is also applied)', BUTTONSTATES, 3, false, -1, IMAGE_ASSET_TYPE));
			register(new PropertyDescription("toggleButtonCheckedUp", 			"", "✓- up", 				"Graphic to show when button \nis checked and up", BUTTONSTATES, 11, false, -1, IMAGE_ASSET_TYPE));
			register(new PropertyDescription("toggleButtonCheckedOver", 		"", "✓- over", 				"Graphic to show when button \nis checked and pointer is over button", BUTTONSTATES, 12, false, -1, IMAGE_ASSET_TYPE));
			register(new PropertyDescription("toggleButtonCheckedDown", 		"", "✓- down", 				"Graphic to show when button \nis checked and down", BUTTONSTATES, 13, false, -1, IMAGE_ASSET_TYPE));
			register(new PropertyDescription("toggleButtonCheckedDisabled", 	"", "✓- Power off", 		'Graphic to show when checked & "Actions: Power" = F \n("Visibility: Opacity- pwr off" is also applied)', BUTTONSTATES, 14, false, -1, IMAGE_ASSET_TYPE));
			register(new PropertyDescription("bitmap9Slicing",					"",	"9-slice scaling",		"Scale graphic with a 3x3 grid\nto avoid distortion",		BUTTONSTATES,	100,	false, -1,	BOOLEAN_TYPE));
			register(new PropertyDescription("buttonFit",						"",	"Graphic fit", 			"Fit all graphics to button size (T),\nor maintain original proportions (F)", BUTTONSTATES, 101, false, -1, BOOLEAN_TYPE)); 
			
			// EFFECTS - id prefix = 

			register(new PropertyDescription("shadowLength",	"3-12-00", 	"Shadow- length", 	"Shadow- length (0-30)",		EFFECTS, -1, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 30)));
			register(new PropertyDescription("shadowRollOverLength",	"3-12-00", 	"Shadow- over", 	"When pointer is over an object,\nadd or subtract shadow length (-30-30)",		EFFECTS, 0, false, -1, 	NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, -30, 30)));
			register(new PropertyDescription("shadowAngle",		"3-12-01", 	"Shadow- angle", 	"Shadow- angle (positive clockwise)", 	EFFECTS, 1, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 360)));
			register(new PropertyDescription("shadowBlur",		"3-12-02", 	"Shadow- blur", 	"Shadow- blur (0-30)", 	EFFECTS, 2, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 30)));
			register(new PropertyDescription("shadowColor",		"3-12-02", 	"Shadow- color", 	"Shadow- color", 	EFFECTS, 3, false, -1, 	COLOR_TYPE));
			register(new PropertyDescription("shadowAlpha",		"3-12-02", 	"Shadow- opac %", 	"Shadow- opacity percent", 	EFFECTS, 4, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			
			register(new PropertyDescription("glowLength",		"3-12-04", 	"Glow- length", 	"Glow- length (0-30)", 	EFFECTS, 5, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 30)));
			register(new PropertyDescription("glowStrength",	"3-12-05", 	"Glow- strength", 	"Glow- strength (0-6)", 	EFFECTS, 6, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 6)));
			register(new PropertyDescription("glowColor",		"3-12-03", 	"Glow- color", 		"Glow- color", 	EFFECTS, 7, false, -1, 	COLOR_TYPE));
			register(new PropertyDescription("glowInner", 		"3-12-06",	"Glow- inner", 		"Set glow to inner (T) or outer (F)", 	EFFECTS, 8, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("blur", 			"3-12-08",	"Blur", 			"Blur (0-100)", 	EFFECTS, 9, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("colorSaturation",	"3-12-07", 	"Saturation",		"Saturation (0-100)", 	EFFECTS, 10, false, -1, 	UNSIGNED_NUMBER_TYPE,
                    new NumberValueControlConstraint(NumberValueControlConstraint.SLIDER, 0, 100)));
			register(new PropertyDescription("reflection", 		"3-12-09",	"Reflection", 		"Mimic a reflective surface", 	EFFECTS, 11, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("blendMode", 		"3-12-10",	"Blend mode", 		"How this object blends with objects below it", 	EFFECTS, 12, false, -1, 	ENUMERATED_TYPE,
				[{label:"Normal", data:BlendMode.NORMAL}, {label:"Add", data:BlendMode.ADD}, {label:"Difference", data:BlendMode.DIFFERENCE}, {label:"Multiply", data:BlendMode.MULTIPLY}, 
					{label:"Overlay", data:BlendMode.OVERLAY} , {label:"Screen", data:BlendMode.SCREEN}, {label:"Erase", data:BlendMode.ERASE}, {label:"Lighten", data:BlendMode.LIGHTEN}, 
					{label:"Darken", data:BlendMode.DARKEN}]));
			
			
			// NOTES - id prefix = 8
			register(new PropertyDescription("authorNotes",		"3-8-00",	"Author notes", 	"Author notes\n(note is attached to object and hides on Run)",						NOTES, 		0, false, -1, 	STRING_TYPE));



			// Outlets - id prefix = 2

			// GESTURES - id prefix = 1
			register(new OutletDescription("click",	"2-1-02", 	"Click", "Pointer is pressed and released on this object", 	GESTURES,		0	));
			register(new OutletDescription("clickStart",	"2-1-01", 	"Click- down",     "Pointer is pressed on this object", 	GESTURES,		1	));
			register(new OutletDescription("clickRelease",	"2-1-02", 	"Click- release", "Pointer is released over this object", 	GESTURES,		2	));
			register(new OutletDescription("rollOver", 		"2-1-03",	"Pointer- over", 	"Pointer is over this object", 	GESTURES,		3	));
			register(new OutletDescription("rollOut", 		"2-1-04",	"Pointer- out", 	"Pointer is no longer over this object", 	GESTURES,		4	));

			// MOVE - id prefix = 2
			register(new OutletDescription("dragStart",		"2-2-00",	"Drag- start", 		"User has started to drag this object", 					DRAG_N_DROP,		0		));
			register(new OutletDescription("dragMove",		"2-2-01",	"Drag- moving", 		"User is continuing to drag this object", 						DRAG_N_DROP,		1		));
			register(new OutletDescription("dragRelease",	"2-2-02",	"Drag- release",	 	"User has released this object", 					DRAG_N_DROP,		2		));

			// STATES - id prefix = 3
			register(new OutletDescription("entryMade", 	"2-3-07",	"Entry made", 	"Return or Enter key pressed", 											STATES,  0		));
			register(new OutletDescription("hitMaxChars", 	"2-3-08",	"Max char reached", 	"Reached \"Max-char count\"\n(set this property in \"Values\")", 									STATES,		1		));
			register(new OutletDescription("objAdded", 		"2-3-00",	"Obj- added",		"Object- added to arena", 		STATES,		2		));
			register(new OutletDescription("objRemoved", 	"2-3-01",	"Obj- removed", 	"Object- removed from arena",	STATES,		3		));
			register(new OutletDescription("hitTop", 		"2-3-02",	"Hit- end value", 		"Hit end value\n(set this property in \"Values\")", 											STATES,		4		));
			register(new OutletDescription("hitBottom", 	"2-3-03",	"Hit- start value", 	"Hit start value\n(set this property in \"Values\")", 											STATES,		5		));
			register(new OutletDescription("tick", 			"2-3-05",	"Tick", 			"Send a tick signal\n(set \"Ticks per cycle\" in \"Values\")", 											STATES,		6		));
			register(new OutletDescription("cycleFinished", "2-3-06",	"Cycle done", 			"Current clock cycle is done", 											STATES,		7		));
			register(new OutletDescription("cyclesFinished", 		"2-3-04",	"All cycles done", 			"All clock cycles are done (time is up)", 											STATES,		8		));
			register(new OutletDescription("finished", 		"2-3-04",	"Done", 			"Playback is done (time is up)", 											STATES,		9		));
			register(new OutletDescription("buttonPressed", 	"2-3-08",	"Button pressed", 	"Button was pressed", 									STATES,		10		));
			register(new OutletDescription("checkedBox", 	"2-3-08",	"Checked- on", 		"Button is checked", 									STATES,		11		));
			register(new OutletDescription("uncheckedBox", 	"2-3-08",	"Checked- off", 	"Button is not checked", 									STATES,		12		));



			//TODO Above here has been evaluated


			

			// AnimationPath
			register(new PropertyDescription("pathNode", 		"3-12-06",	"Path point", 		"Point on a path", 	"PATH POINTS", 1, true, -1, 	POSITION_TYPE));

			// PORT - id prefix = 10
			// TruthTable
			//register(new PropertyDescription("port", 			"3-10-00",	"Port",	"Logic input", PORT, 0, true, 3));
			register(new OutletDescription("colPort", 			"3-10-01",	"ColPort", 	"Column out", PORT, 1, true, -1));
			register(new PropertyDescription("truthPort", 			"3-10-12",	"Truth Port",	"Condition matched", PORT, 2, true, -1, BOOLEAN_TYPE, null, false));
			register(new PropertyDescription("currentStateValues", 		"3-10-03",	"StateValues", 	"Table values", PORT, 3, true, 10000, STRING_TYPE));
			// Calculator
			register(new PropertyDescription("calcInputValues", 			"3-10-00",	"calcInputValues",	"Calculator input", PORT, 0, true, 26, "", null, true, false));
			register(new PropertyDescription("calcOutputValues", 			"3-10-00",	"calcOutputValues",	"Calculator output", PORT, 0, true, -1, STRING_TYPE, null, false));
			// Calc
			register(new PropertyDescription("calcValue", 		"3-10-02",	"Calc value", 	"Value of calculation", 			PORT, 0, false, -1, STRING_TYPE, null, false));
			// CalcTween
			register(new PropertyDescription("calcTweenValue", 	"3-10-03",	"Calc tween value", 	"Value of calculation", 	PORT, 0, false, -1, STRING_TYPE, null));
			// CalcGate
			register(new PropertyDescription("calcValue2",		"3-10-04",	"Calc value 2",	"Second value of gate calculation", PORT, 0, false, -1, STRING_TYPE));
			register(new PropertyDescription("gateValue", 		"3-10-05",	"Gate Value", 	"Calc control value", 				PORT, 0, false, -1, BOOLEAN_TYPE));
			
			// functions
			register(new PropertyDescription("input", 			"3-10-06",	"Input Value", "Input Value", PORT, 0, true, 100, STRING_TYPE));
			register(new PropertyDescription("value", 			"3-10-07",	"Input Value", "Input Value", VALUE, 0, false, -1, STRING_TYPE));
			register(new PropertyDescription("expression",		"3-10-08",	"expression", "expression", VALUE, 0, false, -1, STRING_TYPE));
			register(new PropertyDescription("result",			"3-10-09",	"result", "result", VALUE, 1, false, -1, STRING_TYPE, null, false));
			register(new OutletDescription("output",			"3-10-10",	"output", "output", PORT, 0, true, 8));
			register(new OutletDescription("outputDefault", 	"3-10-11",	"outputDefault", "outputDefault", PORT, 1));
			register(new PropertyDescription("statusMessage",		"",	"Table status", 'Current status of Answer table\n(Off (0), Judging (1), Not Judging (2)\n(Not Judging (2) is only available when \nthe "Judge now" port is wired.)', VALUE, 0, false, -1, NUMBER_TYPE, null, false));
			
			// Show
			register(new PropertyDescription("showControls",	"3-11-00",	"Show- controls", 	"Show control bar",						TABLES, 0, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("showLogicRows",	"3-11-01",	"Show- logic rows", 	"Show logic rows",						TABLES, 1, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("showValueRows",	"3-11-02",	"Show- values", 	"Show value rows",						TABLES, 2, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("showTweens",	"3-11-03",	"Show- tweens", 	"Show tween rows",						TABLES, 3, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("showOuts",	"3-11-04",	"Show- outs", 	"Show outs",						TABLES, 4, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("showBranchingRow",	"3-11-05",	"Show- branch", 	"",			TABLES, 5, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("continuallyJudging",	"3-11-05",	"Judging on", 	"Judge on every logic input change (Reset if false)",			TABLES, 6, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("poweredOnRun", "3-11-05",	"Power on", 	"Table has power on during Run",			TABLES, 7, false, -1, 	BOOLEAN_TYPE));
			
			//Goto URL
			register(new PropertyDescription("theURL",		"",	"The URL", "URL destination for this link \n(web address)", TABLES, 0, false, -1, STRING_TYPE,false));
			register(new PropertyDescription("queryString",		"",	"Query String", "Query data string- \n(this is appended to URL)", TABLES, 0, false, -1, STRING_TYPE))
			register(new PropertyDescription("newWindow", "",	"New Window", 	"Open URL in a new window",			TABLES, 0, false, -1, 	BOOLEAN_TYPE));
			register(new PropertyDescription("jump", "",	"Jump", 	"Goto Url",			TABLES, 0, false, -1, 	BOOLEAN_TYPE));
			register(new InletDescription("navigate", "",	"Jump", 	"Jump to URL destination \n(includes query data string, if defined)",			PORT));
			register(new InletDescription("postData", "",	"Send data", 	"Only send query data to URL\n(do not jump)",			PORT));
			register(new InletDescription("gotoURL",			"1-4-00",	"Execute", "Execute weblink", PORT));
			register(new PropertyDescription("keyPress", "3-11-10",	"Keypress", "Keypress", TWEENS, 1, false, -1, KEY_TYPE));
			register(new PropertyDescription("tweenDelay","","Delay- secs", "Delay before transition- length in seconds", TWEENS, 10001, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("tweenType","","Trans- tween", "Transition tween- tempo or flow of change", TWEENS, 10002, false, -1, ENUMERATED_TYPE,
				[
					{label:"linear", data:"linear"},					{label:"easeInSine", data:"easeInSine"},	
					{label:"easeOutSine", data:"easeOutSine"},			{label:"easeInOutSine", data:"easeInOutSine"},			{label:"easeInQuad", data:"easeInQuad"},
					{label:"easeOutQuad", data:"easeOutQuad"},			{label:"easeInOutQuad", data:"easeInOutQuad"},		{label:"easeInCubic", data:"easeInCubic"},			
					{label:"easeOutCubic", data:"easeOutCubic"},		{label:"easeInOutCubic", data:"easeInOutCubic"},	{label:"easeInQuart", data:"easeInQuart"},	
					{label:"easeOutQuart", data:"easeOutQuart"},		{label:"easeInOutQuart", data:"easeInOutQuart"},	{label:"easeInQuint", data:"easeInQuint"},	
					{label:"easeOutQuint", data:"easeOutQuint"},			{label:"easeInOutQuint", data:"easeInOutQuint"},	{label:"easeInExpo", data:"easeInExpo"},
					{label:"easeOutExpo", data:"easeOutExpo"},			{label:"easeInOutExpo", data:"easeInOutExpo"},		{label:"easeInCirc", data:"easeInCirc"},
					{label:"easeOutCirc", data:"easeOutCirc"},			{label:"easeInOutCirc", data:"easeInOutCirc"},		{label:"easeInElastic", data:"easeInElastic"},	
					{label:"easeOutElastic", data:"easeOutElastic"},	{label:"easeInOutElastic", data:"easeInOutElastic"},{label:"easeInBack", data:"easeInBack"},	
					{label:"easeOutBack", data:"easeOutBack"},			{label:"easeInOutBack", data:"easeInOutBack"},		{label:"easeInBounce", data:"easeInBounce"},	
					{label:"easeOutBounce", data:"easeOutBounce"},		{label:"easeInOutBounce", data:"easeInOutBounce"},	{label:"none", data:"none"}
				]));
			register(new PropertyDescription("tweenTime","","Trans- secs", "Transition time- length in seconds", TWEENS, 10003, false, -1, NUMBER_TYPE));
			register(new PropertyDescription("tweenHold","","Hold- secs", "Hold time after transition (secs)", TWEENS, 10004, false, -1, NUMBER_TYPE));

			register(new PropertyDescription("styleSpec", 		"3-11-09",	"Style Sheets", "Select which style sheets are used for this text object.", FONT, 3, false, -1, 	STYLE_TYPE));
			//register(new PropertyDescription("styleName",		"3-12-09",	"Font- style", "Style name to be applied to an element", FONT, 10, false, -1, STRING_TYPE, null, true));


			// Collaboration
			register(new InletDescription("send", "",	"Send", "Send data", DO, 0));
			register(new InletDescription("receive", "",	"Receive", "Receive data", DO, 1));
			register(new OutletDescription("dataReceived",	"", 	"Data received",     	"Data was received by this client and is available", 		STATES,		0	));

			register(new PropertyDescription("statistic","","Statistic", "This type of statistic to be calculated", VALUE, 8, false, -1, ENUMERATED_TYPE,
				[
					{label:"Mean (Average)", data:"mean"},
					{label:"Median (Middle)", data:"median"},
					{label:"Mode (Most frequent)", data:"mode"}
				]));

            // Text Input properties
            register(new InletDescription("giveTextFocus", "",	"Set focus", "Activate user text input.", DO, 10));
            register(new InletDescription("takeTextFocus", "",	"Remove focus", "Deactivate user text input.", DO, 11));
            register(new OutletDescription("gotTextFocus", "",	"Focus set", "Text field is ready for input.", STATES, 10));
            register(new OutletDescription("lostTextFocus", "",	"Focus removed", "Text field is no longer ready for input.", STATES, 11));


//////////    BELOW UNFINISHED

			//media
			register(new PropertyDescription("cuePointList",	"3-2-03",	"Cue Point List", "cuePointList", VALUE, 4, false, -1, STRING_TYPE));
			register(new PropertyDescription("cuePoint",		"3-2-04",	"Cue Point", "cuePoint", VALUE, 5, false, -1, NUMBER_TYPE));

			// Arena
			// Audio
			// Button
			register(new PropertyDescription("downText", 		"3-2-19",	"Down Text", 		"Text displayed when down", 							VALUE, 3, false, -1, STRING_TYPE));

			// Truth table properties
			register(new PropertyDescription("value", 			"3-2-29",	"Value", 	"Table result", 							VALUE, 0, false, -1, UNSIGNED_NUMBER_TYPE, null, false));
			register(new PropertyDescription("column", 			"3-2-30",	"Column",	"Column number",						VALUE, 0, false, -1, UNSIGNED_NUMBER_TYPE));
			register(new PropertyDescription("activeColumn", 	"3-2-30",	"Active Column",	"Active column number",			VALUE, 0, false, -1, UNSIGNED_NUMBER_TYPE));
			// Clock



			register(new PropertyDescription("output",	"3-3-10",	"Text out", 	"Text content with current values", 	VALUE, 	15, false, -1, FORMATTED_TEXT_TYPE,null, false));
///////////// ABOVE THIS UNFINISHED

			register(new PropertyDescription("objectTransition", 		"3-2-07",	"Trans- type", 		"Transition type- style of change", 					VALUE, 	2, false, -1, 	ENUMERATED_TYPE,
				[{label:"NONE", data:"none"},{label:"Fade", data:TransitionTypes.FADE_IN}, {label:"Zoom", data:TransitionTypes.ZOOM_IN}
				/*	{label:"Ken Burns", data:"kenBurns"} */ ]));
			register(new PropertyDescription("objectTransTweenType","","Trans- tween", "Transition tween- tempo or flow of change", ACTIONS, 3, false, -1, ENUMERATED_TYPE, TWEEN_ENUMERATION_CONSTRAINTS));
			register(new PropertyDescription("objectTransitionSecs", 	"3-2-08",	"Trans- secs", 		"Transition time- length in seconds", 	ACTIONS, 	4, false, -1, 	UNSIGNED_NUMBER_TYPE));




			// Arena
			// slider outlets
			// Clock outlets
		}
		
		public function register(modifier:IModifier):Boolean
		{
			if (_registry[modifier.key] != null)
				return false;
			
			if (modifier == null)
				return false;
			else
				_registry[modifier.key] = modifier;
			return true;
		}
		
		public function fetch(key:*):IModifier
		{
			return _registry[key];
		}
		
		public function getKeyFor(modifier:IModifier):*
		{
			for ( var key:Object in _registry )
				if (_registry[key] === modifier)
					return key;
			return null;
		}

        public function get modifiersRegistry():Dictionary
        {
            return _registry;
        }
		
		/**
		 * Give the weight of a property category.
		 * Currently only for properties, not inlets/outlets. 
		 * @param category
		 * @return 
		 * 
		 */
		public function categoryWeight(category:String):int
		{	
			switch (category) {
		// property heading categories
				case VISIBILITY:	return 10;
				case VALUE:			return 20;
				case ACTIONS:		return 30;
				case FONT:			return 40;
				case STYLE:			return 50;
				case GEOMETRY:		return 60;
				case LAYOUT:        return 70;
				case ALIGN:			return 80;
				case APPEARANCE:	return 90;
				case BUTTONSTATES:  return 100;
				case EFFECTS:		return 110;
				case NOTES:			return 120;
				case CALCULATED:	return 130;
				case TABLES:	    return 140;
				case PORT:          return -1;

		// inlet heading categories
				case DISPLAY:	    return 10;
				case DO:	        return 20;
				case SETTERS:	    return 30;

		// outlet heading categories
				case GESTURES:      return 10;
				case DRAG_N_DROP:   return 20;
				case COLLIDE:       return 30;
				case STATES:        return 40;
				case TRIGGERS:      return 50;


				default:			return 0;
			}
		}
		
		
		
		public static function convertValue(type:String, value:Object):Object
		{
			if(type == null){
				return value;
			}
			if(value is FormattedText && type != FORMATTED_TEXT_TYPE) {
				value = FormattedText(value).toString();
			}
			var result:Object = null;
			switch(type) {
				case BOOLEAN_TYPE:
					switch(value) {
						case null: case false: case "false": case 0: case "":  result = false; break;
						default: result = true; break;
					}
					break;
				case STRING_TYPE:
					if (value==null) {
						result = "";
					} else if (value is Number) {
						result = formatNumber(Number(value));
					} else if (value is AbstractObject) {
						result = AbstractObject(value).title;
					} else if (value is Asset) {
						result = Asset(value).assetID;
					} else if (value is Array) {
						result = (value as Array).map(function(item:Object, index:Number, arr:Array):Object {return Modifiers.convertValue(Modifiers.STRING_TYPE, item);}).join(",");
					} else {
						result = new String(value);
					}
					break;
				case COLOR_TYPE:
					if (value is uint || value is Number)
						result = uint(value);
					else if(value is String) {
						var str:String = value as String;
						if (str.substr(0,1) == "#")
							result = uint( "0x" + str.substr(1));  // convert hex digits
						else
							result = uint(str);  // convert decimal digits
					} else if (value is GraphicFill) {
						result = GraphicFill(value).convertToColor();
					} else {
						result =  uint(0);
					}
					break;
				case UNSIGNED_NUMBER_TYPE:
					if(value == null || isNaN(Number(value))) {
						result = 0;
					} else if (value is String || value is Number || value is uint) {
						result = Math.max(0, Number(value));
					} else {
						result =  0;
					}
					break;
				case NUMBER_TYPE:
					if(value == null || isNaN(Number(value))) {
						result = 0;
					} else {
						result =  Number(value);
					}
					break;
				case INT_TYPE:
					if(value == null || isNaN(Number(value))) {
						result = 0;
					} else {
						result =  Math.round(Number(value));
					}
					break;
				case ENUMERATED_TYPE:
					if(value == null) {
						result = "";
					} else if (value is String) {
						result = value as String;
					} else {
						result = value.toString();
					}
					break;
				case POSITION_TYPE:
					if(value == null) {
						result = new PositionAndAngle();
					} if (value is PositionAndAngle) {
						result = PositionAndAngle(value).clone();
					} else if (value is String) {
						var pos:PositionAndAngle = new PositionAndAngle();
						pos.setFromString(value as String);
						result = pos;
					} else if (value is AbstractObject) {
						result = AbstractObject(value).locationXYR;
					} else {
						result = new PositionAndAngle();
					}
					break;
				case POINT_TYPE:
					if (value is Point) {
						result = value as Point;
					} else if (value is String) {
						result = pointFromString(value as String);
					} else if (value is AbstractObject) {
						result = new Point(AbstractObject(value).x, AbstractObject(value).y);
					} else {
						result = new Point(0.5,0.5);
					}
					break;
				case FILL_TYPE:
					if (value is GraphicFill) {
						result = GraphicFill(value).clone();
					} else if (value is String) {
						result = new GraphicFill(value.toString());
					} else if (value is Number) {
						var fill:GraphicFill = new GraphicFill();
						fill.color = value as Number;
						result = fill;
					} else {
						result = new GraphicFill();
					}
					break;
				case OBJECT_TYPE:
					if (value is AbstractObject) {
						result = value;
					} else if(value is String) {
						result = ApplicationController.instance.authorController.getObjectByTitle(value as String);
					} else {
						result = null;
					}
					break;
				case Modifiers.IMAGE_ASSET_TYPE:
				case Modifiers.AUDIO_ASSET_TYPE:
				case Modifiers.VIDEO_ASSET_TYPE:
				case Modifiers.TEXT_ASSET_TYPE:
				case Modifiers.FONT_ASSET_TYPE:
					if (value as Asset) {
						if (Asset(value).propertyType == type || Asset(value).propertyType == "")
							result = value;
						else
							result = null;  // can't convert sound to image, etc
					} else if (Utilities.UUID_REGEX.test(value as String)) {
						result = AssetController.instance.requestForID(value as String);
					}
					break;
				case FORMATTED_TEXT_TYPE:
					if (value is String){
						result = FormattedText.fromString(value as String);
					} else if(value is FormattedText){
						result = value;
					}else {
						result = FormattedText.fromString(convertValue(STRING_TYPE, value) as String);
					}
					break;

				case LIST_TYPE:
					if (value is Array){
						result = value;
					}else {
						result = [];
					}
					break;
					
				default:
					if(value == null) {
						result = "";
					} else {
                        result = value;
                    }
			}
//			trace("convert: from="+value, "result="+result, "type="+type);
			return result;
		}

        public static function checkCondition(value:*,  condition:*, forceExpression:Boolean, obj:AbstractObject):Boolean {

            if(forceExpression || value is Number) {
                var env:Object = Ast.defaultEnvironment();
                env["#"] = value;
                try {
                    var result:* = new Ast(condition as String).evaluate(env);
                    if(result is Number) {
                        return value == result
                    } else if(result is Boolean) {
                        return result;
                    } else {
                        return false;
                    }
                } catch(e:Error) {
                    LogService.error("Error evaluating expression for condition, " + e.message + " object="+obj.logStr);
                    return false;
                }
            } else if(value is Array) {
                if(condition == null) {
                    return (value as Array).length == 0;
                } else {
                    return (value as Array).indexOf(condition) >= 0;
                }
            } else if((value is String || value is FormattedText) && (condition is String || condition is FormattedText)) {
                var valStr:String = (value is FormattedText)?FormattedText(value).toString():(value as String);
                var conditionStr:String = (condition is FormattedText)?FormattedText(condition).toString():(condition as String);
                return new RegExp(conditionStr).test(valStr);
            } else {
				return isEqualTo(value, condition);
            }
			return false;
        }
		
		public static function isEqualTo(value:*, other:*):Boolean
		{
			const TOLERANCE:Number = 0.00000001;
			if(value is GraphicFill && other is GraphicFill) {
				return GraphicFill(value).equals(other as GraphicFill);
			} else if (value is PositionAndAngle && other is PositionAndAngle) {
				return PositionAndAngle(value).isEqualTo(other as PositionAndAngle);
			} else if (value is Point && other is Point) {
				return Point.distance(value, other) < TOLERANCE;
			} else if ((value is Number && (other is String || other is Number)) || (other is Number && (value is Number || value is String))) {
				return Math.abs(Number(value) - Number(other)) < TOLERANCE;
			} else {
				return value == other;
			}
			return false;
		}
		
		/**
		 * When a new wire is created we need to use the best of the two values...
		 * so a nonzero slider value should override a zero slider val,
		 * and a XYR position should override a blank table cell,
		 * and a zero slider value should override a blank text value,
		 * regardless of which way the wire is drawn. 
		 * @param value
		 * @param other
		 * @return true if "value" is better than "other"
		 * 
		 */
		public static function valueShouldOverride(value:*, other:*):Boolean
		{
			// Very Tricky:  use the test function below when making changes
			
			// zero from slider should show "0" in text, regardless of which way wire is dragged
			if (isZero(value) && isEmptyString(other)) {
				return true;
			} else if (isEmptyString(value) && isZero(other)) {
				return false;
			}
			
			// general case
			if (value != null && isZero(value) == false && !isEmptyString(value)) {
				if (other == null || isZero(other) || isEmptyString(other)) {
					return true;
				}
			}
			return false;
		}
		
		private static function isEmptyString(thing:*):Boolean
		{
			if (thing is String) {
				return thing == "";
			} else {
				var ft:FormattedText = thing as FormattedText;
				if (ft != null) {
					return ft.isEmpty;
				}
			}
			return false;
		}
		
		private static function isZero(thing:*):Boolean
		{
			return thing is Number && thing == 0;
		}
		
//		public static function test_valueShouldOverride():void
//		{
//			testOne("0,blank", 0,"", true);
//			testOne("blank,0", "",0, false);
//			
//			testOne("0,FormattedText", 0, FormattedText.fromString(""), true);
//			testOne("FormattedText,0", FormattedText.fromString(""), 0, false);
//			
//			testOne("a,blank", "a","", true);
//			testOne("blank,a", "","a", false);
//			
//			testOne("true,blank", true,"", true);
//			testOne("blank,true", "",true, false);
//			
//			testOne("false,blank", false,"", true);
//			testOne("blank,false", "",false, false);
//		}
//		
//		private static function testOne(label:String, value:*, other:*, correct:Boolean):void
//		{
//			trace("");
//			trace("TEST: "+label);
//			var result:Boolean = valueShouldOverride(value, other);
//			trace("  result="+result, "correct="+correct, "judgement="+(result == correct));
//		}
		
		/**
		 * Format a number as a string.
		 * Prevent showing extra decimals, such as 123.40000000000001
		 * which seem to result from a defect in flash.
		 * And show up when a slider is connected to a Text object,
		 * and the slider has unit = 0.1 or 0.01 
		 * @param value
		 * @return 
		 * 
		 */
		public static function formatNumber(value:Number):String
		{
			var text:String = value.toString();
			if (text.indexOf(".") != -1) {  // has decimal point
				// match three or more zeroes, followed by one other digit, at end of string
				var cleaned:String = text.replace(/000+[1-9]$/,"");
				return cleaned;
			} else {
				return text;
			}
		}
		
		private static function pointFromString(str:String):Point {
			
			// decode the format produced by Point.toString():   (x=123, y=456)
			var comma:int = str.indexOf(",");
			var xStr:String = str.substr(3,comma-3);
			var yStr:String = str.substr(comma+4, str.length-comma-5);
			trace("pointFromString",str," = ",xStr,yStr);
			return new Point(Number(xStr), Number(yStr));
		}
	}
}
