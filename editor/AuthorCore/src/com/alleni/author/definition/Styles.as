package com.alleni.author.definition
{
	import com.alleni.author.definition.text.Fonts;
	import com.alleni.author.model.ui.DrawingPreset;
	import com.alleni.author.model.ui.TextPresets;
	
	import mx.collections.ArrayList;
	import mx.collections.IList;

	public class Styles
	{
		public static const DRAWING_PRESETS:Vector.<DrawingPreset> = Vector.<DrawingPreset>([
			
			 new DrawingPreset("VectorDrawing", 	0, 			4, 		100, 	Vector.<uint>([0xA6BFD8]), 0)
			,new DrawingPreset("RectangleObject", 	0,			0,		100,	Vector.<uint>([0x8CA8C9]), 100, Vector.<uint>([0x47739C,0xBFD6ED]), true, Vector.<uint>([0x053B70, 0xB3C9E3])) 
			,new DrawingPreset("Oval", 				0,			0,		100,	Vector.<uint>([0x8CA8C9]), 100, Vector.<uint>([0xD6E8FA, 0x47739C]), false, Vector.<uint>([0x072f53,0xD6EBFA])) 
			,new DrawingPreset("Line", 				0, 			2, 		100, 	Vector.<uint>([0]), 	0)
			,new DrawingPreset("Slider", 			0x124578,	1,		100,	Vector.<uint>([0x577DA6, 0xE0EDFC]), 100, Vector.<uint>([0x577DA6, 0xE0EDFC]), true, Vector.<uint>([0x577DA6, 0xE0EDFC]))
			,new DrawingPreset("PushButton", 		0,			0,		100,	Vector.<uint>([0x7094B8, 0XE0EDFC]), 100, Vector.<uint>([0X7094B8, 0XE0EDFC]), true, Vector.<uint>([0X7094B8, 0XE0EDFC]))
			,new DrawingPreset("Button", 			0,			0,		100,	Vector.<uint>([0x7094B8, 0XE0EDFC]), 100, Vector.<uint>([0X7094B8, 0XE0EDFC]), true, Vector.<uint>([0X7094B8, 0XE0EDFC]))
			,new DrawingPreset("TextObject", 		0,			0,		100,	Vector.<uint>([0xFFFFFF]), 0, Vector.<uint>([0XA6BFD8, 0XE0EDFC]), true) 
			,new DrawingPreset("PathObject", 		0x577DA6,	4,		100,	Vector.<uint>([0xBFD6ED]), 0, Vector.<uint>([0x7D9EBF, 0XE0EDFC]), true) 
			,new DrawingPreset("Arena", 			0x124578,	1,		100,	Vector.<uint>([0xFFFFFF]), 100, Vector.<uint>([0XB3C9E3, 0XEBF5FF]), true) 
		]); /* {label:'Highlighter', lineColor:16776960, lineThickness:20, lineAlpha:50, fillSpec:0, fillAlpha:0, fillGradient} linearGradient shineGradient */
		
		public static const TEXT_PRESETS:Vector.<TextPresets> = Vector.<TextPresets>([
			 new TextPresets("PushButton", Fonts.DEFAULT_SANS_FONT, 14, 0x000000, "center", "middle", 0)
			,new TextPresets("Button", Fonts.DEFAULT_SANS_FONT, 14, 0x000000, "center", "middle", 2)
			,new TextPresets("TextObject", Fonts.DEFAULT_SANS_FONT, 14, 0x000000, "left", "top", 0)
			,new TextPresets("TextInput", Fonts.DEFAULT_SANS_FONT, 14, 0x000000, "left", "top", 0)
			,new TextPresets("CheckBox", Fonts.DEFAULT_SANS_FONT, 14, 0x000000, "left", "top", 2)
		]);
		
		// used for Review Vellum sketching. This had been synthesized with the DRAWING_PRESETS above but apparently that is headed in a different direction
		// to accommodate some changes to Inspector presets. Do not remove! --PJK 13OCT10
		public static const SKETCHING_PRESETS:Vector.<DrawingPreset> = Vector.<DrawingPreset>([
			 new DrawingPreset('Green',                     0x006026,       8,     	90)
			,new DrawingPreset('Highlighter',               16776960,       20,     50)
			,new DrawingPreset('Red Felt-tip',              16711680,       5,     	80)
			,new DrawingPreset('Black Permanent',   		0,             	20,     90)
		]);
	}
}