package com.alleni.author.definition
{
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ui.WireAnchor;

	public class InletDescription extends AbstractModifierDescription implements IModifier
	{
		public function InletDescription(key:String="", id:String="", label:String="", description:String="", category:String="", weight:int=0, indexed:Boolean=false, autoAdd:Number=-1)
		{
			super(key, id, label, description, category, weight, indexed, autoAdd);
		}
		
		override public function get writeable():Boolean {
			return true;
		}
		
		/**
		 * Create an anchor for this modifier
		 * only host is required for a ribbon
		 * for a gadget anchor x, y, and angle must be supplied
		 */
		public function createAnchor(host:AbstractObject, index:uint=0, x:Number=NaN, y:Number=NaN, angle:Number=NaN):WireAnchor {
			var anchor:WireAnchor = createAnchorInternal(host, index, x, y, angle);
			return anchor;
		}
	}
}