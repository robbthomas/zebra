package com.alleni.author.document
{
    import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.controller.ui.GadgetController;
	import com.alleni.author.definition.GadgetType;
	import com.alleni.author.definition.action.ActionTree;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.model.World;
	import com.alleni.author.model.gadgets.GadgetState;
	import com.alleni.author.model.gadgets.Gadgets;
    import com.alleni.author.model.project.Project;
    import com.alleni.author.model.test.TestPanel;
    import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.Assets;
	import com.alleni.author.model.ui.Wiring;
	import com.alleni.author.persistence.GadgetDecoder;
	import com.alleni.taconite.document.TaconiteDocument;
    import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.event.ModelEvent;
	import com.alleni.taconite.event.ModelUpdateEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.ModelRoot;

	public class Document extends TaconiteDocument
	{
		private var _project:Project;
		private var _wiringRoot:ModelRoot;
		private var _assetsRoot:ModelRoot;
		private var _gadgetsRoot:ModelRoot;
		
		private var _state:GadgetState;
		private var _actionTree:ActionTree;

        private var _running:Boolean; 	// if running is false we are in author mode
        private var _locked:Boolean; 	// for running locked player mode operation

        public var newDocument:Boolean;

		public function Document(project:Project = null)
		{
			super(new World().modelRoot);
			this.wiringRoot = new Wiring().modelRoot;
			this.assetsRoot = AssetController.instance.freshModel.modelRoot;
			this.gadgetsRoot = GadgetController.instance.freshModel.modelRoot;
			
			if (project) {
                _project = project;
                if(_project.children) {
                    for each(var child:Project in _project.children) {
                        Gadgets(this.gadgetsRoot.value).gadgets.addItem(child);
                    }
                }
                if(_project.assets) {
                    for each(var asset:Asset in _project.assets) {
                        Assets(this.assetsRoot.value).assets.addItem(asset);
                    }
                }
            } else {
                _project = Project.empty();
            }
			
			_project.projectTypeId = GadgetType.PROJECT;
			_state = _project.state;
			
			if (_state != null) {
				const world:World = root.value as World;
				if ("autosaveMinutes" in _state) {
					world.autosaveMinutes = _state["autosaveMinutes"];
				}
                if ("running" in _state) {
                    this.running = _state["running"];
                }
                if ("locked" in _state) {
                    this.locked = _state["locked"];
                }
			}

            if(project != null){
                if(Application.instance.customerType == null){
                    Application.instance.customerType = "CREATOR";
                }
                if(project.accountType == null){
                    project.accountType = "CREATOR";
                }
                if(project.accountTypeId == 3) {
                    //TODO Merge these values, this is a hack to sync these values
                    project.accountType = "PRO";
                }
                if(Application.instance.customerType == "CREATOR" && (project.accountType == "PRO" || project.accountType == "ENTERPRISE")){
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.MUTE_APPLICATION_HOT_KEYS));
                }else{
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS));
                }
            }
		}
		
		public function construct(postProcess:Function=null):void
		{
			if (this._project && this.gadgetsRoot) {
                if(_state && TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
                    if(_state.assets) {
                        for each(var assetRef:Object in _state.assets) {
                            if(assetRef is String) {
                                AssetController.instance.requestForID(assetRef as String);
                            } else {
                                AssetController.instance.requestForID(assetRef.id);
                            }
                        }
                    }
                }

                var doc:Document = this;
                var myPostProcess:Function = function():void {
                    if (doc._state != null) {
                        doc._state = null;
                    }

                    const world:World = doc.root.value as World;
                    if (!world) return;
                    world.controlled = false; // controlled was previously not marked as Transient, so this line can be removed once all legacy data is gone.
                    world.controller.initializeObject(doc.root);
                    if (postProcess != null) {
	                    postProcess();
	                }
                };

				GadgetDecoder.createWorld(root.value as World, 
										this.project,
									 	this.wiringRoot.value as Wiring,
									  	_state,
                                        myPostProcess);
			}
		}
		
		override public function set upToDate(value:Boolean):void
		{
			if (upToDate != value) {
                ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_UNSAVED_CHANGES, !value));
            }
			super.upToDate = value;
		}
		
		public function get project():Project
		{
			return _project;
		}
		
		public function get scripts():TestPanel
		{
			return _project.scripts;
		}
		
		public function get actionTree():ActionTree
		{
			if (!_actionTree)
				_actionTree = new ActionTree();
			return _actionTree;
		}
		
		public function get name():String
		{
			return _project.name;
		}

		public function set name(value:String):void
		{
			_project.name = value;
		}

		override public function get documentId():String
		{
			return _project.projectId;
		}

		override public function set documentId(value:String):void
		{
			_project.projectId = value;
		}
		
		public function set wiringRoot(value:ModelRoot):void
		{
			_wiringRoot = value;
			// 'undo' support for wiring
			_wiringRoot.addEventListener(ModelEvent.MODEL_CHANGE, handleModelChange, false, 2);
			_wiringRoot.addEventListener(ModelUpdateEvent.MODEL_UPDATE, handleModelUpdate, false, 2);
		}
		
		public function get wiringRoot():ModelRoot
		{
			return _wiringRoot;
		}

		public function set assetsRoot(value:ModelRoot):void
		{
			_assetsRoot = value;
			// 'undo' support for assets
			_assetsRoot.addEventListener(ModelEvent.MODEL_CHANGE, handleModelChange, false, 2);
			_assetsRoot.addEventListener(ModelUpdateEvent.MODEL_UPDATE, handleModelUpdate, false, 2);
		}

		public function get assetsRoot():ModelRoot
		{
			return _assetsRoot;
		}

		public function set gadgetsRoot(value:ModelRoot):void
		{
			_gadgetsRoot = value;
			// 'undo' support for gadgets
			_gadgetsRoot.addEventListener(ModelEvent.MODEL_CHANGE, handleModelChange, false, 2);
			_gadgetsRoot.addEventListener(ModelUpdateEvent.MODEL_UPDATE, handleModelUpdate, false, 2);
		}

		public function get gadgetsRoot():ModelRoot
		{
			return _gadgetsRoot;
		}

        public function get running():Boolean {
            return _running;
        }

        public function set running(value:Boolean):void {
            _running = value;
        }

        public function get locked():Boolean {
            return _locked;
        }

        public function set locked(value:Boolean):void {
            _locked = value;
        }
    }
}
