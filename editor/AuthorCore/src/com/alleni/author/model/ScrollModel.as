package com.alleni.author.model
{
	import com.alleni.author.view.ScrollView;
	
	import flash.events.EventDispatcher;
	
	public class ScrollModel extends EventDispatcher
	{	
		private var _scrollControllerLength:Number = 0;
		private var _viewPortLength:Number = 0;;
		private var _enabled:Boolean = true;
		private var _maxScrollPosition:Number = 0;
		private var _minScrollPosition:Number = 0;
		private var _scrollPosition:Number = 0;
		private var _listeners:Vector.<ScrollView> = new Vector.<ScrollView>;
	
	
		
		public function ScrollModel(viewPortLength:Number, minScrollPosition:Number, maxScrollPosition:Number)
		{
			_viewPortLength = viewPortLength;
			_minScrollPosition = minScrollPosition;
			_maxScrollPosition = maxScrollPosition;
		}
		
		public function addListener(scrollView:ScrollView):void
		{
			_listeners.push(scrollView);
		}
		
		public function removeListener(scrollView:ScrollView):void
		{
			var index:Number = _listeners.indexOf(scrollView);
			
			if(index > -1)
				_listeners.splice(index,1);
		}
		
		
		public function get viewPortLength():Number
		{
			return _viewPortLength;
		}
		
		public function set viewPortLength(value:Number):void
		{
			_viewPortLength = value;
			updateListeners();
		}
		
		public function validate():void
		{
		}
		
		
		public function get enabled():Boolean
		{
			return _enabled;
		}
		
		public function set enabled(value:Boolean):void
		{
			_enabled = value;
		}
		
		
		
		public function get maxScrollPosition():Number
		{
			return _maxScrollPosition;
		}
		
		public function set maxScrollPosition(value:Number):void
		{
			_maxScrollPosition = value;
			updateListeners();
		}
		
		public function get minScrollPosition():Number
		{
			return _minScrollPosition;
		}
		
		public function set minScrollPosition(value:Number):void
		{
			_minScrollPosition = value;
			updateListeners();
		}
		
		public function setMinMaxScrollPosition(min:Number, max:Number):void
		{	
			_minScrollPosition = min;
			_maxScrollPosition = max;
			updateListeners();
			
		}
		
		
		
		public function updateListeners():void
		{	
			for each(var listener:ScrollView in _listeners)
				listener.handleModelUpdate();
		}
		
		//to do:
		
		//update listeners if model changes
		
	}
}