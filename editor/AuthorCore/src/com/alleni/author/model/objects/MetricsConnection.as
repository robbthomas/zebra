/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/19/11
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.objects {
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.ObjectInlets;
import com.alleni.author.model.ObjectProperties;

import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.service.BorealOperation;
import com.alleni.taconite.service.RestHttpOperation;

import flash.events.Event;
import flash.net.SharedObject;
import flash.net.SharedObject;

public class MetricsConnection extends AbstractConnectionObject {

	[Bindable]
	public var sendData:String = "";

	public function MetricsConnection() {
		ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("send"));
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("sendData"), "sendData");
	}

	override public function send():void {
		if(local) {
			var s:SharedObject = SharedObject.getLocal(key, "/data/"+getProjectId());
			s.data.value = sendData;
			s.flush();
		} else {
			var projectId:String = getProjectId();
			var operation:RestHttpOperation = new RestHttpOperation("data/"+projectId + "/" + key, {projectId:projectId, memberId:null, value:sendData, jsonData:"", lookupKey:key}, RestHttpOperation.POST);
			operation.displayName = "Setting data";
			BorealOperation.addListeners(operation, handleSent);
			operation.execute();
		}
	}

	private function handleSent(event:Event):void {
		var operation:RestHttpOperation = event.target as RestHttpOperation;
		if(operation.resultDecoded == null || operation.resultDecoded == "") {
			return;
		}
		var response:Object = operation.resultDecoded;
		// nothing here
	}

	override public function receive():void {
		if(local) {
			var s:SharedObject = SharedObject.getLocal(key, "/data/"+getProjectId());
			receiveData = s.data.value;
			WireAnchor(anchors["dataReceived"]).trigger();
		} else {
			var projectId:String = getProjectId();
			var operation:RestHttpOperation = new RestHttpOperation("data/"+projectId + "/" + key, null, RestHttpOperation.GET);
			operation.displayName = "Retrieving from Queue";
			BorealOperation.addListeners(operation, handleReceived);
			operation.execute();
		}
	}

	private function handleReceived(event:Event):void {
		var operation:RestHttpOperation = event.target as RestHttpOperation;
		if(operation.resultDecoded == null || operation.resultDecoded == "") {
			return;
		}
		var response:Object = operation.resultDecoded;
		receiveData = response.value;
		WireAnchor(anchors["dataReceived"]).trigger();
	}
}
}
