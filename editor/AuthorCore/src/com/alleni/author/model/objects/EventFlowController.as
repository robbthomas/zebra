package com.alleni.author.model.objects {
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.IPage;
import com.alleni.author.Navigation.Pager;
import com.alleni.author.Navigation.PagerController;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ProgressBarController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.definition.action.AddPageAction;
import com.alleni.author.definition.action.PageNumberInfo;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.document.Document;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.persistence.GadgetDecoder;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.service.assets.AssetLoadOperation;
import com.alleni.author.util.Set;
import com.alleni.author.util.cache.LRUCache;
import com.alleni.author.util.thread.PseudoThread;
import com.alleni.author.util.thread.PseudoThreadAsyncRunnable;
import com.alleni.author.util.thread.PseudoThreadDebugRunnable;
import com.alleni.author.util.thread.PseudoThreadLock;
import com.alleni.author.util.thread.PseudoThreadSimpleRunnable;
import com.alleni.author.util.thread.PseudoThreadWaitFrameRunnable;
import com.alleni.author.definition.application.ApplicationStrings;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.author.definition.AssetLoadingReport;

import flash.display.BitmapData;
import flash.events.Event;

import mx.collections.IList;
import mx.containers.ApplicationControlBar;
import mx.events.PropertyChangeEvent;

public class EventFlowController extends PagerController {

    public static var instance:EventFlowController = new EventFlowController();

    private static const THREAD_UNITS_PER_OBJECT:Number = 5.5;
    private static const THREAD_UNITS_PER_ASSET:Number = 400;


    public function EventFlowController() {
        super();
    }

    override public function initializeObject(m:TaconiteModel):void
    {
        super.initializeObject(m);
        var flow:EventFlow = m.value as EventFlow;
        if (!flow.loading) {
            flow.active = true;
        }
    }

    override protected function afterLoad(pager:Pager):void
    {
        super.afterLoad(pager);
        var flow:EventFlow = pager as EventFlow;
        pager.active = true;

        // activate the global backstage
        for each (var obj:AbstractObject in pager.objects) {
            activateObject(obj, true);
        }
        flow.pageCache.setSize(flow.cacheSize, flow.cacheSize);

        flow.titleBarPage = (flow.editingEventPage) ? flow.editingEventPage : flow.currentEventPage;
        flow.titleBarRunning = Application.uiRunning;

        if (Application.instance.validationEnabled) {
            ApplicationController.instance.requestEnterFrameAction(function():void{
                PseudoThread.add(new PseudoThreadSimpleRunnable("handleValidationChange", flow.handleValidationChange));
            });
        }
    }

    public function afterSwitchToProfessionalProject():void
    {
        var flow:EventFlow = AuthorController.instance.eventFlow;
        flow.titleBarVisible = true;
        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.STAGE_RESIZED));  // adjust FlowView size
        flow.currentEventPage.hadChildren = false;
        Application.instance.flowVisible = true;
    }

    override public function createFirstPageIfNeeded(pager:Pager):void {
        var pages:int = pager.pageCount;
        super.createFirstPageIfNeeded(pager);
        // TODO: consider a better way to do this.
        if(pages == 0 && pager.pageCount > 0) {
            (pager as EventFlow).wasEditingEvent = pager.masterPages.length;
        }
    }

    override protected function handleModelUpdate(model:TaconiteModel, property:String, newValue:Object, oldValue:Object, parent:TaconiteModel):void
    {
        var flow:EventFlow = model.value as EventFlow;
        if (flow == null) {
            return;  // property bubbling from a child object
        }
        super.handleModelUpdate(model, property, newValue, oldValue, parent);
        if (!flow.loading) {
            switch (property) {
                case "editingPage":
                case "currentPage":
                    flow.titleBarPage = (flow.editingEventPage) ? flow.editingEventPage : flow.currentEventPage;
                    updateProjectObject();
                    var newEditingPage:EventPage = flow.editingPage as EventPage;   // similar code in ArenaController
                    var newCurrentPage:EventPage = flow.currentPage as EventPage;
                    var oldPage:EventPage = oldValue as EventPage;
                    if(property == "currentPage" && Application.uiRunning){
                        WireAnchor(oldPage.anchors["onExit"]).trigger();
                    }
                    if(property == "currentPage" && Application.uiRunning){
                        WireAnchor(newCurrentPage.anchors["onEnter"]).trigger();
                    }
                    if (flow.ignorePageChanges) {
                        showMessageCenterAsNeeded(flow, newEditingPage);
                        showMessageCenterAsNeeded(flow, oldPage);
                        break;
                    }
                    var overlay:EventPage;
                    if (property == "currentPage" && flow.editingPage == null) {
                        // use bitmap overlay for runtime page-change, even when doing transition effect (the effect is done after next page is loaded & built)
                        // overlay is removed by PagerView
                        overlay = oldPage;
                    } else if (flow.editingPage) {
                        overlay = newEditingPage;
                    } else {
                        overlay = newCurrentPage;
                    }
                    changePage(flow, newCurrentPage, newEditingPage, oldPage, overlay);
                    flow.notFirstPage = flow.pageNumber > 1;    // (notFirstPage & notLastPage don't have inits)
                    flow.notLastPage = flow.pageNumber < flow.pages.length;
                    break;
                case "transientTransitionInProgress":
                    updatePagesActive(flow, newValue);  // cause arena controlbars to show. only deactivate outgoing page when in transition.
                    break;
                case "ignorePageChanges":
                    if (newValue == false && !flow.disableLoadUnload) {
                        changePage(flow,  flow.currentPage as EventPage, flow.editingPage as EventPage);
                    }
                    break;
                case "disableLoadUnload":   // load pages if going false
                    disableLoadUnload(flow, oldValue as Boolean, newValue as Boolean);
                    break;
                case "flowVisible":
                    Application.instance.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "flowVisible", oldValue, newValue));
                    if (newValue == false) {
                        updatePagesActive(flow);
                    }
                    break;
                case "cacheSize":
                    flow.pageCache.setSize(flow.cacheSize, flow.cacheSize);
                    enqueueUnloadPagesAsNeeded(flow);
                    break;
                case "notLastPage":
                    World(Application.instance.document.root.value).project.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "nextEventExists", oldValue,newValue));
                    break;
                case "notFirstPage":
                    World(Application.instance.document.root.value).project.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "prevEventExists", oldValue,newValue));
                    break;
                case "pageCount":
                    World(Application.instance.document.root.value).project.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "numberOfEvents", oldValue,newValue));
                    break;
            }
        }
    }

    /**
     * Load or unload the specified page(s) as needed.
     * @param flow
     * @param p1 New Current Page
     * @param p2 Editing Page
     * @param p3 Previous Editing Page
     */
    public function changePage(flow:EventFlow, p1:EventPage, p2:EventPage=null, p3:EventPage=null, overlay:EventPage=null):void
    {
        if (flow.disableLoadUnload) {
            return;  // while at Flow, don't load or unload anything
        }

        // collect set of needed pages regardless of whether already loaded (cache needs to know)
        var neededPages:Set = collectNeededPages(flow, [p1, p2, p3]);
        addSetToCache(flow, neededPages);  // ensure these have highest priority

        // requestedLoads = the pages that must be loaded
        var requestedLoads:Set = new Set(false);
        var requestedPopulation:int = collectPagesToLoad(flow, neededPages, requestedLoads);  // fills the requestLoads set
        if (requestedLoads.cardinality == 0) {
            changePageActivation(flow, p1, p2, p3);
            return;  // no pages need to be loaded, so avoid disableInput & threading
        }

        //trace("changePage p1="+p1, "p2="+p2, "p3="+p3, "population="+requestedPopulation);
        var block:PseudoThreadDebugRunnable = new PseudoThreadDebugRunnable("EventFlowController::changePage ("
            + (p1?p1.title:null) +","+ (p2?p2.title:null) +","+ (p3?p3.title:null) + ")", true);
        PseudoThread.add(block);
        flow.lazyLoading = true;
        var inputToken:Object = ApplicationController.instance.disableInput("EventFlowController:changePage");
        var overlayToken:Object = null;
        if (overlay) {
            overlayToken = showOverlayImage(flow, overlay, "EventFlowController:changePage");
        }

        var assetHoldToken:Object = AssetLoadOperation.hold("EventFlowController::changePage");

        var requestedUnloads:int = 0;
        PseudoThread.add(new PseudoThreadSimpleRunnable("phase=unloading", function():void{
            if (requestedUnloads > 0) {
                ProgressBarController.instance.enqueuePhaseWithName(ApplicationStrings.UNLOADING);
            }
        }));
        requestedUnloads += enqueueUnloadPagesAsNeeded(flow);

        PseudoThread.add(new PseudoThreadSimpleRunnable("phase=loadingObjects", function():void{
            if (requestedPopulation > 0) {
                ProgressBarController.instance.enqueuePhaseWithName(ApplicationStrings.BUILDING_OBJECTS);
            }
        }));
        enqueueLoadPageSet(flow, requestedLoads);

        if (requestedPopulation > 0 || requestedUnloads > 0) {
            //trace("changePage population="+requestedPopulation, "unloads="+requestedUnloads, "disableLoadUnload="+flow.disableLoadUnload);
            for each (var page:EventPage in requestedLoads.allItems) {
                trace("   ",page);
            }

            var constructionFrameCount:int = getFrameCountForLoadUnload(requestedPopulation, requestedUnloads);
            ProgressBarController.instance.startLazyLoadProgress(constructionFrameCount);
        }

        enqueue(false, "EventflowController::changePage load assets", function():void {
            var requestedAssets:Set = new Set(false);
            loadAssets(flow, neededPages, requestedAssets);
            AssetLoadOperation.unhold(assetHoldToken);  // requests assets from server, then starts wait-for-assets
            assetHoldToken = null;
            if (AssetController.instance.requestedCount > 0) {
                ProgressBarController.instance.enqueuePhaseWithName(ApplicationStrings.LOADING_ASSETS);  // Player doesn't show individual asset names, so this message is it
                ApplicationController.addEventListener(NotificationNamesApplication.ASSET_REQUEST_PROGRESS, 
                    handleAssetProgress(requestedAssets));
            } else {
                ProgressBarController.instance.assetLoadFraction = 1;
            }
        });

        enqueue(false, "EventflowController::changePageActivation", changePageActivation, this, flow, p1, p2, p3);
        PseudoThread.add(new PseudoThreadWaitFrameRunnable()); // wait for assets to render
        enqueue(false, "EventflowController::set lazyLoading = false", 
            function():void {
                flow.lazyLoading = false;  // causes overlayImage to be removed
                if (!flow.doingTweenToNextPage) {
                    ProgressBarController.instance.stop();
                }
                LibraryController.instance.updateForAssetChange(true);  // force=true to update while running
                ApplicationController.instance.enableInput(inputToken);
                removeOverlayImage(flow, overlayToken);
            }
        );
        PseudoThread.add(block.end);
    }

    private function changePageActivation(flow:EventFlow, p1:EventPage, p2:EventPage=null, p3:EventPage=null):void
    {
        showMessageCenterAsNeeded(flow, p1);
        showMessageCenterAsNeeded(flow, p2);
        showMessageCenterAsNeeded(flow, p3);
        if (!flow.transientTransitionInProgress) {
            updatePagesActive(flow);  // show child arena controlbars
        }
        ApplicationController.instance.wireController.showWiresForLevel();
    }

    public function getFrameCountForLoadUnload(requestedPopulation, requestedUnloads:int=0):int
    {
        return (requestedUnloads + requestedPopulation) * THREAD_UNITS_PER_OBJECT;
    }

    private function handleAssetProgress(requestedAssets:Set):Function {
        return function(event:ApplicationEvent):void {
            var assetReport:AssetLoadingReport = AssetController.instance.getLoadProgressForSet(requestedAssets);

            AssetController.instance.showLoadProgressFor(event, assetReport.remainingCount, assetReport.progressFraction);

            if (assetReport.remainingCount == 0) {
                ApplicationController.removeEventListener(NotificationNamesApplication.ASSET_REQUEST_PROGRESS, arguments.callee);
            }
        }
    }

    public function pageForPageNumber(flow:EventFlow, pagenum:int):IPage {
        if (flow.pages.length > 0) {
            return flow.pages[pagenum - 1];
        } else {
            return null;
        }
    }

    private function disableLoadUnload(flow:EventFlow, oldValue:Boolean, newValue:Boolean):void
    {
        var token:Object = ApplicationController.instance.disableInput("disableLoadUnload");
        suspendOrResumePage(flow.editingEventPage, newValue);
        suspendOrResumePage(flow.currentEventPage, newValue);
        changePage(flow, flow.currentPage as EventPage, flow.editingPage as EventPage, flow.wasEditingEventPage);
        enqueue(false, "Dispatch disableLoadUnload and enableInput", function():void{
            Application.instance.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "disableLoadUnload", oldValue, newValue));
            ApplicationController.instance.enableInput(token);
        });
    }

    private function suspendOrResumePage(page:EventPage, suspend:Boolean):void
    {
        if (page) {
            if (suspend) {
                page.suspendPage();
            } else {
                page.resumePage();
            }
            for each(var master:EventPage in page.masters) {
                if(master == null) {
                    continue;
                }
                if (suspend) {
                    master.suspendPage();
                } else {
                    master.resumePage();
                }
            }
        }
    }

    private function collectNeededPages(flow:EventFlow, candidates:Array):Set {
        var needed:Set = new Set(false);
        for each(var page:EventPage in candidates) {
            if (page && page.needsInternals()) {
                needed.add(page);
            }
        }
        for each(var master:EventPage in flow.masterPages) {
            if (master && master.needsInternals()) {
                needed.add(master);
            }
        }
        return needed;
    }


    private function collectPagesToLoad(flow:EventFlow, neededPages:Set, requestedLoads:Set):int
    {
        var population:int = 0;

        // decide which pages actually must be loaded
        var page:EventPage;
        for each (page in neededPages.allItems) {
            if (!page.hasInternals) {
                requestedLoads.add(page);
                population += countPopulation(page);
            }
        }

        return population;
    }

    private function enqueueLoadPageSet(flow:EventFlow, requestedLoads:Set):void
    {
        var page:EventPage;
        for each (page in requestedLoads.allItems) {
            enqueueLoadPage(flow, page);
        }
    }

    public function getBackstagePopulation(flow:EventFlow):int
    {
        var population:int = 0;
        if (flow != null) {
            var backstageObjects:Vector.<AbstractObject> = flow.allObjects();
            var ao:AbstractObject;
            var objects:IList = flow.objects;
            for each (ao in backstageObjects) {
                if (!(ao is EventFlow) && !(ao is EventPage)) {
                    if (objects.getItemIndex(ao) >= 0) {
                        population++;
                    }
                }
            }
        }
        return population;
    }

    private function enqueueLoadPage(flow:EventFlow, page:EventPage):void
    {
        // used for changePage and also for rebuildPages
        page.loadInternals();
        flow.loadedPages.add(page);
    }

    private function addSetToCache(flow:EventFlow, neededPages:Set):void
    {
        if (ApplicationController.instance.invokedAsEditor) {
            for each (var page:EventPage in neededPages.allItems) {
                flow.pageCache.setValue(page, page.title);
            }
        }
    }

    private function countPopulation(page:EventPage):int
    {
        if (page.gadget) {
            return page.gadget.estimateBuildoutCount;
        } else {
            return 0;
        }
    }

    private function loadAssets(flow:EventFlow, neededPages:Set, requestedAssets:Set):void
    {
        // for back stage
        collectPendingAssetsForContainer(flow, requestedAssets);
        
        // for each page
        var page:EventPage;
        for each (page in neededPages.allItems) {
            collectPendingAssetsForContainer(page, requestedAssets);
            // preview image, if any
            if (page.gadget != null && page.gadget.previewImage != null) {
                requestedAssets.add(page.gadget.previewImage);
            }
        }
        enqueue(false, "Load Event Assets", AssetController.instance.requestAssetsForSet, AssetController.instance, requestedAssets);
    }

    private function collectPendingAssetsForContainer(container:AbstractContainer, requestedAssets:Set):void
    {
        var asset:Asset;
        var assetSet:Set = AssetController.instance.assetsForContainer(container, true, true);
        var assetSetItems:Array = assetSet.allItems;
        for each (asset in assetSetItems) {
            requestedAssets.add(asset);
        }
    }

    private function enqueueUnloadPagesAsNeeded(flow:EventFlow):int {

        var result:int = 0;
        var list:Array = flow.loadedPages.allItems;
        for each(var page:EventPage in list) {
            assert(page.parent);
            result += unloadOnePageAsNeeded(flow,  page);
        }
        return result;
    }

    private function unloadOnePageAsNeeded(flow:EventFlow, page:EventPage):int
    {
        if(page.hasInternals && !page.needsInternals() && !page.preventUnload && !flow.pageInCache(page)) {
            enqueue(false, "Unload page [" + page.title + "]", page.unloadInternals, page);
            flow.loadedPages.remove(page);
            return 1;
        }
        return 0;
    }

    /**
     * Create an AsyncFunctionOperation and add to the operation queue.
     * @param function(signalCompletion:Function):void    must have a single argument: the completion signal function.
     */
    private function enqueue(async:Boolean, name:String, func:Function, thisArg:Object = null, ...args):void
    {
        if(async) {
            PseudoThread.add(new PseudoThreadAsyncRunnable(name,  func, thisArg));
        } else {
            PseudoThread.add(new PseudoThreadSimpleRunnable(name,  func, thisArg,  args))
        }
    }

    private function showMessageCenterAsNeeded(flow:EventFlow, page:EventPage):void {
        if(page && !Application.instance.flowVisible) {
            var needMC:Boolean = (page == flow.editingPage) && (Application.instance.document.project.accountTypeId >= 3);
            if (needMC != page.needMessageCenter) {
                page.needMessageCenter = needMC;
                ApplicationController.instance.wireController.showWiresForLevel();
            }
        }
    }

    override public function afterRestoreInitialValues(pager:Pager):void
    {
        enqueue(false, "EventFlowController::afterRestoreInitialValues", updatePagesActive, null, pager);
    }

    private function updateProjectObject():void
    {
        var doc:Document = Application.instance.document;
        if(doc) {
            var world:World = World(doc.root.value);
            if(world.project) {
                world.project.pageChanged();
            }
        }
    }

    public function saveResumeData(flow:EventFlow):void
    {
        var page:EventPage = flow.currentEventPage;
        page.updateImage();
        page.saveResumeData();
    }

    override public function updatePagesActive(pager:Pager, deactivateOutgoingPageOnly:Boolean=false):void
    {
        // activate the specified page & descendant objects, deactivate others
        var flow:EventFlow = pager as EventFlow;
        var page:IPage;
        var num:int;
        if (flow.editingPage) {
            num = flow.pages.getItemIndex(flow.editingPage);
            if (num < 0) {
                for each (page in flow.pages) {
                    activateObject(page.object, false);
                }
                for each (page in flow.masterPages) {
                    activateObject(page.object, (page == flow.editingPage));
                }
            } else {  // editing a page, not a master
                activatePageN(pager, num+1);
            }
        } else {  // running a page
            super.updatePagesActive(pager, deactivateOutgoingPageOnly);
        }
    }

    override public function replacePage(pager:Pager, newPage:IPage, oldPage:IPage):void
    {
        var flow:EventFlow = pager as EventFlow;
        super.replacePage(pager, newPage, oldPage);
        var oldEvent:EventPage = oldPage as EventPage;
        var newEvent:EventPage = newPage as EventPage;
        newEvent.image = oldEvent.image;
        newEvent.ribbonCategoriesToOpen = oldEvent.ribbonCategoriesToOpen;
        newEvent.needMessageCenter = oldEvent.needMessageCenter;

        newEvent.messageCenterVisible = newEvent.needMessageCenter;  // update MC visibility

        if (flow.loadedPages.hasItem(oldPage)) {
            flow.loadedPages.remove(oldPage);
            trace("replacingLoadedPage: newPageLoaded="+EventPage(newPage).hasInternals, newPage);
            if (EventPage(newPage).hasInternals) {
                flow.loadedPages.add(newPage);
            }
        }
        if (pager.currentPage == newPage) {
            pager.dispatchEvent(PropertyChangeEvent.createUpdateEvent(pager, "currentPage", oldPage, newPage));
        }
    }

    override protected function copySettingsToNewPage(oldPage:IPage, newPage:IPage):void
    {
        // called for add-page, duplicate-page, replace-Event-after-editing
        super.copySettingsToNewPage(oldPage, newPage);
        var oldEvent:EventPage = oldPage as EventPage;
        var newEvent:EventPage = newPage as EventPage;
        newEvent.eventTransition = oldEvent.eventTransition;
        newEvent.transTweenType = oldEvent.transTweenType;
        newEvent.transitionSecs = oldEvent.transitionSecs;
    }

    override protected function handleModelCollectionUpdate(model:TaconiteModel, property:String, collection:IList, kind:String, index:int, item:Object, oldItem:Object):void
    {
        super.handleModelCollectionUpdate(model, property, collection, kind, index, item, oldItem);
        var flow:EventFlow = model.value as EventFlow;
        var page:IPage = item as IPage;
        if (flow && !flow.loading) {
            var modelIndex:int = flow.getModelIndex(page);
            switch (property) {
                case "pages":
                case "masterPages":
                    flow.notLastPage = flow.pageNumber < flow.pages.length;
                    switch (kind) {
                        case ModelEvent.ADD_ARRAY_ITEM:
                            if(modelIndex <= flow.wasEditingEvent) {
                                flow.wasEditingEvent++;
                            }
                            break;
                        case ModelEvent.REMOVE_ARRAY_ITEM:
                            if (flow.loadedPages.hasItem(page)) {
                                flow.loadedPages.remove(page);
                                flow.pageCache.remove(page);
                            }
                            if(modelIndex == flow.wasEditingEvent) {
                                flow.wasEditingEvent = -1;
                            } else if(modelIndex < flow.wasEditingEvent) {
                                flow.wasEditingEvent--;
                            }
                            break;
                    }
            }
        }
    }

    private var _overlayImageTokens:Set = new Set(false);

    /**
     * Cover the stage with a bitmap image of "page" and leave it there until removeOverlayImage is called.
     * Except that if an overlayImage has already been shown, then it will not be disturbed.
     * @param page
     * @param source
     * @return token that must be supplied to removeOverlayImage().
     */
    public function showOverlayImage(flow:EventFlow, page:EventPage, source:String="Unnamed"):Object
    {
        if (page == null) {
            throw new Error("showOverlayImage: null page");
        }

        const token:Object = {name:source};
        _overlayImageTokens.add(token);

        if (!flow.overlayImage) {
            flow.overlayImage = page;
        }
        return token;
    }

/**
 * Enable keyboard and mouse input.
 * @param token that was received from disableInput().
 */
    public function removeOverlayImage(flow:EventFlow, token:Object):void
    {
        if (token == null) {
            return;
        }
        if (_overlayImageTokens.hasItem(token)) {
            _overlayImageTokens.remove(token);
        } else {
            throw new Error("removeOverlayImage: unknown token="+token);
        }
        if (_overlayImageTokens.isEmpty) {
            flow.overlayImage = null;
        }
    }

    public function dumpOverlayImage(flow:EventFlow):void
    {
        trace("flow.overlay tokens:     overlayImage="+flow.overlayImage);
        var list:Array = _overlayImageTokens.allItems;
        for each (var token:Object in list) {
            trace("   " + "token="+token, "name="+token.name);
        }
    }


    /**
     * Lock the PseudoThread until image of "page" and its masters have loaded.
     * @param flow
     * @param page
     */
    public function waitForPageImages(flow:EventFlow, page:EventPage):void
    {
//        trace("waitForPageImages", page);
        waitForOnePageImage(flow, page);
        for each(var master:EventPage in page.masters) {
            if (master) {
                waitForOnePageImage(flow, master);
            }
        }
    }

    private function waitForOnePageImage(flow:EventFlow, page:EventPage):void
    {
        if (page.gadget && page.image == null) {
            var asset:Asset = page.gadget.previewImage;
//            trace("  waitForOnePageImage asset="+asset);
            if (asset && asset.id != null) {
                var runningLock:PseudoThreadLock = new PseudoThreadLock();
                var lockName:String = "WaitPageImage / "+page.title;
                runningLock.lock(lockName);
//                trace("   lockName="+lockName);
                page.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, function(event:PropertyChangeEvent):void{
                    if (event.property == "image") {
                        page.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, arguments.callee);
                        runningLock.unlock();
                    }
                });
            }
        }
    }

    public function saveEditWhileRunChanges():void
    {
        var flow:EventFlow = AuthorController.instance.eventFlow;
        if (flow) {
            for each (var page:EventPage in flow.pages) {
                if (page && page.unsavedChanges) {
                    page.updateImage();
                    page.saveEditWhileRunChanges();
                }
            }
        }
    }


    public function rebuildAllEventImages(flow:EventFlow):void
    {
        var list:Vector.<EventPage>;

        // cause FlowView to redraw itself with new image proportions
        var page:EventPage;
        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.STAGE_RESIZED));
        list = new Vector.<EventPage>();
        for each (page in flow.masterPages) {
            if (page.gadget) {      // skip empty pages, so a brand-new project won't flash messages here
                list.push(page);
            }
        }
        for each (page in flow.pages) {
            if (page.gadget) {
                list.push(page);
            }
        }

        message(["Rebuilding all Event images", "since project dimensions changed"]);
        var oldDisableLoadUnload = flow.disableLoadUnload;
        flow.disableLoadUnload = false;
        var token:Object = ApplicationController.instance.disableInput("rebuildAllEventImages");
        var block:PseudoThreadDebugRunnable = new PseudoThreadDebugRunnable("EventFlowController::rebuildAllEventImages (" + list.length + ")", true);
        PseudoThread.add(block);
        var index:int = 0;
        for each (page in list) {
            PseudoThread.add(new PseudoThreadDebugRunnable("EventFlowController::rebuildAllEventImages (" + (index++) + ") [" + page.title + "]"));
            enqueueLoadPage(flow, page);
            PseudoThread.add(new PseudoThreadWaitFrameRunnable());
            enqueueRebuildImage(page);

            PseudoThread.add(new PseudoThreadSimpleRunnable("unloadPages", enqueueUnloadPagesAsNeeded, this, [flow]));
        }
        enqueueFinishRebuild(flow,oldDisableLoadUnload, token);
        PseudoThread.add(block.end);
    }

    private function enqueueRebuildImage(page:EventPage):void
    {
        PseudoThread.add(new PseudoThreadWaitFrameRunnable());
        // use a special version of page.updateimage that doesn't look at page.active
        enqueue(false, "EventFlowController::enqueueRebuildImage", page.updateImageForResizeProject, page);
    }

    private function enqueueFinishRebuild(flow:EventFlow, oldDisableLoadUnload:Boolean, tokenFromDisableInput:Object):void
    {
        enqueue(false, "EventFlowController::enqueueFinishRebuild", function():void{
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.STAGE_RESIZED));
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SAVE_PROJECT));
            flow.disableLoadUnload = oldDisableLoadUnload;
            ApplicationController.instance.enableInput(tokenFromDisableInput);
            message(["Images rebuilt"]);
        });
    }


    override public function assetReplaceAll(obj:AbstractObject, oldAsset:IReplaceable, newAsset:IReplaceable):void
    {
        var flow:EventFlow = obj as EventFlow;
        var obj:AbstractObject;
        if (Application.instance.flowVisible) {
            for each (obj in flow.backstageObjectsInOuterWiringScope()) {
                obj.controller.assetReplaceAll(obj, oldAsset, newAsset);
            }
        } else {
            obj = flow.editingEventPage ? flow.editingEventPage : flow.currentEventPage;
            obj.controller.assetReplaceAll(obj, oldAsset, newAsset);
        }
    }

    override public function assetReplaceAllWhole(obj:AbstractObject, oldAsset:IReplaceable, newAsset:IReplaceable):void
    {
        var flow:EventFlow = obj as EventFlow;
        var obj:AbstractObject;
        if (Application.instance.flowVisible) {
            for each (obj in flow.backstageObjectsInOuterWiringScope()) {
                obj.controller.assetReplaceAllWhole(obj, oldAsset, newAsset);
            }
        } else {
            obj = flow.editingEventPage ? flow.editingEventPage : flow.currentEventPage;
            obj.controller.assetReplaceAllWhole(obj, oldAsset, newAsset);
        }
    }

    private function message(strArray:Array):void
    {
        ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.NOTIFY_USER, strArray));
    }

    override public function propertyNeedsInit(property:String, obj:AbstractObject):Boolean
    {
        switch (property) {
            case "pageNumber":
            case "currentPage":
                return false;
        }
        return super.propertyNeedsInit(property, obj);
    }

    public function dumpLoadStatus():void
    {
        var flow:EventFlow = ApplicationController.instance.authorController.eventFlow;
        var page:EventPage;
        trace("Type: Title Needed Has    loadedCount="+flow.loadedPages.cardinality);
        for each(var master:EventPage in flow.masterPages) {
            trace("Master:", master.title, master.needsInternals(), master.hasInternals);
        }
        for each(var evt:EventPage in flow.pages) {
            trace("Event: ", evt.title, evt.needsInternals(), evt.hasInternals);
        }
        trace("Loaded:  count="+flow.loadedPages.cardinality);
        for each (page in flow.loadedPages.allItems) {
            trace("  ", page.title, "inCache="+flow.pageInCache(page), "hasInternals="+page.hasInternals, "preventUnload="+page.preventUnload, "parent="+(page.parent!=null));
        }
        trace("Cache:  count="+flow.pageCache.length);
        for each (page in flow.pageCache.allItems) {
            trace("  ", page.title, "hasInternals="+page.hasInternals, "preventUnload="+page.preventUnload, "parent="+(page.parent!=null));
        }

        EventFlowController.instance.dumpOverlayImage(flow);
        trace(".");
    }


    private function assert(condition:*):void
    {
        if (condition==null || condition==false) {
            throw new Error("EventFlowController");
        }
    }
}
}
