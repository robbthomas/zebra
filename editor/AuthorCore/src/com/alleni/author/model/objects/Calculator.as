package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectProperties;
	import com.alleni.savana.Ast;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;
	
	public class Calculator extends LogicObject
	{		
		public static const TACONITE_COLLECTIONS:Vector.<String> = Vector.<String>([
			"calcInputValues",
			"calcInputDefaultNames",
			"calcInputNames",
			"calcInputTypes",
			"calcInputConstraints",

			"calcOutputValues",
			"calcOutputExpressions",
			"calcOutputAsts",
			"calcOutputTypes",
			"calcOutputConstraints"
		]);

		[Bindable] public var calcInputValues:IList = new ArrayCollection();
		[Bindable] public var calcInputDefaultNames:IList = new ArrayCollection();
		[Bindable] public var calcInputNames:IList = new ArrayCollection();
		[Bindable] public var calcInputTypes:IList = new ArrayCollection();
		[Bindable] public var calcInputConstraints:IList = new ArrayCollection();

		[Bindable] public var calcOutputValues:IList = new ArrayCollection();
		[Bindable] public var calcOutputExpressions:IList = new ArrayCollection();
		[Transient] public var calcOutputAsts:IList = new ArrayCollection();
		[Bindable] public var calcOutputTypes:IList = new ArrayCollection();
		[Bindable] public var calcOutputConstraints:IList = new ArrayCollection();

		[Transient] public var rearranging:Boolean = false;

		[Bindable] public var continuallyJudging:Boolean = true;
		

		public function getInputName(row:int):String {
			var result:String = calcInputNames[row];
			if(result == null || result == "") {
				result = calcInputDefaultNames[row];
			}
			return result;
		}

		public function getInputDescription(row:int):PropertyDescription {
			if(calcInputTypes[row] == null) {
				return new PropertyDescription("", "", "", "", "", 0, false, -1, Modifiers.STRING_TYPE, null, true);
			} else {
				return new PropertyDescription("", "", "", "", "", 0, false, -1, calcInputTypes[row], calcInputConstraints[row], true);
			}
		}

		public function getOutputDescription(row:int):PropertyDescription {
			if(calcOutputTypes[row] == null) {
				return new PropertyDescription("", "", "", "", "", 0, false, -1, Modifiers.STRING_TYPE, null, false);
			} else {
				return new PropertyDescription("", "", "", "", "", 0, false, -1, calcOutputTypes[row], getOutputConstraint(row), false);
			}
		}

		private function getOutputConstraint(row:int):Object
		{
			if (row < calcOutputConstraints.length)
				return calcOutputConstraints[row];
			else
				return null;
		}

		public function getDefaultEnvironment():Object  {
			var result:Object = Ast.defaultEnvironment();
			for(var i:int=0; i<calcInputValues.length; i++) {
				result[String(calcInputNames[i]).toLowerCase()] = calcInputValues[i];
				result[String.fromCharCode("a".charCodeAt()+i)] = calcInputValues[i];
			}
			return result;
		}

		public function getDefaultTypeEnvironment():Object {
			var result:Object = Ast.defaultTypeEnvironment();
			for(var i:int=0; i<calcInputValues.length; i++) {
				result[calcInputNames[i].toLowerCase()] = calcInputTypes[i];
				result[String.fromCharCode("a".charCodeAt()+i)] = calcInputTypes[i];
			}
			return result;
		}


		[Bindable]  public var localVariables:IList = new ArrayCollection(); 		// A set of expressions
		
		[Bindable]  public var expressions:IList = new ArrayCollection(); 			// A set of expressions
		[Bindable]  public var expressionValues:IList = new ArrayCollection(); 		// Values for each step in the calculation
		[Transient] public var expressionAst:IList = new ArrayCollection(); 		// AST for each expression
		[Bindable]  public var expressionBound:IList = new ArrayCollection(); 		// Boolean that dictates whether we currently care about the expression
		
		public function Calculator()
		{
			super();
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("calcInputValues"), "calcInputValues");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("calcOutputValues"), "calcOutputValues");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("continuallyJudging"), "continuallyJudging");
			
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("judgeNow"));

		}
	}
}