package com.alleni.author.model.objects
{
	import com.alleni.taconite.model.TaconiteModel;
	
	public interface IListable
	{
		function get name():String;
		function set name(value:String):void;
		function get id():String;
		function get type():int;
		function get content():Object;
		function get model():TaconiteModel;
	}
}