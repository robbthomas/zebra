package com.alleni.author.model.objects {
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.ObjectProperties;

public class StatsConnection extends AbstractConnectionObject {

	[Bindable]
	public var statistic:String

	public function StatsConnection() {
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("statistic"), "statistic");
	}
}
}
