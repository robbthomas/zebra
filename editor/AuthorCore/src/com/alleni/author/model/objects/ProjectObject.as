package com.alleni.author.model.objects {
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.IPage;
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.OutletDescription;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.event.AnchorEvent;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ObjectInlets;
import com.alleni.author.model.ObjectOutlets;
import com.alleni.author.model.ObjectProperties;
import com.alleni.author.model.World;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.service.users.UserService;

import mx.collections.ArrayCollection;

import mx.collections.IViewCursor;

import mx.events.PropertyChangeEvent;

public class ProjectObject extends AbstractObject {

    [Bindable]
    public var onNextOpen:String = "restart";

    public function ProjectObject() {
        super(false); // don't setup any AO properties
        ObjectInlets.instance.registerInlet(this, new InletDescription("restart", "", "Restart", "Initialize values and run from start", Modifiers.DO, 1));
        ObjectInlets.instance.registerInlet(this, new InletDescription("quit", "", "Quit", "Quit this App", Modifiers.DO, 2));

        ObjectProperties.instance.registerProperty(this, new PropertyDescription("stageWidth", "", "Stage width", "Stage width \n(change in Project Settings)", Modifiers.GEOMETRY, 4, false, -1, Modifiers.NUMBER_TYPE, null, false), "stageWidth");
		ObjectProperties.instance.registerProperty(this, new PropertyDescription("stageHeight", "", "Stage height", "Stage height \n(change in Project Settings)", Modifiers.GEOMETRY, 5, false, -1, Modifiers.NUMBER_TYPE, null, false), "stageHeight");

		ObjectOutlets.instance.registerOutlet(this, new OutletDescription("onRun", "", "On-run", "Resources are loaded and run starts \n(this triggers on initial run, not on restart)", Modifiers.STATES, 1));

        updateProjectMC(Application.instance.document.project.accountTypeId, false);

        title = "Project";
    }

    public function updateProjectMC(accountTypeId:Number, createInitial:Boolean = true):void{
        if(accountTypeId >= 3) {
            if(this.anchors["onNextOpen"] != null){
                return;
            }
//            trace("############## Add Ribbons");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("onNextOpen", "", "On next open", "When user reopens this project, where should it start?\nRestart = at beginning \nResume = exactly as it was left \nUser Choice = Restart or Resume", Modifiers.VALUE, 1, false, -1, Modifiers.ENUMERATED_TYPE, [{label:"Restart", data:"restart"},{label:"Resume", data:"resume"},{label:"User Choice", data:"menu"}]), "onNextOpen");
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["onNextOpen"], false));
            // these properties are listed in ProjectObjectController.propertyNeedsInit() to prevent inits in the ProjectObject
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("pageNumber", "", "Event- current #", "Number of the event currently displayed", Modifiers.VALUE, 5, false, -1, Modifiers.INT_TYPE), "pageNumber");
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["pageNumber"], false));
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("currentPage", "", "Event- current", "Event currently displayed", Modifiers.VALUE, 4, false, -1, Modifiers.EVENTPAGE_TYPE), "currentPage");
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["currentPage"], false));
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("initialPageNumber", "", "Event- start #", "The number of the start event", Modifiers.VALUE,3, false, -1, Modifiers.INT_TYPE), "initialPageNumber");
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["initialPageNumber"], false));
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("initialPage", "", "Event- start", "On Run, project will start with this event.\n(to define, click value, then click an event in list)", Modifiers.VALUE, 2, false, -1, Modifiers.EVENTPAGE_TYPE), "initialPage");
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["initialPage"], false));
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("numberOfEvents", "", "Events- #", "Number of events in the project", Modifiers.VALUE, 6, false, -1,  Modifiers.INT_TYPE, null, false), "numberOfEvents");
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["numberOfEvents"], false));

            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("projectTransition"), "projectTransition");  // similar to "transition" ribbon, but fewer choices
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["projectTransition"], false));

            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("transTweenType"), "transTweenType");
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["transTweenType"], false));
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("transitionSecs"), "transitionSecs");
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["transitionSecs"], false));
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("nextEventExists","",	"Event- next?", 		"Is there a next event? \n(common use: wire to a Next button to activate)", 	Modifiers.VALUE, 	9, false, -1, 	Modifiers.BOOLEAN_TYPE, null, false),"nextEventExists");
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["nextEventExists"], false));
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("prevEventExists","",	"Event- prev?", 		"Is there a previous event? \n(common use: wire to a Previous or Back button to activate)", 	Modifiers.VALUE, 	10, false, -1, 	Modifiers.BOOLEAN_TYPE, null, false),"prevEventExists");
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["prevEventExists"], false));

            ObjectInlets.instance.registerInlet(this, new InletDescription("goStartPage","","Go- first event", "Go to first event in flow", Modifiers.DO, 3));
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["goStartPage"], false));
            ObjectInlets.instance.registerInlet(this, new InletDescription("goNextPage","","Go- next event", "Go to next event in flow", Modifiers.DO, 4));
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["goNextPage"], false));
            ObjectInlets.instance.registerInlet(this, new InletDescription("goPrevPage","","Go- prev event", "Go to previous event in flow", Modifiers.DO, 5));
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["goPrevPage"], false));
            ObjectInlets.instance.registerInlet(this, new InletDescription("goEndPage","","Go- last event", "Go to last event in flow", Modifiers.DO, 6));
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["goEndPage"], false));
            ObjectInlets.instance.registerInlet(this, new InletDescription("goBack","","Go- back", "Go to previously visited event", Modifiers.DO, 8));
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["goBack"], false));
            ObjectInlets.instance.registerInlet(this, new InletDescription("goForward","","Go- forward", "Go to next visited event", Modifiers.DO, 7));
            dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this.anchors["goForward"], false));

            if(createInitial){
                controller.createInitialRibbons(this);
            }
        }
    }

    override public function onResume():void {
        super.onResume();
        WireAnchor(anchors["onRun"]).trigger();
    }

    private function get world():World
    {
        if(parent is World) {
            return parent as World;
        } else {
            return Application.instance.document.root.value as World;
        }
    }

    [Transient]
    [Bindable]
    public function get numberOfEvents():int{
        return world.eventFlow.pageCount;
    }

    [Transient]
    [Bindable]
    public function get stageWidth():Number {
        return world.width;
    }

    public function set stageWidth(value:Number):void {
        world.height = value;
    }

    [Transient]
    [Bindable]
    public function get stageHeight():Number {
        return world.height;
    }

    public function set stageHeight(value:Number):void {
        world.height = value;
    }

    [Transient]
    [Bindable]
    public function get nextEventExists():Boolean {
        if(!world.eventFlow) {
            return false;
        } else {
            return (pageNumber < world.eventFlow.pages.length);
        }
    }

    [Transient]
    [Bindable]
    public function get prevEventExists():Boolean {
        if(!world.eventFlow) {
            return false;
        } else {
            return (pageNumber > 1);
        }
    }

    [Transient]
    [Bindable]
    public function get pageNumber():Number {
        if(!world.eventFlow) {
            return 1;
        } else if (world.eventFlow.editingEventPage) {
            return world.eventFlow.editingEventPage.pageNumber;
        } else {
            return world.eventFlow.pageNumber;
        }
    }

    public function set pageNumber(value:Number):void {
        if(!world.eventFlow || world.eventFlow.editingEventPage) {
            return;
        }
        world.eventFlow.pageNumber = value;
    }

    [Transient]
    [Bindable]
    public function get currentPage():AbstractObject {
        if(!world.eventFlow) {
            return null;
        } else if (world.eventFlow.editingEventPage) {
            return world.eventFlow.editingEventPage;
        } else {
            return world.eventFlow.currentEventPage;
        }
    }

    public function set currentPage(value:AbstractObject):void {
        if(!world.eventFlow || world.eventFlow.editingEventPage) {
            return;
        }
        var pages:ArrayCollection = world.eventFlow.pages;
        var len:Number = pages.length;
        for(var i:int=0; i<len; i++) {
            if(pages[i] == value) {
                world.eventFlow.pageNumber = i+1;
                return;
            }
        }
    }

    public function pageChanged():void {
        var num:Number = pageNumber;
        dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "pageNumber", num, num));
        var page:AbstractObject = currentPage;
        dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "currentPage", page, page));
        dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "nextEventExists", nextEventExists, nextEventExists));
        dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "prevEventExists", prevEventExists, prevEventExists));
    }


    [Transient]
    [Bindable]
    public function get projectTransition():String {
        if(world.eventFlow) {
            return world.eventFlow.transition;
        } else {
            return "";
        }
    }

    public function set projectTransition(value:String):void {
        if(world.eventFlow) {
            world.eventFlow.transition = value;
        }
    }

    [Transient]
    [Bindable]
    public function get transTweenType():String {
        if(world.eventFlow) {
            return world.eventFlow.transTweenType;
        } else {
            return "";
        }
    }

    public function set transTweenType(value:String):void {
        if(world.eventFlow) {
            world.eventFlow.transTweenType = value;
        }
    }

    [Transient]
    [Bindable]
    public function get transitionSecs():Number {
        if(world.eventFlow) {
            return world.eventFlow.transitionSecs;
        } else {
            return 0;
        }
    }

    public function set transitionSecs(value:Number):void {
        if(world.eventFlow) {
            world.eventFlow.transitionSecs = value;
        }
    }

    public function set initialPageNumber(value:int):void {

        if(world.eventFlow) {
            var oldPageNumber:int = initialPageNumber;
            var oldPage:IPage = initialPage;
            world.eventFlow.initialPageNumber = value;
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "initialPageNumber",oldPageNumber,value));
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "initialPage", oldPage, initialPage));
        }
    }

    public function set initialPage(value:IPage):void {
        if(world.eventFlow) {
            var oldPageNumber:int = initialPageNumber;
            var oldPage:IPage = initialPage;
            world.eventFlow.initialPageNumber = value.pageNumber;
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "initialPageNumber",oldPageNumber,initialPageNumber));
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "initialPage", oldPage, value));
        }
    }
    [Transient]
    [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
    public function get initialPageNumber():int {
        if(world.eventFlow) {
            return world.eventFlow.initialPageNumber;
        } else {
            return 0;
        }
    }
    [Transient]
    [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
    public function get initialPage():IPage {
        if(world.eventFlow) {
            return EventFlowController.instance.pageForPageNumber(world.eventFlow,initialPageNumber);
        } else {
            return null;
        }
    }

}
}
