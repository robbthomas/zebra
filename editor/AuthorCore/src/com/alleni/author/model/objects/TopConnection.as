package com.alleni.author.model.objects {
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ObjectProperties;

public class TopConnection extends AbstractConnectionObject {

	[Bindable]
	public var descending:Boolean = false;
	[Bindable]
	public var resultCount:Number = 1;

	public function TopConnection() {
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("descending"), "descending");
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("resultCount"), "resultCount");
	}
}
}
