/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.model.objects
{
import com.alleni.author.definition.AbstractModifierDescription;
import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectOutlets;
	import com.alleni.author.model.ObjectProperties;
	import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.UsageCounts;

import flash.geom.Point;
	
	import flashx.textLayout.formats.TextAlign;
	import flashx.textLayout.formats.VerticalAlign;

	public class ToggleButton extends Text
	{	
		public static const DEFAULT_TEXT:String = "Untitled";
        public static const DEFAULT_VALUE_PREPEND:String = "B";
		
		[Bindable] public var toggleButtonNormalUp:Asset;
		[Bindable] public var toggleButtonNormalDown:Asset;
		[Bindable] public var toggleButtonNormalOver:Asset;
		[Bindable] public var toggleButtonNormalDisabled:Asset;
		[Bindable] public var toggleButtonCheckedUp:Asset;
		[Bindable] public var toggleButtonCheckedDown:Asset;
		[Bindable] public var toggleButtonCheckedOver:Asset;
		[Bindable] public var toggleButtonCheckedDisabled:Asset;

        private static const ASSET_NAMES:Array = [
            "toggleButtonNormalUp",
            "toggleButtonNormalDown",
            "toggleButtonNormalOver",
            "toggleButtonNormalDisabled",
            "toggleButtonCheckedUp",
            "toggleButtonCheckedDown",
            "toggleButtonCheckedOver",
            "toggleButtonCheckedDisabled" ];

		
		[Bindable] public var bitmap9Slicing:Boolean = false;
		
		[Bindable] public var momentary:Boolean = true;  /* true if the button acts as a toggle, false if the button doesnt act as a toggle */

        [Bindable][Transient]public var down:Boolean = false;  /* true if the toggle is "on", false if the toggle is "off" */
		[Bindable][Transient]public var over:Boolean = false;

        [Bindable] public var checked:Boolean = false;
		[Bindable] public var labelPadding:Number = 10;
		[Bindable] public var labelPosition:Point;
		[Bindable] public var textWidth:Number = 200;
		[Bindable] public var labelHeight:Number = 0;

		[Bindable] public var checkColor:uint = 0x000000;
		[Bindable] public var checkOpacity:Number = 100;

		//[Bindable] public var labelDown:String = "";
		[Bindable] public var cornerRadius:Number = 15;
		
		[Bindable]
		public function get downColor():GraphicFill
		{
			// to make changes to fill, client MUST create a new GraphicFill and assign it to obj.fillSpec.    
			// it will not work to say:  obj.color.fillSpec.color = xxx; 
			return _downColor.clone();
		}
		public function set downColor(value:GraphicFill):void
		{
			_downColor = value;
		}
		
		[Bindable]
		public function get labelDown():String
		{
			return _labelDown;
		}
		public function set labelDown(value:String):void
		{
			_labelDown = value;
		}
		
		[Transient][Bindable]
		public function get editingText():Boolean
		{
			return _editingText;
		}
		public function set editingText(value:Boolean):void
		{
			_editingText= value;
		}
		
		[Bindable] public var widthScale:Number = 1;   // listed in ObjectController.propertyNeedsInit()
		[Bindable] public var heightScale:Number = 1;   // listed in ObjectController.propertyNeedsInit()
		[Bindable] public var buttonFit:Boolean = false;
		
		[Bindable] public var toggleButtonGroup:ToggleButtonGroup;
		[Bindable] public var toggleButtonValue:String = "";
        [Bindable] public var toggleButtonGroupNullValue:String = "";
		[Bindable] public var toggleButtonGroupValue:String = ""; // needs to be null to handle no value being set on the buttons

		[Transient][Bindable] public var isGraphicMode:Boolean = false;
		[Transient][Bindable] public var inClickCycle:Boolean = false;
		[Transient][Bindable] public var showingGroupIndicator:Boolean = false;
		[Transient][Bindable] public var assetsLoaded:Number = 0;
		
		private var _downColor:GraphicFill = new GraphicFill('J[7094b8,e0edfc]');
		private var _labelDown:String = "";
		private var _editingText:Boolean = false;
		
		/**
		 * Constructor for the Toggle Button Model
		 * @param setupProperties
		 * 
		 */
		public function ToggleButton()
		{
			label = DEFAULT_TEXT;
			lineThickness = 0;
			fontSize = 14;
			textAlignH = TextAlign.CENTER;
			textAlignV = VerticalAlign.MIDDLE;
			fillAlpha = 100;
			width = 75;
			height = 30;
			anchorPoint = new Point(0,0);
			labelPosition = new Point(1,.5); //center right
			movable = false;
            enclosedActsLikeRunning = true;

            var _fillSpec:GraphicFill = new GraphicFill('J[7094b8,e0edfc]');

			fillSpec = _fillSpec;
			
			// register inlets for this object
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("press"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("toggle"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("check"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("uncheck"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("enableButton"));
            toolTipOverrides["enableButton"] = "Allow this push button to be pressed";
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("disableButton"));
            toolTipOverrides["disableButton"] = 'Do not allow this push button to be pressed \n(deactivates "Triggers: Button press" - other ribbons remain active) \n(set opacity in "Visibility: Opacity- pwr off")';
			
			// outlets
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("clickStart"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("clickRelease"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("checkedBox"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("uncheckedBox"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("buttonPressed"));
			
			// register properties for this object
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("momentary"), "momentary");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("labelDown"), "labelDown");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("checked"), 	"checked");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("checkColor"), "checkColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("checkOpacity"), "checkOpacity");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("cornerRadius"), "cornerRadius");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("downColor"), "downColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("labelPadding"), "labelPadding");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("labelPosition"), "labelPosition");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("textWidth"), "textWidth");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("bitmap9Slicing"), "bitmap9Slicing");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("buttonFit"), "buttonFit");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("disabledAlpha"), "disabledAlpha");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("interactive"), "interactive");
            toolTipOverrides["interactive"] = 'Can this push button be pressed?\n(affects "Triggers: Button press" - other ribbons remain active)';


            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonNormalUp"), 			"toggleButtonNormalUp");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonNormalDown"), 		"toggleButtonNormalDown");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonNormalOver"), 		"toggleButtonNormalOver");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonNormalDisabled"), 	"toggleButtonNormalDisabled");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonCheckedUp"), 		"toggleButtonCheckedUp");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonCheckedDown"), 		"toggleButtonCheckedDown");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonCheckedOver"), 		"toggleButtonCheckedOver");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonCheckedDisabled"), 	"toggleButtonCheckedDisabled");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonValue"), 			"toggleButtonValue");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonGroupValue"), 		"toggleButtonGroupValue");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("toggleButtonGroupNullValue"), 		"toggleButtonGroupNullValue");
        }

        override public function getUsageCounts(usages:UsageCounts):void
        {
            super.getUsageCounts(usages);
            for each (var key:String in ASSET_NAMES) {
                var json:Object = initialValues[key];
                usages.countAssetJson(json);
            }
        }
		
	}
}