/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/8/11
 * Time: 10:57 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.objects {
import com.alleni.author.definition.DataReceivedEvent;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ObjectInlets;
import com.alleni.author.model.ObjectOutlets;
import com.alleni.author.model.ObjectProperties;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.service.CometService;
import com.alleni.taconite.persistence.json.JSON;

import flash.utils.Dictionary;

public class BroadcastConnection  extends AbstractObject {


	private var _local:Boolean = false;

	private var _key:String = "";

	private var _subscribed:Boolean = false;

	[Bindable]
	public var sendData:String = "";
	[Bindable]
	public var receiveData:String = "";

	protected static var broadcastListeners:Dictionary = new Dictionary(true);

	public function BroadcastConnection() {
		modifiers = new Dictionary();
		anchors = new Dictionary();

		hideOnRun = true;
		ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("send"));
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("key"), "key");
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("sendData"), "sendData");
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("receiveData"), "receiveData");
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("local"), "local");
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("subscribed"), "subscribed");
		ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("dataReceived"));
	}

	[Bindable]
	public function get subscribed():Boolean {
		return _subscribed;
	}

	public function set subscribed(value:Boolean):void {
		_subscribed = value;
		if(_subscribed) {
			subscribe();
		} else {
			unsubscribe();
		}
	}

	[Bindable]
	public function get key():String {
		return _key;
	}

	public function set key(value:String):void {
		if(_subscribed) {
			unsubscribe();
		}
		_key = value;
		if(_subscribed) {
			subscribe();
		}
	}

	[Bindable]
	public function get local():Boolean {
		return _local;
	}

	public function set local(value:Boolean):void {
		if(_subscribed) {
			unsubscribe();
		}
		_local = value;
		if(_subscribed) {
			subscribe();
		}
	}

	public function send():void {
		if(_local) {
			for(var listener:Object in broadcastListeners) {
				if(listener is BroadcastConnection) {
					for each(var channel:Object in broadcastListeners[listener]) {
						if(channel == _key) {
							BroadcastConnection(listener).handleDataReceived(new DataReceivedEvent(_key, com.alleni.taconite.persistence.json.JSON.encode(sendData)));
						}
					}
				}
			}
		} else {
			CometService.instance.publish(CometService.getChannel(_key, true), com.alleni.taconite.persistence.json.JSON.encode(sendData));
		}
	}

	public function subscribe():void {
		if(_local) {
			if(this in broadcastListeners == false) {
				broadcastListeners[this] = [];
			}
			broadcastListeners[this].push(_key);
		} else {
			CometService.instance.addEventListener(CometService.getChannel(_key, true), handleDataReceived);
		}
	}

	public function unsubscribe():void {
		if(_local) {
			if(this in broadcastListeners) {
				var arr:Array = broadcastListeners[this];
				var idx:int = arr.indexOf(_key);
				if(idx >= 0) {
					arr.splice(idx, 1);
				}
			}
		} else {
			CometService.instance.removeEventListener(CometService.getChannel(_key, true), handleDataReceived);
		}
	}

	private function handleDataReceived(event:DataReceivedEvent):void {
		receiveData = com.alleni.taconite.persistence.json.JSON.decode(event.data as String) as String;
		WireAnchor(anchors["dataReceived"]).trigger();
	}
}
}
