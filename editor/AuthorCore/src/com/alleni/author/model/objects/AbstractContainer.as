/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 */
package com.alleni.author.model.objects
{
    import com.alleni.author.Navigation.Pager;
    import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ObjectInlets;
import com.alleni.taconite.model.ITaconiteCollection;
	import com.alleni.taconite.model.TaconiteModel;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;

	/**
	 * An abstract base class for containers (Arena, Flipbook)
	 */
	public class AbstractContainer extends NativeVisualObject implements ITaconiteCollection
	{	
		[Transient]
		public static const TACONITE_CHILDREN_PROPERTY:String = "objects";

        private var _accepting:Boolean = true;
        private var _releasing:Boolean = true;


		[Transient]
		public function get children():IList
		{
			return objects;
		}

		public function AbstractContainer(setupProperties:Boolean=true)
		{
			super(setupProperties);

            if(!setupProperties) {
                return;
            }
			
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("randomizeZIndex"));
		}

        [Bindable]
        public function get accepting():Boolean
        {
            return _accepting;
        }

        public function set accepting(value:Boolean):void
        {
            _accepting = value;
        }

        [Bindable]
        public function get releasing():Boolean
        {
            return _releasing;
        }

        public function set releasing(value:Boolean):void
        {
            _releasing = value;
        }


		[Bindable]
		private var _objects:IList = new ArrayCollection();
		
		[Transient]
        [Bindable]
        public function get objects():IList
		{
			return _objects;
		}  /* objects in the container */
		
		public function set objects(value:IList):void
		{
			_objects = value;
		}

		[Transient] [Bindable] public var doingLayerReorder:Boolean = false;
		
		public function findObjByUID(uid:String):AbstractObject {
            var candidate:AbstractObject;
			if(this.uid == uid) {
				return this;
			}
			var queue:Array = [this];
			while(queue.length > 0) {
				var parent:AbstractContainer = queue.shift();
                for each(candidate in parent.objects) {
                    if(candidate.uid == uid) {
                        return candidate
                    } else if(candidate is AbstractContainer) {
                        queue.push(candidate);
                    }
                }
                var lists:Vector.<IList> = parent.model.getAllValueModelLists();
                for each (var list:IList in lists) {
                    for each(candidate in list) {
                        if(candidate.uid == uid) {
                            return candidate
                        } else if(candidate is AbstractContainer) {
                            queue.push(candidate);
                        }
                    }
                }
                for each(var candidateModel:TaconiteModel in parent.model.getAllValueModels()) {
                    candidate = candidateModel.value as AbstractObject;
                    if(candidate.uid == uid) {
                        return candidate;
                    } else if(candidate is AbstractContainer) {
                        queue.push(candidate);
                    }
                }
			}
			return null;
		}

        public function findObjByRef(reference:Object):AbstractObject {
            if(reference != null && "type" in reference && reference.type == "AbstractObjectReference" && reference.id is String) {
                return findObjByUID(reference.id);
            }
            return null;
        }
		

		public function selectableModels(includeLocked:Boolean):Vector.<TaconiteModel> {
			var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
			for each(var child:AbstractObject in _objects) {
				if((includeLocked || !child.locked) && (child.showAll || child.visible)) {
					models.push(child.model);
				}
			}
			return models;
		}
		
		[Transient]
		public function modelsForSelectAll(includeLocked:Boolean):Vector.<TaconiteModel>
		{
			// when an Arena is selected and its the only thing selected, and its not empty, then select all children on current page
			var selection:Vector.<TaconiteModel> = ApplicationController.instance.authorController.selection.selectedModels;
			if (selection.length == 1) {
				var arena:Arena = selection[0].value as Arena;
				if (arena && arena.objCount > 0) {
					return arena.currentArenaPage.selectableModels(includeLocked);
				}
			}
			return selectableModels(includeLocked);
		}

		public function setChildrenZPositions(setInitialValues:Boolean=false):void {
//			trace (">>Setting initial z Index on all children of " + title, uid)
			for(var i:int=0; i<_objects.length; i++) {
				var child:AbstractObject = _objects[i];
                child.zIndex = i;
                if (setInitialValues) {
                    child.initialValues.zIndex = i;
                }
//				trace(">>  Setting initial Z on", child.title, "to", child.initialValues.parent, child.initialValues.zIndex);
			}
		}

        override public function onPause():void {
            super.onPause();
            for each(var child:AbstractObject in _objects) {
                child.onPause();
            }
            var lists:Vector.<IList> = model.getAllValueModelLists();
            for each (var list:IList in lists) {
                for each(var obj:AbstractObject in list) {
                    obj.onPause();
                }
            }
            for each(var m:TaconiteModel in model.getAllValueModels()) {
                AbstractObject(m.value).onPause();
            }
        }

        override public function onRestoreInitial():void {
            super.onRestoreInitial();
            for each(var child:AbstractObject in _objects) {
                child.onRestoreInitial();
            }
            var lists:Vector.<IList> = model.getAllValueModelLists();
            for each (var list:IList in lists) {
                for each(var obj:AbstractObject in list) {
                    obj.onRestoreInitial();
                }
            }
            for each(var m:TaconiteModel in model.getAllValueModels()) {
                AbstractObject(m.value).onRestoreInitial();
            }
        }

        override public function onResume():void {
            super.onResume();
            var valueModelLists:Vector.<IList> = model.getAllValueModelLists();
            var valueModels:Vector.<TaconiteModel> = model.getAllValueModels();
            var m:TaconiteModel;
            var list:IList;
            var obj:AbstractObject;
            for each(obj in _objects) {
                obj.onResume();
            }
            for each (list in valueModelLists) {
                for each(obj in list) {
                    obj.onResume();
                }
            }
            for each(m in valueModels) {
                AbstractObject(m.value).onResume();
            }
        }

        /**
         * Set z position of "child" based on "index" in this container.
         * @param child = obj to be positioned, with obj.layer defining the layer.
         * @param index = relative to the whole container, not the layer.
         */
        public function setChildIndexWithinLayer(child:AbstractObject, index:int):void
        {
            index = Math.max(this.lowerEndOfLayer(child), Math.min(upperEndOfLayer(child), index));
            child.parent.model.setChildIndex(child, index);
        }


        /**
         * Compute the destination indexes for reordering a set of objects, which may be in various containers and
         * may or may not be contiguous.  Layering is preserved (obj.layer).
         * Destination-index means that, for example, bringFront will give a different index for each moved object.
         * @param op = BRING_FRONT or other defined code.
         * @param models = the select set sorted back-to-front and without child redundancies.
         * @return
         */
        public static function computeReorderIndexes(op:String, models:Vector.<TaconiteModel>):Vector.<int>
        {
            var result:Vector.<int> = new <int>[];
            for (var n:int=0; n < models.length; n++) {
                var m:TaconiteModel = models[n];
                var obj:AbstractObject = m.value as AbstractObject;
                result[n] = computeOneReorderIndex(m.value as AbstractObject, op, models);
            }
            return result;
        }

        private static function computeOneReorderIndex(obj:AbstractObject, op:String, models:Vector.<TaconiteModel>):int
        {
            var container:AbstractContainer = obj.parent;
            var index:int = container.objects.getItemIndex(obj);
            switch (op) {
                case NotificationNamesEditor.BRING_TO_FRONT:
                    return index + vacancies(index, container.upperEndOfLayer(obj), container, models);
                case NotificationNamesEditor.BRING_FORWARD:
                    return (vacancies(index, container.upperEndOfLayer(obj), container, models) > 0) ? index+1 : index;
                case NotificationNamesEditor.SEND_BACK:
                    return (vacancies(container.lowerEndOfLayer(obj), index, container, models) > 0) ? index-1 : index;
                case NotificationNamesEditor.SEND_TO_BACK:
                    return index - vacancies(container.lowerEndOfLayer(obj), index, container, models);
            }
            throw new Error("computeOneReorderIndex");
        }

        private static function vacancies(lower:int, upper:int, container:AbstractContainer, models:Vector.<TaconiteModel>):int
        {
            // a vacancy is an index value where the object is not selected
            var result:int = 0, n:int, ao:AbstractObject;
            for (n = lower; n <= upper; n++) {
                ao = container.objects.getItemAt(n) as AbstractObject;
                if (models.indexOf(ao.model) < 0) {  // ao not in the selection, so its a vacancy
                    ++result;
                }
            }
            return result;
        }

        private function lowerEndOfLayer(obj:AbstractObject):int
        {
            var count:int = this.objects.length, incr:int = 0;
            for (var n:int = 0; n < count; n++) {
                var ao:AbstractObject = this.objects.getItemAt(n) as AbstractObject;
                if (ao == obj) {
                    incr = -1;  // possibly "obj" is out of place, so treat it as if it were at lower end of layer
                } else if (ao.layer >= obj.layer) {
                    return n + incr;
                }
            }
            return count-1;
        }

        private function upperEndOfLayer(obj:AbstractObject):int
        {
            var count:int = this.objects.length, incr:int = 0;
            for (var n:int = count-1; n > 0; n--) {
                var ao:AbstractObject = this.objects.getItemAt(n) as AbstractObject;
                if (ao == obj) {
                    incr = +1;  // possibly "obj" is out of place, so treat it as if it were at upper end of layer
                } else if (ao.layer <= obj.layer) {
                    return n + incr;
                }
            }
            return 0;
        }


        public function debugContent(properties:String = null):Object {
            var result:Object = new Object();
            var index:String;
            var child:AbstractObject;
            var resultChildren:Object;
            var container:AbstractContainer;
            var childStr:String;
            var childObj:Object;
            var props:Array = null;

            if (properties != null) {
                props = properties.split(",");
            }

            if (children.length > 0) {
                resultChildren = {};
                for (index in _objects) {
                    child = _objects[index];
                    childStr = childObjToString(index, child, props);
                    childObj = {}; // unless child is a container
                    container = child as AbstractContainer;
                    if (container) {
                        childObj = container.debugContent(properties);
                    }
                    resultChildren[childStr] = childObj;
                }
                result.children = resultChildren;
            }
            var lists:Vector.<String> = model.getAllValueListProperties();
            for each (var listName:String in lists) {
                var list:IList = this[listName] as IList;
                if (list && list.length > 0) {
                    resultChildren = {};
                    for (index in list) {
                        child = list[index];
                        childStr = childObjToString(index, child, props);
                        childObj = null; // unless child is a container
                        container = child as AbstractContainer;
                        if (container) {
                            childObj = container.debugContent(properties);
                        }
                        resultChildren[childStr] = childObj;
                    }
                    result[listName] = resultChildren;
                }
            }
            return result;
         }

        private function childObjToString(prop:String, obj:AbstractObject, props:Array):String
        {
            var result:String = prop + ": " + obj.shortClassName;
            if (props != null) {
                for each (var propName:String in props) {
                    if (propName in obj) {
                        var propValue:* = obj[propName];
                        if (propValue is String) {
                            result += " " + propName + '="' + propValue + '"';
                        } else {
                            result += " " + propName + "=" + propValue;
                        }
                    }
                }
            }

            return result;
        }

    }
}
