/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/5/11
 * Time: 9:16 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.objects {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.document.Document;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ObjectInlets;
import com.alleni.author.model.ObjectOutlets;
import com.alleni.author.model.ObjectProperties;

import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.service.BorealOperation;
import com.alleni.taconite.persistence.json.JSON;
import com.alleni.taconite.service.RestHttpOperation;

import flash.events.Event;
import flash.utils.Dictionary;

public class QueueConnection extends AbstractObject {

	[Bindable]
	public var local:Boolean = false;
	[Bindable]
	public var key:String = "";
	[Bindable]
	public var sendData:String = "";
	[Bindable]
	public var receiveData:String = "";

	protected static var queueData:Dictionary = new Dictionary();

	public function QueueConnection() {
		modifiers = new Dictionary();
		anchors = new Dictionary();

		hideOnRun = true;
		ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("send"));
		ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("receive"));
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("key"), "key");
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("sendData"), "sendData");
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("receiveData"), "receiveData");
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("local"), "local");
		ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("dataReceived"));
	}

	public function send():void {
		if(local) {
			if(key in queueData == false) {
				queueData[key] = [];
			}
			queueData[key].push(sendData);
		} else {
			var projectId:String = Document(ApplicationController.instance.authorController.document).documentId;
			var operation:RestHttpOperation = new RestHttpOperation("p2p/queue/"+projectId + "/" + key, {data:sendData}, RestHttpOperation.POST);
			operation.displayName = "Adding to Queue";
			BorealOperation.addListeners(operation, handleSent);
			operation.execute();
		}
	}

	private function handleSent(event:Event):void {
		var operation:RestHttpOperation = event.target as RestHttpOperation;
		if(operation.resultDecoded == null || operation.resultDecoded == "") {
			return;
		}
		var response:Object = operation.resultDecoded;
		if(response.responseCode == "QUEUE_FULL") {
			// handle full maybe retry
		} else if(response.responseCode == "OK") {
			// all ok
		} else {
			// handle unknown error
		}
	}

	public function receive():void {
		if(local) {
			if(key in queueData == false || queueData[key].length == 0) {
				return;
			}
			receiveData = queueData[key].shift();
			WireAnchor(anchors["dataReceived"]).trigger();
		} else {
			var projectId:String = Document(ApplicationController.instance.authorController.document).documentId;
			var operation:RestHttpOperation = new RestHttpOperation("p2p/queue/"+projectId + "/" + key, null, RestHttpOperation.GET);
			operation.displayName = "Retrieving from Queue";
			BorealOperation.addListeners(operation, handleReceived);
			operation.execute();
		}
	}

	private function handleReceived(event:Event):void {
		var operation:RestHttpOperation = event.target as RestHttpOperation;
		if(operation.resultDecoded == null || operation.resultDecoded == "") {
			return;
		}
		var response:Object = operation.resultDecoded;
		if(response.responseCode == "QUEUE_EMPTY") {
			// handle empty maybe retry
		} else if(response.responseCode == "OK") {
			receiveData = response.data;
			WireAnchor(anchors["dataReceived"]).trigger();
		} else {
			// handle unknown error
		}
	}
}
}
