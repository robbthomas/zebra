package com.alleni.author.model.objects
{
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.objects.IReplaceableReceiver;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.UsageCounts;

public class Composite extends SmartObject implements IReplaceableReceiver
	{
		[Transient]
		public static const TACONITE_CHILDREN_PROPERTY:String = "objects";


    [Transient] override public function get scaleX():Number
    {
        if(viewBox && viewBox.width != 0) {
            return width / viewBox.width;
        } else {
            return 1;
        }
    }

    [Transient] override public function get scaleY():Number
    {
        if(viewBox && viewBox.height != 0) {
            return height / viewBox.height;
        } else {
            return 1;
        }
    }


    public function acceptsReplaceableDrop(newReplaceable:IReplaceable):Boolean
    {
        return false;  // composite can only be replaced by whole-object replacement (eg. to create setter ribbons specified by new gadget)
    }

    [Transient]
    public function get replaceable():IReplaceable
    {
        return gadget;
    }
    public function set replaceable(value:IReplaceable):void
    {

    }

    override public function getUsageCounts(usages:UsageCounts):void
    {
        super.getUsageCounts(usages);
        if (gadget) {
            usages.countGadget(gadget);
        }
    }

}
}
