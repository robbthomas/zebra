package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectProperties;

	public class GotoURL extends LogicObject
	{
		[Bindable] public var theURL:String = "";
		[Bindable] public var queryString:String = "";
		[Bindable] public var jump:Boolean = true;
		[Bindable] public var newWindow:Boolean = true;
		
		public function GotoURL()
		{
			this.hideOnRun = true;
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("theURL"), "theURL");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("queryString"), "queryString");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("newWindow"), "newWindow");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("jump"), "jump");
			
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("navigate"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("postData"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("gotoURL"));
		}
	}
}