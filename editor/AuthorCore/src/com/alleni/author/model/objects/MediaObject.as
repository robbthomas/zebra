package com.alleni.author.model.objects
{
	import com.alleni.author.controller.objects.media.IMediaMediator;
	import com.alleni.author.controller.objects.media.MediaController;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectOutlets;
	import com.alleni.author.model.ObjectProperties;
	import com.alleni.author.model.ui.Application;

	public class MediaObject extends AbstractExternalObject
	{
		[Bindable]
        private var _autorun:Boolean = false;
		
		[Bindable]
		public var volume:Number = 100;
		
		[Bindable]
		[Transient]
		public var buffering:Boolean;
		
		[Bindable]
		[Transient]
		public var fractionLoaded:Number = 0;
		
		[Bindable]
		public var stream:Boolean = false;
		
		[Bindable]
		public var preload:Boolean = false;
		
		[Bindable]
		public var repeater:Boolean = false;
		
		[Bindable]
		public var loop:Boolean = false;
		
		[Bindable]
		public var secondsElapsed:Number = 0;
		
		[Bindable]
		public var secondsRemaining:Number = 0;
		
		[Bindable]
		public var percentElapsed:Number = 0;
		
		[Bindable]
		public var percentRemaining:Number = 100.0;
		
		[Bindable]
		public var duration:Number = 0;
		
		[Bindable]
		public var cuePointList:String = "1:1;3:5"; //"1:1;3:5";
		
		[Bindable]
		public var cuePoint:Number = 0;

        [Bindable]
        public var resumeTicking:Boolean; // When the project is paused what is the state of the media when the project resumes.

        [Bindable]
        public function get autorun():Boolean {
            return _autorun;
        }

        public function set autorun(value:Boolean):void {
            _autorun = value;
        }

        public function MediaObject()
		{
			super();
			
			// unregister
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("reflection"));
			
			// Register
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("volume"), "volume");
			/*
			"preload" is not yet universal to all media types. it only makes sense for audio so far, for repeater sound effects.
			think "bang bang!" for games or "click!" for buttons where the audio must be fully buffered.
			You will find the property registered in the Audio subclass although the property is defined here for maintainability.
			*/
			/*ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("preload"), "preload");*/ 
			//ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("stream"), "stream");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("autorun"), "autorun");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("loop"), "loop");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("uri"), "uri");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("secondsElapsed"), "secondsElapsed");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("secondsRemaining"), "secondsRemaining");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("percentElapsed"), "percentElapsed");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("percentRemaining"), "percentRemaining");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("duration"), "duration");
			
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("start"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("pause"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("togglePause"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("resume"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("restart"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("stop"));
			// record not yet 1mpl3m3nt3d ;)
			//ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("record"));
			
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("finished"));
		}

        override public function onPause():void {
            super.onPause();
            var mediator:IMediaMediator = MediaController(controller).mediatorFor(this);
            if(mediator != null){
                resumeTicking = mediator.playing;
                if (mediator.playing)
                    mediator.pauseResume(false);
            }
        }

        override public function onRestoreInitial():void {
            super.onRestoreInitial();
            var mediator:IMediaMediator = MediaController(controller).mediatorFor(this);
            if(mediator != null){
                mediator.initialize();
            }
        }

        override public function onResume():void {
            super.onResume();
            var mediator:IMediaMediator = MediaController(controller).mediatorFor(this);
            if(mediator != null){
            	resumeTicking = resumeTicking || _autorun;
                if (resumeTicking && active) {
                    mediator.resume(true);
                }
            }
        }
	}
}