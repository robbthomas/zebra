/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: Text.as 3182 2010-01-25 18:34:41Z llecheler $  */

package com.alleni.author.model.objects
{
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.Modifiers;
    import com.alleni.author.definition.VariableStore;
    import com.alleni.author.definition.text.CSSObject;
	import com.alleni.author.definition.text.Fonts;
	import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectProperties;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.model.ui.VariableContainer;
    import com.alleni.author.model.ui.VariableWireAnchor;
	
	import flash.geom.Point;
	import flash.text.engine.Kerning;
	
	import flashx.textLayout.container.ScrollPolicy;
	import flashx.textLayout.formats.TextAlign;
	import flashx.textLayout.formats.VerticalAlign;
	
	import mx.collections.ArrayList;
	
	/**
	 * Value object representing Text in the world. 
	 */
	public class TextObject extends AbstractExternalObject implements VariableContainer
	{
		public var vars:VariableStore = new VariableStore();

		[Transient]
		public var css:CSSObject = new CSSObject();
		
		[Bindable]
		public var vScrollPolicy:String = ScrollPolicy.OFF;
		
		[Bindable]
		public var textLength:Number = -1;
		
		[Bindable]
		public var scrollThumbColor:uint = 0xF5F5F5;
		
		[Bindable]
		public var scrollThumbOverColor:uint = 0x91ADC9;
		
		[Bindable]
		public var scrollThumbAlpha:Number = 100;
		
		[Bindable]
		public var scrollThumbLineColor:uint = 0xCCCCCC;
		
		[Bindable]
		public var scrollTrackColor:uint = 0xF5F5F5;
		
		[Bindable]
		public var scrollTrackAlpha:Number = 100;
		
		[Bindable]
		public var scrollTrackLineColor:uint = 0xC5C5C5;
		
		[Bindable]
		public var verticalScrollPosition:Number = 0;
		
		[Bindable]
		public var autoSize:Boolean = true;
		
		[Bindable]
		public var source:FormattedText = new FormattedText();
		
		[Bindable]
		public var plainText:String = "";
		
		[Bindable]
		public var columnCount:uint = 1;
		
		[Bindable]
		public var columnGap:uint = 20;
		
		[Bindable]
		public var output:FormattedText =  new FormattedText();
		
		[Bindable]
		public var stylesValid:Boolean = true;
		
		[Transient]
		public var hasEmbeddedVariables:Boolean = false;
		
		[Transient]
		public var buffer:String = "";
		
		[Bindable]
		public var inputVal:String = "";

		[Bindable]
		public var fontColor:uint = 0x000000;
		
		[Bindable]
		public var fontFamily:String = Fonts.DEFAULT_SANS_FONT;
		
		[Bindable]
		public var fontSize:Number = 14;
		
		[Bindable]
		public var fontAlpha:Number = 100;
		
		[Bindable]
		public var fontSpacing:Number = 0;
		
		[Bindable]
		public var kerning:String = Kerning.ON;
		
		[Bindable]
		public var italic:Boolean = false;
		
		[Bindable]
		public var textAlignH:String = TextAlign.LEFT;
		
		[Bindable]
		public var textAlignV:String = VerticalAlign.TOP;
		
		[Bindable]
		public var bold:Boolean = false;
		
		[Bindable]
		public var underline:Boolean = false;
		
		[Bindable]
		public var margin:Number = 0;

        [Transient]
		[Bindable]
		public var valid:Boolean = true;
		
		
		//styles:  reinstate styles by uncommenting next line
		//[Bindable]
		//public var styleSpec:Object;
		
		public var styleOverrides:ArrayList = new ArrayList();
		
		[Transient]
		public var storedStyleOverrides:ArrayList = new ArrayList();
		
		public function TextObject():void
		{


			fillAlpha = 0;
			lineThickness = 0;
			anchorPoint = new Point(0,0);  // anchorPoint=topLeft
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("source"), "source");
			//ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("output"), "output");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("plainText"), "plainText");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fontFamily"), "fontFamily");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fontSize"), "fontSize");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("bold"), "bold");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("italic"), "italic");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("underline"), "underline");
			//ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fontSpacing"), "fontSpacing"); not yet implemented
			//ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("kerning"), "kerning");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fontColor"), "fontColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fontAlpha"), "fontAlpha");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("textAlignH"), "textAlignH");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("textAlignV"), "textAlignV");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("columnCount"), "columnCount");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("columnGap"), "columnGap");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("autoSize"), "autoSize");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("vScrollPolicy"), "vScrollPolicy");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("verticalScrollPosition"), "verticalScrollPosition");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("textLength"), "textLength");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollThumbColor"), "scrollThumbColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollThumbOverColor"), "scrollThumbOverColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollThumbAlpha"), "scrollThumbAlpha");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollThumbLineColor"), "scrollThumbLineColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollTrackColor"), "scrollTrackColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollTrackAlpha"), "scrollTrackAlpha");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollTrackLineColor"), "scrollTrackLineColor");
			
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("margin"), "margin");
			
			//styles:  reinstate styles by uncommenting next line
			//ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("styleSpec"), "styleSpec");
			
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("reEval"));
            vars.parent = this;
		}

		public function addVariableAnchor(anchor:VariableWireAnchor):void {
			addAnchor(anchor, true);
		}

		public function removeVariableAnchor(anchor:VariableWireAnchor):void {
			removeAnchor(anchor);
		}

        public function getVariableAnchor(name:String):VariableWireAnchor {
            for each(var obj:Object in anchors) {
                var anchor:VariableWireAnchor = obj as VariableWireAnchor;
                if(anchor && anchor.hostProperty == name) {
                    return anchor;
                }
            }
            return null;
        }
		
		/**
		 * Return true if the "source" ribbon is accessible directly (not in a closed gadget)
		 * or indirectly through custom ribbons. 
		 * @return 
		 * 
		 */
		public function get sourceTextIsAccessible():Boolean
		{
			if (!childOfClosedComposite)
				return true;

			var accessible:AbstractObject = this.accessibleObject;
			if (accessible as SmartObject) {
				var composite:SmartObject = accessible as SmartObject;
				var array:Array = [ anchors["source"] ];  // array of just one anchor
				var custom:Array = composite.findCustomAnchors(array);
				if (custom && custom.length > 0) {
					return true;
				}
			}
			return false;
		}

        public function getStore():VariableStore {
            return vars;
        }

        public function invalidate():void {
            valid = false;
        }

        public function variablesChanged():void {
            for each(var obj:Object in anchors) {
                var anchor:VariableWireAnchor = obj as VariableWireAnchor;
                if(anchor) {
                    anchor.valueChanged();
                }
            }
        }

        public function variableChanged(name:String):void {
            if(Application.running == false && loading == false && ApplicationController.instance.wireController.bindingsEnabled) {
                recordInitialValue("vars");
            }
        }

        override public function onRestoreInitial():void {
            super.onRestoreInitial();
            variablesChanged();
        }

        public function get autoSizeEffective():Boolean {
            // is autoSize effective?  only when text is vertically aligned to the top, etc.
            return textAlignV == VerticalAlign.TOP && columnCount == 1 && autoSize;
        }
    }
}