package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectProperties;
	
	import mx.events.PropertyChangeEvent;
	
	public class Calc extends LogicObject
	{
		private var _calcValue:Object = "";
		
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get calcValue():Object {
			return _calcValue;
		}
		
		public function set calcValue(value:Object):void {
			var old:Object = _calcValue;
			_calcValue = value;
			// always fire regardless of change
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "calcValue", old, value));
		}
		
		[Bindable]
		public var controlValue:Object = "";
		
		[Bindable]
		public var type:String = Modifiers.STRING_TYPE;
		
		[Bindable]
		public var constraints:Object;
		
		public function Calc()
		{
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("calcInlet"));
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("calcValue"), "calcValue");
		}

	}
}