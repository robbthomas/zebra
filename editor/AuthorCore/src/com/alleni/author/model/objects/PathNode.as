/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.model.objects
{
	import com.alleni.taconite.persistence.json.JSON;
	
	import flash.geom.Point;

	public class PathNode
	{
		public var x:Number;
		public var y:Number;
		public var sharp:Boolean;
		public var anchorInfo:PathNodeInfo;
		public var control1:Point;
		public var control2:Point;
		public var steps:int = 10;
		public var length:Number = 0;
		public var selected:Boolean;
		
		public function PathNode(sharp:Boolean=false)
		{
			this.x = 0;
			this.y = 0;
			this.sharp = sharp;
		}
		
		public function toJson():Object {
			var json:Object = {type:"PathRawPoint", x:x, y:y, length:length, steps:steps};
			if (sharp)
				json.sharp = sharp;
			if (anchorInfo)
				json.nodeLink = com.alleni.taconite.persistence.json.JSON.encode(anchorInfo);
			if (control1)
				json.control1 = com.alleni.taconite.persistence.json.JSON.encode(control1);
			if (control2)
				json.control2 = com.alleni.taconite.persistence.json.JSON.encode(control2);
			return json;
		}
		
		public static function fromJson(json:Object):PathNode {
			var node:PathNode = new PathNode();
			node.x = json.x;
			node.y = json.y;
			if (json.steps)
				node.steps = json.steps;
			if (json.sharp)
				node.sharp = json.sharp;
			if (json.nodeLink)
				node.anchorInfo = PathNodeInfo.fromJson(com.alleni.taconite.persistence.json.JSON.decode(json.nodeLink));
			if (json.control1)
				node.control1 = decodePoint(json.control1);
			if (json.control2)
				node.control2 = decodePoint(json.control2);
			node.length = json.length;
			return node;
		}
		
		private static function decodePoint(str:String):Point
		{
			var json:Object = com.alleni.taconite.persistence.json.JSON.decode(str);
			return new Point(json.x, json.y);
		}

		public function toString():String
		{
			var ribbonStr:String = "";
			if (anchorInfo) {
				ribbonStr = " ribbon="+anchorInfo.key;
			}
			return "[" + "PathNode" + " XY="+x+","+y +  " sharp="+sharp + " c1="+fmtPoint(control1) + " c2="+fmtPoint(control1) + " dist="+length + ribbonStr + "]";
		}
		
		private function fmtPoint(point:Point):String
		{
			if (point == null) {
				return "";
			} else {
				return int(point.x).toString() + "," + int(point.y).toString();
			}
		}
	}
}