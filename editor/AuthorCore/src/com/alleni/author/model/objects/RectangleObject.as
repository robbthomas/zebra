package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.ObjectProperties;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class RectangleObject extends VectorObject
	{
		[Bindable]
		public var cornerRadius:Number = 0;
		

		/**
		 * Constructor 
		 * 
		 */
		public function RectangleObject()
		{
			super();
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("cornerRadius"), "cornerRadius");
		}
		
	}
}