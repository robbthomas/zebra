package com.alleni.author.model.objects {
	import com.alleni.author.model.AbstractObject;

	public class ContentObject extends AbstractObject
	{
		public function ContentObject(setupProperties:Boolean=true):void
		{
			super(setupProperties);
		}
	}
}