/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.model.objects
{
	public class PathNodeInfo
	{
		public var serial:int;
		public var key:String;
		public var label:String;
		
		public function PathNodeInfo(serial:int=0, key:String=null, label:String=null)
		{
			this.serial = serial;
			this.key = key;
			this.label = label;
		}
		
		public function toJson():Object {
			return {
				type:"PathNodeLink",
				serial:serial,
				key:key,
				label:label
			};
		}
		
		public static function fromJson(json:Object):PathNodeInfo {
			return new PathNodeInfo(json.serial, json.key, json.label);
		}
	}
}
