/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.model.objects
{
    import com.alleni.author.definition.Modifiers;
    import com.alleni.author.definition.text.Fonts;
    import com.alleni.author.model.AbstractObject;
    import com.alleni.author.model.ObjectProperties;
    
    import flash.text.engine.Kerning;
    
    import flashx.textLayout.formats.TextAlign;
    import flashx.textLayout.formats.VerticalAlign;

    /**
     * Value object representing Text in the world. 
     */
    public class Text extends NativeVisualObject
    {   
        [Bindable]
        public var label:String = "";
        	
        [Bindable]
        public var fontColor:uint = 0x000000;
        
        [Bindable]
        public var fontFamily:String = Fonts.DEFAULT_SANS_FONT;
        
        [Bindable]
        public var fontSize:Number = 14;
		
		[Bindable]
		public var fontAlpha:Number = 100;
		
		[Bindable]
		public var fontSpacing:Number = 0;
        
        [Bindable]
        public var kerning:String = Kerning.ON;
		
        [Bindable]
        public var italic:Boolean = false;
        
        [Bindable]
		public var textAlignH:String = TextAlign.LEFT;
		
		[Bindable]
		public var textAlignV:String = VerticalAlign.TOP;
		
		[Bindable]
		public var bold:Boolean = false;
		
		[Bindable]
		public var underline:Boolean = false;
		
		[Bindable]
		public var margin:Number = 10;
  
		public function Text():void
		{
			fillAlpha = 0;
			lineThickness = 0;
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("label"), "label");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fontFamily"), "fontFamily");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fontSize"), "fontSize");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("bold"), "bold");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("italic"), "italic");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("underline"), "underline");
			// ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fontSpacing"), "fontSpacing"); not yet implemented
			// ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("kerning"), "kerning");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fontColor"), "fontColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fontAlpha"), "fontAlpha");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("textAlignH"), "textAlignH");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("textAlignV"), "textAlignV");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("margin"), "margin");
		}
  	}
}
