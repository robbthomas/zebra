/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 */
package com.alleni.author.model.objects
{
import com.alleni.author.controller.objects.StateTableController;
import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.ObjectProperties;
	import com.alleni.taconite.lang.TaconiteTimer;
	
	import flash.events.TimerEvent;

	/**
	 * State table object
	 */
	public class StateTable extends AbstractTable
	{
		public static const TACONITE_COLLECTIONS:Vector.<String> = Vector.<String>(["guidePositions","port","portValue", "logicRowName", "currentStateValues", "allStateValues", "stateValueExpressions", "stateValueBound", "stateValueName", "stateValueType", "stateValueTypeConstraints", "currentTweenValues", "allTweenValues", "truthPort", "branches"]);

		[Transient] private var holdStartTime:int = 0;
		[Bindable] public var ticking:Boolean = false;
        [Bindable] public var resumePlaying:Boolean = false;
		private static var _taconiteTimer:TaconiteTimer;
		
		private static const columnModifier:PropertyDescription = new PropertyDescription("activeColumn", "3-2-30","Active Column","Active column number",  Modifiers.VALUE, 0,
			false, -1, Modifiers.UNSIGNED_NUMBER_TYPE, null, true);
		
		public function StateTable()
		{
			showLogicRows = false;
			showValueRows = true;
			showControls = true;
			showBranchingRow = true;
			_taconiteTimer = TaconiteTimer.instance;
			branchingDefault = 0;
			super();
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("statusMessage"), "statusMessage");
		}
		
		override protected function registerColumnProperty():void
		{
			ObjectProperties.instance.registerProperty(this, columnModifier, "activeColumn");
		}
		
		override public function handleTween():void
		{
			trace("Handle Tween starting");
            _taconiteTimer.unregisterListener(holdTimerListener);
			super.handleTween();
		}

		override protected function tweenComplete():void
		{
			super.tweenComplete();
			if(ticking) {
				holdStartTime = _taconiteTimer.milliseconds;
				_taconiteTimer.registerListener(holdTimerListener, true);
			}
		}

		private function holdTimerListener(event:TimerEvent):void
		{
			var holdTime:int = currentTweenValues[2]*1000
			var currentTime:int = _taconiteTimer.milliseconds;
			if(holdStartTime + holdTime < currentTime) {
				_taconiteTimer.unregisterListener(holdTimerListener);
				if(ticking) {
					branch();
				}
			}
		}

        public function stopAll():void{
            _taconiteTimer.unregisterListener(holdTimerListener);
        }
		
		private function branch():void{
            if(fireBranches && column > 0){
				switch(branches[column-1]){ // 0 = continue, 1 = pause continue, 2 = replay, 3 = exit, 4 = exit power down.
					case 0:
                        if(column < numColumns){
						    column++;
                        }else{
                            ticking = false;
                        }
						break;
					case 1:
						ticking = false;
						break;
					case 2:
						column = -1;
						ticking = true;
						break;
					case 3:
						column = -1;
						ticking = false;
						break;
					case 4:
						ticking = false;
						if(poweredOnRun){
							poweredOnRun = !poweredOnRun;
						}
						break;
				}
			}else{
				fireBranches = true;
			}
		}

        override public function onRestoreInitial():void {
            super.onRestoreInitial();
		    StateTableController(controller).reset(this);
        }

        override protected function pauseRunningState():void {
            super.pauseRunningState();
            resumePlaying = ticking;
        }

        override protected function resumeRunningState():void {
            super.resumeRunningState();
            ticking = resumePlaying;
            if(ticking){
                fireBranches = resumePlaying;
                branch();
            }
        }
    }
}
