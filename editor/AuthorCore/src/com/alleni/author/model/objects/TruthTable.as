/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 */
package com.alleni.author.model.objects
{
import com.alleni.author.controller.objects.TruthTableController;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.model.ObjectProperties;

/**
	 * TruthTable object
	 */
	public class TruthTable extends AbstractTable
	{
		public static const TACONITE_COLLECTIONS:Vector.<String> = Vector.<String>(["guidePositions","port","portValue", "logicRowName", "currentStateValues", "allStateValues", "stateValueExpressions", "stateValueBound", "stateValueName", "stateValueType", "stateValueTypeConstraints", "currentTweenValues", "allTweenValues", "truthPort", "branches"]);

        private static const truthTablePortModifier:PropertyDescription = (new PropertyDescription("port", "3-10-00",	"Port",	"Logic input", Modifiers.PORT, 0, true, 8, "", null, true, false));

		public function TruthTable()
		{
			numColumns = 2;
			super();
		}

        override protected function registerPortProperty():void{
            ObjectProperties.instance.registerProperty(this, truthTablePortModifier, "port");
        }

    override public function onRestoreInitial():void {
        super.onRestoreInitial();
        TruthTableController(controller).setColumn(this);
    }
}
}
