package com.alleni.author.model.objects
{
	import com.alleni.author.controller.objects.IReplaceableReceiver;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectOutlets;
	import com.alleni.author.model.ObjectProperties;

	public class Audio extends MediaObject implements IReplaceableReceiver
	{
		public function Audio()
		{
			super();
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("preload"), "preload");

			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("duration"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("targetCollide"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("overlapsCollisionTarget"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("targetDrop"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("putBackWhen"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("putBackSecs"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("width"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("height"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("anchorPoint"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pivotPoint"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("alignAreaHorz"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("alignAreaVert"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("rotation"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillSpec"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillAlpha"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineColor"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineAlpha"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineThickness"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("alpha"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("movable"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pathValue"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pathPercent"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pathMaximum"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pathMinimum"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("runtimeRotation"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("overlapsCollisionTarget"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("snapTo"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("snap"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pathOn"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("inertial"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("collidingWith"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("droppedOn"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("acceptIntoOutOfArena"));	
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("maintainAspect"));
			
			ObjectInlets.instance.unregisterInlet(this, Modifiers.instance.fetch("putBack"));
			ObjectInlets.instance.unregisterInlet(this, Modifiers.instance.fetch("snapToNearest"));
			
			ObjectOutlets.instance.unregisterOutlet(this, Modifiers.instance.fetch("dragStart"));
			ObjectOutlets.instance.unregisterOutlet(this, Modifiers.instance.fetch("dragMove"));
			ObjectOutlets.instance.unregisterOutlet(this, Modifiers.instance.fetch("dragRelease"));	
		}
	}
}