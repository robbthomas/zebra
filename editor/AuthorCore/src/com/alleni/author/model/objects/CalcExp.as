package com.alleni.author.model.objects
{
	import com.alleni.author.definition.AST;
	import com.alleni.author.definition.Lexer;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.Parser;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ObjectProperties;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;
	import mx.events.PropertyChangeEvent;
	
	public class CalcExp extends LogicObject
	{
		
		public static const TACONITE_COLLECTIONS:Vector.<String> = Vector.<String>(["input","inputName"]);
		
		[Bindable]
		public var input:IList = new ArrayCollection(); // of Object
		
		[Bindable]
		public var inputName:IList = new ArrayCollection(); // of String
		
		/*public var noMessageCenter:Object;*/
		
		[Bindable]
		public var expression:String = "";
		private var _ast:AST = null;
		
		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get expressionValid():Boolean {
			return _ast != null;
		}
		
		private function get ast():AST {
			return _ast;
		}
		
		private function set ast(ast:AST):void {
			var old:Boolean = expressionValid;
			_ast = ast;
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "expressionValid", old, expressionValid));
		}
		
		private var _result:Object = "";
		
		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get result():Object {
			return _result;
		}
		
		private function setResult(value:Object):void {
			var old:Object = _result;
			_result = value;
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "result", old, value));
		}
		
		public function CalcExp()
		{
			for(var i:Number=0; i<inputName.length; i++) {
				inputName[i] = String.fromCharCode(65+i);
			}
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("input"), "input");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("expression"), "expression");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("result"), "result");
		}
		
		override public function initializeModel(loaded:Boolean):void
		{
			reparse();
		}
		
		public function reparse():void {
			try {
				ast = Parser.parse(Lexer.lex(expression));
				ast.print();
				recalc();
			} catch(e:Object) {
				trace("Error parsing expression", e);
				ast = null;
			}
		}
		
		public function recalc():void {
			var env:Object = AST.defaultEnvironment();
			for(var i:Number=0; i<input.length; i++) {
				env[inputName[i].toString().toLowerCase()] = Number(input[i]);
			}
			var good:Boolean = expressionValid;
			if(result != "" && !good) {
				setResult("");
			} else if(good) {
				try {
					setResult(ast.evaluate(env));
				} catch(e:Object) {
					ast = null;
					trace("Error evaluating expression", e);
				}
			}
		}
	}
}