package com.alleni.author.model.objects
{
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	
	import mx.utils.UIDUtil;

	public class ToggleButtonGroup extends EventDispatcher
	{
		// Array of dictionaries of groups for decoding a project, so each wiring-scope has a separate dictionary,
		// driven by startDeserializing(), below.
		protected static var groups:Array = [];
		
		[Bindable] public var toggleButtonGroupValue:String = "";
        [Bindable] public var toggleButtonGroupNullValue:String = "";
		[Bindable] public var id:String;
		
		
		public function ToggleButtonGroup()
		{
			id = UIDUtil.createUID();
		}
		
		public function toJson():Object
		{
			return {
				id:id,
				type:"ToggleButtonGroup",
				toggleButtonGroupValue:toggleButtonGroupValue,
                toggleButtonGroupNullValue:toggleButtonGroupNullValue
			}
		}
		
		public static function fromJson(json:Object):ToggleButtonGroup
		{
            var group:ToggleButtonGroup = groups[groups.length-1][json.id];
			if(!group)
			{
				group = new ToggleButtonGroup();
				group.toggleButtonGroupValue = json.toggleButtonGroupValue;
                group.toggleButtonGroupNullValue = json.toggleButtonGroupNullValue;
				group.id = json.id;
                groups[groups.length-1][json.id] = group;
			}
			return group;
		}

        public static function startDeserializing():void {
            groups.push(new Dictionary());
        }

        public static function stopDeserializing():void {
            groups.pop();
        }
	}
}