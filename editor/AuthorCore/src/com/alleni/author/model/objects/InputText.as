package com.alleni.author.model.objects
{
import com.alleni.author.definition.AbstractModifierDescription;
import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.GraphicFill;
    import com.alleni.author.model.ObjectInlets;
    import com.alleni.author.model.ObjectOutlets;
	import com.alleni.author.model.ObjectProperties;
	
	public class InputText extends Text
	{
		public static const CONTINUOUSLY:String = "continuously";
		public static const ON_ENTER:String = "onEnter";
		public static const ON_TAB:String = "onTab";

		[Bindable]
		public var numbersOnly:Boolean = false;
		
		[Bindable]
		public var maxCharCount:int = 1000;
		
		[Bindable]
		public var submit:String = ON_ENTER;

        [Bindable]
        public var promptText:String = "";
		
		[Bindable]
		[Transient]
		public function get text():String {
			return label;
		}
		
		public function set text(value:String):void {
			label = value;
		}
		
		public function InputText()
		{
			super();
			
			//default colors
			this.lineThickness = 1;
			this.lineColor = 0x124578;
			this.fillAlpha = 100;
			var graphicFill:GraphicFill = new GraphicFill();
			graphicFill.color = 0xE0EDFC;
			this.fillSpec = graphicFill;
			this.fillGradient = new Vector.<uint>([0xB3C9E3, 0xEBF5FF]);
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("label"));
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("text"), "text");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("promptText"), "promptText");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("interactive"), "interactive");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("disabledAlpha"), "disabledAlpha");

			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("numbersOnly"), "numbersOnly");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("maxCharCount"), "maxCharCount");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("submit"), "submit");

            ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("giveTextFocus"));
            ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("takeTextFocus"));
            ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("enableButton"));
            toolTipOverrides["enableButton"] = "Allow this text input field to enter text";
            ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("disableButton"));
            toolTipOverrides["disableButton"] = 'Do not allow this text input field to enter text \n(set opacity in "Visibility: Opacity- pwr off")';

            ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("gotTextFocus"));
            ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("lostTextFocus"));

			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("entryMade"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("hitMaxChars"));

			ObjectOutlets.instance.unregisterOutlet(this, Modifiers.instance.fetch("dragStart"));
			ObjectOutlets.instance.unregisterOutlet(this, Modifiers.instance.fetch("dragMove"));
			ObjectOutlets.instance.unregisterOutlet(this, Modifiers.instance.fetch("dragRelease"));

            toolTipOverrides["interactive"] = 'Can this text input field enter text?';
		}

		override protected function initializeValues():void
		{
			if ("promptText" in initialValues) {
				text = initialValues["promptText"];
			} else {
				text = "";
			}
            super.initializeValues();
		}
	}
}