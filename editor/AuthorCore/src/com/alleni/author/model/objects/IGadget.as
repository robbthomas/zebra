package com.alleni.author.model.objects
{
	import mx.collections.IList;

	public interface IGadget
	{
		function get objects():IList;
		function get id():String;
		function set id(value:String):void;
	}
}