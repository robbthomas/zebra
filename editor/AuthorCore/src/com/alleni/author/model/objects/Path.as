/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.model.objects
{
	import com.alleni.author.util.BezierSegment;
	import com.alleni.taconite.lang.Geometry;
	import com.alleni.taconite.persistence.json.JSON;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class Path
	{
		private var nodes:Vector.<PathNode> = new Vector.<PathNode>();
				
		public var length:Number;  // length of the path in pixels
		
		public var scaleX:Number = 1;
		public var scaleY:Number = 1;

		
		public function toJson():Object {
			var json:Object = {
				type:"Path",
				length:length,
				scaleX:scaleX,
				scaleY:scaleY
			};
			var array:Array = [];
			for (var n:int=0; n < nodes.length; n++)
				array[n] = nodes[n].toJson();
			json.nodes = array;
			return json;
		}
		
		public static function fromJson(json:Object):Path {
			var path:Path = new Path();
			path.length = json.length;
			if (json.hasOwnProperty("scaleX"))
				path.scaleX = json.scaleX;
			if (json.hasOwnProperty("scaleY"))
				path.scaleY = json.scaleY;
			
			for (var n:int=0; n < json.nodes.length; n++)
				path.nodes[n] = PathNode.fromJson(json.nodes[n]);
			return path;
		}
		
		public function clone():Path
		{
			return fromJson(this.toJson());
		}
		
		public function dump():void
		{
			trace("Path: nodeCount="+nodes.length, "dist="+length);
			for (var n:int=0; n < nodes.length; n++) {
				trace(" " + n, nodes[n]);
			}
		}
		
		public function addPoint(index:int, point:Point=null, sharp:Boolean=false):void
		{
			var node:PathNode = new PathNode(sharp);
			nodes.splice(index, 0, node);
			if (point)
				setPoint(index, point);
		}

		
		public function deletePoint(index:int):void
		{
			nodes.splice(index, 1);
		}

		public function deleteAll():void
		{
			for (var n:int=nodeCount-1; n >= 0; n--) {
				deletePoint(n);
			}
		}

		
		/**
		 * Return the nth point on the path, scaled by scaleX & scaleY. 
		 * @param n
		 * @return 
		 * 
		 */
		public function getPoint(n:int):Point
		{
			var node:PathNode = nodes[n];
			return new Point(node.x * scaleX, node.y * scaleY);
		}
		
		public function setPoint(n:int, point:Point):void
		{
			var node:PathNode = nodes[n];
			node.x = point.x / scaleX;
			node.y = point.y / scaleY;
		}
		
		public function getSharp(n:int):Boolean
		{
			return nodes[n].sharp;
		}
		
		public function setSharp(n:int, sharp:Boolean):void
		{
			nodes[n].sharp = sharp;
		}
		
		public function getSelected(n:int):Boolean
		{
			return nodes[n].selected;
		}
		
		public function setSelected(n:int, selected:Boolean):void
		{
			nodes[n].selected = selected;
		}
		
		public function selectAll(select:Boolean=true):void
		{
			for each (var node:PathNode in nodes) {
				node.selected = select;
			}
		}
		
		public function getAnchorInfo(n:int):PathNodeInfo
		{
			return nodes[n].anchorInfo;
		}
		
		public function setAnchorInfo(n:int, info:PathNodeInfo):void
		{
			nodes[n].anchorInfo = info;
		}

		
		/**
		 * Return a control point on the path, scaled by scaleX & scaleY. 
		 * @param n = which node.
		 * @param control = 1 or 2, indicating which control point.
		 * @return 
		 * 
		 */
		public function getControlPoint(n:int, control:int):Point
		{
			var node:PathNode = nodes[n];
			var point:Point = (control == 1) ? node.control1 : node.control2;
			if (point == null)
				return null;
			return new Point(point.x * scaleX, point.y * scaleY);
		}
		
		public function setControlPoint(n:int, control:int, point:Point):void
		{
			var node:PathNode = nodes[n];
			var pt:Point = new Point(point.x / scaleX, point.y / scaleY);
			if (control == 1)
				node.control1 = pt;
			else
				node.control2 = pt;
		}
		
		
		/*
		Rounded spans begin and end with a quadratic curve.  The inner curves are cubic.
		The simplest curve has 3 points, so it has two quadratic curves:
		Path: nodeCount=3
		0 [object XY=0,0 sharp=false c1= c2=]
		1 [object XY=78,64 sharp=false c1=64,35 c2=64,35]
		2 [object XY=80,143 sharp=false c1= c2=]
		
		With 4 points we get a cubic curve in the middle:
		Path: nodeCount=4
		0 [object XY=0,79 sharp=false c1= c2=]
		1 [object XY=94,0 sharp=false c1=62,-3 c2=62,-3]
		2 [object XY=158,79 sharp=false c1=132,80 c2=132,80]
		3 [object XY=209,1 sharp=false c1= c2=]
		
		*/
		
		public function drawPath(g:Graphics):void
		{
			if (nodes.length < 2)
				return;
			var point:Point;
			point = getPoint(0);
			g.moveTo(point.x, point.y);
			
			var limit:int = nodes.length;
			var index:int = 0;
			while (index < limit-1) {
				if (index+1 < limit && nodes[index+1].control1) {  // check control1 existence
					var spanEnd:int = index+1;
					while (spanEnd+1 < limit && nodes[spanEnd+1].control2)
						++spanEnd;
					if (spanEnd+1 < limit)
						++spanEnd;
					drawRoundedSpan(g, index, spanEnd);
					index = spanEnd;
				} else {
					point = getPoint(index+1);
					g.lineTo(point.x, point.y);
					++index;
				}
			}
//			debugDraw(g);
		}
		
		private function drawRoundedSpan(g:Graphics, start:int, end:int):void
		{
			var point:Point = getPoint(start+1);
			var control1:Point = getControlPoint(start+1, 1);
			g.curveTo(control1.x, control1.y, point.x, point.y);
			
			for(var i:int=start+1; i<end-1; i++) {
				// BezierSegment instance using the current point, its second control point, the next point's first control point, and the next point
				point = getPoint(i);
				control1 = getControlPoint(i+1, 1);
				var control2:Point = getControlPoint(i, 2);
				var bezier:BezierSegment = new BezierSegment(point, control2, control1, getPoint(i+1));
				
				var steps:int = Math.max(nodes[i].steps, nodes[i+1].steps);
				var incr:Number = 1 / steps;
				for (var t:Number=0; t<1+incr/2; t+=incr) {
					var pt2:Point = bezier.getValue(t);	// x,y on the curve for a given t
					g.lineTo(pt2.x,pt2.y);
				}
			}
			
			// Draw a regular quadratic Bézier curve from the first to second points, using the first control point of the second point
			var tail:int = end-1;
			control2 = getControlPoint(tail, 2);
			point = getPoint(end);
			g.curveTo(control2.x, control2.y, point.x, point.y);
		}
		
		
		public function moveToward(cur:Number, target:Point, outputPoint:Point=null):Number
		{
			if (nodes.length == 1) {
				if (outputPoint)  outputPoint = getPoint(0);
				return 0;  
			}
			
			var incr:Number = 2 / this.length;  // advance 2 pixels per step
			
			var resultT:Number = cur;
			var resultPt:Point = getPosition(cur);
			var resultDist:Number = resultPt.subtract(target).length;
			
			// decide whether to move up, or down the path
			if (Point.distance(getPosition(cur+incr), target) < resultDist) {
				// continue +incr
			} else if (Point.distance(getPosition(cur-incr), target) < resultDist) {
				incr = -incr;
			} else {
				if (outputPoint)
					outputPoint = resultPt.clone();
				return cur;
			}
			
			// continue in that direction until our luck changes
			var t:Number = cur + incr;
			while (t > -Math.abs(incr) && t < 1+Math.abs(incr)) {
				var test:Number = Math.max(0, Math.min(t,1));
				var candidate:Point = getPosition(test);
				var dist:Number = candidate.subtract(target).length;
				if(dist <= resultDist) {
					resultT = test;
					resultPt = candidate;
					resultDist = dist;
				} else {
					break;  // our luck has changed  ... reached inflection point
				}
				t = t + incr;
			}
			if (outputPoint)
				outputPoint = resultPt.clone();
			return resultT;
		}
		
		public function closestPoint(target:Point, outputPoint:Point=null):Number
		{
			if (nodeCount == 1) {
				if (outputPoint) {
					var pt:Point = getPoint(0);
					outputPoint.x = pt.x;
					outputPoint.y = pt.y;
				}
				return 0;
			}
			var resultSegment:int = 0;
			var resultDist:Number = Point.distance(getPoint(0), target);
			for (var n:int=0; n < nodes.length-1; n++) {
				var onePoint:Point = new Point();
				closestPointOnSegment(n, target, onePoint);
				var oneDist:Number = Point.distance(onePoint, target);
				if (oneDist < resultDist) {
					resultSegment = n;
					resultDist = oneDist;
				}
			}
			
			var distance:Number = distanceToSegmentStart(resultSegment);
			distance += closestPointOnSegment(resultSegment, target, outputPoint);
			var resultFrac:Number = distance / this.length;
			return resultFrac;
		}
		
		
		private function getLength():Number
		{
			return distanceToSegmentStart(nodes.length-1);
		}
		
		
		private function distanceToSegmentStart(segment:int):Number
		{
			var result:Number = 0;
			for(var i:int=0; i<segment; i++)
				result += nodes[i].length;
			return result;
		}
		
		private function closestPointOnSegment(n:int, target:Point, outputPoint:Point=null):Number
		{
			var steps:int = Math.max(nodes[n].steps, nodes[n+1].steps);
			var result:Number;  // distance from start of segment to the closest point
			var pt:Point = new Point();
			if (isStraight(n)) {
				var first:Point = getPoint(n);
				var second:Point = getPoint(n+1);
				pt = Geometry.closestPointOnLineSegment(first, second, target);
				result = Point.distance(first, pt);
			} else if (isCubic(n)) {
				result = cubicNearestPoint(getPoint(n), getControlPoint(n, 2), getControlPoint(n+1, 1), getPoint(n+1), target, steps, pt);
			} else if (nodes[n+1].control1) {
				result = quadraticNearestPoint(getPoint(n), getControlPoint(n+1, 1), getPoint(n+1), target, steps, pt);
			} else {
				result = quadraticNearestPoint(getPoint(n), getControlPoint(n, 2), getPoint(n+1), target, steps, pt);
			}
			
			if (outputPoint) {
				outputPoint.x = pt.x;
				outputPoint.y = pt.y;
			}
			return result;  // distance from start of segment to the closest point
		}
				
		public function getPosition(frac:Number, steps:int=10):Point
		{
			if (nodes.length == 1 || frac <= 0) {
				return getPoint(0).clone();
			}
			if (frac >= 1) {
				return getPoint(nodes.length-1).clone();
			}
			
			var leftover:Number = this.length * frac;
			var n:int;
			for (n=0; n < nodes.length; n++) {
				if (leftover > nodes[n].length)
					leftover -= nodes[n].length;
				else
					break;
			}
			//			trace("getPosition n="+n, "leftover="+leftover, "frac="+frac);
			return pointAlongSegment(n, leftover, steps);
		}
		
		public function nodeIndexFromFrac(frac:Number):int
		{
			var leftover:Number = this.length * frac;
			var n:int;
			for (n=0; n < nodes.length; n++) {
				if (leftover > nodes[n].length)
					leftover -= nodes[n].length;
				else
					break;
			}
			return Math.min(n, nodes.length-1);
		}
		
		private function pointAlongSegment(n:int, dist:Number, steps:int):Point
		{
			if (n >= nodes.length-1) {
				return getPoint(nodes.length-1);
			} else if (isStraight(n)) {
				var first:Point = getPoint(n);
				var second:Point = getPoint(n+1);
				var diff:Point = second.subtract(first);
				diff.normalize(dist);
				return first.add(diff);
			} else if (isCubic(n)) {
				return cubicPointAlong(getPoint(n), getControlPoint(n, 2), getControlPoint(n+1, 1), getPoint(n+1), dist, steps);
			} else if (nodes[n+1].control1) {
				return quadraticPointAlong(getPoint(n), getControlPoint(n+1, 1), getPoint(n+1), dist, steps);
			} else {
				return quadraticPointAlong(getPoint(n), getControlPoint(n, 2), getPoint(n+1), dist, steps);
			}
		}
		
		private function isStraight(n:int):Boolean
		{
			var node:PathNode = nodes[n];
			if (node.control1)
				return false;
			if (n >= nodes.length-1)
				return true;
			node = nodes[n+1];
			return (node.control1 == null);
		}
		
		private function isCubic(n:int):Boolean
		{
			var node:PathNode = nodes[n];
			if (node.control1==null)
				return false;
			if (n >= nodes.length-1)
				return true;
			node = nodes[n+1];
			return (node.control1 != null);
		}
		
		
		public function evaluateCurve():void
		{
			// clear the control points
			for (var n:Number = 0; n<nodes.length; n++) { 
				nodes[n].control1 = null;
				nodes[n].control2 = null;
				nodes[n].length = 0;
				nodes[n].steps = 1;
			}
			
			// find spans of rounded points and recalc each one
			var limit:int = nodes.length;
			var index:int = 0;
			while (index < limit-1) {
				if (index+1 == limit-1 || nodes[index+1].sharp) {  // next point is the end, or its sharp
					++index;
				} else if (index < limit-2) {
					// find span of rounded points
					var lastRound:int = index+1;  // this is the first rounded point
					while (lastRound+1 < limit && !nodes[lastRound+1].sharp)
						++lastRound;
					var endSpan:int = lastRound;
					if (endSpan+1 < limit)  // include the following sharp point, if possible
						++endSpan;
					recalcRoundedSpan(index, endSpan);
					index = endSpan;
				}
			}
			
			// set segment lengths
			for (index=0; index < limit-1; index++) {
				var steps:int = Math.max(nodes[index].steps, nodes[index+1].steps);
				if (isStraight(index)) {
					nodes[index].length = Point.distance(getPoint(index), getPoint(index+1));
				} else if (isCubic(index)) {
					nodes[index].length = cubicLength(getPoint(index), getControlPoint(index, 2), getControlPoint(index+1, 1), getPoint(index+1), steps);
				} else if (nodes[index+1].control1) {
					nodes[index].length = quadraticLength(getPoint(index), getControlPoint(index+1, 1), getPoint(index+1), steps);
				} else {
					nodes[index].length = quadraticLength(getPoint(index), getControlPoint(index, 2), getPoint(index+1), steps);
				}
			}
			
			this.length = getLength();
		}
		
		private function recalcRoundedSpan(beginPt:int, lastPt:int):void
		{
			// Ordinarily, curve calculations will start with the second point and go through the second-to-last point
			var firstPt:int = beginPt+1;
			//			trace("recalcRoundedSpan begin="+beginPt, "last="+lastPt, "first="+firstPt);
			
			// Loop through all the points to get curve control points for each.
			var n:int;
			for (n=firstPt; n<lastPt; n++) {
				
				// The previous, current, and next points
				var p0:Point = getPoint(n-1);
				var p1:Point = getPoint(n);
				var p2:Point = getPoint(n+1);
				computeControlPoints(p0, p1, p2, n);
			}
		}
		
		private function computeControlPoints(p0:Point, p1:Point, p2:Point, outputIndex:int):void
		{
			const z:Number = .5;  // .5;
			const angleFactor:Number = .75; // .75;
			const minDistRatio:Number = 1.0001;  // less than this indicates extremely shallow angle  (these angles give incorrect results with normal algorithm)
			
			var a:Number = Point.distance(p0,p1);	// Distance from previous point to current point
			if (a < 0.001) a = .001;		// Correct for near-zero distances, which screw up the angles or something
			var b:Number = Point.distance(p1,p2);	// Distance from current point to next point
			if (b < 0.001) b = .001;
			var c:Number = Point.distance(p0,p2);	// Distance from previous point to next point
			if (c < 0.001) c = .001;
			var controlDist:Number = Math.min(a,b)*z;	// Distance of curve control points from current point: a fraction the length of the shorter adjacent triangle side
			var distRatio:Number = (a + b) / c;
//			trace("distRatio="+distRatio);
			if (distRatio < minDistRatio) {  // shortcut for very shallow angles:  place the control points directly along the lines
				var cp:Point = Point.interpolate(p0, p1, controlDist/a);
				setControlPoint(outputIndex, 1, cp);
				cp = Point.interpolate(p2, p1, controlDist/b);
				setControlPoint(outputIndex, 2, cp);
				return;
			}
			var C:Number = Math.acos((b*b+a*a-c*c)/(2*b*a));	// Angle formed by the two sides of the triangle (described by the three points above) adjacent to the current point
			// Duplicate set of points. Start by giving previous and next points values RELATIVE to the current point.
			var aPt:Point = new Point(p0.x-p1.x,p0.y-p1.y);
			var bPt:Point = new Point(p1.x,p1.y);
			var cPt:Point = new Point(p2.x-p1.x,p2.y-p1.y);
//			trace("a="+a, "b="+b, "c="+c, "C="+C);
			/*
			We'll be adding adding the vectors from the previous and next points to the current point,
			but we don't want differing magnitudes (i.e. line segment lengths) to affect the direction
			of the new vector. Therefore we make sure the segments we use, based on the duplicate points
			created above, are of equal length. The angle of the new vector will thus bisect angle C
			(defined above) and the perpendicular to this is nice for the line tangent to the curve.
			The curve control points will be along that tangent line.
			*/
			if (a > b)
				aPt.normalize(b);	// Scale the segment to aPt (bPt to aPt) to the size of b (bPt to cPt) if b is shorter.
			else if (b > a)
				cPt.normalize(a);	// Scale the segment to cPt (bPt to cPt) to the size of a (aPt to bPt) if a is shorter.
			// Offset aPt and cPt by the current point to get them back to their absolute position.
			aPt.offset(p1.x,p1.y);
			cPt.offset(p1.x,p1.y);
			// Get the sum of the two vectors, which is perpendicular to the line along which our curve control points will lie.
			var ax:Number = bPt.x-aPt.x;	// x component of the segment from previous to current point
			var ay:Number  = bPt.y-aPt.y;
			var bx:Number = bPt.x-cPt.x;	// x component of the segment from next to current point
			var by:Number = bPt.y-cPt.y;
			var rx:Number = ax + bx;	// sum of x components
			var ry:Number = ay + by;
			var r:Number = Math.sqrt(rx*rx+ry*ry);	// length of the summed vector - not being used, but there it is anyway
			var theta:Number = Math.atan(ry/rx);	// angle of the new vector
//			trace("ax="+ax, "ay="+ay, "bx="+bx, "by="+by, "rx="+rx, "ry="+ry, "theta="+theta);
			
			var controlScaleFactor:Number = C/Math.PI;	// Scale the distance based on the acuteness of the angle. Prevents big loops around long, sharp-angled triangles.
			controlDist *= ((1-angleFactor) + angleFactor*controlScaleFactor);	// Mess with this for some fine-tuning
			var controlAngle:Number = theta+Math.PI/2;	// The angle from the current point to control points: the new vector angle plus 90 degrees (tangent to the curve).
			var controlPoint2:Point = Point.polar(controlDist,controlAngle);	// Control point 2, curving to the next point.
			var controlPoint1:Point = Point.polar(controlDist,controlAngle+Math.PI);	// Control point 1, curving from the previous point (180 degrees away from control point 2).
//			trace("controlDist="+controlDist, "controlScaleFactor="+controlScaleFactor, "controlAngle="+controlAngle);
			
			// Offset control points to put them in the correct absolute position
			controlPoint1.offset(p1.x,p1.y);
			controlPoint2.offset(p1.x,p1.y);
			
			/*
			Some control points will be reversed.
			In this case controlPoint2 will be farther from the next point than controlPoint1 is.
			Check for that and switch them if it's true.
			*/
			if (isNaN(controlPoint1.x) ||isNaN(controlPoint1.y) ||isNaN(controlPoint2.x) ||isNaN(controlPoint2.y)) {
				// leave the control-points null
			} else {
				if (Point.distance(controlPoint2,p2) > Point.distance(controlPoint1,p2)) {
					setControlPoint(outputIndex, 1, controlPoint2);  // Add the two control points to the array in reverse order
					setControlPoint(outputIndex, 2, controlPoint1);	
				} else {
					setControlPoint(outputIndex, 1, controlPoint1);
					setControlPoint(outputIndex, 2, controlPoint2);
				}
				var near:Point = Geometry.closestPointOnLineSegment(p1, p2, controlPoint1, false);
				var deviation:Number = Point.distance(near, controlPoint1);
				nodes[outputIndex].steps = computeSteps(deviation, distRatio, controlDist);
			}
//			trace("controlPoint1="+controlPoint1, "controlPoint2="+controlPoint2);
//			trace("");
		}
		
		private function computeSteps(deviation:Number, ratio:Number, dist:Number):int
		{
			const STEPS_FACTOR:Number = 0.6;
			const STEPS_FACTOR_TIGHT:Number = 2.0;
			const TIGHT_THRESHOLD:Number = 0.20;
			const DIST_THRESHOLD:Number = 30;
			const STEPS_MIN:Number = 1;
			const STEPS_MAX:Number = 100;

			var tightness:Number = deviation / dist;
			var tight:Boolean = (dist < DIST_THRESHOLD && tightness > TIGHT_THRESHOLD);
			var factor:Number = (tight) ? STEPS_FACTOR_TIGHT : STEPS_FACTOR;
			var steps:int = deviation * factor;
			steps = Math.max(STEPS_MIN, Math.min(steps, STEPS_MAX));
//			trace("steps="+steps, "deviation="+deviation, "dist="+dist, "tight="+tightness, "factor="+factor);
			return steps;
		}
		
		public function cubicLength(p1:Point, c1:Point, c2:Point, p2:Point, steps:int):Number
		{
			var length:Number = 0;
			var bezier:BezierSegment = new BezierSegment(p1,c1,c2,p2);
			var pt1:Point = p1;
			var incr:Number = 1 / steps;
			for (var t:Number =0; t <= 1+incr/2; t+=incr) {
				var pt2:Point = bezier.getValue(t);
				length += pt2.subtract(pt1).length;
				pt1 = pt2;
			}
			return length;
		}
		
		public function cubicPointAlong(p1:Point, c1:Point, c2:Point, p2:Point, length:Number, steps:int):Point
		{
			var bezier:BezierSegment = new BezierSegment(p1,c1,c2,p2);
			var pt1:Point = p1;
			var incr:Number = 1 / steps;
			for (var t:Number=0; t < 1+incr/2; t+=incr) {
				var pt2:Point= bezier.getValue(t);
				var diff:Point = pt2.subtract(pt1);
				var d:Number = diff.length;
				if (d > length) {
					diff.normalize(length);  // interpolate between points
					return pt1.add(diff);
				}
				length -= d;
				pt1 = pt2;
			}
			return pt1;
		}
		
		public function quadraticLength(p1:Point, c1:Point, p2:Point, steps:int):Number
		{
			var length:Number = 0;
			var pt1:Point = p1;
			var incr:Number = 1 / steps;
			for (var t:Number =0; t <= 1+incr/2; t+=incr) {
				var pt2:Point= quadraticPointAt(p1, c1, p2, t);
				length += pt2.subtract(pt1).length;
				pt1 = pt2;
			}
			return length;
		}
		
		public function quadraticPointAlong(p1:Point, c1:Point, p2:Point, length:Number, steps:int):Point
		{
			var pt1:Point = p1;
			var incr:Number = 1 / steps;
			for (var t:Number =0; t <= 1+incr/2; t+=incr) {
				var pt2 :Point= quadraticPointAt(p1, c1, p2, t);
				var diff:Point = pt2.subtract(pt1);
				var d:Number = diff.length;
				if (d > length) {
					diff.normalize(length);  // interpolate between points
					return pt1.add(diff);
				}
				length -= d;
				pt1 = pt2;
			}
			return pt1;
		}
		
		private function quadraticNearestPoint(p1:Point, c1:Point, p2:Point, target:Point, steps:int, outputPoint:Point):Number
		{
			var resultPoint:Point = p1;
			var resultDistance:Number = 0;  // distance along path to the nearest point
			var resultDiff:Number = Point.distance(p1, target);  // default result is p1
			var distance:Number = 0;
			var pt1:Point = p1;
			var incr:Number = 1 / steps;
			for (var frac:Number=0; frac <= 1+incr/2; frac+=incr) {
				var pt2 :Point= quadraticPointAt(p1, c1, p2, frac);
				var near:Point = Geometry.closestPointOnLineSegment(pt1, pt2, target);
				var diff:Number = Point.distance(near, target);
				var dist:Number = Point.distance(pt1, pt2);
				//				trace("  frac="+frac, "near="+near, "pt1="+pt1, "pt2="+pt2, "diff="+diff, "dist="+dist);
				if (diff < resultDiff) {
					resultPoint = near;
					resultDistance = distance + Point.distance(pt1, near);
					resultDiff = diff;
				}
				distance += dist;
				pt1 = pt2;
			}
			outputPoint.x = resultPoint.x;
			outputPoint.y = resultPoint.y;
			return resultDistance;
		}
		
		private function cubicNearestPoint(p1:Point, c1:Point, c2:Point, p2:Point, target:Point, steps:int, outputPoint:Point):Number
		{
			var bezier:BezierSegment = new BezierSegment(p1,c1,c2,p2);
			var resultPoint:Point = p1;
			var resultDistance:Number = 0;  // distance along path to the nearest point
			var resultDiff:Number = Point.distance(p1, target);  // default result is p1
			var distance:Number = 0;
			var pt1:Point = p1;
			var incr:Number = 1 / steps;
			for (var frac:Number =0; frac <= 1+incr/2; frac+=incr) {
				var pt2:Point = bezier.getValue(frac);
				var near:Point = Geometry.closestPointOnLineSegment(pt1, pt2, target);
				var diff:Number = Point.distance(near, target);
				var dist:Number = Point.distance(pt1, pt2);
				if (diff < resultDiff) {
					resultPoint = near;
					resultDistance = distance + Point.distance(pt1, near);
					resultDiff = diff;
				}
				distance += dist;
				pt1 = pt2;
			}
			outputPoint.x = resultPoint.x;
			outputPoint.y = resultPoint.y;
			return resultDistance;
		}
		
		public function quadraticPointAt(p1:Point, c1:Point, p2:Point, t:Number):Point
		{
			return new Point(
				(1-t)*(1-t)*p1.x + 2*(1-t)*t*c1.x + t*t*p2.x,
				(1-t)*(1-t)*p1.y + 2*(1-t)*t*c1.y + t*t*p2.y
			);
		}
		
		private function badPoint(p:Point):Boolean
		{
			if (p == null)
				return false;
			else
				return isNaN(p.x) || isNaN(p.y);
		}
		
		public function get nodeCount():int
		{
			return nodes.length;
		}
						
		public function applyOffset(dx:Number, dy:Number):void
		{
			dx = dx / scaleX;
			dy = dy / scaleY;
			for each (var node:PathNode in nodes) {
				node.x += dx;
				node.y += dy;
				if (node.control1) {
					node.control1.x += dx;
					node.control1.y += dy;
				}
				if (node.control2) {
					node.control2.x += dx;
					node.control2.y += dy;
				}
			}
		}
		
		public function intersectsRect(rect:Rectangle):Boolean
		{
			var count:int = nodeCount;
			for (var n:int=0; n < count; n++){
				var point:Point = getPoint(n);
				if(rect.containsPoint(point))
					return true;
			}
			return false;
		}

		public function get selectedNodeCount():int
		{
			var count:int = 0;
			for each (var node:PathNode in nodes) {
				if (node.selected) {
					++count;
				}
			}
			return count;
		}
		
		public function indexOfNode(node:PathNode):int
		{
			return nodes.indexOf(node);
		}
		
		public function nodeForIndex(index:int):PathNode
		{
			return nodes[index];
		}
		
		/**
		 * Get SVG representation of the path as a string. 
		 * @param func  function (local:Point):Point  --  transform local point to target space.
		 * @return 
		 * 
		 */
		public function getSVGData(func:Function):String
		{
			var result:String = '';
			var limit:int = nodeCount;
			var index:int = 0;
			
			if (nodeCount < 2)
				return result;
			
			var p0:Point, p1:Point, c1:Point, c2:Point;
			p0 = func(getPoint(0));
			result += "M" + (p0.x)+' '+(p0.y);
			
			while (index < limit-1) {
				if (index+1 < limit && nodes[index+1].control1) {
					var spanEnd:int = index+1;
					while (spanEnd+1 < limit && nodes[spanEnd+1].control2)
						++spanEnd;
					if (spanEnd+1 < limit)
						++spanEnd;
					// ------------- create quad
					c1 = func(getControlPoint(index+1,1));
					p1 = func(getPoint(index+1));
					result += " Q"+(c1.x)+","+(c1.y)+" "+(p1.x)+","+(p1.y);
					// ------------- create beziers
					for(var i:int=index+1; i<spanEnd-1; i++) {
						c2 = func(getControlPoint(i,2));
						c1 = func(getControlPoint(i+1,1));
						p1 = func(getPoint(i+1));
						result += " C" +(c2.x)+","+(c2.y)+" "+(c1.x)+","+(c1.y)+" "+(p1.x)+","+(p1.y);
					}
					// ------------- create quad
					var tail:int = spanEnd-1;
					c2 = func(getControlPoint(tail,2));
					p1 = func(getPoint(spanEnd));
					result += " Q"+(c2.x)+","+(c2.y)+" "+(p1.x)+","+(p1.y);
					index = spanEnd;
				} else {
					p1 = func(getPoint(index+1));
					result += " L" + (p1.x)+' '+(p1.y);
					++index;
				}
			}
			return result;
		}
		
		public function buildPolygon(radius:Number, sides:int):void
		{
			deleteAll();
			
			if (sides < 3)
				sides = 3;
			var incr:Number = 360 / sides;
			var start:Number = 90 - incr/2;  // make the first side be horizontal on bottom
			var end:Number = start + 360 + 1;  // +1 to ensure closed
			for (var degrees:Number=start; degrees < end; degrees += incr) {
				var radians:Number = degrees * Math.PI / 180;
				var xx:Number = Math.cos(radians) * radius + radius;
				var yy:Number = Math.sin(radians) * radius + radius;
				addPoint(nodeCount, new Point(xx,yy), true);
			}
		}
		
		public function debugDraw(g:Graphics):void
		{
			g.endFill();
			const RAD:Number = 2;
			const THK:Number = 2;
			for (var n:int=0; n < nodes.length; n++) {
				var c1:Point = getControlPoint(n, 1);
				if (c1) {
					g.lineStyle(THK, 0xff0000);
					g.drawCircle(c1.x, c1.y, RAD);
				}
				var c2:Point = getControlPoint(n, 2);
				if (c2) {
					g.lineStyle(THK, 0x00ff00);
					g.drawCircle(c2.x, c2.y, RAD);
				}
			}
		}
		
		private function fmtPoint(point:Point):String
		{
			if (point == null) {
				return "";
			} else {
				return int(point.x).toString() + "," + int(point.y).toString();
			}
		}
	}
}