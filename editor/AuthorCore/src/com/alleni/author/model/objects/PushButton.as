/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.GraphicFill;
import com.alleni.author.model.ObjectProperties;
	
	import flashx.textLayout.formats.TextAlign;
	import flashx.textLayout.formats.VerticalAlign;

	public class PushButton extends ToggleButton
	{
		public function PushButton()
		{
			super();
			
			margin = 0;
            downColor = new GraphicFill('J[406498,b0bdcc]');  // darker default blue, so state will be visible if switched to non-momentary
			
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("labelPosition"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("labelPadding"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("textWidth"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("checkColor"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("checkWeight"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("checkOpacity"));
		}
	}
}