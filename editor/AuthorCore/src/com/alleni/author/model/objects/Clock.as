/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.model.objects
{
	import com.alleni.author.controller.objects.ClockController;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectOutlets;
	import com.alleni.author.model.ObjectProperties;
	import com.alleni.author.model.ui.Application;

	/**
	 * Value object representing a Clock in the world. 
	 */
	public class Clock extends NativeVisualObject
	{
		[Bindable]
		public var duration:Number = 10; /* duration of clock in seconds */

		public var startTime:Number = 0;  // when not ticking, this is elapsed time in mSecs

        private var _ticking:Boolean = false;  // low-level status of clock: true if measuring time, false if stopped/paused */
		
		[Bindable]
		public var started:Boolean = false;   // high-level status of clock: true if started by Start msg (even if paused by Pause command)

        private var _autorun:Boolean = false; /* whether or not to start upon initialization */
	
		[Bindable]
		public var secondsElapsed:Number = 0; /* how many seconds have passed on the clock */

		[Transient]
		[Bindable]
		public var secondsRemaining:Number = 0; // how many seconds are left
		
		[Transient]
		[Bindable]
		public var percentElapsed:Number = 0;
		
		[Transient]
		[Bindable]
		public var percentRemaining:Number = 100;
		
		[Bindable]
		public var ticksPerCycle:uint = 10;
		
		public var ticksFired:uint = 0;
		
		[Transient]
		public var doingValueUpdate:Boolean;
		
		[Bindable]
		public var cycles:uint = 1;
		
		[Bindable]
		public var cycleNumber:uint = 1;

		[Bindable]
		public var handColor:uint = 0x000000;
		
		[Bindable]
		public var handWeight:Number = 2;
		
		[Bindable]
		public var handAlpha:Number = 100;

        [Bindable]
        public function get ticking():Boolean {
            return _ticking;
        }

        public function set ticking(value:Boolean):void {
            _ticking = value;
            if(!Application.running && !loading && _wireController.bindingsEnabled){  //This happens when we are setting initial value, works like pauseRunningState.
                started = _autorun || value;
            }
        }

        [Bindable]
        public var resumeTicking:Boolean; // When the project is paused what is the state when the project resumes.

        [Bindable]
        public function get autorun():Boolean {
            return _autorun;
        }

        public function set autorun(value:Boolean):void {
            _autorun = value;
        }

        public function Clock() {
			
			var fill:GraphicFill = new GraphicFill();
			fill.color = 0x99B2D1;
			fillSpec = fill;
			maintainAspect = true;
			
			lineThickness = 0;
			this.lineAlpha = 100;
			this.lineColor = 0x124578;
			
			
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("start"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("pause"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("togglePause"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("resume"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("restart"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("stop"));
			// ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("resetTiming")); for future use

			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("duration"), "duration");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("secondsElapsed"), "secondsElapsed");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("secondsRemaining"), "secondsRemaining");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("percentElapsed"), "percentElapsed");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("percentRemaining"), "percentRemaining");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("ticksPerCycle"), "ticksPerCycle");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("cycles"), "cycles");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("cycleNumber"), "cycleNumber");

			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("autorun"), "autorun");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("handColor"), "handColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("handWeight"), "handWeight");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("handAlpha"), "handAlpha");

			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("cyclesFinished"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("cycleFinished"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("tick"));
		}

        override public function onPause():void {
            super.onPause();
            if (active) {
	            resumeTicking = ticking;
	            if (ticking) {
	                ClockController(controller).switchModes(this, false);
	            }
	        }
        }

        override public function onRestoreInitial():void {
            super.onRestoreInitial();
            ClockController(controller).reset(this);
        }

        override public function onResume():void {
            super.onResume();

	        resumeTicking = resumeTicking || _autorun && ticksFired == 0;
            if (resumeTicking && active) {
            	ticking = false;
            	started = true;
            	ClockController(controller).switchModes(this, true);
            }
        }
    }
}
