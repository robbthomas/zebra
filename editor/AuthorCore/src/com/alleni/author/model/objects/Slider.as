/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */

package com.alleni.author.model.objects
{
import com.alleni.author.definition.AbstractModifierDescription;
import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectOutlets;
	import com.alleni.author.model.ObjectProperties;
	import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.UsageCounts;

import flash.geom.Point;
	
	import mx.events.PropertyChangeEvent;

	/**
	 * Value object representing a Slider in the world.
	 * 
	 * All changes go into the "sliderValue" variable, regardless of the source.
	 * This includes binding to sliderPercent, KnobY, or moving the knob
	 * interactively.  The setter for "sliderValue" enforces the bounds and the
	 * units.
	 */
	public class Slider extends NativeVisualObject
	{
		private var _value:Number = 0;  // the current value, used by accessor functions here
		
		[Bindable]
		public var sliderMaximum:Number = 100.0;

		[Bindable]
		public var sliderMinimum:Number = 0.0;
		
		[Bindable]
		public var atMaximum:Boolean;  // true when sliderValue equals sliderMaximum; used to trigger the hitTop outlet message
		
		[Bindable]
		public var atMinimum:Boolean;
		
		[Bindable]
		public var sliderUnit:Number = 1;
		
		[Bindable]
		public var sliderNudgeAmount:Number = 1.0;
		
		[Transient]
		private var _knobFill:GraphicFill = new GraphicFill();
		
		[Bindable]
		public function get knobFill():GraphicFill
		{
			return _knobFill.clone();
		}
		
		public function set knobFill(value:GraphicFill):void
		{
			_knobFill = value;
		}

		
		[Bindable]
		public var knobAlpha:Number = 100;
		
		[Transient]
		[Bindable]
		public var knobX:Number = 0;
		
		[Transient]
		[Bindable]
		public var knobY:Number = 0;  // this is knobY position property visible in ribbon (not knobY for view)
		
		[Transient]
		public var doingKnobPropertyUpdate:Boolean;  // SliderController is updating properties (its not from outside binding)
		
		[Bindable]
		public var sliderKnobGraphic:Asset;
		
		/**
		 *	Returns the Slider sliderValue.
		 */
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get sliderValue():Number
		{
			return _value;
		}

        public function forceSetSliderValue(value:Number):void {
			// value range and offset from sliderMinimum
			var range:Number = sliderMaximum - sliderMinimum;
			var valOffset:Number = value - sliderMinimum;

			// apply units
			if (sliderUnit) {
				var numUnits:int = Math.round(valOffset / sliderUnit);
				valOffset = numUnits * sliderUnit;

				// for units greater than or equal to one, round value to integer
				if (sliderUnit >= 1)
					valOffset = Math.round(valOffset);
			}

			// limit value to ends of scale
			if (sliderMaximum >= sliderMinimum) {
				if (valOffset < 0)
					valOffset = 0;
				else if (valOffset > range)
					valOffset = range;
			} else {  // max is less than min
				if (valOffset > 0)
					valOffset = 0;
				else if (valOffset < range)
					valOffset = range;
			}

            var oldValue:Number = _value;
            var oldPercent:Number = sliderPercent;
			// update the master value, which drives sliderPercent, Knob position, etc
			_value = sliderMinimum + valOffset;

			// values driving hitTop and hitBottom
			atMinimum = (_value == sliderMinimum);
			atMaximum = (_value == sliderMaximum);

			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "sliderValue", oldValue, _value));
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "sliderPercent", oldPercent, sliderPercent));
        }
		
		/**
		 *	Given a value in the caller's units in range from [sliderMinimum, sliderMaximum), set the Slider value.
		 */
		public function set sliderValue(value:Number):void
		{
            if(loading) {
                _value = value;
                return;
            }
            if(_value  != value && !(value != value)) {
                // is a change
                forceSetSliderValue(value);
            }
		}


        [Transient]
        [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get sliderPercent():Number
		{
			var range:Number = sliderMaximum - sliderMinimum;
			if (range == 0) // avoid division by zero
				return 0;
			else
				return 100 * (_value - sliderMinimum) / range;
		}
		
		
		public function set sliderPercent(percent:Number):void
		{
			var range:Number = sliderMaximum - sliderMinimum;
			sliderValue = percent * (range/100) + sliderMinimum;  // use the main setter, so units are enforced
		}
		
		
		
		public function Slider() {
			anchorPoint = new Point(0.5, 0);  // anchorPoint=topCenter

			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("nudgeUp"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("nudgeDown"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("setRandom"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("invertNudge"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("invertAndNudge"));
            ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("enableButton"));
            toolTipOverrides["enableButton"] = "Allow this slider's knob to be moved";
            ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("disableButton"));
            toolTipOverrides["disableButton"] = 'Do not allow this slider\'s knob to be moved \n(set opacity in "Visibility: Opacity- pwr off")';
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("sliderValue"), "sliderValue");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("sliderMaximum"), "sliderMaximum");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("sliderMinimum"), "sliderMinimum");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("sliderPercent"), "sliderPercent");

			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("sliderUnit"), "sliderUnit");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("sliderNudgeAmount"), "sliderNudgeAmount");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("sliderKnobGraphic"), "sliderKnobGraphic");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("knobFill"), "knobFill");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("knobAlpha"), "knobAlpha");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("knobX"), "knobX");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("knobY"), "knobY");

            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("interactive"), "interactive");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("disabledAlpha"), "disabledAlpha");

            ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("hitTop"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("hitBottom"));

            toolTipOverrides["interactive"] = "Can this slider's knob be moved?";
		}
		
		override public function initializeModel(loaded:Boolean):void
		{
			if (!loaded) {
				
				var knob:GraphicFill = new GraphicFill();
				knob.linear = false;
				knob.colors = Vector.<uint>([0xE0EDFC, 0x215282]);
				knob.type = GraphicFill.GRADIENT_FILL;
				knobFill = knob;
			}
		}

        override public function getUsageCounts(usages:UsageCounts):void
        {
            super.getUsageCounts(usages);
            if ("sliderKnobGraphic" in initialValues) {
                usages.countAssetJson(initialValues.sliderKnobGraphic);
            }
        }

	}
}
