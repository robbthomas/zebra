/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/22/11
 * Time: 3:27 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.objects {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.document.Document;
import com.alleni.author.model.AbstractObject;

import com.alleni.author.model.ObjectInlets;

import com.alleni.author.model.ObjectOutlets;
import com.alleni.author.model.ObjectProperties;

import com.alleni.author.model.ui.WireAnchor;

import flash.utils.Dictionary;

import mx.utils.UIDUtil;

public class AbstractConnectionObject extends AbstractObject {
	[Bindable]
	public var local:Boolean = false;

	[Bindable]
	public var key:String = "";

	[Bindable]
	public var receiveData:String = "";

	public function AbstractConnectionObject() {
		modifiers = new Dictionary();
		anchors = new Dictionary();

		hideOnRun = true;
		ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("receive"));
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("key"), "key");
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("receiveData"), "receiveData");
		ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("local"), "local");
		ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("dataReceived"));

	}

	private static var defaultId:String = UIDUtil.createUID();
	protected function getProjectId():String {
		var result:String = Document(ApplicationController.instance.authorController.document).documentId;
		if(result == null) {
			result = defaultId;
		}
		return result;
	}

	public function send():void {}

	public function receive():void {}
}
}
