package com.alleni.author.model.objects
{
    import com.alleni.author.application.ui.EditorUI;
    import com.alleni.author.controller.app.ProjectController;
    import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.InletDescription;
    import com.alleni.author.definition.Modifiers;
    import com.alleni.author.definition.OutletDescription;
    import com.alleni.author.definition.PropertyDescription;
    import com.alleni.author.definition.application.NotificationNamesApplication;
    import com.alleni.author.document.Document;
    import com.alleni.author.model.AbstractObject;
    import com.alleni.author.model.ObjectInlets;
    import com.alleni.author.model.ObjectOutlets;
    import com.alleni.author.model.ObjectProperties;
    import com.alleni.author.model.World;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.model.ui.WireAnchor;
    import com.alleni.taconite.event.ApplicationEvent;
    import com.alleni.taconite.factory.TaconiteFactory;
    import com.alleni.taconite.persistence.json.JSON;
    import com.alleni.author.service.tincan.TinCanClient;
    import com.alleni.taconite.service.LogService;

    import mx.events.PropertyChangeEvent;

    public class LMS extends AbstractObject
    {
        private var _lrs_endpoint:String;
        [Bindable]
        public function get lrs_endpoint():String {
            return _lrs_endpoint;
        }
        public function set lrs_endpoint(value:String):void {
            if (value != _lrs_endpoint) {
                _lrs_endpoint = value;
                lrs_active = false;
            }
        }

        private var _lrs_active:Boolean;
        [Bindable]
        [Transient]
        public function get lrs_active():Boolean {
            return _lrs_active;
        }
        public function set lrs_active(value:Boolean):void {
            if (value != _lrs_active) {
                _lrs_active = value;
                if (_lrs_active) {
                    LogService.info("lrs activated!");
                    WireAnchor(anchors["lrsActivated"]).trigger();
                } else {
                    LogService.info("lrs deactivated!");
                    WireAnchor(anchors["lrsDeactivated"]).trigger();
                }
            }
        }

        [Bindable]
        public var lrs_username:String;
        [Bindable]
        public var lrs_password:String;
        [Bindable]
        public var lrs_api_version:String;

        [Transient]
        public var currentClientConfig:Object;

        private var _activity_id:String;
        [Bindable]
        public function get activity_id():String {
            return _activity_id;
        }
        public function set activity_id(value:String):void {
            if (value != _activity_id) {
                _activity_id = value;
                lrs_active = false;
            }
        }

        [Bindable]
        public var activity_name:String;
        [Bindable]
        public var activity_description:String;
        [Bindable]
        public var activity_type:String;
        
        private var _actor_name;
        [Bindable]
        public function get actor_name():String {
            return _actor_name;
        }

        public function set actor_name(value:String):void {
            value = cleanString(value);
            if (value != null && value.length > 0) {
                _actor_name = value;
                lrs_active = false;
            }
        }

        private static function cleanString(value:String):String {
            return value.replace(/[~\\;:,<>?#{}!@#$%^&*\(\)\[\]]/g, "");
        }

        [Bindable]
        public var statement_verb:String;
        [Bindable]
        public var statement_object:String;

        [Bindable]
        public var statement_include_module:Boolean;
        [Bindable]
        public var statement_include_score:Boolean;
        [Bindable]
        public var statement_include_success:Boolean;
        [Bindable]
        public var statement_include_completion:Boolean;
        
        [Bindable]
        public var result_success:Boolean;
        [Bindable]
        public var result_completion:Boolean;
       
        [Bindable]
        public var module_index:int;

        [Bindable]
        public var statement_preview:String;
        [Bindable]
        public var statement_sent:String;

        [Bindable]
        public var user_id:String;
        [Bindable]
        public var user_firstname:String;
        [Bindable]
        public var user_midname:String;
        [Bindable]
        public var user_lastname:String;
    //    [Bindable]
    //    public var courseTitle:String;

        private var _score:Number = 0;
        [Bindable]
        public var status:String;
        [Bindable]
        public var location:String;

        [Bindable]
        public var score_sendAs:String;

        private var _score_min:Number = 0;

        private var _score_max:Number = 100;

        [Bindable]
        public var score_nudgeAmt:Number = 1;

        public function LMS() {
            super(false); // don't setup any AO properties

            lrs_endpoint = TinCanClient.DEFAULT_ENDPOINT;
            _lrs_active = false;
            lrs_username = TinCanClient.DEFAULT_USERNAME;
            lrs_password = TinCanClient.DEFAULT_PASSWORD;
            lrs_api_version = null;
            activity_type = TinCanClient.DEFAULT_ACTIVITY_TYPE;
            statement_verb = TinCanClient.DEFAULT_VERB;
            module_index = 0;
            statement_include_module = false;
            statement_include_score = false;
            statement_include_success = false;
            statement_include_completion = false;
            result_success = false;
            result_completion = false;

            ObjectInlets.instance.registerInlet(this, new InletDescription("initiateActivity", "", "Initiate activity", "Begin a new activity", Modifiers.EXPERIENCE_API, 1));
            ObjectInlets.instance.registerInlet(this, new InletDescription("sendStatement", "", "Send statement", "Send a simple actor/verb/object statement", Modifiers.EXPERIENCE_API, 2));
            ObjectInlets.instance.registerInlet(this, new InletDescription("updateModule", "", "Update module", "Update status for current module", Modifiers.EXPERIENCE_API, 3));
            ObjectInlets.instance.registerInlet(this, new InletDescription("sendValues", "", "Send values", "Send values to LMS", Modifiers.DO, 1));
            ObjectInlets.instance.registerInlet(this, new InletDescription("score_nudgeUp", "", "Nudge score- up", "Increase score by \"Nudge score amount\"", Modifiers.DO, 2));
            ObjectInlets.instance.registerInlet(this, new InletDescription("score_nudgeDown", "", "Nudge score- down", "Decrease score by \"Nudge score amount\"", Modifiers.DO, 3));
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("lrs_endpoint", "", 
                "LRS- endpoint", "Learning Record Store API endpoint", Modifiers.EXPERIENCE_API, 1, false, -1, 
                Modifiers.STRING_TYPE), "lrs_endpoint");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("lrs_active", "", 
                "LRS- active", "Whether or not the LRS is ready to receive requests", Modifiers.EXPERIENCE_API, 1, false, -1, 
                Modifiers.BOOLEAN_TYPE, null, false), "lrs_active");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("lrs_username", "", 
                "LRS- username", "Learning Record Store username", Modifiers.EXPERIENCE_API, 2, false, -1,
                Modifiers.STRING_TYPE), "lrs_username");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("lrs_password", "", 
                "LRS- password", "Learning Record Store password", Modifiers.EXPERIENCE_API, 3, false, -1,
                Modifiers.STRING_TYPE), "lrs_password");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("lrs_api_version", "", 
                "LRS- API Version", "API version supported by the Learning Record Store", Modifiers.EXPERIENCE_API, 4, false, -1,
                Modifiers.UNSIGNED_NUMBER_TYPE, null, false), "lrs_api_version");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("activity_id", "", 
                "Activity- id", "Activity's unique identifier", Modifiers.EXPERIENCE_API, 10, false, -1, 
                Modifiers.STRING_TYPE, null, false), "activity_id");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("activity_name", "", 
                "Activity- name", "Activity name", Modifiers.EXPERIENCE_API, 11, false, -1, 
                Modifiers.STRING_TYPE, null, false), "activity_name");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("activity_description", "", 
                "Activity- description", "Activity description", Modifiers.EXPERIENCE_API, 12, false, -1, 
                Modifiers.STRING_TYPE, null, false), "activity_description");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("activity_type", "", 
                "Activity- type", "Activity type", Modifiers.EXPERIENCE_API, 13, false, -1, Modifiers.ENUMERATED_TYPE, 
                [
                {label:"Course", data:"course"}, 
                {label:"Module", data:"module"}, 
                {label:"Media", data:"media"}, 
                {label:"Performance", data:"performance"}, 
                {label:"Simulation", data:"simulation"}, 
                {label:"Interaction", data:"interaction"}, 
                {label:"Question", data:"question"}, 
                {label:"Objective", data:"objective"}, 
                {label:"Link", data:"link"}
                ]
                ), "activity_type");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("actor_name", "", 
                "Actor- name", "A name to identify the current user", Modifiers.EXPERIENCE_API, 20, false, -1, 
                Modifiers.STRING_TYPE), "actor_name");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("statement_verb", "", 
                "Statement- verb", "Experience statement activity verb (e.g. attempted, completed, experienced, passed, failed)", 
                Modifiers.EXPERIENCE_API, 30, false, -1, 
                Modifiers.ENUMERATED_TYPE, 
                [
                {label:"Attempted", data:"attempted"}, 
                {label:"Completed", data:"completed"}, 
                {label:"Experienced", data:"experienced"}, 
                {label:"Passed", data:"passed"}, 
                {label:"Failed", data:"failed"}
                ]
                ), "statement_verb");
            /*ObjectProperties.instance.registerProperty(this, new PropertyDescription("statement_object", "", 
                "Statement- object", "Experience activity object", Modifiers.EXPERIENCE_API, 31, false, -1, 
                Modifiers.STRING_TYPE), "statement_object");*/
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("statement_preview", "", 
                "Statement- preview", "A preview of the next statement to be sent", Modifiers.EXPERIENCE_API, 32, false, -1, 
                Modifiers.STRING_TYPE, null, false), "statement_preview");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("statement_sent", "", 
                "Statement- sent", "Most recent statement sent to Learning Record Store", Modifiers.EXPERIENCE_API, 33, false, -1, 
                Modifiers.STRING_TYPE, null, false), "statement_sent");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("statement_include_score", "", 
                "Include- score", "Whether or not the statement should include the score", Modifiers.EXPERIENCE_API, 40, false, -1, 
                Modifiers.BOOLEAN_TYPE), "statement_include_score");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("statement_include_module", "", 
                "Include- module", "Whether or not the statement should include the module index", Modifiers.EXPERIENCE_API, 41, false, -1, 
                Modifiers.BOOLEAN_TYPE), "statement_include_module");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("statement_include_success", "", 
                "Include- success", "Whether or not the statement should specify activity success", Modifiers.EXPERIENCE_API, 42, false, -1, 
                Modifiers.BOOLEAN_TYPE), "statement_include_success");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("statement_include_completion", "", 
                "Include- completion", "Whether or not the statement should specify activity completion", Modifiers.EXPERIENCE_API, 43, false, -1, 
                Modifiers.BOOLEAN_TYPE), "statement_include_completion");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("result_success", "", 
                "Result- success", "Whether or not the activity was done successfully", Modifiers.EXPERIENCE_API, 50, false, -1, 
                Modifiers.BOOLEAN_TYPE), "result_success");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("result_completion", "", 
                "Result- completion", "Whether or not the activity was successfully completed", Modifiers.EXPERIENCE_API, 51, false, -1, 
                Modifiers.BOOLEAN_TYPE), "result_completion");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("module_index", "", 
                "Module- index", "Experience module identifier", Modifiers.EXPERIENCE_API, 60, false, -1, 
                Modifiers.UNSIGNED_NUMBER_TYPE), "module_index");

    //        ObjectProperties.instance.registerProperty(this, new PropertyDescription("courseTitle", "Title", "Title", "", Modifiers.VALUE, 5, false, -1, Modifiers.STRING_TYPE, null, false), "courseTitle");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("score_sendAs", "", "Score- send as", "Send score as number or percent", Modifiers.VALUE, 6, false, -1, Modifiers.ENUMERATED_TYPE, [{label:"Number", data:"number"},{label:"Percent", data:"percent"}]), "score_sendAs");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("score", "", "Score- num", "Score as a number", Modifiers.VALUE, 7, false, -1, Modifiers.NUMBER_TYPE), "score");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("score_percent", "", "Score- %", "Score as a percent between minimum and maximum values", Modifiers.VALUE, 8, false, -1, Modifiers.NUMBER_TYPE), "score_percent");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("score_min", "", "Score- min", "Minimum value of score", Modifiers.VALUE, 9, false, -1, Modifiers.UNSIGNED_NUMBER_TYPE, [0,100]), "score_min");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("score_max", "", "Score- max", "Maximum value of score", Modifiers.VALUE, 10, false, -1, Modifiers.UNSIGNED_NUMBER_TYPE, [0,100]), "score_max");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("score_nudgeAmt", "", "Score- nudge", "Score nudge amount \n(value used to nudge score up or down)", Modifiers.VALUE, 11, false, -1, Modifiers.NUMBER_TYPE), "score_nudgeAmt");

            ObjectProperties.instance.registerProperty(this, new PropertyDescription("user_id", "", "User- id", "User identification code", Modifiers.SCORM, 1, false, -1, Modifiers.STRING_TYPE, null, false), "user_id");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("user_firstname", "", "User- first name", "User's first name", Modifiers.SCORM, 2, false, -1, Modifiers.STRING_TYPE, null, false), "user_firstname");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("user_midname", "", "User- mid name", "User's middle name", Modifiers.SCORM, 3, false, -1, Modifiers.STRING_TYPE, null, false), "user_midname");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("user_lastname", "", "User- last name", "User's last name", Modifiers.SCORM, 4, false, -1, Modifiers.STRING_TYPE, null, false), "user_lastname");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("status", "", "Status", "Lesson status code \n(Use codes recognized by your selected LMS)", Modifiers.SCORM, 12, false, -1, Modifiers.STRING_TYPE), "status");
            ObjectProperties.instance.registerProperty(this, new PropertyDescription("location", "", "Location", "Current lesson location \n(Use terms recognized by your selected LMS)", Modifiers.SCORM, 13, false, -1, Modifiers.STRING_TYPE), "location");
            
            ObjectOutlets.instance.registerOutlet(this, new OutletDescription("lrsActivated", "", "LRS activated", "LRS has been initialized for the current activity", Modifiers.EXPERIENCE_API, 1));
            ObjectOutlets.instance.registerOutlet(this, new OutletDescription("lrsDeactivated", "", "LRS deactivated", "LRS has been deactivated and must be reinitialized to receive statements", Modifiers.EXPERIENCE_API, 2));
            ObjectOutlets.instance.registerOutlet(this, new OutletDescription("statementSuccess", "", "Statement success", "A statement submitted to LRS has succeeded", Modifiers.EXPERIENCE_API, 3));
            ObjectOutlets.instance.registerOutlet(this, new OutletDescription("statementFailure", "", "Statement failure", "A statement submitted to LRS has failed", Modifiers.EXPERIENCE_API, 4));
            ObjectOutlets.instance.registerOutlet(this, new OutletDescription("sendSuccess", "", "Send success", "Submission of values to LMS has succeeded", Modifiers.SCORM, 1));
            ObjectOutlets.instance.registerOutlet(this, new OutletDescription("sendFailure", "", "Send failure", "Submission of values to LMS has failed", Modifiers.SCORM, 2));
            title = "LMS";

            updateStatementPreview();
        }

        override public function onRestoreInitial():void {
            super.onRestoreInitial();

            // reset readonly values that aren't stored by initial values
            user_id = null;
            user_firstname = null;
            user_midname = null;
            user_lastname = null;
        }

        public function getValues():void {
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.LMS_GET_VALUES, {callback:function(values:Object):void {
                handleLmsValuesReady(values);
            }
            }));
        }

        public function initiateExperienceActivity():void {
            currentClientConfig = TinCanClient.initiateActivity(lrs_endpoint, lrs_username, lrs_password,
                activity_id, activity_name, activity_description, activity_type, actor_name, lrs_api_version);
        }

        public function updateStatementPreview():void {
            statement_preview = TinCanClient.formatStatementPreview(this);
        }

        public function sendExperienceStatement(withModule:Boolean=false):void {
            if (lrs_active) {
                var scoreScaled:Number = -1;
                var scoreRaw:Number = -1;
                if (statement_include_score) {
                    if (score_sendAs == "percent") {
                        scoreScaled = score_percent / 100;
                    } else {
                        scoreRaw = score;
                    }
                }

                statement_sent = TinCanClient.sendStatement(
                    statement_verb,
                    ((withModule || statement_include_module) ? module_index : -1),
                    scoreScaled, scoreRaw, _score_min, _score_max,
                    result_success, statement_include_success,
                    result_completion, statement_include_completion,
                    lrs_api_version
                    );
            } else {
                LogService.error("Can't send LRS statements before it's been initialized.")
                // can't send statements before LRS is initialized
                WireAnchor(anchors["statementFailure"]).trigger();
            }
        }

        public function sendModuleExperienceStatement():void {
            sendExperienceStatement(true);
        }

        public function sendValues():void {
            const document:Document = Application.instance.document;
            const world:World = document.root.value as World;
            if(EditorUI.instance || user_id == null || user_id.length == 0) {
                WireAnchor(anchors["sendSuccess"]).trigger(); // simulate success
                return;
            }
            if((world.project.onNextOpen == "menu") || (world.project.onNextOpen == "resume")) {
                ProjectController.instance.saveUserState(this);
            } else {
                sendValuesToLms(null);
            }
        }

        public function handleStateComplete(error:Boolean, id:String):void {
            if(error) {
                WireAnchor(anchors["sendFailure"]).trigger();
            } else {
                sendValuesToLms(id);
            }
        }

        private function sendValuesToLms(id:String):void {
            const document:Document = Application.instance.document;
            const world:World = document.root.value as World;
            var values:Object = {status:status?String(status):"",  location:location?String(location):""};
            if(id) {
                values.suspendData = com.alleni.taconite.persistence.json.JSON.encode({zebraState:id, onNextOpen:world.project.onNextOpen});
            } else {
                values.suspendData = "";
            }
            if(score_sendAs == "percent") {
                values.score_percent = score_percent/100;
            } else {
                values.score = score;
                values.score_min = _score_min;
                values.score_max = _score_max;
            }
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.LMS_SET_VALUES, {lms:this, values:values}));
    		WireAnchor(anchors["sendSuccess"]).trigger();
        }

        public function handleLmsValuesReady(values:Object):void {
            if(values.hasOwnProperty("score")) {
                this.score = values.score;
            }
            if(values.hasOwnProperty("status")) {
                this.status = values.status;
            }
            if(values.hasOwnProperty("location")) {
                this.location = values.location;
            }
            if(values.hasOwnProperty("user_id")) {
                this.user_id = values.user_id;
            }
            if(values.hasOwnProperty("user_firstname")) {
                this.user_firstname = values.user_firstname;
            }
            if(values.hasOwnProperty("user_midname")) {
                this.user_midname = values.user_midname;
            }
            if(values.hasOwnProperty("user_lastname")) {
                this.user_lastname = values.user_lastname;
            }
        }

        public function initializeLms():void {
            getValues();
        }

        public function nudgeScoreUp():void {
            score = _score + score_nudgeAmt;
        }

        public function nudgeScoreDown():void {
            score = _score - score_nudgeAmt;
        }

        //copied from slider object
        public function forceSetScore(value:Number):void {
			// value range and offset from sliderMinimum
			var range:Number = _score_max - _score_min;
			var valOffset:Number = value - _score_min;

			// limit value to ends of scale
			if (_score_max >= _score_min) {
				if (valOffset < 0)
					valOffset = 0;
				else if (valOffset > range)
					valOffset = range;
			} else {  // max is less than min
				if (valOffset > 0)
					valOffset = 0;
				else if (valOffset < range)
					valOffset = range;
			}

            var oldValue:Number = _score;
            var oldPercent:Number = score_percent;
			// update the master value, which drives sliderPercent, Knob position, etc
			_score = _score_min + valOffset;

            if(_score != oldValue) {
                dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "score", oldValue, _score));
                dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "score_percent", oldPercent, score_percent));
            }
        }

        [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
        public function get score():Number {
            return _score;
        }

        public function set score(value:Number):void {
            if(loading) {
                _score = value;
                return;
            }
            if(_score  != value && !(value != value)) {
                // is a change
                forceSetScore(value);
            }
        }

        [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
        public function get score_percent():Number {
            var range:Number = _score_max - _score_min;
            if (range == 0) // avoid division by zero
                return 0;
            else
                return 100 * (_score - _score_min) / range;
        }

        public function set score_percent(value:Number):void {
            var range:Number = _score_max - _score_min;
            score = value * (range/100) + _score_min;  // use the main setter, so units are enforced
        }

    // end from slider

        [Bindable]
        public function get score_min():Number {
            return _score_min;
        }

        public function set score_min(value:Number):void {
            _score_min = value;
            if(!loading) {
                if(_score_max < _score_min) {
                    var oldMax:Number = _score_min;
                    _score_max = _score_min;
                    dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "score_max", oldMax, _score_max));
                }
                forceSetScore(_score);
            }
        }

        [Bindable]
        public function get score_max():Number {
            return _score_max;
        }

        public function set score_max(value:Number):void {
            _score_max = value;
            if(!loading) {
                if(_score_min > _score_max) {
                    var oldMin:Number = _score_min;
                    _score_min = _score_max;
                    dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "score_min", oldMin, _score_min));
                }
                forceSetScore(_score);
            }
        }

        override public function onResume():void {
            super.onResume();
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "activity_id", "", activity_id));
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "activity_name", "", activity_name));
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "activity_description", "", activity_description));
        }
    }
}
