package com.alleni.author.model.objects
{
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectProperties;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class VideoObject extends MediaObject
	{
		[Transient]
		public var contentViewer:Sprite;
		
		[Transient]
		[Bindable]
		public var sourceDimensions:Point;
		
		[Bindable]
		public var highQuality:Boolean = false;
		
		
		public function VideoObject ()
		{
			super();
			
			this.contentViewer = new Sprite();
			this.sourceDimensions = AssetDescription.DEFAULT_VIDEO_SIZE;
			
			//ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("cuePoint"), "cuePoint");
			//ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("cuePointList"), "cuePointList");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("highQuality"), "highQuality");
			
			
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("duration"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("targetCollide"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("overlapsCollisionTarget"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("targetDrop"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("putBackWhen"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("putBackSecs"));
			
			//This is only needed for PBR4, it will be turned back on when working. - rt 3/14/11
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("targetCollide"));
			
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillSpec"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillAlpha"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineColor"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineAlpha"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineThickness"));
		}
	}
}