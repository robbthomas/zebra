/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 */
package com.alleni.author.model.objects
{
	import caurina.transitions.Tweener;

import com.alleni.author.Navigation.EventPage;

import com.alleni.author.controller.TweenController;
	import com.alleni.author.controller.objects.TableHeaderDragMediator;
	import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.WireCreateUtil;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.InletDescription;
    import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.OutletDescription;
	import com.alleni.author.definition.PropertyDescription;
    import com.alleni.author.definition.action.ModifyValueAction;
    import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectOutlets;
	import com.alleni.author.model.ObjectProperties;
	import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.model.ui.Wiring;
    import com.alleni.author.view.objects.tables.AbstractTableView;
    import com.alleni.savana.Ast;
	import com.alleni.taconite.persistence.json.JSON;

import mx.collections.ArrayCollection;
	import mx.collections.IList;

	
	/**
	 * Abstract Table object
	 */
	public class AbstractTable extends LogicObject
	{
		public static const TACONITE_COLLECTIONS:Vector.<String> = Vector.<String>(["guidePositions","port","portValue", "logicRowName", "currentStateValues", "allStateValues", "stateValueExpressions", "stateValueBound", "stateValueName", "stateValueType", "stateValueTypeConstraints", "truthPort", "currentTweenValues", "allTweenValues", "branches"]);
		
		public static const COLLAPSED:int = 0;
		public static const SHOW_TITLES:int = 1;
		public static const EXPANDED:int = 2;
		
		private var _column:int = 1;  // number of the column selected by the input ports (one-based)  (setter & getter below)
		private var _activeColumn:int = 1;
		
		[Bindable]	public var viewState:int = EXPANDED;  // true when truth table view is collapsed horizontally
		
		[Bindable]	public var port:IList = new ArrayCollection(); // Object
		[Bindable]	public var portValue:IList = new ArrayCollection(); // Object
		[Bindable]	public var truthPort:IList = new ArrayCollection(); // Boolean
		
		[Bindable]	public var numColumns:int=0;  // computed to be 2 ** numPorts, but not less than 2
		
		[Bindable]	public var fillString:String = "0-";  // "1-16" or similar
		
		[Bindable]  public var logicRowName:IList = new ArrayCollection(); // saved name for the logic row name
		
		[Bindable]  public var currentStateValues:IList = new ArrayCollection(); // values for current state
		[Bindable]  public var allStateValues:IList = new ArrayCollection(); // values for every state
		[Bindable]  public var stateValueExpressions:IList = new ArrayCollection(); // expression that dictates state values
		[Bindable]  public var stateValueBound:IList = new ArrayCollection(); // Boolean that dictates whether we currently care about the expression
		[Transient] public var stateValueAst:IList = new ArrayCollection(); // AST generated from the expression if indeed it is an evaluatable expression (i.e. not a color)
		[Bindable]  public var stateValueName:IList = new ArrayCollection(); // saved name for the value that might be used as a variable
		[Bindable]  public var stateValueType:IList = new ArrayCollection(); // saved name for the value that might be used as a variable
		[Bindable]  public var stateValueTypeConstraints:IList = new ArrayCollection(); // saved name for the value that might be used as a variable
		
		[Bindable]  public var branches:IList = new ArrayCollection(); // Saves all of the branching states for the columns.
		private var _fireBranches:Boolean = true;
		public var branchingDefault:int;

        [Bindable]
        public var guidePositions:ArrayCollection = new ArrayCollection([-1,-1,-1]);

		public var fireTweens:Boolean = true;
		public static const TWEEN_TYPE:int = 0;
		public static const TWEEN_TIME:int = 1;
		public static const TWEEN_HOLD:int = 2;
		
		[Bindable]  public var currentTweenValues:IList = new ArrayCollection(); // values for current state
		[Bindable]  public var allTweenValues:IList = new ArrayCollection(); // values for every state
		
		// for editing a custom-result value, because its an element of an array
		[Bindable]	public var tempText:String;  // text being edited
		
		// internal var used to prevent recursion when setting ports, resulting from an external binding to "column"
		[Transient] public var ignorePortChanges:Boolean = false;
		
		[Bindable] public var showHeading:Boolean = true;
		[Bindable] public var scrollGrid:Boolean = true;
		[Bindable] public var showControls:Boolean = false;
		[Bindable] public var showLogicRows:Boolean = true;
		[Bindable] public var showValueRows:Boolean = false;
		[Bindable] public var showTweens:Boolean = false;
		[Bindable] public var showOuts:Boolean = true;
		[Bindable] public var showTimeline:Boolean = false;
		[Bindable] public var showBranchingRow:Boolean = true;
		[Bindable] public var continuallyJudging:Boolean = true;
		
		[Transient] [Bindable] public var showScrollControl:Boolean = false;
		[Transient] [Bindable] public var statusMessage:Number = 1;
		
		[Transient] [Bindable] public var rearranging:Boolean = false;
		
		public function AbstractTable()
		{
			currentTweenValues.addItem("");
			currentTweenValues.addItem(0);
			currentTweenValues.addItem(0);
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("currentStateValues"), "currentStateValues");
			//ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("port"), "port");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("truthPort"), "truthPort");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("showControls"), "showControls");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("showLogicRows"), "showLogicRows");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("showValueRows"), "showValueRows");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("showTweens"), "showTweens");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("showOuts"), "showOuts");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("continuallyJudging"), "continuallyJudging");
			
				
			
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("colPort"));
			
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("judgeNow"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("reset"));

			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("first"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("last"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("next"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("previous"));

			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("start"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("pause"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("resume"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("stop"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("togglePause"));
			
			registerColumnProperty();
            registerPortProperty();
		}
		
		private static const readOnlyColumn:PropertyDescription = new PropertyDescription("activeColumn", "3-2-30","Active Column","Active column number",  Modifiers.VALUE, 0,
																							false, -1, Modifiers.UNSIGNED_NUMBER_TYPE, null, false);

        private static const portModifier:PropertyDescription = (new PropertyDescription("port", "3-10-00",	"Port",	"Logic input", Modifiers.PORT, 0, true, 10000, "", null, true, false));
		
		protected function registerColumnProperty():void
		{
			ObjectProperties.instance.registerProperty(this, readOnlyColumn, "activeColumn");
		}

        protected function registerPortProperty():void{
            ObjectProperties.instance.registerProperty(this, portModifier, "port");
        }

		public function getStateValue(row:int, col:int):Object
		{
			if(col < 0) {
				return currentStateValues[row];
			}
			return allStateValues[numColumns * row + col];
		}
		
		public function setStateValue(row:int, col:int, value:Object, asAuthor:Boolean = false):void
		{
			if(col < 0) {
				currentStateValues[row] = value;
				return;
			}
			allStateValues[numColumns * row + col] = value;
			if(asAuthor) {
				recordInitialValue("allStateValues");
			}

			recalcValues(row+1, col);
		}
		
		public function getTweenValue(row:int, col:int):Object
		{
			if(col < 0){
                return currentTweenValues[row];
            }
            return allTweenValues[numColumns * row + col];
		}
		
		public function setTweenValue(row:int, col:int, value:Object, asAuthor:Boolean = false):void
		{   
			if(col < 0) {
				currentTweenValues[row] = value;
				return;
			}
			allTweenValues[numColumns * row + col] = value;
			if(asAuthor) {
				recordInitialValue("allTweenValues");
			}
		}
		
		public function getLogicValue(row:int, col:int):String
		{
			//Must be overridden in subclass
			return null;
		}
		
		public function setLogicValue(row:int, col:int, value:String, asAuthor:Boolean = false):void
		{
			//Must be overridden by the subclass
		}
		
		public function recalcValues(startRow:int,col:int):void
		{
			var env:Object = Ast.defaultEnvironment();
			env["col"] = col + 1;
			for(var row:int = 0; row<currentStateValues.length; row++) {
				var value:Object;
				if(row < startRow || stateValueBound[row] == false) {
					value = getStateValue(row, col);
				} else {
					var ast:Ast = stateValueAst[row];
					if(ast != null) {
						try {
						value = ast.evaluate(env);
						} catch(error:Error) {
							trace(error);
							value = "?";
						}
						if(value is Number && (value == value)) {
							setStateValue(row, col, value);
						} else if(value is Boolean) {
							setStateValue(row, col, value);
						} else {
							setStateValue(row, col, 0);
						}
					} else {
						value = stateValueExpressions[row];
					}
				}
				if(stateValueName[row] && /^[_A-Za-z][_A-Za-z0-9]*$/.test(stateValueName[row])) {
					env[String(stateValueName[row]).toLowerCase()] = value;
				}
			}
		}

		private function isPortValueNumber(index:int):Boolean
		{
			var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchors["port"][index]);
			for each(var w:Wire in wires) {
				if(w.masterAnchor.hostObject == this) {
					if(w.slaveAnchor.isNumericProperty()) {
						return true;
					}
				} else {
					if(w.masterAnchor.isNumericProperty()) {
						return true;
					}
				}
			}
			return false;
		}
		
		public function portBoolean(index:int):Boolean
		{
			if(index >= port.length) {
				return false;
			}
			if(index >= portValue.length) {
				return false;
			}
			if((port[index] is String) && String(port[index]).length == 0 && portValue[index] is Boolean) {
                // this is the default for new rows;
                return portValue[index];
            } else {
				return Modifiers.checkCondition(port[index], portValue[index], isPortValueNumber(index), this);
			}
			return false;
		}
		
		[Bindable]
		public function get column():int
		{
			return _column;
		}
		
		public function set column(value:int):void
		{
			trace("Setting column ", value);
			if (value < 0) {
				value = 0;
			}
			if(value < 1 && (this is TruthTable || this is StateTable)) {
				value = 1;
			}
			if (!loading) {  // when not loading, limit the upper value
				if (value > numColumns)
					value = numColumns;
			}
			if(value == _column) {
				return;
			}
			trace("Setting column ", value);
			_column = value;
			trace("TruthTable set column="+_column, this.title);
		}
		
		[Bindable]
		public function get activeColumn():int
		{
			return _activeColumn;
		}
		
		public function set activeColumn(value:int):void
		{
			if (value < 0) {
				value = 0;
			}
			if(value < 1 && (this is TruthTable || this is StateTable)) {
				value = 1;
			}
			if (!loading) {  // when not loading, limit the upper value
				if (value > numColumns)
					value = numColumns;
			}
			if(value == _activeColumn) {
				return;
			}
			_activeColumn = value;
		}

		public var frac:Number;

		public function handleTween():void
		{
			if(Application.running){
				activeColumn = column;
			}
			var tweenTime:Number = new Number(getTweenValue(TWEEN_TIME, column-1));
			var tweenType:String = getTweenValue(TWEEN_TYPE, column-1) as String;
			if(tweenType == "none" || tweenType == null || tweenType == "" || tweenTime <= 0) {
				tweenTime = 0;
			}
	
			var controllers:Vector.<TweenController> = new Vector.<TweenController>();
	
			for(var row:int=0; row<currentStateValues.length; row++) {
				var newValue:Object = getStateValue(row, column-1);
				var oldValue:Object = currentStateValues[row];
				var type:String = stateValueType[row];

				var twc:TweenController = new TweenController(oldValue, newValue, type);
		        controllers.push(twc);
			}
			frac = 0;
	
			Tweener.addTween(this, {frac:1.0, time:tweenTime, transition:tweenType, onUpdate:updateTween, onUpdateParams:[controllers], onComplete:tweenComplete});
		}
		
		protected function tweenComplete():void
		{
			var anchor:WireAnchor = IList(anchors["colPort"])[column-1] as WireAnchor;
			//trace("TruthTableController::handleColumnChange col="+table.column);
			if (anchor) {
				//trace("TruthTableController: sending trigger for col=",table.column);
				ApplicationController.instance.wireController.anchorTriggered(anchor);
			}
		}

		private function updateTween(controllers:Vector.<TweenController>):void
		{
			for(var row:int=0; row<currentStateValues.length; row++) {
				currentStateValues[row] = controllers[row].computeTween(frac);
			}
		}


		///////////////////////////////////////
		// Row/Column rearrangment functions //
		///////////////////////////////////////

		protected function getWiredAnchors(anchor:WireAnchor):Array
		{
			var result:Array = [];
			var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
			for each(var wire:Wire in wires) {
				if(wire.masterAnchor == anchor) {
                    if(!wire.slaveAnchor.isCreatorHiddenAnchor()) {
					    result.push(wire.slaveAnchor.getJSONLink());
                    }
				} else {
                    if(!wire.masterAnchor.isCreatorHiddenAnchor()) {
					    result.push(wire.masterAnchor.getJSONLink());
                    }
				}
			}
			return result;
		}

		protected function removeWires(anchor:WireAnchor):void
		{
			var wires:Array = ApplicationController.instance.wireController.getWiresForAnchor(anchor);
			for each(var wire:Wire in wires) {
				WireCreateUtil.unCreate(wire);
			}
		}

		protected function removeIndexedAnchor(key:String, index:int):void
		{
			var anchor:WireAnchor;
			var anchors:IList = anchors[key] as IList;
			anchor = anchors.removeItemAt(index) as WireAnchor;
			removeWires(anchor);
			for(var next:int = index; next < anchors.length; next++) {
				anchor = anchors.getItemAt(next) as WireAnchor;
				anchor.hostPropertyIndex--;
			}
		}

		protected function createWires(anchor:WireAnchor,  wiredAnchors:Array, master:Boolean):void
		{
			var wiring:Wiring = Application.instance.document.wiringRoot.value as Wiring;
			for each(var otherAnchor:Object in wiredAnchors) {
				var wireAnchor:WireAnchor = WireAnchor.fromJSONLink(otherAnchor);
				if(wireAnchor){
					if(master) {
						wiring.wires.addItem(new Wire(anchor, wireAnchor));
					} else {
						wiring.wires.addItem(new Wire(wireAnchor, anchor));
					}
				}
			}
		}

		protected function insertIndexedAnchor(key:String, index:int, wiredAnchors:Array, master:Boolean):void
		{
			var anchor:WireAnchor;
			var anchors:IList = anchors[key] as IList;
			var mod:IModifier = modifiers[key] as IModifier;
			if(mod is PropertyDescription) {
				anchor = PropertyDescription(mod).createAnchor(this, key, index);
				anchors.addItemAt(anchor, index);
			} else if(mod is InletDescription) {
				anchor = InletDescription(mod).createAnchor(this, index);
				anchors.addItemAt(anchor, index);
			} else {
				anchor = OutletDescription(mod).createAnchor(this, index);
				anchors.addItemAt(anchor, index);
			}
			createWires(anchor, wiredAnchors, master);
			for(var next:int = index+1; next < anchors.length; next++) {
				anchor = anchors.getItemAt(next) as WireAnchor;
				anchor.hostPropertyIndex++;
			}
		}

		protected function prepValue(obj:Object):Object{
			if(obj is AbstractObject){
				return {type: "AbstractObject",
					id: (obj as AbstractObject).uid
				}
			}
			return com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(obj));
		}
		
		public function copyValueRow(index:int):Object
		{
			var stateValues:Array = [];
			for(var column:int=0; column < numColumns; column++) {
				stateValues[column] = ModifyValueAction.valueToJson(getStateValue(index, column));
			}
			return {
				stateValues:stateValues,
				stateValueWired:getWiredAnchors(IList(anchors["currentStateValues"])[index]),
				stateValueExpressions:stateValueExpressions[index],
				stateValueBound:stateValueBound[index],
				stateValueName:stateValueName[index],
				stateValueType:stateValueType[index],
				stateValueTypeConstraints:stateValueTypeConstraints[index],
                isSelected:index == (getView() as AbstractTableView).currentRow
			};
		}

		public function removeValueRow(index:int):void
		{
			rearranging = true;
			currentStateValues.removeItemAt(index);
			for(var column:int=0; column < numColumns; column++) {
				allStateValues.removeItemAt(numColumns * index + 0);
			}
			stateValueExpressions.removeItemAt(index);
			stateValueBound.removeItemAt(index);
			stateValueAst.removeItemAt(index);
			stateValueName.removeItemAt(index);
			stateValueType.removeItemAt(index);
			stateValueTypeConstraints.removeItemAt(index);
			removeIndexedAnchor("currentStateValues", index);
            if((getView() as AbstractTableView).currentRow == index){
                (getView() as AbstractTableView).setCurrentRow(-1, TableHeaderDragMediator.VALUE_ROWS);
            }
		}

		public function insertValueRow(index:int, values:Object):void
		{
			rearranging = true;
			currentStateValues.addItemAt(values.stateValues[this._column], index);
			for(var column:int=0; column < numColumns; column++) {
				allStateValues.addItemAt(ModifyValueAction.valueFromJson(values.stateValues[column]), numColumns * index + column)
			}
			stateValueExpressions.addItemAt(values.stateValueExpressions,index);
			stateValueBound.addItemAt(values.stateValueBound,index);
			var ast:Ast = null;
			try{
				new Ast(values.stateValueExpressions)
			}catch(e:Error){}
			stateValueAst.addItemAt(ast,index);
			stateValueName.addItemAt(values.stateValueName,index);
			stateValueType.addItemAt(values.stateValueType,index);
			stateValueTypeConstraints.addItemAt(values.stateValueTypeConstraints,index);
			insertIndexedAnchor("currentStateValues", index, values.stateValueWired, true);
            if(values.isSelected){
                (getView() as AbstractTableView).setCurrentRow(index, TableHeaderDragMediator.VALUE_ROWS);
            }
			ApplicationController.instance.wireController.showWiresForLevel();
		}

		public function copyColumn(index:int):Object
		{
			if(index < 0) {
				return {stateValues:currentStateValues.toArray(), tweenValues:currentTweenValues.toArray(), branchValue:branchingDefault}
			}
			var stateValues:Array = [];
			for(var row:int=0; row < currentStateValues.length; row++) {
				stateValues[row] = ModifyValueAction.valueToJson(getStateValue(row, index));
			}
			var tweenValues:Array = [getTweenValue(0,index), getTweenValue(1,index), getTweenValue(2,index)];
			
			var results:Object = new Object();
			results.stateValues = stateValues
			results.tweenValues = tweenValues
			results.outlets = getWiredAnchors(IList(anchors["colPort"])[index]);
			results.branchValue = branches.getItemAt(index);
			return results;
		}

		public function removeColumn(index:int):void
		{
			rearranging = true;
			numColumns--;
            recordInitialValue("numColumns");
			for(var row:int=0; row < currentStateValues.length; row++) {
				allStateValues.removeItemAt(numColumns * row + index);
			}
			allTweenValues.removeItemAt(numColumns * 0 + index);
			allTweenValues.removeItemAt(numColumns * 1 + index);
			allTweenValues.removeItemAt(numColumns * 2 + index);
			branches.removeItemAt(index);
			removeIndexedAnchor("colPort", index);
			column--;
            copySectionToInitialValues(TableHeaderDragMediator.COLUMNS);
		}

		public function insertColumn(index:int, values:Object):void
		{
			rearranging = true;
			numColumns++;
            recordInitialValue("numColumns");
			for(var row:int=0; row < currentStateValues.length; row++) {
                allStateValues.addItemAt(ModifyValueAction.valueFromJson(values.stateValues[row]), numColumns * row + index);
			}
			allTweenValues.addItemAt(values.tweenValues[0], numColumns * 0 + index);
			allTweenValues.addItemAt(values.tweenValues[1], numColumns * 1 + index);
			allTweenValues.addItemAt(values.tweenValues[2], numColumns * 2 + index);
			branches.addItemAt(values.branchValue, index);
			insertIndexedAnchor("colPort", index, values.outlets, true);
		}
		///////////////////////////////////////////
		// End Row/Column rearrangment functions //
		///////////////////////////////////////////
		
		public function copySectionToInitialValues(section:String):void{
			switch(section) {
				case TableHeaderDragMediator.COLUMNS:
					initialValues["allStateValues"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["allStateValues"]));
					initialValues["allTweenValues"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["allTweenValues"]));
					initialValues["branches"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["branches"]));
					break;
				case TableHeaderDragMediator.LOGIC_ROWS:
					initialValues["port"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["port"]));
					initialValues["portValue"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["portValue"]));
					initialValues["allLogicValues"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["allLogicValues"]));
					initialValues["allLogicFulfilled"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["allLogicFulfilled"]));
					initialValues["logicRowName"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["logicRowName"]));
					initialValues["truthPort"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["truthPort"]));
					break;
				case TableHeaderDragMediator.VALUE_ROWS:
					initialValues["currentStateValues"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["currentStateValues"]));
					initialValues["allStateValues"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["allStateValues"]));
					initialValues["stateValueExpressions"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["stateValueExpressions"]));
					initialValues["stateValueBound"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["stateValueBound"]));
					initialValues["stateValueAst"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["stateValueAst"]));
					initialValues["stateValueName"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["stateValueName"]));
					initialValues["stateValueType"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["stateValueType"]));
					initialValues["stateValueTypeConstraints"] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this["stateValueTypeConstraints"]));
					break;
			}
		}
		
		public function get fireBranches():Boolean{
			return _fireBranches;
		}
		public function set fireBranches(value:Boolean):void{
			_fireBranches = value;
		}

        override protected function initializeValues():void {
            super.initializeValues();
			var row:int;
			var length:int = currentTweenValues.length;
			for(row = 0; row < length; row++) {
				currentTweenValues[row] = getTweenValue(row, column-1);
			}
			length = currentStateValues.length;
			for(row = 0; row < length; row++) {
				currentStateValues[row] = getStateValue(row, column-1);
			}
        }

        override public function getUsageCounts(usages:UsageCounts):void
        {
            super.getUsageCounts(usages);
            if ("allStateValues" in initialValues) {
                var len:int = initialValues.allStateValues.length;
                for (var n:int = 0; n < len; n++) {
                    var json:Object = initialValues.allStateValues.source[n];
                    usages.countAssetJson(json);  // ignores non-asset
                }
            }
        }

        public function updateForAssetChange():void
        {
            if (!loading) {
                LibraryController.instance.updateForAssetChange();
            }
        }
    }
}


