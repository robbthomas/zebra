package com.alleni.author.model.objects
{
	import com.alleni.author.definition.InspectorRibbonDescriptions;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.action.ActionGroup;
	import com.alleni.author.model.ObjectProperties;
	import com.alleni.author.model.ui.WireAnchor;
	
	public class Drawing extends AbstractExternalObject
	{
		[Bindable]
		public var resizeImage:String = "native";
		
		[Transient]
		public var pendingMakeGraphicAction:ActionGroup;
		
		public function Drawing() {
			maintainAspect = true;
			
			// Register
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("lineColor"), "lineColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("lineThickness"), "lineThickness");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("lineAlpha"), "lineAlpha");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("replaceable"), "replaceable");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("resizeImage"), "resizeImage");
			
			// Unregister
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillAlpha"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillSpec"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineColor"));	
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineThickness"));	
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineAlpha"));

            var inspectorShowing:Array = [];
            inspectorShowing =inspectorShowing.concat(InspectorRibbonDescriptions.DRAWING_ACTIONS_RIBBONS).concat(InspectorRibbonDescriptions.DRAWING_GEOMETRY_RIBBONS).concat(InspectorRibbonDescriptions.DRAWING_VISIBILITY_RIBBONS).concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
		    for each (var key:String in inspectorShowing) {
                if (anchors[key] as WireAnchor) {
                    (anchors[key] as WireAnchor).propertyInspectorVisible = true;
                }
            }
        }
		
		override public function toString():String
		{
			var assetStr:String = " asset=";
			if (asset) {
				assetStr += asset.assetID + " \"" + asset.name + "\"";
				if ("asset" in initialValues && initialValues.asset != null && "assetID" in initialValues.asset) {
					var init:String = initialValues.asset.assetID;
					var initName:String = initialValues.asset.name;
					assetStr += "  " + ((init != asset.assetID) ? "INIT" : "init") + "=" + init + " \"" + initName + "\"";
				}
			}
			return "[" + shortClassName + " \"" + title+"\" XY="+int(x)+","+int(y) + assetStr + " uid="+uid + "]";
		}
	}
}