/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 *
 * @author Allen Skunkworks
 */
package com.alleni.author.model.objects
{
import com.alleni.author.Navigation.FlowEditingMediator;
import com.alleni.author.Navigation.IPage;
import com.alleni.author.definition.InspectorRibbonDescriptions;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ObjectProperties;
import com.alleni.author.model.World;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.view.ObjectView;
import com.alleni.taconite.view.ViewContext;

import flash.geom.Point;
import flash.utils.Dictionary;

import mx.collections.ArrayCollection;

import mx.collections.IList;


/**
	 * ArenaPage = one page inside an Arena object.
	 */
	public class ArenaPage extends AbstractContainer implements IPage
	{
        public static const TACONITE_CHILDREN_PROPERTY:String = "objects";
        public static const TACONITE_COLLECTIONS:Vector.<String> = Vector.<String>(["masters"]);

		[Bindable] public var contentWidth:Number = 0;
        [Bindable] public var contentHeight:Number = 0;
        [Bindable] public var objCount:int = 0;

        private var _pageNumber:Number;
        private var _masters:IList = new ArrayCollection();

        [Bindable] public var isMaster:Boolean = false;

        // only for masters
        [Bindable] public var isForeground:Boolean;
        [Bindable] public var moveWithTransition:Boolean;

        // ***** properties mirrored in Arena
        [Bindable] public var layout:uint;
        [Bindable] public var marginAll:Number;
        [Bindable] public var marginLeft:Number;
        [Bindable] public var marginTop:Number;
        [Bindable] public var marginBottom:Number;
        [Bindable] public var marginRight:Number;
        [Bindable] public var alignObjsHorz:String;
        [Bindable] public var alignObjsVert:String;
        [Bindable] public var alignCellsHorz:String;
        [Bindable] public var alignCellsVert:String;
        [Bindable] public var hGap:Number;
        [Bindable] public var vGap:Number;
        [Bindable] public var rows:Number;
        [Bindable] public var columns:Number;
        [Bindable] public var defaultColumnsApplied:Boolean = false;
        [Bindable] public var growHorizontal:Boolean;
        [Bindable] public var insertIndicatorColor:uint;
        [Bindable] public var insertIndicatorActive:Boolean;
        [Bindable] public var insertIndicatorWeight:Number;
        [Bindable] public var layoutTweenType:String;
        [Bindable] public var layoutTweenTime:Number;
        // ***** end of mirrored properties

		// recalc:  protection against recursion
        [Transient] [Bindable] public var transientRequestRecalc:Boolean = false;       // recalc requested
        [Transient] [Bindable] public var transientDoingRecalc:Boolean = false;       // now doing recalc of layout

        [Transient] [Bindable] public var transientDraggingChild:Boolean = false;
        [Transient] [Bindable] public var transientAuthorDrag:Boolean = false;
        [Transient] [Bindable] public var transientDraggingCells:* = null;  // cells array kept only while dragging
        [Transient] [Bindable] public var transientTweens:Dictionary = new Dictionary();  // key=propName.uid, value=true
        [Transient] public var transientObjsDraggedIn:Dictionary = null;  // key=uid, value=true   Objects dragged into arena on this drag
        [Transient] [Bindable] public var transientOverIndex:Number = -1;


		private var _hadChildren:Boolean;  // set true when first child added. never cleared

		public function ArenaPage() {
            super(false);  // setupProperties=false, so we don't get the default ribbons

			// *** mirrored with Arena
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("layout"), "layout");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("marginAll"), "marginAll");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("marginLeft"), "marginLeft");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("marginTop"), "marginTop");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("marginBottom"), "marginBottom");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("marginRight"), "marginRight");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alignCellsHorz"), "alignCellsHorz");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alignCellsVert"), "alignCellsVert");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alignObjsHorz"), "alignObjsHorz");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alignObjsVert"), "alignObjsVert");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("hGap"), "hGap");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("vGap"), "vGap");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("rows"), "rows");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("columns"), "columns");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("insertIndicatorColor"), "insertIndicatorColor");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("insertIndicatorActive"), "insertIndicatorActive");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("insertIndicatorWeight"), "insertIndicatorWeight");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("layoutTweenType"), "layoutTweenType");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("layoutTweenTime"), "layoutTweenTime");
			// *** end of mirrored properties

			this.anchorPoint = new Point(0, 0);
            this.marginAll = 0;
		}

        override protected function initializeRunningState():void {
            trace("ArenaPage:initializeRunningState")
            transientDoingRecalc = true;
            doingLayerReorder = true;
            super.initializeRunningState();
            transientDoingRecalc = false;
            doingLayerReorder = false;
            transientTweens = new Dictionary();  // onRestoreInitial() cancels all tweens
        }

        public function setupInspectorRibbons():void
        {
            // creating a path by ctrl-drag
            var inspectorShowing:Array = [];
            inspectorShowing =inspectorShowing.concat(InspectorRibbonDescriptions.PAGE_INSPECTOR_RIBBONS);
            for each (var key:String in inspectorShowing) {
                if (anchors[key] as WireAnchor) {
                    (anchors[key] as WireAnchor).propertyInspectorVisible = true;
                }
            }
        }

        [Bindable]
        public function get pageNumber():int
        {
            return _pageNumber;
        }

        public function set pageNumber(value:int):void
        {
            _pageNumber = value;
        }

        [Bindable]
        public function get masters():IList
        {
            return _masters;
        }

        public function set masters(value:IList):void
        {
            _masters = value;
        }

		override public function get anchorPoint():Point  {
			return new Point(0,0);  // anchorPoint is always topLeft
		}

        override public function get rotation():Number {
        	return 0;
        }

        override public function get title():String
        {
            var ttl:String = super.title;
            return (ttl == "Arena page") ? "Untitled" : ttl;
        }

        [Bindable]
        public function get hadChildren():Boolean
        {
            return _hadChildren;
        }

        public function set hadChildren(value:Boolean):void
        {
            _hadChildren = value;
        }

        [Transient]
        public function get object():AbstractContainer
        {
            return this as AbstractContainer;
        }

        // Arena pages cannot be reordered or reparented at runtime
        override public function resetParent(world:World):void{}
        override public function resetZIndex():void{}


        [Transient]
        override public function toString():String
        {
            return "[" + shortClassName + " \"" + title+"\"  num="+pageNumber+" active="+active+"]";
        }


	}
}
