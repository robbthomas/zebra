/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 */
package com.alleni.author.model.objects
{
import com.alleni.author.Navigation.IPage;
import com.alleni.author.Navigation.Pager;
import com.alleni.author.controller.objects.ArenaController;
import com.alleni.author.controller.objects.ArenaLayoutManager;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ObjectInlets;
import com.alleni.author.model.ObjectOutlets;
import com.alleni.author.model.ObjectProperties;
import com.alleni.author.model.ui.Application;
import com.alleni.taconite.model.TaconiteModel;

import flash.events.Event;

import flashx.textLayout.container.ScrollPolicy;

public class Arena extends Pager
	{
        [Transient] public static const TACONITE_MODEL_LIST_PROPERTIES:Vector.<String> =  new <String>["masterPages","pages"];
        [Transient] public static const TACONITE_COLLECTIONS:Vector.<String> = new <String>["historyStack"];

		private var _vertScrollPosition:Number = 0;
		private var _horzScrollPosition:Number = 0;

		[Bindable] public var pageRate:Number = 1;
        private var _autorun:Boolean = false;
		[Bindable] public var noSkip:Boolean = false;
		[Bindable] public var actualRate:Number = 0;
		[Bindable] public var contentWidth:Number = 0;
		[Bindable] public var contentHeight:Number = 0;
		[Bindable] public var clippedWidth:Number = 0;
		[Bindable] public var clippedHeight:Number = 0;
		[Bindable] public var vertScrollPolicy:String = ScrollPolicy.OFF;
		[Bindable] public var horzScrollPolicy:String = ScrollPolicy.OFF;
		[Bindable] public var scrollThumbColor:uint = 0xA5A5A5;
		[Bindable] public var scrollThumbOverColor:uint = 0x91ADC9;
		[Bindable] public var scrollThumbAlpha:Number = 100;
		[Bindable] public var scrollThumbLineColor:uint = 0xCCCCCC;
		[Bindable] public var scrollTrackColor:uint = 0xF5F5F5;
		[Bindable] public var scrollTrackAlpha:Number = 100;
		[Bindable] public var scrollTrackLineColor:uint = 0xC5C5C5;

        [Bindable] public var resumeTicking:Boolean; // When the project is paused what is the state of the arena when the project resumes.
        [Transient] public var transientPauseWhileDragging:Boolean = false;  // after dropping obj into arena, pause until mouseUp

        // ******* properties mirrored in ArenaPage
        public static const PAGE_LAYOUT_PROPERTIES:Array = [ "layout","marginAll","marginLeft","marginTop","marginBottom","marginRight",
            "alignObjsHorz","alignObjsVert","alignCellsHorz","alignCellsVert","hGap","vGap","rows","columns", "defaultColumnsApplied",
            "insertIndicatorColor","insertIndicatorActive","insertIndicatorWeight","layoutTweenType","layoutTweenTime" ];
        [Bindable] public var layout:uint = ArenaLayoutManager.NONE;
        [Bindable] public var marginAll:Number = 0;
        [Bindable] public var marginLeft:Number = 0;
        [Bindable] public var marginTop:Number = 0;
        [Bindable] public var marginBottom:Number = 0;
        [Bindable] public var marginRight:Number = 0;
        [Bindable] public var alignObjsHorz:String = "";
        [Bindable] public var alignObjsVert:String = "";
        [Bindable] public var alignCellsHorz:String = "";
        [Bindable] public var alignCellsVert:String = "";
        [Bindable] public var hGap:Number = 0;
        [Bindable] public var vGap:Number = 0;
        [Bindable] public var rows:Number = 1;
        [Bindable] public var columns:Number = 1;
        [Bindable] public var defaultColumnsApplied:Boolean = false;
        [Bindable] public var insertIndicatorColor:uint = 0x990000;
        [Bindable] public var insertIndicatorActive:Boolean = true;
        [Bindable] public var insertIndicatorWeight:Number = 3;
        [Bindable] public var layoutTweenType:String = 'linear';
        [Bindable] public var layoutTweenTime:Number = 0.1;
        // ******* end of mirrored properties



        override protected function initializeValues():void
        {
            super.initializeValues();
            transientPauseWhileDragging = false;
        }


		public function set vertScrollPosition(value:Number):void
		{
			var result:Number;
			if(value<0)
				result = 0;
			else if(value>100)
				result = 100;
			else
				result = value
			_vertScrollPosition = result;
		}
		[Bindable] public function get vertScrollPosition():Number
		{
			return _vertScrollPosition;
		}

		public function set horzScrollPosition(value:Number):void
		{
			var result:Number;
			if(value<0)
				result = 0;
			else if(value>100)
				result = 100;
			else
				result = value
			_horzScrollPosition = result;
		}
		[Bindable] public function get horzScrollPosition():Number
		{
			return _horzScrollPosition;
		}

        [Transient] public function get currentArenaPage():ArenaPage
        {
            var page:IPage = currentPage;
            return (page) ? page.object as ArenaPage : null;
        }


        [Bindable]
        public function get autorun():Boolean {
            return _autorun;
        }

        public function set autorun(value:Boolean):void {
            if (_autorun == value) return;
            _autorun = value;
            if(Application.running == false && !loading && _wireController.bindingsEnabled) {
                resumeTicking = _autorun || (initialValues.hasOwnProperty("ticking") && (initialValues.ticking == true))
            }
        }

        [Bindable]
        public function get ticking():Boolean {
            return _ticking;
        }

        public function set ticking(value:Boolean):void {
            if (_ticking == value) return;
            _ticking = value;
            if(Application.running == false && !loading && _wireController.bindingsEnabled){  //This happens when we are setting initial value, works like pauseRunningState.
                resumeTicking = _autorun || value;
            }
        }

        [Transient] [Bindable] public var transientDrawingScrollers:Boolean = false;
		[Transient] [Bindable] public var transientVScrollActive:Boolean = false;
		[Transient] [Bindable] public var transientHScrollActive:Boolean = false;


		// animated page flipping (internal info not available to user)
        private var _ticking:Boolean = false;
		public var startTime:Number;  // time when the current page appeared
		public var clockCycles:int;  // for measuring Rate Actual
		public var lastReportTime:Number; // control the updating of ActualRate property

		
		public function Arena()
        {
            super();
			this.lineThickness = 1;
			this.lineAlpha = 100;
			this.lineColor = 0x42617A;


			// *** mirrored with ArenaPage
            addMirroredProperties(PAGE_LAYOUT_PROPERTIES);
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("layout"), "layout");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("marginAll"), "marginAll");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("marginLeft"), "marginLeft");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("marginTop"), "marginTop");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("marginBottom"), "marginBottom");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("marginRight"), "marginRight");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alignCellsHorz"), "alignCellsHorz");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alignCellsVert"), "alignCellsVert");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alignObjsHorz"), "alignObjsHorz");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alignObjsVert"), "alignObjsVert");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("hGap"), "hGap");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("vGap"), "vGap");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("rows"), "rows");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("columns"), "columns");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("insertIndicatorColor"), "insertIndicatorColor");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("insertIndicatorActive"), "insertIndicatorActive");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("insertIndicatorWeight"), "insertIndicatorWeight");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("layoutTweenType"), "layoutTweenType");
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("layoutTweenTime"), "layoutTweenTime");
            // **** end of mirrored properties

			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("autorun"), "autorun");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("pageRate"), "pageRate");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("noSkip"), "noSkip");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("actualRate"), "actualRate");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("accepting"), "accepting");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("releasing"), "releasing");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("contentWidth"), "contentWidth");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("contentHeight"), "contentHeight");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("clippedWidth"), "clippedWidth");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("clippedHeight"), "clippedHeight");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("vertScrollPolicy"), "vertScrollPolicy");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("horzScrollPolicy"), "horzScrollPolicy");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("vertScrollPosition"), "vertScrollPosition");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("horzScrollPosition"), "horzScrollPosition");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollThumbColor"), "scrollThumbColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollThumbOverColor"), "scrollThumbOverColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollThumbAlpha"), "scrollThumbAlpha");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollThumbLineColor"), "scrollThumbLineColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollTrackColor"), "scrollTrackColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollTrackAlpha"), "scrollTrackAlpha");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scrollTrackLineColor"), "scrollTrackLineColor");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("cloakFrame"), "cloakFrame");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("controlsVisible"), "controlsVisible");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("controlsCloaked"), "controlsCloaked");

			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("start"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("pause"));

			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("objAdded"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("objRemoved"));

            ObjectInlets.instance.registerInlet(this, new InletDescription("resetObject", "", "Reset properties", "Reset properties to initial values\nof this arena and its objects", Modifiers.DO, 34));
        }

//		override public function selectableModels(includeLocked:Boolean):Vector.<TaconiteModel>
//		{
//			var models:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
//			if (currentPage) {
//				for each(var child:AbstractObject in currentPage.objects) {
//					if((includeLocked || !child.locked) && (child.showAll || child.visible)) {
//						models.push(child.model);
//					}
//				}
//			}
//			return models;
//		}

        override protected function initializeRunningState():void {
            trace("Arena:initializeRunningState")
            super.initializeRunningState();    // initialize arena values (children are initialized later, but with bindings off)
            transientDrawingScrollers = false;
        }

        override protected function pauseRunningState():void {
            super.pauseRunningState();
            resumeTicking = _ticking;
            ArenaController(controller).stopClock(this);
            if(controlsCloaked && initialValues.hasOwnProperty("controlsVisible") && (initialValues.controlsVisible == true) && Application.instance.editWhileRunningInProgress == false) {
                controlsVisible = true;
            }
            trace("Arena pauseRunningState: pager="+this, "controlVis="+this.controlsVisible, "resumeControlsVisible="+this.resumeControlsVisible);
        }

        override protected function resumeRunningState():void {
            super.resumeRunningState();
            if(resumeTicking && pageRate!=0) {
               ArenaController(controller).startClock(this);
            }
            controlsVisible = resumeControlsVisible;
            trace("Arena resumeRunningState: pager="+this, "controlVis="+this.controlsVisible, "resumeControlsVisible="+this.resumeControlsVisible);
        }

        [Transient]
        override public function get pageClassname():String
        {
            return "ArenaPage";
        }


	}
}
