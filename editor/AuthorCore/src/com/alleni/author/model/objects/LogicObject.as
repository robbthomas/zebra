package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectProperties;
	
	public class LogicObject extends AbstractObject
	{
		[Bindable] public var poweredOnRun:Boolean = true;
		[Transient] public var noMessageCenter:Boolean;	 // presence of var signals no MsgCtr will be created
		[Bindable] public var minimized:Boolean = false;
		
		public function LogicObject()
		{
			super();
			
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("powerOn"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("powerOff"));
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("poweredOnRun"), "poweredOnRun");
			
			this.hideOnRun = true;
		}
	}
}