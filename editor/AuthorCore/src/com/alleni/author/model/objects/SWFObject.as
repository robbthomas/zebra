package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectProperties;
import com.alleni.author.model.ui.SwfWireAnchor;
import com.alleni.author.model.ui.SwfWireAnchor;
import com.alleni.taconite.service.LogService;

import flash.events.EventDispatcher;

import mx.events.PropertyChangeEvent;

public class SWFObject extends AbstractExternalObject
	{
		public var currentFrame:int = 0;
		
		[Bindable]
		public var autorun:Boolean = false;

		public function SWFObject()
		{
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("autorun"), "autorun");
			
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("start"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("pause"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("resume"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("restart"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("stop"));
		}

		public function setupContent():void
		{
			if(content.hasOwnProperty("objectProperties")) {
				for each(var prop:Object in content["objectProperties"]) {
					if(anchors[prop.name]) {
						continue;
					}
					anchors[prop.name] = SwfWireAnchor.prop(this, prop.name, prop.label, prop.category, prop.type);
				}
			}
			if(content.hasOwnProperty("objectInlets")) {
				for each(var inlet:Object in content["objectInlets"]) {
					if(anchors[inlet.name]) {
						continue;
					}
					anchors[inlet.name] = SwfWireAnchor.inlet(this, inlet.name, inlet.label, inlet.category);
				}
			}
			if(content.hasOwnProperty("objectOutlets")) {
				for each(var outlet:Object in content["objectOutlets"]) {
					if(anchors[outlet.name]) {
						continue;
					}
					anchors[outlet.name] = SwfWireAnchor.outlet(this, outlet.name, outlet.label, outlet.category);
                    if(content is EventDispatcher) {
                        EventDispatcher(content).addEventListener(outlet.name, swfOutletListener(outlet.name));
                    }
				}
			}
			if(content is EventDispatcher) {
				EventDispatcher(content).addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, swfPropChanged);
			}
		}

		public function swfPropChanged(event:Object):void
		{
			var anchor:SwfWireAnchor = anchors[event.property] as SwfWireAnchor;
			if(anchor) {
				anchor.valueChanged();
				anchor.trigger();
			}
		}

        protected function swfOutletListener(key:String):Function {
            return function(event:Object):void
            {
                var anchor:SwfWireAnchor = anchors[key] as SwfWireAnchor;
                if(anchor) {
                    anchor.trigger();
                }
            }
        }

        public function swfHandleInlet(key:String):void {
            try {
                Function(content[key]).apply(content);
            } catch(error:Error) {
                LogService.error("Error calling swf inlet ["+key+"]: " + error.message);
            }
        }
	}
}