package com.alleni.author.model.objects {
import com.alleni.author.Navigation.BackstageView;
import com.alleni.author.Navigation.EventFlowView;
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.FlowView;
import com.alleni.author.Navigation.IPage;
import com.alleni.author.Navigation.Pager;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.action.ActionTree;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.project.Project;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.persistence.GadgetUtils;
import com.alleni.author.util.Set;
import com.alleni.author.util.cache.LRUCache;

import flash.events.Event;

import flash.geom.Point;
import flash.utils.Dictionary;


public class EventFlow extends Pager {

    [Bindable] public var flowVisible:Boolean = true;  // store the view state in the saved project
    [Bindable] public var cacheSize:int = DEFAULT_CACHE_SIZE;
    [Transient] [Bindable] public var disableLoadUnload:Boolean;  // true when FlowView showing
    public var wasEditingEvent:int;  // store event index in the saved project   (0 is first master, etc) (-1 means null)

    [Transient] public static const TACONITE_CHILDREN_PROPERTY:String = "objects";
    [Transient] public static const TACONITE_MODEL_LIST_PROPERTIES:Vector.<String> = new <String>["masterPages","pages"];
    [Transient] public static const TACONITE_COLLECTIONS:Vector.<String> = new <String>["historyStack"];

    [Transient] public static const MAX_MASTERS:int = 12;
    [Transient] public static const MAX_PAGES:int = 100;
    [Transient] public static const DEFAULT_CACHE_SIZE:int = 6;

    [Transient] public var noMessageCenter:Boolean;	 // presence of var signals no MsgCtr will be created

    [Transient] public var eventPagerView:EventFlowView;
    [Transient] public var flowView:FlowView;
    [Transient] public var backstageView:BackstageView;

    [Transient] [Bindable] public var overlayImage:IPage; // should only be set via EventFlowController.showOverlayImage()

    [Transient] public var masterShownForRollover:IPage;
    [Transient] public var doingEditTween:Boolean;
    [Transient] public var doingTweenToNextPage:Boolean;

    [Transient] [Bindable] public var titleBarVisible:Boolean;
    [Transient] [Bindable] public var titleBarRunning:Boolean;  // tracks uiRunning, but can be set early during tween before uiRunning becomes true
    [Transient] [Bindable] public var titleBarPage:IPage;  // tracks flow.currentPage & flow.editingPage, but can be set early for tween

    [Transient] public var loadedPages:Set = new Set(false);
    [Transient] public var pageCache:LRUCache = new LRUCache(DEFAULT_CACHE_SIZE, DEFAULT_CACHE_SIZE);    // the most-recently-needed pages


    [Transient]
    public function get wasEditingEventPage():EventPage {
        if(editingPage != null) {
            return editingPage as EventPage;
        }
        if(wasEditingEvent >=0 && wasEditingEvent < (masterPages.length + pages.length)) {
            return getPageForIndex(wasEditingEvent);
        }
        return null;
    }

    [Transient]
    public var actionTree:ActionTree = new ActionTree();


    public function EventFlow() {
        super();
        anchorPoint = new Point(0,0);
        lineThickness = 0;
    }


    [Transient] public function get currentEventPage():EventPage {
        return currentPage as EventPage;
    }

    [Transient] public function get editingEventPage():EventPage {
        return editingPage as EventPage;
    }

    public function getPageForIndex(index:int):EventPage
    {
        var masterCount:int = masterPages.length;
        var pageCount:int = pages.length;
        if (index < 0) {
            return null;
        } else if (index < masterCount) {
            return masterPages[index] as EventPage;
        } else if (index < masterCount+pageCount) {
            return pages[index - masterCount] as EventPage;
        } else {
            return null;
        }
    }

    public function getIndexForPage(page:IPage):int
    {
        var index:int = masterPages.getItemIndex(page);
        if (index >= 0) {
            return index;
        } else {
            index = pages.getItemIndex(page);
            if (index >= 0) {
                return index + masterPages.length;
            }
        }
        return -1;
    }


    [Transient]
    override public function get pageClassname():String
    {
        return "EventPage";
    }

    override public function onPause():void {
        super.onPause();
        EventFlowController(controller).changePage(this, currentPage as EventPage);
    }

    override public function onResume():void {
        EventFlowController(controller).changePage(this, currentPage as EventPage);
        super.onResume();
    }

    public function pageInCache(page:EventPage):Boolean
    {
        return (pageCache.getValueWithoutUpdating(page));
    }


    public function allBackstageObjects():Vector.<AbstractObject>
    {
        var ao:AbstractObject;
        var result:Vector.<AbstractObject> = new Vector.<AbstractObject>();
        for each (ao in this.objects) {
            result = result.concat(ao.allObjects());
        }
        return result;
    }

    public function backstageObjectsInOuterWiringScope():Vector.<AbstractObject>
    {
        var scope:AbstractContainer = this.wireScopeContainer;
        var ao:AbstractObject;
        var result:Vector.<AbstractObject> = new Vector.<AbstractObject>();
        for each (ao in this.objects) {
            ao.allObjectsInScope(function(obj:AbstractObject):Boolean {
                if (obj.wireScopeContainer == scope) {
                    result.push(obj);
                }
                return true;
            });
        }
        return result;
    }

    public function objectsCurrentPageOuterScope():Vector.<AbstractObject>
    {
        if (Application.instance.flowVisible) {
            return backstageObjectsInOuterWiringScope();
        } else {
            var page:EventPage = editingEventPage ? editingEventPage : currentEventPage;
            return page.allObjectsInWiringScope();
        }
    }

    override public function getUsageCounts(usages:UsageCounts):void
    {
        // not calling super
        for each (var page:EventPage in pages) {
            page.getUsageCounts(usages);
        }
        for each (var master:EventPage in masterPages) {
            master.getUsageCounts(usages);
        }
        // careful not to recurse into child gadgets (assets would be double counted)
        for each (var obj:AbstractObject in backstageObjectsInOuterWiringScope()) {
            obj.getUsageCounts(usages);
        }
    }

    public function getItemLocalUsageCount(item:IReplaceable):int
    {
        trace("getItemLocalUsageCount item="+item.name);
        var usages:UsageCounts = new UsageCounts();
        for each (var obj:AbstractObject in objectsCurrentPageOuterScope()) {
            trace("    ",obj, "runningTotal="+usages.getReplaceableUsageCount(item));
            obj.getUsageCounts(usages);
        }
        return usages.getReplaceableUsageCount(item);
    }

    public function getItemGlobalUsageCount(item:IReplaceable):int
    {
        var usages:UsageCounts = new UsageCounts();
        getUsageCounts(usages);
        return usages.getReplaceableUsageCount(item);
    }

    public function usageCountsJson():Object
    {
        // the cache of usageCounts an each page, stored in root gadget
        var result:Object = {};
        var json:Object, path:String;
        for each (var page:EventPage in pages) {
            if (page.childObjUsageCounts) {
                json = page.childObjUsageCounts.toJson();
                path = this.objectPathArray(page).join(",");
                result[path] = json;
            }
        }
        for each (var master:EventPage in masterPages) {
            if (master && master.childObjUsageCounts) {
                json = master.childObjUsageCounts.toJson();
                path = this.objectPathArray(master).join(",");
                result[path] = json;
            }
        }
        return result;
    }

    public function loadUsageCountsJson(jsonList:Object):void
    {
        for (var path:String in jsonList) {
            var json:Object = jsonList[path];
            var pathArray:Array = path.split(",");
            var page:EventPage = this.resolveObjectPathArray(pathArray) as EventPage;
            var usage:UsageCounts = UsageCounts.fromJson(json);
            page.childObjUsageCounts = usage;
        }
    }

    public function get validationEnabled():Boolean
    {
        return Application.instance.validationEnabled && !abandoned;
    }

    public function handleValidationChange():void
    {
        // its been enabled or disabled
//        AuthorController.instance.log("EventValidation="+validationEnabled);
        if (validationEnabled) {
            ApplicationController.addEventListener(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS, inputEnabledListener);
            AuthorController.instance.eventFlow.validate("validationEnabled");
        } else {
            ApplicationController.removeEventListener(NotificationNamesApplication.UNMUTE_APPLICATION_HOT_KEYS, inputEnabledListener);
        }
    }

    private function inputEnabledListener(e:Event):void
    {
        if (validationEnabled) {
            AuthorController.instance.eventFlow.validate("inputEnabled");
        }
    }

    public function validate(label:String):void
    {
        if (validationEnabled) {
            var error:String = validateToString();
            if (error == null) {
                AuthorController.instance.log(" EventFlow is valid.");
            } else {
                var text:String = "Validation FAILURE: " + error + "  When="+label;
                AuthorController.instance.log(text);
                throw new Error(text);
            }
        }
    }

    public function validateForKeypress():void
    {
        AuthorController.instance.log("validateForKeypress");
        var error:String = validateToString();
        if (error == null) {
            AuthorController.instance.log("  EventFlow is valid.");
        } else {
            AuthorController.instance.log("INVALID: "+error);
        }
    }

    public function validateToString():String
    {
        var error:String;
        error = checkUniqueUIDs();
        if (error != null) {
            return error;
        }
        var page:EventPage;
        for each (page in pages) {
            error = validatePage(page);
            if (error != null) {
                return error;
            }
        }
        for each (page in masterPages) {
            error = validatePage(page);
            if (error != null) {
                return error;
            }
        }
        GadgetUtils.validateLiveObjects(backstageObjectsInOuterWiringScope(), Application.instance.document.project);

        return null;
    }

    private function validatePage(page:EventPage):String
    {
        if (!this.disableLoadUnload) {
            var activePage:EventPage = editingEventPage ? editingEventPage : currentEventPage;
            var needed:Boolean = (page == activePage || activePage.masters.getItemIndex(page) >= 0);
            var loaded:Boolean = page.hasInternals;
            var cached:Boolean = pageInCache(page);
            if (loaded != (needed || cached)) {
                return "Cache err: loaded="+loaded + " needed="+needed + " cached="+cached + " page="+page.logStr;
            }
        }
        var error:String = page.validate();
        if (error != null) {
            return error + '  Page="' + page.title + '"';
        }
        return null;
    }


    private function checkUniqueUIDs():String
    {
        var duplicate:Boolean = false;
        var objs:Vector.<AbstractObject> = AuthorController.instance.world.allObjects();
        var ids:Dictionary = new Dictionary();
        for each (var obj:AbstractObject in objs) {
            if ((obj.uid in ids)) {
                duplicate = true;
                var concatenatedTitles:String = (ids[obj.uid] as String) + ":" + obj.title;
                ids[obj.uid] = concatenatedTitles;
            } else {
                ids[obj.uid] = obj.title;
            }
        }
        if (!duplicate) {
            return null;
        }
//        trace("checkUniqueUIDs:  duplicates:");
//        for (var id:String in ids) {
//            var val:String = ids[id];
//            trace("   id="+id, "objs="+val);
//        }
        return "Duplicate UIDs";
    }

    private var _abandoned:Boolean;

    public function get abandoned():Boolean
    {
        return _abandoned;
    }

    public function set abandoned(value:Boolean):void
    {
        _abandoned = value;
        handleValidationChange();
    }

}
}
