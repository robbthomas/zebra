/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.model.objects
{
import com.alleni.author.definition.AbstractModifierDescription;
import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectProperties;
	
	import flashx.textLayout.formats.TextAlign;
	import flashx.textLayout.formats.VerticalAlign;
	
	public class Checkbox extends ToggleButton
	{
		[Bindable] public var checkMark:Boolean = false;
		[Bindable] public var checkWeight:Number = 3;
		
		public function Checkbox()
		{
			super();
			
			momentary = false;
			textAlignH = TextAlign.LEFT;
			textAlignV = VerticalAlign.MIDDLE;
			cornerRadius = 5;
			margin = 0;
			maintainAspect = true;
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("checkWeight"), "checkWeight");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("checkMark"), "checkMark");
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("textAlignV"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("momentary"));

            toolTipOverrides["enableButton"] = "Allow this checkbox to be checked";
            toolTipOverrides["disableButton"] = 'Do not allow this checkbox to be checked \n(deactivates "Triggers: Button press" - other ribbons remain active) \n(set opacity in "Visibility: Opacity- pwr off")';

            toolTipOverrides["interactive"] = 'Can this checkbox be pressed?\n(affects "Triggers: Button press" - other ribbons remain active)';
		}
	}
}