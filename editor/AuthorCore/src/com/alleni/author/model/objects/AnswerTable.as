/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 */
package com.alleni.author.model.objects
{
	import com.alleni.author.controller.objects.AnswerTableController;
	import com.alleni.author.controller.objects.TableHeaderDragMediator;
	import com.alleni.author.controller.ui.ApplicationController;
    import com.alleni.author.definition.Modifiers
    import com.alleni.author.definition.action.ModifyValueAction;
    import com.alleni.author.model.ObjectProperties;
    import com.alleni.author.view.objects.tables.AbstractTableView;
    import com.alleni.taconite.lang.TaconiteTimer;
	import com.alleni.taconite.persistence.json.JSONEncoder;
	
	import flash.events.TimerEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;

	/**
	 * Answer table object
	 */
	public class AnswerTable extends AbstractTable
	{
		public static const TACONITE_COLLECTIONS:Vector.<String> = Vector.<String>(["guidePositions","port","portValue", "logicRowName", "currentStateValues", "allStateValues", "stateValueExpressions", "stateValueBound", "stateValueName", "stateValueType", "stateValueTypeConstraints", "truthPort", "currentTweenValues", "allTweenValues", "allLogicValues", "allLogicFulfilled", "branches"]);
		
		[Bindable]  public var allLogicValues:IList =  new ArrayCollection(); // values for every logic grid cell
		[Bindable]  public var allLogicFulfilled:IList = new ArrayCollection(); // boolean for if a cell has been fulfilled                                  
		[Transient] public var logicColumns:IList = new ArrayCollection();
		
		[Transient] private var holdStartTime:int = 0;
		private static var _taconiteTimer:TaconiteTimer;
		
		public var lastTriggeredRow:int;
		
		public static const CHECK_MARK:String = "\u2713";
		public static const X_MARK:String = "\u2717";

		public function AnswerTable()
		{			
			branchingDefault = 0;
			super();
			column = 0;
			_taconiteTimer = TaconiteTimer.instance;
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("statusMessage"), "statusMessage");
		}

		override public function getLogicValue(row:int, col:int):String
		{
			if(col < 0) {
				return truthPort[row];
			}
			return allLogicValues[numColumns * row + col];
		}

		override public function setLogicValue(row:int, col:int, value:String, asAuthor:Boolean = false):void
		{
			if(col < 0) {
				truthPort[row] = value;
				return;
			}
			if(allLogicValues[numColumns * row + col] == value) {
				return;
			}
			allLogicValues[numColumns * row + col] = value;
			
			for(var i:int=0; i<allLogicValues.length; i++){
				trace("i", i, "LogicValue", allLogicValues[i]);	
			}
			
			logicColumns[col] = parse(col);
			setCurrentlyFulfilled(row,  col);
            if(continuallyJudging && poweredOnRun) {
                column = firstFulfilledColumn()+1;
            }
			if(asAuthor) {
				recordInitialValue("allLogicValues");
			}
		}

		public function getLogicFulfilled(row:int, col:int):Boolean
		{
			if(col < 0) {
				return allLogicFulfilled[row];
			}
			return allLogicFulfilled[numColumns * row + col];
		}

		public function setLogicFulfilled(row:int, col:int, value:Boolean):void
		{
			if(col < 0) {
				allLogicFulfilled[row] = value;
				return;
			}
			allLogicFulfilled[numColumns * row + col] = value;
		}

        public function setCurrentlyFulfilled(row:int,  col:int):Boolean {
            var logic:String = getLogicValue(row,  col);
            var truth:Boolean = truthPort[row];
            if(logic == AnswerTable.CHECK_MARK) {
                setLogicFulfilled(row, col, truth);
                return truth;
            } else if(logic == AnswerTable.X_MARK) {
                setLogicFulfilled(row, col, !truth);
                return !truth;
            } else {
                setLogicFulfilled(row, col, false);
                return false;
            }
        }

		public function parse(column:int):Object {
			var result:Object = {type:"unordered", children:[]};
			var ordered:Object = {type:"ordered", children:[]};
			for(var row:int=0; row<port.length; row++) {
				var logic:String = getLogicValue(row, column);
				if(logic == null || logic == "") {
					// whether this row is fulfilled or not doesn't matter to this column
					continue;
				} else if(logic == AnswerTable.CHECK_MARK) {
					result.children.push({type:"state", row:row, positive:true});
				} else if(logic == AnswerTable.X_MARK) {
					result.children.push({type:"state", row:row, positive:false});
				} else if(logic.match(/^\d+$/)) {
					var index:int = new Number(logic);
					if(index in ordered.children) {
						// already have one with this order number
						if(ordered.children[index].type != "unordered") {
							// create a new unordered list to hold the existing one and this new one
							ordered.children[index] = {type:"unordered", children:[ordered.children[index]]};
						}
						// add this new one to the existing unordered list
						ordered.children[index].children.push({type:"event", row:row, positive:true});
					} else {
						// this one is the first or only one with this order number
						ordered.children[index] = {type:"event", row:row, positive:true};
					}
				}
			}
			// add any where order matter to the master list
			if(ordered.children.length > 0) {
				result.children.push(ordered);
			}
            result = simplifyRule(result);
			trace("Col:", column, "Rule:", new JSONEncoder(result).getString());
			return result;
		}

        protected static function simplifyRule(rule:Object):Object {
			if(rule == null) {
				return null;
			}
			var subRule:Object;
			switch(rule.type) {
				case "state":
				case "event":
						return rule;
				case "ordered":
				case "unordered":
                    var newChildren:Array = [];
                    for(var index:Object in rule.children) {
                        subRule = simplifyRule(rule.children[index]);
                        if(subRule != null) {
                            newChildren[index] = subRule;
                        }
                    }
                    if(newChildren.length == 0) {
                        return null;
                    } else if(newChildren.length == 1) {
                        return subRule;
                    } else {
                        rule.children = newChildren;
                        return rule;
                    }
			}
            return null; // shouldn't ever get here
        }
		
		public function copyLogicRow(index:int):Object {
			var logicValues:Array = [];
			for(var column:int=0; column < numColumns; column++) {
				logicValues[column] = ModifyValueAction.valueFromJson(getLogicValue(index, column));
			}
			return {
				logicValues:logicValues,
				logicRowName:logicRowName[index],
				port:port[index],
				portValue:portValue[index],
				truthPort:truthPort[index],
				portWired:getWiredAnchors(IList(anchors["port"])[index]),
				truthPortWired:getWiredAnchors(IList(anchors["truthPort"])[index]),
                isSelected:index == (getView() as AbstractTableView).currentRow
			};
		}
		
		public function removeLogicRow(index:int):void {
			rearranging = true;
			port.removeItemAt(index)
			portValue.removeItemAt(index);
            var column:int;
			for(column=0; column < numColumns; column++) {
				allLogicValues.removeItemAt(numColumns * index + 0);
				allLogicFulfilled.removeItemAt(numColumns * index + 0);
			}
			logicRowName.removeItemAt(index);
			truthPort.removeItemAt(index);
			removeIndexedAnchor("port", index);
			removeIndexedAnchor("truthPort", index);
			if((getView() as AbstractTableView).currentRow == index){
                (getView() as AbstractTableView).setCurrentRow(-1, TableHeaderDragMediator.LOGIC_ROWS);
            }
			for(column=0; column < numColumns; column++) {
                logicColumns[column] = parse(column);
            }
		}
		
		public function insertLogicRow(index:int, values:Object):void {
			rearranging = true;
			port.addItemAt(values.port, index);
			portValue.addItemAt(values.portValue, index);
            var column:int;
			for(column=0; column < numColumns; column++) {
				allLogicValues.addItemAt(ModifyValueAction.valueFromJson(values.logicValues[column]), numColumns * index + column);
                allLogicFulfilled.addItemAt(false, numColumns * index + column);
			}
			logicRowName.addItemAt(values.logicRowName, index);
			truthPort.addItemAt(values.truthPort, index);
			insertIndexedAnchor("port", index, values.portWired, false);
			insertIndexedAnchor("truthPort", index, values.truthPortWired, true);
            if(values.isSelected){
                (getView() as AbstractTableView).setCurrentRow(index, TableHeaderDragMediator.LOGIC_ROWS);
            }
			ApplicationController.instance.wireController.showWiresForLevel();
			for(column=0; column < numColumns; column++) {
                logicColumns[column] = parse(column);
                setCurrentlyFulfilled(index,  column);
            }
		}
		
		override public function copyColumn(index:int):Object {
			var results:Object;
			results = super.copyColumn(index);
			
			var logicValues:Array = [];
			for(var row:int=0; row < port.length; row++) {
				logicValues[row] = getLogicValue(row, index);
			}
			
			results.logicValues = logicValues;
			return results;
		}
		
		override public function removeColumn(index:int):void {
			//addingColumn = false;
			
			super.removeColumn(index);
			
			for(var row:int=0; row < port.length; row++) {
				allLogicValues.removeItemAt(numColumns * row + index);
                allLogicFulfilled.removeItemAt(numColumns * row + index);
			}
			logicColumns.removeItemAt(index);
		}
		
		override public function insertColumn(index:int, values:Object):void {
			//addingColumn = true;
			
			super.insertColumn(index, values);
			
			for(var row:int=0; row < port.length; row++) {
				allLogicValues.addItemAt(values.logicValues[row], numColumns * row + index);
				allLogicFulfilled.addItemAt(false, numColumns * row + index);
			}
			var ast:Object = parse(index);
			logicColumns.addItemAt(ast, index);
			for(var row:int=0; row < port.length; row++) {
                setCurrentlyFulfilled(row,  index);
			}
		}
		
		override public function copySectionToInitialValues(section:String):void{
			switch(section) {
				case TableHeaderDragMediator.COLUMNS:
                    recordInitialValue("allStateValues");
                    recordInitialValue("allTweenValues");
                    recordInitialValue("branches");
                    recordInitialValue("allLogicValues");
					break;
				case TableHeaderDragMediator.LOGIC_ROWS:
					super.copySectionToInitialValues(section);
					break;
				case TableHeaderDragMediator.VALUE_ROWS:
					super.copySectionToInitialValues(section);
					break;
			}
		}
		
		public function fulfill(triggeredRow:int, column:int, positive:Boolean):Boolean {
//			if(triggeredRow < 0) {
//				for(var i:int=0; i<allLogicFulfilled.length; i++) {
//					allLogicFulfilled[i] = false;
//				}
//				return false;
//			}
			lastTriggeredRow = triggeredRow;
			return fulfillRule(triggeredRow, column, logicColumns[column], positive);
		}

        public function firstFulfilledColumn():int {
            for(var column:int=0; column < numColumns; column++) {
                if(isRuleFulfilled(column,  logicColumns[column])) {
                    return column
                }
            }
            return -1;
        }

        private function isRuleFulfilled(column:int, rule:Object):Boolean {
			if(rule == null) {
				return true;
			}
			var subRule:Object;
			switch(rule.type) {
				case "state":
				case "event":
						return getLogicFulfilled(rule.row, column);
				case "ordered":
					for each(subRule in rule.children) {
						if(!isRuleFulfilled(column, subRule)) {
							return false;
						}
					}
					return true;
				case "unordered":
					var allFulfilled:Boolean = true;
					for each(subRule in rule.children) {
						if(!isRuleFulfilled(column, subRule)) {
							allFulfilled = false;
						}
					}
					return allFulfilled;

			}
			return false;
        }

		private function fulfillRule(triggeredRow:int, column:int, rule:Object, positive:Boolean):Boolean {
			if(rule == null) {
				return true;
			}
			var subRule:Object;
			switch(rule.type) {
				case "event":
						if(rule.row == triggeredRow && rule.positive == positive) {
                            setLogicFulfilled(triggeredRow, column,  true);
                            return true;
                        } else {
							return getLogicFulfilled(rule.row, column);
						}
				case "state":
						if(rule.row == triggeredRow) {
                            var fulfilled:Boolean = positive == rule.positive;
							setLogicFulfilled(triggeredRow, column, fulfilled);
							return fulfilled;
						} else {
							return getLogicFulfilled(rule.row, column);
						}
				case "ordered":
					for each(subRule in rule.children) {
						if(!fulfillRule(triggeredRow, column, subRule, positive)) {
							return false;
						}
					}
					return true;
				case "unordered":
					var allFulfilled:Boolean = true;
					for each(subRule in rule.children) {
						if(!fulfillRule(triggeredRow, column, subRule, positive)) {
							allFulfilled = false;
						}
					}
					return allFulfilled;

			}
			return false;
		}

		public function clearFulfilled():void {
			for(var row:int=0; row<port.length; row++) {
                for(var col:int = 0; col<numColumns; col++) {
                    setCurrentlyFulfilled(row,  col);
                }
			}
            column = 0;
            if(continuallyJudging && poweredOnRun) {
                column = firstFulfilledColumn()+1;
            }
		}
		
		override protected function tweenComplete():void
		{
			super.tweenComplete();
			if(branches[column-1] != 5){
				holdStartTime = _taconiteTimer.milliseconds;
				_taconiteTimer.registerListener(holdTimerListener);
			}else{
				branch();
			}
		}
		
		private function holdTimerListener(event:TimerEvent):void
		{
			var holdTime:int = currentTweenValues[2]*1000
			var currentTime:int = _taconiteTimer.milliseconds;
			if(holdStartTime + holdTime < currentTime) {
				_taconiteTimer.unregisterListener(holdTimerListener);
				branch();
			}
		}
		
		private function branch():void{
			if(fireBranches && column > 0){
				switch(branches[column-1]){ // 1 = continue, 0 = pause continue, 2 = try again, 3 = exit, 4 = exit power down.
					case 0:
						trace("Pause Continue");
						break;
					case 1:
						AnswerTableController.instance.handleContiuneBranch(this, lastTriggeredRow, truthPort[lastTriggeredRow], column);
						break;
					case 2:
						column = -1;
						clearFulfilled();
						break;
					case 3:
						column = -1;
						AnswerTableController.instance.reset(model.value as AbstractTable);
						break;
					case 4:
						column = -1;
						AnswerTableController.instance.reset(model.value as AbstractTable);
						if(poweredOnRun){
							poweredOnRun = !poweredOnRun;
						}
						break;
				}
			}else{
				fireBranches = true;
			}
		}

        override public function onRestoreInitial():void {
            AnswerTableController(controller).makeValueRowsSafe(this);
            super.onRestoreInitial();
		    AnswerTableController(controller).reset(this);
        }
	}
}