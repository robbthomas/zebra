// ActionScript file
package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ObjectOutlets;
	import com.alleni.author.model.ObjectProperties;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;
	
	public class CalcSwitch extends LogicObject
	{
		[Bindable]
		public var value:Object = "";
		
		[Bindable]
		public var condition:IList = new ArrayCollection();
		
/*		public var noMessageCenter:Object;*/
		
		public function CalcSwitch()
		{
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("value"), "value");
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("output"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("outputDefault"));
		}
		
	}
}