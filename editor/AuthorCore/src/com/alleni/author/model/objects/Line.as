/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.ObjectProperties;
	import com.alleni.author.view.ObjectView;
	import com.alleni.taconite.lang.Geometry;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
		
	/**
	 * Value object representing a Line in the world. 
	 */
	public class Line extends VectorObject
	{
		public function Line()
		{
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillAlpha"));	
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillSpec"));
		}

		/**
		 * 
		 * @return 
		 * 
		 */
		override public function get isAnimationPath():Boolean
		{
			return true;
		}
		
		/**
		 * 
		 * @param frac
		 * @return 
		 * 
		 */
		override public function pathPointForFraction(frac:Number):Point
		{
			var p1:Point = new Point(left,top);
			var p2:Point = new Point(right, bottom);
			return Point.interpolate(p2, p1, frac);
		}
		
		/**
		 * 
		 * @param local
		 * @param outputPoint
		 * @return 
		 * 
		 */
		override public function closestPointOnPath(local:Point, outputPoint:Point=null):Number
		{
			var result:Point;
			var p1:Point = new Point(left,top);
			var p2:Point = new Point(right, bottom);
			var frac:Number;
			if (p1.equals(p2)) {
				frac = 0;
				result = p1;
			} else {
				result = Geometry.closestPointOnLineSegment(p1, p2, local);
				frac = Point.distance(result, p1) / Point.distance(p1,p2);
			}
			if (outputPoint) {
				outputPoint.x = result.x;
				outputPoint.y = result.y;
			}
			return frac;
		}
		
		/**
		 * 
		 * @param local
		 * @param frac
		 * @param outputPoint
		 * @return 
		 * 
		 */
		override public function moveToward(local:Point, frac:Number, outputPoint:Point=null):Number
		{
			return closestPointOnPath(local, outputPoint);
		}
	}
}
