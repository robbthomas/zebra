package com.alleni.author.model.objects {
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.WireController;
	import com.alleni.author.definition.InspectorRibbonDescriptions;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.event.PathEvent;
	import com.alleni.author.model.ObjectProperties;
    import com.alleni.author.model.ui.Application;
    import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.PathNodeWireAnchor;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.view.ObjectView;
	import com.alleni.author.view.objects.PathObjectView;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	

	public class PathObject extends VectorObject
	{
		public static const DEFAULT_SMOOTHING:Number = 30;
		
		[Bindable]
		public var path:Path = new Path();
		
		[Bindable]
		public var viewBox:Rectangle;
		
		public var nextLinkSerial:int = 1;
				
		[Bindable]
		public var bitmapFill:Asset;
		
		[Bindable]
		public var pathSpacing:int = DEFAULT_SMOOTHING;
		
		[Transient]
		public var polygonCreation:Boolean;

        private var _bornAsPolygon:Boolean = false;
		
				
		[Transient]
		public var extendingPath:Boolean;
		
		private var _needsRecalc:Boolean;
		private var _needsRedraw:Boolean = true;  // request redraw of loaded path
		private var _needsReSnap:Boolean; 
		
		
		public function PathObject()
		{			
			maintainAspect = true;
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("bitmapFill"), "bitmapFill");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("maintainAspect"), "maintainAspect");
		}
		
		public function get nodeCount():int
		{
			return path.nodeCount;
		}
		
		public function get scaleX():Number
		{
			// this is copied to obj.path.scaleX
			// it scales the placement of points from the original creation point locations.
			// (it does not scale the view and therefore doesn't affect line thickness)
			if(viewBox && viewBox.width > 0) {
				return width / viewBox.width;
			} else {
				return 1;
			}
		}
		
		public function get scaleY():Number
		{
			if(viewBox && viewBox.height > 0) {
				return height / viewBox.height;
			} else {
				return 1;
			}
		}
		
		override public function set width(value:Number):void
		{
			super.width = value;
			updateScaling();
			requestRecalc();
		}
		
		override public function set height(value:Number):void
		{
			super.height = value;
			updateScaling();
			requestRecalc();
		}
		
		public function updateScaling():void
		{
			path.scaleX = scaleX;
			path.scaleY = scaleY;
		}
		
		override public function initializeModel(loaded:Boolean):void
		{
			if (!loaded) {
				fillAlpha = 0;  // default to no fill
				lineThickness = 3;
				anchorPoint = new Point(0,0);
			}
		}

		public function extendPath(pos:Point, sharp:Boolean, suppliedIndex:int=-1):Boolean
		{
			var addAnotherNode:Boolean = false;
			var index:int = (suppliedIndex >= 0) ? suppliedIndex : path.nodeCount -1;
			if (path.nodeCount < 2 || index < 1) {
				addAnotherNode = true;
			} else if (!sharp && !path.getSharp(index)) {
				addAnotherNode = Point.distance(path.getPoint(index-1), pos) > pathSpacing;
			}  else if (sharp) {
				addAnotherNode = Point.distance(path.getPoint(index-1), pos) > 5;
			}
//			trace("extend: sharp="+sharp, "supplied="+suppliedIndex, "index="+index, "add="+addAnotherNode, "count="+nodeCount);
					
			if (addAnotherNode) {
				var lastSharp:Boolean = path.getSharp(index);  // insert the new node before the last one (since last node is dragging)
				var lastPoint:Point = path.getPoint(index);
				addPoint(index, lastPoint, sharp);  // author probably dragging very last point:  insert new one before last point
				setPoint(index+1, pos);  // set the very last point to the mouse position
				if (!sharp && nodeCount == 2 && path.getSharp(1))
					setSharp(1, false);  // make the 2nd point round
			} else {
				setPoint(index, pos);
			}
			return addAnotherNode;
		}
		
		public function addPoint(index:int, point:Point, sharp:Boolean):void
		{
			// debug:
//			trace("addPoint: index="+index, "sharp="+sharp, "count="+nodeCount);
//			this.path.dump();
			
			path.addPoint(index, point, sharp);
			var event:PathEvent = new PathEvent(PathEvent.ADD_POINT); 
			event.node = path.nodeForIndex(index);
			dispatchEvent(event);
			requestRecalc();
		}
		
		public function moveSelectedNodes(dx:Number, dy:Number):void
		{
			var count:int = nodeCount;
			for (var n:int=0; n < count; n++) {
				if (path.getSelected(n)) {
					var pt:Point = path.getPoint(n);
					pt.x += dx;
					pt.y += dy;
					setPoint(n, pt);
				}
			}
		}
		
		public function setPoint(index:int, pos:Point):void
		{
//			trace("setPoint: index="+index, "count="+nodeCount, "point="+pos);
//			Utilities.dumpStack();

			path.setPoint(index, pos);
			handleNodeMoved(index);
		}
				
		public function setSharp(index:int, sharp:Boolean):void
		{
//			trace("setSharp: index="+index, "sharp="+sharp, "count="+nodeCount);
//			Utilities.dumpStack();

			path.setSharp(index, sharp);
			requestRecalc();
		}
		
		public function selectNode(index:int, select:Boolean=true, extend:Boolean=false):void
		{
			if (!extend)
				selectAll(false);
			path.setSelected(index, select);
			requestUpdate();
		}
		
		public function selectAll(select:Boolean=true):void
		{
			path.selectAll(select);
			requestUpdate();
		}

		public function deletePoint(index:int):void
		{
			var node:PathNode = path.nodeForIndex(index);
			path.deletePoint(index);
			var event:PathEvent = new PathEvent(PathEvent.DELETE_POINT); 
			event.node = node;
			dispatchEvent(event);   // removes rectangular hilite around a node that has a ribbon
			requestRecalc();
		}
				
		private function handleNodeMoved(index:int):void
		{
			requestRecalc();
			
			var info:PathNodeInfo = path.getAnchorInfo(index);
			if (info) {
				var anchor:WireAnchor = anchors[info.key];
				if (anchor) {
					ApplicationController.instance.wireController.anchorTriggered(anchor);  // cause wired objects to move
					anchor.valueChanged(); // cause the ribbon for this node to update
				}
			}
		}
		
		public function handlePathMoved():void {
			var wiring:WireController = ApplicationController.instance.wireController;
			for each (var thing:* in anchors) {
				if (thing is PathNodeWireAnchor) {
					var anchor:PathNodeWireAnchor = thing as PathNodeWireAnchor;
					wiring.anchorTriggered(anchor);  // cause wired objects to move
					anchor.valueChanged();  // cause the ribbon for this node to update
				}
			}
		}

		public function addNodeRibbon(index:int):PathNodeWireAnchor
		{
			if (index < path.nodeCount) {
				var serial:int = nextLinkSerial++;
				var key:String = "pathNode" + serial;
				var label:String = "Point " + serial;
				var link:PathNodeInfo = new PathNodeInfo(serial, key, label);
				path.setAnchorInfo(index, link);
                var anchor:PathNodeWireAnchor = new PathNodeWireAnchor(this, link);
				addAnchor(anchor, true);
				requestUpdate();
                return anchor;
			}
            return null;
		}
		
		public function loadPathNodeAnchors():void
		{
			// LEGACY: this code is for projects before 6/15/11  anchors are now loaded by gadgetDecoder
			var count:int = path.nodeCount;
			for (var n:int=0; n < count; n++) {
				var info:PathNodeInfo = path.getAnchorInfo(n);
				if (info && anchors[info.key] == null) {  // wasn't already loaded by gadgetDecoder
					addAnchor(new PathNodeWireAnchor(this, info), true);
				}
			}
		}
		
		public function nodeForKey(nodeKey:String):int
		{
			var count:int = path.nodeCount;
			for (var n:int=0; n < count; n++) {
				var info:PathNodeInfo = path.getAnchorInfo(n);
				if (info && info.key == nodeKey) 
					return n;
			}
			return -1;
		}
		
		public function getNodePosition(nodeKey:String):PositionAndAngle
		{
			var index:int = nodeForKey(nodeKey);
			if (index >= 0) {
				var view:PathObjectView = this.getView() as PathObjectView;
				if (view && view.parent) {
					var local:Point = path.getPoint(index);
					var point:Point = view.parent.globalToLocal( view.curve.localToGlobal(local));
					return new PositionAndAngle(point.x, point.y, NaN);
				}
			}
			return null;
		}
		
		public function setNodePosition(nodeKey:String, pos:PositionAndAngle):void
		{
			var index:int = nodeForKey(nodeKey);
			if (index >= 0) {
				var view:PathObjectView = this.getView() as PathObjectView;
				if (view && view.parent)  {
					var point:Point = new Point(pos.x, pos.y);
					point = view.curve.globalToLocal( view.parent.localToGlobal(point));
					path.setPoint(index, point);
					if (Application.running) {
						requestRecalc();  // don't recalc bounds
					} else {  // paused:  create init
						path.evaluateCurve();  // immediate recalc, so initial-value can be stored
						initialValues.path = path.toJson();
						requestRedraw();
						_needsReSnap = true;
					}
				}
			}
			var anchor:WireAnchor = anchors[nodeKey];
			if (anchor) {
				anchor.valueChanged();
			}
		}
		
		
		public function finish():void
		{
			var view:ObjectView = getView();
			if (view as PathObjectView)
				PathObjectView(view).recalcBounds();
			
			// generate the control points
			requestRecalc();
			handlePathMoved();
			initialValues.path = path.toJson(); 
			
			// debug (since exceptions are caught during project load)
			//			trace("");
			//			path.dump();
			//			trace("\nJSON:  ", JSON.encode(this),"\n");
			//			trace("\nJSON-:  ", JSON.encode(JSON.decode(JSON.encode(this))),"\n");
			//			var deserialized:Object = SerializerImplementation.deserializeValue(path.toJson(), path)
			//			trace("deserial result="+deserialized);
		}
		
		private function requestRecalc():void
		{
			_needsRecalc = true;
			requestUpdate();
		}
		
		public function requestRedraw():void
		{
			_needsRedraw = true;
			requestUpdate();
		}
		
		private function requestUpdate():void
		{
			var view:ObjectView = getView();
			if (view)
				ApplicationController.instance.requestUpdateView(view);
		}

		[Transient]
		public function get needsRedraw():Boolean
		{
			return _needsRecalc || _needsRedraw;
		}
		
		public function prepRedraw():Boolean
		{
			if (_needsRecalc) {
				path.evaluateCurve();
				_needsRedraw = true;
				_needsReSnap = true;
			}
			if (polygonCreation)
				fillAlpha = 100;  // world mediator tries to enforce PathObject presets
			
			return _needsRedraw;
		}
		
		public function finishRedraw():void
		{
			if (_needsReSnap)
				resnapAll(true);  // nearestPoint = true
			_needsRecalc = false;
			_needsRedraw = false;
			_needsReSnap = false;
		}
		
		public function restorePath(pathValue:Path, position:Point, anchor:Point):void
		{
			// restore the path and path.scaleX,scaleY
			path = pathValue;
			
			// update bounds   Similar to the recalcBounds function in PathEditingMediator
			var view:PathObjectView = getView() as PathObjectView;
			requestRedraw();
			view.render();
			var bounds:Rectangle = view.curve.getBounds(view.curve);
			super.width = bounds.width;  // use super to avoid changing path.scaleX
			super.height = bounds.height;
			
			// when using custom anchorPoint, every recalcBounds will adjust the anchor
			anchorPoint = anchor;
			
			// setting viewBox fires view.placeCurve.  Must be done after setting width & height & anchor above
			viewBox = new Rectangle(0,0, bounds.width / pathValue.scaleX, bounds.height / pathValue.scaleY);

			x = position.x;
			y = position.y;
		}
		
		override public function get isAnimationPath():Boolean
		{
			return path.nodeCount > 0;
		}
		
		override public function pathPointForFraction(frac:Number):Point
		{
			var view:PathObjectView = getView() as PathObjectView;
			if (!view) return null; // if the view is not ready yet, don't bother going further
			var curvePoint:Point = path.getPosition(frac);
			return view.globalToLocal( view.curve.localToGlobal(curvePoint));
		}
		
		override public function closestPointOnPath(local:Point, outputPoint:Point=null):Number
		{
			var view:PathObjectView = getView() as PathObjectView;
			if (view) {
				var curvePoint:Point = view.curve.globalToLocal( view.localToGlobal(local));
				var curveOut:Point;
				var frac:Number = path.closestPoint(curvePoint, curveOut);
				if (outputPoint) {
					var out:Point = view.globalToLocal( view.curve.localToGlobal(curveOut));
					outputPoint.x = out.x;
					outputPoint.y = out.y;
				}
				return frac;
			}
			return 0;
		}
		
		override public function moveToward(local:Point, frac:Number, outputPoint:Point=null):Number
		{
			var view:PathObjectView = getView() as PathObjectView;
			if (view) {
				var curvePoint:Point = view.curve.globalToLocal( view.localToGlobal(local));
				var curveOut:Point;
				var resultFrac:Number = path.moveToward(frac, curvePoint, curveOut);
				if (outputPoint) {
					var out:Point = view.globalToLocal( view.curve.localToGlobal(curveOut));
					outputPoint.x = out.x;
					outputPoint.y = out.y;
				}
				return resultFrac;
			}
			return 0;
		}
		
		override protected function get okToSnapToPath():Boolean
		{
			// prevent snapping this obj to path when recalcBounds
			return (super.okToSnapToPath && !editing);
		}
		
		public function buildPolygon(radius:Number, sides:int, pinBottom:Boolean=false):void
		{
			var oldBottom:Point = new Point(width/2, bottom);
			oldBottom = getView().localToGlobal(oldBottom);
			oldBottom = getView().parent.globalToLocal(oldBottom);
			
			var oldX:Number = x;
			var oldY:Number = y;
			
			path.buildPolygon(radius, sides);
			
			finish();  // does recalc bounds
			
			if (pinBottom) {
				var newBottom:Point = new Point(width/2, bottom);
				newBottom = getView().localToGlobal(newBottom);
				newBottom = getView().parent.globalToLocal(newBottom);
	
				x += oldBottom.x - newBottom.x;
				y += oldBottom.y - newBottom.y;
			} else {  // pin anchorPoint
				x = oldX;
				y = oldY;
			}
		}

        public function set bornAsPolygon(bornAsPolygon:Boolean):void {
            _bornAsPolygon = bornAsPolygon;
        }

        public function get bornAsPolygon():Boolean {
            return _bornAsPolygon;
        }
		
		public function setupInspectorRibbons():void
		{
			// creating a path by ctrl-drag
			var inspectorShowing:Array = [];
			inspectorShowing =inspectorShowing.concat(InspectorRibbonDescriptions.PATH_APPEARANCE_RIBBONS).concat(InspectorRibbonDescriptions.PATH_GEOMETRY_RIBBONS).concat(InspectorRibbonDescriptions.PATH_VISIBILITY_RIBBONS).concat(InspectorRibbonDescriptions.PATH_ACTIONS_RIBBONS);
			for each (var key:String in inspectorShowing) {
				if (anchors[key] as WireAnchor) {
					(anchors[key] as WireAnchor).propertyInspectorVisible = true;
				}
			}
		}

        override public function onRestoreInitial():void {
            super.onRestoreInitial();
			updateScaling();
        }

        override public function getUsageCounts(usages:UsageCounts):void
        {
            super.getUsageCounts(usages);
            if ("bitmapFill" in initialValues) {
                usages.countAssetJson(initialValues.bitmapFill);
            }
        }

    }
}