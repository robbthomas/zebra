/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectProperties;
	
	import flashx.textLayout.formats.TextAlign;
	import flashx.textLayout.formats.VerticalAlign;

	public class RadioButton extends ToggleButton
	{
		public function RadioButton()
		{
			super();
			
			momentary = false;
			textAlignH = TextAlign.LEFT;
			textAlignV = VerticalAlign.MIDDLE;
			cornerRadius = -1; // -1 causes the GraphicFillView to be an ellipse
			margin = 0;
			maintainAspect = true;

            toolTipOverrides["enableButton"] = "Allow this radio button to be pressed";
			toolTipOverrides["disableButton"] = 'Do not allow this radio button to be pressed \n(deactivates "Triggers: Button press" - other ribbons remain active) \n(set opacity in "Visibility: Opacity- pwr off")';
            toolTipOverrides["interactive"] = 'Can this radio button be pressed?\n(affects "Triggers: Button press" - other ribbons remain active)';

			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("checkWeight"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("cornerRadius"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("textAlignV"));
			
		}
	}
}