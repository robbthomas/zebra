package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectProperties;

	public class CalcGate extends Calc
	{
		[Bindable]
		public var gateValue:Boolean = false;
		
		[Bindable]
		public var calcValue2:Object = "";
		
		public function CalcGate()
		{
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("gateValue"), "gateValue");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("calcValue2"), "calcValue2");
		}
	}
}