/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: $
 */
package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectProperties;

	public class Button extends ToggleButton
	{
		public function Button()
		{
			super();
			
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("labelPosition"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("labelPadding")); 
		}
	}
}