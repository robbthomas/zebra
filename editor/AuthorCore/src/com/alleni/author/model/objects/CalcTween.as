package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectProperties;

	public class CalcTween extends Calc
	{
			
		[Bindable]
		public var tweenType:String = "linear";
		
		[Bindable]
		public var tweenTime:Number = 1.5;

		[Bindable]
		public var calcTweenValue:Object;
		
		[Transient]
		public var frac:Number = 0;
		
		public function CalcTween()
		{
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("calcValue"));
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("calcTweenValue"), "calcTweenValue");
		}
		
	}
}