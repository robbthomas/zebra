package com.alleni.author.model.objects
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.model.ObjectInlets;
	import com.alleni.author.model.ObjectOutlets;
	import com.alleni.author.model.ObjectProperties;
	import com.google.maps.Map;
	import flash.geom.Point;
	
	
	
	public class MapObject extends NativeVisualObject
	{
		
		
		[Bindable]
		public var latitude:Number = 0;
		
		[Bindable]
		public var longitude:Number = -0;
		
		[Bindable]
		public var zoom:uint = 1;
		
		[Bindable]
		public var mapType:uint = 0;
		
		[Transient]
		public var map:Map;
		
		
		public function MapObject () {
		
			this.anchorPoint = new Point(.5,.5);  
			
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("latitude"), "latitude");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("longitude"), "longitude");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("zoom"), "zoom");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("mapType"), "mapType");
			
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("reflection"));
			
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("duration"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("targetCollide"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("overlapsCollisionTarget"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("targetDrop"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("putBackWhen"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("putBackSecs"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("width"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("height"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("anchorPoint"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pivotPoint"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("alignAreaHorz"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("alignAreaVert"));
			//ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("rotation"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillSpec"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("fillAlpha"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineColor"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineAlpha"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("lineThickness"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("alpha"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("movable"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pathValue"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pathPercent"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pathMaximum"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pathMinimum"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("runtimeRotation"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("overlapsCollisionTarget"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("snapTo"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("snap"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("pathOn"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("inertial"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("collidingWith"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("droppedOn"));
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("acceptIntoOutOfArena"));	
			ObjectProperties.instance.unregisterProperty(this, Modifiers.instance.fetch("maintainAspect"));
			
			ObjectInlets.instance.unregisterInlet(this, Modifiers.instance.fetch("putBack"));
			ObjectInlets.instance.unregisterInlet(this, Modifiers.instance.fetch("snapToNearest"));
			
			ObjectOutlets.instance.unregisterOutlet(this, Modifiers.instance.fetch("dragStart"));
			ObjectOutlets.instance.unregisterOutlet(this, Modifiers.instance.fetch("dragMove"));
			ObjectOutlets.instance.unregisterOutlet(this, Modifiers.instance.fetch("dragRelease"));	
		}
	}
}