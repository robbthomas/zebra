package com.alleni.author.model.objects
{
    import com.alleni.author.document.Document;
    import com.alleni.author.model.World;
    import com.alleni.author.model.project.Project;

    public class TemplateValueObject
	{
		public var projectName:String;
		public var description:String;
        public var watermarkPosition:String;
        public var width:Number;
        public var height:Number;

        public var accountTypeId:int;
        public var onNextOpen:String;
		
		public function TemplateValueObject(name:String,
											describe:String=null)
		{
			projectName = name;
			description = describe;
		}

        public function apply(document:Document, freshProject:Boolean):void {
			const world:World = document.root.value as World;
			world.width = width;
			world.height = height;
			world.recordInitialValue("width");
			world.recordInitialValue("height");
			if(accountTypeId == 3){
                if(freshProject){
                    world.project.updateProjectMC(accountTypeId);
                }
                world.project.onNextOpen = onNextOpen;
				world.project.recordInitialValue("onNextOpen");
			}
			document.project.description = description;

			const gadget:Project = document.project;
			if (gadget) {
				gadget.name = projectName;
				gadget.watermarkPosition = watermarkPosition;
                if(freshProject){
                    gadget.publishedName = "";
                    gadget.publishedNameIsFromDatabase == true;
                    gadget.published = false;
                }
			}
			document.name = projectName;
            if(freshProject){
                document.project.published = false;
                document.project.publishedNameIsFromDatabase = true;
                document.project.publishedName = "";
                gadget.accountTypeId = accountTypeId;
            }else{
                gadget.accountTypeId = document.project.accountTypeId;
            }
        }
	}
}