package com.alleni.author.model.objects
{
	import com.alleni.author.controller.objects.IReplaceableReceiver;
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.definition.AssetType;
	import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.model.ui.UsageCounts;

public class AbstractExternalObject extends AbstractObject implements IReplaceableReceiver
	{
		[Bindable]
		public var asset:Asset;
		
		[Bindable]
		public var name:String;
		
		[Bindable] public var initialLoadDimensionsApplied:Boolean = false;
		
		[Transient]
		[Bindable]
		public var content:Object;
		
		// loadingAsset = true when loading asset for a loaded project, to prevent changing obj width & height
		// this is needed because loading assets is deferred and when the completion event arrives, obj.loading is false.
		[Transient]
		[Bindable]
		public var loadingAsset:Boolean = false;  
		
		[Bindable]
		public var uri:String = "";
		
		[Transient]
		public var replaceablePropertyChangeListener:Function;
		
		public function acceptsReplaceableDrop(newReplaceable:IReplaceable):Boolean
		{
			return AssetType.getContainerName(newReplaceable.type) == this.shortClassName
				&& ((this.replaceable && this.replaceable.id != newReplaceable.id) || !this.replaceable); // prevent replacement with identical item
		}
		
		[Transient]
		public function get replaceable():IReplaceable
		{
			return this.asset;
		}
		public function set replaceable(value:IReplaceable):void
		{
            AssetController.instance.attachFor(this, value as Asset);   // sets this.asset = value;
		}

        override public function getUsageCounts(usages:UsageCounts):void
        {
            super.getUsageCounts(usages);
            if ("asset" in initialValues) {
                usages.countAssetJson(initialValues.asset);
            }
        }
	}
}
