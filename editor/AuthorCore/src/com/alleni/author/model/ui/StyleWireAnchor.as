/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 3/21/11
 * Time: 12:24 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.ui {
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.text.TextSelection;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.TextObject;
	
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	import flash.utils.Dictionary;
	
	import flashx.textLayout.formats.TextDecoration;

	public class StyleWireAnchor extends WireAnchor {

		private var _styleName:String;
		private var _TLF2CSS:Dictionary = new Dictionary();
		private var _TLF2Props:Dictionary = new Dictionary();
		private var _textSelection:TextSelection;
		

		public function StyleWireAnchor(textObject:TextObject, styleName:String, oldUid:String = null, useNewUid:Boolean=false)
		{
			mapTLF2Props();
			mapTLF2CSS()
			
			
			hostObject = textObject;
			
			if (oldUid && !useNewUid)
				this.uid = oldUid;

			var id:String = oldUid?oldUid:this.uid;
			_styleName = styleName;

			modifierDescription = new PropertyDescription("style:"+_styleName, id, "Style- "+_styleName, "",Modifiers.FONT, 0, false, -1, Modifiers.CSS_SELECTOR_TYPE);

			for each(var fontProp:String in ["fontName", "fontSize", "fontWeight", "fontStyle", "color","textDecoration"]) {  //to do -- there are more
				var anchor:MetaWireAnchor = new MetaWireAnchor(this, _TLF2Props[fontProp]);
				anchor.modifierDescription = PropertyDescription(Modifiers.instance.fetch(_TLF2Props[fontProp]));
				anchor.hostProperty = _TLF2Props[fontProp];
				childAnchors.push(anchor);
			}

			hostObject.addAnchor(this, useNewUid);
			
		}
		
		

		public function setFontProperty(name:String, value:Object):void {
			TextObject(hostObject).css.addProperty("." + _styleName, name, String(value));
			TextObject(hostObject).stylesValid = false;
		}

		public function getFontProperty(name:String):Object {
			return TextObject(hostObject).css.getProperty("." + _styleName, name);
		}
		
		public function set fontFamily(value:String):void {
			setFontProperty("font-family", value);
		}

		public function get fontFamily():String {
			return getFontProperty("font-family") as String;
		}

		public function set fontSize(value:Number):void {
			if (value < 1)return; //fontsize must be > 0 or textflow crashes
			var size:String = String(value) + "pt";
			setFontProperty("font-size", size);
		}
		
		public function get fontSize():Number {
			var size:String = getFontProperty("font-size") as String;
			size = size.replace(/pt|px/i, "");
			var tempFontSize:Number = Number(size);  //fontsize must be > 0 or textflow crashes
			return tempFontSize > 0? tempFontSize:1;  
		}
		
		public function set fontColor(value:uint):void {
			//setFontProperty("color", (value as String).replace("0x","#"));
			var colorString:String = "#" + value.toString(16);
			setFontProperty("color", colorString);
		}
		
		public function get fontColor():uint {
			var value:String =  (getFontProperty("color")).replace("#","0x");
			var color:uint = uint(value);
			return color;
		}
		
		public function set bold(value:Boolean):void {
			setFontProperty("font-weight", value == true?FontWeight.BOLD:FontWeight.NORMAL);
		}
		
		public function get bold():Boolean {
			var prop:String = getFontProperty("font-weight") as String;
			return prop == FontWeight.BOLD?true:false;
		}
	
		public function set italic(value:Boolean):void {
			setFontProperty("font-style", value == true?FontPosture.ITALIC:FontPosture.NORMAL);
		}
		
		public function get italic():Boolean {
			var prop:String = getFontProperty("font-style") as String;
			return prop == FontPosture.ITALIC?true:false;
		}
		
		public function set underline(value:Boolean):void {
			setFontProperty("text-decoration", value == true?TextDecoration.UNDERLINE:TextDecoration.NONE);
		}
		
		public function get underline():Boolean {
			var prop:String = getFontProperty("text-decoration") as String;
			return prop == TextDecoration.UNDERLINE?true:false;
		}
		
		private function mapTLF2CSS():void{
			
			_TLF2CSS["fontName"] = "font-family";
			_TLF2CSS["fontSize"] = "font-size";
			_TLF2CSS["color"]    = "color";
			_TLF2CSS["fontStyle"] = "font-style";
			_TLF2CSS["fontWeight"] = "font-weight";
			_TLF2CSS["textDecoration"] = "text-decoration";
			
		}
		
		private function mapTLF2Props():void{
			
			_TLF2Props["fontName"] = "fontFamily";
			_TLF2Props["fontSize"] = "fontSize";
			_TLF2Props["color"]    = "fontColor";
			_TLF2Props["fontStyle"] = "italic";
			_TLF2Props["fontWeight"] = "bold";
			_TLF2Props["textDecoration"] = "underline";
			
		}
	}
		
		
}
