package com.alleni.author.model.ui
{
	import com.alleni.author.definition.GadgetType;
	import com.alleni.author.model.gadgets.GadgetLicense;
    import com.alleni.author.model.gadgets.Gadgets;
    import com.alleni.author.model.objects.IListable;
    import com.alleni.author.model.project.Project;
    import com.alleni.taconite.dev.Utilities;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.events.PropertyChangeEvent;

	public class Dock extends EventDispatcher
	{
		public static const VIEW_MY_GADGETS:int 		= 0;
		public static const VIEW_SHOPP:int 				= 1;
		public static const VIEWS:Array = [
			 {label:"My Gadgets", 	id:VIEW_MY_GADGETS,	projectTypeId:GadgetType.GENERIC}
			,{label:"Gadget Shopp",	id:VIEW_SHOPP,		projectTypeId:GadgetType.GENERIC}
		];
		
		public static const CATEGORY_ALL:int 			= -1;
		public static const CATEGORIES_DEFAULT:Array 	= [
			{label:"All", 			id:CATEGORY_ALL}
		];
		
		public static const SORT_NAME:int 				= 0;
		public static const SORT_PRICE:int 				= 1;
		public static const SORT_CATEGORY:int 			= 2;
		public static const SORT_DATE:int 				= 3;
		public static const SORT_OPTIONS:Array = [
			  {label:"Name", 		id:SORT_NAME}
			 ,{label:"Price", 		id:SORT_PRICE}
			 ,{label:"Category", 	id:SORT_CATEGORY}
			 ,{label:"Date", 		id:SORT_DATE}
		];
		
		private static var _instance:Dock;
		
		private var _categories:Array;
		public function get categories():Array
		{
			return _categories;
		}
		public function set categories(value:Array):void
		{
			var oldValue:Array = _categories;
			_categories = value;
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "categories", oldValue, _categories));
		}
		
		private var _licenses:Vector.<GadgetLicense>;
		public function get licenses():Vector.<GadgetLicense>
		{
			return _licenses;
		}
		public function set licenses(value:Vector.<GadgetLicense>):void
		{
			var oldValue:Vector.<GadgetLicense> = _licenses;
			_licenses = value;
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "licenses", oldValue, _licenses));
		}
		
		[Bindable] public var totalLength:int;
		[Bindable] public var pageIndex:int;
		[Bindable] public var pageSize:int;
		[Bindable] public var pageList:Gadgets;
		[Bindable] public var userItemList:Vector.<Project>;
		[Bindable] public var searchField:String;
		[Bindable] public var selectedView:int = VIEW_MY_GADGETS;
		[Bindable] public var selectedCategoryId:int = -1;
		[Bindable] public var selectedSort:int = SORT_NAME;
		
		// selected item is an item chosen from the dock grid. Updating this value populates the dock details panel.
		private var _selectedItem:IListable;
		private var _selectedItemPropertyChangeListener:Function;
		
		public function get selectedItem():IListable
		{
			return _selectedItem;
		}
		public function set selectedItem(value:IListable):void
		{
			if (_selectedItem) {
				IEventDispatcher(_selectedItem).removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, _selectedItemPropertyChangeListener);
				_selectedItemPropertyChangeListener = null;
			}
			var oldItem:IListable = _selectedItem;
			_selectedItem = value;
			dispatchEvent(new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE, false, false, null, "selectedItem", oldItem, value));
			
			if (!value) return;
			
			_selectedItemPropertyChangeListener = function(event:PropertyChangeEvent):void {
				dispatchEvent(new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE, false, false, null, "selectedItem", value, value));
			}
			IEventDispatcher(_selectedItem).addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, _selectedItemPropertyChangeListener);
		}
		
		// the publishing candidate is an item which is either being published or updated using an in-context panel
		private var _publishingCandidate:IListable;
		private var _publishingCandidatePropertyChangeListener:Function;
		
		public function get publishingCandidate():IListable
		{
			return _publishingCandidate;
		}
		public function set publishingCandidate(value:IListable):void
		{
			if (_publishingCandidate) {
				IEventDispatcher(_publishingCandidate).removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, _publishingCandidatePropertyChangeListener);
				_publishingCandidatePropertyChangeListener = null;
			}
			var oldItem:IListable = _publishingCandidate;
			_publishingCandidate = value;
			dispatchEvent(new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE, false, false, null, "publishingCandidate", oldItem, value));
			
			if (!value) return;
			
			_publishingCandidatePropertyChangeListener = function(event:PropertyChangeEvent):void {
				dispatchEvent(new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE, false, false, null, "publishingCandidate", value, value));
			}
			IEventDispatcher(_publishingCandidate).addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, _publishingCandidatePropertyChangeListener);
		}
		
		[Bindable] public var listInProgress:Boolean;
		[Bindable] public var publishInProgress:Project;
		[Bindable] public var lastSuccessfullyPublished:Project;
		[Bindable] public var lastSuccessfullyPurchased:Project;
		
		public function Dock()
		{
			Utilities.assert(!_instance);
			super();
		}
		
		public static function get instance():Dock
		{
			if (_instance == null)
				_instance = new Dock();
			return _instance;
		}
	}
}