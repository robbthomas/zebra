package com.alleni.author.model.ui
{
	import com.alleni.author.application.ui.EditorUI;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.DropIntoContainerEvent;
	import com.alleni.author.definition.InletDescription;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.OutletDescription;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.action.CustomRibbonAction;
	import com.alleni.author.definition.action.ModifyRibbonAction;
	import com.alleni.author.definition.application.NotificationNamesEditor;
	import com.alleni.author.event.WireEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.objects.AbstractTable;
	import com.alleni.author.model.objects.Calc;
	import com.alleni.author.model.objects.LogicObject;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.objects.CalcView;
	import com.alleni.author.view.ui.WireView;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.model.TaconiteObject;
	
	import flash.utils.Dictionary;
	
	import mx.collections.IList;
	
	
	public class Wire extends TaconiteObject
	{
		public var x:Number;
		public var y:Number;
		public var angle:Number;
		public var x2:Number;
		public var y2:Number;
		public var angle2:Number;
		public var redraw:Boolean = true;  // redraw has been requested
        public var illegal:Boolean = false;
		

		[Transient]
		private var _views:Dictionary = new Dictionary(true);
		
		[Bindable]
		public var masterAnchor:WireAnchor;
		
		private var _slaveAnchor:WireAnchor;
		
		[Bindable]
		public function get slaveAnchor():WireAnchor
		{
			return _slaveAnchor;
		}
		
		public function set slaveAnchor(value:WireAnchor):void
		{
			var oldAnchor:WireAnchor = _slaveAnchor;
			_slaveAnchor = value;
			if(oldAnchor) {
				oldAnchor.wired = ApplicationController.instance.wireController.getWiresForAnchor(oldAnchor).length>0;
			}
		}
		
		[Bindable]
		public var visible:Boolean = false;  // prevent wires flashing at wrong locations due to MC not being ready

		[Bindable]
		[Transient]
		public var updateView:int=0;  // increment this to cause view update
		
		
		public function Wire (master:WireAnchor, slave:WireAnchor=null)
		{
			masterAnchor = master;
			masterAnchor.wired = true;
			
			if (slave) {
				slaveAnchor = slave;
				slaveAnchor.wired = true;
			} else {
				slaveAnchor = ApplicationController.instance.wireController.ghostAnchor;
			}
		}
		
		public function unattachEnd(masterEnd:Boolean):void
		{
			var oldAnchor:WireAnchor;
			if (masterEnd) {
				oldAnchor = masterAnchor;
				masterAnchor = slaveAnchor;
			} else {
				oldAnchor = slaveAnchor;
			}
			
			slaveAnchor = ApplicationController.instance.wireController.ghostAnchor;
			oldAnchor.wired = ApplicationController.instance.wireController.getWiresForAnchor(oldAnchor).length>0;
			oldAnchor.highlight = false; // gets rid of the wire rollover highlight.
			oldAnchor.hostObject.model.dispatchEvent(new WireEvent(WireEvent.DETACH, this, true, oldAnchor));
		}

        public function notifyAttached(role:int):void {
            if(masterAnchor is InternalSmartWireAnchor == false) {
                ApplicationController.instance.authorController.selectSingleModel(masterAnchor.hostObject.model, true);
            }
            if(slaveAnchor is InternalSmartWireAnchor == false) {
                ApplicationController.instance.authorController.selectSingleModel(slaveAnchor.hostObject.model, true);
            }

            var e:WireEvent = new WireEvent(WireEvent.COMPLETE, this, false, null, role);
            masterAnchor.hostObject.model.dispatchEvent(e);
            slaveAnchor.hostObject.model.dispatchEvent(e);
        }
		
		public function finish(slave:WireAnchor):Boolean
		{
			
			// This boolean is set to true when the master and slave anchor are flipped upon wiring to a table (table property
			// anchors need to always be the master, but we still want wiring TO the table to take on the value of the object
			// we are wiring FROM. - LML
			var flipBinding:Boolean = false;
						
			if(masterAnchor.wiringScope != slave.wiringScope)
				return false;
			
			var slaveInner:Boolean = slave is InternalSmartWireAnchor;
			var masterInner:Boolean = masterAnchor is InternalSmartWireAnchor;
			
			// the master anchor is the one where wire-drag started
			if (masterAnchor.modifierDescription is InletDescription) {
				if (slave.modifierDescription is InletDescription) {
					if (slaveInner) {
						slaveAnchor = slave;
					} else if (masterInner) {
						slaveAnchor = masterAnchor;
						masterAnchor = slave;
					} else {
						return false; // Invalid wire
					}
				} else if (slave.modifierDescription is OutletDescription) {
					slaveAnchor = masterAnchor;
					masterAnchor = slave;
				} else if (slave.modifierDescription is PropertyDescription) {
					if (masterInner) {
						if(isReadOnly(slave))
							return false; // cannot write any value from calc to a readonly property.
						if(!createSetterRibbon(slave))
							return false;
					} else {
						slaveAnchor = masterAnchor;
						if(!createTriggerRibbon(slave)) {
							return false;
						}
					}
				}
			} else if (masterAnchor.modifierDescription is OutletDescription) {
				if (slave.modifierDescription is InletDescription) {
					slaveAnchor = slave;
				} else if (slave.modifierDescription is OutletDescription) {
					if (slaveInner) {
						slaveAnchor = slave;
					} else if (masterInner) {
						slaveAnchor = masterAnchor;
						masterAnchor = slave;
					} else {
						return false; // Invalid wire
					}
				} else {  // slave is property
					if (masterInner) {
						slaveAnchor = masterAnchor;
						if(!createTriggerRibbon(slave))
							return false;
					} else {
						if(isReadOnly(slave))
							return false; 
						if(!createSetterRibbon(slave))
							return false;
					}
				}
			} else {  // master is Property
				if (slaveInner ? slave.modifierDescription is OutletDescription : slave.modifierDescription is InletDescription) {
					slaveAnchor = slave;
					if(!createTriggerRibbon(masterAnchor)) {
						return false;
					}
				} else if (slaveInner ? slave.modifierDescription is InletDescription : slave.modifierDescription is OutletDescription) {
					if(isReadOnly(masterAnchor))
						return false;
					var tmpSlave:WireAnchor = masterAnchor;
					masterAnchor = slave;
					if(!createSetterRibbon(tmpSlave)) 
						return false;
				} else  if (slave.modifierDescription is PropertyDescription) {
					// TODO: Read-only properties can't be slaves.
					if (isReadOnly(slave)) {
						if (isReadOnly(masterAnchor)) {
							return false;
						} else {
							slaveAnchor = masterAnchor;
							masterAnchor = slave;
						}
					} else if(isWriteOnly(masterAnchor)) {
						if (isWriteOnly(slave)) {
							return false;
						} else {
							slaveAnchor = masterAnchor;
							masterAnchor = slave;
						}
                    } else if(slave.hostObject is AbstractTable && slave.modifierDescription.key == "currentStateValues"){
						if (masterAnchor.hostObject is AbstractTable && masterAnchor.modifierDescription.key == "currentStateValues") {
							slaveAnchor = slave; //TODO no longer disabling but we need to examine this.
						} else {
							slaveAnchor = masterAnchor;
							masterAnchor = slave;
							flipBinding = true;
						}
					} else {
						slaveAnchor = slave;
					}
					// if we are wiring to the add anchor port (last anchor when there are more anchors than property values)
					// then add a property value at this time.
					if(slaveAnchor.isAdditionAnchor) {
                        if(masterAnchor.isAdditionAnchor) {
                            // they are both addition anchors, so no help here just pick a value
                            IList(slaveAnchor.hostObject[slaveAnchor.hostProperty]).addItem("");
                            IList(masterAnchor.hostObject[masterAnchor.hostProperty]).addItem("");
                        } else {
						    IList(slaveAnchor.hostObject[slaveAnchor.hostProperty]).addItem(masterAnchor.value);
                        }
					} else if(masterAnchor.isAdditionAnchor) {
						IList(masterAnchor.hostObject[masterAnchor.hostProperty]).addItem(slaveAnchor.value);
					}
					
					// the two anchors must be forced to same value:  decide which one to use
					var source:WireAnchor = flipBinding ? slaveAnchor : masterAnchor;
					var dest:WireAnchor = flipBinding ? masterAnchor : slaveAnchor;
					if (isReadOnly(source) == false && Modifiers.valueShouldOverride(dest.value, source.value)) {
						flipBinding = !flipBinding;  // use the other value because source.value is essentially null
					}
					if (flipBinding) {
						ApplicationController.instance.wireController.handlePropertyWireManuallyPropagated(this, true);
						setAnchorValueWithUndo(masterAnchor, slaveAnchor.value);
					} else {
						ApplicationController.instance.wireController.handlePropertyWireManuallyPropagated(this, false);
						setAnchorValueWithUndo(slaveAnchor, masterAnchor.value);
					}
					
				}
			}
			
			slaveAnchor.wired = true;
			masterAnchor.wired = true;

            ApplicationController.instance.wireController.showWiresForLevel();
			return true;
		}

        private function isReadOnly(anchor:WireAnchor):Boolean{
            if(anchor is InternalSmartWireAnchor){
                return anchor.modifierDescription.writeOnly;
            }else{
                return anchor.modifierDescription.readOnly;
            }
        }

        private function isWriteOnly(anchor:WireAnchor):Boolean{
            if(anchor is InternalSmartWireAnchor){
                return anchor.modifierDescription.readOnly;
            }else{
                return anchor.modifierDescription.writeOnly;
            }
        }

		private function setAnchorValueWithUndo(anchor:WireAnchor, value:*):void
		{
			var oldValue:* = anchor.value;
			anchor.value = value;
			ApplicationController.currentActionTree.commit(ModifyRibbonAction.fromObject(anchor.hostObject, anchor.path, oldValue));
		}
		
		public function legalConnection(slave:WireAnchor):Boolean
		{
			if (slave.hostObject.wiringLocked)
				return false;
			
			if (ApplicationController.instance.wireController.wireExists(masterAnchor, slave))
				return false;
			
			if (masterAnchor == slave)
				return false;
			
			if (masterAnchor is InternalSmartWireAnchor && slave is InternalSmartWireAnchor)
				return false;  // cannot wire from one peg to another
			
			// rules for wires from gadget children to wireboard pegs
			if ((masterAnchor is InternalSmartWireAnchor || slave is InternalSmartWireAnchor) && masterAnchor.wiringScope == slave.wiringScope) {
				if (masterAnchor.modifierDescription is InletDescription && slave.modifierDescription is OutletDescription)
					return false;  // red/orange colors must match when wiring to edge
				if (masterAnchor.modifierDescription is OutletDescription && slave.modifierDescription is InletDescription)
					return false; 
			}
			
			if (masterAnchor.modifierDescription is InletDescription) {
				if (slave.modifierDescription is InletDescription) {
					return (slave is InternalSmartWireAnchor || masterAnchor is InternalSmartWireAnchor);
				} else if (slave.modifierDescription is OutletDescription) {
					return true;
				} else if (slave.modifierDescription is PropertyDescription && masterAnchor is InternalSmartWireAnchor) {
					return legalSetter(slave);
				} else {
					return legalTrigger(slave);
				}
			} else if (masterAnchor.modifierDescription is OutletDescription) {
				if (slave.modifierDescription is InletDescription) {
					return true;
				} else if (slave.modifierDescription is OutletDescription) {
					return (slave is InternalSmartWireAnchor || masterAnchor is InternalSmartWireAnchor);
				} else {
					return legalSetter(slave);
				}
			} else {
				if (slave.modifierDescription is InletDescription) {
					return legalTrigger(masterAnchor);
				} else if (slave.modifierDescription is OutletDescription) {
					if(isReadOnly(masterAnchor)) {
						return false; // cannot write any value from calc to a readonly property.
					}
					return legalSetter(masterAnchor);
				} else {  // can't connect two read-only props
					if (isReadOnly(slave)) {
						return (!isReadOnly(masterAnchor));
					}
				}
			}
			return true;
		}
		
		private function legalSetter(slave:WireAnchor):Boolean
		{
			return !(slave.hostObject is LogicObject || slave.host is WireAnchor);
		}
		
		private function legalTrigger(master:WireAnchor):Boolean
		{
			return !(master.hostObject is LogicObject || master.host is WireAnchor);
		}
				
		private function createTriggerRibbon(master:WireAnchor):Boolean
		{
			if(!legalTrigger(master)) {
				return false;
			}
			masterAnchor = new TriggerWireAnchor(master, true);
			ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(masterAnchor, true));
			
			master.wired = ApplicationController.instance.wireController.getWiresForAnchor(master).length>0;
			master.highlight = false;
			
			return true;
		}
		
		private function createSetterRibbon(slave:WireAnchor):Boolean
		{
			if(!legalSetter(slave)) {
				return false;
			}
			slaveAnchor = new SetterWireAnchor(slave, true);
			ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(slaveAnchor, true));
			slave.wired = ApplicationController.instance.wireController.getWiresForAnchor(slave).length>0;
			slave.highlight = false;
			return true;
		}
		
		public function propagateToSlave():void 
		{
			if (attached) {
				if (slaveAnchor.modifierDescription is PropertyDescription) {
					slaveAnchor.value = masterAnchor.value;
				} else {
					// TODO: propagate other types
				}
			}
		}
		
		public function get attached():Boolean
		{
			return (slaveAnchor != ApplicationController.instance.wireController.ghostAnchor);
		}
		
		public function fireCompleteEvent(existing:Boolean=false):void
		{
			if(masterAnchor && masterAnchor.host) {
				masterAnchor.host.model.dispatchEvent(new WireEvent(WireEvent.COMPLETE, this, existing));
			}
			if(slaveAnchor && slaveAnchor.host) {
				slaveAnchor.host.model.dispatchEvent(new WireEvent(WireEvent.COMPLETE, this, existing));
			}
		}
		
		
		public function register(view:WireView):void
		{
			_views[view] = true;
		}
		
		public function getViews(context:WorldContainer) : Vector.<WireView>
		{
			var result:Vector.<WireView> = new Vector.<WireView>();
			for(var o:Object in _views) {
				var p:WireView = o as WireView;
				if(p && p.stage && p.context == context) {
					result.push(p);
				}
			}
			return result;
		}
		
		override public function toString():String
		{
			return  "[Wire master=" + masterAnchor + " slave=" + (attached?slaveAnchor:"ghost")
				+ ", x="+x+", y="+y+", x2="+x2+", y2="+y2+", vis="+visible+"]";
		}
		
		public function get logStr():String
		{
			return  "[Wire master=" + masterAnchor.logStr + " slave=" + (attached?slaveAnchor.logStr:"ghost") + "]";
		}

        public function describe():String
        {
            return (masterAnchor ? masterAnchor.describe() : "null") + " -> " + (_slaveAnchor ? _slaveAnchor.describe() : "null");
        }
	}
}
