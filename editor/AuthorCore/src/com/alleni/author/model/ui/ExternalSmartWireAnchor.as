package com.alleni.author.model.ui
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.taconite.factory.SerializerImplementation;

import flash.utils.Dictionary;

import mx.events.PropertyChangeEvent;

    import mx.utils.UIDUtil;
	
	
	public class ExternalSmartWireAnchor extends WireAnchor
	{
		public var other:InternalSmartWireAnchor;
		
		private var _externalID:String;


        private var _value:Object;

        [Transient]
        [Bindable]
        public var valueNewerThanInside:Boolean = false;
		
		override public function trigger():void
		{
			ApplicationController.instance.wireController.anchorTriggered(other);
		}
	
		public function ExternalSmartWireAnchor(externID:String = null, makeOther:Boolean = true, otherID:String = null)
		{
			_externalID = (externID? externID : UIDUtil.createUID());
			
			if (makeOther) {
				other = new InternalSmartWireAnchor(false,otherID);
				other.other = this;
			}
			super();
            this.initialValues = getCurrentValues();
            this.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, propertyChanged);
		}
		
		public override function handleTrigger():void
		{
			trigger();
		}

		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		override public function get value ():Object
		{
			return _value;
		}

        internal function different(lhs:*,  rhs:*):Boolean
        {
            return lhs  != rhs && !(rhs != rhs);
        }

		override public function set value (val:Object):void
		{
            val = Modifiers.convertValue(PropertyDescription(modifierDescription).type, val);
            if(!different(_value, val) || val is AbstractObject) {
                return;
            }
            var oldValue:Object = value;
            _value = val;

            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "value", oldValue, value));
            ApplicationController.instance.wireController.anchorTriggered(this);

            var ep:EventPage = _host as EventPage;
            if(ep) {
                var flow:EventFlow = ep.parent as EventFlow;
                if (flow && !ep.editing && (flow.disableLoadUnload || !ep.hasInternals)) {
                    valueNewerThanInside = true;
                    return; // do not continue on to dispatch the internal if this event has no internal objects
                }
            }

            if(other) {
                other.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "value", oldValue, value));
                ApplicationController.instance.wireController.anchorTriggered(other);
            }
		}
		
		public function get externalID():String
		{
			return _externalID;
		}
		
		override public function set hostObject(value:AbstractObject):void
		{	
			_host = value;
			other.hostObject = value;
		}
		
		override public function toString():String
		{
			var hostStr:String = (hostObject) ? hostObject.title : String(host);
			return "[ExternalSmartWireAnchor host="+hostStr+ " id="+_externalID + " " + "other="+(other?other.internalID:"") + "]";
		}

        override public function getCurrentValues(objectIds:Dictionary = null):Object {
            if(modifierDescription is PropertyDescription) {
                var result:Object = {
                    value:value,
                    valueNewerThanInside:valueNewerThanInside
                };
                SerializerImplementation.serializeValue(result,  "value", objectIds);
                return result;
            } else {
                return super.getCurrentValues(objectIds);
            }
        }

        override public function setCurrentValues(values:Object, objectPropStorage:Object=null):void {
            if(modifierDescription is PropertyDescription) {
                value = SerializerImplementation.deserializeValue(values.value, null, objectPropStorage, this, "value");
                valueNewerThanInside = values.valueNewerThanInside;
            } else {
                return super.setCurrentValues(values, objectPropStorage);
            }
        }

        public function propertyChanged(event:PropertyChangeEvent):void {
            if(Application.running || !ApplicationController.instance.wireController.bindingsEnabled || hostObject.loading || !(modifierDescription is PropertyDescription)) {
                return;
            }
            switch(event.property) {
                case "value":
                    initialValues[event.property] = event.newValue;
                    SerializerImplementation.serializeValue(initialValues, event.property);
                    break;
                case "valueNewerThanInside":
                    initialValues[event.property] = event.newValue;
                    break;
            }
        }

        public function get labelWithEditCue():String {
            var hostSmartObject:SmartObject = hostObject as SmartObject;
            if (hostSmartObject && hostSmartObject.editing) {
                return label + "\n(click ribbon to edit its name)";
            } else {
                return label;
            }
        }
    }
}
