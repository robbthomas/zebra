package com.alleni.author.model.ui
{
	public interface IReplaceable
	{
		function get content():Object;
		function set content(value:Object):void
		
		function get id():String;
		function get type():int;
        function get propertyType():String;
        function get name():String;
	}
}