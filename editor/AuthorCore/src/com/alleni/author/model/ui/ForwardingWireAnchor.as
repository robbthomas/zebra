package com.alleni.author.model.ui
{
	import com.alleni.author.definition.InspectorPresets;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.text.TextSelection;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.taconite.model.ITaconiteObject;
	
	import mx.events.PropertyChangeEvent;

	public class ForwardingWireAnchor extends WireAnchor
	{
		private var _selectedObjects:Vector.<ITaconiteObject>;
		
		public function ForwardingWireAnchor(selectedObjects:Vector.<ITaconiteObject>)
		{
			super();
			_selectedObjects = selectedObjects;
		}

		public function set selectedObjects(newSelectedObjects:Vector.<ITaconiteObject>):void
		{
			_selectedObjects = newSelectedObjects;
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "value", null, value));
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "messageCenterVisible", null, messageCenterVisible));
		}

		public function get selectedObjects():Vector.<ITaconiteObject> {
			return _selectedObjects.slice();
		}
		
		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		override public function get value ():Object
		{
			if(modifierDescription.readable == false || modifierDescription.indexed) {
				return null;
			}
			// in the future do something intesting based on if the values
			// are already identical or not, for now this will return the first one
			for each(var host:Object in _selectedObjects) {
				if(host.hasOwnProperty("anchors") && hostProperty in host.anchors) {
					return WireAnchor(host.anchors[hostProperty]).value;
				}
				if(host.hasOwnProperty(hostProperty)) {
					return host[hostProperty];
				}
				if(host.hasOwnProperty("preset")) {
					return null;
				}
			}
			return null;
		}
		
		override public function set value (val:Object):void
		{
			var oldValue:Object = value;
			if(modifierDescription.writeable == false || modifierDescription.indexed) {
				return;
			}
			var type:String = (modifierDescription as PropertyDescription).type;
			var result:* = Modifiers.convertValue(type, val);
			if (result != null) {
				for each(var host:Object in _selectedObjects) {
					if(host.hasOwnProperty("anchors") && hostProperty in host.anchors) {
						WireAnchor(host.anchors[hostProperty]).value = val;
					} else if(host.hasOwnProperty(hostProperty) || host.hasOwnProperty("preset")) {
						host[hostProperty] = val;
					}
				}
			}
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "value", oldValue, value));
		}

        [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		override public function get messageCenterVisible():Boolean {
			for each(var host:Object in _selectedObjects) {
				if(host is AbstractObject && hostProperty in AbstractObject(host).anchors) {
					var obj:Object = AbstractObject(host).anchors[hostProperty];
					if(obj is WireAnchor && WireAnchor(obj).messageCenterVisible) {
						return true;
					}
				} else if(host.hasOwnProperty("preset")) {
					if((host.messageCenterVisible as Array).indexOf(hostProperty) != -1)
						return true;
				}
			}
			return false;
		}
		
		override public function set messageCenterVisible(value:Boolean):void {
			for each(var host:Object in _selectedObjects) {
				if(host is AbstractObject && hostProperty in AbstractObject(host).anchors) {
					var obj:Object = AbstractObject(host).anchors[hostProperty];
					if(obj is WireAnchor) {
                        var wireanchor:WireAnchor = obj as WireAnchor;
						wireanchor.messageCenterVisible = value;
					}
				} else if (host.hasOwnProperty("preset")) {
					var index:int = ((host as InspectorPresets)["messageCenterVisible"] as Array).indexOf(this.modifierDescription.key)
					if (value && index == -1)
						((host as InspectorPresets)["messageCenterVisible"] as Array).push(this.modifierDescription.key);
					else if (!value && index != -1)
						((host as InspectorPresets)["messageCenterVisible"] as Array).splice(index,1);							
				}
			}
		}

        [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		override public function get propertyInspectorVisible():Boolean {
			for each(var host:Object in _selectedObjects) {
				if(host is AbstractObject && hostProperty in AbstractObject(host).anchors) {
					var obj:Object = AbstractObject(host).anchors[hostProperty];
					if(obj is WireAnchor && WireAnchor(obj).propertyInspectorVisible) {
						return true;
					}
				} else if(host.hasOwnProperty("preset")) {
					if((host.propertyInspectorVisible as Array).indexOf(hostProperty) != -1)
						return true;
                } else if (host is TextSelection){
                    return (host as TextSelection).propertyInspector[this.modifierDescription.key];
                }
			}
			return false;
		}
		
		override public function set propertyInspectorVisible(value:Boolean):void {
			for each(var host:Object in _selectedObjects) {
				if(host is AbstractObject && hostProperty in AbstractObject(host).anchors) {
					var obj:Object = AbstractObject(host).anchors[hostProperty];
					if(obj is WireAnchor) {
						WireAnchor(obj).propertyInspectorVisible = value;
					}
				} else if (host is TextSelection){
                    (host as TextSelection).propertyInspector[this.modifierDescription.key] = value;
				} else if (host.hasOwnProperty("preset")) {
					var index:int = ((host as InspectorPresets)["propertyInspectorVisible"] as Array).indexOf(this.modifierDescription.key)
					if (value && index == -1)
						((host as InspectorPresets)["propertyInspectorVisible"] as Array).push(this.modifierDescription.key);
					else if (!value && index != -1)
						((host as InspectorPresets)["propertyInspectorVisible"] as Array).splice(index,1);						
				}
			}
		}

        override public function get keyPress():String
        {
            for each(var host:Object in _selectedObjects) {
                if(host.hasOwnProperty("anchors") && hostProperty in host.anchors) {
                    return WireAnchor(host.anchors[hostProperty]).keyPress;
                }
            }
            return null;
        }

        override public function set keyPress(value:String):void
        {
            for each(var host:Object in _selectedObjects) {
                if(host.hasOwnProperty("anchors") && hostProperty in host.anchors) {
                    WireAnchor(host.anchors[hostProperty]).keyPress = value;
                }
            }
        }

		public function hasWiredReferences():Boolean {
			for each(var host:Object in _selectedObjects) {
				if(host is AbstractObject && hostProperty in AbstractObject(host).anchors) {
					var obj:Object = AbstractObject(host).anchors[hostProperty];
					if(obj is WireAnchor && WireAnchor(obj).wired) {
						return true;
					}
				}
			}
			return false;
		}

		override public function ensureAttached(obj:AbstractObject):void {
			return;
		}

        override public function get hostObject():AbstractObject {
            if(_selectedObjects == null || _selectedObjects.length == 0) {
                return null;
            }
            return _selectedObjects[0] as AbstractObject;
        }
    }
}