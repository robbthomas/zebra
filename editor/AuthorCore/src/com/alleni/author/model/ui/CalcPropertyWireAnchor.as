package com.alleni.author.model.ui
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;

	import com.alleni.savana.Ast;

    import com.alleni.taconite.service.LogService;

    import flash.utils.Dictionary;

    import mx.events.PropertyChangeEvent;
	
	public class CalcPropertyWireAnchor extends WireAnchor implements IDependentAnchor
	{
		private var _anchor:WireAnchor;
		private var _expression:String = "#";
		private var _outVal:Object;
		private var _type:String;
	
		private var _ast:Ast;
	
		public function CalcPropertyWireAnchor(anchor:WireAnchor, display:Boolean, oldUid:String = null, useNewUid:Boolean=false)
		{
			if (oldUid && !useNewUid)
				this.uid = oldUid;
	
			var id:String = oldUid?oldUid:this.uid;
	
			_anchor = anchor;
			modifierDescription = new PropertyDescription("calc;"+_anchor.modifierDescription.key+";"+id, "", _anchor.modifierDescription.label, 'Calculated property- use an expression \nto calculate a new value for this property \n(use "#" for current value)',Modifiers.CALCULATED, 0, false, -1, Modifiers.STRING_TYPE, null, false);
			hostObject = _anchor.hostObject;

			var expressionChild:WireAnchor = new MetaWireAnchor(this, "expression");
			expressionChild.modifierDescription = new PropertyDescription("expression", "", "Expression", "Expression", Modifiers.CALCULATED, 1, false, -1, Modifiers.STRING_TYPE);
			expressionChild.host = this;
			expressionChild.hostProperty = "expression";
			childAnchors.push(expressionChild);
	
			_anchor.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleAnchorChange);
	
			if(display) {
				this.messageCenterVisible = true;
			}
			hostObject.addAnchor(this, false);
            this.initialValues = getCurrentValues();
            this.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, propertyChanged);
			recalc();
		}

        [Bindable]
		public function get expression():String
		{
			return _expression;
		}

		public function set expression(value:String):void
		{
			_expression = value;
			_ast = null;
			recalc();
		}
	
		[Transient]
		[Bindable]
		public function get type():String
		{
			return _type;
		}
	
		public function set type(value:String):void
		{
			_type = value;
		}
	
		private function recalc():void
		{
			if(_ast == null) {
				try {
					var typeEnv:Object = Ast.defaultTypeEnvironment();
					typeEnv["#"] = PropertyDescription(_anchor.modifierDescription).type;
					_ast = new Ast(_expression);
					var t:String = _ast.type(typeEnv);
					if(t == null || t == Modifiers.ENUMERATED_TYPE || t == Modifiers.POINT_TYPE) {
						t = Modifiers.STRING_TYPE;
					}
					PropertyDescription(modifierDescription).overrideType(t);
					type = t;
				} catch(error:Error) {
					LogService.error("Error in parsing calculated ribbon: " + error.message + " object="+hostObject.logStr);
					PropertyDescription(modifierDescription).overrideType(Modifiers.STRING_TYPE);
					type = Modifiers.STRING_TYPE;
					_outVal = "#Error";
					valueChanged();
					trigger();
				}
			}
			try {
				var env:Object = Ast.defaultEnvironment()
				env["#"] = _anchor.value;
				_outVal = Modifiers.convertValue(type, _ast.evaluate(env));
			} catch(error:Error) {
				LogService.error("Error in evaluating calculated ribbon: " + error.message + " object="+hostObject.logStr);
				_outVal = Modifiers.convertValue(type, null);
			}
			valueChanged();
			trigger();
		}
	
	
		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		override public function get value():Object
		{
			return _outVal;
		}
	
		override public function set value(val:Object):void
		{
			//nada readonly
		}
	
		private function handleAnchorChange(event:PropertyChangeEvent):void
		{
			if(event.property != "value") {
				return;
			}
			recalc();
		}


        [Transient]
        public function get prototypeAnchor():WireAnchor
        {
            return _anchor;
        }

        [Transient]
        public function get prototypeProperty():String
        {
            return calcProperty;
        }

		public function get calcProperty():String
		{
			return _anchor.hostProperty;
		}

        override public function getCurrentValues(objectIds:Dictionary = null):Object {
            var result:Object = {
                expression:expression
            };
            return result;
        }

        override public function setCurrentValues(values:Object, objectPropStorage:Object=null):void {
            expression = values.expression;
        }

        public function propertyChanged(event:PropertyChangeEvent):void {
            if(Application.running) {
                return;
            }
            switch(event.property) {
                case "expression":
                    initialValues[event.property] = event.newValue;
                    break;
            }
        }
	}
}
