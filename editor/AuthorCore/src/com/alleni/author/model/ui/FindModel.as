package com.alleni.author.model.ui
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;

	public class FindModel extends EventDispatcher
	{
        [Bindable] public var viewStackPage:int = 0;
        public static const BY_TITLE_PAGE:int = 0;
        public static const BY_RIBBON_PAGE:int = 1;
        public static const BY_ID_PAGE:int = 2;

        // results for all find-pages:
        [Bindable] public var foundObjects:ArrayCollection = new ArrayCollection();

        // BY_TITLE_PAGE
		[Bindable] public var targetTitle:String = "";   // also used on RIBBON page

        // BY_RIBBON_PAGE
        [Bindable] public var selectedCategory:Object;  // Object in dataProvider:  {label:"xyz", data:"xyz"}
        [Bindable] public var selectedRibbon:Object;    // Object in dataProvider:  {label:"Location X", data:"x"}
        [Bindable] public var targetValue:String = "";
        [Bindable] public var allCategories:ArrayCollection = new ArrayCollection();
        [Bindable] public var ribbonsInCategory:ArrayCollection = new ArrayCollection();

        public function get selectionRibbonKey():String
        {
            if (selectedRibbon == null)
                return null;
            else
                return selectedRibbon.data;
        }


        public function get selectedCategoryName():String
        {
            if (selectedCategory == null)
                return null;
            else
                return selectedCategory.data;
        }


        // BY_ID_PAGE
		[Bindable] public var targetUID:String = "";
	}
}