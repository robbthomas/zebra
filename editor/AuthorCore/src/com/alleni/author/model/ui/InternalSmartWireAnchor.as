package com.alleni.author.model.ui
{
    import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.model.AbstractObject;
    import mx.events.PropertyChangeEvent;
	import mx.utils.UIDUtil;
	
	public class InternalSmartWireAnchor extends WireAnchor 
	{
		public var other:ExternalSmartWireAnchor = null;
		private var _internalID:String;
		public var portSide:int;
		public var position:int;
	
		public function InternalSmartWireAnchor(makeOther:Boolean = true, innerID:String = null, otherID:String = null, pPortSide:int = -1, pPosition:int = -1)
		{
			_internalID = (innerID? innerID : UIDUtil.createUID());
			if (makeOther) {
				other = new ExternalSmartWireAnchor(otherID, false);
				other.other = this;
				
				portSide = pPortSide;
				position = pPosition;
			}
			super();
		}

		override public function trigger():void
		{
			ApplicationController.instance.wireController.anchorTriggered(other);
		}

		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		override public function get value ():Object
		{
			return other.value
		}
		
		override public function handleTrigger():void
		{
			trigger();
		}

		override public function set value (val:Object):void
		{
            other.value = val;
		}
		
		public function get internalID():String
		{
			return _internalID;
		}

        override public function get label():String {
            return other.label;
        }

        override public function set label(val:String):void {
            other.label = val;
        }

		override public function ensureAttached(obj:AbstractObject):void {
			other.ensureAttached(obj);
		}
    }
}
