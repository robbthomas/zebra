package com.alleni.author.model.ui
{
	import flash.events.EventDispatcher;

	public class ProgressBarModel extends EventDispatcher
	{
	    [Bindable] public var visible:Boolean;
	    [Bindable] public var progress:Number = 0;  // 0 to 1
	}
}
