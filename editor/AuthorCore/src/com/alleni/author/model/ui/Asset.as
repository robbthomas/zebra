package com.alleni.author.model.ui
{
	import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.definition.AssetDescription;
import com.alleni.author.definition.AssetType;
	import com.alleni.author.model.objects.IListable;
import com.alleni.author.service.assets.AssetLoadOperation;
import com.alleni.taconite.service.IOperation;
	
	/**
	 * some external asset which will be used within AbstractObjects
	 * 
	 */
	public class Asset extends ExternalContentObject implements IListable, IReplaceable
	{
		[Transient]
		[Bindable]
		public var url:String 					= null;
		
		[Transient]
		public var loadOperation:IOperation		= null;
		
		[Transient]
		public var fileID:String 				= null;
		
		private var _type:int = -1;
		[Bindable]		
		public function get type():int
		{
			return _type;
		}
		public function set type(value:int):void
		{
			_type = value;
			propertyType = AssetType.getPropertyType(type);
		}
		
		[Bindable]
		public var propertyType:String 	= "";
		
		[Bindable]
		public var mimeType:String 		= "application/octet-stream";
		
		[Transient]
		[Bindable]
		public var path:String 			= null;
		
		[Transient]
		[Bindable]
		public var upToDate:Boolean 	= false;
		
		function Asset(id:String):void
		{
			_assetID = id;
		}

        [Transient]
        private var _pendingData:Boolean = false;

        public function get pendingData():Boolean {
            return _pendingData;
        }
        public function set pendingData(value:Boolean):void {
            _pendingData = value;
        }


        [Transient]
        [Bindable]
        override public function get content():Object {
            return super.content;
        }

        override public function set content(value:Object):void {
            _content = value;
            pendingData = false;
        }

		private var _assetID:String;
		public function get assetID():String
		{
			return _assetID;
		}
	
		override public function toString():String
		{	
			return "[Asset "+name+" content=" + (this.content != null) + " : " + this.assetID + " : path=" + path +"]";
		}
		
		public function get id():String
		{
			return assetID;
		}

        public function get referenceJSON():Object {
            return {
                type:"AssetReference",
                id:assetID
            }
        }
		
		public function toJson():Object
		{
			return {
				type:"Asset",
				id:assetID,
				mimeType:mimeType,
				name:name
			}
		}

        private static function fromMetadataJson(json:Object, existingAsset:Asset = null):Asset {
            var asset:Asset = existingAsset ? existingAsset : new Asset(json.id);
            asset._assetID = json.id;
            asset.type = AssetDescription.validateMimeType(json.mimeType);
            asset.fileID = json.fileID;
            asset.mimeType = json.mimeType;
        	asset.name = json.name;
            if ("propertyType" in json) {
                asset.propertyType = json.propertyType;
            }
            return asset;
        }
		
		public static function fromJson(json:Object, index:int=-1, preloadMedia:Boolean=true, existingAsset:Asset = null):Asset
		{
            var asset:Asset;
            if("fileID" in json) {
                asset = fromMetadataJson(json, existingAsset);
                AssetController.instance.addToPreviewQueue(asset);
            } else {
                asset = AssetController.instance.requestForID(json.id, index, preloadMedia, existingAsset);
            }

            if (asset == null) {
                asset = existingAsset;
            }
			if ("propertyType" in json) {
				asset.propertyType = json.propertyType;
			}
			return asset;
		}

        public static function fromJsonList(json:Object):Vector.<Asset> {
            var result:Vector.<Asset> = new Vector.<Asset>();
            for each(var assetJson:Object in json) {
                var asset:Asset = fromMetadataJson(assetJson);
                result.push(asset);
            }
            return result;
        }

        public static function idFromJson(json:Object):String {
            if (json != null && "type" in json) {
                var type:String = json.type;
                if ((type == "AssetReference" || type == "Asset") && "id" in json) {
                    var id:String = json["id"];
                    if (id != null && id.length > 0) {
                        return id;
                    }
                }
            }
            return null;
        }
	}
}