/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 11/20/12
 * Time: 8:27 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.ui {
import com.alleni.author.controller.ui.AssetController;
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.model.project.Project;
import com.alleni.taconite.dev.Utilities;

import flash.utils.Dictionary;

import mx.events.PropertyChangeEvent;

public class UsageCounts {

    private var _counts:Dictionary = new Dictionary();   // detail usage in tables, setters, etc.  key=id or Project, value=CountInfo

    public function UsageCounts() {
    }

    public function countAsset(asset:Asset):void
    {
        var id:String = asset.id;
        if (id != null) {
            var info:CountInfo = findOrCreateInfoForKey(id);
            ++info.count;
        }
    }

    public function countAssetJson(json:Object):void
    {
        var id:String = Asset.idFromJson(json);
        if (id != null) {
            var info:CountInfo = findOrCreateInfoForKey(id);
            ++info.count;
        }
    }

    public function countGadget(gadget:Project):void
    {
        if (gadget != null) {
            var key:* = (gadget.id != null) ? gadget.id : gadget;
            var info:CountInfo = findOrCreateInfoForKey(key);
            ++info.count;
        }
    }

    private function findOrCreateInfoForKey(key:*):CountInfo
    {
        var info:CountInfo = _counts[key] as CountInfo;
        if (info == null) {
            info = new CountInfo();
            _counts[key] = info;
            var gadget:Project = key as Project;
            if (gadget) {    // listen for projectID to be delivered from server
                gadget.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, gadgetIdListener);
            }
        }
        return info;
    }

    private function gadgetIdListener(event:PropertyChangeEvent):void {
        // replace the entries keyed by Project and replace with entries keyed by gadget.id
        if (event.property == "projectId") {
            var gadget:Project = event.target as Project;
            gadget.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, gadgetIdListener);
            var info:CountInfo = _counts[gadget] as CountInfo;
            Utilities.assert(info != null);
            _counts[gadget.id] = info;
            delete _counts[gadget];
        }
    }

    public function getAssetUsageCount(asset:Asset):int
    {
        var id:String = asset.id;
        if (id != null) {
            var info:CountInfo = _counts[id];
            if (info) {
                return info.count;
            }
        }
        return 0;
    }

    public function getGadgetUsageCount(gadget:Project):int
    {
        var key:* = (gadget.id != null) ? gadget.id : gadget;
        if (key != null) {
            var info:CountInfo = _counts[key];
            if (info) {
                return info.count;
            }
        }
        return 0;
    }

    public function getReplaceableUsageCount(replaceable:IReplaceable):int
    {
        var gadget:Project = replaceable as Project;
        if (gadget) {
            return getGadgetUsageCount(gadget);
        }
        var asset:Asset = replaceable as Asset;
        if (asset) {
            return getAssetUsageCount(asset);
        }
        return 0;
    }

    public function addFromList(otherList:UsageCounts):void
    {
        for (var key:* in otherList._counts) {
            var other:CountInfo = otherList._counts[key] as CountInfo;
            var info:CountInfo = findOrCreateInfoForKey(key);
            info.addFromOther(other);
        }
    }

    public function getAssetsList():Vector.<Asset>
    {
        var list:Vector.<Asset> = new <Asset>[];
        for (var key:* in _counts) {
            if (key is String) {
                var id:String = key as String;
                var asset:Asset = AssetController.instance.getExistingAsset(id);
                if (asset) {
                    list.push(asset);
                }
            }
        }
        return list;
    }

    public function getGadgetsList():Vector.<Project>
    {
        var list:Vector.<Project> = new <Project>[];
        var key:*;
        var gadget:Project;
        for (key in _counts) {
            if (key is String) {
                var id:String = key as String;
                gadget = GadgetController.instance.getExistingGadget(id);
                if (gadget) {
                    list.push(gadget);
                }
            }
        }
        return list;
    }

    public function clone():UsageCounts
    {
        return UsageCounts.fromJson(this.toJson());
    }

    public function toJson():Object {
        var result:Object = {};
        result.type = "UsageCounts";
        result.counts = [];

        for (var key:* in _counts) {
            var info:CountInfo = _counts[key];
            var json:Object = {};
            json.id = key;  // String or Project ....  Json encoder will change Project to id
            json.count = info.count;
            result.counts.push(json);
        }
        return result;
    }

    public static function fromJson(json:Object):UsageCounts {
        var usage:UsageCounts = new UsageCounts();
        if ("counts" in json && json.counts is Array) {
            var array:Array = json.counts;
            var len:int = array.length;
            for (var n:int = 0; n < len; n++) {
                var oneJson:Object = array[n];
                var id:String = oneJson.id as String;
                if (Utilities.UUID_REGEX.test(id)) {  // id looks good
                    var info:CountInfo = usage.findOrCreateInfoForKey(id);
                    info.count = oneJson.count as int;
                } else {
                    // bad id occurs when Project.projectID is null when JSONEncoder tries to resolve an instance of Project.
                    // its ends up with a string  "[object object]"
                    // this can happen when ObjectExistenceAction tries to save the objects when closing a gadget
                    // The situation will be resolved when SmartObjectController gets loading=false and calls setupUsageCounts.
                    return null;  // null that will be noticed by setupUsageCounts.
                }
            }
        }
        return usage;
    }

    public function dump():void
    {
        trace("UsageCounts:");
        for (var key:* in _counts) {
            var info:CountInfo = _counts[key] as CountInfo;
            var gadget:Project = key as Project;
            if (gadget) {
                trace("   GADGET: name="+gadget.name, "count="+info.count);
            } else {
                var id:String = key as String;
                gadget = GadgetController.instance.getExistingGadget(id);
                if (gadget) {
                    trace("   gadget: name="+gadget.name, "count="+info.count);
                } else {
                    var asset:Asset = AssetController.instance.getExistingAsset(id);
                    trace("   asset: id="+id, "name="+asset.name, "count="+info.count);
                }
            }
        }
    }
}
}

class CountInfo {
    public var count:int = 0;

    public function CountInfo(count:int=0) {
        this.count = count;
    }

    public function addFromOther(other:CountInfo):void
    {
        count += other.count;
    }
}
