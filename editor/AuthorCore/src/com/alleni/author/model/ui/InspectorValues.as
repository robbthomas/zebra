package com.alleni.author.model.ui
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.definition.InspectorPresets;
	import com.alleni.author.definition.InspectorRibbonDescriptions;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.Objects;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.Styles;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
import com.alleni.author.model.objects.ProjectObject;
import com.alleni.taconite.definition.ObjectDefinition;
	import com.alleni.taconite.model.ITaconiteObject;
	import com.alleni.taconite.model.TaconiteObject;
	
	import flash.utils.Dictionary;

	public class InspectorValues extends TaconiteObject
	{
		public static var instance:InspectorValues = new InspectorValues();
		
		// Array of dynamic objects to contain preset values for new objects being created (per object type)
		public var valueArray:Array = [];
		
		[Transient]
		public var anchors:Dictionary = new Dictionary(false); // key -> WireAnchor
		
		[Transient]
		public var modifiers:Dictionary = new Dictionary(false); // key -> IModifier
		
		[Transient]
		public var inletModifiers:Dictionary = new Dictionary(false); // key -> IModifier
		
		[Transient]
		public var outletModifiers:Dictionary = new Dictionary(false); // key -> IModifier
		
		[Transient]
		public var selectedInlets:Dictionary = new Dictionary(false); // key -> IModifier

		[Transient]
		public var selectedOutlets:Dictionary = new Dictionary(false); // key -> IModifier

		[Transient]
		public var selectedProperties:Dictionary = new Dictionary(false); // key -> IModifier

		public static function createNew():void
		{
			instance.anchors = null;
			instance = new InspectorValues();
		}

		public function InspectorValues()
		{
			makeAnchor("poweredOnRun");
		}

		private function makeAnchor(property:String):void
		{
			var prop:PropertyDescription = Modifiers.instance.fetch(property) as PropertyDescription;
			var anchor:ForwardingWireAnchor = new ForwardingWireAnchor(null);
			anchor.modifierDescription = prop;
			anchor.hostObject = null;//maybe inspector
			anchor.hostProperty = property;
			anchors[property] = anchor;
		}

		public function clearModifiers():void
		{
			var anchor:ForwardingWireAnchor;
			var property:String;
			for(property in modifiers) {
				anchor = anchors[property] as ForwardingWireAnchor;
				if(anchor == null) {
					continue;
				}
//				anchor.selectedObjects = new Vector.<ITaconiteObject>();
			}
			
			modifiers = null;
			inletModifiers = null;
			outletModifiers = null;
		}
		
		public function setPresets(shortClassName:String, property:String, value:Object):void
		{
			var values:Object = getPreset(shortClassName);
			values[property] = value;
		}
		
		public function getPreset(shortClassName:String):ITaconiteObject
		{
			var values:InspectorPresets = null;
			
			var n:int;
			var len:int = valueArray.length;
			// Look for existing values for this object type
			for (n = 0; n < len; n++) {
				if (valueArray[n].shortClassName == shortClassName) {
					values = valueArray[n].values;
				}
			}
			
			// TODO: If values don't exist, use default values
			if (values == null) {
				values = new InspectorPresets();
				valueArray.push({shortClassName:shortClassName,	values:values});
				var aoDefinition:ObjectDefinition = (Objects.descriptionForShortClassname(shortClassName) as ObjectDefinition);
				if (aoDefinition && aoDefinition.name != "ProjectObject") {
					var aoClass:Class = aoDefinition.model;
					var temporaryObject:AbstractObject = new aoClass() as AbstractObject;
					values.modifiers = temporaryObject.modifiers;
				}
				
				switch (shortClassName){
					case "VectorDrawing":
						setProperties(values, 0, GraphicFill.SOLID_FILL, false)
						break;
					case "RectangleObject":
						setProperties(values, 1, GraphicFill.GRADIENT_FILL, false, 0, 0, true)
						break;
					case "Oval":
						setProperties(values, 2, GraphicFill.GRADIENT_FILL, false, 0, 0, false)
						break;
					case "Line":
						setProperties(values, 3, GraphicFill.SOLID_FILL, false)
						break;
					case "Slider":
						setProperties(values, 4, GraphicFill.GRADIENT_FILL, false)
						break;
					case "PushButton":
					case "Button":
						setProperties(values, 5, GraphicFill.JELLYBEAN_FILL, true, 0)
						break;
					case "TextObject":
						setProperties(values, 7, GraphicFill.SOLID_FILL, true, 2)
						break;
					case "PathObject":
						setProperties(values, 8, GraphicFill.SOLID_FILL, false)
						break;
					case "Arena":
						setProperties(values, 9, GraphicFill.SOLID_FILL, false)
						break;
                    case "InputText":
                        // The input text object has not been using its presets!! In order to fix a bug where the input
                        // text object's margin was wrong, I added just this specific case.  It's possibly that more of
                        // the presets will need to be adjusted in the future though.  In that case, it might be good to
                        // call "setProperties(...)" like we do for the rest of the objects.
                        values["margin"] = Styles.TEXT_PRESETS[3]["textMargin"];
                        break;
				}

				values["messageCenterVisible"] = [];
				values["propertyInspectorVisible"] = [];

				switch (shortClassName) {
					case "PushButton":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.BUTTON_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.BUTTON_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.BUTTON_FONT_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.BUTTON_PARAGRAPH_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.BUTTON_GEOMETRY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.BUTTON_APPEARANCE_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "Checkbox":
					case "RadioButton":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TOGGLE_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TOGGLE_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TOGGLE_FONT_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.GEOMETRY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TOGGLE_APPEARANCE_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "TextObject":
					case "Text":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TEXT_FONT_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TEXT_PARAGRAPH_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.GEOMETRY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TEXT_APPEARANCE_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "InputText":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.INPUT_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.INPUT_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.INPUT_FONT_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.INPUT_PARAGRAPH_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.GEOMETRY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.INPUT_APPEARANCE_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "RectangleObject":
					case "Oval":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.SHAPE_GEOMETRY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.SHAPE_APPEARANCE_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "Arena":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.ARENA_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.ARENA_GEOMETRY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.ARENA_ACTIONS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.ARENA_APPEARANCE_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.ARENA_ALIGN_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
                    case "ArenaPage":
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.PAGE_INSPECTOR_RIBBONS);
                        break;
					case "Composite":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "Slider":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.SLIDER_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.SLIDER_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.SLIDER_GEOMETRY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.SLIDER_APPEARANCE_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "Clock":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.CLOCK_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.CLOCK_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.CLOCK_ACTIONS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.CLOCK_APPEARANCE_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "MapObject":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.MAP_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.MAP_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.GEOMETRY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "Line":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.GEOMETRY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.LINE_APPEARANCE_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "Audio":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUDIO_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUDIO_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUDIO_ACTIONS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "VideoObject":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.VIDEO_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.VIDEO_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.VIDEO_ACTIONS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.VIDEO_GEOMETRY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.VIDEO_APPEARANCE_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "TruthTable":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TTABLE_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TTABLE_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TTABLE_SHOW_RIBBONS);
						break;
					case "AnswerTable":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.ANSWER_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.ANSWER_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.ANSWER_SHOW_RIBBONS);
						break;
					case "StateTable":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.STABLE_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.STABLE_VALUES_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.STABLE_SHOW_RIBBONS);
						break;
					case "Calculator":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.STABLE_VISIBILITY_RIBBONS);
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
						break;
					case "TextSelection":
						values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.TEXT_FONT_RIBBONS);
						break;
                    case "PathObject":
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.VISIBILITY_RIBBONS);
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.PATH_GEOMETRY_RIBBONS);
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.PATH_APPEARANCE_RIBBONS);
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
                        break;
                    case "Drawing":
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.DRAWING_VISIBILITY_RIBBONS);
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.DRAWING_ACTIONS_RIBBONS);
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.DRAWING_GEOMETRY_RIBBONS);
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EFFECTS_RIBBONS);
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.AUTHOR_NOTES);
                        break;
                    case "EventPage":
                        values["propertyInspectorVisible"] = values["propertyInspectorVisible"].concat(InspectorRibbonDescriptions.EVENT_ACTIONS_RIBBONS);
                        break;
				}

				switch (shortClassName) {
					case "PushButton":
						values["messageCenterVisible"] = values["messageCenterVisible"].concat(["buttonPressed"]);
						break;
					case "Checkbox":
						values["messageCenterVisible"] = values["messageCenterVisible"].concat(["checked"]);
						break;
					case "InputText":
						values["messageCenterVisible"] = values["messageCenterVisible"].concat(["text"]);
						break;
					case "Slider":
						values["messageCenterVisible"] = values["messageCenterVisible"].concat(["sliderValue"]);
						break;
					case "Clock":
						values["messageCenterVisible"] = values["messageCenterVisible"].concat(["restart","secondsElapsed"]);
						break;
				}
			}
			return values;
		}
		
		private function setProperties(values:ITaconiteObject, index:uint, initialFillType:uint, hasText:Boolean = false, textIndex:uint=0, angle:uint = 0, linear:Boolean = false):void
		{
			values["lineColor"] = 		Styles.DRAWING_PRESETS[index]["lineColor"];
			values["lineAlpha"] = 		Styles.DRAWING_PRESETS[index]["lineAlpha"];
			values["lineThickness"] = 	Styles.DRAWING_PRESETS[index]["lineThickness"];
			values["fillAlpha"] = 		Styles.DRAWING_PRESETS[index]["fillAlpha"];
			values["fillGradient"] =    Styles.DRAWING_PRESETS[index]["fillGradient"];
			values["linearGradient"] =  Styles.DRAWING_PRESETS[index]["linearGradient"];
			values["shineGradient"] =   Styles.DRAWING_PRESETS[index]["shineGradient"];
			
			values["visible"] = true;
			values["hideOnRun"] = false;
			values["cloakOnRun"] = false;
			values["cloakedNow"] = false;
			values["alpha"] = 100;
			
			if(hasText){
				values["fontFamily"] = Styles.TEXT_PRESETS[textIndex]["fontName"];
				values["fontSize"] = Styles.TEXT_PRESETS[textIndex]["fontSize"];
				values["fontColor"] = Styles.TEXT_PRESETS[textIndex]["fontColor"];
				values["textAlignH"] = Styles.TEXT_PRESETS[textIndex]["alignHorz"];
				values["textAlignV"] = Styles.TEXT_PRESETS[textIndex]["alignVert"];
				values["margin"] = Styles.TEXT_PRESETS[textIndex]["textMargin"];
				values["bold"] = Styles.TEXT_PRESETS[textIndex]["fontBold"];
				values["italic"] = Styles.TEXT_PRESETS[textIndex]["fontItalic"];
				values["underline"] = Styles.TEXT_PRESETS[textIndex]["fontUnderline"];
				values["fontAlpha"] = Styles.TEXT_PRESETS[textIndex]["fontOpacity"];
			}
			
			switch(initialFillType){
				case GraphicFill.SOLID_FILL:
					values["fillSpec"] = createSolid(Styles.DRAWING_PRESETS[index]["fillColor"]);
					break;
				case GraphicFill.GRADIENT_FILL:
					values["fillSpec"] = createGradient(Styles.DRAWING_PRESETS[index]["fillGradient"], angle, linear);
					break;
				case GraphicFill.JELLYBEAN_FILL:
					values["fillSpec"] = createShine(Styles.DRAWING_PRESETS[index]["shineGradient"]);
					break;
				default:
					values["fillSpec"] = createSolid(Styles.DRAWING_PRESETS[index]["fillColor"]);
					break;
			}
		}
		
		private function createSolid(values:Vector.<uint>):GraphicFill
		{	
			var graphicFill:GraphicFill = new GraphicFill();
			graphicFill.type = GraphicFill.SOLID_FILL;
			graphicFill.color = values[0];
			
			return graphicFill;
		}
		
		private function createGradient(values:Vector.<uint>, angle:uint = 0, linear:Boolean = true):GraphicFill
		{
			var graphicFill:GraphicFill = new GraphicFill();
			graphicFill.type = GraphicFill.GRADIENT_FILL;
			graphicFill.linear = linear;
			graphicFill.colors = values;
			graphicFill.angle = angle;
			
			return graphicFill;
		}
		
		private function createShine(values:Vector.<uint>):GraphicFill
		{
			var graphicFill:GraphicFill = new GraphicFill();
			graphicFill.type = GraphicFill.JELLYBEAN_FILL;
			graphicFill.color1 = values[0];
			graphicFill.color2 = values[1];
			
			return graphicFill;
		}
	}
}