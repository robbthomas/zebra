package com.alleni.author.model.ui
{
	import com.alleni.author.model.objects.IListable;
	import com.alleni.taconite.dev.Utilities;
	
	import flash.events.EventDispatcher;

import mx.collections.IList;

	public class Library extends EventDispatcher
	{
		private static var _instance:Library;
		
		[Bindable] public var pageList:IList;
		
		[Bindable] public var selectedItem:IListable;

        [Bindable] public var usageCounts:UsageCounts = new UsageCounts();

		
		public function Library()
		{
			Utilities.assert(!_instance);
			super();
		}
		
		public static function get instance():Library
		{
			if (_instance == null)
				_instance = new Library();
			return _instance;
		}
	}
}