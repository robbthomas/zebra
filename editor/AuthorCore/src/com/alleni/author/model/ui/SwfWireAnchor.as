package com.alleni.author.model.ui
{
import com.alleni.author.definition.IModifier;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.OutletDescription;
import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.objects.SWFObject;
	
	import mx.events.PropertyChangeEvent;
	
	public class SwfWireAnchor extends WireAnchor
	{
		public function SwfWireAnchor(host:SWFObject, modifier:IModifier, label:String, category:String)
		{
			super();
			_host = host;
			hostPropertyIndex = -1;
		}

        public static function prop(host:SWFObject, prop:String, label:String, category:String, type:String):SwfWireAnchor {
			var modifier:IModifier = new PropertyDescription(prop, "", label, "", category, 0, false, -1, type);
            var result:SwfWireAnchor = new SwfWireAnchor(host, modifier,  label,  category);
            result.hostProperty = prop;
            return result;
        }

        public static function inlet(host:SWFObject, key:String, label:String, category:String):SwfWireAnchor {
			var modifier:IModifier = new InletDescription(key, "", label, "", category, 0, false, -1);
            var result:SwfWireAnchor = new SwfWireAnchor(host, modifier,  label,  category);
            return result;
        }

        public static function outlet(host:SWFObject, key:String, label:String, category:String):SwfWireAnchor {
			var modifier:IModifier = new OutletDescription(key, "", label, "", category, 0, false, -1);
            var result:SwfWireAnchor = new SwfWireAnchor(host, modifier,  label,  category);
            return result;
        }
	
		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		override public function get value():Object
		{
			return SWFObject(_host).content[hostProperty];
		}
	
		override public function set value(val:Object):void
		{
			SWFObject(_host).content[hostProperty] = val;
		}
	
		override public function get isAdditionAnchor():Boolean
		{
			return false;
		}

        override public function handleTrigger():void {
            SWFObject(_host).swfHandleInlet(modifierDescription.key);
        }
    }
}