/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 1/18/13
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.ui {
    public class NumberValueControlConstraint {
        public static const SIMPLE_VALUE:String = "Simple Value";
        public static const SLIDER:String = "Slider";

        private var _controlMin:Number;
        private var _controlMax:Number;
        private var _valueMin:Number;
        private var _valueMax:Number;
        private var _controlType:String

        public function NumberValueControlConstraint(defaultControl:String, controlMin:Number,  controlMax:Number, valueMin:Object=null, valueMax:Object=null) {
            controlType = defaultControl;
            _controlMin = controlMin;
            _controlMax = controlMax;
            _valueMin = valueMin==null?_controlMin:valueMin as Number;
            _valueMax = valueMax==null?_controlMax:valueMax as Number;
        }

        public function get controlType():String{
            return _controlType;
        }
        public function set controlType(value:String):void{
            switch(value){
                case SLIDER:
                case SIMPLE_VALUE:
                    _controlType = value;
                    break;
                default:
                    _controlType = SIMPLE_VALUE;
            }
        }

        public function get controlMin():Number {
            return _controlMin;
        }
        public function set controlMin(value:Number):void {
            _controlMin = value;
        }

        public function get controlMax():Number {
            return _controlMax;
        }
        public function set controlMax(value:Number):void {
            _controlMax = value;
        }

        public function get valueMin():Number {
            return _valueMin;
        }
        public function set valueMin(value:Number):void {
            _valueMin = value;
        }

        public function get valueMax():Number {
            return _valueMax;
        }
        public function set valueMax(value:Number):void {
            _valueMax = value;
        }
    }
}
