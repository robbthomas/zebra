package com.alleni.author.model.ui
{
	import com.alleni.taconite.factory.IQAVellum;

	public class QAVellumSettings
	{
		public static const VELLUM_COLOR:uint = 0xfeffb5;
		public static const VELLUM_ALPHA:Number = 0.1;
		
		public static var drawingPreset:uint = 2;
		public static var vellum:IQAVellum;
	}
}