/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/14/11
 * Time: 10:38 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.ui
{
	import com.alleni.author.definition.PropertyDescription;
	
	/**
	 * This class is for parent anchors that monitor their child so closely they steal the child
	 * property editor while the parent is closed so you can edit the child value even when not
	 * normally visible.
	 */
	public class HelicopterWireAnchor extends WireAnchor
	{
		public function HelicopterWireAnchor()
		{
		}
	
		[Transient]
		public function get childModifier():PropertyDescription
		{
			return childAnchors[0].modifierDescription as PropertyDescription;
		}
	
		[Bindable]
		[Transient]
		public function get childValue():Object
		{
			return childAnchors[0].value;
		}
	
		public function set childValue(value:Object):void
		{
			childAnchors[0].value = value;
		}

        public function get childPath():String {
            return childAnchors[0].path;
        }
	}
}
