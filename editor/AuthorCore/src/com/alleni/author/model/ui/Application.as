/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: AuthorDesktop.mxml 2230 2009-10-02 17:46:40Z mvalentiner $
 */
package com.alleni.author.model.ui
{
import com.alleni.author.application.ui.EditorUI;
import com.alleni.author.controller.test.TestPanelController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.AssetDragDescription;
import com.alleni.author.controller.ui.RibbonDragDescription;
import com.alleni.author.controller.ui.WireDragDescription;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.controller.ui.palettes.ToolboxController;
import com.alleni.author.definition.Objects;
import com.alleni.author.definition.application.ApplicationStrings;
import com.alleni.author.definition.application.NotificationNamesApplication;
import com.alleni.author.definition.application.NotificationNamesEditor;
import com.alleni.author.document.Document;
import com.alleni.author.model.World;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.taconite.definition.ObjectDefinition;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.factory.IEnvironment;
import com.alleni.taconite.factory.ITaconiteWindow;
import com.alleni.taconite.factory.TaconiteFactory;
import com.alleni.taconite.service.IConfigurationService;
import com.alleni.taconite.view.ViewContext;

import flash.events.EventDispatcher;

import mx.events.PropertyChangeEvent;

public class Application extends EventDispatcher
	{
		//This is a temp solution to white list people so they can use templates
        private var _isSpecialFeatureWhiteList:Boolean;
        public function get isSpecialFeatureWhiteList():Boolean{
            return environment.email.indexOf("alleni.com") > 0;
        }


		// format version of a stored project file
		public static const PROJECT_FILE_FORMAT_VERSION:int = 2;
		
		public static const NONE_ARRANGEMENT:int 		= 0;
		public static const SINGLE_ARRANGEMENT:int 		= 1;
		public static const STACKED_ARRANGEMENT:int 	= 2;
		public static const TILED_ARRANGEMENT:int 		= 3;
		
		[Transient]
		public static var instance:Application = null;
		
		[Bindable]
		public var height:Number;
		
		[Bindable]
		public var width:Number;
		
		[Bindable]
		public var libraryVisible:Boolean;
		
		[Bindable]
		public var variableLibraryVisible:Boolean;
		
		[Bindable]
		public var dockVisible:Boolean;
		
		[Bindable]
		public var inspectorVisible:Boolean;

		[Bindable]
		public var ribbonLibraryVisible:Boolean;

		[Bindable]
		[Transient]
		public var qaVisible:Boolean;
		
		[Bindable]
		[Transient]
		public var logVisible:Boolean;
		
		[Bindable]
		[Transient]
		public var testPanelVisible:Boolean;
        [Transient]
        public var testObjectActivitySerial:int = 0;  // for test-panel to detect activity from Tweens

		[Transient]
		[Bindable]
		public var outlineVisible:Boolean;
		
        [Transient]
        [Bindable]
        public var findVisible:Boolean;

		[Bindable]
		[Transient]
		public var embedVisible:Boolean;
		
		[Bindable]
		[Transient]
		public var audioEnabled:Boolean = true;
		
        [Bindable]
        public var toolboxVisible:Boolean;   // current visibility maintained automatically by ToolboxController when pause/resume
        [Bindable]
        public var toolboxVisibleWhenPaused:Boolean = true;  // value set by author actions of hide/show toolbox

		[Bindable]
		public var toolboxX:Number;
		
		[Bindable]
		public var toolboxY:Number;

		[Bindable]
		public var windowArrangement:int;
		
		public var draggingMessageCenter:Boolean;
		public var wireDragDescription:WireDragDescription;
		public var ribbonDragDescription:RibbonDragDescription;
		public var assetDragDescription:AssetDragDescription;
		public var draggingSlider:Boolean = false; // used to disable UI element rollovers (e.g., ribbon controls) when dragging a UI slider
		
		[Transient]
		public var configurationService:IConfigurationService;

		[Bindable]
		public var zoomLevel:Number = 1;
		
		[Transient]
		[Bindable]
		public var showAll:Boolean = false;
				
		public var defaultUserMessage:String = ApplicationStrings.DEFAULT_STATUS_MESSAGE;
		
		[Transient]
		public var viewContext:ViewContext;
		
		[Transient]
		[Bindable]
		public var isAdmin:Boolean;

        [Transient]
		[Bindable]
		public var customerType:String = "CREATOR";

        [Transient]
        public var projectFromOtherUser:Boolean;

        [Transient]
        public var showHistory:Boolean;

        [Transient]
        public var progressBar:ProgressBarModel = new ProgressBarModel();

        [Transient]
        private var _publishingInProgress:Boolean = false;

        private var _lmsEnabled:Boolean;

        public var projectWiringLevel:int = 3;

        //Initial Value changes at run time properties
        private var _tempEditCompleted:Boolean;
        private var _editWhileRunningInProgress:Boolean;

        public function get tempEditComplete():Boolean{
            return _tempEditCompleted;
        }
        public function set tempEditComplete(value:Boolean):void{
            _tempEditCompleted = value;
        }

        public function get editWhileRunningInProgress():Boolean{
            return _editWhileRunningInProgress;
        }
        public function set editWhileRunningInProgress(value:Boolean):void{
            if(_editWhileRunningInProgress == value) {
                return;
            }
            _editWhileRunningInProgress = value;
            if(!_editWhileRunningInProgress){
                _tempEditCompleted = true;
            }
        }

        public function get quaziPauseModo():Boolean {
            return _editWhileRunningInProgress || _tempEditCompleted;
        }


		private var _window:ITaconiteWindow;
		private var _document:Document;
		private var _numberOfUntitledProjects:int;
		private var _currentTool:int;
		
		public function Application()
		{
			Utilities.assert(instance == null);

			instance = this;
			_numberOfUntitledProjects = 0;
		}

		[Bindable]
		public function get currentTool():int
		{
			return _currentTool;
		}
		
		public function set currentTool(value:int):void
		{
			_currentTool = value;
		}
		
		/** Creates a default untitled project name. */
		[Transient]
		public function get nextUntitledProjectName():String
		{
            const environment:IEnvironment = TaconiteFactory.getEnvironmentImplementation();
            if(environment.useCustomProjectName) {
                if(environment.newProjectNameFunc) {
                    return environment.newProjectNameFunc();
                }
                var d:Date = new Date();
                return d.fullYear + '.' + pad(d.month+1,2) + '.' + pad(d.date,2) + '.' + pad(d.hours,2) + '.' + pad(d.minutes,2);
            }
			// first one is #1
			++_numberOfUntitledProjects;

			// first one is "Untitled", second one is "Untitled 2"
			var nextName:String = "Untitled Project";
			if (_numberOfUntitledProjects > 1)
				nextName += " " + _numberOfUntitledProjects;

			return nextName;
		}

        private static function pad(num:Number, digits:Number):String {
            var result:String = num.toString();
            while(result.length < digits) {
                result = '0' + result;
            }
            return result;
        }

		[Transient]
		/**
		 * the window for the desktop app.  This is null in the web version. 
		 * @return 
		 * 
		 */
		public function get window():ITaconiteWindow
		{
			return _window;
		}
		
		public function set window(value:ITaconiteWindow):void
		{
			_window = value;

			this.width = _window.width;
			this.height = _window.height;
		}

		[Transient]
		public function get document():Document
		{
			return _document;
		}

		public function set document(value:Document):void
		{
			try {
				if (_document)
					ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_CLOSING));
	
				_document = value;
				
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_NAME_CHANGED));
				ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.PROJECT_CHANGING));
			}
			catch (error:Error) {
				if (TestPanelController.testRunning)
					TestPanelController.instance.logException(error.toString());
				else
					throw(error);
			}
		}
		
		[Transient]
		public function get childDefinitions():Vector.<ObjectDefinition>
		{
			return Objects.DEFINITION;
		}

		private static var _environment:IEnvironment;

		private static function get environment():IEnvironment
		{
			if (!_environment)
				_environment = TaconiteFactory.getEnvironmentImplementation();
			return _environment;
		}

        public static function get uiRunning():Boolean {
            return instance._document.running || instance.quaziPauseModo;
        }

        public static function get running():Boolean {
            return instance._document.running || !environment.invokedAsEditor;
        }

        public static function get locked():Boolean {
            return instance.document.locked || !environment.invokedAsEditor;
        }

		public static function get runningLocked():Boolean
		{
			return locked && running;
		}

        [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
        [Transient]
        public function get flowVisible():Boolean
        {
            if (world) {
                var flow:EventFlow = world.eventFlow;
                return (flow && flow.flowView) ? flow.flowView.visible : false;
            } else {
                return false;
            }
        }

        public function set flowVisible(showFlow:Boolean):void
        {
            if(showFlow && document.project.accountTypeId < 3) {
                showFlow = false; // Must be pro or higher to show the flow
            }
            // set Event Flow visibility, not closing gadgets or affecting run/pause state
            var flow:EventFlow = world.eventFlow;
            var old:Boolean = flow.flowVisible;
            if(flow.flowView != null) {
                flow.flowView.visible = showFlow;
            }
            flow.backstageView.visible = showFlow;
            flow.eventPagerView.visible = !showFlow;
            flow.flowVisible = showFlow;  // for saving
            ApplicationController.instance.wireController.showWiresForLevel();

            if (showFlow) {
                this.currentTool = ToolboxController.ARROW;
                if(this.variableLibraryVisible){
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_VARIABLE_LIBRARY));
                }
                if(this.ribbonLibraryVisible){
                    ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.TOGGLE_RIBBON_LIBRARY));
                }
            }
            ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesEditor.UPDATE_NOTE_VISIBILITY));
        }

        private function get world():World
        {
            return document ? document.root.value as World : null;
        }

        [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
        [Transient]
        public function get disableLoadUnload():Boolean
        {
            var flow:EventFlow = world.eventFlow;
            return flow ? flow.disableLoadUnload : false;
        }

        public function set disableLoadUnload(value:Boolean):void
        {
            // set Event Flow visibility, not closing gadgets or affecting run/pause state
            var flow:EventFlow = world.eventFlow;
            flow.disableLoadUnload = value;
        }

        [Bindable]
        [Transient]
        public function get lmsEnabled():Boolean {
            return ApplicationController.instance.authorController.getLmsEnabled();
        }

        public function set lmsEnabled(value:Boolean):void {
            ApplicationController.instance.authorController.setLmsEnabled(_lmsEnabled);
        }

        private var _validationEnabled:Boolean;

        [Bindable]
        [Transient]
        public function get validationEnabled():Boolean {
            return _validationEnabled;
        }

        public function set validationEnabled(value:Boolean):void {
            _validationEnabled = value;
            world.eventFlow.handleValidationChange();
        }

        private var _showEventGadgetsInLibrary:Boolean;

        [Bindable]
        [Transient]
        public function get showEventGadgetsInLibrary():Boolean {
            return _showEventGadgetsInLibrary;
        }

        public function set showEventGadgetsInLibrary(value:Boolean):void {
            _showEventGadgetsInLibrary = value;
            LibraryController.instance.updateForEventGadgetToggle();
        }

        public function get publishingInProgress():Boolean {
            return _publishingInProgress;
        }

        public function set publishingInProgress(value:Boolean):void {
            _publishingInProgress = value;
        }
    }
}
