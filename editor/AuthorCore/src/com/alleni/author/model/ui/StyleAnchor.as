package com.alleni.author.model.ui {
	
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.text.TextSelection;
	import com.alleni.author.model.objects.TextObject;
	import com.alleni.taconite.model.ITaconiteObject;
	
	import flash.utils.Dictionary;
	
	public class StyleAnchor extends WireAnchor {
		
		private var _textSelectionObject:TextSelection;
		private var _textObject:TextObject;
		private var _styleName:String;
		
		public function StyleAnchor(textObject:TextObject,textSelectionObject:TextSelection, oldUid:String = null, useNewUid:Boolean=false)
		{
			_textObject = textObject;
			_textSelectionObject = textSelectionObject;
	
			if (oldUid && !useNewUid)
				this.uid = oldUid;
			
			var id:String = oldUid?oldUid:this.uid;
			modifierDescription = new PropertyDescription("styleName", id, "Font- style","Style name to be applied to an element", Modifiers.FONT, 10, false, -1,Modifiers.STRING_TYPE);
		}
		
		override public function set value (val:Object):void
		{
			super.value = val;
			_textSelectionObject.styleName = val as String;
			_styleName = val as String;
		}
		
		override public function set messageCenterVisible(value:Boolean):void {
			super.messageCenterVisible = value;
			
			if (value == true){
				
				if (!_textObject.anchors["style:"+_styleName] && _styleName != null){
					var anchor:StyleWireAnchor = new StyleWireAnchor(_textObject,_styleName);
					_textObject.addAnchor(anchor, true);
				}
			}
				
			else{
				_textObject.removeAnchor(_textObject.anchors["style:" + _styleName]);
			}
			
		}
		
			
		}
		
		
}