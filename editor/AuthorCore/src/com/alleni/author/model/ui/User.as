package com.alleni.author.model.ui
{
	
	[Bindable]
	public class User
	{
		public var dateCreated:Date;
		public var description:String;
		public var username:String;
		public var id:String;
		public var email:String;
		public var emailShow:Boolean;
		public var enabled:Boolean;
		public var isAdmin:Boolean;
		public var lastUpdated:Date;
		public var userRealName:String;		
	}
}