package com.alleni.author.model.ui
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.objects.TextObject;
import com.alleni.taconite.model.TaconiteObject;

import mx.events.PropertyChangeEvent;
	
	public class VariableWireAnchor extends WireAnchor
	{
		public function VariableWireAnchor(container:VariableContainer, variable:String, value:String = "", oldUid:String = null, useNewUid:Boolean=false)
		{
			host = container as TaconiteObject;
			if (oldUid && !useNewUid)
				this.uid = oldUid;
	
			var id:String = oldUid?oldUid:this.uid;
			var prop:PropertyDescription = new PropertyDescription("var;"+variable+";"+id, variable, "Var- {" + variable+"}", "Value of embedded variable {" + variable + "}. \nUse {name} to create additional variables.\nUse {expression} to include calculated values.", Modifiers.VALUE, 1000, false, -1, Modifiers.STRING_TYPE);
			modifierDescription = prop;
			hostProperty = variable;
			VariableContainer(host).getStore().setVariableValue(variable,  value);
			messageCenterVisible = variable != "x";
		}
	
		override public function get value():Object
		{
			return VariableContainer(hostObject).getStore().getVariableValue(hostProperty);
		}
	
		override public function set value(val:Object):void
		{
		    var oldValue:Object = value;
            VariableContainer(host).getStore().setVariableValue(hostProperty,  val);
            valueChanged(oldValue);
		}

        override public function valueChanged(oldValue:Object = null):void {
			super.valueChanged(oldValue);
			VariableContainer(hostObject).invalidate();
            trigger();
        }
	}
}
