package com.alleni.author.model.ui
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;

	import com.alleni.author.model.AbstractObject;

	import mx.events.PropertyChangeEvent;
	
	public class DrawingAnchor extends WireAnchor
	{
		private var _selectedObjects:Array;

		public function DrawingAnchor(selectedObjects:Array)
		{
			super();
			_selectedObjects = selectedObjects;
		}

		public function set selectedObjects(selection:Array):void
		{
			_selectedObjects = selection;
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "value", null, value));
		}

		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		override public function get value ():Object
		{
			if(modifierDescription.readable == false || modifierDescription.indexed) {
				return null;
			}
			// in the future do something intesting based on if the values
			// are already identical or not, for now this will return the first one
			for each(var host:Object in _selectedObjects) {
				if(host.hasOwnProperty(hostProperty)) {
					return host[hostProperty];
				}
				if(host.hasOwnProperty("preset")) {
					return null;
				}
			}
			return null;
		}

		override public function set value (val:Object):void
		{
			var oldValue:Object = value;
			if(modifierDescription.writeable == false || modifierDescription.indexed) {
				return;
			}
			var type:String = (modifierDescription as PropertyDescription).type;
			var result:* = Modifiers.convertValue(type, val);
			if (result != null) {
				for each(var host:Object in _selectedObjects) {
					if(host.hasOwnProperty(hostProperty) || host.hasOwnProperty("preset")) {
						host[hostProperty] = result;
					}
				}
			}
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "value", oldValue, value));
		}

		override public function ensureAttached(obj:AbstractObject):void {
			return;
		}
	}
}