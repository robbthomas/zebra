package com.alleni.author.model.ui
{
	import com.alleni.taconite.model.ITaconiteCollection;
	import com.alleni.taconite.model.TaconiteObject;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;

	public class Assets extends TaconiteObject implements ITaconiteCollection
	{
		[Transient]
		public function get children():IList
		{
			return assets;
		}

		[Transient]
		[Bindable]
		public var assets:IList = new ArrayCollection();  /* of external file assets */
		
		public function getById(assetId:String):Asset
		{
			for each (var testAsset:Asset in assets) 
				if (testAsset.assetID == assetId)
					return testAsset;
			return null;
		}
	}
}