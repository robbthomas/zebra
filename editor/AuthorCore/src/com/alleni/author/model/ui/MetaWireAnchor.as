package com.alleni.author.model.ui
{
	import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.model.AbstractObject;
import com.alleni.taconite.model.TaconiteObject;
	
	import mx.events.PropertyChangeEvent;
	
	public class MetaWireAnchor extends WireAnchor 
	{
		public var parent:WireAnchor
		public var property:String;
		
		public function MetaWireAnchor(parent:WireAnchor, property:String)
		{
			super();
			this.parent = parent;
			this.property = property;
			parent.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleParentPropChange);
		}

		private function handleParentPropChange(event:PropertyChangeEvent):void {
			if(event.property == property) {
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "value", event.oldValue, event.newValue));
				trigger();
			}
		}
	
		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		override public function get value():Object
		{
			return parent[property];
		}
	
		override public function set value(val:Object):void
		{
			super.value = val;
			ApplicationController.instance.wireController.anchorTriggered(this);
		}
			
		override public function get host():TaconiteObject
		{
			return parent;
		}

		override public function get hostObject():AbstractObject {
			return parent.hostObject;
		}

		override public function ensureAttached(obj:AbstractObject):void {
			return;
		}
	}
}