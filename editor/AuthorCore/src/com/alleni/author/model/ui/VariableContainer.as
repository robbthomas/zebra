/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 4/19/11
 * Time: 12:40 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.ui {
import com.alleni.author.definition.VariableStore;

public interface VariableContainer {
	function addVariableAnchor(anchor:VariableWireAnchor):void;
	function removeVariableAnchor(anchor:VariableWireAnchor):void;
    function getVariableAnchor(name:String):VariableWireAnchor;
    function getStore():VariableStore;
    function invalidate():void;
    function variableChanged(name:String):void;
}
}
