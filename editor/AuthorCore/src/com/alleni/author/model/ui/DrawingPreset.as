package com.alleni.author.model.ui
{
	public class DrawingPreset
	{
		public var label:String;
		public var lineColor:uint;
		public var lineThickness:uint;
		public var lineAlpha:uint;
		public var fillColor:Vector.<uint>;
		public var fillAlpha:uint;
		public var fillGradient:Vector.<uint>;
		public var linearGradient:Boolean;
		public var shineGradient:Vector.<uint>;
		
		public function DrawingPreset(label:String, 
									  lineColor:uint, 
									  lineThickness:uint, 
									  lineAlpha:uint, 
									  fillColor:Vector.<uint> = null, 
									  fillAlpha:uint=0, 
									  fillGradient:Vector.<uint> = null, 
									  linearGradient:Boolean = true, 
									  shineGradient:Vector.<uint> = null )
		{
			this.label = label;
			this.lineColor = lineColor;
			this.lineThickness = lineThickness;
			this.lineAlpha = lineAlpha;
			this.fillColor = fillColor;
			this.fillAlpha = fillAlpha;
			this.fillGradient = fillGradient;
			this.linearGradient = linearGradient;
			this.shineGradient = shineGradient;
		}
	}
}