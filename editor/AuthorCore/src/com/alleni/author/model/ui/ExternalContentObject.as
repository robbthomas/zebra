package com.alleni.author.model.ui
{
	import com.alleni.taconite.model.TaconiteObject;
	
	public class ExternalContentObject extends TaconiteObject
	{
        protected var _content:Object = null;

        [Transient]
        [Bindable]
        public function get content():Object {
            return _content;
        }

        public function set content(value:Object):void {
            _content = value;
        }

        public var publishedName:String;
		private var _name:String			= "";
		
		[Transient]
		[Bindable]
		public function get name():String
		{
			return _name;
		}
		public function set name(value:String):void
		{
			if (_name != null && _name.length > 0)
				this.oldName = _name;
			else this.oldName = value;
			_name = value;
		}
		
		[Transient]
		public var oldName:String			= "";
	}
}