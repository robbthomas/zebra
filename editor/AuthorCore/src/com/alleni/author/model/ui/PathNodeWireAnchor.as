/**
 * ¬© Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.model.ui
{
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.model.objects.PathNodeInfo;
	import com.alleni.author.model.objects.PathNode;
	import com.alleni.author.model.objects.PathObject;
	
	import flash.geom.Point;
	
	import mx.events.PropertyChangeEvent;

	public class PathNodeWireAnchor extends WireAnchor
	{
		public function PathNodeWireAnchor(path:PathObject, link:PathNodeInfo)
		{
			hostObject = path;
			
			hostProperty = link.key;
			var weight:int = link.serial;
			var prop:PropertyDescription = new PropertyDescription(hostProperty, "", link.label, link.label, "PATH POINTS",weight, false, -1, Modifiers.POSITION_TYPE);
			modifierDescription = prop;
			messageCenterVisible = true;
		}
		
		override public function get value():Object
		{
			var path:PathObject = PathObject(hostObject);
			return path.getNodePosition(hostProperty);
		}
		
		override public function set value(val:Object):void
		{
			if (val is PositionAndAngle) {
				var path:PathObject = PathObject(hostObject);
				var oldValue:Object = path.getNodePosition(hostProperty);
				path.setNodePosition(hostProperty, val as PositionAndAngle);
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "value", oldValue, val));
			}
		}
		
		public function get pathNodeInfo():PathNodeInfo
		{
			return new PathNodeInfo(modifierDescription.weight, hostProperty, modifierDescription.label);
		}
	}
}