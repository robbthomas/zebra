package com.alleni.author.model.ui
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.OutletDescription;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.action.CustomRibbonAction;
import com.alleni.taconite.factory.SerializerImplementation;

import flash.utils.Dictionary;

    import mx.events.PropertyChangeEvent;
	
	public class TriggerWireAnchor extends HelicopterWireAnchor implements IDependentAnchor
	{
		private var _anchor:WireAnchor;
		private var _condition:Object;
		private var _satisfied:Boolean;
	
		private var _triggerContinuously:Boolean = false;
	
		private function getContinuous(prop:PropertyDescription):Boolean
		{
			switch(prop.key) {
				case "droppedOn":
					return true;
				default:
					return false;
			}
		}
	
		private function getConditionType(valueType:String):String
		{
	        if(Modifiers.isNumericType(valueType)) {
	            return Modifiers.STRING_TYPE;
	        } else if(Modifiers.LIST_TYPE == valueType) {
	            return Modifiers.OBJECT_TYPE;
	        } else {
	            return valueType;
	        }
		}
	
		private function getConditionConstraints(valueType:String, valueConstraints:Object):Object
		{
	        if(Modifiers.isNumericType(valueType)) {
	            return null;
	        } else if(Modifiers.LIST_TYPE == valueType) {
	            return null;
	        } else {
	            return valueConstraints;
	        }
		}
	
		public function TriggerWireAnchor(anchor:WireAnchor, display:Boolean,  oldUid:String = null, useNewUid:Boolean=false)
		{
			if (oldUid && !useNewUid)
				this.uid = oldUid;
	
			var id:String = oldUid?oldUid:this.uid;
	
			var category:String;
			var label:String;
			var tooltip:String;
			var conditionLabel:String;
			var conditionTooltip:String;
			var onFalseLabel:String;
			var onFalseTooltip:String;
			if(anchor.hostProperty == "collidingWith") {
				category = Modifiers.COLLIDE;
				label = "Collided with:";
				tooltip = "Object is colliding (overlapping) with target\n(click value to define target)";
				conditionLabel = "Collision target";
				conditionTooltip = "Define collision target";
				onFalseLabel = "Collision false";
				onFalseTooltip = "Object has stopped colliding with target\n(it no longer overlaps the target)";
			} else if(anchor.hostProperty == "droppedOn") {
				category = Modifiers.DRAG_N_DROP;
				label = "Dropped on:";
				tooltip = "Object is dropped on target\n(click value to define target)";
				conditionLabel = "Drop target";
				conditionTooltip = "Define drop target";
				onFalseLabel = "Missed";
				onFalseTooltip = "Object is not dropped on target";
			} else {
				category = Modifiers.TRIGGERS;
                label = anchor.label;
				tooltip = "Fire when this property matches this condition";
				conditionLabel = "Condition";
				conditionTooltip = "Condition";
				onFalseLabel = "Condition false";
				onFalseTooltip = "Condition is false";
			}
	
			_anchor = anchor;
	        var newWeight:Number = 1000;
	        for each(var other:Object in _anchor.hostObject.anchors) {
                var trigger:TriggerWireAnchor = other as TriggerWireAnchor;
				if(trigger){
	                if(_anchor.hostProperty == trigger.prototypeProperty){
	                    if(newWeight <= other.modifierDescription.weight){
	                        newWeight = other.modifierDescription.weight + 1;
	                    }
	                }
	            }
			}
	
			modifierDescription = new OutletDescription("trigger;"+_anchor.modifierDescription.key+";"+id, "", label, tooltip ,category, newWeight);
			hostObject = _anchor.hostObject;
	
			var prop:PropertyDescription = PropertyDescription(_anchor.modifierDescription);
	
			_triggerContinuously = getContinuous(prop);
	
			_condition = Modifiers.convertValue(getConditionType(prop.type), _anchor.value);
	
			var child:MetaWireAnchor = new MetaWireAnchor(this, "condition");
			child.modifierDescription = new PropertyDescription("condition", "", conditionLabel, conditionTooltip, category, 1, false, -1, getConditionType(prop.type), getConditionConstraints(prop.type, prop.constraints));
			child.hostProperty = "condition";
			childAnchors.push(child);
	
			var falseChild:WireAnchor = new WireAnchor();
			falseChild.modifierDescription = new OutletDescription("onFalse", "", onFalseLabel, onFalseTooltip, category, 2);
			falseChild.host = this;
			childAnchors.push(falseChild);
	
			if(_anchor.hostProperty == "collidingWith" && _condition != null) {
				_anchor.hostObject.updateCollidingPossibilities();
			} else if(_anchor.hostProperty == "droppedOn" && _condition != null) {
				_anchor.hostObject.updateDroppedPossibilities();
			}

			_satisfied = Modifiers.checkCondition(_anchor.value, _condition,  _anchor.isNumericProperty(), _anchor.hostObject);
	
			_anchor.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handleAnchorChange);
	
			if(display) {
				this.messageCenterVisible = true;
			}
            this.initialValues = getCurrentValues();
			hostObject.addAnchor(this, false);
            this.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, propertyChanged);
		}
	
		[Bindable]
		public function get condition():Object
		{
			return _condition;
		}
	
		public function set condition(value:Object):void
		{
			var oldValue:Object = _condition;
			_condition = value;
	
			if(_anchor.hostProperty == "collidingWith") {
				_anchor.hostObject.updateCollidingPossibilities();
				if(oldValue == null) {
					addNullAnchor();
				}
			} else if(_anchor.hostProperty == "droppedOn") {
				_anchor.hostObject.updateDroppedPossibilities();
				if(oldValue == null) {
					addNullAnchor();
				}
			}
	        dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "childValue", oldValue, _condition));
		}
	
		private function addNullAnchor():void
		{
			var found:Boolean = false;
			for each(var other:Object in hostObject.anchors) {
				if(other is TriggerWireAnchor && TriggerWireAnchor(other)._anchor == _anchor && TriggerWireAnchor(other)._condition == null) {
					found = true;
					break;
				}
			}
			if(!found) {
				var otherAnchor:TriggerWireAnchor = new TriggerWireAnchor(_anchor, false);
				ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(otherAnchor, true));
			}
		}
	
		private function handleAnchorChange(event:PropertyChangeEvent):void
		{
			if(event.property != "value") {
				return;
			}
            if(ApplicationController.instance.wireController.bindingsEnabled == false) {
                return;
            }
            if (_condition == null) {
                return;
            }
	
//			trace("checking condition for '"+event.property+"':", event.newValue, _anchor.value);
	
			var satisfiedNow:Boolean = Modifiers.checkCondition(event.newValue, _condition,  _anchor.isNumericProperty(), _anchor.hostObject);
//			trace("satisfied " + satisfiedNow);
			if(satisfiedNow == true && (_triggerContinuously || _satisfied == false)) {
				trigger();
			} else if(satisfiedNow == false && (_triggerContinuously || _satisfied == true)) {
				childAnchors[1].trigger();
			}
			_satisfied = satisfiedNow;
		}

        [Transient]
        public function get prototypeAnchor():WireAnchor
        {
            return _anchor;
        }

        [Transient]
        public function get prototypeProperty():String
        {
            return triggerProperty;
        }

		public function get triggerProperty():String
		{
			return _anchor.modifierDescription.key;
		}

        override public function getCurrentValues(objectIds:Dictionary = null):Object {
            var result:Object = {
                condition:condition
            };
            SerializerImplementation.serializeValue(result,  "condition", objectIds);
            return result;
        }

        override public function setCurrentValues(value:Object, objectPropStorage:Object=null):void {
            condition = SerializerImplementation.deserializeValue(value.condition, null, objectPropStorage, this, "condition");;
        }

        override public function restoreFromInitialValues():void {
            super.restoreFromInitialValues();
            _satisfied = Modifiers.checkCondition(_anchor.value, _condition,  _anchor.isNumericProperty(), _anchor.hostObject);
        }


        public function propertyChanged(event:PropertyChangeEvent):void {
            if(Application.running || !ApplicationController.instance.wireController.bindingsEnabled || hostObject.loading) {
                return;
            }
            switch(event.property) {
                case "condition":
                    initialValues[event.property] = event.newValue;
                    SerializerImplementation.serializeValue(initialValues, "condition");
                    if ((event.oldValue as Asset) || (event.newValue as Asset)) {
                        updateForAssetChange();
                    }
                    break;
            }
        }

        override public function getUsageCounts(usages:UsageCounts):void
        {
            if ("condition" in initialValues) {
                usages.countAssetJson(initialValues.condition);
            }
        }

        override public function get assetID():String
        {
            if ("condition" in initialValues) {
                return Asset.idFromJson(initialValues.condition); // id or null
            }
            return null;
        }

        override public function get hasAssetUsage():Boolean
        {
            if ("condition" in initialValues) {
                var id:String = Asset.idFromJson(initialValues.condition);
                return (id != null);
            }
            return false;
        }

	}
}
