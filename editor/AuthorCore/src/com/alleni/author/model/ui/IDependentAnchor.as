/**
 * Created by IntelliJ IDEA.
 * User: steve
 * Date: 1/22/13
 * Time: 2:03 PM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.ui {
public interface IDependentAnchor {
    function get prototypeAnchor():WireAnchor;
    function get prototypeProperty():String;
}
}
