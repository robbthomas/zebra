package com.alleni.author.model.ui {
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.PropertyDescription;

import com.alleni.author.definition.UserKeyEvent;

import mx.events.PropertyChangeEvent;

public class KeyHandlerWireAnchor extends WireAnchor {

	private var _anchor:WireAnchor;
	private var _key:String;

	private var _continuous:Boolean;

	[Bindable]
	public function get continuous():Boolean {
		return _continuous;
	}

	public function set continuous(value:Boolean):void {
		_continuous = value;
	}

	[Bindable]
	public function get key():String {
		return _key;
	}

	public function set key(value:String):void {
		_key = value;
		if(key.length == 1) {
			ApplicationController.addEventListener(UserKeyEvent.TYPE, keyPressHandler, false, 0, true);
		} else {
			ApplicationController.removeEventListener(UserKeyEvent.TYPE, keyPressHandler);
		}
	}

	[Bindable]
	public var pressed:Boolean;

	public function KeyHandlerWireAnchor(anchor:WireAnchor, oldUid:String = null, useNewUid:Boolean=false, key:String="") {

		if (oldUid && !useNewUid)
			this.uid = oldUid;

		var id:String = oldUid?oldUid:this.uid;

		_key = key;

		_anchor = anchor;
		modifierDescription = new PropertyDescription("key;"+_anchor.modifierDescription.key+";"+id, "", _anchor.modifierDescription.label + " Key", "Call this inlet on key press",Modifiers.SETTERS,0, false, -1, Modifiers.STRING_TYPE);
		hostObject = _anchor.hostObject;

		var continuousAnchor:MetaWireAnchor = new MetaWireAnchor(this,"continuous");
		continuousAnchor.modifierDescription = new PropertyDescription("continuous", "", "Continuous", "Call this inlet repeatedly while key is held down", Modifiers.SETTERS, 1, false, -1, Modifiers.BOOLEAN_TYPE);
		continuousAnchor.hostProperty = "continuous";
		childAnchors.push(continuousAnchor);

		var pressedAnchor:MetaWireAnchor = new MetaWireAnchor(this,"pressed");
		pressedAnchor.modifierDescription = new PropertyDescription("pressed", "", "pressed", "Call this inlet repeatedly while key is held down", Modifiers.SETTERS, 1, false, -1, Modifiers.BOOLEAN_TYPE, null, false);
		pressedAnchor.hostProperty = "pressed";
		childAnchors.push(pressedAnchor);

		hostObject.addAnchor(this, true);
		if(key.length == 1) {
			ApplicationController.addEventListener(UserKeyEvent.TYPE, keyPressHandler, false, 0, true);
		}
	}

	private function keyPressHandler(event:UserKeyEvent):void {
		if(key == event.key) {
			if(event.down) {
				if(pressed) {
					if(_continuous) {
						_anchor.handleTrigger();
					}
				} else {
					pressed = true;
					_anchor.handleTrigger();
				}
			} else {
				pressed = false;
			}
		}
	}

	[Transient]
	[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
	override public function get value():Object {
		return key;
	}

	override public function set value(val:Object):void {
		key = Modifiers.convertValue(Modifiers.STRING_TYPE, val) as String;
		valueChanged();
	}
}
}
