package com.alleni.author.model.ui
{
	import com.alleni.taconite.model.ITaconiteCollection;
	import com.alleni.taconite.model.TaconiteObject;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;
    
    /**
     * Value object representing wiring between objects for the purpose of interactive behavior
     */
	public class Wiring extends TaconiteObject implements ITaconiteCollection
	{
		[Transient]
		public static const TACONITE_CHILDREN_PROPERTY:String = "wires";
		
		[Transient]
		public function get children():IList
		{
			return wires;
		}
		
		[Transient]
        [Bindable]
        public var wires:IList = new ArrayCollection();  /* wires between objects */
        
        [Bindable]
        public var wiringLevel:int = 3; // The wiring level, such as "show all", "show all for selected object", etc. (0-4)

		public function findWireByUID(uid:String):Wire
		{
			var wire:Wire;
			for each (wire in wires) {
				if (wire.uid == uid) {
					return wire;
				}
			}
			return null;
		}
	}
}