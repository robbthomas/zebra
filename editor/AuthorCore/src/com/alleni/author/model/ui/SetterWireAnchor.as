package com.alleni.author.model.ui
{
import caurina.transitions.Tweener;

import com.alleni.author.Navigation.Pager;
import com.alleni.author.controller.AuthorController;
import com.alleni.author.controller.TweenController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.definition.InletDescription;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.action.CustomRibbonAction;
import com.alleni.author.definition.application.TransitionTypes;
import com.alleni.author.model.AbstractObject;
import com.alleni.author.model.objects.Arena;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.objects.ProjectObject;
import com.alleni.author.model.project.Project;
import com.alleni.author.view.ObjectView;
import com.alleni.savana.Ast;
import com.alleni.taconite.factory.SerializerImplementation;
import com.alleni.taconite.service.LogService;

import flash.utils.Dictionary;

import mx.events.PropertyChangeEvent;

public class SetterWireAnchor extends HelicopterWireAnchor implements IDependentAnchor {
		private var _anchor:WireAnchor;
		private var _inValueAnchor:MetaWireAnchor;
		
		private var _inValue:Object;
		private var _tweenType:String = "linear";
		private var _tweenTime:Number = 0;
		private var _tweenDelay:Number = 0;

        [Bindable]
  		public var objectTransition:String = "none";
  		[Bindable]
  		public var objectTransTweenType:String = "linear";
  		[Bindable]
  		public var objectTransitionSecs:Number = 0;

        [Bindable]
        public var eventTransition:String = TransitionTypes.DEFAULT;  // for project MC "Go-event" and pageNumber setters

        [Bindable]
        public var transition:String = TransitionTypes.NONE;   // Arena pageNumber setters
  		[Bindable]
  		public var transTweenType:String = "linear";
  		[Bindable]
  		public var transitionSecs:Number = 0;

		public var customModifier:InletDescription = null;

        [Transient]
        private var _currentObjectTransition:String;

	private function getValueType(valueType:String):String {
        if(Modifiers.isNumericType(valueType)) {
            return Modifiers.STRING_TYPE;
        } else {
            return valueType;
        }
	}

	private function getValueConstraints(valueType:String, valueConstraints:Object):Object {
        if(Modifiers.isNumericType(valueType)) {
            return null;
        } else {
            return valueConstraints;
        }
	}
				
		public function SetterWireAnchor(anchor:WireAnchor, display:Boolean,  oldUid:String = null, useNewUid:Boolean=false, inValue:Object=null,
		                                 customModifier:String = null)
		{
			super();
			if (oldUid && !useNewUid)
				this.uid = oldUid;

			var id:String = oldUid?oldUid:this.uid;

			_anchor = anchor;

			var label:String;
			var description:String;
			var category:String;
			var weight:int;
			if(customModifier != null) {
				this.customModifier = Modifiers.instance.fetch(customModifier) as InletDescription;
            }
            if(this.customModifier != null) {
				label = this.customModifier.label;
				description = this.customModifier.description;
				category = this.customModifier.category;
				weight = this.customModifier.weight;
			} else if(anchor.hostProperty == "currentPage") {
                if(anchor.hostObject is Project || anchor.hostObject is EventFlow) {
                    label = 'Go- page';
                    description = 'Navigate to the listed page';
                    category = Modifiers.DO;
                } else {
                    label = 'Go- event';
                    description = 'Go to an event \n(to define, click value, then click an event in list)';
                    category = Modifiers.DO;
                }
                weight = -1;
            } else {
                if(anchor.labelChanged){
                    label = _anchor.label;
                    labelChanged = true;
                } else {
                    label = _anchor.modifierDescription.label;
                }
				description = "Set this property to a new value";
				category = Modifiers.SETTERS;
				weight = -1;
			}

			if(inValue != null || anchor.hostProperty == 'currentPage'/*allow null created current page setters*/) {
				_inValue = inValue;
			} else {
				_inValue = anchor.value;
			}

			modifierDescription = new InletDescription("set;"+_anchor.modifierDescription.key+";"+id, "", label, description, category, weight);
			hostObject = _anchor.hostObject;
			
			createAnchors();
			if(display) {
				this.messageCenterVisible = true;
			}
            this.initialValues = getCurrentValues();
			hostObject.addAnchor(this, false);
            this.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, propertyChanged);
		}
		
		private function createAnchors():void
		{
			// Create inValue anchor
			if(customModifier == null) {
				_inValueAnchor = new MetaWireAnchor(this,"inValue");
				var md:PropertyDescription = _anchor.modifierDescription as PropertyDescription;
				if (md) {
                    if (_anchor.hostProperty == "currentPage" ) {
                        md = new PropertyDescription("currentPage", "", "Event name", "Define event name\n(to define, click value, then click an event in list)", Modifiers.VALUE, 4, false, -1, Modifiers.EVENTPAGE_TYPE);
                    }
                    _inValueAnchor.modifierDescription = new PropertyDescription("inValue",md.id,labelChanged?label:md.label,md.description,md.category,0,md.indexed,md.autoAddUpTo,getValueType(md.type),getValueConstraints(md.type,  md.constraints),md.writeable);
                }
				_inValueAnchor.hostProperty = "inValue";
				childAnchors.push(_inValueAnchor);
			}
			
			// Create tween anchors
			if(_anchor.modifierDescription is PropertyDescription) {
				var keyPressModifier:PropertyDescription = Modifiers.instance.fetch("keyPress") as PropertyDescription;
                createTweenAnchor("keyPress", keyPressModifier);
                if(TweenController.cannotTween((_anchor.modifierDescription as PropertyDescription).type) && _anchor.hostProperty != "visible" && _anchor.hostProperty != "currentPage") {
					var delayModifier:PropertyDescription = Modifiers.instance.fetch("tweenDelay") as PropertyDescription;
					createTweenAnchor("tweenDelay", delayModifier);
                    _tweenType = "linear";

				} else if(_anchor.hostProperty == "visible") {
					var objectTransitionModifier:PropertyDescription = Modifiers.instance.fetch("objectTransition") as PropertyDescription;
					var objectTransTweenTypeModifier:PropertyDescription = Modifiers.instance.fetch("objectTransTweenType") as PropertyDescription;
					var objectTransitionSecsModifier:PropertyDescription = Modifiers.instance.fetch("objectTransitionSecs") as PropertyDescription;

					createTweenAnchor("objectTransition", objectTransitionModifier);
					createTweenAnchor("objectTransTweenType", objectTransTweenTypeModifier);
					createTweenAnchor("objectTransitionSecs", objectTransitionSecsModifier);

				} else if(_anchor.hostProperty == "currentPage" || _anchor.hostProperty == "pageNumber") {   // ProjectObject.currentPage is a pointer, pageNumber is on Arena or ProjectObject
                    var transitionProp:String = (hostObject is ProjectObject) ? "eventTransition" : "transition";  // ProjectObject or Arena
                    var transitionModifier:PropertyDescription = Modifiers.instance.fetch(transitionProp) as PropertyDescription;
                    var transTweenTypeModifier:PropertyDescription = Modifiers.instance.fetch("transTweenType") as PropertyDescription;
                    var transitionSecsModifier:PropertyDescription = Modifiers.instance.fetch("transitionSecs") as PropertyDescription;

                    createTweenAnchor(transitionProp, transitionModifier);
                    createTweenAnchor("transTweenType", transTweenTypeModifier);
                    createTweenAnchor("transitionSecs", transitionSecsModifier);
                } else {

					var tweenTypeModifier:PropertyDescription = Modifiers.instance.fetch("tweenType") as PropertyDescription;
					var tweenTimeModifier:PropertyDescription = Modifiers.instance.fetch("tweenTime") as PropertyDescription;
					var tweenDelayModifier:PropertyDescription = Modifiers.instance.fetch("tweenDelay") as PropertyDescription;

					createTweenAnchor("tweenType", tweenTypeModifier);
					createTweenAnchor("tweenTime", tweenTimeModifier);
					createTweenAnchor("tweenDelay", tweenDelayModifier);

				}
			}
		}
		
		private function createTweenAnchor(property:String, propertyDescription:PropertyDescription):void
		{
			var anchor:MetaWireAnchor = new MetaWireAnchor(this, property);
			anchor.modifierDescription = propertyDescription;
			anchor.hostProperty = property;

			childAnchors.push(anchor);
		}
		
        [Transient]
        public function get prototypeAnchor():WireAnchor
        {
            return _anchor;
        }

        [Transient]
        public function get prototypeProperty():String
        {
            return setterProperty;
        }

		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get inValue():Object
		{
			return _inValue;
		}
	
		public function set inValue(val:Object):void 
		{
			var oldValue:Object = _inValue;
			_inValue = val;
            if(_anchor.hostProperty == "currentPage") {
                if(oldValue == null) {
                    addNullAnchor();
                }
            }
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "inValue", oldValue, val));
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "childValue", oldValue, val));
		}
		
		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get tweenType():String
		{
			return _tweenType;
		}
		
		public function set tweenType(val:String):void 
		{
			var oldValue:Object = _tweenType;
			_tweenType = val;
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "tweenType", oldValue, val));
		}
		
		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get tweenTime():Number
		{
			return _tweenTime;
		}
		
		public function set tweenTime(val:Number):void 
		{
			var oldValue:Object = _tweenTime;
			_tweenTime = val;
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "tweenTime", oldValue, val));
		}
		
		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get tweenDelay():Number
		{
			return _tweenDelay;
		}
		
		public function set tweenDelay(val:Number):void 
		{
			var oldValue:Object = _tweenDelay;
			_tweenDelay = val;
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "tweenDelay", oldValue, val));
		}

		public function get newValue():Object {
			if(_anchor.isNumericProperty()) {
				var env:Object = Ast.defaultEnvironment();
				env["#"] = _anchor.value;
				try {
					var result:* = new Ast(String(_inValue)).evaluate(env);
					if(result is Number) {
						return result
					} else if(result is Boolean) {
						return result ? 1 : 0;
					} else {
						return 0;
					}
				} catch(e:Error) {
					LogService.error("Error evaluating setter ribbon: "  + e.message);
					return 0;
				}
			} else {
				return _inValue;
			}
			return _inValue;
		}
	
		public var frac:Number;

        public var cancelProperties:Object = {
            "locationXYR": ["x", "y", "rotation"],
            "x": ["locationXYR"],
            "y": ["locationXYR"],
            "rotation": ["locationXYR"]
        };

        public function cancelsAnchor(other:SetterWireAnchor):Boolean {
            if(other.setterProperty == this.setterProperty) {
                return true;
            }
            if(this.setterProperty in cancelProperties) {
                return cancelProperties[this.setterProperty].indexOf(other.setterProperty) >= 0;
            }
            return false;
        }
	
		override public function handleTrigger():void 
		{
			for each(var other:Object in _anchor.hostObject.anchors) {
				var anchor:SetterWireAnchor = other as SetterWireAnchor;
				if(anchor && cancelsAnchor(anchor) ) {
					anchor.cancelTweens();
				}
			}

            var type:String;
			var time:Number;
			var start:*;
			var end:*;
            var valueType:String;

			if(_anchor.hostProperty == "visible") {  // Hide and Show inlets
                if(objectTransTweenType == "none" || objectTransTweenType == null || objectTransTweenType == "") {
                    type = "linear";
                    time = 0;
                } else {
                    type = objectTransTweenType;
                    time = objectTransitionSecs;
                }

                _currentObjectTransition = objectTransition;
                var obj:AbstractObject = _anchor.hostObject;
                var view:ObjectView = obj.getView();
                if(view) ApplicationController.instance.requestUpdateView(view);

                // object should not finish above the object set alpha/zoom
                // also if the view is currently visible it should start
                // at the current apparent alpha/scale
                switch(_currentObjectTransition) {
                    case TransitionTypes.FADE_IN:
                        // fade up from current view alpha, or zero if not visible
                        start = (view && view.visible) ? view.alpha : 0.0;
                        if(newValue) {
                            end = obj.alpha*0.01;
                        } else {
                            end = 0.0;
                        }
                        if(view) {
                            view.alpha = start;
                        }
                            trace("View alpha going from " + start + " to " + end);
                        break;
                    case TransitionTypes.ZOOM_IN:
                        if(newValue) {
                            // zoom up from current view scale or 0 if not visible
                            start = (view && view.visible) ? view.scaleX : 0.0;
                            end = obj.scale*0.01;
                        } else {
                            start = view ? view.scaleX : 1;
                            end = 0.0;
                        }
                        if(view) {
                            view.scaleX = start;
                        }
                            trace("View scale going from " + start + " to " + end);
                        break;
                    default:
                        start = 1;
                        end = 1;
                        break;

                }

                valueType = Modifiers.NUMBER_TYPE;

                if(newValue) {
                    if(view) {
                        trace("Starting view visible");
                        view.visible = true; // get the view visible for zooming or fading
                    }
                }

			} else if(_anchor.hostProperty == "currentPage" || _anchor.hostProperty == "pageNumber") {
                var projObj:ProjectObject = _anchor.hostObject as ProjectObject;
                var pager:Pager = (projObj) ? AuthorController.instance.eventFlow : _anchor.hostObject as Arena;
                if (pager) {
                    pager.setterTransition = (projObj) ? eventTransition : transition;
                    pager.setterTransitionSecs = transitionSecs;
                    pager.setterTransTweenType = transTweenType;
                    if(projObj) {
                        switch (_anchor.hostProperty) {
                            case "currentPage":
                                projObj.currentPage = newValue as AbstractObject;
                                break;
                            case "pageNumber":
                                projObj.pageNumber = newValue as int;
                                break;
                        }
                    } else if (_anchor.hostObject is Arena && _anchor.hostProperty == "pageNumber") {
                        pager.pageNumber = newValue as int;
                    }
                    pager.setterTransition = null;
                    pager.setterTransitionSecs = 0;
                    pager.setterTransTweenType = null;
                }
                return;
            } else {
                if(_tweenType == "none" || _tweenType == null || _tweenType == "") {
                    type = "linear";
                    time = 0;
                } else {
                    type = _tweenType;
                    time = _tweenTime;
                }

                start = _anchor.value  // get start value from wired object
                end = newValue;
                if (start == null)
                    start = end;

                valueType = PropertyDescription(_anchor.modifierDescription).type;
            }
	
			var twc:TweenController = new TweenController(start, end, valueType);
			frac = 0;
			
			Tweener.addTween(this, {frac:1.0, time:time, delay:_tweenDelay, transition:type, onUpdate:updateTween, onUpdateParams:[twc], onComplete:tweenComplete});
		}

		private function updateTween(twc:TweenController):void
		{
            var val:* = twc.computeTween(frac);
            if(_anchor.hostProperty == "visible") {
                var view:ObjectView = _anchor.hostObject.getView();
				if(view)
				{
					switch(_currentObjectTransition) {
						case TransitionTypes.FADE_IN:
							trace("Update view alpha to " + val);
							view.visible = true;
							view.alpha = val;
							break;
						case TransitionTypes.ZOOM_IN:
							trace("Update view scale to " + val);
							view.visible = true;
							view.scaleX = view.scaleY = val;
							break;
						default:
							// not a darn thing
							break;
					}
				}
            } else {
			    _anchor.value = val;
            }
		}

		private function tweenComplete():void
		{
            if(_anchor.hostProperty == "visible") {
                var obj:AbstractObject = _anchor.hostObject;
                obj.cloakedNow = false;
                trace("obj visible " + obj.visible);
                trace("Update obj visible to " + newValue);
			    obj.visible = newValue;

                var view:ObjectView = obj.getView();
                if(view) {
                    trace("Update view alpha ["+view.visible+","+view.scaleX+","+view.alpha+"] to ["+obj.visible+","+(obj.scale * 0.01)+","+(obj.alpha * 0.01)+"]");
                    view.visible = obj.visible;
                    view.scaleX = view.scaleY = obj.scale * 0.01;
                    view.alpha = obj.alpha * 0.01;
                    ApplicationController.instance.requestUpdateView(view);
                }
            } else {
			    // nothing to do for now
            }
		}
		
		override public function toString():String
		{
			if(hostObject == null) return "[WireAnchor Ghost]";
			var prop:String = hostProperty ? hostProperty : label;
			return "[SetterWireAnchor property="+prop+" obj="+hostObject+(modifierDescription.indexed?(" idx="+hostPropertyIndex):"")+" wired="+wired+"]";
		}
				
		public function get setterProperty():String
		{
			return _anchor.modifierDescription.key;
		}

        public function pauseTweens():void {
            Tweener.pauseTweens(this);
        }

        public function resumeTweens():void {
            Tweener.resumeTweens(this);
        }

        public function cancelTweens():void {
            Tweener.removeTweens(this);
        }

        override public function getCurrentValues(objectIds:Dictionary = null):Object {
            var result:Object = {
                inValue:inValue,
                tweenType:tweenType,
                tweenTime:tweenTime,
                tweenDelay:tweenDelay,
                transitionSecs:objectTransitionSecs,
                transTweenType:objectTransTweenType,
                objectTransition:objectTransition,
                keyPress:keyPress
            };
            SerializerImplementation.serializeValue(result,  "inValue", objectIds);
            return result;
        }

        override public function setCurrentValues(value:Object, objectPropStorage:Object=null):void {
            inValue = SerializerImplementation.deserializeValue(value.inValue, null, objectPropStorage, this, "inValue");
            tweenType = value.tweenType;
            tweenTime = value.tweenTime;
            tweenDelay = value.tweenDelay;
            objectTransitionSecs = value.transitionSecs;
            objectTransTweenType = value.transTweenType;
            objectTransition = value.objectTransition;
            keyPress = value.keyPress;
        }

        public function propertyChanged(event:PropertyChangeEvent):void {
            if(Application.running || !ApplicationController.instance.wireController.bindingsEnabled || hostObject.loading) {
                return;
            }
            switch(event.property) {
                case "inValue":
                    initialValues[event.property] = event.newValue;
                    SerializerImplementation.serializeValue(initialValues, "inValue");
                    if ((event.oldValue as Asset) || (event.newValue as Asset)) {
                        updateForAssetChange();
                    }
                    break;
                case "objectTransitionSecs":
                    initialValues["transitionSecs"] = event.newValue;
                    break;
                case "objectTransTweenType":
                    initialValues["transTweenType"] = event.newValue;
                    break;
                case "tweenType":
                case "tweenTime":
                case "tweenDelay":
                case "objectTransition":
                case "keyPress":
                    initialValues[event.property] = event.newValue;
                    break;
            }
        }

        private function addNullAnchor():void
  		{
  			var found:Boolean = false;
  			for each(var other:Object in hostObject.anchors) {
  				if(other is SetterWireAnchor && SetterWireAnchor(other)._anchor == _anchor && SetterWireAnchor(other)._inValue == null) {
  					found = true;
  					break;
  				}
  			}
  			if(!found) {
  				var otherAnchor:SetterWireAnchor = new SetterWireAnchor(_anchor, false);
  				ApplicationController.currentActionTree.commit(CustomRibbonAction.fromAnchor(otherAnchor, true));
  			}
  		}

        override public function getUsageCounts(usages:UsageCounts):void
        {
            if ("inValue" in initialValues) {
                usages.countAssetJson(initialValues.inValue);
            }
        }

    override public function get assetID():String
    {
        if ("inValue" in initialValues) {
            return Asset.idFromJson(initialValues.inValue);  // id or null
        }
        return null;
    }

    override public function get hasAssetUsage():Boolean
    {
        if ("inValue" in initialValues) {
            var id:String = Asset.idFromJson(initialValues.inValue);
            return (id != null);
        }
        return false;
    }

    }
}
