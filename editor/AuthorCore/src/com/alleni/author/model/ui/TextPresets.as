package com.alleni.author.model.ui
{
	public class TextPresets
	{
		public var label:String;
		public var fontName:String;
		public var fontSize:Number;
		public var fontBold:Boolean;
		public var fontItalic:Boolean;
		public var fontUnderline:Boolean;
		public var fontColor:uint;
		public var fontOpacity:uint;
		public var alignHorz:String;
		public var alignVert:String;
		public var textMargin:Number;
		
		public function TextPresets(label:String, fontName:String, fontSize:Number, fontColor:uint, alignHorz:String = "", alignVert:String = "", textMargin:Number = 0, fontBold:Boolean = false, fontItalic:Boolean = false, fontUnderline:Boolean = false, fontOpacity:Number = 100)
		{
			this.label = label;
			this.fontName = fontName;
			this.fontSize = fontSize;
			this.fontColor = fontColor;
			this.fontBold = fontBold;
			this.fontItalic = fontItalic;
			this.fontUnderline = fontUnderline;
			this.fontOpacity = fontOpacity;
			this.alignHorz = alignHorz;
			this.alignVert = alignVert;
			this.textMargin = textMargin;
		}
	}
}