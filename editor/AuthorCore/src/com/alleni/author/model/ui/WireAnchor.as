package com.alleni.author.model.ui
{
import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.SmartObject;
	import com.alleni.author.controller.objects.IReplaceableReceiver;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.PropertyDescription;
	import com.alleni.author.definition.UserKeyEvent;
import com.alleni.author.document.Document;
import com.alleni.author.event.AnchorEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.InputText;
	import com.alleni.author.model.objects.ToggleButton;
	import com.alleni.author.view.WorldContainer;
	import com.alleni.author.view.ui.PortView;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.model.TaconiteObject;
	import com.alleni.taconite.view.ViewContext;
	
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import mx.collections.IList;
	import mx.events.PropertyChangeEvent;
	
	public class WireAnchor extends TaconiteObject implements IReplaceableReceiver
	{	
		[Transient]
		[Bindable]
		public var modifierDescription:IModifier;
		
		protected var _host:TaconiteObject;
		
		[Bindable]
		public var hostProperty:String;
		
		[Bindable]
		public var hostPropertyIndex:int;
		
		[Bindable]
		private var _wired:Boolean = false;
		
		[Bindable]
		protected var _label:String;

        public function get label():String{
            if(_label != null){
                return _label;
            } else {
                return modifierDescription.label;
            }
        }

        public function set label(value:String):void{
            _label = value;
        }
		
		public var labelChanged:Boolean;  // true if changed by author
		
		[Bindable]
		public var highlight:Boolean = false;
		
		[Bindable]
		public var childAnchors:Vector.<WireAnchor> = new Vector.<WireAnchor>();

        public var initialValues:Object = {};

        public function getCurrentValues(objectIds:Dictionary = null):Object {
            return {};
        }

        public function setCurrentValues(value:Object, objectPropStorage:Object=null):void {

        }

        public function restoreFromInitialValues():void {
            setCurrentValues(initialValues);
        }

		protected var _keyPress:String = "";

        [Bindable]
		public function get keyPress():String
		{
			return _keyPress;
		}

		public function set keyPress(value:String):void
		{
			_keyPress = value;
			
			if(_keyPress && _keyPress.length > 0) {
				ApplicationController.addEventListener(UserKeyEvent.TYPE, keyPressHandler, false, 0, true);
			} else {
				ApplicationController.removeEventListener(UserKeyEvent.TYPE, keyPressHandler,false);
			}
		}

		public function child(key:String):WireAnchor {
			for each(var anchor:WireAnchor in childAnchors) {
				if(anchor.modifierDescription.key == key) {
					return anchor;
				}
			}
			return null;
		}

		private var _messageCenterVisible:Boolean = false;

		private var _propertyInspectorVisible:Boolean = false;
		
		[Transient]
		private var _views:Dictionary = new Dictionary(true);
		
		public function trigger():void
		{
			ApplicationController.instance.wireController.anchorTriggered(this);
		}
		
		public function handleTrigger():void
		{
			hostObject.controller.handleTrigger(hostObject, modifierDescription);
		}
		
		[Transient]
		[Bindable]
		public function set hostObject(value:AbstractObject):void
		{	
			_host = value;
		}
		
		public function get hostObject():AbstractObject
		{
			if (hostAnchor != null)
				return hostAnchor.hostObject;
			else
				return host as AbstractObject;
		}
		
		private function get hostAnchor():WireAnchor
		{
			return host as WireAnchor;
		}
		
		[Transient]
		[Bindable]
		public function set host(value:TaconiteObject):void
		{
			_host = value;
		}
		
		public function get host():TaconiteObject
		{
			return _host;
		}


		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get value ():Object
		{
			if(hostProperty == null) {
				return null;
			}
            try {
                if(modifierDescription.indexed) {
                    return host[hostProperty][hostPropertyIndex];
                } else {
                    return host[hostProperty];
                }
            } catch(error:Error) {
                // may get an error on readonly
                // try to grab the data anyway (may be useful for logging/undo)
                // if nothing can be grabbed then just return the normal null
            }
            return null;
		}
		
		public function set value (val:Object):void
		{
			if (!host)
				return;
			//			trace("WireAnchor set val="+val+"/"+Utilities.shortClassName(val), "dest="+hostProperty+"/"+Utilities.shortClassName(host[hostProperty]));
			var oldValue:Object = value;
			if(modifierDescription.writeable == false) {
				return;
			}
			var type:String = PropertyDescription(modifierDescription).type;
			var result:* = Modifiers.convertValue(type, val);
			try {
				if(modifierDescription.indexed) {
					host[hostProperty][hostPropertyIndex] = result;
				} else {
					host[hostProperty] = result;
				}
			} catch(error:TypeError) {
				trace("Type error in WireAnchor set value", error);
			}
			valueChanged(oldValue);
		}
		
		public function valueChanged(oldValue:Object = null):void
		{
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "value", oldValue, value));
		}
		
		public function register(view:PortView):void
		{
			_views[view] = true;
		}
		
		public function getViews(context:WorldContainer,alienated:Boolean = false) : Vector.<PortView>
		{
			var result:Vector.<PortView> = new Vector.<PortView>();
			for(var o:Object in _views) {
				var p:PortView = o as PortView;
				if(p && (p.stage || alienated) && p.worldContainer == context) {
					result.push(p);
				}
			}
			return result;
		}
		
		public function get isAdditionAnchor():Boolean
		{
			return modifierDescription is PropertyDescription
				&& modifierDescription.indexed
				&& hostPropertyIndex == IList(hostObject[hostProperty]).length
				&& hostPropertyIndex < modifierDescription.autoAddUpTo;
		}
		

		public function isGhost():Boolean
		{
			return this == ApplicationController.instance.wireController.ghostAnchor;
		}
		
		public function get path():String
		{
			var result:String = modifierDescription.key + (modifierDescription.indexed?("["+hostPropertyIndex+"]"):"");
			if(host is WireAnchor) {
				result = WireAnchor(host).path + "/" + result;
			}
			return result;
		}

        public function describe():String {
            var path:String = this.path;
            var host:TaconiteObject = this.host;
            while(host is WireAnchor) {
                host = WireAnchor(host).host;
            }
            var hostDsc:String = (host is AbstractObject) ? AbstractObject(host).title : String(host);
            var value:String = (modifierDescription is PropertyDescription) ? ("=" + value) : "";
            return hostDsc + "-" + host.uid + "-" + path + "-" + label + value;
        }
		
		public function get hasAlwaysShownChildren():Boolean {
			for each (var a:WireAnchor in this.childAnchors) {
				if (a.alwaysShow)
					return true;
			}
			return false;
		}
		
		public function get averagePortLocation():Point
		{
			// return global coords
			var context:ViewContext = Application.instance.viewContext;
			var views:Vector.<PortView> = getViews(WorldContainer(context));
			var n:int = views.length;
			if (n == 0)
				return null;
			
			var sumPoint:Point = new Point(0,0);
			var v:PortView;
			var zeroZero:Point = new Point(0,0);
			for each (v in views) {
				var global:Point = v.localToGlobal(zeroZero);
				sumPoint.x += global.x;
				sumPoint.y += global.y;
			}
			sumPoint.x = sumPoint.x / n;
			sumPoint.y = sumPoint.y / n;
			return sumPoint;
		}
		
		public function nearestPortLocation(global:Point):Point
		{
			var context:ViewContext = Application.instance.viewContext;
			var views:Vector.<PortView> = getViews(WorldContainer(context));
			var n:int = views.length;
			if (n == 0)
				return null;
			
			var nearPoint:Point = null;
			var nearDist:Number = 0;
			var v:PortView;
			for each (v in views) {
				var vPoint:Point = v.localToGlobal(new Point(0, 0));
				if (nearPoint == null) {
					nearPoint = vPoint;
					nearDist = nearPoint.subtract(global).length;
				} else {
					var dist:Number = vPoint.subtract(global).length;
					if (dist < nearDist) {
						nearPoint = vPoint;
						nearDist = dist;
					}
				}
			}
			return nearPoint;
		}
		
		public static const pathRegExp:RegExp = /^([^\[]+)(?:\[(\d+)\])?$/;
		
		public static function findByPath(obj:AbstractObject, path:String):WireAnchor
		{
			if(obj == null || path == null || path.length == 0) {
				return null;
			}

			var result:WireAnchor = null;
			var pathElements:Array = path.split("/");
			if (!pathElements || pathElements.length < 1) return null;
			var first:String = pathElements.shift();
			
			var pathElem:Array = pathRegExp.exec(first) as Array;
			if (!pathElem || pathElem.length < 3) return null;
			///if (!(pathElem[1] in obj.anchors)) return null; // removing punt to explore if this caused table issues
			/*const pathIndex:Number = Number(pathElem[2]);
			
			if(pathIndex >= 0) {
				const anchorList:IList = obj.anchors[pathElem[1]] as IList;
				if (!anchorList || !(pathIndex in anchorList)) {
					LogService.error("WireAnchor: Unable to find anchor for obj '"+obj+"' with path '"+path+"'");
					return null; // removing punt to explore if this caused table issues
				}
				result = anchorList[pathIndex] as WireAnchor;*/
			if(Number(pathElem[2]) >= 0) {
				result = IList(obj.anchors[pathElem[1]])[Number(pathElem[2])] as WireAnchor;
			} else {
				result = obj.anchors[pathElem[1]] as WireAnchor;
				const smart:SmartObject = obj as SmartObject;
				if(smart && pathElem[1] in smart.innerAnchors && result == null && pathElem[1].substr(0,9) == "internal-") {
					result = smart.innerAnchors[pathElem[1]];
				}
			}
            if (result) {
                var element:String;
                var child:WireAnchor;
                pathLoop: for each(element in pathElements) {
                    for each(child in result.childAnchors) {
                        if(child.modifierDescription.key == element) {
                            result = child;
                            continue pathLoop;
                        }
                    }
                    // never found a next child at this step in the path chain
                    return null;
                }
            }
			return result;
		}
		
		[Bindable]
		public function get messageCenterVisible():Boolean {
            if(host is WireAnchor) {
                return false; // prevent child ribbons from being always showing
            }
			return _messageCenterVisible;
		}

		public function set messageCenterVisible(value:Boolean):void {
			if(_messageCenterVisible == value) {
				return;
			}
            if(host is WireAnchor) {
                return; // prevent child ribbons from being always showing
            }
			_messageCenterVisible = value;
			if(hostObject == null) {
				return;
			}
			if(_messageCenterVisible) {
				hostObject.dispatchEvent(new AnchorEvent(AnchorEvent.ADD, this, true));
			} else {
				hostObject.dispatchEvent(new AnchorEvent(AnchorEvent.REMOVE, this, false))
			}
		}

		[Bindable]
		public function get propertyInspectorVisible():Boolean {
			return _propertyInspectorVisible;
		}
		

		public function set propertyInspectorVisible(value:Boolean):void {
			if(_propertyInspectorVisible == value) {
				return;
			}
			_propertyInspectorVisible = value;
		}
		
		
		[Bindable]
		public function get wired():Boolean {
			return _wired;
		}
		
		public function set wired(value:Boolean):void {
			_wired = value;
			if(_wired) {
				messageCenterVisible = true;
			}
		}
		
		public function get alwaysShow():Boolean {
			return _messageCenterVisible || wired || hasAlwaysShownChildren;
		}

		public function ensureAttached(obj:AbstractObject):void {
			if(modifierDescription.key in obj.anchors == false) {
				obj.addAnchor(this, false);
			}
		}
		
		public function get wiringScope():AbstractObject
		{
			return (this is InternalSmartWireAnchor) ? hostObject : hostObject.wireScopeContainer;
		}
		
		public function acceptsReplaceableDrop(newReplaceable:IReplaceable):Boolean
		{
			var isAsset:Boolean = newReplaceable is Asset;
			var isSVG:Boolean = (isAsset)?Asset(newReplaceable).content is XML:false;
			if (modifierDescription is PropertyDescription && !(isSVG && hostObject is ToggleButton)) {
				var type:String = PropertyDescription(modifierDescription).type;
				if (newReplaceable.propertyType == type
						&& ((this.replaceable && this.replaceable.id != newReplaceable.id) || !this.replaceable)) { // prevent replacement with identical item
					return true;
				}
			}
			return false;
		}
		
		[Transient]
		public function get replaceable():IReplaceable
		{
			return value as IReplaceable;
		}
		public function set replaceable(replaceable:IReplaceable):void
		{
			value = replaceable;
		}

		override public function toString():String
		{
			if(isGhost()) return "[WireAnchor Ghost]";
			var classname:String = new Utilities().shortClassName(this);
			var hostStr:String = (hostObject) ? hostObject.title : String(host);
			return "[" + classname + " property="+hostProperty+" host="+hostStr + (modifierDescription.indexed?(" idx="+hostPropertyIndex):"")+" wired="+wired+"]";
		}
		
		public function get logStr():String
		{
			if(isGhost()) return "[WireAnchor Ghost]";
			if (modifierDescription==null) return "[WireAnchor modifierDescription=null]";
			return "[\""+modifierDescription.label + "\" path="+path + " obj="+(hostObject ? hostObject.logStr : "") + "]";
		}
		
		private function keyPressHandler(event:UserKeyEvent):void {
			if(ApplicationController.instance.applicationKeyController.isKeyMatched(keyPress, event)){
                var obj:AbstractObject = _host as AbstractObject;
				if(event.down && Application.running && obj.enabled && (obj.active || obj is EventPage)) {
                    handleTrigger();
				}
			}
		}
		
		/* temporary fix -- wireanchor is still being referenced, even though it is deleted */
		public function handleDeleteAnchor():void{
			
			ApplicationController.removeEventListener(UserKeyEvent.TYPE, keyPressHandler,false);
		}
		
		public function getJSONLink():Object{
			return {
				id: hostObject.uid,
				modifier: path
			}
		}
		
		public static function fromJSONLink(obj:Object):WireAnchor{
			var abObject:AbstractObject = (Application.instance.document.root.value as World).findObjByUID(obj.id);
			return abObject?findByPath(abObject, obj.modifier):null;
		}

        public function isNumericProperty():Boolean {
            var prop:PropertyDescription = modifierDescription as PropertyDescription;

            if(prop == null) {
                return false;
            }

            if(Modifiers.isNumericType(prop.type)) {
                return true;
            } else if(prop.key == "text" && hostObject is InputText && InputText(hostObject).numbersOnly) {
                return true;
            }

            return false;
        }

        public function isCreatorHiddenAnchor():Boolean {
            return hostObject is EventPage
            && (this is InternalSmartWireAnchor || this is ExternalSmartWireAnchor)
            && Document(ApplicationController.instance.authorController.document).project.accountTypeId < 3;
        }


        public function getUsageCounts(usages:UsageCounts):void
        {
            // override by SetterWireAnchor & TriggerWireAnchor
        }

        public function get hasAssetUsage():Boolean
        {
            // override by SetterWireAnchor & TriggerWireAnchor
            return false;
        }

        public function get assetID():String
        {
            // override by SetterWireAnchor & TriggerWireAnchor
            return null;
        }

        protected function updateForAssetChange():void
        {
            LibraryController.instance.updateForAssetChange();
        }

	}
}
