package com.alleni.author.model.ui
{
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.factory.TaconiteFactory;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.EventDispatcher;
	
	public class Feedback extends EventDispatcher
	{
		private static var _instance:Feedback;
		
		public static const CONTROL_USERNAME:String = "builder";
		public static const CONTROL_PASSWORD:String = "BuUs2010";
		
		public static const DEFAULT_FEEDBACK_USERNAME:String = "zebrabeta";
		public static const DEFAULT_FEEDBACK_PASSWORD:String = "zebrabeta";
		
		public static const DEFAULT_ASSIGNEE:String = "gdaigle";
		public static const BUG_ASSIGNEE:String = "gdaigle";
		
		public static const JIRA_WSDL:String = "http://jira.zebrabeta.com/rpc/soap/jirasoapservice-v2?wsdl";
		
		public static const PROJECT_KEY_BUG:String = "DEV";
		public static const PROJECT_KEY_FEATURE:String = "GOV";
		public static const PROJECT_KEY_LIVE:String = "PROD";
		
		public static const DEFAULT_VERSION_ID:String = "10000";
		
		public static const PRIORITY_OPTIONS:Array = [
			{label:"Blocker",index:1}
			,{label:"Should Fix",index:3}
			,{label:"Would Be Nice",index:4}
			,{label:"None",index:6}];
		public static const PRIORITY_DEFAULT:int = 3;
		
		public static const FEATURE_IMPORTANCE:Array = [
			{label:"Functional Block"}
			,{label:"UI Feature"}
			,{label:"To Consider"}];
		
		public static const TYPE_INDEXES_NORMAL_USERS:Array = [5, 4, 6, 10, 7];
		public static const TYPE_OPTIONS:Array = [
			{label:"Bug", index:5} 
			,{label:"Feature",index:6}];
		public static const TYPE_DEFAULT:int = 0;
		
		public static const FEEDBACK_OPTIONS:Array = [
			{label:"Please make a selection..."} 
			,{label:"General Feedback"}
			,{label:"Problem in the Editor"}
			,{label:"How do I..."}
			,{label:"I have an idea!"}
			,{label:"Problem in the Site"}
		];
		public static const ISSUE_INDEX:int = 5;

        public static const ISSUE_OPTIONS:Array = [
			{label:"Please select a feature...", data:0}
            ,{label:"------------------------------ ", data:1}
            ,{label:"Site (general)", data:"Site (general)"}
            ,{label:"   Accounts", data:"Accounts"}
            ,{label:"   Shopps", data:"Shopps"}
            ,{label:"   Player", data:"Player"}
            ,{label:"   Support", data:"Support"}
            ,{label:"   Wiki", data:"Wiki Frontend"}
            ,{label:"------------------------------ ", data:2}
            ,{label:"Editor (general)", data:"Editor (general)"}
            ,{label:"   Edit Run Reset", data:"Edit Run Reset"}
            ,{label:"   Event Flow", data:"Event Flow"}
            ,{label:"   Saving / Loading", data:"Saving / Loading"}
            ,{label:"   Publishing", data:"Publish"}
            ,{label:"   Cloud", data:"Cloud"}
            ,{label:"   LMS ", data:"LMS"}
            ,{label:"------------------------------ ", data:3}
            ,{label:"   Message Center", data:"Message Center"}
            ,{label:"   Wiring", data:"Wiring"}
            ,{label:"   Object Behavior", data:"Object Behavior"}
            ,{label:"   Arenas", data:"Arena"}
            ,{label:"   Tables", data:"Tables"}
            ,{label:"   Audio / Video", data:"Media"}
            ,{label:"   Text / Font", data:"Text"}
            ,{label:"   Gadget ", data:"Gadget"}
            ,{label:"------------------------------ ", data:4}
            ,{label:"   Project Settings", data:"Project Settings"}
            ,{label:"   Templates", data:"Templates"}
            ,{label:"   Dialogs/Messages", data:"Dialogs/Messages"}
            ,{label:"   Toolbox", data:"Toolbox"}
            ,{label:"   Resource Library", data:"Library"}
            ,{label:"   Variable Library", data:"Variable Library"}
            ,{label:"   Property Inspector", data:"Property Inspector"}
            ,{label:"   Ribbon Library", data:"Ribbon Library"}
            ,{label:"   Feedback Window", data:"Feedback Window"}
            ,{label:"------------------------------ ", data:5}
            ,{label:"Other ", data:"Other"}
            ,{label:"------------------------------", data:6}
        ];

		public static const FEEDBACK_TYPES_AND_PRIORITIES:Array = [
			{type:-1, priority:-1}
			,{type:7, priority:4}
			,{type:5, priority:1}
			,{type:8, priority:2}
			,{type:6, priority:3}
			,{type:5, priority:2}
		];
		
		
		[Bindable]
		public var enabled:Boolean = false; // whether or not the service is ready to run
		
		[Bindable]
		public var pending:Boolean = false; // whether or not the service is in process, or otherwise suspended
		
		public function get username():String
		{
			if (TaconiteFactory.getEnvironmentImplementation().isAdmin)
				return TaconiteFactory.getEnvironmentImplementation().username;
			else
				return DEFAULT_FEEDBACK_USERNAME;
		}
		public var password:String = DEFAULT_FEEDBACK_PASSWORD;
		public var associatedFeatureId:String = "Other";
		public var disclosureCodeReviewAllowed:Boolean = false;
		public var disclosurePleaseContactMe:Boolean = false;
		public var summary:String = "";
		public var description:String = "";
		public var priorityIndex:int = PRIORITY_DEFAULT;
		public var featureImportance:String = "Added By Beta User";
		public var typeIndex:int = TYPE_DEFAULT;
		public var toggleAttachProject:Boolean = true;
		[Bindable]
		public var toggleAttachScreenshot:Boolean = false;
		public var toggleAttachLog:Boolean = true;
		public var assignee:String = DEFAULT_ASSIGNEE;
		public var project:String = PROJECT_KEY_LIVE;
		public var screenShots:Vector.<BitmapData>;
		
		[Bindable]
		public var toggleVellum:Boolean = false;
		
		[Bindable]
		public var screenshotThumb:Bitmap;
		
		public function Feedback()
		{
			Utilities.assert(!_instance);
			super();
			
			this.screenShots = new Vector.<BitmapData>();
		}
		
		public static function get instance():Feedback
		{
			if (_instance == null)
				_instance = new Feedback();
			return _instance;
		}
	}
}