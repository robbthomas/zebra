package com.alleni.author.model.test
{
import com.alleni.taconite.factory.TaconiteFactory;

import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;

	public class TestPanel extends EventDispatcher
	{
		[Bindable] public var directory:TestDirectory = new TestDirectory();
		

		[Bindable] public var script:TestScript;
        [Bindable] public var scriptName:String;
		[Bindable] public var ditto:Boolean = true;
        [Bindable] public var panelVisible:Boolean = false;
        [Bindable] public var panelTab:int = DIRECTORY_PAGE;
        [Bindable] public var username:String = null;
        [Bindable] public var templates:Boolean = false;
		[Bindable] public var autoFix:Boolean = false;
		[Bindable] public var clipboard:Array = [];

        public static const DIRECTORY_PAGE:int = 0;
        public static const SCRIPT_PAGE:int = 1;
        public static const ALLPROJ_PAGE:int = 3;

		
		public function TestPanel()
		{
			super();
			
			// keep scriptName in sync with "script"
			// and clear "script" if it is deleted
			addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, updateListener);
			directory.scripts.addEventListener(CollectionEvent.COLLECTION_CHANGE, changeListener);
		}
	
		public function toJson():Object
		{
			var directoryData:Object = directory.toJson();
            var scriptNum:int = -1;
            if (script) {
                scriptNum = directory.scripts.getItemIndex(script);
            }
			return {
                directory:directoryData,
                scriptNum:scriptNum,
                panelVisible:panelVisible,
                panelTab:panelTab,
                username:username,
                templates:templates
            };
		}
		
		public function loadFromJson(json:Object):void
		{
			if (json!=null && "directory" in json && json.directory!=null) {
				directory.loadFromJson(json.directory);

                if ("panelVisible" in json) {
                    panelVisible = json.panelVisible;
                }
                if ("panelTab" in json) {
                    panelTab = json.panelTab;
                }
                if ("username" in json) {
                    username = json.username;
                }
                if ("templates" in json) {
                    templates = json.templates;
                }

                script = null;
                if ("scriptNum" in json) {
                    var scriptNum:int = json.scriptNum;
                    if (scriptNum >= 0) {
                        script = directory.scripts.getItemAt(scriptNum) as TestScript;
                    }
                }
			}
		}
		
		private function updateListener(event:PropertyChangeEvent):void
		{
			if (event.property == "script") {
				if (script == null)
					scriptName = "";
				else
					scriptName = script.name;
			}
		}
		
		private function changeListener(event:CollectionEvent):void
		{
			switch (event.kind) {
				case CollectionEventKind.REMOVE:
					if (script && event.items.indexOf(script) >= 0)
						script = null;
					break;
			}
		}
	}
}
