package com.alleni.author.model.test
{
	import com.alleni.taconite.persistence.json.JSONEncoder;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;

	public class TestScript extends EventDispatcher
	{
		// stimulus
		public static const CLICK_STEP:String = "click";
		public static const DRAG_STEP:String = "drag";
		public static const BUTTON_STEP:String = "button";
		public static const SLIDER_STEP:String = "slider";
		
		// verify
		public static const TEXT_STEP:String = "text";
		public static const BITMAP_STEP:String = "bitmap";
		public static const POSITION_STEP:String = "position";
		public static const RIBBON_STEP:String = "ribbon";
		
		// status
		public static const PASS:String = "PASS";
        public static const FAIL:String = "FAIL";
        public static const UNSCRIPTED:String = "UNSCRIPTED";
        public static const INVALID_SCRIPT:String = "BAD SCRIPT";
        public static const EXCEPTION:String = "CRASHED";
        public static const INVALID_PROJECT:String = "INVALID PROJECT";

		
		[Bindable] public var steps:ArrayCollection = new ArrayCollection();
		[Bindable] public var name:String = "";
		[Bindable] public var delay:String = "1";

		// not saved:
		[Bindable] public var status:String = "";
		[Bindable] public var selected:Boolean;

		
		
		public function TestScript()
		{
			super();
		}
		
		public function get selectionSet():Array
		{
			var array:Array = [];
			for each (var step:TestStep in steps) {
				if (step.selected)
					array.push(step);
			}
			return array;
		}
		
		public function get allSelected():Boolean
		{
			var result:Boolean = true;
			for each (var step:TestStep in steps) {
				if (!step.selected)
					result = false;
			}
			return result;
		}
		
        public function select(step:TestStep, sel:Boolean):void
        {
            step.selected = sel;
        }

        public function selectAll(sel:Boolean):void
        {
            for each (var step:TestStep in steps) {
                select(step, sel);
            }
        }

        public function selectRange(one:TestStep, two:TestStep):void
        {
            var oneIndex:int = getStepIndex(one);
            var twoIndex:int = getStepIndex(two);
            if (oneIndex >= 0 && twoIndex >= 0) {
                if (oneIndex > twoIndex) {
                    var temp:int = oneIndex;
                    oneIndex = twoIndex;
                    twoIndex = temp;
                }
                for (var n:int = oneIndex; n <= twoIndex; n++) {
                    select(steps[n], true);
                }
            }
        }

        private function getStepIndex(step:TestStep):int
        {
            for (var n:int=steps.length-1; n >= 0; n--) {
                if (steps[n] == step) {
                    return n;
                }
            }
            return -1;
        }

		public function deleteSelected():void
		{
			for (var n:int=steps.length-1; n >= 0; n--) {
				var step:TestStep = steps[n];
				if (step.selected)
					steps.removeItemAt(n);
			}
		}

		public function clone():TestScript
		{
			var newScript:TestScript = new TestScript();
			newScript.loadFromJson(this.toJson());
			return newScript;
		}

		
		public function toJsonString():String
		{
			var text:String = "[ ";
			var count:int = steps.length;
			for (var n:int=0; n < count; n++) {
				var step:TestStep = steps[n];
				var stepJson:Object = step.toJson();
				var stepString:String = new JSONEncoder(stepJson).getString();
				text += stepString;
				if (n < count-1)
					text += ",\n";
			}
			return text + "]";
		}
		
		public function toJson():Object
		{
			var lines:Array = [];
			var count:int = steps.length;
			for (var n:int=0; n < count; n++) {
				var step:TestStep = steps[n];
				var stepJson:Object = step.toJson();
				lines.push(stepJson);
			}
			return {name:name, delay:delay, steps:lines};  // Object with steps array as one field, so we can add obj.name to it
		}
		
		public function loadFromJson(data:Object):void
		{
			selectAll(true);
			deleteSelected();
			name = data.name;
			if (data.delay)
				delay = data.delay;
			var lines:Array = data.steps;
			var count:int = lines.length;
			for (var n:int=0; n < count; n++) {
				var line:Object = lines[n];
				var step:TestStep = new TestStep();
				step.loadFromJson(line);
				steps.addItem(step);
			}
		}

		
		/**
		 * Load model from Json string. 
		 * @param data
		 * @return true if it worked, false if syntax err.
		 * 
		 */
//		public function loadFromJsonString(data:String):Boolean
//		{
//			deleteAllSteps();
//			try {
//				var lines:Array = new JSONDecoder(data,false).getValue();
//				var count:int = lines.length;
//				for (var n:int=0; n < count; n++) {
//					var line:Object = lines[n];
//					var step:TestStep = new TestStep();
//					step.loadFromJson(line);
//					steps.addItemAt(step, n);
//				}
//			}
//			catch (e:Error) {
//				return false;
//			}
//			return true;  // it worked
//		}
		
		
		
		public function clearStatus():void
		{
			status = "";
			var count:int = steps.length;
			for (var n:int=0; n < count; n++) {
				var step:TestStep = steps[n];
				step.status = "";
			}
		}
		
		public function dump():void
		{
			var count:int = steps.length;
			for (var n:int=0; n < count; n++) {
				var step:TestStep = steps[n];
				trace("     ",step.type,step.label,step.value);
			}
		}
	}
}
