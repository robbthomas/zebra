package com.alleni.author.model.test
{
	import flash.utils.getTimer;
	
	import mx.collections.ArrayCollection;

	public class TestLog
	{
		[Bindable] public var detailLog:String = "";
		[Bindable] public var summaryLog:String = "";

		[Bindable] public var crashed:Boolean = false;

		[Bindable] public var allProjActivity:String = "";
		[Bindable] public var allProjCount:int = 0;
		[Bindable] public var allProjStatus:ArrayCollection = new ArrayCollection();
		[Bindable] public var allProjSuccess:Boolean;
		
		[Bindable] public var countProjects:int = 0;
		[Bindable] public var countSteps:int = 0;
		[Bindable] public var countVerified:int = 0;
        [Bindable] public var countFailed:int = 0;
        [Bindable] public var countWarnings:int = 0;
		[Bindable] public var startTime:uint = 0;
		
		[Bindable] public var nowRecording:Boolean;
		[Bindable] public var nowPlaying:Boolean;
		[Bindable] public var nowPlayingSelected:Boolean;
		[Bindable] public var nowPlayingAllProjs:Boolean;
		
		[Bindable] public var projectName:String = "";
		
		[Bindable] public var prompt:String = "";
		[Bindable] public var promptVisible:Boolean = false;

		[Bindable] public var bitmapTolerance:int = 4;
		[Bindable] public var bitmapDeviation:int = 0;

		
		public function clearStatistics():void
		{
			countProjects = 0;
			countSteps = 0;
			countVerified = 0;
			countFailed = 0;
            countWarnings = 0;
			bitmapDeviation = 0;
			startTime = getTimer();
		}

	}
}