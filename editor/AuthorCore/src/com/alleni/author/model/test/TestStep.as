package com.alleni.author.model.test
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.AssetDescription;
	import com.alleni.author.definition.AssetType;
	import com.alleni.author.event.AssetEvent;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.World;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.service.assets.AssetLoadOperation;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.factory.SerializerImplementation;
	import com.alleni.taconite.persistence.json.JSON;
	import com.alleni.taconite.persistence.json.JSONEncoder;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class TestStep extends EventDispatcher
	{
		public static const BITMAP_NAME:String = "$TestBitmap$.png";  // so we can find them on the server ... convenience, not functional
		
		// stored for all steps
		[Bindable] public var type:String = "";
		[Bindable] public var label:String = "";
		[Bindable] public var value:String = "";
		[Bindable] public var ribbonValue:* = null;

		// only for bitmap step
		[Bindable] public var rect:Rectangle;
		
		// for click & drag
		[Bindable] public var point:Point;
		[Bindable] public var dragPoints:Array;

		// not stored
		[Bindable] public var stepNumber:int = 0;
		[Bindable] public var selected:Boolean = false;
		[Bindable] public var status:String = "";
		
		public var assetID:String;
		public var asset:Asset; 
		
		public var actualBitmap:BitmapData;

        private var _objRefPath:String;
        private var _anchorRefPath:String;  // relative to _objRefPath

		
		public function TestStep()
		{
			super();
		}
		
		public function clone():TestStep
		{
			var newStep:TestStep = new TestStep();
			newStep.loadFromJson(this.toJson());
			newStep.asset = this.asset;
			return newStep;
		}
		
		public function get isStimulus():Boolean
		{
			switch (type) {
				case TestScript.BUTTON_STEP:
				case TestScript.SLIDER_STEP:
				case TestScript.CLICK_STEP:
				case TestScript.DRAG_STEP:
					return true;
				default:
					return false;
			}
		}
		
		public function toJson():Object
		{
			var data:Object = {type:type, label:label, value:value};
			if (rect)
				data.rect = {x:rect.x, y:rect.y, width:rect.width, height:rect.height};
			if (point)
				data.point = {x:point.x, y:point.y};
			if (dragPoints)
				data.dragPoints = pointsToJson(dragPoints);
			if (assetID)
				data.bitmapAssetID = assetID;
			if (objRef) {
				if (_objRefPath != null) {
                    data.reference = _objRefPath;
                }
			}
			if (_anchorRefPath != null && _objRefPath != null)
				data.anchor = _anchorRefPath;  // path is relative to "reference" obj
			if (type == TestScript.RIBBON_STEP)
				data.ribbonValue = encodeRibbonValue(ribbonValue);
			return data;
		}
		
		private static function pointsToJson(points:Array):Array {
			var result:Array = [];
			for each(var point:Point in points) {
				result.push({x:point.x, y:point.y});
			}
			return result;
		}

		
		public function loadFromJson(data:Object):void
		{
			type = data.type;
			label = data.label;
			value = data.value;
						
			if (data.rect) {
				rect = new Rectangle(data.rect.x, data.rect.y, data.rect.width, data.rect.height);
			}
			if (data.point) {
				point = new Point(data.point.x, data.point.y);
			}
			if (data.dragPoints) {
				dragPoints = loadPointsFromJson(data.dragPoints);
			}
			
			if (data.bitmapAssetID) {
				assetID = data.bitmapAssetID;
				value = "";  // after loading the asset, the size will be put here
			}
			if (data.reference!=null && data.reference!="") {
                _objRefPath = data.reference as String;
			}
			if ("anchor" in data) {
                _anchorRefPath = data.anchor;
			}
			if (data.ribbonValue!=null && data.ribbonValue!="") {
				ribbonValue = decodeRibbonValue(data.ribbonValue);
			} else if (type == TestScript.RIBBON_STEP && data.value != null && data.value != "") {
				ribbonValue = decodeRibbonValue(data.value);  // get legacy ribbon value
			}
		}

        public function get objRef():AbstractObject
        {
            if (_objRefPath != null) {
                var array:Array = _objRefPath.split(",");
                return world.resolveObjectPathArray(array);
            } else {
                return null;
            }
        }

        public function set objRef(value:AbstractObject):void
        {
            _objRefPath = null;
            if (value != null) {
                var path:Array = world.objectPathArray(value);
                if (path != null) {
                    _objRefPath = path.join(",");
                }
            }
        }

        public function get anchorRef():WireAnchor
        {
            if (_objRefPath != null && _anchorRefPath != null) {
                var obj:AbstractObject = objRef;
                if (obj) {
                    return WireAnchor.findByPath(obj, _anchorRefPath);
                }
            }
            return null;
        }

        public function set anchorRef(value:WireAnchor):void
        {
            if (value != null) {
                _anchorRefPath = value.path;
            } else {
                _anchorRefPath = null;
            }
        }

		private function encodeRibbonValue(value:*):String
		{
			var json:Object;
			if (value != null && "toJson" in value) {
				json = value.toJson();
			} else if(value as AbstractObject) {
				var obj:AbstractObject = value as AbstractObject;
				var world:World = ApplicationController.instance.authorController.world;
				var id:String = world.objectPathArray(obj).join(",");
				json = {
					type: "TestObjectReference",
					id:id
				}
			} else {
				json = value;  // could be null
			}
			return com.alleni.taconite.persistence.json.JSON.encode(json);
		}
		
		private function decodeRibbonValue(str:String):*
		{
			var json:Object = com.alleni.taconite.persistence.json.JSON.decode(str, false);
			
			var value:*;
			if (json == null) {
				value = null;
			} else if ("type" in json && json.type == "TestObjectReference") {
				var id:String = json.id;
				var path:Array = id.split(",");
				var world:World = ApplicationController.instance.authorController.world;
				value = world.resolveObjectPathArray(path);
			} else {
				value = SerializerImplementation.deserializeValue(json);
			}
			return value;
		}
		
		private function loadPointsFromJson(array:Array):Array
		{
			var result:Array = [];
			for each(var obj:Object in array) {
				result.push(new Point(obj.x, obj.y));
			}
			return result;
		}
		
		private function get world():World
		{
			return Application.instance.document.root.value as World;
		}
	}
}
