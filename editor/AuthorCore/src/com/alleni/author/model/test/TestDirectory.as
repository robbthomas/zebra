package com.alleni.author.model.test
{
import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.events.PropertyChangeEvent;

	public class TestDirectory extends EventDispatcher
	{
		
		[Bindable] public var scripts:ArrayCollection = new ArrayCollection();  // array of TestScript
		
		
		public function TestDirectory()
		{
			super();
			
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var nameSort:Sort = new Sort();
			nameSort.fields = [nameField];
			scripts.sort = nameSort;
			
			addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, updateListener);
		}
		
		private function updateListener(event:PropertyChangeEvent):void
		{
		}
		
		public function get selectionSet():Array
		{
			var array:Array = [];
			for each (var script:TestScript in scripts) {
				if (script.selected)
					array.push(script);
			}
			return array;
		}

		public function get allSelected():Boolean
		{
			var result:Boolean = true;
			for each (var script:TestScript in scripts) {
				if (!script.selected)
					result = false;
			}
			return result;
		}
		
        public function select(entry:TestScript, select:Boolean):void
        {
            entry.selected = select;
        }

        public function selectAll(select:Boolean):void
        {
            for each (var script:TestScript in scripts) {
                script.selected = select;
            }
        }

        public function selectRange(one:TestScript, two:TestScript):void
        {
            var oneIndex:int = getStepIndex(one);
            var twoIndex:int = getStepIndex(two);
            if (oneIndex >= 0 && twoIndex >= 0) {
                if (oneIndex > twoIndex) {
                    var temp:int = oneIndex;
                    oneIndex = twoIndex;
                    twoIndex = temp;
                }
                for (var n:int = oneIndex; n <= twoIndex; n++) {
                    select(scripts[n], true);
                }
            }
        }

        private function getStepIndex(step:TestScript):int
        {
            for (var n:int=scripts.length-1; n >= 0; n--) {
                if (scripts[n] == step) {
                    return n;
                }
            }
            return -1;
        }

		public function clearStatus():void
		{
			for each (var script:TestScript in scripts) {
				script.clearStatus();
			}
		}
		
		public function sort():void
		{
			trace("sort directory");
			scripts.refresh();
		}

		
		public function toJson():Object
		{
			var arrayData:Array = [];
			var count:int = scripts.length;
			for (var n:int=0; n < count; n++) {
				var script:TestScript = scripts[n];
				var scriptJson:Object = script.toJson();
				arrayData.push(scriptJson);
			}
			return {scripts:arrayData};
		}

		
		public function loadFromJson(data:Object):void
		{
			scripts.removeAll();
			var arrayData:Array = data.scripts;
			var count:int = arrayData.length;
			for (var n:int=0; n < count; n++) {
				var scriptData:Object = arrayData[n];
				var script:TestScript = new TestScript();
				script.loadFromJson(scriptData);
				scripts.addItem(script);
			}
		}
		
		public function dump():void
		{
			trace("TestDirectory:");
			var count:int = scripts.length;
			for (var n:int=0; n < count; n++) {
				var script:TestScript = scripts[n];
				trace("  ",script.name);
			}
		}
	}
}
