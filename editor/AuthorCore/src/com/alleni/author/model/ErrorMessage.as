/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 11/10/11
 * Time: 10:40 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model {
    public class ErrorMessage {

        private var _messageId:int;
        private var _messageTitle:String;
        private var _messageBody:String;
        private var _messageButtonList:Vector.<String> = new Vector.<String>();
        private var _messageContainerType:String;

        public function ErrorMessage() {
        }

        public function addButton(label:String):void{
            _messageButtonList.push(label);
        }
        public function buildMessageButtonList(buttons:String):void{
            var btns:Array = buttons.split(" | ");
            for each(var button:String in btns){
                addButton(button);
            }
            _messageButtonList.reverse();
        }

        //Getters and Setters
        public function get messageId():int{
            return _messageId;
        }
        public function set messageId(value:int):void{
            _messageId = value;
        }

        public function get messageTitle():String{
            return _messageTitle;
        }
        public function set messageTitle(value:String):void{
            _messageTitle = value;
        }

        public function get messageBody():String{
            return _messageBody;
        }
        public function set messageBody(value:String):void{
            _messageBody = value;
        }

        public function get messageButtonList():Vector.<String>{
            return _messageButtonList;
        }

        public function get messageContainerType():String{
            return _messageContainerType;
        }
        public function set messageContainerType(value:String):void{
            _messageContainerType = value;
        }
    }
}
