package com.alleni.author.model
{
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.InletDescription;
    import com.alleni.author.model.ui.WireAnchor;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;

	public class ObjectInlets
	{
		public static var instance:ObjectInlets = new ObjectInlets();

		public function ObjectInlets():void
		{
		}

		public function registerInlet(object:AbstractObject, description:IModifier):void
		{
			var ribbon:WireAnchor = InletDescription(description).createAnchor(object);


//			ribbon.messageCenterVisible = InspectorValues.instance.getPreset(Objects.shortClassNameForClass(Object(object).constructor))["messageCenterVisible"].indexOf(description.key) >= 0;

			object.modifiers[description.key] = description;
			if(description.indexed) {
				object.anchors[description.key] = new ArrayCollection();
				IList(object.anchors[description.key]).addItem(ribbon);
			} else {
				object.anchors[description.key] = ribbon;
			}
		}

		public function unregisterInlet(value:Object, modifier:IModifier):void
		{
			delete AbstractObject(value).anchors[modifier.key];
			delete AbstractObject(value).modifiers[modifier.key];
		}
	}
}