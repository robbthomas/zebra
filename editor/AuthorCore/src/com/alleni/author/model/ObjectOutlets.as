package com.alleni.author.model
{
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.OutletDescription;
	import com.alleni.author.model.ui.WireAnchor;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;

	public class ObjectOutlets
	{
		public static var instance:ObjectOutlets = new ObjectOutlets();

		public function ObjectOutlets():void
		{
		}

		public function registerOutlet(object:AbstractObject, description:IModifier):void
		{
			var ribbon:WireAnchor = OutletDescription(description).createAnchor(object);

//			ribbon.messageCenterVisible = InspectorValues.instance.getPreset(Objects.shortClassNameForClass(Object(object).constructor))["messageCenterVisible"].indexOf(description.key) >= 0;

			object.modifiers[description.key] = description;
			if(description.indexed) {
				object.anchors[description.key] = new ArrayCollection();
				IList(object.anchors[description.key]).addItem(ribbon);
			} else {
				object.anchors[description.key] = ribbon;
			}
		}

		public function unregisterOutlet(value:Object, modifier:IModifier):void
		{
			delete AbstractObject(value).anchors[modifier.key];
			delete AbstractObject(value).modifiers[modifier.key];
		}
	}
}