package com.alleni.author.model
{
	import com.alleni.author.controller.TweenController;
	import com.alleni.taconite.dev.ColorUtils;

	public class GraphicFill
	{
		public static const SOLID_FILL:uint 		= 0;
		public static const GRADIENT_FILL:uint 		= 2;
		public static const JELLYBEAN_FILL:uint 	= 3;
		
		public static const SOLID_LABEL:String 		= "solid color";
		public static const GRADIENT_LABEL:String 	= "gradient";
		public static const JELLYBEAN_LABEL:String 	= "jellybean";
		
		public static const MAX_COLORS:int = 16;  // limit imposed by Flash
		public static const JELLYBEAN_GRADIENT_ANGLE:Number = 90;  // this puts the first color at the top
		
		
		// the type of fill
		private var _type:int = SOLID_FILL;
		
		// solid:  the color
		public var color:uint = 0xffffff;  // white by default
		
		// gradient
		public var linear:Boolean = true;
		public var angle:Number = 0.0;
		public var xOffset:Number = 0.0;  // -100..100 offset from center
		public var yOffset:Number = 0.0;
		public var colors:Vector.<uint> = new Vector.<uint>();
		
		
		// jellybean
		public var color1:int = -1;
		public var color2:int = -1;
		public var flipGlare:Boolean = false;
		
		
		public function GraphicFill(value:String=null)
		{
			if (value != null)
				setFromString(value);
		}
		
		public function clone():GraphicFill
		{
			var fill:GraphicFill = new GraphicFill();
			fill._type = type;
			fill.color = color;
			fill.linear = linear;
			fill.angle = angle;
			fill.xOffset = xOffset;
			fill.yOffset = yOffset;
			fill.color1 = color1;
			fill.color2 = color2;
			fill.flipGlare = flipGlare;
			
			if (colors) {
				var count:int = colors.length;
				fill.colors = new Vector.<uint>();
				for (var n:int=0; n<count; n++)
					fill.colors[n] = colors[n];
			}
			
			return fill;
		}

        public function equals(other:GraphicFill):Boolean {				
            if(this.type == other.type){
                switch(this.type){
                    case GraphicFill.SOLID_FILL:
                        return this.color == other.color;
                        break;
                    case GraphicFill.GRADIENT_FILL:
                        if(this.colors.length != other.colors.length){
                            return false;
                        }
                        var i:int;
                        var match:Boolean = false;
                        for(i=0;i<this.colors.length; i++){
                            if(this.colors[i] == other.colors[i]){
                                match = true;
                            }else{
                                match = false;
                                break;
                            }
                        }
                        return match;
                        break;
                    case GraphicFill.JELLYBEAN_FILL:
                        return this.color1 == other.color1 && this.color2 == other.color2;
                        break;
                }
            }
            return false;
        }
		
		/**
		 * Convert from one type of fill to another.
		 * Performs intelligent mapping of data. 
		 * @param value
		 * 
		 */
		public function set type(value:int):void
		{
			if (value != _type) {
				var oldType:int = _type;
				_type = value;
				switch (_type) {
					case SOLID_FILL:  // converting to solid fill
						switch (oldType) {
							case GRADIENT_FILL:
								if (colors.length > 0)
									color = colors[0];
								break;
							case JELLYBEAN_FILL:
								color = color1;
								break;
						}
						break;
					case GRADIENT_FILL: // converting to gradient
						colors = new Vector.<uint>();
						switch (oldType) {
							case SOLID_FILL:
								colors[0] = color;
								colors[1] = color;
								break;
							case JELLYBEAN_FILL:
								colors[0] = color1;
								colors[1] = color2;
								angle = JELLYBEAN_GRADIENT_ANGLE;
								break;
						}
						break;
					case JELLYBEAN_FILL:  // converting to jelly
						switch (oldType) {
							case SOLID_FILL:
								color1 = color;
								color2 = color;
								break;
							case GRADIENT_FILL:
								if (colors.length >= 2) {
									color1 = colors[0];
									color2 = colors[1];
								}
								break;
						}
						break;
				}
			}
		}
		
		public function get type():int
		{
			return _type;
		}
		
		
		public static function typeLabel(type:int):String
		{
			switch (type) {
				case GRADIENT_FILL:
					return GRADIENT_LABEL;
				case JELLYBEAN_FILL:
					return JELLYBEAN_LABEL;
				default:
					return SOLID_LABEL;
			}
		}
		
		
		public function convertToColor():uint
		{
			// supports binding of a fillSpec to a line color
			switch (type) {
				case GRADIENT_FILL:
					return (colors.length > 0) ? colors[0] : color;
				case JELLYBEAN_FILL:
					return color1;
			}
			return color;
		}
		
		/**
		 * This fill has just been converted from solid-color to a gradient or jelly. 
		 * Now we make a nice gradient by inventing a 2nd color to complement the first one.
		 */
		public function makeNiceGradient():void
		{
			var firstColor:uint = convertToColor();
			var brightness:Number = ColorUtils.brightness(firstColor);
			var secondColor:uint = (brightness > 0.5) ? ColorUtils.darkenColor(firstColor, 50) : ColorUtils.brightenColor(firstColor, 50);

			if (_type == GraphicFill.GRADIENT_FILL)
				colors[1] = secondColor;
			else if (_type == GraphicFill.JELLYBEAN_FILL)
				color2 = secondColor;
		}
		

		private static function colorToJson(colorValue:uint):String
		{
			var result:String = "000000" + colorValue.toString(16);
			return "#" + result.substring(result.length - 6, result.length);
		}

		private static function colorsToJson(colors:Vector.<uint>):Array {
			var result:Array = [];
			for each(var color:uint in colors) {
				result.push(colorToJson(color));
			}
			return result;
		}

		public function toJson():Object
		{
			switch (type) {
				case GRADIENT_FILL:
					return {
						type: "Gradient Fill",
						linear: linear,
						angle:angle,
						xOffset:xOffset,
						yOffset:yOffset,
						colors: colorsToJson(colors)
					};
				case JELLYBEAN_FILL:
					return {
						type: "Jellybean Fill",
						flipGlare: flipGlare,
						colors: [colorToJson(color1),colorToJson(color2)]
					};
				default:
					return {
						type: "Solid Fill",
						color: colorToJson(color)
					};
			}
		}

		private static function colorFromJson(json:String):uint {
			return uint(json.replace("#","0x"));
		}

		private static function colorsFromJson(json:Array):Vector.<uint> {
			var result:Vector.<uint> = new Vector.<uint>();
			for each(var color:String in json) {
				result.push(colorFromJson(color));
			}
			return result;
		}

		public static function fromJson(json:Object):GraphicFill {
			var result:GraphicFill = new GraphicFill();
			switch(json.type) {
				case "Gradient Fill":
					result._type = GRADIENT_FILL;
					result.linear = json.linear;
					result.angle = json.angle;
					result.xOffset = json.xOffset;
					result.yOffset = json.yOffset;
					result.colors = colorsFromJson(json.colors);
					break;
				case "Jellybean Fill":
					result._type = JELLYBEAN_FILL;
					result.flipGlare = json.flipGlare;
					result.color1 = colorFromJson(json.colors[0]);
					result.color2 = colorFromJson(json.colors[1]);
					break;
				case "Solid Fill":
					result._type = SOLID_FILL;
					result.color = colorFromJson(json.color);
					break;
			}
			return result;
		}
		
		public static function setupTween(start:GraphicFill, end:GraphicFill):void
		{
			// knock off the cases that won't be gradients
			if (end.type == GraphicFill.JELLYBEAN_FILL) {
				start.type = GraphicFill.JELLYBEAN_FILL;
				return;
			} else if (start.type == GraphicFill.SOLID_FILL && end.type == GraphicFill.SOLID_FILL) {
				return;
			}
			
			// convert both to gradients
			var oldStartType:int = start.type;
			var oldEndType:int = end.type;
			start.type = GraphicFill.GRADIENT_FILL;
			end.type = GraphicFill.GRADIENT_FILL;
			
			// if either was a solid color, then force angles to match, to prevent rotation during tween
			if (oldEndType == GraphicFill.SOLID_FILL) {
				end.angle = start.angle;
				end.linear = start.linear;
			} else if (oldStartType == GraphicFill.SOLID_FILL) {
				start.angle = end.angle;
				start.linear = end.linear;
			} else {
				start.linear = end.linear;
			}
			
			// now both are gradient fills:  adjust to give same number of colors
			if (start.colors.length != end.colors.length) {
//				trace("resolving: start="+start, "  end="+end);
				var newSize:int = start.colors.length * end.colors.length;
				if (newSize > MAX_COLORS)
					newSize = MAX_COLORS;

				start.colors = resolveColors(start.colors, newSize); 
				end.colors = resolveColors(end.colors, newSize);
			}
			
			// force both to be linear, or both radial
			if (start.linear != end.linear) {
				start.linear = end.linear;
				start.angle = end.angle;
				start.xOffset = end.xOffset;
				start.yOffset = end.yOffset;
			}
//			trace("  result: start="+start, "  end="+end);
		}
		
		private static function resolveColors(old:Vector.<uint>, length:int):Vector.<uint>
		{
			var oldLen:int = old.length;
			var colors:Vector.<uint> = new Vector.<uint>();
			
			for (var n:int=0; n < length; n++) {
				var oldIndexReal:Number = Number(n * oldLen) / length;
				var oldIndex:int = oldIndexReal;  // truncate to integer
				var frac:Number = oldIndexReal - oldIndex;  // remainder fraction
				var oldBelow:uint = old[oldIndex];  // color below
				var color:uint;
				if (oldIndex >= oldLen-1) {
					color = oldBelow;
				} else {
					var oldAbove:uint = old[oldIndex+1];  // color above
					color = TweenController.computeColorTween(oldBelow, oldAbove, frac);
				}
				colors[n] = color;
			}
			return colors;
		}
		
		public static function computeTween(startFill:GraphicFill, endFill:GraphicFill, frac:Number):GraphicFill
		{
			if (frac == 1.0)
				return endFill;
			
			var fill:GraphicFill = new GraphicFill();
			var startColor:uint;
			var endColor:uint;
			fill._type = startFill.type;
			switch (fill.type) {
				case GraphicFill.SOLID_FILL:
					fill.color = TweenController.computeColorTween(startFill.color, endFill.color, frac);
					break;
				case GraphicFill.GRADIENT_FILL:
					fill.linear = endFill.linear;
					if (fill.linear) {
						fill.angle = startFill.angle + frac * (endFill.angle - startFill.angle);
					} else { // radial
						fill.xOffset = startFill.xOffset + frac * (endFill.xOffset - startFill.xOffset);
						fill.yOffset = startFill.yOffset + frac * (endFill.yOffset - startFill.yOffset);
					}
					fill.colors = new Vector.<uint>();  // start with empty array
					var colorCount:int = startFill.colors.length;
					for (var n:int=0; n < colorCount; n++) {
						startColor = startFill.colors[n];
						endColor = endFill.colors[n];
						fill.colors[n] = TweenController.computeColorTween(startColor, endColor, frac);
					}
					break;
				case GraphicFill.JELLYBEAN_FILL:
					startColor = startFill.color1;
					endColor = endFill.color1;
					fill.color1 = TweenController.computeColorTween(startColor, endColor, frac);
					startColor = startFill.color2;
					endColor = endFill.color2;
					fill.color2 = TweenController.computeColorTween(startColor, endColor, frac);
					fill.flipGlare = endFill.flipGlare;
					break;
			}
			
//			trace("tween: frac="+frac, "fill="+fill);
			return fill;
		}
		
		
		////
		/// old stuff to translate to and from strings (non json format)
		/// should be deleted ... but currently shows in tables.
		///
		
		private function typeFromFillString(value:String):int
		{
			var firstChar:String = value.substr(0,1);
			switch (firstChar) {
				case "G":
					return GRADIENT_FILL;
				case "J":
					return JELLYBEAN_FILL;
				default:
					return SOLID_FILL;
			}
		}
		
		public function setFromString(value:String):void
		{
			if (value == null)
				return;
			
			_type = typeFromFillString(value);
			
			switch (_type) {
				case GRADIENT_FILL:
					var leftBracket:int = value.search("\\[");
					if (value.length > 5 && leftBracket >= 2) {  // check lengths to prevent crashing on bogus string
						linear = (value.substr(1,1) == "L");
						if (linear)
							angle = Number(value.substr(2,leftBracket-2));
						else {
							// get xOffset, yOffset
							var comma:int = value.search("\\,");
							if (comma > 2 && comma < leftBracket) {
								var offsetStr:String = value.substr(2,comma-2);
								xOffset = Number(offsetStr);  // decimal
								offsetStr = value.substr(comma+1, leftBracket-comma-1);
								yOffset = Number(offsetStr);
							}
						}
						var arrayText:String = value.substr(leftBracket+1,value.length-leftBracket-2);  // array without brackets
						colors = colorsFromText(arrayText);
					}
					break;
				case JELLYBEAN_FILL:
					leftBracket = value.search("\\[");
					if (value.length > 4 && leftBracket > 0) {
						flipGlare = (value.substr(1,1) == "F");  // optional letter F as 2nd char will flip the glare vertically
						var jellyColorsStr:String = value.substr(leftBracket+1, value.length-leftBracket-2);
						var jellyColors:Vector.<uint> = colorsFromText(jellyColorsStr);
						if (jellyColors.length == 2) {
							color1 = jellyColors[0];
							color2 = jellyColors[1];
						}
					}
					break;
				default:
					if (value.substr(0,1) == "#")
						color = uint("0x" + value.substr(1));  // accept color as hex digits
					else
						color = uint(value);  // decimal digits
					break;
			}
			//			trace("GraphicFill: setFromString value="+value, "type="+type, "color="+color);
		}
		
		public function toString():String
		{
			switch (type) {
				case SOLID_FILL:
					return "#" + color.toString(16);
				case GRADIENT_FILL:  // GL90[111,222,333]
					var linearCode:String;
					var detailStr:String = "";  // either angle (for linear) or offsets (for radial)
					if (linear) {
						linearCode = "L";
						if (angle != 0)
							detailStr = angle.toString();  // angle is decimal number
					} else {  // radial
						linearCode = "R";
						if (xOffset != 0 || yOffset != 0)
							detailStr = xOffset.toString() + "," + yOffset.toString();
					}
					return "G" + linearCode + detailStr + vectorToString(colors);
				case JELLYBEAN_FILL: // J[111,222]
					var flipStr:String = flipGlare ? "F" : "";
					return "J" + flipStr + "[" + color1.toString(16) + "," + color2.toString(16) + "]"; 
			}
			return "";
		}
		
		private function vectorToString(vector:Vector.<uint>):String
		{
			var result:String = "[";  // encode an array of colors values as hex numbers with commas such as:   [123,44ff,ffee]
			var count:int = vector.length;
			for (var n:int=0; n < count; n++) {
				result += vector[n].toString(16);
				if (n < count-1)
					result += ",";
			}
			return result + "]";
		}

		private function colorsFromText(text:String):Vector.<uint>
		{
			//			trace("colorsFromText: text="+text);
			var vector:Vector.<uint> = new Vector.<uint>;
			while (true) {
				var word:String;
				var comma:int = text.search("\\,");
				if (comma != -1)
					word = text.substr(0,comma);
				else 
					word = text;
				
				var color:uint = uint("0x" + word);
				vector.push(color);
				
				if (comma <= 0)
					break;
				text = text.substr(comma+1);
			}
			return vector;
		}
		


	}
}
