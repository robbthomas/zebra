package com.alleni.author.model
{
	import com.alleni.author.definition.IModifier;
    import com.alleni.author.definition.PropertyDescription;
    import com.alleni.author.model.ui.WireAnchor;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;

	public class ObjectProperties
	{
		public static var instance:ObjectProperties = new ObjectProperties();

		public function ObjectProperties():void
		{
		}

		public function registerProperty(object:AbstractObject, description:IModifier, property:String):WireAnchor
		{
			var ribbon:WireAnchor = PropertyDescription(description).createAnchor(object, property);


//			ribbon.messageCenterVisible = InspectorValues.instance.getPreset(Objects.shortClassNameForClass(Object(object).constructor))["messageCenterVisible"].indexOf(description.key) >= 0;

			object.modifiers[property] = description;
			if(description.indexed) {
				object.anchors[property] = new ArrayCollection();
				IList(object.anchors[property]).addItem(ribbon);
			} else {
				object.anchors[property] = ribbon;
			}

			// debug:  list the properties having the property variable name different from the modifier key
			var key:String = description.key;
			//	Utilities.assert(key == property);
			
			return ribbon;
		}

		public function unregisterProperty(value:Object, modifier:IModifier):void
		{
			for(var key:Object in AbstractObject(value).anchors) {
				if(WireAnchor(AbstractObject(value).anchors[key]).modifierDescription == modifier) {
					delete AbstractObject(value).anchors[key];
					delete AbstractObject(value).modifiers[key];
				}
			}
		}
	}
}