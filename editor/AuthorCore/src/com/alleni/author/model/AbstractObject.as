 /**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */

package com.alleni.author.model
{
import caurina.transitions.Tweener;

import com.alleni.author.Navigation.EventPage;
import com.alleni.author.Navigation.IPage;

import com.alleni.author.Navigation.Pager;
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.ObjectController;
import com.alleni.author.controller.ObjectDragMediator;
import com.alleni.author.controller.TweenController;
import com.alleni.author.controller.ui.ApplicationController;
import com.alleni.author.controller.ui.WireController;
import com.alleni.author.controller.ui.palettes.InspectorController;
import com.alleni.author.controller.ui.palettes.LibraryController;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.definition.PositionAndAngle;
import com.alleni.author.document.Document;
import com.alleni.author.event.AnchorEvent;
import com.alleni.author.model.objects.AbstractContainer;
import com.alleni.author.model.objects.AbstractTable;
import com.alleni.author.model.objects.Composite;
import com.alleni.author.model.objects.EventFlow;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.SetterWireAnchor;
import com.alleni.author.model.ui.TriggerWireAnchor;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.model.ui.Wire;
import com.alleni.author.model.ui.WireAnchor;
import com.alleni.author.persistence.GadgetEncoder;
import com.alleni.author.util.CustomPoint;
import com.alleni.author.util.PresetPoint;
import com.alleni.author.view.ObjectView;
import com.alleni.author.view.WorldContainer;
import com.alleni.author.view.ui.MessageCenterView;
import com.alleni.author.view.ui.RibbonContainerInfo;
import com.alleni.taconite.controller.TaconiteController;
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.factory.SerializerImplementation;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.model.TaconiteObject;
import com.alleni.taconite.persistence.json.JSON;
import com.alleni.taconite.service.LogService;
import com.alleni.taconite.view.ITaconiteView;
import com.alleni.taconite.view.ViewContext;

import flash.display.BlendMode;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Dictionary;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.events.PropertyChangeEvent;

/**
	 * Abstract value object representing some object in the world.
	 */
	public class AbstractObject extends TaconiteObject
	{
		public static const TACONITE_COLLECTIONS:Vector.<String> = Vector.<String>(["inletRibbons", "outletRibbons", "propertyRibbons"]);
		
		public var serial:uint;

        // this is the array id of the object specifying its location within
        // the containing project or smart object using a series of property names or child indexes.
        public var serializedLocation:String = null;
				
		[Transient] public var controlled:Boolean = false;
		
		[Transient] protected var _wireController:WireController = ApplicationController.instance.wireController;
		
		[Bindable]
		public var layer:int = 0;
		
        [Bindable]
        public var maintainAspect:Boolean = false;

		[Bindable]
		public var acceptIntoOutOfArena:Boolean = true;
		
		private var _zIndex:int = 0;
		[Bindable]
		public function get zIndex():int
		{
			return _zIndex;
		}
		public function set zIndex(value:int):void
		{
			_zIndex = value;
		}

		[Bindable]
		public var forceHilite:Boolean;
		
		[Bindable]
		[Transient]
		public var loading:Boolean = false;

        [Bindable]
        [Transient]
        public var enclosedActsLikeRunning:Boolean = false;

		[Transient]
		public var messageCenterCache:MessageCenterView;
		
		[Bindable]
		public var messageCenter:Boolean = true;

        [Transient]
        public var toolTipOverrides:Dictionary = new Dictionary();

        private var _title:String;

        [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
        public function get title():String {
            return _title;
        }
        public function set title(value:String):void {

            var oldTitle:String = this.title;
            if (oldTitle == value) {
                return;
            }
            _title = value;
            // make sure the propertyChange event will contain actual result of title getter (eg. Event 1), not the raw value assigned by deserialization or initialization
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "title", oldTitle, this.title));

            if(loading == false && complete == true) {
                InspectorController.instance.handleTitleChange(this);

                var stack:Vector.<AbstractContainer> = new Vector.<AbstractContainer>();
                stack.push(ApplicationController.instance.authorController.world);
                var wc:WireController = ApplicationController.instance.wireController;
                while(stack.length > 0) {
                    var root:AbstractContainer = stack.pop();
                    for each(var child:AbstractObject in root.children) {
                        if(child is AbstractContainer) {
                            stack.push(child as AbstractContainer);
                        }
                        for each(var a:Object in child.anchors) {
                            if(a is TriggerWireAnchor && TriggerWireAnchor(a).condition == this) {
                                TriggerWireAnchor(a).dispatchEvent(PropertyChangeEvent.createUpdateEvent(a,  "condition", this, this));
                                TriggerWireAnchor(a).dispatchEvent(PropertyChangeEvent.createUpdateEvent(a, "childValue", this, this));
                            } else if(a is IList && IList(a).length > 0 && ["port","currentStateValues"].indexOf(WireAnchor(IList(a)[0]).hostProperty) >= 0) {
                                for each(var indexedAnchor:WireAnchor in IList(a)) {
                                    var wire:Wire = wc.findWireToObject(indexedAnchor, this);
                                    if(wire) {
                                        wire.fireCompleteEvent(true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

		[Bindable]
        public var titleChangedByAuthor:Boolean;
		
		[Transient]
		public var initialValues:Object = new Object();
		
		[Transient]
		[Bindable]
		public var inletRibbons:IList = new ArrayCollection();
		
		[Transient]
		[Bindable]
		public var outletRibbons:IList = new ArrayCollection();
		
		[Transient]
		[Bindable]
		public var propertyRibbons:IList = new ArrayCollection();
		
		[Transient]
		public var anchors:Dictionary = new Dictionary(false); // key -> WireAnchor
		private var _anchorCount:int = 0;
		
		[Transient]
		public var modifiers:Dictionary = new Dictionary(false); // key -> IModifier

        [Transient] public var ribbonCategoriesToOpen:Vector.<RibbonContainerInfo> = null;
		
		[Bindable]
		public var movable:Boolean = false;  /* Whether or not the object is movable by the student */
		
		[Bindable]
		public var inertial:Boolean = false;  /* Whether or not the object retains its momentum from drag or move */
		
		[Bindable]
		public var lineColor:uint = 0x000000;
		
		[Bindable]
		public var lineThickness:Number = 1;
		
		[Bindable]
		public var lineAlpha:Number = 100;
		
		[Bindable]
		public var frameColor:uint = 0x000000;
		
		[Bindable]
		public var frameThickness:Number = 0;
		
		[Bindable]
		public var frameAlpha:Number = 0;
		
		[Transient]
		private var _fillSpec:GraphicFill = new GraphicFill();

		[Transient]
		public var tickListener:Function = null;
		
		[Bindable]
		public function get fillSpec():GraphicFill
		{
			// to make changes to fill, client MUST create a new GraphicFill and assign it to obj.fillSpec.    
			// it will not work to say:  obj.color.fillSpec.color = xxx; 
			return _fillSpec.clone();
		}
		
		public function set fillSpec(value:GraphicFill):void
		{
			_fillSpec = value;
		}
		
		[Transient]
		public var fillGradient:Vector.<uint>;  //used by preset values object
		
		[Transient]
		public var linearGradient:Boolean;  //used by preset values object
		
		[Transient]
		public var shineGradient:Vector.<uint>;  //used by preset values object
		
		[Bindable]
		public var fillAlpha:Number = 100;
		
		[Transient] [Bindable] public var excludeFromContainerLayouts:Boolean = false; // used to temporarily exclude an object from layout control
		
		// visible and cloakedNow are defined to affect each other
		private var _doingVisSideEffect:int = 0;
		private var _visible:Boolean = true;  // default value will be set in object factory
		[Bindable]
		public function get visible():Boolean{
			return _visible;
		}
		public function set visible(value:Boolean):void{
			_visible = value;
			if(Application.running == false && !loading && _wireController.bindingsEnabled){  //This happens when we are setting initial value, works like pauseRunningState.
                resumeVisible = !hideOnRun && value;
            }
            if (_doingVisSideEffect==0 && !loading && _wireController.bindingsEnabled) {
				++_doingVisSideEffect;
				cloakedNow = false;
				--_doingVisSideEffect;
			}
		}
		
		private var _cloakedNow:Boolean = false;
		[Bindable]
		public function get cloakedNow():Boolean{
			return _cloakedNow;
		}
		public function set cloakedNow(value:Boolean):void{
			_cloakedNow = value;
            if(Application.running == false && !loading && _wireController.bindingsEnabled){   //This happens when we are setting initial value, works like pauseRunningState
                resumeCloaked = _cloakOnRun || value;
            }
			if (_doingVisSideEffect==0 && !loading && _wireController.bindingsEnabled) {
				++_doingVisSideEffect;
				visible = !value;
				--_doingVisSideEffect;
			}
		}

        [Bindable]
        public function get cloakOnRun():Boolean {
            return _cloakOnRun;
        }

        public function set cloakOnRun(value:Boolean):void {
            _cloakOnRun = value;
            if(Application.running == false && !loading && _wireController.bindingsEnabled){   //This happens when we are setting initial value, works like pauseRunningState
                resumeCloaked = _cloakOnRun || (initialValues.hasOwnProperty("cloakedNow") && (initialValues.cloakedNow == true));
            }
        }

        [Bindable]
        public function get hideOnRun():Boolean {
            return _hideOnRun;
        }

        public function set hideOnRun(value:Boolean):void {
            _hideOnRun = value;
            if(Application.running == false && !loading && _wireController.bindingsEnabled){  //This happens when we are setting initial value, works like pauseRunningState.
                resumeVisible = !_hideOnRun && !(initialValues.hasOwnProperty("visible") && (initialValues.visible == false));
            }
        }

		[Bindable]
		public var alpha:Number = 100;  /* opacity for the enabled or "normal" state*/
		
		[Bindable]
		public var xDiagram:Number = NaN;  /* Horizontal position of the object in the Diagram view */
		
		[Bindable]
		public var yDiagram:Number = NaN;  /* Vertical position of the object in the Diagram view */
		
		[Bindable]
		public var thumbnailScale:Number = 1;  /* Scale of thumbnail representation in the Diagram view */

		[Bindable]
		public var enabled:Boolean = true;  // "Enable outs"

        [Bindable]
        public var interactive:Boolean = true;  // "Active"

        [Bindable] public var disabledAlpha:Number = 50;

        private var _cloakOnRun:Boolean = false;  // Cloak on run

        private var _hideOnRun:Boolean = false;
		
		[Bindable]
		public var runtimeRotation:Boolean = false;
		
		[Bindable]
		[Transient]
		public var showAll:Boolean = false;

		[Bindable]
        [Transient]  // transient so we can signal obj when lazyLoad is done
		public var active:Boolean = false;  /* false if on an inactive page of a flipbook */

        [Transient]
        [Bindable]
        public var editing:Boolean = false;
		
		private var _anchorPoint:Point = new Point(0.5, 0.5);
		
		[Bindable]
		public var targetCollide:AbstractObject;
		
		[Bindable]
		public var targetDrop:AbstractObject;
		
		[Bindable]
		public var putBackWhen:String = "never";
		
		[Bindable]
		public var putBackSecs:Number = 0;
		
		[Bindable]
		public var xMessageCenter:Number = 0;  // position of MC when undocked, world coordinates
		[Bindable]
		public var yMessageCenter:Number = 0;  // ... but when messageCenterRelative=true, its gadget or arena local coordinates
						
		[Bindable]
		public var messageCenterRepositioned:Boolean = false;  // MC undocked
		[Bindable]
		public var messageCenterRelative:Boolean = false;

		[Bindable]
		public var messageCenterVisible:Boolean = true;
		
		[Bindable]
		public var authorNotes:String = "";
		public var xAuthorNote:Number = -20; // offset from owner object topLeft corner, world coordinates
		public var yAuthorNote:Number = -20; 
		
		[Bindable]
		public var alignAreaHorz:String = "";  // alignment of this object in the world or arena
		
		[Bindable]
		public var alignAreaVert:String = "";
		
		[Bindable]
		public var scale:Number = 100;

		[Transient]
		[Bindable]
		public var pendingReplacement:Boolean;
		[Transient]
		[Bindable]
		public var pendingData:Boolean;
		
		[Bindable]
		private var _locked:Boolean;
		[Bindable]
		public function get locked():Boolean{
			return _locked;
		}
		public function set locked(value:Boolean):void{
			_locked = value;
		}

		private var _controller:ObjectController = null;
		[Transient]
		public function get controller():ObjectController
		{
			if(_controller == null) {
				_controller = TaconiteController.forModel(model,Application.instance.childDefinitions) as ObjectController;
			}
			return _controller;
		}
		
		
		[Bindable]
		public var messageCenterLocked:Boolean;
		
		[Bindable]
		public var messageCenterRolled:Boolean;
		
		[Bindable]
		[Transient]  // initialize value to current arrangement, so next change can be detected
		public var toDiagram:Boolean =  false; //(ApplicationUI.instance.diagramContainer != null && ApplicationUI.instance.diagramContainer.stage != null); true if diagram now open
		
		
		[Bindable]
		[Transient]
		public var complete:Boolean = false;  // true when author has completed initial drawing of object (true in loaded objects)
		
		public var redrawWires:Boolean;  // set true to request redraw of wires
		
		[Bindable]
		public var snapTo:AbstractObject = null;
				
        private var _pathValue:Number = 0;

    /**
  		 *	Returns the Slider sliderValue.
  		 */
  		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
  		public function get pathValue():Number
  		{
  			return _pathValue;
  		}

          public function forceSetPathValue(value:Number):void {
  			// value range and offset from min/max
  			var range:Number = pathMaximum - pathMinimum;
  			var valOffset:Number = value - pathMinimum;

//  			// apply units
              // no units for paths for now
//  			if (pathUnit) {
//  				var numUnits:int = Math.round(valOffset / pathUnit);
//  				valOffset = numUnits * pathUnit;
//
//  				// for units greater than or equal to one, round value to integer
//  				if (pathUnit >= 1)
//  					valOffset = Math.round(valOffset);
//  			}

  			// limit value to ends of scale
  			if (pathMaximum >= pathMinimum) {
  				if (valOffset < 0)
  					valOffset = 0;
  				else if (valOffset > range)
  					valOffset = range;
  			} else {  // max is less than min
  				if (valOffset > 0)
  					valOffset = 0;
  				else if (valOffset < range)
  					valOffset = range;
  			}

              var oldValue:Number = _pathValue;
              var oldPercent:Number = pathPercent;
              _pathValue = pathMinimum + valOffset;

              // no min/max testing for now
//  			// values driving hitTop and hitBottom
//  			atMinimum = (_pathValue == pathMinimum);
//  			atMaximum = (_pathValue == pathMaximum);

  			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "pathValue", oldValue, _pathValue));
  			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "pathPercent", oldPercent, pathPercent));
          }

  		/**
  		 *	Given a value in the caller's units in range from [sliderMinimum, sliderMaximum), set the Slider value.
  		 */
  		public function set pathValue(value:Number):void
  		{
              if(loading) {
                  _pathValue = value;
                  return;
              }
              if(_pathValue  != value && !(value != value)) {
                  // is a change
                  forceSetPathValue(value);
              }
  		}

        [Transient]
        [Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
  		public function get pathPercent():Number
  		{
  			var range:Number = pathMaximum - pathMinimum;
  			if (range == 0) // avoid division by zero
  				return 0;
  			else
  				return 100 * (pathValue - pathMinimum) / range;
  		}


  		public function set pathPercent(percent:Number):void
  		{
  			var range:Number = pathMaximum - pathMinimum;
  			pathValue = percent * (range/100) + pathMinimum;  // use the main setter, so units are enforced
  		}
				
		private var _pathMaximum:Number = 100.0;
		[Bindable]
		public function get pathMaximum():Number
		{
			return _pathMaximum;	
		}
		public function set pathMaximum(value:Number):void
		{
			_pathMaximum = value;
		}
		
		private var _pathMinimum:Number = 0.0;
		[Bindable]
		public function get pathMinimum():Number
		{
			return _pathMinimum;
		}
		public function set pathMinimum(value:Number):void
		{
			_pathMinimum = value;
		}
		
		[Bindable]
		public var snap:Boolean = true;
		
		[Bindable]
		public var pathOn:Boolean = true;  // when false, this object is not stuck to the snapTo path

		
		
		[Transient]
		[Bindable]
		public var dragging:Boolean;  // true if the author is dragging this object (running or paused)

		[Transient]
		[Bindable]
		public var draggingHandle:Boolean; 
		
		public function setStagePosition(global:Point):void
		{
			if (okToSnapToPath) {
				snapTo.moveObjAlongPath(this, global);
			} else {
				var view:ObjectView = getView();
				if (view) {
					var local:Point = view.parent.globalToLocal(global);
					x = local.x;
					y = local.y;
				}
			}
		}
		
		private var _x:Number;
		private var _y:Number;

		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get x():Number
		{
			return _x;	
		}
		
		public function set x(value:Number):void
		{
			if (value != _x) {
				if (okToSnapToPath) {
					snapTowardPoint(value, _y);
				} else {
					var oldX:Number = _x;
					var oldXYR:PositionAndAngle = locationXYR;
					_x = value;
					
					dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "x", oldX, _x));
					dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "locationXYR", oldXYR, locationXYR));
				}
			}
		}		

		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get y():Number
		{
			return _y;	
		}
		
		public function set y(value:Number):void
		{
			if (value != _y) {
				if (okToSnapToPath) {
					snapTowardPoint(_x, value);
				} else {
					var oldY:Number = _y;
					var oldXYR:PositionAndAngle = locationXYR;
					
					_y = value;
					
					dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "y", oldY, _y));
					dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "locationXYR", oldXYR, locationXYR));
				}
			}
		}	
		
		protected function get okToSnapToPath():Boolean
		{
			// overridden by Composite & PathObject
			return (snapTo != null && pathOn && snapTo.isAnimationPath && !snapTo.dragging && !loading && _wireController.bindingsEnabled && !snapping);
		}

		[Transient]
		public function get top():Number {
			return - anchorPoint.y * height;
		}

		[Transient]
		public function get left():Number {
			return - anchorPoint.x * width;
		}

		[Transient]
		public function get bottom():Number {
			return top + height;
		}

		[Transient]
		public function get right():Number {
			return left + width;
		}
		
		[Transient]
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		public function get locationXYR():PositionAndAngle
		{
			return new PositionAndAngle(x,y,rotation);
		}
				
		public function set locationXYR(value:PositionAndAngle):void
		{		
			if (value) {
				var oldValue:PositionAndAngle = locationXYR;
				if (!value.isEqualTo(oldValue)) {
					if (okToSnapToPath) {
						snapTowardPoint(value.x, value.y);
					} else {
						x = value.x;
						y = value.y;
						if (isNaN(value.angle) == false)  // path nodes give angle=NaN
							rotation = value.angle;
						dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "locationXYR", oldValue, value));
					}
				}
			}
		}

        /*
            Will be true for any of:
            1. child of closed composite
            2. child of closed event-page
            3. child of closed master-page of an arena (not yet implemented)
            4. obj on global backstage.
        */

        [Transient]
        public function get actLikeRunning():Boolean {
            return enclosedActsLikeRunning && childOfClosed;
        }

        [Transient]
        public function get childOfClosed():Boolean
        {
            // loop in case this object is in Arena object(s) inside closed gadget
            var flowVisible:Boolean = Application.instance.flowVisible;
            var obj:AbstractObject = this;
            var parentModel:TaconiteModel = model.parent;
            while (parentModel) {
                var par:AbstractContainer = parentModel.value as AbstractContainer;
                var smart:SmartObject = par as SmartObject;
                var flow:EventFlow = par as EventFlow;
                if (smart) {
                    return !smart.activeVelum;
                } else if (flow && flow.objects.getItemIndex(obj) >= 0) {
                    return !flowVisible;  // object on global backstage, visible only when FlowView is visible
                } else {
                    obj = par;
                    parentModel = parentModel.parent;
                }
            }
            return false;
        }

        [Transient]
        public function get isInEventPage():Boolean
        {
            var parentModel:TaconiteModel = model.parent;
            while (parentModel) {
                var par:AbstractContainer = parentModel.value as AbstractContainer;
                if(par is EventPage){
                    return true;
                }
                parentModel = parentModel.parent;
            }
            return false;
        }

        [Transient]
        public function get childOfClosedComposite():Boolean
        {
            var composite:Composite = wireScopeContainer as Composite;  // specific to Composite, not EventPage
            return composite && !composite.editing;
        }

        [Transient]
        public function get containingEventPage():EventPage
        {
            if(this is EventPage){
                return this as EventPage;
            }
            var obj:AbstractContainer = parent;
            while(obj != null && obj is EventPage == false && obj is World == false) {
                obj = obj.parent;
            }
            return obj as EventPage;
        }

        [Transient]
        public function get childOfClosedMasterPage():Boolean
        {
            var page:EventPage = this.containingEventPage;
            return (page && page.isMaster && !page.editing);
        }

		[Transient]
		public function get wiringLocked():Boolean
		{
			return messageCenterLocked;
		}
		
		[Transient]
		public function get editingLocked():Boolean
		{
			return locked;
		}
		
		// Effects
		[Transient] [Bindable] public var transientPointerOver:Boolean = false; // used for shadowRollOverLength
		[Bindable]
		public var shadowLength:Number = 0;
		[Bindable]
		public var shadowRollOverLength:Number = 0;
		[Bindable]
		public var shadowAngle:Number = 45;
		[Bindable]
		public var shadowColor:Number = 0x000000;
		[Bindable]
		public var shadowAlpha:Number = 80;
		[Bindable]
		public var shadowBlur:Number = 5;
		[Bindable]
		public var glowLength:Number = 0;
		[Bindable]
		public var glowColor:Number = 0xffff00; // yellow
		[Bindable]
		public var glowStrength:Number = 2;
		[Bindable]
		public var glowInner:Boolean = false;
		[Bindable]
		public var colorSaturation:Number = 100;
		[Bindable]
		public var blur:Number = 0;
		[Bindable]
		public var blendMode:String = BlendMode.NORMAL;
		[Bindable]
		public var reflection:Boolean = false;

		[Transient]
		private var _views:Dictionary = new Dictionary(true);

		[Bindable]
		[Transient]
		public var collidingWith:Array = [];

		[Bindable]
		[Transient]
		public var droppedOn:Array = [];

		[Transient]
		public var collidingPossibilities:Array = [];

		[Transient]
		public var droppedPossibilities:Array = [];

		public function updateCollidingPossibilities():void {
			var result:Array = [];
			for each(var obj:Object in anchors) {
				var trigger:TriggerWireAnchor = obj as TriggerWireAnchor;
				if(trigger != null && trigger.triggerProperty == "collidingWith" && trigger.condition != null) {
					if(result.indexOf(trigger.condition) < 0) {
						result.push(trigger.condition);
					}
				}
			}
			collidingPossibilities = result;
		}

		public function updateDroppedPossibilities():void {
			var result:Array = [];
			for each(var obj:Object in anchors) {
				var trigger:TriggerWireAnchor = obj as TriggerWireAnchor;
				if(trigger != null && trigger.triggerProperty == "droppedOn" && trigger.condition != null) {
					if(result.indexOf(trigger.condition) < 0) {
						result.push(trigger.condition);
					}
				}
			}
			droppedPossibilities = result;
		}

        [Bindable]
        public var resumeVisible:Boolean = true;

        [Bindable]
        public var resumeCloaked:Boolean = false;


        public function set bindingsDisabled(value:Boolean):void {
            var d:AbstractObject = ApplicationController.instance.wireController.bindingsDisabled;
            Utilities.assert(d == null || d == this);
            if(value) {
                ApplicationController.instance.wireController.bindingsDisabled = this;
            } else {
                ApplicationController.instance.wireController.bindingsDisabled = null;
            }
        }

        [Transient]
        public function get bindingsDisabled():Boolean {
            var d:AbstractObject = ApplicationController.instance.wireController.bindingsDisabled;
            if(d == null){
                return false;
            } else if(d is World) {
                return true;
            } else {
                var ao:AbstractObject = this;
                while(ao != null) {
                    if(ao == d) {
                        return true;
                    }
                    ao = ao.parent;
                }
            }
            return false;
        }
		
		public function AbstractObject(setupProperties:Boolean=true):void
		{	
			if (!setupProperties)
				return;  // for the World, we don't want properties registered
			
			// Visibility
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("layer"), "layer");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("visible"), "visible");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("cloakOnRun"), "cloakOnRun");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("cloakedNow"), "cloakedNow");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("hideOnRun"), "hideOnRun");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("runtimeRotation"), "runtimeRotation");
			
			// basic properties
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("x"), "x");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("y"), "y");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("locationXYR"), "locationXYR");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("width"), "width");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("height"), "height");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("scale"), "scale");	
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fillSpec"), "fillSpec");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("fillAlpha"), "fillAlpha");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("lineColor"), "lineColor");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("lineThickness"), "lineThickness");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("lineAlpha"), "lineAlpha");
		//	ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("frameColor"), "frameColor");	
		//	ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("frameThickness"), "frameThickness");	
		//	ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("frameAlpha"), "frameAlpha");		
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alpha"), "alpha");

			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("anchorPoint"), "anchorPoint");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("rotation"), "rotation");	
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("enabled"), "enabled");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("movable"), "movable");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("inertial"), "inertial");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("putBackSecs"), "putBackSecs");

			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("collidingWith"), "collidingWith");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("droppedOn"), "droppedOn");

			// alignment: controls positioning of individual objects within an Arena
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alignAreaHorz"), "alignAreaHorz");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("alignAreaVert"), "alignAreaVert");
			
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("authorNotes"), "authorNotes");
			
			// Effects 
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("shadowRollOverLength"), "shadowRollOverLength");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("shadowLength"), "shadowLength");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("shadowAngle"), "shadowAngle");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("shadowColor"), "shadowColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("shadowAlpha"), "shadowAlpha");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("shadowBlur"), "shadowBlur");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("glowColor"), "glowColor");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("glowLength"), "glowLength");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("glowStrength"), "glowStrength");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("glowInner"), "glowInner");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("colorSaturation"), "colorSaturation");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("blur"), "blur");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("blendMode"), "blendMode");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("reflection"), "reflection");

			
			
		//	ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("authorNotes"), "authorNotes");
			
			// basic inlets and outlets
			//   -- we are simply registering these here. Inlet is handled in ObjectController::handleTrigger
			//   -- Outlet is handled using WireController::sendTrigger
			/*ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("show"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("hide"));*/
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("cloak"));

			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("bringFront"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("bringForward"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("sendBackward"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("sendBack"));

			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("putBack"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("snapToNearest"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("snapToValue"));
			ObjectInlets.instance.registerInlet(this, Modifiers.instance.fetch("resetObject"));

			
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("dragStart"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("dragMove"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("dragRelease"));
			
//			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("dropOnTarget"));
//
//			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("collideOverlap"));
//			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("uncollision"));

			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("click"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("clickStart"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("clickRelease"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("rollOver"));
			ObjectOutlets.instance.registerOutlet(this, Modifiers.instance.fetch("rollOut"));

			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("snapTo"), "snapTo");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("pathValue"), "pathValue");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("pathMaximum"), "pathMaximum");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("pathMinimum"), "pathMinimum");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("pathPercent"), "pathPercent");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("snap"), "snap");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("pathOn"), "pathOn");
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("zIndex"), "zIndex");	
			ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("acceptIntoOutOfArena"), "acceptIntoOutOfArena");	
            ObjectProperties.instance.registerProperty(this, Modifiers.instance.fetch("maintainAspect"), "maintainAspect");
		}
		
		
		public function initializeModel(loaded:Boolean):void
		{
			trace("AbstractObject::initializeModel loaded="+loaded, "obj="+this);
		}

		public function addAnchor(anchor:WireAnchor, display:Boolean):void
		{
			if (anchors[anchor.modifierDescription.key] == null) {
				anchors[anchor.modifierDescription.key] = anchor;
				modifiers[anchor.modifierDescription.key] = anchor.modifierDescription;
				dispatchEvent(new AnchorEvent(AnchorEvent.ADD, anchor, display));
				_anchorCount += 1;
			}else if(anchors[anchor.modifierDescription.key])
			{
				dispatchEvent(new AnchorEvent(AnchorEvent.ADD, anchor, display));
				_anchorCount += 1;
			}
            if (!loading && anchor.hasAssetUsage) {
                LibraryController.instance.updateForAssetChange();
            }
		}

		/**
		 * This method removes an anchor and corresponding modifier, then
		 * dispatches an event that causes the related ribbon to be removed
		 * from the message center.
		 *  
		 * @param anchor WireAnchor
		 * 							The WireAnchor retrieved from the model.
		 * 							Ex:  btn.removeAnchor(btn.anchors["fillSpec"])
		 * 
		 */
		public function removeAnchor(anchor:WireAnchor):void {
			if (anchors[anchor.modifierDescription.key] == anchor) {
                anchor.messageCenterVisible = false;
				delete anchors[anchor.modifierDescription.key];
				delete modifiers[anchor.modifierDescription.key];
				dispatchEvent(new AnchorEvent(AnchorEvent.REMOVE, anchor, false));
				_anchorCount -= 1;
			} else if (anchors[anchor.modifierDescription.key]) {
				throw(new Error("This should never happen."))
			}
            if (anchor.hasAssetUsage) {
                LibraryController.instance.updateForAssetChange();
            }
		}
		
		public function pauseTweens():void{
			if(this is AbstractTable){
				Tweener.pauseTweens(this);
			}else{
				Tweener.pauseTweens(this);
				for each(var anchor:Object in anchors){
					if(anchor is SetterWireAnchor){
						SetterWireAnchor(anchor).pauseTweens();
					}
				}
			}
		}
		
		public function resumeTweens():void{
			if(this is AbstractTable){
				Tweener.resumeTweens(this);
			}else{
				Tweener.resumeTweens(this);
				for each(var anchor:Object in anchors){
					if(anchor is SetterWireAnchor){
						SetterWireAnchor(anchor).resumeTweens();
					}
				}
			}
		}
		
		public function cancelTweens():void{
			if(this is AbstractTable){
				Tweener.removeTweens(this);
			}else{
				Tweener.removeTweens(this);
				for each(var anchor:Object in anchors){
					if(anchor is SetterWireAnchor){
						SetterWireAnchor(anchor).cancelTweens();
					}
				}
			}
		}

        public function onPause():void {
            pauseTweens();
            pauseRunningState();
        }

        public function onRestoreInitial():void {
            cancelTweens();
            initializeValues();
            initializeRunningState();
        }

        public function onResume():void {
			resumeRunningState();
            resumeTweens();
        }

        protected function initializeValues():void
		{
			for (var property:String in initialValues) {
				if (property in this) {  // don't crash when a loaded file has obsolete inits
					if (controller.propertyNeedsInit(property, this) && property != "currentStateValues" && property != "toggleButtonGroup") {
						//trace("  prop="+property, "val="+obj.initialValues[property]); uncomment as needed. otherwise, please leave commented
                        if(property == "controlsVisible" && Application.uiRunning){
                            continue;
                        }
						try {
							var serialized:Object = initialValues[property];
							var oldval:Object = this[property];
							var deserialized:Object = SerializerImplementation.deserializeValue(serialized, oldval, null, this, property);
							this[property] = deserialized;
						} catch (error:ReferenceError) {
							// readonly prop or property doesn't exist on the class
							// ignore
							// trace("Skipping property decode", error);
						} catch (error:TypeError) {
							LogService.error("Initial value restoration: " + error + "  prop="+property + " obj="+uid);
						}
					} else{
						//                        trace("Column or something like it")
					}
				}
			}
            droppedOn = [];
            collidingWith = [];
			if (initialValues.snapTo == null) {
				snapTo = null;
			}

            for each(var o:Object in anchors) {
                var anchor:WireAnchor = o as WireAnchor;
                if(anchor) {
                    anchor.restoreFromInitialValues();
                    continue;
                }
                var list:IList = o as IList;
                if(list) {
                    for each(var o2:Object in list) {
                        var anchor2:WireAnchor = o2 as WireAnchor;
                        if(anchor) {
                            anchor2.restoreFromInitialValues();
                        }
                    }
                }
            }
		}

        protected function initializeRunningState():void {
            this.showAll = Application.instance.showAll;
        }

        protected function pauseRunningState():void {
            var oldVisible:Boolean = initialValues["resumeVisible"];
            var oldCloaked:Boolean = initialValues["resumeCloaked"];
            resumeCloaked = _cloakedNow;
            resumeVisible = _visible;
            initialValues["resumeVisible"] = oldVisible;
            initialValues["resumeCloaked"] = oldCloaked;
            if(_cloakOnRun && initialValues.hasOwnProperty("cloakedNow") && (initialValues.cloakedNow == false) && Application.instance.editWhileRunningInProgress == false) {
                cloakedNow = false;
            }
            if(_hideOnRun && initialValues.hasOwnProperty("visible") && (initialValues.visible == true) && Application.instance.editWhileRunningInProgress == false) {
                visible = true;
            }
        }

        protected function resumeRunningState():void {
            var oldVisible:Boolean = _visible;
            var oldCloaked:Boolean = _cloakedNow;
			_visible = resumeVisible;
			_cloakedNow = resumeCloaked;
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "visible", oldVisible, _visible));
            dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "cloakedNow", oldCloaked, _cloakedNow));
        }

		public function cancelLocationTweens():void {
            Tweener.removeTweens(this, "putBackFrac");
            Tweener.removeTweens(this, "x");   // arena layout tween
            Tweener.removeTweens(this, "y");   // arena layout tween
			for each(var anchor:Object in anchors){
				if(anchor is SetterWireAnchor && ["x", "y", "locationXYR"].indexOf(SetterWireAnchor(anchor).setterProperty) >= 0){
						SetterWireAnchor(anchor).cancelTweens();
				}
			}
		}
		
		[Bindable]
		public function get anchorPoint():Point  {
			return _anchorPoint;
		}
		
		public function set anchorPoint(value:Point):void
		{
			if(value is CustomPoint) {
				_isCustomAnchor = true;
			} else if(value is PresetPoint) {
				_isCustomAnchor = false;
			} else if(_isCustomAnchor) { // passed ordinary Point class while dragging anchorPoint
				anchorPoint = new CustomPoint(value.x, value.y);  // recursively set value, to ensure init will have this type
				return;
			}
			// adjust x,y so object will stay at same location  (not when loading or doing inits)
			if (!loading && _wireController.bindingsEnabled) {
				var dx:Number = (value.x - _anchorPoint.x) * width * scale/100;
				var dy:Number = (value.y - _anchorPoint.y) * height * scale/100;
				var radians:Number = - rotation * Math.PI / 180;
				x += dy * Math.sin(radians) + dx * Math.cos(radians);
				y += dy * Math.cos(radians) - dx * Math.sin(radians);
			}
			if (value is CustomPoint)
				_anchorPoint = CustomPoint(value).clonePoint();  // maintain CustomPoint class, while cloning value
			else
				_anchorPoint = value.clone();  // will be Point class
		}
		
		private var _isCustomAnchor:Boolean = false;
		
		public function get hasCustomAnchor():Boolean
		{
			return _isCustomAnchor;
		}
		
				
		private var _width:Number = 0;
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		/**
		 * 
		 * @return object width in pixels.
		 * 
		 */
		public function get width():Number
		{
			return _width;	
		}
		
		/**
		 * Set object width in pixels.  Does not change x or y, but does change x,y.
		 * Fire property change for width, x & y.
		 * 
		 */
		public function set width(value:Number):void
		{
			if (value != _width) {
				
				var oldWidth:Number = _width;
				
				_width = value;
				
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "width", oldWidth, _width));
			}
		}		
		
		private var _height:Number = 0;
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		/**
		 * 
		 * @return object height in pixels.
		 * 
		 */
		public function get height():Number
		{
			return _height;	
		}
		
		/**
		 * Set object height in pixels.  Does not change x or y, but does change x,y.
		 * Fire property change for height, x & y.
		 * 
		 */
		public function set height(value:Number):void
		{
			if (value != _height) {
				
				var oldheight:Number = _height;
				
				_height = value;
				
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "height", oldheight, _height));
			}
		}


		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		[Transient]
		/**
		 * 
		 * @return current angle in degrees
		 * 
		 */
		public function get rotation():Number
		{
			return rawRotation;	
		}
		
		/**
		 * Set rotation angle while holding the pivot point location constant.
		 * This will normally change x,y (the topleft).
		 * When pivotPoint != anchorPoint, this will also change x & y.
		 * @param value desired angle in degrees
		 * 
		 */
		public function set rotation(value:Number):void
		{
			if (value != rawRotation) {
			    rawRotation = value;
			}
		}
		
		
		private var _rawRotation:Number = NaN;  // initial value NaN so init will be created when set to zero by factory
		[Bindable(event=PropertyChangeEvent.PROPERTY_CHANGE)]
		/**
		 * 
		 * @return current angle in degrees.  The value of rawRotation is equal to "rotation"
		 * but the rawRotation setter does not affect x, y or x, y.
		 * The rawRotation value is stored in the file (rotation is not).
		 * 
		 */
		public function get rawRotation():Number
		{
			return _rawRotation;	
		}
		
		/**
		 * Set rotation angle and fire property change events; but do NOT affect x,y values.
		 * Fire property change for rotation, x & y.   Not for rawRotation, x, y.
		 * 
		 */
		public function set rawRotation(value:Number):void
		{
			if (value != _rawRotation) {
				
				var oldRotation:Number = _rawRotation;
				var oldXYR:PositionAndAngle = locationXYR;
				
				_rawRotation = value;  // rotate !

				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "rotation", oldRotation, _rawRotation));
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "rawRotation", oldRotation, _rawRotation));
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "locationXYR", oldXYR, locationXYR));
			}
		}
		
		[Transient]
		public function get localBounds():Rectangle
		{
			return new Rectangle(left, top, width, height);
		}   

		public function register(view:ObjectView):void
		{
			_views[view] = true;
		}

		public function getViews(context:WorldContainer) : Vector.<ObjectView>
		{
			var result:Vector.<ObjectView> = new Vector.<ObjectView>();
			for(var o:Object in _views) {
				var v:ObjectView = o as ObjectView;
				if(v && v.stage && v.context == context) {
					result.push(v);
				}
			}
			return result;
		}

		public function getView(context:ViewContext=null):ObjectView
		{
			if (context == null)
				context = Application.instance.viewContext;
			for(var o:Object in _views) {
				var v:ObjectView = o as ObjectView;
				if(v && v.stage && v.context == context) {
					v.updateViewPosition();  // ensure view position is up to date, so localToGlobal will give valid results
					return v;
				}
			}
			return null;
		}

		[Transient]
		override public function toString():String
		{
			// [Oval #101 "my obj title" x=42 XY=55,90]
			return "[" + shortClassName + " \"" + _title+"\" XY="+int(x)+","+int(y)+ " active="+active+"]";
//            return "[" + shortClassName + " \"" + _title+"\" XY="+int(x)+","+int(y)+" WH="+int(width)+","+int(height)+" uid="+uid+"]";
		}
		
		[Transient]
		public function get logStr():String
		{
			return "[" + shortClassName + " \"" + _title+"\" uid="+uid+"]";
		}

		[Transient]
		public function get parent():AbstractContainer {
			if(model.parent) {
				return model.parent.value as AbstractContainer
			}
			return null;
		}

		[Transient]
		public function get wireScopeContainer():AbstractContainer
		{
			var obj:AbstractContainer;
			
			if (this is SmartObject && SmartObject(this).scopeParent != null){
				obj = SmartObject(this).scopeParent;  // while editing gadget, its temporarily moved to front of World but scopeParent is logical parent
			}else{
				obj = this.parent;
			}
			
			while(obj != null && obj is SmartObject == false && obj is World == false) {
				obj = obj.parent;
			}
            if(obj == null) {
                return ApplicationController.instance.authorController.world;
            }
            assert(obj != this);
			return obj;
		}

        public function isEventualScopeContainer(childScope:AbstractObject):Boolean
        {
            while(childScope != null) {
                if(childScope == this) {
                    return true;
                }
                childScope = childScope.wireScopeContainer;
            }
            return false;
        }
		
		/**
		 * Called on a SmartObject, this function invokes the specified callback for each descendant object
		 * that is within wiring scope.  That is, all immediate children and the children of arena objects
		 * nested to any level. And the object itself is also done.
		 * Callbacks return true to continue, false to stop.
		 * Function result is object where callback returned false, or null to indicate full scan.
		 * @param callback
		 *
		 */
        public function allObjectsInScope(callback:Function):AbstractObject
		{
			var ao:AbstractObject;
			var child:AbstractObject;
			var objects:Vector.<AbstractObject> = new Vector.<AbstractObject>();
            if (!callback(this))
                return this;
			objects.push(this);
			for each (ao in objects) {
				if(ao is AbstractContainer && !(ao != this && ao is SmartObject)) {
					for each(child in AbstractContainer(ao).objects) {
                        if (!callback(child))
                            return child;
						objects.push(child);
                    }
                }
                var lists:Vector.<IList> = ao.model.getAllValueModelLists();  // masterPages, etc
                for each (var list:IList in lists) {
                    for each(child in list) {
                        if (!callback(child))
                            return child;
                        objects.push(child);
                    }
                }
                for each(var m:TaconiteModel in ao.model.getAllValueModels()) {
                    child = m.value as AbstractObject;
                    if (!callback(child))
                        return child;
                    objects.push(child);
                }
			}
			return null;
		}

        public function allObjectsInWiringScope():Vector.<AbstractObject>
        {
            var result:Vector.<AbstractObject> = new Vector.<AbstractObject>();
            var thisObject:AbstractObject = this;
            this.allObjectsInScope(function(obj:AbstractObject):Boolean {
                if (obj != thisObject) {
                    result.push(obj);
                }
                return true;
            });
            return result;
        }

		/**
		 * Given a gadget child, return the nearest containing object having accessible ribbons.
		 * Objects in a closed gadget are considered not-accessible.
		 * When a gadget is being edited, that gadget and its immediate children are both accessible.
		 * Containing arena objects are ignored in this process.
		 * The object iself will be returned if its a child of the world or child of a gadget being edited.
		 */
		[Transient]
		public function get accessibleObject():AbstractObject
		{
			var par:AbstractObject = this.parent;
			var result:AbstractObject = this;
			
			while (par != null && par is World == false && par is EventPage == false) {
				if (par is SmartObject) {
					if (SmartObject(par).editing) {
						return result;
					}
					else {
						result = par;
					}
				}
				par = par.parent;
			}
			return result;	
		}


		[Transient]
		public function get hasMessageCenter():Boolean {
			return messageCenter && (this.hasOwnProperty("noMessageCenter") == false) && (this is World == false);
		}
		
		public function recordInitialValue(property:String):void {
            if(this[property] is AbstractObject) {
                initialValues[property] = AbstractObject(this[property]).referenceJSON;
            } else {
			    initialValues[property] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(this[property]));
            }
		}
		
		[Transient]
		public function get okToSelect():Boolean
		{
			return !locked && (visible || showAll);
		}

		public function setInitialZIndex(changedParents:Boolean = true):void
		{
			//trace(">>Attempting to set initial Z on " + title);
			var parent:AbstractContainer = parent as AbstractContainer;
			if(parent != null && !(parent is Pager)) {
				if(!("parent" in initialValues && initialValues.parent != null && "id" in initialValues.parent) || changedParents || initialValues.parent.id == parent.uid) {
					initialValues.parent = parent.referenceJSON;
					initialValues.zIndex = parent.objects.getItemIndex(this);
					zIndex = parent.objects.getItemIndex(this);
					//trace(">>  Setting initial Z on", title, "to", parent.title, initialValues.parent, initialValues.zIndex);
				}
			}
		}

		public var putBackFrac:Number;

		public function putBack():void
		{
            if (getView() == null) {
                return;  // sorry but we cannot put back an offstage view, since ObjectDragMediator.changeParentObject relies on getView()
            }
			if (initialValues.parent != null) {
				var deserialized:Object = SerializerImplementation.deserializeValue(initialValues.parent, this.parent)
				var newParent:AbstractContainer = deserialized as AbstractContainer;
				if (newParent != this.parent) {
					ObjectDragMediator.changeParentObject(this, newParent);
				}
                if (initialValues.zIndex != null) {
                    var initialZ:int = initialValues.zIndex as int;
                    this.parent.setChildIndexWithinLayer(this, initialZ);
                }
			}
			
			var rotate:Number = ("rawRotation" in initialValues) ? initialValues.rawRotation : rawRotation;
			if(snapTo)
			{
				var pathPerc:Number = ("pathPercent" in initialValues) ? initialValues.pathPercent : pathPercent;
				if(putBackSecs <= 0) {
					this.pathPercent = pathPerc;
					return;
				}
				Tweener.addTween(this, {pathPercent:pathPerc, time:putBackSecs, delay:0, transition:"linear"});
				Tweener.addTween(this, {rotation:rotate, time:putBackSecs, delay:0, transition:"linear"});
				
			}else{
				var x:Number = ("x" in initialValues) ? initialValues.x : x;
				var y:Number = ("y" in initialValues) ? initialValues.y : y;
				var start:PositionAndAngle = this.locationXYR;
				var end:PositionAndAngle = new PositionAndAngle(x, y, rotate);
				
				if(putBackSecs <= 0) {
					this.locationXYR = end;
					return;
				}
				
				var twc:TweenController = new TweenController(start, end, Modifiers.POSITION_TYPE);
				putBackFrac = 0;
				
				Tweener.addTween(this, {putBackFrac:1.0, time:putBackSecs, delay:0, transition:"linear", onUpdate:updatePutBackTween, onUpdateParams:[twc]});
			}
			
		}

		private function updatePutBackTween(twc:TweenController):void
		{
			this.locationXYR = twc.computeTween(putBackFrac);
			//trace("tweening location", putBackFrac, this.locationXYR.toString());
		}
		
		public function get isAnimationPath():Boolean
		{
			// PathObject and Line override this. And check pathOn
			return false;
		}
		
		public function pathPointForFraction(frac:Number):Point
		{
			return null;
		}
		
		/**
		 * Find the point on this path nearest to "local" point.
		 * @param local
		 * @param outputPoint
		 * @return path "fraction" 0 ... 1.
		 * 
		 */
		public function closestPointOnPath(local:Point, outputPoint:Point=null):Number
		{
			return 0
		}
		
		public function moveToward(local:Point, frac:Number, outputPoint:Point=null):Number
		{
			return 0;
		}
		
		private function snapTowardPoint(xx:Number, yy:Number):void
		{
			var view:ObjectView = getView();
			if (view && !this.dragging && !this.draggingHandle) {
				var global:Point = view.parent.localToGlobal(new Point(xx,yy));
				snapTo.moveObjAlongPath(this, global);
			}
		}
		
		public function moveObjAlongPath(obj:AbstractObject, global:Point):void
		{
			// "this" would be animation path, "obj" is on the path
			if (isAnimationPath) {
				var view:ObjectView = getView();
				if (view) {
					var local:Point = view.globalToLocal(global);  // obj loc, in path local coords
					var frac:Number;
					if(obj.snap) {
						frac = closestPointOnPath(local);
					} else {
						frac = moveToward(local, obj.pathPercent/100);
					}
                    var newPoint:Point = obj.snapPoint(frac);
                    // check to see if this snapping would move the object by an appreciable amount
                    //TODO make this check zoom/scale dependent
                    if(Math.abs(newPoint.x - obj.x) > 0.1 || Math.abs(newPoint.y - obj.y) > 0.1) {
                        obj.pathPercent = frac * 100;   // ObjectController will call snapObjectToPath()
                        obj.snapObjectToPath();  // must be done even if pathPercent didn't change (eg. moving path around by binding)
                    }
				}
			}
		}

        [Transient] private var snapping:Boolean = false;

        private function snapPoint(frac:Number):Point
        {
            if(okToSnapToPath) {
                var pos:Point = snapTo.pathPointForFraction(frac);
                var pathView:ObjectView = snapTo.getView();
                var objView:ObjectView = getView();
                if (pos && pathView && objView) {
                    var global:Point = pathView.localToGlobal(pos);
                    var local:Point = objView.parent.globalToLocal(global);
                    return local;
                }
            }
            return new Point(_x, _y);
        }
				
		public function snapObjectToPath():void
		{
			// snap "this" to path specified by this.snapTo, based on this.pathPercent
			if (okToSnapToPath) {
                var local:Point = snapPoint(pathPercent/100);
                snapping = true;
					
                if (local.x != _x || local.y != _y) {
                    var oldXYR:PositionAndAngle = locationXYR;
                    var oldX:Number = _x;
                    var oldY:Number = _y;
                    _x = local.x;
                    _y = local.y;
                    dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "x", oldX, _x));
                    dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "y", oldY, _y));
                    dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "locationXYR", oldXYR, locationXYR));
                }
                snapping = false;
			}
		}
		
		public function resnapAll(nearest:Boolean=false):void
		{
			if (isAnimationPath && _wireController.bindingsEnabled) {
                var scope:AbstractContainer = this.wireScopeContainer;  // may be null during project decoding
                if (scope) {
                    var objs:Array = GadgetEncoder.collectChildren(scope, null, true, false);
                    for each (var obj:AbstractObject in objs) {
                        if(obj && obj.snapTo == this) {
                            if (nearest) {
                                obj.snapTowardPoint(obj.x, obj.y);
                            } else {
                                obj.snapObjectToPath();
                            }
                        }
                    }
                }
			}
		}

        [Transient]
        public function get referenceJSON():Object {
            return {
                type:"AbstractObjectReference",
                id:uid
            }
        }

        public function showAllObjects(value:Boolean):void {
            for each (var obj:AbstractObject in this.allObjects()) {
                if (obj is World || obj is IPage || obj is EventFlow) continue;
                obj.showAll = value;
            }
        }

        public function allObjects():Vector.<AbstractObject>
		{
			var ao:AbstractObject;
            var child:AbstractObject;
            var childModel:TaconiteModel;
			var objects:Vector.<AbstractObject> = new Vector.<AbstractObject>();
			objects.push(this);
			for each (ao in objects) {
				if(ao is AbstractContainer) {
					for each(child in AbstractContainer(ao).objects) {
						objects.push(child);
                    }
                }
                var lists:Vector.<IList> = ao.model.getAllValueModelLists();  // masterPages, etc
                for each (var list:IList in lists) {
                    for each(child in list) {
                        objects.push(child);
                    }
                }
                for each(var m:TaconiteModel in ao.model.getAllValueModels()) {
                    objects.push(m.value as AbstractObject);
                }
			}
			return objects;
		}

        /**
         * Form a reference to an object based on its position in the tree.
         * Such as 3,2,6
         * Meaning 3rd child of world is an arena, 2nd child of that arena is gadget, 6th child of
         * gadget is the desired object.
         * Real example:  eventFlow,pages,0,2,pages,4,1,pages,2
         * Except that the numbers are actually zero based.
         * Returns null if the object has been deleted.
         * Normally call this on the World.
         * @param obj
         * @return
         *
         */
        public function objectPathArray(obj:AbstractObject):Array
        {
    //            trace("objectPathArray")
            var name:String;
            var array:Array = [];
            while (obj != this) {
                var parent:AbstractObject = obj.parent;
                assert(parent);
                var n:int = -1;
                var container:AbstractContainer = parent as AbstractContainer;
                if (container) {
                    n = container.objects.getItemIndex(obj);
                }
                if (n >= 0) {
                    array.unshift(n);
                } else {
                    var ok:Boolean = false;
                    var listNames:Vector.<String> = parent.model.getAllValueListProperties();
                    for each (name in listNames) {
                        var list:IList = parent[name];
                        var index:int = list.getItemIndex(obj);
                        if (index >= 0) {
                            array.unshift(index);
                            array.unshift(name);  // eg. masterPages
                            ok = true;
                            break;
                        }
                    }
                    if (!ok) {
                        var propNames:Vector.<String> = parent.model.getAllValueProperties();
                        for each (name in propNames) {
                            if (parent[name] == obj) {
                                array.unshift(name);
                                ok = true;
                                break;
                            }
                        }
                    }
                    if (!ok) {
                        throw new Error("objectPathArray");  // could not find "obj" in any list
                    }
                }
                obj = parent;
            }
    //            trace("  path="+array);
            return array;
        }

        public function resolveObjectPathArray(path:Array):AbstractObject
        {
            if (path == null || path.length==0)
                return null;

    //            trace("resolveObjectPathArray path="+path.join(","));
            const ALL_DIGITS:RegExp = /\d+$/;
            var index:int = 0;
            var parent:AbstractObject = this;  // normally this is world
            while (index < path.length) {
                var n:int;
                var obj:AbstractObject;
                var thing:String = path[index++];
                if (thing.match(ALL_DIGITS)) {   // object index
                    n = int(thing);
                    if (n >= AbstractContainer(parent).objects.length) {
                        return null;
                    }
                    obj = AbstractContainer(parent).objects[n];
                } else if (thing in parent && parent[thing] is IList) {  // property name followed by index  (eg. pages,2)
                    n = int(path[index++]);
                    obj = parent[thing][n];
                } else if (thing in parent && parent[thing] is AbstractObject) {   // property name (eg. eventFlow)
                    obj = parent[thing] as AbstractObject;
                } else {
                    throw new Error("resolveObjectPathArray");
                    return null;
                }
                if (obj)
                    parent = obj;
                else
                    break;
            }
    //            trace("  from pathArray="+path.join(","), " obj="+obj);
            return obj;
        }


        public function resetObject(world:World):void {
            bindingsDisabled = true;
			var ao:AbstractObject;
			var allProjectObjects:Vector.<AbstractObject> =  this.allObjects();

			// make sure all parenting changes occur in a distinct order
			for each (ao in allProjectObjects) {
                ao.resetParent(world);
			}
			for each (ao in allProjectObjects) {
                ao.resetZIndex();
			}
            this.onRestoreInitial();
            bindingsDisabled = false;
            if(Application.running) {
                this.onResume();
            }
        }

        public function resetParent(world:World):void{
//          trace(">>Checking parent reset for", this.title);
            var curParent:AbstractContainer = this.parent as AbstractContainer;
            var newParent:AbstractContainer = getInitialParent(world);
            if(newParent != null && newParent != this && newParent != curParent) {
//              trace(">>    Setting parent from", curParent.title, curParent.uid, "to", newParent.title, newParent.uid);
                if(curParent != null) {
                    this.model.changeParent(newParent.model); // model changes directly preserve views
                } else {
                    newParent.objects.addItem(this);
                }
            }
        }

        public function getInitialParent(world:World):AbstractContainer {
            if("parent" in this.initialValues  && this.initialValues.parent != null && "id" in this.initialValues.parent) {
                return world.findObjByRef(this.initialValues.parent) as AbstractContainer;
            }
            return null;
        }

        public function resetZIndex():void{
//          trace(">>Checking Z reset for", this.title);
            var curParent:AbstractContainer = this.parent as AbstractContainer;
            // no initial parent, or currently at the initial parent
            // i.e. don't set zIndex if the index belongs to a parent we didn't find
            if("zIndex" in this.initialValues && curParent != null && (!("parent" in this.initialValues && this.initialValues.parent != null && "id" in this.initialValues.parent) || this.initialValues.parent.id == this.parent.uid)) {
                var curIndex:int = curParent.objects.getItemIndex(this);
//              trace(">>  Setting Z from", curIndex, "to", ao.initialValues.zIndex);
                if(curIndex != this.initialValues.zIndex && this.initialValues.zIndex >= 0) {
                    // zIndex in initial values should be kept up to date
                    // this shouldn't be necessary, but is inserted as a hack
                    // fix before launch to make sure things don't break.
                    // if zIndex is out of bounds that means we may be screwing up zOrder
                    var newIndex:int = Math.min(this.initialValues.zIndex, curParent.children.length-1);
                    curParent.model.setChildIndex(this, newIndex); // model changes directly preserve views
                }
            }
        }

        public function get selected():Boolean
        {
            var doc:Document = Application.instance.document;
            if (doc == null || doc.selection == null)
                return false;

            return doc.selection.includes(model);
        }

        public function getUsageCounts(usages:UsageCounts):void
        {
            getUsageCountsInSettersAndTriggers(usages);
        }

        protected function getUsageCountsInSettersAndTriggers(usages:UsageCounts):void
        {
            for each(var thing:Object in anchors) {
                var anchor:WireAnchor = thing as WireAnchor;
                if (anchor) {
                    anchor.getUsageCounts(usages);
                }
            }
        }


        private function assert(condition:*):void
        {
            if (condition==null || condition==false)
                throw new Error("AbstractObject");
        }


        public function dumpInits():void
        {
            trace("INITS:",this);
            for (var property:String in initialValues) {
                var val:* = initialValues[property];
                trace("  "+property + "=" + val);
            }
            trace(" ");
        }


	}
}
