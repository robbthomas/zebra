package com.alleni.author.model
{
	import com.alleni.taconite.lang.TaconiteTimer;

	public class BorealToken
	{
		public var content:String;
		private var _timeout:Number; // in milliseconds
		private var _timeStamp:Number; // in milliseconds
		
		public function BorealToken(content:String, timeout:Number=-1)
		{
			this.content = content;
			_timeout = timeout;
			_timeStamp = TaconiteTimer.instance.milliseconds;
		}
		
		public function get valid():Boolean
		{
			return _timeout > 0 &&  (TaconiteTimer.instance.milliseconds - _timeStamp) < _timeout;
		}
	}
}