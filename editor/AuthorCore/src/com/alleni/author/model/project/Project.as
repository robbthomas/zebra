package com.alleni.author.model.project {
import com.alleni.author.controller.ui.GadgetController;
import com.alleni.author.definition.AssetType;
import com.alleni.author.definition.GadgetDescription;
import com.alleni.author.definition.GadgetType;
import com.alleni.author.definition.Modifiers;
import com.alleni.author.model.World;
import com.alleni.author.model.gadgets.GadgetIconReference;
import com.alleni.author.model.gadgets.GadgetLicense;
import com.alleni.author.model.gadgets.GadgetScreenshotReference;
import com.alleni.author.model.gadgets.GadgetState;
import com.alleni.author.model.objects.IListable;
import com.alleni.author.model.test.TestPanel;
import com.alleni.author.model.ui.Application;
import com.alleni.author.model.ui.Asset;
import com.alleni.author.model.ui.ExternalContentObject;
import com.alleni.author.model.ui.IReplaceable;
import com.alleni.author.persistence.GadgetDecoder;
import com.alleni.author.persistence.GadgetUtils;
import com.alleni.author.service.gadgets.GadgetIconOperation;
import com.alleni.author.service.gadgets.GadgetScreenshotsOperation;
import com.alleni.taconite.persistence.IAtom;

import flash.display.DisplayObject;

public class Project extends ExternalContentObject implements IListable, IReplaceable, IAtom {
    private var _nameFromDatabase:String;
    private var _publishedNameFromDatabase:String;

    [Bindable] public var projectId:String;
    public var published:Boolean;
    public var localIcon:DisplayObject;
    public var width:Number;
    public var height:Number;
    public var description:String;
    [Bindable] public var screenshots:GadgetScreenshotReference;
    [Bindable] public var icon:GadgetIconReference;
    public var initialValues:Object = null;
    public var projectTypeId:int;
    public var state:GadgetState;
    public var permissions:UserAccessPrivs = UserAccessPrivs.makeDefault();
    public var license:GadgetLicense = new GadgetLicense();
    public var categoryId:int;
    public var price:int;
    public var selfPrice:int;
    public var minPrice:int;
    public var hideInStoreList:Boolean;
    public var inviteOnly:Boolean;
    public var watermarkPosition:String; //TODO pull from / save to server from metadata
    public var autoSave:Boolean;
    public var accountId:String;
    public var accountTypeId:int;

    public var accountType:String; //TODO pull from / save to server?

    // only sent to server
    public var copyIconFromProjectId:String;
    public var copyScreenshotsFromProjectId:String;
    public var copyContentFromProjectId:String;
    public var gadgets:Array;


    // only pulled from server
    public var averageRating:int;
    public var numRatings:int;
    public var authorFullName:String;
    public var accountName:String;
    public var createdDateTime:Number;
    public var apnUUID:String;


    private var _scripts:TestPanel;
    public var metadataUpToDate:Boolean;
    [Bindable] public var pendingIconOperation:GadgetIconOperation;
    [Bindable] public var pendingScreenshotOperation:GadgetScreenshotsOperation;

    public var previewImage:Asset;
    // data model for script system

    public function get scripts():TestPanel
    {
        return _scripts;
    }
    public function set scripts(value:TestPanel):void
    {
        _scripts = value;
    }

    public var children:Vector.<Project>;
    public var assets:Vector.<Asset>;

    //non serialized fields

    public function Project(id:String = null) {
        this.projectId = id;
    }

    public function get referenceJSON():Object {
        return {
            type:"ProjectReference",
            id:projectId
        }
    }

    public function toJson():Object {
        var result:Object = {
            projectId: projectId,
            projectTypeId: projectTypeId,
            projectName: name,
            published: published,
            publishedName: publishedName,
            width: width,
            height: height,
            description: description,
            content: content,
            initialValues: initialValues,
            state: state?state.toJson():null,
            gadgets: gadgets,
            license: license?license.toJson():new GadgetLicense().toJson(),
            categoryId:categoryId,
            price:price,
            hideInStoreList:hideInStoreList,
            inviteOnly:inviteOnly,
            autoSave:autoSave,
            ignoreDuplicateName:nameIsFromDatabase,
            ignoreDuplicatePublishedName:publishedNameIsFromDatabase,
            copyContentFromProjectId:copyContentFromProjectId,
            copyIconFromProjectId:copyIconFromProjectId,
            copyScreenshotsFromProjectId:copyScreenshotsFromProjectId,
            accountTypeId: accountTypeId,
            watermarkPosition: watermarkPosition,
            oldName:_nameFromDatabase
        };

        return result;
    }

    public static function fromJson(json:Object, existing:Project=null):Project {
        var result:Project = existing;
        if(result == null) {
            GadgetController.instance.model.getById(json.id);
        }
        if(result == null) {
            result = new Project();
        }
        result.projectId = json.projectId;
        result.name = json.projectName;
        result.projectTypeId = json.projectTypeId;
        result.published = json.published;
        result.publishedName = json.publishedName;
        result.width = json.width;
        result.height = json.height;
        result.description = json.description;
        result.icon = GadgetIconReference.fromJson(json.icon);
        result.screenshots = GadgetScreenshotReference.fromJson(json.screenshots);
        result.permissions = UserAccessPrivs.fromJson(json.permissions);
        result.license = GadgetLicense.fromJson(json.license);
        result.categoryId = json.categoryId;
        result.price = json.price;
        result.selfPrice = json.selfPrice;
        result.minPrice = json.minPrice;
        result.hideInStoreList = json.hideInStoreList;
        result.description = json.description;
        result.inviteOnly = json.inviteOnly;
        result.autoSave = json.autoSave;

        result.averageRating = json.averageRating;
        result.numRatings = json.numRatings;
        result.authorFullName = json.authorDisplayName;
        result.accountName = json.accountName;
        result.createdDateTime = json.createdDateTime;
        result.apnUUID = json.apnUUID;
        result.accountId = json.accountId;
        result.accountTypeId = json.accountTypeId;
        //result.watermarkPosition = json.watermarkPosition;

        if(json.hasOwnProperty("content") && json.content != null) {
            // content is optional
            result.content = json.content;
            if(json.content.hasOwnProperty("watermarkPosition") && json.content.watermarkPosition != null) {
                result.watermarkPosition = json.content.watermarkPosition;
            }
            if(json.hasOwnProperty("initialValues") && json.initialValues != null) {
                // initialValues is optional but only allowed if content is full
                result.initialValues = json.initialValues;
            }
        }
        if(json.hasOwnProperty("state") && json.state != null) {
            result.state = GadgetState.fromJson(json.state);
        }
        if(json.hasOwnProperty("previewImage") && json.previewImage != null) {
            var existingAsset:Asset = null;
            if(result.previewImage && result.previewImage.assetID == null) {
                existingAsset = result.previewImage;
            }
            result.previewImage = Asset.fromJson(json.previewImage, -1, true, existingAsset);
        }
        if(json.hasOwnProperty("children") && json.children != null) {
            result.children = fromJsonList(json.children);
        }
        if(json.hasOwnProperty("assets") && json.assets != null) {
            result.assets = Asset.fromJsonList(json.assets);
        }
        return result;
    }

    public static function fromJsonList(json:Object):Vector.<Project> {
        var result:Vector.<Project> = new Vector.<Project>();
        for each(var projectJson:Object in json) {
            var project:Project = Project.fromJson(projectJson);
            result.push(project);
        }
        return result;
    }

    public function clone():Project {
        var result:Project = new Project();
        result.copyIconFromProjectId  = projectId;
        result.copyScreenshotsFromProjectId = projectId;
        result.projectId = null;
        result.name = name;
        result.projectTypeId = projectTypeId;
        result._nameFromDatabase = _nameFromDatabase;
        result._publishedNameFromDatabase = _publishedNameFromDatabase;
        result.published = published;
        result.publishedName = publishedName;
        result.width = width;
        result.height = height;
        result.description = description;
        result.icon = icon;
        result.screenshots = screenshots;
        result.permissions = permissions;
        result.license = license;         // TODO: certain structures may need to be cloned
        result.categoryId = categoryId;
        result.price = price;
        result.selfPrice = selfPrice;
        result.minPrice = minPrice;
        result.hideInStoreList = hideInStoreList;
        result.description = description;
        result.inviteOnly = inviteOnly;
        result.autoSave = autoSave;
        result.averageRating = averageRating;
        result.numRatings = numRatings;
        result.authorFullName = authorFullName;
        result.accountName = accountName;
        result.createdDateTime = createdDateTime;
        result.apnUUID = apnUUID;
        result.accountTypeId = accountTypeId;
        result.watermarkPosition = watermarkPosition;
        return result;
    }

    public function get id():String {
        return projectId;
    }

    public function get type():int {
        return AssetType.GADGET;
    }

    [Transient]
  	public function get propertyType():String {return Modifiers.GADGET_TYPE;}

    public function setEmptyContent():void
    {
        content = {
            children: [],
            edgeAnchors: [],
            wiring: []
        };
        initialValues = {values:{}};
    }

    public static function empty():Project
    {
        var result:Project = new Project();
        result.setEmptyContent();
        return result;
    }

    public function get estimateBuildoutCount():int {
        return GadgetDecoder.estimateBuildoutCount(content);
    }

    [Deprecated]
    public function copyFromPublished(published:Project):void {

        if (projectTypeId == GadgetType.GENERIC) // only allow published name to override gadget name for generics.. not projects
            name = GadgetDescription.cleanName(this);
        permissions = published.permissions;
        hideInStoreList = published.hideInStoreList;
//        publishedGadgetId = published.publishedGadgetId?published.publishedGadgetId:published.permanentID;
        publishedName = published.publishedName;
        _publishedNameFromDatabase = published._publishedNameFromDatabase;
//        currencyTypeId = published.currencyTypeId;
        projectTypeId = published.projectTypeId;
        license = published.license;
        icon = published.icon;
        screenshots = published.screenshots;
        description = published.description;
        categoryId = published.categoryId;
        price = published.price;
    }

    public function get nameIsFromDatabase():Boolean {
        return _nameFromDatabase == name;
    }

    public function set nameIsFromDatabase(value:Boolean):void {
        if(value) {
            _nameFromDatabase = name;
        } else {
            _nameFromDatabase = null;
        }
    }

    public function get publishedNameIsFromDatabase():Boolean {
        return _publishedNameFromDatabase == publishedName;
    }

    public function set publishedNameIsFromDatabase(value:Boolean):void {
        if(value) {
            _publishedNameFromDatabase = publishedName;
        } else {
            _publishedNameFromDatabase = null;
        }
    }

    override public function toString():String
    {
        var childCount:String = "";
        if (content && content.hasOwnProperty("children") && content.children.hasOwnProperty("length"))
            childCount = content.children.length.toString();
        return "[Project name="+name+" content=" + (this.content != null) + " : childCount="+childCount + " : ID=" + this.projectId + "]";
    }

    public function debugContent(properties:String=null):Object {
        return {
            projectId:projectId,
            content:GadgetUtils.createDebugValuesContent(content, properties),
            initialValues:GadgetUtils.createDebugValuesContent(initialValues, properties)
        };
    }
}
}
