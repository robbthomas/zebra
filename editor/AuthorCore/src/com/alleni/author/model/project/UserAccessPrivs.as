package com.alleni.author.model.project {
public class UserAccessPrivs {
    public var owned:Boolean;

    public var canRead:Boolean;
    public var canEdit:Boolean;
    public var canRepublish:Boolean;
    public var canEmbed:Boolean;
    public var canShare:Boolean;

    public function UserAccessPrivs() {
    }

    public static function fromJson(json:Object):UserAccessPrivs {
        var result:UserAccessPrivs = new UserAccessPrivs();
        if(json){
            result.canRead = json.read;
            result.canShare = json.share;
            result.canEdit = json.edit;
            result.canRepublish = json.republish;
            result.canEmbed = json.embed;
            result.owned = json.owned;
        }
        return result;
    }

    public static function makeDefault():UserAccessPrivs {
        var result:UserAccessPrivs = new UserAccessPrivs();
        result.canRead = true;
        result.canShare = false;
        result.canEdit = true;
        result.canRepublish = false;
        result.canEmbed = false;

        result.owned = true;
        return result;
    }
}
}
