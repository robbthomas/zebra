/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.author.model
{
    import com.alleni.author.controller.app.ProjectController;
    import com.alleni.author.model.objects.AbstractContainer;
    import com.alleni.author.model.objects.EventFlow;
    import com.alleni.author.model.objects.LMS;
    import com.alleni.author.model.objects.ProjectObject;

    import mx.events.PropertyChangeEvent;

    /**
     * Value object representing a set of objects in a 2-dimensional world.
     */
    public class World extends AbstractContainer
    {
		
		[Transient]
		public static const TACONITE_CHILDREN_PROPERTY:String = "objects";

		[Transient]
		public static const TACONITE_MODEL_PROPERTIES:Vector.<String> = Vector.<String>(["project","lms","eventFlow"]);

		[Transient]
		public static const BASE_SERIAL:uint = 100;
		
		public var currentSerial:uint = BASE_SERIAL;
		
		[Bindable]
		public var diagramWidth:Number = 800;
		
		[Bindable]
		public var diagramHeight:Number = 600;
		
		public var autosaveMinutes:Number = ProjectController.AUTOSAVE_DEFAULT_MINUTES;

        private var _project:ProjectObject = null;

        private var _lms:LMS = null;

        private var _eventFlow:EventFlow = null;

        [Bindable]
        [Transient]
        public function get project():ProjectObject {
            return _project;
        }

        public function set project(value:ProjectObject):void {
            _project = value;
        }

        [Bindable]
        [Transient]
        public function get lms():LMS {
            return _lms;
        }

        public function set lms(value:LMS):void {
            _lms = value;
        }

        [Bindable]
        [Transient]
        public function get eventFlow():EventFlow {
            return _eventFlow;
        }

        public function set eventFlow(value:EventFlow):void {
            _eventFlow = value;
        }

        override public function set width(value:Number):void {
            // make sure the project object registers the new value
            var oldValue:Number = width;
            super.width = value;
            if(project) {
                project.dispatchEvent(PropertyChangeEvent.createUpdateEvent(project,  "stageWidth", oldValue,  width));
            }
        }

        override public function set height(value:Number):void {
            // make sure the project object registers the new value
            var oldValue:Number = height;
            super.height = value;
            if(project) {
                project.dispatchEvent(PropertyChangeEvent.createUpdateEvent(project,  "stageHeight", oldValue,  height));
            }
        }

        public function World()
		{
			super(false);  // setupProperties=false, to prevent AbstractObject from setting up properties

			width = 800;
			height = 500;
			title = "World";
			shortClassName = "World";
		}

        [Transient]
        override public function get wireScopeContainer():AbstractContainer {
            return null;
        }
    }
}
