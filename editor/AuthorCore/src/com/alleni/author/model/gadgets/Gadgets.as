package com.alleni.author.model.gadgets
{
import com.alleni.author.model.project.Project;
import com.alleni.taconite.model.ITaconiteCollection;
	import com.alleni.taconite.model.TaconiteObject;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;

	public class Gadgets extends TaconiteObject implements ITaconiteCollection
	{	
		[Transient]
		[Bindable]
		public var gadgets:IList = new ArrayCollection();
		
		public function Gadgets()
		{	
			super();
		}
				
		[Transient]
		public function get children():IList
		{
			return gadgets;
		}
		
		public function getById(projectId:String):Project
		{
			if (projectId == null)
				return null;
			
			for each(var testGadget:Project in gadgets){
				if(testGadget.projectId == projectId){
					return testGadget;
				}
			}
			return null;
		}
	}
}