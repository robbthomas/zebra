/**
 * Created by IntelliJ IDEA.
 * User: robertthomas
 * Date: 6/24/11
 * Time: 10:01 AM
 * To change this template use File | Settings | File Templates.
 */
package com.alleni.author.model.gadgets {
	import flash.display.BitmapData;
	import flash.utils.Dictionary;

    public class GadgetScreenshot {

        private var _screenShotId:String;
        private var _screenshotDBID:String;
        private var _urls:Dictionary;
        private var _imageBitmapData:BitmapData;
        private var _onServer:Boolean;
        private var _visibleIndex:Number = -1;
        private var _largestSize:Number = -1;

        public function GadgetScreenshot(bitmapData:BitmapData, onServer:Boolean, id:String = null) {
            if(bitmapData != null){
                _imageBitmapData = bitmapData.clone();
            }else{
                _imageBitmapData = null;
            }
            _onServer = onServer;
            _screenShotId = id;
            _urls = new Dictionary();
        }

        static public function fromJson(json:Object = null, existing:GadgetScreenshot = null):GadgetScreenshot{
            var result:GadgetScreenshot = existing;
            result.visibleIndex = json.displayOrder;
            result.screenshotDBID = json.publishedImageId;
            var j:int;
            for(j=0; j<json.sizes.length; j++){
                result.addImageUrl((json.sizes[j].toString()), (json.baseUrl as String), (json.fileNameExt as String));
            }

            return result;
        }

        // Getters and Setters
        public function get screenShotId():String {
            return _screenShotId;
        }
        public function set screenShotId(value:String):void {
            _screenShotId = value;
        }

        public function addImageUrl(size:String, url:String, ext:String):void{
            _urls[size] = url+size+ext;
            var sizeNum:Number = Number(size);
            if(sizeNum > _largestSize) {
                _largestSize = sizeNum;
            }
        }
        public function getImageUrl(size:String):String{
            return _urls[size];
        }
        public function getLargestImageUrl():String {
            return _urls[String(_largestSize)];
        }

        public function get imageBitmapData():BitmapData {
            return _imageBitmapData.clone();
        }
        public function set imageBitmapData(value:BitmapData):void {
            _imageBitmapData = value;
        }

        public function get onServer():Boolean {
            return _onServer;
        }
        public function set onServer(value:Boolean):void {
            _onServer = value;
        }

        public function get visibleIndex():Number {
            return _visibleIndex;
        }
        public function set visibleIndex(value:Number):void {
            _visibleIndex = value;
        }

        public function get screenshotDBID():String {
            return _screenshotDBID;
        }
        public function set screenshotDBID(value:String):void {
            _screenshotDBID = value;
        }
    }
}
