package com.alleni.author.model.gadgets
{
	public class GadgetState
	{
		public var id:String;

		public var initialValues:Object;
		public var running:Boolean;
		public var locked:Boolean;
		public var scripts:Object;
        public var gadgets:Array;
        public var assets:Array;
		
		public static function fromJson(json:Object):GadgetState
		{
			var result:GadgetState = new GadgetState();
			if ("id" in json) result.id = json.id;
			if ("content" in json) {
				if ("initialValues" in json.content) result.initialValues = json.content.initialValues;
				if ("running" in json.content) result.running = json.content.running;
				if ("locked" in json.content) result.locked = json.content.locked;
				if ("scripts" in json.content) result.scripts = json.content.scripts;
				if ("gadgets" in json.content) result.gadgets = json.content.gadgets;
				if ("assets" in json.content) result.assets = json.content.assets;
			}
			return result;
		}

        public function toJson():Object {
           return  {
               id: id,
               content: {
                   initialValues:initialValues,
                   running: running,
                   locked: locked,
                   scripts: scripts,
                   gadgets: gadgets,
                   assets: assets
               }
           }
        }
	}
}
