package com.alleni.author.model.gadgets
{
	public class GadgetLicense
	{
		public var id:int;
		public var projectTypeId:int;
		public var edit:Boolean;
		public var republish:Boolean;
		public var embed:Boolean;
		
		public function GadgetLicense()
		{
		}
		
		public static function fromJson(json:Object):GadgetLicense
		{
			var result:GadgetLicense = new GadgetLicense();

            if(json){
                if(json.id){
                    result.id = json.id;
                }
                if(json.projectTypeId){
                    result.projectTypeId = json.projectTypeId;
                }
                if(json.edit){
                    result.edit = json.edit;
                }
                if(json.republish){
                    result.republish = json.republish;
                }
                if(json.embed){
                    result.embed = json.embed;
                }
            }
			
			return result;
		}

        public function toJson():Object {
            return {
                projectTypeId: projectTypeId,
                edit: edit,
                republish: republish,
                embed: embed
            }
        }
		
		public function toString():String
		{
			return "[GadgetLicense ID=" + id + " EDIT=" + edit + " EMBED=" + embed + " REPUBLISH=" + republish + "]";
		}
	}
}