package com.alleni.author.model.gadgets
{
	public class GadgetIconReference
	{
		public static var DEFAULT_BASE_URL:String = "http://static.zebrazapps.com/icons/genericIcon";
		public static var DEFAULT_EXTENSION:String = ".png";
		public static var SMALL_SIZE:String = "50";
		public static var MEDIUM_SIZE:String = "75";
		public static var LARGE_SIZE:String = "100";
		public static var FULL_SIZE:String = "180";

		public var smallURL:String;
		public var mediumURL:String;
		public var largeURL:String;
		public var fullURL:String;
        public var publishedImageId:String;
		
		public function GadgetIconReference()
		{
		}
		
		public static function fromJson(json:Object, existing:GadgetIconReference=null):GadgetIconReference
		{
			if(json == null){
                return null;
            }
			/* JSON for reference
			"icons":[
				{
					"sizes":[
						18,
						50,
						400
					],
					"baseUrl":"http://com.alleni.zebra.site.content.s3.amazonaws.com/icons/4420745c-0427-4c22-a0d6-9723bd2872ed-",
					"fileNameExt":".png",
					"publishedGadgetId":8014,
					"displayOrder":1,
					"publishedImageId":23005,
					"caption":null,
					"label":null,
					"screenshotFileId":null,
					"renderingComplete":true,
					"iconFileId":"4420745c-0427-4c22-a0d6-9723bd2872ed"
				}
			]
			*/
			var result:GadgetIconReference = existing?existing:new GadgetIconReference();
			if (json as Array && json.length > 0) json = json[0];
			
			var baseUrl:String;
			var sizes:Array = [SMALL_SIZE, MEDIUM_SIZE, LARGE_SIZE, FULL_SIZE]; // the back-end will give us these when an issue is resolved. PJK 7JUL11
			var fileNameExt:String;
			
			if ("baseUrl" in json
				&& "fileNameExt" in json
				&& "sizes" in json) { // json has a valid icon
				
				baseUrl = json.baseUrl;
				fileNameExt = json.fileNameExt;
			} else { // no valid icon, resort to default
				baseUrl = DEFAULT_BASE_URL;
				fileNameExt = DEFAULT_EXTENSION;
			}
			result.smallURL = baseUrl + SMALL_SIZE + fileNameExt;
			result.mediumURL = baseUrl + MEDIUM_SIZE + fileNameExt;
			result.largeURL = baseUrl + LARGE_SIZE + fileNameExt;
			result.fullURL = baseUrl + FULL_SIZE + fileNameExt;
            result.publishedImageId = json.publishedImageId;
				
			return result;
		}
	}
}
