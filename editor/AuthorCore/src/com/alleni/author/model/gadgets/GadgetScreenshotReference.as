package com.alleni.author.model.gadgets
{
	import flash.utils.Dictionary;
	
	public class GadgetScreenshotReference
	{
		private var _screenShots:Dictionary;
		
		public function GadgetScreenshotReference()
		{
			_screenShots = new Dictionary();
		}

        public function addScreenShot(id:String):GadgetScreenshot
		{
            var result:GadgetScreenshot = new GadgetScreenshot(null, true, id)
            _screenShots[id] = result;
            return result;
        }

		public static function fromJson(json:Object, existing:GadgetScreenshotReference=null):GadgetScreenshotReference
		{
            var result:GadgetScreenshotReference = existing?existing:new GadgetScreenshotReference();
            if(json is Array){
                if((json as Array).length == 0){
                    return null;
                }
                var i:int;
                for(i=0; i<json.length; i++){
                    var screenShot:GadgetScreenshot = result.addScreenShot(json[i].screenshotFileId);
                    screenShot.visibleIndex = json[i].displayOrder;
                    screenShot.screenshotDBID = json[i].publishedImageId;
                    var j:int;
                    for(j=0; j<json[i].sizes.length; j++){
                        screenShot.addImageUrl((json[i].sizes[j].toString()), (json[i].baseUrl as String), (json[i].fileNameExt as String));
                    }
                }
            }
			return result;
		}


        //Getters and Setters
        public function get screenShots():Dictionary
		{
            return _screenShots;
        }

        public function set screenShots(value:Dictionary):void
		{
            _screenShots = value;
        }
    }
}
