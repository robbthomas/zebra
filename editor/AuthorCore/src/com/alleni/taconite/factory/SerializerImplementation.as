package com.alleni.taconite.factory
{
import com.alleni.author.Navigation.SmartObject;
import com.alleni.author.controller.ObjectController;
import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.controller.ui.AssetController;
	import com.alleni.author.definition.IModifier;
	import com.alleni.author.definition.InletDescription;
	import com.alleni.author.definition.Modifiers;
	import com.alleni.author.definition.OutletDescription;
	import com.alleni.author.definition.PositionAndAngle;
	import com.alleni.author.definition.PropertyDescription;
import com.alleni.author.definition.VariableStore;
import com.alleni.author.definition.text.FormattedText;
	import com.alleni.author.model.AbstractObject;
	import com.alleni.author.model.GraphicFill;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.AbstractExternalObject;
import com.alleni.author.model.objects.ArenaPage;
import com.alleni.author.model.objects.MediaObject;
	import com.alleni.author.model.objects.Path;
	import com.alleni.author.model.objects.PathNodeInfo;
	import com.alleni.author.model.objects.PathObject;
	import com.alleni.author.model.objects.PushButton;
	import com.alleni.author.model.objects.TextObject;
	import com.alleni.author.model.objects.ToggleButtonGroup;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.Asset;
	import com.alleni.author.model.ui.CalcPropertyWireAnchor;
	import com.alleni.author.model.ui.ExternalSmartWireAnchor;
	import com.alleni.author.model.ui.InternalSmartWireAnchor;
	import com.alleni.author.model.ui.PathNodeWireAnchor;
	import com.alleni.author.model.ui.SetterWireAnchor;
	import com.alleni.author.model.ui.StyleWireAnchor;
	import com.alleni.author.model.ui.TriggerWireAnchor;
import com.alleni.author.model.ui.UsageCounts;
import com.alleni.author.model.ui.VariableContainer;
import com.alleni.author.model.ui.VariableWireAnchor;
	import com.alleni.author.model.ui.Wire;
	import com.alleni.author.model.ui.WireAnchor;
	import com.alleni.author.model.ui.Wiring;
	import com.alleni.author.util.CustomPoint;
	import com.alleni.author.util.PresetPoint;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.persistence.json.JSON;
	import com.alleni.taconite.service.LogService;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	
	import mx.collections.IList;
	import mx.utils.ObjectUtil;
	
	public class SerializerImplementation
	{
		public function SerializerImplementation()
		{
		}

		public function serializeWire(wire:Wire, ids:Dictionary = null):Object
		{
			var masterId:Object = wire.masterAnchor.hostObject.uid;
			if (ids && wire.masterAnchor.hostObject in ids)
				masterId = ids[wire.masterAnchor.hostObject];
			if (wire.attached) {
				var slaveId:Object = wire.slaveAnchor.hostObject.uid;
				if (ids && wire.slaveAnchor.hostObject in ids)
					slaveId = ids[wire.slaveAnchor.hostObject];
				return {
					id:(masterId as Array || slaveId as Array)?null:wire.uid,
					masterAnchor: {
						id: masterId,
						modifier: wire.masterAnchor.path
					},
					slaveAnchor:{
						id: slaveId,
						modifier: wire.slaveAnchor.path
					}
				};
			} else {
				return {
					id:(masterId as Array)?null:wire.uid,
					masterAnchor: {
						id: masterId,
						modifier: wire.masterAnchor.path
					}
				};
			}
		}

		public static function findAnchor(json:Object, objects:Dictionary):WireAnchor
		{
			var obj:AbstractObject;
			if (objects && json.id as Array) {
				obj = objects[json.id.join(",")] as AbstractObject;
			} else {
				obj = (Application.instance.document.root.value as World).findObjByUID(json.id);
			}
			if(obj == null) {
				return null;
			}

			var result:WireAnchor = WireAnchor.findByPath(obj, json.modifier);
			if(result != null)
				return result;

			var key:String = json.modifier;
			var path:Array = key.split("/");

			if (key.substr(0,9) == "internal-") {
				return SmartObject(obj).innerAnchors[key];
			} else if (path.length > 1) {
				// meta anchors
				var parent:WireAnchor = ObjectController.instance.findAnchorForObj(obj, new InletDescription(path[0]));
				for each(var child:WireAnchor in parent.childAnchors) {
					if(child.host as WireAnchor && child.modifierDescription.key == path[1]) {
						return child;
					}
					if(child.modifierDescription.key == path[1]) {
						return child;
					}
				}
				return null;
			} else {
				var split:Object = WireAnchor.pathRegExp.exec(key);
				var index:Number = ("index" in json)?json.index:-1;
				if(split && split.length > 2) {
					key = split[1];
					index = split[2];
				}
				var parts:Array = key.split(";");
				key = parts.join(";");
				var modifier:IModifier = obj.modifiers[key];

				return ObjectController.instance.findAnchorForObj(obj, modifier, index);
			}
		}
		
	
		public function deserializeWire(json:Object, parent:Wiring, index:int, objects:Dictionary = null):TaconiteModel
		{
			var masterAnchor:WireAnchor = findAnchor(json.masterAnchor, objects);

			if(masterAnchor == null) {
				LogService.error("Unable to deserialize master wire end"+com.alleni.taconite.persistence.json.JSON.encode(json.masterAnchor));
				return null;
			}
	
			if ("slaveAnchor" in json) {
				var slaveAnchor:WireAnchor = findAnchor(json.slaveAnchor, objects);
				
				if(slaveAnchor == null) {
                    LogService.error("Unable to deserialize slave wire end"+com.alleni.taconite.persistence.json.JSON.encode(json.slaveAnchor));
					return null;
				}

				if(masterAnchor.wiringScope != slaveAnchor.wiringScope) {
                    LogService.debug("Unable to deserialize wire. Endpoints are not in same wire scope. "+com.alleni.taconite.persistence.json.JSON.encode(json));
					return null;
				}

				var wire:Wire = new Wire(masterAnchor, slaveAnchor);
				if (json.id)
					wire.uid = json.id;
				if (index < 0)
					parent.wires.addItem(wire);
				else
					parent.wires.addItemAt(wire, index);
				return wire.model;
			} else {
				var unattached:Wire = new Wire(masterAnchor);
				if (json.id)
					unattached.uid = json.id;
				if (index < 0)
					parent.wires.addItem(unattached);
				else
					parent.wires.addItemAt(unattached, index);
				return unattached.model;
			}
			return null;
		}
	
		private function serializeModifier(modifier:IModifier):Object
		{
			var result:Object = {label:modifier.label, description:modifier.description, category: modifier.category, weight: modifier.weight};
			if (modifier as InletDescription)
				result.type = "inlet";
			else if (modifier as OutletDescription)
				result.type = "outlet";
			else if (modifier as PropertyDescription) {
				result.type = "property";
				result.valueType = PropertyDescription(modifier).type;
				result.valueConstraints = PropertyDescription(modifier).constraints;
				result.writable = PropertyDescription(modifier).writeable;
			} else {
				throw new Error("Unknown modifier type for serialization, " + modifier);
			}
			return result;
		}
	
		private function deserializeModifier(json:Object, key:String):IModifier
		{
			switch (json.type) {
				case "inlet":
					return new InletDescription(key, "", json.label, json.description, json.category, json.weight);
					break;
				case "outlet":
					return new OutletDescription(key, "", json.label, json.description, json.category, json.weight);
					break;
				case "property":
					return new PropertyDescription(key, "", json.label, json.description, json.category, json.weight, false, -1, json.valueType, deserializeValue(json.valueConstraints), json.writable);
					break;
				default:
					LogService.debug("SerializerImplementation::deserializeModifier was asked to deserialize an unhandled modifier type " + json.type);
					break;
			}
			throw new Error("Unknown modifier type for deserialization, " + json.type);
		}
	
		public static function serializeValue(obj:Object, prop:Object, objectIds:Dictionary = null):void
		{
			var value:* = obj[prop];
			if(value == null || value as Number || value as String || value as Boolean) {
				obj[prop] = value;
			} else if ("toJson" in value) {
				obj[prop] = value.toJson();
			} else if(value as AbstractObject) {
                var id:Object;
				if(objectIds != null && value in objectIds) {
					id = objectIds[value];
				} else {
					id = value.uid;
				}
                obj[prop] = {
                    type: "AbstractObjectReference",
                    id:id
                }
			} else {
				obj[prop] = com.alleni.taconite.persistence.json.JSON.decode(com.alleni.taconite.persistence.json.JSON.encode(value));
			}
		}
	
		public function serializeWireAnchor(a:WireAnchor, objectIds:Dictionary = null):Object
		{
			var result:Object;
			if (a == ApplicationController.instance.wireController.ghostAnchor)
				return {kind:"ghost"};
			else if (a.hostObject) {
				if (a as SetterWireAnchor) {
					var setterAnchor:SetterWireAnchor = a as SetterWireAnchor;
					var customModifier:String;
                    if (setterAnchor && setterAnchor.customModifier)
                        customModifier = setterAnchor.customModifier.key;
					result = {kind:"setter", key: a.path,
						customModifier:customModifier,
						uid:a.uid
					};
					return result;
				} else if (a as TriggerWireAnchor) {
					var triggerAnchor:TriggerWireAnchor = a as TriggerWireAnchor;
					
					result = {kind:"trigger", key: a.path,
						uid:a.uid
					};
					serializeValue(result, "condition", objectIds);
					return result;
				} else if (a as CalcPropertyWireAnchor) {
					var calcPropertyAnchor:CalcPropertyWireAnchor = a as CalcPropertyWireAnchor;
					
					result = {kind:"calc", key: a.path,
						uid:a.uid
					};
					return result;
				} else if (a as VariableWireAnchor) {
					var variableAnchor:VariableWireAnchor = a as VariableWireAnchor;
					
					result = {kind:"var", key: a.path,
						variable:variableAnchor.hostProperty,
						uid:a.uid
					};
					return result;
				} else if (a as StyleWireAnchor) {
					
					return {kind:"style", key:a.path, uid:a.uid};
				} else if (a as ExternalSmartWireAnchor) {
					var externalSmartAnchor:ExternalSmartWireAnchor = a as ExternalSmartWireAnchor;
					
					result = {kind:"edge",
						internalID: externalSmartAnchor.other.internalID,
						externalID: externalSmartAnchor.externalID,
						label: a.label,
						labelChanged: a.labelChanged,
						modifier: serializeModifier(a.modifierDescription),
						wireboardSide: externalSmartAnchor.other.portSide,
						wireboardPosition: externalSmartAnchor.other.position,
						uid:a.uid
					};
                    return result;
				} else if (a as InternalSmartWireAnchor) {
					var internalSmartAnchor:InternalSmartWireAnchor = a as InternalSmartWireAnchor;
					
					return {kind:"edge-inner",
						internalID: internalSmartAnchor.internalID,
						externalID: internalSmartAnchor.other.externalID,
						label: a.label,
						modifier: serializeModifier(a.modifierDescription),
						wireboardSide: internalSmartAnchor.portSide,
						wireboardPosition: internalSmartAnchor.position,
						uid:a.uid
					};
				} else if (a as PathNodeWireAnchor) {
					return {kind:"pathNode", pathNodeInfo:PathNodeWireAnchor(a).pathNodeInfo.toJson()};
				}
				return {kind:"normal", key: a.path, uid:a.uid};
			} else {
				LogService.error("Serializer BAD ANCHOR: " + a);
				Utilities.assert(false);
				return null;
			}
		}
	
		public function deserializeWireAnchor(json:Object, parent:AbstractObject, index:int=-1, useNewUid:Boolean=false, objectPropStorage:Object=null):TaconiteModel
		{
			if (json.kind == "ghost")
				return ApplicationController.instance.wireController.ghostAnchor.model;
			if (parent == null)
				return null;
			var split:Object = WireAnchor.pathRegExp.exec(json.key);
			var key:String = json.key;
			if(parent as TextObject && key == "text") {
				key = "source";
			}
			if(parent as PushButton && key == "down") {
				key = "checked";
			}
			var anchorIndex:Number = ("index" in json)?json.index:-1;
			if (split && Number(split[2]) >= 0) {
				key = split[1];
				anchorIndex = Number(split[2]);
			}

			var modifier:IModifier = Modifiers.instance.fetch(key);
			if (modifier == null) {
				if (json.kind == "setter")
					modifier = new InletDescription(json.key);
				else if (json.kind == "edge")
					modifier = deserializeModifier(json.modifier, "external-"+json.externalID);
				else if (json.kind == "edge-inner")
					modifier = deserializeModifier(json.modifier, "internal-"+json.internalID);
//				else
//					LogService.debug("SerializerImplementation::deserializeWireAnchor: Unhandled null modifier")
			}
			var result:WireAnchor = ObjectController.instance.findAnchorForObj(parent,modifier, anchorIndex);
			if (result == null) {
				var parts:Array;
				var prop:WireAnchor;
				var type:String;
				var p1:String;
				var pn:String;
				var inner:String;
				
				if (json.kind == "setter") {
					parts = json.key.split(";");
                    p1 = parts.shift();
                    pn = parts.pop();
                    inner = parts.join(";");
					prop = ObjectController.instance.findAnchorForObj(parent, new PropertyDescription(inner));
					if (!prop) {
                        if (!(parent is ArenaPage)) {
                            LogService.error("Bad Setter detected for parent '"+parent+"' parts='"+inner+"'");
                        }
						return null;
					}
					var setterAnchor:SetterWireAnchor = new SetterWireAnchor(prop, false, pn, useNewUid, null, json.customModifier);
                    if("inValue" in json) {
                        setterAnchor.setCurrentValues(json, objectPropStorage);
                        setterAnchor.initialValues["inValue"] = json["inValue"];
                    }
					return setterAnchor.model;
				} if (json.kind == "trigger") {
					parts = json.key.split(";");
                    p1 = parts.shift();
                    pn = parts.pop();
                    inner = parts.join(";");
					prop = ObjectController.instance.findAnchorForObj(parent, new PropertyDescription(inner));
					if (!prop) {
                        if (!(parent is ArenaPage)) {
						    LogService.error("Bad Trigger detected for parent '"+parent+"' parts='"+inner+"'");
                        }
						return null;
					}
					var triggerAnchor:TriggerWireAnchor = new TriggerWireAnchor(prop, false, pn, useNewUid);
                    if("condition" in json) {
                        triggerAnchor.setCurrentValues(json, objectPropStorage);
                        triggerAnchor.initialValues["condition"] = json["condition"];
                    }
					return triggerAnchor.model;
				}  if (json.kind == "calc") {
					parts = json.key.split(";");
                    p1 = parts.shift();
                    pn = parts.pop();
                    inner = parts.join(";");
					prop = ObjectController.instance.findAnchorForObj(parent, new PropertyDescription(inner));
					var calcAnchor:CalcPropertyWireAnchor = new CalcPropertyWireAnchor(prop, false, pn, useNewUid);
                    if("expression" in json) {
                        calcAnchor.setCurrentValues(json, objectPropStorage);
                        calcAnchor.initialValues["expression"] = json["expression"];
                    }
					return calcAnchor.model;
				}  if (json.kind == "var" && parent is VariableContainer) {
					parts = json.key.split(";");
                    p1 = parts.shift();
                    pn = parts.pop();
                    inner = parts.join(";");
					prop = ObjectController.instance.findAnchorForObj(parent, new PropertyDescription(inner));
					var varAnchor:VariableWireAnchor = new VariableWireAnchor(parent as VariableContainer, json.variable, null, pn, useNewUid);
					parent.addAnchor(varAnchor, false);
                    if("value" in json) {
                        varAnchor.value = deserializeValue(json.value, null, objectPropStorage, varAnchor,  "value");
                    }
					return varAnchor.model;
				} else if (json.kind == "style") {
					parts = json.key.split(";");
					return new StyleWireAnchor(parent as TextObject, parts[1], parts[2], useNewUid).model;
				} else if (json.kind == "edge") {
					var external:ExternalSmartWireAnchor = new ExternalSmartWireAnchor(json.externalID, true, json.internalID);
					external.hostObject= parent;
		
					external.modifierDescription = deserializeModifier(json.modifier, "external-"+json.externalID);
		
					external.other.modifierDescription = deserializeModifier(json.modifier,"internal-"+json.internalID);
					external.other.portSide = json.wireboardSide;
					external.other.position = json.wireboardPosition;
		
					external.label = json.label;
					external.labelChanged = json.labelChanged;

                    if("value" in json && external.modifierDescription is PropertyDescription) {
                        external.other.value = deserializeValue(json.value, null, objectPropStorage, external.other,  "value");
                    }
		
					SmartObject(parent).addAnchor(external, false);
					return external.model;
				} else if (json.kind == "pathNode") {
					var pathNodeInfo:PathNodeInfo = PathNodeInfo.fromJson(json.pathNodeInfo);
					var nodeAnchor:PathNodeWireAnchor = new PathNodeWireAnchor(parent as PathObject, pathNodeInfo);
					parent.addAnchor(nodeAnchor, false);
					return nodeAnchor.model;
				} else {
                    LogService.debug("SerializerImplementation::deserializeWireAnchor: Unhandled null result");
                    return null;
                }
			}
			return result.model
		}
	
		public static function deserializeValue(src:Object, target:Object=null, objectPropStorage:Object=null, host:Object=null,  property:String=null):Object
		{
            /*  TODO: This is a hack to fix DEV-6790.  Previously we just assumed that if src==null, it was a mistake.
                However, certain properties are set to null intentionally.  It'd be a good idea to fix this more architecturally later.*/
            var canBeNullProperties:Array = ["bitmapFill","toggleButtonNormalUp", "toggleButtonNormalDown", "toggleButtonNormalOver", "toggleButtonNormalDisabled", "toggleButtonCheckedUp", "toggleButtonCheckedDown", "toggleButtonCheckedOver", "toggleButtonCheckedDisabled"];
			if (src == null) {
                if (canBeNullProperties.indexOf(property) > -1) {
                    return null;
                }
//                var errorMsg:String = "We are trying to deserialize null to the object "+String(host)+"'s "+String(property)+" property. The target is "+String(target)+". Is this on purpose?";
//                LogService.error(errorMsg);
//                trace(errorMsg);
				return target;
			} else if ("type" in src) {
				if (["Gradient Fill","Jellybean Fill","Solid Fill"].indexOf(src.type) >= 0)
					return GraphicFill.fromJson(src);
				else if(src.type == "PositionAndAngle")
					return PositionAndAngle.fromJson(src);
				else if(src.type == "PresetPoint")
					return PresetPoint.fromJson(src);
				else if(src.type == "CustomPoint")
					return CustomPoint.fromJson(src);
				else if(src.type == "FormattedText")
					return FormattedText.fromJson(src);
				else if(src.type == "Asset")
					return Asset.fromJson(src);
				else if(src.type == "ToggleButtonGroup")
					return ToggleButtonGroup.fromJson(src);
                else if(src.type == "Path")
                    return Path.fromJson(src);
                else if(src.type == "UsageCounts")
                    return UsageCounts.fromJson(src);
                else if(src.type == "AbstractObjectReference") {
                    if(objectPropStorage == null || host == null || property == null) {
                        if (src.id is Array) {   // master reference in EventPage.masters array
                            return ApplicationController.instance.authorController.world.resolveObjectPathArray(src.id);
                        } else {
                            return ApplicationController.instance.authorController.world.findObjByUID(src.id);
                        }
                    } else {
                        if(property in objectPropStorage == false) {
                            objectPropStorage[property] = new Dictionary();
                        }
                        objectPropStorage[property][host] = src.id;
                        return null;
                    }
                } else if(src.type == "VariableStore") {
                    return VariableStore.fromJson(src,  objectPropStorage, host);
                } else if(src.type == "AssetReference") {
                    return Asset.fromJson(src);
                }
			} else if ("source" in src && property != "pages" && property != "masterPages") {
				if (target as IList) {
					copyArrayData(src.source, target as IList, objectPropStorage);
					return target;
				}
			} else if (src as Array) {
				if (target) {
					if (target as IList) {
						copyArrayData(src as Array, target as IList, objectPropStorage);
						return target;
					} else
						LogService.debug("SerializerImplementation: Unhandled target value for src");
				}
				var result:Array = [];
				var i:int;
				var length:int = src.length;
				for (i = 0; i < length; i++)
					result[i] = deserializeValue(src[i], null, objectPropStorage, result, String(i));
				return result;
			} else if ("x" in src && "y" in src) {
				if ("width" in src && "height" in src)
					return new Rectangle(src.x, src.y, src.width, src.height);
				else if ("angle" in src)
					return new PositionAndAngle(src.x, src.y, src.angle);
				return new Point(src.x, src.y);
			} else if ("linear" in src && "colors" in src) {
				var fill:GraphicFill = new GraphicFill();
				copyDeserializedToObject(src, fill);
				return fill;
			} else if ("label" in src && "data" in src) {
				return {label: deserializeValue(src.label), data: deserializeValue(src.data)}
			}
			return src;
		}
	
		public function copyDeserializedToAbstractObject(deserializedObject:Object, newObject:AbstractObject, newUID:Boolean=false, objectPropStorage:Object=null):void
		{
			///trace("copyDeserializedToObject class="+Utilities.shortClassName(newObject), "deserial="+deserializedObject);
			// ensure width and height and anchorPoint are processed before asset and others, since setting of x,y relies on these for anchorPoint calculation
	
			// generate clone so we dont destroy the original when we do deletes below
			// the deletes ensure we don't have to check every property to make sure it
			// isn't one of these special properties
			deserializedObject = ObjectUtil.clone(deserializedObject);
			
			if ("width" in deserializedObject) {
				newObject.width = deserializedObject["width"];
				newObject.height = deserializedObject["height"];
				delete deserializedObject["width"];
				delete deserializedObject["height"];
			}
			if ("anchorPoint" in deserializedObject) {
				var anchorPoint:Object = deserializedObject["anchorPoint"];
				if (anchorPoint.type == "CustomPoint")
					newObject.anchorPoint = CustomPoint.fromJson(anchorPoint);
				else
					newObject.anchorPoint = new Point(anchorPoint.x, anchorPoint.y);
				delete deserializedObject["anchorPoint"];
			}
			if (newObject as AbstractExternalObject) {
				if ("asset" in deserializedObject) {
					if ("viewBox" in deserializedObject) {
						var newValue:* = deserializeValue(deserializedObject["viewBox"], newObject["viewBox"]);
						newObject["viewBox"] = newValue;
						delete deserializedObject["viewBox"];
					}
					if (deserializedObject.asset) {
						var preloadMedia:Boolean = true;
						if (newObject as MediaObject) {
							if ("preload" in deserializedObject) {
								preloadMedia = deserializedObject["preload"];
								MediaObject(newObject).preload = preloadMedia;
							}
						}
						var asset:Asset = Asset.fromJson(deserializedObject.asset, -1, preloadMedia);
						AssetController.instance.attachFor(newObject as AbstractExternalObject, asset);
					}
					delete deserializedObject["asset"];
				}
			}
			if (newObject as SmartObject) {
				if ("gadget" in deserializedObject) {
					delete deserializedObject["gadget"];
				}
			}
			if (newUID)
				if ("uid" in deserializedObject)
					delete deserializedObject["uid"];
            if("snapTo" in deserializedObject && (deserializedObject["snapTo"] is String || deserializedObject["snapTo"] is Array)) {
                deserializedObject["snapTo"] = {type:"AbstractObjectReference", id:deserializedObject["snapTo"]};
            }
            if("parent" in deserializedObject) {
                delete deserializedObject["parent"];
            }
            // TODO: remove this after awhile - SBJ JLC
            // eventually, this will have removed any unintentionally saved editing == true
            if("editing" in deserializedObject){
                delete deserializedObject["editing"];
            }
            if("active" in deserializedObject){
                delete deserializedObject["active"];
            }
            for each(var valueProp:String in newObject.model.getAllValueProperties()) {
                if(valueProp in deserializedObject) {
                    delete deserializedObject[valueProp];
                }
            }
            for each(var valueListProp:String in newObject.model.getAllValueListProperties()) {
                if(valueListProp in deserializedObject) {
                    delete deserializedObject[valueListProp];
                }
            }
			copyDeserializedToObject(deserializedObject, newObject, false, objectPropStorage);
		}
	
		private static function copyDeserializedToObject(deserializedObject:Object, newObject:*, dynamicProperties:Boolean=false, objectPropStorage:Object=null):void
		{
			for (var key:String in deserializedObject) {
				try {
					var newValue:* = deserializeValue(deserializedObject[key], newObject[key], objectPropStorage, newObject, key);
					newObject[key] = newValue;
				} catch (error:ReferenceError) {
					// readonly prop or property doesn't exist on the class
					// ignore
					//trace("Skipping property decode", error);
				} catch (error:TypeError) {
					LogService.error("Type error during deserialize: " + error);
				}
			}
		}
	
		private static function copyArrayData(from:Array, collection:IList, objectPropStorage:Object=null):void
		{
			var i:int;
			var length:int = from.length;
			for (i = 0; i < length; i++) {
				var val:Object = deserializeValue(from[i], null, objectPropStorage, collection, String(i));
				if (i < collection.length)
					collection.setItemAt(val, i);
				else
					collection.addItemAt(val, i);
			}
		}
	}
}
