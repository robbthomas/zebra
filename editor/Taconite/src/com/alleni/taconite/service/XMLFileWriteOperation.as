package com.alleni.taconite.service
{
	import com.alleni.taconite.factory.IFile;
	import com.alleni.taconite.factory.TaconiteFactory;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;

	public class XMLFileWriteOperation extends AbstractOperation
	{
		// the url to be written to
		[Bindable]
		public var url:String;
		
		// the XML to be written
		[Bindable]
		public var data:*;
		
		/**
		 * Construct and XmlFileWriteOperation 
		 * @param url the URL to write to 
		 * @param data the data to use for the request. If data is
		 * of type String or XML, it is saved as the body of the file.
		 * 
		 */
		public function XMLFileWriteOperation(url:String, data:* = null)
		{
			this.url = url;
			this.data = data;
		}
		
		override public function execute() : void
		{
			// make a new file
			var file:IFile = TaconiteFactory.getFileImplementation();
			IEventDispatcher(file).addEventListener(Event.COMPLETE, handleComplete);

			file.url = url;
			
			// write out XML
			file.write(data);
		}
		
		override protected function handleComplete(e:Event) : void
		{
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
	}
}