package com.alleni.taconite.service
{
    import com.alleni.taconite.model.ModelRoot;
    
    /**
     * Value object describing a document in terms of its displayable or indexable content on the website. 
     */
    [Bindable]
    public class TaconiteDocumentDescriptor
    {
        public var name:String = "";
		public var type:String = "";
        public var description:String = "";
		public var sharable:Boolean = false;
        public var tags:String = "";
		public var ownerName:String = "";
		public var price:Number = 0;
		public var selection:String = "";
		public var permissions:String = "";
		public var category:String = "";
		
        public function TaconiteDocumentDescriptor()
        {
        }
    }
}
