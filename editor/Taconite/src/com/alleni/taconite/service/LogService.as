package com.alleni.taconite.service
{
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	
	import flash.events.EventDispatcher;
	import flash.utils.ByteArray;
	
	import mx.logging.LogEvent;
	import mx.logging.LogEventLevel;

	public class LogService extends EventDispatcher
	{
		private static const MAX_LOG_ENTRIES:int = 200;
		private static const MAX_BYTES_DUMP_LENGTH:int = 200;
		
		private static const FLAG_NETWORK_DUMP:uint			= 1 << 1;
		private static const FLAG_NOTIFICATION_DUMP:uint	= 1 << 2;
		private static const FLAG_ROUTE_TO_COMMAND:uint		= 1 << 3;
		private static const FLAG_STACK_TRACING:uint		= 1 << 4;
		
		
		private static var _instance:LogService;
		private static var _enabled:Boolean = false;
		
		private static var _dump:Vector.<String> = new Vector.<String>();
		public static var messages:String = "";
		
		private static var _flags:uint = 0;
		
		public function LogService()
		{
			Utilities.assert(!_instance);
		}
		
		public static function get instance():LogService
		{
			if (!_instance)
				_instance = new LogService();
			return _instance;
		}
		
		public static function get enabled():Boolean
		{
			return _enabled;
		}
		
		public static function set enabled(value:Boolean):void
		{
			_enabled = value;
			if (_enabled) {
				info("Initiating session for " + TaconiteFactory.getEnvironmentImplementation().username + "..."
					+ "\n >> System: " + TaconiteFactory.getEnvironmentImplementation().apiURI
					+ "\n >> " + TaconiteFactory.getEnvironmentImplementation().containerDetails);
			}
		}
		
		public static function get dump():String
		{
			return _dump.join("\n");
		}
		
		public static function info(message:String):void
		{
			dispatch(message, LogEventLevel.INFO);
		}
		
		public static function debug(message:String):void
		{
			dispatch(message, LogEventLevel.DEBUG);
		}
		
		public static function networkInfo(data:*, label:String=""):void
		{
			if (testFlag(FLAG_NETWORK_DUMP|FLAG_ROUTE_TO_COMMAND)) {
				var networkDump:String;
				if (data as ByteArray && data.length > 0) {
					var bytes:ByteArray = ByteArray(data);
					networkDump = bytes.readUTFBytes(
						bytes.length > MAX_BYTES_DUMP_LENGTH ? MAX_BYTES_DUMP_LENGTH : bytes.bytesAvailable
					);
					bytes.position = 0;
				} else
					networkDump = data;
				
				if (label.length > 0)
					networkDump = label + ": " + networkDump;
				dispatch(networkDump, LogEventLevel.INFO);
			}
		}
		
		public static function error(message:String):void
		{
			dispatch(message, LogEventLevel.ERROR);

			if (testFlag(FLAG_STACK_TRACING)) {
				var tempError:Error = new Error();
				dispatch(tempError.getStackTrace(), LogEventLevel.ERROR);
			}
		}
		
        public static function message(message:String):void
        {
            if (_enabled) {
                messages += message + "\n";
            }
        }

        public static function clear():void
        {
            _dump.splice(0, _dump.length);
        }

		public static function set networkDump(value:Boolean):void
		{
			setFlagToValue(FLAG_NETWORK_DUMP, value);
		}
		
		public function sendAsEmail(originDetail:String=""):void
		{
			var emailOperation:EmailOperation = new EmailOperation();
			emailOperation.addressTo = "support@zebrazapps.com";
			emailOperation.subject = "[Zebra Log] From " + TaconiteFactory.getEnvironmentImplementation().username;
			if (originDetail.length > 0)
				emailOperation.subject += " : " + originDetail;
			emailOperation.body = "[ Logged by user " + TaconiteFactory.getEnvironmentImplementation().username;
			if (TaconiteFactory.getEnvironmentImplementation().email.length > 0)
				emailOperation.body += " (" + TaconiteFactory.getEnvironmentImplementation().email + ") ";
			emailOperation.body += " on system " + TaconiteFactory.getEnvironmentImplementation().serverName;
			emailOperation.body += " | Environment details: " + TaconiteFactory.getEnvironmentImplementation().containerDetails + " ]";
			emailOperation.body += "\n\n" + LogService.dump;
			emailOperation.execute();
		}
		
		public static function set notificationDump(value:Boolean):void
		{
			//TODO: Build Out a Logger Plugin for our Logger
			setFlagToValue(FLAG_NOTIFICATION_DUMP, value);
			//if (value)
				//NotificationController.registerListener(_instance);
			//else
				//NotificationController.unregisterListener(_instance);
		}
		
		public static function set routeToCommand(value:Boolean):void
		{
			setFlagToValue(FLAG_ROUTE_TO_COMMAND, value);
		}
		
		public static function set stackTracing(value:Boolean):void
		{
			setFlagToValue(FLAG_STACK_TRACING, value);
		}
		
		public static function receiveNotifications(notification:ApplicationEvent):void
		{
            if(!testFlag(FLAG_NOTIFICATION_DUMP)) {
                return;
            }
			var message:String = "Broadcast notification '" + notification.type + "'";
			if (notification.data) {
				message += " with data=[";
				if (notification.data as Object) {
					for (var property:String in notification.data) {
						message += "\n[" + property + "] " + notification.data[property];
					}
				} else
					message += notification.data;
				message += "]";
			}
			info(message);
		}
		
		private static function logFilter(level:int):Boolean
		{
			/****************  edit this function to change the log filter  *****************/
			switch (level) {
				case LogEventLevel.FATAL:		return true;
				case LogEventLevel.ERROR:		return true;
				case LogEventLevel.WARN:		return true;
				case LogEventLevel.INFO:		return true;  // log of author actions
				case LogEventLevel.DEBUG:		return false;
			}
			return true;
		}
		
		private static function dispatch(message:String, level:int):void
		{
			if (_enabled && logFilter(level)) {
				trace(LogEvent.getLevelString(level) + ": " + message);
				message = "["+Utilities.prettyDate+"] " + message;
				if (_dump.length > MAX_LOG_ENTRIES)
					_dump.shift();
				_dump.push(LogEvent.getLevelString(level) + ": " + message);
				instance.dispatchEvent(new LogEvent(message, level));
			}
		}
		
		private static function testFlag(mask:uint):Boolean
		{
			return (_flags & mask) != 0;
		}
		
		private static function setFlag(mask:uint):void
		{
			_flags |= mask;
		}
		
		private static function clearFlag(mask:uint):void
		{
			_flags &= ~mask;
		}
		
		private static function setFlagToValue(mask:uint, value:Boolean):void
		{
			if (value)
				_flags |= mask;
			else
				_flags &= ~mask;
		}
	}
}