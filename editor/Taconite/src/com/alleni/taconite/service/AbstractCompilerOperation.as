package com.alleni.taconite.service
{
	import flash.utils.ByteArray;
	
	public class AbstractCompilerOperation extends AbstractOperation
	{
		protected var CompiledClass:Class;
		protected var _bytes:ByteArray;
		
		public function AbstractCompilerOperation(bytes:ByteArray)
		{
			super();
			_bytes = bytes;
		}
		
		override public function get result():*
		{
			LogService.debug("Accessing compiled bytes");
			return new CompiledClass();
		}
	}
}