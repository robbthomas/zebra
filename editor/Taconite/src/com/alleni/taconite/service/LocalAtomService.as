package com.alleni.taconite.service
{
	import com.alleni.taconite.persistence.IAtomDecoder;
	import com.alleni.taconite.persistence.IAtomEncoder;
	
	import flash.net.FileReference;
	
	public class LocalAtomService extends RestAtomService
	{
		private var _file:FileReference;
		
		public function LocalAtomService(file:FileReference, decoder:IAtomDecoder=null, encoder:IAtomEncoder=null, purchased:Boolean=false)
		{
			super(decoder, encoder, purchased);
			_file = file;
		}
		
		override public function loadAtom(id:String, published:Boolean, loadVersion:Boolean=false):IOperation
		{
			var operation:IOperation = new LocalFileLoadOperation(_file);
			return new JSONAtomFilterOperation(operation, id, _decoder);
		}
	}
}