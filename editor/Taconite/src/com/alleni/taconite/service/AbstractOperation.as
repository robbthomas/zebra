package com.alleni.taconite.service
{
    import com.alleni.taconite.event.OperationEvent;
    import com.alleni.taconite.event.OperationFaultEvent;
    import com.alleni.taconite.event.ProgressSourceEvent;
    
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IEventDispatcher;
    
    [Event(name="complete",type="flash.events.Event")]
    [Event(name="fault",type="com.alleni.taconite.event.OperationFaultEvent")]

    /**
     * Abstract class representing an operation of some sort,
     * probably but not necessarily asynchronous.  Its execute() method
     * initiates it, after which its contract requires that it
     * eventually dispatch either Event.COMPLETE or ErrorEvent.ERROR.
     * 
     */
    public class AbstractOperation extends EventDispatcher implements IOperation
    {
		
		private var _attempts:int = 0;
		public function get attempts():int {return _attempts;};
		public function set attempts(value:int):void {_attempts = value;};
		
        private var _displayName:String = "operation";
		
		protected var _progressFraction:Number = 0;
		public function get progressFraction():Number
		{
			return _progressFraction;
		}
		
		private var _operationId:String;
		public function get operationId():String
		{
			if (!_operationId)
				_operationId = String(Math.floor(Number(String(new Date().time).substring(0, 10)) * Math.random())); // avoid dependency on MX UIDUtil at this level
			return _operationId;
		}
		
        /**
         * A name for this operation to be shown by the Controller to be shown
         * to indicate its progress or status.
         */
        [Bindable]
        public function get displayName():String
        {
            return _displayName;
        }

        public function set displayName(value:String):void
        {
            _displayName = value;
        }
        
        /**
         * Initiate this Operation.  An event may be dispatched during
         * the execution of this function, or at any point afterwards.
         */
        public function execute():void
        {
        }

        
        /**
         * On success, return the result of this operation.
         */
        public function get result():*
        {
            return null;
        }

        /**
         * Subclasses may override to determine the disposition
         * of an error-related event.
         */
        public function handleError(e:ErrorEvent):void
        {
			LogService.error("Operation Error for '"+this.displayName+"': " + e);
			dispatchEvent(new OperationFaultEvent(OperationFaultEvent.FAULT, e as ErrorEvent));
        }

        /**
         * Subclasses may override to determine disposition of a success event. 
         */
		protected function handleComplete(e:Event):void
		{
			dispatchEvent(new Event(Event.COMPLETE));
		}

		protected function handleUpdate(e:Event):void
		{
			dispatchEvent(new Event(OperationEvent.UPDATE));
		}

        protected function dispatchProgressSourceEvent(source:IEventDispatcher):void
        {
			if (displayName)
	            dispatchEvent(new ProgressSourceEvent(ProgressSourceEvent.PROGRESS_START, source, displayName));
        }
   }
}
