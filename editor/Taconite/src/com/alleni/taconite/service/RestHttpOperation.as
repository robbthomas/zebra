package com.alleni.taconite.service
{
	import com.alleni.taconite.event.OperationFaultEvent;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.persistence.json.JSONDecoder;
	import com.alleni.taconite.persistence.json.JSONEncoder;
import com.alleni.taconite.service.HttpFormData;

import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;

import mx.utils.Base64Encoder;

[Event(name="complete",type="flash.events.Event")]
	[Event(name="error",type="flash.events.ErrorEvent")]
	
	/**
	 * Operation subclass that performs a REST-style HTTP request.  The
	 * request itself is a GET of name/value pairs, while the response
	 * is expected to be XML or overridden by a subclass.
	 * The method may optionally be specified as POST instead.
	 */
	public class RestHttpOperation extends AbstractOperation
	{
		public static const GET:String 		= "GET";
		public static const POST:String 	= "POST";
		public static const PUT:String 		= "PUT";
		public static const DELETE:String 	= "DELETE";
		
		public static var HTTP_TIMEOUT:int = 60 * 1000; // 60 seconds for now (60sec by default)
		
		/** The path to be requested by this operation using the standard environment URI. */
		public var path:String;
		
		/** The ByteArray returned by this operation. */
		public var resultByteArray:ByteArray;
		
		public var reattemptOnFault:Boolean = true;
		
		private var _resultDecoded:Object;
		
		/** An untyped object containing request parameters to accompany the URL. */
		public var data:*;
		
		/** The method to be used for the operation */
		public var method:String = GET;
		
		/** Operation response code */
		public var code:int;
		
		/** Cookies extracted from HTTP headers */
		public var cookie:String = "";
		
		public var timeout:int = -1;

		public var customHeaders:Array;
		
		protected var _uri:String;

        public var queryString:Object = {};

        public var multipartForm:Boolean = false;

        public var cacheContent:Boolean = false;

        public static var networkAvailable:Boolean = true;
        public static var networkInUse:Boolean = false;

		/**
		 * Construct a RestHttpOperation.
		 * @param url the URL to request
		 * @param data the data to use for the request.  If data is
		 * of type String or XML, it is posted as the body of the document.
		 * Otherwise it is treated as an Object whose name-value pairs supply
		 * the parameters in a GET or POST.
		 */
		public function RestHttpOperation(path:String, data:* = null, method:String="", notifyProgress:Boolean=false)
		{
			this.path = path;
			this.data = data;
			this.resultByteArray = new ByteArray();
			this.customHeaders = new Array();
			
			if (method.length > 0)
				this.method = method;
		}
		
		public static function getProtocolForPort(port:int):String
		{
			switch (port) {
				case 443:
					return "https";
					break;
				case 80:
					default:
					return "http";
					break;
			}
		}
		
		/**
		 * Initiate this operation by performing the HTTP request.
		 * If any value among the parameters is XML, it is prettyprinted and the
		 * method is automatically changed to a POST.
		 */
		override public function execute():void
		{
            //Comment this back in if we turn the black screen back on.
//            if(networkAvailable == false && displayName != "Phone Home"){
//                handleError(new ErrorEvent(ErrorEvent.ERROR, false, false, "Network Unavailable"));
//                trace("Operation bypassed");
//                return;
//            }

            this.attempts++;

			_uri = TaconiteFactory.getEnvironmentImplementation().apiURI + path;

			var urlRequest:URLRequest = new URLRequest();
            if(cacheContent){
			    urlRequest.method = GET;
            }else{
                urlRequest.method = POST;
            }
			if (customHeaders && customHeaders.length > 0) {
				var i:int = 0;
				var length:int = customHeaders.length;
				for (i = 0; i < length; i++) {
					var customHeader:URLRequestHeader = customHeaders[i] as URLRequestHeader;
					if (!customHeader) continue;
					urlRequest.requestHeaders.push(customHeader);
				}
			}
			urlRequest.requestHeaders.push(basicAuthenticationHeader);
			urlRequest.contentType = "application/octet-stream";

            var variables:URLVariables = new URLVariables;
            if(cacheContent == false){
                variables._method = this.method;
            }
			if (this.method == GET) {
				if (this.data as Object) {
					for (var prop:String in this.data)
						variables[prop] = this.data[prop];
				}
                if(!cacheContent) {
				    urlRequest.data = postInsurance;
                }

			} else {
                var md:MimeData = serializeData(this.data);
                urlRequest.contentType = md.contentType;
                urlRequest.data = md.data;
			}
            for(var item:String in queryString){
                variables[item] = queryString[item];
            }

            for(var unused:String in variables) {
                // it will only get in the body if there is at least one variable
                _uri += "?" + variables;
                break; // break out after only one loop
            }
			
			LogService.debug("HTTP API " + urlRequest.method + " ("+this.method+") to " + _uri + "  '" + this.displayName + "'");
			LogService.networkInfo(urlRequest.data, "Request Body");
			//trace(urlRequest.data);

			var urlLoader:URLLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			
			addListeners(urlLoader);
			dispatchProgressSourceEvent(urlLoader);
			
			urlRequest.url = _uri;
			urlLoader.load(urlRequest);
            networkInUse = true;
		}

        private function serializeData(data:*):MimeData {
            var result:MimeData = new MimeData()
            if (data as XML) {
                result.data = (data as XML).toXMLString();
            } else if (data as String) {
                result.data = data;
                result.contentType = "text/plain";
            } else if (data as ByteArray) {
                data.position = 0;
                result.data = this.data;
            } else if (data as HttpFormData) {
                result.data = HttpFormData(data).generateBytes();
                result.contentType = HttpFormData(data).generateContentType();
            } else if (data as Object) {
                result.data = toJSONBytes(data);
                result.contentType = "application/json";
            } else {
                result.data = postInsurance;
            }
            return result;
        }
		
		private function addListeners(urlLoader:URLLoader):void
		{
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, handleHTTPResponseStatus);
			urlLoader.addEventListener(ProgressEvent.PROGRESS, handleProgress);
			urlLoader.addEventListener(Event.COMPLETE, handleResult);
		}
		
		private function removeListeners(urlLoader:URLLoader):void
		{
			urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			urlLoader.removeEventListener(HTTPStatusEvent.HTTP_STATUS, handleHTTPResponseStatus);
			urlLoader.removeEventListener(ProgressEvent.PROGRESS, handleProgress);
			urlLoader.removeEventListener(Event.COMPLETE, handleResult);
		}
		
		private function handleIOError(event:IOErrorEvent):void
		{
			LogService.error("NOT handling HTTP error: " + event.type + " : " + event.text);
			networkInUse = false;
			var urlLoader:URLLoader = event.target as URLLoader;
			if (urlLoader)
				removeListeners(urlLoader);
		}
		
		private function handleProgress(event:ProgressEvent):void
		{
			var progressFraction:Number = event.bytesLoaded / event.bytesTotal;
			if (isFinite(progressFraction)) {
				_progressFraction = progressFraction;
				dispatchEvent(event);
			}
		}

		/**
		 * @inheritDoc 
		 */
		override public function get result():*
		{
			return this.resultByteArray;
		}
		
		public function get resultDecoded():Object
		{
			if (_resultDecoded == null) {
				var bytes:ByteArray = ByteArray(this.result);
				if (bytes.length > 0) {
					try { // try not ideal here, but JSON parser crashes hard when it receives an unexpected character.
                        var text:String = bytes.readUTFBytes(bytes.length);
						_resultDecoded = new JSONDecoder(text, true).getValue();
						//trace("\n RESULT:\n"+this.resultByteArray);
					} catch(event:Error) {
						trace("JSON crash: " + event.getStackTrace());
					}
				}
			}
			return _resultDecoded;
		}
		
		private function handleHTTPResponseStatus(event:HTTPStatusEvent):void
		{
			if (event)
				this.code = event.status;
			networkAvailable = true;
            networkInUse = false;
			if (this.code != 200) {
				LogService.debug("HTTP API: '" + this.displayName + "' Response code=" + this.code);
				var errorEvent:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR, false, false, "Service Error: " + this.code);

				switch (this.code) {
					case 302:
						LogService.error(" >>> 302 redirected: " + this.resultByteArray);
						dispatchEvent(new OperationFaultEvent(OperationFaultEvent.REDIRECT, errorEvent));
						break;
					case 400:
						LogService.error(" >>> 400 command error: " + this.resultByteArray);
						dispatchEvent(new OperationFaultEvent(OperationFaultEvent.COMMAND_ERROR, errorEvent));
						break;
					//case 0: // give reauth a try upon the general "punt" error code.
					case 401:
						LogService.error(" >>> 401 unauthorized: " + this.resultByteArray);
						dispatchEvent(new OperationFaultEvent(OperationFaultEvent.UNAUTHORIZED, errorEvent));
						break;
					case 404:
						LogService.error(" >>> 404 controller unavailable: " + this.resultByteArray);
						dispatchEvent(new OperationFaultEvent(OperationFaultEvent.NOT_FOUND, errorEvent));
						break;
					case 500:
						LogService.error(" >>> 500 server error: " + this.resultByteArray);
						dispatchEvent(new OperationFaultEvent(OperationFaultEvent.INTERNAL_SERVER_ERROR, errorEvent));
						break;
					case 503:
						LogService.error(" >>> 503 service unavailable: " + this.resultByteArray);
						dispatchEvent(new OperationFaultEvent(OperationFaultEvent.SERVICE_UNAVAILABLE, errorEvent));
						break;
					default:
						LogService.error(" >>> "+this.displayName+" FAULT code:" + this.code + " error: " + this.resultByteArray);
                        networkAvailable = false;
						handleError(errorEvent);
						break;
				}
			}
		}
		
		private function handleResult(event:Event):void
		{
			networkInUse = false;
            var urlLoader:URLLoader = event.target as URLLoader;
			if (!urlLoader) return;
			removeListeners(urlLoader);
			
			_resultDecoded = null;
			
			this.resultByteArray = urlLoader.data as ByteArray;
			//trace("\n"+this.path+"\n RESULT:\n"+this.resultByteArray);
			
			LogService.networkInfo(this.resultByteArray, "Response bytes");

			var firstByte:String = null;
			if(this.resultByteArray.bytesAvailable > 0) {
				firstByte = String.fromCharCode(this.resultByteArray.readUnsignedByte());
			}
			this.resultByteArray.position = 0;
			
			if (firstByte == "{") { // a string of JSON
				if (this.resultDecoded != null && "http-code" in this.resultDecoded)
					this.code = int(this.resultDecoded["http-code"]);
			}
			
			if (this.code == 200) {
				LogService.debug("HTTP API: '" + this.displayName + "' Response code=" + this.code);
				handleComplete(event);
			} else
				handleHTTPResponseStatus(null);
		}
		
		private static function toJSONBytes(object:*):ByteArray
		{
			var result:ByteArray = new ByteArray();
			var jsonString:String = new JSONEncoder(object).getString();
			result.writeUTFBytes(jsonString);
			result.position = 0;
			return result;
		}
	
		public static function get basicAuthenticationHeader():URLRequestHeader
		{
			var username:String = TaconiteFactory.getEnvironmentImplementation().username;
			var password:String = TaconiteFactory.getEnvironmentImplementation().password;

			if (username != null && password != null) {
				var encoder:Base64Encoder = new Base64Encoder();
				encoder.insertNewLines = false;
				encoder.encode(username + ":" + password);
				return new URLRequestHeader("Authorization", "Basic " + encoder.toString());
			}
			return null;
		}
		
		public static function get postInsurance():Object
		{
			var postInsuranceData:Object = new Object();
			postInsuranceData.postInsurance = "postInsurance"; // Flash will helpfully convert any call into a GET unless we do this
			return postInsuranceData;
		}
	}
}
internal class MimeData {
    public var contentType:String = "application/octet-stream";
    public var data:* = null;
}
