package com.alleni.taconite.service
{
    import com.alleni.taconite.persistence.IAtomDecoder;
    
    [Event(name="complete",type="flash.events.Event")]
    [Event(name="fault",type="com.alleni.taconite.event.OperationFaultEvent")]

    /**
     * FilterOperation subclass that wraps some other operation which loads the information
     * in a TaconiteDocumentData.  This class assumes that the wrapped operation's result is the encoded form
     * of the document. 
     */
    public class BasicDocumentFilterOperation extends FilterOperation
    {
        public var documentId:String;
        public var decoder:IAtomDecoder;
        
        public function BasicDocumentFilterOperation(operation:IOperation, id:String, decoder:IAtomDecoder)
        {
            super(operation);
            this.documentId = id;
            this.decoder = decoder;
        }

        /**
         * On success, return the result of this operation.
         */
        override public function get result():*
        {
            return decoder.decode(operation.result, this.documentId);
        }
	}
}
