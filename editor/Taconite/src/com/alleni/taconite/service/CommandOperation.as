package com.alleni.taconite.service
{
	import com.alleni.taconite.event.OperationEvent;
	import com.alleni.taconite.event.OperationFaultEvent;
	
	import flash.events.Event;
	
	import mx.logging.LogEvent;

	public class CommandOperation extends AbstractOperation
	{
		public static const COMMAND_INLINE_REGEXP:String = "\\w+\\s?[\\w\\s\\.\/]*";
		
		protected const HELP:RegExp = 	/^help/i;
		protected const API:RegExp = 	/^api\s([\w\.\/]+)/i;
		protected const PING:RegExp = 	/^ping/i;
		
		protected const HELP_OUTPUT:String = "Zebra commands: ping, api [path to call]";
		
		protected const NOT_RECOGNIZED_OUTPUT:String = "Sorry, command not recognized";
		
		private var _output:String;
		
		public function CommandOperation(cmd:String)
		{
			this.displayName = cmd;
		}
		
		override public function execute():void
		{
			// could replace this stuff with a fancy ANTLR-generated grammar parser..
			// but that would be too tempting, now, wouldn't it? ;)
			
			var cmd:String = this.displayName;
			
			_output = synchronousCommand(cmd);

			if (_output.length == 0) {
				if (API.exec(cmd)) {
					var action:String = API.exec(cmd)[1];
					executeRestAction(action);
					return;
				} else if (PING.exec(cmd)) {
					executeRestAction("auth/session/keepalive");
					return;
				} else {
					_output = NOT_RECOGNIZED_OUTPUT + ": '" + cmd +"'";
				}
			}
			
			handleComplete(null);
		}
		
		private function executeRestAction(action:String, data:*=null, method:String=null):void
		{
			if (!method) method = RestHttpOperation.POST;
			var restOperation:RestHttpOperation = new RestHttpOperation(action, null, method);
			restOperation.displayName = this.displayName;
			addListeners(restOperation, handleRestComplete);
			LogService.routeToCommand = true;
			LogService.instance.addEventListener(LogEvent.LOG, handleLogEvent, false, 0, true);
			restOperation.execute();
			dispatchEvent(new OperationEvent(OperationEvent.EXECUTING));
		}
		
		private function handleRestComplete(event:Event):void
		{
			_output = this.displayName + " response: " + RestHttpOperation(event.target).resultByteArray as String;
			handleComplete(null);
			
			LogService.instance.removeEventListener(LogEvent.LOG, handleLogEvent);
			LogService.routeToCommand = false;
		}
		
		private function addListeners(operation:IOperation, completeHandler:Function):void
		{
			operation.addEventListener(Event.COMPLETE, completeHandler);
			operation.addEventListener(OperationFaultEvent.REDIRECT, handleFault);
			operation.addEventListener(OperationFaultEvent.COMMAND_ERROR, handleFault);
			operation.addEventListener(OperationFaultEvent.UNAUTHORIZED, handleFault);
			operation.addEventListener(OperationFaultEvent.NOT_FOUND, handleFault);
			operation.addEventListener(OperationFaultEvent.INTERNAL_SERVER_ERROR, handleFault);
			operation.addEventListener(OperationFaultEvent.FAULT, handleFault);
		}
		
		private function handleFault(event:OperationFaultEvent):void
		{
			_output = "Fault for " + RestHttpOperation(event.target).displayName + ": " + event.error.text;
			handleComplete(null);
			
			LogService.instance.removeEventListener(LogEvent.LOG, handleLogEvent);
			LogService.routeToCommand = false;
		}
		
		private function handleLogEvent(event:LogEvent):void
		{
			dispatchEvent(new OperationEvent(OperationEvent.UPDATE, event.message));	
		}
		
		protected function synchronousCommand(cmd:String):String
		{
			if (HELP.exec(cmd))
				return HELP_OUTPUT;
			return "";
		}
		
		override public function get result():*
		{
			return _output;
		}
	}
}