package com.alleni.taconite.service
{
	import flash.external.ExternalInterface;
import flash.net.URLRequest;
import flash.net.navigateToURL;

public class EmailOperation extends AbstractOperation
	{
		public var addressTo:String;
		public var addressFrom:String;
		public var subject:String;
		public var body:*;

		public function EmailOperation()
		{
			super();
		}
		
		override public function execute():void
		{
			this.body = escape(this.body);
			
			var url:String = "mailto:" + this.addressTo + "?subject=" + this.subject + "&body=" + this.body;
			
			var mailRequest:URLRequest = new URLRequest();
			mailRequest.url = url;
//			navigateToURL(mailRequest, "_self");
            ExternalInterface.call("handleURLRequest", {address:url});
		}
	}
}