package com.alleni.taconite.service
{
	import com.alleni.taconite.dev.TaconiteFont;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.system.LoaderContext;
	import flash.text.Font;
	import flash.utils.ByteArray;
	
	public class FontCompilerOperation extends AbstractCompilerOperation
	{
		private var _name:String;
		
		public function FontCompilerOperation(bytes:ByteArray, name:String)
		{
			super(bytes);
			_name = name;
		}
		
		override public function execute():void
		{
			var swf:ByteArray = TaconiteFont.publish(_bytes, _name);
			
			if (swf != null) {
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.INIT, function(event:Event):void {
					CompiledClass = LoaderInfo(event.currentTarget).applicationDomain.getDefinition(TaconiteFont.QNAME) as Class;
					Font.registerFont(CompiledClass);
					handleComplete(event);		
				});
				LogService.debug("Compiling font from raw bytes");
				
				// loaderContext necessary since we are loading a SWF, so allowCodeImport must be true to 
				// configure security specific to environment, and its interface differs between Flash and AIR
				var loaderContext:LoaderContext = new LoaderContext();
				loaderContext.allowCodeImport = true;
				loader.loadBytes(swf, loaderContext);
			} else
				handleError(new ErrorEvent(ErrorEvent.ERROR, false, false, "caught exception compiling font"));
		}
	}
}