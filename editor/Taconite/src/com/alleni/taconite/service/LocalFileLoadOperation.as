package com.alleni.taconite.service
{
	import flash.events.Event;
	import flash.net.FileReference;
	import flash.utils.ByteArray;

	public class LocalFileLoadOperation extends LocalFileOperation
	{
		private var _file:FileReference;
		public var resultByteArray:ByteArray;
		
		public function LocalFileLoadOperation(file:FileReference)
		{
			super();
			_file = file;
		}
		
		override public function execute():void
		{
			load(_file);
		}
		
		override public function get result():*
		{
			return this.resultByteArray;
		}
		
		override protected function handleComplete(event:Event):void
		{
			var file:FileReference = FileReference(event.target);
			resultByteArray = file.data;
			super.handleComplete(event);
		}
	}
}