package com.alleni.taconite.service
{
	import com.alleni.taconite.persistence.IAtomDecoder;
	import com.alleni.taconite.persistence.json.JSONDecoder;
	
	import flash.utils.ByteArray;
	
	public class JSONAtomFilterOperation extends BasicDocumentFilterOperation
	{
		private var _id:String;
		
		public function JSONAtomFilterOperation(operation:IOperation, id:String, decoder:IAtomDecoder)
		{
			super(operation, id, decoder);
			_id = id;
		}
		
		/**
		 * On success, return the result of this operation.
		 */
		override public function get result():*
		{
			// Decode JSON and convert to document data
			var data:ByteArray = ByteArray(operation.result);
			data.position = 0;
			var text:String = data.readUTFBytes(data.length);
			if (text.indexOf("$LOCAL#") >= 0) {
				throw new Error("$LOCAL# found in json text");  // local gadget ID should not have been sent to server
			}
//			trace("JSONAtomFilterOperation:");
//			trace(text);
			var decoded:Object = new JSONDecoder(text, true).getValue();

			var atoms:Object;
			if ("atoms" in decoded)
				atoms = decoded.atoms;
			else atoms = decoded;
			
			var stateObject:Object;
			if ("state" in decoded)
				stateObject = decoded.state;
			
			// in case we are loading this as a purchased record using the Player, we won't originally have the root ID. To be sure, get it from the first atom in the list.
			// this only works for projects (projectTypeId 2) which consist of a 1-length gadget.
			if (atoms as Array) {
				var i:int;
				var length:int = atoms.length;
				for (i = 0; i < length; i++) {
					var root:Object = atoms[i];
					if ("id" in root && "projectTypeId" in root) {
						if (root.projectTypeId == 2)
							_id = root.id;
					}
				}
			}
			
			return decoder.decode(atoms, _id);
		}
	}
}