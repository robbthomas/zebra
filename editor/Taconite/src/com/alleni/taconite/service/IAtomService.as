package com.alleni.taconite.service
{
	import com.alleni.taconite.model.TaconiteObject;
	import com.alleni.taconite.persistence.IAtom;

    /**
     * Interface definition for a service that handles the loading and saving of documents. 
     */
    public interface IAtomService
    {
        /**
         * Return an operation that will load a document from the given documentId when executed.
         * The result of the operation, when completed, is a TaconiteDocumentData object which includes
         * the document model along with other descriptive information.
         * 
         * @param documentUri the URI of a document to be loaded
         */
        function loadAtom(id:String, published:Boolean, loadVersion:Boolean=false):IOperation;
        
        /**
         * Return an operation that will save a document from the given data.  The result of
         * this operation, when completed, is a new TaconiteDocumentData instance that may contain additional
         * descriptive information about the document such as its ID.
         *  
         * @param documentData a TaconiteDocumentData object describing a persisted document.
         */
        function saveAtom(atom:IAtom, object:TaconiteObject=null, metaDataOnly:Boolean=false, autosave:Boolean=false):IOperation;
    }
}
