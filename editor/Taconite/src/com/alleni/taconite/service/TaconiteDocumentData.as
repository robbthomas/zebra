package com.alleni.taconite.service
{
    import com.alleni.taconite.model.TaconiteModel;
    
    /**
     * Document-level model object representing all persisted data concerning a document, including the
     * document itself. 
     */

    [Bindable]
    public class TaconiteDocumentData
    {
        public var root:TaconiteModel;
        public var documentId:String;
        public var saveDocumentURI:String;
        public var documentDescriptor:TaconiteDocumentDescriptor = null;
        
        public function TaconiteDocumentData(root:TaconiteModel, documentId:String)
        {
            this.root = root;
            this.documentId = documentId;
        }
    }
}
