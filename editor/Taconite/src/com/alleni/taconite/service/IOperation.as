package com.alleni.taconite.service
{
    import com.alleni.taconite.event.OperationFaultEvent;
    
    import flash.events.Event;
    import flash.events.IEventDispatcher;
    
    [Event(name="complete",type="flash.events.Event")]
    [Event(name="fault",type="com.alleni.taconite.event.OperationFaultEvent")]

    /**
     * Interface representing an operation of some sort,
     * probably but not necessarily asynchronous.  Its execute() method
     * initiates it, after which its contract requires that it
     * eventually dispatch either Event.COMPLETE or ErrorEvent.ERROR.
     * 
     */
    public interface IOperation extends IEventDispatcher
    {
		function get operationId():String;
		
        /** Displayable name for this operation */
		function get displayName():String;
        function set displayName(value:String):void;
        
        /** Result object from this operation */
        function get result():*;
        
		// how many times have we tried?
		function get attempts():int;
		function set attempts(value:int):void;
		
		function get progressFraction():Number;
		
        /**
         * Initiate this operation.  An event may be dispatched during
         * the execution of this function, or at any point afterwards.
         */
        function execute():void;
    }
}
