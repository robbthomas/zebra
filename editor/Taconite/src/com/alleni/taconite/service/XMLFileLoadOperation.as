package com.alleni.taconite.service
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	public class XMLFileLoadOperation extends RestHttpOperation
	{
		/** The XML returned by this operation. */
		[Bindable]
		public var resultXml:XML;
		
		public function XMLFileLoadOperation(path:String)
		{
			super(path);
		}
		
		override public function execute():void
		{
			var request:URLRequest = new URLRequest(_uri);
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, handleFileResult);
			loader.addEventListener(IOErrorEvent.IO_ERROR, handleError);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleError);
			loader.load(request);
			dispatchProgressSourceEvent(loader);
		}
		
		/**
		 * @inheritDoc 
		 */
		override public function get result():*
		{
			return resultXml;
		}
		
		private function handleFileResult(event:Event):void
		{
			resultXml = new XML(URLLoader(event.target).data);
			handleComplete(event);
		}
	}
}