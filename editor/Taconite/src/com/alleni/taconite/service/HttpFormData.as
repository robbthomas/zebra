package com.alleni.taconite.service {
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import mx.utils.UIDUtil;

public class HttpFormData {
    private var values:Vector.<HttpFormValue> = new Vector.<HttpFormValue>();
    private var boundary:String = "ZEBRA"+UIDUtil.createUID(); // this can be nearly any random String
    public function HttpFormData() {
    }
    public function addVariable(name:String, value:String):void {
        values.push(new HttpFormVariable(name, value));
    }
    public function addFile(name:String, filename:String, contentType:String, content:ByteArray):void {
        values.push(new HttpFormFile(name, filename, contentType, content))
    }
    public function generateBytes():ByteArray {
        // at this point we may want to check to see that our boundary does not actually occur in
        // one of the form content
        var result:ByteArray = new ByteArray();
        for each(var value:HttpFormValue in values) {
            result.writeUTFBytes("\r\n--");
            result.writeUTFBytes(boundary);
            result.writeUTFBytes("\r\n");
            value.writeBytes(result);
        }
        result.writeUTFBytes("\r\n--");
        result.writeUTFBytes(boundary);
        result.writeUTFBytes("--");
        result.position = 0;
        return result;
    }

    public function generateContentType():String {
        return "multipart/form-data; boundary="+boundary;
    }
}
}

import flash.utils.ByteArray;

import mx.utils.Base64Encoder;

import mx.utils.Base64Encoder;

internal class HttpFormValue {
    private var name:String;

    public function HttpFormValue(name:String) {
        this.name = name;
    }

    public function writeBytes(array:ByteArray):void {
        writeHeader(array);
        array.writeUTFBytes("\r\n"); // end the last header line
        array.writeUTFBytes("\r\n"); // blank line between header and body
        writeBody(array);
    }
    protected function writeHeader(array:ByteArray):void {
        array.writeUTFBytes("Content-Disposition: form-data; name=\"");
        array.writeUTFBytes(name);
        array.writeUTFBytes("\"");
    }
    protected function writeBody(array:ByteArray):void {

    }
}
internal class HttpFormVariable extends HttpFormValue {
    private var value:String;

    public function HttpFormVariable(name:String, value:String) {
        super(name);
        this.value = value;
    }

    override protected function writeBody(array:ByteArray):void {
        array.writeUTFBytes(value);
    }
}
internal class HttpFormFile extends HttpFormValue {
    private var filename:String;
    private var contentType:String;
    private var content:ByteArray;

    public function HttpFormFile(name:String, filename:String, contentType:String, content:ByteArray) {
        super(name);
        this.filename = filename;
        this.contentType = contentType;
        this.content = content;
    }

    override protected function writeHeader(array:ByteArray):void {
        super.writeHeader(array);
//        array.writeUTFBytes("; filename=\"");
//        array.writeUTFBytes(filename.replace("[^a-zA-Z0-9_ -.]"));
//        array.writeUTFBytes("\"\r\nContent-Type: ");
//        array.writeUTFBytes(contentType);
    }

    override protected function writeBody(array:ByteArray):void {
        content.position = 0;
        var encoder:Base64Encoder = new Base64Encoder();
        encoder.encodeBytes(content, 0, content.length);
        array.writeUTFBytes(encoder.toString());
    }
}
