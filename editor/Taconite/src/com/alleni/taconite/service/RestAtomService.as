package com.alleni.taconite.service
{
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.model.TaconiteObject;
	import com.alleni.taconite.persistence.IAtom;
	import com.alleni.taconite.persistence.IAtomDecoder;
	import com.alleni.taconite.persistence.IAtomEncoder;
	
	import flash.events.EventDispatcher;
    
    /**
     * Simple implementation of ITaconiteDocumentService that persists data using JSON/REST
     * 
     */
    public class RestAtomService extends EventDispatcher implements IAtomService
    {
		private static const ATOM_REST_METHOD:String = "gadget";
		private static const PLAYER_REST_METHOD:String = "player";
		
		protected var _decoder:IAtomDecoder;
		protected var _encoder:IAtomEncoder;
		protected var _path:String = ATOM_REST_METHOD;
		
		private var _metaDataOnly:Boolean;
		
        
        public function RestAtomService(decoder:IAtomDecoder = null, encoder:IAtomEncoder = null, purchased:Boolean=false)
        {
            _decoder = decoder;
            _encoder = encoder;
			if (purchased) _path = PLAYER_REST_METHOD;
        }
		
        /**
         * @inheritDoc 
         */
        public function loadAtom(id:String, published:Boolean, loadVersion:Boolean=false):IOperation
        {
			var data:Object;
			if (published || loadVersion) {
				data = new Object();
				data.sticky = true;
			}
            var operation:IOperation = new RestHttpOperation(_path + "/" + id, data, RestHttpOperation.GET);
			return new JSONAtomFilterOperation(operation, id, _decoder);
        }
        
        /**
         * @inheritDoc 
         */
        public function saveAtom(atom:IAtom, object:TaconiteObject=null, metaDataOnly:Boolean=false, autosave:Boolean=false):IOperation
        {
			_metaDataOnly = metaDataOnly;
			return saveWithDataAndID(_encoder.encode(atom, object, false, autosave), atom.id);
        }
		
		private function saveWithDataAndID(data:*, id:String):IOperation
		{
			var restHttpOp:RestHttpOperation = new RestHttpOperation("");
			var path:String = ATOM_REST_METHOD;
			
			if (Utilities.UUID_REGEX.test(id)) {
				path += "/" + id;
				restHttpOp.method = RestHttpOperation.PUT;
			} else
				restHttpOp.method = RestHttpOperation.POST;
			restHttpOp.path = path;
			restHttpOp.data = data;
			return restHttpOp;
		}
    }
}
