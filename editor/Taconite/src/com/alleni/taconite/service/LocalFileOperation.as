package com.alleni.taconite.service
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.FileReference;

	public class LocalFileOperation extends AbstractOperation
	{
		public function LocalFileOperation()
		{
			super();
		}
		
		public function load(file:FileReference):void
		{
			file.addEventListener(ProgressEvent.PROGRESS, handleFileProgress, false, 0, true);
			file.addEventListener(IOErrorEvent.IO_ERROR, handleFileIOError, false, 0, true);
			file.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleFileSecurityError, false, 0, true);
			file.addEventListener(Event.COMPLETE, handleComplete, false, 0, true);
			file.load();
		}
		
		protected function handleFileProgress(event:ProgressEvent):void 
		{
		}
		
		protected function handleFileIOError(event:IOErrorEvent):void 
		{
			var file:FileReference = FileReference(event.target);
			LogService.error("IO error for file " + file.name + ": " + event.text);
		}
		
		protected function handleFileSecurityError(event:SecurityErrorEvent):void 
		{
			var file:FileReference = FileReference(event.target);
			LogService.error("Security error for file " + file.name + ": " + event.text);
		}
	}
}