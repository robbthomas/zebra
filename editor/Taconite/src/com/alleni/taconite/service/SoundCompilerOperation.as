package com.alleni.taconite.service
{
	import com.alleni.taconite.dev.TaconiteSound;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.system.LoaderContext;
	import flash.utils.ByteArray;

	public class SoundCompilerOperation extends AbstractCompilerOperation
	{
		public function SoundCompilerOperation(bytes:ByteArray)
		{
			super(bytes);
		}
		
		override public function execute():void
		{
			var swf:ByteArray = TaconiteSound.publish(_bytes);
			
			if (swf != null) {
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.INIT, function(event:Event):void {
					CompiledClass = LoaderInfo(event.currentTarget).applicationDomain.getDefinition(TaconiteSound.QNAME) as Class;
					handleComplete(event);		
				});
				LogService.debug("Compiling sound from raw bytes");
				
				// loaderContext necessary since we are loading a SWF, so allowCodeImport must be true to 
				// configure security specific to environment, and its interface differs between Flash and AIR
				var loaderContext:LoaderContext = new LoaderContext();
				loaderContext.allowCodeImport = true;
				loader.loadBytes(swf, loaderContext);
			} else
				handleError(new ErrorEvent(ErrorEvent.ERROR, false, false, "caught exception compiling sound"));
		}
	}
}