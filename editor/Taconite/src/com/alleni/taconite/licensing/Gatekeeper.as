 /**
  * The Gatekeeper is an application license checker which validates using Drupal services
  * TODO: dynamic configuration
  * 
  * @author Patrick Krekelberg et al
  * Copyright Allen Interactions
  * 
  * $Id$  */

package com.alleni.taconite.licensing {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class Gatekeeper extends EventDispatcher {
		static private var _instance:Gatekeeper;
		static private var _appID:int;
		static private var _dru:Drupal;
		
		public function Gatekeeper ()
		{
			if (_instance) throw new Error("Singleton and can only be accessed through Gatekeeper.instance");
		}
	
		static public function get instance():Gatekeeper {
			if (!_instance) {
				_instance = new Gatekeeper();
			}
			return _instance;
		}
		
 		public function check(appID:int):void
		{
			_appID = appID;
			_dru = new Drupal();
			_dru.gatewayUrl = "http://labs.alleni.com/services/amfphp";
			_dru.connect(onConnection);	
		}
		
		private function onConnection():void
		{
			_dru.service(onSuccess, onFault, "gatekeeper.check", _appID);
		}
		
		private function onSuccess(result:Object):void
		{
			if (result.length > 0) {
				// LICENSE FOUND
				dispatchEvent(new Event("licenseFound"));
			} else {
				// NO LICENSE
				dispatchEvent(new Event("licenseDenied"));
			}
		}
		
		private function onFault(e:Error):void
		{
			trace("there has been an error: " + e);	
		}
	}
}