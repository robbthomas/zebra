 /**
  * Drupal CMS communications
  * 
  * @author Patrick Krekelberg et al
  * Copyright Allen Interactions
  * 
  * $Id$  */

package com.alleni.taconite.licensing {
	import flash.net.Responder;
	
	public class Drupal {
		
		public var gatewayUrl:String = "";
		public var apiKey:String = "";
		public var sessionId:String = "";

		private var rs:RemotingService;
		private var callback:Function;
		
		/**
		 * 
		 * @param onSuccess
		 * 
		 */
		public function connect(onSuccess:Function):void {
			callback = onSuccess;
			this.service(onConnect, onFault, "system.connect");
		}

		/**
		 * 
		 * @param onSuccess
		 * @param onFault
		 * @param command
		 * @param args
		 * @return 
		 * 
		 */
		public function service(onSuccess:Function, onFault:Function, command:String, ... args):Boolean {
			if (gatewayUrl.length > 0) {
				rs = new RemotingService(gatewayUrl);
			} else {
				return false;
			}

			var responder:Responder = new Responder(onSuccess, onFault);
			
			if (sessionId.length > 0) {
				args.unshift(sessionId);
			}
			if (apiKey.length > 0) {
				args.unshift(apiKey);
			}
			
			if (args.length==1) {
				rs.call(command, responder, args[0]);
			}
			else if (args.length==2) {
				rs.call(command, responder, args[0], args[1]);
			}
			else if (args.length==3) {
				rs.call(command, responder, args[0], args[1], args[2]);
			}
			else if (args.length==4) {
				rs.call(command, responder, args[0], args[1], args[2], args[3]);
			}
			else if (args.length==5) {
				rs.call(command, responder, args[0], args[1], args[2], args[3], args[4]);
			}
			else if (args.length==8) {
				rs.call(command, responder, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
			} else {
				rs.call(command, responder);
			}
			
			return true;
		}

		private function onConnect(result:Object):void {
			sessionId = result.sessid;
			if (callback is Function) {
				callback();
			}
		}

		private function onFault(error:Object):void {
			trace('ERROR:drupal.Drupal:netconnection:');
			for each (var item:Object in error) {
				trace(item);
			}
		}
	}
}

/*
 * RemotingService for AMFPHP connection in AS3
 */
import flash.net.NetConnection;
import flash.net.ObjectEncoding;
	 
class RemotingService extends NetConnection {
	
	function RemotingService(url:String) {
		// Set AMF version for AMFPHP
		objectEncoding = ObjectEncoding.AMF3;
		// Connect to gateway
		connect(url);
	}
}