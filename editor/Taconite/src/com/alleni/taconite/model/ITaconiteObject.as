package com.alleni.taconite.model
{
	public interface ITaconiteObject
	{
		function get model():TaconiteModel;
		function get modelRoot():ModelRoot;
	}
}