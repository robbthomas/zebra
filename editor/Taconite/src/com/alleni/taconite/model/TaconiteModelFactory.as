package com.alleni.taconite.model
{
    /**
     * <p>A TaconiteModelFactory is an object that can turn a "naked model" into a subclass of
     * TaconiteModel.  Alternative TaconiteModelFactory subclasses may be created and plugged into
     * the system.  This is the way to control what is actually instantiated by the TaconiteModel.forValue()
     * function.
     */
    public class TaconiteModelFactory
    {
        /** The factory used to create new TaconiteModels. */
        public static var instance:TaconiteModelFactory = new TaconiteModelFactory();
        
        /**
         * Create a TaconiteModel for the given value object.  Override this function
         * in subclasses to create an alternate factory implementation.
         */
        public function createModel(value:TaconiteObject):TaconiteModel
        {
            return new TaconiteModel(value);
        }
    }
}
