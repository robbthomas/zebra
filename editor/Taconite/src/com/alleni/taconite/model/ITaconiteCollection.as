package com.alleni.taconite.model {
	import mx.collections.IList;
	
	public interface ITaconiteCollection
	{
		[Transient]
		function get children():IList;
	}
}