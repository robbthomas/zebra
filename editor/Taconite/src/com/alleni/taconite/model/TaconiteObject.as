package com.alleni.taconite.model
{
	import flash.events.EventDispatcher;
	
	import mx.core.IUID;
	import mx.utils.UIDUtil;
	
	public class TaconiteObject extends EventDispatcher implements IUID, ITaconiteObject
	{
		private var _model:TaconiteModel;
	
		private var _uid:String = UIDUtil.createUID();

		private var _group:GroupSelectionObject;

        [Bindable]
        public var groupID:String;

		public var shortClassName:String;
		
		public function set uid(value:String):void
		{
			_uid = value;
		}
		
		public function get uid():String
		{
			return _uid;
		}
	
		[Transient]
		public function hasModel():Boolean
		{
			if(_model)
				return true;
			return false;
		}
	
		[Transient]
		public function get model():TaconiteModel
		{
			if(!_model) {
				_model = TaconiteModelFactory.instance.createModel(this);
			}
			return _model;
		}
	
		[Transient]
		public function get modelRoot():ModelRoot
		{
			if(!_model) {
				_model = new ModelRoot(this);
			}
			return _model as ModelRoot;
		}
		
		[Transient]
		public function get group():GroupSelectionObject {
			return _group;
		}
		
		public function set group(g:GroupSelectionObject):void {
			_group = g;
            if (g != null)
                groupID = g.uid;
            else {
                groupID = null;
            }
		}
	}
}