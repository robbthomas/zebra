package com.alleni.taconite.model
{
import avmplus.getQualifiedClassName;

import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ModelCollectionEvent;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.event.ModelUpdateEvent;
import com.alleni.taconite.model.ITaconiteCollection;
import com.alleni.taconite.model.ITaconiteCollection;

import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import flash.net.registerClassAlias;
import flash.utils.Dictionary;
import flash.utils.getDefinitionByName;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.collections.IList;
import mx.events.CollectionEvent;
import mx.events.CollectionEventKind;
import mx.events.PropertyChangeEvent;
import mx.events.PropertyChangeEventKind;
import mx.utils.ObjectUtil;
import mx.utils.UIDUtil;

[Event(type="mx.events.PropertyChangeEvent",name="propertyChange")]
    [Event(type="com.alleni.taconite.event.ModelEvent",name="modelChange")]
    [Event(type="com.alleni.taconite.event.ModelUpdateEvent",name="modelUpdate")]
    
    /**
     * <p>A TaconiteModel represents an element of a hierarchical model with child nodes and a parent node.
     * It is a wrapper around a pure value object in the underlying application model that need not expose
     * any methods, only properties.   There is a single, distinguished TaconiteModel for each single value
     * object in the underlying application model.</p>
     *   
     * <p>Any property of a value object that can trigger a view change is required to be Bindable.
     * Also, there is a special property of a value object that is presumed to be the set of child value objects
     * in the application model tree, and which must implement the mx.collections.IList interface.
     * By default, this property's name is "children", but may be overridden
     * by defining a static constant TACONITE_CHILDREN_PROPERTY to reference some other property name.</p>
     * 
     * <p>Additionally, some properties may themselves be other value objects in a 1:1 relationship with
     * this object, that can also have child objects of their own.  The special static constant TACONITE_MODEL_PROPERTIES
     * may be defined as an array of such property names.</p>
     */
    public class TaconiteModel extends EventDispatcher
    {
        /** The parent of this model object. */
        private var _parent:TaconiteModel;
        
        /** The set of children of this model object. */
        private var _children:IList;
        
        /** The underlying value object */
        private var _value:TaconiteObject;
        
        /** Value models: properties of the value object that are themselves models. */
        private var _valueModels:Object;

        /** Value model lists: properties of the value object that contain lists of models. */
        private var _valueModelLists:Object;
        
        /** A reverse-lookup weak dictionary from models to their wrappers. */
        private static var _valueMap:Dictionary = new Dictionary(true);
		
		/** non-zero value indicates we are in process of moving a child object **/
		private var _movingChild:uint = 0;
		
		private static var _utilities:Utilities;

		public static function forUID(uid:String):TaconiteModel
		{
			// NOTE:  AbstractContainer.findObjByUID() should be used intead of this function
			// because it properly handles deserialized objects being give old uid values
			
			for ( var key:Object in _valueMap )
				//trace("UID for current object", UIDUtil.getUID(key));
				if (UIDUtil.getUID(key) == uid)
					return key.model;
			return null;
		}
		
		public static function getLength():uint{
			
			var length:uint = 0;
			for ( var key:Object in _valueMap )
				length++;
			
			return length;
				
		}
		

		public static function clearCaches():void
		{
			for ( var key:Object in _valueMap )
				if ("serial" in key || _valueMap[key] as ModelRoot)
					delete _valueMap[key];
		}
		
        /**
         * Create a TaconiteModel that wraps an underlying value object in the application domain.
         * In practice, callers should almost always call TaconiteModel.forValue() rather than
         * invoking this constructor.
         */
        public function TaconiteModel(value:TaconiteObject)
        {
            super();
	        var oldModel:TaconiteModel = forUID(value.uid);
	        if (oldModel)
		        delete _valueMap[oldModel.value];
	        _valueMap[value] = 1;
            _children = new ArrayCollection();
            _valueModels = {};
            _valueModelLists = {};
            _value = value;

            if (!_utilities)
			    _utilities = new Utilities();
			
            if (_value.hasModel())
                throw new Error("Attempt to create duplicate wrapper for " + value);
			
            if (_value is IEventDispatcher)
                IEventDispatcher(_value).addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, handlePropertyChange);

            if (_value is ITaconiteCollection)
            {
                var children:IList = valueChildren;
                if (children != null) {
                    children.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleChildrenChange);
                    for each (var child:TaconiteObject in children)
                        addChild(child.model);
                }
            }
            
            var p:String;
            
            for each (p in valueModelProperties) {
                var propValue:TaconiteObject = _value[p];
                setValueModelProperty(p, propValue);
            }

            for each (p in valueModelListProperties) {    // the "pages" and "masterPages" lists
            	var propList:IList = ArrayCollection(_value[p]);
                setValueModelListProperty(p,  propList);
				propList.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleModelListCollectionChange);
            }
            
            for each (p in valueModelCollections) {
            	var collection:IList = ArrayCollection(_value[p]);
				collection.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleCollectionChange);
            }
        }
        
        /**
         * Get the TaconiteModel for the named value model property. 
         * @param p a property name of the underlying value object.
         */
        public function getValueModel(p:String):TaconiteModel
        {
            return _valueModels[p] as TaconiteModel;
        }

        public function getAllValueModels():Vector.<TaconiteModel> {
            var result:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
            for(var prop:String in _valueModels) {
                if(_valueModels[prop] is TaconiteModel) {
                    result.push(_valueModels[prop]);
                }
            }
            return result;
        }

        public function getAllValueModelLists():Vector.<IList> {
            var result:Vector.<IList> = new Vector.<IList>();
            for(var prop:String in _valueModelLists) {
                if(_valueModelLists[prop] is ArrayCollection) {
                    result.push(_valueModelLists[prop]);
                }
            }
            return result;
        }

        public function getAllValueProperties():Vector.<String> {
            var result:Vector.<String> = new Vector.<String>();
            for(var prop:String in _valueModels) {
                result.push(prop);
            }
            return result;
        }

        public function getAllValueListProperties():Vector.<String> {
            var result:Vector.<String> = new Vector.<String>();
            for(var prop:String in _valueModelLists) {
                result.push(prop);
            }
            return result;
        }

        public function isValueListProperty(name:String):Boolean {
            for(var prop:String in _valueModelLists) {
                if (prop == name)
                    return true;
            }
            return false;
        }

        private function setValueModelProperty(p:String, propValue:TaconiteObject):void
        {
            var oldValueModel:TaconiteModel = _valueModels[p] as TaconiteModel;
            if (oldValueModel != null) {
                oldValueModel.parent = null;
                oldValueModel.removeEventListener(ModelEvent.MODEL_CHANGE, handleChildModelChange);
                oldValueModel.removeEventListener(ModelUpdateEvent.MODEL_UPDATE, handleChildModelUpdate);
            }

            if(propValue == null) {
                _valueModels[p] = null
            } else {
                var valueModel:TaconiteModel = propValue.model;
                _valueModels[p] = valueModel;

                if (valueModel != null) {
                    valueModel.parent = this;
                    valueModel.addEventListener(ModelEvent.MODEL_CHANGE, handleChildModelChange, false, 2);
                    valueModel.addEventListener(ModelUpdateEvent.MODEL_UPDATE, handleChildModelUpdate, false, 2);
                }
            }
        }

        private function setValueModelListProperty(p:String, propValue:IList):void
        {
            var oldValueModelList:IList = _valueModels[p] as IList;
            if (oldValueModelList != null) {
                oldValueModelList.removeEventListener(CollectionEvent.COLLECTION_CHANGE, handleModelListCollectionChange);
                for each(var oldObject:ITaconiteObject in oldValueModelList) {
                    listenValueModelListChild(oldObject);
                }
            }

            if(propValue != null) {
                _valueModelLists[p] = propValue;
                if(propValue != null) {
                    propValue.addEventListener(CollectionEvent.COLLECTION_CHANGE, handleModelListCollectionChange);
                    for each(var newObject:ITaconiteObject in propValue) {
                        unlistenValueModelListChild(newObject);
                    }
                }
            }
        }

        private function listenValueModelListChild(child:ITaconiteObject):void {
            if(child == null) {
                return;
            }
            child.model.parent = this;
            child.model.addEventListener(ModelEvent.MODEL_CHANGE, handleChildModelChange, false, 2);
            child.model.addEventListener(ModelUpdateEvent.MODEL_UPDATE, handleChildModelUpdate, false, 2);
        }

        private function unlistenValueModelListChild(child:ITaconiteObject):void {
            if(child == null) {
                return;
            }
            child.model.parent = null;
            child.model.removeEventListener(ModelEvent.MODEL_CHANGE, handleChildModelChange);
            child.model.removeEventListener(ModelUpdateEvent.MODEL_UPDATE, handleChildModelUpdate);
        }

        /**
         * The underlying value object that backs this TaconiteModel.
         */
        public function get value():TaconiteObject
        {
            return _value;
        }
        
        /**
         * The children of our backing value object if they exist, or null if the object
         * does not appear to maintain a set of children.
         */
        public function get valueChildren():IList
        {
	        return ITaconiteCollection(value).children;
        }
        
        public function getValueCollection(index:uint):IList
        {
            return value[valueModelCollections[index]] as IList;
        }
        
        /**
         * Access the TaconiteModel at some child index. 
         */
        public function getChildAt(index:uint):TaconiteModel
        {
            if (index < _children.length)
                return TaconiteModel(_children.getItemAt(index));
            else
                return null;
        }
        
        /**
         * Utility function to remove a child value object from the value object for this TaconiteModel. 
         * @param valueChild the value object child to be removed.
         */
        public function removeValueChild(valueChild:Object):void
        {
            var index:int = valueChildren.getItemIndex(valueChild);
            if (index >= 0) {
                valueChildren.removeItemAt(index);
            } else {
                for(var valueProp:String in _valueModels) {
                    var valueChildModel:TaconiteModel = _valueModels[valueProp];
                    if(valueChildModel != null && valueChildModel.value == valueChild) {
                        value[valueProp] = null;
                        return;
                    }
                }
            }
        }

        public function removeValueChildAndDescendants(valueChild:TaconiteObject):void
        {
            var parentProp:Array = findPathToValueObj(valueChild);  // model-list containing valueChild with index, or children index, or value property
            assert(parentProp != null);
            // first remove the descendants, recursively
            var childModel:TaconiteModel = valueChild.model;
            if (valueChild is ITaconiteCollection) {
                var children:IList = ITaconiteCollection(valueChild).children;
                if (children != null) {
                    for (var n:int=children.length-1; n >= 0; n--) {
                        childModel.removeValueChildAndDescendants(children.getItemAt(n) as TaconiteObject);
                    }
                }
            }

            // model properties such as eventFlow (no need for this since they only occur on World)
//            for each (var modelProp:TaconiteModel in getAllValueModels()) {
//                valueChild.model.removeValueChildAndDescendants(modelProp.value);
//            }

            // lists of models, such as pages and masterPages
            var childModelLists:Vector.<IList> = valueChild.model.getAllValueModelLists();
            for each (var list:IList in childModelLists) {
                for (var grand:int = list.length-1; grand >= 0; grand--) {
                    var grandChild:TaconiteObject = list.getItemAt(grand) as TaconiteObject; // master-page, when valueChild is arena
                    childModel.removeValueChildAndDescendants(grandChild);
                }
            }

            // now remove the child itself
            if(parentProp[0] is Number && parentProp.length == 1) {
                ITaconiteCollection(value).children.removeItemAt(parentProp[0]);
            } else if(parentProp[0] is String && parentProp[1] is Number && parentProp.length == 2) {
                IList(value[parentProp[0]]).removeItemAt(parentProp[1]);
            } else if(parentProp[0] is String && parentProp.length == 1) {
                value[parentProp] = null;
            } else {
                throw new Error("Invalid result of findPathToValueObj [" + parentProp.join(",") + "] from " + _value + " to " + valueChild);
            }

        }

        public function findPathToValueObj(obj:TaconiteObject):Array
        {
            var property:String;
            if (value is ITaconiteCollection) {
                var childrenIndex:int = (ITaconiteCollection(value).children).getItemIndex(obj);
                if(childrenIndex >= 0) {
                    return [childrenIndex];
                }
            }
            for (property in _valueModelLists) {
                if(_valueModelLists.hasOwnProperty(property) && _valueModelLists[property] is IList) {
                    var valueListIndex:int = IList(_valueModelLists[property]).getItemIndex(obj)
                    if (valueListIndex >= 0) {
                        return [property, valueListIndex];
                    }
                }
            }
            for (property in _valueModels) {
                if(_valueModels.hasOwnProperty(property) && value[property] is TaconiteObject) {
                    if (value[property] === obj) {
                        return [property];
                    }
                }
            }
            return null;
        }

        public function findListContainingValueObj(obj:*):IList
        {
            var list:IList;
            if (valueChildren.getItemIndex(obj) >= 0) {
                return valueChildren;
            }
            for each (list in getAllValueModelLists()) {
                if (list.getItemIndex(obj) >= 0) {
                    return list;
                }
            }
            return null;
        }

        /**
         * The parent TaconiteModel of this one, determined by watching collection change events
         * on the underlying value object tree.
         */
        public function get parent():TaconiteModel
        {
            return _parent;
        }
        
        public function set parent(m:TaconiteModel):void
        {
            _parent = m;
        }
        
        /**
         * The number of children in this TaconiteModel. 
         */
        public function get numChildren():int
        {
            return _children.length;
        }
        
		public var enabled:Boolean = true;  // active for mouse click and rollover
        
        public function getChildIndex(child:TaconiteModel):int
        {
            return _children.getItemIndex(child);
        }

        /**
         * Obtain a deep clone of this model that refers to a cloned underlying value object.
         */
        public function clone():TaconiteModel
        {
			var qualifiedName:String = getQualifiedClassName(value);
        	var cls:Class = Class(getDefinitionByName(qualifiedName));
        	registerClassAlias(qualifiedName, cls);
            return new TaconiteModel(cls(ObjectUtil.copy(value)));
        }

        
        private function handlePropertyChange(e:PropertyChangeEvent):void
        {			
            if (e.kind == PropertyChangeEventKind.UPDATE)
            {
                // If the changed property points to a TaconiteModel, then remove old listeners on the
                // old value and add new listeners on the new.
                // 
                if (valueModelProperties.indexOf(e.property) >= 0) {

                    if(e.newValue is TaconiteObject) {
                        setValueModelProperty(String(e.property), e.newValue as TaconiteObject);
                    }
                } else if(valueModelListProperties.indexOf(e.property) >= 0) {

                    if(e.newValue is IList) {
                        setValueModelListProperty(String(e.property), e.newValue as IList);
                    }
                }
                
                var e2:ModelUpdateEvent =
                    new ModelUpdateEvent(ModelUpdateEvent.MODEL_UPDATE,
                                         e.property, e.oldValue, e.newValue, this);
          		dispatchModelUpdateEvent(e2);
            }
        }
        
        private function handleCollectionChange(e:CollectionEvent):void
        {
            var i:int;
			var property:String = null;
			
			for each (var p:String in valueModelCollections) {
				if(value[p] == e.currentTarget) {
					property = p;
					break;
				}
			}

            switch (e.kind) {
				case CollectionEventKind.ADD:
	                for (i = 0; i < e.items.length; i++)
			             dispatchModelEvent(new ModelCollectionEvent(ModelEvent.MODEL_CHANGE, ModelEvent.ADD_ARRAY_ITEM, false, this, property, e.currentTarget, e.items[i], e.location + i));
	                break;

	            case CollectionEventKind.REMOVE:
                	for (i = e.items.length - 1; i >= 0; i--)
		            	dispatchModelEvent(new ModelCollectionEvent(ModelEvent.MODEL_CHANGE, ModelEvent.REMOVE_ARRAY_ITEM, false, this, property, e.currentTarget, e.items[i], e.location + i));
               		break;
				
				case CollectionEventKind.REPLACE:
					for (i = 0; i < e.items.length; i++)
						dispatchModelEvent(new ModelCollectionEvent(ModelEvent.MODEL_CHANGE, ModelEvent.CHANGE_ARRAY_ITEM, false, this, property, e.currentTarget, PropertyChangeEvent(e.items[i]).newValue, e.location+i, PropertyChangeEvent(e.items[i]).oldValue));
					break;
				
				case CollectionEventKind.MOVE:
					trace("	>> FORMERLY UNPLANNED FOR ARRAY MOVE: {Model:"+this+",property:"+property+",target:"+e.currentTarget+",old value:"+PropertyChangeEvent(e.items[i]).oldValue+"new value:"+PropertyChangeEvent(e.items[i]).newValue+"," +
						"old location:"+e.oldLocation+",new location:",e.location+"}");
					for (i = 0; i < e.items.length; i++)
						dispatchModelEvent(new ModelCollectionEvent(ModelEvent.MODEL_CHANGE, ModelEvent.CHANGE_ARRAY_ITEM, false, this, property, e.currentTarget, PropertyChangeEvent(e.items[i]).newValue, e.location+i, PropertyChangeEvent(e.items[i]).oldValue));
					break;
                
        	    case CollectionEventKind.UPDATE:
	                for (i = 0; i < e.items.length; i++)
						if (e.location >= 0)
				            dispatchModelEvent(new ModelCollectionEvent(ModelEvent.MODEL_CHANGE, ModelEvent.CHANGE_ARRAY_ITEM, false, this, property, e.currentTarget, PropertyChangeEvent(e.items[i]).newValue, e.location+i, PropertyChangeEvent(e.items[i]).oldValue));
	                break;
                
            	default:
                	throw new Error("Unexpected CollectionEventKind: " + e.kind);
            }
            
            return;
        }
        
        private function handleChildrenChange(e:CollectionEvent):void
        {
            var i:int;

            switch (e.kind) {
 	           	case CollectionEventKind.ADD:
	                for (i = 0; i < e.items.length; i++)
	                    addChildAt(TaconiteObject(e.items[i]).model, e.location + i);
	                break;

	            case CollectionEventKind.REMOVE:
	                for (i = e.items.length - 1; i >= 0; i--)
	                    removeChildAt(e.location + i);
	                break;

    	        case CollectionEventKind.REPLACE:
	                removeChildAt(e.location, true);
	                addChildAt(TaconiteObject(valueChildren.getItemAt(e.location)).model, e.location, true);
	                break;
				
				case CollectionEventKind.MOVE:
					trace("	>> FORMERLY UNPLANNED FOR ARRAY MOVE: {Model:"+this+",items:"+e.items+",target:"+e.currentTarget+"," +
						"old location:"+e.oldLocation+",new location:",e.location+"}");
					break;
                
        	    case CollectionEventKind.UPDATE:
	                // we don't care
	                break;
                
				case CollectionEventKind.REFRESH:
					break;
	                break;
				
            	default:
                	throw new Error("Unexpected CollectionEventKind: " + e.kind);
            }
            
            return;
        }

        private function handleModelListCollectionChange(e:CollectionEvent):void
        {
            var i:int;
			var property:String = null;

			for each (var p:String in valueModelListProperties) {
				if(value[p] == e.currentTarget) {
					property = p;
					break;
				}
			}

            var item:ITaconiteObject;

            switch (e.kind) {
				case CollectionEventKind.ADD:
	                for (i = 0; i < e.items.length; i++) {
                        item = e.items[i] as ITaconiteObject;
                        listenValueModelListChild(item);
			            dispatchModelEvent(new ModelCollectionEvent(ModelEvent.MODEL_CHANGE, ModelEvent.ADD_ARRAY_ITEM, _movingChild, this, property, e.currentTarget, e.items[i], e.location + i));
                    }
	                break;

	            case CollectionEventKind.REMOVE:
                	for (i = e.items.length - 1; i >= 0; i--) {
                        item = e.items[i] as ITaconiteObject;
                        if (!_movingChild)
                            unlistenValueModelListChild(item);
		            	dispatchModelEvent(new ModelCollectionEvent(ModelEvent.MODEL_CHANGE, ModelEvent.REMOVE_ARRAY_ITEM, _movingChild, this, property, e.currentTarget, e.items[i], e.location + i));
                    }
               		break;

				case CollectionEventKind.REPLACE:
					for (i = 0; i < e.items.length; i++) {
                        item = PropertyChangeEvent(e.items[i]).oldValue as ITaconiteObject;
                        unlistenValueModelListChild(item);
                        item = PropertyChangeEvent(e.items[i]).newValue as ITaconiteObject;
                        listenValueModelListChild(item);
						dispatchModelEvent(new ModelCollectionEvent(ModelEvent.MODEL_CHANGE, ModelEvent.CHANGE_ARRAY_ITEM, _movingChild, this, property, e.currentTarget, PropertyChangeEvent(e.items[i]).newValue, e.location+i, PropertyChangeEvent(e.items[i]).oldValue));
                    }
					break;

				case CollectionEventKind.MOVE:
					trace("	>> FORMERLY UNPLANNED FOR ARRAY MOVE: {Model:"+this+",property:"+property+",target:"+e.currentTarget+",old value:"+PropertyChangeEvent(e.items[i]).oldValue+"new value:"+PropertyChangeEvent(e.items[i]).newValue+"," +
						"old location:"+e.oldLocation+",new location:",e.location+"}");
					for (i = 0; i < e.items.length; i++) {
                        item = PropertyChangeEvent(e.items[i]).oldValue as ITaconiteObject;
                        unlistenValueModelListChild(item);
                        item = PropertyChangeEvent(e.items[i]).newValue as ITaconiteObject;
                        listenValueModelListChild(item);
						dispatchModelEvent(new ModelCollectionEvent(ModelEvent.MODEL_CHANGE, ModelEvent.CHANGE_ARRAY_ITEM, _movingChild, this, property, e.currentTarget, PropertyChangeEvent(e.items[i]).newValue, e.location+i, PropertyChangeEvent(e.items[i]).oldValue));
                    }
					break;

        	    case CollectionEventKind.UPDATE:
	                // don't care. Already listening for these directly
	                break;

            	default:
                	throw new Error("Unexpected CollectionEventKind: " + e.kind);
            }

            return;
        }
        
        private function handleChildModelChange(e:ModelEvent):void
        {
            // do event bubbling to our parent model
            dispatchModelEvent(e);
        }
        
        private function handleChildModelUpdate(e:ModelUpdateEvent):void
        {
            // do event bubbling to our parent model
			e.parent = this;
            dispatchModelUpdateEvent(e);
        }
        
        private function dispatchModelEvent(e:ModelEvent):void
        { 
            dispatchEvent(e);
        }

        private function dispatchModelUpdateEvent(e:ModelUpdateEvent):void
        {
            dispatchEvent(e);
        }

        /**
         * Get the names of the properties that hold the value's descendant models
         */
        private function get valueModelProperties():Vector.<String>
        {
            return getMetadataProperty("TACONITE_MODEL_PROPERTIES", new Vector.<String>());
        }

        /**
         * Get the names of the properties that hold the value's descendant models
         */
        private function get valueModelListProperties():Vector.<String>
        {
            return getMetadataProperty("TACONITE_MODEL_LIST_PROPERTIES", new Vector.<String>());
        }

        /**
         * Get the names of any array collections within the value we need to monitor
         */
        private function get valueModelCollections():Vector.<String>
        {
            return getMetadataProperty("TACONITE_COLLECTIONS", new Vector.<String>());
        }
        
        /**
         * Get the defaulted value of a metadata property expressed as a static const attached to the value object's class. 
         */
        private function getMetadataProperty(name:String, defaultValue:*):*
        {
            var cls:Object = getDefinitionByName(getQualifiedClassName(_value));
            if (name in cls)
                return cls[name];
            return defaultValue;
        }

        private function addChild(child:TaconiteModel):void
        {
            addChildAt(child, _children.length);
        }
        
        private function addChildAt(child:TaconiteModel, index:uint, replacing:Boolean=false):void
        {
			if(child == null) return;
            child._parent = this;
			if (!replacing) {
		        child.addEventListener(ModelEvent.MODEL_CHANGE, handleChildModelChange, false, 2);
    		    child.addEventListener(ModelUpdateEvent.MODEL_UPDATE, handleChildModelUpdate, false, 2);
			}
            _children.addItemAt(child, index);
			
			dispatchModelEvent(new ModelEvent(ModelEvent.MODEL_CHANGE, ModelEvent.ADD_CHILD_MODEL, _movingChild, this, child, index));
        }

        private function removeChild(child:TaconiteModel):void
        {
            var index:int = _children.getItemIndex(child);
            if (index >= 0)
                removeChildAt(index);
        }
        
        private function removeChildAt(index:uint, replacing:Boolean=false):void
        {
            var child:TaconiteModel = getChildAt(index);
			if(child == null) return;
			
			// give advance notice, so document can deselect obj & descendants before structure changes
			if (!_movingChild)
				dispatchModelEvent(new ModelEvent(ModelEvent.MODEL_CHANGE, ModelEvent.REMOVING_CHILD_MODEL, false, this, child, index));
			
			if (!replacing) {
				if (!_movingChild)
					child._parent = null;
				child.removeEventListener(ModelEvent.MODEL_CHANGE, handleChildModelChange);
				child.removeEventListener(ModelUpdateEvent.MODEL_UPDATE, handleChildModelUpdate);
			}
			
            _children.removeItemAt(index);
			
			dispatchModelEvent(new ModelEvent(ModelEvent.MODEL_CHANGE, ModelEvent.REMOVE_CHILD_MODEL, _movingChild || replacing, this, child, index));
        }


        public function setChildIndex( child:ITaconiteObject, index:int ):void
        {
//            trace("setChildIndex parent="+_value, "child="+child, "index="+index);
            var childModel:TaconiteModel = child.model;

            var targetList:IList = findListContainingValueObj(child);  // model-list containing valueChild  (null means valueChild is in valueChildren)
            if (targetList.getItemIndex(child) == index) {
                return;  // already at the desired position
            }

            moveChildOutofList(childModel, targetList);
            moveChildIntoList(childModel, index, targetList);
        }

		public function changeParent(newParent:TaconiteModel, listName:String = null):void
		{
//            trace("changeParent obj="+_value, "newParent="+newParent.value, "oldParent="+parent);
            assert(newParent != this);
            var oldList:IList = parent.findListContainingValueObj(value);
            var newList:IList = newParent.valueChildren;
            if (listName != null) {
                newList = newParent.value[listName];
            }
            parent.moveChildOutofList(this, oldList);  // childModel=this
            newParent.moveChildIntoList(this, newParent.numChildren, newList);  // index=numChildren (add at front)
		}
		
		public function moveChildOutofList(childModel:TaconiteModel, list:IList=null):void
		{
//			trace("TaconiteModel::moveChildOutofList: parent="+this.value, "child="+childModel.value);
			assert(childModel.parent == this);
            if (list == null) {
                list = valueChildren;
            }
			
			// find the current position of the item
			var oldPos:int = (list) ? list.getItemIndex(childModel.value) : valueChildren.getItemIndex(childModel.value);
			assert(oldPos >= 0);
			
			++_movingChild; // signal a non-volatile operation

            list.removeItemAt(oldPos);
			childModel._parent = null;
			
			--_movingChild; // restore normal operation

            assert(list.getItemIndex(childModel.value) == -1);
		}
		
		/**
		 * Basic function of structural change:  move child into this model's list of children.
		 * This function is invoked by Undo. 
		 * @param childModel the child to be moved.
		 * @param index the desired position for the child.
		 * 
		 */
		public function moveChildIntoList(childModel:TaconiteModel, index:int, list:IList=null):void
		{
//			trace("TaconiteModel::moveChildIntoList parent="+this.value, "child="+childModel.value, "index="+index);
			assert(index >= 0);
			assert(childModel.parent == null);
			assert(childModel != this);
            if (list == null) {
                list = valueChildren;
            }
            assert(list.getItemIndex(childModel.value) == -1);

			++_movingChild; // signal a non-volatile operation

            list.addItemAt(childModel.value, index);

			--_movingChild; // restore normal operation
		}
		
		public function isDescendant(parentModel:TaconiteModel):Boolean
		{
			if (parentModel) {
				var test:TaconiteModel = this;
				while (test) {
					if (test.parent == parentModel)
						return true;
					else
						test = test.parent;
				}
			}
			return false;
		}

        private function assert(condition:*):void
        {
            if (condition==null || condition==false)
                throw new Error("TaconiteModel");
        }

        override public function toString():String
        {	
        	return "[TaconiteModel "+value+"]";
        }
    }
}
