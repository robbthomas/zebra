package com.alleni.taconite.model
{	
	import com.alleni.taconite.document.ISelection;
	import flash.geom.Rectangle;

	public class GroupSelectionObject extends TaconiteObject
	{
		public var draggingHandle:Boolean;
		public var x:Number=0;
		public var y:Number=0;
		public var width:Number=0;
		public var height:Number=0;
		public var rotation:Number=30;
		
		private var _selection:ISelection;
		
		public function GroupSelectionObject(selection:ISelection)
		{
			_selection = selection;
			super();
		}
		
		public function get selection():ISelection {
			return _selection;
		}
		
		public function setGeometry(rect:Rectangle):void {
			if (rect) {
				x = rect.x;
				y = rect.y;
				width = rect.width;
				height = rect.height;
			}
		}
	}
}