package com.alleni.taconite.model
{
    /**
     * A ModelRoot is used as the superclass for any root element of a hierarchical model. 
     */
    public class ModelRoot extends TaconiteModel
    {
        public function ModelRoot(model:TaconiteObject)
        {
            super(model);
        }
    }
}
