package com.alleni.taconite.model
{
	public class Notification
	{
		public var name:String;
		public var data:Object;
		
		public function Notification(name:String, data:Object = null)
		{
			this.name = name;
			this.data = data;
		}
	}
}