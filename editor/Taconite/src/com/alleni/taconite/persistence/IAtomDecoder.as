package com.alleni.taconite.persistence
{
	
    
    /**
     * Interface implemented by service-layer helper objects that convert from a service operation's
     * result into a root model object. 
     */
    public interface IAtomDecoder
    {
		function decode(encoded:*, id:String):AtomData
    }
}