package com.alleni.taconite.persistence
{
	public interface IAtom
	{
		function get id():String;
		function get content():Object;
		function set content(value:Object):void;
		function toJson():Object;
	}
}