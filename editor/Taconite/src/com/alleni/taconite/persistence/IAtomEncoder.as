package com.alleni.taconite.persistence
{
	import com.alleni.taconite.model.TaconiteObject;
	
	/**
     * Interface implemented by service-layer helper objects that convert from a root model object
     * into a data type that can be passed to a service operation.
	 * @param specificObjects an optional array of objects to be encoded.
     */
    public interface IAtomEncoder
    {
        function encode(atom:IAtom, object:TaconiteObject=null, metaDataOnly:Boolean=false, autosave:Boolean=false):*;
    }
}