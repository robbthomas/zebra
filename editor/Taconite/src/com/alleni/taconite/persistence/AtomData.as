package com.alleni.taconite.persistence
{
	import com.alleni.taconite.model.ITaconiteCollection;

	public class AtomData
	{
		private var _root:IAtom;
		private var _children:ITaconiteCollection;
		
		public function AtomData(root:IAtom, children:ITaconiteCollection)
		{
			_root = root;
			_children = children;
		}
		
		public function get root():IAtom
		{
			return _root;
		}
		
		public function get children():ITaconiteCollection
		{
			return _children;
		}
	}
}