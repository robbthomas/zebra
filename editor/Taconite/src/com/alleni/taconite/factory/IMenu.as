/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.factory
{
	import flash.events.EventDispatcher;
	
	import mx.collections.XMLListCollection;
	
	public interface IMenu
	{
		function get menu():EventDispatcher;
		function set dataProvider(value:XMLListCollection):void;
		function set labelField(value:String):void;
		function set keyEquivalentField(value:String):void;
		function set callback(value:Function):void;
		function set enabled(value:Boolean):void
	}
}