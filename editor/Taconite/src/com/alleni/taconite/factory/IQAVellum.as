package com.alleni.taconite.factory
{
	import flash.display.DisplayObject;

	public interface IQAVellum
	{
		function close():void;
		function clear():void;
		function get visible():Boolean;
		function set visible(value:Boolean):void;
		function get displayObject():DisplayObject;
	}
}