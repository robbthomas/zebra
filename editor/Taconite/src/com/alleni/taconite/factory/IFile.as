package com.alleni.taconite.factory
{
	import flash.net.FileReference;

	public interface IFile
	{
		function get url():String;
		function set url(value:String):void;
		function get extension():String;
		function get isDirectory():Boolean;
		function write(data:*):void;
		function writeToStorage(data:*, path:String):void;
		function browseForOpen(title:String, extension:String=null):void;
		function browseForSave(title:String):void;
		function resolvePath(path:String):FileReference;
	}
}