package com.alleni.taconite.factory
{
	import flash.net.FileReference;
	import flash.system.LoaderContext;
	
	public interface IEnvironment
	{
		function get guestUsername():String;
		function get guestPassword():String;
		function get accountId():String;
		function set accountId(value:String):void;
		function get accountName():String;
		function set accountName(value:String):void;
		function get companyName():String;
		function set companyName(value:String):void;
		function get userId():String;
		function set userId(value:String):void;
		function get username():String;
		function set username(value:String):void;
		function get userFullName():String;
		function set userFullName(value:String):void;
		function get password():String;
		function set password(value:String):void;
		function get email():String;
		function set email(value:String):void;
        function get customerType():String;
        function set customerType(value:String):void;
		function set serverName(value:String):void;
		function get serverName():String;
		function get apiURI():String;
		function get mediaStreamURI():String;
		function get invokedProjectID():String;
		function get invokedPurchaseID():String;
		function get fromLMS():Boolean;
		function get lmsValues():Object;
		function set invokedFile(value:FileReference):void;
		function get invokedFile():FileReference;
		function get invokedAsEditor():Boolean;
        function get showHistory():Boolean;
        function get projectName():String;
		function get isDev():Boolean;
		function get loaderContext():LoaderContext;
		function get containerDetails():String;
		function get isAdmin():Boolean;
		function set isAdmin(value:Boolean):void;
		function get locale():String;
		function getDetailsURIForPublishedId(publishedId:String):String;
		function getEmbedURIForPurchaseId(purchaseId:String=null):String;
		function get hasDomainCapabilities():Boolean;
		function requestDetailsPanel(id:String, owned:Boolean):void;
		function requestSharingDialog(publishedId:String, userAwarePurchaseId:String):void;
        function get targetUserName():String;
        function get targetProjectName():String;
        function handleError(message:String):void;
        function addExternalCallback(name:String, callback:Function):void;
        function callExternal(functionName:String, ...parameters):*;
        function get useCustomProjectName():Boolean;
        function get newProjectNameFunc():Function;
	}
}
