package com.alleni.taconite.factory
{
	import flash.display.BitmapData;
	import flash.display.InteractiveObject;
	import flash.geom.Point;

	public interface ISystemClipboard
	{
		function hasFormat(format:String):Boolean;
		function getData(format:String, transferMode:String="originalPreferred"):Object;
		function setData(format:String, data:Object, serializable:Boolean=true):Boolean;
		function clear():void;
		function acceptDragDrop(target:InteractiveObject):void;
		function doDrag(dragInitiator:InteractiveObject, dragImage:BitmapData=null, offset:Point=null):void;
		
		// clipboard formats
		function get BITMAP_FORMAT():String;
		function get FILE_LIST_FORMAT():String;
		function get HTML_FORMAT():String;
		function get RICH_TEXT_FORMAT():String;
		function get TEXT_FORMAT():String;
		function get TEXT_SCRAP():String;
		function get URL_FORMAT():String;
		function get OBJECTS_FORMAT():String;
		
		// native drag events
		function get DRAG_ENTER():String;
		function get DRAG_OVER():String;
		function get DRAG_DROP():String;
		function get DRAG_COMPLETE():String;
	}
}