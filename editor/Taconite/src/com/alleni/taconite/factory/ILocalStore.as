package com.alleni.taconite.factory
{
	import flash.utils.ByteArray;

	public interface ILocalStore
	{
		function getItem(name:String):ByteArray;
		function setItem(name:String, data:ByteArray, stronglyBound:Boolean=false):void;
	}
}