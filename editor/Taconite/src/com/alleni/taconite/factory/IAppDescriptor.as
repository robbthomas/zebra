package com.alleni.taconite.factory
{
	import flash.geom.Rectangle;

	public interface IAppDescriptor
	{
		function get id():String;
		function get name():String;
		function get version():String;
		function get longName():String;
		function get screenBounds():Rectangle;
		function get mainScreenBounds():Rectangle;
	}
}