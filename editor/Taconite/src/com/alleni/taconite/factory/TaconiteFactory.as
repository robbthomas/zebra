/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.factory
{
	import flash.events.Event;
	import flash.system.ApplicationDomain;
	import flash.system.Security;
	
	import mx.managers.ICursorManager;
	
	public class TaconiteFactory
	{	
		static private var _instance:TaconiteFactory;

		public function TaconiteFactory():void
		{
			if (_instance) throw new Error("Singleton and can only be accessed through ClassName.instance");			
		}
		
		static public function get instance():TaconiteFactory
		{
			if (!_instance)
				_instance = new TaconiteFactory();
			return _instance;
		}
		
		static public function getFileImplementation(file:Object=null):IFile
		{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.FileImplementation");
			return new clsToCreate(file);
		}
		
		static public function getMenuImplementation():IMenu
		{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.MenuImplementation");
			return new clsToCreate();
		}
		
		static public function getQAVellumImplementation(qaPalette:ITaconiteWindow):IQAVellum
		{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.QAVellumImplementation");
			return new clsToCreate(qaPalette);
		}
		
		static public function getStorageImplementation():IStorage
		{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.StorageImplementation");
			return new clsToCreate();
		}
		
		static public function getWindowImplementation():ITaconiteWindow
		{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.WindowImplementation");
			return new clsToCreate();
		}
		
		static public function getClipboardImplementation(e:Event=null):ISystemClipboard
		{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.ClipboardImplementation");
			return new clsToCreate(e);
		}
		
		static public function getAppDescriptorImplementation():IAppDescriptor
		{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.AppDescriptorImplementation");
			return new clsToCreate();
		}
		
		static public function getEnvironmentImplementation():IEnvironment
		{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.EnvironmentImplementation");
			return new clsToCreate();
		}
		
		static public function getLocalStoreImplementation():ILocalStore
		{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.LocalStoreImplementation");
			return new clsToCreate();
		}
		
		static public function getCursorManagerImplementation():ICursorManager{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.CursorManagerImplementation");
			return new clsToCreate();
		}

		static public function getSerializerImplementation():ISerializer{
			var clsToCreate:Class = getClassToCreate("com.alleni.taconite.factory.SerializerImplementation");
			return new clsToCreate();
		}
		
		static protected function getClassToCreate(className:String):Class
		{
			return ApplicationDomain.currentDomain.getDefinition(className) as Class;
		}
				
		/**
		 * Check whether or not we are in AIR
		 * @return Boolean value, true if we are in AIR framework 
		 * 
		 */
		static public function get isAir():Boolean
		{			
			//	"application" is only defined in the AIR framework
			//	we can't use statically compiled constant Security.APPLICATION as it will not compile out of AIR.
			return Security.sandboxType.toString() == "application" ? true : false;
		}	
	}
}