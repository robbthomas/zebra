package com.alleni.taconite.factory {
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.model.TaconiteObject;

public interface ISerializer {

	function serialize(m:TaconiteModel):Object;

	function deserialize(json:Object, parent:TaconiteObject, index:int):TaconiteModel;
}
}