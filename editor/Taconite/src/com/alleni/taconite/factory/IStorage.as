/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.factory
{
	import flash.display.DisplayObject;
	
	/**
	 * Factory interface for setting up either Desktop or Web-deployed application
	 * 
	 */
	public interface IStorage
	{
		function openFile(context:DisplayObject):void;
		function saveFile(context:DisplayObject, saveAs:Boolean=false):void;
	}
}