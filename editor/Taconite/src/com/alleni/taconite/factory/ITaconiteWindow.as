/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.factory
{
	import flash.display.DisplayObject;
	import flash.display.IBitmapDrawable;
	import flash.events.IEventDispatcher;
import flash.geom.Point;
import flash.geom.Rectangle;
	
	import mx.core.IVisualElement;
	import mx.managers.ICursorManager;

	public interface ITaconiteWindow extends IEventDispatcher
	{
		function open(bounds:Rectangle, title:String="Window", resizable:Boolean=true, utility:Boolean=false, hideInitially:Boolean=false):void;
		function close():void;
		function exit():void;
		function requestNewWindowAndInvokeId(id:String, newWindowSize:Object = null):void;
		function get visible():Boolean;
		function set visible(value:Boolean):void;
        function set bounds(bounds:Rectangle):void;
        function get globalPosition():Point;
        function set globalPosition(point:Point):void;
		function get originX():Number;
		function get originY():Number;
		function get width():Number;
        function set width(value:Number):void;
		function get height():Number;
        function set height(value:Number):void;
		function sendToBack():void;
		function set closeCallback(closeCallBackFunction:Function):void;
		function set exitWindow (value:Boolean):void;
		function addElement(child:IVisualElement):IVisualElement;
		function set menu(value:*):void;
		function get title():String;
		function set title(value:String):void;
		function get cursorManager():ICursorManager;
		function get displayObject():DisplayObject;
		function set isPopup(value:Boolean):void;
		
		// native window events
		function get WINDOW_ACTIVATE():String;
		function get WINDOW_COMPLETE():String;
		function get WINDOW_DEACTIVATE():String;
	}
}