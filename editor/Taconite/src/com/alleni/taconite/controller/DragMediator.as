/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.controller
{
    import com.alleni.taconite.lang.TaconiteTimer;
    import com.alleni.taconite.view.ViewContext;
    
    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;
	

    /**
     * An abstract mediator handling the body of a drag gesture, which calls various
     * abstract methods to handle events within the drag.
     */    
    public class DragMediator
    {
		private static const TIME_TO_HOLD:int = 100;
		
		public var inertial:Boolean = false;
		protected var _mouseMoveTimestamp:Number;
		protected static var _taconiteTimer:TaconiteTimer;
        
		protected var _context:ViewContext;
        protected var _dragPoint:Point;
        protected var _dragAllowed:Boolean = true;
        protected var _dragStarted:Boolean = false;
		protected var _stopPropagation:Boolean = true;
		
        /**
         * A distance that the mouse must move from the starting point to be considered an actual drag gesture,
         * as opposed to a click. 
         */
        public static var minimumDrag:Number = 3;
        
        public function DragMediator(context:ViewContext, stopPropagation:Boolean=true)
        {
            _context = context;
			_stopPropagation = stopPropagation;
			_taconiteTimer = TaconiteTimer.instance;
        }
        
        /**
         * Handle the event at the start of the drag. 
         * All coordinates used are Stage coordinates, so it doesn't matter what the mouse event's
         * target is.
         */
        public function handleMouseDown(e:MouseEvent):void
        {
			_context.info.userActionInProgress = true;
			
			if (!_context.stage)
				return;
            _dragStarted = false;
            
            _dragPoint = new Point(_context.stage.mouseX, _context.stage.mouseY);
            _context.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
            _context.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			if (_stopPropagation)
            	e.stopPropagation();
        }
		
        protected function get context():ViewContext
        {
            return _context;
        }        
        
        /**
         * Point at which the drag or click began, in stage coordinates.
         */
        protected function get dragPoint():Point
        {
            return _dragPoint;
        }
        
         /**
         * The vector from the start of the drag to its endpoint in documentViewUnits. 
         */        
        protected function get documentDragDelta():Point
        {
           	throw new Error("documentDragDelta getter must be overridden.");
			/* Put this into the overridden class
			var point:Point = new Point(ApplicationUI.instance.presentationView.mouseX, ApplicationUI.instance.presentationView.mouseY);
			return point.subtract(_dragPoint);
			*/
        }
        
        /**
         * Handle mouse motion during the drag by initiating it if necessary when the mouse
         * exceeds the distance threshold, and then calling the move-handling function. 
         */
        protected function handleMouseMove(e:MouseEvent):void
        {
			if (!_dragStarted
                && e.buttonDown
                && _dragPoint != null
				&& (Math.abs(_context.stage.mouseX -_dragPoint.x) >= minimumDrag
					|| Math.abs( _context.stage.mouseY -_dragPoint.y) >= minimumDrag)) {
                _dragStarted = true;
                handleDragStart(e);
            }
            
            if (_dragStarted)
                handleDragMove(e);

			if(_stopPropagation)
				e.stopPropagation();
			
			_mouseMoveTimestamp = _taconiteTimer.milliseconds;
        }
        
        /**
         * Handle the end of the drag or click gesture. 
         */
        protected function handleMouseUp(e:MouseEvent):void
        {
			_context.info.userActionInProgress = false;
			
			if (_dragStarted) {
				var withMomentum:Boolean = this.inertial && _taconiteTimer.milliseconds - _mouseMoveTimestamp < TIME_TO_HOLD;
				
                handleDragMove(e, withMomentum);
				if (!withMomentum){
					handleDragEnd(e);
                }
            } else {
                handleClick(e);
            }
			if(_stopPropagation)
				e.stopPropagation();
			if (!_context.stage) return;
            _context.stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
            _context.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
        }

		protected function requestInertial(view:DisplayObject, func:Function, event:MouseEvent, displacement:Point):void
		{
			var dragPoint:Point = new Point(event.stageX, event.stageY);
			var referencePoint:Point = _dragPoint;
			var i:int = 0;
			var decelerationFactor:Number;
			view.addEventListener(Event.ENTER_FRAME, function():void {
				if (!displacement
						|| _dragPoint != referencePoint
						|| displacement && (Math.abs(displacement.x) < 0.5 && Math.abs(displacement.y) < 0.5)) {
					view.removeEventListener(Event.ENTER_FRAME, arguments.callee);
					return;
				}
				decelerationFactor = 1 - (0.5 - Math.cos((++i/100)*Math.PI)/2); // ease out, starting near 1 and working toward 0
				dragPoint = new Point(dragPoint.x + displacement.x*decelerationFactor, dragPoint.y + displacement.y*decelerationFactor);
				displacement = func(event, dragPoint);
			});
		}
		
        ///////////////////////
        // ABSTRACT METHODS
        ///////////////////////

        /**
         * Handle the start of a drag gesture.
         */
        protected function handleDragStart(e:MouseEvent):void
        {
        }

        /**
         * Handle an intermediate position of a drag gesture. 
         */
        protected function handleDragMove(e:MouseEvent, doneDragging:Boolean=false):void
        {
        }

        /**
         * Handle the end of a drag gesture. 
         */
        protected function handleDragEnd(e:MouseEvent):void
        {
        }
        
        /**
         * Handle the termination of a gesture during which the mouse effectively did not move. 
         */
        protected function handleClick(e:MouseEvent):void
        {
        }
    }
}
