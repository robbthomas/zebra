package com.alleni.taconite.controller
{
	import com.alleni.taconite.controller.EventController.LEC;
	
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;

	public class NotificationController
	{	
		private var _lec:LEC = new LEC;
		private var _obj:IEventDispatcher;
		
		public function NotificationController(obj:IEventDispatcher)
		{
			_obj = obj;
		}
		
		public function addEventListener(type:String, listener:Function, useCapture:* = false, priority:int = 0, useWeakReference:Boolean = false):Boolean
		{
			return _lec.add(_obj,type,listener,useCapture,priority,useWeakReference, "UNCLUSTERED", true); // clients of NC need to add their own listener
		}
		
		public function removeEventListener(type:String=null, listener:Function=null, useCapture:Boolean = false):Array
		{
			return _lec.remove(_obj,type,listener,useCapture);
		}
		

		public function removeEvent(type:String, listener:Function, useCapture:Boolean = false):Object
		{
			return _lec.removeEvent(_obj,type,listener,useCapture);
		}
		
		public function removeAllEvents():Array
		{
			return _lec.removeObjEvents(_obj);
		}

		public function dump():void
		{
			_lec.dump(_obj);
		}
	}
}