package com.alleni.taconite.controller
{
	import com.alleni.taconite.factory.ITaconiteWindow;

	public interface IWindowListener
	{
		function set window(value:ITaconiteWindow):void;
	}
}