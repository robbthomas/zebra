/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: DragMediator.as 1837 2009-08-28 22:01:28Z pkrekelberg $  */

package com.alleni.taconite.controller
{
	import com.alleni.taconite.definition.ObjectDefinition;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.service.LogService;

	public class ChildControllerFactory
	{
		private static var _instance:ChildControllerFactory;
		private var _utilities:Utilities;
		
		public function ChildControllerFactory():void
		{
			Utilities.assert(!_instance);
			_utilities = new Utilities();
		}
		
		public static function get instance():ChildControllerFactory
		{
			if (!_instance)
				_instance = new ChildControllerFactory();
			return _instance;
		}
		
        /**
         * Create a new controller for a particular model with this description 
         * @param model 
         * @param childDefintion
         * @return a new child controller for the model
         * 
         */
        public function createForModel(model:TaconiteModel, childDefinition:Vector.<ObjectDefinition>):IChildController
        {
        	var shortClassName:String = _utilities.shortClassName(model.value);
        	
        	var descriptionPointer:ObjectDefinition = childDefinition.filter(byShortClassName(shortClassName))[0];
        	if (descriptionPointer != null) {
        		//trace("Controller requested for " + shortClassName + " class=" + descriptionPointer.controller + " instance=" + descriptionPointer.controller.instance);
        		return IChildController(descriptionPointer.controller.instance);
        	} else {
				LogService.error("No controller found for model type: " + shortClassName);
        	}
        	return null;
       	}
		
		private static function byShortClassName(shortClassName:String):Function
		{
			return function(item:ObjectDefinition, index:int, vector:Vector.<ObjectDefinition>):Boolean
			{
				return (item.name == shortClassName);
			}
		}
	}
}