package com.alleni.taconite.controller
{
	import com.alleni.taconite.definition.ObjectDefinition;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.document.ISelection;
	import com.alleni.taconite.document.ObjectSelection;
	import com.alleni.taconite.document.TaconiteDocument;
	import com.alleni.taconite.event.ControllerEvent;
	import com.alleni.taconite.event.SelectEvent;
	import com.alleni.taconite.factory.ISystemClipboard;
	import com.alleni.taconite.factory.TaconiteFactory;
	import com.alleni.taconite.model.ModelRoot;
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.display.InteractiveObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	
	import flashx.textLayout.edit.TextScrap;
	
	[Event(name="documentChange",type="com.alleni.taconite.event.ControllerEvent")]
	[Event(name="addSelection",type="com.alleni.taconite.event.SelectEvent")]
	[Event(name="removeSelection",type="com.alleni.taconite.event.SelectEvent")]
	[Event(name="changeSelection",type="com.alleni.taconite.event.SelectEvent")]
	
	/**
	 * The TaconiteController is an abstract superclass of application controllers.  Its role is to
	 * isolate other parts of the architecture from the implementation of modifications to the model,
	 * and to act as a central dispatch point for common handling of modifications. 
	 */
	public class TaconiteController extends EventDispatcher implements ITaconiteController
	{
		protected static var _selectionCache:ISelection;
		
		private static var _controllers:Dictionary = new Dictionary(true); /* lookup table of controller instances */
		
		private var _document:TaconiteDocument;
		
		/**
		 * Either retrieve or create the distinguished controller for the given object.
		 * If the controller is null, then null is returned also.
		 */
		public static function forModel(model:TaconiteModel, childDefintion:Vector.<ObjectDefinition>):IChildController
		{
			var shortClassName:String = model.value.shortClassName;
			
			if (!_controllers[shortClassName])
				_controllers[shortClassName] = ChildControllerFactory.instance.createForModel(model, childDefintion);
			return _controllers[shortClassName];
		}
		
		public function TaconiteController(document:TaconiteDocument)
		{
			this.document = document;
		}
		
		/**
		 * The root model object of for the document.
		 */
		public function get root():ModelRoot
		{
			return _document.root;
		}
		
		
		/**
		 * The ISelection instance representing the current selection within the document.
		 * Never null.
		 */
		public function get selection():ISelection
		{
			return _document.selection;
		}
		
		/**
		 * control run state of the root model 
		 * @param forceRun whether or not to force into run mode
		 * @return resulting run state of the root model
		 * 
		 */
		public function toggleRun(forceRun:Boolean=false):Boolean
		{
			throw new Error("toggleRun() must be overridden");
		}
		
		public function doRun():Boolean
		{
			throw new Error("doRun() must be overridden");
		}
		
		public function doPause():Boolean
		{
			throw new Error("doPause() must be overridden");
		}
		
		public function doResume():Boolean
		{
			throw new Error("doResume() must be overridden");
		}
		
		/**
		 * The TaconiteDocument which holds the model root, selection state and undo history for
		 * the application.
		 */
		[Bindable("documentChange")]
		public function get document():TaconiteDocument
		{
			return _document;
		}
		
		public function set document(d:TaconiteDocument):void
		{
			if (_document != null)
			{
				// When a document is replaced by another, kill its selection first to get rid
				// of any selection feedback that may be out there.
				_document.selection = new ObjectSelection(root,Vector.<TaconiteModel>([]));
                _selectionCache = null;
				
				_document.removeEventListener(SelectEvent.ADD_SELECTION, handleSelectEvent);
				_document.removeEventListener(SelectEvent.REMOVE_SELECTION, handleSelectEvent);
				_document.removeEventListener(SelectEvent.CHANGE_SELECTION, handleSelectEvent);
			}
			
			_document = d;
			
			if (_document != null)
			{
				// A new document always has no selection.
				_document.selection = new ObjectSelection(root,Vector.<TaconiteModel>([]));
                _selectionCache = null;

				_document.addEventListener(SelectEvent.ADD_SELECTION, handleSelectEvent, false, 0, true);
				_document.addEventListener(SelectEvent.REMOVE_SELECTION, handleSelectEvent, false, 0, true);
				_document.addEventListener(SelectEvent.CHANGE_SELECTION, handleSelectEvent, false, 0, true);
			}
			
			dispatchEvent(new ControllerEvent(ControllerEvent.DOCUMENT_CHANGE));
		}
		
		
		/**
		 * Handle a drag into the editor
		 */
		public function handleDragEnter(e:MouseEvent):void
		{
			// let's just assume we accept it... how bad could it be, eh?
			TaconiteFactory.getClipboardImplementation().acceptDragDrop(e.currentTarget as InteractiveObject);
			e.currentTarget.addEventListener(TaconiteFactory.getClipboardImplementation().DRAG_OVER, handleDragOver);
		}
		
		protected function handleDragOver(e:MouseEvent):void
		{
			// test mouse point for potential replacement candidates
		}
		
		/**
		 * Handle a drag dropped into the editor
		 * Either a file from the desktop (e.relatedObject == null)
		 * Or an asset from the asset palette (e.relatedObject is provided)
		 */
		public function handleDragDrop(e:MouseEvent):void
		{
			e.currentTarget.removeEventListener(TaconiteFactory.getClipboardImplementation().DRAG_OVER, handleDragOver);
			filterDroppedFile(e);
		}
		
		private function clearClipboard():void
		{
			var clipboard:ISystemClipboard = TaconiteFactory.getClipboardImplementation();
			clipboard.clear();
		}
		
		public function copyFormatToClipboard(format:String, data:Object, event:Event, serializable:Boolean=true):void
		{
			var clipboard:ISystemClipboard = TaconiteFactory.getClipboardImplementation(event);
			clipboard.setData(format, data, serializable);
		}
		
		/**
		 * filter the material either dropped in or pasted in.
		 * we can use the clipboard formats to discern what we should do with it. 
		 * 
		 */
		protected function filterDroppedFile(e:MouseEvent):void
		{
			var localX:Number = 0;
			var localY:Number = 0;
			if (e is MouseEvent) {
				localX = MouseEvent(e).localX;
				localY = MouseEvent(e).localY;
			} 
			
			trace("##############################  LocalX",localX,"LocalY",localY);
			
			if (e is MouseEvent && e.relatedObject != null) {
				handlePastedObject(e.relatedObject, localX, localY);  // e.relatedObject is an AssetBox dragged from palette
			} else {
				pasteFromSystemClipboard(localX,localY,e);
			}
		}
		
		public function pasteFromSystemClipboard(localX:Number,localY:Number, event:Event):void
		{
			// get the clipboard implementation:  web, desktop, or drag/drop
			// the contents of evt determine whether drag/drop is used
			var clipboard:ISystemClipboard = TaconiteFactory.getClipboardImplementation(event);
			
			if (clipboard.hasFormat(clipboard.OBJECTS_FORMAT)) {
				handlePastedObjects(clipboard.getData(clipboard.OBJECTS_FORMAT), localX, localY, event);
			} else if (clipboard.hasFormat(clipboard.FILE_LIST_FORMAT)) {
				handlePastedFiles(clipboard.getData(clipboard.FILE_LIST_FORMAT) as Array, localX, localY, event);
			} else if (!clipboard.hasFormat(clipboard.FILE_LIST_FORMAT) && clipboard.hasFormat(clipboard.URL_FORMAT)) {
				handlePastedURL(clipboard.getData(clipboard.URL_FORMAT) as String, localX, localY, event);
			} else {
				if (clipboard.hasFormat(clipboard.TEXT_FORMAT)) {
					handlePastedText(clipboard.getData(clipboard.TEXT_FORMAT) as String, localX, localY, event);
				} else if (clipboard.hasFormat(clipboard.TEXT_SCRAP)) {
					var scrap:TextScrap = clipboard.getData(clipboard.TEXT_SCRAP) as TextScrap;
					// optionally, convert to plain text and paste that way. we have an object which takes TLF so let's paste as-is.
					/*var plainText:String = TextConverter.export(scrap.textFlow, TextConverter.PLAIN_TEXT_FORMAT, ConversionType.STRING_TYPE).toString();
					handlePastedText(plainText, localX, localY);*/
					handlePastedTextScrap(scrap, localX, localY, event);
				} else if (clipboard.hasFormat(clipboard.BITMAP_FORMAT)) {
					handlePastedBitmap(clipboard.getData(clipboard.BITMAP_FORMAT), localX, localY, event);
				} else if (clipboard.hasFormat(clipboard.HTML_FORMAT)) {
					trace("TaconiteController::pasteClipboard received HTML paste");
					trace("Clipboard HTML: " + clipboard.getData(clipboard.HTML_FORMAT));
				} else {
					// unsupported clipboard contents
				}
			}
		}
		
		protected function handlePastedObjects(obj:Object, localX:Number=0, localY:Number=0, event:Event=null):void
		{
			// overridden by AuthorController, to paste Zebra objects
		}
		
		protected function handlePastedObject(relatedObject:Object, localX:Number=0, localY:Number=0, event:Event=null):void
		{
			// overridden by AuthorController, to drag/drop an AssetBox from asset palette
		}
		
		
		protected function handlePastedText(text:String, localX:Number=0, localY:Number=0, event:Event=null):void
		{
			// to be implemented on the application level
		}
		
		protected function handlePastedTextScrap(scrap:TextScrap, localX:Number=0, localY:Number=0, event:Event=null):void
		{
			// to be implemented on the application level
		}
		
		protected function handlePastedBitmap(data:*, localX:Number=0, localY:Number=0, event:Event=null):void
		{
			// to be implemented on the application level
		}
		
		protected function handlePastedFiles(fileList:Array, localX:Number=0, localY:Number=0, event:Event=null):void
		{
			// to be implemented on the application level
		}
		
		protected function handlePastedURL(url:String, localX:Number=0, localY:Number=0, event:Event=null):void
		{
			// to be implemented on the application level
		}
		
		protected function get selectedModels():Vector.<TaconiteModel>
		{
			if (selection != null)
			{
				return selection.selectedModels;
			}
			else
			{
				return new Vector.<TaconiteModel>();
			}
		}
		
		/**
		 * Select exactly one object in the document 
		 * @param m the object to be selected.
		 */
		public function selectSingleModel(m:TaconiteModel, extend:Boolean=false):void
		{
			Utilities.assert(m != null);
			if (extend && document.selection) {
				var addition:ObjectSelection = new ObjectSelection(root, Vector.<TaconiteModel>([m]));
				document.selection = document.selection.union(addition);
			} else {
				document.selection = new ObjectSelection(root, Vector.<TaconiteModel>([m]));
			}
		}
		
		public function deselectSingleModel(m:TaconiteModel):void
		{
			if (selection && selection.contains(m))
			{
				_document.deselect(new ObjectSelection(root, Vector.<TaconiteModel>([m])));
			}
		}
		
		
		/**
		 * Select multiple objects in the document 
		 * @param ms array of objects to be selected.
		 */
		public function selectModels(models:Vector.<TaconiteModel>):void
		{
			// debug: ensure no null models are selected
			//			for each (var model:TaconiteModel in models)
			//				Utilities.assertAndFail(model != null);
			
			document.selection = new ObjectSelection(root, models);
		}
		
		/**
		 * Extend an existing selection to include a model object 
		 * @param n the object whose selected status is to be modified.
		 */
		public function modifySelection(m:TaconiteModel):void
		{
			if (selection == null || selection.empty)
			{
				selectSingleModel(m);
			}
			else
			{
				// NOTE: didn't use polymorphism here because in the ObjectSelection case, we're
				// modifying the document; would prefer selection objects not to know about documents.
				//             
				if (selection as ObjectSelection)
				{
					if (selection.contains(m))
					{
						_document.deselect(new ObjectSelection(root, Vector.<TaconiteModel>([m])));
					}
					else
					{
						_document.select(new ObjectSelection(root, Vector.<TaconiteModel>([m])));
					}
				}
			}
		}
		
		/**
		 * Extend an existing selection to include multiple model objects 
		 * @param ms the array of objects whose selected status is to be modified.
		 */
		public function modifyMultiSelection(ms:Vector.<TaconiteModel>):void
		{
			if (selection == null || selection.empty)
			{
				selectModels(ms);
			}
			else
			{
				var selModels:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
				var unselModels:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
				
				if (selection is ObjectSelection)
				{
					for each (var m:TaconiteModel in ms)
					{
						if (selection.contains(m))
							unselModels.push(m);
						else
							selModels.push(m);
					}
				}
				if (selModels.length > 0)
					_document.select(new ObjectSelection(root, selModels));
				if (unselModels.length > 0)
					_document.deselect(new ObjectSelection(root, unselModels));
			}
		}

        /**
         * Clear selection cache.
         *
         */

        public function clearSelectionCache():void
        {
            _selectionCache = null;
        }

        /**
         *  Get selection cache
         *
         */

        public function get selectionCache():ISelection
        {
            return _selectionCache;
        }

		/**
		 * Clear any current selection for the document.  
		 * 
		 */
		public function clearSelection():void
		{
            if(document.selection && !document.selection.empty){
                _selectionCache = document.selection;
                document.selection = null;
            }

			
		}
		
		/**
		 * Return selection state to before clear
		 * 
		 */
		public function unclearSelection():void
		{
			document.selection = _selectionCache;
		}
		
		/**
		 * Remove all objects in the current selection. 
		 */
		public function removeSelection():void
		{
			if (selection is ObjectSelection)
			{
				applyToSelection(function(m:TaconiteModel):void {
					if(m.parent && !m.value["locked"]){
						m.parent.removeValueChildAndDescendants(m.value);
					}
				});
			}
		}
		
		/**
		 * Apply some function to all selected objects.
		 */
		private function applyToSelection(f:Function):void
		{
			for each (var m:TaconiteModel in selectedModels)
			{
				f(m);
			}
		}
		
		/**
		 * Handle all SelectEvents by redispatching, for convenience to our clients which would
		 * rather not be bothered tracking changes in the TaconiteDocument. 
		 */
		protected function handleSelectEvent(e:SelectEvent):void
		{
			dispatchEvent(e);
		}
		
		public function handleDeleteKey():void
		{
			removeSelection();
		}
		
		public function handleEscKey():void
		{
			clearSelection();
		}
		
	}
}
