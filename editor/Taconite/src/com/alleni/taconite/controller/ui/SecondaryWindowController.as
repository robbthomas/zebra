package com.alleni.taconite.controller.ui
{
	import com.alleni.taconite.factory.ITaconiteWindow;
	import com.alleni.taconite.factory.TaconiteFactory;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.geom.Rectangle;
	
	import mx.events.ResizeEvent;

	public class SecondaryWindowController
	{
		protected var _window:ITaconiteWindow;
		
		public function SecondaryWindowController(bounds:Rectangle, title:String="", resizable:Boolean=true, utility:Boolean=false)
		{
			_window = TaconiteFactory.getWindowImplementation();
			_window.isPopup = true;
			IEventDispatcher(_window).addEventListener(_window.WINDOW_COMPLETE, handleWindowComplete);
			IEventDispatcher(_window).addEventListener(ResizeEvent.RESIZE, handleWindowResize);
            _window.addEventListener("xyChanged", handleWindowMove);
			IEventDispatcher(_window).addEventListener(_window.WINDOW_ACTIVATE, handleWindowActivate);
			IEventDispatcher(_window).addEventListener(_window.WINDOW_DEACTIVATE, handleWindowDeactivate);
			_window.open(bounds, title, resizable, utility, true);
		}
		
		private function handleWindowActivate(event:Event):void
		{
			activate();
		}
		
		private function handleWindowDeactivate(event:Event):void
		{
			deactivate();
		}
		
		protected function activate():void
		{
			// subclasses can use this
		}
		
		protected function deactivate():void
		{
			// subclasses can use this
		}
		
		public function get displayObject():DisplayObject
		{
			return _window.displayObject;
		}
		
		public function get originX():Number
		{
			return _window.originX;
		}
		
		public function get originY():Number
		{
			return _window.originY;
		}
		
		public function setBounds(bounds:Rectangle):void
		{
			_window.bounds = bounds;	
		}
		
		public function get visible():Boolean
		{
			return _window.visible;
		}
		
		/**
		 * A regular method to set visibility as opposed to a setter in case this needs additional parameters. 
		 * @param value whether or not to set the window visible.
		 * 
		 */
		public function setVisible(value:Boolean):void
		{
			_window.visible = value;
		}
		
		public function exit():void
		{
			IEventDispatcher(_window).removeEventListener(_window.WINDOW_COMPLETE, handleWindowComplete);
			IEventDispatcher(_window).removeEventListener(ResizeEvent.RESIZE, handleWindowResize);
			_window.exitWindow = true;
			_window.close();
		}
		
		protected function set closeCallback(closeCallbackFunction:Function):void
		{
			_window.closeCallback = closeCallbackFunction;
		}
		
        protected function handleWindowResize(e:ResizeEvent):void
        {
            // implement as needed in subclass
        }

        protected function handleWindowMove(e:Event):void
        {
            // implement as needed in subclass
        }

		protected function handleWindowComplete(e:Event):void
		{
			// implement as needed in subclass
		}
	}
}