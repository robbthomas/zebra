package com.alleni.taconite.controller
{
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public interface IChildMediator
	{
		function updatePosition(model:TaconiteModel, point:Point):void
		function handleMouseOver(objectView:ITaconiteView, e:MouseEvent):void
		function handleMouseOut(objectView:ITaconiteView, e:MouseEvent):void
		function handleClick(objectView:ITaconiteView, e:MouseEvent):void
		function handleMouseDown(objectView:ITaconiteView, e:MouseEvent):Boolean	
		function handleMouseMove(objectView:ITaconiteView, e:MouseEvent):void
		function handleMouseUp(objectView:ITaconiteView, e:MouseEvent):void
	}
}