package com.alleni.taconite.controller
{
	import com.alleni.taconite.document.ISelection;
	import com.alleni.taconite.document.TaconiteDocument;
	import com.alleni.taconite.model.ModelRoot;
	import com.alleni.taconite.model.TaconiteModel;

import flash.events.Event;

import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	
	[Event(name="documentChange",type="com.alleni.taconite.event.ControllerEvent")]
	[Event(name="addSelection",type="com.alleni.taconite.event.SelectEvent")]
	[Event(name="removeSelection",type="com.alleni.taconite.event.SelectEvent")]
	[Event(name="changeSelection",type="com.alleni.taconite.event.SelectEvent")]
	
	/**
	 * Basic controller interface for operations supported by all Taconite apps. 
	 */
	public interface ITaconiteController extends IEventDispatcher
	{
		/**
		 * The TaconiteDocument that manages this controller's root model. 
		 */
		[Bindable("documentChange")]
		function get document():TaconiteDocument;
		function set document(d:TaconiteDocument):void;
		
		/**
		 * The root model which is modified by this controller. 
		 */
		function get root():ModelRoot;
		
		/**
		 * The current ISelection object if there is a selection, or null if there is not. 
		 */
		function get selection():ISelection;
		
		/**
		 * control run state of the root model 
		 * @param forceRun whether or not to force into run mode
		 * @return resulting run state of the root model
		 * 
		 */
		function toggleRun(forceRun:Boolean=false):Boolean;
		
		
		/**
		 * Handle a drag into the editor
		 */
		function handleDragEnter(e:MouseEvent):void;
		
		/**
		 * Handle a drag dropped into the editor
		 */
		function handleDragDrop(e:MouseEvent):void;
		
		/**
		 * Select exactly one model. Or extend the selection by one model.
		 */
		function selectSingleModel(m:TaconiteModel,extend:Boolean=false):void;
		
		/**
		 * Deselect the specified model, if its currently selected. 
		 * @param m
		 * 
		 */
		function deselectSingleModel(m:TaconiteModel):void;
		
		/**
		 * Select Taconite models.
		 */
		function selectModels(ms:Vector.<TaconiteModel>):void;
		
		/**
		 * Extend an existing selection to include/exclude a model. 
		 * @param n the model object whose selected status is to be modified.
		 */
		function modifySelection(m:TaconiteModel):void;
		
		/**
		 * Extend an existing selection to include/exclude multiple models. 
		 * @param n the array of model objects whose selected status are to be modified.
		 */
		function modifyMultiSelection(ms:Vector.<TaconiteModel>):void;
		
		/**
		 * Deselect all selected objects. 
		 */
		function clearSelection():void
		function unclearSelection():void
		
		/**
		 * Remove all models in the current selection from the document. 
		 */
		function removeSelection():void;
		
		function handleDeleteKey():void;
		function handleEscKey():void;
	}
}
