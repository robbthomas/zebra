package com.alleni.taconite.controller
{
	import com.alleni.taconite.document.ISelection;
	import com.alleni.taconite.model.TaconiteModel;
	import com.alleni.taconite.view.ITaconiteView;
	
	import flash.events.MouseEvent;
	import flash.utils.getTimer;

    /**
     * This Mediator-type object takes on responsibility for all mouse interaction
     * involving click-based selection.
     */
    public class SelectionMediator implements ITaconiteMediator
    {
        private var _view:ITaconiteView;

        private var _lastClickMillis:Number = 0;
        
        public function SelectionMediator()
        {
        }

        public function handleViewEvents(view:ITaconiteView):void
        {
            _view = view;
                  
            _view.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
        }
        
        private function handleMouseDown(e:MouseEvent):void
        {
            var now:Number = getTimer();
            var doubleClick:Boolean = (now - _lastClickMillis) <= _view.context.info.doubleClickMillis;
            _lastClickMillis = now;

            if (!_view.model.enabled)
            {
                // some models don't permit interaction
                return;
            }
            
            var model:TaconiteModel = _view.model;
			
			// Check if this is a mouse down on the currently selected model or if its a new selection.
			var currentSelection:ISelection = _view.context.document.selection;
			if (currentSelection != null && !currentSelection.contains(model)) {
				// New selection.
				// Need to reset the focus early incase a RichTextField was in editing mode
				//TaconiteEditor.resetKeyboardFocus(true);
			}

            if (e.ctrlKey || e.shiftKey)
            {
                // Ctrl-click (on message-center) toggles selection
				if (_view.okToSelect)
	                _view.context.controller.modifySelection(model);
            }
            else if (doubleClick)
            {
				_view.dispatchEvent(new MouseEvent(MouseEvent.DOUBLE_CLICK));
			}
            else if ((_view.context.document.selection == null
                     || !_view.context.document.selection.includes(model)) && _view.okToSelect)
            {
                // Regular click: select the single model that was just clicked
	            _view.context.controller.selectSingleModel(model);
            }
            e.stopPropagation();
            
            //_view.context.editor.setFocus();
            //TaconiteEditor.resetKeyboardFocus();
        }
    }
}
