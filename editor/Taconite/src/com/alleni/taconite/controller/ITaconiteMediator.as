package com.alleni.taconite.controller
{
    import com.alleni.taconite.view.ITaconiteView;
    
    /**
     * An ITaconiteMediator brokers events between a view, its model and its layout.  Different
     * implementations provide different support for gestures on the view and on how it responds to
     * model and layout changes.
     */
    public interface ITaconiteMediator
    {
        /**
         * Inform this mediator of the view, notation and layout for which it is to mediate events
         *  
         * @param view an ITaconiteView
         * 
         */
        function handleViewEvents(view:ITaconiteView):void
    }
}
