package com.alleni.taconite.controller
{
	import com.alleni.taconite.model.Notification;

	public interface INotificationListener
	{
		function receiveNotifications(notification:Notification):void;
	}
}