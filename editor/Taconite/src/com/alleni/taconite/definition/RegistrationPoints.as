/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.definition
{
	import com.alleni.taconite.lang.Trigonometry;
	
	import flash.geom.Point;

	/**
	 * Constants defining what role[s] a given handle is for.
	 **/
	public class RegistrationPoints
	{
		public static const TOP_LEFT 		: uint = 1; 
		public static const TOP 			: uint = 2;
		public static const TOP_RIGHT 		: uint = 4;
		public static const RIGHT 			: uint = 8;
		public static const BOTTOM_RIGHT 	: uint = 16;
		public static const BOTTOM 			: uint = 32;
		public static const BOTTOM_LEFT 	: uint = 64;
		public static const LEFT 			: uint = 128;
		public static const CENTER			: uint = 256;
		
		// some convenience methods
		
		public static function isTopLeft( val:uint ) : Boolean
		{
			return (val & TOP_LEFT) == TOP_LEFT;
		}
		public static function isTop( val:uint ) : Boolean
		{
			return (val & TOP) == TOP;
		}
		public static function isTopRight( val:uint ) : Boolean
		{
			return (val & TOP_RIGHT) == TOP_RIGHT;
		}
		public static function isRight( val:uint ) : Boolean
		{
			return (val & RIGHT) == RIGHT;
		}
		public static function isBottomRight( val:uint ) : Boolean
		{
			return (val & BOTTOM_RIGHT) == BOTTOM_RIGHT;
		}
		public static function isBottom( val:uint ) : Boolean
		{
			return (val & BOTTOM) == BOTTOM;
		}
		public static function isBottomLeft( val:uint ) : Boolean
		{
			return (val & BOTTOM_LEFT) == BOTTOM_LEFT;
		}
		public static function isLeft( val:uint ) : Boolean
		{
			return (val & LEFT) == LEFT;
		}
		public static function isCenter( val:uint ) : Boolean
		{
			return (val & CENTER) == CENTER;
		}
		
		public static function toRegistrationPoint(offset:Point):uint{
			
			if(offset.x == 0 && offset.y == 0)
				return RegistrationPoints.TOP_LEFT;
			if(offset.x == .5 && offset.y == 0)
				return RegistrationPoints.TOP;
			if(offset.x == 1 && offset.y == 0)
				return RegistrationPoints.TOP_RIGHT;
			if(offset.x == 0 && offset.y == .5)
				return RegistrationPoints.LEFT;
			if(offset.x == .5 && offset.y == .5)
				return RegistrationPoints.CENTER;
			if(offset.x == 1 && offset.y == .5)
				return RegistrationPoints.RIGHT;
			if(offset.x == 0 && offset.y == 1)
				return RegistrationPoints.BOTTOM_LEFT;
			if(offset.x == .5 && offset.y == 1)
				return RegistrationPoints.BOTTOM;
			if(offset.x == 1 && offset.y == 1)
				return RegistrationPoints.BOTTOM_RIGHT;
			
			return RegistrationPoints.CENTER;
			
		}
		
		public static function toOffsetPoint(registrationPoint:uint):Point
		{
			var offset:Point;
			
			if (isTopLeft(registrationPoint))
            	offset = new Point(0, 0);
			if (isTop(registrationPoint))
            	offset = new Point(50, 0);
			if (isTopRight(registrationPoint))
            	offset = new Point(100, 0);
			if (isRight(registrationPoint))
            	offset = new Point(100, 50);
			if (isBottomRight(registrationPoint))
            	offset = new Point(100, 100);
			if (isBottom(registrationPoint))
            	offset = new Point(50, 100);
			if (isBottomLeft(registrationPoint))
            	offset = new Point(0, 100);
			if (isLeft(registrationPoint))
            	offset = new Point(0, 50);
			if (isCenter(registrationPoint))
           		offset = new Point(50, 50);
           		
           	return offset;
 		}
		
		
		/**
		 * Return the offset from one registration point (eg. RegistrationPoints.TOP_LEFT) to another one (eg. anchorPoint).
		 * This function is important because we are storing each objects anchorPoint, which can be at any registration point.
		 * @param width object width.
		 * @param height object height.
		 * @param fromReg the first registration point.
		 * @param toReg the destination registration point.  Can be an arbitrary value from a custom anchorPoint
		 * @param rotation current rotation of object.
		 * @return offset from "fromReg" to "toReg" in the form of a Point.
		 * 
		 */
		
		
		
		public static function registrationOffset(width:Number, height:Number, fromReg:uint, toReg:*, rotation:Number):Point
		{
			var fromPoint:Point = toOffsetPoint(fromReg);
			var toPoint:Point = new Point(0,0);
			
			if(toReg is uint){
				toPoint = toOffsetPoint(toReg as uint);
			}
			
			else if (toReg is Point){
				toPoint = new Point(toReg.x * 100,toReg.y * 100);
			}
			
			var offset:Point = toPoint.subtract(fromPoint);  // this is a pair of percentage values
			//trace("registrationOffset",offset);
			var result:Point = Trigonometry.transformOffset(width, height, offset, rotation);	
			
			// ensure NaN is not returned (typically results from width=0 or height=0)
			if (isNaN(result.x))
				result.x = 0;
			if (isNaN(result.y))
				result.y = 0;
			return result;
		}
	}
}