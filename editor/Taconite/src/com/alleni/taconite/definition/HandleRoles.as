/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.definition
{
	/**
	 * Constants defining what role[s] a given handle is for.
	 **/
	public class HandleRoles
	{
		public static const RESIZE_UP 		: uint = 1;
		public static const RESIZE_DOWN 	: uint = 2;
		public static const RESIZE_LEFT 	: uint = 4;
		public static const RESIZE_RIGHT 	: uint = 8;
		public static const ROTATE 			: uint = 16;
		public static const MOVE 			: uint = 32;
		public static const MOVE_PIVOTANCHOR		: uint = 64;

		public static function flipVertical(val:uint) : uint
		{
			var result:uint = val;
			if(isResizeUp(val)) {
				result = (result ^ RESIZE_UP) | RESIZE_DOWN;
			}
			if(isResizeDown(val)) {
				result = (result ^ RESIZE_DOWN) | RESIZE_UP;
			}
			return result;
		}

		public static function flipHorizontal(val:uint) : uint
		{
			var result:uint = val;
			if(isResizeLeft(val)) {
				result = (result ^ RESIZE_LEFT) | RESIZE_RIGHT;
			}
			if(isResizeRight(val)) {
				result = (result ^ RESIZE_RIGHT) | RESIZE_LEFT;
			}
			return result;
		}
		
		
		// some convenience methods
		
		public static function isResizeUp( val:uint ) : Boolean
		{
			return (val & RESIZE_UP) == RESIZE_UP;
		}
		public static function isResizeDown( val:uint ) : Boolean
		{
			return (val & RESIZE_DOWN) == RESIZE_DOWN;
		}
		
		
		public static function isResizeLeft( val:uint ) : Boolean
		{
			return (val & RESIZE_LEFT) == RESIZE_LEFT;
		}
		
		public static function isResizeRight( val:uint ) : Boolean
		{
			return (val & RESIZE_RIGHT) == RESIZE_RIGHT;
		}
		
		public static function isRotate( val:uint ) : Boolean
		{
			return (val & ROTATE) == ROTATE;
		}
		
		public static function isMove( val:uint ) : Boolean
		{
			return (val & MOVE) == MOVE;
		}
		
		public static function isMovePivot( val:uint ) : Boolean
		{
			return (val & MOVE_PIVOTANCHOR) == MOVE_PIVOTANCHOR;
		}

	}
}