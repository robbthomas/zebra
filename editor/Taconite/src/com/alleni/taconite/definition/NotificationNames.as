package com.alleni.taconite.definition
{
	public class NotificationNames
	{
		public static const START_OPERATION_PROGRESS:String = "Start Busy Cursor";
		public static const STOP_OPERATION_PROGRESS:String = "Stop Busy Cursor";
	}
}