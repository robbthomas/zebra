package com.alleni.taconite.definition
{
	

	public class ObjectDefinition
	{
		public var name:String;
		public var defaultTitle:String;
		public var model:Class;
		public var view:Class;
		public var controller:Class;
		public var icon:Class;
		
		public function ObjectDefinition(name:String, defaultTitle:String, model:Class, view:Class, controller:Class, icon:Class=null)
		{
			this.name = name;
			this.defaultTitle = defaultTitle;
			this.model = model;
			this.view = view;
			this.controller = controller;
			this.icon = icon;
		}
	}
}