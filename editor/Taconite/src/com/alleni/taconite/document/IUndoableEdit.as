package com.alleni.taconite.document
{
    /**
     * Abstraction of an edit that can be undone and redone. 
     */
    public interface IUndoableEdit
    {
        /**
         * Undo this edit. 
         */
		function undo():void;
        
        /**
         * Redo this edit.
         */        
        function redo():void;
		
		function toString():String;
		function dump(indent:String=""):void;
    }
}
