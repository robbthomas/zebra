package com.alleni.taconite.document
{
    import com.alleni.taconite.model.TaconiteModel;
    
    /**
     * A clipboard implementation that consists of a simple flat collection of model objects. 
     */
    public class CollectionClipboard implements IClipboard
    {
        private var _models:Vector.<TaconiteModel>;
        
        public function CollectionClipboard(models:Vector.<TaconiteModel>)
        {
            _models = models;
        }
        
        public function get models():Vector.<TaconiteModel>
        {
            return _models;
        }
    }
}