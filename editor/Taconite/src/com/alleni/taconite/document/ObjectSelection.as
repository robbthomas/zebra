package com.alleni.taconite.document
{
    import com.alleni.taconite.event.ModelStatusEvent;
    import com.alleni.taconite.model.ModelRoot;
    import com.alleni.taconite.model.TaconiteModel;
    
    import flash.utils.Dictionary;
    
    /**
     * A simple type of selection consisting of an unordered set of selected model objects.
     */
    public class ObjectSelection implements ISelection
    {
        private var _root:ModelRoot;
        private var _selectedModels:Vector.<TaconiteModel>;
        private var _dictionary:Dictionary;
        
        public function ObjectSelection(root:ModelRoot, selectedModels:Vector.<TaconiteModel>)
        {
            super();
            _root = root;
            _selectedModels = new Vector.<TaconiteModel>();
            _dictionary = new Dictionary();
			var obj:Object;
            for each (obj in selectedModels) {
                if (!(obj in _dictionary)) {
                    _dictionary[obj] = obj;
                    _selectedModels.push(obj);
                }
            }
        }
        
        public function get selectedModels():Vector.<TaconiteModel>
        {
            return _selectedModels;
        }
        
        public function get root():ModelRoot
        {
            return _root;
        }
        		
        public function includes(obj:Object):Boolean
        {
            return contains(obj);
        }
        
        public function contains(obj:Object):Boolean
        {
            return obj in _dictionary;
        }
        
        /** return the union of this selection with another ObjectSelection */
        public function union(sel:ISelection):ISelection
        {
			var objectSelection:ObjectSelection = sel as ObjectSelection;
            if (objectSelection) {
                return new ObjectSelection(root, _selectedModels.concat(objectSelection._selectedModels));
            } else {
                return super.union(sel);
            }
        }
        
        /** return all objects in this selection that are not in some other selection */
        public function difference(sel:ObjectSelection):ISelection
        {
            var diff:Vector.<TaconiteModel> = new Vector.<TaconiteModel>();
			var obj:Object;
            for each (obj in _selectedModels) {
                if (!sel.contains(obj)) {
                    diff.push(obj);
                }
            }
            return new ObjectSelection(root, diff);
        }

        public function dispatchStatusChange():void
        {
            // For an object selection, dispatch status change events off the actual model objects
            // in the selection so that they will be redrawn in their changed selection status.
            // 
            for each (var obj:TaconiteModel in _selectedModels) {
                obj.dispatchEvent(new ModelStatusEvent(ModelStatusEvent.STATUS_CHANGE));
            }
        }
        
        public function createClipboard():IClipboard
        {
            return new CollectionClipboard(selectedModels);
        }

        /** return true if empty */
        public function get empty():Boolean
        {
            return (_selectedModels.length == 0);
        }
        
        public function dumpSelection(title:String=null):String
		{
			var buffer:String = "ObjectSelection::dump count=" + _selectedModels.length + "\n";
        	trace(buffer);
			
			var bufferLine:String = "";
        	for each (var m:TaconiteModel in _selectedModels) {
				bufferLine = "    " + m.value.toString();
        		trace(bufferLine);
				buffer += bufferLine + "\n";
        	}
        	trace("");
			return buffer;
        }
    }
}
