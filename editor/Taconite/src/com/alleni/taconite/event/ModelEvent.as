package com.alleni.taconite.event
{
    import com.alleni.taconite.model.TaconiteModel;
    
    import flash.events.Event;
    
    /**
     * Event representing a structural change to a model's parent/child relationships.
     * Dispatched by TaconiteModel in response to model topology changes.
     */
    public class ModelEvent extends Event
    {
        public static const MODEL_CHANGE:String = "modelChange";

        public static const ADD_CHILD_MODEL:String = "addChildModel";
        public static const REMOVING_CHILD_MODEL:String = "removingChildModel";
		public static const REMOVE_CHILD_MODEL:String = "removeChildModel";

        public static const ADD_ARRAY_ITEM:String = "addArrayItem";
        public static const CHANGE_ARRAY_ITEM:String = "changeArrayItem";
        public static const REMOVE_ARRAY_ITEM:String = "removeArrayItem";

        public var kind:String;
        public var moving:Boolean;
        public var parent:TaconiteModel;
        public var child:TaconiteModel;
        public var index:int;
        
        public function ModelEvent(type:String, kind:String, moving:Boolean, parent:TaconiteModel, child:TaconiteModel, index:int)
        {
            super(type);
            this.kind = kind;
            this.moving = moving;
            this.parent = parent;
            this.child = child;
            this.index = index;
        }
        
        override public function clone():Event
        {
            return new ModelEvent(type, kind, moving, parent, child, index);
        }
		
		
		override public function toString() : String {
			return "[ModelEvent type="+type + " kind="+kind + " parent="+parent + " child="+child + " index="+index +"]";
		}
    }
}
