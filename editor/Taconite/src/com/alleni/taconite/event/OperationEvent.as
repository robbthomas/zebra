package com.alleni.taconite.event
{
	import flash.events.Event;
	
	/**
	 * General-purpose wrapper event around different kinds of error, used in the service layer.
	 */
	public class OperationEvent extends Event
	{
		public static const EXECUTING:String = "executing"
		public static const UPDATE:String = "update"

		public var text:String;
		
		public function OperationEvent(type:String, text:String="")
		{
			super(type);
			this.text = text;
		}
		
		override public function clone():Event
		{
			return new OperationEvent(type, text);
		}
	}
}
