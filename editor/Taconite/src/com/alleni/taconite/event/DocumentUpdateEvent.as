package com.alleni.taconite.event
{
    import com.alleni.taconite.document.TaconiteDocument;
    
    import flash.events.Event;
    
    /**
     * A property change event with respect to a TaconiteDocument. 
     * @author joeb
     * 
     */
    public class DocumentUpdateEvent extends Event
    {
        public static const DOCUMENT_UPDATE:String = "documentUpdate";

        public var property:Object;
        public var oldValue:Object;
        public var newValue:Object;
        public var document:TaconiteDocument;
        
        public function DocumentUpdateEvent(type:String, property:Object, oldValue:Object, newValue:Object, document:TaconiteDocument)
        {
            super(type);
            this.property = property;
            this.oldValue = oldValue;
            this.newValue = newValue;
            this.document = document;
        }
        
        override public function clone():Event
        {
            return new DocumentUpdateEvent(type, property, oldValue, newValue, document);
        }
    }
}
