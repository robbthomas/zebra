package com.alleni.taconite.event
{
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.events.Event;

	public class ModelCollectionEvent extends ModelEvent
	{
		public var property:String;
		public var collection:Object;
		public var item:Object;
		public var oldItem:Object;
		
		public function ModelCollectionEvent(type:String, kind:String, moving:Boolean, parent:TaconiteModel, property:String, collection:Object, item:Object, index:int, oldItem:Object=null)
		{
			super(type, kind, moving, parent, null, index);
			this.property = property;
			this.collection = collection;
			this.item = item;
			if (oldItem != null) this.oldItem = oldItem;
		}
        
        override public function clone():Event
        {
            return new ModelCollectionEvent(type, kind, moving, parent, property, collection, item, index, oldItem);
        }
        
        override public function toString() : String {
        	return "[ModelCollectionEvent type="+type + " collection="+collection + " item="+item + " oldItem="+oldItem +"]";
        }
	}
}