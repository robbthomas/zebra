package com.alleni.taconite.event
{
import com.alleni.taconite.service.LogService;

import flash.events.Event;
	
	public class ApplicationEvent extends Event
	{
		public var data:Object;
		//SEE NotificationNamesApp.as for CONSTs
		
		public function ApplicationEvent(type:String, incomingData:Object = null)
		{
			super(type);
			data=incomingData;
            LogService.receiveNotifications(this);
		}
		
		override public function clone():Event
		{
			return new ApplicationEvent(type, data);
		}
	}
}