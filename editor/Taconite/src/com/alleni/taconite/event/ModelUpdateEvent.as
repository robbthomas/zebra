package com.alleni.taconite.event
{
    import com.alleni.taconite.model.TaconiteModel;
    
    import flash.events.Event;
    
    /**
     * Event representing a property change on some value object associated with a TaconiteModel.
     * Dispatched by TaconiteModel automatically.
     */
    public class ModelUpdateEvent extends Event
    {
        public static const MODEL_UPDATE:String = "modelUpdate";

        public var property:Object;
        public var oldValue:Object;
        public var newValue:Object;
        public var source:TaconiteModel;
		public var parent:TaconiteModel;
        
        public function ModelUpdateEvent(type:String, property:Object, oldValue:Object, newValue:Object, source:TaconiteModel, parent:TaconiteModel=null)
        {
            super(type);
            this.property = property;
            this.oldValue = oldValue;
            this.newValue = newValue;
            this.source = source;
			this.parent = parent;
        }
        
        override public function clone():Event
        {
            return new ModelUpdateEvent(type, property, oldValue, newValue, source, parent);
        }
        
        override public function toString() : String {
        	return "[ModelUpdateEvent type="+type + " property="+property + " oldValue="+oldValue + " newValue="+newValue + " source="+source+" parent="+parent+"]";
        }
    }
}
