package com.alleni.taconite.event
{
    import flash.events.ErrorEvent;
    import flash.events.Event;
    
    /**
     * General-purpose wrapper event around different kinds of error, used in the service layer.
     */
    public class OperationFaultEvent extends ErrorEvent
    {
		public static const FAULT:String = "fault";
		public static const REDIRECT:String = "redirect";
		public static const COMMAND_ERROR:String = "commandError"
		public static const UNAUTHORIZED:String = "unauthorized";
		public static const NOT_FOUND:String = "notFound";
		public static const INTERNAL_SERVER_ERROR:String = "internalServerError";
		public static const SERVICE_UNAVAILABLE:String = "serviceUnavailable";
		
		public var error:ErrorEvent;
		public var recoverable:Boolean = true;
        
        public function OperationFaultEvent(type:String, error:ErrorEvent, recoverable:Boolean=true)
        {
            super(type);
            this.error = error;
			this.recoverable = recoverable;
        }
        
        override public function clone():Event
        {
            return new OperationFaultEvent(type, error);
        }
    }
}
