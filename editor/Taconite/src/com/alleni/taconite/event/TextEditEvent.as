package com.alleni.taconite.event
{
	import flash.events.Event;

	public class TextEditEvent extends Event
	{
        public static const GOT_FOCUS:String = "gotFocus";
        public static const LOST_FOCUS:String = "lostFocus"
        public static const EDIT_UPDATE:String = "editUpdate";
		public static const EDIT_COMPLETE:String = "editComplete";
		public static const UNDO:String = "undo";
		public static const REDO:String = "redo";
		public static const CUT:String = "cut";
		public static const COPY:String = "copy";
		public static const PASTE:String = "paste";
		public static const SELECT_ALL:String = "selectAll";
                
        public var contents:String;
        
        public function TextEditEvent(type:String, contents:String)
        {
            super(type);
            this.contents = contents;
        }
        
        override public function clone():Event
        {
            return new TextEditEvent(type, contents);
        }

	}
}