package com.alleni.taconite.lang
{
	public class Pair implements IPair
	{
		private var _key:Object;
		private var _value:Object;

		public function Pair(aKey:Object, aValue:Object)
		{
			_key = aKey;
			_value = aValue;
		}

		public function get key():Object {
			return _key;
		}

		public function get value():Object {
			return _value;
		}
	}
}