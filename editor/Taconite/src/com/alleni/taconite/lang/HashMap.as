/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */
package com.alleni.taconite.lang
{
	import flash.utils.Dictionary;

	/**
	 * A basic Hash Map that keeps track of how many entries are in the map and allows iteration
	 * over the set of objects in the map with use of HashMapIterator.
	 */
	public class HashMap
	{
		private var _dict:Object;
		private var _size:int;

		/** Construct an empty HashMap. */
		public function HashMap(weakReferences:Boolean = false)
		{
			_dict = new Object();	//Dictionary(weakReferences);	
			_size = 0;
		}
		
		/** Add a key value pair to the hash map. */
		public function add(key:Object, value:Object):void {
			_dict[key] = value;
			_size++;
		}

		/** Get an Array of all keys in the hash map. */
        public function get keys() : Array
        {
            var keys:Array = [];
            for (var key:Object in _dict) {
                keys.push(key);
            }
            return keys;
        }

		/** Remove an entry from the hash map. */
		public function remove(key:Object):void {
			delete _dict[key];
			_size--;
		}

		/** Return the number of entries in the hash map. */
		public function get size():int {
			return _size;
		}

		/** Get the value associated with the given key. */
		public function value(key:Object):Object {
			return _dict[key];
		}
	}
}
