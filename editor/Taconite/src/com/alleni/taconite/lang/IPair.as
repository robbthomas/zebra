package com.alleni.taconite.lang
{
	/** The interface an IPair must implement.  Represents the association
	 * between a key and a value.
	 */
	public interface IPair
	{
		function get key():Object;
		function get value():Object;
	}
}