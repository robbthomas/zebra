/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: AbstractObject.as 1779 2009-08-24 16:19:22Z pkrekelberg $  */

package com.alleni.taconite.lang
{
	import com.alleni.taconite.dev.Utilities;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	/**
	 * A Taconite wrapper for Timer, to trigger any objects or behaviors which depend on the time domain
	 * 
	 */
	public class TaconiteTimer
	{
		private static const DELAY:Number = 50;
		
		private static var _instance:TaconiteTimer;
		private static var _timer:Timer;
		
		
		public function TaconiteTimer():void
		{
			Utilities.assert(!_instance);	
		}
		
		public static function get instance():TaconiteTimer
		{
			if (!_instance)
				_instance = new TaconiteTimer();
			return _instance;
		}
		
		/**
		 * External interface for adding callback functions to the queue. 
		 * @param callback a function to call
		 * 
		 */
		public function registerListener(callback:Function, weakRef:Boolean=false):void
		{
			if (!_timer) initialize();
			_timer.addEventListener(TimerEvent.TIMER, callback, false, 0, weakRef);
		}
		
		/**
		 * External interface for removing a callback function from the queue 
		 * @param callback a function to remove
		 * 
		 */
		public function unregisterListener(callback:Function):void
		{
			// if we have a reference to this method, remove it from the list of listeners
			_timer.removeEventListener(TimerEvent.TIMER, callback);
		}
		
		public function get milliseconds():int
		{
			// return milliseconds since the runtime started up
			return getTimer();
		}
		
		private function initialize():void
		{
			_timer = new Timer(DELAY);
			_timer.start();
		}
	}
}