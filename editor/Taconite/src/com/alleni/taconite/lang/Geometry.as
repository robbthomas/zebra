package com.alleni.taconite.lang
{
	import flash.geom.Point;

	public class Geometry
	{
		public static function distanceBetweenPoints(x1:Number, y1:Number, x2:Number, y2:Number):Number
		{
			return Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
		}

		/*
		 * Given a fixed beginning of a line and an initial endpoint p, this function calculates a new end point
		 * such that the length of the line remains constant but the angle of the line has been snapped either
		 * to vertical or horizontal or optionally one of the diagonals.
		 */
		public static function snapToDirection(fixed:Point, p:Point, diagonalAllowed:Boolean):Point {
			var vector:Point = p.subtract(fixed);
			var length:Number = vector.length;
			if(diagonalAllowed) {
				var angle:Number = Math.atan2(vector.x,vector.y);
				// angle is now a value from -PI to PI where zero is straight down, negatives are to the left
				// positives are to the right and both -PI and PI are straight up
				angle = Math.floor(angle*4/Math.PI + .5)/4*Math.PI;
				// angle has now been rounded to the nearest 45 degree mark
				vector = new Point(Math.sin(angle), Math.cos(angle));
				vector.normalize(length);
			} else {
				// easy case where we just create a vector of the appropreate length where the less extreme component
				// is zero
				if(Math.abs(vector.x) > Math.abs(vector.y)) {
					vector = new Point(sign(vector.x) * length, 0);
				} else {
					vector = new Point(0, sign(vector.y) * length);
				}
			}
			return fixed.add(vector);
		}

		public static function sign(x:Number):Number {
			return (x < 0) ? -1 : 1;
		}
		
		/**
		 * Returns the point that is closest to point 3 along the line segment described by point 1 and point 2
		 */
		public static function closestPointOnLineSegment(p1:Point, p2:Point, p3:Point, capped:Boolean=true):Point {
			// point along a line segment is P = P1 + u (P2-P1)
			// where u is 0 to 1 if P is between P1 and P2 (greater if beyond P2)
			// Assuming our test point is P3
			// P3 is closest to the line at the tangent to the line which passes 
			// through P3 and the dot product of a line and it's tangent is zero
			//   (a dot b => ax*bx + ay*by)
			// so (P3 - P) dot (p2 - P1) = 0
			// substitute P from above:
			// so [P3 - P1 - u(P2 - P1)] dot (P2 - P1) = 0
			// solve for u
			// [(p3-p1) dot (p2-p1)] / (length(p2-p1)^2)
			var length:Number = p2.subtract(p1).length;
			if (length == 0)
				return p1.clone();
			var u:Number = ((p3.x - p1.x)*(p2.x - p1.x) + (p3.y - p1.y)*(p2.y - p1.y)) / (length*length);
			
			if(capped && u < 0) {
				return p1;
			} else if(capped && u > 1) {
				return p2;
			} else {
				return new Point(p1.x + u*(p2.x - p1.x), p1.y + u*(p2.y - p1.y));
			}
		}
	}
}