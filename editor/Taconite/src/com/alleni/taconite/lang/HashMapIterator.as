/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */
package com.alleni.taconite.lang
{
	/** An IIterator to iterator over (enumerate) a HashMap. */
	public class HashMapIterator implements IIterator
	{
		/* The implementation strategy is to take a snapshoot of the hash map when the iterator is
		 * instantiated and iterate over the snapshot.  This amounts to getting an array of the keys and iterating over
		 * that array.
		 */
		private var _currentIndex:int;
			// The "current index of the iterator" but really, an index in to the _keys array.

		private var _hashMap:HashMap;
			// The HashMap we're iterating over.

		private var _keys:Array;
			// The list of keys in the HashMap at the time this iterator is instantiated.

		/**
		 * Construct an iterator for the given HashMap.
		 */
		public function HashMapIterator(hashMap:HashMap) {
			_currentIndex = 0;
			_hashMap = hashMap;
			_keys = hashMap.keys;	// Copy
		}

		/**
		 * Ask if the iterator has another entry to return.
		 */
		public function hasNext():Boolean {
			var returnValue:Boolean = false;
			if (_currentIndex < _keys.length) {
				returnValue = true;
			}
			
			return returnValue;
		}
		
		/**
		 * Get the next entry in the HashMap.
		 */
		public function next():IPair {
			// Get the "next" key.
			var key:Object = _keys[_currentIndex++];

			// Return an IPair of the key and its value.
			var pair:IPair = new Pair(key, _hashMap.value(key));
			return pair;
		}

		/**
		 * Reset the iterator to iterate through the HashMap.
		 */
		public function reset():void {
			_currentIndex = 0;
			_keys = _hashMap.keys;
		}
	}
}
