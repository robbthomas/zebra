/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */
package com.alleni.taconite.lang
{
	/** Interafce for class that iterator over a collection. */
	public interface IIterator
	{
		/**
		 * Ask if the iterator has another entry to return.
		 */
		function hasNext():Boolean;

		/**
		 * Get the next key-value entry in the collection.
		 */
		function next():IPair;

		/**
		 * Reset the iterator to iterate through the HashMap.
		 */
		function reset():void;
	}
}
