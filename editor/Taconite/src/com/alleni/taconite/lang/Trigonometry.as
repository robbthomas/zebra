/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.lang
{
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	public class Trigonometry
	{
		private static const ZERO:Point = new Point(0,0);
		
		public static function toRadians( degrees:Number ) :Number
		{
			return degrees * Math.PI / 180;
		}
		
		public static function toDegrees( radians:Number ) :Number
		{
			return radians *  180 / Math.PI;
		}
		
		public static function transformOffset(width:Number, height:Number, percentageOffset:Point, 
				rotation:Number=0, offset:Point=null):Point
		{
			if (!percentageOffset) percentageOffset = new Point(0,0);
			if (!offset) offset = new Point(0,0);
			
			var m:Matrix = new Matrix(	1, // first four form partial identity matrix
										0, 
										0, 
			 							1, 
										width * percentageOffset.x / 100 + offset.x, // The tX 
			 							height * percentageOffset.y / 100 + offset.y); // the tY 
			m.rotate( Trigonometry.toRadians( rotation ) );
			
			return m.transformPoint( ZERO ); 
		}
	}
}