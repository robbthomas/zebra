package com.alleni.taconite.graphics
{
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Matrix;

	public class ReflectiveFloor extends Sprite
	{
		private static const DEFAULT_BASE_COLOR:uint = 0x000000;
		private static const DEFAULT_THEME_COLOR:uint = 0x60c1ba;
		
		public var unscaledWidth:Number = 1000;
		public var unscaledHeight:Number = 300;
		public var baseColor:uint = DEFAULT_BASE_COLOR;
		public var themeColor:uint = DEFAULT_THEME_COLOR;
		
		
		public function ReflectiveFloor()
		{
			this.alpha = 0.5;
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
			 
		private function initialize(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			this.graphics.clear();
			/*this.graphics.beginFill(this.baseColor);
			this.graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);*/
			
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [0xffffff, DEFAULT_THEME_COLOR];
			var alphas:Array = [1.0, 0.0];
			var ratios:Array = [0x00, 0xff];
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(200,unscaledHeight,(Math.PI/2), 0,0);
			this.graphics.beginGradientFill(fillType, colors, alphas, ratios, matrix);
			this.graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);
			this.graphics.endFill();
			
			this.graphics.moveTo(0, 0);
			this.graphics.lineStyle(1, 0xffffff, 0.25);
			this.graphics.lineTo(unscaledWidth, 0);
			//this.graphics.moveTo(0, unscaledHeight-2);
			//this.graphics.lineStyle(1, 0xffffff, 0.15);
			//this.graphics.lineTo(unscaledWidth, unscaledHeight-2);
		}
		
		override public function set width(value:Number):void
		{
			this.unscaledWidth = value;
			initialize();
		}
		
		override public function set height(value:Number):void
		{
			this.unscaledHeight = value;
			initialize();
		}
	}
}