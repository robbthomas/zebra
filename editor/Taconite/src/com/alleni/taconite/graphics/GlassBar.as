package com.alleni.taconite.graphics
{
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Matrix;
	
	public class GlassBar extends Sprite
	{
		private static const DEFAULT_BASE_COLOR:uint = 0x000000;
		private static const DEFAULT_THEME_COLOR:uint = 0x60c1ba;
		private static const DEFAULT_GRADIENT_ANGLE:Number = Math.PI/2;
		
		public var unscaledWidth:Number = 1000;
		public var unscaledHeight:Number = 50;
		public var baseColor:uint = DEFAULT_BASE_COLOR;
		public var themeColor:uint = DEFAULT_THEME_COLOR;
		
		private var _highlight:Shape = null;
		private var _mainGradientAngle:Number = DEFAULT_GRADIENT_ANGLE;
		
		public function GlassBar(flip:Boolean=false)
		{
			_highlight = new Shape();
			
			if (flip) _mainGradientAngle = -_mainGradientAngle;
			if (stage) initialize();
			else addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		 
		private function initialize(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);

			this.graphics.clear();
			//this.graphics.beginFill(this.baseColor);
			//this.graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);
			
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [0x444444, 0x000000];
			var alphas:Array = [0.5, 1.0];
			var ratios:Array = [0x00, 0x99];
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(200, unscaledHeight, _mainGradientAngle, 0, 0);

			this.graphics.beginGradientFill(fillType, colors, alphas, ratios, matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
			this.graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);
			this.graphics.endFill();

			/*
			matrix.identity();
			matrix.createGradientBox(200, 30, (Math.PI/2), 0, 0);
			colors = [0xffffff, this.themeColor];
			alphas = [0.1, 0.05];
			ratios = [0x00, 0xff];
			this.graphics.beginGradientFill(fillType, colors, alphas, ratios, matrix, SpreadMethod.PAD, InterpolationMethod.RGB);
			this.graphics.drawRect(0, 0, unscaledWidth, unscaledHeight/2);
			this.graphics.endFill();
			*/

			this.graphics.moveTo(0, 0);
			this.graphics.lineStyle(1, 0xffffff, 0.25);
			this.graphics.lineTo(unscaledWidth, 0);
			this.graphics.moveTo(0, unscaledHeight-2);
			this.graphics.lineStyle(1, 0xffffff, 0.15);
			this.graphics.lineTo(unscaledWidth, unscaledHeight-2);
			
			_highlight.graphics.beginFill(0xffffff);
			_highlight.graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);
			_highlight.graphics.endFill();
			_highlight.alpha = 0;
		}
		
		override public function set width(value:Number):void
		{
			this.unscaledWidth = value;
			initialize();
		}
		
		override public function set height(value:Number):void
		{
			this.unscaledHeight = value;
			initialize();
		}
	}
}