package com.alleni.taconite.dev
{
	import com.alleni.taconite.service.LogService;
	import com.codeazur.as3swf.SWF;
	import com.codeazur.as3swf.SWFData;
	import com.codeazur.as3swf.data.SWFScene;
	import com.codeazur.as3swf.data.SWFSymbol;
	import com.codeazur.as3swf.tags.TagDefineSceneAndFrameLabelData;
	import com.codeazur.as3swf.tags.TagDefineSound;
	import com.codeazur.as3swf.tags.TagDoABC;
	import com.codeazur.as3swf.tags.TagEnd;
	import com.codeazur.as3swf.tags.TagFileAttributes;
	import com.codeazur.as3swf.tags.TagShowFrame;
	import com.codeazur.as3swf.tags.TagSymbolClass;
	
	import flash.utils.ByteArray;
	
	import org.as3commons.bytecode.abc.AbcFile;
	import org.as3commons.bytecode.emit.IAbcBuilder;
	import org.as3commons.bytecode.emit.IClassBuilder;
	import org.as3commons.bytecode.emit.IPackageBuilder;
	import org.as3commons.bytecode.emit.impl.AbcBuilder;
	import org.as3commons.bytecode.io.AbcSerializer;

	public class TaconiteSound
	{
		private static const PACKAGENAME:String = "tmp";
		private static const CLASSNAME:String = "SoundClass";
		public static const QNAME:String = PACKAGENAME + "." + CLASSNAME;
		
		
		public static function publish(bytes:ByteArray):ByteArray
		{
			// Create an empty SWF
			// Defaults to v10, 550x400px, 24fps, one frame
			var swf:SWF = new SWF();
			
			// Add FileAttributes tag
			// Defaults: as3 true, all other flags false
			swf.tags.push(new TagFileAttributes());
			
			// Add DefineSceneAndFrameLabelData tag 
			// (with the only entry being "Scene 1" at offset 0)
			var defineSceneAndFrameLabelData:TagDefineSceneAndFrameLabelData = new TagDefineSceneAndFrameLabelData();
			defineSceneAndFrameLabelData.scenes.push(new SWFScene(0, "Scene 1"));
			swf.tags.push(defineSceneAndFrameLabelData);
			
			// Add DefineSound tag
			// The ID is 1, all other parameters are automatically
			// determined from the mp3 itself.
			try {
				swf.tags.push(TagDefineSound.createWithMP3(1, bytes));
			} catch(event:Error) {
				LogService.error("caught exception in runtime compiler: " + event);
				return null;
			}
			
			// Add DoABC tag
			// Contains the AS3 byte code for the class definition for the embedded sound:
			// package tmp {
			//    public dynamic class SoundClass extends flash.media.Sound {
			//    }
			// }
			var abcBuilder:IAbcBuilder = new AbcBuilder();
			var packageBuilder:IPackageBuilder = abcBuilder.definePackage(PACKAGENAME);
			var classBuilder:IClassBuilder = packageBuilder.defineClass(CLASSNAME, "flash.media.Sound");
			var abcFile:AbcFile = abcBuilder.build();
			var abcSerializer:AbcSerializer = new AbcSerializer();
			var abcBytes:ByteArray = abcSerializer.serializeAbcFile(abcFile);
			swf.tags.push(TagDoABC.create(abcBytes));
			
			// Add SymbolClass tag
			// Specifies the document class and binds the sound class
			// definition to the embedded sound
			var symbolClass:TagSymbolClass = new TagSymbolClass();
			symbolClass.symbols.push(SWFSymbol.create(1, QNAME));
			swf.tags.push(symbolClass);
			
			// Add ShowFrame tag
			swf.tags.push(new TagShowFrame());
			
			// Add End tag
			swf.tags.push(new TagEnd());
			
			// Publish the SWF
			var swfData:SWFData = new SWFData();
			swf.publish(swfData);
			
			return swfData;
		}
	}
}