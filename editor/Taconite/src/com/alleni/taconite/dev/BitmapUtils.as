package com.alleni.taconite.dev
{
	import com.alleni.taconite.service.LogService;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.PixelSnapping;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.filters.BitmapFilter;
	import flash.filters.BlurFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class BitmapUtils
	{
		private static const ZERO:Point = new Point(0, 0);
		public static const BITMAPDATA_MAXIMUM_PIXELS:Number = 16777215.0;
			
		public static function center(bitmap:Bitmap):Bitmap
		{
			bitmap.x = -bitmap.width;
			bitmap.y = -bitmap.height;
			return bitmap;
		}
		
		public static function crop(displayObject:DisplayObject, x:Number, y:Number, width:Number, height:Number):Bitmap
		{
			var cropArea:Rectangle = new Rectangle(0, 0, width, height);
			var croppedBitmap:Bitmap = new Bitmap(new BitmapData(width, height), PixelSnapping.ALWAYS, true);
			croppedBitmap.bitmapData.draw(displayObject, new Matrix(1, 0, 0, 1, -x, -y), null, null, cropArea, true);
			return croppedBitmap;
		}
		
		public static function makePreviewRoundedIcon(bitmap:Bitmap, size:Number, ellipseSize:Number=0):DisplayObject
		{
			var mask:Shape = new Shape();
			mask.graphics.beginFill(0, 1);
			mask.graphics.drawRoundRect(0, 0, size, size, ellipseSize, ellipseSize);
			mask.graphics.endFill();
			
			// make smallest dimension the key scaling dimension
			var ratio:Number;
			var tx:Number = 0;
			var ty:Number = 0;
			if (bitmap.width < bitmap.height) {
				ratio = bitmap.height / bitmap.width;
				ty = (size * ratio - size)/2;
			} else {
				ratio = bitmap.width / bitmap.height;
				tx = (size * ratio - size)/2;
			}
			
			var bitmapSource:Bitmap = bilinearDownscale(bitmap, size * ratio);
			// translate and mask to perform the center crop
			var icon:Sprite = new Sprite;
			icon.addChild(crop(bitmapSource, tx, ty, size, size));
			icon.addChild(mask);
			icon.mask = mask;
			return icon;
		}
		
		// bilinear is suitable for downscaling provided new size is not more than half the original
		public static function bilinearDownscale(bitmap:Bitmap, width:Number, height:Number=0):Bitmap
		{
			if (height == 0) height = width; // constraints are square
			 
			if (bitmap.width < width && bitmap.height < height)
				return bitmap;
			else
				return bilinearScale(bitmap, width, height, true);
		}
		
		private static function bilinearScale(bitmap:Bitmap, width:Number, height:Number=0, maintainAspect:Boolean=true):Bitmap
		{	
			bitmap.smoothing = true;
			
			var scaleX:Number = width / bitmap.width;
			var scaleY:Number = height / bitmap.height;

			if (maintainAspect) {
				if (bitmap.width > bitmap.height)
					scaleX = width / bitmap.width;
				else
					scaleX = height / bitmap.height;
				scaleY = scaleX;

                if (width == 0) {
					scaleY = height / bitmap.height;
                    scaleX = scaleY;
                }
			}
			
			const floatWidth:Number = bitmap.width * scaleX;
			const floatHeight:Number = bitmap.height * scaleY;
			
			// ensure non-zero dimensions
			var width:Number = floatWidth > 0 ? floatWidth : 1;
			var height:Number = floatHeight > 0 ? floatHeight : 1;
			
			// ensure real dimensions
			width = width < 1 ? Math.ceil(width) : width;
			height = height < 1 ? Math.ceil(height) : height;
			
			var output:BitmapData = new BitmapData(width, height, true, 0x00ff0000);
			
			var matrix:Matrix = new Matrix();
			matrix.scale(scaleX, scaleY); // this performs bilinear algorithm
			 
			bitmap.smoothing = true;
			output.draw(bitmap, matrix, null, null, null, true);
			
			bitmap.bitmapData.dispose();
			bitmap = new Bitmap(output, PixelSnapping.AUTO, true); // smoothing=true for downscale
			return bitmap;
		}
		
		public static function tint(displayObject:DisplayObject, color:uint, alphaMultiplier:Number=1.0):DisplayObject
		{
			var myColor:ColorTransform = displayObject.transform.colorTransform;
			myColor.color = color;
			myColor.alphaMultiplier = alphaMultiplier;
			displayObject.transform.colorTransform = myColor;
			
			return displayObject;
		}
		
		public static function untint(displayObject:DisplayObject):DisplayObject
		{
			displayObject.transform.colorTransform = new ColorTransform();
			
			return displayObject;
		}
		
		public static function compositeVectorBase(base:DisplayObject, top:DisplayObject, blendMode:String="normal", center:Boolean=true):Bitmap // maybe add one which composites a Vector of layers with union bounds
		{
			var bitmapData:BitmapData = new BitmapData(base.width, base.height, true, 0x00ff0000);
			bitmapData.draw(base, null, null, null, null, true);
			return composite(new Bitmap(bitmapData), top, blendMode, center);
		}
		
		public static function composite(base:Bitmap, top:DisplayObject, blendMode:String="normal", center:Boolean=true):Bitmap // maybe add one which composites a Vector of layers with union bounds
		{
			var matrix:Matrix = new Matrix();
			if (center) {
				var baseBounds:Rectangle = base.getBounds(base);
				var topBounds:Rectangle = top.getBounds(top);
				matrix.tx = (baseBounds.width - topBounds.width)/2;
				matrix.ty = (baseBounds.height - topBounds.height)/2;
			}
			
			var composite:Bitmap = new Bitmap(base.bitmapData, PixelSnapping.AUTO, true);
			//composite.bitmapData.draw(base, null, new ColorTransform(0.5,0,0,1,0,0,0,0), BlendMode.OVERLAY); // example of tinting the base
			composite.bitmapData.copyPixels(base.bitmapData, new Rectangle(0, 0, base.width, base.height), ZERO, null, null, true);
			composite.bitmapData.draw(top, matrix, null, blendMode, null, true);
			return composite;
		}

		public static function pixelHitTest(displayObject:DisplayObject, point:Point):Boolean
		{
			/* If we're already dealing with a BitmapData object then we just use the hitTest
			* method of that BitmapData.
			*/
			var bmd:BitmapData = displayObject as BitmapData;
			if (bmd) {
				return bmd.hitTest(ZERO, 0, displayObject.globalToLocal(point));
			} else {
				
				/* First we check if the hitTestPoint method returns false. If it does, that
				* means that we definitely do not have a hit, so we return false. But if this
				* returns true, we still don't know 100% that we have a hit because it might
				* be a transparent part of the image. 
				*/
				if (!displayObject.hitTestPoint(point.x, point.y, true)) {
					return false;
				} else {
					/* So now we make a new BitmapData object and draw the pixels of our object
					* in there. Then we use the hitTest method of that BitmapData object to
					* really find out of we have a hit or not.
					*/
					var bitmapData:BitmapData = new BitmapData(displayObject.width, displayObject.height, true, 0x00000000);
					bitmapData.draw(displayObject, new Matrix());
					
					var isHit:Boolean = bitmapData.hitTest(ZERO, 0, displayObject.globalToLocal(point));
					
					bitmapData.dispose();
					
					return isHit;
				}
			}
		}
		 
		public static function createReflectionBitmapData(displayObject:DisplayObject, stretchFactor:Number=1.0, alpha:Number=1.0, blurFactor:Number=2.0, width:Number=0, height:Number=0, top:Number = 0, left:Number = 0):DisplayObject
		{
			var outputSprite:Sprite = new Sprite();

			// ensure non-zero dimensions
			width = width > 0 ? width : 1;
			height = height > 0 ? height : 1;
			if (width * height > BITMAPDATA_MAXIMUM_PIXELS) return null; // ensure we are not trying to render beyond the limits
			
			var bmd:BitmapData = new BitmapData(width, height, true, 0x00ffffff);
			var matrix:Matrix = new Matrix();
			 
			// filters can cause a display object to render outside of its rectangle
			for each (var filter:BitmapFilter in displayObject.filters) {
				var filterRect:Rectangle = bmd.generateFilterRect(bmd.rect, filter);
				width = filterRect.width > width ? filterRect.width : width;
				height = filterRect.height > height ? filterRect.height : height;
			}
			
			// create, invert, and position relfection bitmapdata
			bmd.dispose();
			bmd = new BitmapData(width+2, height * stretchFactor, true, 0x00ffffff);
			matrix.createBox(1, -1*stretchFactor, 0, -left, (height - (-top*2)/2)*stretchFactor);
			bmd.draw(displayObject, matrix);
			 
			var bitmapReflect:Bitmap = new Bitmap(bmd);
			outputSprite.addChild(bitmapReflect);
			
			// create gradient for reflection
			var grad:Sprite = new Sprite();
			matrix.createGradientBox(bitmapReflect.width, bitmapReflect.height / 2, Math.PI / 2, 0, 0);
			grad.graphics.beginGradientFill("linear", [0xffffff, 0xffffff], [1, 0], [0, 255], matrix);
			grad.graphics.drawRect(0, 0, bitmapReflect.width, bitmapReflect.height);
			grad.x = bitmapReflect.x;
			grad.y = bitmapReflect.y;
			grad.cacheAsBitmap = true;
			bitmapReflect.cacheAsBitmap = true;
			bitmapReflect.mask = grad;
			outputSprite.addChild(grad);
			
			outputSprite.alpha = alpha;
			if (blurFactor > 0) {
				var blurFilter:BitmapFilter = new BlurFilter(blurFactor, blurFactor, 2);
				outputSprite.filters = [blurFilter];		
			}
			return outputSprite;
		}
		
		/**
		 * manual bitmap caching, to prevent issues with sub-pixel shift caused by automatic caching. 
		 * @param displayObject the view to cache.
		 * @param overrideBounds bounds to use instead of the bounds of the view, if necessary
		 * @return 
		 * 
		 */
		public static function cacheMe(displayObject:DisplayObject, overrideBounds:Rectangle=null, withFilters:Boolean=false):DisplayObject
		{
			try {
				var bounds:Rectangle = displayObject.getBounds(displayObject);

				if (bounds.width*bounds.height > BITMAPDATA_MAXIMUM_PIXELS)
					return null;

				var bmd:BitmapData = new BitmapData(bounds.width, bounds.height, true, 0x00ffffff);
				var filterHolder:Array;
				
				if (overrideBounds) {
					bounds = overrideBounds;
					bmd.fillRect(new Rectangle(0, 0, bounds.width, bounds.height), 0xffffffff);
				} else {
					// ensure no filters make it into the copy, but reapply when done
					filterHolder = displayObject.filters;
					displayObject.filters = null;

					var matrix:Matrix = new Matrix();
					matrix.createBox(1, 1, 0, -bounds.x, -bounds.y);
					bmd.draw(displayObject, matrix, null, null, null, true); // smoothing helps with subpixel shifting
					displayObject.filters = filterHolder;
				}

				// create a bitmap to contain the data
				var bitmapCopy:Bitmap = new Bitmap(bmd);
				bitmapCopy.x = bounds.x;
				bitmapCopy.y = bounds.y;

				if (withFilters && filterHolder)
					bitmapCopy.filters = filterHolder;
				return bitmapCopy;
			} catch(error:Error) {
				LogService.error("Unable to create bitmap cache " + error.message);
			}
			return null;
		}
	}
}