package com.alleni.taconite.dev
{
	import com.codeazur.as3swf.SWF;
	import com.codeazur.as3swf.SWFData;
	import com.codeazur.as3swf.data.SWFSymbol;
	import com.codeazur.as3swf.tags.TagDefineFont4;
	import com.codeazur.as3swf.tags.TagDoABC;
	import com.codeazur.as3swf.tags.TagEnd;
	import com.codeazur.as3swf.tags.TagFileAttributes;
	import com.codeazur.as3swf.tags.TagShowFrame;
	import com.codeazur.as3swf.tags.TagSymbolClass;
	
	import flash.utils.ByteArray;
	
	import org.as3commons.bytecode.abc.AbcFile;
	import org.as3commons.bytecode.emit.IAbcBuilder;
	import org.as3commons.bytecode.emit.IClassBuilder;
	import org.as3commons.bytecode.emit.IPackageBuilder;
	import org.as3commons.bytecode.emit.impl.AbcBuilder;
	import org.as3commons.bytecode.io.AbcSerializer;

	public class TaconiteFont
	{
		private static const PACKAGENAME:String = "tmp";
		private static const CLASSNAME:String = "FontClass";
		public static const QNAME:String = PACKAGENAME + "." + CLASSNAME;
		

		public static function publish(bytes:ByteArray, name:String):ByteArray
		{
			// Create an empty SWF
			// Defaults to v10, 550x400px, 24fps, one frame
			var swf:SWF = new SWF();
			
			// Add FileAttributes tag
			// Defaults: as3 true, all other flags false
			swf.tags.push(new TagFileAttributes());
			
			// Add DefineFont4 tag
			// The ID is 1, all other parameters are automatically
			// determined from the font itself.
			///swf.tags.push(TagDefineFont4.createWithFont(1, bytes, name));
			
			/*
				[56:ExportAssets] 
					Assets:
					[0] TagID: 1, Name: Verdana
			*/
			//swf.tags.push(TagExportAssets.createWithSimpleSymbol(1, name));
				
			// Add DoABC tag
			// Contains the AS3 byte code for the class definition for the embedded font
			var abcBuilder:IAbcBuilder = new AbcBuilder();
			var packageBuilder:IPackageBuilder = abcBuilder.definePackage(PACKAGENAME);
			var classBuilder:IClassBuilder = packageBuilder.defineClass(CLASSNAME, "flash.text.Font");
			var abcFile:AbcFile = abcBuilder.build();
			var abcSerializer:AbcSerializer = new AbcSerializer();
			var abcBytes:ByteArray = abcSerializer.serializeAbcFile(abcFile);
			swf.tags.push(TagDoABC.create(abcBytes));
			
			// Binds the document class definition to the embedded font
			var symbolClass:TagSymbolClass = new TagSymbolClass();
			symbolClass.symbols.push(SWFSymbol.create(1, QNAME));
			swf.tags.push(symbolClass);
			
			// Add ShowFrame tag
			swf.tags.push(new TagShowFrame());
			
			// Add End tag
			swf.tags.push(new TagEnd());
			
			// Publish the SWF
			var swfData:SWFData = new SWFData();
			swf.publish(swfData);
			
			return swfData;
		}
	}
}