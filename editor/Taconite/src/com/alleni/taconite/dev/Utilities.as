package com.alleni.taconite.dev
{
	import avmplus.getQualifiedClassName;
	
	import com.alleni.taconite.model.TaconiteModel;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
import flash.globalization.DateTimeFormatter;

import mx.collections.IList;
	import mx.formatters.DateFormatter;

	public class Utilities
	{	
		public static const FILENAME_FROM_URL_REGEX:RegExp = /[\w_.-]*?(?=\?)|[\'\&\%\+\s\w_\(\).-]*$/i;
        public static const PLAYER_ID:RegExp = /^(verlock)?[A-Za-z0-9]{32}$/;
		public static const UUID_REGEX:RegExp = /^[A-Za-z0-9]{32}$/;
		public static const UID_REGEX:RegExp = /^[A-Za-z0-9-]{36}$/;
		
		private static const COPY_PREFIX:String = " copy";
		
		public static function assert(condition:*, message:String=""):void
		{
			if (!condition) {
				throw new Error("Assertion failed! " + message);
			}
		}
		

		/**
		 * Get the short class name of an object, such as "Rectangle" 
		 * @param obj
		 * @return 
		 * 
		 */
		public function shortClassName(obj:Object):String
		{
			var array:Array = getQualifiedClassName(obj).split("::");
			if (array.length >= 2)
				return array[1];
			else 
				return array[0];
		}
		
		public static function nameForCopy(originalName:String, overrideCount:int=-1):String
		{
			const MIN_INDEX:int = 1; // 2 would be the smallest index as this is at least the second copy
			var newName:String = originalName;
			var match:Boolean =  originalName.indexOf(COPY_PREFIX) > -1;
			if (!match)
				newName = newName+COPY_PREFIX;
			
			var copyCount:int = overrideCount > -1?overrideCount:MIN_INDEX; 
			var copyTestArray:Array = /(.*)\s([\d+])$/.exec(newName);
			if (copyTestArray != null) {
				newName = copyTestArray[1]; // name without the copy count
				copyCount = int(copyTestArray[2]); // copy count extracted from original name
				copyCount++;
			}
			if (copyCount >= MIN_INDEX)
				newName += " " + copyCount;
			
			return newName;
		}
		
		public static function dumpStack():String
		{
			var buffer:String = "";
			var str:String = new Error().getStackTrace();
            if(str == null) {
                // we are not in a debugger
                return null;
            }
			var str2:String = str.replace(/Err.*\n.*\n/m, "");  // replace first two lines with nothing
			buffer = str2;
			trace(buffer);
			return buffer;
		}

		public static function dumpDisplayObjectContainer(parent:DisplayObjectContainer, title:String="",levels:int=99):String
		{	
			var buffer:String = "";
			if (parent==null) {
				buffer = "\nDump: ",title+"=null"
				trace(buffer);
			} else {
				buffer = "\nDump: "+title, " visible="+parent.visible;
				trace(buffer);
				buffer += dumpChildren(parent, "  ",levels) + "\n";
				trace("");
			}
			return buffer;
		}
		
		private static function dumpChildren( parent:DisplayObjectContainer, indent:String, levels:int):String
		{	
			var buffer:String = "";
			if (levels > 0) {
				var count:int = parent.numChildren;
				for (var i:int = 0; i < count; i++)
				{
					var view:DisplayObject = parent.getChildAt(i);
					var viewStr:String = view.toString();
					var viewCoordStr:String = "";
					if (viewStr.indexOf("XY=") == -1)  // if view doesn't show its coordinates, we will  (ObjectView does show its coordinates)
						viewCoordStr = "XY=" + int(view.x) + "," + int(view.y) + " WH="+int(view.width) + "," + int(view.height);
					if (view is DisplayObjectContainer) {
						var bufferLine:String = indent + viewStr + " " + viewCoordStr + " vis="+view.visible;
						trace(bufferLine);
						bufferLine += dumpChildren(view as DisplayObjectContainer, indent + "  ",levels-1);
					}
					buffer += bufferLine + "\n";
				}
			}
			return buffer;
		}
		
		public static function dumpModelList( parent:TaconiteModel, title:String ):String
		{
			var buffer:String = "";
			if (parent==null) {
				buffer = "\nDump: ",title+"=null";
				trace(buffer);
			} else {
				buffer = "\nDump: "+title, " numChildren="+parent.numChildren
				trace(buffer);
				buffer += dumpModelChildren(parent, "  ") + "\n";
				trace("");
			}
			return buffer;
		}
		
		public static function get prettyDate():String
		{
			var df:DateFormatter = new DateFormatter();
			df.formatString = "EEE MMM D L:NNA";
			return df.format(new Date());
		}
		
		public static function get uniqueDateBasedName():String
		{
			var df:DateFormatter = new DateFormatter();
			df.formatString = "MM-DD.L:NN:S";
			return df.format(new Date());
		}
			
		private static function dumpModelChildren(parent:TaconiteModel,indent:String):String
		{
			var buffer:String = "";
			var count:int = parent.numChildren;
			for (var i:int=0; i < count; i++)
			{
				var m:TaconiteModel = parent.getChildAt(i);
				var bufferLine:String = indent + m;
				trace(bufferLine);
				buffer += bufferLine + "\n";
				dumpModelChildren(m,indent+"   ");
			}
			return buffer;
		}
		
		public static function dumpJson(json:Object, level:int=0):void
		{
			for (var name:String in json) {
				var val:* = json[name];
				var type:String = getQualifiedClassName(val);
				var indent:String = "";
				for (var i:int=0; i < level; i++) 
					indent += " ";
				
				if (val == null) {
					trace(indent + name + ":null"); 
				} else if (type == "Object" || type == "Array") {
					var values:String = allScalar(val);
					if (values == null) {
						trace(indent + name + ":" + type); 
						dumpJson(val, level+2);
					} else {
						trace(indent + name + ":" + type + "  [" + values + "]"); 
					}
				} else if (type == "String") {
					trace(indent + name + ":\"" + val + "\" "); 
				} else {
					trace(indent + name + ":" + val.toString()); 
				}
			}
		}
		
		private static function allScalar(json:Object):String
		{
			const spacing:String = " ";
			var result:String = "";
			for (var name:String in json) {
				var val:* = json[name];
				var type:String = getQualifiedClassName(val);
				
				if (val == null) {
					result += name + ":" + null + spacing;
				} else if (type == "Object" || type == "Array") {
					return null;  // not all scalar values
				} else if (type == "String") {
					result += name + ":" + "\"" + val + "\"" + spacing; 
				} else {
					result += name + ":" + val.toString() + spacing; 
				}
			}
			return result;
		}
        private static var dtf:DateTimeFormatter = new DateTimeFormatter("en-US");
        {
            dtf.setDateTimePattern("yyyy-MM-dd'T'HH:mm:ss");
        }

        public static function get timestamp():String {
            return dtf.format(new Date());
        }

	}
}
