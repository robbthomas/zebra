package com.alleni.taconite.dev
{
	import flash.display.DisplayObject;
	import flash.geom.Matrix;

	public class GraphicUtils
	{
		public static function rotate(degrees:Number, displayObject:DisplayObject):DisplayObject
		{
			var matrix:Matrix = displayObject.transform.matrix;
			matrix.translate(-displayObject.width/2, -displayObject.height /2);
			matrix.rotate(degrees/180 * Math.PI);
			matrix.translate(displayObject.width/2, displayObject.height /2);
			displayObject.transform.matrix = matrix;
			return displayObject;	
		}
		
		public static function scaleForAspect(width:Number, height:Number, constraint:Number):Number
		{
			if (width > height)
				return constraint / width;
			else
				return constraint / height;
		}
	}
}