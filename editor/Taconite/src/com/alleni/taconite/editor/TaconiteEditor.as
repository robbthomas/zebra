package com.alleni.taconite.editor
{
	//import com.alleni.taconite.service.IConfigurationService;
	
	import mx.containers.Canvas;

	/*
	Keep these in comments, since FB keeps deleting them.  When it does, here they are to copy and paste
	back into the code.  Ugh!
	
	import mx.core.Application;
	import mx.core.ScrollPolicy;
	import mx.events.ResizeEvent;
	*/
	/*import com.alleni.taconite.controller.ITaconiteController;
	import com.alleni.taconite.document.TaconiteDocument;
	import com.alleni.taconite.event.EditorEvent;
	import com.alleni.taconite.event.ProgressSourceEvent;
	import com.alleni.taconite.service.IConfigurationService;
	import com.alleni.taconite.service.ITaconiteDocumentService;
	import com.alleni.taconite.service.TaconiteDocumentData;
	import com.alleni.taconite.view.ITaconiteView;
	import com.alleni.taconite.view.ViewContext;
	import com.alleni.taconite.view.ViewInfo;
	
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.printing.PrintJob;
	
	import mx.containers.Canvas;
	import mx.core.ScrollPolicy;
	import mx.core.UIComponent;
	import mx.events.ResizeEvent;
	import mx.managers.PopUpManager;
	
	[Event(type="com.alleni.taconite.event.EditorEvent",name="documentChanged")]
	[Event(type="com.alleni.taconite.event.EditorEvent",name="documentLayoutChange")]
	[Event(type="com.alleni.taconite.event.EditorEvent",name="displayScaleChange")]*/
	
	/**
	 * TaconiteEditor is the top-level UI component in the Taconite framework.  An application need only
	 * include and initialize the editor properly in order to utilize Taconite.  An instance of this object
	 * contains:
	 * 
	 * <ul>
	 *   <li>an instance of TaconiteDocument containing the current selection state and undo history
	 *   <li>an MVC triad based on the root model object in that document
	 *   <li>a family of superimposed layers for feedback and overlays on top of the base view of the model
	 *   <li>service objects and document state for managing persistence
	 * </ul>
	 * 
	 * Note that menu bars, tool bars, and so forth are completely separate from the editor.
	 * 
	 * TaconiteEditor view hierarchy:
	 * 		Editor Canvas (this, unscaled, contains scrollers)
	 * 		[
	 *			documentContainer
	 *			[
	 *				documentLayer (displays the document.root)
	 * 				[
	 * 					documentView
	 * 				]
	 *				feedbackLayer
	 * 			]
	 *			overlayLayer
	 * 		]
	 */
	public class TaconiteEditor extends Canvas
	{
		/**
		 * Flag indicating that the editor is completely loaded.  
		 */
		/*[Bindable]
		public var complete:Boolean = false;*/
		
		/**
		 * ViewInfo object representing the viewing state of this editor.
		 */		
		/*[Bindable]
		public var viewInfo:ViewInfo = new ViewInfo();*/
		
		/**
		 * Magnification scale in use for this editor's view. 
		 */		
		/*[Bindable]
		public var viewScale:Number = 1.0;*/
		
		/**
		 * Configuration service instance used by this editor. 
		 */
		/*[Bindable]
		public var configurationService:IConfigurationService = null;*/
		
		/**
		 * Document service used by this editor to load and save documents. 
		 */		
		/*[Bindable]
		public var documentService:ITaconiteDocumentService = null;*/
		
		/**
		 * Service-related document state retrieved from the service layer. 
		 */		
		/*[Bindable]
		public var documentData:TaconiteDocumentData = null;*/
		
		/*protected var _document:TaconiteDocument;*/
		//REMOVE ME
		//private var _controller:ITaconiteController;
		/*private var _documentView:ITaconiteView;*/
		/*protected var _viewContext:ViewContext;*/
		
		//I hope we can remove this reference at some point
		//public var controller:ITaconiteController;
		
		// Current pointer tool to be used by ViewContext	 
		//private var _pointerTool:String = ViewContext.SELECT_TOOL;
		
		/**
		 * UIComponent containing all views of the document model, appropriately scaled. 
		 */		
		//public var documentLayer:UIComponent;
		//public var childDefinitions:Array;
		
		/**
		 * Transparent layer on top of the documentLayer, scaled and offset identically.  Contains temporary
		 * visual feedback objects.
		 */		
		//protected var feedbackLayer:UIComponent;  // scaled feedback aligned with document view
		//public var feedbackHandlesView:Sprite;
		//public var feedbackUIView:UIComponent;
		//public var feedbackControlsView:UIComponent;
		//public var feedbackAnnotationView:UIComponent;
		
		//public var documentContainer:Canvas;
		
		//protected var overlayLayer:UIComponent;	// unscaled feedback for global overlays
		//private var loadingPopup:LoadingPopup = null;
		
		
		/**
		 * Keyboard focus management.  Gives focus holders a chance to decline releasing focus. 
		 */
		//private static var _focusCloseFunction:Function = null;
		//private static var _focusLockObj:Object;
		//private static var _stage:Stage;
		
		/**
		 * Construct a TaconiteEditor.
		 */
		public function TaconiteEditor()
		{
			super();
			
			/*configurationService = configService;*/
		}
		/**
		 * Initialize this editor. 
		 * 
		 */
		public function initializeEditor():void
		{
			// Size the editor so it is (and stays) the size of the EditorContainer.
			/*percentWidth = 100;
			percentHeight = 100;*/
			
			// TODO: these should be set based off the initialize size of the editor (the world) relative to the editor container.
			/*horizontalScrollPolicy = ScrollPolicy.OFF;
			verticalScrollPolicy = ScrollPolicy.OFF;*/
			
			//REMOVE ME
			/*controller = createController();
			_stage = stage;*/
			
			//_textEditMediator = new TextEditMediator(_controller, this);
			/*documentService = createDocumentService();
			childDefinitions = createChildDefinitions();*/
			
			// aggressively funnel keystrokes into our key mediator
			
			//setFocus();
			
			//REMOVE ME
			/*if (configurationService.documentUri != null)
			{
				loadDocument(configurationService.documentUri);
			}*/
		}
		
		/**
		 * Initialize the child components of this editor.
		 */
		/*override protected function createChildren():void
		{
			super.createChildren();
			
			
			documentContainer = new Canvas();
			addChild(documentContainer);
			// Listen for sizing changes to the documentContainer, so we can center it within the editor.
			documentContainer.addEventListener(ResizeEvent.RESIZE, handleDocumentContainerResize);
			
			// DocumentLayer
			documentLayer = createDocumentLayer();
			documentContainer.addChild(documentLayer);
			
			// FeedbackLayer
			feedbackLayer = new UIComponent();
			feedbackHandlesView = new Sprite();
			feedbackAnnotationView = new UIComponent();
			feedbackLayer.addChild(feedbackHandlesView);
			feedbackLayer.addChild(createFeedbackControlsView());
			feedbackLayer.addChild(createFeedbackUIView());
			feedbackLayer.addChild(feedbackAnnotationView);
			documentContainer.addChild(feedbackLayer);
			
			
			overlayLayer = new UIComponent();
			addChild(overlayLayer);
		}*/
		
		// Respond to a resize event.
		/*private function handleDocumentContainerResize(event:ResizeEvent):void {
			centerDocumentContainer(width, height);
		}*/
		
		/**
		 * Abstract factory method to create this application's controller instance.
		 */
		/*protected function createController():ITaconiteController
		{
			throw new Error("createController() must be overridden");
		}*/
		
		/**
		 * Factory method to create a document layer to contain the top level ITaconiteView. 
		 */
		/*protected function createDocumentLayer():UIComponent
		{
			if (flexDocumentView)
			{
				var canvas:Canvas = new Canvas();
				canvas.percentWidth = canvas.percentHeight = 100;
				canvas.clipContent = false;
				return canvas;
			}
			else
			{
				return new UIComponent();
			}
		}*/
		
		/**
		 * Abstract factory method to create this application's top level view.
		 */
		/*protected function createDocumentView(context:ViewContext):ITaconiteView
		{
			throw new Error("createDocumentView() must be overridden");
		} */
		
		/**
		 * Abstract factory method to create this application's top level view.
		 */
		/*protected function createFeedbackUIView():UIComponent
		{
			throw new Error("createDocumentView() must be overridden");
		} */
		
		/**
		 * Abstract factory method to create this application's top level view.
		 */
		/*protected function createFeedbackControlsView():UIComponent
		{
			throw new Error("createDocumentView() must be overridden");
		} */
		
		/**
		 * Abstract factory method to create this application's document service
		 */
		/*protected function createDocumentService():ITaconiteDocumentService
		{
			throw new Error("createDocumentService() must be overridden");
		} */
		
		/*protected function createChildDefinitions():Array
		{
			throw new Error("createChildDefinitions() must be overridden");
		} */
		
		/**
		 * Abstract factory method to create this application's view context
		 */
		/*protected function createViewContext(info:ViewInfo, controller:ITaconiteController, editor:TaconiteEditor, stage:Stage):ViewContext
		{
			return new ViewContext(info, controller, editor, stage);
		}*/
		
		
		/**
		 * Override default key down handling in Container that would affect scroll bar position, etc.  
		 */
		/*override protected function keyDownHandler(e:KeyboardEvent):void
		{
		}*/
		
		
		/**
		 * Recreate the current top-level view and position it within the editor as needed
		 */
		/*private function validateDocumentLayout():void
		{
			while (documentLayer.numChildren > 0) {
				documentLayer.removeChildAt(0);
				_documentView.warnViewToDelete();
				_documentView = null;
				_viewContext = null;
			}
			//viewInfo.displayScale = viewScale;
			_viewContext = createViewContext(viewInfo, controller, this, stage);
			_viewContext.pointerTool = _pointerTool;
			_documentView = createDocumentView(_viewContext);
			_documentView.initialize();
			documentLayer.addChild(_documentView as DisplayObject);
			updateDimensions();
			
			dispatchEvent(new EditorEvent(EditorEvent.DOCUMENT_LAYOUT_CHANGE));
		}*/
		
		/*private function updateDimensions():void
		{
			documentLayer.height = _documentView.height;
			documentLayer.width = _documentView.width;
		}*/
		
		/*private function handleProgressStart(e:ProgressSourceEvent):void
		{
			if (loadingPopup == null)
			{
				loadingPopup = PopUpManager.createPopUp(this, LoadingPopup, true) as LoadingPopup;
				PopUpManager.centerPopUp(loadingPopup);
			}
			loadingPopup.addEventListener(Event.REMOVED, handleLoadingPopupRemoved);
			loadingPopup.addProgressSource(e.source, e.sourceName);
		}*/
		
		/*private function handleLoadingPopupRemoved(e:Event):void
		{
			loadingPopup = null;
		}*/
		
		/**
		 * Set a view offset in scaled document coordinates. 
		 */
		/*public function setViewOffset(x:Number, y:Number):void
		{
			documentLayer.x = feedbackLayer.x = x;
			documentLayer.y = feedbackLayer.y = y;
		}*/
		
		/**
		 * Set the display scale, scale the documentContainer, and adjust the scrollbars as needed. 
		 */
		/*public function setScale(newScale:Number):void
		{
			// Round down the newScale to 1 place of precision.
			var roundedScale:Number = Math.floor(newScale * 10);
			newScale = roundedScale / 10.0;
			
			// Calculate the scaling factor.
			var factor:Number = newScale / viewScale;
			
			// Remember the current documentContainer dimensions.
			var currentDocWidth:Number = documentContainer.width;
			var currentDocHeight:Number = documentContainer.height;
			
			// Remember the new scale. 
			viewScale = newScale;
			viewInfo.displayScale = viewScale;
			
			// Scale the documentContainer.
			documentContainer.scaleX = viewScale; 
			documentContainer.scaleY = viewScale;
			
			// Calculate the new documentContainer dimensions and center the documentContainer with respect to the editor view
			var newDocWidth:Number = (currentDocWidth * factor);
			var newDocHeight:Number = (currentDocHeight * factor);
			var widthDelta:Number = width - newDocWidth;
			var heightDelta:Number = height - newDocHeight;
			var newX:Number = widthDelta / 2.0;
			var newY:Number = heightDelta / 2.0;
			documentContainer.x = newX;
			documentContainer.y = newY;
			
			// Hide/show the scrollers.
			if (newDocWidth <= width) {
				horizontalScrollPolicy = ScrollPolicy.OFF;
			}
			else {
				// Check if the scroller is current invisible.
				if (horizontalScrollPolicy == ScrollPolicy.OFF) {
					// The scroller is becoming visible, so center the scroll.
					horizontalScrollPosition = (documentContainer.width * factor) / 2.0;
				}
				
				horizontalScrollPolicy = ScrollPolicy.ON;
			}
			if (newDocHeight <= height) {
				verticalScrollPolicy = ScrollPolicy.OFF;
			}
			else {
				// Check if the scroller is current invisible.
				if (verticalScrollPolicy == ScrollPolicy.OFF) {
					// The scroller is becoming visible, so center the scroll.
					verticalScrollPosition = (documentContainer.height * factor) / 2.0;
				}
				
				verticalScrollPolicy = ScrollPolicy.ON;
			}
			
			// Tell our listeners that the scale changed.
			dispatchEvent(new EditorEvent(EditorEvent.DISPLAY_SCALE_CHANGE));
		}*/
		
		/**
		 * Called in response to resize events by the EditorContainer.  Center the documentContainer within
		 * the given dimensions and enable/disable the scrollers as necessary.
		 */
		/*public function centerDocumentContainer(parentWidth:Number, parentHeight:Number):void {
			if (documentContainer.width <= 0 || documentContainer.height <= 0) {
				// Nothing to center in.  Mostly likely, the documentContainer hasn't finished initializing.
				return;
			}
			
			updateDimensions();
			
			// Calculate and set the documentContainer position.
			var widthDelta:Number = parentWidth - documentContainer.width;
			var heightDelta:Number = parentHeight - documentContainer.height;
			var newX:Number = widthDelta / 2.0;
			var newY:Number = heightDelta / 2.0;
			
			// Only center the documentContainer if the origin is visible.
			if (newX >= 0.0) {
				documentContainer.x = newX;
			}
			else {
				documentContainer.x = 0.0;
			}
			if (newY >= 0.0) {
				documentContainer.y = newY;
			}
			else {
				documentContainer.y = 0.0;
			}
			
			// Hide/show the scrollers.
			if (documentContainer.width > parentWidth) {
				horizontalScrollPolicy = ScrollPolicy.ON;
			}
			else {
				horizontalScrollPolicy = ScrollPolicy.OFF;
			}
			if (documentContainer.height > parentHeight) {
				verticalScrollPolicy = ScrollPolicy.ON;
			}
			else {
				verticalScrollPolicy = ScrollPolicy.OFF;
			}
		}*/
		
		/*public function fitEverything():void {
			
			// Fit the _documentView.width into the editor's width
			var scaleFactor:Number = width / _documentView.width;
			
			// Make sure the scaleFactor is enough to fit the _documentView.height into the editor's height
			var newDocContainerHeight:Number = scaleFactor * _documentView.height;
			if (newDocContainerHeight > height) {
				// Fit the _documentView.height into the editor's height
				scaleFactor = height / _documentView.height;
			}
			
			// Fit.
			setScale(scaleFactor);
		}
		
		public function fillView():void {
			
			// Fit the _documentView.width into the editor's width
			var scaleFactor:Number = width / _documentView.width;
			
			// Make sure the scaleFactor is enough to fit the _documentView.height into the editor's height
			var newDocContainerHeight:Number = scaleFactor * _documentView.height;
			if (newDocContainerHeight < height) {
				// Fit the _documentView.height into the editor's height
				scaleFactor = height / _documentView.height;
			}
			
			// Fill.
			setScale(scaleFactor);
		}*/
		
		/**
		 * Request keyboard focus. 
		 * @param objectName = name for debug purposes.  Typically pass this.toString()
		 * @param closingFunction = function for closing text:  closeFunction():Boolean
		 * @return true if focus is successfully changed.  Returns false if previous holder declines to give up focus.
		 * 
		 */
		/*public static function requestKeyboardFocus( obj:InteractiveObject, closingFunction:Function, stealFocus:Boolean=false ):Boolean
		{
			if (_focusLockObj == obj) return true;
			
			// ask the current holder to release focus ... if true then he released it
			if (TaconiteEditor.resetKeyboardFocus(stealFocus)) {
				Application.application.removeEventListener(KeyboardEvent.KEY_DOWN, _keyMediator.handleKey);
				_stage.removeEventListener(KeyboardEvent.KEY_DOWN, _keyMediator.handleKey);
				
				_stage.focus = obj;
				_focusCloseFunction = closingFunction;
				_focusLockObj = obj;
				return true;
			} else
				return false;
		}*/
		
		/**
		 * Close keyboard focus. 
		 *	This would be called by Run, Save, Close commands...  to close the current editing text.
		 * @return true if successful ... and the Run/Save can proceed
		 * 
		 */
		/*public static function resetKeyboardFocus(stealFocus:Boolean=false):Boolean
		{	
			// Reinstate the keyboard event listener on the application
			//Application.application.addEventListener(KeyboardEvent.KEY_DOWN, _keyMediator.handleKey);
			//_stage.addEventListener(KeyboardEvent.KEY_DOWN, _keyMediator.handleKey);
			
			_stage.focus = _focusLockObj as InteractiveObject;
			
			if (_focusCloseFunction != null) {
				if (_focusCloseFunction(stealFocus)) {
					_focusCloseFunction = null;
					_focusLockObj = null;
					return true;
				} else
					return false;
			}						
			return true;
		}
		
		public static function get focusLocked():Boolean
		{
			return (_focusLockObj != null);	
		}
		
		public function contentPointFor(view:DisplayObject, point:Point=null):Point
		{
			if (view != null)
				return documentContainer.globalToContent( view.localToGlobal(point == null ? new Point(0,0) : point) );
			else
				return documentContainer.globalToContent(point == null ? new Point(0,0) : point)
		}
		
		public function get mousePoint():Point
		{
			return new Point(documentContainer.contentMouseX, documentContainer.contentMouseY);	
		}
		
		public function get croppedWidth():Number
		{
			return documentContainer.parent.width;
		}
		
		public function get croppedHeight():Number
		{
			return documentContainer.parent.height;
		}
		
		public function get editorStage():Stage{
			return _stage;
		}*/
	}
}
