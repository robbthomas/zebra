package com.alleni.taconite.editor
{
	import com.alleni.taconite.factory.ITaconiteWindow;
	
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;

	public class TaconiteCursors
	{
		public static var instance:TaconiteCursors = new TaconiteCursors();
		
		public static const ARROW:String 		= MouseCursor.ARROW;
		public static const AUTO:String 		= MouseCursor.AUTO;
		public static const BUTTON:String 		= MouseCursor.BUTTON;
		public static const HAND:String 		= MouseCursor.HAND;
		public static const IBEAM:String 		= MouseCursor.IBEAM;
		public static const EYEDROPPER:String 	= "eyedropper";
		public static const ZOOMIN:String 		= "zoomIn";
		public static const ZOOMOUT:String 		= "zoomOut";
		
		[Embed(source="/assets/cursors/eyedrop.png")]
		private var Eyedropper:Class;
		
		[Embed(source="/assets/cursors/zoomIn.png")]
		private var ZoomIn:Class;
		
		[Embed(source="/assets/cursors/zoomOut.png")]
		private var ZoomOut:Class;
		
		private var _windowCache:ITaconiteWindow;
		private var _cursorID:int;
		private var _lock:Boolean = false;
		
		public var defaultCursor:String = MouseCursor.ARROW;
		
		public function showDefault(unlock:Boolean=false):void
		{
			/*_lock = !unlock	
			Mouse.cursor = defaultCursor;
			Mouse.show();
			clearCustom();*/
			_lock = !unlock;
			showAuto();
		}
		
		public function setCustom(cursor:String, window:ITaconiteWindow, lock:Boolean=false):void
		{
			var customCursor:Object;
			switch(cursor){
				case EYEDROPPER:
					customCursor = {theClass:Eyedropper, priority:10, xOffset:-2, yOffset:-15};
					break;
				case ZOOMIN:
					customCursor = {theClass:ZoomIn, priority:10, xOffset:-6, yOffset:-6};
					break;
			}
			if(_cursorID){
				_windowCache.cursorManager.removeCursor(_cursorID);
			}
			//_cursorID = window.cursorManager.setCursor(customCursor.theClass, customCursor.priority, customCursor.xOffset, customCursor.yOffset);
			_windowCache = window;
			_lock = lock;
		}
		
		private function clearCustom():void
		{
			if (_windowCache != null && !_lock) _windowCache.cursorManager.removeCursor(_cursorID);
		}
		
		public function showArrow():void
		{
			clearCustom();
			Mouse.cursor = ARROW;
		}
		
		public function showAuto():void
		{
			clearCustom();
			Mouse.cursor = AUTO;
		}

		public function showFinger():void
		{
			clearCustom();
			Mouse.cursor = BUTTON;
		}

		public function showHand():void
		{
			clearCustom();
			Mouse.cursor = HAND;
		}

		public function showText():void
		{
			clearCustom();
			Mouse.cursor = IBEAM;
		}
	}
}