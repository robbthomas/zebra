/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$
 */
package com.alleni.taconite.editor
{
	/** Functions that ITaconiteMenuMediators must implement to handle application specific behaviors for the associate events. */
	public interface ITaconiteMenuMediator
	{
		function handleQuit():void;
	}
}