/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.view
{
    import com.alleni.taconite.definition.HandleDescription;
    import com.alleni.taconite.definition.HandleRoles;
    
    import flash.display.BlendMode;
    import flash.display.CapsStyle;
    import flash.display.LineScaleMode;
    import flash.display.Shape;
    import flash.geom.Point;

    /**
     * Graphic used for dragging some control point of an object.
     */
    public class AnchorHandle extends AbstractHandle
    {
    	public var descriptor:HandleDescription;
    	
        /** Thickness of border drawn in rollover state. */
        public var highlightThickness:Number = 1;
		
		private var _center:Shape;
        
        public function AnchorHandle(context:ViewContext)
        {
			descriptor = new HandleDescription(HandleRoles.MOVE_PIVOTANCHOR,new Point(0,0), new Point(0,0));
			//this.mouseEnabled = false;
            super(context);
        }
        
        override protected function updateGraphics():void
        {
			if (!_center) {
				_center = new Shape();
				this.addChild(_center);
			}
			graphics.clear();
			graphics.lineStyle(anchorSize+1, 0x323232, 0.5, false, LineScaleMode.NONE, CapsStyle.ROUND);
			graphics.moveTo(0,0);
			graphics.lineTo(0.25,0.25);
			
			graphics.lineStyle(anchorSize, 0xffffff, 0.1, false, LineScaleMode.NONE, CapsStyle.ROUND);
			graphics.moveTo(0,0);
			graphics.lineTo(0.25,0.25);
			
			// used later to erase the center of the circle
			_center.graphics.clear();
			_center.graphics.lineStyle(handleSize+1, 1, 1, false, LineScaleMode.NONE, CapsStyle.ROUND);
			_center.graphics.moveTo(0,0);
			_center.graphics.lineTo(0.25,0.25);
			
			// create the "broken circle" anchor
			const crossSize:Number = anchorSize-2.375;
			
			_center.graphics.lineStyle(anchorSize/4, 0xff0000, 1, false, LineScaleMode.NONE, CapsStyle.SQUARE);
			_center.graphics.moveTo(-crossSize/2,0);
			_center.graphics.lineTo(crossSize/2,0);
			_center.graphics.moveTo(0,-crossSize/2);
			_center.graphics.lineTo(0,crossSize/2);
			
			this.blendMode = BlendMode.LAYER;
			_center.blendMode = BlendMode.ERASE;
        }
    }
}