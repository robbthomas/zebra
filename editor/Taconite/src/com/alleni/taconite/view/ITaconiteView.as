package com.alleni.taconite.view
{
    import com.alleni.taconite.model.TaconiteModel;
    
    import flash.display.IBitmapDrawable;
    import flash.events.IEventDispatcher;

    /**
     * ITaconiteView is an interface common to all Taconite views, regardless of their concrete superclass
     * (which may be any one of a number of various Flash or Flex classes).
     */
    public interface ITaconiteView extends IEventDispatcher
    {
        //
        // DisplayObject METHODS
        //
		
		/**
		 * X position of this view relative to its parent.
		 */
		function get x():Number;
        
        /**
         * Y position of this view relative to its parent.
         */        
        function get y():Number;

        /**
         * Width of this view. 
         */
        function get width():Number;

        /**
         * Height of this view. 
         */
        function get height():Number;
        
        //
        // TaconiteView/TaconiteCanvas METHODS
        //
        
        /**
         * initialize the children, display, status 
         * 
         */
        function initialize():void
        
        /**
         * The ViewContext used by this ITaconiteView.
         */
        function get context():ViewContext;
        
        /**
         * The underlying TaconiteModel object displayed by this view.
         */        
        function get model():TaconiteModel;
              
        function setFocus():void;
		
		function warnViewToDelete():void;

        /**
         * Create a new child view corresponding to a child model object. 
         * @param child the child TaconiteModel for which some appropriate new child view should be created
         * @return a new ITaconiteView instance.
         */
        function createChildView(child:TaconiteModel):ITaconiteView;
		
		function get okToSelect():Boolean;
    }
}
