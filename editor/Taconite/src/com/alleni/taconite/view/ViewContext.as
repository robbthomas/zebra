package com.alleni.taconite.view
{
    import com.alleni.taconite.controller.ITaconiteController;
    import com.alleni.taconite.document.TaconiteDocument;
    
    import flash.display.Sprite;
    
    /**
     * A ViewContext houses data and references to objects that must be accessible
     * to all views relating to some TaconiteDocument. 
     */
    public class ViewContext extends Sprite
    {
        public var info:ViewInfo;
        public var controller:ITaconiteController;

        public static const SELECT_TOOL:String = "select";
        public var pointerTool:String = SELECT_TOOL; 
        
        public function ViewContext(info:ViewInfo,
                                    controller:ITaconiteController)
        {
            this.info = info;
            this.controller = controller;
        }
        
        public function get document():TaconiteDocument
        {
            return (controller != null) ? controller.document : null;
        }
    }
}
