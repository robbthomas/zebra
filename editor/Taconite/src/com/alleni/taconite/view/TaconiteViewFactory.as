/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: DragMediator.as 1837 2009-08-28 22:01:28Z pkrekelberg $  */

package com.alleni.taconite.view
{
	import com.alleni.taconite.definition.ObjectDefinition;
	import com.alleni.taconite.model.TaconiteModel;
	
	public class TaconiteViewFactory
	{
        /**
         * create a new TaconiteView for the model within the context and description given 
         * @param context
         * @param model
         * @param childDefintion
         * @return a new TaconiteView for the model
         * 
         */
        public static function createForModel(context:ViewContext, model:TaconiteModel, role:uint, childDefinition:Vector.<ObjectDefinition>):ITaconiteView
        {
        	var shortClassName:String = model.value.shortClassName;
			
			var descriptionPointer:ObjectDefinition = childDefinition.filter(byShortClassName(shortClassName))[0];
			if (descriptionPointer.view != null) {
				var view:ITaconiteView = new descriptionPointer.view(context, model, role);
				/*if (descriptionPointer.icon != null) { icons are no longer handled here.
				if (descriptionPointer.icon as Class)
				view["icon"] = Bitmap(new descriptionPointer.icon());
				else if (descriptionPointer.icon as DisplayObject)
				view["icon"] = descriptionPointer.icon;
				}*/
				view.initialize();
				return view;
			} else {
				throw new Error("No view found for model type: " + shortClassName);
			}
			return null;
       	}

		private static function byShortClassName(shortClassName:String):Function
		{
			return function(item:ObjectDefinition, index:int, vector:Vector.<ObjectDefinition>):Boolean
			{
				return (item.name == shortClassName);
			}
		}
	}
}