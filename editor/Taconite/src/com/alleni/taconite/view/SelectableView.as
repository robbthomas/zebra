package com.alleni.taconite.view
{
    import com.alleni.taconite.controller.SelectionMediator;
    import com.alleni.taconite.model.TaconiteModel;
    
    import flash.display.DisplayObject;
    import flash.events.MouseEvent;

    /**
     * Abstract class providing a range of functionality for selectable views and objects,
     * supporting mouse-based selection and rollover feedback.
     * @author joeb
     * 
     */
    public class SelectableView extends TaconiteView
    {
        // flag tracking rollover state
        protected var _rolled:Boolean = false;
        
        // associated feedback view, if any
        protected var _feedback:ITaconiteView;

        /**
         * Flag controlling whether selection is shown using a default color transform. 
         */
        public var useSelectionTransform:Boolean = false;
        

        public function SelectableView(context:ViewContext, model:TaconiteModel)
        {
            super(context, model);
        }
        
        /**
         * Use a SelectionMediator to provide handling of selection gestures.
         */
        override public function initialize():void
        {
            super.initialize();
            setSelectionMediator();
			addEventListener(MouseEvent.ROLL_OVER, addRollHighlight);
			addEventListener(MouseEvent.ROLL_OUT, removeRollHighlight);
        }
		
		protected function setSelectionMediator():void
		{
			new SelectionMediator().handleViewEvents(this);
		}

        /**
         * Determine whether this object is selected or not by consulting the document's selection.
         */
        override public function get selected():Boolean
        {
            if (context.document == null || context.document.selection == null)
                return false;
            
            return context.document.selection.includes(model);
        }
		
        /**
         * Update the appearance of this object with respect to selection and rollover.  If useSelectionTransform
         * is set, then a default color transform will be applied to the hue of the object.
         */
        override protected function updateStatus():void
        {
            super.updateStatus();

            // Display default selection highlighting if requested
            //
            if (useSelectionTransform)
                transform.colorTransform = getColorTransform();

            // Display default rollover feedback if requested
			rolloverGlow = useRolloverGlow;
            
            // Manage creation/destruction of a specialized feedback view on a different layer.m
			if (selected && _feedback == null) {
				_feedback = createFeedbackView();
				if (_feedback != null) {
					trace("adding feecback handles to the feedbackHandlesView...");
					_feedback.initialize();
					//context.editor.feedbackHandlesView.addChild(_feedback as DisplayObject);
				}
			}
			if (_feedback)
				(_feedback as DisplayObject).visible = selected;
        }

        /**
         * Flag controlling use of rollover glow.
         */
        public function get useRolloverGlow():Boolean
        {
            return _rolled && !selected && model.enabled;
        }
		
		protected function set rolloverGlow(value:Boolean):void
		{
			// override this to put on the glow
		}

        
        protected function createFeedbackView():ITaconiteView
        {
            return null;
        }
        
        protected function addRollHighlight(e:MouseEvent):void
        {
            _rolled = true;
            updateStatus();
			if (e != null)
				e.stopPropagation();
        }
        
        protected function removeRollHighlight(e:MouseEvent):void
        {
            _rolled = false;
            if (stage == null) {
                // Disregard roll highlight removal for off-stage views
                return;
            }
            updateStatus();
            if (e != null)
				e.stopPropagation();
        }
		
		override public function warnViewToDelete():void
		{
			super.warnViewToDelete();
			removeEventListener(MouseEvent.ROLL_OVER, addRollHighlight);
			removeEventListener(MouseEvent.ROLL_OUT, removeRollHighlight);
		}
    }
}
