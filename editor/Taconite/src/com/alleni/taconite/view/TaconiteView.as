package com.alleni.taconite.view
{
import com.alleni.taconite.dev.Utilities;
import com.alleni.taconite.event.ModelCollectionEvent;
import com.alleni.taconite.event.ModelEvent;
import com.alleni.taconite.event.ModelStatusEvent;
import com.alleni.taconite.event.ModelUpdateEvent;
import com.alleni.taconite.model.TaconiteModel;
import com.alleni.taconite.model.TaconiteObject;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.geom.ColorTransform;
import flash.utils.Dictionary;

import mx.collections.IList;

/**
	 * TaconiteView is the superclass of all DisplayObject-based views in Taconite.  It provides
	 * basic hookups to the events that drive view refresh based on model changes,
	 * and a very basic implementation of selection feedback based on color transforms.
     *
     * Child views:
     * To enable automatic management of child views:
     * 1. The model object must have a "children" IList.  (for AbstractContainer this is equated to "objects", for Wiring its "wires").
     * 2. The view subclass must override childViewContainer.
     * For most cases the ObjectView.createChildView() will create the appropriate view.
     * For model arrays other than "children", override manageViewsForList(), which is done by PagerView.
     *
	 */
	public class TaconiteView extends Sprite implements ITaconiteView
	{
		protected var _context:ViewContext;
		private var _model:TaconiteModel;
        private var _childViewLists:Dictionary = new Dictionary();  // key=property name such as "pages", value=Vector.<ITaconiteView>



        public function TaconiteView(context:ViewContext, model:TaconiteModel)
        {
            _context = context;
            _model = model;
        }

        private function createViewsForList(prop:String):Vector.<ITaconiteView>
        {
            var list:IList = _model.value[prop] as IList;
            var array:Vector.<ITaconiteView> = new Vector.<ITaconiteView>();
            for each (var obj:TaconiteObject in list) {
                var view:ITaconiteView = createChildView(obj.model);
                if (view == null) {
                    throw new Error("TaconiteView");
                }
                array.push(view);
            }
            return array;
        }

        protected function manageViewsForList(property:String):Boolean
        {
            return false;  // true to enable management of views with caching
        }

        protected function viewCountForList(listName:String):int
        {
            var list:Vector.<ITaconiteView> = _childViewLists[listName];
            return list.length;
        }

        protected function getViewForListItem(listName:String, index:int):ITaconiteView
        {
            var list:Vector.<ITaconiteView> = _childViewLists[listName];
            return list[index];
        }

        public function get childViewContainer():DisplayObjectContainer
        {
            return null;  // override to have child views automatically added and removed
        }

		public function showSelf(onlyVisible:Boolean=false):void {
			if(onlyVisible) {
				if(_model.value.hasOwnProperty("visible")) {
					this.visible = _model.value["visible"];
				}
			} else {
				this.visible = true;
			}
		}
		
		public function showAllChildren(superClass:Class, onlyVisible:Boolean=false):void {
			showSelf(onlyVisible);
			for(var i:int=0; i<this.numChildren; i++) {
				var child:TaconiteView = this.getChildAt(i) as TaconiteView;
				if(child is superClass) {
					child.showAllChildren(superClass, onlyVisible);
				}
			}
		}
		
		private var _movingView:Dictionary = new Dictionary(); // TaconiteModel->DisplayObject // temporary storage for a view that is moving between parents.
		
		protected function storeView(model:TaconiteModel, view:ITaconiteView):void {
			
//			Utilities.assert(_movingView[model] == null);  // should not already be cached
			_movingView[model] = view;
		}
		
		protected function retrieveView(model:TaconiteModel):ITaconiteView {
			var result:ITaconiteView = _movingView[model];
			delete _movingView[model];
			return result;
		}

		public function get context():ViewContext
		{
			return _context;
		}
		
		public function get model():TaconiteModel
		{
			return _model;
		}
		
		/**
		 * Call this function in every constructor after the object is built out, to set up the view and
		 * add event listeners. 
		 */
		public function initialize():void
		{
            var prop:String = "children";
            if (prop in model.value && (manageViewsForList(prop) || childViewContainer != null)) {
                _childViewLists[prop] = createViewsForList(prop);
            }
            for each (prop in model.getAllValueListProperties()) {
                if (manageViewsForList(prop)) {
                    _childViewLists[prop] = createViewsForList(prop);
                }
            }

			initializeView();
			if (model != null)
			{
				model.addEventListener(ModelStatusEvent.STATUS_CHANGE, handleStatusChange, false, 4, true);
				model.addEventListener(ModelEvent.MODEL_CHANGE, handleModelChange, false, 4, true);
				model.addEventListener(ModelUpdateEvent.MODEL_UPDATE, handleModelUpdate, false, 4, true);
			}
		}
		
		private function destroy():void
		{
			model.removeEventListener(ModelStatusEvent.STATUS_CHANGE, handleStatusChange);
			model.removeEventListener(ModelEvent.MODEL_CHANGE, handleModelChange);
			model.removeEventListener(ModelUpdateEvent.MODEL_UPDATE, handleModelUpdate);
			
			// the cached view should have been removed as part TaconiteModel::changeParent or setChildIndex.
			// we need to ensure its no longer cached because a destroyed view won't behave well, if used later
			Utilities.assert(_movingView[model] == null);
		}
		
		protected function initializeView():void
		{
            createChildren();
			updateView();
			updateStatus();
		}

        protected function createChildren():void
        {
            if (childViewContainer) {
                var count:int = viewCountForList("children");
                for (var i:int = 0; i < count; i++) {
                    var childView:ITaconiteView = getViewForListItem("children", i);
                    childViewContainer.addChildAt(childView as DisplayObject, i);
                }
            }
        }

		
		// Abstract methods
		

		/**
		 * Called to update the view's contents, typically by consulting its layout and constructing its children. 
		 */
		protected function updateView():void
		{
			graphics.clear();
		}
		
		public function setFocus():void
		{
			context.stage.focus = this;
		} 
		
		/**
		 * Factory method to create the appropriate TaconiteView for a new child model.
		 */
		public function createChildView(child:TaconiteModel):ITaconiteView
		{
			return null;
		}
		
		/**
		 * Handle a specific property change by some incremental adjustment and return true,
		 * otherwise return false to reinitialize the view. 
		 */
		protected function updateModelProperty(property:Object, oldValue:Object, newValue:Object):Boolean
		{
			return false;
		}
		
		/**
		 * Handle an array change by some incremental adjustment and return true,
		 * otherwise return false to reinitialize the view. 
		 */
		protected function updateArrayProperty(property:String, modelEventKind:String, array:IList, index:int):Boolean
		{
			return false;  // did not handle it ... update is still needed
		}
		
		/**
		 * Update the status of this object with respect to selection status, etc. 
		 */
		protected function updateStatus():void
		{
		}
		
		/** Flag that determines whether an element in the view appears selected or not. */
		public function get selected():Boolean
		{
			return false;
		}
		
		/**
		 * A ColorTransform dependent on this object's status; used to affect how it looks
		 * depending on whether it's selected, a feedback object, or just plain normal.
		 */
		protected function getColorTransform():ColorTransform
		{
			if (selected)
			{
				return lightenTransform(context.info.selectionColors[0]);
			}
			else
			{
				return new ColorTransform();
			}
		}
		
		public static function lightenTransform(color:uint):ColorTransform
		{
			return new ColorTransform(1, 1, 1, 1,
				color >> 16, (color >> 8) & 0xFF, color & 0xFF);
		}
		
		public static function darkenTransform(color:uint):ColorTransform
		{
			return new ColorTransform((color >> 16) / 255.0, ((color >> 8) & 0xFF) / 255.0, (color & 0xFF) / 255.0);
		}
		
		private function handleStatusChange(e:ModelStatusEvent):void
		{
			if (stage != null)
			{
				updateStatus();
			}
		}
		
		/**
		 * Handle structural changes in the model by adding or removing views.  The z-order of views
		 * corresponds to the order in the model's children.
		 */
		private function handleModelChange(e:ModelEvent):void
		{
			// we're going to try this without checking to see if stage is null, since all TaconiteViews have a ViewContext
			// the ViewContext gives them a reference to the stage so generally, if it's a TaconiteView, it's got a stage.
			//if (e.parent != model || stage == null)
			if (e.parent != model)
				return;
			
            var ce:ModelCollectionEvent = e as ModelCollectionEvent;
			var childView:ITaconiteView;
//			trace("TaconiteView:ModelChange on " + this + " kind " + e.kind + " model " + e.child);
			switch (e.kind)
			{
				case ModelEvent.ADD_CHILD_MODEL:
                    if ("children" in _childViewLists) {
                        childView = addChildView("children", e.index, e.child.value as TaconiteObject, e.moving);
                        if (childViewContainer) {
                            childViewContainer.addChildAt(childView as DisplayObject, e.index);
                        }
                    }
					if (!handleChildListUpdate(e.kind, e.index, e.child.value))
						updateView();
					break;
				
				case ModelEvent.REMOVE_CHILD_MODEL:
                    if ("children" in _childViewLists) {
                        removeChildView("children", e.index, e.child.value as TaconiteObject, e.moving);
                        if (childViewContainer) {
                            childViewContainer.removeChildAt(e.index);
                        }
                    }
					if (!handleChildListUpdate(e.kind, e.index, e.child.value))
						updateView();
					break;

                case ModelEvent.CHANGE_ARRAY_ITEM:
                    if (ce.property in _childViewLists) {
                        throw new Error("TaconiteView");
                    }
					if (!updateArrayProperty(ce.property, ce.kind, ce.collection as IList, ce.index))
						updateView();
                    break;

				case ModelEvent.ADD_ARRAY_ITEM:
                    if (ce.property in _childViewLists) {
                        addChildView(ce.property, ce.index, ce.item as TaconiteObject, ce.moving);
                    }
					if (!updateArrayProperty(ce.property, ce.kind, ce.collection as IList, ce.index))
						updateView();
                    break;
				case ModelEvent.REMOVE_ARRAY_ITEM:
                    if (ce.property in _childViewLists) {
                        removeChildView(ce.property, ce.index, ce.item as TaconiteObject, ce.moving);
                    }
					if (!updateArrayProperty(ce.property, ce.kind, ce.collection as IList, ce.index))
						updateView();
					break;
			}
		}

        private function addChildView(listName:String, index:int, child:TaconiteObject, moving:Boolean):ITaconiteView
        {
//            trace("ADD " + child + " this="+this + " list="+listName + " moving="+moving);
            var childView:ITaconiteView = null;
            if (moving) {
                childView = topMostView.retrieveView(child.model);
            }
            if(childView != null) {
//                trace("  addChildView found cached view " + childView);
            } else {
                childView = createChildView(child.model);
//                trace("  addChildView creating view " + childView);
            }
            if (childView) {
                var list:Vector.<ITaconiteView> = _childViewLists[listName];
                list.splice(index, 0, childView);
            }
            return childView;
        }

        private function removeChildView(listName:String, index:int, child:TaconiteObject, moving:Boolean):ITaconiteView
        {
//            trace("REMOVE " + child + " this="+this);
            var list:Vector.<ITaconiteView> = _childViewLists[listName];
            var childView:ITaconiteView = list[index];
            list.splice(index, 1);

            if (moving) {
//                trace("  removeChildView caching view " + childView);
                topMostView.storeView(child.model, childView);
            } else {
//                trace("  removeChildView warning view "+childView);
                childView.warnViewToDelete();
            }
            return childView
        }

        protected function get topMostView():TaconiteView
        {
            var topMost:DisplayObject = this;
            var topMostView:TaconiteView = this;
            while(topMost.parent != null) {
                topMost = topMost.parent;
                if(topMost is TaconiteView) {
                    topMostView = topMost as TaconiteView;
                }
            }
            return topMostView
        }
		
		public function warnViewToDelete():void
		{
//            trace("warnViewToDelete:", this);
			destroy();
		}
		
		/**
		 * Handle property changes to the model by attempting some sort of incremental change
		 * via updateModelProperty(), then falling back to reinitializing the view completely.
		 */
		private function handleModelUpdate(e:ModelUpdateEvent):void
		{
			// we're going to try this without checking to see if stage is null, since all TaconiteViews have a ViewContext
			// the ViewContext gives them a reference to the stage so generally, if it's a TaconiteView, it's got a stage.
			// stage is usually null for a time when atomic models are updates.
			//if (e.source != model || stage == null)
			if (e.source != model)
				return;
			if (!updateModelProperty(e.property, e.oldValue, e.newValue))
				updateView();
		}
		
		protected function handleChildListUpdate(type:String, index:int, child:TaconiteObject):Boolean
		{
			return true;
		}
		
		public function get okToSelect():Boolean
		{
			return true;
		}
	}
}
