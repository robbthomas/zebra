/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.view
{
    import com.alleni.taconite.definition.HandleDescription;

    /**
     * Square graphic used for dragging some control point of an object.
     */
    public class PivotHandle extends AbstractHandle
    {
    	public var descriptor:HandleDescription;
    	
        /** Thickness of border drawn in rollover state. */
        public var highlightThickness:Number = 1;
        
        public function PivotHandle(context:ViewContext)
        {
            super(context);
        }
        
        override protected function updateGraphics():void
        {
            graphics.clear();
            graphics.lineStyle(rolled ? (highlightThickness / context.info.displayScale) : 1, 0xffffff);
            graphics.beginFill(rolled ? 0x006600 : 0x003300);
            graphics.drawEllipse(-handleSize/2, -handleSize/2, handleSize, handleSize);
            
            //dot.graphics.lineStyle(1, 0xffffff);
            //dot.graphics.beginFill(0x330000, 0.5);
            graphics.moveTo(-handleSize/2+1,-handleSize/2+1)
            graphics.lineTo(handleSize/2-1,handleSize/2-1);
            graphics.moveTo(handleSize/2-1,-handleSize/2+1)
            graphics.lineTo(-handleSize/2+1,handleSize/2-1);
            graphics.endFill();
        }
    }
}