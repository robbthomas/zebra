/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.view
{

    import com.alleni.taconite.definition.HandleDescription;
    import com.alleni.taconite.definition.HandleRoles;
    
    import flash.display.CapsStyle;
    import flash.display.LineScaleMode;
	

	/**
     * Square graphic used for dragging some control point of an object.
     */
    public class SelectionHandle extends AbstractHandle
    {
		public var descriptor:HandleDescription;
		
        /** Thickness of border drawn in rollover state. */
        public var highlightThickness:Number = 1;
		private var _role:uint;
        
        public function SelectionHandle(context:ViewContext, role:uint)
        {
            super(context);
			_role = role;
			updateGraphics();
        }
        
        override protected function updateGraphics():void
        {
			var fillColor:uint = _role == HandleRoles.ROTATE ? 0x75D7FC:0xF5F5F5;
			var rollColor:uint = 0x969696;
			
			graphics.clear();
			graphics.lineStyle(handleSize+1, 0x323232, 0.4, false, LineScaleMode.NONE, CapsStyle.ROUND);
			graphics.moveTo(0,0);
			graphics.lineTo(0.25,0.25);
			
			graphics.lineStyle(handleSize, rolled ? rollColor : fillColor, 0.4, false, LineScaleMode.NONE, CapsStyle.ROUND);
			graphics.moveTo(0,0);
			graphics.lineTo(0.25,0.25);
        }
    }
}