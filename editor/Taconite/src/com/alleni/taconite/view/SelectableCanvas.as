package com.alleni.taconite.view
{
    import com.alleni.taconite.controller.SelectionMediator;
    import com.alleni.taconite.model.TaconiteModel;
    
    import flash.display.DisplayObject;
    
    /**
     * Abstract class providing a range of functionality for selectable views and objects,
     * supporting mouse-based selection and rollover feedback.  The Flex Canvas-based version
     * of SelectableView.
     */
    public class SelectableCanvas extends TaconiteCanvas
    {
        // flag tracking rollover state
        private var _rolled:Boolean = false;
        
        // associated feedback view, if any
        private var _feedback:ITaconiteView;

        public function SelectableCanvas(context:ViewContext, model:TaconiteModel)
        {
            super(context, model);
            new SelectionMediator().handleViewEvents(this);
        }
        
        /**
         * Determine whether this object is selected or not by consulting the document's selection.
         */
        public function get selected():Boolean
        {
            if (context.document == null || context.document.selection == null)
            {
                return false;
            }
            
            return context.document.selection.includes(model);
        }
        
        override protected function updateStatus():void
        {
            super.updateStatus();
            
            // Manage creation/destruction of a specialized feedback view on a different layer.
            //
            if (selected && _feedback == null)
            {
                _feedback = createFeedbackView();
                if (_feedback != null)
                {
                	_feedback.initialize();
                    //context.editor.feedbackHandlesView.addChild(_feedback as DisplayObject);
                }
            }
            else if (!selected && _feedback != null)
            {
                //context.editor.feedbackHandlesView.removeChild(_feedback as DisplayObject);
                _feedback = null;
            }
        }
        
        protected function createFeedbackView():ITaconiteView
        {
            return null;
        }
    }
}
