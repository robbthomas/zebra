package com.alleni.taconite.view.collision
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;

	public class CollisionDetector
	{
		public static var instance:CollisionDetector = new CollisionDetector();
		
		private var _bitmap:Bitmap;  // for debug
		
		
		/**
		 * Detect overlap of two graphics, potentially in different contexts.
		 * Counts only the non-transparent parts of the graphic, including bitmaps.
		 * @param one
		 * @param two
		 * @return true if collision is detected.
		 * 
		 */
		public function detectOverlap(one:Sprite, two:Sprite):Boolean
		{
			/*
			If the bounds do not intersect, false is returned and this is the easy case.
			When bounds intersect, a temp bitmap is created as large as intersect and both objects are
			drawn into it.
			The objects are drawn with blending=multiply and one object has a color-transform to
			make all non-transparent pixels red.  The other object has a transform to blue.
			Consequently the overlapping opaque pixels will become black.
			*/
//			trace("detectOverlap:",one,two);
			var theStage:Stage = one.stage;
			if (theStage == null)
				return false;
			var oneBounds:Rectangle = one.getBounds(theStage);
			var twoBounds:Rectangle = two.getBounds(theStage);
			var sect:Rectangle = oneBounds.intersection(twoBounds);
			if (sect.width == 0 || sect.height == 0)
				return false;  // no intersection
			
			var data:BitmapData = new BitmapData(sect.width+1, sect.height+1,false,0xdddddd);  // transparent=false
			
			/*
			this is a quick fix for release -- but can be improved with safe view handling
			
			At this point we know that we overlap, but are we over transparent pixels?
			If drawViewIntoBitmap throws a security error, we're colliding over a map
			or image that we can't get bitmap data from.  In this case simply return true.  The
			map or image is treated as an opaque block.  This is better than crashing.
			
			*/
			
			try{
				drawViewIntoBitmap(one, sect, createBlueTransform(), data);
				drawViewIntoBitmap(two, sect, createRedTransform(), data);
			}
			catch(error:SecurityError){
				return true;
			}
			
//			// debug:  enable this code to reveal the temp bitmap
//			if (_bitmap)
//				theStage.removeChild(_bitmap);
//			_bitmap = new Bitmap(data);
//			theStage.addChild(_bitmap);
//			_bitmap.x = 0;
//			_bitmap.y = 0;

			var result:Boolean = hasBlackPixel(data);
//			trace("  detected="+result);
			return result;
		}

		private function drawViewIntoBitmap(view:Sprite, sect:Rectangle, colorTrans:ColorTransform, data:BitmapData):void
		{
			var anchor:Point = view.localToGlobal(new Point(0,0));
			var bounds:Rectangle = view.getBounds(view.stage);
			var clipRect:Rectangle = new Rectangle(-999,-999,9999,9999);

			var matrix:Matrix = netTransform(view);
			matrix.tx = anchor.x - sect.x;
			matrix.ty = anchor.y - sect.y;
		
			data.draw(view, matrix, colorTrans, BlendMode.MULTIPLY);
		}

		private function netTransform(view:DisplayObjectContainer):Matrix
		{
			var matrix:Matrix = new Matrix();
			while (view) {
				if (view.scaleX > 0)  // checking for NaN
					matrix.scale(view.scaleX, view.scaleY);
				if (view.rotation > 0 || view.rotation < 0)
					matrix.rotate(view.rotation*Math.PI/180);
				view = view.parent;
			}
			return matrix;
		}

		private function hasBlackPixel(data:BitmapData):Boolean
		{
			var pixels:ByteArray = data.getPixels(new Rectangle(0, 0, data.width, data.height));
			pixels.position = 0;
			var len:int = pixels.length / 4;  // 4 bytes per word
			for (var k:int=0; k < len; k++) {
				var word:uint = pixels.readUnsignedInt();
				var alpha:uint = word >> 24 & 0xFF;
				if (alpha > 0) {
					var color:uint = word & 0xffffff;
					if (color == 0)
						return true;  // black pixel means collision
				}
			}
			return false;
		}
		
		private function createBlueTransform():ColorTransform
		{
			var transform:ColorTransform = new ColorTransform();
			// all pixels blue
			transform.alphaOffset = +256;
			transform.redOffset = -256;
			transform.blueOffset = +255;
			transform.greenOffset = -256;
			return transform;
		}
		
		private function createRedTransform():ColorTransform
		{
			var transform:ColorTransform = new ColorTransform();
			// all pixels red
			transform.alphaOffset = +256;
			transform.redOffset = +255; 
			transform.blueOffset = -256;
			transform.greenOffset = -256;
			return transform;
		}
		
	}
}
