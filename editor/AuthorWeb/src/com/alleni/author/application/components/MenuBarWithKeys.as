package com.alleni.author.application.components
{
	
	
	import com.alleni.author.application.components.MenuItemRendererWithKeys;
	
	import flash.events.KeyboardEvent;
	
	import mx.collections.ICollectionView;
	import mx.controls.Label;
	import mx.controls.Menu;
	import mx.controls.MenuBar;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.core.ClassFactory;
	import mx.core.UIFTETextField;
	
	/**
	 *     This class doesn't currently support the iconField property.
	 *     All icons are rendered as labels, as specified by the iconFunction.
	 */
	[Exclude(name="iconField", kind="property")]
	
	/**
	 *     The AcceleratorMenuBar is a MenuBar which displays windows/mac style
	 *     accelerators next to the menu labels.
	 * 
	 *     The Menu's 'icon' is used to display the accelerator.
	 * 
	 *     @see com.rphelan.controls.menuClasses.AcceleratorMenuItemRenderer
	 *     @see mx.controls.MenuBar
	 */
	public class MenuBarWithKeys extends MenuBar
	{
		/**
		 *     Constructor.
		 */
		public function MenuBarWithKeys()
		{
			super();
		}
		
		/**
		 *     By overriding getMenuAt, we can ensure that each new Menu
		 *     uses MenuItemRendererWithKeys for its itemRenderer
		 *     and Label for all of its icons.
		 */
		public override function getMenuAt( index:int ):Menu
		{
			var menu:Menu = super.getMenuAt(index);
			menu.itemRenderer = new ClassFactory(MenuItemRendererWithKeys);
			menu.iconFunction = getIcon;
			return menu;
		}
		
		/**
		 *     @private
		 *     this is an iconFunction for a Menu
		 * 
		 *     @return a reference to the Label class
		 */
		private function getIcon( item:Object ):Class
		{
			return UIFTETextField;
		}
	}
}