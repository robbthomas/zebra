package com.alleni.author.application.components
{
	
	import flash.text.TextFormat;
	
	import mx.controls.Label;
	import mx.controls.listClasses.ListData;
	import mx.controls.menuClasses.MenuItemRenderer;
	import mx.controls.menuClasses.MenuListData;
	import mx.core.UIFTETextField;
	

	public class MenuItemRendererWithKeys extends MenuItemRenderer
	{
		
		private var _shortCut:String;
		
		/**
		 *     Constructor.
		 */
		public function MenuItemRendererWithKeys()
		{
			super();
		}
		
		/**
		 *  @private
		 */
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			if( icon && icon is UIFTETextField )
			{ 
				var keyEquivalent:String = ""; 
				if( data )
				{
					if( data.@altKey.toString() != '' && data.@altKey.toString().toLowerCase() == 'true' )
						keyEquivalent += "ALT + ";
					
					if( data.@ctrlKey.toString() != '' && data.@ctrlKey.toString().toLowerCase() == 'true' )
						keyEquivalent += "CTRL + ";
					
					if( data.@shiftKey.toString() != '' && data.@shiftKey.toString().toLowerCase() == 'true' )
						keyEquivalent += "SHIFT + ";
					
					if( data.@keyEquivalent.toString() != '' )
						keyEquivalent += data.@keyEquivalent.toString().toUpperCase();					
					
				}
				
				UIFTETextField(icon).text = keyEquivalent;
			}
		}
		
		/**
		 *  @private
		 */
		override protected function updateDisplayList( unscaledWidth:Number, unscaledHeight:Number ):void
		{
			super.updateDisplayList( unscaledWidth, unscaledHeight );
			
			var iconWidth:Number = MenuListData(listData).maxMeasuredIconWidth;
			var typeIconWidth:Number = MenuListData(listData).maxMeasuredTypeIconWidth;
			var branchIconWidth:Number = MenuListData(listData).maxMeasuredBranchIconWidth;
			var useTwoColumns:Boolean = MenuListData(listData).useTwoColumns;
			
			var leftMargin:Number = Math.max(getStyle("leftIconGap"), typeIconWidth);
			var rightMargin:Number = Math.max(getStyle("rightIconGap"), iconWidth + branchIconWidth);
			
			var right:Number = unscaledWidth - (rightMargin - (iconWidth + branchIconWidth))/2;
			
			if (typeIcon)
			{
				typeIcon.x = (leftMargin - typeIcon.measuredWidth)/2;
				typeIcon.setActualSize(typeIcon.measuredWidth, typeIcon.measuredHeight);
			}
			if (icon)
			{
				icon.x = right - branchIconWidth - iconWidth;
				icon.setActualSize(icon.measuredWidth, icon.measuredHeight);
			}            
			if (branchIcon)
			{
				branchIcon.x = right - branchIconWidth;
				branchIcon.setActualSize(branchIcon.measuredWidth, branchIcon.measuredHeight);
			}            
			
			label.x = leftMargin;        
			label.setActualSize(unscaledWidth - leftMargin - rightMargin,
			label.getExplicitOrMeasuredHeight());
				
			
			// modify the text formatting to use windows style underlines on ctrl and letter
			// underline any character preceded by a '&'
			if( super.data) 
			{
				if( ListData(super.listData) )
				{
					var str_label:String = ListData(super.listData).label;
					var underlinePosition:int = str_label.indexOf("&");
					if( underlinePosition >= 0 )
					{
						label.text = label.text.replace("&", "");
						_shortCut = label.text.charAt(underlinePosition);
						
						var format:TextFormat = new TextFormat();
						format.underline = true;
						label.setTextFormat(format, underlinePosition, underlinePosition+1 );
					}
				}
			}           
		}
		
		public function get shortCut():String
		{
			return _shortCut ? _shortCut : '';
		}
	}
	
	
	
	
	
}