package com.alleni.author.application.components
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.menuClasses.IMenuBarItemRenderer;
	
	public class MenuBarItem extends mx.controls.menuClasses.MenuBarItem
	{
		public function MenuBarItem()
		{
			super();
			this.addEventListener(MouseEvent.MOUSE_OUT, handleMouseOut);
		}
		
		private function handleMouseOut(event:Event):void
		{
			// prevent insane crash due to standard MX MenuBar component assuming event.target is an IMenuBarItemRenderer
			if (!(event.target as IMenuBarItemRenderer)) {
				event.stopImmediatePropagation();
				event.stopPropagation();
			}
		}
	}
}