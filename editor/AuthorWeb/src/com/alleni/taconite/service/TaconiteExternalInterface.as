package com.alleni.taconite.service
{
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.service.LogService;
	
	import flash.external.ExternalInterface;
	import flash.system.Security;


	public class TaconiteExternalInterface
	{
		private static var _instance:TaconiteExternalInterface;
		
		public function TaconiteExternalInterface()
		{
			if (_instance != null) throw new Error("Singleton, do not instantiate");
			Security.allowDomain("*");
			ExternalInterface.marshallExceptions = true;
		}
		
		static public function get instance():TaconiteExternalInterface
		{
			if (_instance == null)
				_instance = new TaconiteExternalInterface();
			return _instance;
		}
		
		public static function call(functionName:String, ...parameters):*
		{
			return callWithArrayArgs(functionName, parameters);
		}
		
		public static function callWithArrayArgs(functionName:String, parameters:Array):*
		{
			var response:* = null;
			if (jsFunctionExists(functionName)) {
				try {
					response = ExternalInterface.call(functionName, parameters);
                } catch (error:SecurityError) {
					LogService.error("ExternalInterface security error calling " + functionName + ", error:" + error.message);
                } catch (error:Error) {
					LogService.error("ExternalInterface general error calling " + functionName + ", error:" + error.message);
                }		
			}
			return response;
		}
		
		public static function addCallback(functionName:String, closure:Function):void
		{
			if (ExternalInterface.available) {
				try {
					ExternalInterface.addCallback(functionName, closure);
                } catch (error:SecurityError) {
					LogService.error("Security error adding ExternalInterface callback " + functionName + ", error:" + error.message);
                } catch (error:Error) {
					LogService.error("Couldn't add ExternalInterface callback " + functionName + ", error:" + error.message);
                }			
			}
		}

		private static function jsFunctionExists(functionName:String):Boolean {
			if (ExternalInterface.available) {
				try {
					return ExternalInterface.call("function() { return (typeof(eval("+functionName+")) === 'function'); }");
                } catch (error:SecurityError) {
					LogService.info("ExternalInterface security error verifying the existence of '" + functionName + "', error:" + error.message);
					return false;
                } catch (error:Error) {
					LogService.info("ExternalInterface general error verifying the existence of '" + functionName + "', error:" + error.message);
					return false;
                }
			}
			LogService.error("Tried to call JavaScript function that is not available: " + functionName)
			return false;
		}
	}
}