/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.factory
{
import com.alleni.taconite.service.TaconiteExternalInterface;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.core.IVisualElement;
    import mx.events.ResizeEvent;
	import mx.managers.ICursorManager;
	import mx.managers.PopUpManager;
	
	import spark.components.Application;
	import spark.components.TitleWindow;
	import spark.primitives.BitmapImage;

	public class WindowImplementation extends EventDispatcher implements ITaconiteWindow
	{
		private static const STANDARD_MENUBAR_COMPONENT_HEIGHT:Number = 22.0;
		private static const TITLEWINDOW_HEADER_HEIGHT:Number = 30.0;  // for popup
		
		private static var _application:Application;
		
		private var _titleWindow:TitleWindow; 
		private var _title:String;
		private var _isPopup:Boolean = false;

		[Embed(source="/embeddedAssets/appui/window_resize_gripper.png",compression="true",quality="80")]
		private static const WindowResizeGripper:Class;
		private var _windowResizeGripper:BitmapImage;
		
		public function WindowImplementation()
		{
		}
		
		public function set application(value:Application):void
		{
			_application = value;
			_application.addEventListener(FocusEvent.FOCUS_IN, handleWindowActivate);
			_application.addEventListener(FocusEvent.MOUSE_FOCUS_CHANGE, handleWindowDeactivate);
			
			if (TaconiteFactory.getEnvironmentImplementation().invokedAsEditor) {
				setupResizing();
			}
		}
		
		public function open(bounds:Rectangle, title:String="Window", resizable:Boolean=true, utility:Boolean=false, hideInitially:Boolean=false):void
		{
			if (_isPopup) {
				_titleWindow = new TitleWindow();
				_titleWindow.visible = !hideInitially;
				_titleWindow.styleName = "blackBackground";
				_titleWindow.addEventListener(Event.ADDED_TO_STAGE, handleWindowComplete);
				_titleWindow.addEventListener(Event.CLOSE, handleWindowClose);
				_titleWindow.addEventListener(FocusEvent.FOCUS_IN, handleWindowActivate);
				_titleWindow.addEventListener(FocusEvent.MOUSE_FOCUS_CHANGE, handleWindowDeactivate);
                _titleWindow.addEventListener("xChanged", handlePopupXYChanged);
                _titleWindow.addEventListener("yChanged", handlePopupXYChanged);
				_titleWindow.title = title;
				_titleWindow.x = bounds.x;
				_titleWindow.y = bounds.y;
				_titleWindow.width = bounds.width;
				_titleWindow.height = bounds.height + TITLEWINDOW_HEADER_HEIGHT;
				PopUpManager.addPopUp(_titleWindow, _application, false);

				if (resizable) {
					setupResizing();
				}
			}
		}
		
		public function close():void
		{
			if (_isPopup) {
				_titleWindow.removeEventListener(Event.ADDED_TO_STAGE, handleWindowComplete);
				_titleWindow.removeEventListener(Event.CLOSE, handleWindowClose);
				_titleWindow.removeEventListener(FocusEvent.FOCUS_IN, handleWindowActivate);
				_titleWindow.removeEventListener(FocusEvent.MOUSE_FOCUS_CHANGE, handleWindowDeactivate);
                _titleWindow.removeEventListener("xChanged", handlePopupXYChanged);
                _titleWindow.removeEventListener("yChanged", handlePopupXYChanged);
				PopUpManager.removePopUp(_titleWindow);
			}
		}
		
		public function requestNewWindowAndInvokeId(id:String, newWindowSize:Object = null):void
		{
			if(newWindowSize == null){
                TaconiteExternalInterface.call("requestPopup", id);
            } else {
                TaconiteExternalInterface.call("requestPopup", {id:id, width:newWindowSize.width,  height:newWindowSize.height});
            }
		}
		
		public function sendToBack():void
		{
			// web need not support this
		}
		
		public function exit():void
		{
			this.close();
			TaconiteExternalInterface.call("handleApplicationQuit");
		}

		public function set exitWindow(value:Boolean):void
		{
		}
				
		public function addElement(element:IVisualElement):IVisualElement
		{
			if (_isPopup) {
				_titleWindow.addElement(element);
				if (_windowResizeGripper && !_gripperAddedToPopup) {
					addGripperToPopup();
				}
			} else {
				_application.addElement(element);
				if (!_windowResizeGripper) return element;
				_application.addElement(_windowResizeGripper);
			}
			return element;
		}
		
		
		private var _gripperAddedToPopup:Boolean;
		private var _clickX:Number;
		private var _clickY:Number;
		private var _clickWidth:Number;
		private var _clickHeight:Number;

		private function setupResizing():void
		{
			var windowResizeGripperSource:DisplayObject = new WindowResizeGripper();
			_windowResizeGripper = new BitmapImage();
			_windowResizeGripper.source = windowResizeGripperSource;
			handleRepositionGripperUponResize(windowResizeGripperSource)();
			if (_isPopup) {
				_titleWindow.addEventListener(ResizeEvent.RESIZE, handleRepositionGripperUponResize(windowResizeGripperSource));
			} else {
				_application.addEventListener(ResizeEvent.RESIZE, handleRepositionGripperUponResize(windowResizeGripperSource));
			}
		}
		
		private function addGripperToPopup():void
		{
			_titleWindow.addElement(_windowResizeGripper);
			_titleWindow.addEventListener(MouseEvent.MOUSE_DOWN, resizeMouseDown, true, 99999);
			_gripperAddedToPopup = true;
		}
		
		private function handleRepositionGripperUponResize(windowResizeGripperSource:DisplayObject):Function
		{	
			return function(event:Event=null):void
			{
				if (_isPopup) {
					_windowResizeGripper.x = _titleWindow.width - windowResizeGripperSource.width -5;
					_windowResizeGripper.y = _titleWindow.height - windowResizeGripperSource.height - TITLEWINDOW_HEADER_HEIGHT - 5;
				} else {
					_windowResizeGripper.x = _application.width - windowResizeGripperSource.width;
					_windowResizeGripper.y = _application.height - windowResizeGripperSource.height;
				}
			}
		}
		
		private function resizeMouseDown(event:MouseEvent):void
		{
			const GRIP_WIDTH:Number = 20;  // size of gripper hit-test area;  the actual bitmap is larger
			const GRIP_HEIGHT:Number = 20;

			if (event.localX > _titleWindow.width - GRIP_WIDTH && event.localY > _titleWindow.height - GRIP_HEIGHT - TITLEWINDOW_HEADER_HEIGHT) {
				_clickX = event.stageX;
				_clickY = event.stageY;
				_clickWidth = _titleWindow.width;
				_clickHeight = _titleWindow.height;
				_titleWindow.stage.addEventListener(MouseEvent.MOUSE_MOVE, resizeMouseMove, true,99999);
				_titleWindow.stage.addEventListener(MouseEvent.MOUSE_UP, resizeMouseUp, true,99999);
			}
		}
		
		private function resizeMouseMove(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
			var oldWidth:Number = _titleWindow.width;
			var oldHeight:Number = _titleWindow.height;
			_titleWindow.width = _clickWidth + event.stageX - _clickX;
			_titleWindow.height = _clickHeight + event.stageY - _clickY;
			dispatchEvent(new ResizeEvent(ResizeEvent.RESIZE, false,false, oldWidth, oldHeight));
		}
		
		private function resizeMouseUp(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
			_titleWindow.stage.removeEventListener(MouseEvent.MOUSE_MOVE, resizeMouseMove, true);
			_titleWindow.stage.removeEventListener(MouseEvent.MOUSE_UP, resizeMouseUp, true);
		}
		
		public function set menu(value:*):void
		{
			_application.addElementAt(value, 0);
			for (var i:int=1 ; i < _application.numElements ; i++) // place all prior elements below the menubar, if there are any
				if (_application.getElementAt(i).y < STANDARD_MENUBAR_COMPONENT_HEIGHT)
					_application.getElementAt(i).y = STANDARD_MENUBAR_COMPONENT_HEIGHT;
		}
		
		public function get title():String
		{
			if (_isPopup)
				return _titleWindow.title;
			else
				return _title;
		}
		
		public function set title(value:String):void
		{
			if (_isPopup)
				_titleWindow.title = value;
			else
				_title = value;
			TaconiteExternalInterface.call("changeTitle", value);
		}
		
		public function get displayObject():DisplayObject
		{
			if (_isPopup)
				return _titleWindow;
			else
				return _application;
		}
		
		public function get cursorManager():ICursorManager
		{
			return null;
		}
		
		public function get WINDOW_ACTIVATE():String
		{
			return "windowActivate";
		}
		
		public function get WINDOW_COMPLETE():String
		{
			return "windowComplete";
		}
		
		public function get WINDOW_DEACTIVATE():String
		{
			return "windowDeactivate";
		}
		
		private function handleWindowActivate(e:Event):void
		{
			dispatchEvent(new Event(WINDOW_ACTIVATE));
		}
		
		private function handleWindowDeactivate(e:Event):void
		{
			dispatchEvent(new Event(WINDOW_DEACTIVATE));
		}
		
		private function handleWindowComplete(e:Event):void
		{
			dispatchEvent(new Event(WINDOW_COMPLETE));
		}
		
		private function handleWindowClose(e:Event):void
		{
			dispatchEvent(e);
		}

		public function get visible():Boolean
		{		
			if (_isPopup)
				return _titleWindow.visible;
			else
				return _application.visible;
		}
		
		public function set visible(value:Boolean):void
		{
			if (_isPopup)
				_titleWindow.visible = value;
			else
				_application.visible = value;
		}

        public function get globalPosition():Point
        {
            // global position of topLeft of content area
            var global:Point = _titleWindow.localToGlobal(new Point(0,0));
            global.y += TITLEWINDOW_HEADER_HEIGHT;
            return global;
        }

        public function set globalPosition(value:Point):void
        {
            if (_isPopup) {
                _titleWindow.x = value.x;
                _titleWindow.y = value.y - TITLEWINDOW_HEADER_HEIGHT;
            }
        }

		public function set bounds(value:Rectangle):void
		{
			if (_isPopup) {
				_titleWindow.x = value.x;
				_titleWindow.y = value.y;
				_titleWindow.width = value.width;
				_titleWindow.height = value.height + TITLEWINDOW_HEADER_HEIGHT;
			} else {
				_application.x = value.x;
				_application.y = value.y;
				_application.width = value.width;
				_application.height = value.height;
			}
		}
		
		public function set preventClose(value:Boolean):void
		{
		}

		public function get originX():Number
		{
			if (_isPopup)
				return _titleWindow.x;
			else
				return _application.x;
		}
		
		public function get originY():Number
		{
			if (_isPopup)
				return _titleWindow.y;
			else
				return _application.y;
		}
		
		public function get width():Number
		{
			if (_isPopup)
				return _titleWindow.width;
			else
				return _application.width;
		}
		
		public function get height():Number
		{
			if (_isPopup)
				return _titleWindow.height - TITLEWINDOW_HEADER_HEIGHT;
			else
				return _application.height;
		}

        public function set width(value:Number):void
        {
            if (_isPopup)
                _titleWindow.width = value;
            else
                _application.width = value;
        }

        public function set height(value:Number):void
        {
            if (_isPopup)
                _titleWindow.height = value;
            else
                _application.height = value;
        }


		public function get screenBounds():Rectangle
		{
			return new Rectangle(0, 0, _application.stage.stageWidth, _application.stage.stageHeight);
		}

		public function set closeCallback(closeCallBackFunction:Function):void
		{
		}

		public function set isPopup(value:Boolean):void
		{
			_isPopup = value;
		}

        private function handlePopupXYChanged(e:Event):void
        {
        	var bounds:Rectangle = screenBounds;
        	if (_titleWindow != null && bounds != null) {
        		_titleWindow.x = Math.max(Math.min(_titleWindow.x, bounds.width-_titleWindow.width), 0);
        		_titleWindow.y = Math.max(Math.min(_titleWindow.y, bounds.height-TITLEWINDOW_HEADER_HEIGHT), 0);
        	}
            dispatchEvent(new Event("xyChanged"));
        }

	}
}