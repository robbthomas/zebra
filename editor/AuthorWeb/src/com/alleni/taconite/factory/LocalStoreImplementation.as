package com.alleni.taconite.factory
{
	import flash.utils.ByteArray;

	public class LocalStoreImplementation implements ILocalStore
	{
		public function getItem(name:String):ByteArray
		{
			// get encrypted item (say, from a browser cookie)
			return null;
		}
		
		public function setItem(name:String, data:ByteArray, stronglyBound:Boolean=false):void
		{
			// set encrypted item (say, from a browser cookie)
		}
	}
}