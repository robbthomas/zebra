package com.alleni.taconite.factory
{
	import com.alleni.author.controller.app.ProjectController;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.document.Document;
	import com.alleni.author.document.Document;
	import com.alleni.author.model.World;
	import com.alleni.author.model.objects.LMS;
	import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.persistence.json.JSON;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	import com.alleni.taconite.service.TaconiteExternalInterface;

	import flash.events.Event;
	import flash.globalization.LocaleID;
	import flash.net.FileReference;
	import flash.system.Capabilities;
	import flash.system.LoaderContext;
import flash.utils.ByteArray;

import mx.utils.Base64Decoder;
import mx.utils.Base64Encoder;

public class EnvironmentImplementation implements IEnvironment
	{
		//private static var MEDIA_STREAM_URI:String = "media.zebrabeta.com/cfx/st";
		//private static var MEDIA_STREAM_URI:String = "s35ogpzlqny9gi.cloudfront.net/cfx/st";
		private static var MEDIA_STREAM_URI:String = "ec2-50-17-141-139.compute-1.amazonaws.com";

		private static const DEV_REG_EXP:RegExp = /^localhost/i;
		private static const GUEST_USERNAME:String = "guest";
		private static const GUEST_PASSWORD:String = "none";
		
		private static var _flashVars:Object;
		
		private static var _isAdmin:Boolean;
		private static var _accountId:String;
		private static var _accountName:String;
		private static var _companyName:String;
		private static var _userId:String;
		private static var _username:String;
		private static var _userFullName:String;
		private static var _password:String;
		private static var _email:String = "";
        private static var _customerType:String = "";
		private static var _serverName:String;
		private static var _serverPort:int = 80;
		private static var _apiURI:String;
        private static var _projectId:String;
        private static var _fromLMS:Boolean = false;
        private static var _lmsValues:Object = {};
        private static var _projectName:String; // for showHistory
        private static var _showHistory:Boolean;
        private static var _hasDomainCapabilities:Boolean;
		
		private static var _contextPath:String = "/zephyr";
		
		private static var _invokedAsEditor:Boolean = true;
        private static var _gotPersonalData:Boolean = false;

        private static var _lmsValuesCallback:Function = null;


        private static var _useCustomProjectName:Boolean = false;
        private static var _newProjectNameFunc:Function = null;

        function EnvironmentImplementation()
        {
            if (_flashVars == null && !_gotPersonalData) {
                var data:Object = EnvironmentPersonal.data;
                for (var key:String in data) {
                    this[key] = data[key];
                }
                _gotPersonalData = true;
            }
        }

		public function get targetUserName():String
		{
            return null;
        }

        public function get targetProjectName():String
		{
            return null;
        }

        public function setupContainerDialog():void
		{
			TaconiteExternalInterface.addCallback("handleInvoke", handleInvoke);
			TaconiteExternalInterface.addCallback("lmsValuesReady", lmsValuesReady);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_UNSAVED_CHANGES, handleUnsavedChanges);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_SAVED, handleProjectSaved);
			ApplicationController.addEventListener(NotificationNamesApplication.LMS_GET_VALUES, handleLmsGetValues);
			ApplicationController.addEventListener(NotificationNamesApplication.LMS_SET_VALUES, handleLmsSetValues);
            ApplicationController.addEventListener(NotificationNamesApplication.PREP_TO_SHARE_PROJECTS, handleShareProject);
            ApplicationController.addEventListener(NotificationNamesApplication.REFRESH_MAIN_SITE, handleRefreshMainSite);
		}

        private function handleRefreshMainSite(event:ApplicationEvent):void{
            TaconiteExternalInterface.call("refreshMainSite", event.data);
        }

		private function handleUnsavedChanges(event:ApplicationEvent):void
		{
			TaconiteExternalInterface.call("handleUnsavedChanges", event.data);
		}

		private function handleProjectSaved(event:ApplicationEvent):void
		{
			TaconiteExternalInterface.call("handleProjectSaved", event.data);
		}

        private function handleShareProject(event:ApplicationEvent):void{
            TaconiteExternalInterface.call("openTransfers", event.data);
        }

        public function handleError(message:String):void {
            TaconiteExternalInterface.call("handleError", message);
        }

        public function addExternalCallback(name:String, callback:Function):void
        {
			TaconiteExternalInterface.addCallback(name, callback);
        }

        public function callExternal(functionName:String, ...parameters):*
        {
			return TaconiteExternalInterface.callWithArrayArgs(functionName, parameters);
        }
		
		private function handleInvoke(id:String):void
		{
			LogService.debug("asked to invoke: " + id);
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.INVOKE_PROJECT, id));
		}

        private function handleLmsGetValues(event:ApplicationEvent):void {
            _lmsValuesCallback = null;
            if(event.data && "callback" in event.data) {
                _lmsValuesCallback = event.data.callback as Function;
            }
            TaconiteExternalInterface.call("getLmsValues");
        }

        private function lmsValuesReady(values:Object):void {
            if(_lmsValuesCallback) {
                _lmsValuesCallback(values);
            } else {
                World(Application.instance.document.root.value).lms.handleLmsValuesReady(values);
            }
            _lmsValuesCallback = null;
        }

        private function handleLmsSetValues(event:ApplicationEvent):void {
            TaconiteExternalInterface.call("setLmsValues", event.data.values);
        }
		
		public function set envVars(value:Object):void
		{
			_flashVars = value;

//            _flashVars["projectName"] = "SSBBTSBQTFVTICsrKyBPT0dBTEU=";
//            _flashVars["showHistory"] = "true";

            if (paramExists("username")) {
				if (_flashVars["username"] == "" || _flashVars["username"] == null)
					_username = GUEST_USERNAME;
				else
					var decoder:Base64Decoder = new Base64Decoder();
                    decoder.decode(String(_flashVars["username"]));
                    _username = decoder.drain().readUTFBytes(decoder.toByteArray().bytesAvailable);
			} else {
				if (paramExists("serverhostname")) // if we are live, default to guest credentials for undefined username
					_username = GUEST_USERNAME;
			}
			
			if (paramExists("password")) {
				if (_flashVars["password"] == "" || _flashVars["password"] == null)
					_password = GUEST_PASSWORD;
				else
					var decoder:Base64Decoder = new Base64Decoder();
                    decoder.decode(String(_flashVars["password"]));
                    _password = decoder.drain().readUTFBytes(decoder.toByteArray().bytesAvailable);
			} else {
				if (paramExists("serverhostname")) // if we are live, default to guest credentials for undefined password
					_password = GUEST_PASSWORD;
			}
			
			if (paramExists("email"))		
				_email = String(_flashVars["email"]);
			
			if (paramExists("serverhostname"))
				_serverName = String(_flashVars["serverhostname"]);	
			
			if (paramExists("serverport"))
				_serverPort = int(_flashVars["serverport"]);
			
			if (paramExists("contextPath"))
				_contextPath = String(_flashVars["contextPath"]);
			// replace encoded slash from GET var with real slash
			_contextPath = _contextPath.replace(/%2F/g, "/");
			
			if (paramExists("isEditor"))		
				_invokedAsEditor = _flashVars["isEditor"] == "true";

            if (paramExists("projectId"))
                _projectId = String(_flashVars["projectId"]);

            if (paramExists("fromLMS"))
                _fromLMS = String(_flashVars["fromLMS"]);

            if (paramExists("lmsValues"))
                _lmsValues = com.alleni.taconite.persistence.json.JSON.decode(_flashVars["lmsValues"], false);

            if (paramExists("projectName") ) {
                if(String(_flashVars["projectName"]) != null && String(_flashVars["projectName"]) != ""){
                    var decoder:Base64Decoder = new Base64Decoder();
                    decoder.decode(String(_flashVars["projectName"]));
                    _projectName = decoder.drain().readUTFBytes(decoder.toByteArray().bytesAvailable);
                }else{
                    _projectName = null;
                }
            }

            if (paramExists("showHistory")) {
                _showHistory = _flashVars["showHistory"] == "true";
            }

            _apiURI = null;


            // now that we have username, start the log (which shows the username)
			LogService.enabled = true;
		}

        public function get customerType():String{
            return _customerType;
        }
        public function set customerType(value:String):void{
            _customerType = value;
        }

		public function get containerDetails():String
		{
			return Capabilities.playerType + " (debugger:"+Capabilities.isDebugger+") version " + Capabilities.version + " on " + Capabilities.os;
		}
		
		public function get isAdmin():Boolean
		{
			return _isAdmin;
		}
		
		public function set isAdmin(value:Boolean):void
		{
			_isAdmin = value;
		}
		
		public function get guestUsername():String
		{
			if (!invokedAsEditor) return GUEST_USERNAME;
			else return null;
		}
		
		public function get guestPassword():String
		{
			if (!invokedAsEditor) return GUEST_PASSWORD;
			else return null;
		}
		
		public function get accountId():String
		{
			return _accountId;
		}
		
		public function set accountId(value:String):void
		{
			_accountId = value;
		}
		
		public function get accountName():String
		{
			return _accountName;
		}
		
		public function set accountName(value:String):void
		{
			_accountName = value;
		}
		
		public function get companyName():String
		{
			return _companyName;
		}
		
		public function set companyName(value:String):void
		{
			_companyName = value;
		}
		
		public function get userId():String
		{
			return _userId;
		}
		
		public function set userId(value:String):void
		{
			_userId = value;
		}
		
		public function get username():String
		{
			return _username;
		}
		
		public function set username(value:String):void
		{
			_username = value;
		}
		
		public function get userFullName():String
		{
			return _userFullName;
		}
		
		public function set userFullName(value:String):void
		{
			_userFullName = value;
		}
		
		public function get password():String
		{			
			return _password;
		}
		
		public function set password(value:String):void
		{
			_password = value;
		}
		
		public function get email():String
		{			
			return _email;
		}
		
		public function set email(value:String):void
		{
			_email = value;
		}

		public function get serverName():String
		{
			return _serverName;
		}
		
		public function set serverName(value:String):void
		{
			_serverName = value;
			_apiURI = null;  // clear the cache
			_serverPort = (value == "zebrazapps.com" || value == "qa.zebrazapps.com" || value == "pro.zebrazapps.com" || value == "staging.zebrazapps.com") ? 443 : 80;
			LogService.debug("set serverName="+value);
		}
		
		public function get apiURI():String
		{	
			if (!_apiURI)
				_apiURI = RestHttpOperation.getProtocolForPort(_serverPort) + "://"+ _serverName + _contextPath + "/";
			return _apiURI;
		}
		
		public function get mediaStreamURI():String
		{
			return MEDIA_STREAM_URI;
		}
		
        public function get invokedAsEditor():Boolean
        {
            return _invokedAsEditor;
        }

        public function set invokedAsEditor(value:Boolean):void
        {
            _invokedAsEditor = value;
        }

        public function get showHistory():Boolean
        {
            return _showHistory;
        }

        public function set showHistory(value:Boolean):void
        {
            _showHistory = value;
        }

        public function get invokedProjectID():String
        {
            if (_projectId != null && (Utilities.PLAYER_ID.test(_projectId) || _projectId == "open"))
                return _projectId;
            return null;
        }

        public function set invokedProjectID(id:String):void
        {
            _projectId = id;
        }
		
		public function get invokedPurchaseID():String
		{
			// project ID in environment vars is to be invoked at boot
			/*if (paramExists("purchaseId") && Utilities.UUID_REGEX.test(_flashVars["purchaseId"]))	
				return _flashVars["purchaseId"];*/
			if (!invokedAsEditor)
				return invokedProjectID;
			return null;
		}

        public function set fromLMS(id:Boolean):void
        {
            _fromLMS = id;
        }

		public function get fromLMS():Boolean
		{
			return _fromLMS;
		}

        public function get lmsValues():Object {
            return _lmsValues;
        }

        public function set lmsValues(value:Object):void {
            _lmsValues = value;
        }

        public function get projectName():String
        {
            return _projectName;
        }

        public function set projectName(name:String):void
        {
            _projectName = name;
        }

		public function set invokedFile(value:FileReference):void
		{
		}
		
		public function get invokedFile():FileReference
		{
			return null;
		}
		
		public function get isDev():Boolean
		{
			return DEV_REG_EXP.test(_serverName);
		}
		
		public function get loaderContext():LoaderContext
		{
			var loaderContext:LoaderContext = new LoaderContext();
			loaderContext.checkPolicyFile = true;
			return loaderContext;
		}
		
		private function paramExists(param:String):Boolean
		{
			if (_flashVars == null) return false;
			
			if ((param in _flashVars) &&  (_flashVars[param] != undefined))		
				return true;
			
			return false;
		}
		
		public function get locale():String
		{
			var locale:LocaleID = new LocaleID("en_US");
			return locale.name;
		}
		
		public function getDetailsURIForPublishedId(publishedId:String):String
		{
			if (publishedId)
				return RestHttpOperation.getProtocolForPort(_serverPort) + "://"+ _serverName + "/share/" + publishedId;
			return null;
		}
		
		public function getEmbedURIForPurchaseId(purchaseId:String=null):String
		{	
			if (!purchaseId)
				purchaseId = invokedPurchaseID;
			if (purchaseId)
				return RestHttpOperation.getProtocolForPort(_serverPort) + "://"+ _serverName + "/e/" + purchaseId;
			return null;
		}
		
        public function get hasDomainCapabilities():Boolean
        {
            return _hasDomainCapabilities || (TaconiteExternalInterface.call("getDomainCapabilities") == true);
        }

        public function set hasDomainCapabilities(value:Boolean):void
        {
            _hasDomainCapabilities = value;
        }

		public function requestDetailsPanel(id:String, owned:Boolean):void
		{
			TaconiteExternalInterface.call("requestDetailsPanel", {id:id, owned:owned});
		}
		
		public function requestSharingDialog(publishedId:String, userAwarePurchaseId:String):void
		{
			var o:Object = new Object();
			o.publishedId = publishedId;
			o.userAwarePurchaseId = userAwarePurchaseId;
			TaconiteExternalInterface.call("requestSharingDialog", o);
		}

        public function get useCustomProjectName():Boolean {
            return _useCustomProjectName;
        }

        public function set useCustomProjectName(value:Boolean):void {
            _useCustomProjectName = value;
        }

        public function set newProjectNameFunc(value:Function):void {
            _newProjectNameFunc = value;
        }

        public function get newProjectNameFunc():Function {
            return _newProjectNameFunc;
        }

        private static const VERSION_REG_EXP:RegExp = /(\d+),(\d+)/;

        private function get majorPlayerVersion():int {
        	var majorVersion:int = 0;
			var versionObj:Object = VERSION_REG_EXP.exec(Capabilities.version);
			if (versionObj != null && versionObj.length > 2) {
				majorVersion = int(versionObj[1]);
			}
			return majorVersion;
        }

        private function get minorPlayerVersion():int {
        	var minorVersion:int = 0;
			var versionObj:Object = VERSION_REG_EXP.exec(Capabilities.version);
			if (versionObj != null && versionObj.length > 2) {
				minorVersion = int(versionObj[2]);
			}
			return minorVersion;
        }
    }
}
