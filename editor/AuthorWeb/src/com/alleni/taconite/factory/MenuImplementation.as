/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.factory
{
	import com.alleni.author.application.components.MenuBarItem;
	import com.alleni.author.application.components.MenuBarWithKeys;
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	
	import mx.collections.XMLListCollection;
	import mx.controls.MenuBar;
	import mx.core.ClassFactory;
	import mx.events.MenuEvent;
	
	public class MenuImplementation implements IMenu
	{
		private static var _menu:MenuBarWithKeys;
		private static var _callbackFunc:Function = null;
		
		public function MenuImplementation():void
		{
			if (!_menu) {
				_menu = new MenuBarWithKeys(); 
				
				_menu.addEventListener(MenuEvent.ITEM_CLICK, handleMenuItemClick);
				// fix bug with arrow keys
				_menu.addEventListener(FocusEvent.FOCUS_IN, handleFocusIn);
				_menu.percentWidth = 100;
				_menu.menuBarItemRenderer = new ClassFactory(MenuBarItem);
				_menu.addEventListener(MenuEvent.MENU_SHOW, handleMenuShow);
			}
		}
		
		private function handleMenuShow(event:MenuEvent):void
		{
			ApplicationController.addEventListener(NotificationNamesApplication.STAGE_MOUSE_DOWN, function(e:Event):void {
				event.menu.hide();
				ApplicationController.removeEventListener(NotificationNamesApplication.STAGE_MOUSE_DOWN, arguments.callee);
			});
		}
		
		public function get menu():EventDispatcher
		{
			return _menu;
		}
		
		public function set dataProvider(value:XMLListCollection):void
		{
			_menu.dataProvider = value;
		}
				
		public function set labelField(value:String):void
		{
			_menu.labelField = value;
		}
		
		public function set keyEquivalentField(value:String):void
		{
			// unsupported
			null;
		}
		
		public function set callback(value:Function):void
		{
			_callbackFunc = value;
		}
		
		private function handleMenuItemClick(event:MenuEvent):void
		{
			_callbackFunc(event.item.@id.toString());
		}
		
		/**
		 * This fixes a bug with keys that should go to our app instead going to the menubar.
		 * Specifically, when a menu heading such as "Project" is clicked a 2nd time to close it
		 * (regardless of whether or not it is highlighted).
		 * Subsequent left-right arrow keys were popping up the next menu, instead of going to move a selected object. 
		 * @param event
		 * 
		 */
		private function handleFocusIn(event:FocusEvent):void
		{
			if (_menu) {
				if (_menu.stage.focus) {
					var focus:* = _menu.stage.focus;
					if (focus as MenuBar) {
						_menu.stage.focus = _menu.stage;
					}
				}
			}
		}
		
		public function set enabled(value:Boolean):void
		{
			_menu.enabled = value;
		}

	}
}