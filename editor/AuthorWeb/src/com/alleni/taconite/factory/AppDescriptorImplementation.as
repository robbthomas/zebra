package com.alleni.taconite.factory
{
	import flash.display.Stage;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;

	public class AppDescriptorImplementation implements IAppDescriptor
	{
		namespace ns = "http://ns.adobe.com/air/application/2.0";
		use namespace ns;
		
		private static var _stage:Stage;
		
		private static var _xml:XML;
		[Embed(source='/AuthorWeb-app.xml', mimeType="application/octet-stream")]
		public static const AppDescriptor:Class;
		
		public function AppDescriptorImplementation()
		{
			var ba:ByteArray = (new AppDescriptor()) as ByteArray;
			var s:String = ba.readUTFBytes(ba.length);
			_xml = new XML(s);
		}

		public function set stage(value:Stage):void
		{
			_stage = value	
		}
		
		public function get id():String
		{
			return _xml.id.toString();	
		}
		
		public function get name():String
		{
			return _xml.name.toString();
		}
		
		public function get version():String
		{
			return _xml.version.toString();		
		}
		
		public function get longName():String
		{
			return name + " " + version;	
		}
		
		public function get screenBounds():Rectangle
		{
			return new Rectangle(0, 0, _stage.stageWidth, _stage.stageHeight);
		}
		
		public function get mainScreenBounds():Rectangle
		{
			return new Rectangle(0, 0, _stage.stageWidth, _stage.stageHeight);
		}
	}
}