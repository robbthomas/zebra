/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.factory
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;

	public class StorageImplementation implements IStorage
	{
		public function openFile(context:DisplayObject):void
		{
			// open file
		}
		
		public function saveFile(context:DisplayObject, saveAs:Boolean=false):void
		{
			// save file
		}
	}
}