package com.alleni.taconite.factory
{
    public class EnvironmentPersonal
	{
        // personalized login data for the Zebra engineer
        // you can add any fields here that have a public setter in EnvironmentImplementation
        public static const data:Object = {
		    username:"jcoyle",
            password:"testpassword",
            serverName:"qa.zebrazapps.com"
            };

	}
}
