#!/usr/bin/perl

if (@ARGV < 2) {    
	print "usage: adjustVersionNumber.pl filename [increment|decrement]\n";
	print "   example: adjustVersionNumber.pl AppDescriptor-app.xml increment\n";
	exit 0;
}

open FILE, $ARGV[0] or die $!;
@newContents = ();
print $ARGV[1] . "ing version number in descriptor file " . $ARGV[0] . "\n";
while (<FILE>) {
	if ($_ =~ /<version>/) {
		if ($_ =~ m/(.*<version>)([a-zA-Z])(\d+)(.*\d*)(<\/version>.*)/ && $ARGV[1] !~ /revision/ ) {
			$sub1 = $1 . $2;
			$newVersion = $3;
			$sub2 = $5 . "\n";
			print "Replacing " . $sub1 . $newVersion . $sub2 . "\n";

			if ($ARGV[1] =~ /increment/) {
				$newVersion++;
			} elsif ($ARGV[1] =~ /decrement/) {
				$newVersion--;
			#} elsif () {
			#	$newVersion = $newVersion . "." . $ENV{'SVN_REVISION'};
			} elsif ($ARGV[1] =~ /patched/) {
				$newVersion = $newVersion . "p" . $ENV{'SVN_REVISION'};
			}
			print "with " . $sub1 . $newVersion . $sub2 . "\n";
			push(@newContents, $sub1 . $newVersion . $sub2);
		} else  {
			$basicVersionLine = "<version>" . $ENV{'SVN_REVISION'} ."</version>"; 
			print "Replacing " . $_ . " with new revision line: " . $basicVersionLine . "\n";
			push(@newContents, $basicVersionLine);
		}
	} else {
		push(@newContents, $_);
	}
}
close FILE or die $!;

print "Writing file ...\n";
open FILE, ">", $ARGV[0] or die $!;

foreach (@newContents) {
	print FILE $_;	
}

close FILE or die $!;
