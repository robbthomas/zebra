#!/usr/bin/perl

if (!-d ".svn") {
	print "Not in an SVN working copy. Exiting.\n";
	exit;
}

$svn_url = `svn --config-dir $ENV{'SVN_CONFIG'} info | grep '^URL' | sed -e 's/URL: *//'`;
chomp($svn_url);

if ($svn_url !~ /trunk/) {
	print "This working copy does not appear to have a trunk on the repository. Exiting.\n";
	exit;
}

if ($ENV{'SVN_REVISION'} == "") {
	print "SVN_REVISION environment variable is not set, this script is to be run within a Hudson SVN workspace. Exiting.\n";
	exit;
}

$svn_url =~ s/\/trunk.*$//;

print "SVN URL: " . $svn_url . "\n";

print "SVN Revision: " . $ENV{'SVN_REVISION'} . "\n";

$svn_patched_url = $svn_url . "/branches/patched";

$checkForCurrentPatch = `svn --config-dir $ENV{'SVN_CONFIG'} info $svn_patched_url | grep URL`;

if ($checkForCurrentPatch =~ /URL/) {
	print "Moving old patched " . $svn_patched_url . " to " . " $svn_url/branches/patched_$ENV{'SVN_REVISION'}" . "\n";
	system "svn --config-dir $ENV{'SVN_CONFIG'} mv $svn_patched_url $svn_url/branches/patched_$ENV{'SVN_REVISION'} -m 'move old patched branch to reference branch'";
}

print "Creating new patch-ready branch at " . $svn_patched_url . "\n";
system "svn --config-dir $ENV{'SVN_CONFIG'} cp $svn_url/trunk $svn_patched_url -m 'creating new patch-ready branch'";
