#!/usr/bin/perl

use Atlassian::Confluence;

print "Posting build $ENV{'BUILD_NUMBER'} as release $ENV{'RELEASE'}\n";

$confluence = new Confluence("http://confluence.alleni.com/confluence/rpc/xmlrpc", "builder", "BuUs2010");

$buildNumber = $ENV{'BUILD_NUMBER'};
$releaseNumber = $ENV{'RELEASE'};
$title = "Vermillion Desktop $releaseNumber";
$content = "\{authordesktop-release:ignored|1.5|$buildNumber|$releaseNumber\}\n
\n
h4. Release Notes\n" . $ENV{'RELEASE_NOTES'};

#$newBlog = {space => "TracImported", title => $title, content => $content};
$newBlog = {space => "release", title => $title, content => $content};

print($confluence->storeBlogEntry($newBlog));

