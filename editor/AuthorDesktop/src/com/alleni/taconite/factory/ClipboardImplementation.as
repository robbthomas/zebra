package com.alleni.taconite.factory
{
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.desktop.NativeDragManager;
	import flash.display.BitmapData;
	import flash.display.InteractiveObject;
	import flash.events.Event;
	import flash.events.NativeDragEvent;
	import flash.geom.Point;
	
	import flashx.textLayout.edit.TextClipboard;
	import flashx.textLayout.edit.TextScrap;

	public class ClipboardImplementation implements ISystemClipboard
	{
		private var _clipboard:Clipboard;
		
		public function ClipboardImplementation(e:Event=null)
		{
			if (e == null || !(e is NativeDragEvent)){
				_clipboard = Clipboard.generalClipboard;
			}
			else if (e != null && e is NativeDragEvent){
				_clipboard = NativeDragEvent(e).clipboard;
			}
		}
		
		public function hasFormat(format:String):Boolean
		{
			return _clipboard.hasFormat(format);
		}
		
		public function getData(format:String, transferMode:String="originalPreferred"):Object
		{
			if (format != TEXT_SCRAP)
				return _clipboard.getData(format, transferMode);
			else
				return TextClipboard.getContents();
		}
		
		public function setData(format:String, data:Object, serializable:Boolean=true):Boolean
		{
			_clipboard.clear();
			
			if (format != TEXT_SCRAP)
				return _clipboard.setData(format, data, serializable);
			else {
				TextClipboard.setContents(data as TextScrap);
				return true;
			}
		}
		
		public function clear():void
		{
			_clipboard.clear();
		}
		
		public function acceptDragDrop(target:InteractiveObject):void
		{
			NativeDragManager.acceptDragDrop(target);
		}
		
		public function doDrag(dragInitiator:InteractiveObject, dragImage:BitmapData=null, offset:Point=null):void
		{
			NativeDragManager.doDrag(dragInitiator, new Clipboard(), dragImage, offset);
		}
		
		public function get BITMAP_FORMAT():String
		{
			return ClipboardFormats.BITMAP_FORMAT;
		}
		
		public function get FILE_LIST_FORMAT():String
		{
			return ClipboardFormats.FILE_LIST_FORMAT;
		}
		
		public function get HTML_FORMAT():String
		{
			return ClipboardFormats.HTML_FORMAT;
		}
		
		public function get RICH_TEXT_FORMAT():String
		{
			return ClipboardFormats.RICH_TEXT_FORMAT;
		}
		
		public function get TEXT_FORMAT():String
		{
			return ClipboardFormats.TEXT_FORMAT;
		}
		
		public function get TEXT_SCRAP():String
		{
			return "textScrap"; // TODO: handle text scrap specifically using the TextClipboard
		}
		
		public function get URL_FORMAT():String
		{
			return ClipboardFormats.URL_FORMAT;
		}	
		
		public function get OBJECTS_FORMAT():String
		{
			return "ZebraObjectsFormat";
		}	
		
		public function get DRAG_ENTER():String
		{
			return NativeDragEvent.NATIVE_DRAG_ENTER;
		}
		
		public function get DRAG_OVER():String
		{
			return NativeDragEvent.NATIVE_DRAG_OVER;
		}
		
		public function get DRAG_DROP():String
		{
			return NativeDragEvent.NATIVE_DRAG_DROP;
		}
		
		public function get DRAG_COMPLETE():String
		{
			return NativeDragEvent.NATIVE_DRAG_COMPLETE;
		}
	}
}