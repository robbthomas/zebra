package com.alleni.taconite.factory
{
	import com.alleni.author.document.Document;
import com.alleni.author.model.World;
import com.alleni.author.model.gadgets.Gadget;
	import com.alleni.author.model.ui.Application;
import com.alleni.taconite.event.ApplicationEvent;
import com.alleni.taconite.service.RestHttpOperation;
	
	import flash.globalization.LocaleID;
	import flash.net.FileReference;
	import flash.system.Capabilities;
	import flash.system.LoaderContext;

	public class EnvironmentImplementation implements IEnvironment
	{
		// DESKTOP VERSION
		
		private static var MEDIA_STREAM_URI:String = "media.zebrabeta.com/cfx/st";

		private const DEV_REG_EXP:RegExp = /^int.*|qa|zebraqa|localhost/i;
		private static const GUEST_USERNAME:String = "guest";
		private static const GUEST_PASSWORD:String = "none";
		
		private static var _isAdmin:Boolean = true;
		private static var _accountId:String;
		private static var _accountName:String;
		private static var _companyName:String;
		private static var _userId:String;
		private static var _userFullName:String;
		private static var _email:String = "";
        private static var _customerType:String = "";
		private static var _invokedFile:FileReference;

		//		private static var _serverName:String = "integration.zebrazapps.com";
		//		private static var _username:String = "sbirth";
		//		private static var _password:String = "testpassword";
		
		private static var _serverName:String = "zebrazapps.com";
		private static var _username:String = "sbirth";
		private static var _password:String = "AllenZ";
//        private static var _projectID:String = null;  // Full screen arena
//        private static var _projectID:String = "2e4fbe074c0b4f5ba609e14ed7a70eb9";  // Fruit pages
//    private static var _projectID:String = "95ef0e71caa94a5db01c96212437c92c"  // master page fruit
    private static var _projectID:String = "c05041ce1a134a979a4548a7b686193b"  // 1 arena diversity

		//		private static var _serverName:String = "staging.zebrazapps.com";
		//		private static var _username:String = "sbirth@alleni.com";
		//		private static var _password:String = "AllenZ";
		//		private static var _username:String = "mallen@alleni.com";
		//		private static var _password:String = "michael";
		
		//		private static var _serverName:String = "preview.zebrazapps.com";
		//		private static var _username:String = "sbirth@alleni.com";
		//		private static var _password:String = "AllenZ";

        private static function get _serverPort():int
        {
            return (_serverName == "zebrazapps.com" || _serverName == "qa.zebrazapps.com") ? 443 : 80;
        }

		private static var _apiURI:String;
		
		private static var _contextPath:String = "/zephyr";

        public function get customerType():String{
            return _customerType;
        }
        public function set customerType(value:String):void{
            _customerType = value;
        }

        public function get targetUserName():String {
            return null;
        }

        public function get targetProjectName():String {
            return null;
        }

        public function handleError(message:String):void {
            //TODO nothing to do yet
        }

        public function addExternalCallback(name:String, callback:Function):void
        {
        }


        private function handleLmsGetValues(event:ApplicationEvent) {
        }

        private function lmsValuesReady(values:Object) {
        }

        private function handleLmsSetValues(event:ApplicationEvent):void {
        }
        public function set fromLMS(id:Boolean):void
        {
        }

		public function get fromLMS():Boolean
		{
            return false;
		}

        public function get lmsValues():Object {
            return null;
        }

        public function set lmsValues(value:Object):void {
        }


		public function get containerDetails():String
		{
			return Capabilities.os;
		}
		
		public function get isAdmin():Boolean
		{
			return true; // _isAdmin;
		}
		
		public function set isAdmin(value:Boolean):void
		{
			_isAdmin = value;
		}
		
		public function get guestUsername():String
		{
			if (!invokedAsEditor) return GUEST_USERNAME;
			else return null;
		}
		
		public function get guestPassword():String
		{
			if (!invokedAsEditor) return GUEST_PASSWORD;
			else return null;
		}
		
		public function get accountId():String
		{
			return _accountId;
		}
		
		public function set accountId(value:String):void
		{
			_accountId = value;
		}
		
		public function get accountName():String
		{
			return _accountName;
		}
		
		public function set accountName(value:String):void
		{
			_accountName = value;
		}
		
		public function get companyName():String
		{
			return _companyName;
		}
		
		public function set companyName(value:String):void
		{
			_companyName = value;
		}
		
		public function get userId():String
		{
			return _userId;
		}
		
		public function set userId(value:String):void
		{
			_userId = value;
		}
		
		public function get username():String
		{	
			return _username;	
		}
		
		public function set username(value:String):void
		{
			_username = value;
		}
		
		public function get userFullName():String
		{
			return _userFullName;
		}
		
		public function set userFullName(value:String):void
		{
			_userFullName = value;
		}
		
		public function get password():String
		{	
			return _password;	
		}
		
		public function set password(value:String):void
		{
			_password = value;
		}
		
		public function get email():String
		{			
			return _email;
		}
		
		public function set email(value:String):void
		{			
			_email = value;
		}
		
		public function get serverName():String
		{
			return _serverName;
		}
		
		public function set serverName(value:String):void
		{
			_serverName = value;
		}
		
		public function get apiURI():String
		{
			if (!_apiURI)
				_apiURI = RestHttpOperation.getProtocolForPort(_serverPort) + "://"+ _serverName + _contextPath + "/";
			return _apiURI;
		}
		
		public function get mediaStreamURI():String
		{	
			return MEDIA_STREAM_URI;
		}
		
		public function get invokedAsEditor():Boolean
		{
			return true;
		}
		
        public function get showHistory():Boolean
        {
            return false;
        }
		
        public function get invokedProjectID():String
        {
            return _projectID;
        }

		public function get invokedPurchaseID():String
		{
			return null;
		}

        public function get projectName():String
        {
           return null;
        }

		public function set invokedFile(value:FileReference):void
		{
			_invokedFile = value;
		}
		
		public function get invokedFile():FileReference
		{
			return _invokedFile;	
		}
		
		public function get isDev():Boolean
		{
			return DEV_REG_EXP.test(_serverName);
		}
		
		public function get loaderContext():LoaderContext
		{
			var loaderContext:LoaderContext = new LoaderContext();
			loaderContext.allowCodeImport = true;
			return loaderContext;
		}
		
		public function get locale():String
		{
			var locale:LocaleID = new LocaleID("en_US");
			return locale.name;
		}
		
		public function getDetailsURIForPublishedId(publishedId:String):String
		{
			if (publishedId)
				return RestHttpOperation.getProtocolForPort(_serverPort) + "://"+ _serverName + "/share/" + publishedId;
			return null;
		}
		
		public function getEmbedURIForPurchaseId(purchaseId:String=null):String
		{	
			if (!purchaseId)
				purchaseId = invokedPurchaseID;
			if (purchaseId)
				return RestHttpOperation.getProtocolForPort(_serverPort) + "://"+ _serverName + "/e/" + purchaseId;
			else return null;
		}
		
		public function get hasDomainCapabilities():Boolean
		{
			return false; // currently exclusive to the Player, to determine if JS wrapper is present
		}
		
		public function requestDetailsPanel(id:String):void
		{
			// desktop todo: open window/browser containing published details
		}
		
		public function requestSharingDialog(publishedId:String, userAwarePurchaseId:String):void
		{
			// desktop todo: open window/browser containing sharing dialog
		}
	}
}