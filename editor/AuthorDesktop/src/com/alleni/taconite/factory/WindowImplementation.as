/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.factory
{
import com.alleni.author.model.ui.Application;

import flash.display.DisplayObject;
	import flash.display.NativeWindowType;
	import flash.events.Event;
	import flash.events.EventDispatcher;
import flash.events.NativeWindowBoundsEvent;
import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.core.IVisualElement;
	import mx.core.IWindow;
	import mx.events.AIREvent;
	import mx.managers.ICursorManager;
	
	import spark.components.Window;
	
	public class WindowImplementation extends EventDispatcher implements ITaconiteWindow
	{
        //  DESKTOP VERSION

        private static var _utilityWindows:Vector.<Window> = new Vector.<Window>();


		private var _window:Window = null;
		private var _closeCallbackFunction:Function = null;
		private var _exitWindow:Boolean = false;

        private static const TITLEBAR_HEIGHT:Number = 14;
		
		public function WindowImplementation()
		{
			_window = new Window();
		}
		
		private function closingListener(e:Event):void
		{
            trace("Desktop:WindowImp: closingListener");
			if (!_exitWindow && _closeCallbackFunction != null) {
				e.preventDefault();
				_closeCallbackFunction();
			}
		}
		
		public function open(bounds:Rectangle, title:String="Window", resizable:Boolean=true, utility:Boolean=false, hideInitially:Boolean=false):void
		{
            trace("Desktop:WindowImp: open");
			_window.visible = !hideInitially;
			_window.width = bounds.width;
			_window.height = bounds.height;
			_window.styleName = "grayBackground";
			//_window.minWidth = 704;		// derived by inspection (i.e., what looks good), 10/13/09
			//_window.minHeight = 204;	// derived by inspection (i.e., what looks good), 10/13/09
			_window.title = title;
			_window.resizable = resizable;
			_window.maximizable = resizable;
			_window.showStatusBar = false;
			////_window.showGripper = resizable;
			////_window.horizontalScrollPolicy = "off";
			////_window.verticalScrollPolicy = "off";
			if (utility) {
                _window.type = NativeWindowType.UTILITY;
                _window.alwaysInFront = true;
                _window.addEventListener(Event.DEACTIVATE, onAppDeactivate);
                _window.addEventListener(Event.ACTIVATE, onAppActivate);
                _utilityWindows.push(_window);
            } else {
                _window.type = NativeWindowType.NORMAL;
            }
			_window.open(true);
			_window.move(bounds.x, bounds.y);
			_window.orderToFront();
			_window.addEventListener(Event.CLOSING, closingListener);
			_window.addEventListener(AIREvent.WINDOW_ACTIVATE, handleWindowActivate);
			_window.addEventListener(AIREvent.WINDOW_DEACTIVATE, handleWindowDeactivate);
			_window.addEventListener(AIREvent.WINDOW_COMPLETE, handleWindowComplete);
			_window.addEventListener(Event.CLOSE, handleWindowClose);
		}
		
		public function close():void
		{
            trace("Desktop:WindowImp: close()");
            if (_window.type == NativeWindowType.UTILITY) {
                var index:int = _utilityWindows.indexOf(_window);
                if (index >= 0) {
                    _utilityWindows.splice(index, 1);
                }
            }

			_window.close();
		}
		
		public function exit():void
		{
            trace("Desktop:WindowImp: exit()");
            if (_window.type == NativeWindowType.NORMAL) {   // the App is being closed
                for each (var w:Window in _utilityWindows) {
                    w.close();
                }
            }

			_window.removeEventListener(Event.CLOSING, closingListener);
			_window.removeEventListener(AIREvent.WINDOW_ACTIVATE, handleWindowActivate);
			_window.removeEventListener(AIREvent.WINDOW_COMPLETE, handleWindowComplete);
			_window.removeEventListener(AIREvent.WINDOW_DEACTIVATE, handleWindowDeactivate);
			_window.close();
		}
		
		public function requestNewWindowAndInvokeId(id:String):void
		{
			
		}
		
		public function get nativeWindow():IWindow
		{
			return _window;
		}

		public function get visible():Boolean
		{
			return (_window != null) && _window.visible;
		}
		
		public function set visible(value:Boolean):void
		{
            trace("Desktop:WindowImp: setVis="+value);
			_window.visible = value;
			if (value)
				_window.activate();
		}

        public function get globalPosition():Point
        {
            var originOnScreen:Point = _window.nativeWindow.globalToScreen(new Point(0,0));
            var app:WindowImplementation = Application.instance.window as WindowImplementation;
            var appOrigin:Point = app.nativeWindow.nativeWindow.globalToScreen(new Point(0,0));
            var global:Point = originOnScreen.subtract(appOrigin);
            trace("Desktop:WindowImp: get globalPosition: origin="+originOnScreen, "appOrigin="+appOrigin, "global="+global);
            return global;
        }

        public function set globalPosition(value:Point):void
        {
            var app:WindowImplementation = Application.instance.window as WindowImplementation;
            var screenOrigin:Point = app.nativeWindow.nativeWindow.globalToScreen(value);
            trace("Desktop:WindowImp: set position="+value, "screenOrigin="+screenOrigin);

            move(screenOrigin.x, screenOrigin.y - TITLEBAR_HEIGHT);
        }

		public function set bounds(value:Rectangle):void
		{
            trace("Desktop:WindowImp: setBounds="+value);
			_window.width = value.width;
			_window.height = value.height;
			
			move(value.x, value.y);
		}
		
		private function move(x:Number, y:Number):void
		{
            trace("Desktop:WindowImp: move="+x, y);
			var width:Number = _window.width;
			var height:Number = _window.height;
			
			_window.move(x,y);
			
			_window.width = width;
			_window.height = height;
		}

		public function sendToBack():void
		{
			_window.orderToBack();
		}
		
		public function bringToFront():void
		{
			_window.orderToFront();
			_window.activate();
		}
		
		public function get originX():Number
		{
			return _window.nativeWindow.globalToScreen(new Point(_window.x, _window.y)).x;
		}

		public function get originY():Number
		{
			return _window.nativeWindow.globalToScreen(new Point(_window.x, _window.y)).y;
		}

		public function get width():Number
		{
			return _window.width;
		}

		public function get height():Number
		{
			return _window.height;
		}

        public function set width(value:Number):void
        {
            _window.width = value;
        }

        public function set height(value:Number):void
        {
            _window.height = value;
        }

        public function set exitWindow(value:Boolean):void
        {
            trace("Desktop:WindowImp: exitWindow()");
            _exitWindow = value;
        }

		public function addElement(element:IVisualElement):IVisualElement
		{
			_window.addElement(element);
			return element;
		}

		public function set menu(value:*):void
		{
			_window.menu = value;
		}
		
		public function get title():String
		{
			return _window.title;
		}
		
		public function set title(value:String):void
		{
			_window.title = value;
		}
		
		public function get displayObject():DisplayObject
		{
			return _window;
		}
		
		public function get cursorManager():ICursorManager
		{
			return _window.cursorManager;
		}
		
		public function set closeCallback(closeCallBackFunction:Function):void
		{
			_closeCallbackFunction = closeCallBackFunction;
		}
		
		public function set isPopup(value:Boolean):void
		{
			_window.isPopUp = value;
		}

        private function onAppDeactivate(event:Event):void {
            _window.alwaysInFront=false;
        }

        private function onAppActivate(event:Event):void {
            _window.alwaysInFront=true;
        }

        private function onWindowMove(event:Event):void {
            _window.dispatchEvent(new Event("xyChanged"));  // dispatch on _window, since addEventListener is redirected to there
        }

		override public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void
		{
			_window.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}

		override public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void
		{
			_window.removeEventListener(type, listener, useCapture);
		}

		public function get WINDOW_ACTIVATE():String
		{
			return AIREvent.WINDOW_ACTIVATE;
		}
		
		public function get WINDOW_COMPLETE():String
		{
			return AIREvent.WINDOW_COMPLETE;
		}
		
		public function get WINDOW_DEACTIVATE():String
		{
			return AIREvent.WINDOW_DEACTIVATE;
		}
		
		private function handleWindowActivate(e:Event):void
		{
			dispatchEvent(new Event(WINDOW_ACTIVATE));
		}
		
		private function handleWindowComplete(e:Event):void
		{
            if (_window.type == NativeWindowType.UTILITY) {
                _window.nativeWindow.addEventListener(NativeWindowBoundsEvent.MOVE, onWindowMove);
            }

			dispatchEvent(new Event(WINDOW_COMPLETE));
		}
		
		private function handleWindowDeactivate(e:Event):void
		{
			dispatchEvent(new Event(WINDOW_DEACTIVATE));
		}
		
		private function handleWindowClose(e:Event):void
		{
            trace("Desktop:WindowImp: handleWindowClose()");
			dispatchEvent(new Event(Event.CLOSE));
		}
	}
}
