package com.alleni.taconite.factory
{
	import com.alleni.taconite.controller.IWindowListener;
	
	import flash.ui.Mouse;
	
	import mx.core.Window;
	import mx.managers.ICursorManager;
	
	public class CursorManagerImplementation implements ICursorManager, IWindowListener
	{
		private var _window:ITaconiteWindow;
		
		public function CursorManagerImplementation()
		{
		}
		
		public function get currentCursorID():int
		{
			return _window.cursorManager.currentCursorID;
		}
		
		public function set currentCursorID(value:int):void
		{
			_window.cursorManager.currentCursorID = value;
		}
		
		public function get currentCursorXOffset():Number
		{
			return _window.cursorManager.currentCursorXOffset;
		}
		
		public function set currentCursorXOffset(value:Number):void
		{
			_window.cursorManager.currentCursorXOffset = value;
		}
		
		public function get currentCursorYOffset():Number
		{
			return _window.cursorManager.currentCursorYOffset;
		}
		
		public function set currentCursorYOffset(value:Number):void
		{
			_window.cursorManager.currentCursorYOffset = value;
		}
		
		public function showCursor():void
		{
			_window.cursorManager.showCursor();
		}
		
		public function hideCursor():void
		{
			try{
				_window.cursorManager.hideCursor();
				(_window as Window).stage.nativeWindow.activate();
				(_window as Window).stage.nativeWindow.orderToBack();
				(_window as Window).stage.nativeWindow.orderToFront();
				Mouse.hide();
			}catch(e:Error){}
		}
		
		public function setCursor(cursorClass:Class, priority:int=2, xOffset:Number=0, yOffset:Number=0):int
		{
			return _window.cursorManager.setCursor(cursorClass, priority, xOffset, yOffset);
		}
		
		public function removeCursor(cursorID:int):void
		{
			_window.cursorManager.removeCursor(cursorID);
		}
		
		public function removeAllCursors():void
		{
			_window.cursorManager.removeAllCursors();
		}
		
		public function setBusyCursor():void
		{
			_window.cursorManager.removeAllCursors();
		}
		
		public function removeBusyCursor():void
		{
			_window.cursorManager.removeBusyCursor();
		}
		
		public function registerToUseBusyCursor(source:Object):void
		{
			// Nothing should be needed here
		}
		
		public function unRegisterToUseBusyCursor(source:Object):void
		{
			// Nothing should be needed here
		}
		
		public function set window(value:ITaconiteWindow):void{
			_window = value;
		}
	}
}