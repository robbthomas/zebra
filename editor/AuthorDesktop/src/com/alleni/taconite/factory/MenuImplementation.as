/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id$  */

package com.alleni.taconite.factory
{
	/*import com.alleni.taconite.editor.ITaconiteMenuMediator;*/
	
	import flash.events.EventDispatcher;
	import flash.system.Capabilities;
	import flash.ui.Keyboard;
	
	import mx.collections.XMLListCollection;
	import mx.controls.FlexNativeMenu;
	import mx.events.FlexNativeMenuEvent;
	
	public class MenuImplementation implements IMenu
	{
		private static var _menu:FlexNativeMenu = null;
		private static var _callbackFunc:Function = null;
		private static var _enabled:Boolean = true;
		
		
		public function MenuImplementation():void
		{
			if (!_menu) {
				_menu = new FlexNativeMenu();
				_menu.addEventListener(FlexNativeMenuEvent.ITEM_CLICK, menuEventListener);
				_menu.keyEquivalentFunction = keyEquivalentFunction;
			}
		}
		
		public function get menu():EventDispatcher
		{
			return _menu;
		}
		
		public function set dataProvider(value:XMLListCollection):void
		{
			_menu.dataProvider = value;
		}
				
		public function set labelField(value:String):void
		{
			_menu.labelField = value;
		}
		
		public function set keyEquivalentField(value:String):void
		{
			_menu.keyEquivalentField = value;
		}

		public function set callback(value:Function):void
		{
			_callbackFunc = value;
		}
		
		private function menuEventListener(event:FlexNativeMenuEvent):void
		{
			if (_enabled) {
				_callbackFunc(event.item.@id.toString());
			}
		}
		
		private function keyEquivalentFunction(item:Object):String
		{
			return "";  // hotkeys now come into KeyMediator;  this prevents a double-execution via the menu
		}
		
		public function set enabled(value:Boolean):void
		{
			_enabled = value;
		}

	}
}