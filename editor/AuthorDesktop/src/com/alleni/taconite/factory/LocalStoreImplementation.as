package com.alleni.taconite.factory
{
	import flash.data.EncryptedLocalStore;
	import flash.utils.ByteArray;

	public class LocalStoreImplementation implements ILocalStore
	{
		public function getItem(name:String):ByteArray
		{
			return EncryptedLocalStore.getItem(name);
		}
		
		public function setItem(name:String, data:ByteArray, stronglyBound:Boolean=false):void
		{
			EncryptedLocalStore.setItem(name, data, stronglyBound);
		}
	}
}