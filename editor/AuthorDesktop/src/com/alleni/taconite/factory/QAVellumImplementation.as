package com.alleni.taconite.factory
{	
	import com.alleni.author.definition.Styles;
	import com.alleni.author.model.ui.DrawingPreset;
	import com.alleni.author.model.ui.QAVellumSettings;
	
	import flash.display.DisplayObject;
	import flash.display.NativeWindowSystemChrome;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mx.core.IWindow;
	import mx.core.Window;
	
	public class QAVellumImplementation extends Window implements IQAVellum
	{
		private var _qaWindow:IWindow;
		private var _vellum:Sprite;
		private var _annotation:Shape;
		
		public function QAVellumImplementation(qaPalette:ITaconiteWindow)
		{
			super();
		
			_qaWindow = WindowImplementation(qaPalette).nativeWindow;
			this.systemChrome = NativeWindowSystemChrome.NONE;
			this.setStyle("showFlexChrome", "false");
			this.visible = false;
			this.transparent = true;
			this.orderToFront();
			this.width = TaconiteFactory.getAppDescriptorImplementation().screenBounds.width;
			this.height = TaconiteFactory.getAppDescriptorImplementation().screenBounds.height;
			this.layout = "absolute";
			this.open();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			// Create the vellum overlay
			_vellum = new Sprite();
			_vellum.alpha = QAVellumSettings.VELLUM_ALPHA;
			
			// Create the drawing annotation
			_annotation = new Shape();
		}
		
		override protected function childrenCreated():void
		{
			super.childrenCreated();
			
			// Set up event listeners
			_vellum.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			_vellum.doubleClickEnabled = true;
			_vellum.addEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
			
			_vellum.graphics.clear();
			_vellum.graphics.beginFill(QAVellumSettings.VELLUM_COLOR);
			_vellum.graphics.drawRect(0, 0, width, height);
			_vellum.graphics.endFill();
		}
		
		override public function set initialized(value:Boolean):void
		{
			super.initialized = value;
			
			this.nativeWindow.x = 0;
			this.nativeWindow.y = 0;
			this.rawChildren.addChild(_annotation);
			this.rawChildren.addChild(_vellum);
		}
		
		override public function set visible(value:Boolean):void
		{
			super.visible = value;
			if (value) this.orderInBackOf(_qaWindow);
		}
		
		private function handleMouseDown(e:MouseEvent):void
		{
			_vellum.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_vellum.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			this.orderInBackOf(_qaWindow);
			var drawingPreset:DrawingPreset = Styles.SKETCHING_PRESETS[QAVellumSettings.drawingPreset];
			_annotation.graphics.lineStyle(drawingPreset.lineThickness, drawingPreset.lineColor, drawingPreset.lineAlpha/100);
			_annotation.graphics.moveTo(stage.mouseX, stage.mouseY);
		}
		
		private function handleMouseMove(e:MouseEvent):void
		{
			if (!e.buttonDown) return;
			_annotation.graphics.lineTo(stage.mouseX, stage.mouseY);
			e.updateAfterEvent();
		}
		
		private function handleMouseUp(e:MouseEvent):void
		{
			_vellum.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_vellum.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		}
		
		public function get displayObject():DisplayObject
		{
			return _annotation;
		}
		
		// Close the vellum on a double-click event
		private function handleDoubleClick(e:MouseEvent):void
		{
			//this.visible = false;
		}
		
		override public function close():void
		{
			super.close();
		}
		
		public function clear():void
		{
			if (this.visible)
				_annotation.graphics.clear();
		}
	}
}