package com.alleni.taconite.factory
{
	import com.alleni.taconite.service.LogService;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.utils.ByteArray;

	public class FileImplementation extends EventDispatcher implements IFile
	{
		private var _file:File;
		private var _url:String;
		
		public function FileImplementation(file:Object=null)
		{
			if (file == null)
				_file = new File();
			else
				_file = file as File;
		}
		
		public function get url():String
		{
			return _file.url;	
		}
		
		public function set url(value:String):void
		{
			_file.url = value;
		}
		
		public function get extension():String
		{
			return _file.extension;
		}
		
		public function get isDirectory():Boolean
		{
			return _file.isDirectory;
		}
		
		public function write(data:*):void
		{
			var fileStream:FileStream = new FileStream();
			fileStream.open(_file, FileMode.WRITE);
			
			if (data is XML)
				fileStream.writeUTFBytes((data as XML).toXMLString());
			else if (data is String)
				fileStream.writeUTFBytes(data);
			fileStream.close();

			dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public function writeToStorage(data:*, path:String):void
		{
			_file = File.documentsDirectory.resolvePath("Zebra/"+path);
			trace("writing bytes to " + _file.nativePath);
			
			var fileDir:File = _file.parent;
			fileDir.createDirectory();
			
			if (data is ByteArray) {
				var bytes:ByteArray = data as ByteArray;
				var fileStream:FileStream = new FileStream();
				fileStream.open(_file, FileMode.WRITE);
				fileStream.writeBytes(bytes, 0, bytes.length);
				fileStream.close();
				
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}
		
		public function browseForOpen(title:String, extension:String=null):void
		{
			if (extension != null) var typeFilter:FileFilter = new FileFilter("Zebra Authoring XML", "*."+extension);
			_file.addEventListener(Event.SELECT, handleDispatch);
			_file.addEventListener(Event.CANCEL, handleDispatch);
			_file.browseForOpen(title, [typeFilter]);
		}
		
		public function browseForSave(title:String):void
		{
			_file = File.documentsDirectory;
			_file.addEventListener(Event.SELECT, handleDispatch);
			_file.addEventListener(Event.CANCEL, handleDispatch);
			_file.browseForSave(title);
		}
		
		private function handleDispatch(e:Event):void
		{
			dispatchEvent(e);
		}
		
		public function resolvePath(path:String):FileReference
		{
			var file:FileReference = File.desktopDirectory.resolvePath(path);
			return file;
		}
	}
}