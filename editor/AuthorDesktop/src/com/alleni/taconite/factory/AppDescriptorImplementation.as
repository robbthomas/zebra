package com.alleni.taconite.factory
{
	import flash.desktop.NativeApplication;
	import flash.display.Screen;
	import flash.geom.Rectangle;
	
	public class AppDescriptorImplementation implements IAppDescriptor
	{
		namespace ns = "http://ns.adobe.com/air/application/2.0";
		use namespace ns;
		
		public function get id():String
		{
			return NativeApplication.nativeApplication.applicationDescriptor.id.toString();	
		}
		
		public function get name():String
		{
			return NativeApplication.nativeApplication.applicationDescriptor.name.toString();	
		}
		
		public function get version():String
		{
			return NativeApplication.nativeApplication.applicationDescriptor.version.toString();	
		}
		
		public function get longName():String
		{
			return name + " " + version;	
		}
		
		public function get screenBounds():Rectangle
		{
			var bounds:Rectangle = Screen.screens[0].bounds;
			
			for (var i:int = 1 ; i < Screen.screens.length ; i++)
				bounds = Rectangle(Screen.screens[0].bounds).union(Screen.screens[i].bounds);

			return bounds;
		}
		
		public function get mainScreenBounds():Rectangle
		{
			return Screen.mainScreen.bounds;
		}
	}
}