package com.alleni.author.application.components
{
	import com.alleni.author.application.subAppLinkedComponents.Preloader;
	import com.alleni.taconite.factory.EnvironmentImplementation;
	
	import flash.display.Sprite;
	
	public class Preloader extends com.alleni.author.application.subAppLinkedComponents.Preloader
	{
		public function Preloader()
		{
			super();
		}
		
		override public function set preloader(value:Sprite):void
		{
			super.preloader = value;
			isEditor = new EnvironmentImplementation().invokedAsEditor;
		}
	}
}