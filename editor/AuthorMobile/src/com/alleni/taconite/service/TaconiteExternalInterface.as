package com.alleni.taconite.service
{
	import com.alleni.taconite.dev.Utilities;
	
	import flash.external.ExternalInterface;
	import flash.system.Security;

	public class TaconiteExternalInterface
	{
		private static var _instance:TaconiteExternalInterface;
		
		public function TaconiteExternalInterface()
		{
			if (_instance != null) throw new Error("Singleton, do not instantiate");
			Security.allowDomain("*");
			ExternalInterface.marshallExceptions = true;
		}
		
		static public function get instance():TaconiteExternalInterface
		{
			if (_instance == null)
				_instance = new TaconiteExternalInterface();
			return _instance;
		}
		
		public static function call(functionName:String, ...parameters):*
		{
			if (ExternalInterface.available) {
				var response:* = null;
				
				try {
					response = ExternalInterface.call(functionName, parameters);
					return response;
				} catch(e:Error) {
					trace("Couldn't call " + functionName + ", ignoring error:", e);
				}
			}
			return null;
		}
		
		public static function addCallback(functionName:String, closure:Function):void
		{
			try {
				ExternalInterface.addCallback(functionName, closure);
			} catch(e:Error) {
				trace("Couldn't add ExternalInterface callback " + functionName + ", ignoring error:", e);
			}
		}
	}
}