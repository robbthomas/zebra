package com.alleni.taconite.factory
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.author.document.Document;
	import com.alleni.author.model.gadgets.Gadget;
	import com.alleni.author.model.ui.Application;
	import com.alleni.taconite.dev.Utilities;
	import com.alleni.taconite.event.ApplicationEvent;
	import com.alleni.taconite.service.LogService;
	import com.alleni.taconite.service.RestHttpOperation;
	import com.alleni.taconite.service.TaconiteExternalInterface;
	
	import flash.globalization.LocaleID;
	import flash.net.FileReference;
	import flash.system.LoaderContext;
	
	public class EnvironmentImplementation implements IEnvironment
	{
		//private static var MEDIA_STREAM_URI:String = "media.zebrabeta.com/cfx/st";
		//private static var MEDIA_STREAM_URI:String = "s35ogpzlqny9gi.cloudfront.net/cfx/st";
		private static var MEDIA_STREAM_URI:String = "ec2-50-17-141-139.compute-1.amazonaws.com";

		private static const DEV_REG_EXP:RegExp = /^localhost/i;
		private static const GUEST_USERNAME:String = "guest";
		private static const GUEST_PASSWORD:String = "none";
		
		private static var _flashVars:Object;
		private static var _sessionId:String = "";
		
		private static var _isAdmin:Boolean;
		private static var _accountId:String;
		private static var _accountName:String;
		private static var _companyName:String;
		private static var _userId:String;
		private static var _username:String = "bbilyk";
		private static var _userFullName:String;
		private static var _password:String = "AllenZ";
		private static var _email:String = "";
	//	private static var _serverName:String = "zebraqa.alleni.com";
		private static var _serverName:String = "staging.zebrazapps.com";
	//	private static var _serverName:String = "qa.zebrazapps.com";
	//	private static var _serverName:String = "staging.zebrazapps.com";
		private static var _serverPort:int = 80;
		private static var _apiURI:String;

		private static var _contextPath:String = "/zephyr";
		
		private static var _invokedAsEditor:Boolean = false;
		private static var _showHistory:Boolean = false;
		
		public function get targetUserName():String
		{
			return null;
		}
		
		public function get targetProjectName():String
		{
			return null;
		}
		
		public function setupContainerDialog():void
		{
			TaconiteExternalInterface.addCallback("handleInvoke", handleInvoke);
			ApplicationController.addEventListener(NotificationNamesApplication.PROJECT_UNSAVED_CHANGES, handleUnsavedChanges);
		}
		
		private function handleUnsavedChanges(event:ApplicationEvent):void
		{
			TaconiteExternalInterface.call("handleUnsavedChanges", event.data);
		}
		
		public function handleError(message:String):void {
			TaconiteExternalInterface.call("handleError", message);
		}
		
		private function handleInvoke(id:String):void
		{
			LogService.info("asked to invoke: " + id);
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.INVOKE_PROJECT, id));
		}
		
		public function set envVars(value:Object):void
		{
			_flashVars = value;
			
			if (paramExists("username")) {
				if (_flashVars["username"] == "" || _flashVars["username"] == null)
					_username = GUEST_USERNAME;
				else
					_username = String(_flashVars["username"]);
			} else {
				if (paramExists("serverhostname")) // if we are live, default to guest credentials for undefined username
					_username = GUEST_USERNAME;
			}
			
			if (paramExists("password")) {
				if (_flashVars["password"] == "" || _flashVars["password"] == null)
					_password = GUEST_PASSWORD;
				else
					_password = String(_flashVars["password"]);
			} else {
				if (paramExists("serverhostname")) // if we are live, default to guest credentials for undefined password
					_password = GUEST_PASSWORD;
			}
			
			if (paramExists("email"))		
				_email = String(_flashVars["email"]);
			
			if (paramExists("serverhostname"))
				_serverName = String(_flashVars["serverhostname"]);	
			
			if (paramExists("serverport"))
				_serverPort = int(_flashVars["serverport"]);
			
			if (paramExists("contextPath"))
				_contextPath = String(_flashVars["contextPath"]);
			
			if (paramExists("isEditor"))		
				_invokedAsEditor = _flashVars["isEditor"] == "true";
			
			if (paramExists("showHistory"))
				_showHistory = true;

			var cookieValue:String = "";
			cookieValue = TaconiteExternalInterface.call("function(){return document.cookie;}");
			
			if (cookieValue != null && cookieValue.length > 0) {
				var parsedCookieArray:Array = cookieValue.match(new RegExp('(^|;) ?JSESSIONID=([^;]*)(;|$)'));
				
				if(parsedCookieArray != null && parsedCookieArray.length > 0) {
					_sessionId = parsedCookieArray[2];
				}
			}
			
			// only enable the log service at launch if we are in dev
			//LogService.enabled = this.isDev;
			// temporarily, enable by default for private beta
			LogService.enabled = true;
		}
		
		public function get containerDetails():String
		{
			var browser:String = TaconiteExternalInterface.call("function(){return BrowserDetect.browser}");
			var version:String = TaconiteExternalInterface.call("function(){return BrowserDetect.version}");
			var OS:String = TaconiteExternalInterface.call("function(){return BrowserDetect.OS}");
			if (OS)
				return browser + " " + version + " on " + OS;
			else
				return "";
		}
		
		public function get isAdmin():Boolean
		{
			return _isAdmin;
		}
		
		public function set isAdmin(value:Boolean):void
		{
			_isAdmin = value;
		}
		
		public function get guestUsername():String
		{
			if (!invokedAsEditor) return GUEST_USERNAME;
			else return null;
		}
		
		public function get guestPassword():String
		{
			if (!invokedAsEditor) return GUEST_PASSWORD;
			else return null;
		}
		
		public function get accountId():String
		{
			return _accountId;
		}
		
		public function set accountId(value:String):void
		{
			_accountId = value;
		}
		
		public function get accountName():String
		{
			return _accountName;
		}
		
		public function set accountName(value:String):void
		{
			_accountName = value;
		}
		
		public function get companyName():String
		{
			return _companyName;
		}
		
		public function set companyName(value:String):void
		{
			_companyName = value;
		}
		
		public function get userId():String
		{
			return _userId;
		}
		
		public function set userId(value:String):void
		{
			_userId = value;
		}
		
		public function get username():String
		{
			return _username;
		}
		
		public function set username(value:String):void
		{
			_username = value;
		}
		
		public function get userFullName():String
		{
			return _userFullName;
		}
		
		public function set userFullName(value:String):void
		{
			_userFullName = value;
		}
		
		public function get password():String
		{			
			return _password;
		}
		
		public function set password(value:String):void
		{
			_password = value;
		}
		
		public function get email():String
		{			
			return _email;
		}
		
		public function set email(value:String):void
		{			
			_email = value;
		}
		
		public function get sessionId():String
		{	
			return _sessionId;
		}
		
		public function set sessionId(value:String):void
		{
			_sessionId = value;
		}
		
		public function get serverName():String
		{
			return _serverName;
		}
		
		public function set serverName(value:String):void
		{
			_serverName = value;
		}
		
		public function get appURI():String
		{
			if (!_apiURI)
				_apiURI = RestHttpOperation.getProtocolForPort(_serverPort) + "://"+ _serverName + _contextPath + "/";
			return _apiURI;
		}
		
		public function get apiURI():String
		{	
			//return this.appURI + "api/";
			return this.appURI;
		}
		
		public function get mediaStreamURI():String
		{	
			return MEDIA_STREAM_URI;
		}
		
		public function get invokedAsEditor():Boolean
		{
			return _invokedAsEditor;
		}
		
		public function get showHistory():Boolean
		{
			return _showHistory;
		}
		
		public function get invokedProjectID():String
		{
			// project ID in environment vars is to be invoked at boot
			if (paramExists("projectId") && Utilities.UUID_REGEX.test(_flashVars["projectId"]))
				return _flashVars["projectId"];
			return null;
		}
		
		public function get invokedPurchaseID():String
		{///return "dc0b6ffb5fa340de95aa41850e685f8b"; // paint book
			return "0abc3c337457461b9e7a6f875e67550d"; // character
			// project ID in environment vars is to be invoked at boot
			/*if (paramExists("purchaseId") && Utilities.UUID_REGEX.test(_flashVars["purchaseId"]))	
				return _flashVars["purchaseId"];*/
			if (!_invokedAsEditor)
				return invokedProjectID;
			return null;
		}
		
		public function set invokedFile(value:FileReference):void
		{
		}
		
		public function get invokedFile():FileReference
		{
			return null;
		}

        public function get projectName():String
        {
           return null;
        }
		
		public function get isDev():Boolean
		{
			return DEV_REG_EXP.test(_serverName);
		}
		
		public function get loaderContext():LoaderContext
		{
			var loaderContext:LoaderContext = new LoaderContext();
			loaderContext.checkPolicyFile = true;
			return loaderContext;
		}
		
		private function paramExists(param:String):Boolean
		{
			if (_flashVars == null) return false;
			
			if ((param in _flashVars) &&  (_flashVars[param] != undefined))		
				return true;
			
			return false;
		}
		
		public function get locale():String
		{
			var locale:LocaleID = new LocaleID("en_US");
			return locale.name;
		}
		
		public function getDetailsURIForPublishedId(publishedId:String):String
		{
			if (publishedId)
				return RestHttpOperation.getProtocolForPort(_serverPort) + "://"+ _serverName + "/share/" + publishedId;
			return null;
		}
		
		public function getEmbedURIForPurchaseId(purchaseId:String=null):String
		{	
			if (!purchaseId)
				purchaseId = invokedPurchaseID;
			if (purchaseId)
				return RestHttpOperation.getProtocolForPort(_serverPort) + "://"+ _serverName + "/e/" + purchaseId;
			else return null;
		}
		
		public function get hasDomainCapabilities():Boolean
		{
			return TaconiteExternalInterface.call("getDomainCapabilities") == true;
		}
		
		public function requestDetailsPanel(id:String):void
		{
			TaconiteExternalInterface.call("requestDetailsPanel", id);
		}
		
		public function requestSharingDialog(publishedId:String, userAwarePurchaseId:String):void
		{
			var o:Object = new Object();
			o.publishedId = publishedId;
			o.userAwarePurchaseId = userAwarePurchaseId;
			TaconiteExternalInterface.call("requestSharingDialog", o);
		}
	}
}
