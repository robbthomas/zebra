package com.alleni.taconite.factory
{
	import mx.managers.CursorManager;
	import mx.managers.ICursorManager;
	
	public class CursorManagerImplementation implements ICursorManager
	{
		public function CursorManagerImplementation()
		{
		}
		
		public function get currentCursorID():int
		{
			return CursorManager.currentCursorID;
		}
		
		public function set currentCursorID(value:int):void
		{
			CursorManager.currentCursorID = value;
		}
		
		public function get currentCursorXOffset():Number
		{
			return CursorManager.currentCursorXOffset;
		}
		
		public function set currentCursorXOffset(value:Number):void
		{
			CursorManager.currentCursorXOffset = value;
		}
		
		public function get currentCursorYOffset():Number
		{
			return CursorManager.currentCursorYOffset;
		}
		
		public function set currentCursorYOffset(value:Number):void
		{
			CursorManager.currentCursorYOffset = value;
		}
		
		public function showCursor():void
		{
			CursorManager.showCursor();
		}
		
		public function hideCursor():void
		{
			CursorManager.hideCursor();
		}
		
		public function setCursor(cursorClass:Class, priority:int=2, xOffset:Number=0, yOffset:Number=0):int
		{
			return CursorManager.setCursor(cursorClass, priority, xOffset, yOffset);
		}
		
		public function removeCursor(cursorID:int):void
		{
			CursorManager.removeCursor(cursorID);
		}
		
		public function removeAllCursors():void
		{
			CursorManager.removeAllCursors();
		}
		
		public function setBusyCursor():void
		{
			CursorManager.removeAllCursors();
		}
		
		public function removeBusyCursor():void
		{
			CursorManager.removeBusyCursor();
		}
		
		public function registerToUseBusyCursor(source:Object):void
		{
			// Nothing should be needed here
		}
		
		public function unRegisterToUseBusyCursor(source:Object):void
		{
			// Nothing should be needed here
		}
		
		public function set window(value:ITaconiteWindow):void{
			// Window is not used for the web version of cursor manager.  This is needed since it will be called by the cursor controller.
		}
	}
}