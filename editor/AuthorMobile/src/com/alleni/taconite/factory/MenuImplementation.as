/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: MenuImplementation.as 12019 2011-06-16 19:05:36Z sbirth $  */

package com.alleni.taconite.factory
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	
	import mx.collections.XMLListCollection;
	import mx.core.ClassFactory;
	
	public class MenuImplementation implements IMenu
	{
		private static var _callbackFunc:Function = null;
		
		public function MenuImplementation():void
		{
		}
		
		public function get menu():EventDispatcher
		{
			return null;
		}
		
		public function set dataProvider(value:XMLListCollection):void
		{
		}
				
		public function set labelField(value:String):void
		{
		}
		
		public function set keyEquivalentField(value:String):void
		{
			// unsupported
			null;
		}
		
		public function set callback(value:Function):void
		{
			_callbackFunc = value;
		}
		
		public function set enabled(value:Boolean):void
		{
		}
	}
}