package com.alleni.taconite.factory
{
	import com.alleni.author.controller.ui.ApplicationController;
	import com.alleni.author.definition.application.NotificationNamesApplication;
	import com.alleni.taconite.event.ApplicationEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.FileReference;

	public class FileImplementation extends EventDispatcher implements IFile
	{
		// TODO: implement web-friendly File

		public function FileImplementation(file:Object=null)
		{
		}
		
		public function get url():String
		{
			return "";
		}
		
		public function set url(value:String):void
		{
			//
		}
		
		public function get extension():String
		{
			return "";
		}
		
		public function get isDirectory():Boolean
		{
			return false;
		}
		
		public function write(data:*):void
		{
			dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public function writeToStorage(data:*, path:String):void
		{
			// nothing to do yet for web
		}
		
		public function browseForOpen(title:String, extension:String=null):void
		{
			// nothing to do yet for web
		}
		
		public function browseForSave(title:String):void
		{
			ApplicationController.instance.dispatchEvent(new ApplicationEvent(NotificationNamesApplication.SAVE_PROJECT));
		}
		
		public function resolvePath(path:String):FileReference
		{
			return null;
		}
	}
}