/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: WindowImplementation.as 11856 2011-06-10 20:04:34Z pkrekelberg $  */

package com.alleni.taconite.factory
{
	import com.alleni.taconite.service.TaconiteExternalInterface;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.geom.Rectangle;
	
	import mx.core.IVisualElement;
	import mx.core.UIComponent;
	import mx.managers.ICursorManager;
	import mx.managers.PopUpManager;
	
	import spark.components.Application;
	import spark.components.TitleWindow;

	public class WindowImplementation extends EventDispatcher implements ITaconiteWindow
	{
		private static const STANDARD_MENUBAR_COMPONENT_HEIGHT:Number = 50.0;
		private static const TITLEWINDOW_HEADER_HEIGHT:Number = 30.0;
		
		private static var _application:Application;
		
		private var _titleWindow:TitleWindow; 
		private var _title:String;
		private var _isPopup:Boolean = false;
		
		public function WindowImplementation()
		{
		}
		
		public function set application(value:Application):void
		{
			_application = value;
			_application.addEventListener(FocusEvent.FOCUS_IN, handleWindowActivate);
			_application.addEventListener(FocusEvent.MOUSE_FOCUS_CHANGE, handleWindowDeactivate);
		}
		
		public function open(bounds:Rectangle, title:String="Window", resizable:Boolean=true, utility:Boolean=false, hideInitially:Boolean=false):void
		{
			if (_isPopup) {
				if (!_titleWindow) {
					_titleWindow = new TitleWindow();
					_titleWindow.visible = !hideInitially;
					_titleWindow.styleName = "blackBackground";
					_titleWindow.title = title;
					_titleWindow.x = bounds.x;
					_titleWindow.y = bounds.y;
					_titleWindow.width = bounds.width;
					_titleWindow.height = bounds.height + TITLEWINDOW_HEADER_HEIGHT;
				}
				_titleWindow.addEventListener(Event.ADDED_TO_STAGE, handleWindowComplete);
				_titleWindow.addEventListener(Event.CLOSE, handleWindowClose);
				_titleWindow.addEventListener(FocusEvent.FOCUS_IN, handleWindowActivate);
				_titleWindow.addEventListener(FocusEvent.MOUSE_FOCUS_CHANGE, handleWindowDeactivate);
				PopUpManager.addPopUp(_titleWindow, _application, false);
			}
		}
		
		public function close():void
		{
			if (_isPopup) {
				_titleWindow.removeEventListener(Event.ADDED_TO_STAGE, handleWindowComplete);
				_titleWindow.removeEventListener(Event.CLOSE, handleWindowClose);
				_titleWindow.removeEventListener(FocusEvent.FOCUS_IN, handleWindowActivate);
				_titleWindow.removeEventListener(FocusEvent.MOUSE_FOCUS_CHANGE, handleWindowDeactivate);
				PopUpManager.removePopUp(_titleWindow);
			}
		}
		
		public function requestNewWindowAndInvokeId(id:String):void
		{
			TaconiteExternalInterface.call("requestPopup", id);
		}
		
		public function sendToBack():void
		{
			// web need not support this
		}
		
		public function exit():void
		{
			this.close();
			TaconiteExternalInterface.call("handleApplicationQuit");
		}

		public function set exitWindow(value:Boolean):void
		{
		}
		
		public function addElement(element:IVisualElement):IVisualElement
		{
			if (_isPopup)
				_titleWindow.addElement(element);
			else {
				_application.addElement(element);
			}
			return element;
		}
		
		public function set menu(value:*):void
		{
			if (!value) return;
			var element:IVisualElement = value as IVisualElement;
			if (!element) {
				element = new UIComponent();
				(element as UIComponent).addChild(value);
			}
			for (var i:int = 0; i < _application.numElements; i++) // place all prior elements below the menubar, if there are any
				if (_application.getElementAt(i).y < STANDARD_MENUBAR_COMPONENT_HEIGHT)
					_application.getElementAt(i).y = STANDARD_MENUBAR_COMPONENT_HEIGHT;
			_application.addElement(element);
		}
		
		public function get title():String
		{
			if (_isPopup)
				return _titleWindow.title;
			else
				return _title;
		}
		
		public function set title(value:String):void
		{
			if (_isPopup)
				_titleWindow.title = value;
			else
				_title = value;
			TaconiteExternalInterface.call("changeTitle", value);
		}
		
		public function get displayObject():DisplayObject
		{
			if (_isPopup)
				return _titleWindow;
			else
				return _application;
		}
		
		public function get cursorManager():ICursorManager
		{
			return null;
		}
		
		public function get WINDOW_ACTIVATE():String
		{
			return "windowActivate";
		}
		
		public function get WINDOW_COMPLETE():String
		{
			return "windowComplete";
		}
		
		public function get WINDOW_DEACTIVATE():String
		{
			return "windowDeactivate";
		}
		
		private function handleWindowActivate(e:Event):void
		{
			dispatchEvent(new Event(WINDOW_ACTIVATE));
		}
		
		private function handleWindowDeactivate(e:Event):void
		{
			dispatchEvent(new Event(WINDOW_DEACTIVATE));
		}
		
		private function handleWindowComplete(e:Event):void
		{
			dispatchEvent(new Event(WINDOW_COMPLETE));
		}
		
		private function handleWindowClose(e:Event):void
		{
			this.close();
			e.stopImmediatePropagation();
			dispatchEvent(e);
		}

		public function get visible():Boolean
		{		
			if (_isPopup)
				return _titleWindow.visible;
			else
				return _application.visible;
		}
		
		public function set visible(value:Boolean):void
		{
			if (_isPopup)
				_titleWindow.visible = value;
			else
				_application.visible = value;
		}
		
		public function set bounds(value:Rectangle):void
		{
			if (_isPopup) {
				_titleWindow.x = value.x;
				_titleWindow.y = value.y;
				_titleWindow.width = value.width;
				_titleWindow.height = value.height + TITLEWINDOW_HEADER_HEIGHT;
			} else {
				_application.x = value.x;
				_application.y = value.y;
				_application.width = value.width;
				_application.height = value.height;
			}
		}
		
		public function set preventClose(value:Boolean):void
		{
		}

		public function get originX():Number
		{
			if (_isPopup)
				return _titleWindow.x;
			else
				return _application.x;
		}
		
		public function get originY():Number
		{
			if (_isPopup)
				return _titleWindow.y;
			else
				return _application.y;
		}
		
		public function get width():Number
		{
			if (_isPopup)
				return _titleWindow.width;
			else
				return _application.width;
		}
		
		public function get height():Number
		{
			if (_isPopup)
				return _titleWindow.height;
			else
				return _application.height;
		}

		public function get screenBounds():Rectangle
		{
			return new Rectangle(0, 0, _application.stage.stageWidth, _application.stage.stageHeight);
		}

		public function set closeCallback(closeCallBackFunction:Function):void
		{
		}

		public function set isPopup(value:Boolean):void
		{
			_isPopup = value;
		}
	}
}