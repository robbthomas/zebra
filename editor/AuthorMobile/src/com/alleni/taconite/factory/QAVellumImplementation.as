package com.alleni.taconite.factory
{	
	import com.alleni.author.definition.Styles;
	import com.alleni.author.model.ui.Application;
	import com.alleni.author.model.ui.DrawingPreset;
	import com.alleni.author.model.ui.QAVellumSettings;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.core.UIComponent;

	public class QAVellumImplementation extends UIComponent implements IQAVellum
	{
		private var _vellum:Sprite;
		private var _annotation:Shape;
		
		public function QAVellumImplementation(qaPalette:ITaconiteWindow)
		{
			super();
			
			this.visible = false;
			this.width = 100;
			this.height = 100;
			Application.instance.window.addElement(this);
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			// Create the vellum overlay
			_vellum = new Sprite();
			_vellum.alpha = QAVellumSettings.VELLUM_ALPHA;
			
			// Create the drawing annotation
			_annotation = new Shape();
		}
		
		override protected function childrenCreated():void
		{
			super.childrenCreated();
			
			// Set up event listeners
			_vellum.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			_vellum.doubleClickEnabled = true;
			_vellum.addEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
			
			_vellum.graphics.clear();
			_vellum.graphics.beginFill(QAVellumSettings.VELLUM_COLOR);
			_vellum.graphics.drawRect(0, 0, width, height);
			_vellum.graphics.endFill();
			
			setSize();
		}
		
		override public function set initialized(value:Boolean):void
		{
			super.initialized = value;
			
			// ensure vellum is atop all other window children
			Application.instance.window.addElement(this);
			this.x = 0;
			this.y = 0;
			this.addChild(_annotation);
			this.addChild(_vellum);
			this.stage.addEventListener(Event.RESIZE, setSize);
		}
		
		private function setSize(event:Event=null):void
		{
			_vellum.width = TaconiteFactory.getAppDescriptorImplementation().screenBounds.width;
			_vellum.height = TaconiteFactory.getAppDescriptorImplementation().screenBounds.height;
		}
		
		private function handleMouseDown(e:MouseEvent):void
		{
			_vellum.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_vellum.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			var drawingPreset:DrawingPreset = Styles.SKETCHING_PRESETS[QAVellumSettings.drawingPreset];
			_annotation.graphics.lineStyle(drawingPreset.lineThickness, drawingPreset.lineColor, drawingPreset.lineAlpha/100);
			_annotation.graphics.moveTo(stage.mouseX, stage.mouseY);
		}
		
		private function handleMouseMove(e:MouseEvent):void
		{
			if (!e.buttonDown) return;
			_annotation.graphics.lineTo(stage.mouseX, stage.mouseY);
			e.updateAfterEvent();
		}
		
		private function handleMouseUp(e:MouseEvent):void
		{
			_vellum.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			_vellum.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		}
		
		public function get displayObject():DisplayObject
		{
			return _annotation;
		}
		
		// Close the vellum on a double-click event
		private function handleDoubleClick(e:MouseEvent):void
		{
			//this.visible = false;
		}
		
		public function close():void
		{
			this.visible = false;
		}
		
		public function clear():void
		{
			if (this.visible)
				_annotation.graphics.clear();
		}
	}
}