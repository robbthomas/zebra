/**
 * © Copyright 2009 Allen Learning Technologies LLC.  All rights reserved.
 * 
 * @author Allen Skunkworks
 * @version $Id: StorageImplementation.as 4912 2010-06-02 22:36:47Z pkrekelberg $  */

package com.alleni.taconite.factory
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;

	public class StorageImplementation implements IStorage
	{
		public function openFile(context:DisplayObject):void
		{
			// open file
		}
		
		public function saveFile(context:DisplayObject, saveAs:Boolean=false):void
		{
			// save file
		}
	}
}