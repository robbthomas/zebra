package com.alleni.taconite.factory
{
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.display.BitmapData;
	import flash.display.InteractiveObject;
	import flash.events.Event;
	import flash.geom.Point;
	
	import flashx.textLayout.edit.TextClipboard;
	import flashx.textLayout.edit.TextScrap;

	public class ClipboardImplementation implements ISystemClipboard
	{
		// this is an incomplete first pass at a web clipboard
		// just trying to enable copy/paste of zebra objects
		
		private static var _format:String = "";
		private static var _data:Object;
		private static var _event:Event;
		private static var _clipboard:Clipboard;
		
		public function ClipboardImplementation(e:Event=null)
		{
			_event = e;
			if (!_clipboard)
				_clipboard = Clipboard.generalClipboard;
		}
		
		public function hasFormat(format:String):Boolean
		{
			trace("WebClipboard::hasFormat: format="+format, "result="+_clipboard.hasFormat(format));
			return _clipboard.hasFormat(format);
		}

		public function getData(format:String, transferMode:String="originalPreferred"):Object
		{
			trace("WebClipboard::getData: format="+format);
			
			if (_event && _event.type == Event.PASTE) {
				if (format != TEXT_SCRAP)
					return _clipboard.getData(format, transferMode);
				else
					return TextClipboard.getContents();
			} else {
				if (_format == format)
					return _data;
				else
					return null;
			}
		}
		
		public function setData(format:String, data:Object, serializable:Boolean=true):Boolean
		{
			trace("WebClipboard::setData: format="+format);
			
			// also add this to Flash object in case we are met with an unauthorized paste request not from Event.PASTE.
			_data = null;
			_format = format;
			_data = data;
			
			if (_event) {
				_clipboard.clear();
				
				if (format != TEXT_SCRAP)
					return _clipboard.setData(format, data, serializable);
				else {
					TextClipboard.setContents(data as TextScrap);
					return true;
				}
			} else {
				return true;
			}
		}
		
		
		public function clear():void
		{
			trace("WebClipboard::clear");
			_format = "";
			_data = "";
		}

		
		public function acceptDragDrop(target:InteractiveObject):void
		{
			// nothing to do, yet
		}
		
		public function doDrag(dragInitiator:InteractiveObject, dragImage:BitmapData=null, offset:Point=null):void
		{
			// nothing to do yet
		}
		
		public function get BITMAP_FORMAT():String
		{
			return "unsupported";
		}
		
		public function get FILE_LIST_FORMAT():String
		{
			return "unsupported";
		}
		
		public function get HTML_FORMAT():String
		{
			return ClipboardFormats.HTML_FORMAT;
		}
		
		public function get RICH_TEXT_FORMAT():String
		{
			return ClipboardFormats.RICH_TEXT_FORMAT;
		}
		
		public function get TEXT_FORMAT():String
		{
			return ClipboardFormats.TEXT_FORMAT;
		}
		
		public function get TEXT_SCRAP():String
		{
			return "textScrap";
		}
		
		public function get URL_FORMAT():String
		{
			return "unsupported";
		}
		
		public function get OBJECTS_FORMAT():String
		{
			return "ZebraObjectsFormat";
		}	
		
		public function get DRAG_ENTER():String
		{
			return "dragEnter";
		}
		
		public function get DRAG_OVER():String
		{
			return "dragOver";
		}
		
		public function get DRAG_DROP():String
		{
			return "dragDrop";
		}
		
		public function get DRAG_COMPLETE():String
		{
			return "dragComplete";
		}
	}
}