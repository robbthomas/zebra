﻿package com.codeazur.as3swf.data.consts
{
	public class SoundRate
	{
		public static const KHZ_5:uint = 0;
		public static const KHZ_8:uint = 1;
		public static const KHZ_11:uint = 2;
		public static const KHZ_16:uint = 3;
		public static const KHZ_22:uint = 4;
		public static const KHZ_32:uint = 5;
		public static const KHZ_44:uint = 6;
		public static const KHZ_48:uint = 7;
		public static const KHZ_96:uint = 8;
		
		public static function toString(soundRate:uint):String {
			switch(soundRate) {
				case KHZ_5: return "5.5kHz"; break;
				case KHZ_8: return "8kHz"; break;
				case KHZ_11: return "11kHz"; break;
				case KHZ_16: return "16kHz"; break;
				case KHZ_22: return "22kHz"; break;
				case KHZ_32: return "32kHz"; break;
				case KHZ_44: return "44kHz"; break;
				case KHZ_48: return "48kHz"; break;
				case KHZ_96: return "98kHz"; break;
				default: return "unknown"; break;
			}
		}
	}
}
