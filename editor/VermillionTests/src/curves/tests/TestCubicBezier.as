package curves.tests {
import com.alleni.author.util.BezierSegment;

import com.alleni.author.util.CurveGenerator;

import com.alleni.taconite.lang.Geometry;

import flash.geom.Point;

import org.flexunit.Assert;

public class TestCubicBezier {

	[Test(description="length and distance of segments")]
	public function segmentLength() {
		trace("segmentLength");
		var p1:Point = new Point(595.5, -38.5);
		var c1:Point = new Point(604.6412259095104, 55.98550864091115);
		var c2:Point = new Point(-1.599354372714415, -9.167306709344672);
		var p2:Point = new Point(-3, 96);

		var b:BezierSegment = new BezierSegment(p1, c1, c2, p2);

		var length:Number = CurveGenerator.cubicLength(p1, c1, c2, p2);
		trace(length);
		for(var target:Number =.01;target<1.01;target+=0.01) {
			var pt = b.getValue(target);
			var t:Number;
			var len:Number = 0;
			var resultLen:Number = 0;
			var pt1:Point = p1;
			var pt2:Point;
			var resultPt:Point = p1;
			var distance:Number = p1.subtract(pt).length;
			var d:Number;
			for (t =.01;t<1.01;t+=0.01){
				pt2 = b.getValue(t);	// x,y on the curve for a given t
				d = pt2.subtract(pt).length;
				len += pt2.subtract(pt1).length;
				if(d < distance) {
					distance = d;
					resultPt = pt;
					resultLen = len;
				}
				pt1 = pt2;
			}
			trace("len " + resultLen, "percent " + resultLen/length);
			Assert.assertTrue("len " + resultLen, resultLen < length + .005);
			var len:Number = resultLen;
			var pt1:Point = p1;
			for (t =.01;t<1.01;t+=0.01){
				var pt2:Point= b.getValue(t);
				//trace("current " + len + "  next " + pt2.subtract(pt1).length);
				len -= pt2.subtract(pt1).length;
				if(len < .005) {
					trace("cubic length along t=", t, "len left", len);
					Assert.assertTrue("t=" + t + " target=" + target, Math.abs(t - target) <= .01);
					break;
				}
				pt1 = pt2;
			}
		}
	}

	[Test(description="straight")]
	public function straightLength() {
		trace("straightLength");
		var p1:Point = new Point(595.5, -38.5);
		var c1:Point = new Point(604.6412259095104, 55.98550864091115);
		var p2:Point = c1.subtract(p1);
		p2.normalize(500);
		p2 = p2.add(p1);

		var offLine:Number = Geometry.closestPointOnLineSegment(p1, p2, c1).subtract(c1).length;
		trace(offLine);
		trace(CurveGenerator.quadraticLength(p1,c1,p2));
	}
}
}