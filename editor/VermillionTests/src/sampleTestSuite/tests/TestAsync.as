package sampleTestSuite.tests
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.flexunit.Assert;
	import org.flexunit.async.Async;

	public class TestAsync
	{
		private var _timer:Timer;
		
		public function TestAsync()
		{
		}
		
		[Before]
		public function setup():void{
			_timer = new Timer(100, 1);
		}
		
		[Ignore]
		[Test(async, description="Async Example")]
		public function timerLongWay():void{
			var asyncHandler:Function = Async.asyncHandler( this, handleTimerComplete, 500, null, handleTimeout );
			_timer.addEventListener(TimerEvent.TIMER_COMPLETE, asyncHandler, false, 0, true );
			_timer.start();
		}
		
		protected function handleTimerComplete( event:TimerEvent, passThroughData:Object ):void{
			
		}
		
		protected function handleTimeout( passThroughData:Object ):void{
			Assert.fail("Timeout reached before event");
		}
		
		[After]
		public function tearDown():void{
			if (_timer){
				_timer.stop();
			}
			
			_timer = null;
		}
	}
}