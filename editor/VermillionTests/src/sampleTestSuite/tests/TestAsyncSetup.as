package sampleTestSuite.tests
{
	import flash.events.Event;
	
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	
	import org.flexunit.Assert;
	import org.flexunit.async.Async;
	import org.fluint.uiImpersonation.UIImpersonator;

	public class TestAsyncSetup
	{
		private var textInput:TextInput;
		
		public function TestAsyncSetup()
		{
		}
		
		[Before( async, ui )]
		public function setUp():void{
			textInput = new TextInput();
			Async.proceedOnEvent(this, textInput, FlexEvent.CREATION_COMPLETE, 1000 );
			UIImpersonator.addChild( textInput );
		}
		
		[Ignore]
		[Test( async )]
		public function testSetTextProperty():void{
			var passThroughData:Object = new Object();
			passThroughData.propertyName = "text";
			passThroughData.propertyValue = "digitalprimates";
			textInput.addEventListener( FlexEvent.VALUE_COMMIT,
				Async.asyncHandler(this, handleVerifyProperty, 100, passThroughData, handleEventNeverOccured ),
				false, 0, true);
			textInput.text = passThroughData.propertyValue;
		}
		
		public function handleVerifyProperty( event:Event, passThroughData:Object ):void{
			Assert.assertEquals( event.target[ passThroughData.propertyName ], passThroughData.propertyValue );
		}
		
		public function handleEventNeverOccured( passThroughData:Object):void{
			Assert.fail("Pending Event Never Occurred");
		}
		
		[After( ui )]
		public function tearDown():void{
			UIImpersonator.removeAllChildren();
			
			textInput = null;
		}
	}
}