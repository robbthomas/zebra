package sampleTestSuite.tests
{
	import org.flexunit.Assert;

	public class TestCase1
	{
		public function TestCase1(){	
		}
		
		[Ignore]
		[Test( description = "This tests addition" )]
		public function simpleAdd():void{
			var x:int = 5 + 3;
			Assert.assertEquals( 8, x );
		}
	}
}