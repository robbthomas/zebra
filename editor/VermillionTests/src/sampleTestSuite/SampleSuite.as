package sampleTestSuite
{
	import sampleTestSuite.tests.TestAsync;
	import sampleTestSuite.tests.TestAsyncSetup;
	import sampleTestSuite.tests.TestCase1;
	
	[Suite]
	[RunWith("org.flexunit.runners.Suite")]
	public class SampleSuite
	{
		public var test1:TestCase1;
		public var test2:TestAsync;
		public var test3:TestAsyncSetup;
	}
}