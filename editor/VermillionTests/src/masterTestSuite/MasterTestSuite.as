package masterTestSuite
{
	import controlsTestSuite.ControlsTestSuite;
	
	import expression.ExpressionTestSuite;
	
	import messageCenterPosition.MessageCenterPositionTest;
	
	[Suite]
	[RunWith("org.flexunit.runners.Suite")]
	public class MasterTestSuite
	{
		public var controlsTest:ControlsTestSuite;
		public var expressionTest:ExpressionTestSuite;
		public var messageCenterPosition:MessageCenterPositionTest;
	}
}