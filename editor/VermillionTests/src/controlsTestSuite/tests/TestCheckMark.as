package controlsTestSuite.tests
{
	import com.alleni.author.view.ui.controls.CheckMark;
	
	import flash.events.Event;
	
	import mx.containers.Canvas;
	
	import org.flexunit.Assert;
	import org.flexunit.async.Async;

	public class TestCheckMark
	{
		private var _checkMark:CheckMark
		private var _canvas:Canvas = new Canvas();
		
		public function TestCheckMark(){
		}
		
		[Before(async)]
		public function setup():void{
			_checkMark = new CheckMark();
		}
		
		[Test]
		public function testCheckMark():void{
			Assert.assertNotNull(_checkMark);
		}
		
		[After]
		public function tearDown():void{
			_checkMark = null;
		}
	}
}