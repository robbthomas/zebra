package controlsTestSuite.tests
{
	import com.alleni.author.view.ui.controls.Button;
	
	import flash.display.Loader;
	import flash.events.Event;
	
	import mx.containers.Canvas;
	
	import org.flexunit.Assert;
	import org.flexunit.async.Async;
	
	public class TestButton
	{
		private static var _button:Button;
		
		public function TestButton()
		{
		}
			
		[BeforeClass]
		public static function setup():void{
			_button = new Button();
			_button.id = "toolBoxButton";
			_button.skin_unsel_down_source = "assets/appicons/icons/tb-unsel-down.png";
			_button.skin_unsel_roll_source = "assets/appicons/icons/tb-unsel-roll.png";
			_button.skin_unsel_up_source = "assets/appicons/icons/tb-unsel-up.png";
			_button.skin_sel_down_source = "assets/appicons/icons/tb-sel-down.png";
			_button.skin_sel_roll_source = "assets/appicons/icons/tb-sel-roll.png";
			_button.skin_sel_up_source = "assets/appicons/icons/tb-sel-up.png"
			_button.buildButton(true, true);
			_button.buttonMode = true;
			_button.toolTip = "Tool Box";
		}
		
		[Test]
		public function testButtonProperties():void{
			Assert.assertEquals(_button.id, "toolBoxButton");
			Assert.assertEquals(_button.toolTip, "Tool Box");
			Assert.assertEquals(_button.selected, true);
		}
		
		[Test(async, order=1)]
		public function testButtonLoaders():void{
			_button.addEventListener( Button.BUTTON_ASSETS_LOADED,
				Async.asyncHandler(this, buttonAssetsLoaded, 750, null, buttonAssetsNotLoaded ),
				false, 0, true);
		}
		
		[Test(async, order=2)]
		public function testButtonRollover():void{
			_button.addEventListener( Button.ZEBRA_BUTTON_OVER,
				Async.asyncHandler(this, buttonRolloverSuccess, 500, null, buttonOverFailed ),
				false, 0, true);
			_button.buttonRollover(null);
		}
		
		[Test(async, order=3)]
		public function testButtonMouseDown():void{
			_button.addEventListener( Button.ZEBRA_BUTTON_DOWN,
				Async.asyncHandler(this, buttonDownSuccess, 500, null, buttonDownFailed ),
				false, 0, true);
			_button.buttonMouseDown(null);
		}
		
		[Test(async, order=4)]
		public function testButtonRollout():void{
			_button.addEventListener( Button.ZEBRA_BUTTON_OUT,
				Async.asyncHandler(this, buttonRolloutSuccess, 500, null, buttonOutFailed ),
				false, 0, true);
			_button.buttonRollout(null);
		}
		
		[Test(async, order=10)]
		public function testButtonClick():void{
			_button.selected = false;
			_button.addEventListener( Button.ZEBRA_BUTTON_CLICKED,
				Async.asyncHandler(this, buttonClickedSuccess, 500, null, buttonClickFailed ),
				false, 0, true);
			_button.buttonClicked(null);
		}
		
		[Test(async, order=11)]
		public function testUnselectedRollover():void{
			_button.addEventListener( Button.ZEBRA_BUTTON_OVER,
				Async.asyncHandler(this, buttonUnselectedRolloverSuccess, 500, null, buttonOverFailed ),
				false, 0, true);
			_button.buttonRollover(null);
		}
		
		[Test(async, order=13)]
		public function testUnselectedRollout():void{
			_button.addEventListener( Button.ZEBRA_BUTTON_OUT,
				Async.asyncHandler(this, buttonUnselectedRolloutSuccess, 500, null, buttonOutFailed ),
				false, 0, true);
			_button.buttonRollout(null);
		}
		
		[Test(async, order=12)]
		public function testUnselectedMouseDown():void{
			_button.addEventListener( Button.ZEBRA_BUTTON_DOWN,
				Async.asyncHandler(this, buttonUnselectedDownSuccess, 500, null, buttonDownFailed ),
				false, 0, true);
			_button.buttonMouseDown(null);
		}
		
		protected function buttonAssetsLoaded(event:Event, data:Object):void{
			var fileNameIndex:int = ((_button.getChildAt(0) as Loader).contentLoaderInfo.url.lastIndexOf("/"))+1
			var fileName:String = (_button.getChildAt(0) as Loader).contentLoaderInfo.url.substring(fileNameIndex);
			Assert.assertEquals("tb-sel-up.png", fileName);
		}
		
		protected function buttonRolloutSuccess(event:Event, data:Object):void{
			var fileNameIndex:int = ((_button.getChildAt(0) as Loader).contentLoaderInfo.url.lastIndexOf("/"))+1
			var fileName:String = (_button.getChildAt(0) as Loader).contentLoaderInfo.url.substring(fileNameIndex);
			Assert.assertEquals("tb-sel-up.png", fileName);
		}
		
		protected function buttonRolloverSuccess(event:Event, data:Object):void{
			var fileNameIndex:int = ((_button.getChildAt(0) as Loader).contentLoaderInfo.url.lastIndexOf("/"))+1
			var fileName:String = (_button.getChildAt(0) as Loader).contentLoaderInfo.url.substring(fileNameIndex);
			Assert.assertEquals("tb-sel-roll.png", fileName);
		}
		
		protected function buttonDownSuccess(event:Event, data:Object):void{
			var fileNameIndex:int = ((_button.getChildAt(0) as Loader).contentLoaderInfo.url.lastIndexOf("/"))+1
			var fileName:String = (_button.getChildAt(0) as Loader).contentLoaderInfo.url.substring(fileNameIndex);
			Assert.assertEquals("tb-sel-down.png", fileName);
		}
		
		protected function buttonClickedSuccess(event:Event, data:Object):void{
			var fileNameIndex:int = ((_button.getChildAt(0) as Loader).contentLoaderInfo.url.lastIndexOf("/"))+1
			var fileName:String = (_button.getChildAt(0) as Loader).contentLoaderInfo.url.substring(fileNameIndex);
			Assert.assertEquals("tb-unsel-up.png", fileName);
		}
		
		protected function buttonUnselectedRolloverSuccess(event:Event, data:Object):void{
			var fileNameIndex:int = ((_button.getChildAt(0) as Loader).contentLoaderInfo.url.lastIndexOf("/"))+1
			var fileName:String = (_button.getChildAt(0) as Loader).contentLoaderInfo.url.substring(fileNameIndex);
			Assert.assertEquals("tb-unsel-roll.png", fileName);
		}
		
		protected function buttonUnselectedRolloutSuccess(event:Event, data:Object):void{
			var fileNameIndex:int = ((_button.getChildAt(0) as Loader).contentLoaderInfo.url.lastIndexOf("/"))+1
			var fileName:String = (_button.getChildAt(0) as Loader).contentLoaderInfo.url.substring(fileNameIndex);
			Assert.assertEquals("tb-unsel-up.png", fileName);
		}
		
		protected function buttonUnselectedDownSuccess(event:Event, data:Object):void{
			var fileNameIndex:int = ((_button.getChildAt(0) as Loader).contentLoaderInfo.url.lastIndexOf("/"))+1
			var fileName:String = (_button.getChildAt(0) as Loader).contentLoaderInfo.url.substring(fileNameIndex);
			Assert.assertEquals("tb-unsel-down.png", fileName);
		}
		
		protected function buttonClickFailed(data:Object):void{
			Assert.fail("Button Click method failed to fire event");
		}
		
		protected function buttonOverFailed(data:Object):void{
			Assert.fail("Button Over method failed to fire event");
		}
		
		protected function buttonOutFailed(data:Object):void{
			Assert.fail("Button Out method failed to fire event");
		}
		
		protected function buttonDownFailed(data:Object):void{
			Assert.fail("Button Down method failed to fire event");
		}
		
		protected function buttonAssetsNotLoaded(data:Object):void{
			Assert.fail("Button Assets were not loaded within 1000ms");
		}
		
		[AfterClass]
		public static function tearDown():void{
			_button = null;
		}
		
	}
}