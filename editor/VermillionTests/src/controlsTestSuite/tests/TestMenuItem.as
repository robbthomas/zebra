package controlsTestSuite.tests
{
	import com.alleni.author.view.ui.controls.MenuItem;
	import com.alleni.author.view.ui.controls.PopOutMenu;
	
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	
	import org.flexunit.Assert;

	public class TestMenuItem
	{
		private static var _tempLabelMenuItem:MenuItem;
		
		[BeforeClass]
		public static function setup():void{
			_tempLabelMenuItem = buildMenuLabelItem();
		}
		
		[Test]
		public function testLabelMenuItem():void{
			Assert.assertNotNull(_tempLabelMenuItem);
		}
		
		[Test]
		public function testProperties():void{
			Assert.assertEquals(MenuItem.TYPE_LABEL_ONLY, _tempLabelMenuItem.type);
			Assert.assertEquals("Test Menu Item", _tempLabelMenuItem.label);
			Assert.assertEquals("testData", _tempLabelMenuItem.data);
		}
		
		[Test]
		public function testSelected():void{
			Assert.assertEquals(true, _tempLabelMenuItem.selected);
			_tempLabelMenuItem.selected = false;
			Assert.assertEquals(false, _tempLabelMenuItem.selected);
		}
		
		[AfterClass]
		public static function tearDown():void{
			_tempLabelMenuItem = null
		}
		
		// OBJECT CREATION METHODS
		private static function buildMenuLabelItem():MenuItem{
			var tempMenuItem:MenuItem = new MenuItem();
			tempMenuItem = new MenuItem();
			tempMenuItem.type = MenuItem.TYPE_LABEL_ONLY;
			tempMenuItem.label ="Test Menu Item";
			tempMenuItem.labelSize = PopOutMenu.LABEL_SIZE_LARGE;
			tempMenuItem.data = "testData";
			tempMenuItem.response = MenuItem.RESPONSE_SELECTABLE;
			tempMenuItem.menuWidth = 100;
			tempMenuItem.checkMarkScale = 1;
			tempMenuItem.buildMenuItem();
			tempMenuItem.selected = true;
			
			return tempMenuItem;
		}
	}
}