package controlsTestSuite.tests
{
	import com.alleni.author.view.ui.controlArea.ControlAreaToolTip;
	
	import org.flexunit.Assert;

	public class TestToolTip
	{
		public static var _toolTip:ControlAreaToolTip;
		
		public function TestToolTip()
		{
		}
		
		[BeforeClass]
		public static function setup():void{
			_toolTip = new ControlAreaToolTip();
			_toolTip.displayDirection = ControlAreaToolTip.DISPLAY_BELOW;
			_toolTip.arrowDisplay = ControlAreaToolTip.ARROW_SHOW;
			_toolTip.labelString = "tool tip test";
		}
		
		[Test]
		public function testToolTipCreation():void{
			Assert.assertNotNull(_toolTip);
		}
		
		[Test]
		public function testToolTipProperties():void{
			Assert.assertEquals(_toolTip.displayDirection, ControlAreaToolTip.DISPLAY_BELOW);
			Assert.assertEquals(_toolTip.arrowDisplay, ControlAreaToolTip.ARROW_SHOW);
			Assert.assertEquals(_toolTip.labelString, "tool tip test");
		}
		
		[AfterClass]
		public static function tearDown():void{
			_toolTip = null;
		}
	}
}