package controlsTestSuite
{
	import controlsTestSuite.tests.TestButton;
	import controlsTestSuite.tests.TestCheckMark;
	import controlsTestSuite.tests.TestMenuItem;
	import controlsTestSuite.tests.TestToolTip;

	[Suite]
	[RunWith("org.flexunit.runners.Suite")]
	public class ControlsTestSuite
	{
		public var buttonTestCase:TestButton;
		public var checkMarkTestCase:TestCheckMark;
		public var toolTipTest:TestToolTip;
		public var menuItemTest:TestMenuItem;
	}
}