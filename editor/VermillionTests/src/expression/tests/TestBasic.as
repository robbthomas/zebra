package expression.tests
{

import com.alleni.savana.Ast;

import org.flexunit.Assert;
	import org.flexunit.assertThat;
	import org.hamcrest.core.anyOf;
	import org.hamcrest.number.closeTo;
	import org.hamcrest.object.equalTo;
	
	public class TestBasic
	{
		public static var operators:Object =  {
			"+":function(lhs:Number, rhs:Number):Number { return lhs + rhs; },
			"-":function(lhs:Number, rhs:Number):Number { return lhs - rhs; },
			"*":function(lhs:Number, rhs:Number):Number { return lhs * rhs; },
			"/":function(lhs:Number, rhs:Number):Number { return lhs / rhs; }
		};
		
		public function TestBasic(){	
		}
		
		[Test(description="test lexing and parsing single digits")]
		public function testDigits():void {
			for(var i:Number=0; i<=9; i++) {
				var ast:Ast = new Ast(new String(i));
				Assert.assertEquals(i, ast.evaluate({}));
			}
		}
		
		[Test(description="test lexing and parsing floating point")]
		public function testFloatingPoint():void {
			for each(var num:String in ["2.5", "0.5", ".5", "2e3", "2E3", ".1e-3", ".1e+10"]) {
				var ast:Ast = new Ast(num);
				Assert.assertEquals(num, Number(num), ast.evaluate({}));
			}
		}
		
		[Test(description="test single digit addition")]
		public function testDigitAdd():void {
			for(var lhs:Number=0; lhs<=9; lhs++) {
				for(var rhs:Number=0; rhs<=9; rhs++) {
					var ast:Ast = new Ast(new String(lhs) + "+" + new String(rhs));
					Assert.assertEquals(lhs + rhs, ast.evaluate({}));
				}
			}
		}
		
		[Test(description="test single digit operators")]
		public function testDigitOp():void {
			for(var lhs:Number=0; lhs<=9; lhs++) {
				for(var rhs:Number=0; rhs<=9; rhs++) {
					for(var op:String in operators) {
						var ast:Ast = new Ast(new String(lhs) + op + new String(rhs));
						var result:Object = ast.evaluate({});
						Assert.assertTrue(result is Number);
						var expected:Number = (operators[op] as Function).call(this, lhs, rhs);
						var description:String = "LHS " + lhs + " RHS " + rhs + " OP " + op;
						assertThat(description, result, anyOf(equalTo(expected),closeTo(expected, 0.0001)));
					}
				}
			}
		}
		
//		[Ignore("Works well, but a bit slow")]
//		[Test(description="test multi digit operators")]
//		public function testMultiDigitOp():void {
//			for(var lhs:Number=0; lhs<=99; lhs++) {
//				for(var rhs:Number=0; rhs<=99; rhs++) {
//					for(var op:String in operators) {
//						var ast:Ast = new Ast(new String(lhs) + op + new String(rhs));
//						var result:Object = ast.evaluate({});
//						Assert.assertTrue(result is Number);
//						var expected:Number = (operators[op] as Function).call(this, lhs, rhs);
//						var description:String = "LHS " + lhs + " RHS " + rhs + " OP " + op;
//						assertThat(description, result, anyOf(equalTo(expected),closeTo(expected, 0.0001)));
//					}
//				}
//			}
//		}
	}
}