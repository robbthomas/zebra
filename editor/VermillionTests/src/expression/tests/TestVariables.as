/**
 * Created by IntelliJ IDEA.
 * User: jessecoyle
 * Date: 12/6/11
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
package expression.tests {
import com.alleni.savana.Ast;

import mx.utils.ObjectUtil;

import org.hamcrest.assertThat;
import org.hamcrest.collection.array;
import org.hamcrest.collection.arrayWithSize;
import org.hamcrest.collection.hasItems;
import org.hamcrest.core.allOf;
import org.hamcrest.core.isA;
import org.hamcrest.number.between;
import org.hamcrest.object.equalTo;
import org.hamcrest.object.hasProperties;

public class TestVariables {
    [Test(description="trivial test")]
    public function trivial():void {
        var ast:Ast = new Ast("0");
        var vars:Array;

        assertThat("correct value", ast.evaluate({}), equalTo(0));
        assertThat("correct value", ast.evaluate({a:2}), equalTo(0));

        vars = ast.variables({a:2});
        assertThat("no variables", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("no variables", vars, arrayWithSize(0));
    }


    [Test(description="variables can be used")]
    public function simple():void {
        var ast:Ast = new Ast("a");
        var vars:Array;

        assertThat("correct value", ast.evaluate({a:2}), equalTo(2));
        assertThat("correct value", ast.evaluate({a:3}), equalTo(3));

        vars = ast.variables({a:2});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(1));
        assertThat("correct undefined variable", vars, hasItems("a"));
    }

    [Test(description="binary operators")]
    public function binary():void {
        var ast:Ast = new Ast("a = 2");
        var vars:Array;

        assertThat("correct value", ast.evaluate({a:2}), equalTo(true));
        assertThat("correct value", ast.evaluate({a:3}), equalTo(false));

        vars = ast.variables({a:2});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(1));
        assertThat("correct undefined variable", vars, hasItems("a"));
    }

    [Test(description="chained binary operators")]
    public function chainedBinary():void {
        var ast:Ast = new Ast("a + 2 + b + c");
        var vars:Array;

        assertThat("correct value", ast.evaluate({a:2, b:3, c:4}), equalTo(11));

        vars = ast.variables({a:2, b:3, c:4});
        assertThat("defined variables", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variables", vars, arrayWithSize(3));
        assertThat("correct undefined variables", vars, hasItems("a", "b", "c"));

        vars = ast.variables({b:3});
        assertThat("partially undefined variables", vars, arrayWithSize(2));
        assertThat("correct partially undefined variables", vars, hasItems("a", "c"));
    }

    [Test(description="unary operators")]
    public function unary():void {
        var ast:Ast = new Ast("-a");
        var vars:Array;

        assertThat("correct value", ast.evaluate({a:2}), equalTo(-2));

        vars = ast.variables({a:2});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(1));
        assertThat("correct undefined variable", vars, hasItems("a"));
    }

    [Test(description="ternary operators")]
    public function ternary():void {
        var ast:Ast = new Ast("a?b:c");
        var vars:Array;

        assertThat("correct value", ast.evaluate({a:true, b:2, c:3}), equalTo(2));

        vars = ast.variables({a:true, b:2, c:3});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(3));
        assertThat("correct undefined variable", vars, hasItems("a", "b", "c"));
    }

    [Test(description="function call")]
    public function functionCall():void {
        var ast:Ast = new Ast("f(a, b)");
        var toBase:Function = function(num, base) {
                return Number(num).toString(base);
            };
        var vars:Array;

        assertThat("correct value", ast.evaluate({f:toBase,a:255, b:16}), equalTo("ff"));

        vars = ast.variables({f:toBase,a:255, b:16});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(3));
        assertThat("correct undefined variable", vars, hasItems("f", "a", "b"));
    }

    [Test(description="array literal")]
    public function arrayLiteral():void {
        var ast:Ast = new Ast("[a, b, c]");
        var vars:Array;

        assertThat("correct value", ast.evaluate({a:255, b:16, c:42}), array(equalTo(255), equalTo(16), equalTo(42)));

        vars = ast.variables({a:255, b:16, c:42});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(3));
        assertThat("correct undefined variable", vars, hasItems("a", "b", "c"));
    }

    [Test(description="subscript access")]
    public function subscriptAccess():void {
        var ast:Ast = new Ast("[2,3][a]");
        var vars:Array;

        assertThat("correct value", ast.evaluate({a:1}), equalTo(3));

        vars = ast.variables({a:1});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(1));
        assertThat("correct undefined variable", vars, hasItems("a"));
    }

    [Test(description="member access")]
    public function memberAccess():void {
        var ast:Ast = new Ast("a.b");
        var vars:Array;

        assertThat("correct value", ast.evaluate({a:{b:2}}), equalTo(2));

        vars = ast.variables({a:{b:2}});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(1));
        assertThat("correct undefined variable", vars, hasItems("a"));
    }

    [Test(description="function literal")]
    public function functionLiteral():void {
        var ast:Ast = new Ast("function(a) a + b");
        var vars:Array;

        assertThat("correct value", ast.evaluate({b:3}).apply(null, [2]), equalTo(5));

        vars = ast.variables({b:3});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(1));
        assertThat("correct undefined variable", vars, hasItems("b"));
    }

    [Test(description="let scope")]
    public function letScope():void {
        var ast:Ast = new Ast("let(a:2) a + b");
        var vars:Array;

        assertThat("correct value", ast.evaluate({b:3}), equalTo(5));

        vars = ast.variables({b:3});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(1));
        assertThat("correct undefined variable", vars, hasItems("b"));
    }

    [Test(description="objectLiteral")]
    public function objectLiteral():void {
        var ast:Ast = new Ast("{a:b, c:d}");
        var vars:Array;

        assertThat("correct value", ast.evaluate({b:3, d:2}),
                hasProperties({
                    a: equalTo(3),
                    c: equalTo(2)
                }));

        vars = ast.variables({b:3, d:2});
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables({});
        assertThat("undefined variable", vars, arrayWithSize(2));
        assertThat("correct undefined variable", vars, hasItems("b", "d"));
    }

    [Test(description="interesting")]
    public function interesting():void {
        var ast:Ast = new Ast("let(cc:frombase(b, 16), x:a/255) rgb(red(cc)*x, green(cc)*x, blue(cc)*x)");
        var vars:Array;

        var basicEnv:Function = function():Object { return {
        rgb:function(r, g, b) {
                r = Math.max(0, Math.min(255, Number(r)));
                g = Math.max(0, Math.min(255, Number(g)));
                b = Math.max(0, Math.min(255, Number(b)));
                return (r << 16) + (g << 8) + b;
            },
        red:function(c) {
                return Number(c) >> 16 & 0xFF;
            },
        green:function(c) {
                return Number(c) >> 8 & 0xFF;
            },
        blue:function(c) {
                return Number(c) & 0xFF;
            },
        tobase:function(num, base) {
                return Number(num).toString(base);
            },
        frombase:function(num, base) {
                return parseInt(String(num),base);
            }
        };};

        var emptyEnv:Object = basicEnv();
        var expandedEnv:Object = basicEnv();
        expandedEnv.a = 128;
        expandedEnv.b = "2200ff";

        var evaluated:Object = ast.evaluate(expandedEnv);
        assertThat("sanity check", evaluated, allOf(isA(Number), between(0, Math.pow(2, 24))));
        assertThat("correct value", Number(evaluated).toString(16), equalTo("110080"));

        vars = ast.variables(expandedEnv);
        assertThat("defined variable", vars, arrayWithSize(0));

        vars = ast.variables(emptyEnv);
        assertThat("undefined variable", vars, arrayWithSize(2));
        assertThat("correct undefined variable", vars, hasItems("a", "b"));
    }


}
}
