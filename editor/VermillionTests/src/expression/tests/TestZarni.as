/**
 * Created with IntelliJ IDEA.
 * User: AI
 * Date: 10/18/12
 * Time: 11:01 AM
 * To change this template use File | Settings | File Templates.
 */
package expression.tests {
import assets.icons.messageCenter.xyrWhiteTrans;
import assets.icons.variableLibrary.MathBlack;

import avmplus.typeXml;

import com.alleni.savana.Ast;
import com.alleni.savana.Lexer;

import flexunit.framework.Assert;

import org.flexunit.asserts.assertEquals;
import org.flexunit.asserts.assertTrue;

import org.hamcrest.assertThat;
import org.hamcrest.collection.arrayWithSize;
import org.hamcrest.core.anyOf;
import org.hamcrest.number.closeTo;
import org.hamcrest.object.equalTo;

public class TestZarni {
    public function TestZarni() {
    }
    [Test(description="test number concatenation")]
    public function testnumconcatenation():void {
        var ast:Ast = new Ast( "4 ^ 6" );
        var result:* = ast.evaluate({});
        var expected:* = 46;
        Assert.assertEquals(expected, result);
    }
    [Test(description="test character concatenation")]
    public function testnumcharconcatenation():void {
        var ast:Ast = new Ast( "a ^ b" );
        var result:* = ast.evaluate({a:"a", b:"b"});
        var expected:* = "ab";
        Assert.assertEquals(expected, result);
    }
    [Test(description="test less than")]
    public function testlessthan():void {
        var ast:Ast = new Ast( "4 < 6" );
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected, result);
    }
    [Test(description="test less than wv")]
    public function testlessthanwv():void{
        var ast:Ast = new Ast (" a < b ");
        var result:* =  ast.evaluate({a:3, b:4});
        var expected:* = true;
        Assert.assertEquals(expected, result);
    }
    [Test(description="test less than whv")]
    public function testlessthanwhv():void{
        var ast:Ast = new Ast (" a < 10");
        var result:* = ast.evaluate({a:5});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than decimals")]
    public function testlessthandecimals():void{
        var ast:Ast = new Ast (" 3.5 < 4.5");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than defined values")]
    public function testlessthandefinedvalues():void{
        var ast:Ast = new Ast ("e < pi");
        var result:* = ast.evaluate({e:Math.E,pi:Math.PI});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test greater than")]
    public function testgreaterthan():void{
        var ast:Ast = new Ast( "6 > 4" );
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected, result);
    }
    [Test(description="test greater than wv")]
    public function testgreaterthanwv():void{
        var ast:Ast = new Ast (" a > b ");
        var result:* =  ast.evaluate({a:4, b:3});
        var expected:* = true;
        Assert.assertEquals(expected, result);
    }
    [Test(description="test greater than whv")]
    public function testgreatthanwhv():void{
        var ast:Ast = new Ast (" a > 10");
        var result:* = ast.evaluate({a:15});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test greater than decimals")]
    public function testgreaterthandecimals():void{
        var ast:Ast = new Ast (" 4.5 > 3.5");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test greater than define values")]
    public function testgreaterthandefinedvalues():void{
        var ast:Ast = new Ast ("pi > e");
        var result:* = ast.evaluate({pi:Math.PI,e:Math.E});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test equal to")]
    public function testequalto():void{
        var ast:Ast = new Ast ("3 = 3");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test equal to")]
    public function testequaltowv():void{
        var ast:Ast = new Ast ("a = b");
        var result:* = ast.evaluate({a:2, b:2});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test equal to")]
    public function testequaltowhv():void{
        var ast:Ast = new Ast ("2 = b");
        var result:* = ast.evaluate({b:2});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to")]
    public function testlessthanequalto():void{
        var ast:Ast = new Ast ("2 <= 3");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to for equal")]
    public function testlessthanequaltoforequal():void{
        var ast:Ast = new Ast ("2 <= 2");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to wd")]
    public function testlessthanequaltowd():void{
        var ast:Ast = new Ast ("2.9999 <= 3");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to wd equal")]
    public function testlessthanequaltowdequalwdequal():void{
        var ast:Ast = new Ast ("2.99999 <= 2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to wd variations of space")]
    public function testlessthanequaltowdequalwvariations1():void{
        var ast:Ast = new Ast ("2.99999<= 2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to wd variations of space")]
    public function testlessthanequaltowdequalwvariations2():void{
        var ast:Ast = new Ast ("2.99999 <=2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }

    [Test(description="test less than equal to wd variations of space")]
    public function testlessthanequaltowdequalwvariations3():void{
        var ast:Ast = new Ast ("2.99999<=2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }

    [Test(description="test less than equal to wd variations of space")]
    public function testlessthanequaltowdequalwvariations4():void{
        var ast:Ast = new Ast ("2.99999  <=2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to wd variations of space")]
    public function testlessthanequaltowdequalwvariations5():void{
        var ast:Ast = new Ast ("2.99999<=  2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to wd variations of space")]
    public function testlessthanequaltowdequalwvariations6():void{
        var ast:Ast = new Ast ("2.99999  <=  2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }


    [Test(description="test greater than equal to wd equal")]
    public function testgreaterthanequaltowdequalwdequal():void{
        var ast:Ast = new Ast ("2.99999 >= 2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to wd variations of space")]
    public function testgreaterthanequaltowdequalwvariations1():void{
        var ast:Ast = new Ast ("2.99999>= 2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to wd variations of space")]
    public function testgreaterthanequaltowdequalwvariations2():void{
        var ast:Ast = new Ast ("2.99999 >=2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }

    [Test(description="test less than equal to wd variations of space")]
    public function testgreaterthanequaltowdequalwvariations3():void{
        var ast:Ast = new Ast ("2.99999>=2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }

    [Test(description="test less than equal to wd variations of space")]
    public function testgreaterthanequaltowdequalwvariations4():void{
        var ast:Ast = new Ast ("2.99999  >=2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to wd variations of space")]
    public function testgreaterthanequaltowdequalwvariations5():void{
        var ast:Ast = new Ast ("2.99999>=  2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test less than equal to wd variations of space")]
    public function testgreaterthanequaltowdequalwvariations6():void{
        var ast:Ast = new Ast ("2.99999  >=  2.99999");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }

    [Test(description="test not equal to with space variations")]
    public function notequaltows1():void{
        var ast:Ast = new Ast ("3.5<> 4");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test not equal to with space variations")]
    public function notequaltows2():void{
        for each(var exp:String in ["2.5 <>4", "2.5  <> 4"]) {
            var ast:Ast = new Ast (exp);
            var result:* = ast.evaluate({});
            var expected:* = true;
            Assert.assertEquals(expected,result);
        }
    }
    [Test(description="test not equal to with space variations")]
    public function notequaltows3():void{
        var ast:Ast = new Ast ("3.5  <> 4");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test not equal to with space variations")]
    public function notequaltows4():void{
        var ast:Ast = new Ast ("3.5  <>  4");
        var result:* = ast.evaluate({});
        var expected:* = true;
        Assert.assertEquals(expected,result);
    }
    [Test(description="test multiplication with space")]
    public function multiplicationws():void{
        var ast:Ast = new Ast ("2 * 3");
        var result:* = ast.evaluate({});
        var expected:* = 6;
        Assert.assertEquals(expected, result);
    }
    [Test(description="test divison with space")]
    public function divisionws():void{
        var ast:Ast = new Ast ("3 / 2");
        var result:* = ast.evaluate({});
        var expected:* = 1.5;
        Assert.assertEquals(expected, result);
    }
    [Test(description="test modulus with space")]
    public function modulusws():void{
        var ast:Ast = new Ast ("3 % 2");
        var result:* = ast.evaluate({});
        var expected:* = 1;
        Assert.assertEquals(expected, result);
    }
    [Test(description="test e")]
    public function testE():void {
        var ast:Ast = new Ast("1+e");
        var result:Object = ast.evaluate({e:Math.E});
        var expected:Number = 3.72;
        assertThat("1+e", result, anyOf(equalTo(expected),closeTo(expected, 0.01)));
    }
    [Test(description="test addition")]
    public function testaddition():void {
        var ast:Ast = new Ast( "4 + 6" );
        var result:* = ast.evaluate({});
        var expected:* = 10;
        Assert.assertEquals(expected, result);
    }
    [Test(description="test e for subtraction")]
    public function testEsub():void {
        var ast:Ast = new Ast("1-e");
        var result:Object = ast.evaluate({e:Math.E});
        var expected:Number = -1.72;
        assertThat("1+e", result, anyOf(equalTo(expected),closeTo(expected, 0.01)));
    }
    [Test(description="test & for boolean cases")]
    public function testbooamphersand():void {
        var ast:Ast = new Ast("a & b");
        var result:*= ast.evaluate({a:true, b:true});
        var expected:*= true;
        Assert.assertEquals(expected,  result);
    }
    [Test(description="test | for boolean cases")]
    public function testbooor():void {
        var ast:Ast = new Ast("a | b");
        var result:*= ast.evaluate({a:false, b:true});
        var expected:*= true;
        Assert.assertEquals(expected,  result);
    }
    [Test(description="test ~ for boolean cases")]
    public function testbootilda():void {
        var ast:Ast = new Ast("~a");
        var result:*= ast.evaluate({a:true});
        var expected:*= false;
        Assert.assertEquals(expected,  result);
    }
    [Test(description="test ^ for carrot cases")]
    public function testboocarrot():void {
        var ast:Ast = new Ast("a ^ b");
        var result:*= ast.evaluate({a:"zarni", b:"htet"});
        var expected:*= "zarnihtet";
        Assert.assertEquals(expected,  result);
    }
    [Test(description="test ** for power cases")]
    public function testpower():void {
        var ast:Ast = new Ast("a ** b");
        var result:*= ast.evaluate({a:2, b:3});
        var expected:*= 8;
        Assert.assertEquals(expected,  result);
    }
    [Test(description="test ** for conditional cases")]
    public function testconditional():void {
        var ast:Ast = new Ast("a?b:c");
        var result:*= ast.evaluate({a:true, b:3, c:5});
        var expected:*= 3;
        Assert.assertEquals(expected,  result);
    }
    [Test(description="test ** for order of operation cases with finger brackets")]
    public function testorderofoperation():void {
        var ast:Ast = new Ast("a + (b / c)");
        var result:*= ast.evaluate({a:2, b:25, c:5});
        var expected:*= 7;
        Assert.assertEquals(expected,  result);
    }
    [Test(description="test ** for more complex order of operation")]
    public function testorderofoperationmc():void {
        var ast:Ast = new Ast("[a + (b / c)] * d");
        var result:*= ast.evaluate({a:2, b:25, c:5, d:3});
        var expected:*= 21;
        Assert.assertEquals(expected,  result);
    }
    [Test(description="test ** for more complex order of operation")]
    public function testorderofoperationmcoo():void {
        var ast:Ast = new Ast("(a * b) - (c * d)");
        var result:*= ast.evaluate({a:2, b:25, c:5, d:3});
        var expected:*= 35;
        Assert.assertEquals(expected,  result);
    }

    [Test(description="test ** for more complex order of operation")]
    public function testorderofoperationmco():void {
        for each(var exp:String in ["[a + (b / c) * d] + e * (7 % 3) - (f?g:h)", "[a+(b / c)*d]+e*(7%3)-(f?g:h)","[  a + ( b /  c) * d] +  e * ( 7 %  3) - (f ? g : h)" ])
        var ast:Ast = new Ast(exp);
        var result:*= ast.evaluate({a:2, b:25, c:5, d:3, e:6,f:true, g:5, h:6});
        var expected:*= 18;
        Assert.assertEquals(expected,  result);
    }

    [Test(description="test for more complex n confusing calculation operations")]
    public function testmcncco1():void {
        var ast:Ast = new Ast("1+-2");
        var result:*= ast.evaluate({});
        var expected:*= -1;
        Assert.assertEquals(expected,  result);
    }

    [Test(description="test for more complex n confusing calculation operations")]
    public function testmcncco2():void {
        var ast:Ast = new Ast("-1+2");
        var result:*= ast.evaluate({});
        var expected:*= 1;
        Assert.assertEquals(expected,  result);
    }

    [Test(description="test for more complex n confusing calculation operations")]
    public function testmcncco3():void {
        var ast:Ast = new Ast("-1*-2");
        var result:*= ast.evaluate({});
        var expected:*= 2;
        Assert.assertEquals(expected,  result);
    }

    [Test(description="test for more complex n confusing calculation operations")]
    public function testmcncco4():void {
        var ast:Ast = new Ast("1/-2");
        var result:*= ast.evaluate({});
        var expected:*= -1/2;
        Assert.assertEquals(expected,  result);
    }

    [Test(description="test for more complex n confusing calculation operations")]
    public function testmcncco5():void {
        var ast:Ast = new Ast("-1/-2");
        var result:*= ast.evaluate({});
        var expected:*= 1/2;
        Assert.assertEquals(expected,  result);
    }

    [Test(description="test for more modulus")]
    public function testmodulus():void {
        var ast:Ast = new Ast("-7%2");
        var result:*= ast.evaluate({});
        var expected:*=-1;
        Assert.assertEquals(expected,  result);
    }

    [Test(description="test fail", expects="com.alleni.savana.PositionalError")]
    public function testFail():void {
       // for each (var exp:String in ["1 +-1", ""])
        var ast:Ast = new Ast("1 */ 2");
        ast.evaluate({});
        assertTrue(false);
    }

    [Test(description="test fail for brackes", expects="com.alleni.savana.PositionalError")]
    public function testFailb():void {
        for each (var exp:String in ["1 ( 2", "1 [( 2", "(1 + 2","[(1/2]", "1&|2","1&|&2", "true&|&true","true&&true", "true || false" ])
        var ast:Ast = new Ast(exp);
        ast.evaluate({});
        assertTrue(false);
    }

    [Test]
    public function testMultiOp():void {
        var tokens:Object;
        tokens = Lexer.lex(new String("1+-1"));
        assertThat(tokens, TestLexing.isTokens([
            ["number",new String("1")],
            ["operator",new String("+")],
            ["operator",new String("-")],
            ["number",new String("1")]
        ]));
        tokens = Lexer.lex(new String("1*/1"));
        assertThat(tokens, TestLexing.isTokens([
            ["number",new String("1")],
            ["operator",new String("*")],
            ["operator",new String("/")],
            ["number",new String("1")]
        ]));
        tokens = Lexer.lex(new String("1**1"));
        assertThat(tokens, TestLexing.isTokens([
            ["number",new String("1")],
            ["operator",new String("**")],
            ["number",new String("1")]
        ]));
        tokens = Lexer.lex(new String("1%*1"));
        assertThat(tokens, TestLexing.isTokens([
            ["number",new String("1")],
            ["operator",new String("%")],
            ["operator", new String ("*")],
            ["number",new String("1")]
        ]));
        tokens = Lexer.lex(new String("1^~1<>="));
        assertThat(tokens, TestLexing.isTokens([
            ["number",new String("1")],
            ["operator",new String("^")],
            ["operator",new String("~")],
            ["number", new String ("1")],
            ["operator",new String("<>")],
            ["operator",new String("=")]
        ]));
        tokens = Lexer.lex(new String("1<>1"));
        assertThat(tokens, TestLexing.isTokens([
            ["number",new String("1")],
            ["operator", new String ("<>")],
            ["number",new String("1")]
        ]));

        tokens = Lexer.lex(new String("|1>=1"));
        assertThat(tokens, TestLexing.isTokens([
            ["operator", new String ("|")],
            ["number",new String("1")],
            ["operator", new String (">=")],
            ["number",new String("1")]
        ]));

        tokens = Lexer.lex(new String("<="));
        assertThat(tokens, TestLexing.isTokens([
            ["operator", new String ("<=")],
        ]));


        tokens = Lexer.lex(new String("><=%<>>=:^[(}(]/-+=***"));
        assertThat(tokens, TestLexing.isTokens([
            ["operator", new String (">")],
            ["operator", new String ("<=")],
            ["operator", new String ("%")],
            ["operator", new String ("<>")],
            ["operator", new String (">=")],
            ["operator", new String (":")],
            ["operator", new String ("^")],
            ["operator", new String ("[")],
            ["operator", new String ("(")],
            ["operator", new String ("}")],
            ["operator", new String ("(")],
            ["operator", new String ("]")],
            ["operator", new String ("/")],
            ["operator", new String ("-")],
            ["operator", new String ("+")],
            ["operator", new String ("=")],
            ["operator", new String ("**")],
            ["operator", new String ("*")]
        ]));

    }




}
}
