/**
 * Created by IntelliJ IDEA.
 * User: jessecoyle
 * Date: 8/22/11
 * Time: 12:44 PM
 * To change this template use File | Settings | File Templates.
 */
package expression.tests {
import com.alleni.savana.Ast;

import org.flexunit.Assert;

public class TestObject {
    [Test(description="test simple objects")]
    public function testSimple():void {
        var ast:Ast = new Ast("{a:1, b:2}");
        var result:Object = ast.evaluate({});
        Assert.assertTrue("is Object", result is Object);
        Assert.assertTrue("has a", result.hasOwnProperty("a"));
        Assert.assertTrue("has b", result.hasOwnProperty("b"));
        Assert.assertEquals("a", 1, result["a"]);
        Assert.assertEquals("b", 2, result["b"]);
    }

    [Test(description="test property access")]
    public function testAccess():void {
        var ast:Ast = new Ast("{a:1, b:2}.a");
        Assert.assertEquals("a", 1, ast.evaluate({}));
    }

    [Ignore] // for now this is disabled as objects are rarely used to catch likely array errors
    [Test(description="test property access subscript")]
    public function testAccessSubscript():void {
        var ast:Ast = new Ast("{a:1, b:2}['a']");
        Assert.assertEquals("a", 1, ast.evaluate({}));
    }

    [Test(description="test let access")]
    public function testLet():void {
        var ast:Ast = new Ast("let(o : {a:1, b:2}) o.a");
        Assert.assertEquals("a", 1, ast.evaluate({}));
    }
}
}
