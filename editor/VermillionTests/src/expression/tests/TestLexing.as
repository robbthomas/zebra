package expression.tests
{

import com.alleni.savana.Lexer;

import org.flexunit.assertThat;
	import org.hamcrest.Matcher;
	import org.hamcrest.core.not;
	import org.hamcrest.object.equalTo;
	import org.hamcrest.object.hasProperties;
	import org.hamcrest.object.nullValue;

	public class TestLexing
	{
		public function TestLexing()
		{
		}
		
		public static function isTokens(tokens:Array):Matcher {
			var matchProps:Array = new Array();
			var pos:int = 0;
			// calculate positions and put names to the token array elements
			for each(var token:Object in tokens) {
				if(token[0] != "space") {
					matchProps.push({kind:String(token[0]), text:String(token[1]), position:pos});
				}
				pos += String(token[1]).length;
			}
			// init result to be the last token (eof)
			var result:Matcher = hasProperties({
				obj: hasProperties({
					kind: equalTo("eof"),
					text: equalTo("[EOF]"),
					position: equalTo(pos)
				}),
				next: nullValue()
			});
			// build result from the back forward each time putting the next element at the head of the list
			for(var i:int = matchProps.length-1; i>= 0; i--) {
				result = hasProperties({
					obj: hasProperties({
						kind: equalTo(matchProps[i].kind),
						text: equalTo(matchProps[i].text),
						position: equalTo(matchProps[i].position)
					}),
					next: result
				});
			}
			return result;
		}
		
		[Test]
		public function testDigit():void {
			for(var i:Number=0; i<10; i++) {
				var tokens:Object = Lexer.lex(new String(i));
				assertThat(tokens, isTokens([
					["number",new String(i)]
				]));
			}
		}
		
		[Test]
		public function testMultiDigit():void {
			for(var i:Number=0; i<1000; i++) {
				var tokens:Object = Lexer.lex(new String(i));
				assertThat(tokens, isTokens([
					["number",new String(i)]
				]));
			}
		}
		
		[Test]
		public function testSpacing():void {
			var tokens:Object = Lexer.lex(" 123  ");
			assertThat(tokens, isTokens([
				["space"," "],
				["number","123"],
				["space","  "]
			]));
			assertThat(tokens, not(isTokens([
				["space"," "],
				["number","123"],
				["space"," "]
			])));
		}
		
		[Test]
		public function testComplex():void {
			var tokens:Object = Lexer.lex(" 123 +4 ");
			assertThat(tokens, isTokens([
				["space"," "],
				["number","123"],
				["space"," "],
				["operator","+"],
				["number","4"],
				["space"," "]
			]));
			assertThat(tokens, not(isTokens([
				["space"," "],
				["number","123"],
				["space"," "],
				["operator","-"],
				["number","4"],
				["space"," "]
			])));
		}

        [Test]
        public function testMultiOp():void {
            var tokens:Object;
            tokens = Lexer.lex(new String("1+-1"));
            assertThat(tokens, isTokens([
                ["number",new String("1")],
                ["operator",new String("+")],
                ["operator",new String("-")],
                ["number",new String("1")]
            ]));
            tokens = Lexer.lex(new String("1*-1"));
            assertThat(tokens, isTokens([
                ["number",new String("1")],
                ["operator",new String("*")],
                ["operator",new String("-")],
                ["number",new String("1")]
            ]));
            tokens = Lexer.lex(new String("1**1"));
            assertThat(tokens, isTokens([
                ["number",new String("1")],
                ["operator",new String("**")],
                ["number",new String("1")]
            ]));
        }

        [Test]
        public function testKeywords():void {
            var tokens:Object;
            tokens = Lexer.lex(new String("and"));
            assertThat(tokens, isTokens([
                ["operator",new String("&")],
                ["space","  "] // space out for the length of the word and
            ]));
            tokens = Lexer.lex(new String("or"));
            assertThat(tokens, isTokens([
                ["operator",new String("|")],
                ["space"," "] // space out for the length of the word or
            ]));
            tokens = Lexer.lex(new String("not"));
            assertThat(tokens, isTokens([
                ["operator",new String("~")],
                ["space","  "] // space out for the length of the word not
            ]));
            tokens = Lexer.lex(new String("true"));
            assertThat(tokens, isTokens([
                ["boolean",new String("true")]
            ]));
            tokens = Lexer.lex(new String("false"));
            assertThat(tokens, isTokens([
                ["boolean",new String("false")]
            ]));
            tokens = Lexer.lex(new String("function"));
            assertThat(tokens, isTokens([
                ["operator",new String("function")]
            ]));
            tokens = Lexer.lex(new String("let"));
            assertThat(tokens, isTokens([
                ["operator",new String("let")]
            ]));
            tokens = Lexer.lex(new String("mod"));
            assertThat(tokens, isTokens([
                ["operator",new String("%")],
                ["space","  "] // space out for the length of the word mod
            ]));
            tokens = Lexer.lex(new String("and or not true false function let mod"));
            assertThat(tokens, isTokens([
                ["operator",new String("&")],
                ["space","   "], // space out for the length of the word and
                ["operator",new String("|")],
                ["space","  "], // space out for the length of the word or
                ["operator",new String("~")],
                ["space","   "],// space out for the length of the word not
                ["boolean",new String("true")],
                ["space"," "],
                ["boolean",new String("false")],
                ["space"," "],
                ["operator",new String("function")],
                ["space"," "],
                ["operator",new String("let")],
                ["space"," "],
                ["operator",new String("%")],
                ["space","  "] // space out for the length of the word mod
            ]));
        }
	}
}