package expression.tests
{
import com.alleni.savana.Ast;

import org.flexunit.Assert;

public class TestInteresting
	{
		public function TestInteresting()
		{
		}
		
		[Test]
		public function testCircleCircumference():void
		{
			var ast:Ast = new Ast("2*PI*radius");
			for each(var r:Number in [1,24,235,54,.05,1/2,56/3]) {
				Assert.assertEquals("2*PI*radius with radius=" + r, 2*Math.PI*r, ast.evaluate({pi:Math.PI, radius:r}));
			}
		}

		[Test]
		public function testSineWavePosition():void
		{
			var ast:Ast = new Ast("200 * sin(angle * PI/180) + 200");
			for(var a:Number=-180; a< 540; a++) {
				Assert.assertEquals("200 * sin(angle * PI/180) + 200 with angle=" + a, 200 * Math.sin(a * Math.PI/180) + 200, ast.evaluate({pi:Math.PI, angle:a, sin:Math.sin}));
			}
		}

		[Test]
		public function testOrderOfOperationsReorder():void
		{
			trace("\n\nshort\n\n");
			var ast:Ast = new Ast("1-2+2");
			Assert.assertEquals("1-2+2", 1-2+2, ast.evaluate({}));
		}

		[Test]
		public function testOrderOfOperationsMedium():void
		{
			trace("\n\medium\n\n");
			var ast:Ast = new Ast("4-34+4-3");
			Assert.assertEquals("4-34+4-3", 4-34+4-3, ast.evaluate({}));
		}

		[Test]
		public function testOrderOfOperations():void
		{
			trace("\n\nlong\n\n");
			var ast:Ast = new Ast("1+2-2*5+43-3*2/3+34-4*3/2-34+4*4/3-3");
			Assert.assertEquals("1+2-2*5+43-3*2/3+34-4*3/2-34+4*4/3-3", 1+2-2*5+43-3*2/3+34-4*3/2-34+4*4/3-3, ast.evaluate({}));
		}

		[Test]
		public function testSineWaveComparison():void
		{
			var ast:Ast = new Ast("sin(angle * PI/180) < cos(angle*PI/180) or sin(angle * PI/180) = tan(angle*PI/180)");
			for(var a:Number=-180; a< 540; a++) {
				Assert.assertEquals("sin(angle * PI/180) < cos(angle*PI/180) or sin(angle * PI/180) = tan(angle*PI/180) with angle=" + a, Math.sin(a * Math.PI/180) < Math.cos(a*Math.PI/180) || Math.sin(a * Math.PI/180) == Math.tan(a*Math.PI/180), ast.evaluate({pi:Math.PI, angle:a, sin:Math.sin, cos:Math.cos, tan:Math.tan}));
			}
		}

		[Test]
		public function testComparisonChainingLong():void
		{
			var ast:Ast = new Ast("3 < x < 5 < y < 10");

			for(var x:int = 0; x < 12; x++) {
				for(var y:int = 0; y < 12; y++) {
					Assert.assertEquals("3 < x < 5 < y < 10 with x=" + x + " and y=" + y, 3<x && x<5 && 5<y && y<10, ast.evaluate({x:x,y:y}));
				}
			}
		}

        [Test(expects="com.alleni.savana.PositionalError")]
  		public function testInvalidOperator():void
  		{
  			trace("\n\nOperator error\n\n");
  			var ast:Ast = new Ast("1*/3");
            ast.evaluate({})
  		}

        [Test]
        public function testNegation():void
        {
            trace("\n\nOperator error\n\n");
            var ast:Ast;
            ast = new Ast("~ true");
            Assert.assertEquals("not true", false, ast.evaluate({}));
            ast = new Ast("~ false");
            Assert.assertEquals("not false", true, ast.evaluate({}));
            ast = new Ast("not true");
            Assert.assertEquals("not true", false, ast.evaluate({}));
            ast = new Ast("not false");
            Assert.assertEquals("not false", true, ast.evaluate({}));
        }
	}
}