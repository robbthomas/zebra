package expression.tests {

import com.alleni.savana.Ast;

import org.flexunit.Assert;
import org.hamcrest.assertThat;
import org.hamcrest.core.anyOf;
import org.hamcrest.number.closeTo;
import org.hamcrest.object.equalTo;

public class TestConditional {
	public function TestConditional() {
	}
	public static var operators:Object =  {
		"<":function(lhs:Number, rhs:Number):Boolean { return lhs < rhs; },
		">":function(lhs:Number, rhs:Number):Boolean { return lhs > rhs; },
		"=":function(lhs:Number, rhs:Number):Boolean { return lhs == rhs; },
		"<=":function(lhs:Number, rhs:Number):Boolean { return lhs <= rhs; },
		">=":function(lhs:Number, rhs:Number):Boolean { return lhs >= rhs; },
		"<>":function(lhs:Number, rhs:Number):Boolean { return lhs != rhs; }
	};

	[Test]
	public function testBasic():void
	{
		for(var lhs:Number=0; lhs<=9; lhs++) {
			for(var rhs:Number=0; rhs<=9; rhs++) {
				for(var op:String in operators) {
					var ast:Ast = new Ast(new String(lhs) + op + new String(rhs));
					var result:Object = ast.evaluate({});
					Assert.assertTrue(result is Boolean);
					var expected:Number = (operators[op] as Function).call(this, lhs, rhs);
					var description:String = "LHS " + lhs + " RHS " + rhs + " OP " + op;
					assertThat(description, result, anyOf(equalTo(expected),closeTo(expected, 0.0001)));
				}
			}
		}
	}
}
}