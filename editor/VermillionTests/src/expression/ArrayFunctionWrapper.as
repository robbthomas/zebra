package expression
{
	import flash.utils.Proxy;
	import flash.utils.flash_proxy;

	public dynamic class ArrayFunctionWrapper extends Proxy
	{
		private var f:Function;
		private var _thisArg:*;
		private var _item:Array;
		private var _length:int;
		
		public function ArrayFunctionWrapper(length:int, func:Function, thisArg:*=null)
		{
			_length = length;
			this.f = func;
			_thisArg = thisArg;
		}
		
		override flash_proxy function callProperty(methodName:*, ... args):* {
			return _item[methodName].apply(_item, args);
		}
		
		override flash_proxy function getProperty(name:*):* {
			if(name == "length") {
				return _length;
			}
			if(!isNaN(name)) {
				var index:int = name;
				return f.apply(_thisArg,index);
			}
			
			return _item[name];
		}
		
		override flash_proxy function setProperty(name:*, value:*):void {
			_item[name] = value;
		}
	}
}