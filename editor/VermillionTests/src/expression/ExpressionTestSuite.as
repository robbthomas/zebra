package expression
{
	
import expression.tests.TestBasic;
import expression.tests.TestConditional;
import expression.tests.TestInteresting;
import expression.tests.TestLexing;
import expression.tests.TestObject;
import expression.tests.TestVariables;

[Suite]
[RunWith("org.flexunit.runners.Suite")]
public class ExpressionTestSuite
{
    public var lexing:TestLexing;
    public var basic:TestBasic;
    public var interesting:TestInteresting;
    public var conditional:TestConditional;
    public var object:TestObject;
    public var variables:TestVariables;
}
}