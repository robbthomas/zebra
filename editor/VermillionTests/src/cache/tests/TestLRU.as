/**
 * Created by ${PRODUCT_NAME}.
 * User: jessecoyle
 * Date: 1/26/11
 * Time: 11:38 AM
 * To change this template use File | Settings | File Templates.
 */
package cache.tests {
import com.alleni.author.util.cache.LRUCache;

import org.hamcrest.assertThat;
import org.hamcrest.object.equalTo;

public class TestLRU {
	public function TestLRU() {
	}

	[Test(description="test lru")]
	public function testSimple():void {
		var cache:LRUCache = new LRUCache(2, 4);
		cache.setValue("a", 1);
		cache.setValue("b", 2);
		assertThat("added a and b", cache.listKeys(), equalTo("ba"));
		cache.setValue("c", 3);
		cache.setValue("d", 4);
		assertThat(" added c and d", cache.listKeys(), equalTo("dcba"));
		cache.setValue("e", 5);
		assertThat(" added e", cache.listKeys(), equalTo("edc"));
	}

	[Test(description="test lru")]
	public function testSimplePlusGets():void {
		var cache:LRUCache = new LRUCache(2, 4);
		cache.setValue("a", 1);
		cache.setValue("b", 2);
		assertThat("added a and b", cache.listKeys(), equalTo("ba"));
		assertThat("a", cache.getValue("a"), equalTo(1));
		assertThat("b", cache.getValue("b"), equalTo(2));
		cache.setValue("c", 3);
		cache.setValue("d", 4);
		assertThat(" added c and d", cache.listKeys(), equalTo("dcba"));
		assertThat("a", cache.getValue("a"), equalTo(1));
		assertThat("b", cache.getValue("b"), equalTo(2));
		cache.setValue("e", 5);
		assertThat(" added e", cache.listKeys(), equalTo("eba"));
	}

    [Test(description="test LRU moving around")]
    public function testMovingAround():void
    {
        // minimal-sized cache
        var cache:LRUCache = new LRUCache(1,1);
        assertThat("add a to 1,1", testOne(cache, "a"), equalTo("a"));
        assertThat("add b to 1,1", testOne(cache, "b"), equalTo("b"));
        assertThat("add c to 1,1", testOne(cache, "c"), equalTo("c"));
        assertThat("add d to 1,1", testOne(cache, "d"), equalTo("d"));
        assertThat("add e to 1,1", testOne(cache, "e"), equalTo("e"));
        assertThat("add f to 1,1", testOne(cache, "f"), equalTo("f"));
        assertThat("add g to 1,1", testOne(cache, "g"), equalTo("g"));
        assertThat("add f to 1,1", testOne(cache, "f"), equalTo("f"));
        assertThat("add e to 1,1", testOne(cache, "e"), equalTo("e"));
        assertThat("add d to 1,1", testOne(cache, "d"), equalTo("d"));
        assertThat("add c to 1,1", testOne(cache, "c"), equalTo("c"));
        assertThat("add b to 1,1", testOne(cache, "b"), equalTo("b"));
        assertThat("add a to 1,1", testOne(cache, "a"), equalTo("a"));

        // softsize=3, hardSize=3
        cache = new LRUCache(3,3);
        assertThat("add a to 3,3", testOne(cache, "a"), equalTo("a"));
        assertThat("add b to 3,3", testOne(cache, "b"), equalTo("ba"));
        assertThat("add c to 3,3", testOne(cache, "c"), equalTo("cba"));
        assertThat("add d to 3,3", testOne(cache, "d"), equalTo("dcb"));
        assertThat("add e to 3,3", testOne(cache, "e"), equalTo("edc"));
        assertThat("add f to 3,3", testOne(cache, "f"), equalTo("fed"));
        assertThat("add g to 3,3", testOne(cache, "g"), equalTo("gfe"));
        assertThat("add f to 3,3", testOne(cache, "f"), equalTo("fge"));
        assertThat("add e to 3,3", testOne(cache, "e"), equalTo("efg"));
        assertThat("add d to 3,3", testOne(cache, "d"), equalTo("def"));
        assertThat("add c to 3,3", testOne(cache, "c"), equalTo("cde"));
        assertThat("add b to 3,3", testOne(cache, "b"), equalTo("bcd"));
        assertThat("add a to 3,3", testOne(cache, "a"), equalTo("abc"));

        // x simulates a master-page being always present
        var cache:LRUCache = new LRUCache(3,3);
        assertThat("add a,x", testTwo(cache, "a", "x"), equalTo("xa"));
        assertThat("add b,x", testTwo(cache, "b", "x"), equalTo("xba"));
        assertThat("add c,x", testTwo(cache, "c", "x"), equalTo("xcb"));
        assertThat("add d,x", testTwo(cache, "d", "x"), equalTo("xdc"));
        assertThat("add e,x", testTwo(cache, "e", "x"), equalTo("xed"));
        assertThat("add f,x", testTwo(cache, "f", "x"), equalTo("xfe"));
        assertThat("add g,x", testTwo(cache, "g", "x"), equalTo("xgf"));
        assertThat("add f,x", testTwo(cache, "f", "x"), equalTo("xfg"));
        assertThat("add e,x", testTwo(cache, "e", "x"), equalTo("xef"));
        assertThat("add d,x", testTwo(cache, "d", "x"), equalTo("xde"));
        assertThat("add c,x", testTwo(cache, "c", "x"), equalTo("xcd"));
        assertThat("add b,x", testTwo(cache, "b", "x"), equalTo("xbc"));
        assertThat("add a,x", testTwo(cache, "a", "x"), equalTo("xab"));

        // a real case that created a circular link within the list
        cache = new LRUCache(4,4);
        assertThat("add a to 4,4", testOne(cache, "a"), equalTo("a"));
        assertThat("add b to 4,4", testOne(cache, "b"), equalTo("ba"));
        assertThat("add a to 4,4", testOne(cache, "a"), equalTo("ab"));
        assertThat("add b to 4,4", testOne(cache, "b"), equalTo("ba"));
        assertThat("add a to 4,4", testOne(cache, "a"), equalTo("ab"));
        assertThat("add b to 4,4", testOne(cache, "b"), equalTo("ba"));
        assertThat("add a to 4,4", testOne(cache, "b"), equalTo("ba"));
        assertThat("add a to 4,4", testOne(cache, "b"), equalTo("ba"));

        // remove
        cache = new LRUCache(5,5);
        assertThat("add a to 5,5", testOne(cache, "e"), equalTo("e"));
        assertThat("add b to 5,5", testOne(cache, "d"), equalTo("de"));
        assertThat("add a to 5,5", testOne(cache, "c"), equalTo("cde"));
        assertThat("add a to 5,5", testOne(cache, "b"), equalTo("bcde"));
        assertThat("add a to 5,5", testOne(cache, "a"), equalTo("abcde"));
        assertThat("add a to 5,5", testOne(cache, "a"), equalTo("abcde"));

        assertThat("add a to 5,5", removeOne(cache, "x"), equalTo("abcde"));
        assertThat("add a to 5,5", removeOne(cache, "c"), equalTo("abde"));
        assertThat("add a to 5,5", removeOne(cache, "a"), equalTo("bde"));
        assertThat("add a to 5,5", removeOne(cache, "e"), equalTo("bd"));
        assertThat("add a to 5,5", removeOne(cache, "b"), equalTo("d"));
        assertThat("add a to 5,5", removeOne(cache, "d"), equalTo(""));
        assertThat("add a to 5,5", removeOne(cache, "d"), equalTo(""));
    }

    private function testOne(cache:LRUCache, key:String):String
    {
        cache.setValue(key, key);
        cache.validate();
        return cache.listKeys();
    }

    private function testTwo(cache:LRUCache, key:String, key2:String):String
    {
        cache.setValue(key, key);
        cache.setValue(key2, key2);
        cache.validate();
        return cache.listKeys();
    }

    private function removeOne(cache:LRUCache, key:String):String
    {
        cache.remove(key);
        cache.validate();
        return cache.listKeys();
    }

}
}
