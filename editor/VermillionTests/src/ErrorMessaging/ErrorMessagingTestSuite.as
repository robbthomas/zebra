/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 8/9/12
 * Time: 5:08 PM
 * To change this template use File | Settings | File Templates.
 */
package ErrorMessaging {
import ErrorMessaging.tests.TestBasic;
import ErrorMessaging.tests.TestInteresting;
import ErrorMessaging.tests.TestMultiple;

[Suite]
[RunWith("org.flexunit.runners.Suite")]
public class ErrorMessagingTestSuite {
    public var basic:TestBasic;
    public var multiple:TestMultiple;
    public var interesting:TestInteresting;
}
}
