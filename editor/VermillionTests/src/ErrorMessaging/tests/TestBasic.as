/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 8/9/12
 * Time: 5:08 PM
 * To change this template use File | Settings | File Templates.
 */
package ErrorMessaging.tests {
import com.alleni.author.controller.ErrorMessagingController;

import flexunit.framework.Assert;

public class TestBasic {
    public function TestBasic() {
    }

    [Test(description="Single Trivial")]
    public function testTrivial():void {
        var src:String = "Sample String";
        var result:String = ErrorMessagingController.processInserts(src);
        Assert.assertEquals(src,  result);
    }

    [Test(description="No replacement")]
    public function testNone():void {
        var src:String = "Sample {INSERT1} String";
        var result:String = ErrorMessagingController.processInserts(src);
        Assert.assertEquals(src,  result);
    }

    [Test(description="Single replacement")]
    public function testSingle():void {
        var src:String = "Sample {INSERT1} String";
        var expected:String = "Sample foo String";
        var result:String = ErrorMessagingController.processInserts(src, ["foo"]);
        Assert.assertEquals(expected,  result);
    }

}
}
