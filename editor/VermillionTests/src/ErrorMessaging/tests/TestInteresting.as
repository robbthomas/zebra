/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 8/10/12
 * Time: 10:51 AM
 * To change this template use File | Settings | File Templates.
 */
package ErrorMessaging.tests {
import com.alleni.author.controller.ErrorMessagingController;

import flexunit.framework.Assert;

public class TestInteresting {
    public function TestInteresting() {
    }
    [Test(description="Mixed replacements")]
    public function testMixed():void {
        var src:String = "Sample {INSERT1} {INSERT2} String {INSERT1}";
        var expected:String = "Sample foo bar String foo";
        var result:String = ErrorMessagingController.processInserts(src, ["foo", "bar"]);
        Assert.assertEquals(expected,  result);
    }
    [Test(description="Chained replacements")]
    public function testChained():void {
        var src:String = "Sample {INSERT1}";
        var expected:String = "Sample {INSERT2}";
        var result:String = ErrorMessagingController.processInserts(src, ["{INSERT2}", "bar"]);
        Assert.assertEquals(expected,  result);
    }

}
}
