/**
 * Created with IntelliJ IDEA.
 * User: jessecoyle
 * Date: 8/10/12
 * Time: 10:50 AM
 * To change this template use File | Settings | File Templates.
 */
package ErrorMessaging.tests {
import com.alleni.author.controller.ErrorMessagingController;

import flexunit.framework.Assert;

public class TestMultiple {
    public function TestMultiple() {
    }

    [Test(description="Multiple insert replacement")]
    public function testMultipleInsert():void {
        var src:String = "Sample {INSERT1} {INSERT2} String";
        var expected:String = "Sample foo bar String";
        var result:String = ErrorMessagingController.processInserts(src, ["foo", "bar"]);
        Assert.assertEquals(expected,  result);
    }

    [Test(description="Out of order replacement")]
    public function testInsertOrder():void {
        var src:String = "Sample {INSERT2} {INSERT1} String";
        var expected:String = "Sample bar foo String";
        var result:String = ErrorMessagingController.processInserts(src, ["foo", "bar"]);
        Assert.assertEquals(expected,  result);
    }

    [Test(description="Multiple source replacement")]
    public function testMultipleSource():void {
        var src:Array = ["Sample {INSERT1} String", "Other{INSERT1}"];
        var expected:Array = ["Sample foo String", "Otherfoo"];
        for(var i:int = 0; i < src.length; i++) {
            var result:String = ErrorMessagingController.processInserts(src[i], ["foo"]);
            Assert.assertEquals(expected[i],  result);
        }
    }

    [Test(description="Multiple insert and source replacement")]
    public function testMultipleInsertAndSource():void {
        var src:Array = ["Sample {INSERT1} {INSERT2} String", "Other{INSERT1}{INSERT2}"];
        var expected:Array = ["Sample foo bar String", "Otherfoobar"];
        for(var i:int = 0; i < src.length; i++) {
            var result:String = ErrorMessagingController.processInserts(src[i], ["foo", "bar"]);
            Assert.assertEquals(expected[i],  result);
        }
    }
}
}
