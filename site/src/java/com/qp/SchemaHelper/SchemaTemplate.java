/*
 * Created February 12, 2010
 * Ben Resner - ben@qualityprocess.com
 *
 * Holder for fields and other data used to uniquely define a schema
 * using QualityProcess naming conventions.
 *
 * Contains data about a schema table and the information necessary to
 * generate the support Java, HTML, SQL, and JSP files to support new tables.
 *
 */

package com.qp.SchemaHelper;

/**
 *
 * @author vmware
 */
public class SchemaTemplate {

    protected final SchemaField m_FieldList[];
    protected final String m_NameLower;            // someSampleText
    protected final String m_NameUScore;           // some_sample_text
    protected final String m_NameCamelUpperPlural; // SomeSampleTexts
    protected final String m_NameCamelUpper;       // SomeSampleText
    protected final String m_NameCamelLower;       // someSampleText
    protected final String m_NameCamelLowerSingle; // someSampleText
    protected final String m_NameFull;             // Some Sample Text
    protected final String m_FolderName;
    protected final Boolean m_IsUpdateable;

    public SchemaTemplate(String nameLower, String nameUScore, String nameCamelUpperPlural, String nameCamelUpper, String nameCamelLower, String nameCamelLowerSingle, String nameFull, String folderName, Boolean isUpdateable, SchemaField fieldList[]) {
        m_FieldList             = fieldList;
        m_NameLower             = nameLower;
        m_NameUScore            = nameUScore;
        m_NameCamelUpperPlural  = nameCamelUpperPlural;
        m_NameCamelUpper        = nameCamelUpper;
        m_NameCamelLower        = nameCamelLower;
        m_NameCamelLowerSingle  = nameCamelLowerSingle;
        m_NameFull              = nameFull;
        m_FolderName            = folderName;
        m_IsUpdateable          = isUpdateable;
    }

    public SchemaField[] getFieldList() {
        return m_FieldList;
    }

    public String getNameLower() {
        return m_NameLower;
    }

    public String getNameUScore() {
        return m_NameUScore;
    }

    public String getNameCamelUpper() {
        return m_NameCamelUpper;
    }

    public String getNameCamelLower() {
        return m_NameCamelLower;
    }

    public String getNameCamelLowerSingle() {
        return m_NameCamelLowerSingle;
    }

    public String getNameFull() {
        return m_NameFull;
    }

    public String getNameFullLower() {
        return m_NameFull.toLowerCase();
    }

    public Boolean isUpdateable() {
        return m_IsUpdateable;
    }

    // variable length arguments.  Hooray!
    public int countTypes(SchemaField.Type ... types) {
        int     count = 0;
        for (SchemaField.Type type : types) {
            count += countTypes(type);
        }
        return count;
    }

    public int countTypes(SchemaField.Type type) {
        int count = 0;
        for (SchemaField sf : m_FieldList) {
            if (sf.getType() == type) {
                count++;
            }
        }
        return count;
    }

    protected static boolean containsType(SchemaField.Type typeList[], SchemaField.Type type) {
        for (SchemaField.Type sf : typeList) {
            if (sf == type) {
                return true;
            }
        }
        return false;
    }

    // converts "foo_bear" to "Foo Bear"
    // Expands "ms" to "Managed Service or ms, depending on 'ms_replace'"
    public static String toFriendlyName(String input, String ms_replace, String id_replace) {
        int ii;
        StringBuilder retVal = new StringBuilder();

        input = input.replaceAll("ms", ms_replace);
        input = input.replaceAll("id", id_replace);

        for (ii = 0; ii < input.length(); ii++) {
            char c = input.charAt(ii);

            if (c == '_') {
                retVal.append(" ");
                retVal.append((new String("" + input.charAt(++ii)).toUpperCase()));
            } else {
                if (ii == 0) {
                    retVal.append(new String("" + c).toUpperCase());
                }
                else {
                    retVal.append(c);
                }
            }

        }
        return retVal.toString();
    }

    // converts "foo_bear" to "fooBear";
    public static String toCamelCase(String input) {
        int ii;
        StringBuilder retVal = new StringBuilder();

        for (ii = 0; ii < input.length(); ii++) {
            char c = input.charAt(ii);

            if (c == '_') {
                retVal.append((new String("" + input.charAt(++ii)).toUpperCase()));
            } else {
                retVal.append(c);
            }

        }
        return retVal.toString();
    }
}
