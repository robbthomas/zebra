--------------------------------------------------------------------------------
-- fn_invoicelineitems.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
--------------------------------------------------------------------------------
-- InvoiceLineItems functions roles map
-- Clear existing entries for this table and then readd
--------------------------------------------------------------------------------

delete from TableFunctionRoleMap WHERE tableName='InvoiceLineItems'
    AND (functionName='CREATE' OR functionName='UPDATE' OR functionName='RETIRE' OR functionName='FILTER' OR functionName='FIND');

select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','CREATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','CREATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','CREATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','UPDATE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','UPDATE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','UPDATE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','RETIRE','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','RETIRE','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','RETIRE','ROLE_TYPE_AUTHOR');

select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FILTER','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FILTER','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FILTER','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FILTER','ROLE_TYPE_VIEWER');

select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FIND','ROLE_TYPE_SYSTEM_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FIND','ROLE_TYPE_ACCOUNT_ADMINISTRATOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FIND','ROLE_TYPE_AUTHOR');
select * from fn_TableFunctionRoleMap_insert('InvoiceLineItems','FIND','ROLE_TYPE_VIEWER');



--------------------------------------------------------------------------------
-- Function: fn_invoicelineitem_create(text, text, /*{{*/text, text, integer, integer, text, text, integer/*}}*/)

-- DROP FUNCTION fn_invoicelineitem_create(text, text, /*{{*/text, text, integer, integer, text, text, integer/*}}*/);

CREATE OR REPLACE FUNCTION fn_invoicelineitem_create(in_credentials_email text, in_credentials_password text, /*{{*/in_short_name text, in_long_name text, in_display_order integer, in_visibility integer, in_definition text, in_color text, in_edited_by_member_id integer/*}}*/)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'InvoiceLineItems';
        var_functionName text := 'CREATE';
--        var_editedById text;

        new_invoiceLineItemId int := -1;
        var_count int := -1;

    BEGIN
        var_credentialsId := id FROM App_User WHERE (lower(in_credentials_email)=lower(email) OR in_credentials_email = displayName)
            AND passwordHash = encode(digest(in_credentials_password || COALESCE(passwordSalt,''),'sha1'),'hex');

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

--        var_editedById := COALESCE(in_editedById,'');
--        IF LENGTH(var_editedById) = 0 THEN var_editedById := var_credentialsId; END IF;

        INSERT INTO InvoiceLineItems(
/*{{*/
            short_name,
            long_name,
            display_order,
            visibility,
            definition,
            color,
            edited_by_member_id
/*}}*/
        )
        VALUES(
/*{{*/
            in_short_name,
            in_long_name,
            in_display_order,
            in_visibility,
            in_definition,
            in_color,
            in_edited_by_member_id
/*}}*/
        );

        GET DIAGNOSTICS var_count = Row_Count;
        IF var_count = 1 THEN
            new_invoiceLineItemId := currval('seq_InvoiceLineItems_invoiceLineItemId');
        END IF;

        return new_invoiceLineItemId;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_invoicelineitem_create(text, text, /*{{*/text, text, integer, integer, text, text, integer/*}}*/) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoicelineitem_update(text, text, integer, /*{{*/text, text, integer, integer, text, text, integer/*}}*/, text)

-- DROP FUNCTION fn_invoicelineitem_update(text, text, integer, /*{{*/text, text, integer, integer, text, text, integer/*}}*/, text);

CREATE OR REPLACE FUNCTION fn_invoicelineitem_update(in_credentials_email text, in_credentials_password text, in_invoiceLineItemId integer, /*{{*/in_short_name text, in_long_name text, in_display_order integer, in_visibility integer, in_definition text, in_color text, in_edited_by_member_id integer/*}}*/, in_param_list text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'InvoiceLineItems';
        var_functionName text := 'UPDATE';

        rec InvoiceLineItems%ROWTYPE;
        param_name text;
        var_count int := 1;

    BEGIN
        var_credentialsId := id FROM App_User WHERE (lower(in_credentials_email)=lower(email) OR in_credentials_email = displayName)
            AND passwordHash = encode(digest(in_credentials_password || COALESCE(passwordSalt,''),'sha1'),'hex');

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        -- Select existing values into the rec variable.
        SELECT into rec * from InvoiceLineItems where invoiceLineItemId = in_invoiceLineItemId;

        -- Replace existing values with parameter values, but only for ones
        -- that are named in in_param_list. This way, we can avoid just update
        -- the columns we have actual data for.
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

/*{{*/
            IF param_name = 'in_short_name' THEN
                rec.short_name := in_short_name;
            ELSEIF param_name = 'in_long_name' THEN
                rec.long_name := in_long_name;
            ELSEIF param_name = 'in_display_order' THEN
                rec.display_order := in_display_order;
            ELSEIF param_name = 'in_visibility' THEN
                rec.visibility := in_visibility;
            ELSEIF param_name = 'in_definition' THEN
                rec.definition := in_definition;
            ELSEIF param_name = 'in_color' THEN
                rec.color := in_color;
            ELSEIF param_name = 'in_edited_by_member_id' THEN
                rec.edited_by_member_id := in_edited_by_member_id;
            END IF;
/*}}*/
            var_count := var_count + 1;
        END LOOP;

        -- Update with the combined data.
        UPDATE InvoiceLineItems SET
/*{{*/
            short_name = rec.short_name,
            long_name = rec.long_name,
            display_order = rec.display_order,
            visibility = rec.visibility,
            definition = rec.definition,
            color = rec.color,
            edited_by_member_id = rec.edited_by_member_id,
            updated_date_time = now()
/*}}*/
        WHERE invoiceLineItemId = in_invoiceLineItemId;

        GET DIAGNOSTICS var_count = Row_Count;

        return var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_invoicelineitem_update(text, text, integer, /*{{*/text, text, integer, integer, text, text, integer/*}}*/, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoicelineitem_retire(text, text, integer, text)

-- DROP FUNCTION fn_invoicelineitem_retire(text, text, integer, text);

CREATE OR REPLACE FUNCTION fn_invoicelineitem_retire(in_credentials_email text, in_credentials_password text, in_invoiceLineItemId integer, in_retiredById text)
  RETURNS integer AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'InvoiceLineItems';
        var_functionName text := 'RETIRE';

        var_count int := -1;
        var_retiredById text;

    BEGIN
        var_credentialsId := id FROM App_User WHERE (lower(in_credentials_email)=lower(email) OR in_credentials_email = displayName)
            AND passwordHash = encode(digest(in_credentials_password || COALESCE(passwordSalt,''),'sha1'),'hex');

        IF var_credentialsId IS NULL THEN
            RETURN -1;
        END IF;

        var_retiredById := COALESCE(in_retiredById,'');
        IF LENGTH(var_retiredById) = 0 THEN
            var_retiredById := var_credentialsId;
        END IF;

        UPDATE InvoiceLineItems SET retired = 1, retiredById = var_retiredById, retiredDateTime = now()
        WHERE invoiceLineItemId = in_invoiceLineItemId;

        GET DIAGNOSTICS var_count = Row_Count;

        RETURN var_count;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION fn_invoicelineitem_retire(text, text, integer, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoicelineitem_filter(text, text, integer, integer, text, /*{{*/text, text, integer, integer, text, text/*}}*/, text)

-- DROP FUNCTION fn_invoicelineitem_filter(text, text, integer, integer, text, /*{{*/text, text, integer, integer, text, text/*}}*/, text);

CREATE OR REPLACE FUNCTION fn_invoicelineitem_filter(in_credentials_email text, in_credentials_password text, in_count integer, in_offset integer, in_order_list text, /*{{*/in_short_name text, in_long_name text, in_display_order integer, in_visibility integer, in_definition text, in_color text/*}}*/, in_filter_list text)
  RETURNS SETOF InvoiceLineItems AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'InvoiceLineItems';
        var_functionName text := 'FILTER';

        rec InvoiceLineItems%ROWTYPE;
        filter_name text;
        var_count int := 1;
        var_order text;
        var_query text := 'SELECT * FROM InvoiceLineItems WHERE retired = 0';

    BEGIN
        var_credentialsId := id FROM App_User WHERE (lower(in_credentials_email)=lower(email) OR in_credentials_email = displayName)
            AND passwordHash = encode(digest(in_credentials_password || COALESCE(passwordSalt,''),'sha1'),'hex');

        IF var_credentialsId IS NOT NULL THEN

        LOOP
            filter_name := split_part(in_filter_list, ',', var_count);
            filter_name := trim(both ' ' from filter_name);
            EXIT WHEN filter_name = '';

/*{{*/
            IF filter_name = 'in_short_name' THEN
                all_short_name := false;
            ELSEIF filter_name = 'in_long_name' THEN
                all_long_name := false;
            ELSEIF filter_name = 'in_display_order' THEN
                all_display_order := false;
            ELSEIF filter_name = 'in_visibility' THEN
                all_visibility := false;
            ELSEIF filter_name = 'in_definition' THEN
                all_definition := false;
            ELSEIF filter_name = 'in_color' THEN
                all_color := false;
            END IF;
/*}}*/
            var_count := var_count + 1;
        END LOOP;

        IF LENGTH(in_order_list) > 0 THEN
            var_order := replace(in_order_list,':a',' ASC');
            var_order := replace(var_order,':d',' DESC');
            var_query := var_query || ' ORDER BY ' || var_order;
        END IF;

        var_count := in_count;
        IF var_count <= 0 THEN
            SELECT count(*) INTO var_count FROM InvoiceLineItems;
        END IF;
        var_query := var_query || ' LIMIT ' || var_count || ' OFFSET ' || in_offset;

        FOR rec IN
            EXECUTE var_query
        LOOP
            RETURN NEXT rec;
        END LOOP;

        END IF;

    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_invoicelineitem_filter(text, text, integer, integer, text, /*{{*/text, text, integer, integer, text, text/*}}*/, text) OWNER TO postgres;

--------------------------------------------------------------------------------
-- Function: fn_invoicelineitem_find(text, text, integer)

-- DROP FUNCTION fn_invoicelineitem_find(text, text, integer);

CREATE OR REPLACE FUNCTION fn_invoicelineitem_find(in_credentials_email text, in_credentials_password text, in_invoiceLineItemId integer)
  RETURNS SETOF InvoiceLineItems AS
$BODY$
    DECLARE
        var_credentialsId text;
        var_tableName text := 'InvoiceLineItems';
        var_functionName text := 'FIND';

        rec InvoiceLineItems%ROWTYPE;

    BEGIN
        var_credentialsId := id FROM App_User WHERE (lower(in_credentials_email)=lower(email) OR in_credentials_email = displayName)
            AND passwordHash = encode(digest(in_credentials_password || COALESCE(passwordSalt,''),'sha1'),'hex');

        IF var_credentialsId IS NOT NULL THEN
            FOR rec IN
                SELECT *
                FROM InvoiceLineItems
                WHERE invoiceLineItemId = in_invoiceLineItemId
            LOOP
                RETURN NEXT rec;
            END LOOP;
        END IF;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_invoicelineitem_find(text, text, integer) OWNER TO postgres;
