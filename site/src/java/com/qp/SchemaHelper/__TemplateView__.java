package com.qp.SchemaHelper;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class __TemplateView__ extends BaseBean implements Serializable
{
    public enum Right {
        __TEMPLATEVIEW___FIND
    }

    static private __TemplateViewDao__ _dao() { return new __TemplateViewDao__(); }

    protected __TemplateView__(JSONObject daoJson) throws JSONException
    {
/*{{*/
        this.jsonValues.put("id", daoJson.has("invoiceLineItemId" ) ? daoJson.getInt("invoiceLineItemId") : -1);
        this.jsonValues.put("shortName", daoJson.has("short_name") ? daoJson.getString("short_name") : "");
        this.jsonValues.put("longName", daoJson.has("long_name") ? daoJson.getString("long_name") : "");
        this.jsonValues.put("displayOrder", daoJson.has("display_order") ? daoJson.getInt("display_order") : -1);
        this.jsonValues.put("visibility", daoJson.has("visibility") ? daoJson.getInt("visibility") : -1);
        this.jsonValues.put("definition", daoJson.has("definition") ? daoJson.getString("definition") : "");
        this.jsonValues.put("color", daoJson.has("color") ? daoJson.getString("color") : "");
        this.jsonValues.put("created", daoJson.has("created_date_time") ? (Timestamp)daoJson.get("created_date_time") : new Timestamp(0));
        this.jsonValues.put("updated", daoJson.has("updated_date_time") ? (Timestamp)daoJson.get("updated_date_time") : null);
        this.jsonValues.put("editedByMemberId", daoJson.has("edited_by_member_id") ? daoJson.getInt("edited_by_member_id") : -1);
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
/*}}*/
    }

    public static String find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.__TEMPLATEVIEW___FIND, credentialValues))
            return JsonError.error(JsonError.Error.__TEMPLATEVIEW___FIND, "Insufficient rights");

        int id = findValues.getInt("id");
        ArrayList __templateView__List = _dao().find(id, credentialValues);

        if (__templateView__List.size() < 1)
            return JsonError.error(JsonError.Error.__TEMPLATEVIEW___FIND, "Could not find invoice line items id "+id);
        if (__templateView__List.size() > 1)
            return JsonError.error(JsonError.Error.__TEMPLATEVIEW___FIND, "Found multiple invoice line items records for invoice line items id "+id);

        __TemplateView__ __templateView__ = new __TemplateView__((JSONObject)__templateView__List.get(0));
        if (__templateView__ == null)
            return JsonError.error(JsonError.Error.__TEMPLATEVIEW___FIND, "Could not make invoice line items object for id "+id);

        String s = __templateView__.toString();
        return s;
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.__TEMPLATEVIEW___FIND, credentialValues))
            return JsonError.jsonError(JsonError.Error.__TEMPLATEVIEW___FIND, "Insufficient rights");

        ArrayList __templateView__List = _dao().findAll(credentialValues);
        return jsonList(__templateView__List);
    }

    public static String filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.__TEMPLATEVIEW___FIND, credentialValues))
            return JsonError.error(JsonError.Error.__TEMPLATEVIEW___FIND, "Insufficient rights");

        // get an array of invoice line items json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList __templateView__List = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(__templateView__List, limit);

        JSONObject jsonList = jsonList(__templateView__List);
        jsonList.put("hasMoreData", hasMoreData);
        return jsonList.toString();
    }

    protected static JSONObject jsonList(ArrayList arrayList) throws JSONException
    {
        for (int j = 0; j < arrayList.size(); j++)
        {
            __TemplateView__ __templateView__ = new __TemplateView__((JSONObject)arrayList.get(j));
            if (__templateView__ == null)
                return JsonError.jsonError(JsonError.Error.__TEMPLATEVIEW___FIND, "Could not make invoice line items object " + j);
            JSONObject json = new JSONObject(__templateView__.toString());
            arrayList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("__templateView__s", arrayList);

        return jsonList;
    }

    protected static boolean hasRight(Right right, JSONObject credentialValues) throws JSONException, SQLException
    {
        if (!validateAndUpdateCredentials(credentialValues))
            return false;

        // Now check against the rights.
        return true;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        __TEMPLATEVIEW___FIND,

 */