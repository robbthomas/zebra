/*
 * Created March 25 12, 2011
 * Ben Resner - ben@qualityprocess.com
 *
 * Holds the Schemas for which we need to generate files.
 * The file "SchemaTemplate.java" was getting large so files got put here
 *
 */
package com.qp.SchemaHelper;

import com.qp.SchemaHelper.SchemaField.Type;

public class SchemaHelper {

    // parallels revised structure of SpringSource Zephyr App_User database table per task #622
    public static final SchemaGenerator m_AppUser = new SchemaGenerator(
        "appuser",              // lower, all one word, singular
        "app_user",             // lower, with underscore, singular
        "AppUsers",             // camel case, leading upper, plural
        "AppUser",              // camel case, leading upper, singular
        "appUsers",             // camel case, leading lower, plural
        "appUser",              // camel case, leading lower, singular
        "Application User",     // display name with spaces, singular
        "member",               // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("Id",                   Type.PrimaryKey),
            new SchemaField("version",              Type.Integer),
            new SchemaField("displayName",          Type.Text), // formerly username
            new SchemaField("description",          Type.Text),
            new SchemaField("email",                Type.Text),
            //new SchemaField("emailShow",            Type.Boolean), // add Type.Boolean or as Type.Integer and map or just not support???
            //new SchemaField("enabled",              Type.Boolean), // add Type.Boolean or as Type.Integer and map or just not support???
            new SchemaField("firstName",            Type.Text),
            new SchemaField("lastName",             Type.Text),
            new SchemaField("passwordHash",         Type.Text),
            new SchemaField("passwordSalt",         Type.Text),
            new SchemaField("editedById",           Type.Text), // FK to App_User.Id (aka Members.memberId)
            new SchemaField("editedDateTime",       Type.Timestamp), // defaults to now()
            new SchemaField("lastUpdated",          Type.Special_editedBy),
            new SchemaField("retired",              Type.Special_retired),
            new SchemaField("retiredById",          Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",      Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_ContactPreferenceType = new SchemaGenerator(
        "contactpreferencetype",              // lower, all one word, singular
        "contact_preference_type",             // lower, with underscore, singular
        "ContactPreferenceTypes",             // camel case, leading upper, plural
        "ContactPreferenceType",              // camel case, leading upper, singular
        "contactPreferenceTypes",             // camel case, leading lower, plural
        "contactPreferenceType",              // camel case, leading lower, singular
        "Contact Preference Type",             // display name with spaces, singular
        "util",                  // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("contactPreferenceTypeId",      Type.PrimaryKey),
            new SchemaField("tag",                          Type.Text),
            new SchemaField("name",                         Type.Text),
            new SchemaField("displayOrder",                 Type.Integer),
            new SchemaField("createdDateTime",              Type.Timestamp_created),
            new SchemaField("updatedDateTime",              Type.Timestamp_updated),
        }
    );

    public static final SchemaGenerator m_CreditCardType = new SchemaGenerator(
        "creditcardtype",              // lower, all one word, singular
        "credit_card_type",             // lower, with underscore, singular
        "CreditCardTypes",             // camel case, leading upper, plural
        "CreditCardType",              // camel case, leading upper, singular
        "creditCardTypes",             // camel case, leading lower, plural
        "creditCardType",              // camel case, leading lower, singular
        "Credit Card Type",             // display name with spaces, singular
        "finance",                  // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("creditCardTypeId",      Type.PrimaryKey),
            new SchemaField("tag",                          Type.Text),
            new SchemaField("name",                         Type.Text),
            new SchemaField("displayOrder",                 Type.Integer),
            new SchemaField("createdDateTime",              Type.Timestamp_created),
            new SchemaField("updatedDateTime",              Type.Timestamp_updated),
        }
    );


    public static final SchemaGenerator m_CommentFeedback = new SchemaGenerator(
        "commentfeedback",              // lower, all one word, singular
        "comment_feedback",             // lower, with underscore, singular
        "CommentFeedbacks",             // camel case, leading upper, plural
        "CommentFeedback",              // camel case, leading upper, singular
        "commentFeedbacks",             // camel case, leading lower, plural
        "commentFeedback",              // camel case, leading lower, singular
        "Comment Feedback",             // display name with spaces, singular
        "feedback",                  // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("Id",                   Type.PrimaryKey),
            new SchemaField("commentId",            Type.Integer),
            new SchemaField("memberId",             Type.Text),
            new SchemaField("feedback",             Type.Text),
            new SchemaField("createdDateTime",      Type.Timestamp_created),
            new SchemaField("updatedDateTime",      Type.Timestamp_updated),
            new SchemaField("flagReason",			Type.Text)	
        }
    );


    public static final SchemaGenerator m_ZappComment = new SchemaGenerator(
        "zappcomment",              // lower, all one word, singular
        "zapp_comment",             // lower, with underscore, singular
        "ZappComments",             // camel case, leading upper, plural
        "ZappComment",              // camel case, leading upper, singular
        "zappComments",             // camel case, leading lower, plural
        "zappComment",              // camel case, leading lower, singular
        "Zapp Comment",             // display name with spaces, singular
        "feedback",                  // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("Id",                   Type.PrimaryKey),
            new SchemaField("publishId",            Type.Integer),
            new SchemaField("memberId",             Type.Text),
            new SchemaField("comment",              Type.Text),
            new SchemaField("helpfulCount",         Type.Integer),
            new SchemaField("feedbackCount",        Type.Integer),
            new SchemaField("createdDateTime",      Type.Timestamp_created),
            new SchemaField("updatedDateTime",      Type.Timestamp_updated),
        }
    );

    public static final SchemaGenerator m_ZappRating = new SchemaGenerator(
        "zapprating",              // lower, all one word, singular
        "zapp_rating",             // lower, with underscore, singular
        "ZappRatings",             // camel case, leading upper, plural
        "ZappRating",              // camel case, leading upper, singular
        "zappRatings",             // camel case, leading lower, plural
        "zappRating",              // camel case, leading lower, singular
        "Zapp Rating",             // display name with spaces, singular
        "feedback",                  // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("Id",                   Type.PrimaryKey),
            new SchemaField("publishId",            Type.Integer),
            new SchemaField("memberId",             Type.Text),
            new SchemaField("rating",               Type.Integer),
            new SchemaField("createdDateTime",      Type.Timestamp_created),
            new SchemaField("updatedDateTime",      Type.Timestamp_updated),
        }
    );
    
    public static final SchemaGenerator m_projectFeedback = new SchemaGenerator(
    		"projectfeedback",           // lower, all one word, singular
            "project_feedback",          // lower, with underscore, singular
            "ProjectFeedbacks",          // camel case, leading upper, plural
            "ProjectFeedback",           // camel case, leading upper, singular
            "projectFeedbacks",          // camel case, leading lower, plural
            "projectFeedback",           // camel case, leading lower, singular
            "Project Feedback",          // display name with spaces, singular
            "feedback",                  // folder, singular
            true,
            new SchemaField[] {
    			new SchemaField("projectFeedbackID", 	Type.PrimaryKey),
    			new SchemaField("projectID",			Type.Text),
    			new SchemaField("feedbackType",			Type.Text),
    			new SchemaField("memberID", 			Type.Text),
    			new SchemaField("flagReason",			Type.Text),
    			new SchemaField("createddatetime",		Type.Timestamp_created),
    			new SchemaField("updateddatetime",		Type.Timestamp_updated),
    			new SchemaField("retired", 				Type.Special_retired),
    			new SchemaField("retiredById",          Type.Text),
    			new SchemaField("retiredDateTime", 		Type.Timestamp)
    		}
    );

    
    public static final SchemaGenerator m_ZappRatingCache = new SchemaGenerator(
        "zappratingcache",              // lower, all one word, singular
        "zapp_rating_cache",             // lower, with underscore, singular
        "ZappRatingCaches",             // camel case, leading upper, plural
        "ZappRatingCache",              // camel case, leading upper, singular
        "zappRatingCaches",             // camel case, leading lower, plural
        "zappRatingCache",              // camel case, leading lower, singular
        "Zapp Rating Cache",             // display name with spaces, singular
        "feedback",                  // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("Id",                   Type.PrimaryKey),
            new SchemaField("publishId",            Type.Integer),
            new SchemaField("numRatings",           Type.Integer),
            new SchemaField("ratingTotal",          Type.Integer),
            new SchemaField("createdDateTime",      Type.Timestamp_created),
            new SchemaField("updatedDateTime",      Type.Timestamp_updated),

        }
    );

    public static final SchemaGenerator m_AccountType = new SchemaGenerator(
        "accounttype",      // lower, all one word, singular
        "account_type",     // lower, with underscore, singular
        "AccountTypes",     // camel case, leading upper, plural
        "AccountType",      // camel case, leading upper, singular
        "accountTypes",     // camel case, leading lower, plural
        "accountType",      // camel case, leading lower, singular
        "Account Type",     // display name with spaces, singular
        "account",          // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("accountTypeId",        Type.PrimaryKey),
            new SchemaField("tag",                  Type.Text),
            new SchemaField("name",                 Type.Text),
            new SchemaField("description",          Type.Text),
            new SchemaField("terms",                Type.Text),
            new SchemaField("price",                Type.Integer),
            new SchemaField("currencyTypeId",       Type.Integer),
            new SchemaField("displayOrder",         Type.Integer),
            new SchemaField("editedById",           Type.Special_editedBy), // FK to App_User.Id
            new SchemaField("editedDateTime",       Type.Timestamp_created),
            new SchemaField("retired",              Type.Special_retired),
            new SchemaField("retiredById",          Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",      Type.Timestamp)
        }
    );

    // parallels revised structure of SpringSource Zephyr Parent database table per task #625
    public static final SchemaGenerator m_Project = new SchemaGenerator(
        "project",      // lower, all one word, singular
        "project",      // lower, with underscore, singular
        "Projects",     // camel case, leading upper, plural
        "Project",      // camel case, leading upper, singular
        "projects",     // camel case, leading lower, plural
        "project",      // camel case, leading lower, singular
        "Project",      // display name with spaces, singular
        "project",      // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("accountId",            Type.Integer),
            new SchemaField("memberId",             Type.Integer), // FK to App_User.Id (aka Members.memberId)
            new SchemaField("typeId",               Type.Integer),
            new SchemaField("retired",              Type.Special_retired),
            new SchemaField("retiredById",          Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",      Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_Company = new SchemaGenerator(
        "company",              // lower, all one word, singular
        "company",              // lower, with underscore, singular
        "Companies",            // camel case, leading upper, plural
        "Company",              // camel case, leading upper, singular
        "companies",            // camel case, leading lower, plural
        "company",              // camel case, leading lower, singular
        "Account Company",      // display name with spaces, singular
        "account",              // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("companyId",            Type.PrimaryKey),
            new SchemaField("name",                 Type.Text),
            new SchemaField("description",          Type.Text),
            new SchemaField("companyUrl",           Type.Text),
            new SchemaField("editedById",           Type.Special_editedBy),
            new SchemaField("editedDateTime",       Type.Timestamp_created)
        }
    );

    public static final SchemaGenerator m_Account = new SchemaGenerator(
        "account",      // lower, all one word, singular
        "account",      // lower, with underscore, singular
        "Accounts",     // camel case, leading upper, plural
        "Account",      // camel case, leading upper, singular
        "accounts",     // camel case, leading lower, plural
        "account",      // camel case, leading lower, singular
        "Account",      // display name with spaces, singular
        "account",      // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("accountId",        Type.PrimaryKey),
            new SchemaField("companyId",        Type.Integer),
            new SchemaField("type",             Type.Text),
            new SchemaField("termDueDateTime",  Type.Timestamp),
            new SchemaField("goodStanding",     Type.Integer),
            new SchemaField("name",             Type.Text),
            new SchemaField("description",      Type.Text),
            new SchemaField("tokenBalance",     Type.Integer),
            new SchemaField("editedById",       Type.Special_editedBy),
            new SchemaField("editedDateTime",   Type.Timestamp_created),
            new SchemaField("retired",          Type.Special_retired),
            new SchemaField("retiredById",      Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",  Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_Address = new SchemaGenerator(
        "address",          // lower, all one word, singular
        "address",          // lower, with underscore, singular
        "Addresses",        // camel case, leading upper, plural
        "Address",          // camel case, leading upper, singular
        "addresses",        // camel case, leading lower, plural
        "address",          // camel case, leading lower, singular
        "Account Address",  // display name with spaces, singular
        "account",          // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("addressId",        Type.PrimaryKey),
            new SchemaField("type",             Type.Text),
            new SchemaField("accountId",        Type.Integer),
            new SchemaField("name",             Type.Text),
            new SchemaField("company",          Type.Text),
            new SchemaField("address1",         Type.Text),
            new SchemaField("address2",         Type.Text),
            new SchemaField("city",             Type.Text),
            new SchemaField("stateProvince",    Type.Text),
            new SchemaField("zipPostalCode",    Type.Text),
            new SchemaField("country",          Type.Text),
            new SchemaField("website",          Type.Text),
            new SchemaField("twitter",          Type.Text),
            new SchemaField("phone",            Type.Text),
            new SchemaField("editedById",       Type.Special_editedBy),
            new SchemaField("editedDateTime",   Type.Timestamp_created),
            new SchemaField("retired",          Type.Special_retired),
            new SchemaField("retiredById",      Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",  Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_Telephone = new SchemaGenerator(
        "telephone",           // lower, all one word, singular
        "telephone",           // lower, with underscore, singular
        "Telephones",          // camel case, leading upper, plural
        "Telephone",           // camel case, leading upper, singular
        "telephones",          // camel case, leading lower, plural
        "telephone",           // camel case, leading lower, singular
        "Account Telephone",   // display name with spaces, singular
        "account",             // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("telephoneId",      Type.PrimaryKey),
            new SchemaField("type",             Type.Text),
            new SchemaField("accountId",        Type.Integer),
            new SchemaField("memberId",         Type.Text), // optional - FK to Members table (aka App_User)
            new SchemaField("label",            Type.Text),
            new SchemaField("number",           Type.Text),
            new SchemaField("editedById",       Type.Special_editedBy), // FK to App_User.Id (aka Members.memberId)
            new SchemaField("editedDateTime",   Type.Timestamp_created),
            new SchemaField("retired",          Type.Special_retired),
            new SchemaField("retiredById",      Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",  Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_RoleType = new SchemaGenerator(
        "roletype",         // lower, all one word, singular
        "role_type",        // lower, with underscore, singular
        "RoleTypes",        // camel case, leading upper, plural
        "RoleType",         // camel case, leading upper, singular
        "roleTypes",        // camel case, leading lower, plural
        "roleType",         // camel case, leading lower, singular
        "Role Type",        // display name with spaces, singular
        "role",             // folder, singular
        true,               
        new SchemaField[] {
            new SchemaField("roleTypeId",       Type.PrimaryKey),
            new SchemaField("tag",              Type.Text),
            new SchemaField("name",             Type.Text),
            new SchemaField("description",      Type.Text),
            new SchemaField("displayOrder",     Type.Integer),
            new SchemaField("retired",          Type.Special_retired),
            new SchemaField("retiredById",      Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",  Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_AccountMemberRole = new SchemaGenerator(
        "accountmemberrole",       // lower, all one word, singular
        "account_member_role",     // lower, with underscore, singular
        "AccountMemberRoles",      // camel case, leading upper, plural
        "AccountMemberRole",       // camel case, leading upper, singular
        "accountMemberRoles",      // camel case, leading lower, plural
        "accountMemberRole",       // camel case, leading lower, singular
        "Account Member Role",     // display name with spaces, singular
        "role",                    // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("accountId",        Type.Integer),
            new SchemaField("memberId",         Type.Text), // FK to App_User.Id (aka Members.memberId)
            new SchemaField("type",             Type.Text), // corresponsds to RoleTypes.tag
            new SchemaField("retired",          Type.Special_retired),
            new SchemaField("retiredById",      Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",  Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_ProjectMemberRole = new SchemaGenerator(
        "projectmemberrole",        // lower, all one word, singular
        "project_member_role",      // lower, with underscore, singular
        "ProjectMemberRoles",       // camel case, leading upper, plural
        "ProjectMemberRole",        // camel case, leading upper, singular
        "projectMemberRoles",       // camel case, leading lower, plural
        "projectMemberRole",        // camel case, leading lower, singular
        "Project Member Role",      // display name with spaces, singular
        "role",                     // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("projectId",        Type.Integer),
            new SchemaField("memberId",         Type.Text), // FK to App_User.Id (aka Members.memberId)
            new SchemaField("type",             Type.Text), // corresponsds to RoleTypes.tag
            new SchemaField("retired",          Type.Special_retired),
            new SchemaField("retiredById",      Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",  Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_Feedback = new SchemaGenerator(
        "feedback",     // lower, all one word, singular
        "feedback",     // lower, with underscore, singular
        "Feedback",     // camel case, leading upper, plural
        "Feedback",     // camel case, leading upper, singular
        "feedback",     // camel case, leading lower, plural
        "feedback",     // camel case, leading lower, singular
        "Feedback",     // display name with spaces, singular
        "feedback",     // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("feedbackId",       Type.PrimaryKey),
            new SchemaField("reMemberId",       Type.Text), // optional, member addressed by feedback, FK to App_User.Id (aka Members.memberId)
            new SchemaField("reAccountId",      Type.Integer), // optional, account addressed by feedback, FK to Accounts
            new SchemaField("reProjectId",      Type.Text), // optional, project addressed by feedback, FK to Parent.Id (aka Projects.projectId)
            new SchemaField("comments",         Type.Text),
            new SchemaField("editedById",       Type.Special_editedBy), // optional, member giving the feedback, FK to App_User.Id (aka Members.memberId), null for anonoymous
            new SchemaField("editedDateTime",   Type.Timestamp_created)
        }
    );

    public static final SchemaGenerator m_Category = new SchemaGenerator(
        "category",           // lower, all one word, singular
        "category",           // lower, with underscore, singular
        "Categories",         // camel case, leading upper, plural
        "Category",           // camel case, leading upper, singular
        "categories",         // camel case, leading lower, plural
        "category",           // camel case, leading lower, singular
        "Project Category",   // display name with spaces, singular
        "project",            // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("categoryId",           Type.PrimaryKey),
            new SchemaField("parentCategoryId",     Type.Integer),
            new SchemaField("name",                 Type.Text),
            new SchemaField("description",          Type.Text),
            new SchemaField("typeId",               Type.Integer),
            new SchemaField("categoryOrder",        Type.Integer),
            new SchemaField("categoryLevel",        Type.Integer),
            new SchemaField("editedById",           Type.Special_editedBy), // optional, member giving the feedback, FK to App_User.Id (aka Members.memberId), null for anonoymous
            new SchemaField("editedDateTime",       Type.Timestamp_created),
            new SchemaField("retired",              Type.Special_retired),
            new SchemaField("retiredById",          Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",      Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_ScreenShot = new SchemaGenerator(
        "screenshot",           // lower, all one word, singular
        "screen_shot",          // lower, with underscore, singular
        "ScreenShots",          // camel case, leading upper, plural
        "ScreenShot",           // camel case, leading upper, singular
        "screenShots",          // camel case, leading lower, plural
        "screenShot",           // camel case, leading lower, singular
        "Project Screen Shot",  // display name with spaces, singular
        "project",              // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("screenShotId",         Type.PrimaryKey),
            new SchemaField("projectId",            Type.Text), // FK to Projects.projectId
            new SchemaField("orderNumber",          Type.Integer),
            new SchemaField("assetId",              Type.Text), // FK to Asset.Id
            new SchemaField("description",          Type.Text),
            new SchemaField("editedById",           Type.Special_editedBy), // FK to App_User.Id (aka Members.memberId)
            new SchemaField("editedDateTime",       Type.Timestamp_updated),
            new SchemaField("retired",              Type.Special_retired),
            new SchemaField("retiredById",          Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",      Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_Publish = new SchemaGenerator(
        "publish",              // lower, all one word, singular
        "publish",              // lower, with underscore, singular
        "Published",            // camel case, leading upper, plural
        "Publish",              // camel case, leading upper, singular
        "published",            // camel case, leading lower, plural
        "publish",              // camel case, leading lower, singular
        "Project Published",    // display name with spaces, singular
        "project",              // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("publishId",            Type.PrimaryKey),
            new SchemaField("accountId",            Type.Integer),
            new SchemaField("projectId",            Type.Text),
            new SchemaField("firstPublishId",       Type.Integer),
            new SchemaField("versionMajor",         Type.Integer),
            new SchemaField("versionMinor",         Type.Integer),
            new SchemaField("versionDot",           Type.Integer),
            new SchemaField("versionBuildNumber",   Type.Integer),
            new SchemaField("categoryId",           Type.Integer),
            new SchemaField("name",                 Type.Text),
            new SchemaField("description",          Type.Text),
            new SchemaField("thumbnailId",          Type.Text),
            new SchemaField("status",               Type.Text),
            new SchemaField("publishType",          Type.Text),
            new SchemaField("licenseType",          Type.Text),
            new SchemaField("price",                Type.Integer),
            new SchemaField("currencyTypeId",       Type.Integer),
            new SchemaField("editedById",           Type.Special_editedBy), // FK to App_User.Id (aka Members.memberId)
            new SchemaField("editedDateTime",       Type.Timestamp_created),
            new SchemaField("retired",              Type.Special_retired),
            new SchemaField("retiredById",          Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",      Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_PublishLaunchCount = new SchemaGenerator(
        "publishlaunchcount",               // lower, all one word, singular
        "publish_launch_count",             // lower, with underscore, singular
        "PublishLaunchCounts",              // camel case, leading upper, plural
        "PublishLaunchCount",               // camel case, leading upper, singular
        "publishLaunchCounts",              // camel case, leading lower, plural
        "publishLaunchCount",               // camel case, leading lower, singular
        "Project Publish Launch Counts",    // display name with spaces, singular
        "project",                          // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("publishLaunchCountId", Type.PrimaryKey),
            new SchemaField("publishId",            Type.Integer),
            new SchemaField("parentPublishId",      Type.Integer),
            new SchemaField("launchedByAccountId",  Type.Integer)
        }
    );

    public static final SchemaGenerator m_CreditCard = new SchemaGenerator(
        "creditcard",            // lower, all one word, singular
        "credit_card",           // lower, with underscore, singular
        "CreditCards",           // camel case, leading upper, plural
        "CreditCard",            // camel case, leading upper, singular
        "creditCards",           // camel case, leading lower, plural
        "creditCard",            // camel case, leading lower, singular
        "Finance Credit Card",   // display name with spaces, singular
        "finance",               // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("creditCardId",             Type.PrimaryKey),
            new SchemaField("accountId",                Type.Integer),
            new SchemaField("addressId",                Type.Integer),
            new SchemaField("nameOnCreditCard",         Type.Text), // as used by CIM
            new SchemaField("maskedNumber",             Type.Text), // as used by CIM
            new SchemaField("expirationMonth",          Type.Text), // 01 to 12
            new SchemaField("expirationYear",           Type.Text), // yyyy
            new SchemaField("customerProfileId",        Type.Text), // as returned by CIM
            new SchemaField("customerPaymentProfileId", Type.Text), // as returned by CIM
            new SchemaField("editedById",               Type.Special_editedBy), // FK to App_User.Id (aka Members.memberId)
            new SchemaField("editedDateTime",           Type.Timestamp_created),
            new SchemaField("retired",                  Type.Special_retired),
            new SchemaField("retiredById",              Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",          Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_ECheck = new SchemaGenerator(
        "echeck",           // lower, all one word, singular
        "e_check",          // lower, with underscore, singular
        "EChecks",          // camel case, leading upper, plural
        "ECheck",           // camel case, leading upper, singular
        "eChecks",          // camel case, leading lower, plural
        "eCheck",           // camel case, leading lower, singular
        "Finance ECheck",   // display name with spaces, singular
        "finance",          // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("eCheckId",                 Type.PrimaryKey),
            new SchemaField("accountId",                Type.Integer),
            new SchemaField("bankRoutingNumber",        Type.Text),
            new SchemaField("bankAccountNumber",        Type.Text),
            new SchemaField("bankName",                 Type.Text),
            new SchemaField("bankAccountName",          Type.Text),
            new SchemaField("customerProfileId",        Type.Text), // as returned by CIM
            new SchemaField("customerPaymentProfileId", Type.Text), // as returned by CIM
            new SchemaField("editedById",               Type.Special_editedBy), // FK to App_User.Id (aka Members.memberId)
            new SchemaField("editedDateTime",           Type.Timestamp_created),
            new SchemaField("retired",                  Type.Special_retired),
            new SchemaField("retiredById",              Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",          Type.Timestamp)
        }
    );

    public static final SchemaGenerator m_FinancialTransaction = new SchemaGenerator(
        "financialtransaction",             // lower, all one word, singular
        "financial_transaction",            // lower, with underscore, singular
        "FinancialTransactions",            // camel case, leading upper, plural
        "FinancialTransaction",             // camel case, leading upper, singular
        "financialTransactions",            // camel case, leading lower, plural
        "financialTransaction",             // camel case, leading lower, singular
        "Finance Financial Transactions",   // display name with spaces, singular
        "finance",                          // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("financialTransactionId",       Type.PrimaryKey),
            new SchemaField("accountId",                    Type.Integer),
            new SchemaField("amount",                       Type.Integer),
            new SchemaField("perTransactionFee",            Type.Integer),
            new SchemaField("currencyTypeId",               Type.Integer),
            new SchemaField("label",                        Type.Text),
            new SchemaField("type",                         Type.Text),
            new SchemaField("agentId",                      Type.Integer),
            new SchemaField("cimTransactionId",             Type.Text),
            new SchemaField("status",                       Type.Text),
            new SchemaField("statusReasonCode",             Type.Integer),
            new SchemaField("statusReasonText",             Type.Text),
            //new SchemaField("transactionDateTime",          Type.Timestamp),
        }
    );

    public static final SchemaGenerator m_Invoice = new SchemaGenerator(
        "invoice",              // lower, all one word, singular
        "invoice",              // lower, with underscore, singular
        "Invoices",             // camel case, leading upper, plural
        "Invoice",              // camel case, leading upper, singular
        "invoices",             // camel case, leading lower, plural
        "invoice",              // camel case, leading lower, singular
        "Finance Invoice",      // display name with spaces, singular
        "finance",              // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("invoiceId",        Type.PrimaryKey),
            new SchemaField("accountId",        Type.Integer),
            new SchemaField("description",      Type.Text),
            new SchemaField("cachedTotal",      Type.Integer),
            new SchemaField("currencyTypeId",   Type.Integer),
            new SchemaField("state",            Type.Text),
            new SchemaField("holdingDateTime",  Type.Timestamp),
            new SchemaField("transactionId",    Type.Integer)
        }
    );

    public static final SchemaGenerator m_InvoiceLineItem = new SchemaGenerator(
        "invoicelineitem",              // lower, all one word, singular
        "invoice_line_item",            // lower, with underscore, singular
        "InvoiceLineItems",             // camel case, leading upper, plural
        "InvoiceLineItem",              // camel case, leading upper, singular
        "invoiceLineItems",             // camel case, leading lower, plural
        "invoiceLineItem",              // camel case, leading lower, singular
        "Finance Invoice Line Item",    // display name with spaces, singular
        "finance",                      // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("invoiceLineItemId",    Type.PrimaryKey),
            new SchemaField("invoiceId",            Type.Integer),
            new SchemaField("lineNumber",           Type.Integer),
            new SchemaField("quantity",             Type.Integer),
            new SchemaField("amountTotal",          Type.Integer),
            new SchemaField("description",          Type.Text),
            new SchemaField("publishId",            Type.Integer) // FK to Published.publishId (or null if something else)
        }
    );

    public static final SchemaGenerator m_Payment = new SchemaGenerator(
        "payment",              // lower, all one word, singular
        "payment",              // lower, with underscore, singular
        "Payments",             // camel case, leading upper, plural
        "Payment",              // camel case, leading upper, singular
        "payments",             // camel case, leading lower, plural
        "payment",              // camel case, leading lower, singular
        "Finance Payment",      // display name with spaces, singular
        "finance",              // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("paymentId",        Type.PrimaryKey),
            new SchemaField("accountId",        Type.Integer),
            new SchemaField("cachedTotal",      Type.Integer),
            new SchemaField("currencyTypeId",   Type.Integer),
            new SchemaField("state",            Type.Text),
            new SchemaField("holdingDateTime",  Type.Timestamp),
            new SchemaField("transactionId",    Type.Integer)
        });

    public static final SchemaGenerator m_PaymentLineItem = new SchemaGenerator(
        "paymentlineitem",              // lower, all one word, singular
        "payment_line_item",            // lower, with underscore, singular
        "PaymentLineItems",             // camel case, leading upper, plural
        "PaymentLineItem",              // camel case, leading upper, singular
        "paymentLineItems",             // camel case, leading lower, plural
        "paymentLineItem",              // camel case, leading lower, singular
        "Finance Payment Line Item",    // display name with spaces, singular
        "finance",                      // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("paymentLineItemId",   Type.PrimaryKey),
            new SchemaField("paymentId",           Type.Integer),
            new SchemaField("lineNumber",          Type.Integer),
            new SchemaField("quantity",            Type.Integer),
            new SchemaField("amountTotal",         Type.Integer),
            new SchemaField("description",         Type.Text),
            new SchemaField("publishId",           Type.Integer) // FK to Published.publishId (or null if something else)
        }
    );

    public static final SchemaGenerator m_AccountProjectNumber = new SchemaGenerator(
        "accountprojectnumber",             // lower, all one word, singular
        "account_project_number",           // lower, with underscore, singular
        "AccountProjectNumbers",            // camel case, leading upper, plural
        "AccountProjectNumber",             // camel case, leading upper, singular
        "accountProjectNumbers",            // camel case, leading lower, plural
        "accountProjectNumber",             // camel case, leading lower, singular
        "Project Account Project Number",   // display name with spaces, singular
        "project",                          // folder, singular
        true,
        new SchemaField[] {
            new SchemaField("accountProjectNumberId",   Type.PrimaryKey),
            new SchemaField("accountId",                Type.Integer),
            new SchemaField("publishId",                Type.Integer),
            new SchemaField("invoiceLineItemId",        Type.Integer),
            new SchemaField("retired",                  Type.Special_retired),
            new SchemaField("retiredById",          Type.Text), // FK to App_User.Id (aka Members.memberId)
            //new SchemaField("retiredDateTime",      Type.Timestamp),
            new SchemaField("retiredReasonId",          Type.Integer), // FK to RetiredReasonTypes.retiredReasonTypeId
        }
    );

    public static final SchemaGenerator m_CouponCodeType = new SchemaGenerator(
            "couponcodetype",             // lower, all one word, singular
            "coupon_code_type",           // lower, with underscore, singular
            "CouponCodeTypes",            // camel case, leading upper, plural
            "CouponCodeType",             // camel case, leading upper, singular
            "couponCodeTypes",            // camel case, leading lower, plural
            "couponCodeType",             // camel case, leading lower, singular
            "Coupon Code Type",   // display name with spaces, singular
            "coupon",                          // folder, singular
            true,
            new SchemaField[] {
                new SchemaField("couponCodeTypeId",   Type.PrimaryKey),
                new SchemaField("description",  Type.Text)
            }
        );
    
    public static final SchemaGenerator m_CouponCodes = new SchemaGenerator(
            "couponcode",             // lower, all one word, singular
            "coupon_code",           // lower, with underscore, singular
            "CouponCodes",            // camel case, leading upper, plural
            "CouponCode",             // camel case, leading upper, singular
            "couponCodes",            // camel case, leading lower, plural
            "couponCode",             // camel case, leading lower, singular
            "Coupon Codes",   // display name with spaces, singular
            "coupon",                          // folder, singular
            true,
            new SchemaField[] {
                new SchemaField("couponId",   Type.PrimaryKey),
                new SchemaField("code",  Type.Text),
                new SchemaField("couponCodeTypeId", Type.Text), // FK
                new SchemaField("retiredDateTime", Type.Special_retired)
            }
        );

    public static final SchemaGenerator m_ShareTypes = new SchemaGenerator(
            "sharetype",             // lower, all one word, singular
            "share_type",           // lower, with underscore, singular
            "ShareTypes",            // camel case, leading upper, plural
            "ShareType",             // camel case, leading upper, singular
            "ShareTypes",            // camel case, leading lower, plural
            "shareType",             // camel case, leading lower, singular
            "Share Type",   		// display name with spaces, singular
            "project",                          // folder, singular
            true,
            new SchemaField[] {
                new SchemaField("shareTypeId",   Type.PrimaryKey),
                new SchemaField("tag",                          Type.Text),
                new SchemaField("name",                         Type.Text),
                new SchemaField("displayOrder",                 Type.Integer),
                new SchemaField("updatedDateTime",              Type.Timestamp_updated),
            }
    	);
    
    public static final SchemaGenerator m_SharedProjects = new SchemaGenerator(
            "sharedproject",             // lower, all one word, singular
            "shared_project",           // lower, with underscore, singular
            "SharedProjects",            // camel case, leading upper, plural
            "SharedProject",             // camel case, leading upper, singular
            "sharedProjects",            // camel case, leading lower, plural
            "sharedProject",             // camel case, leading lower, singular
            "Shared Project",   // display name with spaces, singular
            "project",                          // folder, singular
            true,
            new SchemaField[] {
                new SchemaField("sharedProjectId",   Type.PrimaryKey),
                new SchemaField("publishId",                Type.Integer),
                new SchemaField("retired",                  Type.Special_retired),
                new SchemaField("updatedDateTime",              Type.Timestamp_updated),
                new SchemaField("retiredById",          Type.Text), // FK to App_User.Id (aka Members.memberId)
                new SchemaField("retiredDateTime",      Type.Timestamp),
                new SchemaField("retiredReasonId",          Type.Integer), // FK to RetiredReasonTypes.retiredReasonTypeId
            }
       );
    
    public static final SchemaGenerator m_GuestList = new SchemaGenerator(
            "guestlist",             // lower, all one word, singular
            "guest_list",           // lower, with underscore, singular
            "GuestLists",            // camel case, leading upper, plural
            "GuestList",             // camel case, leading upper, singular
            "guestLists",            // camel case, leading lower, plural
            "guestList",             // camel case, leading lower, singular
            "Guest List",   		// display name with spaces, singular
            "project",                          // folder, singular
            true,
            new SchemaField[] {
                new SchemaField("guestListId",              Type.PrimaryKey),
                new SchemaField("retired",                  Type.Special_retired),
                new SchemaField("updatedDateTime",          Type.Timestamp_updated),
                new SchemaField("retiredById",          	Type.Text), // FK to App_User.Id (aka Members.memberId)
                new SchemaField("retiredDateTime",      	Type.Timestamp),
                new SchemaField("retiredReasonTypeId",      Type.Integer), // FK to RetiredReasonTypes.retiredReasonTypeId
                new SchemaField("emailText",          		Type.Text) // FK to RetiredReasonTypes.retiredReasonTypeId
            }
       );

    public static final SchemaGenerator m_GuestListInvitees = new SchemaGenerator(
            "guestlistinvitee",             // lower, all one word, singular
            "guest_list_invitee",           // lower, with underscore, singular
            "GuestListInvitees",            // camel case, leading upper, plural
            "GuestListInvitee",             // camel case, leading upper, singular
            "guestListInvitees",            // camel case, leading lower, plural
            "guestListInvitee",             // camel case, leading lower, singular
            "Guest List Invitee",   // display name with spaces, singular
            "project",                          // folder, singular
            true,
            new SchemaField[] {
                new SchemaField("guestListInviteeId",   Type.PrimaryKey),
                new SchemaField("guestListId",          Type.Integer),
                new SchemaField("email",                Type.Text),
                new SchemaField("retired",              Type.Special_retired),
                new SchemaField("updatedDateTime",      Type.Timestamp_updated),
                new SchemaField("retiredById",          Type.Text), // FK to App_User.Id (aka Members.memberId)
                new SchemaField("retiredDateTime",      Type.Timestamp),
                new SchemaField("retiredReasonTypeId",  Type.Integer), // FK to RetiredReasonTypes.retiredReasonTypeId
                new SchemaField("uuid",					Type.Text),
                new SchemaField("firstName",			Type.Text),
                new SchemaField("lastName",				Type.Text),
            }
       );
    
    
    public static final SchemaGenerator m_ProductCodes = new SchemaGenerator(
            "productcode",      // lower, all one word, singular
            "product_code",     // lower, with underscore, singular
            "ProductCodes",     // camel case, leading upper, plural
            "ProductCode",      // camel case, leading upper, singular
            "productCodes",     // camel case, leading lower, plural
            "productCodes",      // camel case, leading lower, singular
            "Product Code",     // display name with spaces, singular
            "productcode",          // folder, singular
            true,
            new SchemaField[] {
                new SchemaField("productCodeId",        Type.PrimaryKey),
                new SchemaField("tag",                  Type.Text),
                new SchemaField("name",                 Type.Text),
                new SchemaField("description",          Type.Text),
                new SchemaField("displayOrder",         Type.Integer),
                new SchemaField("retired",              Type.Special_retired),
                new SchemaField("retiredById",          Type.Text), // FK to App_User.Id (aka Members.memberId)
                new SchemaField("retiredDateTime",      Type.Timestamp)
            }
        );

    public static final SchemaGenerator m_AllSchemas[] = {
        //m_AppUser, // java and sql has been customized
        m_Company,
        m_AccountType,
        m_Account, // sql has been customized to create account admin and handle text in_type
        m_Address, // sql has been customized to handle text in_type
        m_Telephone, // sql has been customized to handle text in_type
        m_RoleType,
        m_AccountMemberRole, // java and sql has been customized
        m_ProjectMemberRole, // java and sql has been customized
        m_Feedback,
        m_Category,
        m_ScreenShot,
        m_Publish,
        m_PublishLaunchCount,
        m_CreditCard,
        m_ECheck,
        m_FinancialTransaction,
        m_Invoice,
        m_InvoiceLineItem,
        m_Payment,
        m_PaymentLineItem,
        m_AccountProjectNumber,
        m_ProductCodes
    };

    public static void checklist(SchemaTemplate schemaTemplate) {

        String nameLower        = schemaTemplate.getNameLower();
        String nameUscore       = schemaTemplate.getNameUScore();
        String nameCamelUpper   = schemaTemplate.getNameCamelUpper();
        String nameCamelLower   = schemaTemplate.getNameCamelLower();

        System.out.println("----------------------------------------------");
        System.out.println("Copy/Replace:  \"invoicelineitems\" to \"" + nameLower + "\"");
        System.out.println("Copy/Replace:  \"invoice_line_items\" to \"" + nameUscore + "\"");
        System.out.println("Copy/Replace:  \"InvoiceLineItems\" to \"" + nameCamelUpper + "\"");
        System.out.println("Copy/Replace:  \"invoiceLineItems\" to \"" + nameCamelLower + "\"");
        System.out.println(" _");
        System.out.println("|_| CREATE /src/database/fn_" + nameLower + ".sql");
        System.out.println("|_| ADD \"" + "\\i fn_" + nameLower + ".sql" + "\" to: \"/src/database/all_functions.sql\"");
        System.out.println("|_| RUN \"" + "fn_" + nameLower + ".sql" + "\" from Postgress Client");
        System.out.println(" _");
        System.out.println("|_| CREATE /src/java/com/alleni/invoicelineitems/" + nameCamelUpper + ".java");
        System.out.println("|_| CREATE /src/java/com/alleni/invoicelineitems/" + nameCamelUpper + "Dao.java");
        System.out.println("|_| ADD Error lines to \"/common/jsonError.java\"");
        System.out.println(" _");
        System.out.println("|_| CREATE /web/services/" + nameLower + "/create.jsp");
        System.out.println("|_| CREATE /web/services/" + nameLower + "/find.jsp");
        System.out.println("|_| CREATE /web/services/" + nameLower + "/filter.jsp");
        System.out.println("|_| CREATE /web/services/" + nameLower + "/retire.jsp");
        System.out.println("|_| CREATE /web/services/" + nameLower + "/update.jsp");
        System.out.println("|_| DELETE SPURIOUS \"import\" statement.  Netbeans adds then when copying a folder");
        System.out.println(" _");
        System.out.println("|_| ADD lines to: /web/testbench/tests.js");
        System.out.println(" _");
        System.out.println("|_| CREATE /web/testbench/tests/" + nameCamelLower + "CreateUpdate.html, |_| Human Readable Group Name");
        System.out.println("|_| CREATE /web/testbench/tests/" + nameCamelLower + "Filter.html, |_| Human Readable Group Name");
        System.out.println("|_| CREATE /web/testbench/tests/" + nameCamelLower + "FindReture.html, |_| Human Readable Group Name");
        System.out.println();
        System.out.println("TEST running website for \""+ SchemaTemplate.toFriendlyName(nameUscore, "Managed Resource", "ID") + "\"");
        System.out.println("|_|  Create");
        System.out.println("|_|  Find");
        System.out.println("|_|  Filter");
        System.out.println("|_|  Retire");
        System.out.println("|_|  Update");
        System.out.println("====================================================");
    }

    public static void generateAllChecklists() {
        for (SchemaTemplate     st : m_AllSchemas) {
            checklist(st);
            System.out.println("=============================================");
        }
    }

    public static void main(String args[]) {
        try {
//            m_ZappRating.createAllFiles();
//            m_ZappComment.createAllFiles();
//            m_ZappRatingCache.createAllFiles();

//            m_CreditCardType.createAllFiles();
//            m_ContactPreferenceType.createAllFiles();

//            m_Address.createAllFiles();

//            m_ZappComment.createAllFiles();
        	m_ProductCodes.createAllFiles();
        }
        catch(Exception e) {
            System.out.println("Got exception: " + e);
        }
    }
}
