
-- fn_invoicelineitems.sql
--
-- Allen Interactions Copyright 2011
--
-- Create PostgreSQL plpgsql database functions for base database
-- tables.
--
---------------------------------------------------------------------

-- Use InvoiceLineItems as a definition of the returned data record.
-- It never actually has data in it.
DROP TABLE IF EXISTS InvoiceLineItems CASCADE;
CREATE TABLE InvoiceLineItems (
    LIKE    ManagedServiceOffering,
    managed_service_name	text,
    service_provider_name   text,
    theater_name            text,
    subregion_name          text,
    service_state_name      text
);

---------------------------------------------------------------------
-- Function: fn_invoicelineitem_filter(integer, integer, /*{{*/text, text, integer, integer, text, text/*}}*/, text)

-- DROP FUNCTION fn_invoicelineitem_filter(integer, integer, /*{{*/text, text, integer, integer, text, text/*}}*/, text);

CREATE OR REPLACE FUNCTION fn_invoicelineitem_filter(in_count integer, in_offset integer, /*{{*/in_short_name text, in_long_name text, in_display_order integer, in_visibility integer, in_definition text, in_color text/*}}*/, in_param_list text)
  RETURNS SETOF InvoiceLineItems AS
$BODY$
    DECLARE
/*{{*/
        all_short_name boolean := true;
        all_long_name boolean := true;
        all_display_order boolean := true;
        all_visibility boolean := true;
        all_definition boolean := true;
        all_color boolean := true;
/*}}*/
        rec InvoiceLineItems%ROWTYPE;
        param_name text;
        var_count int := 1;

    Begin
/*{{*/
        BEGIN
            CREATE TEMPORARY TABLE tmp_tbl_invoiceLineItemsId (
                invoiceLineItemsId  int
            );
        EXCEPTION
            WHEN DUPLICATE_TABLE THEN
                TRUNCATE TABLE tmp_tbl_invoiceLineItemsId;
        END;
/*}}*/
        LOOP
            param_name := split_part(in_param_list, ',', var_count);
            param_name := trim(both ' ' from param_name);
            EXIT WHEN param_name = '';

/*{{*/
            IF param_name = 'in_short_name' THEN
                all_short_name := false;
            ELSEIF param_name = 'in_long_name' THEN
                all_long_name := false;
            ELSEIF param_name = 'in_display_order' THEN
                all_display_order := false;
            ELSEIF param_name = 'in_visibility' THEN
                all_visibility := false;
            ELSEIF param_name = 'in_definition' THEN
                all_definition := false;
            ELSEIF param_name = 'in_color' THEN
                all_color := false;
            END IF;
/*}}*/
            var_count := var_count + 1;
        END LOOP;

        var_count := in_count;
        IF var_count <= 0 THEN
            -- Get the count from InvoiceLineItems, not from InvoiceLineItemsView
            -- since the former is the only table that actually has data.
            SELECT count(*) INTO var_count FROM InvoiceLineItems;
        END IF;

        FOR rec IN
            SELECT mso.*,
                ms.name AS managed_service_name,
                sp.name AS service_provider_name,
                t.short_name AS theater_name,
                sr.name AS subregion_name,
                ss.service_state_name AS service_state_name
                -- The table entries most likely exist, but use an outer join
                -- just in case. If they exist, the initial join will find them
                -- so there won't be any extra overhead. And if the records for
                -- some reason don't exist in the other tables, we'll still get
                -- the required data, just not the names.
            FROM InvoiceLineItems mso LEFT OUTER JOIN ManagedService ms USING (managed_service_id)
                                            LEFT OUTER JOIN ServiceState ss USING (service_state_id)
                                            LEFT OUTER JOIN Subregion sr USING (subregion_id),
                 ServiceProvider sp         LEFT OUTER JOIN Theater t ON (sp.hq_id = t.theater_id)
            WHERE mso.service_provider_id = sp.service_provider_id
                AND mso.retired = 0
/*{{*/
                AND (all_short_name OR short_name ILIKE '%' || in_short_name || '%')
                AND (all_long_name OR long_name ILIKE '%' || in_long_name || '%')
                AND (all_display_order OR display_order = in_display_order)
                AND (all_visibility OR visibility = in_visibility)
                AND (all_definition OR definition ILIKE '%' || in_definition || '%')
                AND (all_color OR color ILIKE '%' || in_color || '%')
/*}}*/
            ORDER BY  ms.sort_order, t.sort_order, t.short_name, service_provider_name
            LIMIT var_count OFFSET in_offset
        LOOP
            RETURN NEXT rec;
        END LOOP;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_invoicelineitem_filter(integer, integer, /*{{*/text, text, integer, integer, text, text/*}}*/, text) OWNER TO postgres;

---------------------------------------------------------------------
-- Function: fn_invoicelineitem_find(integer)

-- DROP FUNCTION fn_invoicelineitem_find(integer);

CREATE OR REPLACE FUNCTION fn_invoicelineitem_find(in_invoiceLineItemsId integer)
  RETURNS SETOF InvoiceLineItems AS
$BODY$
    DECLARE
        rec InvoiceLineItems%ROWTYPE;

    BEGIN
        FOR rec IN
            SELECT mso.*,
                ms.name AS managed_service_name,
                sp.name AS service_provider_name,
                t.short_name AS theater_name,
                sr.name AS subregion_name,
                ss.service_state_name AS service_state_name
                -- The table entries most likely exist, but use an outer join
                -- just in case. If they exist, the initial join will find them
                -- so there won't be any extra overhead. And if the records for
                -- some reason don't exist in the other tables, we'll still get
                -- the required data, just not the names.
            FROM InvoiceLineItems mso LEFT OUTER JOIN ManagedService ms USING (managed_service_id)
                                            LEFT OUTER JOIN ServiceState ss USING (service_state_id)
                                            LEFT OUTER JOIN Subregion sr USING (subregion_id),
                 ServiceProvider sp         LEFT OUTER JOIN Theater t ON (sp.hq_id = t.theater_id)
            WHERE mso.service_provider_id = sp.service_provider_id
                AND invoiceLineItemsId = in_invoiceLineItemsId
        LOOP
            RETURN NEXT rec;
        END LOOP;
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100
  ROWS 1000;
-- ALTER FUNCTION fn_invoicelineitem_find(integer) OWNER TO postgres;
