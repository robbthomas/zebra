package com.qp.SchemaHelper;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class __Template__ extends BaseBean implements Serializable
{
    static private __TemplateDao__ _dao() { return new __TemplateDao__(); }

    protected __Template__(JSONObject daoJson) throws JSONException
    {
/*{{*/
        this.jsonValues.put("id", daoJson.has("invoiceLineItemId" ) ? daoJson.getInt("invoiceLineItemId") : -1);
        this.jsonValues.put("shortName", daoJson.has("short_name") ? daoJson.getString("short_name") : "");
        this.jsonValues.put("longName", daoJson.has("long_name") ? daoJson.getString("long_name") : "");
        this.jsonValues.put("displayOrder", daoJson.has("display_order") ? daoJson.getInt("display_order") : -1);
        this.jsonValues.put("visibility", daoJson.has("visibility") ? daoJson.getInt("visibility") : -1);
        this.jsonValues.put("definition", daoJson.has("definition") ? daoJson.getString("definition") : "");
        this.jsonValues.put("color", daoJson.has("color") ? daoJson.getString("color") : "");
        this.jsonValues.put("created", daoJson.has("created_date_time") ? (Timestamp)daoJson.get("created_date_time") : new Timestamp(0));
        this.jsonValues.put("updated", daoJson.has("updated_date_time") ? (Timestamp)daoJson.get("updated_date_time") : null);
        this.jsonValues.put("editedByMemberId", daoJson.has("edited_by_member_id") ? daoJson.getInt("edited_by_member_id") : -1);
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
/*}}*/
    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("__TEMPLATE__.create - could not create InvoiceLineItems record");

        return success("invoiceLineItemId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("__TEMPLATE__.update - could not update InvoiceLineItems record");

        return success("invoiceLineItemId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("__TEMPLATE__.retire - could not retire InvoiceLineItems record");

        return success("invoiceLineItemId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("invoiceLineItemId");
        ArrayList __template__List = _dao().find(id, credentialValues);

        if (__template__List.size() < 1)
            return error("__TEMPLATE__.find - could not find InvoiceLineItems id "+id);
        if (__template__List.size() > 1)
            return error("__TEMPLATE__.find - found multiple InvoiceLineItems records for InvoiceLineItems id "+id);

        __Template__ __template__ = new __Template__((JSONObject)__template__List.get(0));
        if (__template__ == null)
            return error("__TEMPLATE__.find - __template__==null for id "+id);

        return success(__template__.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList __template__List = _dao().findAll(credentialValues);
        return jsonFoundList(__template__List);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of InvoiceLineItems json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList __template__List = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(__template__List, limit);

        JSONObject jsonList = jsonFoundList(__template__List);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            __Template__ __template__ = new __Template__((JSONObject)foundList.get(j));
            if (__template__ == null)
                return JsonError.jsonError(JsonError.Error.__TEMPLATE___FIND, "Could not make InvoiceLineItems object " + j);
            JSONObject json = new JSONObject(__template__.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("__template__", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        __TEMPLATE___CREATE,
        __TEMPLATE___RETIRE,
        __TEMPLATE___FIND,
        __TEMPLATE___UPDATE,

 */