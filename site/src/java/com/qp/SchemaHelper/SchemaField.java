/*
 * Created February 12, 2010
 * Ben Resner - ben@qualityprocess.com
 *
 * Holds information about a field we may want to use in a table.
 */

package com.qp.SchemaHelper;

/**
 *
 * @author benres
 */
public class SchemaField {

    private final String m_Name;
    private final Type m_Type;



    public SchemaField(String name, Type type) {
        m_Name = name;
        m_Type = type;
    }

    public Type getType() {
        return m_Type;
    }

    public String getName() {
        return m_Name;
    }

    public String getNameCamel() {
        return SchemaTemplate.toCamelCase(getName());
    }

    public String getNameFriendly() {
        return SchemaTemplate.toFriendlyName(getName(), "MS", "Id");
    }

    enum Type {

        Text("text"),
        Integer("integer"),
        IntegerArray("integer[]"),
        PrimaryKey,
        Timestamp("timestamp"),
        Timestamp_created,
        Timestamp_updated,
        Special_editedBy("text"),
        Special_retired("integer");

        private final String m_Label;

        private Type() {
            m_Label = "";
        }

        private Type(String label) {
            m_Label = label;
        }

        public String getLabel() {
            return m_Label;
        }
    };
}
