/*
 * Created February 12, 2010
 * Ben Resner - ben@qualityprocess.com
 *
 * Contains the tools to generate the files for new database tables.  Below is an
 * example of the nine generated files to support the MSStage table.  Other tables
 * follow a similar pattern, but with a different name (e.g. MSResource).  Also
 * listed are the templates for each file which serve as the source.
 *
 * The code reads the template file and replaces code snippets inside the escape
 * blocks with generated snippets.  The code also replaces text with "MSStage" that
 * is outside escape blocks.
 *
 * Here's the nine generated files:
 *
 (1)  src/database/fn_invoicelineitem.sql                       TEMPLATE:  fn_template.sql
 *
 * SQL File.  This code must be executed in the SQL database.
 * Also, need to add "\i fn_invoicelineitem.sql" to "all_functions.sql"
 *
 (2)  src/java/com/alleni/managedservice/MSStage.java    TEMPLATE:  __Template__.java
 (3)  src/java/com/alleni/managedservice/MSStageDao.java TEMPLATE:  __TemplateDao__.java
 *
 * These are the java objects for interacting with the database.  At the bottom
 * of the file "MSStage.java" is a small chunk of commented code that goes in
 * com.alleni.common.jsonError.java.  You'll get errors without this code.
 * 
 (4)  web/services/invoicelineitem/create.jsp               TEMPLATE:  invoicelineitem/create.jsp
 (5)  web/services/invoicelineitem/find.jsp                 TEMPLATE:  invoicelineitem/find.jsp
 (6)  web/services/invoicelineitem/filter.jsp               TEMPLATE:  invoicelineitem/filter.jsp
 (7)  web/services/invoicelineitem/retire.jsp               TEMPLATE:  invoicelineitem/retiore.jsp
 (8)  web/services/invoicelineitem/update.jsp               TEMPLATE:  invoicelineitem/update.jsp
 * (Note:  These use original invoicelineitem/... files not a separte template like the
 * other files.)
 *
 (9)  web/testbench/tests/CreateUpdate.html             TEMPLATE: /tests/template_CreateUpdate.html
 (10) web/testbench/tests/filter.html               TEMPLATE: /tests/template_filter.html
 (11) web/testbench/tests/FindRetire.html               TEMPLATE: /tests/template_FindRetire.html
 *
 * HTML files for testing database objects.  SchemaTemplate also generates a
 * short text file that must be inserted into "tests.js".
 *
 *
 * --------------- ARCHITECTURE NOTES -------------
 *
 * This code tends to be more flexible and not throw errors if something is
 * not perfect.  For example, some tables don't have a "created_date_time" and/or
 * "edited_date_time".  This code is happy to omit these fields rather than
 * create an error.  This behavior is easy to change.
 *
 * This code also tries to balance the ability to make changes to the script
 * template with not getting too complex.  In general, new files are created
 * from template files that contain escape chunks demarking code that is replaced
 * for each new file.  I've tried to avoid putting template code in .java files.
 *
 * This code tries not to give any advantage to shortcuts.  This means you reparse
 * ALL files to reparse ANY file.  This will alert you very early if there's any
 * problems with the parser failing to correctly generate files it previously handled
 * without any error.  Netbeans does a good job of updating the status of changed
 * files so you'll quickly see if something is amiss.
 *
 * --------------- ADDING NEW TABLES ---------------
 *
 (1) Create the new table in "SchemaHelper.java".  Use existing tables as a
 * reference.  There is currently no tool for automating this process by reading,
 * for example, a RX->RY file, but this could be written
 *
 (2) Run the code in SchemaParser.main().  As mentioned above, please regenerate
 * all files and not just the ones you need.  We want to avoid the situation where
 * code generation is incremental and can't easily be redone from scratch.  This
 * is important if we need to reparse everything to accomodate an updated
 * naming convention or some other global change.
 *
 */

package com.qp.SchemaHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.qp.SchemaHelper.SchemaField.Type;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


public class SchemaGenerator extends SchemaTemplate {

    private Map<String, String>     m_ReplacementMap = new HashMap<String, String>();
    private Map<String, Integer>    m_ReplacementMapUse = new HashMap<String, Integer>();
    private List<String>            m_UnmatchedTokenList = new ArrayList<String>();

    public SchemaGenerator(String nameLower, String nameUScore, String nameCamelUpperPlural, String nameCamelUpper, String nameCamelLower, String nameCamelLowerSingle, String nameFull, String folderName, Boolean isUpdateable, SchemaField fieldList[]) {
        super(nameLower, nameUScore, nameCamelUpperPlural, nameCamelUpper, nameCamelLower, nameCamelLowerSingle, nameFull, folderName, isUpdateable, fieldList);

        buildReplaceHashmap();
    }

    /**
     * This replaces the "easy" terms with terms specific for the target SchemaTemplate.
     * Note the usage of this routine is careful NOT to replace search terms inside
     * escape blocks.  This only replaces terms OUTSIDE escape blocks.
     *
     * The "hard" blocks to replace are handled below.
     * 
     * @param sb
     * @return
     */
    private String replaceMSStage(StringBuilder sb) {

        String  string = sb.toString();

        string = string.replaceAll("com.qp.SchemaHelper", "com.alleni." + m_FolderName);
        string = string.replaceAll("finance",             m_FolderName);
        string = string.replaceAll("invoicelineitem",     m_NameLower);
        string = string.replaceAll("InvoiceLineItems",    m_NameCamelUpperPlural); // used to name Java class, should be singular version of the name
        string = string.replaceAll("InvoiceLineItem",     m_NameCamelUpper); // used to name Java class, should be singular version of the name
        string = string.replaceAll("invoice_line_item",   m_NameUScore);
        string = string.replaceAll("invoiceLineItems",    m_NameCamelLower);
        string = string.replaceAll("invoiceLineItem",     m_NameCamelLowerSingle);
        string = string.replaceAll("INVOICELINEITEM",     m_NameCamelLowerSingle.toUpperCase());
        string = string.replaceAll("__Template__",        m_NameCamelUpper);
        string = string.replaceAll("__template__",        m_NameCamelLower);
        string = string.replaceAll("__TEMPLATE__",        m_NameCamelLowerSingle.toUpperCase());
        string = string.replaceAll("__TemplateDao__",     m_NameCamelUpper + "Dao");
        string = string.replaceAll("__TemplateView__",    m_NameCamelUpper);
        string = string.replaceAll("__templateView__",    m_NameCamelLower);
        string = string.replaceAll("__TEMPLATEVIEW__",    m_NameCamelLowerSingle.toUpperCase());
        string = string.replaceAll("__TemplateViewDao__", m_NameCamelUpper + "Dao");
        string = string.replaceAll("finance invoice line items",   getNameFullLower());
        string = string.replaceAll("Finance Invoice Line Items",   getNameFull());

        return string;
    }

    /**
     * This routine builds the hash map where escaped chunks are replaced with code for this SchemaTable.
     * If new files are added to the generator, any new terms need to be placed here.
     */
    private void buildReplaceHashmap() {

        SchemaField.Type    intTextOnly[] = new SchemaField.Type[] {SchemaField.Type.Integer, SchemaField.Type.IntegerArray, SchemaField.Type.Text, SchemaField.Type.Timestamp};
        SchemaField.Type    intTextEditedBy[] = new SchemaField.Type[] {SchemaField.Type.Special_editedBy, SchemaField.Type.Integer, SchemaField.Type.Text, SchemaField.Type.Timestamp};

        // #### arguments lists for SQL
        m_ReplacementMap.put("/*{{*/text, text, integer, integer, text, text, integer/*}}*/",
            generateNameTypeList(true, false, ", ", "", "", intTextEditedBy));

        m_ReplacementMap.put("/*{{*/text, text, integer, integer, text, text/*}}*/",
            generateNameTypeList(true, false, ", ", "", "", intTextOnly));

        m_ReplacementMap.put("/*{{*/in_short_name text, in_long_name text, in_display_order integer, in_visibility integer, in_definition text, in_color text, in_edited_by_member_id integer/*}}*/",
            generateNameTypeList(true, true, ", ", "in_", "", intTextEditedBy));

        m_ReplacementMap.put("/*{{*/in_short_name text, in_long_name text, in_display_order integer, in_visibility integer, in_definition text, in_color text/*}}*/",
            generateNameTypeList(true, true, ", ", "in_", "", intTextOnly));

        m_ReplacementMap.put("/*{{*/\n        all_short_name boolean := true;\n        all_long_name boolean := true;\n        all_display_order boolean := true;\n        all_visibility boolean := true;\n        all_definition boolean := true;\n        all_color boolean := true;\n/*}}*/",
            generateNameTypeList(false, true, "\n", "        all_", " boolean := true;", intTextOnly));

        m_ReplacementMap.put("/*{{*/\n            short_name,\n            long_name,\n            display_order,\n            visibility,\n            definition,\n            color,\n            edited_by_member_id\n/*}}*/",
            generateNameTypeList(false, true, ",\n", "            ", "", intTextEditedBy));

        m_ReplacementMap.put("/*{{*/\n            in_short_name,\n            in_long_name,\n            in_display_order,\n            in_visibility,\n            in_definition,\n            in_color,\n            in_edited_by_member_id\n/*}}*/",
            generateNameTypeList(false, true, ",\n", "            in_", "", intTextEditedBy));


        // #### Temporary table for handling multiple-value selects
        m_ReplacementMap.put("/*{{*/\n        BEGIN\n            CREATE TEMPORARY TABLE tmp_tbl_invoiceLineItemId (\n                invoiceLineItemId  int\n            );\n        EXCEPTION\n            WHEN DUPLICATE_TABLE THEN\n                TRUNCATE TABLE tmp_tbl_invoiceLineItemId;\n        END;\n/*}}*/",
                generateSQL_tempTable());

        // #### IF-THEN lists for SQL
        m_ReplacementMap.put("/*{{*/\n            IF param_name = 'in_short_name' THEN\n                rec.short_name := in_short_name;\n            ELSEIF param_name = 'in_long_name' THEN\n                rec.long_name := in_long_name;\n            ELSEIF param_name = 'in_display_order' THEN\n                rec.display_order := in_display_order;\n            ELSEIF param_name = 'in_visibility' THEN\n                rec.visibility := in_visibility;\n            ELSEIF param_name = 'in_definition' THEN\n                rec.definition := in_definition;\n            ELSEIF param_name = 'in_color' THEN\n                rec.color := in_color;\n            ELSEIF param_name = 'in_edited_by_member_id' THEN\n                rec.edited_by_member_id := in_edited_by_member_id;\n            END IF;\n/*}}*/",
            generateSQL_IFTHEN());

        m_ReplacementMap.put("/*{{*/\n            IF param_name = 'in_short_name' THEN\n                all_short_name := false;\n            ELSEIF param_name = 'in_long_name' THEN\n                all_long_name := false;\n            ELSEIF param_name = 'in_display_order' THEN\n                all_display_order := false;\n            ELSEIF param_name = 'in_visibility' THEN\n                all_visibility := false;\n            ELSEIF param_name = 'in_definition' THEN\n                all_definition := false;\n            ELSEIF param_name = 'in_color' THEN\n                all_color := false;\n            END IF;\n/*}}*/",
            generateSQL_IFTHEN_v2());

        m_ReplacementMap.put("/*{{*/\n            IF filter_name = 'in_short_name' THEN\n                all_short_name := false;\n            ELSEIF filter_name = 'in_long_name' THEN\n                all_long_name := false;\n            ELSEIF filter_name = 'in_display_order' THEN\n                all_display_order := false;\n            ELSEIF filter_name = 'in_visibility' THEN\n                all_visibility := false;\n            ELSEIF filter_name = 'in_definition' THEN\n                all_definition := false;\n            ELSEIF filter_name = 'in_color' THEN\n                all_color := false;\n            END IF;\n/*}}*/",
            generateSQL_IFTHEN_v3());

        // #### Other SQL LIsts
        m_ReplacementMap.put("/*{{*/\n            short_name = rec.short_name,\n            long_name = rec.long_name,\n            display_order = rec.display_order,\n            visibility = rec.visibility,\n            definition = rec.definition,\n            color = rec.color,\n            edited_by_member_id = rec.edited_by_member_id,\n            updated_date_time = now()\n/*}}*/",
            generateSQL_IdentityList("", "rec."));

        m_ReplacementMap.put("/*{{*/\n                AND (all_short_name OR short_name ILIKE '%' || in_short_name || '%')\n                AND (all_long_name OR long_name ILIKE '%' || in_long_name || '%')\n                AND (all_display_order OR display_order = in_display_order)\n                AND (all_visibility OR visibility = in_visibility)\n                AND (all_definition OR definition ILIKE '%' || in_definition || '%')\n                AND (all_color OR color ILIKE '%' || in_color || '%')\n/*}}*/",
            generateSQL_ANDlist());

        // ### Managed Service Tokens
        m_ReplacementMap.put("/*{{*/\n        this.jsonValues.put(\"id\", daoJson.has(\"invoiceLineItemId\" ) ? daoJson.getInt(\"invoiceLineItemId\") : -1);\n        this.jsonValues.put(\"shortName\", daoJson.has(\"short_name\") ? daoJson.getString(\"short_name\") : \"\");\n        this.jsonValues.put(\"longName\", daoJson.has(\"long_name\") ? daoJson.getString(\"long_name\") : \"\");\n        this.jsonValues.put(\"displayOrder\", daoJson.has(\"display_order\") ? daoJson.getInt(\"display_order\") : -1);\n        this.jsonValues.put(\"visibility\", daoJson.has(\"visibility\") ? daoJson.getInt(\"visibility\") : -1);\n        this.jsonValues.put(\"definition\", daoJson.has(\"definition\") ? daoJson.getString(\"definition\") : \"\");\n        this.jsonValues.put(\"color\", daoJson.has(\"color\") ? daoJson.getString(\"color\") : \"\");\n        this.jsonValues.put(\"created\", daoJson.has(\"created_date_time\") ? (Timestamp)daoJson.get(\"created_date_time\") : new Timestamp(0));\n        this.jsonValues.put(\"updated\", daoJson.has(\"updated_date_time\") ? (Timestamp)daoJson.get(\"updated_date_time\") : null);\n        this.jsonValues.put(\"editedByMemberId\", daoJson.has(\"edited_by_member_id\") ? daoJson.getInt(\"edited_by_member_id\") : -1);\n        this.jsonValues.put(\"retired\", daoJson.has(\"retired\") ? daoJson.getInt(\"retired\") : 0);\n/*}}*/",
            generateMS_jsonValuesList());

        m_ReplacementMap.put("/*{{*/\n    private static final String CREATE      = \"{? = call fn_invoicelineitem_create(?,?,?,?,?,?,?)}\";\n    private static final String UPDATE      = \"{? = call fn_invoicelineitem_update(?,?,?,?,?,?,?,?,?)}\";\n    private static final String RETIRE      = \"{? = call fn_invoicelineitem_retire(?,?)}\";\n    private static final String FILTER      = \"{call fn_invoicelineitem_filter(?,?,?,?,?,?,?,?,?)}\";\n    private static final String FIND        = \"{call fn_invoicelineitem_find(?)}\";\n/*}}*/",
            generateMS_prototypes());

        m_ReplacementMap.put("/*{{*/\n            .add(\"in_short_name\", new HashMapBuilder<String, String>().add(\"key\", \"shortName\").add(\"type\", \"text\").build())\n            .add(\"in_long_name\", new HashMapBuilder<String, String>().add(\"key\", \"longName\").add(\"type\", \"text\").build())\n            .add(\"in_display_order\", new HashMapBuilder<String, String>().add(\"key\", \"displayOrder\").add(\"type\", \"integer\").build())\n            .add(\"in_visibility\", new HashMapBuilder<String, String>().add(\"key\", \"visibility\").add(\"type\", \"integer\").build())\n            .add(\"in_definition\", new HashMapBuilder<String, String>().add(\"key\", \"definition\").add(\"type\", \"text\").build())\n            .add(\"in_color\", new HashMapBuilder<String, String>().add(\"key\", \"color\").add(\"type\", \"text\").build())\n/*}}*/",
            generateMS_hashList());

        m_ReplacementMap.put("/*{{*/invoiceLineItems/*}}*/", m_NameCamelLower);

        m_ReplacementMap.put("/*{{*/invoice_line_item/*}}*/", m_NameUScore);

        m_ReplacementMap.put("/*{{*/\n            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add(\"type\", \"text\").add(\"key\", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())\n/*}}*/",
              (countTypes(Type.Special_editedBy) == 0) ? "" : "            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add(\"type\", \"text\").add(\"key\", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())");

         m_ReplacementMap.put("/*{{*/\n            if (map['shortName'].length) t['shortName'] = map['shortName'];\n            if (map['longName'].length) t['longName'] = map['longName'];\n            if (map['displayOrder'].length) t['displayOrder'] = map['displayOrder'];\n            if (map['visibility'].length) t['visibility'] = map['visibility'];\n            if (map['definition'].length) t['definition'] = map['definition'];\n            if (map['color'].length) t['color'] = map['color'];\n/*}}*/",
              generateHTML_Map());

         m_ReplacementMap.put("/*{{*/\n    <li>\n        <label for=\"shortName\">Short Name</label>\n        <input id=\"shortName\" name=\"shortName\" />\n    </li>\n    <li>\n        <label for=\"longName\">Long Name</label>\n        <input id=\"longName\" name=\"longName\" />\n    </li>\n    <li>\n        <label for=\"displayOrder\">Display Order</label>\n        <input id=\"displayOrder\" name=\"displayOrder\" />\n    </li>\n    <li>\n        <label for=\"visibility\">Visibility</label>\n        <input id=\"visibility\" name=\"visibility\" />\n    </li>\n    <li>\n        <label for=\"definition\">Definition</label>\n        <input id=\"definition\" name=\"definition\" />\n    </li>\n    <li>\n        <label for=\"color\">Color</label>\n        <input id=\"color\" name=\"color\" />\n    </li>\n/*}}*/",
              generateHTML_List());

    }

    /**
     * Generates a list of terms based on a Schema Field name and its type.
     * @param includeType:  TRUE if should include type field (e.g. "Text" or "Integer")
     * @param includeName:  TRUE if should include name (e.g. "short_name");
     * @param elementSeparator:  Element separator such as comma.  Inserted between elements and NOT after last element
     * @param namePrefix:  String to placed before name element
     * @param nameSuffix:  String to be placed after name element
     * @param typeList:  Array of types to be included in list.  Types not include are omitted from this list.
     * @return
     */
     protected String generateNameTypeList(boolean includeType, boolean includeName, String elementSeparator, String namePrefix, String nameSuffix, SchemaField.Type typeList[]) {
        StringBuilder   sb = new StringBuilder();
        boolean firstTime = true;
        for (SchemaField sh : m_FieldList) {

            if (containsType(typeList, sh.getType())) {

                if (!firstTime) {
                    sb.append(elementSeparator);
                }
                if (includeName) {
                    sb.append(namePrefix + sh.getName() + nameSuffix);
                }
                if (includeType) {
                    if (includeName) {
                        sb.append(" ");
                    }
                    sb.append(sh.getType().getLabel());
                }
                firstTime = false;
            }
        }
        return sb.toString();
    }

     /**
      * Similar to above but each name term appears twice -- once on the left and once on the right.
      * @param lPrefix
      * @param rPrefix
      * @return
      */
     public String generateSQL_IdentityList(String lPrefix, String rPrefix) {
         StringBuilder       sb = new StringBuilder();
        for (SchemaField sf : m_FieldList) {
            switch (sf.getType()) {
                case Text:
                case Integer:
                case IntegerArray:
                case Special_editedBy:
                case Timestamp:
                    sb.append("            " + sf.getName() + " = rec." + sf.getName() + ",\n");
                    break;
                case Timestamp_updated:
                    sb.append("            " + sf.getName() + " = now(),\n");
                    break;
            }
        }

        eatFinalChars(sb, ",\n");
        return sb.toString();
    }


     /**
      * Single-purpose IF-THEN tempalte for SQL file
      * @return
      */
     protected String generateSQL_IFTHEN() {
         StringBuilder      sb = new StringBuilder();
        boolean firstTime = true;
        for (SchemaField sh : m_FieldList) {
            switch (sh.getType()) {
                case Text:
                case Integer:
                case IntegerArray:
                case Special_editedBy:
                case Timestamp:
                    sb.append("            ");
                    if (!firstTime) {
                        sb.append("ELSE");
                    }
                    firstTime = false;
                    sb.append("IF param_name = 'in_" + sh.getName() + "' THEN\n");
                    sb.append("                rec." + sh.getName() + " := in_" + sh.getName() + ";\n");
                    break;
            }
        }
        sb.append("            END IF;");
        return sb.toString();
    }


     /**
      * Single-purpose temp table for SQL file. Used for matching multiple values.
      * @return
      */
     protected String generateSQL_tempTable() {
         StringBuilder  sb = new StringBuilder();
         for (SchemaField sh : m_FieldList) {
             switch (sh.getType()) {
                 case IntegerArray:
                     sb.append("        BEGIN\n");
                     sb.append("            CREATE TEMPORARY TABLE tmp_tbl_" + sh.getName() + "(\n");
                     sb.append("                " + sh.getName() + "  int\n");
                     sb.append("            );\n");
                     sb.append("        EXCEPTION\n");
                     sb.append("            WHEN DUPLICATE_TABLE THEN\n");
                     sb.append("                TRUNCATE TABLE tmp_tbl_" + sh.getName() + ";\n");
                     sb.append("        END;\n");
                     break;
             }
         }

         return sb.toString();
     }
      /**
      * Single-purpose IF-THEN tempalte for SQL file
      * @return
      */
     protected String generateSQL_IFTHEN_v2() {
         StringBuilder      sb = new StringBuilder();
        boolean firstTime = true;
        for (SchemaField sh : m_FieldList) {
            switch (sh.getType()) {
                case Text:
                case Integer:
                case IntegerArray:
                case Timestamp:
                    sb.append("            ");
                    if (!firstTime) {
                        sb.append("ELSE");
                    }
                    firstTime = false;
                    sb.append("IF param_name = 'in_" + sh.getName() + "' THEN\n");
                    sb.append("                all_" + sh.getName() + " := false;\n");
                    if (sh.getType() == Type.IntegerArray)
                    {
                        sb.append("                FOR i IN coalesce(array_lower(in_" + sh.getName() + ", 1), 0) .. coalesce(array_upper(in_" + sh.getName() + ", 1), -1)\n");
                        sb.append("                LOOP\n");
                        sb.append("                    INSERT INTO tmp_tbl_" + sh.getName() + " VALUES (in_" + sh.getName() + "[i]);\n");
                        sb.append("                END LOOP;\n");
                    }
                    break;
            }
        }
        sb.append("            END IF;");

        return sb.toString();
    }

      /**
      * Single-purpose IF-THEN template for SQL file
      * @return
      */
     protected String generateSQL_IFTHEN_v3() {
         StringBuilder      sb = new StringBuilder();
        boolean firstTime = true;
        for (SchemaField sh : m_FieldList) {
            sb.append("            ");
            if (!firstTime) {
                sb.append("ELSE");
            }
            firstTime = false;
            sb.append("IF filter_name = 'in_" + sh.getName() + "' THEN\n");
            switch (sh.getType()) {
                case Integer:
                    sb.append("                var_query := var_query || ' AND " + sh.getName() + " = ' || in_" + sh.getName() + ";\n");
                    break;
                case Timestamp:
                    sb.append("                var_query := var_query || ' AND " + sh.getName() + " = ''' || in_" + sh.getName() + " || '''';\n");
                    break;
                case Text:
                    sb.append("                var_query := var_query || ' AND " + sh.getName() + " ILIKE ''%' || in_" + sh.getName() + " || '%''';\n");
                    break;
                case IntegerArray:
                    sb.append("                FOR i IN coalesce(array_lower(in_" + sh.getName() + ", 1), 0) .. coalesce(array_upper(in_" + sh.getName() + ", 1), -1)\n");
                    sb.append("                LOOP\n");
                    sb.append("                    INSERT INTO tmp_tbl_" + sh.getName() + " VALUES (in_" + sh.getName() + "[i]);\n");
                    sb.append("                END LOOP;\n");
                    sb.append("                var_query := var_query || ' AND " + sh.getName() + " IN (SELECT * FROM tmp_tbl_" + sh.getName() + ")';\n");
                    break;
            }
        }
        sb.append("            END IF;");

        return sb.toString();
    }

     /**
      * Single purpose generator for SQL file
      * @return
      */

     public String generateSQL_ANDlist() {
         StringBuilder      sb = new StringBuilder();

        for (SchemaField sh : m_FieldList) {
            switch (sh.getType()) {
                case Text:
                    sb.append("                AND (all_" + sh.getName() + " OR " + sh.getName() + " ILIKE '%' || in_" + sh.getName() + " || '%')\n");
                    break;
                case IntegerArray:
                    sb.append("                AND (all_" + sh.getName() + " OR " + sh.getName() + " IN (SELECT * FROM tmp_tbl_" + sh.getName() + "))\n");
                    break;
                case Integer:
                case Timestamp:
                    sb.append("                AND (all_" + sh.getName() + " OR " + sh.getName() + " = in_" + sh.getName() + ")\n");
                    break;
            }
        }

        eatFinalChars(sb, "\n");

        return sb.toString();
    }

    /**
     * Eats trailing characters if they match the pattern.
     * @param sb
     * @param charsToEat
     */
    private static void eatFinalChars(StringBuilder sb, String charsToEat) {
         // eat final '\n'
        if (sb.toString().endsWith(charsToEat))  {
            sb.setLength(sb.length() - charsToEat.length());
        }
    }

    /**
     * Single-purpose routine for generating HTML files
     * @return
     */
    private String generateHTML_Map() {
        StringBuilder   sb = new StringBuilder();

        for (SchemaField sf : m_FieldList) {
            String      name = sf.getNameCamel();
            switch (sf.getType()) {
                case Integer:
                case IntegerArray:
                case Text:
                case Timestamp:
                    // if (map['shortName'].length) t['shortName'] = map['shortName'];
                    sb.append("            if (map['" + name + "'].length) t['" + name + "'] = map['" + name + "'];\n");
            }
        }

        return sb.toString();
    }

    /**
     * Single-purpose routine for generating HTML files
     * @return
     */
    private String generateHTML_List() {
        StringBuilder   sb = new StringBuilder();

        for (SchemaField sf : m_FieldList) {

            String      name = sf.getNameCamel();
            String      friendly = sf.getNameFriendly();

            switch (sf.getType()) {
                case Integer:
                case IntegerArray:
                case Text:
                case Timestamp:
//                     <li>
//                        <label for="shortName">Short Name</label>
//                        <input id="shortName" name="shortName" />
//                    </li>

                    sb.append("    <li>\n");
                    sb.append("        <label for=\"" + name + "\">" + friendly + "</label>\n");
                    sb.append("        <input id=\"" + name + "\" name=\"" + name + "\" />\n");
                    sb.append("    </li>\n");
            }
        }

        return sb.toString();
    }

/*
    private static final String CREATE      = "{? = call fn_invoicelineitem_create(?,?,?,?,?,?,?)}";
    private static final String UPDATE      = "{? = call fn_invoicelineitem_update(?,?,?,?,?,?,?,?,?)}";
    private static final String RETIRE      = "{? = call fn_invoicelineitem_retire(?,?)}";
    private static final String FILTER      = "{call fn_invoicelineitem_filter(?,?,?,?,?,?,?,?,?)}";
    private static final String FIND        = "{call fn_invoicelineitem_find(?)}";
*/

    /**
     * Generates series of question marks for Managed Service Java objects.
     * @param count
     * @return
     */

    private String qMarks(int count) {
        StringBuilder    sb = new StringBuilder();

        while (count-- > 0) {
            sb.append("?,");
        }

        // eat final comma
        sb.setLength(sb.length() - 1);

        return sb.toString();
    }

    /**
     * Single-purpose routine for generating Managed Service Java objects
     * @return
     */
    private String generateMS_prototypes() {
        StringBuilder       sb = new StringBuilder();

        String      name = getNameLower();

        // text, ints, and "editedBy"
        int     numCreateParams = countTypes(Type.Integer, Type.IntegerArray, Type.Text, Type.Special_editedBy, Type.Timestamp);

        // text, ints, primaryKey, "editedBy", plus 'paramList'  (typically two larger than 'numCreateParams'
        int     numUpdateParams = 1 + countTypes(Type.Integer, Type.IntegerArray, Type.Text, Type.Special_editedBy, Type.PrimaryKey, Type.Timestamp);

        // text, ints, NO primaryKey, NO "editedBy".  Adds a "count", "offset", "orderList", and "paramList"
        int     numFindParams   = 4 + countTypes(Type.Integer, Type.IntegerArray, Type.Text, Type.Timestamp);

        if (isUpdateable())
        {
            sb.append("   private static final String CREATE      = \"{? = call fn_" + name + "_create(?,?," + qMarks(numCreateParams) +")}\";\n");
            sb.append("   private static final String UPDATE      = \"{? = call fn_" + name + "_update(?,?," + qMarks(numUpdateParams) +")}\";\n");
            sb.append("   private static final String RETIRE      = \"{? = call fn_" + name + "_retire(?,?,?,?)}\";\n");
        }
        sb.append("   private static final String FILTER      = \"{call fn_" + name + "_filter(?,?," + qMarks(numFindParams) +")}\";\n");
        sb.append("   private static final String FIND        = \"{call fn_" + name + "_find(?,?,?)}\";\n");

        return sb.toString();
    }

    /**
     * Single-purpose routine for generating Managed Service Java objects.
     * @return
     */
    private String generateMS_hashList() {
        StringBuilder       sb = new StringBuilder();

//        .add("in_long_name", new HashMapBuilder<String, String>().add("key", "longName").add("type", "text").build())
//        .add("in_display_order", new HashMapBuilder<String, String>().add("key", "displayOrder").add("type", "integer").build())

        for (SchemaField sf : m_FieldList) {

            String      name = sf.getName();
            String      caml = sf.getNameCamel();

            switch (sf.getType()) {
                case Text:
                    sb.append("            .add(\"in_" + name + "\", new HashMapBuilder<String, String>().add(\"key\", \"" + caml + "\").add(\"type\", \"text\").build())\n");
                    break;
                case Integer:
                    sb.append("            .add(\"in_" + name + "\", new HashMapBuilder<String, String>().add(\"key\", \"" + caml + "\").add(\"type\", \"integer\").build())\n");
                    break;
                case IntegerArray:
                    sb.append("            .add(\"in_" + name + "\", new HashMapBuilder<String, String>().add(\"key\", \"" + caml + "\").add(\"type\", \"integer[]\").build())\n");
                    break;
                case Timestamp:
                    sb.append("            .add(\"in_" + name + "\", new HashMapBuilder<String, String>().add(\"key\", \"" + caml + "\").add(\"type\", \"timestamp\").build())\n");
                    break;
            }
        }

        return sb.toString();
    }

    /**
     * Single-purpose routine for generating Java objects.
     * @return
     */
    private String generateMS_jsonValuesList() {
        StringBuilder       sb = new StringBuilder();

        for (SchemaField sf : m_FieldList) {

            String  name = sf.getName().toLowerCase();
            String  caml = sf.getNameCamel();

            String prepend = "        this.jsonValues.put(\"" + caml + "\", daoJson.has(\"" + name + "\") ? ";

            switch (sf.getType()) {
                case PrimaryKey:
                    // this.jsonValues.put("id", daoJson.has("invoiceLineItemId" ) ? daoJson.getInt("invoiceLineItemId") : -1);
                    sb.append("        this.jsonValues.put(\"id\", daoJson.has(\"" + name + "\" ) ? daoJson.getInt(\"" + name + "\") : -1);");
                    break;
                case Integer:
                case IntegerArray:
                    // this.jsonValues.put("displayOrder", daoJson.has("display_order") ? daoJson.getInt("display_order") : -1);
                    sb.append(prepend + "daoJson.getInt(\"" + name + "\") : -1);");
                    break;
                case Special_editedBy:
                    // this.jsonValues.put("displayOrder", daoJson.has("display_order") ? daoJson.getInt("display_order") : -1);
                    sb.append(prepend + "daoJson.getString(\"" + name + "\") : -1);");
                    break;
                case Special_retired:
                    // this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
                    sb.append(prepend + "daoJson.getInt(\"" + name + "\") : 0);");
                    break;
                case Text:
                    // this.jsonValues.put("shortName", daoJson.has("short_name") ? daoJson.getString("short_name") : "");
                    sb.append(prepend + "daoJson.getString(\"" + name + "\") : \"\");");
                    break;
                case Timestamp:
                    // this.jsonValues.put("nextPlannedUpdate", daoJson.has("next_planned_update") ? (Timestamp)daoJson.get("next_planned_update") : new Timestamp(0));
                    sb.append(prepend + "(Timestamp)daoJson.get(\"" + name + "\") : new Timestamp(0));");
                    break;
                case Timestamp_created:
                    // this.jsonValues.put("created", daoJson.has("created_date_time") ? (Timestamp)daoJson.get("created_date_time") : new Timestamp(0));
                    sb.append("        this.jsonValues.put(\"created\", daoJson.has(\"" + name + "\") ? (Timestamp)daoJson.get(\"" + name + "\") : new Timestamp(0));");
                    break;
                case Timestamp_updated:
                    // this.jsonValues.put("updated", daoJson.has("updated_date_time") ? (Timestamp)daoJson.get("updated_date_time") : null);
                    sb.append("        this.jsonValues.put(\"updated\", daoJson.has(\"" + name + "\") ? (Timestamp)daoJson.get(\"" + name + "\") : null);");
                    break;
                default:
                    System.out.println("UNRECOGNIZED TYPE: <" + sf.getType() + "> in <" + sf.getName() + ">");
                    System.exit(1);
            }

            sb.append("\n");
        }

        return sb.toString();
    }

    /**
     * Generates a new file by replacing tokens in the "fileInName" file.
     *
     * @param fileInName:  filename and path of template
     * @param fileOutName:  filename and path of output file
     * @throws java.io.IOException
     */
    private void replaceTokens(String fileInName, String fileOutName) throws IOException {

        boolean         escaped = false;
        int             ii, jj;
        File            fileIn = new File(fileInName);
        File            fileOut = new File(fileOutName);
        StringBuilder   escapeToken = new StringBuilder();
        StringBuilder   unescapedText = new StringBuilder();
        String          tokenKey, replaceValue;

        int             size = (int)fileIn.length();
        char            fileChars[] = new char[size];

        FileReader      fr = new FileReader(fileIn);
        PrintWriter     pw = new PrintWriter(fileOut);

        fr.read(fileChars);

        for (ii = 0; ii < size - 6; ii++) {
            if ((new String(fileChars, ii, 6)).equals("/*{{*/")) {

                pw.print(replaceMSStage(unescapedText));
                unescapedText.setLength(0);

                if (escaped) {
                    System.out.println("Already escaped!!!");
                    System.out.println("-----------------------------------");
                    System.out.println(new String(fileChars, Math.max(0, ii - 100), Math.min(100, size - ii)));
                    System.out.println("-----------------------------------");
                    System.exit(0);
                }
                escaped = true;
            }
            if ((new String(fileChars, ii, 6)).equals("/*}}*/")) {
                escaped = false;
                tokenKey = escapeToken.toString();

                if (!tokenKey.trim().startsWith("/*{{*/") ) {
                    System.out.println("TOKEN DELIMITER ERROR:  " + new String(fileChars, ii, 100));
                    System.exit(0);
                }
                tokenKey += "/*}}*/";
                ii += 6;

                replaceValue = m_ReplacementMap.get(tokenKey);

                if (replaceValue == null) {
                    replaceValue = "############## ERROR -- TOKEN NOT MATCHED ##############";

                    tokenKey = tokenKey.replaceAll("\n", "\\\\n");
                    tokenKey = tokenKey.replaceAll("\r", "\\\\r");
                    tokenKey = tokenKey.replaceAll("\"", "\\\\\\\"");

                    if (!m_UnmatchedTokenList.contains(tokenKey)) {
                        m_UnmatchedTokenList.add(tokenKey);
                    }
                }
                else {
                    Integer     useCount = m_ReplacementMapUse.get(tokenKey);
                    if (useCount == null) {
                        useCount = new Integer(0);
                    }
                    useCount = useCount + 1;
                    m_ReplacementMapUse.put(tokenKey, useCount);
                }
                pw.print(replaceValue);

                escapeToken.setLength(0);
            }

            if (escaped) {
                escapeToken.append(fileChars[ii]);
            }
            else {
                unescapedText.append(fileChars[ii]);
            }
        }

        pw.print(replaceMSStage(unescapedText));

        // include trailing chars
        while (ii < size) {
            pw.print(fileChars[ii++]);
        }

        pw.flush();
        pw.close();
    }

    /**
     * Shows warings if a token was defined in m_ReplacementMap but never used.
     * This might indicate a bug.  If not, then remove unused token.
     *
     * Shows error if a template file defines a token but there's no match in
     * m_ReplacementMap.  The token is printed in a format that can be
     * copied into the code.  
     *
     */
    public void showUseStats() {
        // Check to see if there were any tokens that are not in our list.
        // this is a fatal error.
        if (m_UnmatchedTokenList.size() == 0) {
            System.out.println("There were zero umatched tokens (good)");
        }
        else {
            System.out.println("#### ERROR:  There were [" + m_UnmatchedTokenList.size() + "] unmatched tokens");

            for (String token : m_UnmatchedTokenList) {
                System.out.println("\"" + token + "\"");
            }
            System.out.println("");
        }

        // Check if any tokens in our list were not used.  This is not necessarily a problem
        // but should be looked into to make sure nothing is fishy.
        boolean     unusedTokens = false;
        for (String tokenKey : m_ReplacementMap.keySet()) {
            Integer     useCount = m_ReplacementMapUse.get(tokenKey);

            if (useCount == null || useCount == 0) {
                System.out.println("#### WARNING:  Token not used at all. Please look into this");
                System.out.println("     --> TOKEN: \"" + tokenKey + "\"");
                unusedTokens = true;
            }
        }
        if (!unusedTokens) {
            System.out.println("There were zero unused tokens (good)");
        }
    }


    /**
     * create SQL file
     * @throws java.io.IOException
     */
    private void createSQLFile() throws IOException {
        String templateFileName = isUpdateable() ? "./src/java/com/qp/SchemaHelper/fn_template.sql" : "./src/java/com/qp/SchemaHelper/fn_template_view.sql";
        replaceTokens(templateFileName, "../database/fn_" + m_NameLower + ".sql");
    }

    /**
     * Create the Managed Service java files
     * @throws java.io.IOException
     */
    public void createJavaFiles() throws IOException {
        String templateFileName = isUpdateable() ?
            "./src/java/com/qp/SchemaHelper/__Template__.java" :
            "./src/java/com/qp/SchemaHelper/__TemplateView__.java";
        replaceTokens(
            templateFileName,
            "./src/java/com/alleni/" + m_FolderName + "/" + m_NameCamelUpper + ".java");

        String templateDaoFileName = isUpdateable() ?
            "./src/java/com/qp/SchemaHelper/__TemplateDao__.java" :
            "./src/java/com/qp/SchemaHelper/__TemplateViewDao__.java";
        replaceTokens(
            templateDaoFileName,
            "./src/java/com/alleni/" + m_FolderName + "/" + m_NameCamelUpper + "Dao.java");
    }

    /**
     * Create the JSP files
     * @throws java.io.IOException
     */
    private void createJSPFiles() throws IOException {

        if (m_NameLower == "invoicelineitem") {
            System.out.println("skipping 'invoicelineitem' for createJSPFiles");
            return;
        }

        String      jspFullList[] = {"create", "find", "filter", "retire", "update"};
        String      jspViewList[] = {"find", "filter"};
        String      jspList[] = isUpdateable() ? jspFullList : jspViewList;

        String      from = "./web/services/invoicelineitem/";
        String      to = "./web/services/" + m_NameLower + "/";

        File        dir = new File(to);

        // create directory if doesn't exist
        if (!dir.exists()) {
            dir.mkdir();
        }

        for (String item : jspList) {
            replaceTokens(from + item + ".jsp", to + item + ".jsp");
        }
    }

    /**
     * Create the HTML files
     * @throws java.io.IOException
     */
    private void createHTMLFiles() throws IOException {

        String      htmlFullList[] = {"CreateUpdate", "Filter", "FindRetire"};
        String      htmlViewList[] = {"Filter", "FindRetire"};
        String      htmlList[] = isUpdateable() ? htmlFullList : htmlViewList;

        for (String item : htmlList) {
            String      from = "./web/com/alleni/html/tests/template_" + item + ".html";
            String      to   = "./web/com/alleni/html/tests/" + m_NameCamelLowerSingle + item + ".html";

            replaceTokens(from, to);
        }
    }

/*
   invoiceLineItemCreate: {
        group: 'Finance Invoice Line Item',
        name: 'create',
        html: 'invoiceLineItemCreateUpdate.html',
        jsp: 'invoicelineitem/create.jsp'
    },
    invoiceLineItemUpdate: {
        group: 'Finance Invoice Line Item',
        name: 'update',
        html: 'invoiceLineItemCreateUpdate.html',
       jsp: 'invoicelineitem/update.jsp'
    },
    invoiceLineItemRetire: {
        group: 'Finance Invoice Line Item',
        name: 'retire',
        html: 'msStageFindRetire.html',
        jsp: 'invoicelineitem/retire.jsp'
    },
    invoiceLineItemFind: {
        group: 'Finance Invoice Line Item',
        name: 'find by id',
        html: 'msStageFindRetire.html',
        jsp: 'invoicelineitem/find.jsp'
    },
    invoiceLineItemFilter: {
        group: 'Finance Invoice Line Item',
        name: 'find filter',
        html: 'msStageFilter.html',
        jsp: 'invoicelineitem/filter.jsp'
    },
 */

    /**
     * Create all files necessary for new table
     * @throws java.io.IOException
     */
    public void createAllFiles() throws IOException {
        createSQLFile();
        createJavaFiles();
        createJSPFiles();
        createHTMLFiles();

        System.out.println("-----------------------------");
        System.out.println("PUT THIS IN tests.js");
        System.out.println(showTestScripts());
        System.out.println("-----------------------------");

        showUseStats();
    }

    /**
     * Displays in console the code that needs to be inserted into "tests.js"
     * @return
     */
    public String showTestScripts() {
        StringBuilder       sb = new StringBuilder();

        String list[][] = {
            {"Create",      "create",       "CreateUpdate"},
            {"Update",      "update",       "CreateUpdate"},
            {"Retire",      "retire",       "FindRetire"},
            {"Find",        "find",         "FindRetire"},
            {"Filter",      "filter",       "Filter"}
        };

        for (String line[] : list) {


            String  camelLower = line[0].substring(0, 1).toLowerCase() + line[0].substring(1);

            sb.append("    \"" + m_NameCamelLowerSingle + line[0] + "\": {\n");
            sb.append("        \"group\": \"" + m_NameFull + "\",\n");
            sb.append("        \"name\": \"" + line[1] + "\",\n");
            if (line[2] != null) {
                sb.append("        \"html\": \"" + m_NameCamelLowerSingle + line[2] + ".html\",\n");
            }
            sb.append("        \"jsp\": \"" + m_NameCamelLowerSingle.toLowerCase() + "/" + camelLower + ".jsp\"\n");
            sb.append("    },\n");
        }

        return sb.toString();
    }

    public static void main(String args[]) {

        try {

// RUN EVERYTHING

           // for (SchemaGenerator  sp : SchemaHelper.m_AllSchemas) {
            //    sp.createAllFiles();
            //}
//        	SchemaHelper.m_CouponCodes.createAllFiles();
//        	SchemaHelper.m_SharedProjects.createAllFiles();
//        	SchemaHelper.m_ShareTypes.createAllFiles();
//        	SchemaHelper.m_GuestList.createAllFiles();
//        	SchemaHelper.m_GuestListInvitees.createAllFiles();
//        	SchemaHelper.m_ProductCodes.createAllFiles();
        	SchemaHelper.m_projectFeedback.createAllFiles();
// Run create for one table
//              SchemaHelper.m_Account.createAllFiles();
//              SchemaHelper.m_Category.createAllFiles();
// Just create the HTML files for one table
//            SchemaHelper.m_Account.createHTMLFiles();

        }
        catch (IOException e) {
            System.out.println("Got fatal exception: " + e);
            e.printStackTrace();
        }
    }
}
