package com.qp.SchemaHelper;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class __TemplateViewDao__ extends BaseDAO
{
/*{{*/
    private static final String CREATE      = "{? = call fn_invoicelineitem_create(?,?,?,?,?,?,?)}";
    private static final String UPDATE      = "{? = call fn_invoicelineitem_update(?,?,?,?,?,?,?,?,?)}";
    private static final String RETIRE      = "{? = call fn_invoicelineitem_retire(?,?)}";
    private static final String FILTER      = "{call fn_invoicelineitem_filter(?,?,?,?,?,?,?,?,?)}";
    private static final String FIND        = "{call fn_invoicelineitem_find(?)}";
/*}}*/
    public __TemplateViewDao__()
    {
        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
/*{{*/
            .add("in_short_name", new HashMapBuilder<String, String>().add("key", "shortName").add("type", "text").build())
            .add("in_long_name", new HashMapBuilder<String, String>().add("key", "longName").add("type", "text").build())
            .add("in_display_order", new HashMapBuilder<String, String>().add("key", "displayOrder").add("type", "integer").build())
            .add("in_visibility", new HashMapBuilder<String, String>().add("key", "visibility").add("type", "integer").build())
            .add("in_definition", new HashMapBuilder<String, String>().add("key", "definition").add("type", "text").build())
            .add("in_color", new HashMapBuilder<String, String>().add("key", "color").add("type", "text").build())
/*}}*/
            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

}