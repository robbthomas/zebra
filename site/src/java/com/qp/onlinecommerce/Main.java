/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.qp.onlinecommerce;

import java.util.ArrayList;
import java.math.BigDecimal;

import net.authorize.Environment;
import net.authorize.ResponseField;
import net.authorize.Transaction;
import net.authorize.cim.Result;
import net.authorize.xml.Message;
import net.authorize.cim.TransactionType;
import net.authorize.cim.ValidationModeType;
import net.authorize.data.Order;
import net.authorize.data.OrderItem;
import net.authorize.data.ShippingCharges;
import net.authorize.data.cim.CustomerProfile;
import net.authorize.data.cim.PaymentProfile;
import net.authorize.data.cim.PaymentTransaction;
import net.authorize.data.creditcard.CreditCard;
import net.authorize.data.echeck.BankAccountType;
import net.authorize.data.echeck.ECheckType;
import net.authorize.data.xml.Address;
import net.authorize.data.xml.BankAccount;
import net.authorize.data.xml.Customer;
import net.authorize.data.xml.CustomerType;
import net.authorize.data.xml.Payment;


/**
 *
 * @author suz
 */
public class Main {
    /*
     *   Suz' developer login 422m8nZR9  key 26W9J3kShp6k9Lkx
     *
     * additional developer test accounts can be obtained from:
     *     https://developer.authorize.net/testaccount/
     */
    

    String refId;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
    /* TODO take out the test login and key */
        /*
         * the apiLoginId and transactionKey are issued to the merchant
         * when they get an account with authorize.net
         * these values should not change and can be stored in a
         * config file or in the database
         * they should be kept safe
         */
        String apiLoginId = "422m8nZR9";
        String transactionKey = "26W9J3kShp6k9Lkx";
        Result<Transaction> result;
        /*
         * refId  - reference Id - a way to check that the answer you get
         * is for the question you asked
         * some actions REQUIRE refId, others do not take it
         * none are optional (not very consistant)
         */
        String refId;
        String customerProfileIdB = "";
        String customerPaymentProfileIdB = "";
        String customerProfileIdA = "";
        String customerPaymentProfileIdA = "";
        String customerPaymentProfileIdA2 = "";
        CIMTransaction cimTransaction;

        /*
         * the following code I kept around for times when the code
         * crashed and I needed to delete the profiles from the previous
         * run or get errors "duplicate profile"
         * This can also be done from the merchant interface
         *      https://sandbox.authorize.net
         */
        /* deleteing profiles for customer A and B

        cimTransaction = new CIMTransaction();
        refId = "REFID:" + System.currentTimeMillis();
        result = cimTransaction.deleteCustomerProfile(apiLoginId, transactionKey,
                refId, "3356987");
       

        refId = "REFID:" + System.currentTimeMillis();
        result = cimTransaction.deleteCustomerProfile(apiLoginId, transactionKey,
                refId, "3356985");
         * 
         */


        // customer A with a credit card
        /*
         * creating a customer profile with a credit card
         *
         * I think this the minimum set of attributes needed for a customer profile
         *
         * If email is set then authorize.net will send a receipt, not sure we
         * want that  (sample included in package)
         */
        cimTransaction = new CIMTransaction();
        cimTransaction.customerProfile = CustomerProfile.createCustomerProfile();
        cimTransaction.customerProfile.setMerchantCustomerId("zebra00035");
        //cimTransaction.customerProfile.setDescription("Sam Diamond of ICU");
        //cimTransaction.customerProfile.setEmail("sam@diamondworks.net");

        /*
         * these are all the objects/attributes needed for the payment profile
         * 
         */
        cimTransaction.creditCard = CreditCard.createCreditCard();
        cimTransaction.creditCard.setCreditCardNumber("4007000000027");
        cimTransaction.creditCard.setExpirationDate("2027-07");

        cimTransaction.billingInfo = (Address) Address.createAddress();
        cimTransaction.billingInfo.setFirstName("Sam");
        cimTransaction.billingInfo.setLastName("Diamond");
        cimTransaction.billingInfo.setCompany("ICU");
        cimTransaction.billingInfo.setAddress("101 Main");
        cimTransaction.billingInfo.setCity("Norman");
        cimTransaction.billingInfo.setState("OK");
        cimTransaction.billingInfo.setCountry("USA");
        cimTransaction.billingInfo.setZipPostalCode("73070");
        cimTransaction.billingInfo.setPhoneNumber("8885551212");

        cimTransaction.paymentProfile = PaymentProfile.createPaymentProfile();
        cimTransaction.paymentProfile.setBillTo(cimTransaction.billingInfo);
	cimTransaction.paymentProfile.addPayment(Payment.createPayment(cimTransaction.creditCard));
        cimTransaction.paymentProfile.setCustomerType(CustomerType.INDIVIDUAL);
        refId = "REFID:" + System.currentTimeMillis();
        result = cimTransaction.createCustomerProfile(apiLoginId,
                transactionKey, refId);
        if(result.isOk())
        {
            /*
             * at this point, we need to save the customerProfileId and
             * paymentProfileId in the database for later use
             */
            customerProfileIdA = result.getCustomerProfileId();
            customerPaymentProfileIdA = result.getCustomerPaymentProfileIdList().get(0);
        }
        printResult("Adding Customer A", result, refId);

        /* customer B with an eCheck
         *
         * I think we are only doing credit cards for now (?)
         * if so, ignore the echeck stuff
         */
        cimTransaction = new CIMTransaction();
        cimTransaction.customerProfile = CustomerProfile.createCustomerProfile();
        cimTransaction.customerProfile.setMerchantCustomerId("zebra00149");
        //cimTransaction.customerProfile.setDescription("Zelda Zip Design Studio");
        //cimTransaction.customerProfile.setEmail("zelda@merchant.net");
        /*
         * eCheck objects/attributes
         * 
         * customerType is supposed to be optional but the create method fails
         * without it !?!
         */
        cimTransaction.bankAccount = BankAccount.createBankAccount();
        cimTransaction.bankAccount.setBankAccountName("Test Checking");
        cimTransaction.bankAccount.setBankAccountNumber("1234567890");
        cimTransaction.bankAccount.setBankAccountType(BankAccountType.CHECKING);
        cimTransaction.bankAccount.setBankName("FEDERAL RESERVE BK OF SAN FRANCISCO");
        cimTransaction.bankAccount.setRoutingNumber("121000374");
        cimTransaction.bankAccount.setECheckType(ECheckType.WEB);

        cimTransaction.billingInfo = (Address) Address.createAddress();
        cimTransaction.billingInfo.setFirstName("Zelda");
        cimTransaction.billingInfo.setLastName("Zip");
        cimTransaction.billingInfo.setCompany("ZZ Designs");
        cimTransaction.billingInfo.setAddress("101 Main");
        cimTransaction.billingInfo.setCity("Normal");
        cimTransaction.billingInfo.setState("CA");
        cimTransaction.billingInfo.setCountry("USA");
        cimTransaction.billingInfo.setZipPostalCode("93070");
        cimTransaction.billingInfo.setPhoneNumber("8885551212");

        cimTransaction.paymentProfile = PaymentProfile.createPaymentProfile();
        cimTransaction.paymentProfile.setBillTo(cimTransaction.billingInfo);
	cimTransaction.paymentProfile.addPayment(Payment.createPayment(cimTransaction.bankAccount));
        cimTransaction.paymentProfile.setCustomerType(CustomerType.INDIVIDUAL);

        refId = "REFID:" + System.currentTimeMillis();
        result = cimTransaction.createCustomerProfile(apiLoginId, transactionKey, refId);
        printResult("Adding Customer B", result, refId);
        if(result.isOk())
        {
            customerProfileIdB = result.getCustomerProfileId();
            customerPaymentProfileIdB = result.getCustomerPaymentProfileIdList().get(0);
            /*
             * not sure when we would use the validete method, but it was part
             * of their api so I included it
             */
            cimTransaction = new CIMTransaction();
            refId = "REFID:" + System.currentTimeMillis();
            result = cimTransaction.validateCustomerPaymentProfile(apiLoginId,
                transactionKey, refId, customerProfileIdB, customerPaymentProfileIdB, "");
            printResult("Validating Customer B", result, refId);
        }
 
        // updating customer A profile
        /*
         * all the docs say to get a profile first then change the attributes
         * you want to change, then do the update
         * if you leave attributes undefined, they are removed from the CIM
         */
        cimTransaction = new CIMTransaction();

        result = cimTransaction.getCustomerProfile(apiLoginId, transactionKey,
                customerProfileIdA);
        printResult("getting customer A", result, "");
        if(result.isOk())
        {
            cimTransaction = new CIMTransaction();
            CustomerProfile customerProfileA = result.getCustomerProfile();
            cimTransaction.customerProfile = result.getCustomerProfile();
            cimTransaction.customerProfile.setEmail("nobody@qualityprocess.com");
            refId = "REFID:" + System.currentTimeMillis();
            result = cimTransaction.updateCustomerProfile(apiLoginId, transactionKey,
                    refId, customerProfileIdA);
            printResult("updating customer A", result, refId);
            /*
             * this recheck was part of my debugging, its not needed for production
             */
            if (result.isOk()) {
                cimTransaction = new CIMTransaction();
                Result<Transaction> resultCheck = cimTransaction.getCustomerProfile(apiLoginId, 
                        transactionKey, customerProfileIdA);
                printResult("Checking customer A", resultCheck, "");
            }

            /*
             * adding an additional payment profile for an existing customer
             */
            cimTransaction = new CIMTransaction();
            cimTransaction.creditCard = CreditCard.createCreditCard();
            cimTransaction.creditCard.setCreditCardNumber("4012888818888");
            cimTransaction.creditCard.setExpirationDate("2011-07");
            cimTransaction.billingInfo = (Address) Address.createAddress();
            cimTransaction.billingInfo.setFirstName("Bob");
            cimTransaction.billingInfo.setLastName("Palendrome");
            cimTransaction.billingInfo.setCompany("XYZ");
            cimTransaction.billingInfo.setAddress("101 Main");
            cimTransaction.billingInfo.setCity("Quarter Moon Bay");
            cimTransaction.billingInfo.setState("OK");
            cimTransaction.billingInfo.setCountry("USA");
            cimTransaction.billingInfo.setZipPostalCode("73080");
            cimTransaction.billingInfo.setPhoneNumber("8885551212");
            cimTransaction.paymentProfile = PaymentProfile.createPaymentProfile();
            cimTransaction.paymentProfile.setBillTo(cimTransaction.billingInfo);
            cimTransaction.paymentProfile.addPayment(Payment.createPayment(cimTransaction.creditCard));
            cimTransaction.paymentProfile.setCustomerType(CustomerType.INDIVIDUAL);

            refId = "REFID:" + System.currentTimeMillis();
            result = cimTransaction.createCustomerPaymentProfile(apiLoginId, transactionKey,
                    refId, customerProfileIdA);
            if(result.isOk())
            {
                /*
                 * again, need to save the paymentProfileId in the database
                 */
                printResult("new payment for customer A", result, refId);
                String newPaymentProfileId = result.getCustomerPaymentProfileIdList().get(0);
                /*
                 * getting the profile to check it is all there
                 */
                result = cimTransaction.getCustomerProfile(apiLoginId, transactionKey,
                customerProfileIdA);
                printResult("get profile for customer A", result, "");
                
                /*
                 * deleteing a payment profile, only need the customerProfileId
                 * and the paymentProfileId
                 */
                cimTransaction = new CIMTransaction();
                refId = "REFID:" + System.currentTimeMillis();
                result = cimTransaction.deleteCustomerPaymentProfile(apiLoginId, transactionKey,
                    refId, customerProfileIdA, newPaymentProfileId);
                printResult("deleted payment for customer A", result, refId);
                /*
                 * testing that it happened
                 */
                result = cimTransaction.getCustomerProfile(apiLoginId, transactionKey,
                customerProfileIdA);
                printResult("get profile AGAIN for customer A", result, "");
            }

            /*
             * a transaction to charge a customer's credit card
             *
             * how much of this we want to use is a question
             * the more information in the transaction, the easier to figure out
             * what was what when things go wrong or there is a dispute.
             * Also, if we set the email address, all of this is used for the
             * receipt, which looks nice.
             *
             * we are not likely to have shipping charges, as such, but
             * sales tax is included in that object for some reason and
             * I think we might need to deal with sales tax.
             */
            cimTransaction = new CIMTransaction();
            cimTransaction.order = Order.createOrder();
            cimTransaction.order.setDescription("4 widgets and a line drawing");
            cimTransaction.order.setInvoiceNumber("lion000543");

            // create order item
            OrderItem orderItem = OrderItem.createOrderItem();
            orderItem.setItemDescription("Simple Widget");
            orderItem.setItemId("W001");
            orderItem.setItemName("widget");
            orderItem.setItemPrice(new BigDecimal(25.00));
            orderItem.setItemQuantity(new BigDecimal(4.00));
            orderItem.setItemTaxable(true);
            cimTransaction.order.addOrderItem(orderItem);

            orderItem = OrderItem.createOrderItem();
            orderItem.setItemDescription("small line art");
            orderItem.setItemId("L002");
            orderItem.setItemName("lineArt");
            orderItem.setItemPrice(new BigDecimal(18.00));
            orderItem.setItemQuantity(new BigDecimal(1.00));
            orderItem.setItemTaxable(false);
            cimTransaction.order.addOrderItem(orderItem);
                            // shipping charges
            ShippingCharges shippingCharges = ShippingCharges.createShippingCharges();
            shippingCharges.setTaxAmount(new BigDecimal(9.75));
            shippingCharges.setTaxDescription("California Sales Tax");
            shippingCharges.setTaxItemName("salesTax");
            cimTransaction.order.setShippingCharges(shippingCharges);
            cimTransaction.order.setTotalAmount(new BigDecimal(127.75));
            refId = "REFID:" + System.currentTimeMillis();
            result = cimTransaction.createCustomerProfileTransaction_AuthCapture(apiLoginId,
                transactionKey, refId, customerProfileIdA, customerPaymentProfileIdA, "123");
            if(result.isOk())
            {
                /*
                 * very important at this point to save that transactionId in the database
                 */
                String AtransId = result.getDirectResponseList().get(0).getDirectResponseMap().get(ResponseField.TRANSACTION_ID);
                printResult("transaction " + AtransId + " for A", result, refId);
                
                /*
                 * example of a credit (refund)
                 * I think this one doesn't work because it is done too soon
                 * (error is "does not meet the criteria for issuing a credit")
                 * the correct way to do this is a void
                 * I think if you waited until the next day, the credit would work
                 */
                cimTransaction = new CIMTransaction();
                cimTransaction.order = Order.createOrder();
                cimTransaction.order.setTotalAmount(new BigDecimal(127.75));
                refId = "REFID:" + System.currentTimeMillis();
                result = cimTransaction.createCustomerProfileTransaction_Credit(apiLoginId,
                    transactionKey, refId, customerProfileIdA, customerPaymentProfileIdA, AtransId);
                printResult("CREDIT transaction " + AtransId + " for A", result, refId);
                if(!result.isOk())
                {
                    cimTransaction = new CIMTransaction();
                    refId = "REFID:" + System.currentTimeMillis();
                    result = cimTransaction.createCustomerProfileTransaction_Void(apiLoginId,
                        transactionKey, refId, customerProfileIdA, customerPaymentProfileIdA, AtransId);
                    printResult("VOID transaction " + AtransId + " for A", result, refId);
                }
            }

        
        }
        
        /* customer A' add another payment profile
         * so that we can update it
         */
        cimTransaction = new CIMTransaction();
        cimTransaction.paymentProfile = PaymentProfile.createPaymentProfile();

        cimTransaction.creditCard = CreditCard.createCreditCard();
        cimTransaction.creditCard.setCreditCardNumber("4012888818888");
        cimTransaction.creditCard.setExpirationDate("2011-07");

        cimTransaction.billingInfo = (Address) Address.createAddress();
        cimTransaction.billingInfo.setFirstName("Bob");
        cimTransaction.billingInfo.setLastName("Palendrome");
        cimTransaction.billingInfo.setCompany("XYZ");
        cimTransaction.billingInfo.setAddress("101 Main");
        cimTransaction.billingInfo.setCity("Quarter Moon Bay");
        cimTransaction.billingInfo.setState("OK");
        cimTransaction.billingInfo.setCountry("USA");
        cimTransaction.billingInfo.setZipPostalCode("73080");
        cimTransaction.billingInfo.setPhoneNumber("8885551212");

        cimTransaction.paymentProfile.setBillTo(cimTransaction.billingInfo);
	cimTransaction.paymentProfile.addPayment(Payment.createPayment(cimTransaction.creditCard));
        cimTransaction.paymentProfile.setCustomerType(CustomerType.INDIVIDUAL);
        refId = "REFID:" + System.currentTimeMillis();
        result = cimTransaction.createCustomerPaymentProfile(apiLoginId, transactionKey,
                    refId, customerProfileIdA);
        printResult("Adding another payment profile", result, "");

        if(result.isOk())
        {
            customerPaymentProfileIdA2 = result.getCustomerPaymentProfileIdList().get(0);

            cimTransaction = new CIMTransaction();
            result = cimTransaction.getCustomerPaymentProfile(apiLoginId, transactionKey,
                        customerProfileIdB, customerPaymentProfileIdA2);
            if(result.isOk())
            {
                /*
                 * as with the customer profile, the docs say to get the paymentProfile,
                 * change the values that need changing and do the update
                 * the creditcard number will be XXXX1234 but that is ok, as
                 * long as it is the same as was in the "get"
                 */
                cimTransaction = new CIMTransaction();
                cimTransaction.paymentProfile = result.getCustomerPaymentProfile();
                CreditCard creditCard = cimTransaction.paymentProfile.getPaymentList().get(0).getCreditCard();

                creditCard.setExpirationMonth("11");
                creditCard.setExpirationYear("2015");
                cimTransaction.paymentProfile.getPaymentList().get(0).setCreditCard(creditCard);

                refId = "REFID:" + System.currentTimeMillis();

                result = cimTransaction.updateCustomerPaymentProfile(apiLoginId, transactionKey,
                        refId, customerProfileIdA, customerPaymentProfileIdA2);
                printResult("updating customer payment A", result, refId);
                if (result.isOk()) {
                    cimTransaction = new CIMTransaction();

                    Result<Transaction> resultCheck = cimTransaction.getCustomerProfile(apiLoginId,
                            transactionKey, customerProfileIdA);
                    printResult("Checking customer A", resultCheck, "");
                }
            }
        }

        /*
         * another example of validate
         */
        cimTransaction = new CIMTransaction();
        refId = "REFID:" + System.currentTimeMillis();
        result = cimTransaction.validateCustomerPaymentProfile(apiLoginId,
                transactionKey, refId, customerProfileIdA, customerPaymentProfileIdA, "");
        printResult("Validating Customer A (should work)", result, refId);

        /* deleting profiles for customer A and B
         * only need the customerProfileId
         */
        if(!customerProfileIdA.equals(""))
        {
            refId = "REFID:" + System.currentTimeMillis();
            result = cimTransaction.deleteCustomerProfile(apiLoginId, transactionKey,
                refId, customerProfileIdA);
            printResult("deleting profiles for customer A", result, refId);
        }
        if(!customerProfileIdA.equals(""))
        {
            refId = "REFID:" + System.currentTimeMillis();
            result = cimTransaction.deleteCustomerProfile(apiLoginId, transactionKey,
                    refId, customerProfileIdB);
            printResult("deleting profiles for customer B", result, refId);
        }
        

    }



    private static void printResult(String title, Result result, String refId)
    {
        System.out.println("****" + title);
        System.out.println("Results for refId: " + refId);
        if(!refId.equals("") & result.getRefId() != null)
            if(!result.getRefId().equals(refId) )
                System.out.println("Return for refId: " + result.getRefId() + " not " + refId);
        result.printMessages();
        if(result.isOk())
        {
            System.out.println("    customerProfileId:" + result.getCustomerProfileId());
            if(result.getCustomerPaymentProfileIdList().size() > 0)
                System.out.println("    customerPaymentProfileId:" +
                    result.getCustomerPaymentProfileIdList().get(0));
            if(result.getCustomerPaymentProfileList().size() > 0)
            {
                ArrayList<PaymentProfile> paymentProfileArray = result.getCustomerPaymentProfileList();
                for(int i = 0;i < result.getCustomerPaymentProfileList().size(); i++)
                {
                    PaymentProfile paymentProfile = paymentProfileArray.get(i);

                    System.out.println("    customerPaymentProfileId:" +
                        paymentProfile.getCustomerPaymentProfileId());
                    if(paymentProfile.getPaymentList().size() > 0)
                    {
                        CreditCard creditCard = paymentProfile.getPaymentList().get(0).getCreditCard();
                        if (creditCard != null)
                        {
                            System.out.println("        creditcardnum:"
                                    + creditCard.getCreditCardNumber());
                        }
                        BankAccount bankAccount = paymentProfile.getPaymentList().get(0).getBankAccount();
                        if (bankAccount != null)
                        {
                            System.out.println("        bankAccountnum:"
                                    + bankAccount.getBankAccountNumber());
                        }
                    }
                }
            }
        }
        else
        {
            /* Only one error message at a time is reported.
             So if you have multiple errors you won't know
             until you correct them one at a time. */
            String resultCode = result.getResultCode();
            ArrayList<Message> messageList = result.getMessages();
            System.out.println("  FAILED");
            if(messageList.size() > 0)
            {
                System.out.println("    ResultCode: " + messageList.get(0).getResultCode());
                System.out.println("    Code: " + messageList.get(0).getCode());
                System.out.println("    Text: " + messageList.get(0).getText());
            }

        }
    }

}
