/*
 * PropertyUtil.java
 *
 * Created on May 24, 2007, 3:16 PM
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.qp.onlinecommerce;

import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author prashant gaurav
 */
public class PropertyUtil {
    
    private static Properties prop;
    
    static {        
        try {
            prop = new Properties();            
            InputStream fin = PropertyUtil.class.getResourceAsStream("/config/onlinecommerce.properties");
            prop.load(fin);

        } catch (Exception ex) {
            System.out.println("Can't get file from config");
            ex.printStackTrace();
        }    
    }
    
    public static String getProperty(String propName) {        
        return prop.getProperty(propName);
    }
}
