/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.qp.onlinecommerce;

import java.util.ArrayList;
import java.math.BigDecimal;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import net.authorize.Environment;
import net.authorize.ResponseField;
import net.authorize.Transaction;
import net.authorize.cim.Result;
import net.authorize.xml.Message;
import net.authorize.cim.TransactionType;
import net.authorize.cim.ValidationModeType;
import net.authorize.data.Order;
import net.authorize.data.OrderItem;
import net.authorize.data.ShippingCharges;
import net.authorize.data.cim.CustomerProfile;
import net.authorize.data.cim.PaymentProfile;
import net.authorize.data.cim.PaymentTransaction;
import net.authorize.data.creditcard.AVSCode;
import net.authorize.data.creditcard.CreditCard;
import net.authorize.data.echeck.BankAccountType;
import net.authorize.data.echeck.ECheckType;
import net.authorize.data.xml.Address;
import net.authorize.data.xml.BankAccount;
import net.authorize.data.xml.Customer;
import net.authorize.data.xml.CustomerType;
import net.authorize.data.xml.Payment;

/**
 *
 * @author qpi
 */
public class CreditCardAccount {

    private String apiLoginId;
    private String transactionKey;

    private CIMTransaction cimTransaction;
    private Result<Transaction> result;

    public CreditCardAccount()
    {
        this.apiLoginId = PropertyUtil.getProperty("authorizenet.merchant.account.apiLoginId");
        this.transactionKey = PropertyUtil.getProperty("authorizenet.merchant.account.transactionKey");
    }

    public JSONObject createProfile (JSONObject json) throws JSONException
    {
        String memberId = json.getString("memberId");
        String merchantCustomerId = json.getString("merchantCustomerId");
        String transactionReferenceId = json.getString("transactionReferenceId");

        // Authorize.net maxlength for refId is 20 chars
        if (transactionReferenceId.length() > 20) transactionReferenceId = transactionReferenceId.substring(0,20);

        JSONObject reply = new JSONObject();
        reply.put("memberId", memberId);
        reply.put("transactionReferenceId", transactionReferenceId);

        cimTransaction = new CIMTransaction();
        cimTransaction.customerProfile = CustomerProfile.createCustomerProfile();
        cimTransaction.customerProfile.setMerchantCustomerId(merchantCustomerId);
        cimTransaction.customerProfile.setEmail(json.getString("email"));

        JSONObject creditCard = json.getJSONObject("creditCard");
        cimTransaction.creditCard = CreditCard.createCreditCard();
        cimTransaction.creditCard.setCardCode(creditCard.getString("securityCode"));
        cimTransaction.creditCard.setCreditCardNumber(creditCard.getString("creditCardNumber"));
        cimTransaction.creditCard.setExpirationDate(creditCard.getString("expirationYear") + "-" + creditCard.getString("expirationMonth"));

        JSONObject billingInfo = json.getJSONObject("billingInfo");
        cimTransaction.billingInfo = (Address) Address.createAddress();
        cimTransaction.billingInfo.setFirstName(billingInfo.getString("firstName"));
        cimTransaction.billingInfo.setLastName(billingInfo.getString("lastName"));
        if (billingInfo.has("company")) cimTransaction.billingInfo.setCompany(billingInfo.getString("company"));
        if(billingInfo.has("address1")){
        	cimTransaction.billingInfo.setAddress(billingInfo.getString("address1"));
        	cimTransaction.billingInfo.setCity(billingInfo.getString("city"));
            cimTransaction.billingInfo.setState(billingInfo.getString("stateProvince"));
            cimTransaction.billingInfo.setCountry(billingInfo.getString("country"));
            cimTransaction.billingInfo.setZipPostalCode(billingInfo.getString("zipPostalCode"));
        }
        
        if (billingInfo.has("phoneNumber")) cimTransaction.billingInfo.setPhoneNumber(billingInfo.getString("phoneNumber"));

        cimTransaction.paymentProfile = PaymentProfile.createPaymentProfile();
        cimTransaction.paymentProfile.setBillTo(cimTransaction.billingInfo);
        cimTransaction.paymentProfile.addPayment(Payment.createPayment(cimTransaction.creditCard));
        cimTransaction.paymentProfile.setCustomerType(CustomerType.INDIVIDUAL);

        result = cimTransaction.createCustomerProfile(apiLoginId, transactionKey, transactionReferenceId);

        if (result.isOk())
        {
            reply.put("customerProfileId", result.getCustomerProfileId());
            reply.put("customerPaymentProfileId", result.getCustomerPaymentProfileIdList().get(0));
        }
        else
        {
            if(result.getMessages().size() == 1 && "E00039".equals(result.getMessages().get(0).getCode())) {
               java.util.regex.Matcher m = Pattern.compile("^A duplicate record with ID (\\d+) already exists\\.$").matcher(result.getMessages().get(0).getText());
               if(m.find()) {
                   String customerProfileId = m.group(1);
                   json.put("transactionReferenceId", "UPDATE "+memberId);
                   json.put("customerProfileId", customerProfileId);
                   return updateProfile(json);
               }
            }
            addResultErrorDetails(reply);
        }

        reply.put("success", result.isOk());
        
        return reply;
    }

    public JSONObject updateProfile (JSONObject json) throws JSONException
    {
        JSONObject reply = new JSONObject();
        
        String memberId = json.getString("memberId");
        String customerProfileId = json.getString("customerProfileId");
        String transactionReferenceId = json.getString("transactionReferenceId");
        
        // Authorize.net maxlength for refId is 20 chars
        if (transactionReferenceId.length() > 20) transactionReferenceId = transactionReferenceId.substring(0,20);

        reply.put("transactionReferenceId", transactionReferenceId);

        // To update the customer profile, FIRST get the profile, then change the attributes
        // you wish to change, and then do the update. If you leave attributes undefined,
        // they will be removed from the CIM.

        cimTransaction = new CIMTransaction();
        result = cimTransaction.getCustomerProfile(apiLoginId, transactionKey, customerProfileId);

        if (result.isOk())
        {
            cimTransaction = new CIMTransaction();

            // load retrieved customer profile
            cimTransaction.customerProfile = result.getCustomerProfile();
            cimTransaction.paymentProfile = result.getCustomerPaymentProfileList().get(0); // Automatically assume we are updating the first payment profile (At least one should always be created by us when the customer profile is created)

            if (json.has("creditCard"))
            {
                JSONObject creditCard = json.getJSONObject("creditCard");

                cimTransaction.creditCard = CreditCard.createCreditCard();
                cimTransaction.creditCard.setCardCode(creditCard.getString("securityCode"));
                cimTransaction.creditCard.setCreditCardNumber(creditCard.getString("creditCardNumber"));
                cimTransaction.creditCard.setExpirationDate(creditCard.getString("expirationYear") + "-" + creditCard.getString("expirationMonth"));

                cimTransaction.paymentProfile.getPaymentList().get(0).setCreditCard(cimTransaction.creditCard);
            }

            if (json.has("billingInfo"))
            {
                JSONObject billingInfo = json.getJSONObject("billingInfo");

                cimTransaction.billingInfo = (Address) Address.createAddress();
                cimTransaction.billingInfo.setFirstName(billingInfo.getString("firstName"));
                cimTransaction.billingInfo.setLastName(billingInfo.getString("lastName"));
                if (billingInfo.has("company")) cimTransaction.billingInfo.setCompany(billingInfo.getString("company"));
                if(billingInfo.has("address1")){
                	cimTransaction.billingInfo.setAddress(billingInfo.getString("address1"));
                	cimTransaction.billingInfo.setCity(billingInfo.getString("city"));
                    cimTransaction.billingInfo.setState(billingInfo.getString("stateProvince"));
                    cimTransaction.billingInfo.setCountry(billingInfo.getString("country"));
                    cimTransaction.billingInfo.setZipPostalCode(billingInfo.getString("zipPostalCode"));
                }
                if (billingInfo.has("phoneNumber")) cimTransaction.billingInfo.setPhoneNumber(billingInfo.getString("phoneNumber"));

                cimTransaction.paymentProfile.setBillTo(cimTransaction.billingInfo);
            }

            // always update the existing payment profile
            // this prevents a problem where authorize.net does not allow multiple payment profiles with the same credit card number (updating a credit card expiration date)
            // it is not a problem to change the credit card on an existing payment profile, so we will do this regardless
            result = cimTransaction.updateCustomerPaymentProfile(apiLoginId, transactionKey, transactionReferenceId, customerProfileId, cimTransaction.paymentProfile.getCustomerPaymentProfileId());
            
            if (result.isOk())
            {
                reply.put("customerProfileId", customerProfileId);
                reply.put("customerPaymentProfileId", cimTransaction.paymentProfile.getCustomerPaymentProfileId());
            }
            else
            {
                // failed to retrieve the updated customer profile
                reply.put("customerProfileId", customerProfileId);
                addResultErrorDetails(reply);
            }
        }
        else
        {
            // failed to retrieve the customer profile to update
            reply.put("customerProfileId", customerProfileId);
            addResultErrorDetails(reply);
        }

        reply.put("success", result.isOk());

        return reply;
    }

    public JSONObject validateProfile (JSONObject json) throws JSONException
    {
        JSONObject reply = new JSONObject();

        String transactionReferenceId = json.getString("transactionReferenceId");
        String customerProfileId = json.getString("customerProfileId");
        String customerPaymentProfileId = json.getString("customerPaymentProfileId");
        String securityCode = "";

        // Authorize.net maxlength for refId is 20 chars
        if (transactionReferenceId.length() > 20) transactionReferenceId = transactionReferenceId.substring(0,20);

        reply.put("transactionReferenceId", transactionReferenceId);

        cimTransaction = new CIMTransaction();
        result = cimTransaction.validateCustomerPaymentProfile(apiLoginId, transactionKey,
                                    transactionReferenceId, customerProfileId, customerPaymentProfileId, securityCode);

        reply.put("success", result.isOk());
        addResultErrorDetails(reply);

        return reply;
    }
    
    public JSONObject deleteCustomerProfile (JSONObject json) throws JSONException
    {
        JSONObject reply = new JSONObject();

        String transactionReferenceId = json.getString("transactionReferenceId");
        String customerProfileId = json.getString("customerProfileId");

        // Authorize.net maxlength for refId is 20 chars
        if (transactionReferenceId.length() > 20) transactionReferenceId = transactionReferenceId.substring(0,20);

        cimTransaction = new CIMTransaction();
        result = cimTransaction.deleteCustomerProfile(apiLoginId, transactionKey, transactionReferenceId, customerProfileId);

        reply.put("success", result.isOk());
        reply.put("transactionReferenceId", transactionReferenceId);

        addResultErrorDetails(reply);

        return reply;
    }

    public JSONObject deleteCustomerPaymentProfile (JSONObject json) throws JSONException
    {
        JSONObject reply = new JSONObject();

        String transactionReferenceId = json.getString("transactionReferenceId");
        String customerProfileId = json.getString("customerProfileId");
        String customerPaymentProfileId = json.getString("customerPaymentProfileId");

        // Authorize.net maxlength for refId is 20 chars
        if (transactionReferenceId.length() > 20) transactionReferenceId = transactionReferenceId.substring(0,20);

        cimTransaction = new CIMTransaction();
        result = cimTransaction.deleteCustomerPaymentProfile(apiLoginId, transactionKey,
                                    transactionReferenceId, customerProfileId, customerPaymentProfileId);

        reply.put("success", result.isOk());
        reply.put("transactionReferenceId", transactionReferenceId);

        addResultErrorDetails(reply);

        return reply;
    }

    public JSONObject chargeFromAccount (JSONObject json) throws JSONException
    {
        JSONObject reply = new JSONObject();

        String transactionReferenceId = json.getString("transactionReferenceId");
        String customerProfileId = json.getString("customerProfileId");
        String customerPaymentProfileId = json.getString("customerPaymentProfileId");
        String securityCode = "";

        // Authorize.net maxlength for refId is 20 chars
        if (transactionReferenceId.length() > 20) transactionReferenceId = transactionReferenceId.substring(0,20);

        reply.put("transactionReferenceId", transactionReferenceId);

        cimTransaction = new CIMTransaction();
        cimTransaction.order = Order.createOrder();
        cimTransaction.order.setDescription(json.getString("description"));
        cimTransaction.order.setInvoiceNumber(transactionReferenceId);
        cimTransaction.order.setTotalAmount(new BigDecimal(json.getString("totalAmount")));

        result = cimTransaction.createCustomerProfileTransaction_AuthCapture(apiLoginId, transactionKey,
                                    transactionReferenceId, customerProfileId, customerPaymentProfileId, securityCode);

        if (result.isOk())
        {
            String cimTransactionId = result.getDirectResponseList().get(0).getDirectResponseMap().get(ResponseField.TRANSACTION_ID);
            reply.put("cimTransactionId", cimTransactionId);
            reply.put("statusId", "1");
        }
        else
        {
            addResultErrorDetails(reply);
        }

        reply.put("success", result.isOk());

        return reply;
    }

    public JSONObject creditToAccount (JSONObject json) throws JSONException
    {
        return creditCIMTransaction(json);
    }

    public JSONObject creditCIMTransaction (JSONObject json) throws JSONException
    {
        // Create a credit against a previous charge against a credit card --
        // must be done within 120 days of transaction.

        JSONObject reply = new JSONObject();

        String transactionReferenceId = json.getString("transactionReferenceId");
        String customerProfileId = json.getString("customerProfileId");
        String customerPaymentProfileId = json.getString("customerPaymentProfileId");
        String cimTransactionId = json.getString("cimTransactionId");

        // Authorize.net maxlength for refId is 20 chars
        if (transactionReferenceId.length() > 20) transactionReferenceId = transactionReferenceId.substring(0,20);

        reply.put("transactionReferenceId", transactionReferenceId);

        cimTransaction = new CIMTransaction();
        cimTransaction.order = Order.createOrder();
        cimTransaction.order.setTotalAmount(new BigDecimal(json.getString("totalAmount")));

        result = cimTransaction.createCustomerProfileTransaction_Credit(apiLoginId, transactionKey,
                                    transactionReferenceId, customerProfileId, customerPaymentProfileId, cimTransactionId);

        reply.put("success", result.isOk());
        addResultErrorDetails(reply);

        return reply;
    }

    public JSONObject voidCIMTransaction (JSONObject json) throws JSONException
    {
        // Void a previous charge against a credit card -- only works for transactions
        // that are pending and not for transactions which have cleared. For transactions
        // that have cleared, use creditCITTransaction.

        JSONObject reply = new JSONObject();

        String transactionReferenceId = json.getString("transactionReferenceId");
        String customerProfileId = json.getString("customerProfileId");
        String customerPaymentProfileId = json.getString("customerPaymentProfileId");
        String cimTransactionId = json.getString("cimTransactionId");

        // Authorize.net maxlength for refId is 20 chars
        if (transactionReferenceId.length() > 20) transactionReferenceId = transactionReferenceId.substring(0,20);

        reply.put("transactionReferenceId", transactionReferenceId);

        cimTransaction = new CIMTransaction();
        result = cimTransaction.createCustomerProfileTransaction_Void(apiLoginId, transactionKey,
                                    transactionReferenceId, customerProfileId, customerPaymentProfileId, cimTransactionId);

        reply.put("success", result.isOk());
        addResultErrorDetails(reply);

        return reply;
    }

    private void addResultErrorDetails(JSONObject reply) throws JSONException
    {
        JSONObject errors = new JSONObject();

        if (result != null && result.isError())
        {
            // NOTE that only one error is reported at a time is reported.
            String resultCode = result.getResultCode();
            ArrayList<Message> messageList = result.getMessages();
            if (messageList.size() > 0) {
                reply.put("statusId", messageList.get(0).getResultCode());
                reply.put("statusReasonCode", messageList.get(0).getCode());
                reply.put("statusReasonText", messageList.get(0).getText());
            } else {
            	reply.put("statusId", "3");
            	reply.put("statusReasonText", "Unknown processing error; No messages provided.");
            }
        } else {
        	reply.put("statusId", "1");
        }
    }
}
