

package com.qp.onlinecommerce;

import net.authorize.Environment;
import net.authorize.ResponseField;
import net.authorize.Transaction;
import net.authorize.Merchant;
import net.authorize.cim.Result;
import net.authorize.cim.TransactionType;
import net.authorize.cim.ValidationModeType;
import net.authorize.data.Order;
import net.authorize.data.OrderItem;
import net.authorize.data.cim.CustomerProfile;
import net.authorize.data.cim.PaymentProfile;
import net.authorize.data.cim.PaymentTransaction;
import net.authorize.data.creditcard.CreditCard;
import net.authorize.data.echeck.BankAccountType;
import net.authorize.data.echeck.ECheckType;
import net.authorize.data.xml.Address;
import net.authorize.data.xml.BankAccount;
import net.authorize.data.xml.Customer;
import net.authorize.data.xml.Payment;

/**
 *
 * @author suz
 */
public class CIMTransaction {
    String authCode = null;
	String transactionId = null;

	CreditCard creditCard;
	BankAccount bankAccount;
	Address billingInfo;
	Address shippingInfo;
	Customer customer;
	CustomerProfile customerProfile;
	Merchant merchant;
	Order order;
	PaymentProfile paymentProfile;
	Payment payment;
	PaymentTransaction paymentTransaction;
	
	String env =  PropertyUtil.getProperty("authorizenet.merchant.account.transactionMode");
    /*
	set attributes in the following before calling:
	    customerProfile
	    paymentProfile
     *

    */
    public Result<Transaction> createCustomerProfile(String apiLoginId, String transactionKey, String refId)
    {
	merchant = Merchant.createMerchant( getEnvironment() , apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.CREATE_CUSTOMER_PROFILE);

	transaction.setRefId(refId);
	transaction.setCustomerProfile(customerProfile);
	transaction.addPaymentProfile(paymentProfile);

	transaction.setValidationMode(getValidationModeType());

	return (Result<Transaction>)merchant.postTransaction(transaction);
    }

    private Environment getEnvironment()
    {
    	return (env.equals("production"))?Environment.PRODUCTION:Environment.SANDBOX;
    }

    private ValidationModeType getValidationModeType()
    {
    	return (env.equals("production"))?ValidationModeType.LIVE_MODE:ValidationModeType.TEST_MODE;
    }
    
    /*
	must set:
	    customerProfile

	recomend get customerProfile and then change the values that need to be changed
    */
    public Result<Transaction> updateCustomerProfile(String apiLoginId, String transactionKey,
            String refId, String customerProfileId)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.UPDATE_CUSTOMER_PROFILE);

        transaction.setRefId(refId);
	transaction.setCustomerProfile(customerProfile);
	transaction.setCustomerProfileId(customerProfileId);

	return (Result<Transaction>)merchant.postTransaction(transaction);
    }
/*
 *     will not return refId
 *
 *     returns all payment profiles in result.getCustomerPaymentProfileList()
 *     BUT there is only one item in result.getCustomerPaymentProfileIdList()
 */
    public Result<Transaction> getCustomerProfile(String apiLoginId, String transactionKey,
	String customerProfileId)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	    merchant.createCIMTransaction(TransactionType.GET_CUSTOMER_PROFILE);

	transaction.setCustomerProfileId(customerProfileId);

	return (Result<Transaction>)merchant.postTransaction(transaction);
    }


    /*
    */
    public Result<Transaction> deleteCustomerProfile(String apiLoginId, String transactionKey,
	String refId, String customerProfileId)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.DELETE_CUSTOMER_PROFILE);
	transaction.setRefId(refId);
	transaction.setCustomerProfileId(customerProfileId);

	return (Result<Transaction>)merchant.postTransaction(transaction);

    }

    /*
        must set:
	    paymentProfile
    */
    public Result<Transaction> createCustomerPaymentProfile(String apiLoginId, String transactionKey,
	String refId, String customerProfileId)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.CREATE_CUSTOMER_PAYMENT_PROFILE);
	transaction.setRefId(refId);
	transaction.setCustomerProfileId(customerProfileId);
	transaction.addPaymentProfile(paymentProfile);
	/*  TODO: set to LIVE_MODE TEST_MODE*/
	transaction.setValidationMode(getValidationModeType());

	return (Result<Transaction>)merchant.postTransaction(transaction);
    }

    /*
        must set:
	    paymentProfile
    */
    public Result<Transaction> updateCustomerPaymentProfile(String apiLoginId, String transactionKey,
	String refId, String customerProfileId, String customerPaymentProfileId)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.UPDATE_CUSTOMER_PAYMENT_PROFILE);
	transaction.setRefId(refId);
	transaction.setCustomerProfileId(customerProfileId);
	paymentProfile.setCustomerPaymentProfileId(customerPaymentProfileId);
	transaction.addPaymentProfile(paymentProfile);
	/*  TODO: set to LIVE_MODE  */
	transaction.setValidationMode(getValidationModeType());

	return (Result<Transaction>)merchant.postTransaction(transaction);
    }

    public Result<Transaction> getCustomerPaymentProfile(String apiLoginId, String transactionKey,
	    String customerProfileId, String customerPaymentProfileId)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.GET_CUSTOMER_PAYMENT_PROFILE);

	transaction.setCustomerProfileId(customerProfileId);
	transaction.setCustomerPaymentProfileId(customerPaymentProfileId);

	return (Result<Transaction>)merchant.postTransaction(transaction);
    }


    public Result<Transaction> deleteCustomerPaymentProfile(String apiLoginId, String transactionKey,
	    String refId, String customerProfileId, String customerPaymentProfileId)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.DELETE_CUSTOMER_PAYMENT_PROFILE);
	transaction.setRefId(refId);
	transaction.setCustomerProfileId(customerProfileId);
	transaction.setCustomerPaymentProfileId(customerPaymentProfileId);
	/*  TODO: set to LIVE_MODE  */
	transaction.setValidationMode(getValidationModeType());

	return (Result<Transaction>)merchant.postTransaction(transaction);
    }

    /*
        pass in cardholderAuthenticationValue as null if do not want to use
    */
    public Result<Transaction> validateCustomerPaymentProfile(String apiLoginId, String transactionKey,
	String refId, String customerProfileId, String customerPaymentProfileId,
	String cardholderAuthenticationValue)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.VALIDATE_CUSTOMER_PAYMENT_PROFILE);

	transaction.setCustomerProfileId(customerProfileId);
	transaction.setCustomerPaymentProfileId(customerPaymentProfileId);
	//transaction.setCustomerShippingAddressId(customerShippingAddressId);
	if(!cardholderAuthenticationValue.equals(""))
	    transaction.setCardCode(cardholderAuthenticationValue);
	transaction.setValidationMode(getValidationModeType());

	return (Result<Transaction>)merchant.postTransaction(transaction);
    }

    /*
        must create and assign values to order items and order
    */
    public Result<Transaction> createCustomerProfileTransaction_AuthCapture(String apiLoginId,
	String transactionKey, String refId, String customerProfileId, String customerPaymentProfileId,
	String cardholderAuthenticationValue)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.CREATE_CUSTOMER_PROFILE_TRANSACTION);
	transaction.setRefId(refId);
	transaction.setCustomerProfileId(customerProfileId);
	transaction.setCustomerPaymentProfileId(customerPaymentProfileId);
	//transaction.setCustomerShippingAddressId(customerShippingAddressId);
	paymentTransaction = PaymentTransaction.createPaymentTransaction();
	paymentTransaction.setTransactionType(net.authorize.TransactionType.AUTH_CAPTURE);
	paymentTransaction.setOrder(order);
	
	transaction.setPaymentTransaction(paymentTransaction);
        transaction.addExtraOption("ip_address","127.0.0.1");
        transaction.setValidationMode(getValidationModeType());

	return (Result<Transaction>)merchant.postTransaction(transaction);

    }

    /*
        transactionId should have been returned from a previous call to
	createCustomerProfileTransaction_AuthCapture
    */
    public Result<Transaction> createCustomerProfileTransaction_Void(String apiLoginId,
	String transactionKey, String refId, String customerProfileId, String customerPaymentProfileId,
	String transactionId)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.CREATE_CUSTOMER_PROFILE_TRANSACTION);
	transaction.setRefId(refId);
	transaction.setCustomerProfileId(customerProfileId);
	transaction.setCustomerPaymentProfileId(customerPaymentProfileId);
        paymentTransaction = PaymentTransaction.createPaymentTransaction();
	paymentTransaction.setOrder(null);
	paymentTransaction.setTransactionType(net.authorize.TransactionType.VOID);
	paymentTransaction.setTransactionId(transactionId);
        transaction.addExtraOption("aType","unknown");
	transaction.setPaymentTransaction(paymentTransaction);

	return (Result<Transaction>)merchant.postTransaction(transaction);
    }

/*
        transactionId should have been returned from a previous call to
	createCustomerProfileTransaction_AuthCapture
    */
    public Result<Transaction> createCustomerProfileTransaction_Credit(String apiLoginId,
	String transactionKey, String refId, String customerProfileId, String customerPaymentProfileId,
	String transactionId)
    {
	merchant = Merchant.createMerchant( getEnvironment(), apiLoginId, transactionKey);
	net.authorize.cim.Transaction transaction =
	  merchant.createCIMTransaction(TransactionType.CREATE_CUSTOMER_PROFILE_TRANSACTION);
	transaction.setRefId(refId);
	transaction.setCustomerProfileId(customerProfileId);
	transaction.setCustomerPaymentProfileId(customerPaymentProfileId);
        paymentTransaction = PaymentTransaction.createPaymentTransaction();
	paymentTransaction.setOrder(order);
	paymentTransaction.setTransactionType(net.authorize.TransactionType.CREDIT);
	paymentTransaction.setTransactionId(transactionId);
        paymentTransaction.setCustomerPaymentProfileId(customerPaymentProfileId);

	transaction.setPaymentTransaction(paymentTransaction);
        transaction.addExtraOption("aType","unknown");

	return (Result<Transaction>)merchant.postTransaction(transaction);
    }


}
