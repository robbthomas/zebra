package com.alleni.db.util;

import java.io.PrintWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

public class QueryTools {
	public static class Row implements Map<String, Object> {
		private Table table;
		private Object[] data;
		
		public Row(Table t, ResultSet rs) throws SQLException {
			this.table = t;
			this.data = new Object[t.getNumColumns()];
            for (int i = 0; i < t.getNumColumns(); i++)
            {
                int type = table.getColumnType(i);
                int rsIndex = i+1;
                switch(type)
                {
                 case java.sql.Types.BOOLEAN:
                	data[i] = rs.getBoolean(rsIndex);
                    break;
                 case java.sql.Types.CHAR:
                 	data[i] = rs.getByte(rsIndex); // is this the right type?
                    break;
                 case java.sql.Types.DATE:
                    data[i] = rs.getDate(rsIndex);
                    break;
                 case java.sql.Types.DOUBLE:
                    data[i] = rs.getDouble(rsIndex);
                    break;
                 case java.sql.Types.FLOAT:
                    data[i] = rs.getFloat(rsIndex);
                    break;
                 case java.sql.Types.INTEGER:
                    data[i] = rs.getInt(rsIndex);
                    break;
                 case java.sql.Types.TIMESTAMP:
                    data[i] = rs.getTimestamp(rsIndex);
                    break;
                 case java.sql.Types.VARCHAR:
                    data[i] = rs.getString(rsIndex);
                    break;
                 default:
                    // TODO - log this please
                    data[i] = rs.getString(rsIndex);
                    break;
                }
            }
		}
		
		public Object getColumn(int index) {
			return data[index];
		}

		public void clear() {
			throw new UnsupportedOperationException("Row cannot be structurally changed");
		}

		public boolean containsKey(Object key) {
			return table.getColumnIndex(key) >= 0;
		}

		public boolean containsValue(Object value) {
			for(int i=0; i<data.length; i++) {
				if((value == null && data[i] == null) || value.equals(data[i])) {
					return true;
				} 
			}
			return false;
		}

		public Set<java.util.Map.Entry<String, Object>> entrySet() {
			return new AbstractSet<Map.Entry<String,Object>>() {

				@Override
				public Iterator<java.util.Map.Entry<String, Object>> iterator() {
					return new RowDataIterator<Map.Entry<String,Object>>(IteratorType.ENTRY);
				}

				@Override
				public int size() {
					return table.getNumColumns();
				}
			};
		}

		public Object get(Object key) {
			return data[table.getColumnIndex(key)];
		}

		public boolean isEmpty() {
			return table.getNumColumns() == 0;
		}

		public Set<String> keySet() {
			return new AbstractSet<String>() {

				@Override
				public Iterator<String> iterator() {
					return new RowDataIterator<String>(IteratorType.KEY);
				}

				@Override
				public int size() {
					return table.getNumColumns();
				}
			};
		}

		public Object put(String key, Object value) {
			int index = table.getColumnIndex(key);
			Object old = data[index];
			data[index] = value;
			return old;
		}

		public void putAll(Map<? extends String, ? extends Object> m) {
			@SuppressWarnings("unchecked")
			Map<String, Object> m2 = (Map<String, Object>) m;
			for(Entry<String, Object> e : m2.entrySet()) {
				put(e.getKey(), e.getValue());
			}
		}

		public Object remove(Object key) {
			throw new UnsupportedOperationException("Removal of Table/Row column is not allowed");
		}

		public int size() {
			return table.getNumColumns();
		}

		public Collection<Object> values() {
			return new AbstractCollection<Object>() {

				@Override
				public Iterator<Object> iterator() {
					return new RowDataIterator<Object>(IteratorType.VALUE);
				}

				@Override
				public int size() {
					return table.getNumColumns();
				}
			};
		}
		
		private class RowDataEntry implements Entry<String, Object> {
			private String key;
			private int index;

			public RowDataEntry(String key, int index) {
				this.key = key;
				this.index = index;
			}

			public String getKey() {
				return key;
			}

			public Object getValue() {
				return data[index];
			}

			public Object setValue(Object arg0) {
				Object old = data[index];
				data[index] = arg0;
				return old;
			}
			
		}
		
		private static enum IteratorType {
			ENTRY, KEY, VALUE;
		}
		
		private class RowDataIterator<T> implements Iterator<T> {
			private IteratorType type;
			private String[] columns;
			private int index = 0;
			private int numColumns;
			
			public RowDataIterator(IteratorType type) {
				this.type = type;
				if(type != IteratorType.VALUE) {
					columns = table.getColumns();
				}
				numColumns = table.getNumColumns();
			}

			public boolean hasNext() {
				return index < numColumns;
			}

			@SuppressWarnings("unchecked")
			public T next() {
				if(index >= numColumns) {
					return null;
				}
				int i = index++;
				switch(type) {
					case KEY: return (T)columns[i];
					case VALUE: return (T)data[i];
					default: return (T)new RowDataEntry(columns[i], i);
				}
			}

			public void remove() {
				throw new UnsupportedOperationException("Removal of Table/Row column is not allowed");
			}
			
		}
		
	}
	public interface Table extends Iterable<Row> {
		public int getColumnIndex(Object name);
		public String getColumnName(int index);
		public int getColumnType(int index);
		public int getNumColumns();
		public String[] getColumns();
	}
	
	public static abstract class AbstractTable implements Table {
		private Map<String, Integer> columns = new HashMap<String, Integer>();
		private String[] columnNames;
		private int[] columnTypes;
		
		public AbstractTable(ResultSetMetaData rsmd) throws SQLException {
			int count = rsmd.getColumnCount();
			columnNames = new String[count];
			columnTypes = new int[count];
        	for (int i = 1; i < count+1; i++) {
	            String label = rsmd.getColumnLabel(i);
	       	 	columns.put(label, i-1);
	       	 	columnNames[i-1] = label;
	       	 	columnTypes[i-1] = rsmd.getColumnType(i);
        	}
		}
		
		public int getColumnIndex(Object name) {
			Integer result = columns.get(name);
			if(result == null) {
				return -1;
			} else {
				return result.intValue();
			}
		}
		public String getColumnName(int index) {
			return columnNames[index];
		}
		public int getColumnType(int index) {
			return columnTypes[index];
		}
		public int getNumColumns() {
			return columnNames.length;
		}
		public String[] getColumns() {
			return Arrays.copyOf(columnNames, columnNames.length);
		}
		
	}
	
	public static class LazyTable extends AbstractTable {
		private ResultSet rs;
		private Row next = null;
		
		public LazyTable(ResultSet rs) throws SQLException {
			super(rs.getMetaData());
			this.rs = rs;
		}
		
		private void advance() {
			try {
				if(rs.next()) {
					next = new Row(this, rs);
				} else {
					close();
				}
			} catch (SQLException e) {
				close();
			}
		}
		
		public void close() {
			next = null;
			try {
				rs.close();
			} catch(Exception e) {
				// ignore
			} finally {
				rs = null;
			}
		}

		public Iterator<Row> iterator() {
			return new Iterator<Row>() {

				public boolean hasNext() {
					return next != null;
				}

				public Row next() {
					Row current = next;
					advance();
					return current;
				}

				public void remove() {
					throw new UnsupportedOperationException("Row removal is not supported");
				}
			};
		}
		
	}
	
	public static class FullTable extends AbstractTable implements List<Row> {
		private List<Row> rows = new ArrayList<Row>();
		
		public FullTable(ResultSet rs) throws SQLException {
			super(rs.getMetaData());
			while(rs.next()) {
				rows.add(new Row(this, rs));
			}
			rs.close();
		}
		public boolean add(Row e) {
			return rows.add(e);
		}
		public void add(int index, Row element) {
			rows.add(index, element);
		}
		public boolean addAll(Collection<? extends Row> c) {
			return rows.addAll(c);
		}
		public boolean addAll(int index, Collection<? extends Row> c) {
			return rows.addAll(index, c);
		}
		public void clear() {
			rows.clear();
		}
		public boolean contains(Object o) {
			return rows.contains(o);
		}
		public boolean containsAll(Collection<?> c) {
			return rows.containsAll(c);
		}
		public Row get(int index) {
			return rows.get(index);
		}
		public int indexOf(Object o) {
			return rows.indexOf(o);
		}
		public boolean isEmpty() {
			return rows.isEmpty();
		}
		public Iterator<Row> iterator() {
			return rows.iterator();
		}
		public int lastIndexOf(Object o) {
			return rows.lastIndexOf(o);
		}
		public boolean equals(Object o) {
			return rows.equals(o);
		}
		public int hashCode() {
			return rows.hashCode();
		}
		public ListIterator<Row> listIterator() {
			return rows.listIterator();
		}
		public ListIterator<Row> listIterator(int index) {
			return rows.listIterator(index);
		}
		public Row remove(int index) {
			return rows.remove(index);
		}
		public boolean remove(Object o) {
			return rows.remove(o);
		}
		public boolean removeAll(Collection<?> c) {
			return rows.removeAll(c);
		}
		public boolean retainAll(Collection<?> c) {
			return rows.retainAll(c);
		}
		public Row set(int index, Row element) {
			return rows.set(index, element);
		}
		public int size() {
			return rows.size();
		}
		public List<Row> subList(int fromIndex, int toIndex) {
			return rows.subList(fromIndex, toIndex);
		}
		public Object[] toArray() {
			return rows.toArray();
		}
		public <T> T[] toArray(T[] a) {
			return rows.toArray(a);
		}
		

		
		public static String csvEscape(Object o) {
			if(o == null) {
				return "";
			}
			String s = o.toString();
			boolean quote = false;
			if(s.contains("\"")) {
				quote = true;
				s = s.replace("\\\"", "\"\"");
			} else if(s.contains("\n")) {
				quote = true;
			} else if(s.contains(",")) {
				quote = true;
			} else if(s.startsWith(" ") || s.endsWith(" ")){
				quote = true;
			}
			if(quote) {
				return "\"" + s + "\"";
			} else {
				return s;
			}
		}
		
		public void writeCSV(Writer w) {
			if(getNumColumns() == 0) {
				return;
			}
			PrintWriter p = new PrintWriter(w);
			String[] keys = getColumns();
			p.print(keys[0]);
			for(int i=1; i<keys.length; i++) {
				p.print(",");
				p.print(csvEscape(keys[i]));
			}
			for(Row r : rows) {
				p.println();
				p.print(csvEscape(r.getColumn(0)));
				for(int i=1; i<keys.length; i++) {
					p.print(",");
					p.print(csvEscape(r.getColumn(i)));
				}
			}
		}
		
		public static String htmlEscape(Object o) {
			if(o == null) {
				return "&nbsp;";
			}
			String s = o.toString().trim();
			if(s.length() == 0) {
				return "&nbsp;";
			}
			s = s.replace("&", "&amp;")
					.replace(";", "&semi;")
					.replace("/", "&sol;")
					.replace("\'", "&apos;")
					.replace("\"", "&quot;")
					.replace("<", "&lt;")
					.replace(">", "&gt;");
			return s;
		}
		
		public void writeHTML(Writer w) {
			if(getNumColumns() == 0) {
				return;
			}
			PrintWriter p = new PrintWriter(w);
			String[] keys = getColumns();
			p.println("<table border=1>");
			p.println("<tr>");
			for(int i=0; i<keys.length; i++) {
				p.print("<th>");
				p.print(htmlEscape(keys[i]));
				p.print("</th>");
			}
			p.println("</tr>");
			for(Row r : rows) {
				p.println("<tr>");
				for(int i=0; i<keys.length; i++) {
					p.print("<td>");
					p.print(htmlEscape(r.getColumn(i)));
					p.print("</td>");
				}
				p.println("</tr>");
			}
			p.println("<table border=1>");
		}
	}

    public static class RuntimeSqlException extends RuntimeException {
        public RuntimeSqlException(SQLException e) {
            super(e);
        }
    }

    public static class DbCon {

        public static interface MessageHandler {
            void handleMessage(String msg);
        }
		private Connection con;
        private MessageHandler mh;
        public DbCon() {
            this(new MessageHandler() {
                public void handleMessage(String msg) {
                    System.out.println(msg);
                }
            });
        }
        public DbCon(MessageHandler mh) {
            try {
                con = JDBCHelper.getJdbcConnection();
                this.mh = mh;
            } catch (SQLException e) {
                throw new RuntimeSqlException(e);
            }
        }

        public List<FullTable> query(String sql, Object[] args) {
            Statement s = null;
		    ResultSet rs = null;
            List<FullTable> result = new ArrayList<FullTable>();
            try {
                if(args == null) {
                    s = con.createStatement();
                    s.execute(sql);
                } else {
                    s = con.prepareStatement(sql);
                    if(args != null) {
                        for(int i=0; i<args.length; i++) {
                        	Object o = args[i];
                        	if (o instanceof String)
                        		((PreparedStatement)s).setString(i+1, (String)o);
                        	else if (o instanceof Boolean) 
                        		((PreparedStatement)s).setBoolean(i+1, (Boolean)o);
                        	else if (o instanceof java.util.Date)
                        		((PreparedStatement)s).setDate(i+1, (new java.sql.Date( ((java.util.Date)o).getTime())   ));
                        	else if (o instanceof Integer)
                        		((PreparedStatement)s).setInt(i+1, (Integer)o);
                        	else
                        		throw new IllegalArgumentException();
                        }
                    }
                    ((PreparedStatement)s).execute();
                }
                do {
                    rs = s.getResultSet();
                    int update = s.getUpdateCount();
                    if(update >= 0) {
                        mh.handleMessage("Updated [" + update + "] rows.");
                    } else {
                        SQLWarning warning = rs.getWarnings();
                        while(warning != null) {
                            mh.handleMessage(warning.getSQLState() + " " + warning.getErrorCode() + " " + warning.getMessage());
                            warning = warning.getNextWarning();
                        }
                        result.add(new FullTable(rs));
                    }

                } while(s.getMoreResults(Statement.CLOSE_CURRENT_RESULT) || s.getUpdateCount() >= 0);
                SQLWarning warning = s.getWarnings();
                while(warning != null) {
                    mh.handleMessage(warning.getSQLState() + " " + warning.getErrorCode() + " " + warning.getMessage());
                    warning = warning.getNextWarning();
                }
            } catch(SQLException e) {
                throw new RuntimeSqlException(e);
            } finally {
                if(rs != null) try {
                    rs.close();
                } catch(SQLException e) {
                    // ignore
                } finally {
                    rs = null;

                    if(s != null) try {
                        s.close();
                    } catch(SQLException e) {
                        // ignore
                    } finally {
                        s = null;
                    }
                }
            }
            return result;
        }

        public void close() {
            if(con != null) try {
                con.close();
            } catch(SQLException e) {
                // ignore
            } finally {
                con = null;
            }
        }
    }
	
	public static FullTable query(String sql) throws SQLException {
		Connection con = JDBCHelper.getJdbcConnection();
		Statement s = con.createStatement();
		ResultSet rs = s.executeQuery(sql);
		FullTable t = new FullTable(rs);
		JDBCHelper.close(rs);
		JDBCHelper.close(s);
		JDBCHelper.close(con);
		return t;
	}

	public static FullTable queryPrepared(String sql, String[] args) throws SQLException {
		Connection con = JDBCHelper.getJdbcConnection();
		PreparedStatement s = con.prepareStatement(sql);
        for(int i=0; i<args.length; i++) {
		    s.setString(i+1, args[i]);
        }
        ResultSet rs = s.executeQuery();
		FullTable t = new FullTable(rs);
		JDBCHelper.close(rs);
		JDBCHelper.close(s);
		JDBCHelper.close(con);
		return t;
	}
}
