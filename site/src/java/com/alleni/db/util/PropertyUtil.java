/*
 * PropertyUtil.java
 *
 * Created on May 24, 2007, 3:16 PM
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.alleni.db.util;

import java.io.InputStream;
import java.util.Properties;
import com.alleni.util.Utility;

/**
 *
 * @author prashant gaurav
 */
public class PropertyUtil {
    
    private static Properties prop;
    
    static {        
        try {
            prop = new Properties();               
            
            InputStream fin = PropertyUtil.class.getResourceAsStream("/config/environment.properties");
            //InputStream fin = new FileInputStream(url.get) ;
            prop.load(fin);
            
            Utility.log("Property loaded.");
        } catch (Exception ex) {
            System.out.println("Can't get file from config");
            ex.printStackTrace();
        }    
    }
    
    public static String getProperty(String propName) {        
        Utility.log("Getting property "+propName+" : "+prop.getProperty(propName));
        return prop.getProperty(propName);
    }    
}
