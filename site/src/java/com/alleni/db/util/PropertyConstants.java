/*
 * PropertyConstants.java
 *
 * Created on May 24, 2007, 3:36 PM
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.alleni.db.util;

/**
 *
 * @author prashant gaurav
 */
public class PropertyConstants {
    
    public static final String DB_URL       = "db.url";
    public static final String DB_USER      = "db.username";
    public static final String DB_PASSWORD  = "db.password";
    public static final String DB_DATABASE  = "db.database";
    public static final String DB_SCHEMA    = "db.schema";
    public static final String DB_DRIVER    = "db.driver";
    
    
    public static final String SMTP_USER                    = "mail.smtp.user";
    public static final String SMTP_PASSWORD                = "mail.smtp.password";
    public static final String SMTP_HOST                    = "mail.smtp.host";
    public static final String SMTP_PORT                    = "mail.smtp.port";
    public static final String SMTP_AUTH                    = "mail.smtp.auth";
    public static final String SMTP_DEBUG                   = "mail.smtp.debug";
    public static final String SMTP_STARTTLS                = "mail.smtp.starttls.enable";
    public static final String SMTP_SOCKET_PORT             = "mail.smtp.socketFactory.port";
    public static final String SMTP_SOCKET_FACTORY_CLASS    = "mail.smtp.socketFactory.class";
    public static final String SMTP_SOCKET_FACTORY_FALLBACK = "mail.smtp.socketFactory.fallback";            
    
    
    public static final String FAX_USERNAME = "fax.username";            
    public static final String FAX_PASSWORD = "fax.password";            

}
