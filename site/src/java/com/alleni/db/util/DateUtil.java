/*
 * DateUtil.java
 *
 * Created on May 22, 2007, 7:08 PM
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.alleni.db.util;

/**
 *
 * @author prashant gaurav
 */

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * This an utility class provides static methods for dates to perform
 * manipulate, format, compare, and other date related operations.
 */
public class DateUtil {

    /* Date representing January 1st 2003 can be converted to string
      * of any following patterns. And similarly  a Date string
      * representing any of following formats can be converted
      * back to Date object.
      */

    /**
     * Example - 01/01/03 (MM/dd/yy)
     */
    public static final String DEFAULT_DATE_FORMAT = "MM/dd/yy";

    /**
     * Example - 01/01/2003 (MM/dd/yyyy)
     */
    public static final String JSP_FORMAT = "MM/dd/yyyy";

    /**
     * Example - 2003-01-01 (yyyy-MM-dd)
     */
    public static final String DB_FORMAT = "yyyy-MM-dd";

    public static final String DB_FORMAT_LONG = "yyyy-MM-dd H:mm:ss.SS";
    /**
     * Example - 01/01/2003 (MM/dd/yyyy)
     */
    public static final String NUMBER_DATE_FULL_FORMAT = "MM/dd/yyyy";

    /**
     * Example - 1/1/03 (M/d/yy)
     */
    public static final String NUMBER_DATE_ABBR_FORMAT = "M/d/yy";

    /**
     * Example - 01/01/2003 6:15:30 (MM/dd/yyyy H:mm:ss)
     */
    public static final String NUMBER_DATE_TIME_FORMAT = "MM/dd/yyyy H:mm:ss";

    /**
     * Example - 01/01/2003 6:15:30 PM,PDT (MM/dd/yyyy h:mm:ss a,z)
     */
    public static final String NUMBER_DATE_TIME_AM_FORMAT =
            "MM/dd/yyyy h:mm:ss a,z";

    /**
     * Example - 01/01/2003 18:15:55:624 (MM/dd/yyyy H:mm:ss:SSS)
     */
    public static final String TRANSACTION_DATE_FORMAT =
            "MM/dd/yyyy H:mm:ss:SSS";

    /**
     * Example - January 01, 2003 6:15:30 PM,PDT (MMMM dd, yyyy h:mm:ss a,z)
     */
    public static final String STRING_DATE_TIME_FULL_FORMAT =
            "MMMM dd, yyyy h:mm:ss a,z";

    /**
     * Example - January 01, 2003 (MMMM dd, yyyy)
     */
    public static final String STRING_DATE_FULL_FORMAT = "MMMM dd, yyyy";

    /**
     * Example - Jan 01, 2003 (MMM dd, yyyy)
     */
    public static final String STRING_DATE_FORMAT = "MMM dd, yyyy";

    /**
     * Example - Jan 1, 02 (MMM d, yy)
     */
    public static final String STRING_DATE_ABBR_FORMAT = "MMM d, yy";

    /**
     * Example - Wednesday, January 01, 2003 (EEEEE, MMMMM dd, yyyy)
     */
    public static final String STRING_WEEK_DATE_FULL_FORMAT =
            "EEEEE, MMMMM dd, yyyy";

    /**
     * Example - Wed, Jan 1, 03 (EEE, MMM d, yy)
     */
    public static final String STRING_WEEK_DATE_ABBR_FORMAT = "EEE, MMM d, yy";

    /**
     * Example - January 2003 (MMMMM yyyy)
     */
    public static final String MONTH_YEAR_FULL_FORMAT = "MMMMM yyyy";

    /**
     * Example - Jan 01 (MMM dd)
     */
    public static final String MONTH_DAY_ABBR_FORMAT = "MMM dd";

    /**
     * Example - January 01
     */
    public static final String MONTH_DAY_STRING_FORMAT = "MMMMM dd";

    /**
     * Example - 01/01 (MM/dd)
     */
    public static final String MONTH_DAY_NUMBER_FORMAT = "MM/dd";

    /**
     * Example - 1/01 (M/dd)
     */
    public static final String CHART_DISPLAY_FORMAT = "M/d/yy";

    /**
     * Example - 1/01 (M/dd)
     */
    public static final String CHART_DAILY = "dd MMM";

    /**
     * Example - 1/01 (M/dd)
     */
    public static final String CHART_MONTHLY = "MMM yy";
    /**
     * Example - 1/01 (M/dd)
     */
    public static final String CHART_YEARLY = "yyyy";

    /**
     * Example - 01/01 (dd/MM)
     */
    public static final String DAY_MONTH_NUMBER_FORMAT = "dd/MM";

    /**
     * Example - 2003 (yyyy)
     */
    public static final String YEAR_FULL_FORMAT = "yyyy";

    /**
     * Example - 03 (yy)
     */
    public static final String YEAR_ABBR_FORMAT = "yy";

    /**
     * Example - January
     */
    public static final String MONTH_IN_YEAR_FULL_TEXT_FORMAT = "MMMMM";

    /**
     * Example - January_2003
     */
    public static final String MONTH_AND_YEAR_TEXT_FORMAT = "MMMMM_yyyy";

    /**
     * Example - Jan
     */
    public static final String MONTH_IN_YEAR_ABBR_TEXT_FORMAT = "MMM";

    /**
     * Example - 01 (MM)
     */
    public static final String MONTH_IN_YEAR_NUMBER_FORMAT = "MM";

    /**
     * Example - Wednesday.
     */
    public static final String DAY_IN_WEEK_FULL_TEXT_FORMAT = "EEEEE";

    /**
     * Example - Wed.
     */
    public static final String DAY_IN_WEEK_ABBR_TEXT_FORMAT = "EEE";

    private static String[] monthNames =
            {
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"};

    private static String[] monthAbbrs =
            {
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"};

    private static int[] days =
            {
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                26,
                27,
                28,
                29,
                30,
                31};

    public final static int FIRST_QUARTER = 1;
    public final static int SECOND_QUARTER = 2;
    public final static int THIRD_QUARTER = 3;
    public final static int FOURTH_QUARTER = 4;

    /**
     * Gets date string for a given date object.
     *
     * @param date - a java.util Date object to be formated into date string.
     * @return String - the date string in defaut format dd-MMM-yyyy.
     */
    public static String getDateString(Date date) {
        return getDateString(date, NUMBER_DATE_FULL_FORMAT);
    }

    /**
     * Gets date string for a given date object.
     *
     * @param date - a java.util Date object to be formated into string.
     * @return String - the date time string in defaut format dd-MMM-yyyy,HH:MM.
     */
    public static String getDateTimeString(Date date) {
        return getDateString(date, NUMBER_DATE_TIME_FORMAT);
    }

    /**
     * Gets date string given date object.
     *
     * @param date   - a java.util Date object to be formated into string.
     * @param format - string pattern. E.g dd-MMM-yyyy,HH:MM.
     * @return String - the date time string of specified format.
     */
    public static String getDateString(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    /**
     * Gets date string given date object, format, Locale.
     *
     * @param date   - a java.util Date object to be formated into string.
     * @param format - string pattern. E.g dd-MMM-yyyy,HH:MM.
     * @param locale - Locale object.
     * @return String - the date time string of specified format.
     */
    public static String getDateString(Date date,
                                       String format,
                                       Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
        return sdf.format(date);
    }

    /**
     * Converts date string from one format to another format.
     *
     * @param dateStr - String Date.
     * @param format  - existing date string foramt. From format.
     * @return String - the date time string of specified new format.
     */
    public static String getDateString(String dateStr,
                                       String format,
                                       String newForamt) {
        SimpleDateFormat sdf = new SimpleDateFormat(newForamt);
        return sdf.format(getDate(dateStr, format, true));
    }

    /**
     * Gets Date Object for date, month and year values.
     *
     * @param date  - a day of a month.
     * @param month - a month of an year (1-12);
     * @param year  - an year. E.g. 2003 or 03.
     * @return Date - date object on the date.
     */
    public static Date getDate(int date, int month, int year) {
        Calendar calendar = new GregorianCalendar(year, month - 1, date);
        return calendar.getTime();
    }

    /**
     * Gets Date Object for month and year values.
     *
     * @param month - a month of an year (1-12);
     * @param year  - an year. E.g. 2003 or 03.
     * @return Date - date object on the date.
     */
    public static Date getDate(int month, int year) {
        Calendar calendar = new GregorianCalendar(year, month - 1, 1);
        return calendar.getTime();
    }

    /**
     * Gets Date object for a valid date string.
     *
     * @param dateStr - string date. E.g "03 Jan 2003".
     * @param format  - foramt of the dateStr param. E.g "dd MMM yyyy"
     * @return Date - date object.
     */
    public static Date getDate(String dateStr, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setLenient(false);
        return sdf.parse(dateStr, new ParsePosition(0));
    }

    /**
     * Gets Date object for a locale specific date string.
     *
     * @param dateStr - a date string
     * @param format  - date pattern
     * @param locale  - locale of the string
     * @return Date - Date object.
     */
    public static Date getDate(String dateStr, String format, Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
        sdf.setLenient(false);
        return sdf.parse(dateStr, new ParsePosition(0));
    }

    /**
     * Gets Date object for a valid date string.
     *
     * @param dateStr - string date. E.g "03 Jan 2003".
     * @param format  - foramt of the dateStr param. E.g "dd MMM yyyy"
     * @param lenient - whether date/time parsing is to be lenient.
     * @return Date - date object.
     */
    public static Date getDate(String dateStr,
                               String format,
                               boolean lenient) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setLenient(lenient);
        return sdf.parse(dateStr, new ParsePosition(0));
    }

    /**
     * Gets Today date without time.
     *
     * @return Date - today's date object.
     */
    public static Date getTodayDate() {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * Gets today's date string.
     *
     * @param format - a string pattern representing a date format.
     * @return String - today's date string.
     */
    public static String getTodayDateString(String format) {
        return getDateString(getTodayDate(), format);
    }

    /**
     * Gets current date and time with Hours, minutes, seconds.
     *
     * @return Date - current date object.
     */
    public static Date getCurrentDateTime() {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * Gets yesterday's date.
     *
     * @return Date - yesterday's date
     */
    public static Date getYesterdayDate() {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.roll(Calendar.DATE, -1);
        return calendar.getTime();
    }

    /**
     * Gets previous day's date form a specified date.
     *
     * @param date - a date object whoes previous day date is required.
     * @return Date - previous day's date object.
     */
    public static Date getPreviousDayDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    /**
     * Gets previous year's date from a date.
     *
     * @param date - a date object whoes previous year date is required.
     * @return Date - previous year's date object.
     */
    public static Date getPreviousYearDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, -1);
        return calendar.getTime();
    }

    /**
     * Gets month number in an year. E.g January represents 0.
     *
     * @param date - a date object.
     * @return int - month number in Calander.
     */
    public static int getMonthNo(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH);
    }

    /**
     * Gets month name for a date.
     *
     * @param date - a date object.
     * @return String - name of a month.
     */
    public static String getMonthString(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return monthNames[calendar.get(Calendar.MONTH)];
    }

    /**
     * Gets short form of a month for a date. E.g Jan, Feb ect.
     *
     * @param date - a date object.
     * @return String - short name of a month.
     */
    public static String getMonthAbbreviation(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return monthAbbrs[calendar.get(Calendar.MONTH) - 1];
    }

    /**
     * Gets the day of a month for a date.
     *
     * @param date - a date object.
     * @return int - day of the month.
     */
    public static int getDayOfMonth(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.DATE);
    }

    /**
     * Gets year part of a date.
     *
     * @param date - a date object.
     * @return int - year.
     */
    public static int getYear(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * Tells whether a date is greater than or equal to anoth date.
     *
     * @param toDate   - a date object to be comapared.
     * @param fromDate - a date object to be comapared with.
     * @return boolean - true or false.
     */
    public static boolean greaterThanOrEqualTo(Date toDate, Date fromDate) {
        boolean compare = false;

        if ((toDate == null) || (fromDate == null)) {
            throw new IllegalArgumentException();
        }

        if ((toDate.compareTo(fromDate)) >= 0) {
            compare = true;
        }
        return compare;
    }

    /**
     * Tells whether a date is greater than the other.
     *
     * @param toDate   - a date which must be lesser than or equal.
     * @param fromDate - a date which must be greater than or equal
     * @return boolean - true if the first date is greater than the second.
     */
    public static boolean greaterThan(Date toDate, Date fromDate) {
        boolean compare = false;

        if ((toDate == null) || (fromDate == null)) {
            throw new IllegalArgumentException();
        }

        if ((toDate.compareTo(fromDate)) > 0) {
            compare = true;
        }

        return compare;
    }

    /**
     * Gets the difference between two dates in no of days
     *
     * @param date1 - a date which is treated as the lower date.
     * @param date2 - a date which is treated as the greater date
     * @return Long - the difference in no of days.
     */
    public static long getDifferenceInDays(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException();
        }

        long day = 24 * 60 * 60 * 1000;
        long ms = 0;
        if (date1.before(date2)) {
            ms = date2.getTime() - date1.getTime();
        } else {
            ms = date1.getTime() - date2.getTime();
        }
        return ms / day;
    }

    /**
     * Gets the difference between two dates in no of days
     *
     * @param date1 - a date which is treated as the lower date.
     * @param date2 - a date which is treated as the greater date
     * @return Long - the difference in no of days.
     */
    public static long getDifferenceInHr(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException();
        }

        long hr = 60 * 60 * 1000;
        long ms = 0;
        if (date1.before(date2)) {
            ms = date2.getTime() - date1.getTime();
        } else {
            ms = date1.getTime() - date2.getTime();
        }
        return ms / hr;
    }

    /**
     * Gets the difference between two dates in no of years
     *
     * @param date1 - a date which is treated as the lower date.
     * @param date2 - a date which is treated as the greater date
     * @return Long - the difference in no of days.
     */
    public static long getDifferenceInYears(Date date1, Date date2) {
        return getDifferenceInDays(date1, date2) / 365;
    }

    /**
     * Gets the week end date for a given date.
     *
     * @param date - a date object
     * @return Date - week end date.
     */
    public static Date getWeekEndDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(calendar.DAY_OF_WEEK, Calendar.FRIDAY);
        return calendar.getTime();
    }

    /**
     * Gets the week end date for a given date.
     *
     * @param date - a date object
     * @return Date - week end date.
     */
    public static Date getPreviousWeekEndDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(getWeekEndDate(date));
        calendar.add(calendar.WEEK_OF_MONTH, -1);
        return calendar.getTime();
    }

    /**
     * Gets the month end date for a given date.
     *
     * @param date - a date object
     * @return Date - month end date.
     */
    public static Date getMonthEndDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * Gets previous month end date for a date.
     *
     * @param date - a date object.
     * @return Date - previous month end date.
     */
    public static Date getPreviousMonthEndDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * Gets start date of month for a date.
     *
     * @param date - a date object.
     * @return Date - month start date.
     */
    public static Date getMonthStartDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * Gets the quarter number in which a date falls. Eg 1,2,3,4.
     *
     * @param date - a date object.
     * @return String - quarter number string.
     */
    public static int getQuarterNo(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int quarter = FIRST_QUARTER;
        int month = calendar.get(Calendar.MONTH);
        if ((month >= Calendar.JANUARY) && (month <= Calendar.MARCH)) {
            quarter = FIRST_QUARTER;
        } else if ((month >= Calendar.JULY) && (month <= Calendar.SEPTEMBER)) {
            quarter = THIRD_QUARTER;
        } else if (
                (month >= Calendar.OCTOBER) && (month <= Calendar.DECEMBER)) {
            quarter = FOURTH_QUARTER;
        }
        return quarter;
    }

    /**
     * Gets a quarter end date for a date.
     *
     * @param date - a date object.
     * @return Date - quarter end date.
     */
    public static Date getQuarterEndDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int factor = 0;
        int month = calendar.get(Calendar.MONTH);

        if (month == Calendar.JANUARY
                || month == Calendar.APRIL
                || month == Calendar.JULY
                || month == Calendar.OCTOBER) {
            factor = 2;
        } else if (
                month == Calendar.FEBRUARY
                || month == Calendar.MAY
                || month == Calendar.AUGUST
                || month == Calendar.NOVEMBER) {
            factor = 1;
        } else {
            factor = 0;
        }

        calendar.add(Calendar.MONTH, factor);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * Gets previous quarter end date for a date.
     *
     * @param date - a date object.
     * @return Date - previous quarter end date.
     */
    public static Date getPreviousQuarterEndDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int factor = 0;
        int month = calendar.get(Calendar.MONTH);
        if (month == Calendar.JANUARY
                || month == Calendar.APRIL
                || month == Calendar.JULY
                || month == Calendar.OCTOBER) {
            factor = -1;
        } else if (
                month == Calendar.FEBRUARY
                || month == Calendar.MAY
                || month == Calendar.AUGUST
                || month == Calendar.NOVEMBER) {
            factor = -2;
        } else {
            factor = -3;
        }
        calendar.add(Calendar.MONTH, factor);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * Gets quarter start date for a date.
     *
     * @param date - a date object.
     * @return Date - quarter start date.
     */
    public static Date getQuarterStartDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int factor = 0;
        int month = calendar.get(Calendar.MONTH);

        if (month == Calendar.JANUARY
                || month == Calendar.APRIL
                || month == Calendar.JULY
                || month == Calendar.OCTOBER) {
            factor = 2;
        } else if (
                month == Calendar.FEBRUARY
                || month == Calendar.MAY
                || month == Calendar.AUGUST
                || month == Calendar.NOVEMBER) {
            factor = 1;
        } else {
            factor = 0;
        }

        calendar.add(Calendar.MONTH, factor);
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * Gets semiannual end date for a date.
     *
     * @param date - a date object.
     * @return Date - semiannual end date.
     */
    public static Date getSemiAnnualEndDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);

        if (month >= Calendar.JANUARY && month <= Calendar.JUNE) {
            calendar.set(Calendar.MONTH, Calendar.JUNE);
        } else {
            calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        }

        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * Gets semiannual start date for a date.
     *
     * @param date - a date object.
     * @return Date - semiannual end date.
     */
    public static Date getSemiAnnualStartDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);

        if (month >= Calendar.JANUARY && month <= Calendar.JUNE) {
            calendar.set(Calendar.MONTH, Calendar.JUNE);
        } else {
            calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        }

        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * Gets previous semiannual end date for a date.
     *
     * @param date - a date object.
     * @return Date - previous semiannual end date.
     */
    public static Date getPreviousSemiAnnualEndDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);

        if (month >= Calendar.JANUARY && month <= Calendar.JUNE) {
            calendar.set(Calendar.MONTH, Calendar.DECEMBER);
            calendar.add(Calendar.YEAR, -1);
        } else {
            calendar.set(Calendar.MONTH, Calendar.JUNE);
        }

        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * Gets annual end date for a date.
     *
     * @param date - a date object.
     * @return Date - annual end date.
     */
    public static Date getAnnualEndDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH, calendar.getActualMaximum(Calendar.MONTH));
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * Gets previous annual end date for a date.
     *
     * @param date - a date object.
     * @return Date - previous annual end date.
     */
    public static Date getPreviousAnnualEndDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH, calendar.getActualMaximum(Calendar.MONTH));
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        calendar.add(calendar.YEAR, -1);
        return calendar.getTime();
    }

    /**
     * Gets year start date for a date.
     *
     * @param date - a date object.
     * @return Date - year strart date.
     */
    public static Date getYearStartDate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        calendar.set(Calendar.MONTH, calendar.getActualMinimum(Calendar.MONTH));
        return calendar.getTime();
    }

    /**
     * Method getSqlDate.
     *
     * @param date - a java.util Date object to be formated into string.
     * @return Date
     */
    public static java.sql.Date getSqlDate(Date date) {
        if (date == null) {
            return null;
        }
        return new java.sql.Date(date.getTime());
    }

    /**
     * Tells whether a date is a weekend. Saturday or Sunday
     *
     * @param date - a date object.
     * @return boolean - true if a date is a weekend date.
     */
    public static boolean isWeekend(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Adds days to a date.
     *
     * @param date - a date object.
     * @return Date - date with the added days
     */
    public static Date addDays(Date date, int days) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }

    /**
     * Adds months to a date.
     *
     * @param date - a date object.
     * @return Date - date with the added days
     */
    public static Date addMonths(Date date, int months) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, months);
        return calendar.getTime();
    }

    /**
     * Adds years to a date.
     *
     * @param date - a date object.
     * @return Date - date with the added days
     */
    public static Date addYears(Date date, int years) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, years);
        return calendar.getTime();
    }

    /**
     * Subtracts days from a date.
     *
     * @param date - a date object.
     * @return Date - date with the added days
     */
    public static Date subtractDays(Date date, int days) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -days);
        return calendar.getTime();
    }

    /**
     * Subtracts days from a date.
     *
     * @param date - a date object.
     * @return Date - date with the added days
     */
    public static Date subtractMonths(Date date, int months) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -months);
        return calendar.getTime();
    }

    /**
     * Subtracts days from a date.
     *
     * @param date - a date object.
     * @return Date - date with the added days
     */
    public static Date subtractYears(Date date, int years) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, -years);
        return calendar.getTime();
    }

    /**
     * Gets List of Month Names
     *
     * @return String[] - month name.
     */
    public static String[] getMonthNamesList() {
        return monthNames;
    }

    /**
     * Gets List of Days
     *
     * @return int[]
     */
    public static int[] getDaysList() {
        return days;
    }

    public static void main(String[] args) {
//        String str = null;
//        int period = 1;
//        Date date = getTodayDate();
//        Date startDate = getDate(20, 3, 2003);
//        Date endDate = getDate(20, 3, 2003);

//        Collection coll = getDatesByRange(startDate, endDate, freq);
//        for (Iterator iter = coll.iterator(); iter.hasNext();) {
//            Date element = (Date) iter.next();
//            System.out.println(getDateString(element));
//        }
    }
}

