/*
 * ConnectionStats.java
 *
 * Created on May 16, 2007, 11:22 AM
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.alleni.db.util;

import java.util.Date;

/**
 *
 * @author prashant gaurav
 */
public class ConnectionStats {
    
    public Date creationDate;
    public Date lastRequested;
    public String lastRequestTrace;
    public int totalRequests;
    public boolean free;

    public ConnectionStats()
    {
        creationDate = new Date();
        lastRequested = new Date();
        lastRequestTrace = "";
        totalRequests = 0;
        free = true;
    }    
}
