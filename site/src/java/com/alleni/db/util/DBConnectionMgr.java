/*
 * DBConnectionMgr.java
 *
 * Created on May 16, 2007, 11:13 AM
 */
package com.alleni.db.util;

import java.io.InputStream;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import com.alleni.util.Utility;

/**
 *
 * @author prashant gaurav
 * This Class has been developed for maintaining database pools and getting
 *  Default Connection and Connection by database names.
 */
public class DBConnectionMgr
{
    private static Map busyPool = Collections.synchronizedMap(new HashMap());
    private static List freePool = Collections.synchronizedList(new LinkedList());
    private static ConnectionReaper reaper = null;
    private static final long timeout = 60000;
    public static int intGC = 0;
    

    static
    {
        ConfigConnection configConnection = new ConfigConnection();
        reaper = new ConnectionReaper();
        reaper.start();
        DecoratorConnection con = null;
        for (int i = 0; i < 5; i++)
        {
            try
            {
                con = new DecoratorConnection(configConnection.getConnection());
                con.setPooled(true);
                freePool.add(new DecoratorConnection(configConnection.getConnection()));
            }
            catch (SQLException ex)
            {
                System.out.println("error in creating active connetions" + ex);
            }
        }
    }

    /**
     * This is the default Constructor
     */
    public DBConnectionMgr()
    {
    }

    public static synchronized Connection getNewConnection() throws SQLException
    {
        DecoratorConnection con = null;
        try
        {
            con = new DecoratorConnection(new ConfigConnection().getConnection());
            con.closed = false;
            con.stats.lastRequested = new java.util.Date();
            con.stats.free = false;
            con.stats.totalRequests++;
            con.setPooled(false);
        }
        catch (SQLException ex)
        {
            Utility.log("error in getting connetions" + ex);
        }

        return con;
    }

    /**
     * @return a DecoratorConnection
     * @throws SQLException
     */
    public static synchronized Connection getConnection() throws SQLException
    {
        DecoratorConnection con = null;
        synchronized (freePool)
        {
            if (freePool.size() > 0)
            {
                con = (DecoratorConnection) freePool.remove(0);
                con.closed = false;
            }
            else
            {
                try
                {
                    con = new DecoratorConnection(new ConfigConnection().getConnection());
                    con.closed = false;
                    con.setPooled(true);
                }
                catch (SQLException ex)
                {
                    Utility.log("error in getting connetions" + ex);
                }
            }
            if (freePool.size() > 5)
            {
                for (int intCon = 5; intCon < freePool.size(); intCon++)
                {
                    DecoratorConnection conn = (DecoratorConnection) freePool.remove(intCon);
                    conn.close();
                }
            }
        }
        con.stats.lastRequested = new java.util.Date();
        con.stats.free = false;
        con.stats.totalRequests++;

        synchronized (busyPool)
        {
            busyPool.put(con, new java.util.Date());
        }
        return con;
    }

    /**
     * @param strDBName
     * @return a java.sql.Connection
     * @throws SQLException
     */
    public static synchronized Connection getConnection(String strDBName) throws SQLException
    {
        Connection con = null;
        try
        {
            con = getConnection();
        }
        catch (SQLException ex)
        {
            /*SystemToLog*/            Utility.log("error in getting connetions" + ex);
        }

        return con;
    }

    /**
     * This method takes in a Connection and removes it from the database pool
     * @param Takes in a connection
     * @throws SQLException
     */
    public static synchronized void returnConnection(Connection con) throws SQLException
    {
        DecoratorConnection dcon = (DecoratorConnection) con;
        if (dcon.isPooled())
        {
            synchronized (busyPool)
            {
                busyPool.remove(con);
            }
            ConnectionStats stats = ((DecoratorConnection) con).stats;
            stats.free = true;
            stats.lastRequestTrace = "";
            synchronized (freePool)
            {
                if (freePool.size() < 5)
                {
                    freePool.add(con);
                }
                else
                {
                    con.close();
                }
            }
        }
        else
        {
//            dcon = null;
            con.close();
        }
    }

    /**
     * This method removes the timeout and invalid connections from the database pool
     *
     */
    public static synchronized void reapConnections()
    {
        long stale = System.currentTimeMillis() - timeout;
        synchronized (freePool)
        {
            try
            {
                for (int intCon = 0; intCon < freePool.size(); intCon++)
                {
                    DecoratorConnection con = (DecoratorConnection) freePool.get(intCon);
                    if (con.stats.lastRequested.getTime() < stale || !validate(con))
                    {
                        DecoratorConnection conn = (DecoratorConnection) freePool.remove(intCon);
                        Connection tempCon = (Connection) conn;
                        tempCon.close();
                        tempCon = null;
                    }
                }
            }
            catch (SQLException se)
            {
                /*SystemToLog*/                Utility.log("Error occured while reaping the connections.");
            }
        }
    }

    private static boolean validate(Connection conn)
    {
        try
        {
            conn.getMetaData();
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    /**
     * @return the no of free Connections in the pool
     */
    public synchronized static int getFreeConnectionsCount()
    {
        return freePool.size();
    }

    /**
     * @return the no of busy Connections in the pool
     */
    public synchronized static int getBusyConnectionsCount()
    {
        return busyPool.size();
    }
}

class ConfigConnection
{
    InputStream _inputStream;
    Properties _props;
    String _hostName;
    String _userName;
    String _password;
    String _database;
    String _schema;
    String _strURL;

    /**
     * This is the default Constructor which reads the property File
     * Database.cfg for the database Details
     */
    ConfigConnection()
    {
        try
        {
            _hostName    = PropertyUtil.getProperty(PropertyConstants.DB_URL);
            _userName    = PropertyUtil.getProperty(PropertyConstants.DB_USER);
            _password    = PropertyUtil.getProperty(PropertyConstants.DB_PASSWORD);
            _database    = PropertyUtil.getProperty(PropertyConstants.DB_DATABASE);
            _schema        = PropertyUtil.getProperty(PropertyConstants.DB_SCHEMA);

            Utility.log(">>" + _hostName + _database + "/" + _userName + "/" + _password);
            Class.forName(PropertyUtil.getProperty(PropertyConstants.DB_DRIVER));
        }
        catch (Exception ex)
        {
            /*SystemToLog*/            Utility.log("error in creating  connetions" + ex);
        }
    }

    /**
     * @return Returns a java.sql.Connection
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException
    {
        _strURL = _hostName + _database;
        return DriverManager.getConnection(_strURL, _userName, _password);
    }

    /**
     * @param Takes database name as String Parameter
     * @return Returns a java.sql.Connection
     * @throws SQLException
     */
    public Connection getConnection(String dbName) throws SQLException
    {
        _strURL = _hostName + dbName;
        return DriverManager.getConnection(_strURL, _userName, _password);
    }
}

class ConnectionReaper extends Thread
{
    private final long delay = 300000;

    ConnectionReaper()
    {
    }

    public void run()
    {
        while (true)
        {
            try
            {
                sleep(delay);
            }
            catch (InterruptedException e)
            {
            }
            DBConnectionMgr.reapConnections();
        }
    }
}

/**
 * This Class implements java.sql.Connection. This Class is used for managing the database pools.
 */
class DecoratorConnection implements java.sql.Connection
{
    private Connection connection;
    public boolean closed = true;
    private List statements = Collections.synchronizedList(new LinkedList());
    ConnectionStats stats = new ConnectionStats();
    private boolean pooled;
    private final String origCatalog;
    private final boolean origReadOnly;

    /**
     * @param Takes a java.sql.COnnection object as Parameter.
     * @throws SQLException
     */
    public DecoratorConnection(Connection con) throws SQLException
    {

        connection = con;
        origCatalog = con.getCatalog();
        origReadOnly = con.isReadOnly();

    }

    public void clearWarnings() throws java.sql.SQLException
    {
        connection.clearWarnings();
    }
    /*  *******************
    Closes the associated connection object
     *******************  */

    public void close() throws java.sql.SQLException
    {

        closed = true;
        synchronized (statements)
        {
            while (statements.size() > 0)
            {
                ((Statement) statements.remove(0)).close();
            }
        }
        connection.setAutoCommit(true);
        connection.clearWarnings();
        connection.setCatalog(origCatalog);
        connection.setReadOnly(origReadOnly);
        connection.close();

    }
    /*  *******************
    Makes all changes made since the previous
    commit/rollback permanent and releases any database locks
    currently held by the Connection. This method should be
    used only when auto-commit mode has been disabled.
     *******************  */

    public void commit() throws java.sql.SQLException
    {
        connection.commit();
    }
    /*  *******************
    Creates a statement object that will generate a resultset object
     *******************  */

    public java.sql.Statement createStatement() throws java.sql.SQLException
    {
        Statement stmt = connection.createStatement();
        statements.add(stmt);
        return stmt;
    }
    /*  *******************
    Creates a statement object that will generate a resultset object
    with the param as resultset type and param1 as the concurrency of
    resultset object
     *******************  */

    public java.sql.Statement createStatement(int param, int param1) throws java.sql.SQLException
    {
        Statement stmt = connection.createStatement(param, param1);
        statements.add(stmt);
        return connection.createStatement(param, param1);
    }
    /*  *******************
    Gets the current auto commit state
     *******************  */

    public boolean getAutoCommit() throws java.sql.SQLException
    {
        return connection.getAutoCommit();
    }
    /*  *******************
    Returns the Connection's current catalog name
     *******************  */

    public String getCatalog() throws java.sql.SQLException
    {
        return connection.getCatalog();
    }
    /*  *******************
    Gets the metadata regarding this connection's database.
     *******************  */

    public java.sql.DatabaseMetaData getMetaData() throws java.sql.SQLException
    {
        return connection.getMetaData();
    }
    /*  *******************
    Gets the isolation level of the connection
     *******************  */

    public int getTransactionIsolation() throws java.sql.SQLException
    {
        return connection.getTransactionIsolation();
    }
    /*  *******************
    Gets the type map object associated with this connection.
    Unless the application has added an entry to the type map,
    the map returned will be empty.
     ********************/

    public java.util.Map getTypeMap() throws java.sql.SQLException
    {
        return connection.getTypeMap();
    }
    /*  *******************
    Returns the SQLWarning reported for this Connection object
     *******************  */

    public java.sql.SQLWarning getWarnings() throws java.sql.SQLException
    {
        return connection.getWarnings();
    }
    /*  *******************
    Returns true if the connection is closed else returns false
     *******************  */

    public boolean isClosed() throws java.sql.SQLException
    {
        return closed;
    }
    /*  *******************
    Returns true if the connection is in readonly mode else returns false
     *******************  */

    public boolean isReadOnly() throws java.sql.SQLException
    {
        return connection.isReadOnly();
    }

    public String nativeSQL(String str) throws java.sql.SQLException
    {
        return connection.nativeSQL(str);
    }
    /*  *******************
    Returns a Callable Statement whioch can be used to invoke a
    storedprocedure.
     *******************  */

    public java.sql.CallableStatement prepareCall(String str) throws java.sql.SQLException
    {
        CallableStatement stmt = connection.prepareCall(str);
        statements.add(stmt);
        return stmt;
    }
    /*  *******************
    Creates a CallableStatement object that will generate
    ResultSet objects with the given type and concurrency.
     *******************  */

    public java.sql.CallableStatement prepareCall(String str, int param, int param2) throws java.sql.SQLException
    {
        CallableStatement stmt = connection.prepareCall(str, param, param2);
        statements.add(stmt);
        return stmt;
    }
    /*  *******************
    Creates a PreparedStatement object that will a generate
    ResultSet object
     *******************  */

    public java.sql.PreparedStatement prepareStatement(String str) throws java.sql.SQLException
    {
        PreparedStatement stmt = connection.prepareStatement(str);
        statements.add(stmt);
        return stmt;
    }
    /*  *******************
    Creates a PreparedStatement object that will generate
    ResultSet objects with the given type and concurrency.
     *******************  */

    public java.sql.PreparedStatement prepareStatement(String str, int param, int param2) throws java.sql.SQLException
    {
        PreparedStatement stmt = connection.prepareStatement(str, param, param2);
        statements.add(stmt);
        return stmt;
    }
    /*  *******************
    Drops all changes made since the previous
    commit/rollback and releases any database locks currently held
    by this Connection.
     ******************* */

    public void rollback() throws java.sql.SQLException
    {
        connection.rollback();
    }
    /*  *******************
    Sets this connection's auto-commit mode.
     ******************* */

    public void setAutoCommit(boolean param) throws java.sql.SQLException
    {
        connection.setAutoCommit(param);
    }

    public void setCatalog(String str) throws java.sql.SQLException
    {
        connection.setCatalog(str);
    }
    /*  *******************
    Enables or disables readonly mode based on boolean value passed
     ******************* */

    public void setReadOnly(boolean param) throws java.sql.SQLException
    {
        connection.setReadOnly(param);
    }
    /*  *******************
    Sets the isolation level for the transactions
     ******************* */

    public void setTransactionIsolation(int param) throws java.sql.SQLException
    {
        connection.setTransactionIsolation(param);
    }

    public void setTypeMap(java.util.Map map) throws java.sql.SQLException
    {
        connection.setTypeMap(map);
    }

    public void setHoldability(int holdability) throws SQLException
    {
        connection.setHoldability(holdability);
    }

    public int getHoldability() throws SQLException
    {
        return connection.getHoldability();
    }

    public Savepoint setSavepoint() throws SQLException
    {
        return connection.setSavepoint();
    }

    public Savepoint setSavepoint(String name) throws SQLException
    {
        return connection.setSavepoint(name);
    }

    public void rollback(Savepoint savepoint) throws SQLException
    {
        connection.rollback(savepoint);
    }

    public void releaseSavepoint(Savepoint savepoint) throws SQLException
    {
        connection.releaseSavepoint(savepoint);
    }

    public Statement createStatement(int resultSetType, int resultSetConcurrency,
                                     int resultSetHoldability) throws SQLException
    {
        return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    public PreparedStatement prepareStatement(String sql, int resultSetType,
                                              int resultSetConcurrency, int resultSetHoldability)
            throws SQLException
    {
        return connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    public CallableStatement prepareCall(String sql, int resultSetType,
                                         int resultSetConcurrency, int resultSetHoldability)
            throws SQLException
    {
        return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException
    {
        return connection.prepareStatement(sql, autoGeneratedKeys);
    }

    public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException
    {
        return connection.prepareStatement(sql, columnIndexes);
    }

    public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException
    {
        return connection.prepareStatement(sql, columnNames);
    }

    public void finalize()
    {
        DBConnectionMgr.intGC++;
        /*SystemToLog*/        Utility.log("The Connection is Garbage Collected     " + DBConnectionMgr.intGC);
    }

    public Clob createClob() throws SQLException
    {
        return null;
    }

    public Blob createBlob() throws SQLException
    {
        return null;
    }

    public NClob createNClob() throws SQLException
    {
        return null;
    }

    public SQLXML createSQLXML() throws SQLException
    {
        return null;
    }

    public boolean isValid(int timeout) throws SQLException
    {
        return false;
    }

    public void setClientInfo(String name, String value) throws SQLClientInfoException
    {
    }

    public void setClientInfo(Properties properties) throws SQLClientInfoException
    {
    }

    public String getClientInfo(String name) throws SQLException
    {
        return null;
    }

    public Properties getClientInfo() throws SQLException
    {
        return null;
    }

    public Array createArrayOf(String typeName, Object[] elements) throws SQLException
    {
        return connection.createArrayOf(typeName, elements);
    }

    public Struct createStruct(String typeName, Object[] attributes) throws SQLException
    {
        return null;
    }

    public Object unwrap(Class iface) throws SQLException
    {
        return null;
    }

    public boolean isWrapperFor(Class iface) throws SQLException
    {
        return false;
    }

    public boolean isPooled()
    {
        return pooled;
    }

    public void setPooled(boolean pooled)
    {
        this.pooled = pooled;
    }
}
