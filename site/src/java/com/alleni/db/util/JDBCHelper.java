/*
 * JDBCHelper.java
 *
 * Created on May 22, 2007, 2:24 PM
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.alleni.db.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class JDBCHelper {
    //Single Connection ONlY TO be used by junit;
    private static Connection junitConn = null;
    
    /**
     * Closes given result set hard
     */
    public static void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (Exception excp) {
                // Ignore, we don't care, we do care when we open it
            }
        }
    }
    
    /**
     * Closes given CallableStatement set hard
     */
    public static void close(CallableStatement ps) {
        if (ps != null) {
            try {
                ps.close();
            } catch (Exception excp) {
                // Ignore, we don't care, we do care when we open it
            }
        }
    }
    
    
    /**
     * Closes given PreparedStatement set hard
     */
    public static void close(PreparedStatement ps) {
        if (ps != null) {
            try {
                ps.close();
            } catch (Exception excp) {
                // Ignore, we don't care, we do care when we open it
            }
        }
    }
    
    /**
     * Closes given Statement set hard
     */
    public static void close(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (Exception excp) {
                // Ignore, we don't care, we do care when we open it
            }
        }
    }
    
    /**
     * Closes given Connection set hard
     */
    public static void close(Connection cn) {
        
        if(junitConn != null){
            return; //Do not close the connection, used by junit
        }
        
        try {
            //if (cn != null && !cn.isClosed()) {
                //Log.debugPerf("************************* - connection - Returning ",3);
//                cn.setAutoCommit(true);// Reset the state before returning to db pool
//                cn.close();
                DBConnectionMgr.returnConnection (cn);
            //}
        } catch (Exception excp) {
            System.err.println("caught");
            // Ignore, we don't care, we do care when we open it
        }
    }
    
    /**
     * Closes given Connection set hard
     */
    public static void rollback(Connection cn) throws Exception{
        if (cn != null) {
            try {
                cn.rollback();
            } catch (Exception excp) {
                throw excp;
            }
        }
    }
    
    /**
     * Closes given Connection set hard
     */
    public static void setAutoCommit(Connection cn, boolean value) throws Exception{
        if (cn != null) {
            try {
                cn.setAutoCommit(value);
            } catch (Exception excp) {
                throw excp;
            }
        }
    }
    
    
    public static void close(ResultSet rs, PreparedStatement ps) {
        close(rs);
        close(ps);
    }
    
    public static void close(ResultSet rs, PreparedStatement ps, Connection conn) {
        close(rs);
        close(ps);
        close(conn);
    }
    public static void close(ResultSet rs, Statement stmt, Connection conn) {
        close(rs);
        close(stmt);
        close(conn);
    }
    
    public static void close(Statement s, Connection conn) {
        close(s);
        close(conn);
    }
    
    public static void close(PreparedStatement ps, Connection conn) {
        close(ps);
        close(conn);
    }
    
    public static void close(CallableStatement ps, Connection conn) {
        close(ps);
        close(conn);
    }
    
    public static Connection getJdbcConnection() throws SQLException {
        Connection conn = null;
        try {
            conn = DBConnectionMgr.getConnection();
        }catch(SQLException e){
            e.printStackTrace();
            throw e;
        }
        return conn;
    }
    
    public static Connection getNewJdbcConnection() throws SQLException {
        Connection conn = null;
        try {
            conn = DBConnectionMgr.getNewConnection ();
        }catch(SQLException e){
            e.printStackTrace();
            throw e;
        }
        return conn;
    }
 
    /**
     * The method is only to be used when doing unit testing
     *
     */
    public static void setJunitConnection() throws Exception {
        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver");
            String _strURL = "jdbc:postgresql://localhost:5432/boreal";
            String _userName = "postgres";
            String _password = "postgres";
            
            conn = DriverManager.getConnection(_strURL,_userName,_password);
            if( conn == null){
                System.out.println("Connection is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        
        junitConn = conn;
    }  
    
    public static Connection getJunitConnection() throws Exception {
        if(junitConn == null || junitConn.isClosed())
            setJunitConnection();
        
        return junitConn;
    }
    
    public static Connection getNewJunitConnection() throws SQLException {
        Connection conn = null;
        try {
            try
            {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex)
            {
                ex.printStackTrace();
            }
            String _strURL = "jdbc:postgresql://localhost:5432/boreal";
            String _userName = "postgres";
            String _password = "postgres";
            
            conn = DriverManager.getConnection(_strURL,_userName,_password);
            if( conn == null){
                System.out.println("Connection is null");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        
        return conn;
    }  
}

