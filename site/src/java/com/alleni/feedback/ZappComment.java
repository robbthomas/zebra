package com.alleni.feedback;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class ZappComment extends BaseBean implements Serializable
{
    static private ZappCommentDao _dao() { return new ZappCommentDao(); }

    protected ZappComment(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("zappcommentid" ) ? daoJson.getInt("zappcommentid") : -1);
        this.jsonValues.put("projectId", daoJson.has("projectid") ? daoJson.getString("projectid") : -1);
        this.jsonValues.put("memberId", daoJson.has("memberid") ? daoJson.getString("memberid") : "");
        this.jsonValues.put("comment", daoJson.has("comment") ? daoJson.getString("comment") : "");
        this.jsonValues.put("helpfulCount", daoJson.has("helpfulcount") ? daoJson.getInt("helpfulcount") : -1);
        this.jsonValues.put("feedbackCount", daoJson.has("feedbackcount") ? daoJson.getInt("feedbackcount") : "");
        this.jsonValues.put("created", daoJson.has("createddatetime") ? (Timestamp)daoJson.get("createddatetime") : new Timestamp(0));
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("ZAPPCOMMENT.create - could not create ZappComments record");

        return success("zappCommentId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("ZAPPCOMMENT.update - could not update ZappComments record");

        return success("zappCommentId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("ZAPPCOMMENT.retire - could not retire ZappComments record");

        return success("zappCommentId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("id");
        ArrayList zappCommentsList = _dao().find(id, credentialValues);

        if (zappCommentsList.size() < 1)
            return error("ZAPPCOMMENT.find - could not find ZappComments id "+id);
        if (zappCommentsList.size() > 1)
            return error("ZAPPCOMMENT.find - found multiple ZappComments records for ZappComments id "+id);

        ZappComment zappComments = new ZappComment((JSONObject)zappCommentsList.get(0));
        if (zappComments == null)
            return error("ZAPPCOMMENT.find - zappComments==null for id "+id);

        return success(zappComments.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList zappCommentsList = _dao().findAll(credentialValues);
        return jsonFoundList(zappCommentsList, credentialValues);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of ZappComments json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList zappCommentsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(zappCommentsList, limit);

        JSONObject jsonList = jsonFoundList(zappCommentsList, credentialValues);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList, JSONObject credentialValues) throws JSONException, SQLException
    {
        long        now = System.currentTimeMillis();

        for (int j = 0; j < foundList.size(); j++)
        {
            ZappComment zappComments = new ZappComment((JSONObject)foundList.get(j));
            if (zappComments == null)
                return JsonError.jsonError(JsonError.Error.ZAPPCOMMENT_FIND, "Could not make ZappComments object " + j);
            JSONObject json = new JSONObject(zappComments.toString());

            json.put("ago", now - Timestamp. valueOf(json.getString("created")).getTime());

            JSONObject findValues = new JSONObject();
            findValues.put("id", json.getString("memberId"));
            JSONObject  usernameWrapper = com.alleni.member.AppUser.find(findValues, credentialValues);
            JSONObject username = usernameWrapper.getJSONObject("data");

            json.put("memberName", username.getString("firstName") + " " + username.getString("lastName"));

            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("zappComments", foundList);

        return jsonList;
    }

    // MySQL has a nice way to get a search with LIMIT as well as the number of items total
    // http://stackoverflow.com/questions/156114/best-way-to-get-result-count-before-limit-was-applied-in-php-postgres
    public static JSONObject countCommentsForProjectId(String projectId, JSONObject credentials) throws JSONException
    {
        try {
            int    commentsCount = _dao().countCommentsForProjectId(projectId, credentials);

            JSONObject  reply = new JSONObject();
            reply.put("commentsCount", commentsCount);

            return success(reply);
}
        catch (SQLException e) {
            return error(e.toString());
        }
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        ZAPPCOMMENT_CREATE,
        ZAPPCOMMENT_RETIRE,
        ZAPPCOMMENT_FIND,
        ZAPPCOMMENT_UPDATE,

 */