package com.alleni.feedback;

import com.alleni.common.BaseDAO;
import com.alleni.db.util.JDBCHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONException;
import org.json.JSONObject;


public class UpdateRatingAndCache extends BaseDAO
{
   private static final String UPDATECACHE           = "{call fn_updateratingandcache(?,?,?,?,?)}";
   private static final String UPDATECOMMENTFEEDBACK = "{call fn_updatecommentfeedback(?,?,?,?,?)}";

    public UpdateRatingAndCache()
    {
    }

    public static int computeSmartAverage(int ratingTotal, int numRatings) {

        // decimal must be above 0.6 to be bumped up to higher integer
        int     average = (int)Math.floor((float)ratingTotal / (float)numRatings + 0.4);

        return average;
    }

    public JSONObject updateZappRating(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException {
        Connection conn = null;
        CallableStatement cs = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(UPDATECACHE);

            cs.setString(1, credentialValues.getString("email"));
            cs.setString(2, credentialValues.getString("password"));

            cs.setString(3, updateValues.getString("projectId"));
            cs.setString(4, updateValues.getString("memberId"));
            cs.setInt(5, updateValues.getInt("rating"));

            ResultSet rs = cs.executeQuery();
            rs.next();

            JSONObject json = resultSetToJSON(rs);

            json.put("average", computeSmartAverage(json.getInt("ratingtotal"), json.getInt("numratings")));

            return json;            
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

    public JSONObject updateCommentFeedback(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException {
        Connection conn = null;
        CallableStatement cs = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(UPDATECOMMENTFEEDBACK);

            cs.setString(1, credentialValues.getString("email"));
            cs.setString(2, credentialValues.getString("password"));

            cs.setInt(3, updateValues.getInt("commentId"));
            cs.setString(4, updateValues.getString("memberId"));
            cs.setString(5, updateValues.getString("feedback"));

            ResultSet rs = cs.executeQuery();
            rs.next();

            JSONObject json = resultSetToJSON(rs);
            
            return json;
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

}