package com.alleni.feedback;

import com.alleni.common.BaseDAO;
import com.alleni.db.util.JDBCHelper;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;


public class ZappCommentDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_zappcomment_create(?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_zappcomment_update(?,?,?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_zappcomment_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_zappcomment_filter(?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String FIND        = "{call fn_zappcomment_find(?,?,?)}";

    public ZappCommentDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_projectId", new HashMapBuilder<String, String>().add("key", "projectId").add("type", "text").build())
            .add("in_memberId", new HashMapBuilder<String, String>().add("key", "memberId").add("type", "text").build())
            .add("in_comment", new HashMapBuilder<String, String>().add("key", "comment").add("type", "text").build())
            .add("in_helpfulCount", new HashMapBuilder<String, String>().add("key", "helpfulCount").add("type", "integer").build())
            .add("in_feedbackCount", new HashMapBuilder<String, String>().add("key", "feedbackCount").add("type", "integer").build())


            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_zappCommentsId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_projectId", new HashMapBuilder<String, String>().add("key", "projectId").add("type", "text").build())
            .add("in_memberId", new HashMapBuilder<String, String>().add("key", "memberId").add("type", "text").build())
            .add("in_comment", new HashMapBuilder<String, String>().add("key", "comment").add("type", "text").build())
            .add("in_helpfulCount", new HashMapBuilder<String, String>().add("key", "helpfulCount").add("type", "integer").build())
            .add("in_feedbackCount", new HashMapBuilder<String, String>().add("key", "feedbackCount").add("type", "integer").build())


            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_projectId", new HashMapBuilder<String, String>().add("key", "projectId").add("type", "text").build())
            .add("in_memberId", new HashMapBuilder<String, String>().add("key", "memberId").add("type", "text").build())
            .add("in_comment", new HashMapBuilder<String, String>().add("key", "comment").add("type", "text").build())
            .add("in_helpfulCount", new HashMapBuilder<String, String>().add("key", "helpfulCount").add("type", "integer").build())
            .add("in_feedbackCount", new HashMapBuilder<String, String>().add("key", "feedbackCount").add("type", "integer").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }
    
    protected int setRetireParameters(Connection conn, CallableStatement cs, int offset) throws SQLException,  JSONException
    {
        offset = setCredentialsParameters(conn, cs, offset);
        setParameter(cs, this.crudValues.getInt("zappCommentId"), offset++);
        setParameter(cs, this.crudValues.getString("retiredById"), offset++);
        return offset;
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

    public int countCommentsForProjectId(String projectId, JSONObject credentials) throws JSONException, SQLException
    {
        Connection conn = null;
        CallableStatement cs = null;
        ArrayList jsonList = new ArrayList();
        try
        {
            conn = getConnection();

            cs = conn.prepareCall("{call fn_zappcomment_countCommentsForprojectId(?,?,?)}");

            cs.setString(1, credentials.getString("email"));
            cs.setString(2, credentials.getString("password"));
            cs.setString(3, projectId);

            ResultSet rs = cs.executeQuery();

            rs.next();

            return rs.getInt(1);

        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

}
