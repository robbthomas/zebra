package com.alleni.feedback;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class ZappRating extends BaseBean implements Serializable
{
    static private ZappRatingDao _dao() { return new ZappRatingDao(); }

    protected ZappRating(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("id" ) ? daoJson.getInt("id") : -1);
        this.jsonValues.put("projectId", daoJson.has("projectid") ? daoJson.getString("projectid") : -1);
        this.jsonValues.put("memberId", daoJson.has("memberid") ? daoJson.getString("memberid") : "");
        this.jsonValues.put("rating", daoJson.has("rating") ? daoJson.getInt("rating") : -1);
        this.jsonValues.put("created", daoJson.has("createddatetime") ? (Timestamp)daoJson.get("createddatetime") : new Timestamp(0));
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("ZAPPRATING.create - could not create ZappRatings record");

        return success("zappRatingId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("ZAPPRATING.update - could not update ZappRatings record");

        return success("zappRatingId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("ZAPPRATING.retire - could not retire ZappRatings record");

        return success("zappRatingId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("id");
        ArrayList zappRatingsList = _dao().find(id, credentialValues);

        if (zappRatingsList.size() < 1)
            return error("ZAPPRATING.find - could not find ZappRatings id "+id);
        if (zappRatingsList.size() > 1)
            return error("ZAPPRATING.find - found multiple ZappRatings records for ZappRatings id "+id);

        ZappRating zappRatings = new ZappRating((JSONObject)zappRatingsList.get(0));
        if (zappRatings == null)
            return error("ZAPPRATING.find - zappRatings==null for id "+id);

        return success(zappRatings.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList zappRatingsList = _dao().findAll(credentialValues);
        return jsonFoundList(zappRatingsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of ZappRatings json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList zappRatingsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(zappRatingsList, limit);

        JSONObject jsonList = jsonFoundList(zappRatingsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            ZappRating zappRatings = new ZappRating((JSONObject)foundList.get(j));
            if (zappRatings == null)
                return JsonError.jsonError(JsonError.Error.ZAPPRATING_FIND, "Could not make ZappRatings object " + j);
            JSONObject json = new JSONObject(zappRatings.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("zappRatings", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        ZAPPRATING_CREATE,
        ZAPPRATING_RETIRE,
        ZAPPRATING_FIND,
        ZAPPRATING_UPDATE,

 */