package com.alleni.feedback;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class Feedback extends BaseBean implements Serializable
{
    static private FeedbackDao _dao() { return new FeedbackDao(); }

    protected Feedback(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("feedbackid" ) ? daoJson.getInt("feedbackid") : -1);
        this.jsonValues.put("reMemberId", daoJson.has("rememberid") ? daoJson.getString("rememberid") : "");
        this.jsonValues.put("reAccountId", daoJson.has("reaccountid") ? daoJson.getInt("reaccountid") : -1);
        this.jsonValues.put("reProjectId", daoJson.has("reprojectid") ? daoJson.getString("reprojectid") : "");
        this.jsonValues.put("comments", daoJson.has("comments") ? daoJson.getString("comments") : "");
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : -1);
        this.jsonValues.put("created", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("FEEDBACK.create - could not create Feedback record");

        return success("feedbackId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("FEEDBACK.update - could not update Feedback record");

        return success("feedbackId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("FEEDBACK.retire - could not retire Feedback record");

        return success("feedbackId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("feedbackId");
        ArrayList feedbackList = _dao().find(id, credentialValues);

        if (feedbackList.size() < 1)
            return error("FEEDBACK.find - could not find Feedback id "+id);
        if (feedbackList.size() > 1)
            return error("FEEDBACK.find - found multiple Feedback records for Feedback id "+id);

        Feedback feedback = new Feedback((JSONObject)feedbackList.get(0));
        if (feedback == null)
            return error("FEEDBACK.find - feedback==null for id "+id);

        return success(feedback.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList feedbackList = _dao().findAll(credentialValues);
        return jsonFoundList(feedbackList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Feedback json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList feedbackList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(feedbackList, limit);

        JSONObject jsonList = jsonFoundList(feedbackList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Feedback feedback = new Feedback((JSONObject)foundList.get(j));
            if (feedback == null)
                return JsonError.jsonError(JsonError.Error.FEEDBACK_FIND, "Could not make Feedback object " + j);
            JSONObject json = new JSONObject(feedback.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("feedback", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        FEEDBACK_CREATE,
        FEEDBACK_RETIRE,
        FEEDBACK_FIND,
        FEEDBACK_UPDATE,

 */