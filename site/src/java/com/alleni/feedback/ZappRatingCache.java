package com.alleni.feedback;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class ZappRatingCache extends BaseBean implements Serializable
{
    static private ZappRatingCacheDao _dao() { return new ZappRatingCacheDao(); }

    protected ZappRatingCache(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("id" ) ? daoJson.getInt("id") : -1);
        this.jsonValues.put("projectid", daoJson.has("projectid") ? daoJson.getString("projectid") : -1);
        this.jsonValues.put("numRatings", daoJson.has("numratings") ? daoJson.getInt("numratings") : -1);
        this.jsonValues.put("ratingTotal", daoJson.has("ratingtotal") ? daoJson.getInt("ratingtotal") : -1);
        this.jsonValues.put("created", daoJson.has("createddatetime") ? (Timestamp)daoJson.get("createddatetime") : new Timestamp(0));
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);
        
    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("ZAPPRATINGCACHE.create - could not create ZappRatingCaches record");

        return success("zappRatingCacheId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("ZAPPRATINGCACHE.update - could not update ZappRatingCaches record");

        return success("zappRatingCacheId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("ZAPPRATINGCACHE.retire - could not retire ZappRatingCaches record");

        return success("zappRatingCacheId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("id");
        ArrayList zappRatingCachesList = _dao().find(id, credentialValues);

        if (zappRatingCachesList.size() < 1)
            return error("ZAPPRATINGCACHE.find - could not find ZappRatingCaches id "+id);
        if (zappRatingCachesList.size() > 1)
            return error("ZAPPRATINGCACHE.find - found multiple ZappRatingCaches records for ZappRatingCaches id "+id);

        ZappRatingCache zappRatingCaches = new ZappRatingCache((JSONObject)zappRatingCachesList.get(0));
        if (zappRatingCaches == null)
            return error("ZAPPRATINGCACHE.find - zappRatingCaches==null for id "+id);

        return success(zappRatingCaches.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList zappRatingCachesList = _dao().findAll(credentialValues);
        return jsonFoundList(zappRatingCachesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of ZappRatingCaches json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList zappRatingCachesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(zappRatingCachesList, limit);

        JSONObject jsonList = jsonFoundList(zappRatingCachesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            ZappRatingCache zappRatingCaches = new ZappRatingCache((JSONObject)foundList.get(j));
            if (zappRatingCaches == null)
                return JsonError.jsonError(JsonError.Error.ZAPPRATINGCACHE_FIND, "Could not make ZappRatingCaches object " + j);
            JSONObject json = new JSONObject(zappRatingCaches.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("zappRatingCaches", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        ZAPPRATINGCACHE_CREATE,
        ZAPPRATINGCACHE_RETIRE,
        ZAPPRATINGCACHE_FIND,
        ZAPPRATINGCACHE_UPDATE,

 */