package com.alleni.feedback;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class ProjectFeedback extends BaseBean implements Serializable
{
    static private ProjectFeedbackDao _dao() { return new ProjectFeedbackDao(); }

    protected ProjectFeedback(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("projectfeedbackid" ) ? daoJson.getInt("projectfeedbackid") : -1);
        this.jsonValues.put("projectID", daoJson.has("projectid") ? daoJson.getString("projectid") : "");
        this.jsonValues.put("feedbackType", daoJson.has("feedbacktype") ? daoJson.getString("feedbacktype") : "");
        this.jsonValues.put("memberID", daoJson.has("memberid") ? daoJson.getString("memberid") : "");
        this.jsonValues.put("flagReason", daoJson.has("flagReason") ? daoJson.getString("flagreason") : "");
        this.jsonValues.put("created", daoJson.has("createddatetime") ? (Timestamp)daoJson.get("createddatetime") : new Timestamp(0));
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
        this.jsonValues.put("retiredById", daoJson.has("retiredbyid") ? daoJson.getString("retiredbyid") : "");
        this.jsonValues.put("retiredDateTime", daoJson.has("retireddatetime") ? (Timestamp)daoJson.get("retireddatetime") : new Timestamp(0));

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("PROJECTFEEDBACK.create - could not create ProjectFeedbacks record");

        return success("projectFeedbackId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("PROJECTFEEDBACK.update - could not update ProjectFeedbacks record");

        return success("projectFeedbackId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("PROJECTFEEDBACK.retire - could not retire ProjectFeedbacks record");

        return success("projectFeedbackId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("projectFeedbackId");
        ArrayList projectFeedbacksList = _dao().find(id, credentialValues);

        if (projectFeedbacksList.size() < 1)
            return error("PROJECTFEEDBACK.find - could not find ProjectFeedbacks id "+id);
        if (projectFeedbacksList.size() > 1)
            return error("PROJECTFEEDBACK.find - found multiple ProjectFeedbacks records for ProjectFeedbacks id "+id);

        ProjectFeedback projectFeedbacks = new ProjectFeedback((JSONObject)projectFeedbacksList.get(0));
        if (projectFeedbacks == null)
            return error("PROJECTFEEDBACK.find - projectFeedbacks==null for id "+id);

        return success(projectFeedbacks.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList projectFeedbacksList = _dao().findAll(credentialValues);
        return jsonFoundList(projectFeedbacksList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of ProjectFeedbacks json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList projectFeedbacksList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(projectFeedbacksList, limit);

        JSONObject jsonList = jsonFoundList(projectFeedbacksList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            ProjectFeedback projectFeedbacks = new ProjectFeedback((JSONObject)foundList.get(j));
            if (projectFeedbacks == null)
                return JsonError.jsonError(JsonError.Error.PROJECTFEEDBACK_FIND, "Could not make ProjectFeedbacks object " + j);
            JSONObject json = new JSONObject(projectFeedbacks.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("projectFeedbacks", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        PROJECTFEEDBACK_CREATE,
        PROJECTFEEDBACK_RETIRE,
        PROJECTFEEDBACK_FIND,
        PROJECTFEEDBACK_UPDATE,

 */