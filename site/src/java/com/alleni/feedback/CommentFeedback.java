package com.alleni.feedback;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class CommentFeedback extends BaseBean implements Serializable
{
    static private CommentFeedbackDao _dao() { return new CommentFeedbackDao(); }

    protected CommentFeedback(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("commentfeedbackid" ) ? daoJson.getInt("commentfeedbackid") : -1);
        this.jsonValues.put("commentId", daoJson.has("commentid") ? daoJson.getInt("commentid") : -1);
        this.jsonValues.put("memberId", daoJson.has("memberid") ? daoJson.getString("memberid") : "");
        this.jsonValues.put("feedback", daoJson.has("feedback") ? daoJson.getString("feedback") : "");
        this.jsonValues.put("created", daoJson.has("createddatetime") ? (Timestamp)daoJson.get("createddatetime") : new Timestamp(0));
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);
        this.jsonValues.put("flagReason", daoJson.has("flagReason") ? daoJson.getString("flagReason") : "");

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("COMMENTFEEDBACK.create - could not create CommentFeedbacks record");

        return success("commentFeedbackId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("COMMENTFEEDBACK.update - could not update CommentFeedbacks record");

        return success("commentFeedbackId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("COMMENTFEEDBACK.retire - could not retire CommentFeedbacks record");

        return success("commentFeedbackId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("commentFeedbackId");
        ArrayList commentFeedbacksList = _dao().find(id, credentialValues);

        if (commentFeedbacksList.size() < 1)
            return error("COMMENTFEEDBACK.find - could not find CommentFeedbacks id "+id);
        if (commentFeedbacksList.size() > 1)
            return error("COMMENTFEEDBACK.find - found multiple CommentFeedbacks records for CommentFeedbacks id "+id);

        CommentFeedback commentFeedbacks = new CommentFeedback((JSONObject)commentFeedbacksList.get(0));
        if (commentFeedbacks == null)
            return error("COMMENTFEEDBACK.find - commentFeedbacks==null for id "+id);

        return success(commentFeedbacks.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList commentFeedbacksList = _dao().findAll(credentialValues);
        return jsonFoundList(commentFeedbacksList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of CommentFeedbacks json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList commentFeedbacksList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(commentFeedbacksList, limit);

        JSONObject jsonList = jsonFoundList(commentFeedbacksList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            CommentFeedback commentFeedbacks = new CommentFeedback((JSONObject)foundList.get(j));
            if (commentFeedbacks == null)
                return JsonError.jsonError(JsonError.Error.COMMENTFEEDBACK_FIND, "Could not make CommentFeedbacks object " + j);
            JSONObject json = new JSONObject(commentFeedbacks.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("commentFeedbacks", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        COMMENTFEEDBACK_CREATE,
        COMMENTFEEDBACK_RETIRE,
        COMMENTFEEDBACK_FIND,
        COMMENTFEEDBACK_UPDATE,

 */