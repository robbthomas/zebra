package com.alleni.feedback;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class ProjectFeedbackDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_projectfeedback_create(?,?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_projectfeedback_update(?,?,?,?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_projectfeedback_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_projectfeedback_filter(?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String FIND        = "{call fn_projectfeedback_find(?,?,?)}";

    public ProjectFeedbackDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_projectID", new HashMapBuilder<String, String>().add("key", "projectID").add("type", "text").build())
            .add("in_feedbackType", new HashMapBuilder<String, String>().add("key", "feedbackType").add("type", "text").build())
            .add("in_memberID", new HashMapBuilder<String, String>().add("key", "memberID").add("type", "text").build())
            .add("in_flagReason", new HashMapBuilder<String, String>().add("key", "flagReason").add("type", "text").build())
            .add("in_retiredById", new HashMapBuilder<String, String>().add("key", "retiredById").add("type", "text").build())
            .add("in_retiredDateTime", new HashMapBuilder<String, String>().add("key", "retiredDateTime").add("type", "timestamp").build())


            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_projectFeedbacksId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_projectID", new HashMapBuilder<String, String>().add("key", "projectID").add("type", "text").build())
            .add("in_feedbackType", new HashMapBuilder<String, String>().add("key", "feedbackType").add("type", "text").build())
            .add("in_memberID", new HashMapBuilder<String, String>().add("key", "memberID").add("type", "text").build())
            .add("in_flagReason", new HashMapBuilder<String, String>().add("key", "flagReason").add("type", "text").build())
            .add("in_retiredById", new HashMapBuilder<String, String>().add("key", "retiredById").add("type", "text").build())
            .add("in_retiredDateTime", new HashMapBuilder<String, String>().add("key", "retiredDateTime").add("type", "timestamp").build())


            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_projectID", new HashMapBuilder<String, String>().add("key", "projectID").add("type", "text").build())
            .add("in_feedbackType", new HashMapBuilder<String, String>().add("key", "feedbackType").add("type", "text").build())
            .add("in_memberID", new HashMapBuilder<String, String>().add("key", "memberID").add("type", "text").build())
            .add("in_flagReason", new HashMapBuilder<String, String>().add("key", "flagReason").add("type", "text").build())
            .add("in_retiredById", new HashMapBuilder<String, String>().add("key", "retiredById").add("type", "text").build())
            .add("in_retiredDateTime", new HashMapBuilder<String, String>().add("key", "retiredDateTime").add("type", "timestamp").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

}