package com.alleni.common;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonError
{
    public enum Error
    {
        UNHANDLED_EXCEPTION,

        // These exist so that the SchemaHelper templates can resolve all their symbols
        // even though they are replaced by actual symbols pertinent to the Web Service.
        __TEMPLATE___CREATE,
        __TEMPLATE___RETIRE,
        __TEMPLATE___FIND,
        __TEMPLATE___UPDATE,
        __TEMPLATEVIEW___FIND,

        LOADER_PRELOAD,
        LOADER_LOAD_SERVICES,
        LOADER_LOAD_STAGE_STEPS,
        LOADER_LOAD_RESOURCES,
        LOADER_LOAD_RESOURCE_MANAGED_SERVICES,
        DATA_MAINTENANCE_UPDATE,
        MAINTENANCE_CREATE,
        MAINTENANCE_RETIRE,
        MAINTENANCE_FIND,
        MAINTENANCE_UPDATE,
        VERSION_CREATE,
        VERSION_RETIRE,
        VERSION_FIND,
        VERSION_UPDATE,
        APPUSER_CREATE,
        APPUSER_RETIRE,
        APPUSER_FIND,
        APPUSER_UPDATE,
        MEMBER_CREATE,
        MEMBER_RETIRE,
        MEMBER_FIND,
        MEMBER_UPDATE,
        MEMBER_LOGIN,
        PROJECT_CREATE,
        PROJECT_RETIRE,
        PROJECT_FIND,
        PROJECT_UPDATE,
        ACCOUNTTYPE_CREATE,
        ACCOUNTTYPE_RETIRE,
        ACCOUNTTYPE_FIND,
        ACCOUNTTYPE_UPDATE,
        ACCOUNT_CREATE,
        ACCOUNT_RETIRE,
        ACCOUNT_FIND,
        ACCOUNT_UPDATE,
        ADDRESS_CREATE,
        ADDRESS_RETIRE,
        ADDRESS_FIND,
        ADDRESS_UPDATE,
        COMMENTFEEDBACK_CREATE,
        COMMENTFEEDBACK_RETIRE,
        COMMENTFEEDBACK_FIND,
        COMMENTFEEDBACK_UPDATE,
        COMPANY_CREATE,
        COMPANY_RETIRE,
        COMPANY_FIND,
        COMPANY_UPDATE,
        TELEPHONE_CREATE,
        TELEPHONE_RETIRE,
        TELEPHONE_FIND,
        TELEPHONE_UPDATE,
        FEEDBACK_CREATE,
        FEEDBACK_RETIRE,
        FEEDBACK_FIND,
        FEEDBACK_UPDATE,
        CREDITCARD_CREATE,
        CREDITCARD_RETIRE,
        CREDITCARD_FIND,
        CREDITCARD_UPDATE,
        DEPOSITLINEITEM_CREATE,
        DEPOSITLINEITEM_RETIRE,
        DEPOSITLINEITEM_FIND,
        DEPOSITLINEITEM_UPDATE,
        DEPOSIT_CREATE,
        DEPOSIT_RETIRE,
        DEPOSIT_FIND,
        DEPOSIT_UPDATE,
        ECHECK_CREATE,
        ECHECK_RETIRE,
        ECHECK_FIND,
        ECHECK_UPDATE,
        FINANCIALTRANSACTION_CREATE,
        FINANCIALTRANSACTION_RETIRE,
        FINANCIALTRANSACTION_FIND,
        FINANCIALTRANSACTION_UPDATE,
        INVOICE_CREATE,
        INVOICE_RETIRE,
        INVOICE_FIND,
        INVOICE_UPDATE,
        INVOICELINEITEM_CREATE,
        INVOICELINEITEM_RETIRE,
        INVOICELINEITEM_FIND,
        INVOICELINEITEM_UPDATE,
        PAYMENT_CREATE,
        PAYMENT_RETIRE,
        PAYMENT_FIND,
        PAYMENT_UPDATE,
        PAYMENTLINEITEM_CREATE,
        PAYMENTLINEITEM_RETIRE,
        PAYMENTLINEITEM_FIND,
        PAYMENTLINEITEM_UPDATE,
        ACCOUNTPROJECTNUMBER_CREATE,
        ACCOUNTPROJECTNUMBER_RETIRE,
        ACCOUNTPROJECTNUMBER_FIND,
        ACCOUNTPROJECTNUMBER_UPDATE,
        CATEGORY_CREATE,
        CATEGORY_RETIRE,
        CATEGORY_FIND,
        CATEGORY_UPDATE,
        PUBLISH_CREATE,
        PUBLISH_RETIRE,
        PUBLISH_FIND,
        PUBLISH_UPDATE,
        PUBLISHLAUNCHCOUNT_CREATE,
        PUBLISHLAUNCHCOUNT_RETIRE,
        PUBLISHLAUNCHCOUNT_FIND,
        PUBLISHLAUNCHCOUNT_UPDATE,
        SCREENSHOT_CREATE,
        SCREENSHOT_RETIRE,
        SCREENSHOT_FIND,
        SCREENSHOT_UPDATE,
        SUBSCRIPTION_CREATE,
        SUBSCRIPTION_RETIRE,
        SUBSCRIPTION_FIND,
        SUBSCRIPTION_UPDATE,
        ACCOUNTMEMBERROLE_CREATE,
        ACCOUNTMEMBERROLE_RETIRE,
        ACCOUNTMEMBERROLE_FIND,
        ACCOUNTMEMBERROLE_UPDATE,
        PROJECTMEMBERROLE_CREATE,
        PROJECTMEMBERROLE_RETIRE,
        PROJECTMEMBERROLE_FIND,
        PROJECTMEMBERROLE_UPDATE,
        ROLETYPE_CREATE,
        ROLETYPE_RETIRE,
        ROLETYPE_FIND,
        ROLETYPE_UPDATE,
        ZAPPCOMMENT_CREATE,
        ZAPPCOMMENT_RETIRE,
        ZAPPCOMMENT_FIND,
        ZAPPCOMMENT_UPDATE,
        ZAPPRATING_CREATE,
        ZAPPRATING_RETIRE,
        ZAPPRATING_FIND,
        ZAPPRATING_UPDATE,
        ZAPPRATINGCACHE_CREATE,
        ZAPPRATINGCACHE_RETIRE,
        ZAPPRATINGCACHE_FIND,
        ZAPPRATINGCACHE_UPDATE,

        CONTACTPREFERENCETYPE_CREATE,
        CONTACTPREFERENCETYPE_RETIRE,
        CONTACTPREFERENCETYPE_FIND,
        CONTACTPREFERENCETYPE_UPDATE,
        CREDITCARDTYPE_CREATE,
        CREDITCARDTYPE_RETIRE,
        CREDITCARDTYPE_FIND,
        CREDITCARDTYPE_UPDATE,

        ZAPPSTATISTICS_GET,
        
        COUPONCODETYPE_CREATE,
        COUPONCODETYPE_RETIRE,
        COUPONCODETYPE_FIND,
        COUPONCODETYPE_UPDATE,
        
        COUPONCODE_CREATE,
        COUPONCODE_RETIRE,
        COUPONCODE_FIND,
        COUPONCODE_UPDATE,
        
        SHAREDPROJECT_CREATE,
        SHAREDPROJECT_RETIRE,
        SHAREDPROJECT_FIND,
        SHAREDPROJECT_UPDATE,

        SHARETYPE_CREATE,
        SHARETYPE_RETIRE,
        SHARETYPE_FIND,
        SHARETYPE_UPDATE,

        GUESTLISTS_CREATE,
        GUESTLISTS_RETIRE,
        GUESTLISTS_FIND,
        GUESTLISTS_UPDATE,
        
        
        GUESTLISTINVITEES_CREATE,
        GUESTLISTINVITEES_RETIRE,
        GUESTLISTINVITEES_FIND,
        GUESTLISTINVITEES_UPDATE,
        
        GUESTLIST_FIND,
        GUESTLIST_RETIRE,
        GUESTLIST_UPDATE,
        GUESTLIST_CREATE,

        GUESTLISTINVITEE_FIND,
        GUESTLISTINVITEE_RETIRE,
        GUESTLISTINVITEE_UPDATE,
        GUESTLISTINVITEE_CREATE,

        PRODUCTCODES_FIND,
        PRODUCTCODES_RETIRE,
        PRODUCTCODES_UPDATE,
        PRODUCTCODES_CREATE,
        
        PROJECTFEEDBACK_CREATE,
        PROJECTFEEDBACK_RETIRE,
        PROJECTFEEDBACK_FIND,
        PROJECTFEEDBACK_UPDATE,
        _end_of_list_ 
    }

    protected static String quoted(String str)
    {
        StringBuffer quotedStr = new StringBuffer();
        quotedStr.append("'");

        String[] lines = str.split("\n");
        for (int j=0; j < lines.length; j++)
        {
            if (j > 0)
                quotedStr.append("\\n");
            quotedStr.append(lines[j]);
        }

        quotedStr.append("'");
        return quotedStr.toString();
    }

    public static JSONObject jsonError(JsonError.Error error, String description) throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put("status", -1);
        json.put("error", quoted(error.toString()));
        json.put("description", quoted(description));

        return json;
    }

    public static String error(JsonError.Error error, StringBuffer description) throws JSONException
    {
        String s = description.toString();
        return error(error, s);
    }

    public static String error(JsonError.Error error, String description) throws JSONException
    {
        return jsonError(error, description).toString();
    }

    public static String error(JsonError.Error error, StringBuffer description, String jsonpCallback) throws JSONException
    {
        String s = description.toString();
        return error(error, s, jsonpCallback);
    }

    public static String error(JsonError.Error error, String description, String jsonpCallback) throws JSONException
    {
        String s = jsonpCallback + "(\n" +
                error(error, description) +
        ")";
        return s;
    }

    public static String error(Exception ex, String jsonpCallback) throws JSONException
    {
        return error(Error.UNHANDLED_EXCEPTION, ex.getMessage(), jsonpCallback);
    }

}
