package com.alleni.common;

public class Validator
{
    public static boolean validEmail(String email, StringBuffer e)
    {
        if (!email.contains("@"))
        {
            e.append("Email - Invalid email address");
            return false;
        }
        return true;        
    }
    
    public static boolean validPassword(String password, StringBuffer e)
    {
        if (password.isEmpty())
        {
            e.append("Password - A password must be specified");
            return false;
        }
        if (password.equals("1234"))
        {
            e.append("Password - please do not use 1234");
            return false;
        }
        return true;
    }
}