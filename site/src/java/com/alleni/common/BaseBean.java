package com.alleni.common;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.Header;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.alleni.db.util.JDBCHelper;
import com.alleni.db.util.PropertyUtil;
import com.alleni.member.AppUser;
import com.alleni.role.AccountMemberRole;

public class BaseBean extends Object implements Serializable
{
    protected JSONObject jsonValues = null;

    public BaseBean()
    {
        jsonValues = new JSONObject();
    }

    public int id() throws org.json.JSONException
    {
        return jsonValues.getInt("id");
    }

    @Override
    public String toString()
    {
        return jsonValues.toString();
    }


    // BENRES TODO Move this to a stored procedure once it's safe to do so...
    public static boolean usernameUnique(String name) throws SQLException {
       Connection          conn = null;
       PreparedStatement   statement = null;
       ResultSet           rs;
       int                 retVal;

       try {
           conn = JDBCHelper.getJdbcConnection();
           statement = conn.prepareStatement("select count(*) from App_User where displayName ilike ?");
           statement.setString(1, name);

           rs = statement.executeQuery();
           rs.next();
           retVal = rs.getInt(1);
       }
       catch (SQLException e) {
           throw e;
       }
       finally {
           JDBCHelper.close(conn);
       }
       return (retVal == 0);
    }
    // PMQ Copy and adapt the above to check email
    public static boolean emailUnique(String email) throws SQLException {
       Connection          conn = null;
       PreparedStatement   statement = null;
       ResultSet           rs;
       int                 retVal;

       try {
           conn = JDBCHelper.getJdbcConnection();
           statement = conn.prepareStatement("select count(*) from App_User where email ilike ?");
           statement.setString(1, email);

           rs = statement.executeQuery();
           rs.next();
           retVal = rs.getInt(1);
       }
       catch (SQLException e) {
           throw e;
       }
       finally {
           JDBCHelper.close(conn);
       }
       return (retVal == 0);
    }
    
    /*
     * Publish.update is broken in the generated code.  Use this for today.
     */
    public static int setGuestListProjectId(String projectId, long guestListId) throws Exception {
        Connection          conn = null;
        PreparedStatement   statement = null;
        ResultSet           rs;
        int					retVal = 0;
        
        String sql = "update Published set projectId = ? where guestListId = ?";
        try {
            conn = JDBCHelper.getJdbcConnection();
            statement = conn.prepareStatement(sql);
            statement.setString(1, projectId);
            statement.setLong(2, guestListId);

            retVal = statement.executeUpdate();
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            JDBCHelper.close(conn);
        }
        return retVal;
    }

    /*
     * DWH:  doesn't use all the JSON passing, but it sure is easy to use!
     */
    public static Map<String, String> getAppUserFirstAndLastNamesFromCreditCardId(Integer ccId) throws SQLException {
        Connection          conn = null;
        PreparedStatement   statement = null;
        ResultSet           rs;
        Map<String, String> retVal = new HashMap<String,String>();
        
        String sql = "select au.firstName, au.lastName from app_user au" +
    			" inner join AccountMemberRoles amr on amr.memberid = au.id" +
    			" inner join CreditCards cc on cc.accountid = amr.accountid" +
    			" where cc.creditcardid = ?";
        try {
            conn = JDBCHelper.getJdbcConnection();
            statement = conn.prepareStatement(sql);
            statement.setInt(1, ccId);

            rs = statement.executeQuery();
            rs.next();
            retVal.put("firstName", rs.getString(1));
            retVal.put("lastName", rs.getString(2));
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            JDBCHelper.close(conn);
        }
        return retVal;
    }
    
    
    /*
     * DWH:  doesn't use all the JSON passing, but it sure is easy to use!
     */
    public static Map<String, String> getAppUserFirstAndLastNamesFromEmail(String email) throws SQLException {
        Connection          conn = null;
        PreparedStatement   statement = null;
        ResultSet           rs;
        Map<String, String> retVal = new HashMap<String,String>();
        
        String sql = "select au.firstName, au.lastName from app_user au where email = ?";
        try {
            conn = JDBCHelper.getJdbcConnection();
            statement = conn.prepareStatement(sql);
            statement.setString(1, email);

            rs = statement.executeQuery();
        	if (rs.next()) {
            	retVal.put("firstName", rs.getString(1));
            	retVal.put("lastName", rs.getString(2));
            }
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            JDBCHelper.close(conn);
        }
        return retVal;
    }

    
    public static final Long getPublishIdFromInviteeUUID(String uuid) throws Exception {
        Connection          conn = null;
        PreparedStatement   statement = null;
        ResultSet           rs;
        Long				retVal = 0l;
        
        String sql = "select publishid from sharedprojectinvitee where uuid = ?";
        try {
            conn = JDBCHelper.getJdbcConnection();
            statement = conn.prepareStatement(sql);
            statement.setString(1, uuid);

            rs = statement.executeQuery();
            
            if (rs.next()) {
            	retVal = rs.getLong("publishid");
            }
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            JDBCHelper.close(conn);
        }
        return retVal;
    }
    

    public static final Long getGuestListIdForPublished(String projectId) throws Exception {
        Connection          conn = null;
        PreparedStatement   statement = null;
        ResultSet           rs;
        Long				retVal = 0l;
        
        String sql = "select guestListId from published where projectId = ?";
        try {
            conn = JDBCHelper.getJdbcConnection();
            statement = conn.prepareStatement(sql);
            statement.setString(1, projectId);

            rs = statement.executeQuery();
            
            if (rs.next()) {
            	retVal = rs.getLong("guestListId");
            }
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            JDBCHelper.close(conn);
        }
        return retVal;
    }

    
    /**
     * Exists because AccountDao only allows lookups of an AppUser's own account.  For private publishing,
     * we need to look up the publisher's account type regardless of the purchaser's account credentials.
     * @param accountId
     * @return
     * @throws SQLException
     */
    public static int getAccountTypeForAccount(int accountId) throws SQLException {
        Connection          conn = null;
        PreparedStatement   statement = null;
        ResultSet           rs;
        int ret = -1;
        
    	String sql = "select accounts.typeid from accounts where accountid = ?";
        try {
            conn = JDBCHelper.getJdbcConnection();
            statement = conn.prepareStatement(sql);
            statement.setInt(1, accountId);

            rs = statement.executeQuery();
            if(rs.next()) { 
            	ret = rs.getInt(1);
            }
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            JDBCHelper.close(conn);
        }
        return ret;
    }
    
    public static String getAccountTypeNameForAccount(int accountId) throws SQLException {
        Connection          conn = null;
        PreparedStatement   statement = null;
        ResultSet           rs;
        String ret = "";
        
    	String sql = "select accountTypes.name " +
    			"from accountTypes " +
    			"    inner join accounts " +
    			"        on accounts.typeid = accounttypes.accounttypeid " +
    			"where accountid = ?";
        try {
            conn = JDBCHelper.getJdbcConnection();
            statement = conn.prepareStatement(sql);
            statement.setInt(1, accountId);

            rs = statement.executeQuery();
            if(rs.next()) { 
            	ret = rs.getString(1);
            }
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            JDBCHelper.close(conn);
        }
        return ret;
    }
    
    public static String getCreditCardTypeNameById(Integer ccTypeId) throws SQLException {
        Connection          conn = null;
        PreparedStatement   statement = null;
        ResultSet           rs;
        String ret = null;
        
        String sql = "select name from CreditCardTypes where creditcardtypeid = ?";
        try {
            conn = JDBCHelper.getJdbcConnection();
            statement = conn.prepareStatement(sql);
            statement.setInt(1, ccTypeId);

            rs = statement.executeQuery();
            if(rs.next()) { 
            	ret = rs.getString(1);
            }
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            JDBCHelper.close(conn);
        }
        return ret;
    }
    
     
    public static JSONObject getAppUserFromCredentials(JSONObject credentials) throws JSONException, SQLException {
        String email = credentials.getString("email");
        if ("guest@zebrazapps.com".equals(email)) {
            return null;
        }

        // login() function acts like a GET and returns App_User row matching the credentials
        //JSONObject retVal = com.alleni.member.AppUser.login(credentials);
        JSONObject retVal = com.alleni.member.AppUser.login2(credentials, 1, 0);

        return retVal;
    }

    public static String getAppUserIdFromCredentials(JSONObject credentials) throws JSONException, SQLException {
        JSONObject obj = getAppUserFromCredentials(credentials);
        String  id = obj.getJSONObject("data").getString("id");
        return id;
    }

    
    public static int getAccountIdForUser(JSONObject credentials) throws JSONException, SQLException {
        String email = credentials.getString("email");
        String id = getAppUserIdFromCredentials(credentials);

        JSONObject accountList = AccountMemberRole.filter(filterValues("memberId", id), recordRangeValues(0, 1), credentials);

        if (!accountList.getBoolean("success")) {
            throw new SQLException("account list filter failed for user: " + email);
        }
        JSONArray accountArray = accountList.getJSONObject("data").getJSONArray("accountMemberRoles");

        if (accountArray.length() == 0) {
            throw new SQLException("No Accounts found for user: " + email);
        }
        // PMQ -- member may have multiple roles for same account. And in general it should be fine
        // for a member to belong to more than one account but we'll deal with that at a later time.
        /*
        if (accountArray.length() > 1 || accountList.getJSONObject("data").getBoolean("hasMoreData")) {
            throw new SQLException("Multipe accounts found for user: " + email);
        }
         *
         */
        return accountArray.getJSONObject(0).getInt("accountId");
    }

    public static JSONObject getAccountForUser(JSONObject credentials) throws JSONException, SQLException {
        String email = credentials.getString("email");
        int accountId = getAccountIdForUser(credentials);

        JSONObject account = com.alleni.account.Account.find(findValues("accountId", accountId), credentials);

        if (!account.getBoolean("success")) {
            throw new SQLException("getAccountForUser failed for user: " + email);
        }

        return account.getJSONObject("data");
    }

    public static JSONObject getAccountTypeForUser(JSONObject credentials) throws JSONException, SQLException {
        String email = credentials.getString("email");

        JSONObject account = getAccountForUser(credentials);
        
        JSONObject retVal = com.alleni.account.AccountType.find(findValues("accountTypeId", account.getInt("typeId")), credentials);

        if (!retVal.getBoolean("success")) {
            throw new SQLException("getAccountTypeForUser failed for user: " + email);
        }

        return retVal.getJSONObject("data");
    }

    public static String getAuthorEmailForProjectId(String projectId) throws Exception {
        Connection          conn = null;
        PreparedStatement   statement = null;
        ResultSet           rs;
        String				email = null;
        
        String sql = "select au.email from" +
        		" app_user au" +
        		" inner join projects on projects.authorMemberId = au.id" +
        		" where projects.projectid = ?";
        try {
            conn = JDBCHelper.getJdbcConnection();
            statement = conn.prepareStatement(sql);
            statement.setString(1, projectId);

            rs = statement.executeQuery();
            if (rs.next()) {
            	email =  rs.getString(1);
            }
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            JDBCHelper.close(conn);
        }
        return email;
    }

    public static JSONObject getAddressForUser(JSONObject credentials, String addressType) throws JSONException, SQLException {

        String email = credentials.getString("email");
        int accountId = getAccountIdForUser(credentials);

        JSONObject addressList = com.alleni.account.Address.filter(
                filterValues("accountId", accountId, "type", addressType),
                recordRangeValues(0, 1),
                credentials);

        if (!addressList.getBoolean("success")) {
            throw new SQLException("web service call for Address.filter did not return success for " + email);
        }

        JSONArray       addressArray = addressList.getJSONObject("data").getJSONArray("addresses");

        if (addressArray.length() > 1 || addressList.getJSONObject("data").getBoolean("hasMoreData")) {
            throw new SQLException("More than one account for user: " + email);
        }
        
        if (addressArray.length() == 0) {
            return null;
        }

        JSONObject retVal = addressArray.getJSONObject(0);
        
        return retVal;
    }

    public static JSONObject getPhoneForUser(JSONObject credentials, String addressType) throws JSONException, SQLException {

        String email = credentials.getString("email");
        int accountId = getAccountIdForUser(credentials);

        JSONObject phoneList = com.alleni.account.Telephone.filter(
                filterValues("accountId", accountId, "type", addressType),
                recordRangeValues(0, 1),
                credentials);

        if (!phoneList.getBoolean("success")) {
            throw new SQLException("web service call for Address.filter did not return success for " + email);
        }

        JSONArray       addressArray = phoneList.getJSONObject("data").getJSONArray("telephones");

        if (addressArray.length() > 1 || phoneList.getJSONObject("data").getBoolean("hasMoreData")) {
            throw new SQLException("More than one phone for user: " + email);
        }

        if (addressArray.length() == 0) {
            return null;
        }

        JSONObject retVal = addressArray.getJSONObject(0);

        return retVal;
    }

    public static void updatePhoneNumber(String phoneNumber, String type, int accountId, JSONObject credentials) throws JSONException, SQLException {
        // update phone number
        JSONObject phone = com.alleni.common.BaseBean.getPhoneForUser(credentials, type);
        if (phone == null) {
            phone = new JSONObject();
            phone.put("memberId", com.alleni.common.BaseBean.getAppUserIdFromCredentials(credentials));
            phone.put("accountId", accountId);
            phone.put("type", type);
            phone.put("number", phoneNumber);
            com.alleni.account.Telephone.create(phone, credentials);
        }
        else {
            JSONObject  phoneCopy = new JSONObject();
            phoneCopy.put("number", phoneNumber);
            phoneCopy.put("id", phone.get("id"));
            phoneCopy.put("type", type);
            com.alleni.account.Telephone.update(phoneCopy, credentials);
        }
    }

    public static JSONObject getCreditCardForUser(JSONObject credentials) throws JSONException, SQLException {
        String email = credentials.getString("email");
        int accountId = getAccountIdForUser(credentials);

        JSONObject  creditCardList = com.alleni.finance.CreditCard.filter(
            filterValues("accountId", accountId),
            recordRangeValues(0, 1),
            credentials);

        if (!creditCardList.getBoolean("success")) {
            throw new SQLException("get CreditCardList did not return success for " + email);
        }
        JSONArray   creditCardArray = creditCardList.getJSONObject("data").getJSONArray("creditCards");

        if (creditCardArray.length() > 1 || creditCardList.getJSONObject("data").getBoolean("hasMoreData")) {
            throw new SQLException("More than one credit card for " + email);
        }
        if (creditCardArray.length() == 0) {
            return null;
        }

        JSONObject retVal = creditCardArray.getJSONObject(0);

        return retVal;
    }

    public static JSONObject recordRangeValues(int offset, int count) throws JSONException {
        JSONObject      recordRangeValues = new JSONObject();
        recordRangeValues.put("offset", offset);
        recordRangeValues.put("count", count);

        return recordRangeValues;
    }

    public static JSONObject recordRangeValues(int offset, int count, String order) throws JSONException {
        JSONObject      recordRangeValues = new JSONObject();
        recordRangeValues.put("offset", offset);
        recordRangeValues.put("count", count);
        recordRangeValues.put("order", order);

        return recordRangeValues;
    }

    public static JSONObject filterValues(String key1, Object value1) throws JSONException {
        JSONObject      filterValues = new JSONObject();
        filterValues.put(key1, value1);

        JSONObject      filterWrapper = new JSONObject();
        filterWrapper.put("filters", filterValues);

        return filterWrapper;
    }

    public static JSONObject filterValues(String key1, Object value1, String key2, Object value2) throws JSONException {
        JSONObject      filterValues = new JSONObject();
        filterValues.put(key1, value1);
        filterValues.put(key2, value2);

        JSONObject      filterWrapper = new JSONObject();
        filterWrapper.put("filters", filterValues);

        return filterWrapper;
    }

    public static JSONObject filterValues(String key1, Object value1, String key2, Object value2, String key3, Object value3) throws JSONException {
        JSONObject      filterValues = new JSONObject();
        filterValues.put(key1, value1);
        filterValues.put(key2, value2);
        filterValues.put(key3, value3);

        JSONObject      filterWrapper = new JSONObject();
        filterWrapper.put("filters", filterValues);

        return filterWrapper;
    }

    public static JSONObject findValues(String key, Object value) throws JSONException {
        JSONObject      findValues = new JSONObject();

        findValues.put(key, value);

        return findValues;
    }


    protected static boolean trimList(List<?> fullList, int limit)
    {
        boolean hasMoreData = false;

        while (limit >= 0 && fullList.size() > limit)
        {
            fullList.remove(fullList.size()-1);
            hasMoreData = true;
        }

        return hasMoreData;
    }

    // This method validates the credentials against the database but it also adds the
    // validated member_id back into the credentialValues with the key "editedByMemberId".
    // This value can be passed into SQL functions so they can record who is making
    // changes to the database.
    protected static boolean validateAndUpdateCredentials(JSONObject credentialValues) throws JSONException, SQLException
    {
        // Try the credentials as potential login. If the cost of doing this
        // database lookup on every call is too high, we can optimize here.
        // Just as long as we get the current member's id from somewhere so
        // we can use it below.

        // Check if it succeeded.
        JSONObject jsonLogin = AppUser.login(credentialValues);
        if (jsonLogin.has("status") && jsonLogin.getInt("status") == 1)
        {
            // We've got valid credentials. Copy the AppUser values into the credentials.
            for (Iterator itKey = jsonLogin.keys(); itKey.hasNext();)
            {
                String key = (String) itKey.next();
                credentialValues.put(key, jsonLogin.get(key));
            }

            // Put the member_id into the credentialValues as "editedByMemberId".
            credentialValues.put(BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME,
                                jsonLogin.has("id") ? jsonLogin.getString("id") : "");
            return true;
        }

        // Invalid credentials.
        return false;
    }

    public static JSONObject success(JSONObject o)
    {
        JSONObject json = new JSONObject();
        try
        {
            json.put("success", true);
            json.put("data", o);
        }
        catch (Exception e)
        {
            return error("BaseBean.success(JSONObject) - caught exception");
        }
        return json;
    }

    protected static JSONObject success(String s)
    {
        try
        {
            return success(new JSONObject(s));
        }
        catch (Exception e)
        {
            return error("BaseBean.success(String) - caught exception");
        }
    }

    protected static JSONObject success(String key, int value)
    {
        try
        {
            JSONObject json = new JSONObject();
            json.put(key, value);
            return success(json);
        }
        catch (Exception e)
        {
            return error("BaseBean.success(String, int) - caught exception");
        }
    }

    protected static JSONObject success(String key, String value)
    {
        try
        {
            JSONObject json = new JSONObject();
            json.put(key, value);
            return success(json);
        }
        catch (Exception e)
        {
            return error("BaseBean.success(String, String) - caught exception");
        }
    }

    protected static JSONObject error(String error)
    {
        return error(error, null);
    }

    public static JSONObject error(String error, String detail)
    {
        JSONObject json = new JSONObject();
        try
        {
            json.put("success", false);
            json.put("error", error);
            json.put("detail", detail);
        }
        catch (Exception e)
        {
            json = new JSONObject();
        }
        return json;
    }

    public static String jsonp(String callback, JSONObject json)
    {
        String s = json.toString();
        if (callback != null)
            s = callback + "(" + s + ")";
        return s;
    }

    public static JSONObject credentials(String authorizationHeader)
    {
        JSONObject credentials = new JSONObject();       
        try
        {
            credentials.put("email", "").put("password", "");

            // authorizationHeader shoud be 'Basic ' concatenated with base64 encoded 'email:password'
            // note that there is a space between Basic and the base64 encoding
            if (authorizationHeader.length() > 9)
            {
                String[] emailPassword = new String(Base64.decodeBase64(authorizationHeader.substring(6))).split(":");

//                sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
//                String[] emailPassword = new String(decoder.decodeBuffer(authorizationHeader.substring(6))).split(":");
                
                if (emailPassword.length == 2)
                {
                    credentials.put("email", emailPassword[0]).put("password", emailPassword[1]);
                    //System.out.println("[acavan] email: "+emailPassword[0]+", password:"+emailPassword[1]);
                }
            }
        }
        catch (Exception e)
        {
            credentials = new JSONObject();
        }
        return credentials;
    }

    public static JSONObject mockdata(String filename)
    {
        String mockdata = "";
        String mockdataPath = PropertyUtil.getProperty("mockdata.path");
        try {
            mockdata = org.apache.commons.io.FileUtils.readFileToString(new java.io.File(mockdataPath+filename));
            return new JSONObject(mockdata);
        }
        catch (Exception e)
        {
            //TODO acavan - do something useful here...
        }
        // the mockdata is already wrapped in data{} (and has a success property) so just return the data object
        return new JSONObject();
    }
    
    public static String proxy(String username, String password, String host, String path, String protocol, String method, String data) throws Exception {
        String json = "";

        // Re-map all requests to port 80 unless coming in on ssl port 443
        // DWH:  use 8080, as the apps will always be available on this port, regardless of environment
        //       port 80 on remote servers goes to apache+mod_jk
        HttpHost targetHost = new HttpHost(host, 80, "http");
        if (protocol.equals("https"))
            targetHost = new HttpHost(host, -1, "https");

        // DWH:  debugging output
//        System.out.println("BaseBean.proxy: username="+username+" password="+password+" host="+host+" path="+path+" protocol="+protocol);
        System.out.println("BaseBean.proxy: targetHost="+targetHost.toString());

        DefaultHttpClient httpclient = null;
        try {
            httpclient = new DefaultHttpClient();
            httpclient.getCredentialsProvider().setCredentials(
                new AuthScope(targetHost.getHostName(), targetHost.getPort()),
                new UsernamePasswordCredentials(username, password)
            );
            // Create AuthCache instance
            AuthCache authCache = new BasicAuthCache();
            // Generate BASIC scheme object and add it to the local auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(targetHost, basicAuth);
            // Add AuthCache to the execution context
            BasicHttpContext localcontext = new BasicHttpContext();
            localcontext.setAttribute(ClientContext.AUTH_CACHE, authCache);

            HttpRequestBase     http;

            if (method.equalsIgnoreCase("get")) {
                http = new HttpGet(path);
            }
            else if (method.equalsIgnoreCase("post")) {
                http = new HttpPost(path);
                if (data != null) {
                	http.setHeader(new BasicHeader("Content-Type", "application/json"));
                	
                	((HttpEntityEnclosingRequestBase)http).setEntity(new StringEntity(data));
                }
            }
            else {
                throw new Exception("BaseBean.proxy - unknown method: '" + method + "'.  Must be one of 'get' or 'post'");
            }

            //System.out.println("    request: " + httpget.getRequestLine());
            //System.out.println("    target: " + targetHost);

            HttpResponse response = httpclient.execute(targetHost, http, localcontext);
            if(response.getStatusLine().getStatusCode() != 200) {
                throw new Exception("HTTP Status " + response.getStatusLine().getStatusCode());
            }
            HttpEntity entity = response.getEntity();

            //System.out.println("    status: " + response.getStatusLine());
            if (entity != null) {
                //System.out.println("    response content length: " + entity.getContentLength());
                json = EntityUtils.toString(entity);
            }
            EntityUtils.consume(entity);
        }
        catch (Exception _e) {
        	_e.printStackTrace();
            //System.out.println("BaseBean.proxy - caught exception:"+_e.getMessage());
            Exception e = new Exception("BaseBean.proxy - caught exception:"+_e.getMessage());
            throw e;
        }
        finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            if (httpclient != null)
                httpclient.getConnectionManager().shutdown();
        }

        // handle some known issues which include:
        //     zephyr calls can come back wrapped in '[]' which is legal JSON but our JSON utility only accepts '{}'
        //     zephyr return HTTP Error 401 when there are authentication problems (there may be other cases)
        char firstCharacter = json.charAt(0);

        if (firstCharacter == '{') // good to go
            return json;

        if (firstCharacter == '[') // looks like an array so wrap in a (lamely named) property
            return "{\"anArray\":"+json+"}";

        if (firstCharacter == '<')
        {
            // look for HTTP status codes
            String status = json.replaceAll("^.*(HTTP Status.*?) - <.*$", "$1");
            if (!status.equals(""))
            {
                //System.out.println("BaseBean.proxy - error (host:"+host+", path:"+path+"):"+status);
                Exception e = new Exception("BaseBean.proxy - error (host:"+host+", path:"+path+"):"+status);
                throw e;
            }
        }

        // don't know what we got so fire exception with the unhandled results...
        //System.out.println("BaseBean.proxy - unhandled results (host:"+host+", path:"+path+"):"+json);
        Exception e = new Exception("BaseBean.proxy - unhandled results (host:"+host+", path:"+path+"):"+json);
        throw e;
    }
}
