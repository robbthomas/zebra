package com.alleni.common;

import com.alleni.db.util.JDBCHelper;
import com.alleni.util.Utility;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.text.ParseException;

public abstract class BaseDAO
{
    // updateParameters maps from the parameters of the "update" stored procedure
    // onto the crud keys to use to retrieve the value to update to.
    // Assuming your update SQL procedure is defined like
    //
    //  FUNCTION fn_update(in_id int, in_param_1 int, in_param_2 text, in_param_list text)
    //
    //  use your contructor to assign as follows.
    //
    // updateParameters = new LinkedHashMapBuilder<String, Map>()
    //    .add("in_id", new HashMapBuilder<String, String>().add("key", "id").add("type", "int").build())
    //    .add("in_param_1", new HashMapBuilder<String, String>().add("key", "param1").add("type", "int").build())
    //    .add("in_param_2", new HashMapBuilder<String, String>().add("key", "param2").add("type", "text").build())
    //    .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
    //    .build();
    //
    // "in_param_list" should be the last parameter in any "update" stored procedure.
    // It is a string containing the names of the parameters that have corresponding
    // crud values, separated by commas.
    //
    // Use LinkedHashMapBuilder to create a LinkedHashMap so the iterator order
    // is guaranteed to be the same as the order the entries go in.
    protected Map<String, Map> createParameters;
    protected Map<String, Map> updateParameters;
    protected Map<String, Map> findParameters;

    // CRUD is an acronym for Create, Retrieve, Update and Delete.
    // Except that we are using Find instead of Retrieve, and Retire instad of Delete.
    // So it should probably be called crufValues, but what the heck.
    protected JSONObject crudValues;
    protected JSONObject recordRangeValues;
    protected JSONObject credentialValues;

    public static String EDITED_BY_MEMBER_ID_PARAM_NAME = "in_editedById";
    public static String EDITED_BY_MEMBER_ID_COLUMN_NAME = "editedById";
    public static String EDITED_BY_MEMBER_ID_VALUE_NAME = "editedById";

    /** Creates a new instance of BaseDAO */
    public BaseDAO ()
    {
    }

    protected int setCredentialsParameters(Connection conn, CallableStatement cs, int offset) throws SQLException, JSONException
    {
        setParameter(cs, this.credentialValues.getString("email"), offset++);
        setParameter(cs, this.credentialValues.getString("password"), offset++);
        return offset;
    }

    protected int setCreateParameters(Connection conn, CallableStatement cs, int offset) throws SQLException, JSONException
    {
        // Don't add in the list of supplied parameters. All are required for Create.
        offset = setCredentialsParameters(conn, cs, offset);
        return setParameters(conn, cs, createParameters, false, offset);
    }

    // Override this if your update parameters are different from the create parameters
    protected int setRetireParameters(Connection conn, CallableStatement cs, int offset) throws SQLException,  JSONException
    {
        offset = setCredentialsParameters(conn, cs, offset);
        setParameter(cs, this.crudValues.getInt("id"), offset++);
        //setParameter(cs, this.credentialValues.getString(BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME), offset++); // App_User.id no longer available in credentialValues
        //setParameter(cs, this.crudValues.getInt("retiredById"), offset++); // so instead plug retiredById in directly from crud values
        setParameter(cs, "", offset++); // unfortunately, retiredById not yet available in crud values
        return offset;
    }

    protected int setUpdateParameters(Connection conn, CallableStatement cs, int offset) throws SQLException,  JSONException
    {
        offset = setCredentialsParameters(conn, cs, offset);
        return setParameters(conn, cs, updateParameters, true, offset);
    }

    protected int setFindParameters(Connection conn, CallableStatement cs, int offset) throws SQLException, JSONException
    {
        offset = setCredentialsParameters(conn, cs, offset);
        
        // A default count of zero means to return all records.
        setParameter(cs, "count",  0, this.recordRangeValues, offset++);
        setParameter(cs, "offset", 0, this.recordRangeValues, offset++);
        setParameter(cs, "order", "", this.recordRangeValues, offset++);

        return setParameters(conn, cs, findParameters, true, offset);
    }

    protected int setParameters(Connection conn, CallableStatement cs, Map<String, Map> parameters, boolean addParamList, int offset) throws SQLException, JSONException
    {
        if (parameters == null)
            return offset;
        
        String paramList = "";
        for (Iterator its=parameters.entrySet().iterator(); its.hasNext();)
        {
            Map.Entry<String, HashMap> paramEntry = (Entry<String, HashMap>) its.next();
            String paramName = paramEntry.getKey();
            String paramType = (String) paramEntry.getValue().get("type");
            String crudKey = (String) paramEntry.getValue().get("key");
            // Check if a param entry has it's own default value. Otherwise, use "" for text and 0 for int.
            String defaultValue = (paramEntry.getValue().containsKey("default") ? (String)paramEntry.getValue().get("default") : "");

            // Don't process the last parameter, since we are adding it in below.
            // If we're not adding the param list, then we want to process all params.
            if (addParamList && !its.hasNext())
                continue;

            // in_edited_by_member_id is a special case. It comes from the credential values,
            // not from the crud values. Unless the credential values haven't been set.
            JSONObject jsonValues = (paramName.equals(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME) && this.credentialValues != null)
                                  ? this.credentialValues
                                  : this.crudValues;

            if (jsonValues.has(crudKey))
                paramList += paramName + ",";

            if (paramType.equals("text"))
            {
                setParameter(cs, crudKey, defaultValue, jsonValues, offset++);
            }
            else if (paramType.equals("int") || paramType.equals("integer"))
            {
                int defInt = Utility.forceInteger(defaultValue, 0);
                setParameter(cs, crudKey, defInt, jsonValues, offset++);
            }
            else if (paramType.equals("int[]") || paramType.equals("integer[]"))
            {
                Integer[] ia;
                if (jsonValues.has(crudKey))
                {
                    // Convert the values to an array of Integers
                    String values = jsonValues.getString(crudKey);
                    String vals[] = values.split(",");
                    
                    // An empty values string results (after split) in a single-
                    // element array containing a zero-length string. Treat this
                    // Don't bother trying to parse this rare occurrence.
                    if (vals.length == 1 && vals[0].length() == 0)
                    {
                        ia = new Integer[] {};
                    }
                    else
                    {
                        ia = new Integer[vals.length];
                        for (int j=0; j < vals.length; j++)
                        {
                            int i = Integer.parseInt(vals[j].trim());
                            ia[j] = i;
                        }
                    }
                }
                else
                {
                    // Use an empty array.
                    ia = new Integer[] {};
                }
                Array ar = conn.createArrayOf("int4", ia);
                cs.setArray(offset++, ar);
            }
            else if (paramType.equals("double precision"))
            {
                double defDbl = Utility.forceDouble(defaultValue, 0);
                setParameter(cs, crudKey, defDbl, jsonValues, offset++);
            }
            else if (paramType.equals("timestamp"))
            {
                Timestamp defTimestamp = new Timestamp(0);
                setParameter(cs, crudKey, defTimestamp, jsonValues, offset++);
            }
            else    // some other data type. get it as a string for now.
            {
                setParameter(cs, crudKey, defaultValue, jsonValues, offset++);
            }
        }

        if (addParamList)
            setParameter(cs, paramList, offset++);

        return offset;
    }

    protected JSONObject resultSetToJSON(ResultSet rs) throws SQLException
    {
        JSONObject json = new JSONObject();
        ResultSetMetaData rsmd = rs.getMetaData();
        try
        {
            for (int i = 1; i < rsmd.getColumnCount()+1; i++)
            {
                String label = rsmd.getColumnLabel(i);
                int type = rsmd.getColumnType(i);
                switch(type)
                {
                 case java.sql.Types.BOOLEAN:
                    json.put(rsmd.getColumnLabel(i), rs.getBoolean(i));
                    break;
                 case java.sql.Types.CHAR:
                    json.put(rsmd.getColumnLabel(i), rs.getByte(i)); // is this the right type?
                    break;
                 case java.sql.Types.DATE:
                    json.put(rsmd.getColumnLabel(i), rs.getDate(i));
                    break;
                 case java.sql.Types.DOUBLE:
                    json.put(rsmd.getColumnLabel(i), rs.getDouble(i));
                    break;
                 case java.sql.Types.FLOAT:
                    json.put(rsmd.getColumnLabel(i), rs.getFloat(i));
                    break;
                 case java.sql.Types.INTEGER:
                    json.put(rsmd.getColumnLabel(i), rs.getInt(i));
                    break;
                 case java.sql.Types.TIMESTAMP:
                    json.put(rsmd.getColumnLabel(i), rs.getTimestamp(i));
                    break;
                 case java.sql.Types.VARCHAR:
                    json.put(rsmd.getColumnLabel(i), rs.getString(i));
                    break;
                 default:
                    // TODO - log this please
                    json.put(rsmd.getColumnLabel(i), rs.getString(i));
                    break;
                }
            }
        }
        catch (JSONException e)
        {
            // TODO - log this please
            json = new JSONObject();
        }
        return json;
    }

    protected int create(String sql, JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.crudValues = createValues;
        this.credentialValues = credentialValues;
        return create(sql);
    }

    protected int create(String sql) throws SQLException, JSONException
    {
        Connection conn = null;
        CallableStatement cs = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(sql);
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
            setCreateParameters(conn, cs, 2);
            return execute(cs, -1);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

    // TEXT Id version of the above to accomodate AppUser.create
    protected String createStr(String sql, JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.crudValues = createValues;
        this.credentialValues = credentialValues;
        return createStr(sql);
    }

    protected String createStr(String sql) throws SQLException, JSONException
    {
        Connection conn = null;
        CallableStatement cs = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(sql);
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            setCreateParameters(conn, cs, 2);
            return execute(cs, "");
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }


    // Actual credential checking is done only at the Bean level. At this level,
    // it is only used to access validated member information. If this info is
    // not used, it is possible to not pass any in.
    public int retire(String sql, JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.crudValues = retireValues;
        this.credentialValues = credentialValues;
        return retire(sql);
    }

    protected int retire(String sql) throws SQLException, JSONException
    {
        int id  = -1;
        // Execute the query
        CallableStatement cs = null;
        Connection conn = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(sql);
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
            setRetireParameters(conn, cs, 2);
            cs.execute();
            id = cs.getInt(1);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
        return id;
    }

    // Actual credential checking is done only at the Bean level. At this level,
    // it is only used to access validated member information. If this info is
    // not used, it is possible to not pass any in.
    protected int update(String sql, JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.crudValues = updateValues;
        this.credentialValues = credentialValues;
        return update(sql);
    }

    protected int update(String sql) throws SQLException, JSONException
    {
        int id  = -1;
        // Execute the query
        CallableStatement cs = null;
        Connection conn = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(sql);
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
            setUpdateParameters(conn, cs, 2);
            cs.execute();
            id = cs.getInt(1);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
        return id;
    }

    protected ArrayList findByOneParameter(String sql, int param, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.credentialValues = credentialValues;

        Connection conn = null;
        CallableStatement cs = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(sql);
            setCredentialsParameters(conn, cs, 1);
            cs.setInt(3, param);
            return execute(cs);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

    protected ArrayList findByOneParameter(String sql, long param, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.credentialValues = credentialValues;

        Connection conn = null;
        CallableStatement cs = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(sql);
            setCredentialsParameters(conn, cs, 1);
            cs.setLong(3, param);
            return execute(cs);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }
    
    protected ArrayList execute(String sql, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.credentialValues = credentialValues;

        Connection conn = null;
        CallableStatement cs = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(sql);
            setCredentialsParameters(conn, cs, 1);
            return execute(cs);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

    protected ArrayList findByOneParameter(String sql, String parameter, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(sql, parameter, false, credentialValues);
    }

    protected ArrayList findByOneParameter(String sql, String parameter, boolean exactMatch, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.credentialValues = credentialValues;

        Connection conn = null;
        CallableStatement cs = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(sql);
            setCredentialsParameters(conn, cs, 1);
            String patternMatch = "%" + parameter + "%";
            cs.setString(3, exactMatch ? parameter : patternMatch);
            return execute(cs);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

    protected ArrayList findWithNoParameters(String sql, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.credentialValues = credentialValues;

        Connection conn = null;
        CallableStatement cs = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(sql);
            setCredentialsParameters(conn, cs, 1);
            return execute(cs);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

    // Use this method sparingly, since it will return all the rows instead
    // of a bounded list. This should only be used for pre-loading data.
    protected ArrayList findAll(String sql, JSONObject credentialValues) throws SQLException, JSONException
    {
        return filter(sql, new JSONObject(), new JSONObject(), credentialValues);
    }

    protected ArrayList filter(String sql, JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // returns ArrayList of JSONObjects
        this.crudValues = findValues;
        this.recordRangeValues = recordRangeValues;
        this.credentialValues = credentialValues;
        return find(sql);
    }

    protected ArrayList find(String sql, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.credentialValues = credentialValues;
        return find(sql);
    }

    protected ArrayList find(String sql) throws SQLException, JSONException
    {
        Connection conn = null;
        CallableStatement cs = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(sql);
            setFindParameters(conn, cs, 1);
            return execute(cs);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

    protected int execute(CallableStatement cs, int defaultValue) throws SQLException
    {
        // Execute a query that returns an int
        int retVal = defaultValue;
        try
        {
            cs.execute();
            retVal = cs.getInt(1);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        return retVal;
    }

    protected String execute(CallableStatement cs, String defaultValue) throws SQLException
    {
        // Execute a query that returns an int
        String retVal = defaultValue;
        try
        {
            cs.execute();
            retVal = cs.getString(1);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        return retVal;
    }

    protected ArrayList execute(CallableStatement cs) throws SQLException
    {
        // Execute a query that returns a result set
        ArrayList jsonList = new ArrayList();
        ResultSet rs = null;

        try
        {
            rs = cs.executeQuery();
            while (rs.next())
            {
                JSONObject json = resultSetToJSON(rs);
                jsonList.add(json);
            }
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(rs);
        }
        return jsonList;
    }

    protected void setParameter(CallableStatement cs, String s, int offset) throws SQLException
    {
        cs.setString(offset, s);
    }

    protected void setParameter(CallableStatement cs, int i, int offset) throws SQLException
    {
        cs.setInt(offset, i);
    }

    protected void setParameter(CallableStatement cs, String column, String s, int offset) throws SQLException
    {
        // column is just used to get sort order right in callee function
        cs.setString(offset, s);
    }

    protected  void setParameter(CallableStatement cs, String column, int i, int offset) throws SQLException
    {
        // column is just used to get sort order right in callee function
        cs.setInt(offset, i);
    }

    protected void setParameter(CallableStatement cs, String key, String defaultValue, JSONObject json, int offset) throws SQLException, JSONException
    {
        cs.setString(offset, json.has(key) && !json.isNull(key) ? json.getString(key) : defaultValue);
    }

    protected void setParameter(CallableStatement cs, String key, int defaultValue, JSONObject json, int offset) throws SQLException, JSONException
    {
        try
        {
            cs.setInt(offset, (json.has(key) &&  json.getString(key).length()>0) ? json.getInt(key) : defaultValue);
        }
        catch (JSONException ex)
        {
            cs.setInt(offset,defaultValue);
        }
    }

    protected void setParameter(CallableStatement cs, String key, double defaultValue, JSONObject json, int offset) throws SQLException, JSONException
    {
        cs.setDouble(offset, json.has(key) ? json.getDouble(key) : defaultValue);
    }

    protected void setParameter(CallableStatement cs, String key, Timestamp defaultValue, JSONObject json, int offset) throws SQLException, JSONException
    {
        // single line call unable to cast json.get String to Timestamp and gets exception
        //cs.setTimestamp(offset, json.has(key) ? (Timestamp)json.get(key) : defaultValue);
        if (json.has(key)) {
            SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy");
            try { defaultValue = new Timestamp(sdf.parse(json.getString(key)).getTime()); }
            catch(ParseException ex) { }
        }
        cs.setTimestamp(offset, defaultValue);
    }

    static protected Connection getConnection () throws SQLException
    {
        return JDBCHelper.getJdbcConnection ();
    }
}
