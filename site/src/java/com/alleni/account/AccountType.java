package com.alleni.account;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class AccountType extends BaseBean implements Serializable
{
    static private AccountTypeDao _dao() { return new AccountTypeDao(); }

    protected AccountType(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("accounttypeid" ) ? daoJson.getInt("accounttypeid") : -1);
        this.jsonValues.put("tag", daoJson.has("tag") ? daoJson.getString("tag") : "");
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("terms", daoJson.has("terms") ? daoJson.getString("terms") : "");
        this.jsonValues.put("price", daoJson.has("price") ? daoJson.getInt("price") : -1);
        this.jsonValues.put("currencyTypeId", daoJson.has("currencytypeid") ? daoJson.getInt("currencytypeid") : -1);
        this.jsonValues.put("displayOrder", daoJson.has("displayorder") ? daoJson.getInt("displayorder") : -1);
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : -1);
        this.jsonValues.put("created", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("ACCOUNTTYPE.create - could not create AccountTypes record");

        return success("accountTypeId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("ACCOUNTTYPE.update - could not update AccountTypes record");

        return success("accountTypeId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("ACCOUNTTYPE.retire - could not retire AccountTypes record");

        return success("accountTypeId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("accountTypeId");
        ArrayList accountTypesList = _dao().find(id, credentialValues);

        if (accountTypesList.size() < 1)
            return error("ACCOUNTTYPE.find - could not find AccountTypes id "+id);
        if (accountTypesList.size() > 1)
            return error("ACCOUNTTYPE.find - found multiple AccountTypes records for AccountTypes id "+id);

        AccountType accountTypes = new AccountType((JSONObject)accountTypesList.get(0));
        if (accountTypes == null)
            return error("ACCOUNTTYPE.find - accountTypes==null for id "+id);

        return success(accountTypes.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList accountTypesList = _dao().findAll(credentialValues);
        return jsonFoundList(accountTypesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of AccountTypes json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList accountTypesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(accountTypesList, limit);

        JSONObject jsonList = jsonFoundList(accountTypesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            AccountType accountTypes = new AccountType((JSONObject)foundList.get(j));
            if (accountTypes == null)
                return JsonError.jsonError(JsonError.Error.ACCOUNTTYPE_FIND, "Could not make AccountTypes object " + j);
            JSONObject json = new JSONObject(accountTypes.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("accountTypes", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        ACCOUNTTYPE_CREATE,
        ACCOUNTTYPE_RETIRE,
        ACCOUNTTYPE_FIND,
        ACCOUNTTYPE_UPDATE,

 */