package com.alleni.account;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AccountDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_account_create(?,?,?,?,?,?,?,?,?,? ,?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_account_update(?,?,?,?,?,?,?,?,?,?,?,? ,?,?,?,?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_account_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_account_filter(?,?,?, ?,?,?, ?,?,?, ?,?,?, ? ,?,?,?,?,?)}";
   private static final String FIND        = "{call fn_account_find(?,?,?)}";

    // NOT CRUD generated
    private static final String FILTERTERMDUE = "{call fn_account_filter_term_due(?,?, ?,?,?, ?,?,?, ?,?,?, ?,?)}";
    private static final String DEMOTEDECLINED = "{call fn_accounts_demote_charges_declined(?,?)}";

    public AccountDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_companyId", new HashMapBuilder<String, String>().add("key", "companyId").add("type", "integer").build())
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_termDueDateTime", new HashMapBuilder<String, String>().add("key", "termDueDateTime").add("type", "timestamp").build())
            .add("in_goodStanding", new HashMapBuilder<String, String>().add("key", "goodStanding").add("type", "integer").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_tokenBalance", new HashMapBuilder<String, String>().add("key", "tokenBalance").add("type", "integer").build())
            .add("in_showCompanyInfoForAll", new HashMapBuilder<String, String>().add("key", "showCompanyInfoForAll").add("type", "integer").build())
            .add("in_contactPreference", new HashMapBuilder<String, String>().add("key", "contactPreference").add("type", "text").build())
            .add("in_convertMethod", new HashMapBuilder<String, String>().add("key", "convertMethod").add("type", "text").build())
            .add("in_convertAmount", new HashMapBuilder<String, String>().add("key", "convertAmount").add("type", "integer").build())
            .add("in_paypalLogin", new HashMapBuilder<String, String>().add("key", "paypalLogin").add("type", "text").build())
            .add("in_allowSubscriptionPayment", new HashMapBuilder<String, String>().add("key", "allowSubscriptionPayment").add("type", "integer").build())
            .add("in_renewalNoticeSent", new HashMapBuilder<String, String>().add("key", "renewalNoticeSent").add("type", "integer").build())
            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            
            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            
            .add("in_companyId", new HashMapBuilder<String, String>().add("key", "companyId").add("type", "integer").build())
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_termDueDateTime", new HashMapBuilder<String, String>().add("key", "termDueDateTime").add("type", "timestamp").build())
            .add("in_goodStanding", new HashMapBuilder<String, String>().add("key", "goodStanding").add("type", "integer").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_tokenBalance", new HashMapBuilder<String, String>().add("key", "tokenBalance").add("type", "integer").build())
            .add("in_showCompanyInfoForAll", new HashMapBuilder<String, String>().add("key", "showCompanyInfoForAll").add("type", "integer").build())
            .add("in_contactPreference", new HashMapBuilder<String, String>().add("key", "contactPreference").add("type", "text").build())
            .add("in_convertMethod", new HashMapBuilder<String, String>().add("key", "convertMethod").add("type", "text").build())
            .add("in_convertAmount", new HashMapBuilder<String, String>().add("key", "convertAmount").add("type", "integer").build())
            .add("in_paypalLogin", new HashMapBuilder<String, String>().add("key", "paypalLogin").add("type", "text").build())
            .add("in_allowSubscriptionPayment", new HashMapBuilder<String, String>().add("key", "allowSubscriptionPayment").add("type", "integer").build())
            .add("in_billingContactSameAsPersonal", new HashMapBuilder<String, String>().add("key", "billingContactSameAsPersonal").add("type", "integer").build())
            .add("in_billingAddressSameAsCompany", new HashMapBuilder<String, String>().add("key", "billingAddressSameAsCompany").add("type", "integer").build())
            .add("in_companySameAsPersonal", new HashMapBuilder<String, String>().add("key", "companySameAsPersonal").add("type", "integer").build())
            .add("in_renewalNoticeSent", new HashMapBuilder<String, String>().add("key", "renewalNoticeSent").add("type", "integer").build())
            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            
            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_companyId", new HashMapBuilder<String, String>().add("key", "companyId").add("type", "integer").build())
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_termDueDateTime", new HashMapBuilder<String, String>().add("key", "termDueDateTime").add("type", "timestamp").build())
            .add("in_goodStanding", new HashMapBuilder<String, String>().add("key", "goodStanding").add("type", "integer").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_tokenBalance", new HashMapBuilder<String, String>().add("key", "tokenBalance").add("type", "integer").build())
            .add("in_showCompanyInfoForAll", new HashMapBuilder<String, String>().add("key", "showCompanyInfoForAll").add("type", "integer").build())
            .add("in_contactPreference", new HashMapBuilder<String, String>().add("key", "contactPreference").add("type", "text").build())
            .add("in_convertMethod", new HashMapBuilder<String, String>().add("key", "convertMethod").add("type", "text").build())
            .add("in_convertAmount", new HashMapBuilder<String, String>().add("key", "convertAmount").add("type", "integer").build())
            .add("in_paypalLogin", new HashMapBuilder<String, String>().add("key", "paypalLogin").add("type", "text").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }


    //=========================================================================/
    //==================== CUSTOM LOGIC NOT CRUD GENERATED ====================/
    //=========================================================================/

    public ArrayList filterTermDue(JSONObject findValues, JSONObject rangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return filter(FILTERTERMDUE, findValues, rangeValues, credentialValues);
    }

    public ArrayList demoteChargesDeclined(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findWithNoParameters(DEMOTEDECLINED, credentialValues);
    }

}