package com.alleni.account;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class Telephone extends BaseBean implements Serializable
{
    static private TelephoneDao _dao() { return new TelephoneDao(); }

    protected Telephone(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("telephoneid" ) ? daoJson.getInt("telephoneid") : -1);
        this.jsonValues.put("type", daoJson.has("type") ? daoJson.getString("type") : "");
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("memberId", daoJson.has("memberid") ? daoJson.getString("memberid") : "");
        this.jsonValues.put("label", daoJson.has("label") ? daoJson.getString("label") : "");
        this.jsonValues.put("number", daoJson.has("number") ? daoJson.getString("number") : "");
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : -1);
        this.jsonValues.put("created", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("TELEPHONE.create - could not create Telephones record");

        return success("telephoneId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("TELEPHONE.update - could not update Telephones record");

        return success("telephoneId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("TELEPHONE.retire - could not retire Telephones record");

        return success("telephoneId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("telephoneId");
        ArrayList telephonesList = _dao().find(id, credentialValues);

        if (telephonesList.size() < 1)
            return error("TELEPHONE.find - could not find Telephones id "+id);
        if (telephonesList.size() > 1)
            return error("TELEPHONE.find - found multiple Telephones records for Telephones id "+id);

        Telephone telephones = new Telephone((JSONObject)telephonesList.get(0));
        if (telephones == null)
            return error("TELEPHONE.find - telephones==null for id "+id);

        return success(telephones.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList telephonesList = _dao().findAll(credentialValues);
        return jsonFoundList(telephonesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Telephones json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList telephonesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(telephonesList, limit);

        JSONObject jsonList = jsonFoundList(telephonesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Telephone telephones = new Telephone((JSONObject)foundList.get(j));
            if (telephones == null)
                return JsonError.jsonError(JsonError.Error.TELEPHONE_FIND, "Could not make Telephones object " + j);
            JSONObject json = new JSONObject(telephones.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("telephones", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        TELEPHONE_CREATE,
        TELEPHONE_RETIRE,
        TELEPHONE_FIND,
        TELEPHONE_UPDATE,

 */