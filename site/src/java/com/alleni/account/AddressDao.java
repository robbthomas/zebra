package com.alleni.account;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AddressDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_address_create(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_address_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_address_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_address_filter(?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?,?,?)}";
   private static final String FIND        = "{call fn_address_find(?,?,?)}";

    public AddressDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_company", new HashMapBuilder<String, String>().add("key", "company").add("type", "text").build())
            .add("in_address1", new HashMapBuilder<String, String>().add("key", "address1").add("type", "text").build())
            .add("in_address2", new HashMapBuilder<String, String>().add("key", "address2").add("type", "text").build())
            .add("in_city", new HashMapBuilder<String, String>().add("key", "city").add("type", "text").build())
            .add("in_stateProvince", new HashMapBuilder<String, String>().add("key", "stateProvince").add("type", "text").build())
            .add("in_zipPostalCode", new HashMapBuilder<String, String>().add("key", "zipPostalCode").add("type", "text").build())
            .add("in_country", new HashMapBuilder<String, String>().add("key", "country").add("type", "text").build())
            .add("in_website", new HashMapBuilder<String, String>().add("key", "website").add("type", "text").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "twitter").add("type", "text").build())
            .add("in_twitter", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_phone", new HashMapBuilder<String, String>().add("key", "phone").add("type", "text").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_addressesId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_company", new HashMapBuilder<String, String>().add("key", "company").add("type", "text").build())
            .add("in_address1", new HashMapBuilder<String, String>().add("key", "address1").add("type", "text").build())
            .add("in_address2", new HashMapBuilder<String, String>().add("key", "address2").add("type", "text").build())
            .add("in_city", new HashMapBuilder<String, String>().add("key", "city").add("type", "text").build())
            .add("in_stateProvince", new HashMapBuilder<String, String>().add("key", "stateProvince").add("type", "text").build())
            .add("in_zipPostalCode", new HashMapBuilder<String, String>().add("key", "zipPostalCode").add("type", "text").build())
            .add("in_country", new HashMapBuilder<String, String>().add("key", "country").add("type", "text").build())
            .add("in_website", new HashMapBuilder<String, String>().add("key", "website").add("type", "text").build())
            .add("in_twitter", new HashMapBuilder<String, String>().add("key", "twitter").add("type", "text").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_phone", new HashMapBuilder<String, String>().add("key", "phone").add("type", "text").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_company", new HashMapBuilder<String, String>().add("key", "company").add("type", "text").build())
            .add("in_address1", new HashMapBuilder<String, String>().add("key", "address1").add("type", "text").build())
            .add("in_address2", new HashMapBuilder<String, String>().add("key", "address2").add("type", "text").build())
            .add("in_city", new HashMapBuilder<String, String>().add("key", "city").add("type", "text").build())
            .add("in_stateProvince", new HashMapBuilder<String, String>().add("key", "stateProvince").add("type", "text").build())
            .add("in_zipPostalCode", new HashMapBuilder<String, String>().add("key", "zipPostalCode").add("type", "text").build())
            .add("in_country", new HashMapBuilder<String, String>().add("key", "country").add("type", "text").build())
            .add("in_website", new HashMapBuilder<String, String>().add("key", "website").add("type", "text").build())
            .add("in_twitter", new HashMapBuilder<String, String>().add("key", "twitter").add("type", "text").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_phone", new HashMapBuilder<String, String>().add("key", "phone").add("type", "text").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

}