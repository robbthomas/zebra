package com.alleni.account;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class Address extends BaseBean implements Serializable
{
    static private AddressDao _dao() { return new AddressDao(); }

    protected Address(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("addressId", daoJson.has("addressid" ) ? daoJson.getInt("addressid") : -1);
        this.jsonValues.put("type", daoJson.has("type") ? daoJson.getString("type") : "");
        this.jsonValues.put("typeId", daoJson.has("typeid") ? daoJson.getInt("typeid") : -1);
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("company", daoJson.has("company") ? daoJson.getString("company") : "");
        this.jsonValues.put("address1", daoJson.has("address1") ? daoJson.getString("address1") : "");
        this.jsonValues.put("address2", daoJson.has("address2") ? daoJson.getString("address2") : "");
        this.jsonValues.put("city", daoJson.has("city") ? daoJson.getString("city") : "");
        this.jsonValues.put("stateProvince", daoJson.has("stateprovince") ? daoJson.getString("stateprovince") : "");
        this.jsonValues.put("zipPostalCode", daoJson.has("zippostalcode") ? daoJson.getString("zippostalcode") : "");
        this.jsonValues.put("country", daoJson.has("country") ? daoJson.getString("country") : "");
        this.jsonValues.put("website", daoJson.has("website") ? daoJson.getString("website") : "");
        this.jsonValues.put("email", daoJson.has("email") ? daoJson.getString("email") : "");
        this.jsonValues.put("twitter", daoJson.has("twitter") ? daoJson.getString("twitter") : "");
        this.jsonValues.put("phone", daoJson.has("phone") ? daoJson.getString("phone") : "");
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : -1);
        this.jsonValues.put("created", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("ADDRESS.create - could not create Addresses record");

        return success("addressId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("ADDRESS.update - could not update Addresses record");

        return success("addressId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("ADDRESS.retire - could not retire Addresses record");

        return success("addressId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("addressId");
        ArrayList addressesList = _dao().find(id, credentialValues);

        if (addressesList.size() < 1)
            return error("ADDRESS.find - could not find Addresses id "+id);
        if (addressesList.size() > 1)
            return error("ADDRESS.find - found multiple Addresses records for Addresses id "+id);

        Address addresses = new Address((JSONObject)addressesList.get(0));
        if (addresses == null)
            return error("ADDRESS.find - addresses==null for id "+id);

        return success(addresses.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList addressesList = _dao().findAll(credentialValues);
        return jsonFoundList(addressesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Addresses json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList addressesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(addressesList, limit);

        JSONObject jsonList = jsonFoundList(addressesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Address addresses = new Address((JSONObject)foundList.get(j));
            if (addresses == null)
                return JsonError.jsonError(JsonError.Error.ADDRESS_FIND, "Could not make Addresses object " + j);
            JSONObject json = new JSONObject(addresses.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("addresses", foundList);

        return jsonList;
    }

    //=========================================================================/
    //==================== CUSTOM LOGIC NOT CRUD GENERATED ====================/
    //=========================================================================/

    public static JSONObject findByIdRaw(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList addressesList = _dao().find(id, credentialValues);

        if (addressesList.size() < 1)
            return error("ADDRESS.find - could not find Addresses id "+id);
        if (addressesList.size() > 1)
            return error("ADDRESS.find - found multiple Addresses records for Addresses id "+id);

        Address addresses = new Address((JSONObject)addressesList.get(0));
        if (addresses == null)
            return error("ADDRESS.find - addresses==null for id "+id);

        return addresses.jsonValues;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        ADDRESS_CREATE,
        ADDRESS_RETIRE,
        ADDRESS_FIND,
        ADDRESS_UPDATE,

 */