package com.alleni.account;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class Company extends BaseBean implements Serializable
{
    static private CompanyDao _dao() { return new CompanyDao(); }

    protected Company(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("companyid" ) ? daoJson.getInt("companyid") : -1);
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("companyUrl", daoJson.has("companyurl") ? daoJson.getString("companyurl") : "");
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : -1);
        this.jsonValues.put("created", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("COMPANY.create - could not create Companies record");

        return success("companyId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("COMPANY.update - could not update Companies record");

        return success("companyId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("COMPANY.retire - could not retire Companies record");

        return success("companyId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("comapanyId");
        ArrayList companiesList = _dao().find(id, credentialValues);

        if (companiesList.size() < 1)
            return error("COMPANY.find - could not find Companies id "+id);
        if (companiesList.size() > 1)
            return error("COMPANY.find - found multiple Companies records for Companies id "+id);

        Company companies = new Company((JSONObject)companiesList.get(0));
        if (companies == null)
            return error("COMPANY.find - companies==null for id "+id);

        return success(companies.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList companiesList = _dao().findAll(credentialValues);
        return jsonFoundList(companiesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Companies json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList companiesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(companiesList, limit);

        JSONObject jsonList = jsonFoundList(companiesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Company companies = new Company((JSONObject)foundList.get(j));
            if (companies == null)
                return JsonError.jsonError(JsonError.Error.COMPANY_FIND, "Could not make Companies object " + j);
            JSONObject json = new JSONObject(companies.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("companies", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        COMPANY_CREATE,
        COMPANY_RETIRE,
        COMPANY_FIND,
        COMPANY_UPDATE,

 */