package com.alleni.account;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AccountTypeDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_accounttype_create(?,?,?,?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_accounttype_update(?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_accounttype_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_accounttype_filter(?,?,?, ?,?,?, ?,?,?, ?,?,?, ?)}";
   private static final String FIND        = "{call fn_accounttype_find(?,?,?)}";

    public AccountTypeDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_tag", new HashMapBuilder<String, String>().add("key", "tag").add("type", "text").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_terms", new HashMapBuilder<String, String>().add("key", "terms").add("type", "text").build())
            .add("in_price", new HashMapBuilder<String, String>().add("key", "price").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_displayOrder", new HashMapBuilder<String, String>().add("key", "displayOrder").add("type", "integer").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountTypesId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_tag", new HashMapBuilder<String, String>().add("key", "tag").add("type", "text").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_terms", new HashMapBuilder<String, String>().add("key", "terms").add("type", "text").build())
            .add("in_price", new HashMapBuilder<String, String>().add("key", "price").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_displayOrder", new HashMapBuilder<String, String>().add("key", "displayOrder").add("type", "integer").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_tag", new HashMapBuilder<String, String>().add("key", "tag").add("type", "text").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_terms", new HashMapBuilder<String, String>().add("key", "terms").add("type", "text").build())
            .add("in_price", new HashMapBuilder<String, String>().add("key", "price").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_displayOrder", new HashMapBuilder<String, String>().add("key", "displayOrder").add("type", "integer").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

}