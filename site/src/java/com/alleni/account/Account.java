package com.alleni.account;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class Account extends BaseBean implements Serializable
{
    static private AccountDao _dao() { return new AccountDao(); }

    protected Account(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("accountId", daoJson.has("accountid" ) ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("companyId", daoJson.has("companyid") ? daoJson.getInt("companyid") : -1);
        this.jsonValues.put("type", daoJson.has("type") ? daoJson.getString("type") : "");
        this.jsonValues.put("typeId", daoJson.has("typeid") ? daoJson.getString("typeid") : "");
        this.jsonValues.put("termDueDateTime", daoJson.has("termduedatetime") ? (Timestamp)daoJson.get("termduedatetime") : new Timestamp(0));
        this.jsonValues.put("goodStanding", daoJson.has("goodstanding") ? daoJson.getInt("goodstanding") : 1);
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("tokenBalance", daoJson.has("tokenbalance") ? daoJson.getInt("tokenbalance") : -1);
        this.jsonValues.put("showCompanyInfoForAll", daoJson.has("showcompanyinfoforall") ? daoJson.getInt("showcompanyinfoforall") : 0);
        this.jsonValues.put("contactPreference", daoJson.has("contactpreference") ? daoJson.getString("contactpreference") : "");
        this.jsonValues.put("convertMethod", daoJson.has("convertmethod") ? daoJson.getString("convertmethod") : "convert_method_convert_all");
        this.jsonValues.put("convertAmount", daoJson.has("convertamount") ? daoJson.getInt("convertamount") : "0");
        this.jsonValues.put("paypalLogin", daoJson.has("paypallogin") ? daoJson.getString("paypallogin") : "");
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : "");
        this.jsonValues.put("editedDateTime", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
        this.jsonValues.put("retiredById", daoJson.has("retiredbyid") ? daoJson.getString("retiredbyid") : "");
        this.jsonValues.put("allowSubscriptionPayment", daoJson.has("allowsubscriptionpayment") ? daoJson.getInt("allowsubscriptionpayment") : 0);
        this.jsonValues.put("billingContactSameAsPersonal", daoJson.has("billingcontactsameaspersonal") ? daoJson.getInt("billingcontactsameaspersonal") : 0);
        this.jsonValues.put("billingAddressSameAsCompany", daoJson.has("billingaddresssameascompany") ? daoJson.getInt("billingaddresssameascompany") : 0);
        this.jsonValues.put("companySameAsPersonal", daoJson.has("companysameaspersonal") ? daoJson.getInt("companysameaspersonal") : 0);
        this.jsonValues.put("renewalNoticeSent", daoJson.has("renewalnoticesent") ? daoJson.getInt("renewalnoticesent") : 0);
        // extra parameter returned by Accounts reads but never written -- parameter is created and added once in fn_account_create
        this.jsonValues.put("merchantCustomerId", daoJson.has("merchantcustomerid") ? daoJson.getString("merchantcustomerid") : "");
    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("ACCOUNT.create - could not create Accounts record");

        return success("accountId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("ACCOUNT.update - could not update Accounts record");

        return success("accountId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("ACCOUNT.retire - could not retire Accounts record");

        return success("accountId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("accountId");
        ArrayList accountsList = _dao().find(id, credentialValues);

        if (accountsList.size() < 1)
            return error("ACCOUNT.find - could not find Accounts id "+id);
        if (accountsList.size() > 1)
            return error("ACCOUNT.find - found multiple Accounts records for Accounts id "+id);

        Account accounts = new Account((JSONObject)accountsList.get(0));
        if (accounts == null)
            return error("ACCOUNT.find - accounts==null for id "+id);

        return success(accounts.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList accountsList = _dao().findAll(credentialValues);
        return jsonFoundList(accountsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Accounts json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList accountsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(accountsList, limit);

        JSONObject jsonList = jsonFoundList(accountsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Account accounts = new Account((JSONObject)foundList.get(j));
            if (accounts == null)
                return JsonError.jsonError(JsonError.Error.ACCOUNT_FIND, "Could not make Accounts object " + j);
            JSONObject json = new JSONObject(accounts.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("accounts", foundList);

        return jsonList;
    }

    //=========================================================================/
    //==================== CUSTOM LOGIC NOT CRUD GENERATED ====================/
    //=========================================================================/

    public static JSONObject filterTermDue(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Accounts json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList accountsList = _dao().filterTermDue(findValues, recordRangeValues, credentialValues);

        JSONObject jsonList = jsonFoundList(accountsList);
        return success(jsonList);
    }

    public static JSONObject demoteChargesDeclined(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList accountsList = _dao().demoteChargesDeclined(credentialValues);

        JSONObject jsonList = jsonFoundList(accountsList);
        return success(jsonList);
    }


}

/*
 PUT IN com.alleni.common.jsonError.java

        ACCOUNT_CREATE,
        ACCOUNT_RETIRE,
        ACCOUNT_FIND,
        ACCOUNT_UPDATE,

 */
