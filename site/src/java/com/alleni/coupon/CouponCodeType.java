package com.alleni.coupon;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class CouponCodeType extends BaseBean implements Serializable
{
    static private CouponCodeTypeDao _dao() { return new CouponCodeTypeDao(); }

    protected CouponCodeType(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("couponcodetypeid" ) ? daoJson.getInt("couponcodetypeid") : -1);
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("COUPONCODETYPE.create - could not create CouponCodeTypes record");

        return success("couponCodeTypeId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("COUPONCODETYPE.update - could not update CouponCodeTypes record");

        return success("couponCodeTypeId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("COUPONCODETYPE.retire - could not retire CouponCodeTypes record");

        return success("couponCodeTypeId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("couponCodeTypeId");
        ArrayList couponCodeTypesList = _dao().find(id, credentialValues);

        if (couponCodeTypesList.size() < 1)
            return error("COUPONCODETYPE.find - could not find CouponCodeTypes id "+id);
        if (couponCodeTypesList.size() > 1)
            return error("COUPONCODETYPE.find - found multiple CouponCodeTypes records for CouponCodeTypes id "+id);

        CouponCodeType couponCodeTypes = new CouponCodeType((JSONObject)couponCodeTypesList.get(0));
        if (couponCodeTypes == null)
            return error("COUPONCODETYPE.find - couponCodeTypes==null for id "+id);

        return success(couponCodeTypes.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList couponCodeTypesList = _dao().findAll(credentialValues);
        return jsonFoundList(couponCodeTypesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of CouponCodeTypes json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList couponCodeTypesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(couponCodeTypesList, limit);

        JSONObject jsonList = jsonFoundList(couponCodeTypesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            CouponCodeType couponCodeTypes = new CouponCodeType((JSONObject)foundList.get(j));
            if (couponCodeTypes == null)
                return JsonError.jsonError(JsonError.Error.COUPONCODETYPE_FIND, "Could not make CouponCodeTypes object " + j);
            JSONObject json = new JSONObject(couponCodeTypes.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("couponCodeTypes", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        COUPONCODETYPE_CREATE,
        COUPONCODETYPE_RETIRE,
        COUPONCODETYPE_FIND,
        COUPONCODETYPE_UPDATE,

 */