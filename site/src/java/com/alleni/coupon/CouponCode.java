package com.alleni.coupon;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class CouponCode extends BaseBean implements Serializable
{
    static private CouponCodeDao _dao() { return new CouponCodeDao(); }

    protected CouponCode(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("couponid" ) ? daoJson.getInt("couponid") : -1);
        this.jsonValues.put("code", daoJson.has("code") ? daoJson.getString("code") : "");
        this.jsonValues.put("couponCodeTypeId", daoJson.has("couponcodetypeid") ? daoJson.getString("couponcodetypeid") : "");
        this.jsonValues.put("retiredDateTime", daoJson.has("retireddatetime") ? daoJson.getInt("retireddatetime") : 0);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("COUPONCODE.create - could not create CouponCodes record");

        return success("couponCodeId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("COUPONCODE.update - could not update CouponCodes record");

        return success("couponCodeId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("COUPONCODE.retire - could not retire CouponCodes record");

        return success("couponCodeId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("couponCodeId");
        ArrayList couponCodesList = _dao().find(id, credentialValues);

        if (couponCodesList.size() < 1)
            return error("COUPONCODE.find - could not find CouponCodes id "+id);
        if (couponCodesList.size() > 1)
            return error("COUPONCODE.find - found multiple CouponCodes records for CouponCodes id "+id);

        CouponCode couponCodes = new CouponCode((JSONObject)couponCodesList.get(0));
        if (couponCodes == null)
            return error("COUPONCODE.find - couponCodes==null for id "+id);

        return success(couponCodes.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList couponCodesList = _dao().findAll(credentialValues);
        return jsonFoundList(couponCodesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of CouponCodes json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList couponCodesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(couponCodesList, limit);

        JSONObject jsonList = jsonFoundList(couponCodesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            CouponCode couponCodes = new CouponCode((JSONObject)foundList.get(j));
            if (couponCodes == null)
                return JsonError.jsonError(JsonError.Error.COUPONCODE_FIND, "Could not make CouponCodes object " + j);
            JSONObject json = new JSONObject(couponCodes.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("couponCodes", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        COUPONCODE_CREATE,
        COUPONCODE_RETIRE,
        COUPONCODE_FIND,
        COUPONCODE_UPDATE,

 */