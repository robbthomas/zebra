
package com.alleni.util;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Simple mail send class adapted from msgsendsample.java example
 * included with JavaMail 1.4.2.
 *
 * @author root
 */
public class Mail {

	private boolean bypass = false;
	private String bypassToEmails; // can be single or comma delimited list of emails


	// Mail server port options:
	// SSL SMTP 465, POP3 995
	// non-SSL SMTP 3535, POP3 110

	private String smtpServer = "localhost"; //"smtp.qualityprocess.com";

	private String defaultFrom = "donotreply@zebrazapps.com";
	private String mailer = "AI Zebra";



	public void setBypass(String emails)
	{
		if (emails != null && emails.trim().length() > 0)
		{
			bypassToEmails = emails.trim();
			bypass = true;
		}
	}




	public void sendMultiPartMessage(String from, String to, String subject, String body, String bodyHTML)
	{
		if (from == null) {
			from = this.defaultFrom;
		}

		// create some properties and get the default Session
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpServer);
		props.put("mail.smtp.timeout", "30000"); // 30 seconds

		//Session session = Session.getInstance(props, null);
		Session session = Session.getDefaultInstance(props, null);

		try {
			// create a message
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			//
			// If single recipient address can use the following line:
			//InternetAddress[] address = {new InternetAddress(to)};
			//
			// Instead, support multiple recipient addresses separated by commas.
			String [] arrto = bypass ? bypassToEmails.split(",") : to.split(",");
			InternetAddress[] address = new InternetAddress[arrto.length];
			for (int i = 0; i < arrto.length; i++) {
				address[i] = new InternetAddress(arrto[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, address);

			// set BCC
			//InternetAddress[] bcc = {new InternetAddress("paulq@qualityprocess.com")};
			//            InternetAddress[] bcc = {new InternetAddress("info@alleni.com")};
			//            msg.setRecipients(Message.RecipientType.BCC, bcc);

			msg.setSubject(subject);
			msg.setSentDate(new Date());


			Multipart mp = new MimeMultipart("alternative");

			if (body != null && body.length() > 0) {
                                if (bypass) body = "***** Bypass: Message intendended for " + to + " *****\n\n" + body;
				// text alternative
				BodyPart bpText = new MimeBodyPart();
				bpText.setDataHandler(new DataHandler(body,"text/plain"));
				mp.addBodyPart(bpText);
			}

			
			if (bodyHTML != null && bodyHTML.length() > 0) {
                                if (bypass) bodyHTML = "***** Bypass: Message intendended for " + to + " *****<br/><br/>" + bodyHTML;
				// html
				BodyPart bpHTML = new MimeBodyPart();
				bpHTML.setDataHandler(new DataHandler(bodyHTML,"text/html"));
				mp.addBodyPart(bpHTML);

			}


			msg.setContent(mp);

			// setup header
			msg.setHeader("X-Mailer", mailer);


			Transport.send(msg);

		} catch (MessagingException mex) {
			System.out.println("\n--Exception handling in Mail.java");

			mex.printStackTrace();
			System.out.println();
			Exception ex = mex;
			do {
				if (ex instanceof SendFailedException) {
					SendFailedException sfex = (SendFailedException)ex;
					Address[] invalid = sfex.getInvalidAddresses();
					if (invalid != null) {
						System.out.println("    ** Invalid Addresses");
						if (invalid != null) {
							for (int i = 0; i < invalid.length; i++)
								System.out.println("         " + invalid[i]);
						}
					}
					Address[] validUnsent = sfex.getValidUnsentAddresses();
					if (validUnsent != null) {
						System.out.println("    ** ValidUnsent Addresses");
						if (validUnsent != null) {
							for (int i = 0; i < validUnsent.length; i++)
								System.out.println("         "+validUnsent[i]);
						}
					}
					Address[] validSent = sfex.getValidSentAddresses();
					if (validSent != null) {
						System.out.println("    ** ValidSent Addresses");
						if (validSent != null) {
							for (int i = 0; i < validSent.length; i++)
								System.out.println("         "+validSent[i]);
						}
					}
				}
				System.out.println();
				if (ex instanceof MessagingException)
					ex = ((MessagingException)ex).getNextException();
				else
					ex = null;
			} while (ex != null);
		}
	}

}
