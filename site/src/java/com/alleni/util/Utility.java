/**
 * Copyright (c) 2000 by Molten Media International, Inc.  All rights reserved
 * File:
 * Purpose:
 * Author:    Alan Peters of Electric Pig for Molten Media International
 */
package com.alleni.util;

import java.io.*;
import java.net.URLDecoder;
import java.sql.*;
import java.util.*;
import java.util.regex.*;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.NumberFormat;

public class Utility
{
    public final static NumberFormat m_NumberFormat = NumberFormat.getCurrencyInstance();

    // base path of the webapp
    private static String sWebappBasePath;
    
    // the path to the log file.
    private static String sLogPath;
        
        // Database version number
        private static String strDatabaseVersion;
        
        // SVN revision number for this build
        private static String strSVNRevision;

    public static String getAgoString(long secondsAgo) {
        String  agoLabels[] = {"minute", "hour", "day", "month", "year"};
        int     agoValues[] = new int[agoLabels.length];

        agoValues[0] = (int)(secondsAgo / 60);                  // minute
        agoValues[1] = agoValues[0] / 60;                       // hour
        agoValues[2] = agoValues[1] / 24;                       // day
        agoValues[3] = (int)((float)agoValues[2] / 30.5);       // month
        agoValues[4] = agoValues[2] / 365;                      // year

        int     ii;

        for (ii = agoValues.length - 1; ii >= 0; ii--) {
            int     ago = agoValues[ii];

            if (ago > 1) {
                return ago + " " + agoLabels[ii] + "s ago";
            }
            if (ago == 1) {
                return "1 " + agoLabels[ii] + " ago";
            }
        }
        return "less than a minute ago";
    }

    // allows keys to have multiple values (e.g. ?foo=11&foo=12&foo=13)
    public static Map<String, List<String>> parseQueryString(String queryString) throws UnsupportedEncodingException {
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        for (String param : queryString.split("&")) {
            String[] pair = param.split("=");
            String key = URLDecoder.decode(pair[0], "UTF-8");
            String value = URLDecoder.decode(pair[1], "UTF-8");
            List<String> values = params.get(key);
            if (values == null) {
                values = new ArrayList<String>();
                params.put(key, values);
            }
            values.add(value);
        }
        return params;
    }

    // assumes that for each key there is only one value.
    public static Map<String, String> parseQueryStringSimple(String queryString) throws UnsupportedEncodingException {
        Map<String, String> params = new HashMap<String, String>();
        for (String param : queryString.split("&")) {
            String[] pair = param.split("=");
            String key = URLDecoder.decode(pair[0], "UTF-8");
            String value = URLDecoder.decode(pair[1], "UTF-8");
            params.put(key, value);
        }
        return params;
    }

    public static void appendNamedJSONValues(JSONObject dst, JSONObject src, String keys[]) throws JSONException {
        for (String key : keys) {
            dst.put(key, src.get(key));
        }
    }

    public static int cleanCurrencyString(String currency) {
        currency = currency.trim();

        if (currency.charAt(0) == '$') {
            currency = currency.substring(1);
        }
        if (currency.length() == 0) {
            return 0;
        }
        
        return Math.round(Float.parseFloat(currency) * 100);
    }

    public static boolean checkString( String str )
    {
        try
        {
            if ( str != null && str.length() > 0 )
                return true;
        }
        catch( Exception ignored )
        {
            ;
        }

        return false;
    }

    public static boolean compare( String str, StringBuffer sb )
    {
        try
        {
            if ( str.compareTo( sb.toString() ) == 0 )
                return true;
        }
        catch ( Exception ignored )
        {
            ;
        }

        return false;
    }

    public static String forceString( String str ) { return forceString( str, "" ); }
    public static String forceString( String str, String strDefault )
    {
        return str == null ? strDefault : str;
    }


    public static float parseFloat( String str ){ return parseFloat( str, -1 ); }
    public static float parseFloat( String str, float fDefault )
    {        
        log( "parseFloat str: " + str + "\r\n");
        
        float f = fDefault;
        try
        {
            f = new Float( str ).floatValue();
        }
        catch( Exception ignored )
        {
        }
        
        return f;
    }
    
    public static Float forceFloat( String str )
    {
        return forceFloat( str, -1 );
    }
    
    public static Float forceFloat( String str, float fDefault )
    {
        if ( !checkString( str ) )
            return new Float( fDefault );
        
        Float floatvalue;
        
        try
        {
            floatvalue = new Float( str );
        }
        catch( Throwable ignored )
        {
            ignored.printStackTrace();
            log( "\r\n forceFloat e: " + ignored.getMessage() );
            log( "\r\n forceFloat str: " + str );
            floatvalue = new Float( fDefault );
        }
        
        return floatvalue;
    }
    

        public static Double forceDouble( String str )
    {
        return forceDouble( str, -1 );
    }
    
    public static Double forceDouble( String str, double dblDefault )
    {
        if ( !checkString( str ) )
            return new Double( dblDefault );
        
        Double doublevalue;
        
        try
        {
            doublevalue = new Double( str );
        }
        catch( Throwable ignored )
        {
            ignored.printStackTrace();
            log( "\r\n forceDouble e: " + ignored.getMessage() );
            log( "\r\n forceDouble str: " + str );
            doublevalue = new Double( dblDefault );
        }
        
        return doublevalue;
    }
        
    public static int parseInt( String str ){ return parseInt( str, -1 ); }
    public static int parseInt( String str, int iDefault )
    {
        int i = iDefault;
        try
        {
            i = new Integer( str ).intValue();
        }
        catch( Exception ignored )
        {
        }

        return i;
    }

    public static Integer forceInteger( String str )
    {
        return forceInteger( str, -1 );
    }

    public static Integer forceInteger( String str, int iDefault )
    {
        if ( !checkString( str ) )
            return new Integer( iDefault );

        Integer integer;

        try
        {
            integer = new Integer( str );
        }
        catch( Throwable ignored )
        {
            ignored.printStackTrace();
            log( "\r\n forceInteger e: " + ignored.getMessage() );
            log( "\r\n forceInteger str: " + str );
            integer = new Integer( iDefault );
        }

        return integer;
    }

    public static Boolean forceBoolean( String str )
    {
        Boolean bool;
        try
        {
            bool = new Boolean( str );
        }
        catch( Throwable ignored )
        {
            bool = new Boolean( "false" );
        }

        return bool;
    }

    public static boolean parseBool( String str ){ return parseBool( str, false ); }
    public static boolean parseBool( String str, boolean bDefault )
    {
        boolean bRes;
        try
        {
            bRes = new Boolean( str ).booleanValue();
        }
        catch( Throwable ignored )
        {
            bRes = bDefault;
        }

        return bRes;
    }

    public static void setSmallInt(int nArg, short s, PreparedStatement ps )  throws SQLException
    {
        String strShort = new Short(s).toString() + "::smallint";
        ps.setObject( nArg, strShort, Types.SMALLINT );
    }

    public static void setVarChar(int nArg, String str, PreparedStatement ps )  throws SQLException
    {
        if ( Utility.checkString( str ) )
            ps.setString( nArg,str );
        else if ( str != null )
            ps.setString( nArg,str );
        else
            ps.setNull( nArg, Types.VARCHAR );
    }

    public static void setDate(int nArg, java.sql.Date date, PreparedStatement ps )  throws SQLException
    {
        if ( date != null )
            ps.setDate( nArg, date );
        else
            ps.setNull( nArg, Types.DATE );
    }

    public static void setBit(int nArg, Boolean bool, PreparedStatement ps )  throws SQLException
    {
        if ( bool != null )
            ps.setObject( nArg, new Integer(bool.booleanValue()?1:0), Types.BIT );
        else
            ps.setNull( nArg, Types.BIT );
    }

    public static boolean setOutParam( int nArg, int iType, PreparedStatement ps )  throws SQLException
    {
         CallableStatement cs;
         boolean bRes;
         try
         {
             cs = (CallableStatement)ps;
             cs.registerOutParameter( nArg, iType );
             bRes = true;
         }
         catch( ClassCastException x )
         {
             bRes = false;
         }

         return bRes;
    }

    public static String buildQueryParam( String strName, String strValue )
    {
        StringBuffer sbParam = new StringBuffer( 256 );

        sbParam.append( "&" );
        sbParam.append( strName );
        sbParam.append( "=" );
        sbParam.append( strValue );

        return sbParam.toString();
    }
        
    public static void setDatabaseVersion(String strVersion)
    {
        strDatabaseVersion = new String(strVersion);
    }
    
    public static String getDatabaseVersion()
    {
        
        if (strDatabaseVersion == null) strDatabaseVersion = new String();
            
        return strDatabaseVersion;
    }
    
    public static String getSVNRevision() 
    {
        if (strSVNRevision == null || strSVNRevision.length() == 0) {
            strSVNRevision = new String();
            
            try {
                ResourceBundle rb = ResourceBundle.getBundle("svn");
                
                // strip off ending M and/or S characters
                Pattern p = Pattern.compile("([^MS]+)[MS]+$");
                Matcher m = p.matcher(rb.getString("svnrevision"));
                
                if (m.find()) { strSVNRevision = m.group(1); }
            } catch (Exception e) {
                log("Utility.getSVNRevision ERROR: "+e.getMessage());
            }            
        }
        
        return strSVNRevision;
    }
    
    public static String getWebappBasePath()
    {
        if (sWebappBasePath == null) {
            try {
                sWebappBasePath = AppProperties.getProperties().getProperty("Utility.WebappBasePath", "/");
            } catch(Exception x) {
                System.err.println("Utility.getWebappBasePath: Exception: " + x.toString());
            }
        }        
        return sWebappBasePath;
    }

    public static void log( String str )
    {
        System.out.println(str);
    }

    public static void deleteRsEnum( Enumeration enumRS )
    {
        try
        {
            ((IResultSetEnumeration)enumRS).cleanup();
        }
        catch ( Exception x )
        {
            x.printStackTrace();
        }
    }

    public static String getStackTrace()    // gets a stack trace
    {
        Throwable t = new Throwable();        // for getting stack trace
        ByteArrayOutputStream os = new ByteArrayOutputStream();    // for storing stack trace
        PrintWriter  pw = new PrintWriter (os);// printing destination
        t.printStackTrace(pw);
        return os.toString();
    }


    public static int CalendarMonth2IntegerMonth( int CalendarMonth )
    {
        int iIntegerMonth;

        switch ( CalendarMonth ) {
            case Calendar.JANUARY: iIntegerMonth = 1; break;
            case Calendar.FEBRUARY: iIntegerMonth = 2; break;
            case Calendar.MARCH: iIntegerMonth = 3; break;
            case Calendar.APRIL: iIntegerMonth = 4; break;
            case Calendar.MAY: iIntegerMonth = 5; break;
            case Calendar.JUNE: iIntegerMonth = 6; break;
            case Calendar.JULY: iIntegerMonth = 7; break;
            case Calendar.AUGUST: iIntegerMonth = 8; break;
            case Calendar.SEPTEMBER: iIntegerMonth = 9; break;
            case Calendar.OCTOBER: iIntegerMonth = 10; break;
            case Calendar.NOVEMBER: iIntegerMonth = 11; break;
            case Calendar.DECEMBER: iIntegerMonth = 12; break;
            default: iIntegerMonth = 1; break;
        }
        return iIntegerMonth;
    }

    public static int IntegerMonth2CalendarMonth( int IntegerMonth )
    {
        int iCalendarMonth;

        switch ( IntegerMonth ) {
            case 1: iCalendarMonth = Calendar.JANUARY; break;
            case 2: iCalendarMonth =  Calendar.FEBRUARY; break;
            case 3: iCalendarMonth =  Calendar.MARCH; break;
            case 4: iCalendarMonth =  Calendar.APRIL; break;
            case 5: iCalendarMonth =  Calendar.MAY; break;
            case 6: iCalendarMonth =  Calendar.JUNE; break;
            case 7: iCalendarMonth =  Calendar.JULY; break;
            case 8: iCalendarMonth =  Calendar.AUGUST; break;
            case 9: iCalendarMonth =  Calendar.SEPTEMBER; break;
            case 10: iCalendarMonth =  Calendar.OCTOBER; break;
            case 11: iCalendarMonth =  Calendar.NOVEMBER; break;
            case 12: iCalendarMonth =  Calendar.DECEMBER; break;
            default: iCalendarMonth = Calendar.JANUARY; break;
        }
        return iCalendarMonth;
    }

    public static String CalendarMonth2MonthString( int CalendarMonth ) {
        String strMonth;

        switch ( CalendarMonth ) {
            case Calendar.JANUARY: strMonth = "January"; break;
            case Calendar.FEBRUARY: strMonth = "February"; break;
            case Calendar.MARCH: strMonth = "March"; break;
            case Calendar.APRIL: strMonth = "April"; break;
            case Calendar.MAY: strMonth = "May"; break;
            case Calendar.JUNE: strMonth = "June"; break;
            case Calendar.JULY: strMonth = "July"; break;
            case Calendar.AUGUST: strMonth = "August"; break;
            case Calendar.SEPTEMBER: strMonth = "September"; break;
            case Calendar.OCTOBER: strMonth = "October"; break;
            case Calendar.NOVEMBER: strMonth = "November"; break;
            case Calendar.DECEMBER: strMonth = "December"; break;
            default: strMonth = "January"; break;
        }
        return strMonth;

    }

    public static String IntegerMonth2MonthString( int IntegerMonth ) {
        String strMonth;

        switch ( IntegerMonth ) {
            case 1: strMonth = "January"; break;
            case 2: strMonth = "February"; break;
            case 3: strMonth = "March"; break;
            case 4: strMonth = "April"; break;
            case 5: strMonth = "May"; break;
            case 6: strMonth = "June"; break;
            case 7: strMonth = "July"; break;
            case 8: strMonth = "August"; break;
            case 9: strMonth = "September"; break;
            case 10: strMonth = "October"; break;
            case 11: strMonth = "November"; break;
            case 12: strMonth = "December"; break;
            default: strMonth = "January"; break;
        }
        return strMonth;

    }


    public static String IntegerMonth2MonthAbbreviationString( int IntegerMonth ) {
        String strMonth;

        switch ( IntegerMonth ) {
            case 1: strMonth = "Jan"; break;
            case 2: strMonth = "Feb"; break;
            case 3: strMonth = "Mar"; break;
            case 4: strMonth = "Apr"; break;
            case 5: strMonth = "May"; break;
            case 6: strMonth = "Jun"; break;
            case 7: strMonth = "Jul"; break;
            case 8: strMonth = "Aug"; break;
            case 9: strMonth = "Sep"; break;
            case 10: strMonth = "Oct"; break;
            case 11: strMonth = "Nov"; break;
            case 12: strMonth = "Dec"; break;
            default: strMonth = "Jan"; break;
        }
        return strMonth;

    }




    public static int IntegerYear2DateYear( int iIntegerYear )
    {
        int iDateYear = iIntegerYear - 1900;

        return iDateYear;
    }

    public static int DateYear2IntegerYear( int iDateYear )
    {
        int iIntegerYear = iDateYear + 1900;

        return iIntegerYear;
    }






    // The encodeHTMLString method substitutes problem characters
    // for their HTML coded equivalents.  This function can be
    // expanded as other problem characters are identified.
    // Compare this function to encodeHTMLQuoteString.  encodeHTMLQuoteString should
    // be used when we want to honor HTML tags in the data.  encodeHTMLString
    // should be used when we want to display text exactly as it comes from the
    // database.
    // The following substitutions are done:
    //
    //  char    HTML equivalent
    //  '        &#039;
    //  "        &#034;
    //  <        &#060;
    //  >        &#062;
    //  &        &#038;
        //
        //
        //
        // JJC: 09272006 - add support to convert curly quotes and m-dash unicode chars.
        //
        // UNICODE see: http://www.alanwood.net/unicode/general_punctuation.html
        // or http://www.cs.sfu.ca/~ggbaker/reference/characters/
        // character "calculator" :  http://www.cs.tut.fi/cgi-bin/run/~jkorpela/char.cgi?code=201D
        // *** UNICODE CHART: http://www.unicode.org/charts/PDF/U2000.pdf
        // &mdash;
        // 
    public static String encodeHTMLString( String strIn ) {

            if (strIn != null) {

                // We know that the StringBuffer will need to be at least strIn.length()
                // so we make it a bit longer to reduce the likelihood of StringBuffer
                // having to allocate more capacity.
                
                StringBuffer sbOut = new StringBuffer( strIn.length() * 2 );

                int i = 0;
                char c = 0;

                // Iterate through strIn and generate sbOut
                for (i=0; i < strIn.length(); ++i) {
                    c = strIn.charAt(i);
                    if (c == '\'') {
                        sbOut.append("&#039;");
                    } else if (c == '"') {
                        sbOut.append("&#034;");
                    } else if (c == '<') {
                        sbOut.append("&#060;");
                    } else if (c == '>') {
                        sbOut.append("&#062;");
                    } else if (c == '&') {
                        sbOut.append("&#038;");
                    } else if ( 127 < (int)c) { // treat all chars above 7 bits as unicode
                        sbOut.append("&#" + (int)c + ";");
                    } else {
                        sbOut.append(c); // do not encode anything else
                    }
                }

                return sbOut.toString();
            }

            return new String();
    }


    // The encodeHTMLQuoteString method substitutes single and double quotes
    // for their HTML coded equivalents.
    // Compare this function to encodeHTMLString.  encodeHTMLQuoteString should
    // be used when we want to honor HTML tags in the data.  encodeHTMLString
    // should be used when we want to display text exactly as it comes from the
    // database.
    // The following substitutions are done:
    //
    //  char    HTML equivalent
    //  '        &#039;
    //  "        &#034;
    public static String encodeHTMLQuoteString( String strIn ) {

        if (strIn != null) {

            // We know that the StringBuffer will need to be at least strIn.length()
                        // so we make it a bit longer to reduce the likelihood of StringBuffer
                        // having to allocate more capacity.

                        StringBuffer sbOut = new StringBuffer( strIn.length() * 2 );

            int i = 0;
            char c = 0;

            // Iterate through strIn and generate sbOut
            for (i=0; i < strIn.length(); ++i) {
                c = strIn.charAt(i);
                if (c == '\'') {
                    sbOut.append("&#039;");
                } else if (c == '"') {
                    sbOut.append("&#034;");
                } else {
                    sbOut.append(c);
                }
            }

            return sbOut.toString();
        }

        return new String();
    }


    // The encodeHTMLWhiteSpace method substitutes "white characters"
    // like TAB, RETURN, nad space for their HTML coded equivalents.
    // The following substitutions are done:
    //
    //  char    HTML equivalent
    //  space    &nbsp;
    //    tab        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    //  return    <BR>
    //  Note: Windows will typically put a return and linefeed at the end of a line
    //        Macintosh will only put the return
    public static String encodeHTMLWhiteSpace( String strIn )
    {
                // We know that the StringBuffer will need to be at least strIn.length()
                // so we make it a bit longer to reduce the likelihood of StringBuffer
                // having to allocate more capacity.

                StringBuffer sbOut = new StringBuffer( strIn.length() * 2 );
                
        int i = 0;
        char charIn = 0;
        String strCharReplaceOut = "";

        if (strIn != null)
        {
            // Iterate through strIn and generate sbOut
            for (i=0; i < strIn.length(); ++i)
            {
                charIn = strIn.charAt(i);
                if (charIn == ' ')
                {
                    // If previous char was a space and was replaced by a &nbsp
                    // then let the space pass thru to the output
                    // We just want to avoid two regular spaces in a row
                    // because browser will not honor
                    if (strCharReplaceOut == " ")
                    {
                        strCharReplaceOut = "&nbsp;";
                    }
                    else
                    {
                        strCharReplaceOut = " ";
                    }
                }
                else if (charIn == '\t')
                {
                    strCharReplaceOut = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                }
                else if (charIn == '\r' || charIn == '\n')
                {
                    strCharReplaceOut = "<BR>";
                }
                else
                {
                    strCharReplaceOut = "" + charIn;
                } //end if else

                sbOut.append(strCharReplaceOut);

            } //end for
        }    // end if
        return sbOut.toString();
    }

    // The encodeJavaScriptString method substitutes
    // like TAB, RETURN, for their escaped equivalents.

    //  Note: Windows will typically put a return and linefeed at the end of a line
    //        Macintosh will only put the return
    public static String encodeJavaScriptString( String strIn )
    {
        // escape backslash character MUST be done first
        strIn =  strIn.replaceAll("\\\\","\\\\\\\\");
        // escape single quotes
        strIn = strIn.replaceAll("\'","\\\\'");
        // escape double quotes
        strIn =  strIn.replaceAll("\"","\\\\\"");
        // escape returns and new lines
        strIn =  strIn.replaceAll("\r","\\\\r").replaceAll("\n","\\\\n");
        // escape tabs
        strIn =  strIn.replaceAll("\t","\\\\t");

        return strIn;
    }


    // <a href="article://31">
    // <a href="article.jsp?Link2CareArticleId=31">
    public static String encodeAnchorTag( String strIn ) {

        String strCompare = "article://";
        String strReplace = "article.jsp?Link2CareArticleId=";
        int iEditIndex = 0;
        boolean bAnchorToken = false;
        boolean bHrefToken = false;
        boolean bEqualsToken = false;
        StringBuffer sbRecent = new StringBuffer();

        if (strIn != null) {

            // We know that the StringBuffer will need to be at least strIn.length()
                        // so we make it a bit longer to reduce the likelihood of StringBuffer
                        // having to allocate more capacity.
                    
            StringBuffer sbOut = new StringBuffer( strIn.length() * 2 );

            int i = 0;
            char c = 0;

            // Iterate through strIn and generate sbOut
            for (i=0; i < strIn.length(); ++i) {
                c = strIn.charAt(i);
                sbRecent.append(c);

                //if ( bAnchorToken && sbRecent.toString().toLowerCase().compareTo("href") == 1 ) {
                if ( sbRecent.toString().toLowerCase().compareTo("href") == 1 ) {
                    bHrefToken = true;
                    sbOut.append( sbRecent.toString() );
                    sbOut.append( "|" );
                    sbRecent.setLength( 0 );
                }
                //else if ( bAnchorToken && bHrefToken && sbRecent.toString().toLowerCase().compareTo("=") == 1 ) {
                else if ( sbRecent.toString().toLowerCase().compareTo("=") == 1 ) {
                    bEqualsToken = true;
                    sbOut.append( sbRecent.toString() );
                    sbOut.append( "|" );
                    sbRecent.setLength( 0 );
                }
                else if ( sbRecent.toString().toLowerCase().compareTo("<a ") == 1 ) {
                    bAnchorToken = true;
                    sbOut.append( sbRecent.toString() );
                    sbOut.append( "|" );
                    sbRecent.setLength( 0 );
                }
                //else if ( bAnchorToken && bHrefToken && bEqualsToken && sbRecent.toString().toLowerCase().compareTo( strCompare ) == 1 ) {
                else if ( sbRecent.toString().toLowerCase().compareTo( strCompare ) == 1 ) {
                    sbOut.append( strReplace );
                    sbOut.append( "|" );
                    //if ( bAnchorToken ) { sbOut.append( "t" ); } else { sbOut.append( "f" ); }
                    //if ( bHrefToken ) { sbOut.append( "t" ); } else { sbOut.append( "f" ); }
                    //if ( bEqualsToken ) { sbOut.append( "t" ); } else { sbOut.append( "f" ); }
                    sbRecent.setLength( 0 );
                }

                if ( c == ' ' || c == '"' || c == '\'' ) {
                    sbOut.append( sbRecent.toString() );
                    sbOut.append( "|" );
                    sbRecent.setLength( 0 );
                }
                else if (c == '>') {
                    bAnchorToken = true;
                    bHrefToken = false;
                    bEqualsToken = false;

                    sbOut.append( sbRecent.toString() );
                    sbOut.append( "|" );
                    sbRecent.setLength( 0 );
                }


/*
                if (c == ' ') {
                    if ( sbRecent.toString().toLowerCase().compareTo( "<a" ) == 1 ) {
                        if ( !bAnchorToken ) {
                            bAnchorToken = true;
                            bHrefToken = false;
                            bEqualsToken = false;
                            sbOut.append(" tff ");
                        }
                    }
                    else if ( sbRecent.toString().toLowerCase().compareTo( "href" ) == 1 ) {
                        if ( bAnchorToken ) {
                            bHrefToken = true;
                            sbOut.append(" ttf ");
                        }
                    }
                    else if ( sbRecent.toString().toLowerCase().compareTo( "=" ) == 1 ) {
                        if ( bAnchorToken && bHrefToken ) {
                            bEqualsToken = true;
                            sbOut.append(" ttt ");
                        }
                    }
                    sbOut.append( sbRecent.toString() );
                    sbOut.append( "|" );
                    // is there a better way to truncate a StringBuffer?
                    sbRecent.setLength( 0 );
                }
                else if (c == '>') {
                        if ( bAnchorToken ) {
                            bAnchorToken = false;
                            bHrefToken = false;
                            bEqualsToken = false;
                            sbOut.append(" fff ");
                        }
                }
                sbRecent.append(c);

                if ( bAnchorToken && bHrefToken && bEqualsToken ) {

                    iEditIndex = sbRecent.toString().indexOf( strCompare );

                    if ( iEditIndex >= 0 ) {
                        //sbRecent.setLength ( sbRecent.length() - strCompare.length() );
                        sbRecent.append ( strReplace );
                    }
                }
*/

            }

            sbOut.append( sbRecent.toString() );



            return sbOut.toString();
        }

        return new String();
    }




    public static String encodeMailToText(String strMailToString)
    {
        // Strip out all Html...
        //
        strMailToString = strMailToString.replaceAll("<(.|\n)*?>","");

        // Replace all Html escape decimal encodings with spaces...
        //
        strMailToString = strMailToString.replaceAll("&#([0-9]{3,4});"," ");

        // Decode all remaining embedded Html escapes. Java and RegEx do not support
        // a decimal escape character literal (only octal, hex, and unicode...also hex)
        // so we need to do some additional processing to convert Html decimal encodings
        // into actual characters.
        //
        // NOTES:
        // (1) Need to screen for alpha versions of these separately like &nbps;
        //     and &amp;.
        // (2) Need to complete conversion to char so that URLEncoder.encode()
        //     can do its thing. Alternately, might just put in the %hex codes
        //     here, then run URLEncoder.decode() to make these into chars, then
        //     to the additional processing below, and then run URLEncoder.encode()
        //     as a last step. (So use decode() first and then encode() to get it
        //     all right.)
        /*
        Pattern pHtmlEncodings = Pattern.compile("&#([0-9]{3,4});");
        Matcher m = pHtmlEncodings.matcher(strMailToString);

        int iEndLast = 0;
        String strMailToStringNew = "";

        int cnt = 0;
        while ( m.find() ) {
            cnt++;
            int i_m_start = m.start();
            int i_m_end = m.end();
            String strHex = Integer.toHexString(Integer.parseInt(m.group(1)));
            switch( strHex.length() )
            {
                case 1:
                    strHex = "%0" + strHex;
                    break;
                case 2:
                    strHex = "%" + strHex;
                    break;
                case 3:
                    strHex = "%u0" + strHex;
                    break;
                case 4:
                    strHex = "%u" + strHex;
                    break;
                default:
                    strHex = "";
                    break;
            }
            strMailToStringNew += strMailToString.substring(iEndLast,i_m_start) + strHex;
            iEndLast = i_m_end;
        }
        strMailToStringNew += strMailToString.substring(iEndLast); // get any remaining ending
        strMailToString = strMailToStringNew;
        */

        // Encode to survive the mailto URL...
        //
        try {
            strMailToString = URLEncoder.encode(strMailToString,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            log("Utility.encodeMailToText: UnsupportedEncodingException: " + e.getMessage());
        }

        // Convert all the URLEncoder.encode() "+" spaces to javascript %20 spaces so they
        // will appear as spaces (and not "+" chars) in the Email. Do same for title in the
        // anchor below.
        //
        // NOTE: need double-backslash so that one will survive the string to backslash-escape
        // the "+" in the RegEx.
        //
        strMailToString = strMailToString.replaceAll("\\+","%20");

        return strMailToString;
    }


}




