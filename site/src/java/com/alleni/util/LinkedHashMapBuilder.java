package com.alleni.util;

import java.util.LinkedHashMap;

public class LinkedHashMapBuilder<K, V> {
    private LinkedHashMap<K, V> map;

    public LinkedHashMapBuilder() {
        map = new LinkedHashMap<K, V>();
	}

	public LinkedHashMapBuilder<K, V> add(final K key, final V value) {
		map.put(key, value);
		return this;
	}

	public LinkedHashMap<K, V> build() {
		return map;
	}
}
