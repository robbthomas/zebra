package com.alleni.util;

import java.util.*;

public class RandomPassword
{
	// Characters to use
	// Exlcude the following to avoid confusion: numbers 1, 5, 8, 0 and letters Q, I, O, S, D, J, l, Z, B
	public static final String [] k_astrChars = {   "A","C","E","F","G","H","K","L","M","N","P","R","T","U","V","W","X","Y",
                                                        "2","3","4","6","7","9",
                                                        "a","b","c","d","e","f","g","h","i","j","k","m","n","o","p","q","r","s","t","u","v","w","x","y","z" };
	// number of letters in password
	public static final int k_nPasswordChars = 8;

	// seed and generator
	private static long		m_lLastSeed = -1;
	private static Random	m_rnum = null;

    // generate random sequence with default length and charset
	public static synchronized String generate()
	{
        return generate(k_nPasswordChars,k_astrChars);
    }
    
    // generate random sequence using arbitrary length and default charset
	public static synchronized String generate(int length)
	{
        return generate(length,k_astrChars);
    }
    
    // generate random sequence using arbitrary length and charset as string
	public static synchronized String generate(int length, String strChars)
	{
        if (strChars == null || strChars.length() == 0) {
            return generate(length);
        }
        
        // null string regular expression should split out every character
        String[] astrChars = strChars.split("");
        return generate(length,astrChars);
    }
    
    // generate random sequence using arbitrary length and charset
	public static synchronized String generate(int length, String[] astrChars)
	{
		//
		// if we're using the same seed, use the next random numbers in the sequence
		// so that we don't generate the same passwords if we're called several times
		// in one ms
		//
		long lSeed = System.currentTimeMillis();
		if ( lSeed != m_lLastSeed )
			m_rnum	= new java.util.Random( lSeed );

		m_lLastSeed = lSeed;

		//
		// convert random doubles into password characters
		//
		StringBuffer	sbPass	= new java.lang.StringBuffer();
		int				iCoeff	= astrChars.length;
		int				iIndex	= 0;

		for ( int i = 0; i < length; i++ )
		{
			iIndex = new Double( m_rnum.nextDouble() * iCoeff ).intValue();
			sbPass.append( astrChars[iIndex] );
		}

		return sbPass.toString();
	}
}