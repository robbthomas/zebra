package com.alleni.util;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class ContactPreferenceType extends BaseBean implements Serializable
{
    static private ContactPreferenceTypeDao _dao() { return new ContactPreferenceTypeDao(); }

    protected ContactPreferenceType(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("contactpreferencetypeid" ) ? daoJson.getInt("contactpreferencetypeid") : -1);
        this.jsonValues.put("tag", daoJson.has("tag") ? daoJson.getString("tag") : "");
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("displayOrder", daoJson.has("displayorder") ? daoJson.getInt("displayorder") : -1);
        this.jsonValues.put("created", daoJson.has("createddatetime") ? (Timestamp)daoJson.get("createddatetime") : new Timestamp(0));
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("CONTACTPREFERENCETYPE.create - could not create ContactPreferenceTypes record");

        return success("contactPreferenceTypeId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("CONTACTPREFERENCETYPE.update - could not update ContactPreferenceTypes record");

        return success("contactPreferenceTypeId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("CONTACTPREFERENCETYPE.retire - could not retire ContactPreferenceTypes record");

        return success("contactPreferenceTypeId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("contactPreferenceTypeId");
        ArrayList contactPreferenceTypesList = _dao().find(id, credentialValues);

        if (contactPreferenceTypesList.size() < 1)
            return error("CONTACTPREFERENCETYPE.find - could not find ContactPreferenceTypes id "+id);
        if (contactPreferenceTypesList.size() > 1)
            return error("CONTACTPREFERENCETYPE.find - found multiple ContactPreferenceTypes records for ContactPreferenceTypes id "+id);

        ContactPreferenceType contactPreferenceTypes = new ContactPreferenceType((JSONObject)contactPreferenceTypesList.get(0));
        if (contactPreferenceTypes == null)
            return error("CONTACTPREFERENCETYPE.find - contactPreferenceTypes==null for id "+id);

        return success(contactPreferenceTypes.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList contactPreferenceTypesList = _dao().findAll(credentialValues);
        return jsonFoundList(contactPreferenceTypesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of ContactPreferenceTypes json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList contactPreferenceTypesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(contactPreferenceTypesList, limit);

        JSONObject jsonList = jsonFoundList(contactPreferenceTypesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            ContactPreferenceType contactPreferenceTypes = new ContactPreferenceType((JSONObject)foundList.get(j));
            if (contactPreferenceTypes == null)
                return JsonError.jsonError(JsonError.Error.CONTACTPREFERENCETYPE_FIND, "Could not make ContactPreferenceTypes object " + j);
            JSONObject json = new JSONObject(contactPreferenceTypes.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("contactPreferenceTypes", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        CONTACTPREFERENCETYPE_CREATE,
        CONTACTPREFERENCETYPE_RETIRE,
        CONTACTPREFERENCETYPE_FIND,
        CONTACTPREFERENCETYPE_UPDATE,

 */