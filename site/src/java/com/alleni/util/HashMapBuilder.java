package com.alleni.util;

import java.util.HashMap;

public class HashMapBuilder<K, V> {
    private HashMap<K, V> map;

    public HashMapBuilder() {
        map = new HashMap<K, V>();
	}

	public HashMapBuilder<K, V> add(final K key, final V value) {
		map.put(key, value);
		return this;
	}

	public HashMap<K, V> build() {
		return map;
	}
}
