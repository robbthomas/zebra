/**
 * Copyright (c) 2001 by Molten Media International, Inc.  All rights reserved
 * File:    IResultSetEnumeration.java
 * Purpose: Definition of IResultSetEnumeration interface.
 * Author:    Alan Peters of Electric Pig for Molten Media International, Inc
 */
package com.alleni.util;

import java.util.*;
import java.sql.*;


public interface IResultSetEnumeration extends Enumeration
{
    public void setResultSet( ResultSet rs );
    public ResultSet getResultSet( );
    public void setPreparedStatement( PreparedStatement ps );
    public PreparedStatement getPreparedStatement();
    public void setMoreElements( boolean bMore );
    public void setNextElement( IStoProContainer spo );
    public IStoProContainer getNextElement( );
    public StringBuffer getProcName();
    public void setProcName( StringBuffer sbProcName );

    public void cleanup(); // close connection, free resources

    // --o enumeration (standard interface listed here for reference)
    public boolean hasMoreElements();
    public Object nextElement();
}