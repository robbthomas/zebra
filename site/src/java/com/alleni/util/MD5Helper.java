/**
 *
 */
package com.alleni.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class that provides helper methods for generation of MD5 hashes
 *
 * @author Aleksandar Miljusevic
 *
 */
public class MD5Helper {

    /**
     * Default constructor
     */
    public MD5Helper() {
    }

    /**
     * Creates MD5 hash for each string in the input array.
     *
     * @param source
     *            Array of strings to be hashed
     *
     * @return Array that contains an MD5-hashed string for each member of the
     *         original array
     */
    public String[] generateHash(String[] source) {

        String[] md5Values = null;

        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");

            String md5Value = "";
            int count = 0;
            md5Values = new String[source.length];

            for (String hashElement : source) {
                byte[] defaultBytes = hashElement.getBytes();

                algorithm.reset();
                algorithm.update(defaultBytes);

                byte messageDigest[] = algorithm.digest();
                StringBuffer md5Hex = new StringBuffer();

                for (int i = 0; i < messageDigest.length; i++) {
                    String hex = Integer.toHexString(0xFF & messageDigest[i]);
                    if (hex.length() == 1) {
                        md5Hex.append('0');
                    }

                    md5Hex.append(hex);

                }

                md5Value = md5Hex.toString();
                md5Values[count] = md5Value;
                count++;

                // TODO: Use log4j if it's configured/usable
                //Utility.log("MD5 (" + hashElement + "): " + md5Value);
            }
        } catch (NoSuchAlgorithmException nsae) {
            // TODO: Use log4j if it's configured/usable
            Utility.log("Cannot find digest algorithm");
        }

        return md5Values;
    }

    /**
     * Creates MD5 hash for the source string.
     *
     * @param source
     *            String to be hashed
     *
     * @return MD5-hashed string
     */
    public String generateHash(String source) {

        String md5Value = null;

        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            md5Value = generateHash(source, algorithm);

            // TODO: Use log4j if it's configured/usable
            //Utility.log("MD5 (" + source + "): " + md5Value);

        } catch (NoSuchAlgorithmException nsae) {
            // TODO: Use log4j if it's configured/usable
            Utility.log("Cannot find digest algorithm");
        }

        return md5Value;
    }

    /**
     * Creates MD5 hash for the source string using the specified instance of
     * the MD5 algorithm.
     *
     * @param source
     *            String to be hashed
     *
     * @param algorithm
     *            MD5 algorithm to use
     *
     * @return MD5-hashed string
     */
    private String generateHash(String source, MessageDigest algorithm) {

        String md5Value = "";

        byte[] defaultBytes = source.getBytes();
        algorithm.reset();
        algorithm.update(defaultBytes);
        byte messageDigest[] = algorithm.digest();
        StringBuffer md5Hex = new StringBuffer();

        for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xFF & messageDigest[i]);
            if (hex.length() == 1) {
                md5Hex.append('0');
            }
            md5Hex.append(hex);
        }

        md5Value = md5Hex.toString();
        // TODO: Use log4j if it's configured/usable
        Utility.log("MD5 (" + source + "): " + md5Value);

        return md5Value;
    }

    public static void main(String[] args) {

        String[] hashes = new MD5Helper().generateHash(args);
        if (hashes != null) {
            for (int i = 0; i < hashes.length; i++) {
                System.out.println("MD5 hash [" + args[i] + "]: " + hashes[i]);
            }
        }
    }
}
