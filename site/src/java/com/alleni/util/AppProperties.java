/**
 * Copyright (c) 2001 by Molten Media International, Inc.  All rights reserved
 * File:
 * Purpose:
 * Author:    Alan Peters of Electric Pig for Molten Media International (Quality Process)
 */

package com.alleni.util;

import java.io.*;
import java.util.*;
import javax.servlet.*;

public class AppProperties
{
    private static AppProperties s_Properties;
    private static boolean s_Initialized = false;
    private static Properties s_PropData;
    private static ServletContext s_Context;
    private static String s_PathStr;

    // USE ONE OF THE getProperties METHODS TO GET PROPERTIES - do not new up your own.
    public static AppProperties getProperties()
    {
        init();
        return s_Properties;
    }

    public static AppProperties getProperties(ServletContext context)
    {
        init(context);
        return s_Properties;
    }


    public static AppProperties getProperties(String pathToPropsFile)
    {
        init(pathToPropsFile);
        return s_Properties;
    }

    public static void init(ServletContext context)
    {
        s_Context = context;
        init();
    }

    public static void init(String pathToPropsFile)
    {
        s_PathStr = pathToPropsFile;
        init();
    }

    public static void init()
    {
        if (s_Properties == null)
        {
            s_Properties = new AppProperties();

            try
            {
                if (s_Context != null)
                {
                    initProperties(s_Context);

                    // if we have gotten this far, we have successfully loaded the properties given.
                    s_Initialized = true;

                }
                else if (s_PathStr != null)
                {
                    s_PropData.load( new BufferedInputStream( new FileInputStream( s_PathStr ) ) );

                    // if we have gotten this far, we have successfully loaded the properties given.
                    s_Initialized = true;
                }

            }
            catch(Exception x)
            {
                Utility.log( "Unable to load AppParams " + x.getMessage() + "\r\n" );
                Utility.log( Utility.getStackTrace() );
            }

        }
    }


    public AppProperties()
    {
        s_PropData = new Properties();
    }


    //Searches for the property with the specified key in this property list.
    public String getProperty(String key, String defaultValue)
        throws Exception
    {
        //###ASJ:TB 272 what is the error strategy?
        if (!s_Initialized)
            throw new Exception("AppProperties failed: getProperty: " + key);

        return s_PropData.getProperty(key, defaultValue);
    }


    // Sets values into the properties
    public Object setProperty(String key, String value)
        throws Exception
    {
        //###ASJ:TB 272 what is the error strategy?
        if (!s_Initialized)
            throw new Exception("AppProperties failed: setProperty: " + key + "," + value);

        return s_PropData.setProperty(key, value);
    }

    // Loads the properties from a servlet context
    private static void initProperties(ServletContext context)
    {
        Enumeration paramNames;
        String keyName;

             for (paramNames = context.getInitParameterNames(); paramNames.hasMoreElements() ;)
        {
            keyName = (String)paramNames.nextElement();
                    s_PropData.setProperty(keyName, context.getInitParameter(keyName));
                }

    }

}

