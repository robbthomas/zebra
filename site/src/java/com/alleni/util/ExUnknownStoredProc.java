/**
 * Copyright (c) 2000 by Molten Media International, Inc.  All rights reserved
 * File:    ExUnknownStoredProc.java
 * Purpose: Definition of the ExUnknownStoredProc class. ExUnknownStoredProc is
 *            an exception thrown by classes that support IPxStoredProcedureObject.
 *            It is thrown when an object does support the requested operation
 *            within the context of the designated procedure
 * Author:    Alan Peters of Electric Pig for Molten Media International
 */
package com.alleni.util;

public class ExUnknownStoredProc extends Exception
{
    ExUnknownStoredProc()
    {
        super( "Unknown Stored Procedure" );
    }
    ExUnknownStoredProc( StringBuffer sbProcName )
    {
        super( "Unknown Stored Procedure " + sbProcName.toString() );
    }
}
