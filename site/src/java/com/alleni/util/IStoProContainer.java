/**
 * Copyright (c) 2000 by Molten Media International, Inc.  All rights reserved
 * File:    IStoProContainer.java
 * Purpose: Definition of the IStoProContainer interface. IStoProContainer
 *            should be implemented by classes that model a data object present in the database
 *            for which there is at least one stored procedure.  Often, a class that implements
 *            this interface is a model of a single table within a particular schema in the
 *            database.  This is not necessarily the case, however, so no implicit association
 *            to a particular table should be assumed.
 * Author:    Alan Peters of Electric Pig for Molten Media International
 */
package com.alleni.util;

import java.sql.*;

public interface IStoProContainer
{
    // check if this object knows about this procedure
    boolean    isAssociated( StringBuffer sbProcName );

    // query<->object data transfer
    void toPreparedStatement( PreparedStatement ps,
                              StringBuffer sbProcName,
                              Object obParam,
                              int nArgs,
                              String dbms) throws SQLException, ExUnknownStoredProc;
    void fromResultSet( ResultSet rs, StringBuffer sbProcName ) throws SQLException, ExUnknownStoredProc;
    void fromCallableStatement( CallableStatement cs, StringBuffer sbProcName ) throws SQLException, ExUnknownStoredProc;
    boolean fromPreparedStatement( int iResultValue, StringBuffer sbProcName ) throws SQLException, ExUnknownStoredProc;

    public IStoProContainer duplicate( );
}
