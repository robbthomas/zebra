package com.alleni.util;

import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONHelper
{
    // Merge all the [key,value] pairs from the src object into the dst object
    // and return the dst object. Use the "accumulate" method so multilple values
    // for the same key are kept in a JSONArray instead of replacing previous ones.
    public static JSONObject merge(JSONObject jsonDst, JSONObject jsonSrc) throws JSONException
    {
        for (Iterator itKey = jsonSrc.keys(); itKey.hasNext();)
        {
            String key = (String) itKey.next();

            // Get the value from the source and destination objects.
            Object srcValue = jsonSrc.get(key);
            Object dstValue = jsonDst.opt(key);

            if (dstValue == null)
            {
                // dstValue doesn't exist. just put the srcValue in.
                jsonDst.put(key, srcValue);
            }
            else if (srcValue instanceof JSONArray)
            {
                // Adding a JSONArray to an existing object. Replace the dstValue
                // with a JSONArray if it isn't already one.
                JSONArray dstArray = (dstValue instanceof JSONArray) ? (JSONArray)dstValue : new JSONArray(dstValue);
                JSONArray srcArray = (JSONArray)srcValue;

                // Add the src elements to the end of the dst array.
                for (int j=0; j < srcArray.length(); j++)
                {
                    dstArray.put(srcArray.get(j));
                }
            }
            else
            {
                // append the srcValue. if dstValue already exists, puts both into a JSONArray.
                jsonDst.accumulate(key, srcValue);
            }
        }

        return jsonDst;
    }

}
