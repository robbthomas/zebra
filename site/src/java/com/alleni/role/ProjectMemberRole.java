package com.alleni.role;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class ProjectMemberRole extends BaseBean implements Serializable
{
    static private ProjectMemberRoleDao _dao() { return new ProjectMemberRoleDao(); }

    protected ProjectMemberRole(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("projectId", daoJson.has("projectid") ? daoJson.getString("projectid") : -1);
        this.jsonValues.put("memberId", daoJson.has("memberid") ? daoJson.getString("memberid") : "");
        this.jsonValues.put("type", daoJson.has("type") ? daoJson.getString("type") : "");
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("PROJECTMEMBERROLE.create - could not create ProjectMemberRoles record");

        return success("projectMemberRoleId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("PROJECTMEMBERROLE.update - could not update ProjectMemberRoles record");

        return success("projectMemberRoleId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("PROJECTMEMBERROLE.retire - could not retire ProjectMemberRoles record");

        return success("projectMemberRoleId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("id");
        ArrayList projectMemberRolesList = _dao().find(id, credentialValues);

        if (projectMemberRolesList.size() < 1)
            return error("PROJECTMEMBERROLE.find - could not find ProjectMemberRoles id "+id);
        if (projectMemberRolesList.size() > 1)
            return error("PROJECTMEMBERROLE.find - found multiple ProjectMemberRoles records for ProjectMemberRoles id "+id);

        ProjectMemberRole projectMemberRoles = new ProjectMemberRole((JSONObject)projectMemberRolesList.get(0));
        if (projectMemberRoles == null)
            return error("PROJECTMEMBERROLE.find - projectMemberRoles==null for id "+id);

        return success(projectMemberRoles.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList projectMemberRolesList = _dao().findAll(credentialValues);
        return jsonFoundList(projectMemberRolesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of ProjectMemberRoles json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList projectMemberRolesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(projectMemberRolesList, limit);

        JSONObject jsonList = jsonFoundList(projectMemberRolesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            ProjectMemberRole projectMemberRoles = new ProjectMemberRole((JSONObject)foundList.get(j));
            if (projectMemberRoles == null)
                return JsonError.jsonError(JsonError.Error.PROJECTMEMBERROLE_FIND, "Could not make ProjectMemberRoles object " + j);
            JSONObject json = new JSONObject(projectMemberRoles.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("projectMemberRoles", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        PROJECTMEMBERROLE_CREATE,
        PROJECTMEMBERROLE_RETIRE,
        PROJECTMEMBERROLE_FIND,
        PROJECTMEMBERROLE_UPDATE,

 */