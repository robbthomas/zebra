package com.alleni.role;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class AccountMemberRole extends BaseBean implements Serializable
{
    static private AccountMemberRoleDao _dao() { return new AccountMemberRoleDao(); }

    protected AccountMemberRole(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("memberId", daoJson.has("memberid") ? daoJson.getString("memberid") : "");
        this.jsonValues.put("type", daoJson.has("type") ? daoJson.getString("type") : "");
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);

        // additional field returned by find and filter
        this.jsonValues.put("typeId", daoJson.has("typeid") ? daoJson.getInt("typeid") : -1);
    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("ACCOUNTMEMBERROLE.create - could not create AccountMemberRoles record");

        return success("accountMemberRoleId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("ACCOUNTMEMBERROLE.update - could not update AccountMemberRoles record");

        return success("accountMemberRoleId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("ACCOUNTMEMBERROLE.retire - could not retire AccountMemberRoles record");

        return success("accountMemberRoleId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("id");
        ArrayList accountMemberRolesList = _dao().find(id, credentialValues);

        if (accountMemberRolesList.size() < 1)
            return error("ACCOUNTMEMBERROLE.find - could not find AccountMemberRoles id "+id);
        if (accountMemberRolesList.size() > 1)
            return error("ACCOUNTMEMBERROLE.find - found multiple AccountMemberRoles records for AccountMemberRoles id "+id);

        AccountMemberRole accountMemberRoles = new AccountMemberRole((JSONObject)accountMemberRolesList.get(0));
        if (accountMemberRoles == null)
            return error("ACCOUNTMEMBERROLE.find - accountMemberRoles==null for id "+id);

        return success(accountMemberRoles.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList accountMemberRolesList = _dao().findAll(credentialValues);
        return jsonFoundList(accountMemberRolesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of AccountMemberRoles json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList accountMemberRolesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(accountMemberRolesList, limit);

        JSONObject jsonList = jsonFoundList(accountMemberRolesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            AccountMemberRole accountMemberRoles = new AccountMemberRole((JSONObject)foundList.get(j));
            if (accountMemberRoles == null)
                return JsonError.jsonError(JsonError.Error.ACCOUNTMEMBERROLE_FIND, "Could not make AccountMemberRoles object " + j);
            JSONObject json = new JSONObject(accountMemberRoles.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("accountMemberRoles", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        ACCOUNTMEMBERROLE_CREATE,
        ACCOUNTMEMBERROLE_RETIRE,
        ACCOUNTMEMBERROLE_FIND,
        ACCOUNTMEMBERROLE_UPDATE,

 */