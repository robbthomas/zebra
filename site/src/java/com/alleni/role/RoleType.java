package com.alleni.role;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class RoleType extends BaseBean implements Serializable
{
    static private RoleTypeDao _dao() { return new RoleTypeDao(); }

    protected RoleType(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("roletypeid" ) ? daoJson.getInt("roletypeid") : -1);
        this.jsonValues.put("tag", daoJson.has("tag") ? daoJson.getString("tag") : "");
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("displayOrder", daoJson.has("displayorder") ? daoJson.getInt("displayorder") : -1);
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("ROLETYPE.create - could not create RoleTypes record");

        return success("roleTypeId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("ROLETYPE.update - could not update RoleTypes record");

        return success("roleTypeId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("ROLETYPE.retire - could not retire RoleTypes record");

        return success("roleTypeId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("roleTypeId");
        ArrayList roleTypesList = _dao().find(id, credentialValues);

        if (roleTypesList.size() < 1)
            return error("ROLETYPE.find - could not find RoleTypes id "+id);
        if (roleTypesList.size() > 1)
            return error("ROLETYPE.find - found multiple RoleTypes records for RoleTypes id "+id);

        RoleType roleTypes = new RoleType((JSONObject)roleTypesList.get(0));
        if (roleTypes == null)
            return error("ROLETYPE.find - roleTypes==null for id "+id);

        return success(roleTypes.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList roleTypesList = _dao().findAll(credentialValues);
        return jsonFoundList(roleTypesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of RoleTypes json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList roleTypesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(roleTypesList, limit);

        JSONObject jsonList = jsonFoundList(roleTypesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            RoleType roleTypes = new RoleType((JSONObject)foundList.get(j));
            if (roleTypes == null)
                return JsonError.jsonError(JsonError.Error.ROLETYPE_FIND, "Could not make RoleTypes object " + j);
            JSONObject json = new JSONObject(roleTypes.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("roleTypes", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        ROLETYPE_CREATE,
        ROLETYPE_RETIRE,
        ROLETYPE_FIND,
        ROLETYPE_UPDATE,

 */