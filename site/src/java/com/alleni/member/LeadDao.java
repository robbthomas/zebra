package com.alleni.member;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class LeadDao extends BaseDAO
{
    private static final String CREATE      = "{? = call fn_lead_create(?,?,?)}";
    private static final String FILTER      = "{call fn_leaditemvalue_filter(?,?,?, ?,?,?, ?,?,?, ?,?)}";

    protected Map<String, Map> createItemParameters;

    public LeadDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_leadSource", new HashMapBuilder<String, String>().add("key", "leadSource").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_leadSource", new HashMapBuilder<String, String>().add("key", "leadSource").add("type", "text").build())
            .add("in_item", new HashMapBuilder<String, String>().add("key", "item").add("type", "text").build())
            .add("in_value", new HashMapBuilder<String, String>().add("key", "value").add("type", "text").build())
            .add("in_fromDate", new HashMapBuilder<String, String>().add("key", "toDate").add("type", "text").build())
            .add("in_toDate", new HashMapBuilder<String, String>().add("key", "fromDate").add("type", "text").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

}