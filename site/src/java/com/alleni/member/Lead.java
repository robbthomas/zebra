package com.alleni.member;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;
import com.alleni.util.Mail;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.json.*;

public class Lead extends BaseBean implements Serializable
{
	private static final Logger log = Logger.getLogger(Lead.class);

	static private LeadDao _dao() { return new LeadDao(); }

    protected Lead(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("leadSource", daoJson.has("leadsource") ? daoJson.getString("leadsource") : "");
        this.jsonValues.put("leadDateTime", daoJson.has("leaddatetime") ? (Timestamp)daoJson.get("leaddatetime") : new Timestamp(0));
        this.jsonValues.put("leadId", daoJson.has("leadid" ) ? daoJson.getInt("leadid") : -1);
        this.jsonValues.put("leadItemValueId", daoJson.has("leaditemvalueid" ) ? daoJson.getInt("leaditemvalueid") : -1);
        this.jsonValues.put("item", daoJson.has("item") ? daoJson.getString("item") : "");
        this.jsonValues.put("value", daoJson.has("value") ? daoJson.getString("value") : "");
    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
    	System.out.println("  values:  " + createValues);
    	System.out.println("  credentials:  " + credentialValues);
    	
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("LEADS.create - could not create Leads record");

        return success("leadId", id);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Lead json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList feedbackList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(feedbackList, limit);

        JSONObject jsonList = jsonFoundList(feedbackList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Lead feedback = new Lead((JSONObject)foundList.get(j));
            if (feedback == null)
                return JsonError.jsonError(JsonError.Error.FEEDBACK_FIND, "Could not make Feedback object " + j);
            JSONObject json = new JSONObject(feedback.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("feedback", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        FEEDBACK_CREATE,
        FEEDBACK_RETIRE,
        FEEDBACK_FIND,
        FEEDBACK_UPDATE,

 */