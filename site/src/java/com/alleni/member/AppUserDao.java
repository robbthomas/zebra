package com.alleni.member;

import com.alleni.common.BaseDAO;
import com.alleni.db.util.JDBCHelper;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;
import com.alleni.util.RandomPassword;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AppUserDao extends BaseDAO
{
    private static final String CREATE      = "{? = call fn_appuser_create(?,?,?, ?,?,?, ?,?,?, ?,?)}";
    private static final String UPDATE      = "{? = call fn_appuser_update(?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?,  ?)}";
    private static final String RETIRE      = "{? = call fn_appuser_retire(?,?,?, ?)}";
    private static final String FILTER        = "{call fn_appuser_filter(?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?,  ?)}";
    private static final String FIND_BY_ID  = "{call fn_appuser_getbyid(?,?,?)}";
    private static final String FIND_BY_EMAIL = "{call fn_appuser_getbyemail(?,?,?)}";

    private static final String LOGIN           = "{call fn_appuser_login(?,?)}";
    private static final String LOGIN2          = "{call fn_appuser_login2(?,?,?,?)}";
    private static final String UPDATE_PASSWORD = "{call fn_appuser_updatePassword(?,?,?,?)}";
    private static final String RESET_PASSWORD = "{call fn_appuser_resetPassword(?,?,?,?,?)}";

    private String findBy;
    private String email;
    private String id;
    private String randomPassword;

    public AppUserDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_displayName", new HashMapBuilder<String, String>().add("key", "displayName").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_firstName", new HashMapBuilder<String, String>().add("key", "firstName").add("type", "text").build())
            .add("in_lastName", new HashMapBuilder<String, String>().add("key", "lastName").add("type", "text").build())
            .add("in_nameFormat", new HashMapBuilder<String, String>().add("key", "nameFormat").add("type", "text").build())
            .add("in_password", new HashMapBuilder<String, String>().add("key", "password").add("type", "text").build())
            .add("in_passwordSalt", new HashMapBuilder<String, String>().add("key", "passwordSalt").add("type", "text").build())
            //.add("in_editedById", new HashMapBuilder<String, String>().add("key", "editedById").add("type", "text").build())
            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_appUserId", new HashMapBuilder<String, String>().add("key", "id").add("type", "text").build())
            
            .add("in_version", new HashMapBuilder<String, String>().add("key", "in_version").add("type", "integer").build())
            .add("in_displayName", new HashMapBuilder<String, String>().add("key", "displayName").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "userDescription").add("type", "text").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_firstName", new HashMapBuilder<String, String>().add("key", "firstName").add("type", "text").build())
            .add("in_lastName", new HashMapBuilder<String, String>().add("key", "lastName").add("type", "text").build())
            .add("in_nameFormat", new HashMapBuilder<String, String>().add("key", "nameFormat").add("type", "text").build())
            .add("in_website", new HashMapBuilder<String, String>().add("key", "website").add("type", "text").build())
            .add("in_twitter", new HashMapBuilder<String, String>().add("key", "twitter").add("type", "text").build())
            .add("in_password", new HashMapBuilder<String, String>().add("key", "password").add("type", "text").build())
            .add("in_editedById", new HashMapBuilder<String, String>().add("key", "editedById").add("type", "text").build())
            .add("in_retiredById", new HashMapBuilder<String, String>().add("key", "retiredById").add("type", "text").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_displayName", new HashMapBuilder<String, String>().add("key", "displayName").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_firstName", new HashMapBuilder<String, String>().add("key", "firstName").add("type", "text").build())
            .add("in_lastName", new HashMapBuilder<String, String>().add("key", "lastName").add("type", "text").build())
            .add("in_nameFormat", new HashMapBuilder<String, String>().add("key", "nameFormat").add("type", "text").build())
            .add("in_editedById", new HashMapBuilder<String, String>().add("key", "editedById").add("type", "text").build())
            .add("in_editedDateTime", new HashMapBuilder<String, String>().add("key", "editedDateTime").add("type", "timestamp").build())
            .add("in_retiredById", new HashMapBuilder<String, String>().add("key", "retiredById").add("type", "text").build())
            .add("in_retiredDateTime", new HashMapBuilder<String, String>().add("key", "retiredDateTime").add("type", "timestamp").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public String create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return createStr(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues,credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList findById(String id, JSONObject credentialValues) throws SQLException, JSONException
    {
        // NOTE that App_User.Id is varchar(255)
        this.findBy = FIND_BY_ID;
        return findByOneParameter(FIND_BY_ID, id, true, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.findBy = FILTER;
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        this.findBy = FILTER;
        return findAll(FILTER, credentialValues);
    }

    public ArrayList findByEmail(String email, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.findBy = FIND_BY_EMAIL;
        return findByOneParameter(FIND_BY_EMAIL, email, true, credentialValues);
    }

    public ArrayList login(JSONObject credentials) throws SQLException, JSONException
    {
        this.findBy = LOGIN;
        return findWithNoParameters(LOGIN, credentials);
    }

    public int resetPassword(JSONObject updateValues, JSONObject credentials) throws SQLException, JSONException
    {
        Connection conn = null;
        CallableStatement cs = null;
        ArrayList jsonList = new ArrayList();
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(RESET_PASSWORD);

            cs.setString(1, credentials.getString("email"));
            cs.setString(2, credentials.getString("password"));
            cs.setString(3, updateValues.getString("email"));
            cs.setString(4, updateValues.getString("newPassword"));
            cs.setString(5, "" /*RandomPassword.generate(16)*/);  // do not generate salt until Zephyr encoding same

            ResultSet rs = cs.executeQuery();

            rs.next();
            return rs.getInt(1);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

    public int updatePassword(JSONObject updateValues, JSONObject credentials) throws SQLException, JSONException
    {
        Connection conn = null;
        CallableStatement cs = null;
        ArrayList jsonList = new ArrayList();
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(UPDATE_PASSWORD);

            cs.setString(1, credentials.getString("email"));
            cs.setString(2, credentials.getString("password"));
            cs.setString(3, updateValues.getString("newPassword"));
            cs.setString(4, "" /*RandomPassword.generate(16)*/);  // do not generate salt until Zephyr encoding same

            ResultSet rs = cs.executeQuery();

            rs.next();
            return rs.getInt(1);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
    }

    public ArrayList login2(JSONObject credentials, int count, int offset) throws SQLException, JSONException
    {
        Connection conn = null;
        CallableStatement cs = null;
        ArrayList jsonList = new ArrayList();
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(LOGIN2);

            cs.setString(1, credentials.getString("email"));
            cs.setString(2, credentials.getString("password"));
            cs.setInt(3, count);
            cs.setInt(4, offset);

            ResultSet rs = cs.executeQuery();

            while (rs.next()) {
                JSONObject json = resultSetToJSON(rs);
                jsonList.add(json);
            }

        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
        return jsonList;
    }

    @Override
    protected int setCreateParameters(Connection conn, CallableStatement cs, int offset) throws SQLException, JSONException
    {
        setPasswordSalt();
        setRandomPassword();
        return super.setCreateParameters(conn, cs, offset);
    }

    @Override
    protected int setUpdateParameters(Connection conn, CallableStatement cs, int offset) throws SQLException,  JSONException
    {
        return super.setUpdateParameters(conn, cs, offset);
    }

    @Override
    protected int setFindParameters(Connection conn, CallableStatement cs, int offset) throws SQLException, JSONException
    {
        if (this.findBy.equals(LOGIN))
        {
            setParameter(cs, "displayName", "", crudValues, offset++);
            setParameter(cs, "password", "", crudValues, offset++);
        }
        else if (this.findBy.equals(FIND_BY_ID))
        {
            setParameter(cs, this.id, offset++);
        }
        else if (this.findBy.equals(FIND_BY_EMAIL))
        {
            setParameter(cs, this.email, offset++);
        }
        else
        {
            offset = super.setFindParameters(conn, cs, offset);
        }

        return offset;
    }

    protected void setPasswordSalt() throws JSONException
    {
        if (!crudValues.has("passwordSalt"))
        {
            try
            {
                String passwordSalt = RandomPassword.generate(16);
                crudValues.put("passwordSalt", "" /*passwordSalt*/); // do not generate salt until Zephyr encoding same
            }
            catch (JSONException ex)
            {
                if (!crudValues.has("passwordSalt"))
                    throw (ex);
            }
        }
    }

    protected void setRandomPassword() throws JSONException
    {
        if (!crudValues.has("password") || crudValues.getString("password").length() == 0)
        {
            try
            {
                randomPassword = RandomPassword.generate(10);
                crudValues.put("password", randomPassword);
            }
            catch (JSONException ex)
            {
                if (!crudValues.has("password"))
                    throw (ex);
            }
        }
    }

    public String getRandomPassword()
    {
        return randomPassword != null ? randomPassword : "";
    }

}