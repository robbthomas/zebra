package com.alleni.member;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.*;

public class AppUser extends BaseBean implements Serializable
{
    static private AppUserDao _dao() { return new AppUserDao(); }

    protected AppUser(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("id" ) ? daoJson.getString("id") : "");
        this.jsonValues.put("version", daoJson.has("version") ? daoJson.getInt("version") : -1);
        this.jsonValues.put("displayName", daoJson.has("displayname") ? daoJson.getString("displayname") : "");
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("email", daoJson.has("email") ? daoJson.getString("email") : "");
        this.jsonValues.put("firstName", daoJson.has("firstname") ? daoJson.getString("firstname") : "");
        this.jsonValues.put("lastName", daoJson.has("lastname") ? daoJson.getString("lastname") : "");
        this.jsonValues.put("nameFormat", daoJson.has("nameformat") ? daoJson.getString("nameformat") : "");
        this.jsonValues.put("passwordHash", daoJson.has("passwordhash") ? daoJson.getString("passwordhash") : "");
        this.jsonValues.put("passwordSalt", daoJson.has("passwordsalt") ? daoJson.getString("passwordsalt") : "");
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : "");
        this.jsonValues.put("editedDateTime", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));
        this.jsonValues.put("lastUpdated", daoJson.has("lastupdated") ? (Timestamp)daoJson.get("lastupdated") : new Timestamp(0));
        this.jsonValues.put("website", daoJson.has("website") ? daoJson.getString("website") : "");
        this.jsonValues.put("twitter", daoJson.has("twitter") ? daoJson.getString("twitter") : "");
        //this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getBoolean("retired") : false);
        this.jsonValues.put("retiredById", daoJson.has("retiredbyid") ? daoJson.getString("retiredbyid") : "");
        this.jsonValues.put("retiredDateTime", daoJson.has("retireddatetime") ? (Timestamp)daoJson.get("retireddatetime") : new Timestamp(0));
        this.jsonValues.put("avatarid", daoJson.has("avatarid") ? daoJson.getString("avatarid") : "");
        this.jsonValues.put("bannerid", daoJson.has("bannerid") ? daoJson.getString("bannerid") : "");
        
        // additional fields returned by FILTER function
        this.jsonValues.put("accountName", daoJson.has("accountname") ? daoJson.getString("accountname") : "");
        this.jsonValues.put("accountDescription", daoJson.has("accountdescription") ? daoJson.getString("accountdescription") : "");
        
        this.jsonValues.put("addressName", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("addressCompany", daoJson.has("addresscompany") ? daoJson.getString("addresscompany") : "");
        this.jsonValues.put("address1", daoJson.has("address1") ? daoJson.getString("address1") : "");
        this.jsonValues.put("address2", daoJson.has("address2") ? daoJson.getString("address2") : "");
        this.jsonValues.put("city", daoJson.has("city") ? daoJson.getString("city") : "");
        this.jsonValues.put("stateProvince", daoJson.has("stateprovince") ? daoJson.getString("stateprovince") : "");
        this.jsonValues.put("zipPostalCode", daoJson.has("zippostalcode") ? daoJson.getString("zippostalcode") : "");
        this.jsonValues.put("country", daoJson.has("country") ? daoJson.getString("country") : "");

        // additional fields returned by LOGIN function
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("projectId", daoJson.has("projectid") ? daoJson.getString("projectid") : -1);
        this.jsonValues.put("roleType", daoJson.has("roletype") ? daoJson.getString("roletype") : "");
        this.jsonValues.put("roleTypeId", daoJson.has("roletypeid") ? daoJson.getInt("roletypeid") : -1);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        String id = _dao().create(createValues, credentialValues);
        if (id.length() == 0)
            return error("APPUSER.create - could not create App_User record");

        JSONObject json = new JSONObject();
        json.put("appUserId", id);
        return success(json);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("APPUSER.update - could not update invoice line items record");

        return success("appUserId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("APPUSER.retire - could not retire invoice line items record");

        return success("appUserId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        //int id = findValues.getInt("id");
        String id = findValues.getString("id"); // App_User.Id varchar(255)!?
        ArrayList appUserList = _dao().findById(id, credentialValues);

        if (appUserList.size() < 1)
            return error("APPUSER.find - could not find invoice line items id "+id);
        if (appUserList.size() > 1)
            return error("APPUSER.find - found multiple invoice line items records for invoice line items id "+id);

        AppUser appUser = new AppUser((JSONObject)appUserList.get(0));
        if (appUser == null)
            return error("APPUSER.find - appUser==null for id "+id);

        return success(appUser.toString());
    }

    public static JSONObject findByEmail(String email, JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList appUserList = _dao().findByEmail(email, credentialValues);

        if (appUserList.size() < 1)
            return error("APPUSER.findByEmail - could not find appUser for email "+email);
        if (appUserList.size() > 1)
            return error("APPUSER.findByEmail - found multiple appUsers for email "+email);

        AppUser appUser = new AppUser((JSONObject)appUserList.get(0));
        if (appUser == null)
            return error("APPUSER.findByEmail - appUser==null for email "+email);

        return success(appUser.toString());
    }

    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList appUserList = _dao().findAll(credentialValues);
        return jsonFoundList(appUserList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of invoice line items json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList appUserList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(appUserList, limit);

        JSONObject jsonList = jsonFoundList(appUserList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            AppUser appUser = new AppUser((JSONObject)foundList.get(j));
            if (appUser == null)
                return JsonError.jsonError(JsonError.Error.APPUSER_FIND, "Could not make invoice line items object " + j);
            JSONObject json = new JSONObject(appUser.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("appUser", foundList);

        return jsonList;
    }

    // FOLLOWING FUNCTIONS NOT CRUD GENERATED

    public static JSONObject resetPassword(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().resetPassword(updateValues, credentialValues);
        if (id == -1)
            return error("APPUSER.resetPassword - could not reset password");

        return success("appUserId", id);
    }

    public static JSONObject updatePassword(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().updatePassword(updateValues, credentialValues);
        if (id == -1)
            return error("APPUSER.updatePassword - could not update password");

        return success("appUserId", id);
    }

    // TODO: rename methods and sql function to getFromCredentials???
    public static JSONObject login(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList appUserList = _dao().login(credentialValues);
        if (appUserList.size() < 1)
            return error("AppUser.login - credentials are not valid.");

        AppUser appUser = new AppUser((JSONObject)appUserList.get(0));
        return success(appUser.toString());
    }

    public static JSONObject login2(JSONObject credentialValues, int count, int offset) throws SQLException, JSONException
    {
        ArrayList appUserList = _dao().login2(credentialValues, count + 1, offset);
        boolean hasMoreData = false;

        if (appUserList.size() < 1) {
            return error("AppUser.login - credentials are not valid.");
        }
        if (appUserList.size() == count + 1) {
            hasMoreData = true;
            appUserList.remove(count);
        }

        JSONObject appUserJSON = (JSONObject)appUserList.get(0);

        // build the return type.
        /*
         * TODO -SBJ: figure out what this was for and remove if no longer needed.
         * 
        JSONArray accountList = new JSONArray();
        int ii;

        for (ii = 0; ii < appUserList.size(); ii++) {
            JSONObject      obj = (JSONObject)appUserList.get(ii);
            JSONObject      newobj = new JSONObject();

            newobj.put("id", obj.has("accountid") ? obj.get("accountid") : -1);
            newobj.put("title", obj.has("accountname") ? obj.get("accountname") : "");
            newobj.put("description", obj.has("accountdescription") ? obj.get("accountdescription") : "");
            newobj.put("accounttypeid", obj.has("accounttypeid") ? obj.get("accounttypeid") : "");
            newobj.put("accounttypetag", obj.has("accounttypetag") ? obj.get("accounttypetag") : "");
            newobj.put("accounttermduedate", obj.has("accounttermduedate") ? obj.get("accounttermduedate") : "");
            newobj.put("accountgoodstanding", obj.has("accountgoodstanding") ? obj.get("accountgoodstanding") : "");
            newobj.put("customerprofileid", obj.has("customerprofileid") ? obj.get("customerprofileid") : "");

            accountList.put(ii, newobj);
        }
        */

        JSONObject dataJSON = new JSONObject();
        dataJSON.put("id", appUserJSON.has("id") ? appUserJSON.get("id") : -1);
        dataJSON.put("accountid", appUserJSON.has("accountid") ? appUserJSON.get("accountid") : -1);
        dataJSON.put("systemAdministrator", appUserJSON.has("systemadministrator") ? "t".equals(appUserJSON.get("systemadministrator")) : false);
        dataJSON.put("userDescription", appUserJSON.has("description") ? appUserJSON.get("description") : "");
        dataJSON.put("description", appUserJSON.has("accountdescription") ? appUserJSON.get("accountdescription") : "");
        dataJSON.put("accounttypeid", appUserJSON.has("accounttypeid") ? appUserJSON.get("accounttypeid") : "");
        dataJSON.put("accounttypetag", appUserJSON.has("accounttypetag") ? appUserJSON.get("accounttypetag") : "");
        dataJSON.put("accounttermduedate", appUserJSON.has("accounttermduedatetime") ? appUserJSON.get("accounttermduedatetime") : "");
        dataJSON.put("accountgoodstanding", appUserJSON.has("accountgoodstanding") ? appUserJSON.get("accountgoodstanding") : "");
        dataJSON.put("allowsubscriptionpayment", appUserJSON.has("allowsubscriptionpayment") ? appUserJSON.get("allowsubscriptionpayment") : "");
        dataJSON.put("authenticated", Boolean.TRUE);
        dataJSON.put("userId", appUserJSON.get("id"));
        dataJSON.put("displayName", appUserJSON.get("displayname"));
        dataJSON.put("lastName", appUserJSON.has("lastname")? appUserJSON.get("lastname") : "");
        dataJSON.put("firstName", appUserJSON.has("firstname")? appUserJSON.get("firstname") : "");
        dataJSON.put("nameFormat", appUserJSON.has("nameformat") ? appUserJSON.get("nameformat") : "firstLast");
        dataJSON.put("email", appUserJSON.get("email"));
        dataJSON.put("website",appUserJSON.has("website") ? appUserJSON.get("website"):"");
        dataJSON.put("twitter", appUserJSON.has("twitter") ?appUserJSON.get("twitter"):"");
        dataJSON.put("avatarid", appUserJSON.has("avatarid") ? appUserJSON.get("avatarid"):"");
        dataJSON.put("bannerid", appUserJSON.has("bannerid") ? appUserJSON.get("bannerid"):"");
        //dataJSON.put("accounts", accountList);
        dataJSON.put("hasCreditCardProfile", appUserJSON.has("customerprofileid") && !appUserJSON.optString("customerprofileid","").equals("") ? true : false);

        JSONObject retVal = new JSONObject();
        retVal.put("success", Boolean.TRUE);
        retVal.put("data", dataJSON);
        retVal.put("hasMoreData", hasMoreData);

        return retVal;
    }
}

/*
 PUT IN com.alleni.common.jsonError.java

        APPUSER_CREATE,
        APPUSER_RETIRE,
        APPUSER_FIND,
        APPUSER_UPDATE,

 */