package com.alleni.member;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class LeadItemValueDao extends BaseDAO
{
    private static final String CREATE = "{? = call fn_leaditemvalue_create(?,?,?, ?,?)}";

    protected Map<String, Map> createItemParameters;

    public LeadItemValueDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_leadId", new HashMapBuilder<String, String>().add("key", "leadId").add("type", "integer").build())
            .add("in_item", new HashMapBuilder<String, String>().add("key", "item").add("type", "text").build())
            .add("in_value", new HashMapBuilder<String, String>().add("key", "value").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

}