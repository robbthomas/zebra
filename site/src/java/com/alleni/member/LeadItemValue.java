package com.alleni.member;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class LeadItemValue extends BaseBean implements Serializable
{
    static private LeadItemValueDao _dao() { return new LeadItemValueDao(); }

    protected LeadItemValue(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("leadItemValueId", daoJson.has("leaditemvalueid" ) ? daoJson.getInt("leaditemvalueid") : -1);
        this.jsonValues.put("leadId", daoJson.has("leadid" ) ? daoJson.getInt("leadid") : -1);
        this.jsonValues.put("item", daoJson.has("item") ? daoJson.getString("item") : "");
        this.jsonValues.put("value", daoJson.has("value") ? daoJson.getString("value") : "");
    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("LeadItemValue.create - could not create LeadItemValue record");

        return success("leadItemValueId", id);
    }

}

