package com.alleni.loader;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;
import com.alleni.account.Account;
import com.alleni.account.Address;
import com.alleni.account.Company;
import com.alleni.account.Telephone;
import com.alleni.feedback.Feedback;
import com.alleni.finance.CreditCard;
import com.alleni.finance.ECheck;
import com.alleni.finance.FinancialTransaction;
import com.alleni.finance.Invoice;
import com.alleni.finance.InvoiceLineItem;
import com.alleni.finance.Payment;
import com.alleni.finance.PaymentLineItem;
import com.alleni.member.AppUser;
import com.alleni.project.AccountProjectNumber;
import com.alleni.project.Category;
import com.alleni.project.Publish;
import com.alleni.project.ScreenShot;
import com.alleni.role.AccountMemberRole;
import com.alleni.role.ProjectMemberRole;
import com.alleni.role.RoleType;
import com.alleni.util.JSONHelper;

import java.io.Serializable;
import java.sql.SQLException;
import org.json.*;

public class Loader extends BaseBean implements Serializable
{
    public enum Right {
        LOADER_PRELOAD,
        LOADER_LOAD_SERVICES,
        LOADER_LOAD_STAGE_STEPS,
        LOADER_LOAD_RESOURCES,
        LOADER_LOAD_RESOURCE_MANAGED_SERVICES
    }

    protected Loader(JSONObject daoJson) throws JSONException
    {
    }

    public static String preload(JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.LOADER_PRELOAD, credentialValues))
            return JsonError.error(JsonError.Error.LOADER_PRELOAD, "Insufficient rights");

        // Get the preload data
        JSONObject json = new JSONObject();

        JSONObject stageList = Account.findAll(credentialValues);
        json = JSONHelper.merge(json, stageList);

        JSONObject msStageStep = Address.findAll(credentialValues);
        json = JSONHelper.merge(json, msStageStep);

        JSONObject statusTypeList = Company.findAll(credentialValues);
        json = JSONHelper.merge(json, statusTypeList);

        JSONObject serviceCreationTeamList = Telephone.findAll(credentialValues);
        json = JSONHelper.merge(json, serviceCreationTeamList);

        JSONObject businessUnitList = Feedback.findAll(credentialValues);
        json = JSONHelper.merge(json, businessUnitList);

        //JSONObject stageCoreContactList = AppUser.filter(credentialValues);
        //json = JSONHelper.merge(json, stageCoreContactList);

        JSONObject tamTypeList = AccountProjectNumber.findAll(credentialValues);
        json = JSONHelper.merge(json, tamTypeList);

        JSONObject camTypeList = Category.findAll(credentialValues);
        json = JSONHelper.merge(json, camTypeList);

        JSONObject evaTypeList = Publish.findAll(credentialValues);
        json = JSONHelper.merge(json, evaTypeList);

        JSONObject serviceProcessList = ScreenShot.findAll(credentialValues);
        json = JSONHelper.merge(json, serviceProcessList);

        JSONObject architectureList = AccountMemberRole.findAll(credentialValues);
        json = JSONHelper.merge(json, architectureList);

        JSONObject managedServiceViewList = ProjectMemberRole.findAll(credentialValues);
        json = JSONHelper.merge(json, managedServiceViewList);

        JSONObject msStatusTypeList = RoleType.findAll(credentialValues);
        json = JSONHelper.merge(json, msStatusTypeList);

        // Build up the list of default values.
        JSONObject defaults = new JSONObject();

        /*
        JSONObject serviceCreationTeamDefaults = ServiceCreationTeam.findDefaults(credentialValues);
        defaults = JSONHelper.merge(defaults, serviceCreationTeamDefaults);

        JSONObject stageDefaults = Stage.findDefaults(credentialValues);
        defaults = JSONHelper.merge(defaults, stageDefaults);

        JSONObject statusTypeDefaults = StatusType.findDefaults(credentialValues);
        defaults = JSONHelper.merge(defaults, statusTypeDefaults);

        JSONObject serviceProcessDefaults = ServiceProcess.findDefaults(credentialValues);
        defaults = JSONHelper.merge(defaults, serviceProcessDefaults);
         * 
         */

        json.put("defaults", defaults);

        return json.toString();
    }

    /*
    public static String loadServices(JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.LOADER_LOAD_SERVICES, credentialValues))
            return JsonError.error(JsonError.Error.LOADER_LOAD_SERVICES, "Insufficient rights");

        JSONObject serviceList = Service.findAll(credentialValues);

        return serviceList.toString();
    }

    public static String loadStageSteps(JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.LOADER_LOAD_STAGE_STEPS, credentialValues))
            return JsonError.error(JsonError.Error.LOADER_LOAD_STAGE_STEPS, "Insufficient rights");

        JSONObject stageStepList = StageStep.findAll(credentialValues);

        return stageStepList.toString();
    }

    public static String loadResources(JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.LOADER_LOAD_RESOURCES, credentialValues))
            return JsonError.error(JsonError.Error.LOADER_LOAD_RESOURCES, "Insufficient rights");

        JSONObject resourceList = MSResource.findAll(credentialValues);

        return resourceList.toString();
    }

    public static String loadResourceManagedServices(JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.LOADER_LOAD_RESOURCE_MANAGED_SERVICES, credentialValues))
            return JsonError.error(JsonError.Error.LOADER_LOAD_RESOURCE_MANAGED_SERVICES, "Insufficient rights");

        JSONObject resourceManagedServiceList = MSResourceManagedService.findAll(credentialValues);

        return resourceManagedServiceList.toString();
    }
     * 
     */

    protected static boolean hasRight(Right right, JSONObject credentialValues) throws JSONException, SQLException
    {
        if (!validateAndUpdateCredentials(credentialValues))
            return false;

        // Now check against the rights.
        return true;
    }

}
