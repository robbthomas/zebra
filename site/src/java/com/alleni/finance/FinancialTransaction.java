package com.alleni.finance;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class FinancialTransaction extends BaseBean implements Serializable
{
    static private FinancialTransactionDao _dao() { return new FinancialTransactionDao(); }

    protected FinancialTransaction(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("financialTransactionId", daoJson.has("financialtransactionid" ) ? daoJson.getInt("financialtransactionid") : -1);
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("amount", daoJson.has("amount") ? daoJson.getInt("amount") : -1);
        this.jsonValues.put("perTransactionFee", daoJson.has("pertransactionfee") ? daoJson.getInt("pertransactionfee") : -1);
        this.jsonValues.put("currencyTypeId", daoJson.has("currencytypeid") ? daoJson.getInt("currencytypeid") : -1);
        this.jsonValues.put("label", daoJson.has("label") ? daoJson.getString("label") : "");
        this.jsonValues.put("type", daoJson.has("type") ? daoJson.getString("type") : "");
        this.jsonValues.put("agentId", daoJson.has("agentid") ? daoJson.getInt("agentid") : -1);
        this.jsonValues.put("cimTransactionId", daoJson.has("cimtransactionid") ? daoJson.getString("cimtransactionid") : "");
        this.jsonValues.put("status", daoJson.has("status") ? daoJson.getString("status") : "");
        this.jsonValues.put("statusReasonCode", daoJson.has("statusreasoncode") ? daoJson.getInt("statusreasoncode") : -1);
        this.jsonValues.put("statusReasonText", daoJson.has("statusreasontext") ? daoJson.getString("statusreasontext") : "");

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("FINANCIALTRANSACTION.create - could not create FinancialTransactions record");

        return success("financialTransactionId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("FINANCIALTRANSACTION.update - could not update FinancialTransactions record");

        return success("financialTransactionId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("FINANCIALTRANSACTION.retire - could not retire FinancialTransactions record");

        return success("financialTransactionId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("financialTransactionId");
        ArrayList financialTransactionsList = _dao().find(id, credentialValues);

        if (financialTransactionsList.size() < 1)
            return error("FINANCIALTRANSACTION.find - could not find FinancialTransactions id "+id);
        if (financialTransactionsList.size() > 1)
            return error("FINANCIALTRANSACTION.find - found multiple FinancialTransactions records for FinancialTransactions id "+id);

        FinancialTransaction financialTransactions = new FinancialTransaction((JSONObject)financialTransactionsList.get(0));
        if (financialTransactions == null)
            return error("FINANCIALTRANSACTION.find - financialTransactions==null for id "+id);

        return success(financialTransactions.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList financialTransactionsList = _dao().findAll(credentialValues);
        return jsonFoundList(financialTransactionsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of FinancialTransactions json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList financialTransactionsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(financialTransactionsList, limit);

        JSONObject jsonList = jsonFoundList(financialTransactionsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            FinancialTransaction financialTransactions = new FinancialTransaction((JSONObject)foundList.get(j));
            if (financialTransactions == null)
                return JsonError.jsonError(JsonError.Error.FINANCIALTRANSACTION_FIND, "Could not make FinancialTransactions object " + j);
            JSONObject json = new JSONObject(financialTransactions.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("financialTransactions", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        FINANCIALTRANSACTION_CREATE,
        FINANCIALTRANSACTION_RETIRE,
        FINANCIALTRANSACTION_FIND,
        FINANCIALTRANSACTION_UPDATE,

 */