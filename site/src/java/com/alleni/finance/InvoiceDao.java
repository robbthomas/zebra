package com.alleni.finance;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import com.alleni.db.util.JDBCHelper;
import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class InvoiceDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_invoice_create(?,?,?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_invoice_update(?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_invoice_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_invoice_filter(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String FIND        = "{call fn_invoice_find(?,?,?)}";
   private static final String CANCELPENDING        = "{call fn_cancelPendingSubscriptionInvoices(?,?)}";

    private static final String BATCHCHARGES = "{call fn_invoice_batch_charges_create(?,?,?)}";
    private static final String BATCHUPDATES = "{? = call fn_invoice_batch_state_update(?,?,?,?)}";

    public InvoiceDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_cachedTotal", new HashMapBuilder<String, String>().add("key", "cachedTotal").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_state", new HashMapBuilder<String, String>().add("key", "state").add("type", "text").build())
            .add("in_holdingDateTime", new HashMapBuilder<String, String>().add("key", "holdingDateTime").add("type", "timestamp").build())
            .add("in_transactionId", new HashMapBuilder<String, String>().add("key", "transactionId").add("type", "integer").build())


            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_invoiceId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_cachedTotal", new HashMapBuilder<String, String>().add("key", "cachedTotal").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_state", new HashMapBuilder<String, String>().add("key", "state").add("type", "text").build())
            .add("in_holdingDateTime", new HashMapBuilder<String, String>().add("key", "holdingDateTime").add("type", "timestamp").build())
            .add("in_transactionId", new HashMapBuilder<String, String>().add("key", "transactionId").add("type", "integer").build())


            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_cachedTotal", new HashMapBuilder<String, String>().add("key", "cachedTotal").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_state", new HashMapBuilder<String, String>().add("key", "state").add("type", "text").build())
            .add("in_holdingDateTime", new HashMapBuilder<String, String>().add("key", "holdingDateTime").add("type", "timestamp").build())
            .add("in_transactionId", new HashMapBuilder<String, String>().add("key", "transactionId").add("type", "integer").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }
    
    public ArrayList cancelPending(JSONObject credentialValues) throws SQLException, JSONException
    {
        return execute(CANCELPENDING, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }


    //====================/  NON-CRUD-GENERATED  /====================/

    public ArrayList batchPendingInvoices(int accountId, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(BATCHCHARGES, accountId, credentialValues);
    }

    public int updateStateByTransaction(int transactionId, String state, JSONObject credentialValues) throws SQLException, JSONException
    {
        this.credentialValues = credentialValues;
        int id  = -1;

        // Execute the query
        CallableStatement cs = null;
        Connection conn = null;
        try
        {
            conn = getConnection();
            cs = conn.prepareCall(BATCHUPDATES);
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
            setCredentialsParameters(conn, cs, 2);
            setParameter(cs, transactionId, 4);
            setParameter(cs, state, 5);
            cs.execute();
            id = cs.getInt(1);
        }
        catch (SQLException ex)
        {
            throw ex;
        }
        finally
        {
            JDBCHelper.close(cs, conn);
        }
        return id;
    }

}