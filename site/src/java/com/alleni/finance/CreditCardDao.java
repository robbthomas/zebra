package com.alleni.finance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.alleni.common.BaseDAO;
import com.alleni.db.util.JDBCHelper;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

public class CreditCardDao extends BaseDAO
{
    private static final String CREATE      = "{? = call fn_creditcard_create(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"; //16
    private static final String UPDATE      = "{? = call fn_creditcard_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"; // 18
    private static final String RETIRE      = "{? = call fn_creditcard_retire(?,?,?,?)}";
    private static final String FILTER      = "{call fn_creditcard_filter(?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,? ,?,?,?,?)}";
    private static final String FIND        = "{call fn_creditcard_find(?,?,?)}";

    // Not CRUD generated
    private static final String EXPIRING    = "{call fn_creditcard_expiring(?,?,?)}";
    private static final String VALIDFORACCOUNT = "{call fn_creditcard_validforaccount(?,?,?)}";

    public CreditCardDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_addressId", new HashMapBuilder<String, String>().add("key", "addressId").add("type", "integer").build())
            .add("in_nameOnCreditCard", new HashMapBuilder<String, String>().add("key", "nameOnCreditCard").add("type", "text").build())
            .add("in_maskedNumber", new HashMapBuilder<String, String>().add("key", "maskedNumber").add("type", "text").build())
            .add("in_expirationMonth", new HashMapBuilder<String, String>().add("key", "expirationMonth").add("type", "text").build())
            .add("in_expirationYear", new HashMapBuilder<String, String>().add("key", "expirationYear").add("type", "text").build())
            .add("in_customerProfileId", new HashMapBuilder<String, String>().add("key", "customerProfileId").add("type", "text").build())
            .add("in_customerPaymentProfileId", new HashMapBuilder<String, String>().add("key", "customerPaymentProfileId").add("type", "text").build())
            .add("in_firstName", new HashMapBuilder<String, String>().add("key", "firstName").add("type", "text").build())
            .add("in_lastName", new HashMapBuilder<String, String>().add("key", "lastName").add("type", "text").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_expirationNoticeSent", new HashMapBuilder<String, String>().add("key", "expirationNoticeSent").add("type", "integer").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_creditCardId", new HashMapBuilder<String, String>().add("key", "creditCardId").add("type", "integer").build())
            
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_addressId", new HashMapBuilder<String, String>().add("key", "addressId").add("type", "integer").build())
            .add("in_nameOnCreditCard", new HashMapBuilder<String, String>().add("key", "nameOnCreditCard").add("type", "text").build())
            .add("in_maskedNumber", new HashMapBuilder<String, String>().add("key", "maskedNumber").add("type", "text").build())
            .add("in_expirationMonth", new HashMapBuilder<String, String>().add("key", "expirationMonth").add("type", "text").build())
            .add("in_expirationYear", new HashMapBuilder<String, String>().add("key", "expirationYear").add("type", "text").build())
            .add("in_customerProfileId", new HashMapBuilder<String, String>().add("key", "customerProfileId").add("type", "text").build())
            .add("in_customerPaymentProfileId", new HashMapBuilder<String, String>().add("key", "customerPaymentProfileId").add("type", "text").build())
            .add("in_firstName", new HashMapBuilder<String, String>().add("key", "firstName").add("type", "text").build())
            .add("in_lastName", new HashMapBuilder<String, String>().add("key", "lastName").add("type", "text").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_expirationNoticeSent", new HashMapBuilder<String, String>().add("key", "expirationNoticeSent").add("type", "integer").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_addressId", new HashMapBuilder<String, String>().add("key", "addressId").add("type", "integer").build())
            .add("in_nameOnCreditCard", new HashMapBuilder<String, String>().add("key", "nameOnCreditCard").add("type", "text").build())
            .add("in_maskedNumber", new HashMapBuilder<String, String>().add("key", "maskedNumber").add("type", "text").build())
            .add("in_expirationMonth", new HashMapBuilder<String, String>().add("key", "expirationMonth").add("type", "text").build())
            .add("in_expirationYear", new HashMapBuilder<String, String>().add("key", "expirationYear").add("type", "text").build())
            .add("in_customerProfileId", new HashMapBuilder<String, String>().add("key", "customerProfileId").add("type", "text").build())
            .add("in_customerPaymentProfileId", new HashMapBuilder<String, String>().add("key", "customerPaymentProfileId").add("type", "text").build())
            .add("in_firstName", new HashMapBuilder<String, String>().add("key", "firstName").add("type", "text").build())
            .add("in_lastName", new HashMapBuilder<String, String>().add("key", "lastName").add("type", "text").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

    //====================/  NON-CRUD-GENERATED  /====================/

    public ArrayList findCreditCardsExpiring(int withinDays, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(EXPIRING, withinDays, credentialValues);
    }

    public ArrayList findValidCreditCardsForAccount(long accountId, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(VALIDFORACCOUNT, accountId, credentialValues);
    }

    public static void setExpirationNoticeSent(int creditCardId, int oneEqualsYesZeroEqualsFalse) throws Exception {
        Connection          conn = null;
        PreparedStatement   statement = null;
        ResultSet           rs;
        int					retVal = 0;
        
        String sql = "update CreditCards set expirationNoticeSent = ? where creditCardId = ?";
        try {
            conn = JDBCHelper.getJdbcConnection();
            statement = conn.prepareStatement(sql);
            statement.setInt(1, creditCardId);
            statement.setInt(2, oneEqualsYesZeroEqualsFalse);

            retVal = statement.executeUpdate();
        }
        catch (SQLException e) {
        	e.printStackTrace();
            throw e;
        }
        finally {
            JDBCHelper.close(conn);
        }
    }
}