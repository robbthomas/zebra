package com.alleni.finance;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class PaymentLineItem extends BaseBean implements Serializable
{
    static private PaymentLineItemDao _dao() { return new PaymentLineItemDao(); }

    protected PaymentLineItem(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("paymentlineitemid" ) ? daoJson.getInt("paymentlineitemid") : -1);
        this.jsonValues.put("paymentId", daoJson.has("paymentid") ? daoJson.getInt("paymentid") : -1);
        this.jsonValues.put("lineNumber", daoJson.has("linenumber") ? daoJson.getInt("linenumber") : -1);
        this.jsonValues.put("quantity", daoJson.has("quantity") ? daoJson.getInt("quantity") : -1);
        this.jsonValues.put("amountTotal", daoJson.has("amounttotal") ? daoJson.getInt("amounttotal") : -1);
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("projectId", daoJson.has("projectid") ? daoJson.getString("projectid") : -1);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("PAYMENTLINEITEM.create - could not create PaymentLineItems record");

        return success("paymentLineItemId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("PAYMENTLINEITEM.update - could not update PaymentLineItems record");

        return success("paymentLineItemId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("PAYMENTLINEITEM.retire - could not retire PaymentLineItems record");

        return success("paymentLineItemId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("paymentLineItemId");
        ArrayList paymentLineItemsList = _dao().find(id, credentialValues);

        if (paymentLineItemsList.size() < 1)
            return error("PAYMENTLINEITEM.find - could not find PaymentLineItems id "+id);
        if (paymentLineItemsList.size() > 1)
            return error("PAYMENTLINEITEM.find - found multiple PaymentLineItems records for PaymentLineItems id "+id);

        PaymentLineItem paymentLineItems = new PaymentLineItem((JSONObject)paymentLineItemsList.get(0));
        if (paymentLineItems == null)
            return error("PAYMENTLINEITEM.find - paymentLineItems==null for id "+id);

        return success(paymentLineItems.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList paymentLineItemsList = _dao().findAll(credentialValues);
        return jsonFoundList(paymentLineItemsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of PaymentLineItems json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList paymentLineItemsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(paymentLineItemsList, limit);

        JSONObject jsonList = jsonFoundList(paymentLineItemsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            PaymentLineItem paymentLineItems = new PaymentLineItem((JSONObject)foundList.get(j));
            if (paymentLineItems == null)
                return JsonError.jsonError(JsonError.Error.PAYMENTLINEITEM_FIND, "Could not make PaymentLineItems object " + j);
            JSONObject json = new JSONObject(paymentLineItems.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("paymentLineItems", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        PAYMENTLINEITEM_CREATE,
        PAYMENTLINEITEM_RETIRE,
        PAYMENTLINEITEM_FIND,
        PAYMENTLINEITEM_UPDATE,

 */