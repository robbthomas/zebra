package com.alleni.finance;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.*;

public class CreditCard extends BaseBean implements Serializable
{
    static private CreditCardDao _dao() { return new CreditCardDao(); }

    static private Map<Integer, String> creditCardTypeTagLookup = null;
    static private Map<Integer, String> creditCardTypeNameLookup = null;

    protected CreditCard(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("creditCardId", daoJson.has("creditcardid" ) ? daoJson.getInt("creditcardid") : -1);
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("addressId", daoJson.has("addressid") ? daoJson.getInt("addressid") : -1);
        this.jsonValues.put("nameOnCreditCard", daoJson.has("nameoncreditcard") ? daoJson.getString("nameoncreditcard") : "");
        this.jsonValues.put("maskedNumber", daoJson.has("maskednumber") ? daoJson.getString("maskednumber") : "");
        this.jsonValues.put("expirationMonth", daoJson.has("expirationmonth") ? daoJson.getString("expirationmonth") : "");
        this.jsonValues.put("expirationYear", daoJson.has("expirationyear") ? daoJson.getString("expirationyear") : "");
        this.jsonValues.put("customerProfileId", daoJson.has("customerprofileid") ? daoJson.getString("customerprofileid") : "");
        this.jsonValues.put("customerPaymentProfileId", daoJson.has("customerpaymentprofileid") ? daoJson.getString("customerpaymentprofileid") : "");
        this.jsonValues.put("firstName", daoJson.has("firstname") ? daoJson.getString("firstname") : "");
        this.jsonValues.put("lastName", daoJson.has("lastname") ? daoJson.getString("lastname") : "");
        this.jsonValues.put("email", daoJson.has("email") ? daoJson.getString("email") : "");
        this.jsonValues.put("creditCardType", daoJson.has("creditcardtype") ? daoJson.getInt("creditcardtype") : -1);
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : -1);
        this.jsonValues.put("editedDateTime", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
        this.jsonValues.put("expirationNoticeSent", daoJson.has("expirationnoticesent") ? daoJson.getInt("expirationnoticesent") : 0);

        // need to fix up type
        if (daoJson.has("creditcardtype")) {
            this.jsonValues.put("creditCardTypeTag", creditCardTypeTagLookup.get(daoJson.getInt("creditcardtype")));
            this.jsonValues.put("creditCardTypeName", creditCardTypeNameLookup.get(daoJson.getInt("creditcardtype")));
        }
    }

    protected static void checkCreditCardTypeLookup(JSONObject credentials) throws SQLException, JSONException {
        if (creditCardTypeTagLookup == null) {
            JSONObject creditCardTypes = CreditCardType.findAll(credentials);

            JSONArray creditCardArray = creditCardTypes.getJSONArray("creditCardTypes");

            creditCardTypeTagLookup = new HashMap<Integer, String>();
            creditCardTypeNameLookup = new HashMap<Integer, String>();

            for (int ii = 0; ii < creditCardArray.length(); ii++) {
                JSONObject cardType = creditCardArray.getJSONObject(ii);
                creditCardTypeTagLookup.put(cardType.getInt("id"), cardType.getString("tag"));
                creditCardTypeNameLookup.put(cardType.getInt("id"), cardType.getString("name"));
            }
        }
    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("CREDITCARD.create - could not create CreditCards record");

        return success("creditCardId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        checkCreditCardTypeLookup(credentialValues);
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("CREDITCARD.update - could not update CreditCards record");

        return success("creditCardId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("CREDITCARD.retire - could not retire CreditCards record");

        return success("creditCardId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        checkCreditCardTypeLookup(credentialValues);
        int id = findValues.getInt("creditCardId");
        ArrayList creditCardsList = _dao().find(id, credentialValues);

        if (creditCardsList.size() < 1)
            return error("CREDITCARD.find - could not find CreditCards id "+id);
        if (creditCardsList.size() > 1)
            return error("CREDITCARD.find - found multiple CreditCards records for CreditCards id "+id);

        CreditCard creditCards = new CreditCard((JSONObject)creditCardsList.get(0));
        if (creditCards == null)
            return error("CREDITCARD.find - creditCards==null for id "+id);

        return success(creditCards.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        checkCreditCardTypeLookup(credentialValues);
        ArrayList creditCardsList = _dao().findAll(credentialValues);
        return jsonFoundList(creditCardsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        checkCreditCardTypeLookup(credentialValues);
        // get an array of CreditCards json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList creditCardsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(creditCardsList, limit);

        JSONObject jsonList = jsonFoundList(creditCardsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            CreditCard creditCards = new CreditCard((JSONObject)foundList.get(j));
            if (creditCards == null)
                return JsonError.jsonError(JsonError.Error.CREDITCARD_FIND, "Could not make CreditCards object " + j);
            JSONObject json = new JSONObject(creditCards.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("creditCards", foundList);

        return jsonList;
    }
    
    //=========================================================================/
    //==================== CUSTOM LOGIC NOT CRUD GENERATED ====================/
    //=========================================================================/

    public static String maskNumber(String number)
    {
        String padding = "XXXXXXXXXXXXXXXXXXXX";
        if (number == null)
            return "";
        else if (number.length() <= 4)
            return number;
        else
            return padding.substring(0,number.length()-4) + number.substring(number.length()-4);
    }

    public static JSONObject findCreditCardsExpiring(int withinDays, JSONObject credentialValues) throws SQLException, JSONException
    {
        checkCreditCardTypeLookup(credentialValues);
        ArrayList creditCardsList = _dao().findCreditCardsExpiring(withinDays, credentialValues);
        JSONObject jsonList = jsonFoundList(creditCardsList);
        return success(jsonList);
    }

    public static JSONObject findValidCreditCardsForAccount(long accountId, JSONObject credentialValues) throws SQLException, JSONException
    {
        checkCreditCardTypeLookup(credentialValues);
        ArrayList creditCardsList = _dao().findValidCreditCardsForAccount(accountId, credentialValues);
        JSONObject jsonList = jsonFoundList(creditCardsList);
        return success(jsonList);
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        CREDITCARD_CREATE,
        CREDITCARD_RETIRE,
        CREDITCARD_FIND,
        CREDITCARD_UPDATE,

 */