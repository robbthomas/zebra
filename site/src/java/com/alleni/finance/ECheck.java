package com.alleni.finance;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class ECheck extends BaseBean implements Serializable
{
    static private ECheckDao _dao() { return new ECheckDao(); }

    protected ECheck(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("echeckid" ) ? daoJson.getInt("echeckid") : -1);
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("bankRoutingNumber", daoJson.has("bankroutingnumber") ? daoJson.getString("bankroutingnumber") : "");
        this.jsonValues.put("bankAccountNumber", daoJson.has("bankaccountnumber") ? daoJson.getString("bankaccountnumber") : "");
        this.jsonValues.put("bankName", daoJson.has("bankname") ? daoJson.getString("bankname") : "");
        this.jsonValues.put("bankAccountName", daoJson.has("bankaccountname") ? daoJson.getString("bankaccountname") : "");
        this.jsonValues.put("customerProfileId", daoJson.has("customerprofileid") ? daoJson.getString("customerprofileid") : "");
        this.jsonValues.put("customerPaymentProfileId", daoJson.has("customerpaymentprofileid") ? daoJson.getString("customerpaymentprofileid") : "");
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : -1);
        this.jsonValues.put("created", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("ECHECK.create - could not create EChecks record");

        return success("eCheckId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("ECHECK.update - could not update EChecks record");

        return success("eCheckId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("ECHECK.retire - could not retire EChecks record");

        return success("eCheckId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("eCheckId");
        ArrayList eChecksList = _dao().find(id, credentialValues);

        if (eChecksList.size() < 1)
            return error("ECHECK.find - could not find EChecks id "+id);
        if (eChecksList.size() > 1)
            return error("ECHECK.find - found multiple EChecks records for EChecks id "+id);

        ECheck eChecks = new ECheck((JSONObject)eChecksList.get(0));
        if (eChecks == null)
            return error("ECHECK.find - eChecks==null for id "+id);

        return success(eChecks.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList eChecksList = _dao().findAll(credentialValues);
        return jsonFoundList(eChecksList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of EChecks json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList eChecksList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(eChecksList, limit);

        JSONObject jsonList = jsonFoundList(eChecksList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            ECheck eChecks = new ECheck((JSONObject)foundList.get(j));
            if (eChecks == null)
                return JsonError.jsonError(JsonError.Error.ECHECK_FIND, "Could not make EChecks object " + j);
            JSONObject json = new JSONObject(eChecks.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("eChecks", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        ECHECK_CREATE,
        ECHECK_RETIRE,
        ECHECK_FIND,
        ECHECK_UPDATE,

 */