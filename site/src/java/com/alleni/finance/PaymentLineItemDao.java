package com.alleni.finance;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentLineItemDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_paymentlineitem_create(?,?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_paymentlineitem_update(?,?,?,?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_paymentlineitem_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_paymentlineitem_filter(?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String FIND        = "{call fn_paymentlineitem_find(?,?,?)}";

    public PaymentLineItemDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_paymentId", new HashMapBuilder<String, String>().add("key", "paymentId").add("type", "integer").build())
            .add("in_lineNumber", new HashMapBuilder<String, String>().add("key", "lineNumber").add("type", "integer").build())
            .add("in_quantity", new HashMapBuilder<String, String>().add("key", "quantity").add("type", "integer").build())
            .add("in_amountTotal", new HashMapBuilder<String, String>().add("key", "amountTotal").add("type", "integer").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_projectId", new HashMapBuilder<String, String>().add("key", "projectId").add("type", "text").build())


            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_paymentLineItemsId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_paymentId", new HashMapBuilder<String, String>().add("key", "paymentId").add("type", "integer").build())
            .add("in_lineNumber", new HashMapBuilder<String, String>().add("key", "lineNumber").add("type", "integer").build())
            .add("in_quantity", new HashMapBuilder<String, String>().add("key", "quantity").add("type", "integer").build())
            .add("in_amountTotal", new HashMapBuilder<String, String>().add("key", "amountTotal").add("type", "integer").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_projectId", new HashMapBuilder<String, String>().add("key", "projectId").add("type", "text").build())


            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_paymentId", new HashMapBuilder<String, String>().add("key", "paymentId").add("type", "integer").build())
            .add("in_lineNumber", new HashMapBuilder<String, String>().add("key", "lineNumber").add("type", "integer").build())
            .add("in_quantity", new HashMapBuilder<String, String>().add("key", "quantity").add("type", "integer").build())
            .add("in_amountTotal", new HashMapBuilder<String, String>().add("key", "amountTotal").add("type", "integer").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_projectId", new HashMapBuilder<String, String>().add("key", "projectId").add("type", "text").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

}