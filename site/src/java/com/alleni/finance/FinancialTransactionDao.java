package com.alleni.finance;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class FinancialTransactionDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_financialtransaction_create(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_financialtransaction_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String FILTER      = "{call fn_financialtransaction_filter(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String FIND        = "{call fn_financialtransaction_find(?,?,?)}";

    public FinancialTransactionDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_amount", new HashMapBuilder<String, String>().add("key", "amount").add("type", "integer").build())
            .add("in_perTransactionFee", new HashMapBuilder<String, String>().add("key", "perTransactionFee").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_label", new HashMapBuilder<String, String>().add("key", "label").add("type", "text").build())
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_agentId", new HashMapBuilder<String, String>().add("key", "agentId").add("type", "integer").build())
            .add("in_cimTransactionId", new HashMapBuilder<String, String>().add("key", "cimTransactionId").add("type", "text").build())
            .add("in_status", new HashMapBuilder<String, String>().add("key", "status").add("type", "text").build())
            .add("in_statusReasonCode", new HashMapBuilder<String, String>().add("key", "statusReasonCode").add("type", "integer").build())
            .add("in_statusReasonText", new HashMapBuilder<String, String>().add("key", "statusReasonText").add("type", "text").build())


            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_financialTransactionsId", new HashMapBuilder<String, String>().add("key", "financialTransactionId").add("type", "integer").build())
            
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_amount", new HashMapBuilder<String, String>().add("key", "amount").add("type", "integer").build())
            .add("in_perTransactionFee", new HashMapBuilder<String, String>().add("key", "perTransactionFee").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_label", new HashMapBuilder<String, String>().add("key", "label").add("type", "text").build())
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_agentId", new HashMapBuilder<String, String>().add("key", "agentId").add("type", "integer").build())
            .add("in_cimTransactionId", new HashMapBuilder<String, String>().add("key", "cimTransactionId").add("type", "text").build())
            .add("in_status", new HashMapBuilder<String, String>().add("key", "status").add("type", "text").build())
            .add("in_statusReasonCode", new HashMapBuilder<String, String>().add("key", "statusReasonCode").add("type", "integer").build())
            .add("in_statusReasonText", new HashMapBuilder<String, String>().add("key", "statusReasonText").add("type", "text").build())


            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_amount", new HashMapBuilder<String, String>().add("key", "amount").add("type", "integer").build())
            .add("in_perTransactionFee", new HashMapBuilder<String, String>().add("key", "perTransactionFee").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_label", new HashMapBuilder<String, String>().add("key", "label").add("type", "text").build())
            .add("in_type", new HashMapBuilder<String, String>().add("key", "type").add("type", "text").build())
            .add("in_agentId", new HashMapBuilder<String, String>().add("key", "agentId").add("type", "integer").build())
            .add("in_cimTransactionId", new HashMapBuilder<String, String>().add("key", "cimTransactionId").add("type", "text").build())
            .add("in_status", new HashMapBuilder<String, String>().add("key", "status").add("type", "text").build())
            .add("in_statusReasonCode", new HashMapBuilder<String, String>().add("key", "statusReasonCode").add("type", "integer").build())
            .add("in_statusReasonText", new HashMapBuilder<String, String>().add("key", "statusReasonText").add("type", "text").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return -1;
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

}