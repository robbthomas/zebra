package com.alleni.finance;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class Payment extends BaseBean implements Serializable
{
    static private PaymentDao _dao() { return new PaymentDao(); }

    protected Payment(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("paymentid" ) ? daoJson.getInt("paymentid") : -1);
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("cachedTotal", daoJson.has("cachedtotal") ? daoJson.getInt("cachedtotal") : -1);
        this.jsonValues.put("currencyTypeId", daoJson.has("currencytypeid") ? daoJson.getInt("currencytypeid") : -1);
        this.jsonValues.put("state", daoJson.has("state") ? daoJson.getString("state") : "");
        this.jsonValues.put("holdingDateTime", daoJson.has("holdingdatetime") ? (Timestamp)daoJson.get("holdingdatetime") : new Timestamp(0));
        this.jsonValues.put("transactionId", daoJson.has("transactionid") ? daoJson.getInt("transactionid") : -1);

        // extra fields returned by fn_paymentbatchcredits.sql functions
        this.jsonValues.put("email", daoJson.has("email") ? daoJson.getString("email") : "");
        this.jsonValues.put("transactionLabel", daoJson.has("transactionlabel") ? daoJson.getString("transactionlabel") : "");
        this.jsonValues.put("totalCredit", daoJson.has("totalcredit") ? daoJson.getInt("totalcredit") : -1);

        // TBD: get PaymentStates values from the database as we do with CreditCardTypes in CreditCard.java
        if (daoJson.has("stateid")) {
            int stateId = daoJson.getInt("stateid");
            this.jsonValues.put("stateId", stateId);
            this.jsonValues.put("state", (stateId == 1 ? "payment_state_pending" : "payment_state_credited"));
            this.jsonValues.put("stateName", (stateId == 1 ? "Payment Pending" : "Payment Credited"));
        }
    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("PAYMENT.create - could not create Payments record");

        return success("paymentId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("PAYMENT.update - could not update Payments record");

        return success("paymentId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("PAYMENT.retire - could not retire Payments record");

        return success("paymentId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("paymentId");
        ArrayList paymentsList = _dao().find(id, credentialValues);

        if (paymentsList.size() < 1)
            return error("PAYMENT.find - could not find Payments id "+id);
        if (paymentsList.size() > 1)
            return error("PAYMENT.find - found multiple Payments records for Payments id "+id);

        Payment payments = new Payment((JSONObject)paymentsList.get(0));
        if (payments == null)
            return error("PAYMENT.find - payments==null for id "+id);

        return success(payments.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList paymentsList = _dao().findAll(credentialValues);
        return jsonFoundList(paymentsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Payments json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList paymentsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(paymentsList, limit);

        JSONObject jsonList = jsonFoundList(paymentsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Payment payments = new Payment((JSONObject)foundList.get(j));
            if (payments == null)
                return JsonError.jsonError(JsonError.Error.PAYMENT_FIND, "Could not make Payments object " + j);
            JSONObject json = new JSONObject(payments.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("payments", foundList);

        return jsonList;
    }

    //====================/  NON-CRUD-GENERATED  /====================/

    public static JSONObject createPendingPayments(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        try
        {
            int holdingDays = findValues.getInt("holdingDays");

            ArrayList paymentsList = _dao().createPendingPayments(holdingDays, credentialValues);

            JSONObject jsonList = jsonFoundList(paymentsList);
            return success(jsonList);
        }
        catch (Exception e)
        {
            return error(e.getMessage());
        }
    }

    public static JSONObject batchPendingPayments(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        try
        {
            int accountId = findValues.getInt("accountId");

            ArrayList paymentsList = _dao().batchPendingPayments(accountId, credentialValues);

            JSONObject jsonList = jsonFoundList(paymentsList);
            return success(jsonList);
        }
        catch (Exception e)
        {
            return error(e.getMessage());
        }
    }

    public static JSONObject updateStateByTransaction(int transactionid, String state, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id =  _dao().updateStateByTransaction(transactionid, state, credentialValues);
        if (id == -1)
            return error("Payment.updateStateByTransaction - could not update Payments to state="+state+" for transactionId="+transactionid);

        return success("count", id);
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        PAYMENT_CREATE,
        PAYMENT_RETIRE,
        PAYMENT_FIND,
        PAYMENT_UPDATE,

 */