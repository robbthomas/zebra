package com.alleni.finance;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class Invoice extends BaseBean implements Serializable
{
    static private InvoiceDao _dao() { return new InvoiceDao(); }

    protected Invoice(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("invoiceid" ) ? daoJson.getInt("invoiceid") : -1);
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("cachedTotal", daoJson.has("cachedtotal") ? daoJson.getInt("cachedtotal") : -1);
        this.jsonValues.put("currencyTypeId", daoJson.has("currencytypeid") ? daoJson.getInt("currencytypeid") : 1); // default USD
        this.jsonValues.put("state", daoJson.has("state") ? daoJson.getString("state") : "");
        this.jsonValues.put("holdingDateTime", daoJson.has("holdingdatetime") ? (Timestamp)daoJson.get("holdingdatetime") : new Timestamp(0));
        this.jsonValues.put("transactionId", daoJson.has("transactionid") ? daoJson.getInt("transactionid") : -1);

        // extra fields returned by fn_invoicebatchcharge.sql functions
        this.jsonValues.put("email", daoJson.has("email") ? daoJson.getString("email") : "");
        this.jsonValues.put("firstName", daoJson.has("firstname") ? daoJson.getString("firstname") : "");
        this.jsonValues.put("lastName", daoJson.has("lastname") ? daoJson.getString("lastname") : "");
        this.jsonValues.put("transactionLabel", daoJson.has("transactionlabel") ? daoJson.getString("transactionlabel") : "");
        this.jsonValues.put("totalCharge", daoJson.has("totalcharge") ? daoJson.getInt("totalcharge") : -1);
        this.jsonValues.put("includesSubscriptionCharge", daoJson.has("includessubscriptioncharge") ? daoJson.getInt("includessubscriptioncharge") : -1);
        this.jsonValues.put("subscrCharge", daoJson.has("subscrcharge") ? daoJson.getInt("subscrcharge") : 0);

        // TBD: get InvoiceStates values from the database as we do with CreditCardTypes in CreditCard.java
        if (daoJson.has("stateid")) {
            int stateId = daoJson.getInt("stateid");
            this.jsonValues.put("stateId", stateId);
            this.jsonValues.put("state", (stateId == 1 ? "invoice_state_pending" : stateId == 2 ? "invoice_state_charged" : "invoice_state_credits_created"));
            this.jsonValues.put("stateName", (stateId == 1 ? "Invoice Pending" : stateId == 2 ? "Invoice Charged" : "Invoice Credits Created"));
        }
    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("INVOICE.create - could not create Invoices record");

        return success("invoiceId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("INVOICE.update - could not update Invoices record");

        return success("invoiceId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("INVOICE.retire - could not retire Invoices record");

        return success("invoiceId", id);
    }
    
    public static JSONObject cancelPendingSubscriptionInvoices(JSONObject credentialValues) throws SQLException, JSONException
    {
       _dao().cancelPending(credentialValues);
        return success("");
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("invoiceId");
        ArrayList invoicesList = _dao().find(id, credentialValues);

        if (invoicesList.size() < 1)
            return error("INVOICE.find - could not find Invoices id "+id);
        if (invoicesList.size() > 1)
            return error("INVOICE.find - found multiple Invoices records for Invoices id "+id);

        Invoice invoices = new Invoice((JSONObject)invoicesList.get(0));
        if (invoices == null)
            return error("INVOICE.find - invoices==null for id "+id);

        return success(invoices.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList invoicesList = _dao().findAll(credentialValues);
        return jsonFoundList(invoicesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Invoices json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList invoicesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(invoicesList, limit);

        JSONObject jsonList = jsonFoundList(invoicesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Invoice invoices = new Invoice((JSONObject)foundList.get(j));
            if (invoices == null)
                return JsonError.jsonError(JsonError.Error.INVOICE_FIND, "Could not make Invoices object " + j);
            JSONObject json = new JSONObject(invoices.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("invoices", foundList);

        return jsonList;
    }


    //====================/  NON-CRUD-GENERATED  /====================/

    public static JSONObject batchPendingInvoices(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // optional accountId, if <= 0 batches pending invoices for all accounts
        int accountId = -1;
        try { accountId = findValues.getInt("accountId"); }
        catch (JSONException e) { }

        ArrayList invoicesList = _dao().batchPendingInvoices(accountId, credentialValues);

        JSONObject jsonList = jsonFoundList(invoicesList);
        return success(jsonList);
    }

    public static JSONObject updateStateByTransaction(int transactionid, String state, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id =  _dao().updateStateByTransaction(transactionid, state, credentialValues);
        if (id == -1)
            return error("Invoice.updateStateByTransaction - could not update Invoices to state="+state+" for transactionId="+transactionid);

        return success("count", id);
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        INVOICE_CREATE,
        INVOICE_RETIRE,
        INVOICE_FIND,
        INVOICE_UPDATE,

 */