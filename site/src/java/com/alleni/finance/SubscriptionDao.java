package com.alleni.finance;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SubscriptionDao extends BaseDAO
{
    /*
    private static final String CREATE      = "{? = call fn_invoice_create(?,?,?,?,?,?,?)}";
    private static final String UPDATE      = "{? = call fn_invoice_update(?,?,?,?,?,?,?,?,?)}";
    private static final String RETIRE      = "{? = call fn_invoice_retire(?,?)}";
    private static final String FILTER      = "{call fn_invoice_filter(?,?,?,?,?,?,?,?,?,?)}";
    private static final String FIND        = "{call fn_invoice_find(?)}";
    */

    private static final String NEW         = "{call fn_subscription_new(?,?,?)}";
    private static final String RENEW       = "{call fn_subscriptions_renew(?,?,?)}";

    public SubscriptionDao()
    {
        /*
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_cachedTotal", new HashMapBuilder<String, String>().add("key", "cachedTotal").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_state", new HashMapBuilder<String, String>().add("key", "state").add("type", "text").build())
            .add("in_holdingDateTime", new HashMapBuilder<String, String>().add("key", "holdingDateTime").add("type", "timestamp").build())
            .add("in_transactionId", new HashMapBuilder<String, String>().add("key", "transactionId").add("type", "integer").build())


            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_invoicesId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_cachedTotal", new HashMapBuilder<String, String>().add("key", "cachedTotal").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_state", new HashMapBuilder<String, String>().add("key", "state").add("type", "text").build())
            .add("in_holdingDateTime", new HashMapBuilder<String, String>().add("key", "holdingDateTime").add("type", "timestamp").build())
            .add("in_transactionId", new HashMapBuilder<String, String>().add("key", "transactionId").add("type", "integer").build())


            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_cachedTotal", new HashMapBuilder<String, String>().add("key", "cachedTotal").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_state", new HashMapBuilder<String, String>().add("key", "state").add("type", "text").build())
            .add("in_holdingDateTime", new HashMapBuilder<String, String>().add("key", "holdingDateTime").add("type", "timestamp").build())
            .add("in_transactionId", new HashMapBuilder<String, String>().add("key", "transactionId").add("type", "integer").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
        */
    }

    /*
    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues,credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int architectureId) throws SQLException
    {
        return findByOneParameter(FIND, architectureId);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues);
    }

    public ArrayList findAll() throws SQLException, JSONException
    {
        return findAll(FILTER);
    }
    */

    public int newSubscription(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(NEW, createValues, credentialValues);
    }

    public ArrayList renewSubscriptions(int holdingDays, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(RENEW, holdingDays, credentialValues);
    }

}