package com.alleni.finance;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class Subscription extends BaseBean implements Serializable
{
    public enum Right {
        SUBSCRIPTION_CREATE,
        SUBSCRIPTION_RETIRE,
        SUBSCRIPTION_FIND,
        SUBSCRIPTION_UPDATE
    }

    static private SubscriptionDao _dao() { return new SubscriptionDao(); }

    protected Subscription(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("invoiceid" ) ? daoJson.getInt("invoiceid") : -1);
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("accountName", daoJson.has("accountname") ? daoJson.getString("accountname") : "");
        this.jsonValues.put("goodStanding", daoJson.has("goodstanding") ? daoJson.getInt("goodstanding") : 1);
        this.jsonValues.put("termDueDateTime", daoJson.has("termduedatetime") ? (Timestamp)daoJson.get("termduedatetime") : new Timestamp(0));
        this.jsonValues.put("newTermDueDateTime", daoJson.has("newtermduedatetime") ? (Timestamp)daoJson.get("newtermduedatetime") : new Timestamp(0));
        this.jsonValues.put("accountTypeName", daoJson.has("accounttypename") ? daoJson.getString("accounttypename") : "");
        this.jsonValues.put("terms", daoJson.has("terms") ? daoJson.getString("terms") : "");
        this.jsonValues.put("price", daoJson.has("price") ? daoJson.getInt("price") : -1);
        this.jsonValues.put("currencyTypeId", daoJson.has("currencytypeid") ? daoJson.getInt("currencytypeid") : -1);
        this.jsonValues.put("holdingDays", daoJson.has("holdingdays") ? daoJson.getInt("holdingdays") : -1);
        this.jsonValues.put("holdingDateTime", daoJson.has("holdingdatetime") ? (Timestamp)daoJson.get("holdingdatetime") : new Timestamp(0));
        this.jsonValues.put("transactionId", daoJson.has("transactionid") ? daoJson.getInt("transactionid") : -1);
        this.jsonValues.put("allowSubscriptionPayment", daoJson.has("allowsubscriptionpayment") ? daoJson.getInt("allowsubscriptionpayment") : -1);
        this.jsonValues.put("creditCardId", daoJson.has("creditcardid") ? daoJson.getInt("creditcardid") : -1);
        
        // from fn_subscriptions.sql
        this.jsonValues.put("email", daoJson.has("email") ? daoJson.getString("email") : "");
        this.jsonValues.put("firstName", daoJson.has("firstname") ? daoJson.getString("firstname") : "");
        this.jsonValues.put("lastName", daoJson.has("lastname") ? daoJson.getString("lastname") : "");
        this.jsonValues.put("renewalNoticeSent", daoJson.has("renewalnoticesent") ? daoJson.getInt("renewalnoticesent") : 0);
        }

    /*
    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.SUBSCRIPTION_CREATE, credentialValues))
            return error("SUBSCRIPTION.create - credentials error");

        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("SUBSCRIPTION.create - could not create Invoices record");

        return success("invoiceId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.SUBSCRIPTION_UPDATE, credentialValues))
            return error("SUBSCRIPTION.update - credentials error");

        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("SUBSCRIPTION.update - could not update Invoices record");

        return success("invoiceId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.SUBSCRIPTION_RETIRE, credentialValues))
            return error("SUBSCRIPTION.retire - credentials error");

        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("SUBSCRIPTION.retire - could not retire Invoices record");

        return success("invoiceId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.SUBSCRIPTION_FIND, credentialValues))
            return error("SUBSCRIPTION.find - credentials error");

        int id = findValues.getInt("id");
        ArrayList invoicesList = _dao().find(id);

        if (invoicesList.size() < 1)
            return error("SUBSCRIPTION.find - could not find Invoices id "+id);
        if (invoicesList.size() > 1)
            return error("SUBSCRIPTION.find - found multiple Invoices records for Invoices id "+id);

        Subscription subscriptions = new Subscription((JSONObject)invoicesList.get(0));
        if (subscriptions == null)
            return error("SUBSCRIPTION.find - subscriptions==null for id "+id);

        return success(subscriptions.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.SUBSCRIPTION_FIND, credentialValues))
            return error("SUBSCRIPTION.findAll - credentials error");

        ArrayList invoicesList = _dao().findAll();
        return jsonFoundList(invoicesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        if (!hasRight(Right.SUBSCRIPTION_FIND, credentialValues))
            return error("SUBSCRIPTION.filter - credentials error");

        // get an array of Invoices json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList invoicesList = _dao().filter(findValues, recordRangeValues);

        boolean hasMoreData = trimList(invoicesList, limit);

        JSONObject jsonList = jsonFoundList(invoicesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }
     *
     */

    public static JSONObject newSubscription(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().newSubscription(createValues, credentialValues);
        if (id == -1)
            return error("SUBSCRIPTION.newSubscription - could not create Invoices record");

        return success("invoiceId", id);
    }

    public static JSONObject renewSubscriptions(JSONObject renewValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int holdingDays = renewValues.getInt("holdingDays");
        ArrayList renewalsList = _dao().renewSubscriptions(holdingDays,credentialValues);

        JSONObject jsonList = jsonFoundList(renewalsList);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Subscription subscriptions = new Subscription((JSONObject)foundList.get(j));
            if (subscriptions == null)
                return JsonError.jsonError(JsonError.Error.SUBSCRIPTION_FIND, "Could not make Subscription object " + j);
            JSONObject json = new JSONObject(subscriptions.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("subscriptions", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        SUBSCRIPTION_CREATE,
        SUBSCRIPTION_RETIRE,
        SUBSCRIPTION_FIND,
        SUBSCRIPTION_UPDATE,

 */