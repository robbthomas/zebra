package com.alleni.finance;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class InvoiceLineItem extends BaseBean implements Serializable
{
    static private InvoiceLineItemDao _dao() { return new InvoiceLineItemDao(); }

    protected InvoiceLineItem(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("invoicelineitemid" ) ? daoJson.getInt("invoicelineitemid") : -1);
        this.jsonValues.put("invoiceId", daoJson.has("invoiceid") ? daoJson.getInt("invoiceid") : -1);
        this.jsonValues.put("lineNumber", daoJson.has("linenumber") ? daoJson.getInt("linenumber") : -1);
        this.jsonValues.put("quantity", daoJson.has("quantity") ? daoJson.getInt("quantity") : -1);
        this.jsonValues.put("amountTotal", daoJson.has("amounttotal") ? daoJson.getInt("amounttotal") : -1);
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("projectId", daoJson.has("projectid") ? daoJson.getString("projectid") : -1);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("INVOICELINEITEM.create - could not create InvoiceLineItems record");

        return success("invoiceLineItemId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("INVOICELINEITEM.update - could not update InvoiceLineItems record");

        return success("invoiceLineItemId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("INVOICELINEITEM.retire - could not retire InvoiceLineItems record");

        return success("invoiceLineItemId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("invoiceLineItemId");
        ArrayList invoiceLineItemsList = _dao().find(id, credentialValues);

        if (invoiceLineItemsList.size() < 1)
            return error("INVOICELINEITEM.find - could not find InvoiceLineItems id "+id);
        if (invoiceLineItemsList.size() > 1)
            return error("INVOICELINEITEM.find - found multiple InvoiceLineItems records for InvoiceLineItems id "+id);

        InvoiceLineItem invoiceLineItems = new InvoiceLineItem((JSONObject)invoiceLineItemsList.get(0));
        if (invoiceLineItems == null)
            return error("INVOICELINEITEM.find - invoiceLineItems==null for id "+id);

        return success(invoiceLineItems.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList invoiceLineItemsList = _dao().findAll(credentialValues);
        return jsonFoundList(invoiceLineItemsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of InvoiceLineItems json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList invoiceLineItemsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(invoiceLineItemsList, limit);

        JSONObject jsonList = jsonFoundList(invoiceLineItemsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            InvoiceLineItem invoiceLineItems = new InvoiceLineItem((JSONObject)foundList.get(j));
            if (invoiceLineItems == null)
                return JsonError.jsonError(JsonError.Error.INVOICELINEITEM_FIND, "Could not make InvoiceLineItems object " + j);
            JSONObject json = new JSONObject(invoiceLineItems.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("invoiceLineItems", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        INVOICELINEITEM_CREATE,
        INVOICELINEITEM_RETIRE,
        INVOICELINEITEM_FIND,
        INVOICELINEITEM_UPDATE,

 */