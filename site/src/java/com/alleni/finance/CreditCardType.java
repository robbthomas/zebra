package com.alleni.finance;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class CreditCardType extends BaseBean implements Serializable
{
    static private CreditCardTypeDao _dao() { return new CreditCardTypeDao(); }

    protected CreditCardType(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("creditcardtypeid" ) ? daoJson.getInt("creditcardtypeid") : -1);
        this.jsonValues.put("tag", daoJson.has("tag") ? daoJson.getString("tag") : "");
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("displayOrder", daoJson.has("displayorder") ? daoJson.getInt("displayorder") : -1);
        this.jsonValues.put("created", daoJson.has("createddatetime") ? (Timestamp)daoJson.get("createddatetime") : new Timestamp(0));
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("CREDITCARDTYPE.create - could not create CreditCardTypes record");

        return success("creditCardTypeId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("CREDITCARDTYPE.update - could not update CreditCardTypes record");

        return success("creditCardTypeId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("CREDITCARDTYPE.retire - could not retire CreditCardTypes record");

        return success("creditCardTypeId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("creditCardTypeId");
        ArrayList creditCardTypesList = _dao().find(id, credentialValues);

        if (creditCardTypesList.size() < 1)
            return error("CREDITCARDTYPE.find - could not find CreditCardTypes id "+id);
        if (creditCardTypesList.size() > 1)
            return error("CREDITCARDTYPE.find - found multiple CreditCardTypes records for CreditCardTypes id "+id);

        CreditCardType creditCardTypes = new CreditCardType((JSONObject)creditCardTypesList.get(0));
        if (creditCardTypes == null)
            return error("CREDITCARDTYPE.find - creditCardTypes==null for id "+id);

        return success(creditCardTypes.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList creditCardTypesList = _dao().findAll(credentialValues);
        return jsonFoundList(creditCardTypesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of CreditCardTypes json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList creditCardTypesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(creditCardTypesList, limit);

        JSONObject jsonList = jsonFoundList(creditCardTypesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            CreditCardType creditCardTypes = new CreditCardType((JSONObject)foundList.get(j));
            if (creditCardTypes == null)
                return JsonError.jsonError(JsonError.Error.CREDITCARDTYPE_FIND, "Could not make CreditCardTypes object " + j);
            JSONObject json = new JSONObject(creditCardTypes.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("creditCardTypes", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        CREDITCARDTYPE_CREATE,
        CREDITCARDTYPE_RETIRE,
        CREDITCARDTYPE_FIND,
        CREDITCARDTYPE_UPDATE,

 */