package com.alleni.finance;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class ECheckDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_echeck_create(?,?,?,?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_echeck_update(?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_echeck_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_echeck_filter(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String FIND        = "{call fn_echeck_find(?,?,?)}";

    public ECheckDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_bankRoutingNumber", new HashMapBuilder<String, String>().add("key", "bankRoutingNumber").add("type", "text").build())
            .add("in_bankAccountNumber", new HashMapBuilder<String, String>().add("key", "bankAccountNumber").add("type", "text").build())
            .add("in_bankName", new HashMapBuilder<String, String>().add("key", "bankName").add("type", "text").build())
            .add("in_bankAccountName", new HashMapBuilder<String, String>().add("key", "bankAccountName").add("type", "text").build())
            .add("in_customerProfileId", new HashMapBuilder<String, String>().add("key", "customerProfileId").add("type", "text").build())
            .add("in_customerPaymentProfileId", new HashMapBuilder<String, String>().add("key", "customerPaymentProfileId").add("type", "text").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_eChecksId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_bankRoutingNumber", new HashMapBuilder<String, String>().add("key", "bankRoutingNumber").add("type", "text").build())
            .add("in_bankAccountNumber", new HashMapBuilder<String, String>().add("key", "bankAccountNumber").add("type", "text").build())
            .add("in_bankName", new HashMapBuilder<String, String>().add("key", "bankName").add("type", "text").build())
            .add("in_bankAccountName", new HashMapBuilder<String, String>().add("key", "bankAccountName").add("type", "text").build())
            .add("in_customerProfileId", new HashMapBuilder<String, String>().add("key", "customerProfileId").add("type", "text").build())
            .add("in_customerPaymentProfileId", new HashMapBuilder<String, String>().add("key", "customerPaymentProfileId").add("type", "text").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_bankRoutingNumber", new HashMapBuilder<String, String>().add("key", "bankRoutingNumber").add("type", "text").build())
            .add("in_bankAccountNumber", new HashMapBuilder<String, String>().add("key", "bankAccountNumber").add("type", "text").build())
            .add("in_bankName", new HashMapBuilder<String, String>().add("key", "bankName").add("type", "text").build())
            .add("in_bankAccountName", new HashMapBuilder<String, String>().add("key", "bankAccountName").add("type", "text").build())
            .add("in_customerProfileId", new HashMapBuilder<String, String>().add("key", "customerProfileId").add("type", "text").build())
            .add("in_customerPaymentProfileId", new HashMapBuilder<String, String>().add("key", "customerPaymentProfileId").add("type", "text").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

}