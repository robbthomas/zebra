package com.alleni.productcode;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class ProductCode extends BaseBean implements Serializable
{
    static private ProductCodeDao _dao() { return new ProductCodeDao(); }

    protected ProductCode(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("productcodeid" ) ? daoJson.getInt("productcodeid") : -1);
        this.jsonValues.put("tag", daoJson.has("tag") ? daoJson.getString("tag") : "");
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("displayOrder", daoJson.has("displayorder") ? daoJson.getInt("displayorder") : -1);
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
        this.jsonValues.put("retiredById", daoJson.has("retiredbyid") ? daoJson.getString("retiredbyid") : "");
        this.jsonValues.put("retiredDateTime", daoJson.has("retireddatetime") ? (Timestamp)daoJson.get("retireddatetime") : new Timestamp(0));

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("PRODUCTCODES.create - could not create ProductCodes record");

        return success("productCodesId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("PRODUCTCODES.update - could not update ProductCodes record");

        return success("productCodesId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("PRODUCTCODES.retire - could not retire ProductCodes record");

        return success("productCodesId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("productCodesId");
        ArrayList productCodesList = _dao().find(id, credentialValues);

        if (productCodesList.size() < 1)
            return error("PRODUCTCODES.find - could not find ProductCodes id "+id);
        if (productCodesList.size() > 1)
            return error("PRODUCTCODES.find - found multiple ProductCodes records for ProductCodes id "+id);

        ProductCode productCodes = new ProductCode((JSONObject)productCodesList.get(0));
        if (productCodes == null)
            return error("PRODUCTCODES.find - productCodes==null for id "+id);

        return success(productCodes.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList productCodesList = _dao().findAll(credentialValues);
        return jsonFoundList(productCodesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of ProductCodes json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList productCodesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(productCodesList, limit);

        JSONObject jsonList = jsonFoundList(productCodesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            ProductCode productCodes = new ProductCode((JSONObject)foundList.get(j));
            if (productCodes == null)
                return JsonError.jsonError(JsonError.Error.PRODUCTCODES_FIND, "Could not make ProductCodes object " + j);
            JSONObject json = new JSONObject(productCodes.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("productCodes", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        PRODUCTCODES_CREATE,
        PRODUCTCODES_RETIRE,
        PRODUCTCODES_FIND,
        PRODUCTCODES_UPDATE,

 */