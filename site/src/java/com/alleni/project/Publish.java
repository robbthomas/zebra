package com.alleni.project;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class Publish extends BaseBean implements Serializable
{
    static private PublishDao _dao() { return new PublishDao(); }

    protected Publish(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("publishid" ) ? daoJson.getInt("publishid") : -1);
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("projectId", daoJson.has("projectid") ? daoJson.getString("projectid") : "");
        this.jsonValues.put("firstPublishId", daoJson.has("firstpublishid") ? daoJson.getInt("firstpublishid") : -1);
        this.jsonValues.put("versionMajor", daoJson.has("versionmajor") ? daoJson.getInt("versionmajor") : -1);
        this.jsonValues.put("versionMinor", daoJson.has("versionminor") ? daoJson.getInt("versionminor") : -1);
        this.jsonValues.put("versionDot", daoJson.has("versiondot") ? daoJson.getInt("versiondot") : -1);
        this.jsonValues.put("versionBuildNumber", daoJson.has("versionbuildnumber") ? daoJson.getInt("versionbuildnumber") : -1);
        this.jsonValues.put("categoryId", daoJson.has("categoryid") ? daoJson.getInt("categoryid") : -1);
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("thumbnailId", daoJson.has("thumbnailid") ? daoJson.getString("thumbnailid") : "");
        this.jsonValues.put("status", daoJson.has("status") ? daoJson.getString("status") : "");
        this.jsonValues.put("publishType", daoJson.has("publishtype") ? daoJson.getString("publishtype") : "");
        this.jsonValues.put("licenseType", daoJson.has("licensetype") ? daoJson.getString("licensetype") : "");
        this.jsonValues.put("price", daoJson.has("price") ? daoJson.getInt("price") : -1);
        this.jsonValues.put("currencyTypeId", daoJson.has("currencytypeid") ? daoJson.getInt("currencytypeid") : -1);
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : -1);
        this.jsonValues.put("created", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
        String hide = daoJson.has("hideinstorelist")? daoJson.getString("hideinstorelist") : "f";
        this.jsonValues.put("hideInStoreList", hide.equals("t")); // DWH
    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("PUBLISH.create - could not create Published record");

        return success("publishId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("PUBLISH.update - could not update Published record");

        return success("publishId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("PUBLISH.retire - could not retire Published record");

        return success("publishId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("publishId");
        ArrayList publishedList = _dao().find(id, credentialValues);

        if (publishedList.size() < 1)
            return error("PUBLISH.find - could not find Published id "+id);
        if (publishedList.size() > 1)
            return error("PUBLISH.find - found multiple Published records for Published id "+id);

        Publish published = new Publish((JSONObject)publishedList.get(0));
        if (published == null)
            return error("PUBLISH.find - published==null for id "+id);

        return success(published.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList publishedList = _dao().findAll(credentialValues);
        return jsonFoundList(publishedList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Published json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList publishedList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(publishedList, limit);

        JSONObject jsonList = jsonFoundList(publishedList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Publish published = new Publish((JSONObject)foundList.get(j));
            if (published == null)
                return JsonError.jsonError(JsonError.Error.PUBLISH_FIND, "Could not make Published object " + j);
            JSONObject json = new JSONObject(published.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("published", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        PUBLISH_CREATE,
        PUBLISH_RETIRE,
        PUBLISH_FIND,
        PUBLISH_UPDATE,

 */