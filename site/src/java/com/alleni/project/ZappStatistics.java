package com.alleni.project;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import org.json.*;

public class ZappStatistics extends BaseBean implements Serializable
{
    static private ZappStatisticsDao _dao() { return new ZappStatisticsDao(); }

    protected ZappStatistics(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("accountId", daoJson.has("accountid" ) ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("zappsCreated", daoJson.has("zappscreated") ? daoJson.getInt("zappscreated") : 0);
        this.jsonValues.put("zappsPublished", daoJson.has("zappspublished") ? daoJson.getInt("zappspublished") : 0);
        this.jsonValues.put("zappsSold", daoJson.has("zappssold") ? daoJson.getInt("zappssold") : 0);
        this.jsonValues.put("zappsRevenue", daoJson.has("zappsrevenue") ? daoJson.getInt("zappsrevenue") : 0);
        this.jsonValues.put("gadgetsCreated", daoJson.has("gadgetscreated") ? daoJson.getInt("gadgetscreated") : 0);
        this.jsonValues.put("gadgetsPublished", daoJson.has("gadgetspublished") ? daoJson.getInt("gadgetspublished") : 0);
        this.jsonValues.put("gadgetsSold", daoJson.has("gadgetssold") ? daoJson.getInt("gadgetssold") : 0);
        this.jsonValues.put("gadgetsRevenue", daoJson.has("gadgetsrevenue") ? daoJson.getInt("gadgetsrevenue") : 0);
        this.jsonValues.put("totalRevenue", daoJson.has("totalrevenue") ? daoJson.getInt("totalrevenue") : 0);
        this.jsonValues.put("convertedCreditBalance", daoJson.has("convertedcreditbalance") ? daoJson.getInt("convertedcreditbalance") : 0);
    }

    public static JSONObject get(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList statsList = _dao().get(credentialValues);
        return jsonFoundList(statsList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int i = 0; i < foundList.size(); i++)
        {
            ZappStatistics stats = new ZappStatistics((JSONObject)foundList.get(i));
            if (stats == null)
                return JsonError.jsonError(JsonError.Error.ZAPPSTATISTICS_GET, "Could not make ZappStatistics object " + i);
            JSONObject json = new JSONObject(stats.toString());
            foundList.set(i, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("statistics", foundList);

        return jsonList;
    }

}
