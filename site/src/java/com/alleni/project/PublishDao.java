package com.alleni.project;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class PublishDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_publish_create(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_publish_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_publish_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_publish_filter(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String FIND        = "{call fn_publish_find(?,?,?)}";

    public PublishDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_projectId", new HashMapBuilder<String, String>().add("key", "projectId").add("type", "text").build())
            .add("in_firstPublishId", new HashMapBuilder<String, String>().add("key", "firstPublishId").add("type", "integer").build())
            .add("in_versionMajor", new HashMapBuilder<String, String>().add("key", "versionMajor").add("type", "integer").build())
            .add("in_versionMinor", new HashMapBuilder<String, String>().add("key", "versionMinor").add("type", "integer").build())
            .add("in_versionDot", new HashMapBuilder<String, String>().add("key", "versionDot").add("type", "integer").build())
            .add("in_versionBuildNumber", new HashMapBuilder<String, String>().add("key", "versionBuildNumber").add("type", "integer").build())
            .add("in_categoryId", new HashMapBuilder<String, String>().add("key", "categoryId").add("type", "integer").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_thumbnailId", new HashMapBuilder<String, String>().add("key", "thumbnailId").add("type", "text").build())
            .add("in_status", new HashMapBuilder<String, String>().add("key", "status").add("type", "text").build())
            .add("in_publishType", new HashMapBuilder<String, String>().add("key", "publishType").add("type", "text").build())
            .add("in_licenseType", new HashMapBuilder<String, String>().add("key", "licenseType").add("type", "text").build())
            .add("in_price", new HashMapBuilder<String, String>().add("key", "price").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_hideInStoreList", new HashMapBuilder<String, String>().add("key", "hideInStoreList").add("type", "boolean").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_publishedId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_projectId", new HashMapBuilder<String, String>().add("key", "projectId").add("type", "text").build())
            .add("in_firstPublishId", new HashMapBuilder<String, String>().add("key", "firstPublishId").add("type", "integer").build())
            .add("in_versionMajor", new HashMapBuilder<String, String>().add("key", "versionMajor").add("type", "integer").build())
            .add("in_versionMinor", new HashMapBuilder<String, String>().add("key", "versionMinor").add("type", "integer").build())
            .add("in_versionDot", new HashMapBuilder<String, String>().add("key", "versionDot").add("type", "integer").build())
            .add("in_versionBuildNumber", new HashMapBuilder<String, String>().add("key", "versionBuildNumber").add("type", "integer").build())
            .add("in_categoryId", new HashMapBuilder<String, String>().add("key", "categoryId").add("type", "integer").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_thumbnailId", new HashMapBuilder<String, String>().add("key", "thumbnailId").add("type", "text").build())
            .add("in_status", new HashMapBuilder<String, String>().add("key", "status").add("type", "text").build())
            .add("in_publishType", new HashMapBuilder<String, String>().add("key", "publishType").add("type", "text").build())
            .add("in_licenseType", new HashMapBuilder<String, String>().add("key", "licenseType").add("type", "text").build())
            .add("in_price", new HashMapBuilder<String, String>().add("key", "price").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_hideInStoreList", new HashMapBuilder<String, String>().add("key", "hideInStoreList").add("type", "boolean").build())

            .add(BaseDAO.EDITED_BY_MEMBER_ID_PARAM_NAME, new HashMapBuilder<String, String>().add("type", "text").add("key", BaseDAO.EDITED_BY_MEMBER_ID_VALUE_NAME).build())
            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_accountId", new HashMapBuilder<String, String>().add("key", "accountId").add("type", "integer").build())
            .add("in_projectId", new HashMapBuilder<String, String>().add("key", "projectId").add("type", "text").build())
            .add("in_firstPublishId", new HashMapBuilder<String, String>().add("key", "firstPublishId").add("type", "integer").build())
            .add("in_versionMajor", new HashMapBuilder<String, String>().add("key", "versionMajor").add("type", "integer").build())
            .add("in_versionMinor", new HashMapBuilder<String, String>().add("key", "versionMinor").add("type", "integer").build())
            .add("in_versionDot", new HashMapBuilder<String, String>().add("key", "versionDot").add("type", "integer").build())
            .add("in_versionBuildNumber", new HashMapBuilder<String, String>().add("key", "versionBuildNumber").add("type", "integer").build())
            .add("in_categoryId", new HashMapBuilder<String, String>().add("key", "categoryId").add("type", "integer").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_description", new HashMapBuilder<String, String>().add("key", "description").add("type", "text").build())
            .add("in_thumbnailId", new HashMapBuilder<String, String>().add("key", "thumbnailId").add("type", "text").build())
            .add("in_status", new HashMapBuilder<String, String>().add("key", "status").add("type", "text").build())
            .add("in_publishType", new HashMapBuilder<String, String>().add("key", "publishType").add("type", "text").build())
            .add("in_licenseType", new HashMapBuilder<String, String>().add("key", "licenseType").add("type", "text").build())
            .add("in_price", new HashMapBuilder<String, String>().add("key", "price").add("type", "integer").build())
            .add("in_currencyTypeId", new HashMapBuilder<String, String>().add("key", "currencyTypeId").add("type", "integer").build())
            .add("in_hideInStoreList", new HashMapBuilder<String, String>().add("key", "hideInStoreList").add("type", "boolean").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

}