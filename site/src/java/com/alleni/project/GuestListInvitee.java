package com.alleni.project;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class GuestListInvitee extends BaseBean implements Serializable
{
    static private GuestListInviteeDao _dao() { return new GuestListInviteeDao(); }

    protected GuestListInvitee(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("guestlistinviteeid" ) ? daoJson.getInt("guestlistinviteeid") : -1);
        this.jsonValues.put("guestListId", daoJson.has("guestlistid") ? daoJson.getInt("guestlistid") : -1);
        this.jsonValues.put("email", daoJson.has("email") ? daoJson.getString("email") : "");
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);
        this.jsonValues.put("retiredById", daoJson.has("retiredbyid") ? daoJson.getString("retiredbyid") : "");
        this.jsonValues.put("retiredDateTime", daoJson.has("retireddatetime") ? (Timestamp)daoJson.get("retireddatetime") : new Timestamp(0));
        this.jsonValues.put("retiredReasonTypeId", daoJson.has("retiredreasontypeid") ? daoJson.getInt("retiredreasontypeid") : -1);
        this.jsonValues.put("uuid", daoJson.has("uuid") ? daoJson.getString("uuid") : "");
        this.jsonValues.put("firstName", daoJson.has("firstname") ? daoJson.getString("firstname") : "");
        this.jsonValues.put("lastName", daoJson.has("lastname") ? daoJson.getString("lastname") : "");

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("GUESTLISTINVITEE.create - could not create GuestListInvitees record");

        return success("guestListInviteeId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("GUESTLISTINVITEE.update - could not update GuestListInvitees record");

        return success("guestListInviteeId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("GUESTLISTINVITEE.retire - could not retire GuestListInvitees record");

        return success("guestListInviteeId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("guestListInviteeId");
        ArrayList guestListInviteesList = _dao().find(id, credentialValues);

        if (guestListInviteesList.size() < 1)
            return error("GUESTLISTINVITEE.find - could not find GuestListInvitees id "+id);
        if (guestListInviteesList.size() > 1)
            return error("GUESTLISTINVITEE.find - found multiple GuestListInvitees records for GuestListInvitees id "+id);

        GuestListInvitee guestListInvitees = new GuestListInvitee((JSONObject)guestListInviteesList.get(0));
        if (guestListInvitees == null)
            return error("GUESTLISTINVITEE.find - guestListInvitees==null for id "+id);

        return success(guestListInvitees.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList guestListInviteesList = _dao().findAll(credentialValues);
        return jsonFoundList(guestListInviteesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of GuestListInvitees json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList guestListInviteesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(guestListInviteesList, limit);

        JSONObject jsonList = jsonFoundList(guestListInviteesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            GuestListInvitee guestListInvitees = new GuestListInvitee((JSONObject)foundList.get(j));
            if (guestListInvitees == null)
                return JsonError.jsonError(JsonError.Error.GUESTLISTINVITEE_FIND, "Could not make GuestListInvitees object " + j);
            JSONObject json = new JSONObject(guestListInvitees.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("guestListInvitees", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        GUESTLISTINVITEE_CREATE,
        GUESTLISTINVITEE_RETIRE,
        GUESTLISTINVITEE_FIND,
        GUESTLISTINVITEE_UPDATE,

 */