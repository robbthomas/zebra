package com.alleni.project;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class ShareType extends BaseBean implements Serializable
{
    static private ShareTypeDao _dao() { return new ShareTypeDao(); }

    protected ShareType(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("sharetypeid" ) ? daoJson.getInt("sharetypeid") : -1);
        this.jsonValues.put("tag", daoJson.has("tag") ? daoJson.getString("tag") : "");
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("displayOrder", daoJson.has("displayorder") ? daoJson.getInt("displayorder") : -1);
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("SHARETYPE.create - could not create ShareTypes record");

        return success("shareTypeId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("SHARETYPE.update - could not update ShareTypes record");

        return success("shareTypeId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("SHARETYPE.retire - could not retire ShareTypes record");

        return success("shareTypeId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("shareTypeId");
        ArrayList ShareTypesList = _dao().find(id, credentialValues);

        if (ShareTypesList.size() < 1)
            return error("SHARETYPE.find - could not find ShareTypes id "+id);
        if (ShareTypesList.size() > 1)
            return error("SHARETYPE.find - found multiple ShareTypes records for ShareTypes id "+id);

        ShareType ShareTypes = new ShareType((JSONObject)ShareTypesList.get(0));
        if (ShareTypes == null)
            return error("SHARETYPE.find - ShareTypes==null for id "+id);

        return success(ShareTypes.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList ShareTypesList = _dao().findAll(credentialValues);
        return jsonFoundList(ShareTypesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of ShareTypes json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList ShareTypesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(ShareTypesList, limit);

        JSONObject jsonList = jsonFoundList(ShareTypesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            ShareType ShareTypes = new ShareType((JSONObject)foundList.get(j));
            if (ShareTypes == null)
                return JsonError.jsonError(JsonError.Error.SHARETYPE_FIND, "Could not make ShareTypes object " + j);
            JSONObject json = new JSONObject(ShareTypes.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("ShareTypes", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        SHARETYPE_CREATE,
        SHARETYPE_RETIRE,
        SHARETYPE_FIND,
        SHARETYPE_UPDATE,

 */