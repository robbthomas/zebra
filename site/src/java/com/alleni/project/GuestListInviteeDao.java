package com.alleni.project;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class GuestListInviteeDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_guestlistinvitee_create(?,?,?,?,?,?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_guestlistinvitee_update(?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_guestlistinvitee_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_guestlistinvitee_filter(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
   private static final String FIND        = "{call fn_guestlistinvitee_find(?,?,?)}";

    public GuestListInviteeDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_guestListId", new HashMapBuilder<String, String>().add("key", "guestListId").add("type", "integer").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_retiredById", new HashMapBuilder<String, String>().add("key", "retiredById").add("type", "text").build())
            .add("in_retiredDateTime", new HashMapBuilder<String, String>().add("key", "retiredDateTime").add("type", "timestamp").build())
            .add("in_retiredReasonTypeId", new HashMapBuilder<String, String>().add("key", "retiredReasonTypeId").add("type", "integer").build())
            .add("in_uuid", new HashMapBuilder<String, String>().add("key", "uuid").add("type", "text").build())
            .add("in_firstName", new HashMapBuilder<String, String>().add("key", "firstName").add("type", "text").build())
            .add("in_lastName", new HashMapBuilder<String, String>().add("key", "lastName").add("type", "text").build())


            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_guestListInviteesId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_guestListId", new HashMapBuilder<String, String>().add("key", "guestListId").add("type", "integer").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_retiredById", new HashMapBuilder<String, String>().add("key", "retiredById").add("type", "text").build())
            .add("in_retiredDateTime", new HashMapBuilder<String, String>().add("key", "retiredDateTime").add("type", "timestamp").build())
            .add("in_retiredReasonTypeId", new HashMapBuilder<String, String>().add("key", "retiredReasonTypeId").add("type", "integer").build())
            .add("in_uuid", new HashMapBuilder<String, String>().add("key", "uuid").add("type", "text").build())
            .add("in_firstName", new HashMapBuilder<String, String>().add("key", "firstName").add("type", "text").build())
            .add("in_lastName", new HashMapBuilder<String, String>().add("key", "lastName").add("type", "text").build())


            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_guestListId", new HashMapBuilder<String, String>().add("key", "guestListId").add("type", "integer").build())
            .add("in_email", new HashMapBuilder<String, String>().add("key", "email").add("type", "text").build())
            .add("in_retiredById", new HashMapBuilder<String, String>().add("key", "retiredById").add("type", "text").build())
            .add("in_retiredDateTime", new HashMapBuilder<String, String>().add("key", "retiredDateTime").add("type", "timestamp").build())
            .add("in_retiredReasonTypeId", new HashMapBuilder<String, String>().add("key", "retiredReasonTypeId").add("type", "integer").build())
            .add("in_uuid", new HashMapBuilder<String, String>().add("key", "uuid").add("type", "text").build())
            .add("in_firstName", new HashMapBuilder<String, String>().add("key", "firstName").add("type", "text").build())
            .add("in_lastName", new HashMapBuilder<String, String>().add("key", "lastName").add("type", "text").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

}