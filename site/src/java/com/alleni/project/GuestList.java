package com.alleni.project;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class GuestList extends BaseBean implements Serializable
{
    static private GuestListDao _dao() { return new GuestListDao(); }

    protected GuestList(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("guestlistid" ) ? daoJson.getInt("guestlistid") : -1);
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);
        this.jsonValues.put("retiredById", daoJson.has("retiredbyid") ? daoJson.getString("retiredbyid") : "");
        this.jsonValues.put("retiredDateTime", daoJson.has("retireddatetime") ? (Timestamp)daoJson.get("retireddatetime") : new Timestamp(0));
        this.jsonValues.put("retiredReasonTypeId", daoJson.has("retiredreasontypeid") ? daoJson.getInt("retiredreasontypeid") : -1);
        this.jsonValues.put("emailText", daoJson.has("emailtext") ? daoJson.getString("emailtext") : "");

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("GUESTLIST.create - could not create GuestLists record");

        return success("guestListId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("GUESTLIST.update - could not update GuestLists record");

        return success("guestListId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("GUESTLIST.retire - could not retire GuestLists record");

        return success("guestListId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("guestListId");
        ArrayList guestListsList = _dao().find(id, credentialValues);

        if (guestListsList.size() < 1)
            return error("GUESTLIST.find - could not find GuestLists id "+id);
        if (guestListsList.size() > 1)
            return error("GUESTLIST.find - found multiple GuestLists records for GuestLists id "+id);

        GuestList guestLists = new GuestList((JSONObject)guestListsList.get(0));
        if (guestLists == null)
            return error("GUESTLIST.find - guestLists==null for id "+id);

        return success(guestLists.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList guestListsList = _dao().findAll(credentialValues);
        return jsonFoundList(guestListsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of GuestLists json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList guestListsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(guestListsList, limit);

        JSONObject jsonList = jsonFoundList(guestListsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            GuestList guestLists = new GuestList((JSONObject)foundList.get(j));
            if (guestLists == null)
                return JsonError.jsonError(JsonError.Error.GUESTLIST_FIND, "Could not make GuestLists object " + j);
            JSONObject json = new JSONObject(guestLists.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("guestLists", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        GUESTLIST_CREATE,
        GUESTLIST_RETIRE,
        GUESTLIST_FIND,
        GUESTLIST_UPDATE,

 */