package com.alleni.project;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class SharedProject extends BaseBean implements Serializable
{
    static private SharedProjectDao _dao() { return new SharedProjectDao(); }

    protected SharedProject(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("sharedprojectid" ) ? daoJson.getInt("sharedprojectid") : -1);
        this.jsonValues.put("publishId", daoJson.has("publishid") ? daoJson.getInt("publishid") : -1);
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
        this.jsonValues.put("updated", daoJson.has("updateddatetime") ? (Timestamp)daoJson.get("updateddatetime") : null);
        this.jsonValues.put("retiredById", daoJson.has("retiredbyid") ? daoJson.getString("retiredbyid") : "");
        this.jsonValues.put("retiredDateTime", daoJson.has("retireddatetime") ? (Timestamp)daoJson.get("retireddatetime") : new Timestamp(0));
        this.jsonValues.put("retiredReasonId", daoJson.has("retiredreasonid") ? daoJson.getInt("retiredreasonid") : -1);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("SHAREDPROJECT.create - could not create SharedProjects record");

        return success("sharedProjectId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("SHAREDPROJECT.update - could not update SharedProjects record");

        return success("sharedProjectId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("SHAREDPROJECT.retire - could not retire SharedProjects record");

        return success("sharedProjectId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("sharedProjectId");
        ArrayList sharedProjectsList = _dao().find(id, credentialValues);

        if (sharedProjectsList.size() < 1)
            return error("SHAREDPROJECT.find - could not find SharedProjects id "+id);
        if (sharedProjectsList.size() > 1)
            return error("SHAREDPROJECT.find - found multiple SharedProjects records for SharedProjects id "+id);

        SharedProject sharedProjects = new SharedProject((JSONObject)sharedProjectsList.get(0));
        if (sharedProjects == null)
            return error("SHAREDPROJECT.find - sharedProjects==null for id "+id);

        return success(sharedProjects.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList sharedProjectsList = _dao().findAll(credentialValues);
        return jsonFoundList(sharedProjectsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of SharedProjects json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList sharedProjectsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(sharedProjectsList, limit);

        JSONObject jsonList = jsonFoundList(sharedProjectsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            SharedProject sharedProjects = new SharedProject((JSONObject)foundList.get(j));
            if (sharedProjects == null)
                return JsonError.jsonError(JsonError.Error.SHAREDPROJECT_FIND, "Could not make SharedProjects object " + j);
            JSONObject json = new JSONObject(sharedProjects.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("sharedProjects", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        SHAREDPROJECT_CREATE,
        SHAREDPROJECT_RETIRE,
        SHAREDPROJECT_FIND,
        SHAREDPROJECT_UPDATE,

 */