package com.alleni.project;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class ZappStatisticsDao extends BaseDAO
{
   private static final String GET = "{call fn_zappstatistics(?,?)}";

    public ArrayList get(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findWithNoParameters(GET, credentialValues);
    }

}