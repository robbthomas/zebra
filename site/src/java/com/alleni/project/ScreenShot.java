package com.alleni.project;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class ScreenShot extends BaseBean implements Serializable
{
    static private ScreenShotDao _dao() { return new ScreenShotDao(); }

    protected ScreenShot(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("screenshotid" ) ? daoJson.getInt("screenshotid") : -1);
        this.jsonValues.put("projectId", daoJson.has("projectid") ? daoJson.getString("projectid") : "");
        this.jsonValues.put("orderNumber", daoJson.has("ordernumber") ? daoJson.getInt("ordernumber") : -1);
        this.jsonValues.put("assetId", daoJson.has("assetid") ? daoJson.getString("assetid") : "");
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : -1);
        this.jsonValues.put("updated", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : null);
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("SCREENSHOT.create - could not create ScreenShots record");

        return success("screenShotId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("SCREENSHOT.update - could not update ScreenShots record");

        return success("screenShotId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("SCREENSHOT.retire - could not retire ScreenShots record");

        return success("screenShotId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("screenShotId");
        ArrayList screenShotsList = _dao().find(id, credentialValues);

        if (screenShotsList.size() < 1)
            return error("SCREENSHOT.find - could not find ScreenShots id "+id);
        if (screenShotsList.size() > 1)
            return error("SCREENSHOT.find - found multiple ScreenShots records for ScreenShots id "+id);

        ScreenShot screenShots = new ScreenShot((JSONObject)screenShotsList.get(0));
        if (screenShots == null)
            return error("SCREENSHOT.find - screenShots==null for id "+id);

        return success(screenShots.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList screenShotsList = _dao().findAll(credentialValues);
        return jsonFoundList(screenShotsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of ScreenShots json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList screenShotsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(screenShotsList, limit);

        JSONObject jsonList = jsonFoundList(screenShotsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            ScreenShot screenShots = new ScreenShot((JSONObject)foundList.get(j));
            if (screenShots == null)
                return JsonError.jsonError(JsonError.Error.SCREENSHOT_FIND, "Could not make ScreenShots object " + j);
            JSONObject json = new JSONObject(screenShots.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("screenShots", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        SCREENSHOT_CREATE,
        SCREENSHOT_RETIRE,
        SCREENSHOT_FIND,
        SCREENSHOT_UPDATE,

 */