package com.alleni.project;

import com.alleni.common.BaseDAO;
import com.alleni.util.HashMapBuilder;
import com.alleni.util.LinkedHashMapBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class ShareTypeDao extends BaseDAO
{
   private static final String CREATE      = "{? = call fn_sharetype_create(?,?,?,?,?)}";
   private static final String UPDATE      = "{? = call fn_sharetype_update(?,?,?,?,?,?,?)}";
   private static final String RETIRE      = "{? = call fn_sharetype_retire(?,?,?,?)}";
   private static final String FILTER      = "{call fn_sharetype_filter(?,?,?,?,?,?,?,?,?)}";
   private static final String FIND        = "{call fn_sharetype_find(?,?,?)}";

    public ShareTypeDao()
    {
        createParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_tag", new HashMapBuilder<String, String>().add("key", "tag").add("type", "text").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_displayOrder", new HashMapBuilder<String, String>().add("key", "displayOrder").add("type", "integer").build())


            .build();
        
        updateParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_ShareTypesId", new HashMapBuilder<String, String>().add("key", "id").add("type", "integer").build())
            
            .add("in_tag", new HashMapBuilder<String, String>().add("key", "tag").add("type", "text").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_displayOrder", new HashMapBuilder<String, String>().add("key", "displayOrder").add("type", "integer").build())


            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();

        // in_count and in_offset are taken care of by super because they come from recordRangeValues instead of crudValues.
        findParameters = new LinkedHashMapBuilder<String, Map>()
            .add("in_tag", new HashMapBuilder<String, String>().add("key", "tag").add("type", "text").build())
            .add("in_name", new HashMapBuilder<String, String>().add("key", "name").add("type", "text").build())
            .add("in_displayOrder", new HashMapBuilder<String, String>().add("key", "displayOrder").add("type", "integer").build())

            .add("in_param_list", new HashMapBuilder<String, String>().add("key", "").add("type", "text").build())
            .build();
    }

    public int create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return create(CREATE, createValues, credentialValues);
    }

    public int update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return update(UPDATE, updateValues, credentialValues);
    }

    public int retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        return retire(RETIRE, retireValues, credentialValues);
    }

    public ArrayList find(int id, JSONObject credentialValues) throws SQLException, JSONException
    {
        return findByOneParameter(FIND, id, credentialValues);
    }

    public ArrayList filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        JSONObject filterValues = findValues.has("filters") ? (JSONObject)findValues.get("filters") : new JSONObject();
        return filter(FILTER, filterValues, recordRangeValues, credentialValues);
    }

    public ArrayList findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        return findAll(FILTER, credentialValues);
    }

}