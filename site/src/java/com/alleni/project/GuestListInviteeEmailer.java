package com.alleni.project;

import java.util.HashMap;
import java.util.Map;

import com.alleni.common.BaseBean;
import com.alleni.template.ClosureUtil;
import com.alleni.util.Mail;

public class GuestListInviteeEmailer {

	public static void sendInvite(Map<String, Object> params, String baseUrl) throws Exception {
		// private-publishing #10, #11
		// send email based on whether or not the invitee is a registered user
		String firstName = null;
		String lastName = null;
		boolean isMember = false;
		String email = (String)params.get("email");

		String userBody = "";
		if (params.containsKey("userBody")) {
			userBody = (String)params.get("userBody");
		}


		// first find out if they've got an account
		Map<String,String> names = BaseBean.getAppUserFirstAndLastNamesFromEmail(email);
		if (names.containsKey("firstName")) firstName = names.get("firstName");
		if (names.containsKey("lastName")) lastName = names.get("lastName");

		if (firstName == null && lastName == null) {
			// use values from invitee record
			firstName = params.containsKey("firstName")? (String)params.get("firstName") : "";
			lastName = params.containsKey("lastName")? (String)params.get("lastName") : "";

		} else {
			isMember = true;
		}



		Map<String,String> authorNames = null;
		if (params.containsKey("authorEmail")) {
			authorNames = BaseBean.getAppUserFirstAndLastNamesFromEmail((String)params.get("authorEmail"));
		}

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("url", baseUrl);
		args.put("userBody", userBody);

		String projectId = (String)params.get("publishId");
		args.put("projectId", String.valueOf(projectId));

		if (authorNames != null) {
			args.put("authorFirstName", authorNames.get("firstName"));
		}

		String body = "undefined";
		String bodyHTML = "undefined";
		String subject = "undefined";
		if (isMember) {
			body = ClosureUtil.render("inviteMember.soy",
					"services.sharedprojectinvitee.create.member", "plaintext", args);
			bodyHTML = ClosureUtil.render("inviteMember.soy",
					"services.sharedprojectinvitee.create.member", "html", args);
			subject = ClosureUtil.render("inviteMember.soy",
					"services.sharedprojectinvitee.create.member", "subject", args);
		} else {
			body = ClosureUtil.render("inviteNonMember.soy",
					"services.sharedprojectinvitee.create.nonmember", "plaintext", args);
			bodyHTML = ClosureUtil.render("inviteNonMember.soy",
					"services.sharedprojectinvitee.create.nonmember", "html", args);
			subject = ClosureUtil.render("inviteNonMember.soy",
					"services.sharedprojectinvitee.create.nonmember", "subject", args);
		}

		// send email
		Mail mail = new Mail();
		mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.ecommerce"));
		mail.sendMultiPartMessage("donotreply@zebrazapps.com", email, subject, body, bodyHTML);


	}
}
