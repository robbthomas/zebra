package com.alleni.project;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class Category extends BaseBean implements Serializable
{
    static private CategoryDao _dao() { return new CategoryDao(); }

    protected Category(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("categoryid" ) ? daoJson.getInt("categoryid") : -1);
        this.jsonValues.put("parentCategoryId", daoJson.has("parentcategoryid") ? daoJson.getInt("parentcategoryid") : -1);
        this.jsonValues.put("name", daoJson.has("name") ? daoJson.getString("name") : "");
        this.jsonValues.put("description", daoJson.has("description") ? daoJson.getString("description") : "");
        this.jsonValues.put("typeId", daoJson.has("typeid") ? daoJson.getInt("typeid") : -1);
        this.jsonValues.put("categoryOrder", daoJson.has("categoryorder") ? daoJson.getInt("categoryorder") : -1);
        this.jsonValues.put("categoryLevel", daoJson.has("categorylevel") ? daoJson.getInt("categorylevel") : -1);
        this.jsonValues.put("editedById", daoJson.has("editedbyid") ? daoJson.getString("editedbyid") : -1);
        this.jsonValues.put("created", daoJson.has("editeddatetime") ? (Timestamp)daoJson.get("editeddatetime") : new Timestamp(0));
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("CATEGORY.create - could not create Categories record");

        return success("categoryId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("CATEGORY.update - could not update Categories record");

        return success("categoryId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("CATEGORY.retire - could not retire Categories record");

        return success("categoryId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("categoryId");
        ArrayList categoriesList = _dao().find(id, credentialValues);

        if (categoriesList.size() < 1)
            return error("CATEGORY.find - could not find Categories id "+id);
        if (categoriesList.size() > 1)
            return error("CATEGORY.find - found multiple Categories records for Categories id "+id);

        Category categories = new Category((JSONObject)categoriesList.get(0));
        if (categories == null)
            return error("CATEGORY.find - categories==null for id "+id);

        return success(categories.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList categoriesList = _dao().findAll(credentialValues);
        return jsonFoundList(categoriesList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of Categories json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList categoriesList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(categoriesList, limit);

        JSONObject jsonList = jsonFoundList(categoriesList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            Category categories = new Category((JSONObject)foundList.get(j));
            if (categories == null)
                return JsonError.jsonError(JsonError.Error.CATEGORY_FIND, "Could not make Categories object " + j);
            JSONObject json = new JSONObject(categories.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("categories", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        CATEGORY_CREATE,
        CATEGORY_RETIRE,
        CATEGORY_FIND,
        CATEGORY_UPDATE,

 */