package com.alleni.project;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class PublishLaunchCount extends BaseBean implements Serializable
{
    static private PublishLaunchCountDao _dao() { return new PublishLaunchCountDao(); }

    protected PublishLaunchCount(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("id", daoJson.has("publishlaunchcountid" ) ? daoJson.getInt("publishlaunchcountid") : -1);
        this.jsonValues.put("publishId", daoJson.has("publishid") ? daoJson.getInt("publishid") : -1);
        this.jsonValues.put("parentPublishId", daoJson.has("parentpublishid") ? daoJson.getInt("parentpublishid") : -1);
        this.jsonValues.put("launchedByAccountId", daoJson.has("launchedbyaccountid") ? daoJson.getInt("launchedbyaccountid") : -1);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().create(createValues, credentialValues);
        if (id == -1)
            return error("PUBLISHLAUNCHCOUNT.create - could not create PublishLaunchCounts record");

        return success("publishLaunchCountId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("PUBLISHLAUNCHCOUNT.update - could not update PublishLaunchCounts record");

        return success("publishLaunchCountId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("PUBLISHLAUNCHCOUNT.retire - could not retire PublishLaunchCounts record");

        return success("publishLaunchCountId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = findValues.getInt("publishLaunchCountId");
        ArrayList publishLaunchCountsList = _dao().find(id, credentialValues);

        if (publishLaunchCountsList.size() < 1)
            return error("PUBLISHLAUNCHCOUNT.find - could not find PublishLaunchCounts id "+id);
        if (publishLaunchCountsList.size() > 1)
            return error("PUBLISHLAUNCHCOUNT.find - found multiple PublishLaunchCounts records for PublishLaunchCounts id "+id);

        PublishLaunchCount publishLaunchCounts = new PublishLaunchCount((JSONObject)publishLaunchCountsList.get(0));
        if (publishLaunchCounts == null)
            return error("PUBLISHLAUNCHCOUNT.find - publishLaunchCounts==null for id "+id);

        return success(publishLaunchCounts.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList publishLaunchCountsList = _dao().findAll(credentialValues);
        return jsonFoundList(publishLaunchCountsList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of PublishLaunchCounts json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList publishLaunchCountsList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(publishLaunchCountsList, limit);

        JSONObject jsonList = jsonFoundList(publishLaunchCountsList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            PublishLaunchCount publishLaunchCounts = new PublishLaunchCount((JSONObject)foundList.get(j));
            if (publishLaunchCounts == null)
                return JsonError.jsonError(JsonError.Error.PUBLISHLAUNCHCOUNT_FIND, "Could not make PublishLaunchCounts object " + j);
            JSONObject json = new JSONObject(publishLaunchCounts.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("publishLaunchCounts", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        PUBLISHLAUNCHCOUNT_CREATE,
        PUBLISHLAUNCHCOUNT_RETIRE,
        PUBLISHLAUNCHCOUNT_FIND,
        PUBLISHLAUNCHCOUNT_UPDATE,

 */