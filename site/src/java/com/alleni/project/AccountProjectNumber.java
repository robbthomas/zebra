package com.alleni.project;

import com.alleni.common.BaseBean;
import com.alleni.common.JsonError;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.json.*;

public class AccountProjectNumber extends BaseBean implements Serializable
{
    static private AccountProjectNumberDao _dao() { return new AccountProjectNumberDao(); }

    protected AccountProjectNumber(JSONObject daoJson) throws JSONException
    {
        this.jsonValues.put("accountProjectNumberId", daoJson.has("accountprojectnumberid" ) ? daoJson.getString("accountprojectnumberid") : "");
        this.jsonValues.put("accountId", daoJson.has("accountid") ? daoJson.getInt("accountid") : -1);
        this.jsonValues.put("projectId", daoJson.has("projectid") ? daoJson.getString("projectid") : -1);
        this.jsonValues.put("invoiceLineItemId", daoJson.has("invoicelineitemid") ? daoJson.getInt("invoicelineitemid") : -1);
        this.jsonValues.put("retired", daoJson.has("retired") ? daoJson.getInt("retired") : 0);
        this.jsonValues.put("retiredReasonId", daoJson.has("retiredreasonid") ? daoJson.getInt("retiredreasonid") : -1);

    }

    public static JSONObject create(JSONObject createValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        String id = _dao().create(createValues, credentialValues);
        if (id.length() <= 0)
            return error("ACCOUNTPROJECTNUMBER.create - could not create AccountProjectNumbers record");

        return success("accountProjectNumberId", id);
    }

    public static JSONObject update(JSONObject updateValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().update(updateValues, credentialValues);
        if (id == -1)
            return error("ACCOUNTPROJECTNUMBER.update - could not update AccountProjectNumbers record");

        return success("accountProjectNumberId", id);
    }

    public static JSONObject retire(JSONObject retireValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        int id = _dao().retire(retireValues, credentialValues);
        if (id == -1)
            return error("ACCOUNTPROJECTNUMBER.retire - could not retire AccountProjectNumbers record");

        return success("accountProjectNumberId", id);
    }

    public static JSONObject find(JSONObject findValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        String id = findValues.getString("accountProjectNumberId");
        ArrayList accountProjectNumbersList = _dao().find(id, credentialValues);

        if (accountProjectNumbersList.size() < 1)
            return error("ACCOUNTPROJECTNUMBER.find - could not find AccountProjectNumbers id "+id);
        if (accountProjectNumbersList.size() > 1)
            return error("ACCOUNTPROJECTNUMBER.find - found multiple AccountProjectNumbers records for AccountProjectNumbers id "+id);

        AccountProjectNumber accountProjectNumbers = new AccountProjectNumber((JSONObject)accountProjectNumbersList.get(0));
        if (accountProjectNumbers == null)
            return error("ACCOUNTPROJECTNUMBER.find - accountProjectNumbers==null for id "+id);

        return success(accountProjectNumbers.toString());
    }

    // Don't expose this to Web Services. It should only be called by other Java
    // objects when creating an aggregate of several Web Service objects.
    public static JSONObject findAll(JSONObject credentialValues) throws SQLException, JSONException
    {
        ArrayList accountProjectNumbersList = _dao().findAll(credentialValues);
        return jsonFoundList(accountProjectNumbersList);
    }

    public static JSONObject filter(JSONObject findValues, JSONObject recordRangeValues, JSONObject credentialValues) throws SQLException, JSONException
    {
        // get an array of AccountProjectNumbers json objects
        // ask for one more than requested and return hasMoreData accordingly...
        int limit = recordRangeValues.getInt("count"); // TODO - use 'limit' instead of 'count' in json objects
        recordRangeValues.put("count", recordRangeValues.getInt("count") + 1);
        ArrayList accountProjectNumbersList = _dao().filter(findValues, recordRangeValues, credentialValues);

        boolean hasMoreData = trimList(accountProjectNumbersList, limit);

        JSONObject jsonList = jsonFoundList(accountProjectNumbersList);
        jsonList.put("hasMoreData", hasMoreData);
        return success(jsonList);
    }

    protected static JSONObject jsonFoundList(ArrayList foundList) throws JSONException
    {
        for (int j = 0; j < foundList.size(); j++)
        {
            AccountProjectNumber accountProjectNumbers = new AccountProjectNumber((JSONObject)foundList.get(j));
            if (accountProjectNumbers == null)
                return JsonError.jsonError(JsonError.Error.ACCOUNTPROJECTNUMBER_FIND, "Could not make AccountProjectNumbers object " + j);
            JSONObject json = new JSONObject(accountProjectNumbers.toString());
            foundList.set(j, json);
        }

        JSONObject jsonList = new JSONObject();
        jsonList.put("accountProjectNumbers", foundList);

        return jsonList;
    }

}

/*
 PUT IN com.alleni.common.jsonError.java

        ACCOUNTPROJECTNUMBER_CREATE,
        ACCOUNTPROJECTNUMBER_RETIRE,
        ACCOUNTPROJECTNUMBER_FIND,
        ACCOUNTPROJECTNUMBER_UPDATE,

 */