package com.alleni.template;

import java.io.File;
import java.net.URL;
import java.util.Map;

import com.google.template.soy.SoyFileSet;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.tofu.SoyTofu;


public class ClosureUtil {

	/**
	 * One-line wrapper for Google Closure framework.
	 * 
	 * Template files live in src/java/com/alleni/template.
	 * 
	 * 
	 * @param fileName name of template file, e.g. joinWelcome.soy.  Should mirror the name of the jsp that calls it.
	 * @param namespace generally "path to the servlet" + message intent e.g. services.join.create.welcome
	 * @param templateName as defined in the template file, for our uses should be one of "plaintext" or "html".
	 * @param args <code>HashMap</code> of name-value pairs required by the template
	 * @return formatted text
	 */
	public static String render(String fileName, String namespace, String templateName, Map<String,Object> args) {
		// no idea why google makes us do this
		templateName = "." + templateName;
		
		URL url =  ClosureUtil.class.getClassLoader().getResource("/com/alleni/template/" + fileName);
		String absPath = url.getFile();
		File file = new File(absPath);
		
		SoyFileSet sfs = (new SoyFileSet.Builder()).add(file).build();

		SoyTofu tofu = sfs.compileToJavaObj();
		
	    SoyTofu simpleTofu = tofu.forNamespace(namespace);

	    String ret = simpleTofu.newRenderer(templateName).setData(new SoyMapData(args)).render();
	    
		return ret;
	}
}
