#!/bin/sh
#
# Run this script as the default user in that user's home directory
# or subfolder where user has full privileges.
#
# Script devtools.sh assumes production.sh has been run successfully.
#
# Some of the pieces installed below may be redundant but should simply
# not install if the product script has indeed installed them.

# install default packages
#
sudo apt-get -y install ssh
sudo apt-get -y install subversion
sudo apt-get -y install postgresql
sudo apt-get -y install pgadmin
sudo apt-get -y install filezilla
sudo apt-get -y install curl

# Download full featured netbeans 6.9.1 installer and launch.
# NOTE that to support another version of Netbeans such as 7.0 that the
# installer will first need to be run on an instance of the target platform
# and have a state file (the xml file) recorded. Download that version to
# a target version and run as follows:
#
# Example:
# wget http://download.netbeans.org/netbeans/7.0/final/bundles/netbeans-7.0-ml-linux.sh
# sh netbeans-7.0-ml-linux.sh --record netbeans-7.0-state.xml
#
# The xml file can then be added to the setup folder where this script is and
# the version to install can be updated or changed as needed below.
#
NBVERSION="6.9.1"
#NBVERSION="7.0"
wget http://download.netbeans.org/netbeans/${NBVERSION}/final/bundles/netbeans-${NBVERSION}-ml-linux.sh
chmod +x netbeans-${NBVERSION}-ml-linux.sh

# The xml driving the install was recorded as user qpi. Modify all occurrences of qpi
# in place to match the current default user and run the installer.
sed -i -e "s/qpi/`WHOAMI`/g" netbeans-${NBVERSION}-state.xml
sh netbeans-${NBVERSION}-ml-linux.sh --silent --state netbeans-${NBVERSION}-state.xml
