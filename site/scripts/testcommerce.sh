#!/bin/bash
#
# Call each of the 6 commerce cronN webservices
#
curl --verbose --user eCommCron:o4vBnE4Q11 --data "json={\"commerceCron1\":{\"holdingDays\":7}}" http://localhost:8084/services/commerce/cron1.jsp

sleep 1

curl --verbose --user eCommCron:o4vBnE4Q11 --data "json={\"commerceCron2\":{\"accountId\":-1}}" http://localhost:8084/services/commerce/cron2.jsp

sleep 1

curl --verbose --user eCommCron:o4vBnE4Q11 --data "json={\"commerceCron3\":{}}" http://localhost:8084/services/commerce/cron3.jsp

sleep 1

curl --verbose --user eCommCron:o4vBnE4Q11 --data "json={\"commerceCron4\":{\"holdingDays\":21}}" http://localhost:8084/services/commerce/cron4.jsp

sleep 1

curl --verbose --user eCommCron:o4vBnE4Q11 --data "json={\"commerceCron5\":{\"accountId\":-1}}" http://localhost:8084/services/commerce/cron5.jsp

sleep 1

curl --verbose --user eCommCron:o4vBnE4Q11 --data "json={\"commerceCron6\":{\"withinDays\":9}}" http://localhost:8084/services/commerce/cron6.jsp

