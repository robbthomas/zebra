var ProjectFeedbackProxy = function() {
    this.Extends = Proxy;

    this.initialize = function(proxyName) {
        this.parent((proxyName != undefined)?proxyName:ProjectFeedbackProxy.NAME, new Object());
    }
    
    this.projectFeedback = function() {
    	return this.getData();
    }
    
    this.loadByProjectId = function(projectId) {
    	var payload = {
    			findByProjectId: {projectId:projectId}
    	};
    	
        var _this = this;
        WebService.request({
            payload: payload,
            pathFilename: 'projectFeedback/find.jsp',
            callbacks: {
                success: function (data) {
                	_this.sendNotification(ProjectFeedbackProxy.LOAD_SUCCESS,data);
                },
                failure: function (error, detail) {
                	_this.sendNotification(ProjectFeedbackProxy.LOAD_FAILURE,null);
                }
            }
        });
    }

    this.flagZapp = function(comment, projectId) {
        var _this = this;
    	_this.save(0, projectId,'flag',comment);
    }
    
    this.load = function(projectFeedbackId) {
    	var payload = {
    			projectFeedbackFind: {projectFeedbackId:projectFeedbackId}
    	};
        
        var _this = this;
        WebService.request({
            payload: payload,
            pathFilename: 'projectFeedback/find.jsp',
            callbacks: {
                success: function (data) {
                	_this.sendNotification(ProjectFeedbackProxy.LOAD_SUCCESS,data);
                },
                failure: function (error, detail) {
                	_this.sendNotification(ProjectFeedbackProxy.LOAD_FAILURE,_this);
                }
            }
        });
        //return _this.data;
    }
    
    this.save = function(projectID,feedbackType,flagReason) {
    	var payload;
    	var pathFilename = "";
    	// toggle create/update
		payload = {
				projectFeedbackCreate: {
					projectFeedbackId:0, // Existing pattern expects a zero here for CREATE operations
					projectID:projectID, 
					feedbackType: feedbackType, 
					flaggedByEmail: Credentials.email(),
					flagReason: flagReason
				}
		};
		pathFilename = 'projectfeedback/create.jsp';
        // service call
        var _this = this;
        WebService.request({
            payload: payload,
            pathFilename: pathFilename,
            callbacks: {
                success: function (data) {
                    _this.sendNotification(ProjectFeedbackProxy.SAVE_COMPLETE,_this);
                },
                failure: function (error, detail) {
                	_this.sendNotification(ProjectFeedbackProxy.SAVE_FAILURE,_this);
                }
            }
        });
        
        payload = {
        		projectFeedbackNotification: {
        			projectID:projectID,
        			flagReason:flagReason,
        			flaggedByEmail: Credentials.email()
        		}
        };
        pathFilename = 'projectfeedback/sendFlagNotification.jsp';
        WebService.request({
            payload: payload,
            pathFilename: pathFilename,
            callbacks: {
                success: function (data) {
                    _this.sendNotification(ProjectFeedbackProxy.NOTIFICATION_COMPLETE,_this);
                },
                failure: function (error, detail) {
                	_this.sendNotification(ProjectFeedbackProxy.NOTIFICATION_FAILURE,_this);
                }
            }
        });
    }

}
ProjectFeedbackProxy = new Class(new ProjectFeedbackProxy());
ProjectFeedbackProxy.NAME = 'ProjectFeedbackProxy';
ProjectFeedbackProxy.SAVE_COMPLETE = 'ProjectFeedbackProxy.SAVE_COMPLETE';
ProjectFeedbackProxy.SAVE_FAILURE = 'ProjectFeedbackProxy.SAVE_FAILURE';
ProjectFeedbackProxy.LOAD_SUCCESS = 'ProjectFeedbackProxy.LOAD_SUCCESS';
ProjectFeedbackProxy.LOAD_FAILURE = 'ProjectFeedbackProxy.LOAD_FAILURE';
ProjectFeedbackProxy.NOTIFICATION_COMPLETE = 'ProjectFeedbackProxy.NOTIFICATION_COMPLETE';
ProjectFeedbackProxy.NOTIFICATION_FAILURE = 'ProjectFeedbackProxy.NOTIFICATION_FAILURE';
