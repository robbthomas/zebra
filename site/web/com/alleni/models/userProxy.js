var UserProxy = function()
{
    this.Extends = BaseProxy;
    this.tests = null;

    this.initialize = function()
    {
        this.parent(UserProxy.NAME, new Object());
    }
    this.objectToValues = function (object, mode)
    {
        var v = {}
        return v;
    }
    this.valuesToObject = function (values, mode)
    {
        var o = {};
        return o;
    }
    this.logout = function ()
    {
        // reset gracefully to default visitor view
        Credentials.visitor();
        this.sendNotification(UserProxy.LOGOUT_SUCCESS);
    }
    this.myProfile = function ()
    {
        
    }
    this.logout = function (o)
    {
        // reset gracefully to default visitor view
        Credentials.visitor();
        Common.reloadApplication();
        //this.sendNotification(UserProxy.LOGOUT_SUCCESS);
    }
    this.login = function (o)
    {
        // var username = o.username;
        // var password = o.password;
        // var keepSignedIn = o.keepSignedIn;
        
        var that = this;
        WebService.request({
            payload: {
                appuserLogin: {
                    accounts: {
                        offset: 0, 
                        count: 10
                    }
                }
            },
            pathFilename: 'appuser/login.jsp',
            callbacks: {
                success: function (data) {
                    if (data.authenticated)
                    {
                        if (data.id == undefined)
                            Log.error('UserProxy.login.success - accountId:undefined');
                        Credentials.set({
                            accountAdministrator: data.systemAdministrator,
                            accountId: data.accountid, 
                            authenticated: true, 
                            email: data.email,
                            keepSignedIn: o.keepSignedIn,
                            password: o.password, 
                            systemAdministrator: data.systemAdministrator,
                            accountTypeTag: data.accounttypetag,
                            userId: data.userId,
                            username: o.email,
	                        displayName: data.displayName,
                            firstName: data.firstName,
                            lastName: data.lastName,
                            nameFormat: data.nameFormat,
                            hasCreditCardProfile: data.hasCreditCardProfile,
                            goodStanding: data.accountgoodstanding,
                            termDueDate: Common.parseDate(data.accounttermduedate.split(' ')[0])
                        });
                        that.sendNotification(UserProxy.LOGIN_SUCCESS, data);
                    }
                    else
                        that.sendNotification(UserProxy.LOGIN_FAILURE);
                },
                failure: function (error, detail) {
                    that.sendNotification(UserProxy.LOGIN_BAD_CREDENTIALS);
                }
            }
        }, {username: o.email, password: o.password});
    }
    this.refresh = function (o)
    {
        // var username = o.username;
        // var password = o.password;
        // var keepSignedIn = o.keepSignedIn;
        
        var that = this;
        WebService.request({
            payload: {
                appuserLogin: {
                    accounts: {
                        offset: 0, 
                        count: 10
                    }
                }
            },
            pathFilename: 'appuser/login.jsp',
            callbacks: {
                success: function (data) {
                    if (data.authenticated)
                    {
                        if (data.id == undefined)
                            Log.error('UserProxy.login.success - accountId:undefined');
                        Credentials.set({
                            accountAdministrator: data.title == 'admin account',
                            accountId: data.accountid, 
                            authenticated: true, 
                            email: data.email,
                            keepSignedIn: o.keepSignedIn,
                            password: o.password, 
                            systemAdministrator: data.title == 'admin account',  //TODO acavan - get real sys admin role 
                            accountTypeTag: data.accounttypetag,
                            userId: data.userId,
                            username: o.email,
	                        displayName: data.displayName,
                            firstName: data.firstName,
                            lastName: data.lastName,
                            nameFormat: data.nameFormat,
                            hasCreditCardProfile: data.hasCreditCardProfile,
                            goodStanding: data.accountgoodstanding,
                            termDueDate: Common.parseDate(data.accounttermduedate.split(' ')[0])
                        });
                        that.sendNotification(UserProxy.REFRESH_SUCCESS, data);
                    }
                    else
                        that.sendNotification(UserProxy.REFRESH_FAILURE);
                },
                failure: function (error, detail) {
                    that.sendNotification(UserProxy.REFRESH_BAD_CREDENTIALS);
                }
            }
        }, {username: o.email, password: o.password});
    }
    this.resetPassword = function(email)
    {
        var that = this;
        WebService.request({
            payload: {
                appUserResetPassword: {
                    email: email
                }
            },
            pathFilename: 'appuser/resetPassword.jsp',
            callbacks: {
                success: function(data) {
                    if (data.userFound)
                        that.sendNotification(UserProxy.RESET_PASSWORD_SUCCESS, email);
                    else
                        that.sendNotification(UserProxy.RESET_PASSWORD_USER_NOT_FOUND, email);
                },
                failure: function(error, detail) {
                    that.sendNotification(UserProxy.RESET_PASSWORD_FAILURE, detail);
                    
                }
            }
        });
    }
    
    this.upgrade = function(upgradeTo)
    {
    	var that = this;
    	WebService.request({
    		payload: {
    			accountUpdate:{
    				group: 'upgrade',
    				type: upgradeTo,
    				values: {}
    			}
    		},
    		pathFilename: 'account/updateExtended.jsp',
    		callbacks: {
    			success: function(data){
    				 that.sendNotification(UserProxy.UPGRADE_DOWNGRADE_SUCCESS, data);
    			},
    			failure: function(error, detail){
    				 that.sendNotification(UserProxy.UPGRADE_DOWNGRADE_FAILURE, {error:error, detail:detail});
    			}
    		}
    	})
    }

    this.usernameUnique = function(username)
    {
        var that = this;
        WebService.request({
            payload: {
                appUserUsernameUnique: {
                    displayName: username
                }
            },
            pathFilename: 'appuser/usernameUnique.jsp',
            callbacks: {
                success: function(data) {
                    that.sendNotification(UserProxy.USERNAME_UNIQUE_SUCCESS, data);
                },
                failure: function(error, detail) {
                    that.sendNotification(UserProxy.USERNAME_UNIQUE_FAILURE, detail);
                }
            }
        });
    }
    this.emailUnique = function(email)
    {
        var that = this;
        WebService.request({
            payload: {
                appUserEmailUnique: {
                    email: email
                }
            },
            pathFilename: 'appuser/emailUnique.jsp',
            callbacks: {
                success: function(data) {
                    that.sendNotification(UserProxy.EMAIL_UNIQUE_SUCCESS, data);
                },
                failure: function(error, detail) {
                    that.sendNotification(UserProxy.EMAIL_UNIQUE_FAILURE, detail);
                }
            }
        });
    }
    this.confirmUserEmail = function(payload)
    {
    	var that = this;
    	WebService.request({
    		host: 'zephyr',
            pathFilename: 'user?email=' + encodeURIComponent(payload),
            payload:{},
            callbacks: {
                success: function(data) {
                	that.sendNotification(UserProxy.CONFIRM_EMAIL_SUCCESS, data);
                },
                failure: function(error, detail) {
                    that.sendNotification(UserProxy.CONFIRM_EMAIL_FAILURE, detail);
                }
            }
        });
    }
    
    this.getAuthorProfileData = function(appUserId)
    {
    	var that = this;
    	WebService.request({
    		pathFilename: 'appuser/getAuthorProfileData.jsp',
    		payload:{id: appUserId},
    		callbacks: {
    			success: function(data) {
    				that.sendNotification(UserProxy.GET_AUTHOR_PROFILE_DATA_SUCCESS, data);
    			}, 
    			failure: function(error, detail) {
    				that.sendNotification(UserProxy.GET_AUTHOR_PROFILE_DATA_FAILURE, detail);
    			}
    		}
    	});
    }
    
    this.getAuthorAvatarImage = function(avatarId, token) {
    	var successNotification = (undefined == token) ? UserProxy.GET_AUTHOR_AVATAR_SUCCESS : UserProxy.GET_AUTHOR_AVATAR_SUCCESS + token;
    	var failureNotification = (undefined == token) ? UserProxy.GET_AUTHOR_AVATAR_FAILURE : UserProxy.GET_AUTHOR_AVATAR_FAILURE + token;
    	
    	this.zephyrRequest(
    		  'uploadableAsset/' + avatarId
    		, {}
    		, successNotification
    		, failureNotification
    	);
    }
    
    this.getAuthorBannerImage = function(bannerId, token) {
    	var successNotification = (undefined == token) ? UserProxy.GET_AUTHOR_BANNER_SUCCESS : UserProxy.GET_AUTHOR_BANNER_SUCCESS + token;
    	var failureNotification = (undefined == token) ? UserProxy.GET_AUTHOR_BANNER_FAILURE : UserProxy.GET_AUTHOR_BANNER_FAILURE + token;
    	
    	this.zephyrRequest(
    		  'uploadableAsset/' + bannerId
    		, {}
    		, successNotification
    		, failureNotification
    	);
    }
}
UserProxy = new Class(new UserProxy());
UserProxy.NAME = 'UserProxy';
UserProxy.LOGIN_FAILURE = 'UserProxy.LOGIN_FAILURE';
UserProxy.LOGIN_BAD_CREDENTIALS = 'UserProxy.LOGIN_BAD_CREDENTIALS';
UserProxy.REFRESH_BAD_CREDENTIALS = 'UserProxy.LOGIN_BAD_CREDENTIALS';
UserProxy.LOGIN_SUCCESS = 'UserProxy.LOGIN_SUCCESS';
UserProxy.REFRESH_SUCCESS = 'UserProxy.REFRESH_SUCCESS';
UserProxy.REFRESH_FAILURE = 'UserProxy.REFRESH_FAILURE';
UserProxy.LOGOUT_SUCCESS = 'UserProxy.LOGOUT_SUCCESS';
UserProxy.REQUEST_MYSTUFF_FAILURE = "UserProxy.REQUEST_MYSTUFF_FAILURE";
UserProxy.RESET_PASSWORD_SUCCESS = 'UserProxy.RESET_PASSWORD_SUCCESS';
UserProxy.RESET_PASSWORD_FAILURE = 'UserProxy.RESET_PASSWORD_FAILURE';
UserProxy.RESET_PASSWORD_USER_NOT_FOUND = 'UserProxy.RESET_PASSWORD_USER_NOT_FOUND';
UserProxy.USERNAME_UNIQUE_SUCCESS = 'UserProxy.USERNAME_UNIQUE_SUCCESS';
UserProxy.USERNAME_UNIQUE_FAILURE = 'UserProxy.USERNAME_UNIQUE_FAILURE';
UserProxy.EMAIL_UNIQUE_SUCCESS = 'UserProxy.EMAIL_UNIQUE_SUCCESS';
UserProxy.EMAIL_UNIQUE_FAILURE = 'UserProxy.EMAIL_UNIQUE_FAILURE';
UserProxy.UPGRADE_DOWNGRADE_SUCCESS = 'UserProxy.UPGRADE_DOWNGRADE_SUCCESS';
UserProxy.UPGRADE_DOWNGRADE_FAILURE = 'UserProxy.UPGRADE_DOWNGRADE_FAILURE';
UserProxy.CONFIRM_EMAIL_SUCCESS = 'UserProxy.CONFIRM_EMAIL_SUCCESS';
UserProxy.CONFIRM_EMAIL_FAILURE = 'UserProxy.CONFIRM_EMAIL_FAILURE';
UserProxy.GET_AUTHOR_PROFILE_DATA_SUCCESS = 'UserProxy.GET_AUTHOR_PROFILE_DATA_SUCCESS';
UserProxy.GET_AUTHOR_PROFILE_DATA_FAILURE = 'UserProxy.GET_AUTHOR_PROFILE_DATA_FAILURE';
UserProxy.GET_AUTHOR_AVATAR_SUCCESS = 'UserProxy.GET_AUTHOR_AVATAR_SUCCESS';
UserProxy.GET_AUTHOR_AVATAR_FAILURE = 'UserProxy.GET_AUTHOR_AVATAR_FAILURE';
UserProxy.GET_AUTHOR_BANNER_SUCCESS = 'UserProxy.GET_AUTHOR_BANNER_SUCCESS';
UserProxy.GET_AUTHOR_BANNER_FAILURE = 'UserProxy.GET_AUTHOR_BANNER_FAILURE';
// ScriptLoader.loaded(UserProxy.NAME);
