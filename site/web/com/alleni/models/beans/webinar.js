var Webinar = function ()
{
	this._isPrimary = true;
	this._id = ";"
	this._name = "";
	this._title = "";
	this._videoId = "";
	this._description = "";
	this._downloadsURL = "";
	this._topics = [];
	this._lead = "";
	this._leadPosition = "";
	this._leadPhotoURL = "";
	this._leadTwitterURL = "";
	this._additionalWebinars = [];
	this._parts = [];
	
	/* CONSTRUCTOR */
	this.initialize = function (isPrimary,id,name,title,videoId,description,downloadsURL,topics,lead,leadPosition, leadPhotoURL, leadTwitterURL)
    {
	   this.isPrimary(isPrimary);
       this.id(id);
       this.name(name);
       this.title(title);
       this.videoId(videoId);
       this.description(description);
       this.downloadsURL(downloadsURL);
       this.topics(topics);
       this.lead(lead);
       this.leadPosition(leadPosition);
       this.leadPhotoURL(leadPhotoURL);
       this.leadTwitterURL(leadTwitterURL);
    }
	
	this.isPrimary = function (isPrimary)
	{
		if (isPrimary != undefined){
			this._isPrimary = isPrimary;
		}
		return this._isPrimary;
	}
	
	this.id = function (id)
	{
		if (id != undefined){
			this._id = id;
		}
		return this._id;
	}
	
	this.name = function (name)
	{
		if (name != undefined){
			this._name = name;
		}
		return this._name;
	}
	
	this.title = function (title)
	{
		if (title != undefined){
			this._title = title;
		}
		return this._title;
	}
	
	this.videoId = function (videoId)
	{
		if (videoId != undefined){
			this._videoId = videoId;
		}
		return this._videoId;
	}
	
	this.description = function (description)
	{
		if (description != undefined){
			this._description = description;
		}
		return this._description;
	}
	
	this.downloadsURL = function (downloadsURL)
	{
		if (downloadsURL != undefined){
			this._downloadsURL = downloadsURL;
		}
		return this._downloadsURL;
	}

	this.topics = function (topics)
	{
		if (topics != undefined){
			 this._topics = topics;
		}
		return this._topics;
	}
	
	this.lead = function (lead)
	{
		if (lead != undefined){
			 this._lead = lead;
		}
		return this._lead;
	}
	
	this.leadPosition = function (leadPosition)
	{
		if (leadPosition != undefined){
			 this._leadPosition = leadPosition;
		}
		return this._leadPosition;
	}
	
	this.leadPhotoURL = function (leadPhotoURL)
	{
		if (leadPhotoURL != undefined){
			 this._leadPhotoURL = leadPhotoURL;
		}
		return this._leadPhotoURL;
	}
	
	this.leadTwitterURL = function (leadTwitterURL)
	{
		if (leadTwitterURL != undefined){
			 this._leadTwitterURL = leadTwitterURL;
		}
		return this._leadTwitterURL;
	}
	
	this.additionalWebinars = function (additionalWebinars)
	{
		if (additionalWebinars != undefined){
			 this._additionalWebinars = additionalWebinars;
		}
		return this._additionalWebinars;
	}
	
	this.parts = function (parts)
	{
		if (parts != undefined){
			 this._parts = parts;
		}
		return this._parts;
	}

}
Webinar = new Class(new Webinar());