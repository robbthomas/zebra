var ScriptManagingProxy = function()
{
    this.Extends = Proxy;
    this.loadedViews = {};

    this.initialize = function(proxyName)
    {
        this.parent((proxyName != undefined)?proxyName:ScriptManagingProxy.NAME, new Array());
    }

    this.isLoaded = function (primaryViewComponent) {
    	this.loadedViews[primaryViewComponent];
    }
    
    this.setLoaded = function (primaryViewComponent) {
    	this.loadedViews[primaryViewComponent] = true;
    }
    
    this.getAll = function () {
    	return this.loadedViews;
    }
    
}
ScriptManagingProxy = new Class(new ScriptManagingProxy());
ScriptManagingProxy.NAME = 'ScriptManagingProxy';
