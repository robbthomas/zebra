var TestbenchProxy = function()
{
    this.Extends = Proxy;
    this.tests = null;

    this.initialize = function()
    {
        this.parent(TestbenchProxy.NAME, new Object());
    }
    this.submit = function (test, form)
    {
        //Log.debug('TestbenchProxy.submit');
        // var object = {};
        // var data = {};
        // var url = 'services/'+test.jsp;
        
        var payload = {};
        if (test.makeObject)
            payload = test.makeObject(form, test.id);
        else
        {
            Log.error('TestbenchProxy.submit: no makeObject for ' + test.id);
            payload = form;
        }
        
        var email = 'paulq@qualityprocess.com';
        var password = 'testpassword';
        
        if (payload.credentials)
        {
            email = payload.credentials.email;
            password = payload.credentials.password;
        }
        delete payload.credentials;
            
        this.sendNotification(TestbenchProxy.SUBMIT_OUT, {out: payload});
        
        var that = this;
        var url = WebService.request({
            payload: payload,
            pathFilename: test.jsp,
            callbacks: {
                success: function (data) {
                    that.sendNotification(TestbenchProxy.SUBMIT_IN, {json: data});
                },
                failure: function (error, detail) {
                    that.sendNotification(TestbenchProxy.SUBMIT_IN, {json: {error: error, detail: detail}});
                },
                error: function (error) {
                    that.sendNotification(TestbenchProxy.SUBMIT_RAW, {error: error});
                },
                raw: function (data) {
                    that.sendNotification(TestbenchProxy.SUBMIT_RAW, data);
                }
            },
            host: test.host
        }, {email: email, password: password});

        this.sendNotification(TestbenchProxy.SUBMIT_URL, {url: url});
    }
    this.test = function (id)
    {
        return this.tests[id];
    }
    this.requestTestList = function ()
    {
       //Log.debug('TestbenchProxy.requestTestList');
        var that = this;
        var testList = function (tests) {
            var a = [];
            for (var i in tests)
            {
                var test = tests[i];
                test.id = i;
                if (!test.html)
                    test.html = i + '.html'; // add the filename if missing
                a.push({group: test.group, test: test.name, id: test.id});
                that.tests[i] = test;
            }
            return a;
        }
        if (this.tests == null)
        {
            this.tests = {}; // lock out any subsequent calls
            File.requestJSON({filename:'tests.json', success:function (data, status) {
                var a = testList(data.tests);
                a = a.concat(testList(data.generatedTests));
                a.sort(function (a, b) {
                    if (a.group == b.group)
                        return a.test.localeCompare(b.test);
                    return a.group.localeCompare(b.group);
                });
                //Log.debug('TestbenchProxy.requestTestList - send notification');
                that.sendNotification(TestbenchProxy.REQUEST_TEST_LIST_REPLY, a);
            }});
        }
    }
}
TestbenchProxy = new Class(new TestbenchProxy());
TestbenchProxy.NAME = 'TestbenchProxy';
TestbenchProxy.SUBMIT_URL = 'TestbenchProxy.SUBMIT_URL';
TestbenchProxy.SUBMIT_OUT = 'TestbenchProxy.SUBMIT_OUT';
TestbenchProxy.SUBMIT_IN  = 'TestbenchProxy.SUBMIT_IN';
TestbenchProxy.SUBMIT_RAW = 'TestbenchProxy.SUBMIT_RAW';
TestbenchProxy.REQUEST_TEST_LIST = 'TestbenchProxy.REQUEST_TEST_LIST';
TestbenchProxy.REQUEST_TEST_LIST_REPLY = 'TestbenchProxy.REQUEST_TEST_LIST_REPLY';
ScriptLoader.loaded(TestbenchProxy.NAME);