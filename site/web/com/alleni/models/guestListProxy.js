var GuestListProxy = function()
{
    this.Extends = Proxy;

    this.initialize = function(proxyName)
    {
        this.parent((proxyName != undefined)?proxyName:GuestListProxy.NAME, new Object());
    }
    
    this.guestList = function()
    {
    	return this.getData();
    }
    
    this.loadByProjectId = function(projectId) {
    	var payload = {
    			findByProjectId: {projectId:projectId}
    	};
    	
        var _this = this;
        WebService.request({
            payload: payload,
//            pathFilename: 'guestlist/find.jsp',
            pathFilename: 'guestlist/project/' + projectId,
            host: 'zephyrDirect',
            type: 'GET',
            callbacks: {
                success: function (data) 
                {
                	_this.sendNotification(GuestListProxy.LOAD_SUCCESS,data);
                },
                failure: function (error, detail) 
                {
                	_this.sendNotification(GuestListProxy.LOAD_FAILURE,_this);
                }
            }
        });
    }


    
    this.load = function(guestListId) 
    {
    	var payload = {
    			guestListFind: {guestListId:guestListId}
    	};
        
        var _this = this;
        WebService.request({
            payload: payload,
            pathFilename: 'guestlist/project/' + projectId,
            host: 'zephyrDirect',
            type: 'GET',
            callbacks: {
                success: function (data) 
                {
                	_this.sendNotification(GuestListProxy.LOAD_SUCCESS,data);
                },
                failure: function (error, detail) 
                {
                	_this.sendNotification(GuestListProxy.LOAD_FAILURE,_this);
                }
            }
        });
        //return _this.data;
    }
    
    this.save = function(guestListId,projectId,emailText,emailList)
    {
    	var payload;
    	var pathFilename = "";
    	var type = '';
    	// toggle create/update
    	ProcessingScreen.show("Emailing Guests...");
    	if(guestListId == 0 || guestListId == undefined)
		{
    		payload = {
    				guestlistid:guestListId, 
    				projectid:projectId, 
    				emailtext:emailText, 
    				guestlistinvitees:emailList,
    				retiredbyid:null,
//    				retired:0, // doesn't exist in table
    				retiredreasontypeid:null,
    				retireddatetime:null
    		};
    		pathFilename = 'guestlist';
    		type = 'POST';
		}else{
			payload = {
					guestlistid:guestListId, 
					projectid:projectId, 
					emailtext:emailText, 
					guestlistinvitees:emailList
			};
			pathFilename = 'guestlist';
			type = 'PUT';
		}
        // service call
        var _this = this;
        WebService.request({
            payload: payload,
            pathFilename: pathFilename,
            type: type,
            host: 'zephyrDirect',
            callbacks: {
                success: function (data) 
                {
                    _this.sendNotification(GuestListProxy.SAVE_COMPLETE,_this);
                    ProcessingScreen.hide("Thank you.", 2000);
                },
                failure: function (error, detail) 
                {
                	_this.sendNotification(GuestListProxy.SAVE_FAILURE,_this);
                	ProcessingScreen.hide("There was a problem emailing your Guests.", 4000);
                }
            }
        });
    }

}
GuestListProxy = new Class(new GuestListProxy());
GuestListProxy.NAME = 'GuestListProxy';
GuestListProxy.SAVE_COMPLETE = 'GuestListProxy.SAVE_COMPLETE';
GuestListProxy.SAVE_FAILURE = 'GuestListProxy.SAVE_FAILURE';
GuestListProxy.LOAD_SUCCESS = 'GuestListProxy.LOAD_SUCCESS';
GuestListProxy.LOAD_FAILURE = 'GuestListProxy.LOAD_FAILURE';