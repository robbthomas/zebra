var BaseProxy = function()
{
    this.Extends = Proxy;
    this.token = undefined;
    
    this.initialize = function(proxyName, token)
    {
    	this.token = token;
    	this.parent((proxyName != undefined)?proxyName:BaseProxy.NAME, new Object());
    }

    this.zephyrRequest = function(path, payloadToSend, successAction, failureAction) {
    	var that = this;
    	var successFunction = undefined;
    	var failureFunction = undefined;
    	
    	if (typeof successAction == 'function') successFunction = successAction;
    	else successFunction = function(data) {that.sendNotification(successAction, data);};
    	
    	if (typeof failureAction == 'function') failureFunction = failureAction;
    	else failureFunction = function(error, detail) {that.sendNotification(failureAction, detail);};
    	
    	WebService.request({
    		host: 'zephyr',
    		pathFilename: path,
    		payload: payloadToSend,
    		callbacks: {
    			success: successFunction,
    			failure: failureFunction
    		}
    		
    	});
    }
    
}
BaseProxy = new Class(new BaseProxy());
BaseProxy.NAME = 'BaseProxy';
