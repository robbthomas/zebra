var JoinProxy = function()
{
    this.Extends = Proxy;
    this.tests = null;
    this._selectedPlan = undefined;
    this._plans = new Array();

    this.initialize = function()
    {
        this.parent(JoinProxy.NAME, new Object());
    }
    this.objectToValues = function (object, mode)
    {
        var valueName = {
            accountType: 'accountType',
            member_firstName: 'firstName',
            member_lastName: 'lastName',
            member_email: 'email',
            member_password: 'password'
        }
    }
    this.selectedPlan = function (selectedPlan)
    {
        if (selectedPlan != undefined)
            this._selectedPlan = selectedPlan;
        return this._selectedPlan;
    }
    // this.valuesToObject = function (values, mode)
    this.valuesToObject = function (values)
    {
        var o = {};
        o.joinCreate = {};
        o.joinCreate.accountType = values['accountType'];
        o.joinCreate.couponCode = values['couponCode'];
        o.joinCreate.member = {
                displayName: values['displayName'] != undefined ? values['displayName'] : values['username'],
                email: values['email'],
                firstName: values['firstName'],
                lastName: values['lastName'],
                password: values['password'],
                nameFormat: values['nameFormat']
        }
        o.joinCreate.billingAddress = {
        	name:           	values['billingNameCompany'],
            address1:           values['billingAddress1'],
            address2:           values['billingAddress2'],
            city:               values['billingCity'],
            stateProvince:      values['billingStateProvince'],
            zipPostalCode:      values['billingZipPostalCode'],
            country:            values['billingCountry']
        }
        o.joinCreate.creditCard = {
            expirationMonth:    values['expirationMonth'],
            expirationYear:     values['expirationYear'],
            nameOnCreditCard:   values['nameOnCreditCard'],
            number:             values['creditCardNumber'],
            securityCode:       values['securityCode'],
            type:               values['creditCardType']
        }
        return o;
    }
    this.create = function (values)
    {
        var that = this;
        WebService.request({
            // payload: that.valuesToObject(values, mode),
            payload: that.valuesToObject(values),
            pathFilename: 'join/create.jsp',
            callbacks: {
                success: function (data) {
                    that.sendNotification(JoinProxy.CREATE_SUCCESS, data);
                    that.sendNotification(Application.ACCOUNT_CREATE_SUCCESS, {displayName: values.displayName, email: values.email, password: values.password});
                },
                failure: function (error, detail) {
                    Log.error('JoinProxy.create - failure callback, error:'+error+', detail:'+detail);
                    that.sendNotification(JoinProxy.CREATE_FAILURE, {error: error,  detail: detail});
                }
            }
        });
    }
    this.getPlans = function ()
    {
    	var that = this;
    	WebService.request({
    		payload:{
    			accountTypeFilter:{
    				/*retired:0*/
    			},
    			recordRange:{
					offset:0,
					count:-1
				}
    		},
    		pathFilename: 'accounttype/filter.jsp',
    		callbacks: {
    			success: function (data) {
    				//that.plans = data.accountTypes;
    				for (var i=0; i<data.accountTypes.length; i++)
					{
    					that._plans.push({
			                name: data.accountTypes[i].name,
			                type: data.accountTypes[i].tag,
			                price: data.accountTypes[i].price,
			                formattedPrice: data.accountTypes[i].description
    					})
					}  				
    			},
    			failure: function (data, detail) {
    				alert(detail);
    			}
    		}
    	});
    }
    this.plan = function (plan)
    {
        if (plan == undefined) {
            plan = this.selectedPlan();
            // TODO: SJ - this proxy is not persisted
            // so a refresh yeilds the default plan
            // should store in a cookie or come up with
            // some other way to keep hold of the selection
            if(plan == undefined)
        	{
            	plan = "creator";
        	}
        }

        var result = undefined;
        for (var i=0; i<this._plans.length; i++)
        {
        	if (this._plans[i].type == plan)
    		{
        		result = this._plans[i];
    		}
        }
        return result;
    }

}
JoinProxy = new Class(new JoinProxy());
JoinProxy.NAME = 'JoinProxy';
JoinProxy.CREATE_SUCCESS = 'JoinProxy.CREATE_SUCCESS';
JoinProxy.CREATE_FAILURE = 'JoinProxy.CREATE_FAILURE';
ScriptLoader.loaded(JoinProxy.NAME);
