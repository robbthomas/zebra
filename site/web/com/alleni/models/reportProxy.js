var ReportProxy = function()
{
    this.Extends = BaseProxy;
    this.token = undefined;
    
    this.initialize = function(proxyName, token)
    {
    	this.token = token;
    	this.parent((proxyName != undefined)?proxyName:ReportProxy.NAME, new Object());
    }
    
    this.getPayeeSummaryResults = function(criteria) {
    	getReportResults(criteria, 'payee summary');
    };
    
    this.getTransactionDetailResults = function(criteria) {
    	getReportResults(criteria, 'transaction detail');
    };
    
    this.getReportCriteria = function(reportUUID) {
    	this.zephyrRequest(
    		  'report/' + reportUUID
    		, {}
    		, ReportProxy['GET_REPORT_CRITERIA_SUCCESS']
    		, ReportProxy['GET_REPORT_CRITERIA_FAILURE']
    	);
    };
    
    this.getReportResults = function(criteria, reportName) {
    	var notificationReportName = this.format(reportName, 'notification');
    	
    	this.zephyrRequest(
      		  'report/result/' + this.format(reportName, 'camel')
      		, criteria
      		, ReportProxy['GET_' + notificationReportName + '_REPORT_SUCCESS']
      		, ReportProxy['GET_' + notificationReportName + '_REPORT_FAILURE']
      	);
    };
    
    this.format = function(stringToFormat, formatType) {
    	switch (formatType) {
    	case 'camel':
    		return stringToFormat.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
    			return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
    		}).replace(/\s+/g, '');
    		break;
    	case 'notification':
    		return stringToFormat.toUpperCase().replace(' ', '_');
    		break;
    	default:
    		return stringToFormat;
    	}
    };
}
ReportProxy = new Class(new ReportProxy());
ReportProxy.NAME = 'ReportProxy';
ReportProxy.GET_PAYEE_SUMMARY_REPORT_SUCCESS = 'ReportProxy.GET_PAYEE_SUMMARY_REPORT_SUCCESS';
ReportProxy.GET_PAYEE_SUMMARY_REPORT_FAILURE = 'ReportProxy.GET_PAYEE_SUMMARY_REPORT_FAILURE';
ReportProxy.GET_TRANSACTION_DETAIL_REPORT_SUCCESS = 'ReportProxy.GET_TRANSACTION_DETAIL_REPORT_SUCCESS';
ReportProxy.GET_TRANSACTION_DETAIL_REPORT_FAILURE = 'ReportProxy.GET_TRANSACTION_DETAIL_REPORT_FAILURE';
ReportProxy.GET_REPORT_CRITERIA_SUCCESS = 'ReportProxy.GET_REPORT_CRITERIA_SUCCESS';
ReportProxy.GET_REPORT_CRITERIA_FAILURE = 'ReportProxy.GET_REPORT_CRITERIA_FAILURE';
