var CouponProxy = function()
{
    this.Extends = Proxy;
    this.tests = null;

    this.initialize = function(proxyName)
    {
        this.parent((proxyName != undefined)?proxyName:CouponProxy.NAME, new Object());
    }

    this.validate = function (code)
    {
        var that = this;
        WebService.request({
            payload: {couponCode: code},
            pathFilename: 'couponcode/validate.jsp',
            callbacks: {
                success: function (data) {
                	var name = that.getProxyName();
                    that.sendNotification(CouponProxy.VALIDATE_SUCCESS+that.getProxyName(), data);
                },
                failure: function (error, detail) {
                   var  payload = {
                       error: error,
                       errorDetail: detail
                   }
                   that.sendNotification(CouponProxy.VALIDATE_FAILURE+that.getProxyName(), payload);
                }
            }
        });
    }

}
CouponProxy = new Class(new CouponProxy());
CouponProxy.NAME = 'CouponProxy';
CouponProxy.VALIDATE_SUCCESS = 'CouponProxy.VALIDATE_SUCCESS';
CouponProxy.VALIDATE_FAILURE = 'CouponProxy.VALIDATE_FAILURE';
ScriptLoader.loaded(CouponProxy.NAME);
