var SharedProjectProxy = function()
{
    this.Extends = Proxy;
    this.tests = null;

    this.initialize = function(proxyName)
    {
        this.parent((proxyName != undefined)?proxyName:SharedProjectProxy.NAME, undefined);
    }
    
    this.saveShareTypeForProject = function (publishId,shareTypeId)
    {
    	var payload = {sharedProjectCreate:{publishId:publishId,shareTypeId:shareTypeId}};
        // service call
        var _this = this;
        WebService.request({
            payload: payload,
            pathFilename: 'sharedproject/create.jsp',
            callbacks: {
                success: function (data) 
                {
                    this.sendNotification(SharedProjectProxy.SAVE_SUCCESS,_this);
                },
                failure: function (error, detail) 
                {
                	this.sendNotification(SharedProjectProxy.SAVE_FAILURE,_this);
                }
            }
        });
    }
    
    

}
SharedProjectProxy = new Class(new SharedProjectProxy());
SharedProjectProxy.NAME = 'SharedProjectProxy';
SharedProjectProxy.SAVE_SUCCESS = 'SharedProjectProxy.SAVE_SUCCESS';
SharedProjectProxy.SAVE_FAILURE = 'SharedProjectProxy.SAVE_FAILURE';