var AccountProxy = function()
{
    this.Extends = Proxy;
    this.tests = null;

    this.initialize = function()
    {
        this.parent(AccountProxy.NAME, new Object());
    }
    this.objectToValues = function (object, mode)
    {
        var v = {};
        return v;
    }
    this.valuesToObject = function (values, mode)
    {
        var o = {};
        return o;
    }    
    this.get = function (formName)
    {
        var that = this;
        WebService.request({
            payload: {accountGet: {group: formName}},
            pathFilename: 'account/get.jsp',
            callbacks: {
                success: function (data) {
                   //Log.debug('AccountProxy.get - success ('+JSON.encode(data)+')');
                    that.sendNotification(AccountProxy.GET_SUCCESS, data[formName]);
                },
                failure: function (error, detail) {
                   //Log.debug('AccountProxy.get - failure ('+JSON.encode({error: error, detail: detail})+')');
                   var  payload = {
                       error: error,
                       errorDetail: detail
                   }
                   that.sendNotification(AccountProxy.GET_FAILURE, payload);
                }
            }
        });
    }
    this.getAccountInformation = function(){
    	var that = this;
        WebService.request({
        	host: 'zephyr',
            payload: {},
            pathFilename: 'account/me',
            method: 'get',
            callbacks: {
                success: function (data) {
                   that.sendNotification(AccountProxy.GET_ACCOUNT_INFORMATION_SUCCESS, data);
                },
                failure: function (error, detail) {
                	that.sendNotification(AccountProxy.GET_ACCOUNT_INFORMATION_FAILURE, detail);
                }
            }
        });
    }
    this.update = function (formName, values)
    {
       //Log.debug('AccountProxy.update - payload:'+JSON.encode({values: values}));
        var that = this;

        var payload = {accountUpdate: {group: formName, values: values}};

        WebService.request({
            payload: payload,
            pathFilename: 'account/updateExtended.jsp',
            callbacks: {
                success: function (data) {
                   //Log.debug('AccountProxy.update - success ('+JSON.encode(data)+')');
                	payload.data = data;
                    that.sendNotification(AccountProxy.UPDATE_SUCCESS, payload);
                },
                failure: function (error, detail) {
                   //Log.debug('AccountProxy.requestMyProfile - failure ('+JSON.encode({error: error, detail: detail})+')');
                   payload.error = error;
                   payload.errorDetail = detail;
                   that.sendNotification(AccountProxy.UPDATE_FAILURE, payload);
                }
            }
        });
    }
    
    this.cancelTransfer = function(projectMemberAccessId) {
        var that = this;
         
        var path = 'project/transfer/cancel/' + projectMemberAccessId;
        ProcessingScreen.show("Canceling project copy...");
        WebService.request({
             host: 'zephyr',
             payload:{},
             method:'post',
             pathFilename: path,
             callbacks: {
                 success: function(data) {
                     if (data.message == 'OK') {
                         that.sendNotification(AccountProxy.CANCEL_PROJECT_TRANSFER_SUCCESS, data);
                         ProcessingScreen.hide("Thank you.", 2000);
                     }
                     else {
                         that.sendNotification(AccountProxy.CANCEL_PROJECT_TRANSFER_FAILURE, data);
                         ProcessingScreen.hide("There was a problem canceling the project copy.", 4000);
                     }
                 },
                 failure: function(error, detail) {
                     that.sendNotification(AccountProxy.CANCEL_PROJECT_TRANSFER_FAILURE, detail);
                     ProcessingScreen.hide("There was a problem canceling the project copy.", 4000);
                 }
             }
         });
     }
    
    this.clearTransfer = function(projectMemberAccessId) {
        var that = this;
         
        var path = 'project/transfer/cancel/' + projectMemberAccessId;
        ProcessingScreen.show("Clearing project copy...");
        WebService.request({
             host: 'zephyr',
             payload:{},
             method:'post',
             pathFilename: path,
             callbacks: {
                 success: function(data) {
                     if (data.message == 'OK') {
                         that.sendNotification(AccountProxy.CANCEL_PROJECT_TRANSFER_SUCCESS, data);
                         ProcessingScreen.hide("Thank you.", 2000);
                     }
                     else {
                         that.sendNotification(AccountProxy.CANCEL_PROJECT_TRANSFER_FAILURE, data);
                         ProcessingScreen.hide("There was a problem clearning the project copy.", 4000);
                     }
                 },
                 failure: function(error, detail) {
                     that.sendNotification(AccountProxy.CANCEL_PROJECT_TRANSFER_FAILURE, detail);
                     ProcessingScreen.hide("There was a problem clearing the project copy.", 4000);
                 }
             }
         });
     }
    
    this.acceptTransfer = function(projectMemberAccessId) {
        var that = this;
         
        var path = 'project/transfer/accept/' + projectMemberAccessId;
        ProcessingScreen.show("Copying project to your account...");
        WebService.request({
             host: 'zephyr',
             payload:{},
             method:'post',
             pathFilename: path,
             callbacks: {
                 success: function(data) {
                     if (data.message == 'OK') {
                         that.sendNotification(AccountProxy.ACCEPT_PROJECT_TRANSFER_SUCCESS, data);
                         ProcessingScreen.hide("Thank you.", 2000);
                     }
                     else {
                         that.sendNotification(AccountProxy.ACCEPT_PROJECT_TRANSFER_FAILURE, data);
                         ProcessingScreen.hide("There was a problem copying project to your account.", 4000);
                     }
                 },
                 failure: function(error, detail) {
                     that.sendNotification(AccountProxy.ACCEPT_PROJECT_TRANSFER_FAILURE, detail);
                     ProcessingScreen.hide("There was a problem copying project to your account.", 4000);
                 }
             }
         });
     }
    
    this.startTransfer = function(transferData) {
    	var projectId = transferData.projectId;
		var receiverEmail = encodeURI(transferData.email);
    	var that = this; 
    	var path = 'project/transfer/' + projectId + '/to/' + receiverEmail;
    	
        WebService.request({
            host: 'zephyrDirect',
            payload:{},
            method:'post',
            pathFilename: path,
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                        that.sendNotification(AccountProxy.BEGIN_PROJECT_TRANSFER_SUCCESS, data);
                    }
                    else {
                        that.sendNotification(AccountProxy.BEGIN_PROJECT_TRANSFER_FAILURE, data);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(AccountProxy.BEGIN_PROJECT_TRANSFER_FAILURE, detail);
                }
            }
        });
    }
    
    this.startBulkTransfer = function(transferData) {
    	var data = transferData;
    	var that = this; 
    	var path = 'project/transfer/';
        WebService.request({
            host: 'zephyrDirect',
            payload:data,
            method:'post',
            pathFilename: path,
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                        that.sendNotification(AccountProxy.BEGIN_PROJECT_TRANSFER_SUCCESS, data);
                    }
                    else {
                        that.sendNotification(AccountProxy.BEGIN_PROJECT_TRANSFER_FAILURE, data);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(AccountProxy.BEGIN_PROJECT_TRANSFER_FAILURE, detail);
                }
            }
        });
    }
    
    
    this.getPendingIncomingTransfers = function()
    {
       var that = this;
        
        var path = 'project/transfer/incoming';
        WebService.request({
            host: 'zephyr',
            payload:{},
            method:'get',
            pathFilename: path,
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                        that.sendNotification(AccountProxy.GET_INCOMING_TRANSFER_SUCCESS, data);
                    } else {
                        that.sendNotification(AccountProxy.GET_INCOMING_TRANSFER_FAILURE, data);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(AccountProxy.GET_INCOMING_TRANSFER_FAILURE, detail);
                }
            }
        });
   	
    }
    
    this.getPendingIncomingTransferCount = function()
    {
       var that = this;
        
        var path = 'project/transfer/count';
        WebService.request({
            host: 'zephyr',
            payload:{},
            method:'get',
            pathFilename: path,
            callbacks: {
                success: function(data) {
                	that.sendNotification(SidebarPanelMediator.REFRESH_PENDING_TRANSFERS_SUCCESS, data);
                },
                failure: function(error, detail) {
                    that.sendNotification(SidebarPanelMediator.REFRESH_PENDING_TRANSFERS_FAILURE, detail);
                }
            }
        });
   	
    }
    
    this.getPendingOutgoingTransfers = function()
    {
       var that = this;
        
        var path = 'project/transfer/sent';
        WebService.request({
            host: 'zephyr',
            payload:{},
            method:'get',
            pathFilename: path,
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                        that.sendNotification(AccountProxy.GET_OUTGOING_TRANSFER_SUCCESS, data);
                    }
                    else {
                        that.sendNotification(AccountProxy.GET_OUTGOING_TRANSFER_FAILURE, data);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(AccountProxy.GET_OUTGOING_TRANSFER_FAILURE, detail);
                }
            }
        });
   	
    }
    
    this.setNumberLearners = function(numLearners)
    {
    	var that = this;
    	var path = 'account/learners';
        WebService.request({
            host: 'zephyr',
            payload:{numLearners: numLearners},
            method:'post',
            pathFilename: path,
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                        that.sendNotification(AccountProxy.SET_NUMBER_LEARNERS_SUCCESS, data);
                    }
                    else {
                        that.sendNotification(AccountProxy.SET_NUMBER_LEARNERS_FAILURE, data);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(AccountProxy.SET_NUMBER_LEARNERS_FAILURE, detail);
                }
            }
        });
    }
}
AccountProxy = new Class(new AccountProxy());
AccountProxy.NAME = 'AccountProxy';
AccountProxy.BEGIN_PROJECT_TRANSFER_SUCCESS = 'AccountProxy.BEGIN_PROJECT_TRANSFER_SUCCESS';
AccountProxy.GET_SUCCESS = 'AccountProxy.GET_SUCCESS';
AccountProxy.GET_FAILURE = 'AccountProxy.GET_FAILURE';
AccountProxy.UPDATE_SUCCESS = 'AccountProxy.UPDATE_SUCCESS';
AccountProxy.UPDATE_FAILURE = 'AccountProxy.UPDATE_FAILURE';
AccountProxy.GET_OUTGOING_TRANSFER_SUCCESS = 'AccountProxy.GET_OUTGOING_TRANSFER_SUCCESS';
AccountProxy.GET_OUTGOING_TRANSFER_FAILURE = 'AccountProxy.GET_OUTGOING_TRANSFER_FAILURE';
AccountProxy.GET_INCOMING_TRANSFER_SUCCESS = 'AccountProxy.GET_INCOMING_TRANSFER_SUCCESS';
AccountProxy.GET_INCOMING_TRANSFER_FAILURE = 'AccountProxy.GET_INCOMING_TRANSFER_FAILURE';
AccountProxy.GET_INCOMING_TRANSFER_COUNT_SUCCESS = 'AccountProxy.GET_INCOMING_TRANSFER_COUNT_SUCCESS';
AccountProxy.GET_INCOMING_TRANSFER_COUNT_FAILURE = 'AccountProxy.GET_INCOMING_TRANSFER_COUNT_FAILURE';
AccountProxy.CANCEL_PROJECT_TRANSFER_SUCCESS = 'AccountProxy.CANCEL_PROJECT_TRANSFER_SUCCESS';
AccountProxy.CANCEL_PROJECT_TRANSFER_FAILURE = 'AccountProxy.CANCEL_PROJECT_TRANSFER_FAILURE';
AccountProxy.ACCEPT_PROJECT_TRANSFER_SUCCESS = 'AccountProxy.ACCEPT_PROJECT_TRANSFER_SUCCESS';
AccountProxy.ACCEPT_PROJECT_TRANSFER_FAILURE = 'AccountProxy.ACCEPT_PROJECT_TRANSFER_FAILURE';
AccountProxy.GET_ACCOUNT_INFORMATION_SUCCESS = 'AccountProxy.GET_ACCOUNT_INFORMATION_SUCCESS';
AccountProxy.GET_ACCOUNT_INFORMATION_FAILURE = 'AccountProxy.GET_ACCOUNT_INFORMATION_FAILURE';
ScriptLoader.loaded(AccountProxy.NAME);
