var FeedbackProxy = function()
{
    this.Extends = Proxy;
    this.tests = null;

    this.initialize = function()
    {
        this.parent(FeedbackProxy.NAME, new Object());
    }
    this.objectToValues = function (object, mode)
    {
        var v = {};
        return v;
    }
    this.valuesToObject = function (values, mode)
    {
        var o = {};
        return o;
    }

    this.createComment = function(params)
    {
        var that = this;
        WebService.request({
            payload: {
                zappCommentCreate: {
                    comment: params.comment,
                    projectId: params.projectId,
                    memberId: Credentials.userId(),
                    urlName: params.urlName,
                    projectType: params.projectType
                }
            },
            pathFilename: 'zappcomment/create.jsp',
            callbacks: {
                success: function(data) {
                    data._projectId = params.projectId;
                    data._urlName = params.urlName;
                    that.sendNotification(FeedbackProxy.COMMENT_CREATED_SUCCESS, data);
                },
                failure: function(error, detail) {
                    that.sendNotification(FeedbackProxy.COMMENT_CREATED_FALURE, error);
                }
            }
        });
    }

    this.requestCommentsPaged = function(projectId, offset, count) {
        var that = this;
        if (offset == undefined || offset == 0) {
        	offset = 0;
        }
        if (count == undefined || count == 0) {
        	count = 10;
        }
        
        WebService.request({
            payload: {
                "zappCommentFilter":{
                    "filters":{
                        "publishId":projectId
                    }
                },
                "recordRange": {
                    "offset":offset,
                    "count":count,
                    "order":"createdDateTime:d",
                    "exclude": false
                }
            },
            pathFilename: 'zappcomment/filter.jsp',
            callbacks: {
                success: function (data) {
                   //Log.debug('FeedbackProxy.get - success ('+JSON.encode(data)+')');
                    that.sendNotification(FeedbackProxy.COMMENT_FILTER_SUCCESS, data);
                },
                failure: function (error, detail) {
                   //Log.debug('FeedbackProxy.get - failure ('+JSON.encode({error: error, detail: detail})+')');
                   var  payload = {
                       error: error,
                       errorDetail: detail
                   }
                   that.sendNotification(FeedbackProxy.COMMENT_FILTER_FAILURE, payload);
                }
            }
        });
    }

    this.requestComments = function (publishId)
    {
    	this.requestCommentsPaged(projectId, 0, 10);
    }

    this.rating = function(myRating, publishId)
    {
        var that = this;
        WebService.request({
            payload: {
                zappRating: {
                    rating: myRating,
                    projectId: publishId,
                    memberId: Credentials.userId()
                }
            },
            pathFilename: 'zapp/rating.jsp',
            callbacks: {
                success: function(data) {
                    data._publishId = publishId;
                    that.sendNotification(FeedbackProxy.RATING_SUCCESS, data);
                },
                failure: function(error, detail) {
                    that.sendNotification(FeedbackProxy.RATING_FALURE, error);
                }
            }
        });
    }

    this.commentFeedback = function(myFeedback, commentId)
    {
        var that = this;
        WebService.request({
            payload: {
                commentFeedback: {
                    feedback: myFeedback,
                    commentId: commentId,
                    memberId: Credentials.userId()
                }
            },
            pathFilename: 'commentfeedback/feedback.jsp',
            callbacks: {
                success: function(data) {
                    data._commentId = commentId;
                    that.sendNotification(FeedbackProxy.COMMENT_FEEDBACK_SUCCESS, data);
                },
                failure: function(error, detail) {
                    that.sendNotification(FeedbackProxy.COMMENT_FEEDBACK_ERROR, error);
                }
            }
        });
    }
    
    this.deleteComment= function(commentId)
    {
        var that = this;
        WebService.request({
            payload: {
            	zappCommentRetire: {
                	zappCommentId: commentId,
                    retiredById: Credentials.userId()
                }
            },
            pathFilename: 'zappcomment/retire.jsp',
            callbacks: {
                success: function(data) {
                    that.sendNotification(FeedbackProxy.COMMENT_DELETE_SUCCESS, data);
                },
                failure: function(error, detail) {
                    that.sendNotification(FeedbackProxy.COMMENT_DELETE_ERROR, error);
                }
            }
        });
    }

    this.requestCommentsRatings = function(projectId, offset, count, excludeComments)
    {
        var that = this;
        if (offset == undefined) {
        	offset = 0;
        }
        if (count == undefined) {
        	count = 10;
        }
        if (excludeComments == undefined) {
        	excludeComments = false;
        }
        WebService.request({
            payload: {
                zappCommentsRatings: {
                    ids: [projectId],
                    comments: {
                        exclude: excludeComments,
                        offset: offset,
                        count: count
                    }
                }
            },
            pathFilename: 'zapp/commentsRatings.jsp',
            callbacks: {
                success: function(data) {
                    data._publishId = projectId;
                    that.sendNotification(FeedbackProxy.GOT_COMMENTS_RATINGS_SUCCESS, data);
                },
                failure: function(error, detail) {
                    that.sendNotification(FeedbackProxy.GOT_COMMENTS_RATINGS_FAILURE, error);
                }
            }
        });
    }

    this.get = function (formName)
    {
        var that = this;
        WebService.request({
            payload: {feedbackGet: {group: formName}},
            pathFilename: 'feedback/get.jsp',
            callbacks: {
                success: function (data) {
                   //Log.debug('FeedbackProxy.get - success ('+JSON.encode(data)+')');
                    that.sendNotification(FeedbackProxy.GET_SUCCESS, data[formName]);
                },
                failure: function (error, detail) {
                   //Log.debug('FeedbackProxy.get - failure ('+JSON.encode({error: error, detail: detail})+')');
                   var  payload = {
                       error: error,
                       errorDetail: detail
                   }
                   that.sendNotification(FeedbackProxy.GET_FAILURE, payload);
                }
            }
        });
    }
    this.update = function (formName, values)
    {
       //Log.debug('FeedbackProxy.update - payload:'+JSON.encode({values: values}));
        var that = this;

        var payload = {feedbackUpdate: {group: formName, values: values}};

        WebService.request({
            payload: payload,
            pathFilename: 'feedback/updateExtended.jsp',
            callbacks: {
                success: function (data) {
                   //Log.debug('FeedbackProxy.update - success ('+JSON.encode(data)+')');
                    that.sendNotification(FeedbackProxy.UPDATE_SUCCESS, payload);
                },
                failure: function (error, detail) {
                   //Log.debug('FeedbackProxy.requestMyProfile - failure ('+JSON.encode({error: error, detail: detail})+')');
                   payload.error = error;
                   payload.errorDetail = detail;
                   that.sendNotification(FeedbackProxy.UPDATE_FAILURE, payload);
                }
            }
        });
    }
}
FeedbackProxy = new Class(new FeedbackProxy());
FeedbackProxy.NAME = 'FeedbackProxy';
FeedbackProxy.GET_SUCCESS = 'FeedbackProxy.GET_SUCCESS';
FeedbackProxy.GET_FAILURE = 'FeedbackProxy.GET_FAILURE';
FeedbackProxy.UPDATE_SUCCESS = 'FeedbackProxy.UPDATE_SUCCESS';
FeedbackProxy.UPDATE_FAILURE = 'FeedbackProxy.UPDATE_FAILURE';

FeedbackProxy.COMMENT_CREATED_SUCCESS = 'FeedbackProxy.COMMENT_CREATED_SUCCESS';
FeedbackProxy.COMMENT_CREATED_FAILURE = 'FeedbackProxy.COMMENT_CREATED_FAILURE';
FeedbackProxy.COMMENT_DELETE_SUCCESS = 'FeedbackProxy.COMMENT_DELETE_SUCCESS';
FeedbackProxy.COMMENT_DELETE_FAILURE = 'FeedbackProxy.COMMENT_DELETE_FAILURE';
FeedbackProxy.COMMENT_FILTER_SUCCESS = 'FeedbackProxy.COMMENT_FILTER_SUCCESS';
FeedbackProxy.COMMENT_FILTER_FAILURE = 'FeedbackProxy.COMMENT_FILTER_FAILURE';

FeedbackProxy.RATING_SUCCESS = 'FeedbackProxy.RATING_SUCCESS';
FeedbackProxy.RATING_FAILURE = 'FeedbackProxy.RATING_FAILURE';
FeedbackProxy.GOT_COMMENTS_RATINGS_SUCCESS = 'FeedbackProxy.GOT_COMMENTS_RATINGS_SUCCESS';
FeedbackProxy.GOT_COMMENTS_RATINGS_FAILURE = 'FeedbackProxy.GOT_COMMENTS_RATINGS_FAILURE';

FeedbackProxy.COMMENT_FEEDBACK_SUCCESS = 'FeedbackProxy.COMMENT_FEEDBACK_SUCCESS';
FeedbackProxy.COMMENT_FEEDBACK_FAILURE = 'FeedbackProxy.COMMENT_FEEDBACK_FAILURE';
ScriptLoader.loaded(FeedbackProxy.NAME);
