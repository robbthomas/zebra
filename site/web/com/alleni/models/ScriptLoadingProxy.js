var ScriptLoadingProxy = function()
{
    this.Extends = Proxy;
    this.config = undefined;
    this.scriptManagingProxy = undefined; // need to null this upon removal of this volatile proxy (see this.onComplete)

    this.initialize = function(proxyName)
    {
    	try{
    		if(proxyName == undefined){
    			throw ScriptLoadingProxy.ERROR_NOT_A_SINGLETON;
    		}else{
    			this.parent(proxyName, undefined);
    	        this.scriptManagingProxy = this.facade.retrieveProxy("ScriptManagingProxy");
    		}
    	}catch(er){
    		if(er == ScriptLoadingProxy.ERROR_NOT_A_SINGLETON){
    			alert(ScriptLoadingProxy.ERROR_NOT_A_SINGLETON);
    		}
    	}
    }
    
    this.load = function(config) {
    	var _this = this;
    	this.config = config;
    	if(!this.scriptManagingProxy.isLoaded(this.config.component()))
    	{
	    	this.loadScript( Application.config.paths.components+'/'+this.config.component()+'?cb=' + Common.cb,function(){_this.onViewComplete(_this)});
			this.loadScript( Application.config.paths.mediators+'/'+this.config.mediator()+'?cb=' + Common.cb,function(){_this.onMediatorComplete(_this)});
			this.loadFragment();
    	}else{
    		this.onComplete();
    	}
    }
    
    this.loadFragment = function() {
    	var _this = this;
    	var o = {
	            filename: this.config.fragment(),
	            success: function (fragment, status) {
	                _this.onFragmentComplete(fragment, _this);
	            }
	    }
	    File.requestFragment(o);
    }
    
    this.onViewComplete = function(scope) {
    	scope.viewLoaded = true;
    	scope.checkParts();
    }
    
    this.onMediatorComplete = function(scope) {
    	scope.mediatorLoaded = true;
    	scope.checkParts();
    }
    
    this.onFragmentComplete = function(fragment,scope) {
    	scope.fragment = fragment;
    	scope.fragmentLoaded = true;
    	scope.checkParts();
    }
    
    this.checkParts = function() {
    	if(this.viewLoaded && this.mediatorLoaded && this.fragmentLoaded){
    		this.onComplete();
    	}
    }
    
    this.onComplete = function() {
    	this.scriptManagingProxy.setLoaded(this.config.component());
    	
    	// TO SOLVE: how to separate calls to this from a mediator vs. a command such as loadDialogueViewCommand???
    	// Separate set of notifications?  What to do with the tokens? Extend and override the onComplete()?
    	
    	if(this.config.token() != undefined){
    		// targeted notification
    		this.sendNotification(ApplicationFacade.VIEW_LOADED + this.config.token(), this.config);
    	}else{
    		// generic notification
        	this.sendNotification(ApplicationFacade.VIEW_LOADED, this.config);
    	}
    	// clean this proxy
    	this.scriptManagingProxy = undefined;
    	this.config = undefined;
    	// unregister this proxy
    	this.facade.removeProxy(this.getProxyName());
    }
    
    // TODO: replace this is with something similar to QP's ScriptLoader pattern
    this.loadScript = function (sScriptSrc, oCallback) {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = 'text/javascript';
        oScript.src = sScriptSrc;
        // most browsers
        oScript.onload = oCallback;
        // IE 6 & 7
        oScript.onreadystatechange = function() {
	        if (this.readyState == 'complete') {
	        	oCallback();
	        }
        }
        oHead.appendChild(oScript);
    }
    
    
}
ScriptLoadingProxy = new Class(new ScriptLoadingProxy());
ScriptLoadingProxy.NAME = 'ScriptLoadingProxy';
ScriptLoadingProxy.ERROR_NOT_A_SINGLETON = "ScriptLoadingProxy cannot be a singleton.";