var ZappGadgetProjectProxy = function()
{
    this.Extends = Proxy;
    this.token = undefined;
    
    this.categories = { //TODO acavan - load via web service (curl -u guest:testpassword "int.alleni.com/zephyr/gadget/categories")
    		'6000': 'Navigation',
    		'6001': 'Buttons',
    		'6002': 'Questions',
    		'6003': 'Calculations',
    		'6004': 'Simulations',
    		'6005': 'Interactions',
    		'6006': 'Accessibility',
    		'6007': 'Animation',
    		'6008': 'Audio &amp; Media',
    		'6009': 'CCAF Sets',
    		'6010': 'Charting &amp; Graphing',
    		'6011': 'Experiments',
    		'6012': 'Feedback &amp; Messaging',
    		'6013': 'Graphics',
    		'6014': 'Interface &amp; Design',
    		'6015': 'Logic &amp; Branching',
    		'6016': 'Quiz &amp; Test',
    		'6017': 'Utilities </option>',
    		'6018': 'Business',
    		'6019': 'Education',
    		'6020': 'Entertainment',
    		'6021': 'Experiments',
    		'6022': 'Fitness',
    		'6023': 'Friends &amp; Family',
    		'6024': 'Fun &amp; Games',
    		'6025': 'Health &amp; Medical',
    		'6026': 'How To &amp; DIY',
    		'6027': 'Lifestyle',
    		'6028': 'Music &amp; Media',
    		'6029': 'News &amp; Weather',
    		'6030': 'Productivity',
    		'6031': 'Social',
    		'6032': 'Sports',
    		'6033': 'Training',
    		'6034': 'Zebra',
    		'6035': 'Zebra Zapps Examples',
    		'6036': 'Zebra Zapps Examples'
    }

    this.initialize = function(proxyName, token)
    {
    	this.token = token;
    	this.parent((proxyName != undefined)?proxyName:ZappGadgetProjectProxy.NAME, new Object());
    }
    this.buyZappGadget = function (zg, dialog)
    {
        return; // PMQ -- attempt to move buy dialog functionatility here but not ready for prime time
    //dialog.open(zg);
    }
    this.requestPurchase = function (ids)
    {
        if (!Credentials.authenticated())
        {
            Log.error('ZappGadgetProjectProxy.requestPurchase - not authenticated');
            return;
        }
        var payload = {
            zappPurchase : {
                accountId: Credentials.accountId(),
                zappList: []
            }
        }
        for (var i = 0; i < ids.length; i++)
            payload.zappPurchase.zappList.push({
                projectId: ids[i]
                });
        var that = this;
        WebService.request({
            payload: payload,
            pathFilename: 'zapp/purchase.jsp',
            callbacks: {
                success: function (data) {
                    // BENRES:  need to check for errors
                	for(var j=0; j< data.newZapps.length; j++) {
                		Credentials.owned(data.newZapps[j].projectId, true);
                	}
                    that.sendNotification(ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS, data);
                },
                failure: function (error, detail) {
                    //alert('ZappGadgetProjectProxy.requestPurchase - failure\nerror:'+error+'\ndetail:'+detail);
                    that.sendNotification(ZappGadgetProjectProxy.REQUEST_PURCHASE_FAILURE, error);
                //Log.debug('ZappGadgetProjectProxy.requestPurchase - failure');
                }
            }
        });
    }
    this.deleteProject = function(projectId)
    {
        var that = this;
        WebService.request({
            host: 'zephyr',
            payload:{},
            method:'post',
            pathFilename: 'project/' + projectId + '?_method=DELETE',
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                    	that.sendNotification(ZappGadgetProjectProxy.PROJECT_DELETED_SUCCESS, data);
                    }
                    else {
                        that.sendNotification(ZappGadgetProjectProxy.PROJECT_DELETED_FAILURE, data);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(ZappGadgetProjectProxy.PROJECT_DELETED_FAILURE, detail);
                }
            }
        });
    }
    this.unpublishProject = function(projectId)
    {
        var that = this;
        WebService.request({
            host: 'zephyr',
            payload:{},
            method:'post',
            pathFilename: 'project/' + projectId + '/unpublish?_method=POST',
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                        that.sendNotification(ZappGadgetProjectProxy.PROJECT_UNPUBLISH_SUCCESS, data);
                    }
                    else {
                        that.sendNotification(ZappGadgetProjectProxy.PROJECT_UNPUBLISH_FAILURE, data);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(ZappGadgetProjectProxy.PROJECT_UNPUBLISH_FAILURE, detail);
                }
            }
        });
    }
    
    this.unpurchaseProject = function(purchaseId)
    {
        var that = this;
        WebService.request({
            host: 'zephyr',
            payload:{},
            method:'post',
            pathFilename: 'project/' + purchaseId + '/unpurchase?_method=POST',
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                        that.sendNotification(ZappGadgetProjectProxy.PROJECT_UNPURCHASE_SUCCESS, data);
                    }
                    else {
                        that.sendNotification(ZappGadgetProjectProxy.PROJECT_UNPURCHASE_FAILURE, data);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(ZappGadgetProjectProxy.PROJECT_UNPURCHASE_FAILURE, detail);
                }
            }
        });
    }
    
    
     
    /**
     * permissions is a json:
     * 
     * {
     * 	hideInStoreList: true | false,
     *  inviteOnly:  true | false
     * }
     */
    this.setPermissionsPrice = function(id, permissions, price, licenseTypeId) {
        var that = this;
 
        var payloadLicense = {
            	hideInStoreList: permissions.hideInStoreList,
            	inviteOnly:permissions.inviteOnly,
            	embed:permissions.embed,
//                edit:permissions.edit != undefined? permissions.edit : false,
//                republish:permissions.republish != undefined? permissions.republish : false,
            	price:price, 
            	licenseTypeId:licenseTypeId
        };
        ProcessingScreen.show("Updating your app...");
        var path = 'project/' + id + '/permissionsPrice';
        WebService.request({
            host: 'zephyr',
            payload:{"license": payloadLicense},
            method:'post',
            pathFilename: path,
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                        that.sendNotification(ApplicationFacade.UPDATE_PROJECT_ID, {newValue: data.playerId, idType: IdType.APN_UUID});
                        ProcessingScreen.hide("Thank you.", 2000);
                    }
                    else {
                        that.sendNotification(ZappGadgetProjectProxy.PROJECT_PERMISSIONS_FAILURE, data);
                        ProcessingScreen.hide("There was a problem updating your app.", 4000);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(ZappGadgetProjectProxy.PROJECT_PERMISSIONS_FAILURE, detail);
                }
            }
        });

    }
    
    this.acceptZappUpgrade = function(urlName) {
        var that = this;
        WebService.request({
            host: 'zephyr',
            payload:{},
            method:'post',
            pathFilename: 'project/upgrade/' + urlName,
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                        that.sendNotification(ZappGadgetProjectProxy.PROJECT_UPGRADE_SUCCESS, data);
                        that.sendNotification(ApplicationFacade.UPDATE_PROJECT_ID, {newValue: data.id, idType: IdType.APN_UUID});
                    }
                    else {
                        that.sendNotification(ZappGadgetProjectProxy.PROJECT_UPGRADE_FAILURE, data);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(ZappGadgetProjectProxy.PROJECT_UNPURCHASE_FAILURE, detail);
                }
            }
        });
    }
    
    this.toggleFeatured = function (id) {
        var that = this;
        
        var path = 'project/' + id + '/toggleFeatured/';
        WebService.request({
            host: 'zephyr',
            payload:{},
            method:'post',
            pathFilename: path,
            callbacks: {
                success: function(data) {
                    if (data.message == 'OK') {
                        that.sendNotification(ApplicationFacade.UPDATE_PROJECT_ID, {newValue: data.urlName, idType: IdType.URL_NAME});
                    }
                    else {
                        that.sendNotification(ZappGadgetProjectProxy.PROJECT_PERMISSIONS_FAILURE, data);
                    }
                },
                failure: function(error, detail) {
                    that.sendNotification(ZappGadgetProjectProxy.PROJECT_PERMISSIONS_FAILURE, detail);
                }
            }
        });
   	
    }
    
    this.requestList = function (o)
    {
    	if (!Require(o, ['group', 'type', 'page', 'count', 'notifications'], 'ZappGadgetProjectProxy.requestList'))
            return;
        
    	var pathFilename = this.pathFilename(o);
    	
    	Log.warn(pathFilename);
        var that = this;
        
        WebService.request({
            host: 'zephyr',
            payload: {},
            pathFilename: pathFilename,
            callbacks: {
                success: function (data) {
                	var successNotification = o.notifications.success;
            		var loopLimit = 0;
            		var i = 0;
            		var p = {};
            		var num = 0;
            		var owned = false;
                	
                    if (data.projects == undefined && data.projects.anArray == undefined)
                    {
                        that.sendNotification(o.notifications.failure);
                        return;
                    }
                    
                    if (data.projects.length < loopLimit) {
                    	loopLimit = data.projects.length;
                    }
                    
                    p = {
                        count: o.count,
                        group: o.group,
                        page: o.page,
                        size: data.totalCount,
                        type: o.type,
                        token: o.token,
                        items: []
                    }
                    
                    loopLimit = (p.count <= data.projects.length) ? p.count : data.projects.length;
                    
                    switch (o.type) {
                        case 'zapp':
                        case 'gadget':
                            for (i = 0; i < loopLimit; i++) {
                                p.items.push(that.makeListItem(data.projects[i]));
                                owned = Credentials.email() != 'guest@zebrazapps.com' && data.projects[i].owned;
                                Credentials.owned(data.projects[i].projectId, owned);
                            }
                            that.appendCommentsRatings(p, true, o.notifications);
                            that.sendNotification(ZappGadgetProjectProxy.UPDATE_OWNED_LIST);
                            break;
                        case 'project':
                        	//Log.warn('Yes I got here');
                            for (i = 0; i < loopLimit; i++) {
                            	num = data.projects[i].categoryId;
                            	if(null != num)
                            		data.projects[i].category = that.category(num);
                                p.items.push(data.projects[i]);
                            }
                            
                            Log.warn('ZappGadgetProjectProxy sending success notification: ' + successNotification);
                            that.sendNotification(successNotification, p);
                            break;
                        default:
                            break;
                    }
                },
                failure: function (error, detail) {
                	var failureNotification = o.notifications.failure;
                	
                	that.sendNotification(failureNotification);
                }
            }
        });
    // }
    };
    
    this.pathFilename = function (o)
    {
	    var params = {};
	    var s = 'project';
	    var paramParts = [];
	    var i = 0;
	    
        var storeList = (o.group == 'visitor');
    	if (storeList) {
		    params['published'] = 'true';
		    params['storeList'] = 'true';
        	if (o.type == 'zapp') {
		        params['projectTypeId'] = '2';
        	} else {                  
		        params['projectTypeId'] = '1';
        	}
    	} else { 
    		if (o.tiny) {
    			s += '/tiny';
    		} else {
    			s += '/my';
    		}
    		
        	switch (o.type)
            {
                case 'project':    
	                params['authorMemberId'] = Credentials.userId();
	                params['projectTypeId'] = '2';
                    break;
                case 'zapp':        
	                params['ownerAccountId'] = Credentials.accountId();
	                params['published'] = 'true';
	                params['projectTypeId'] = '2';
                    break;
                case 'gadget':              
	                params['ownerAccountId'] = Credentials.accountId();
	                params['published'] = 'true';
	                params['projectTypeId'] = '1';
                    break;
                default:
                    break;
            }
    	}

    	// Override. There's probably a better way to do this...
    	if (o.authorMemberId != undefined) {
    		s = "project";
    		params['authorMemberId'] = o.authorMemberId;
    	}
    		
	    params['size'] = (o.count); // zephyr return one more than requested...
	    params['offset'] = (o.page * o.count);
        if (o.filters != undefined)
        {
            if (undefined != o.filters.category && o.filters.category != 'all') {
	            params['categoryId'] = o.filters.category;
            }
            if(o.filters.featured != undefined) {
	            params['featured'] = o.filters.featured;
            }
	        switch(o.filters.sort) {
		        case 'createdDateTime':      params['sort'] = 'createdDateTime'; params['order'] = 'desc'; break;
		        case 'nameAscending':        params['sort'] = 'name';           params['order'] = 'asc';  break;
		        case 'nameDescending':       params['sort'] = 'name';           params['order'] = 'desc'; break;
		        case 'popularityDescending': params['sort'] = 'popularity';     params['order'] = 'desc'; break;
		        case 'priceAscending':       params['sort'] = 'price';          params['order'] = 'asc';  break;
		        case 'priceDescending':      params['sort'] = 'price';          params['order'] = 'desc'; break;
		        case 'ratingDescending':     params['sort'] = 'rating';         params['order'] = 'desc'; break;
		        default: Log.error('ZappGadgetProjectProxy.pathFilename - unknown sort:'+o.filters.sort);
	        }
            if (o.filters.search != ''&& o.filters.search != undefined) {
	            params['genericSearch'] = encodeURIComponent(o.filters.search);
            }
            if (o.filters.projectName != '' && o.filters.projectName != undefined) {
	            params['projectName'] = encodeURIComponent(o.filters.projectName);
            }
            if (o.filters.retired != '' && o.filters.retired != undefined) {
	            params['retired'] = o.filters.retired;
            }
        }
	    
	    for(var name in params) {
		    if(params.hasOwnProperty(name)) {
		        paramParts[i++] = name + '=' + encodeURIComponent(params[name]);
		    }
	    }
	    if(paramParts.length > 0) {
		    s += '?' + paramParts.join('&');
	    }
      Log.debug('ZappGadgetProjectProxy.pathFilename - s:'+s);
        return s;
    }
    this.appendCommentsRatings = function (o, excludeComments, notifications)
    {
        //DWR acavan - don't get commments and ratings until anonymous access is implemented...
        this.sendNotification(notifications.success, o);
        return;
        
        var payload = {
            commentsRatings: {
                ids: [],
                comments: {
                    exclude: excludeComments
                }
            }
        }
        for (var i = 0; i < o.gadgets.length; i++)
            payload.commentsRatings.ids.push(o.gadgets[i].id);

        var that = this;
        WebService.request({
            payload: payload,
            pathFilename: 'zapp/commentsRatings.jsp',
            callbacks: {
                success: function (data) {
                    // add the comments and ratings in data to o...
                    for (var i = 0; i < o.gadgets.length; i++)
                    {
                        var commentRating = data.commentsRatings[o.gadgets[i].id];
                        if (commentRating != undefined)
                        {
                            o.gadgets[i].rating = commentRating.rating;
                            o.gadgets[i].myRating = commentRating.myRating;
                            o.gadgets[i].totalComments = commentRating.totalComments;
                            o.gadgets[i].comments = commentRating.comments;
                        }
                    }
                    that.sendNotification(notifications.success, o);
                },
                failure: function (error, detail) {
                    that.sendNotification(notifications.failure);
                }
            }
        });
    }
    this.requestAuthor = function (id, coordinates)
    {
        //TODO - waiting for web service
        var o = {
            'author.email': 'john@smith.com',
            'author.name': 'John Smith',
            'author.phone': '123-456-7890',
            'author.published.gadgets': 6,
            'author.published.zapps': new Date().getMilliseconds(),
            'author.twitter': '@johnsmith',
            'author.website': 'www.johnsmith.com',
            'company.address1':  '123 Main Street',
            'company.city': 'Your Town',
            'company.country': 'USA',
            'company.email': 'company@smith.com',
            'company.name':  'Smith & Smith, Inc.',
            'company.phone': '234-567-8901',
            'company.published.gadgets': 12, 
            'company.published.zapps': 6,
            'company.state': 'AL',
            'company.twitter': '@smithAndSmith',
            'company.website': 'www.smithandsmith.com',
            coordinates: coordinates
        }
        //Log.debug('ZappGadgetProjectProxy.requestAuthor - author.published.zapps:'+o['author.published.zapps']);
        this.sendNotification(ZappGadgetProjectProxy.REQUEST_AUTHOR_REPLY, o);
    }
    

    this.requestDetailByApnUUID = function (apnUUID)
    {
    	var path = 'project/apn/' + apnUUID;
    	this.requestDetailInternal(path);
    }

    this.requestDetail = function (id, idType) {
    	var path = 'project/';
    	var cmd = '';
    	var loadMinimal = false;
    	switch(idType) {
    	case IdType.URL_NAME:
    		cmd = 'urlName/';
    		break;
    	case IdType.PROJECT_ID:
    		cmd = '';
    		loadMinimal = true;
    		break;
    	case IdType.APN_UUID:
    		cmd = 'apn/';
    		break;
    	case IdType.PUBLISH_ID:
    		cmd = 'dep_pubId/';
   		default:
   			break;
    	}
    	var fullPath = path + cmd + id;
    	if(loadMinimal) {
    		fullPath += '?lod=minimal';
    	}
    	this.requestDetailInternal(fullPath);
    }
    
    this.requestDetailInternal = function (path) {
        //Log.debug('ZappGadgetProjectProxy.requestDetail - pathFilename:'+pathFilename);

        var that = this;
        WebService.request({
            host: 'zephyr',
            payload: {},
            pathFilename: path,
            callbacks: {
                success: function (data) {
                    //Log.debug('ZappGadgetProjectProxy.requestFeaturedList - success ('+JSON.encode(data)+')');

	                that.sendNotification(ZappGadgetProjectProxy.REQUEST_DETAIL_REPLY, that.makeDetailItem(data));
                },
                failure: function (error, detail) {
                    //Log.debug('ZappGadgetProjectProxy.get - failure ('+JSON.encode({error: error, detail: detail})+')');
                    that.sendNotification(ZappGadgetProjectProxy.REQUEST_DETAIL_FAILURE);
                }
            }
        });
    }

    
    
    this.iconURL = function (zg, size)
    {
        if (zg.icon == undefined || zg.icon.baseUrl == undefined)
        {
            //Log.warn('ZappGadgetProjectProxy.iconURL - zg.icons undefined');
            return 'https://s3.amazonaws.com/com.alleni.zebra.site.content/icons/genericIcon50.png';
        }
        return zg.icon.baseUrl+size+zg.icon.fileNameExt;
    }
    this.screenshotURL = function (zg, size)
    {
        var url = '';
        if (zg.screenshots.length > 0)
            url = zg.screenshots[0].baseUrl+'size'+zg.screenshots[0].fileNameExt;
        return url
    }
    this.makeListItem = function (zg)
    {
    	var o = {
            id: zg.projectId,
            playerId: zg.playerId,
            author: ((zg.nameFormat == null || zg.nameFormat == 'firstLast')? zg.authorFirstName + ' ' + zg.authorLastName : zg.authorDisplayName),
            twitter: zg.twitter,
            website: zg.website,
            nameFormat: zg.nameFormat,
            displayName: zg.authorDisplayName,
            firstName: zg.authorFirstName,
            lastName: zg.authorLastName,
            authorMemberId: zg.authorMemberId,
            authorAccountId: zg.authorAccountId,
            category: this.category(zg.categoryId),
            description: zg.description != null ? zg.description : "",
            projectName: zg.projectName,
            publishedName: zg.publishedName,
            editedDateTime: zg.editedDateTime,
            published: zg.published,
            icon: this.iconURL(zg, 50),
            rating: zg.averageRating,
            commentsCount: zg.commentCount,
            commentsCountText: (zg.commentCount == 0) ? 'no comments': (zg.commentCount == 1) ? '1 comment': zg.commentCount + ' comments',
            price: zg.price/100,
            formattedPrice: AIUtils.formatPrice(zg.price),
            version: zg.versionMajor+'.'+zg.versionMinor+'.'+zg.versionDot+' ('+zg.versionBuildNumber+')', //DRY acavan
            ownerAccountId: zg.ownerAccountId,
            hideInStoreList: zg.hideInStoreList,
            shareTypes: zg.shareTypes,
            inviteOnly: zg.inviteOnly,
            userAccessPrivs: zg.permissions,
            source: Credentials.accountId()== zg.accountId? 'Published' : 'Purchased',
	        publisher: zg.publisher,
	        owned: zg.owned,
	        featured: zg.featured,
	        urlName: zg.urlName,
	        updateAvailable: zg.updateAvailable,
	        projectAccountTypeName: zg.projectAccountTypeName
	        
        }
        //Log.debug('category'+o.category);
        if (zg.isHideInStoreList) {
            o.source = 'Cloud';
        }
        o.buy = this.makeBuyItem(o);
        return o;
    }
    this.makeDetailItem = function (zg)
    {
    	var temp1 = '';
    	var temp2 = '';
    	var strLim = 0;
    	
    	var authorListValue = (zg.nameFormat == null || zg.nameFormat == 'firstLast')? zg.authorFirstName + ' ' + zg.authorLastName : zg.authorDisplayName;
    	var authorListLink = '<a href="#authorProfile?id=' + zg.authorMemberId + '">' + authorListValue + '</a>';
    	
    	var o = {
            detail: {
                authors: [
                   (zg.nameFormat == null || zg.nameFormat == 'firstLast')? zg.authorFirstName + ' ' + zg.authrLastName : zg.authorDisplayName
                ],
                authorList: authorListLink ,
                category: this.category(zg.categoryId),
                commentCount: 0,
                comments: [],
                company: zg.companyName,
                description: zg.description != null ? zg.description : "",
                editLevel: '[?]',
                embedPermission: zg.permissions.embed,
                embedContentWidth: zg.width,
                embedContentHeight: zg.height,
                hideInStoreList: zg.hideInStoreList,
                icon: this.iconURL(zg, 50),
                id: zg.projectId,
                playerId: zg.apnUUID,
                publishedName: zg.publishedName,
                editedDateTime: zg.editedDateTime,
                published: zg.published,
                projectName: zg.projectName,
                memberRating: '[?]',
                numberSold: '[?]',
                playedCount: '[?]',
                previewScreenshot: this.iconURL(zg, 180),
                price: zg.price/100,
                minPrice: zg.minPrice,
                formattedPrice: AIUtils.formatPrice(zg.price),
                publishedCount: '[?]',
                ratedCount: '[?]',
                rating: zg.averageRating, //'[?]',
                screenshots: [
                ],
                sharedCount: '[?]',
                tags: [
                ],
                totalRevenue: '[?]',
                type: zg.projectTypeId == 1 ? 'gadget' : 'zapp',
                formattedType: zg.projectTypeId == 1 ? 'Gadget' : 'App',
                url: '[?]',
                version: zg.versionMajor+'.'+zg.versionMinor+'.'+zg.versionDot+' ('+zg.versionBuildNumber+')',  //DRY acavan
                hideInStoreList: zg.hideInStoreList,
                shareTypes: zg.shareTypes,
                inviteOnly: zg.inviteOnly,
                userAccessPrivs: zg.permissions,
                licenseTypeId: zg.licenseTypeId,
                publisher: zg.publisher,
    	        owned: zg.owned,
    	        featured: zg.featured,
    	        urlName: zg.urlName,
                updateAvailable: zg.updateAvailable,
    	        projectAccountTypeName: zg.projectAccountTypeName
            }
        }

        if (o.previewScreenshot == undefined)
            o.previewScreenshot = './com/alleni/images/detailPlayerBackground.jpg';
        o.detail.buy = this.makeBuyItem(o.detail);
        for(var j=0; j<zg.screenshots.length; j++)
    	{
        	o.detail.screenshots.push({baseUrl:zg.screenshots[j].baseUrl,fileNameExt:zg.screenshots[j].fileNameExt});
    	}
        
        return o;
    }
    this.makeBuyItem = function (zg)
    {
        //TOTO acavan - ultimately feature/browse/detail should all use the same mechanism (whatever that might be)
        o = {
            id: zg.id,
            description: zg.description != null ? zg.description : "",
            name: zg.projectName,
     		publishedName: zg.publishedName,
            price: zg.price,
            formattedPrice: zg.formattedPrice,
            icon: zg.icon,
            featured: zg.featured,
            owned: zg.owned,
            urlName: zg.urlName,
            embedPermission: zg.embedPermission,
            embedContentWidth: zg.embedContentWidth,
            embedContentHeight: zg.embedContentHeight,
            embedContentWidthPadding: zg.embedContentWidthPadding,
            embedContentHeightPadding: zg.embedContentHeightPadding
        }
        return o;
    }
    this.category = function (id)
    {
        return this.categories[id];
    }

    this.emailLink = function(o) {
        var that = this;
        ProcessingScreen.show("Sending email...");
        WebService.request({
            payload: { to: o.emailTo, from: o.emailFrom, msg: o.emailBody },
            pathFilename: 'zapp/email.jsp',
            callbacks: {
                success: function(data) {
                    that.sendNotification(ZappGadgetProjectProxy.SEND_EMAIL_REPLY, data.tinyUrl);
                    ProcessingScreen.hide("Thank you.", 2000);
                },
                failure: function(error, detail) {
                    that.sendNotification(ZappGadgetProjectProxy.SEND_EMAIL_FAILURE, error);
                    ProcessingScreen.hide("There was a problem sending your email.", 4000);
                }
            }
        });
    }
    
    this.tinyUrl = function(tinyUrlParams) {
        var that = this;
        WebService.request({
            payload: { id: tinyUrlParams.projectId, idType: tinyUrlParams.idType },
            pathFilename: 'zapp/tinyurl.jsp',
            callbacks: {
                success: function(data) {
                    that.sendNotification(ZappGadgetProjectProxy.TINY_URL_REPLY, data);
                },
                failure: function(error, detail) {
                    that.sendNotification(ZappGadgetProjectProxy.TINY_URL_FALURE, error);
                }
            }
        });
    }
}
ZappGadgetProjectProxy = new Class(new ZappGadgetProjectProxy());
ZappGadgetProjectProxy.NAME = 'ZappGadgetProjectProxy';
ZappGadgetProjectProxy.REQUEST_AUTHOR_REPLY = 'ZappGadgetProjectProxy.REQUEST_AUTHOR_REPLY';
ZappGadgetProjectProxy.REQUEST_DETAIL_FAILURE = 'ZappGadgetProjectProxy.REQUEST_DETAIL_FAILURE';
ZappGadgetProjectProxy.REQUEST_DETAIL_REPLY = 'ZappGadgetProjectProxy.REQUEST_DETAIL_REPLY';
ZappGadgetProjectProxy.TYPE_ZAPP = 'ZappGadgetProjectProxy.TYPE_ZAPP';
ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS = 'ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS';
ZappGadgetProjectProxy.REQUEST_PURCHASE_FAILURE = 'ZappGadgetProjectProxy.REQUEST_PURCHASE_FAILURE';
ZappGadgetProjectProxy.UPDATE_OWNED_LIST = 'ZappGadgetProjectProxy.UPDATE_OWNED_LIST';
ZappGadgetProjectProxy.TINY_URL_REPLY = 'ZappGadgetProjectProxy.TINY_URL_REPLY';
ZappGadgetProjectProxy.TINY_URL_FAILURE = 'ZappGadgetProjectProxy.TINY_URL_FAILURE';
ZappGadgetProjectProxy.SEND_EMAIL_REPLY = 'ZappGadgetProjectProxy.SEND_EMAIL_REPLY';
ZappGadgetProjectProxy.SEND_EMAIL_FAILURE = 'ZappGadgetProjectProxy.SEND_EMAIL_FAILURE';
ZappGadgetProjectProxy.PROJECT_DELETED_SUCCESS = 'ZappGadgetProjectProxy.PROJECT_DELETED_SUCCESS';
ZappGadgetProjectProxy.PROJECT_DELETED_FAILURE = 'ZappGadgetProjectProxy.PROJECT_DELETED_FAILURE';
ZappGadgetProjectProxy.PROJECT_UNPUBLISH_SUCCESS = 'ZappGadgetProjectProxy.PROJECT_UNPUBLISH_SUCCESS';
ZappGadgetProjectProxy.PROJECT_UNPUBLISH_FAILURE = 'ZappGadgetProjectProxy.PROJECT_UNPUBLISH_FAILURE';
ZappGadgetProjectProxy.PROJECT_UNPURCHASE_SUCCESS = 'ZappGadgetProjectProxy.PROJECT_UNPURCHASE_SUCCESS';
ZappGadgetProjectProxy.PROJECT_UNPURCHASE_FAILURE = 'ZappGadgetProjectProxy.PROJECT_UNPURCHASE_FAILURE';
ZappGadgetProjectProxy.PROJECT_UNPURCHASE_SUCCESS = 'ZappGadgetProjectProxy.PROJECT_UPGRADE_SUCCESS';
ZappGadgetProjectProxy.PROJECT_UNPURCHASE_FAILURE = 'ZappGadgetProjectProxy.PROJECT_UPGRADE_FAILURE';
ZappGadgetProjectProxy.ZAPPGADGET_UPDATE_FAILURE = 'ZappGadgetProjectProxy.PROJECT_PERMISSIONS_FAILURE';
ZappGadgetProjectProxy.ZAPPGADGET_UPDATE_SUCCESS = 'ZappGadgetProjectProxy.PROJECT_PERMISSIONS_FAILURE';
