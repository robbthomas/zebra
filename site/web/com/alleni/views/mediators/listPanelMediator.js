var ListPanelMediator = function(viewComponent/*ListPanel*/)
{
    this.Extends = BaseMediator;
    this.subViewNames = ['BrowsePanel', 'FeaturedPanel'];
    
    this.group = '';
    this.type = '';
    this.token = '';
    this.view = undefined;
    
    this.buyDialog = undefined;
    
    this.initialize = function(mediatorName,viewComponent)
    {
    	this.token = (mediatorName != undefined)?mediatorName:ListPanelMediator.NAME;
        this.parent(this.token, viewComponent);
        this.view = this.getViewComponent();
        
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.view.addEvent(ListPanel.INITIALIZED, this.onInitialized);
    };
    
    this.onInitialized = function()
    {
    	//Move this to BaseMediator.onInitialized() and have this mediator call super.onInitialized() here
    	for (var i = 0; i < this.subViewNames.length; i++) {
    		this.createSubMediator(this.subViewNames[i]);
    	}
    	
     	this.onTabClicked = this.onTabClicked.bindWithEvent(this);
        this.view.addEvent(ListPanel.TAB_CLICKED, this.onTabClicked);
        
        this.onDisplayChange = this.onDisplayChange.bindWithEvent(this);
        this.view.addEvent(ListPanel.DISPLAY, this.onDisplayChange);
        
        this.sendNotification(ListPanelMediator.INITIALIZED);
    };
    
    this.onCreationComplete = function() {
    	this.sendNotification(BrowsePanel.LIST_TYPE_CHANGE + this.token, {caller: ListPanelMediator.NAME+ " checkParts()", group: this.view.getGroup(), type: this.view.getType(), authorMemberId: 0, hash: 'list'});
    };
    
    this.onTabClicked = function(o) {
    	Log.warn("ListPanelMediator::onTabClicked()");
    	o.authorMemberId = this.authorMemberId;
    	o.caller = ListPanelMediator.NAME + " onTabClicked() - " + (new Date()).getMilliseconds();
    	this.sendNotification(BrowsePanel.LIST_TYPE_CHANGE + this.token, o);
    };
    
    this.onDisplayChange = function (o) {
    	o.authorMemberId = 0;
    	o.caller = ListPanelMediator.NAME + " onDisplayChange()";
    	this.sendNotification(BrowsePanel.LIST_TYPE_CHANGE + this.token, o);
    };
    
    this.listNotificationInterests = function()/*Array*/
    {
        return [
            BrowsePanelMediator.INITIALIZED + this.token,
            FeaturedPanelMediator.INITIALIZED + this.token,
            ZappGadgetProjectProxy.REQUEST_AUTHOR_REPLY,
            ApplicationFacade.LOAD_UPGRADE_COMPLETE,
            BrowsePanel.LIST_TYPE_CHANGE
        ];
    };
    
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.getBody();
        
        switch (notificationName)
        {
        case BrowsePanelMediator.INITIALIZED + this.token:
        case FeaturedPanelMediator.INITIALIZED + this.token:
        	var subMediatorName = notificationName.split('.')[0];
        	var subViewName = subMediatorName.substring(0, subMediatorName.length - 8);
        	this.subComponents[subViewName].mediatorInitialized = true;
        	this.checkParts();
        	break;
        case ZappGadgetProjectProxy.REQUEST_AUTHOR_REPLY:
            this.view.displayAuthorCompanyDialog(payload);
            break;
        case ApplicationFacade.LOAD_UPGRADE_COMPLETE:
        	this.facade.retrieveMediator(UpgradeDialogMediator.NAME).open(payload);
        	break;
        case BrowsePanel.LIST_TYPE_CHANGE:
        	this.view.updateTabs(payload.type);
        	break;
        default:
            break;
        }
    };
}
ListPanelMediator = new Class(new ListPanelMediator());
ListPanelMediator.NAME = 'ListPanelMediator';
ListPanelMediator.INITIALIZED = 'ListPanelMediator.INITIALIZED'
ScriptLoader.loaded(ListPanelMediator.NAME);