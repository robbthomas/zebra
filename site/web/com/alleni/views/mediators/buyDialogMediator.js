var BuyDialogMediator = function(viewComponent/*BuyDialog*/)
{
	/**
	 * TODO - SBJ: Combine this and the JoinPanelMediator,
	 * buyDialog.js and joinPanel.js because too much
	 * of the logic is exactly the same.  The remaining UI 
	 * items that are different can be conditionally changed
	 * by the view.js.  Styles can switch between overlayContent 
	 * & contentRight.  No sense in maintaining all this in two spots.
	 */
    this.Extends = Mediator;
    this.buyDialog = undefined;
    this.successCallback = undefined;
    this.initialized = false;
    this.openWhenInitialized = false;
    this.openArgument = undefined;
    this.couponProxy = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(BuyDialogMediator.NAME, viewComponent);
        this.buyDialog = this.getViewComponent();

        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.INITIALIZED, this.onInitialized);

        this.onMemberSignIn = this.onMemberSignIn.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.MEMBER_SIGN_IN, this.onMemberSignIn);

        this.onGetItemDetails = this.onGetItemDetails.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.GET_ITEM_DETAILS, this.onGetItemDetails);
        
        this.onMemberPurchaseItem = this.onMemberPurchaseItem.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.MEMBER_PURCHASE_ITEM, this.onMemberPurchaseItem);

        this.onVisitorPurchaseItem = this.onVisitorPurchaseItem.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.VISITOR_PURCHASE_ITEM, this.onVisitorPurchaseItem);

        this.onUsernameUnique = this.onUsernameUnique.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.USERNAME_UNIQUE, this.onUsernameUnique);

        this.onEmailUnique = this.onEmailUnique.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.EMAIL_UNIQUE, this.onEmailUnique);

        this.buyDialog.setJoinProxy(this.facade.retrieveProxy(JoinProxy.NAME));

        this.onJoin = this.onJoin.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.JOIN, this.onJoin);
        
        this.onUpdate = this.onUpdate.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.UPDATE, this.onUpdate);
        
        this.onUpgrade = this.onUpgrade.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.LOAD_UPGRADE, this.onUpgrade);
        
        this.validateCoupon = this.validateCoupon.bindWithEvent(this);
        this.buyDialog.addEvent(BuyDialog.VALIDATE_COUPON, this.validateCoupon);
        
        this.couponProxy = new CouponProxy("buyFlow");
        this.facade.registerProxy(this.couponProxy);

    }
    
    this.onUpgrade = function (o)
    {
    	this.sendNotification(ApplicationFacade.LOAD_UPGRADE, {item:o, upgradeTo:undefined});
    }
    this.onSubmit = function (o)
    {
        this.facade.retrieveProxy(JoinProxy.NAME).create(o);
    }
    this.open = function (o)
    {
        //Log.debug('BuyDialogMediator.display');
        if (this.initialized) {
            this.buyDialog.open(o);
        } else {
            Log.debug('BuyDialogMediator.open - open deferred');    
            this.openWhenInitialized = true;
            this.openArgument = o;
        }
    }
    this.onMemberSignIn = function (o)
    {
        //Log.debug('BuyDialogMediator.onMemberSignIn');
        this.facade.retrieveProxy(UserProxy.NAME).login(o);
    }
    
    this.onGetItemDetails = function (o) {
    	this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).requestDetail(o.id, IdType.PROJECT_ID);
    }
    
    this.onMemberPurchaseItem = function (o)
    {
        if (!Require(o, ['itemId'], 'BuyDialogMediator.onMemberPurchaseItem'))
            return;

        //Log.debug('BuyDialogMediator.onMemberPurchaseItem - itemId:'+o.itemId);
        this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).requestPurchase([o.itemId]);
    }
    this.onVisitorPurchaseItem = function (o)
    {
        if (!Require(o, ['itemId', 'form'], 'BuyDialogMediator.onVisitorPurchaseItem'))
            return;
        
        //Log.debug('BuyDialogMediator.onVisitorPurchaseItem');
    }
    this.onInitialized = function()
    {
        this.initialized = true;
        if (this.openWhenInitialized) {
            this.open(this.openArgument);
            this.openWhenInitialized = false;
        }

    }
    this.onUsernameUnique = function(username)
    {
        this.facade.retrieveProxy(UserProxy.NAME).usernameUnique(username);
    }
    this.onEmailUnique = function(email)
    {
        this.facade.retrieveProxy(UserProxy.NAME).emailUnique(email);
    }
    this.onJoin = function (o)
    {
        this.facade.retrieveProxy(JoinProxy.NAME).create(o);
    }
    this.onUpdate = function (o)
    {
    	this.facade.retrieveProxy(AccountProxy.NAME).update(o.formName,o.values);
    }
    this.validateCoupon = function (o)
    {
    	this.couponProxy.validate(o);
    }
    
    this.listNotificationInterests = function()/*Array*/
    {
        return [
            UserProxy.LOGIN_SUCCESS,
            UserProxy.LOGIN_FAILURE,
            UserProxy.LOGIN_BAD_CREDENTIALS,
            UserProxy.EMAIL_UNIQUE_SUCCESS,
            UserProxy.EMAIL_UNIQUE_FAILURE,
            UserProxy.USERNAME_UNIQUE_SUCCESS,
            UserProxy.USERNAME_UNIQUE_FAILURE,
            JoinProxy.CREATE_SUCCESS,
            AccountProxy.UPDATE_SUCCESS,
            JoinProxy.CREATE_FAILURE,
            CouponProxy.VALIDATE_SUCCESS+"buyFlow",
            CouponProxy.VALIDATE_FAILURE+"buyFlow",
            ZappGadgetProjectProxy.REQUEST_PURCHASE_FAILURE,
            ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS,
            ZappGadgetProjectProxy.REQUEST_DETAIL_REPLY
        ];
     }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName()
        var payload = notification.getBody();
        switch(notificationName)
        {
        case UserProxy.LOGIN_SUCCESS:
            this.buyDialog.onLoginSuccess();
            break;
        case UserProxy.LOGIN_BAD_CREDENTIALS:
            this.buyDialog.onLoginBadCredentials();
            break;
        case UserProxy.EMAIL_UNIQUE_SUCCESS:
            this.buyDialog.emailUnique(payload);
            break;
        case UserProxy.EMAIL_UNIQUE_FAILURE:
            break;
        case UserProxy.USERNAME_UNIQUE_SUCCESS:
            this.buyDialog.usernameUnique(payload);
            break;
        case UserProxy.USERNAME_UNIQUE_FAILURE:
            break;
        case JoinProxy.CREATE_SUCCESS:
        	this.buyDialog.onSubmitSuccess(payload);
        	break
        case AccountProxy.UPDATE_SUCCESS:
            this.buyDialog.onSubmitSuccess(payload.data);
            break;
        case JoinProxy.CREATE_FAILURE:
        	this.buyDialog.onSubmitFailure(payload);
            break;
        case CouponProxy.VALIDATE_SUCCESS+"buyFlow":
        	this.buyDialog.onCouponValidateSuccess(payload);
        	break;
        case CouponProxy.VALIDATE_FAILURE+"buyFlow":
        	this.buyDialog.onCouponValidateFailure(payload);
        	break;
        case ZappGadgetProjectProxy.REQUEST_PURCHASE_FAILURE:
        	 this.buyDialog.hideUpdateAnimation();
        	 this.buyDialog.muteZappGadgetBuy(true);
        	 this.buyDialog.close();
        	 //alert('ZappGadgetProjectProxy.requestPurchase - failure\nerror:'+payload);
        	 break;
        case ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS:
        	this.buyDialog.hideUpdateAnimation();
        	this.buyDialog.muteZappGadgetBuy(true);
        	this.buyDialog.close();
        	break;
        case ZappGadgetProjectProxy.REQUEST_DETAIL_REPLY:
        	this.buyDialog.setItemAfterLogin(payload);
        	break;
        default:
            break;
        }
    }
}
BuyDialogMediator = new Class(new BuyDialogMediator());
BuyDialogMediator.NAME = 'BuyDialogMediator';
// ScriptLoader.loaded(BuyDialogMediator.NAME);
