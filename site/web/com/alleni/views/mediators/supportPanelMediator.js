var SupportPanelMediator = function(viewComponent/*SupportPanel*/)
{

    this.Extends = Mediator;

    this.supportPanel = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(SupportPanelMediator.NAME, viewComponent);
        this.supportPanel = this.getViewComponent();
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.supportPanel.addEvent(SupportPanel.INITIALIZED, this.onInitialized);

        this.onLinkItemSelected = this.onLinkItemSelected.bindWithEvent(this);
        this.supportPanel.addEvent(SupportPanel.LINK_ITEM_SELECTED, this.onLinkItemSelected);

    }
    this.onInitialized = function()
    {
    }
    this.onLinkItemSelected = function(filename) {
        this.sendNotification(SupportPanelMediator.LINK_ITEM_SELECTED, filename);
    }

this.listNotificationInterests = function()/*Array*/
    {
        return [
            SupportPanelMediator.LINK_ITEM_SELECTED
        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName()
        switch(notificationName)
        {
            case SupportPanelMediator.LINK_ITEM_SELECTED:
                this.supportPanel.loadFragment(notification.getBody())
                break;
            default:
                break;
        }
    }
}
SupportPanelMediator = new Class(new SupportPanelMediator());
SupportPanelMediator.NAME = 'SupportPanelMediator';
SupportPanelMediator.LINK_ITEM_SELECTED = 'SupportPanelLinkItemSelected';
ScriptLoader.loaded(SupportPanelMediator.NAME);
