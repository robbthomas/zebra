var AuthorProfilePanelMediator = function(viewComponent)
{
	this.Extends = BaseMediator;
    this.subViewNames = ['BrowsePanel'];
    
    this.group = '';
    this.type = '';
    this.token = '';
    this.view = undefined;
    
    this.authorMemberId = undefined;
    
    this.initialize = function(mediatorName,viewComponent,token)
    {
    	this.parent((mediatorName != undefined)?mediatorName:AuthorProfilePanelMediator.NAME, viewComponent);
        
        this.token = AuthorProfilePanelMediator.NAME;  
        this.view = this.getViewComponent();
        
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.view.addEvent(AuthorProfilePanel.INITIALIZED, this.onInitialized);
    };
    
    this.onInitialized = function(o)
    {
    	//Move this to BaseMediator.onInitialized() and have this mediator call super.onInitialized() here
    	for (var i = 0; i < this.subViewNames.length; i++) {
    		this.createSubMediator(this.subViewNames[i]);
    	}
		
    	this.onTabClicked = this.onTabClicked.bindWithEvent(this);
        this.view.addEvent(AuthorProfilePanel.TAB_CLICKED, this.onTabClicked);
        
        this.onDisplayChange = this.onDisplayChange.bindWithEvent(this);
        this.view.addEvent(AuthorProfilePanel.DISPLAY, this.onDisplayChange);
        
        this.authorMemberId = o.authorMemberId;
        this.getAuthorData(this.authorMemberId);
    };
    
    this.onCreationComplete = function() {
    	Log.debug('AuthorProfilePanelMediator.onCreationComplete() sending BrowsePanel.LIST_TYPE_CHANGE');
    	this.sendNotification(BrowsePanel.LIST_TYPE_CHANGE + this.token, {caller: AuthorProfilePanelMediator.NAME+ " checkParts()", group: this.view.getGroup(), type: this.view.getType(), authorMemberId: this.authorMemberId, hash: 'authorProfile'});
    };
    
    this.onTabClicked = function(o) {
    	var payloadToSend = o;
    	payloadToSend.authorMemberId = this.authorMemberId;
    	payloadToSend.caller = AuthorProfilePanel.NAME + " onTabClicked()";
    	Log.debug('AuthorProfilePanelMediator.onTabClicked() sending BrowsePanel.LIST_TYPE_CHANGE');
    	this.sendNotification(BrowsePanel.LIST_TYPE_CHANGE + this.token, payloadToSend);
    }
    
    this.onDisplayChange = function (o) {
    	this.authorMemberId = o.authorMemberId;
    	this.sendNotification(BrowsePanel.CLEAR_CONTENTS + this.token);
    	this.getAuthorData(this.authorMemberId);
    	var payloadToSend = {
  			  group: this.view.getGroup()
  			, type: this.view.getType()
  			, authorMemberId: this.authorMemberId
  			, hash: 'authorProfile'
  			, caller: AuthorProfilePanelMediator.NAME + " onDisplayChange()"
  		};
    	
    	Log.debug('AuthorProfilePanelMediator.onDisplayChange() sending BrowsePanel.LIST_TYPE_CHANGE');
    	this.sendNotification(BrowsePanel.LIST_TYPE_CHANGE + this.token, payloadToSend);
    };
    
    this.getAuthorData = function(authorMemberId) {
    	this.facade.retrieveProxy(UserProxy.NAME).getAuthorProfileData(appUserId = this.authorMemberId);
    };
    
    this.getAuthorAvatar = function(avatarId) {
    	this.facade.retrieveProxy(UserProxy.NAME).getAuthorAvatarImage(avatarId, this.token);
    };
    
    this.getAuthorBanner = function(bannerId) {
    	this.facade.retrieveProxy(UserProxy.NAME).getAuthorBannerImage(bannerId, this.token);
    };
    
    this.listNotificationInterests = function()/*Array*/
    {
        return [
                UserProxy.GET_AUTHOR_PROFILE_DATA_SUCCESS
              , UserProxy.GET_AUTHOR_AVATAR_SUCCESS+this.token
              , UserProxy.GET_AUTHOR_BANNER_SUCCESS+this.token
              , BrowsePanelMediator.INITIALIZED + this.token
              , BrowsePanel.LIST_TYPE_CHANGE
        ];
    };
    
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.getBody();
        
        switch(notificationName)
        {
        case BrowsePanelMediator.INITIALIZED + this.token:
        	var subMediatorName = notificationName.split('.')[0];
        	var subViewName = subMediatorName.substring(0, subMediatorName.length - 8);
        	this.subComponents[subViewName].mediatorInitialized = true;
        	this.checkParts();
        	break;
        case BrowsePanel.LIST_TYPE_CHANGE:
        case BrowsePanel.LIST_TYPE_CHANGE + this.token:
        	this.view.setAuthorMemberId(this.authorMemberId);
        	this.view.updateTabs(payload.type);
        	break;
        case UserProxy.GET_AUTHOR_PROFILE_DATA_SUCCESS:
        	var displayName = (payload.nameFormat == 'firstLast') ? payload.firstName + ' ' + payload.lastName : payload.displayName;
        	var displayData = {
        		displayName: displayName,
        		twitter: payload.twitter,
        		website: payload.website,
        		bio: payload.description,
        		company: payload.addressCompany
        	};
        	this.view.displayAuthorData(displayData);
        	
        	//this needs to be here so that we have avatarId before we need it :)
        	if (payload.avatarid > 0) {
        		this.getAuthorAvatar(payload.avatarid);
        	} else {
        		this.view.hideAuthorAvatar();
        	}
        	
        	if (payload.bannerid > 0) {
        		this.getAuthorBanner(payload.bannerid);
        	} else {
        		this.view.hideAuthorBanner();
        	}
        	
        	break;
        case UserProxy.GET_AUTHOR_AVATAR_SUCCESS+this.token:
        	this.view.displayAuthorAvatar(payload);
        	break;
        case UserProxy.GET_AUTHOR_BANNER_SUCCESS+this.token:
        	this.view.displayAuthorBanner(payload);
        	break;
        default:
            break;
        }
    };
    
};
AuthorProfilePanelMediator = new Class(new AuthorProfilePanelMediator());
AuthorProfilePanelMediator.NAME = 'AuthorProfilePanelMediator';
ScriptLoader.loaded(AuthorProfilePanelMediator.NAME);
