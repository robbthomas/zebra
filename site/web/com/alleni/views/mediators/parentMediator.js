var ParentMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.view = undefined;
    
    this.initialize = function(viewComponent)
    {
        this.parent(ParentMediator.NAME, viewComponent);
        
        this.view = this.getViewComponent();

        this.onInitialized = this.onInitialized.bindWithEvent(this);
    }
    
    this.onInitialized = function()
    {
    	// Nothing much do to here.
    }
    
    this.setSubMediator = function(subMediator, viewName, subViewComponent) {
    	this.facade.registerMediator(subMediator);
		this[viewName].injectView(subViewComponent.element);
		//TODO: anything else need to happen here?
    }
}
ParentMediator = new Class(new ParentMediator());
ParentMediator.NAME = 'ParentMediator';
ScriptLoader.loaded(ParentMediator.NAME);