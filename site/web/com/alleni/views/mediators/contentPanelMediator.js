var ContentPanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.contentPanel = undefined;
    /**
     * 
     */
    this.initialize = function(viewComponent)
    {
        this.parent(ContentPanelMediator.NAME, viewComponent);
        this.contentPanel = this.getViewComponent();
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.contentPanel.addEvent(ContentPanel.INITIALIZED, this.onInitialized);
    }
    /**
     * 
     */
    this.onInitialized = function()
    {
        this.sendNotification(ContentPanelMediator.INITIALIZED);
    }
    /**
     * 
     */
    this.listNotificationInterests = function()
    {
        return [
                ApplicationPanelMediator.CONTENT_DISPLAY
        ];
    }
    /**
     * 
     */
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.getBody();
        switch(notificationName)
        {
        case ApplicationPanelMediator.CONTENT_DISPLAY:
        	
        	/**
        	 * We will be migrating over all content panels to subscribe to notifications to receive 
        	 * display arguments - this is needed in order to allow a mediator to create sub-views/mediators
        	 * in a clean way.  The first panel to get this treatment will be the account panel.
        	 */
        	if(payload.contentPanel == Application.contentPanels.account && payload.menuArgument == "startTransfer"){
        		this.sendNotification(AccountPanelMediator.CHANGE_DISPLAY_ARGUEMENT, payload.menuArgument);
        	}else{
        		this.contentPanel.setDisplayArgument(payload);
        	}
        	
        	this.contentPanel.injectView(payload.contentPanel.panel);
        	break;
        default:
            break;
        }
    }
}
ContentPanelMediator = new Class(new ContentPanelMediator());
ContentPanelMediator.NAME = 'ContentPanelMediator';
ContentPanelMediator.INITIALIZED = 'ContentPanelMediator.INITIALIZED';
// ScriptLoader.loaded(ContentPanelMediator.NAME);