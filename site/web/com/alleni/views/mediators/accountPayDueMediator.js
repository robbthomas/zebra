var AccountPayDueMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.view = undefined;
    this.token = "";
    this.payload = undefined;
    /**
     * 
     */
    this.initialize = function(mediatorName, viewComponent, token)
    {
        this.parent((mediatorName != undefined)?mediatorName:AccountPayDueMediator.NAME, viewComponent);
        this.token = token;
        this.view = this.getViewComponent();
        
        this.loadComplete = this.loadComplete.bindWithEvent(this);
        this.view.addEvent(AccountPayDue.LOAD_COMPLETE, this.loadComplete);
        

        this.close = this.close.bindWithEvent(this);
        this.view.addEvent(AccountPayDue.CLOSE, this.close);

    }
    
    this.close = function()
    {
    	this.sendNotification(ApplicationFacade.CLOSE_DIALOG+this.token);
    }
    
    this.loadComplete = function()
    {
    	
    }
    /**
     * 
     */
    this.listNotificationInterests = function()
    {
        return [
              ApplicationFacade.CONFIG_DIALOG+this.token,
              ApplicationFacade.CLOSING_DIALOG+this.token
        ];
    }
    /*
     * 
     */
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        
        switch(notificationName)
        {
        case ApplicationFacade.CLOSING_DIALOG+this.token:
        	this.onClose();
        	break;
        case ApplicationFacade.CONFIG_DIALOG+this.token:
        	this.payload = notification.getBody();
        	
        	this.view.setValues(Credentials._firstName);
        	this.sendNotification(ApplicationFacade.CENTER_DIALOG+this.token);
        	this.sendNotification(ApplicationFacade.SHOW_CLOSE_DIALOG+this.token, true);
        	break;
        default:
            break;
        }
        
    }
    /**
     * 
     */
    this.onClose = function()
    {
    	this.view = undefined;
    	this.facade.removeMediator(this.getMediatorName());
    }
    /**
     * 
     */
    this.onRegister = function()
    {
    	
    }
    /**
     * 
     */
    this.onRemove = function()
    {
    	
    }
}
AccountPayDueMediator = new Class(new AccountPayDueMediator());
AccountPayDueMediator.NAME = 'AccountPayDueMediator';
