var BaseMediator = function(viewComponent) {
	this.Extends = Mediator;
	
	this.view = undefined;
    this.token = undefined;
    this.subComponents = {};
    
    this.createSubMediator = function(subViewName) {
    	var ViewClass = getClassByName(subViewName);
    	var subMediatorName = subViewName + 'Mediator';
    	var MediatorClass = getClassByName(subMediatorName);
    	this.subComponents[subViewName] = {};
    	this.subComponents[subViewName].mediatorInitialized = false;
    	this.subComponents[subViewName].mediator = 	new MediatorClass(
    														  subMediatorName + '_' + this.token
    														, new ViewClass(subViewName.charAt(0).toLowerCase() + subViewName.slice(1))
    														, this.token
    													);
    	this.facade.registerMediator(this.subComponents[subViewName].mediator);
    };
    
    this.onRemove = function() {
    	for(var i = 0; i < this.subViewNames.length; i++) {
    		this.facade.removeMediator(this.subComponents[this.subViewNames[i]].mediator);
    	}
    	this.view = null;
    };
    
    this.handleNotification = function (notification) {
    	var notificationName = notification.getName();
        var subMediatorName = notificationName.split('.')[0];
    	var subViewName = subMediatorName.substring(0, subMediatorName.length - 8);
    	
    	this.subComponents[subViewName].mediatorInitialized = true;
    	this.checkParts();
    };
    
    this.checkParts = function() {
    	var allPartsReady = true;
    	
    	for (var i = 0; i < this.subViewNames.length; i++) {
    		if (!this.subComponents[this.subViewNames[i]].mediatorInitialized) {
    			allPartsReady = false;
    		}
    	}
    	
    	if (allPartsReady) {
    		this.onCreationComplete();
    	}
    };
    
    
    this.onCreationComplete = function() {};
}
BaseMediator = new Class(new BaseMediator());
BaseMediator.NAME = 'BaseMediator';
ScriptLoader.loaded(BaseMediator.NAME);