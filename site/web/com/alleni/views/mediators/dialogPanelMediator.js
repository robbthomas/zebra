var DialogPanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.view = undefined;
    this.childClose = null;
    this.token = "";
    /**
     * 
     */
    this.initialize = function(mediatorName, viewComponent, token)
    {
        this.parent((mediatorName != undefined)?mediatorName:DialogPanelMediator.NAME, viewComponent);
        this.view = this.getViewComponent();
        this.token = token;
        
        this.close = this.close.bindWithEvent(this);
        this.view.addEvent(DialogPanel.CLOSE, this.close);
        
        this.closeByUser = this.closeByUser.bindWithEvent(this);
        this.view.addEvent(DialogPanel.CLOSE_BY_USER, this.closeByUser);
    }
    
    this.closeByUser = function()
    {
    	this.view = undefined;
    	this.facade.removeMediator(this.getMediatorName());
    	this.sendNotification(ApplicationFacade.CLOSING_DIALOG_BY_USER+this.token);
    }
    
    this.close = function()
    {
    	this.view = undefined;
    	this.facade.removeMediator(this.getMediatorName());
    	this.sendNotification(ApplicationFacade.CLOSING_DIALOG+this.token);
    }
    
    /**
     * 
     */
    this.listNotificationInterests = function()
    {
        return [
                ApplicationFacade.CENTER_DIALOG+this.token,
                ApplicationFacade.RETITLE_DIALOG+this.token,
                ApplicationFacade.CLOSE_DIALOG+this.token,
                ApplicationFacade.SHOW_CLOSE_DIALOG+this.token,
                ApplicationFacade.CLOSE_ALL_DIALOGS
        ];
    }
    /*
     * 
     */
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        
        switch(notificationName)
        {
        case ApplicationFacade.CENTER_DIALOG+this.token:
        	this.view.recenter();
        	break;
        case ApplicationFacade.RETITLE_DIALOG+this.token:
        	this.view.title(notification.getBody());
        	break;
        case ApplicationFacade.CLOSE_DIALOG+this.token:
        case ApplicationFacade.CLOSE_ALL_DIALOGS:
        	// handle child view request dialog close
        	this.view.close();
        	break;
        case ApplicationFacade.SHOW_CLOSE_DIALOG+this.token:
        	this.view.showClose(notification.getBody());
        	break;
        default:
            break;
        }
        
    }
    /**
     * 
     */
    this.onRegister = function()
    {
    	
    }
    /**
     * 
     */
    this.onRemove = function()
    {
    	
    }

}
DialogPanelMediator = new Class(new DialogPanelMediator());
DialogPanelMediator.NAME = 'DialogPanelMediator';
