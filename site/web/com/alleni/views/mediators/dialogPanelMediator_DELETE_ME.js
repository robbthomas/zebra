var DialogPanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.dialogPanel = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(DialogPanelMediator.NAME, viewComponent);
        this.dialogPanel = this.getViewComponent();
    }
    this.listNotificationInterests = function()
    {
        return [
        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName()
        switch(notificationName)
        {
        default:
            break;
        }
    }
}
DialogPanelMediator = new Class(new DialogPanelMediator());
DialogPanelMediator.NAME = 'DialogPanelMediator';
