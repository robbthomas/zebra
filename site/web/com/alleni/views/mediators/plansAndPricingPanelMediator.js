var PlansAndPricingPanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.plansAndPricingPanel = undefined;

    this.initialize = function (viewComponent)
    {
        this.parent(PlansAndPricingPanelMediator.NAME, viewComponent);
        this.plansAndPricingPanel = this.getViewComponent();
        
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.plansAndPricingPanel.addEvent(PlansAndPricingPanel.INITIALIZED, this.onInitialized);

        this.onPlanSelected = this.onPlanSelected.bindWithEvent(this);
        this.plansAndPricingPanel.addEvent(PlansAndPricingPanel.PLAN_SELECTED, this.onPlanSelected);
    }
    this.onInitialized = function ()
    {
    }
    this.onPlanSelected = function (plan)
    {
        this.facade.retrieveProxy(JoinProxy.NAME).selectedPlan(plan);
        this.sendNotification(Application.PLAN_SELECTED);
    }
    this.listNotificationInterests = function ()
    {
        return [
        ];
    }
    this.handleNotification = function (notification)
    {
        var notificationName = notification.getName()
        switch(notificationName)
        {
        default:
            break;
        }
    }
}
PlansAndPricingPanelMediator = new Class(new PlansAndPricingPanelMediator());
PlansAndPricingPanelMediator.NAME = 'PlansAndPricingPanelMediator';
ScriptLoader.loaded(PlansAndPricingPanelMediator.NAME);
