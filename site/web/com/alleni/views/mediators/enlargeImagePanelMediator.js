var EnlargeImagePanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.view = undefined;
    this.token = "";
    this.payload = undefined;
    
    this.index = undefined;
    this.total = undefined;
    /**
     * 
     */
    this.initialize = function(mediatorName, viewComponent, token)
    {
        this.parent((mediatorName != undefined)?mediatorName:EnlargeImagePanelMediator.NAME, viewComponent);
        this.token = token;
        this.view = this.getViewComponent();
        
        this.loadComplete = this.loadComplete.bindWithEvent(this);
        this.view.addEvent(EnlargeImagePanel.LOAD_COMPLETE, this.loadComplete);
        
        this.showPrevious = this.showPrevious.bindWithEvent(this);
        this.view.addEvent(EnlargeImagePanel.PREVIOUS, this.showPrevious);
        
        this.showNext = this.showNext.bindWithEvent(this);
        this.view.addEvent(EnlargeImagePanel.NEXT, this.showNext);
        // turn on dialog close capability
        this.sendNotification(ApplicationFacade.SHOW_CLOSE_DIALOG+this.token, true);
    }
    
    this.loadComplete = function()
    {
    	this.sendNotification(ApplicationFacade.CENTER_DIALOG+this.token);
    }
    
    this.showPrevious = function() {
    	this.index--;
    	this.showScreenshot(this.index);
    }
    
    this.showNext = function() {
    	this.index++;
    	this.showScreenshot(this.index);
    };
    
    this.showScreenshot = function(index) {
    	this.view.setPic(this.payload.screenshots[index].baseUrl+'450'+this.payload.screenshots[index].fileNameExt);
    	var prevVisible = index > 0;
    	var nextVisible = index < this.total - 1;
    	this.view.setLinkVisibility(prevVisible, nextVisible);
    };
    
    /**
     * 
     */
    this.listNotificationInterests = function()
    {
        return [
              ApplicationFacade.CONFIG_DIALOG+this.token,
              ApplicationFacade.CLOSING_DIALOG+this.token
        ];
    }
    /*
     * 
     */
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.getBody();
        
        switch(notificationName)
        {
        case ApplicationFacade.CLOSING_DIALOG+this.token:
        	this.onClose();
        	break;
        case ApplicationFacade.CONFIG_DIALOG+this.token:
        	this.payload = payload;
        	this.index = this.payload.positions[0];
        	this.total = this.payload.positions[1];
        	this.showScreenshot(this.index);
        	break;
        default:
            break;
        }
        
    }
    /**
     * 
     */
    this.onClose = function()
    {
    	this.view = undefined;
    	this.facade.removeMediator(this.getMediatorName());
    }
    /**
     * 
     */
    this.onRegister = function()
    {
    	
    }
    /**
     * 
     */
    this.onRemove = function()
    {
    	
    }
}
EnlargeImagePanelMediator = new Class(new EnlargeImagePanelMediator());
EnlargeImagePanelMediator.NAME = 'EnlargeImagePanelMediator';
