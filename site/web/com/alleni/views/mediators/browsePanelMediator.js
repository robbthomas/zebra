var BrowsePanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.view = undefined;
    this.token = "";
    this.zappGadgetProjectProxy = undefined;
    
    this.browseCount = 9;
    this.browsePage = 0;
    this.group = 'visitor';
    this.type = 'zapp';
    this.mediatorName = ""
    
    this.browseFilters = {
        category: 'all',
        search: '',
        sort: 'createdDateTime'
    };

    this.initialize = function(mediatorName,viewComponent,token) {
    	this.token = token;
        this.mediatorName = mediatorName;
    	this.parent((mediatorName != undefined)?mediatorName:BrowsePanelMediator.NAME, viewComponent);
    	
        this.view = this.getViewComponent();
        
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.INITIALIZED, this.onInitialized);
        
        this.zappGadgetProjectProxy = new ZappGadgetProjectProxy('zappGadgetProjectProxy_' + mediatorName, token);
        this.facade.registerProxy(this.zappGadgetProjectProxy);
    };    
    
    this.onInitialized = function() {
        this.onBrowseNext = this.onBrowseNext.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.NEXT, this.onBrowseNext);
    	
        this.onBrowsePrevious = this.onBrowsePrevious.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.PREVIOUS, this.onBrowsePrevious);
        
        this.onBrowseProfileNext = this.onBrowseProfileNext.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.PROFILE_NEXT, this.onBrowseProfileNext);
    	
        this.onBrowseProfilePrevious = this.onBrowseProfilePrevious.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.PROFILE_PREVIOUS, this.onBrowseProfilePrevious);
        
        this.onBuyClicked = this.onBuyClicked.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.BUY_CLICKED, this.onBuyClicked);
        
    	this.onUpdateZappGadget = this.onUpdateZappGadget.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.UPDATE_ZAPPGADGET, this.onUpdateZappGadget);
        
        this.onToggleFeatured = this.onToggleFeatured.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.TOGGLE_FEATURED, this.onToggleFeatured);

        this.launchEditor = this.launchEditor.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.LAUNCH_EDITOR,this.launchEditor);
        
        this.onBrowseFilterChanged = this.onBrowseFilterChanged.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.FILTER_CHANGED, this.onBrowseFilterChanged);

        this.onDeleteProject = this.onDeleteProject.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.DELETE_PROJECT, this.onDeleteProject);

        this.onUnpublishProject = this.onUnpublishProject.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.UNPUBLISH_PROJECT, this.onUnpublishProject);
        
        this.onUnpurchaseProject = this.onUnpurchaseProject.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.UNPURCHASE_PROJECT, this.onUnpurchaseProject);

        this.onClickHref = this.onClickHref.bindWithEvent(this);
        this.view.addEvent(BrowsePanel.HREF_CLICKED, this.onClickHref);
        
        this.sendNotification(BrowsePanelMediator.INITIALIZED + this.token, {html:this.getViewHtml()});
    }

    this.setGroupType = function(group, type) {
    	this.group = group;
    	this.type = type;
    }
    
    this.onDisplay = function (o)
    {
    	if(undefined == o.authorMemberId) {
    		this.authorMemberId = undefined;
    	} else {
    		this.authorMemberId = o.authorMemberId;
    	}
    	this.setGroupType(o.group, o.type);
        this.browsePage = ((o.browseStart>0)?o.browseStart-1:0)/this.browseCount;
    }
    
    this.getViewHtml = function() {
    	return this.view.getHtml();
    };
    
    this.onClickHref = function (o)
    {
        switch (o.href) {
        	case 'authorProfile':
        		this.sendNotification(Application.HREF_DISPATCH + o.href, o);
        		break;
            default:
            	this.sendNotification(Application.HREF_DISPATCH + o.href);
            	break;
        }
        return false;
    }
    
    this.onBrowsePrevious = function () 
    {
        if (this.browsePage > 0) {
        	this.requestList(this.browsePage-1, this.authorMemberId, this.type);
        }
    }
    
    this.onBrowseNext = function ()
    {
        this.requestList(this.browsePage+1, this.authorMemberId, this.type);
    }
    
    this.onBrowseProfilePrevious = function () 
    {
        if (this.browsePage > 0) {
        	this.view.setPageValues(this.browsePage - 1);
        	this.requestList(this.browsePage-1, this.authorMemberId, this.type);
        }
    }
    
    this.onBrowseProfileNext = function ()
    {
    	this.view.setPageValues(this.browsePage + 1);
        this.requestList(this.browsePage+1, this.authorMemberId, this.type);
    }
    
    this.onBuyClicked = function (o)
    {
    	this.sendNotification(ApplicationFacade.REQUEST_PURCHASE, o);
    }
    
    this.onUpdateZappGadget = function (o) 
    {
       	this.zappGadgetProjectProxy.acceptZappUpgrade(o.urlName);
    }
    
    this.onToggleFeatured = function(o) 
    {
    	this.zappGadgetProjectProxy.toggleFeatured(o.id);
    }
    
    this.launchEditor = function (cfg) 
    {
    	this.sendNotification(ApplicationFacade.LAUNCH_EDITOR, cfg);
    }
    
    this.onBrowseFilterChanged = function (o) 
    {
        if (this.browseFilters[o.group] == undefined) {
            Log.error('BrowsePanelMediator.onBrowseFilterChanged - unknown group:'+o.group);
            return;
        }
        if (this.browseFilters[o.group] != o.value) {
            this.browseFilters[o.group] = o.value;
            this.requestList(0, this.authorMemberId, this.type);
        }
    }
    
    this.onDeleteProject = function(projectId) 
    {
    	this.zappGadgetProjectProxy.deleteProject(projectId);
    }
    
    this.onUnpublishProject = function(projectId) 
    {
        this.zappGadgetProjectProxy.unpublishProject(projectId);
    }
    
    this.onUnpurchaseProject = function(purchaseId) 
    {
        this.zappGadgetProjectProxy.unpurchaseProject(purchaseId);
    }
    
    this.requestList = function (page, authorMemberId, projectType)
    {
    	Log.debug("This.requestlist() has been called.");
    	var typeToSend = 'zapp';
    	
    	if (undefined != projectType) {
    		typeToSend = projectType;
    	}
    	var o = {
        	count: this.browseCount,
            filters: this.browseFilters,
            group: this.group,
            notifications: {
                success: BrowsePanel.RETURN_BROWSE_PANEL_LIST + this.token,
                failure: BrowsePanel.RETURN_BROWSE_PANEL_LIST_FAILURE + this.token
            },
            page: page,
            refresh: true,
            type: typeToSend
        }
        
        if (undefined != authorMemberId && authorMemberId != 0) {
        	this.authorMemberId = authorMemberId; // let's see if this works...
        	o.authorMemberId = authorMemberId;
        }
        
        this.zappGadgetProjectProxy.requestList(o);
    }
    
    this.removeItems = function() {
    	this.view.removeItems();
    }
   
    this.groupTypeSupported = function(group, type) {
		switch (group+'.'+type)
	    {
	    case 'visitor.zapp':
	    case 'visitor.gadget':
	    case 'member.zapp':
	    case 'member.project':
	    case 'member.gadget':
	    	return true;
	    default:
	    	return false;
	    }
	};

   this.listNotificationInterests = function() 
   {
       return [
           BrowsePanel.RETURN_BROWSE_PANEL_LIST + this.token,
           BrowsePanel.RETURN_BROWSE_PANEL_LIST_FAILURE + this.token,
           FeedbackProxy.RATING_SUCCESS,
           FeedbackProxy.RATING_FAILURE,
           ZappGadgetProjectProxy.REQUEST_AUTHOR_REPLY,
           ZappGadgetProjectProxy.PROJECT_DELETED_SUCCESS,
           ZappGadgetProjectProxy.PROJECT_DELETED_FAILURE,
           ZappGadgetProjectProxy.PROJECT_UNPUBLISH_SUCCESS,
           ZappGadgetProjectProxy.PROJECT_UNPUBLISH_FAILURE,
           ZappGadgetProjectProxy.PROJECT_UPGRADE_SUCCESS,
           ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS,
           ZappGadgetProjectProxy.PROJECT_UNPURCHASE_SUCCESS,
           ApplicationFacade.UPDATE_PROJECT_ID,
           ApplicationFacade.LOAD_UPGRADE_COMPLETE,
           BrowsePanel.CLEAR_CONTENTS + this.token,
           BrowsePanel.LIST_TYPE_CHANGE + this.token,
           Application.REFRESH
       ];
   }
   
   this.handleNotification = function(notification) 
   {
        var notificationName = notification.getName()
        var payload = notification.getBody();
        var headerStart = 0;
        var headerEnd = 0;
        switch(notificationName) {
        	case BrowsePanel.CLEAR_CONTENTS + this.token:
        		this.removeItems();
        		break;
	        case BrowsePanel.RETURN_BROWSE_PANEL_LIST + this.token:
	        	this.removeItems();
	        	this.browsePage = payload.page;
	        	headerStart = payload.page * this.browseCount + 1;
	        	headerEnd = (payload.page * this.browseCount + 9 > payload.size) ? payload.size : payload.page * this.browseCount + 9;
	        	this.view.updateHeaderFooter(payload.type, headerStart, headerEnd, payload.size);
	        	this.view.populateView(payload);
	        	//this.sendNotification(BrowsePanelMediator.LIST_COMPLETE + this.token);
	        	break;
	        case BrowsePanel.LIST_TYPE_CHANGE + this.token:
	        	if (this.groupTypeSupported(payload.group, payload.type)) {
		        	this.authorMemberId = payload.authorMemberId;
		        	this.browsePage = 0;
		        	if (payload.type != this.type) {
		        		this.browseFilters.category = 'all';
		        		this.browseFilters.search = '';
		        		this.browseFilters.sort = 'createdDateTime';
		        	}
		        	this.setGroupType(payload.group, payload.type);
		        	this.requestList(this.browsePage, payload.authorMemberId, payload.type);
	        	}
	        	break;
	        case BrowsePanel.RETURN_BROWSE_PANEL_LIST_FAILURE + this.token:
	        	this.view.populateViewFailure(payload);
	        	break;
	        case FeedbackProxy.RATING_SUCCESS:
	        	this.requestList(this.browsePage,payload.authorMemberId, this.type);
	        	break;
	        case FeedbackProxy.RATING_FAILURE:
	        	alert ('Failed to register rating');
	        	break;
	        case ZappGadgetProjectProxy.PROJECT_UPGRADE_SUCCESS:
	        case ZappGadgetProjectProxy.PROJECT_UNPURCHASE_SUCCESS:
	        case ZappGadgetProjectProxy.PROJECT_UNPUBLISH_SUCCESS:
	        case ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS:
	        case ZappGadgetProjectProxy.PROJECT_DELETED_SUCCESS:
	        case ApplicationFacade.UPDATE_PROJECT_ID:
	        case Application.REFRESH:
	        	Log.debug('BrowsePanelMediator listening on ZGPP notifications and calling this.requestList()');
	        	this.requestList(this.browsePage, this.authorMemberId, this.type);
	            break;
	        case ZappGadgetProjectProxy.PROJECT_DELETED_FAILURE:
	            alert('failed to delete project');
	            break;
	        case ZappGadgetProjectProxy.PROJECT_UNPUBLISH_FAILURE:
	            alert('failed to unpublish project');
	            break;  
	        case ZappGadgetProjectProxy.PROJECT_UNPURCHASE_FAILURE:
	            alert('failed to remove from collection');
	            break;
	        default:
	            break;
	        
        }
    }
}
BrowsePanelMediator = new Class(new BrowsePanelMediator());
BrowsePanelMediator.NAME = 'BrowsePanelMediator';
BrowsePanelMediator.INITIALIZED = 'BrowsePanelMediator.INITIALIZED';
BrowsePanelMediator.REQUEST_LIST_REPLY = 'BrowsePanelMediator.REQUEST_LIST_REPLY';
BrowsePanelMediator.REQUEST_LIST_FAILURE = 'BrowsePanelMediator.REQUEST_LIST_FAILURE';
BrowsePanelMediator.LIST_COMPLETE = 'BrowsePanelMediator.LIST_COMPLETE';
ScriptLoader.loaded(BrowsePanelMediator.NAME);
