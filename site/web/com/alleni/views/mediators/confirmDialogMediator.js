var ConfirmDialogMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.view = undefined;
    this.token = "";
    this.payload = undefined;
    /**
     * 
     */
    this.initialize = function(mediatorName, viewComponent, token)
    {
        this.parent((mediatorName != undefined)?mediatorName:ConfirmDialogMediator.NAME, viewComponent);
        this.token = token;
        this.view = this.getViewComponent();

        // turn on dialog close capability
        //this.sendNotification(ApplicationFacade.SHOW_CLOSE_DIALOG+this.token, false);
        
        this.onYes = this.onYes.bindWithEvent(this);
        this.view.addEvent(ConfirmDialog.YES, this.onYes);
        
        this.onNo = this.onNo.bindWithEvent(this);
        this.view.addEvent(ConfirmDialog.NO, this.onNo);
    }
    
    this.onYes = function()
    {
    	//alert("YES");
    	this.sendNotification(ConfirmDialogCommand.YES+this.token,this.payload.arguments); // for future
    	this.sendNotification(ConfirmDialogCommand.YES+this.payload.token,this.payload.arguments); // token provided by requesting mediator
    	this.sendNotification(ApplicationFacade.CLOSE_DIALOG+this.token);
    }
    
    this.onNo = function()
    {
    	//alert("NO");
    	this.sendNotification(ConfirmDialogCommand.NO+this.token,this.payload.arguments); // for future
    	this.sendNotification(ConfirmDialogCommand.NO+this.payload.token,this.payload.arguments); // token provided by requesting mediator
    	this.sendNotification(ApplicationFacade.CLOSE_DIALOG+this.token);
    }
    
    this.onOk = function()
    {
    	//alert("NO");
    	this.sendNotification(ConfirmDialogCommand.OK+this.token,this.payload.arguments); // for future
    	this.sendNotification(ConfirmDialogCommand.OK+this.payload.token,this.payload.arguments); // token provided by requesting mediator
    	this.sendNotification(ApplicationFacade.CLOSE_DIALOG+this.token);
    }
    
    this.loadComplete = function()
    {
    	this.sendNotification(ApplicationFacade.CENTER_DIALOG+this.token);
    }
    /**
     * 
     */
    this.listNotificationInterests = function()
    {
        return [
              ApplicationFacade.CONFIG_DIALOG+this.token,
              ApplicationFacade.CLOSING_DIALOG+this.token,
              ApplicationFacade.CLOSING_DIALOG_BY_USER+this.token
        ];
    }
    /*
     * 
     */
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        
        switch(notificationName)
        {
        case ApplicationFacade.CLOSING_DIALOG+this.token:
        	this.onClose();
        	break;
        case ApplicationFacade.CLOSING_DIALOG_BY_USER+this.token:
        	
        	break;
        case ApplicationFacade.CONFIG_DIALOG+this.token:
        	this.payload = notification.getBody();
        	this.view.config(this.payload.message);
        	break;
        default:
            break;
        }
        
    }
    /**
     * 
     */
    this.onClose = function()
    {
    	this.view = undefined;
    	this.facade.removeMediator(this.getMediatorName());
    }
    /**
     * 
     */
    this.onCloseByUser = function()
    {
    	this.onNo();
    	this.view = undefined;
    	this.facade.removeMediator(this.getMediatorName());
    }
    /**
     * 
     */
    this.onRegister = function()
    {
    	
    }
    /**
     * 
     */
    this.onRemove = function()
    {
    	
    }
}
ConfirmDialogMediator = new Class(new ConfirmDialogMediator());
ConfirmDialogMediator.NAME = 'ConfirmDialogMediator';
