var HeaderPanelMediator = function(viewComponent/*HeaderPanel*/)
{
    this.Extends = Mediator;
    this.headerPanel = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(HeaderPanelMediator.NAME, viewComponent);
        this.headerPanel = this.getViewComponent();

        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.headerPanel.addEvent(HeaderPanel.INITIALIZED, this.onInitialized);

        this.onMenuItemSelected = this.onMenuItemSelected.bindWithEvent(this);
        this.headerPanel.addEvent(HeaderPanel.MENU_ITEM_SELECTED, this.onMenuItemSelected);
        
        this.onLinkItemSelected = this.onLinkItemSelected.bindWithEvent(this);
        this.headerPanel.addEvent(HeaderPanel.LINK_ITEM_SELECTED, this.onLinkItemSelected);

        this.onTabItemSelected = this.onTabItemSelected.bindWithEvent(this);
        this.headerPanel.addEvent(HeaderPanel.TAB_ITEM_SELECTED, this.onTabItemSelected);

        this.onLogin = this.onLogin.bindWithEvent(this);
        this.headerPanel.addEvent(HeaderPanel.LOGIN, this.onLogin);

        this.onLogout = this.onLogout.bindWithEvent(this);
        this.headerPanel.addEvent(HeaderPanel.LOGOUT, this.onLogout);
        //Log.debug('HeaderPanelMediator.initialize');
    }
    this.onInitialized = function()
    {
        this.sendNotification(HeaderPanelMediator.INITIALIZED);
    }
    this.onMenuItemSelected = function (menuItem)
    {
        this.sendNotification(HeaderPanelMediator.MENU_ITEM_SELECTED, {menuItem: menuItem});
    }
    
    this.onLinkItemSelected = function (menuItem)
    {
        this.sendNotification(HeaderPanelMediator.MENU_ITEM_SELECTED, {menuItem: menuItem});
    }
    this.onTabItemSelected = function (tabItem)
    {   
    	Log.info("received tab click");
    	if(tabItem.split('?')[0] = 'account')
            this.sendNotification(HeaderPanelMediator.MENU_ITEM_SELECTED, {menuItem: tabItem});
    	else
    		this.sendNotification(HeaderPanelMediator.TAB_ITEM_SELECTED, {tabItem: tabItem});
    }
    this.onLogin = function (o)
    {
        //TODO acavan - send notification and catch in ApplicationMediator
        this.facade.retrieveProxy(UserProxy.NAME).login(o);
    }
    this.onLogout = function ()
    {
        //TODO acavan - send notification and catch in ApplicationMediator
        this.facade.retrieveProxy(UserProxy.NAME).logout();
    }
    this.listNotificationInterests = function()/*Array*/
    {
        return [
            SidebarPanelMediator.DASHBOARD_DISPLAYED,
            UserProxy.LOGIN_SUCCESS,
            UserProxy.LOGOUT_SUCCESS,
            UserProxy.LOGIN_FAILURE,
            UserProxy.REFRESH_SUCCESS,
            Application.HASHCHANGED
        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.getBody();
        switch(notificationName)
        {
        case UserProxy.REFRESH_SUCCESS:
        	this.headerPanel.toggleMasterLogo();
        	break;
        case UserProxy.LOGIN_SUCCESS:
            this.headerPanel.onLoginLogout('login');
            this.headerPanel.highlightTab('myStuff');
            this.headerPanel.onLogin();
            break;
        case UserProxy.LOGOUT_SUCCESS:
        case UserProxy.LOGIN_FAILURE:
            this.headerPanel.onLoginLogout('logout');
            this.headerPanel.onLogout();
            break;
        case SidebarPanelMediator.DASHBOARD_DISPLAYED:
            this.headerPanel.highlightTab(payload);
            break;
        case Application.HASHCHANGED:
        	this.headerPanel.setMenuState(payload);
        	break;
        default:
            break;
        }
    }
}
HeaderPanelMediator = new Class(new HeaderPanelMediator());
HeaderPanelMediator.NAME = 'HeaderPanelMediator';
HeaderPanelMediator.INITIALIZED = 'HeaderPanelMediator.INITIALIZED';
HeaderPanelMediator.MENU_ITEM_SELECTED = 'HeaderPanelMediator.MENU_ITEM_SELECTED';
HeaderPanelMediator.TAB_ITEM_SELECTED = 'HeaderPanelMediator.TAB_ITEM_SELECTED';
