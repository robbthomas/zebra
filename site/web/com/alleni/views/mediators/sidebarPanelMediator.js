var SidebarPanelMediator = function(viewComponent)
{

    this.Extends = Mediator;

    this.sidebarPanel = undefined;
    this.loginSuccessCalledBeforeInitialized = false;
    this.hrefFunctions = {
        signin: function (form, facade) {
        }
    };

    this.initialize = function(viewComponent)
    {
        this.parent(SidebarPanelMediator.NAME, viewComponent);
        this.sidebarPanel = this.getViewComponent();

        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.sidebarPanel.addEvent(SidebarPanel.INITIALIZED, this.onInitialized);

        this.onLostPassword = this.onLostPassword.bindWithEvent(this);
        this.sidebarPanel.addEvent(SidebarPanel.LOST_PASSWORD, this.onLostPassword);
        
        this.checkCredentialsForLaunch = this.checkCredentialsForLaunch.bindWithEvent(this);
        this.sidebarPanel.addEvent(SidebarPanel.LAUNCH_EDITOR, this.checkCredentialsForLaunch);
        
        this.checkCredentialsForLaunchNew = this.checkCredentialsForLaunchNew.bindWithEvent(this);
        this.sidebarPanel.addEvent(SidebarPanel.LAUNCH_EDITOR_NEW, this.checkCredentialsForLaunchNew);
        
        this.onClickHref = this.onClickHref.bindWithEvent(this);
        this.sidebarPanel.addEvent(SidebarPanel.HREF_CLICKED, this.onClickHref);
        
        this.onDisplayChange = this.onDisplayChange.bindWithEvent(this);
        this.sidebarPanel.addEvent(SidebarPanel.DISPLAY_CHANGE, this.onDisplayChange);
    };
    
    this.checkCredentialsForLaunch = function(cfg) {
    	var config = {
    		successNotification: SidebarPanelMediator.LAUNCH_EDITOR,
    		failureNotification: ApplicationFacade.SHOW_ACCOUNT_DUE_DIALOG,
    		failureConfig: {},
    		payload: cfg
    	};
    	
    	this.sendNotification(ApplicationFacade.CHECK_ACCOUNT_STANDING, config);
    };
    
    this.checkCredentialsForLaunchNew = function() {
    	var config = {
    		successNotification: SidebarPanelMediator.LAUNCH_EDITOR_NEW,
    		failureNotification: ApplicationFacade.SHOW_ACCOUNT_DUE_DIALOG,
    		failureConfig: {},
    		payload: {}
    	};
        	
        this.sendNotification(ApplicationFacade.CHECK_ACCOUNT_STANDING, config);
    };
    
    this.launchEditor = function(cfg)
    {
    	this.sendNotification(ApplicationFacade.LAUNCH_EDITOR, cfg);
    };
    
    this.launchEditorNew = function()
    {
    	this.sendNotification(ApplicationFacade.LAUNCH_EDITOR, {});
    };

    this.onLostPassword = function(username)
    {
        this.sendNotification('PopupPanelMediator.LOST_PASSWORD');
    };

    this.onInitialized = function(counts)
    {
		this.sidebarPanel.clearSignIn();
		this.sidebarPanel.onRefresh();
    };
    
    this.onClickHref = function (o)
    {
        if (!Require(o, ['href', 'form', 'parent'], 'SidebarPanelMediator.onClickHref'))
            return false;

        switch (o.href)
        {
        case 'signin': //TODO acavan - move proxy calls to application mediator...
            this.facade.retrieveProxy(UserProxy.NAME).login(o.form);
            break;
        case 'signout':
            this.facade.retrieveProxy(UserProxy.NAME).logout(o.form);
            break;
        //TODO acavan - no Footer fodder here...
        case 'plansAndPricing':
            this.sendNotification(Application.HREF_DISPATCH + 'plansAndPricing');
            break;
        case 'aboutZebra|what':
            this.sendNotification(HeaderPanelMediator.MENU_ITEM_SELECTED, {menuItem: 'aboutZebra'});
            break;
        default:
            //Log.debug('SidebarPanelMediator.onClickHref - href:'+o.href+', parent:'+o.parent);
            this.sendNotification(Application.HREF_DISPATCH+o.href, o.form);
            //this.sendNotification(Application.HREF_DISPATCH+o.parent, o.form);
            break;
        }
        return false;
    };
    
    this.onDisplayChange = function (o) {
    	o.caller = SidebarPanelMediator.NAME + " onDisplayChange()";
    	this.sendNotification(BrowsePanel.LIST_TYPE_CHANGE+"myPanel", o);
    };
    
    this.listNotificationInterests = function()/*Array*/
    {
        return [
            ApplicationPanelMediator.CONTENT_DISPLAYED,
            HeaderPanelMediator.TAB_ITEM_SELECTED,
            HeaderPanelMediator.MENU_ITEM_SELECTED,
            SidebarPanelMediator.LAUNCH_EDITOR,
            SidebarPanelMediator.LAUNCH_EDITOR_NEW,
            SidebarPanelMediator.REFRESH_PENDING_TRANSFERS_SUCCESS,
            SidebarPanelMediator.REFRESH_PENDING_TRANSFERS_FAILURE,
            SidebarPanelMediator.REQUEST_GADGET_LIST_REPLY,
            SidebarPanelMediator.REQUEST_LIST_FAILURE,
            SidebarPanelMediator.REQUEST_PROJECT_LIST_REPLY,
            SidebarPanelMediator.REQUEST_ZAPP_LIST_REPLY,
            UserProxy.LOGIN_BAD_CREDENTIALS,
            UserProxy.LOGIN_FAILURE,
            UserProxy.LOGIN_SUCCESS,
            UserProxy.REFRESH_SUCCESS,
            UserProxy.LOGOUT_SUCCESS,
            ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS,
            ZappGadgetProjectProxy.PROJECT_UNPURCHASE_SUCCESS,
            Application.REFRESH,
            ZappGadgetProjectProxy.PROJECT_DELETED_SUCCESS
        ];
    };
    
    this.handleNotification = function(notification)
    {
    	var that = this;
        var notificationName = notification.getName();
        //Log.debug('SidebarPanelMediator.handleNotification - notificationName:'+notificationName);
        var payload = notification.getBody();
        switch(notificationName)
        {
        case ApplicationPanelMediator.CONTENT_DISPLAYED:
            var dashboard = this.sidebarPanel.displayDashboard(payload);
            if (dashboard != undefined)
                this.sendNotification(SidebarPanelMediator.DASHBOARD_DISPLAYED, dashboard);
            this.sidebarPanel.displayHeadlines(payload);
            break;
        case HeaderPanelMediator.TAB_ITEM_SELECTED:
            this.sidebarPanel.displayDashboard(payload.tabItem);
        	break;
        case HeaderPanelMediator.MENU_ITEM_SELECTED:
            this.sidebarPanel.updateMenuSelection(payload.menuArgument);
            break;
        case SidebarPanelMediator.LAUNCH_EDITOR:
        	this.launchEditor(payload);
        	break;
        case SidebarPanelMediator.LAUNCH_EDITOR_NEW:
        	this.launchEditorNew();
        	break;
        case UserProxy.LOGIN_SUCCESS:
        	this.sidebarPanel.clearSignIn();
            this.sidebarPanel.onLogin();
        case UserProxy.REFRESH_SUCCESS:
        	this.sidebarPanel.onRefresh();
        case ZappGadgetProjectProxy.PROJECT_DELETED_SUCCESS:
        case ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS:
        case ZappGadgetProjectProxy.PROJECT_UNPURCHASE_SUCCESS:
            // BENRES:  This should be a real dialog and redirect user to account page
            if (payload.errors != undefined) {
                alert('ERROR: ' + payload.errors[0].error);
            }
            this.sendNotification(Application.REFRESH); // Do this here to ensure that Application.REFRESH actually gets done, so that other components (say, BrowsePanel) are aware of it having happened.
            break;
        case Application.REFRESH:
            this.requestList({type: 'project', count: 5, notification: SidebarPanelMediator.REQUEST_PROJECT_LIST_REPLY});
            this.requestList({type: 'zapp',    count: 9, notification: SidebarPanelMediator.REQUEST_ZAPP_LIST_REPLY});
            this.requestList({type: 'gadget',  count: 1, notification: SidebarPanelMediator.REQUEST_GADGET_LIST_REPLY});
            
            this.facade.retrieveProxy(AccountProxy.NAME).getPendingIncomingTransferCount();
            break;
        case SidebarPanelMediator.REFRESH_PENDING_TRANSFERS_SUCCESS:
        	if (payload != undefined) {
        		this.sidebarPanel.updatePendingTransfers(payload.count);
        	}
        	setTimeout(function() {
        		that.facade.retrieveProxy(AccountProxy.NAME).getPendingIncomingTransferCount();
        	}, 10000);
            break;
        case SidebarPanelMediator.REFRESH_PENDING_TRANSFERS_FAILURE:
        	alert('Failed to retrieve pending transfers!');
        	break;
        case UserProxy.LOGIN_BAD_CREDENTIALS:
            this.sidebarPanel.onLoginBadCredentials();
            break;
        case UserProxy.LOGOUT_SUCCESS:
            this.sidebarPanel.onLogout();
            this.sidebarPanel.clearSignIn();
            break;
        case UserProxy.LOGIN_FAILURE:
            this.sidebarPanel.onLogout();
            break;
        case SidebarPanelMediator.REQUEST_PROJECT_LIST_REPLY:
            this.sidebarPanel.displayMyStuff('project', payload.items, payload.size);
            break;
        case SidebarPanelMediator.REQUEST_ZAPP_LIST_REPLY:
            this.sidebarPanel.displayMyStuff('zapp', payload.items, payload.size);
            break;
        case SidebarPanelMediator.REQUEST_GADGET_LIST_REPLY:
            this.sidebarPanel.displayMyStuff('gadget', payload.items, payload.size);
            break;
        case SidebarPanelMediator.REQUEST_LIST_FAILURE:
            //Log.debug('SidebarPanelMediator.handleNotification - REQUEST_LIST_FAILURE group:'+payload.group);
            break;
        default:
            break;
        }
    };
    
    this.requestList = function (o) // {count:n, notification:s, type:s}
    {
        //Log.debug('SidebarPanelMediator.requestMemberLists');
        var cfg = {
            count: o.count,
            group: 'member',
            notifications: {
                success: o.notification,
                failure: SidebarPanelMediator.REQUEST_LIST_FAILURE
            },
            page: 0,
            refresh: true,
            type: o.type
        };
        this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).requestList(cfg);
    };
};
SidebarPanelMediator = new Class(new SidebarPanelMediator());
SidebarPanelMediator.NAME = 'SidebarPanelMediator';
SidebarPanelMediator.REFRESH_PENDING_TRANSFERS_SUCCESS = 'SidebarPanelMediator.REFRESH_PENDING_TRANSFERS_SUCCESS';
SidebarPanelMediator.REFRESH_PENDING_TRANSFERS_FAILURE = 'SidebarPanelMediator.REFRESH_PENDING_TRANSFERS_FAILURE';
SidebarPanelMediator.REQUEST_PROJECT_LIST_REPLY = 'SidebarPanelMediator.REQUEST_PROJECT_LIST_REPLY';
SidebarPanelMediator.REQUEST_ZAPP_LIST_REPLY = 'SidebarPanelMediator.REQUEST_ZAPP_LIST_REPLY';
SidebarPanelMediator.REQUEST_GADGET_LIST_REPLY = 'SidebarPanelMediator.REQUEST_GADGET_LIST_REPLY';
SidebarPanelMediator.REQUEST_LIST_FAILURE = 'SidebarPanelMediator.REQUEST_LIST_FAILURE';
SidebarPanelMediator.DASHBOARD_DISPLAYED = 'SidebarPanelMediator.DASHBOARD_DISPLAYED';
SidebarPanelMediator.LAUNCH_EDITOR = 'SidebarPanelMediator.LAUNCH_EDITOR';
SidebarPanelMediator.LAUNCH_EDITOR_NEW = 'SidebarPanelMediator.LAUNCH_EDITOR_NEW';