var UpgradeDialogMediator  = function(viewComponent/*UpgradeDialog*/)
{

    this.Extends = Mediator;
    this.upgradeDialog = undefined;
    this.successCallback = undefined;
    this.initialized = false;
    this.openWhenInitialized = false;
    this.openArgument = undefined;
    this.couponProxy = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(UpgradeDialogMediator.NAME, viewComponent);
        this.upgradeDialog = this.getViewComponent();

        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.upgradeDialog.addEvent(UpgradeDialog.INITIALIZED, this.onInitialized);

        this.onMemberSignIn = this.onMemberSignIn.bindWithEvent(this);
        this.upgradeDialog.addEvent(UpgradeDialog.MEMBER_SIGN_IN, this.onMemberSignIn);

        this.onMemberPurchaseItem = this.onMemberPurchaseItem.bindWithEvent(this);
        this.upgradeDialog.addEvent(UpgradeDialog.MEMBER_PURCHASE_ITEM, this.onMemberPurchaseItem);

        this.onVisitorPurchaseItem = this.onVisitorPurchaseItem.bindWithEvent(this);
        this.upgradeDialog.addEvent(UpgradeDialog.VISITOR_PURCHASE_ITEM, this.onVisitorPurchaseItem);

        this.upgradeDialog.setJoinProxy(this.facade.retrieveProxy(JoinProxy.NAME));

        this.onJoin = this.onJoin.bindWithEvent(this);
        this.upgradeDialog.addEvent(UpgradeDialog.JOIN, this.onJoin);
        
        this.onUpdate = this.onUpdate.bindWithEvent(this);
        this.upgradeDialog.addEvent(UpgradeDialog.UPDATE, this.onUpdate);
        
        this.validateCoupon = this.validateCoupon.bindWithEvent(this);
        this.upgradeDialog.addEvent(UpgradeDialog.VALIDATE_COUPON, this.validateCoupon);
        
        this.upgrade = this.upgrade.bindWithEvent(this);
        this.upgradeDialog.addEvent(UpgradeDialog.UPGRADE, this.upgrade);
        
        this.couponProxy = new CouponProxy("upgradeFlow");
        this.facade.registerProxy(this.couponProxy);

    }
    this.upgrade = function (o)
    {
    	this.facade.retrieveProxy(UserProxy.NAME).upgrade(o);
    }
    this.onSubmit = function (o)
    {
        this.facade.retrieveProxy(JoinProxy.NAME).create(o);
    }
    this.open = function (o)
    {
        if (this.initialized) {
            this.upgradeDialog.open(o);
        } else {
            Log.debug('UpgradeDialogMediator.open - open deferred');    
            this.openWhenInitialized = true;
            this.openArgument = o;
        }
    }
    this.onMemberSignIn = function (o)
    {
        this.facade.retrieveProxy(UserProxy.NAME).login(o);
    }
    this.onMemberPurchaseItem = function (o)
    {
        this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).requestPurchase([o.itemId]);
    }
    this.onVisitorPurchaseItem = function (o)
    {
        if (!Require(o, ['itemId', 'form'], 'UpgradeDialogMediator.onVisitorPurchaseItem'))
            return;
    }
    this.onInitialized = function()
    {
        this.initialized = true;
        if (this.openWhenInitialized) {
            this.open(this.openArgument);
            this.openWhenInitialized = false;
        }

    }

    this.onJoin = function (o)
    {
        this.facade.retrieveProxy(JoinProxy.NAME).create(o);
    }
    this.onUpdate = function (o)
    {
    	this.facade.retrieveProxy(AccountProxy.NAME).update(o.formName,o.values);
    }
    this.validateCoupon = function (o)
    {
    	this.couponProxy.validate(o);
    }
    
    this.listNotificationInterests = function()/*Array*/
    {
        return [
            UserProxy.LOGIN_SUCCESS,
            UserProxy.LOGIN_FAILURE,
            UserProxy.LOGIN_BAD_CREDENTIALS,
            UserProxy.EMAIL_UNIQUE_SUCCESS,
            UserProxy.EMAIL_UNIQUE_FAILURE,
            UserProxy.USERNAME_UNIQUE_SUCCESS,
            UserProxy.USERNAME_UNIQUE_FAILURE,
            CouponProxy.VALIDATE_SUCCESS+"upgradeFlow",
            CouponProxy.VALIDATE_FAILURE+"upgradeFlow",
            UserProxy.UPGRADE_DOWNGRADE_SUCCESS,
            UserProxy.UPGRADE_DOWNGRADE_FAILURE,
            AccountProxy.UPDATE_SUCCESS,
            AccountProxy.UPDATE_FAILURE,
            ZappGadgetProjectProxy.REQUEST_PURCHASE_FAILURE,
            ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS
        ];
     }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName()
        var payload = notification.getBody();
        switch(notificationName)
        {
        case UserProxy.LOGIN_SUCCESS:
        case UserProxy.LOGIN_FAILURE:
            this.upgradeDialog.displayStep(Credentials.authenticated() ? 'stepOneMember' : 'stepOneVisitor');
            break;
        case UserProxy.LOGIN_BAD_CREDENTIALS:
            this.upgradeDialog.onLoginBadCredentials();
            break;
        case UserProxy.EMAIL_UNIQUE_SUCCESS:
            this.upgradeDialog.emailUnique(payload);
            break;
        case UserProxy.EMAIL_UNIQUE_FAILURE:
            break;
        case UserProxy.USERNAME_UNIQUE_SUCCESS:
            this.upgradeDialog.usernameUnique(payload);
            break;
        case UserProxy.USERNAME_UNIQUE_FAILURE:
            break;
        case CouponProxy.VALIDATE_SUCCESS+"upgradeFlow":
        	this.upgradeDialog.onCouponValidateSuccess(payload);
        	break;
        case CouponProxy.VALIDATE_FAILURE+"upgradeFlow":
        	this.upgradeDialog.onCouponValidateFailure(payload);
        	break;
        case ZappGadgetProjectProxy.REQUEST_PURCHASE_FAILURE:
        	 alert('ZappGadgetProjectProxy.requestPurchase - failure\nerror:'+payload);
        	 this.upgradeDialog.close();
        	 break;
        case ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS:
        	this.upgradeDialog.close();
        	break;
        case UserProxy.UPGRADE_DOWNGRADE_SUCCESS:
        	this.upgradeDialog.onUpgradeSuccess(payload);
        	break;
        case UserProxy.UPGRADE_DOWNGRADE_FAILURE:
        	this.upgradeDialog.onUpgradeFailure(payload);
        	break;
        case AccountProxy.UPDATE_SUCCESS:
        	this.upgradeDialog.onUpdateSuccess(payload.data);
        	break;
        case AccountProxy.UPDATE_FAILURE:
        	this.upgradeDialog.onUpdateFailure(payload);
        	break;
        default:
            break;
        }
    }
}
UpgradeDialogMediator = new Class(new UpgradeDialogMediator());
UpgradeDialogMediator.NAME = 'UpgradeDialogMediator';
// ScriptLoader.loaded(UpgradeDialogMediator.NAME);
