var PopupPanelMediator = function(viewComponent/*PopupPanel*/)
{
    this.Extends = Mediator;
    this.popupPanel = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(PopupPanelMediator.NAME, viewComponent);
        this.popupPanel = this.getViewComponent();

        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.popupPanel.addEvent(PopupPanel.INITIALIZED, this.onInitialized);

        this.onRequestNewPassword = this.onRequestNewPassword.bindWithEvent(this);
        this.popupPanel.addEvent(PopupPanel.REQUEST_NEW_PASSWORD, this.onRequestNewPassword);
    }
    this.onInitialized = function()
    {
        this.sendNotification(PopupPanelMediator.INITIALIZED);
    }
    this.listNotificationInterests = function()/*Array*/
    {
        return [
            PopupPanelMediator.LOST_PASSWORD,
            UserProxy.RESET_PASSWORD_SUCCESS,
            UserProxy.RESET_PASSWORD_FAILURE,
            UserProxy.RESET_PASSWORD_USER_NOT_FOUND
        ];
    }

    this.onRequestNewPassword = function(email)
    {
        this.facade.retrieveProxy(UserProxy.NAME).resetPassword(email);
    }

    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.getBody();
        
        switch(notificationName)
        {

        case PopupPanelMediator.LOST_PASSWORD:
            this.popupPanel.lostPassword();
            break;
        case UserProxy.RESET_PASSWORD_SUCCESS:
            this.popupPanel.resetPasswordSuccess(payload);
            break;
        case UserProxy.RESET_PASSWORD_FAILURE:
            alert("An unexpected error occured that prevented your password from being reset: ", payload)
            break;
        case UserProxy.RESET_PASSWORD_USER_NOT_FOUND:
            this.popupPanel.resetPasswordUserNotFound(payload);
            break;
        default:
            break;
        }
    }
}
PopupPanelMediator = new Class(new PopupPanelMediator());
PopupPanelMediator.NAME = 'PopupPanelMediator';
PopupPanelMediator.INITIALIZED = 'PopupPanelMediator.INITIALIZED';
PopupPanelMediator.LOST_PASSWORD = 'PopupPanelMediator.LOST_PASSWORD';
//ScriptLoader.loaded(PopupPanelMediator.NAME);