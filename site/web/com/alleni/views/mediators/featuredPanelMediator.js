var FeaturedPanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.view = undefined;
    this.token = "";
    this.zappGadgetProjectProxy = undefined;
    
    this.featuredCount = 3;
    this.featuredPage = 0;
    this.group = 'visitor';
    this.type = 'zapp';

    this.initialize = function(mediatorName,viewComponent,token) {
    	Log.debug('FeaturedPanelMediator.initialize() -- name: ' + mediatorName + ', token: ' + token);
    	this.token = token;
        
    	this.parent((mediatorName != undefined)?mediatorName:FeaturedPanelMediator.NAME, viewComponent);
    	
        this.view = this.getViewComponent();
        
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.INITIALIZED, this.onInitialized);
        
        this.zappGadgetProjectProxy = new ZappGadgetProjectProxy('zappGadgetProjectProxy_' + mediatorName, token);
        Log.debug('end FeaturedPanelMediator.initialize()');
    };    
    
    this.onInitialized = function() {
    	this.onFeaturedNext = this.onFeaturedNext.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.NEXT, this.onFeaturedNext);
        
        this.onFeaturedPrevious = this.onFeaturedPrevious.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.PREVIOUS, this.onFeaturedPrevious);
        
        this.onBuyClicked = this.onBuyClicked.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.BUY_CLICKED, this.onBuyClicked);
        
    	this.onUpdateZappGadget = this.onUpdateZappGadget.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.UPDATE_ZAPPGADGET, this.onUpdateZappGadget);
        
        this.onToggleFeatured = this.onToggleFeatured.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.TOGGLE_FEATURED, this.onToggleFeatured);

        this.launchEditor = this.launchEditor.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.LAUNCH_EDITOR,this.launchEditor);

        this.onDeleteProject = this.onDeleteProject.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.DELETE_PROJECT, this.onDeleteProject);

        this.onUnpublishProject = this.onUnpublishProject.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.UNPUBLISH_PROJECT, this.onUnpublishProject);
        
        this.onUnpurchaseProject = this.onUnpurchaseProject.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.UNPURCHASE_PROJECT, this.onUnpurchaseProject);

        this.onClickHref = this.onClickHref.bindWithEvent(this);
        this.view.addEvent(FeaturedPanel.HREF_CLICKED, this.onClickHref);
        
        this.sendNotification(FeaturedPanelMediator.INITIALIZED + this.token, {html:this.getViewHtml()});
    }

    this.setGroupType = function(group, type) {
    	this.group = group;
    	this.type = type;
    }
    
    this.setProjectType = function(projectType) {
    	this.type = projectType;
    };
    
    this.onDisplay = function (o)
    {
    	this.setPage(((o.featuredStart>0)?o.featuredStart-1:0)/this.featuredCount);
        this.requestList(this.featuredPage);
    }
    
    this.getViewHtml = function() {
    	return this.view.getHtml();
    };
    
    this.onClickHref = function (o)
    {
        switch (o.href) {
        	case 'authorProfile':
        		this.sendNotification(Application.HREF_DISPATCH + o.href, o);
        		break;
            default:
            	this.sendNotification(Application.HREF_DISPATCH + o.href);
            	break;
        }
        return false;
    }
    
    this.onFeaturedPrevious = function () 
    {
        if (this.featuredPage > 0) {
        	this.requestList(this.featuredPage-1, this.type);
        }
    }
    
    this.onFeaturedNext = function ()
    {
        this.requestList(this.featuredPage+1, this.type);
    }
    
    this.onBuyClicked = function (o)
    {
    	this.sendNotification(ApplicationFacade.REQUEST_PURCHASE, o);
    }
    
    this.onUpdateZappGadget = function (o) 
    {
       	this.zappGadgetProjectProxy.acceptZappUpgrade(o.urlName);
    }
    
    this.onToggleFeatured = function(o) 
    {
    	this.zappGadgetProjectProxy.toggleFeatured(o.id);
    }
    
    this.launchEditor = function (cfg) 
    {
    	this.sendNotification(ApplicationFacade.LAUNCH_EDITOR, cfg);
    }
    
    this.onDeleteProject = function(projectId) 
    {
    	this.zappGadgetProjectProxy.deleteProject(projectId);
    	this.sendNotification(Application.REFRESH);
    }
    
    this.onUnpublishProject = function(projectId) 
    {
        this.zappGadgetProjectProxy.unpublishProject(projectId);
    }
    
    this.onUnpurchaseProject = function(purchaseId) 
    {
        this.zappGadgetProjectProxy.unpurchaseProject(purchaseId);
    }
    
    this.setType = function(type) {
    	this.type = type;
    	this.view.setType(type);
    }
    
    this.setPage = function(page) {
    	this.featuredPage = page;
    }
    
    this.requestList = function (page, projectType)
    {
    	var typeToSend = 'zapp';
    	
    	if (undefined != projectType) {
    		typeToSend = projectType;
    	}
    	
        var o = {
        	count: this.featuredCount,
            group: this.group,
            filters: {
            	category: 'all',
    	        search: '',
    	        sort: 'createdDateTime',
    	        featured: true
            },
            notifications: {
                success: FeaturedPanel.RETURN_FEATURED_PANEL_LIST + this.token,
                failure: FeaturedPanel.RETURN_FEATURED_PANEL_LIST_FAILURE + this.token
            },
            page: page,
            refresh: true,
            type: typeToSend
        }
        
        this.zappGadgetProjectProxy.requestList(o);
    }
    
   this.listNotificationInterests = function() 
   {
       return [
           Application.REFRESH,
           FeaturedPanel.RETURN_FEATURED_PANEL_LIST + this.token,
           FeaturedPanel.RETURN_FEATURED_PANEL_LIST_FAILURE + this.token,
           FeedbackProxy.RATING_SUCCESS,
           FeedbackProxy.RATING_FAILURE,
           ZappGadgetProjectProxy.REQUEST_AUTHOR_REPLY,
           ZappGadgetProjectProxy.PROJECT_DELETED_SUCCESS,
           ZappGadgetProjectProxy.PROJECT_DELETED_FAILURE,
           ZappGadgetProjectProxy.PROJECT_UNPUBLISH_SUCCESS,
           ZappGadgetProjectProxy.PROJECT_UNPUBLISH_FAILURE,
           ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS,
           ZappGadgetProjectProxy.PROJECT_UNPURCHASE_SUCCESS,
           ApplicationFacade.UPDATE_PROJECT_ID,
           ApplicationFacade.LOAD_UPGRADE_COMPLETE,
           BrowsePanel.LIST_TYPE_CHANGE,
           BrowsePanel.LIST_TYPE_CHANGE + this.token
       ];
   }
   
   this.handleNotification = function(notification) 
   {
        var notificationName = notification.getName()
        var payload = notification.getBody();
        
        switch(notificationName) {
	        case BrowsePanel.LIST_TYPE_CHANGE:
	        case BrowsePanel.LIST_TYPE_CHANGE + this.token:
	    	    this.setPage(0);
	        	this.setType(payload.type);
	        	Log.warn("FeaturedPanel::LIST_TYPE_CHANGE "+payload.caller);
	        case FeedbackProxy.RATING_SUCCESS:
	        case ZappGadgetProjectProxy.PROJECT_UNPUBLISH_SUCCESS:
	        case ZappGadgetProjectProxy.REQUEST_PURCHASE_SUCCESS:
	        case ZappGadgetProjectProxy.PROJECT_UNPURCHASE_SUCCESS:
	        case ZappGadgetProjectProxy.PROJECT_DELETED_SUCCESS:
	        case ApplicationFacade.UPDATE_PROJECT_ID:
        	case Application.REFRESH:
        		this.requestList(this.featuredPage, this.type);
        		break;
        	case FeaturedPanel.RETURN_FEATURED_PANEL_LIST + this.token:
        		this.setPage(payload.page);
	        	this.view.populateView(payload.group, payload.type, payload.page, payload.items, payload.size, payload.count);
        		break;
	        case FeaturedPanel.RETURN_FEATURED_PANEL_LIST_FAILURE + this.token:
	        	this.view.populateViewFailure(payload);
	        	break;
  	        case FeedbackProxy.RATING_FAILURE:
 	        	alert ('Failed to register rating');
 	        	break;
	        case ZappGadgetProjectProxy.PROJECT_DELETED_FAILURE:
	            alert('failed to delete project');
	            break;
	        case ZappGadgetProjectProxy.PROJECT_UNPUBLISH_FAILURE:
	            alert('failed to unpublish project');
	            break;
	        case ZappGadgetProjectProxy.PROJECT_UNPURCHASE_FAILURE:
	            alert('failed to remove from collection');
	            break;
	        default:
	            break;
        }
    }
   
   
}
FeaturedPanelMediator = new Class(new FeaturedPanelMediator());
FeaturedPanelMediator.NAME = 'FeaturedPanelMediator';
FeaturedPanelMediator.INITIALIZED = 'FeaturedPanelMediator.INITIALIZED';
ScriptLoader.loaded(FeaturedPanelMediator.NAME);
