var AboutUsPanelMediator = function(viewComponent/*AboutUsPanel*/)
{
    this.Extends = Mediator;
    this.aboutUsPanel = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(AboutUsPanelMediator.NAME, viewComponent);
        this.aboutUsPanel = this.getViewComponent();

        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.aboutUsPanel.addEvent(AboutUsPanel.INITIALIZED, this.onInitialized);
    }
    this.onInitialized = function()
    {
    }
    this.listNotificationInterests = function()/*Array*/
    {
        return [
        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName()
        switch(notificationName)
        {
            default:
                break;
        }
    }
}
AboutUsPanelMediator = new Class(new AboutUsPanelMediator());
AboutUsPanelMediator.NAME = 'AboutUsPanelMediator';
ScriptLoader.loaded(AboutUsPanelMediator.NAME);
