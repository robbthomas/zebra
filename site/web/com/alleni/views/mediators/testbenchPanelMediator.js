var TestbenchPanelMediator = function(viewComponent/*TestbenchPanel*/)
{
    this.Extends = Mediator;
    this.testbenchPanel = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(TestbenchPanelMediator.NAME, viewComponent);
        this.testbenchPanel = this.getViewComponent();
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.testbenchPanel.addEvent(TestbenchPanel.INITIALIZED, this.onInitialized);
    }
    this.onTestSelected = function (test)
    {
        this.testbenchPanel.testForm.displayForm(this.facade.retrieveProxy(TestbenchProxy.NAME).test(test));
    }
    this.onSubmit = function (o)
    {
        this.testbenchPanel.jsonPanel.clear();
        this.facade.retrieveProxy(TestbenchProxy.NAME).submit(o.test, o.form);
        return false;
    }
    this.onInitialized = function ()
    {
        this.onTestSelected = this.onTestSelected.bindWithEvent(this);
        this.testbenchPanel.testsPanel.addEvent(TestbenchTestsPanel.TEST_SELECTED, this.onTestSelected);
        this.onSubmit = this.onSubmit.bindWithEvent(this);
        this.testbenchPanel.testForm.addEvent(TestbenchTestForm.SUBMIT, this.onSubmit);
        this.facade.retrieveProxy(TestbenchProxy.NAME).requestTestList();
    }
    this.listNotificationInterests = function()/*Array*/
    {
       //Log.debug('TestbenchPanelMediator.listNotificationInterests');
        return [
            TestbenchProxy.SUBMIT_URL,
            TestbenchProxy.SUBMIT_OUT,
            TestbenchProxy.SUBMIT_IN,
            TestbenchProxy.SUBMIT_RAW,
            TestbenchProxy.REQUEST_TEST_LIST_REPLY
        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.body;
        switch(notificationName)
        {
            case TestbenchProxy.SUBMIT_URL:
                this.testbenchPanel.jsonPanel.display(payload, 'url');
                break;
            case TestbenchProxy.SUBMIT_OUT:
                this.testbenchPanel.jsonPanel.display(payload, 'out');
                break;
            case TestbenchProxy.SUBMIT_IN:
                this.testbenchPanel.jsonPanel.display(payload, 'in');
                break;
            case TestbenchProxy.SUBMIT_RAW:
                this.testbenchPanel.jsonPanel.display(payload, 'raw');
                break;
            case TestbenchProxy.REQUEST_TEST_LIST_REPLY:
               //Log.debug('TestbenchPanelMediator.REQUEST_TEST_LIST_REPLY');
                this.testbenchPanel.testsPanel.loadTests(payload);
                break;
            default:
                break;
        }
    }
}
TestbenchPanelMediator.tests = null;
TestbenchPanelMediator = new Class(new TestbenchPanelMediator());
TestbenchPanelMediator.NAME = 'TestbenchPanelMediator';
TestbenchPanelMediator.TEST_SELECTED = 'TestbenchPanelMediator.TEST_SELECTED';
ScriptLoader.loaded(TestbenchPanelMediator.NAME);