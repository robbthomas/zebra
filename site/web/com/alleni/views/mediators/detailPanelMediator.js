var DetailPanelMediator = function(viewComponent/*DetailPanel*/)
{
    this.Extends = Mediator;

    this.buyDialog = undefined;
    this.detailPanel = undefined;
    this.detail = undefined;
    
    // USE FOR COMMAND PAYLOAD - SEE ROBB
	//
    this.PANEL_SECURITY = 0;
    this.PANEL_SHARE = 1;
    //
    this.PRIV_SERVER = 0;
    this.PRIV_GUEST = 1;
    this.PRIV_HIDDEN = 2;
    this.PRIV_SHOP = 3;
    //
	this.SHARE_DEFAULT = 0;
    this.SHARE_GUEST = 1;
    this.SHARE_EMBED = 2;
    this.SHARE_LMS = 3;
    this.SHARE_LINK = 4;
    this.SHARE_EMAIL = 5;
    this.SHARE_FACEBOOK = 6;
    this.SHARE_LINKEDIN = 7;
    this.SHARE_TWITTER = 8;

	
    
    this.initialize = function(viewComponent)
    {
        //this.parent(BuyDialogMediator.NAME, viewComponent);
        //this.buyDialog = this.getViewComponent();

        this.parent(DetailPanelMediator.NAME, viewComponent);
        this.detailPanel = this.getViewComponent();
        
        this.detailPanel.joinProxy = this.facade.retrieveProxy(JoinProxy.NAME);

        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.detailPanel.addEvent(DetailPanel.INITIALIZED, this.onInitialized);

        this.onDisplay = this.onDisplay.bindWithEvent(this);
        this.detailPanel.addEvent(DetailPanel.DISPLAY, this.onDisplay);
        
        this.onBuyClicked = this.onBuyClicked.bindWithEvent(this);
        this.detailPanel.addEvent(DetailPanel.BUY_CLICKED, this.onBuyClicked);

        this.onFlagClicked = this.onFlagClicked.bindWithEvent(this);
        this.detailPanel.addEvent(DetailPanel.FLAG_CLICKED, this.onFlagClicked);
        
        this.onShareClicked = this.onShareClicked.bindWithEvent(this);
        this.detailPanel.addEvent(DetailPanel.SHARE_CLICKED, this.onShareClicked);

        this.onClickHref = this.onClickHref.bindWithEvent(this);
        this.detailPanel.addEvent(DetailPanel.HREF_CLICKED, this.onClickHref);
		
        this.onRating = this.onRating.bindWithEvent(this);
        this.detailPanel.addEvent(DetailPanel.RATING, this.onRating);

        this.onRequestCommentsRatings = this.onRequestCommentsRatings.bindWithEvent(this);
        this.detailPanel.addEvent(DetailPanel.REQUEST_COMMENTS_RATINGS, this.onRequestCommentsRatings);
        
        this.onEnlargeImage = this.onEnlargeImage.bindWithEvent(this);
        this.detailPanel.addEvent(DetailPanel.ENLARGE_IMAGE, this.onEnlargeImage);
        
        this.onUpdate = this.onUpdate.bindWithEvent(this);
        this.detailPanel.addEvent(DetailPanel.UPDATE_ZAPPGADGET, this.onUpdate);
    }
    
    
    

    this.onUpdate = function(o) {
    	this.sendNotification(Application.REFRESH);
    }
    
    this.onEnlargeImage = function (o)
    {
    	var config = new LoadDialogueViewConfig(
    			this.detail.publishedName+" Screenshot",
    			"enlargeImagePanel.js",
    			"enlargeImagePanelMediator.js",
    			"enlargeImagePanel.html",
    			o);
    	this.sendNotification(ApplicationFacade.SHOW_DIALOG, config);
    }

    this.onShareClicked = function (o)
    {
    	var payload = {};
    	var hashTag = o.arguments;
    	var privacyPanel = false;
    	switch(hashTag){
	    	case 'link':
	    		payload.shareDefault = this.SHARE_LINK;
	    		break;
	    	case 'embed':
	    		payload.shareDefault = this.SHARE_EMBED;
	    		break;
	    	case 'email':
	    		payload.shareDefault = this.SHARE_EMAIL;
	    		break;
	    	case 'lms':
	    		payload.shareDefault = this.SHARE_LMS;
	    		break;
	    	case 'facebook':
	    		payload.shareDefault = this.SHARE_FACEBOOK;
	    		break;
	    	case 'twitter':
	    		payload.shareDefault = this.SHARE_TWITTER;
	    		break;
	    	case 'linkedin':
	    		payload.shareDefault = this.SHARE_LINKEDIN;
	    		break;
	    	case 'guest':
	    		payload.shareDefault = this.SHARE_GUEST;
	    		break;
	    	case 'privacy':
	    		privacyPanel = true;
	    		break;
    	}
    	
    	payload.panel = (privacyPanel)?0:1;
	    var project = o.project;
    	payload.publishId = project.id;
	    var level = 0;
	    if(project.inviteOnly) {
			level = 1;
	    } else if(project.hideInStoreList) {
		    level = 2;
	    } else {
		    level = 3;
	    }
    	payload.privacyLevel = level;
    	payload.project = project;
    	
    	var config = new LoadDialogueViewConfig(
    			"Share Title Temp",
    			"shareDialog.js",
    			"shareDialogMediator.js",
    			"shareDialog.html",
    			payload
    			);
    	
       this.facade.sendNotification(ApplicationFacade.SHOW_DIALOG, config );
    }	
	
    this.onBuyClicked = function (o)
    {
    	this.sendNotification(ApplicationFacade.REQUEST_PURCHASE, o);
    }
    
    this.onFlagClicked = function (o) {
    	// validate() does not work with textareas
    	if(jQuery.trim(jQuery('#flagComment').val()) == '') {
            alert('To flag an app, you must include a reason.');
            return false;
        }
    	this.facade.retrieveProxy(ProjectFeedbackProxy.NAME).save(o.projectId,'flag',o.flagReason);
    }
    
    this.onInitialized = function()
    {

    }
    this.onRating = function(o)
    {
        this.facade.retrieveProxy(FeedbackProxy.NAME).rating(o.myRating, o.projectId);
    }

    this.onClickHref = function (o)
    {
        switch (o.href) {
        	case 'authorProfile':
        		this.sendNotification(Application.HREF_DISPATCH + o.href, o);
        		break;
            case 'plansAndPricing':
                this.sendNotification(Application.HREF_DISPATCH + o.href);
                break;
            case 'signIn':
                this.facade.retrieveProxy(UserProxy.NAME).login(o.form);
                break;
            case 'postComment':
                this.facade.retrieveProxy(FeedbackProxy.NAME).createComment({comment:o.form.comment, projectId:o.projectId, urlName:o.urlName, projectType:o.projectType});
//                alert("posting comment: " + o.form.comment);
                break;
            case 'deleteComment':
            	this.facade.retrieveProxy(FeedbackProxy.NAME).deleteComment(o.commentId);
            	break;
            case 'helpful':
            case 'notHelpful':
            case 'flag':
                this.facade.retrieveProxy(FeedbackProxy.NAME).commentFeedback(o.href, o.commentId);
                break;
            case 'updateApp':
                this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).acceptZappUpgrade(o.urlName);
                break;
            default:
            	this.sendNotification(Application.HREF_DISPATCH + o.href);
            	break;

        }
        return false;
    }
    this.onDisplay = function (menuArgument)
    {
    	// close any existing dialogs if they are already open
    	this.facade.sendNotification(ApplicationFacade.CLOSE_ALL_DIALOGS);
    	
    	var args = IdType.menuArgsToIdAndType(menuArgument);
    	this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).requestDetail(args.id, args.type);
        //Log.debug('DetailPanelMediator.onDisplay - id:'+id);
    }
    this.onRequestCommentsRatings = function(o)
    {
        this.facade.retrieveProxy(FeedbackProxy.NAME).requestCommentsRatings( o.projectId, 
        	o.offset, 
        	o.count,
        	false);
    }
    
    this.onZappGadgetUpdateSuccess = function(payload) {
    	this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).requestDetail(payload.playerId, IdType.APN_UUID);
    	this.sendNotification(Application.REFRESH);
    }
    this.listNotificationInterests = function()/*Array*/
    {
        return [
            DetailPanel.HREF_CLICKED,
            ZappGadgetProjectProxy.REQUEST_DETAIL_REPLY,
	        ZappGadgetProjectProxy.REQUEST_DETAIL_FAILURE,
            UserProxy.LOGIN_FAILURE,
            UserProxy.LOGIN_SUCCESS,
            UserProxy.LOGIN_BAD_CREDENTIALS,
            UserProxy.LOGOUT_SUCCESS,
            FeedbackProxy.COMMENT_CREATED_SUCCESS,
            FeedbackProxy.COMMENT_CREATED_FAILURE,
            FeedbackProxy.COMMENT_DELETE_SUCCESS,
            FeedbackProxy.COMMENT_DELETE_FAILURE,
            FeedbackProxy.RATING_SUCCESS,
            FeedbackProxy.RATING_FAILURE,
            FeedbackProxy.GOT_COMMENTS_RATINGS_SUCCESS,
            FeedbackProxy.GOT_COMMENTS_RATINGS_FAILURE,
            ProjectFeedbackProxy.SAVE_COMPLETE,
            ProjectFeedbackProxy.SAVE_FAILURE,
            ZappGadgetProjectProxy.UPDATE_OWNED_LIST,
            FeedbackProxy.COMMENT_FEEDBACK_SUCCESS,
            FeedbackProxy.COMMENT_FEEDBACK_FAILURE,
            ApplicationFacade.UPDATE_PROJECT_ID,
            ShareDialogMediator.LOAD_DETAIL,
            ZappGadgetProjectProxy.ZAPPGADGET_UPDATE_SUCCESS
        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName()
        var payload = notification.getBody();
        switch(notificationName)
        {
        case ZappGadgetProjectProxy.REQUEST_DETAIL_REPLY:
        	this.detail = payload.detail;
            this.detailPanel.displayDetail(payload.detail);
            
            this.onRequestCommentsRatings({
            	projectId: payload.detail.id,
            	offset: this.detailPanel.commentOffset,
            	count: this.detailPanel.commentPageSize});
            break;
	    case ZappGadgetProjectProxy.REQUEST_DETAIL_FAILURE:
	        this.detail = undefined;
	        this.detailPanel.displayError();
	        break;
        case UserProxy.LOGIN_SUCCESS:
            this.detailPanel.onLogin();
            if(this.detail != undefined){
            	this.detailPanel.displayDetail(this.detail);
            }
            break;
        case UserProxy.LOGOUT_SUCCESS:
            this.detailPanel.onLogout();
            if(this.detail != undefined){
            	this.detailPanel.displayDetail(this.detail);
            }
            break;
        case UserProxy.LOGIN_BAD_CREDENTIALS:
            this.detailPanel.onLoginBadCredentials();
            break;

        case FeedbackProxy.COMMENT_CREATED_SUCCESS:
            this.onRequestCommentsRatings({
            	projectId: payload._projectId, offset:0, count:10
            	});
            break;
        case FeedbackProxy.COMMENT_CREATED_FAILURE:
        	alert("failed to create comment.");
        	break;
        case FeedbackProxy.COMMENT_DELETE_SUCCESS:
        	this.detailPanel.removeComment(payload.zappCommentId);
        	break;
        case FeedbackProxy.COMMENT_DELETE_FAILURE:
            alert("failed to delete comment.");
            break;
        case FeedbackProxy.GOT_COMMENTS_RATINGS_SUCCESS:
            this.detailPanel.gotCommentsRatings(payload);
            break;
        case FeedbackProxy.GOT_COMMENTS_RATINGS_FAILURE:
            alert("failed to fetch comments/ratings");
            break;
        case FeedbackProxy.RATING_SUCCESS:
            this.detailPanel.ratingSuccess(payload);
            break;
        case FeedbackProxy.RATING_FAILURE:
            alert("Failed to register ratings");
            break;
        case ProjectFeedbackProxy.SAVE_COMPLETE:
        	this.detailPanel.projectFeedbackSuccess(payload);
        	break;
        case ProjectFeedbackProxy.SAVE_FAILURE:
        	alert('Failed to leave project feedback');
        	break;
        case ZappGadgetProjectProxy.UPDATE_OWNED_LIST:
            this.detailPanel.updateOwnedList();
            break;
        case FeedbackProxy.COMMENT_FEEDBACK_SUCCESS:
            this.detailPanel.commentFeedbackSuccess(payload);
            break;
        case FeedbackProxy.COMMENT_FEEDBACK_FAILURE:
            alert("Failed to leave comment feedback");
            break;
        case ApplicationFacade.UPDATE_PROJECT_ID:
        	Common.loadURL("/#/detail?apn&"+notification.body.newValue);
        	this.sendNotification(Application.REFRESH);
        	break;
        case ShareDialogMediator.LOAD_DETAIL:
        	this.onDisplay(payload);
        	break;
        case ZappGadgetProjectProxy.ZAPPGADGET_UPDATE_SUCCESS:
        	this.onZappGadgetUpdateSuccess(payload);
        default:
            break;
        }
    }
}
DetailPanelMediator = new Class(new DetailPanelMediator());
DetailPanelMediator.NAME = 'DetailPanelMediator';
ScriptLoader.loaded(DetailPanelMediator.NAME);
