var ApplicationPanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.applicationPanel = undefined;
    this.abortRedirectToProjectList = false;
    /**
     * 
     */        
    this.initialize = function(viewComponent)
    {
        this.parent(ApplicationPanelMediator.NAME, viewComponent);
        this.applicationPanel = this.getViewComponent();

        this.onInitializationComplete = this.onInitializationComplete.bindWithEvent(this);
        this.applicationPanel.addEvent(ApplicationPanel.INITIALIZATION_COMPLETE, this.onInitializationComplete);
        
        var that = this;
        jQuery.History.bind(function (hashtag) {
            that.onHashtagChange(hashtag);
        });
    }
    /**
     * 
     */
    this.onInitializationComplete = function ()
    {
    	this.facade.registerMediator(new HeaderPanelMediator(this.applicationPanel.headerPanel));
    	this.facade.registerMediator(new PopupPanelMediator(this.applicationPanel.popupPanel));
    	this.facade.registerMediator(new FooterPanelMediator(this.applicationPanel.footerPanel));
    	this.facade.registerMediator(new ContentPanelMediator(this.applicationPanel.contentPanel));
    	this.facade.registerMediator(new SidebarPanelMediator(this.applicationPanel.sidebarPanel));
    	
    	var username = Credentials.username();
        if (username.length > 0 && username != 'guest')
        {
            var o = {
                email: username,
                password: Credentials.password(),
                keepSignedIn: Credentials.keepSignedIn(),
                failSilently: true
            }
            this.facade.retrieveProxy(UserProxy.NAME).login(o);
        }

        jQuery.History.go(location.hash);
    }
    /**
     * 
     */
    this.listNotificationInterests = function()
    {
        return [
            Application.ACCOUNT_CREATE_SUCCESS,
            Application.DISPLAY_ITEM_DETAIL,
            Application.HREF_DISPATCH+'join',
            Application.HREF_DISPATCH+'plansAndPricing',
            Application.HREF_DISPATCH+'account?incomingTransfers',
            Application.HREF_DISPATCH+'confirm',
            Application.HREF_DISPATCH+'skip',
            Application.HREF_DISPATCH+'authorProfile',
            Application.HREF_DISPATCH+'list?visitor&zapp',
            Application.PLAN_SELECTED,
            ContentPanelMediator.INITIALIZED,
            FooterPanelMediator.LINK_ITEM_SELECTED,
            HeaderPanelMediator.MENU_ITEM_SELECTED,
            UserProxy.LOGIN_SUCCESS,
            UserProxy.LOGOUT_SUCCESS,
            UserProxy.UPGRADE_DOWNGRADE_SUCCESS,
            ApplicationFacade.UPDATE_CREDENTIALS,
            AccountProxy.UPDATE_SUCCESS
        ];
    }
    /**
     * 
     */
    this.handleNotification = function(notification/*Notification*/)
    {
        var payload = notification.getBody()
        switch(notification.getName())
        {
        case Application.ACCOUNT_CREATE_SUCCESS:
        	this.abortRedirectToProjectList = true;
            this.facade.retrieveProxy(UserProxy.NAME).login(payload);
            break;
        case Application.DISPLAY_ITEM_DETAIL:
            this.onMenuItemSelected('detail');
            break;
        case Application.BUY_ZAPP:
            BuyZapp.display(payload.id);
            break;
        case Application.HREF_DISPATCH+'join':
        case Application.HREF_DISPATCH+'plansAndPricing':
            this.onMenuItemSelected('plansAndPricing');
            break;
        case Application.HREF_DISPATCH+'account?incomingTransfers':
        	//alert('Proof that the event is firing twice...');
            this.onMenuItemSelected('account?incomingTransfers');
            break;
        case Application.HREF_DISPATCH+'confirm':
        	this.onMenuItemSelected('confirm');
        	break;
        case Application.HREF_DISPATCH+'skip':
        	this.onMenuItemSelected('skip');
        	break;
        case Application.HREF_DISPATCH+'authorProfile':
        	this.onMenuItemSelected('authorProfile', 'id=' + payload.id);
        	break;
        case Application.HREF_DISPATCH+'list?visitor&zapp':
        	this.onMenuItemSelected('list?visitor&zapp');
        	break;
        case Application.PLAN_SELECTED:
            this.onMenuItemSelected('join');
            break;
        case AccountProxy.UPDATE_SUCCESS:
        case UserProxy.UPGRADE_DOWNGRADE_SUCCESS:
        case ApplicationFacade.UPDATE_CREDENTIALS:
        	 var email = Credentials.email();
             if (email.length > 0 && email != 'guest@zebrazapps.com')
             {
                 var o = {
                     email: email,
                     password: Credentials.password(),
                     keepSignedIn: Credentials.keepSignedIn(),
                     failSilently: true
                 }
                 this.facade.retrieveProxy(UserProxy.NAME).refresh(o);
             }
        	break;
        case HeaderPanelMediator.MENU_ITEM_SELECTED:
            this.onMenuItemSelected(payload.menuItem, payload.menuArgument);
            break;
        case UserProxy.LOGIN_SUCCESS:
        		jQuery.History.go(location.hash);
        		if(!Credentials.viewedWelcomeMessage()){
        			Credentials.viewedWelcomeMessage(true);
        			var payload = {};
                	var config = new LoadDialogueViewConfig(
                			"Welcome to ZebraZapps Professional!",
                			"welcomeDialog.js",
                			"welcomeDialogMediator.js",
                			"welcomeDialog.html",
                			payload
                			);
                	
                
                   this.facade.sendNotification(ApplicationFacade.SHOW_DIALOG, config );
        		}
            break;
        case UserProxy.LOGIN_FAILURE:
            alert('Sign in - authentication failed');
            // fall through
            break;
        case UserProxy.LOGOUT_SUCCESS:
            this.onMenuItemSelected('list?visitor&zapp');
            break;
        default:
            break;
        }
    }
    /**
     * 
     */
    this.onMenuItemSelected = function (menuItem, menuArgument)
    {
    	if(menuItem == undefined){
    		jQuery.History.go('list?visitor&zapp');
    	}else{
    		var go = '/'+menuItem+(menuArgument !== undefined && menuArgument.length > 0 ? '?'+menuArgument : '');
            jQuery.History.go(go);
    	}
    }
    /**
     * 
     */
    this.onHashtagChange = function (hashtag)
    {
    	var a = hashtag.split('?');
        var menuItem = a[0].substring(1);
        var menuArgument = a.length > 1 ? a[1] : '';
        var homeHash = '#list?visitor&zapp';
        var homeMemberHash = '#list?member&zapp';
        var loggedIn = Credentials.authenticated();
        var admin = Credentials.systemAdministrator();
        var filetypes = /\.(zip|exe|pdf|doc*|xls*|ppt*|mp3)$/i;

        Log.info('ApplicationPanelMediator.onHashtagChange - hashtag:'+hashtag+', menuItem:'+menuItem+', menuArgument:'+menuArgument);

        jQuery('body, div#mainPanel').css('backgroundColor', menuItem == '#BFDFFF'); //TODO acavan - move to panel

        window.scrollTo(0,0);

        var isLegal = false;
        var contentPanel = Application.contentPanels[menuItem];

        if(!loggedIn && (
        				menuArgument.indexOf("member") != -1 || 
        				contentPanel == Application.contentPanels["account"]
        		)
        	)
    	{
        	contentPanel = undefined;
    	}
        if(loggedIn && (
        			//contentPanel == Application.contentPanels["join"] || 
        			// we are showing info after join, so need to leave joinPanel up
        			contentPanel == Application.contentPanels["plansAndPricing"]
        		)
        	)
    	{
        	contentPanel = undefined;
    	}
      
        // same deal here with sys admins and testbench
        if(!admin && menuArgument.indexOf("testbench") != -1)
        {
        	contentPanel = undefined;
        }
        
        if (contentPanel == undefined)
        {
            switch (menuItem)
            {
            case 'legal':
                Overlay.display(menuArgument+'.html');
                isLegal = true;
                break;
            case 'video':
                var a = menuArgument.split('&');
                if (a.length != 2) // menuArgument should be 'videoId&title'
                    return
                window.open('/video.jsp?videoId='+a[0]+'&title='+escape(a[1]), 'newWindow', 'width=1300, height=830, toolbar=no');
                break;
            default:
            	a = homeHash.split('?');
            	menuItem = a[0].substring(1);
            	menuArgument = a.length > 1 ? a[1] : '';
            	this.sendNotification(HeaderPanelMediator.MENU_ITEM_SELECTED, {menuItem: menuItem, menuArgument: menuArgument});
            	Log.warn('ApplicationPanelMediator.onHashtagChange - contentPanel undefined for hashtag: ['+hashtag+']');
                break;
            }
            return;
        }

        var that = this;
        var callback = function () {
            that.sendNotification(HeaderPanelMediator.MENU_ITEM_SELECTED, {menuItem: menuItem, menuArgument: menuArgument});
        }
        if (!contentPanel.scriptsLoaded)
            Application.loadScripts(menuItem, callback);
        else
        {
        	Common.pageTracker._trackEvent('Menu', 'Click', menuItem +'?'+ menuArgument);

            that.sendNotification(ApplicationPanelMediator.CONTENT_DISPLAY, {contentPanel:contentPanel,menuArgument:menuArgument});

            if (contentPanel.mediator.refresh != undefined)
                contentPanel.mediator.refresh();
            that.sendNotification(ApplicationPanelMediator.CONTENT_DISPLAYED, menuItem);
            that.sendNotification(Application.HASHCHANGED, {menuItem:menuItem, menuArgument:menuArgument});
        }
    }
}
ApplicationPanelMediator = new Class(new ApplicationPanelMediator());
ApplicationPanelMediator.NAME = 'ApplicationPanelMediator';
ApplicationPanelMediator.CONTENT_DISPLAY = 'ApplicationPanelMediator.CONTENT_DISPLAY';
ApplicationPanelMediator.CONTENT_DISPLAYED = 'ApplicationPanelMediator.CONTENT_DISPLAYED';
