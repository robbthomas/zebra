var ShareDialogMediator = function(viewComponent)
{
	//alert("pre-construct");
    this.Extends = Mediator;
    this.view = undefined;
    this.token = "";
    this.payload = undefined;

    this.initialize = function(mediatorName, viewComponent, token)
    {
    	
    	this.token = token;
    	
        this.parent((mediatorName != undefined)?mediatorName:ShareDialogMediator.NAME, viewComponent);
        this.view = this.getViewComponent();
        
        this.onEmailLink = this.onEmailLink.bindWithEvent(this);
        this.view.addEvent(ShareDialog.EMAIL, this.onEmailLink);
        
        this.onRetitle = this.onRetitle.bindWithEvent(this);
        this.view.addEvent(ShareDialog.RETITLE, this.onRetitle);
        
        this.onCancel = this.onCancel.bindWithEvent(this);
        this.view.addEvent(ShareDialog.CLOSE, this.onCancel);
        
        this.onRequestDetail = this.onRequestDetail.bindWithEvent(this);
        this.view.addEvent(ShareDialog.LOAD_DETAIL, this.onRequestDetail);
        
        this.onRequestTinyUrl = this.onRequestTinyUrl.bindWithEvent(this);
        this.view.addEvent(ShareDialog.LOAD_TINYURL, this.onRequestTinyUrl);
        
        this.onSaveGuestList = this.onSaveGuestList.bindWithEvent(this);
        this.view.addEvent(ShareDialog.SAVE_GUEST_LIST, this.onSaveGuestList);

        this.onRequestGuestList = this.onRequestGuestList.bindWithEvent(this);
        this.view.addEvent(ShareDialog.LOAD_GUEST_LIST, this.onRequestGuestList);

	    this.onSavePrivacyLevel = this.onSavePrivacyLevel.bindWithEvent(this);
	    this.view.addEvent(ShareDialog.SAVE_PRIVACY_LEVEL, this.onSavePrivacyLevel);	
    }  
    
    this.onRequestGuestList = function (e)
    {
    	this.facade.retrieveProxy(GuestListProxy.NAME).loadByProjectId(e);
    }
    
    this.onSaveGuestList = function (e)
    {
    	this.facade.retrieveProxy(GuestListProxy.NAME).save(e.guestListId, e.projectId, e.emailText, e.emailList);
    	this.onCancel();
    }
    
    this.onRequestTinyUrl = function (e)
    {
    	this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).tinyUrl(e);
    }
    
    this.onRequestDetail = function (e)
    {
    	this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).requestDetail(e.projectId, e.idType);
    }
    
    this.onRetitle = function (e){
    	this.sendNotification(ApplicationFacade.RETITLE_DIALOG+this.token,e);
    }
    
    this.onCancel = function ()
    {
    	this.sendNotification(ApplicationFacade.CLOSE_DIALOG+this.token);
    }
    
    this.onClose = function ()
    {
    	this.view = undefined;
    	this.facade.removeMediator(this.getMediatorName());
    }

	this.onSavePrivacyLevel = function (e) {
		var that = this;
		this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).setPermissionsPrice(this.payload.project.id, e.permissions, e.price, e.licenseTypeId);
    	setTimeout(function(){
    		that.sendNotification(Application.REFRESH);
    	    if (window.opener != null)
    	        window.opener.EditorPlayer.refresh();
        },
        5000);
	    if (! e.keepDialogOpen) {
			this.facade.sendNotification(ApplicationFacade.CLOSE_ALL_DIALOGS);
			// no need to requestDetail from here... listener to whatever event setPermissionsPrice fires will handle that.
		}
	}
    
    this.listNotificationInterests = function()
    {
        return [
            ShareDialog.EMAIL,
            ShareDialog.EMBED,
            ShareDialog.LINK,
            ZappGadgetProjectProxy.SEND_EMAIL_REPLY,
            ZappGadgetProjectProxy.TINY_URL_REPLY,
            ZappGadgetProjectProxy.TINY_URL_FAILURE,
            ZappGadgetProjectProxy.REQUEST_DETAIL_REPLY,
            GuestListProxy.LOAD_SUCCESS,
            GuestListProxy.LOAD_FAILURE,
            GuestListProxy.SAVE_COMPLETE,
            ApplicationFacade.CLOSING_DIALOG+this.token,
            ApplicationFacade.CONFIG_DIALOG+this.token
        ];
     }

    this.onEmailLink = function(o) {
        this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).emailLink(o);
    }

    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.getBody();
        switch(notificationName)
        {
        case ApplicationFacade.CLOSING_DIALOG+this.token:
        	this.onClose();
        	break;
        case ApplicationFacade.CONFIG_DIALOG+this.token:
        	this.payload = notification.getBody();
        	this.view.config(this.payload);
        	
        	break;
        case ZappGadgetProjectProxy.TINY_URL_REPLY:
            this.view.setTinyUrl(payload);
            break;
        case ZappGadgetProjectProxy.TINY_URL_FAILURE:
            this.view.setTinyUrl("Failed to generate shortened url");
            break;
        case ShareDialog.EMBED:
            // TODO
            break;
        case ShareDialog.EMAIL:
            this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).emailLink(payload);
            break;
        case ZappGadgetProjectProxy.SEND_EMAIL_REPLY:
        	this.sendNotification(ApplicationFacade.CLOSE_DIALOG+this.token);
        	break;
        case ShareDialog.LINK:
            // TODO
            break;
        case ZappGadgetProjectProxy.REQUEST_DETAIL_REPLY:
        	this.payload.project = payload.detail;
        	this.view.config(this.payload);
        	break;
        case GuestListProxy.LOAD_SUCCESS:
        	this.view.onRetrievedGuestList(payload);
        	break;
        case GuestListProxy.LOAD_FAILURE:
        	
        	break;
        case GuestListProxy.SAVE_COMPLETE:
        	//this.view.hideUpdateAnimation();
        	//this.view.showInvitationSent();
        	break;
        default:
            break;
        }
    }
}
ShareDialogMediator = new Class(new ShareDialogMediator());
ShareDialogMediator.NAME = 'ShareDialogMediator';
ShareDialogMediator.LOAD_DETAIL = 'ShareDialogMediator.LOAD_DETAIL';