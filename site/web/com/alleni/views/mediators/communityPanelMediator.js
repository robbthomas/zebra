var CommunityPanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.communityPanel = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(CommunityPanelMediator.NAME, viewComponent);
        this.communityPanel = this.getViewComponent();
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.communityPanel.addEvent(CommunityPanel.INITIALIZED, this.onInitialized);
    }
    this.onInitialized = function()
    {
    }
    this.listNotificationInterests = function()
    {
        return [
        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName()
        switch(notificationName)
        {
        default:
            break;
        }
    }
}
CommunityPanelMediator = new Class(new CommunityPanelMediator());
CommunityPanelMediator.NAME = 'CommunityPanelMediator';
ScriptLoader.loaded(CommunityPanelMediator.NAME);
