var JoinPanelMediator = function(viewComponent/*JoinPanel*/)
{
    this.Extends = Mediator;
    this.joinPanel = undefined;
    this.successCallback = undefined;
    this.failureCallback = undefined;
    this.couponProxy = undefined;
    this.submitted = false;

    this.initialize = function(viewComponent)
    {
        this.parent(JoinPanelMediator.NAME, viewComponent);
        this.joinPanel = this.getViewComponent();
        
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.joinPanel.addEvent(JoinPanel.INITIALIZED, this.onInitialized);
        
        this.onSubmit = this.onSubmit.bindWithEvent(this);
        this.joinPanel.addEvent(JoinPanel.SUBMIT, this.onSubmit);
        
        this.onPlansAndPricing = this.onPlansAndPricing.bindWithEvent(this);
        this.joinPanel.addEvent(JoinPanel.PLANS_AND_PRICING, this.onPlansAndPricing);

        this.onUsernameUnique = this.onUsernameUnique.bindWithEvent(this);
        this.joinPanel.addEvent(JoinPanel.USERNAME_UNIQUE, this.onUsernameUnique);

        this.onEmailUnique = this.onEmailUnique.bindWithEvent(this);
        this.joinPanel.addEvent(JoinPanel.EMAIL_UNIQUE, this.onEmailUnique);
        
        this.validateCoupon = this.validateCoupon.bindWithEvent(this);
        this.joinPanel.addEvent(JoinPanel.VALIDATE_COUPON, this.validateCoupon);
        
        this.couponProxy = new CouponProxy("joinFlow");
        this.facade.registerProxy(this.couponProxy);
    }
    this.onInitialized = function ()
    {
        this.updatePlan();
    }
    this.onUsernameUnique = function(username)
    {
        this.facade.retrieveProxy(UserProxy.NAME).usernameUnique(username);
    }
    this.onEmailUnique = function(email)
    {
        this.facade.retrieveProxy(UserProxy.NAME).emailUnique(email);
    }
    this.onPlansAndPricing = function ()
    {
        this.sendNotification(Application.HREF_DISPATCH+'plansAndPricing');
    }
    
    this.onSubmit = function (o)
    {
    	if (!this.submitted) {
    		this.submitted = true;
    		this.facade.retrieveProxy(JoinProxy.NAME).create(o);
    	}
        
    }
    
    this.validateCoupon = function (o)
    {
    	this.couponProxy.validate(o);
    }
    
    this.listNotificationInterests = function ()
    {
        return [
            Application.PLAN_SELECTED,
            JoinProxy.CREATE_SUCCESS,
            JoinProxy.CREATE_FAILURE,
            UserProxy.EMAIL_UNIQUE_SUCCESS,
            UserProxy.EMAIL_UNIQUE_FAILURE,
            CouponProxy.VALIDATE_SUCCESS+"joinFlow",
            CouponProxy.VALIDATE_FAILURE+"joinFlow",
            UserProxy.USERNAME_UNIQUE_SUCCESS,
            UserProxy.USERNAME_UNIQUE_FAILURE
        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.getBody();
        switch(notificationName)
        {
        case JoinProxy.CREATE_SUCCESS:
        	this.submitted = false;
            this.joinPanel.onSubmitSuccess(payload);
            break;
        case JoinProxy.CREATE_FAILURE:
        	this.joinPanel.onSubmitFailure(payload);
            break;
        case Application.PLAN_SELECTED:
            this.updatePlan();
            break;
        case UserProxy.EMAIL_UNIQUE_SUCCESS:
            this.joinPanel.emailUnique(payload);
            break;
        case UserProxy.EMAIL_UNIQUE_FAILURE:
            break;
        case UserProxy.USERNAME_UNIQUE_SUCCESS:
            this.joinPanel.usernameUnique(payload);
            break;
        case UserProxy.USERNAME_UNIQUE_FAILURE:
            break;
        case CouponProxy.VALIDATE_SUCCESS+"joinFlow":
        	this.joinPanel.onCouponValidateSuccess(payload);
        	break;
        case CouponProxy.VALIDATE_FAILURE+"joinFlow":
        	this.joinPanel.onCouponValidateFailure(payload);
        	break;

        default:
            break;
        }
    }
    this.updatePlan = function ()
    {
            var plan = this.facade.retrieveProxy(JoinProxy.NAME).plan();
            if (plan == undefined)
            {
                Log.error('JoinPanelMediator.handleNotification Application.PLAN_SELECTED - plan:undefined');
                return;
            }
            this.joinPanel.plan(plan);
    }
}
JoinPanelMediator = new Class(new JoinPanelMediator());
JoinPanelMediator.NAME = 'JoinPanelMediator';
ScriptLoader.loaded(JoinPanelMediator.NAME);
