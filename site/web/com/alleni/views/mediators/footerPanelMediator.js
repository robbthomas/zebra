var FooterPanelMediator = function(viewComponent)
{

    this.Extends = Mediator;

    this.footerPanel = undefined;
    var that = this;

    this.initialize = function(viewComponent)
    {
        this.parent(FooterPanelMediator.NAME, viewComponent);
        this.footerPanel = this.getViewComponent();


    }
    this.onInitialized = function(counts)
    {
    }

    this.onLinkItemSelected = function(filename) {
        this.sendNotification(FooterPanelMediator.LINK_ITEM_SELECTED, filename);
    }

    this.listNotificationInterests = function()/*Array*/
    {
        return [

        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.getBody();
        switch(notificationName)
        {
        
        default:
            break;
        }
    }
}
FooterPanelMediator = new Class(new FooterPanelMediator());
FooterPanelMediator.NAME = 'FooterPanelMediator';
FooterPanelMediator.LINK_ITEM_SELECTED = 'FooterPanelMediator.LINK_ITEM_SELECTED';
FooterPanelMediator.HREF_DISPATCH;

