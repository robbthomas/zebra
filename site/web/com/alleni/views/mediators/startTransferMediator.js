var StartTransferMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.view = undefined;
    this.token = "";

    this.defaultEmailConfirmationFailureMessage = 'Unable to verify email';

    this.initialize = function(mediatorName,viewComponent,token)
    {
    	this.token = token;
        this.parent(StartTransferMediator.NAME, viewComponent);
        this.view = this.getViewComponent();
        
        this.onInitialize = this.onInitialize.bindWithEvent(this);
        this.view.addEvent(StartTransfer.INITIALIZED, this.onInitialize);
        
        this.onGetProjectsAvailableToTransfer = this.onGetProjectsAvailableToTransfer.bindWithEvent(this);
        this.view.addEvent(StartTransfer.GET_TRANSFERS_PROJECTS, this.onGetProjectsAvailableToTransfer);
        
        this.onConfirmUserEmailAddress = this.onConfirmUserEmailAddress.bindWithEvent(this);
        this.view.addEvent(StartTransfer.CONFIRM_USER_BY_EMAIL, this.onConfirmUserEmailAddress);

        this.checkCredentialsForSingle = this.checkCredentialsForSingle.bindWithEvent(this);
        this.view.addEvent(StartTransfer.START_SINGLE_PROJECT_TRANSFER, this.checkCredentialsForSingle);
        
        this.checkCredentialsForBulk = this.checkCredentialsForBulk.bindWithEvent(this);
        this.view.addEvent(StartTransfer.START_BULK_PROJECT_TRANSFER, this.checkCredentialsForBulk);
        
        this.sendNotification(ApplicationFacade.SHOW_CLOSE_DIALOG+this.token, true);
    }    
    
    this.onInitialize = function(){
    	var searchObject = {
        		count: 9999,
        		page: 0,
        		group: 'member',
        		type: 'project',
        		refresh: true,
        		tiny: true,
        		notifications: {
        			success: StartTransfer.RETURN_TRANSFER_PROJECT_LIST,
        			failure: StartTransfer.RETURN_TRANSFER_PROJECT_LIST_FAILURE
        		},
        		filters: {
        			retired: 'false',
        			category: 'all',
        			search: '',
        			sort: "nameAscending"
        		}
        	}
    	this.onGetProjectsAvailableToTransfer(searchObject);
    }
    
    // Confirm the email is a valid ZebraZapps user
    this.onConfirmUserEmailAddress = function(payload)
    {
    	this.facade.retrieveProxy(UserProxy.NAME).confirmUserEmail(payload);
    }
    
    this.checkCredentialsForSingle = function(cfg) {
    	var config = {
    		successNotification: StartTransferMediator.START_SINGLE_TRANSFER,
    		failureNotification: ApplicationFacade.SHOW_ACCOUNT_DUE_DIALOG,
    		failureConfig: {},
    		payload: cfg
    	};
    	
    	this.sendNotification(ApplicationFacade.CHECK_ACCOUNT_STANDING, config);
    };
    
    this.checkCredentialsForBulk = function(cfg) {
    	var config = {
    		successNotification: StartTransferMediator.START_BULK_TRANSFER,
    		failureNotification: ApplicationFacade.SHOW_ACCOUNT_DUE_DIALOG,
    		failureConfig: {},
    		payload: cfg
    	};
    	
    	this.sendNotification(ApplicationFacade.CHECK_ACCOUNT_STANDING, config);
    };
    
    this.onStartSingleProjectTransfer = function(payload)
    {
    	this.facade.retrieveProxy(AccountProxy.NAME).startTransfer(payload);
    }
    this.onStartBulkProjectTransfer = function(payload)
    {
    	var strMessage = "Are you sure you want to<br />transfer "+payload.projectList.length+" project";
    	if (payload.projectList.length > 1) {
    		strMessage += 's';
    	}
    	strMessage += '?';
    	this.sendNotification(ApplicationFacade.CONFIRM_DIALOG, 
    			{
    				token:StartTransferMediator.NAME+"_startBulk", 
    				message:strMessage, 
    				arguments:payload.projectList
    			});
    	//this.facade.retrieveProxy(AccountProxy.NAME).startBulkTransfer(payload.projectList);
    }
    // Go get the list of available projects that can be transfered
    this.onGetProjectsAvailableToTransfer = function(payload)
    {
    	this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).requestList(payload);
    }
    
    

    
    
    /**
     * Cancel request from the view (handler)
     */
    this.onCancel = function ()
    {
    	this.sendNotification(ApplicationFacade.CLOSE_DIALOG+this.token);
    }
    
    /**
     * required to be in a dialog
     */
    this.onClose = function ()
    {
    	this.view = undefined;
    	this.facade.removeMediator(this.getMediatorName());
    }

    /**
     * 
     */
    this.listNotificationInterests = function()/*Array*/
    {
        return [
            ApplicationFacade.CLOSING_DIALOG+this.token,
            ApplicationFacade.CONFIG_DIALOG+this.token,
            StartTransfer.RETURN_TRANSFER_PROJECT_LIST,
            StartTransfer.RETURN_TRANSFER_PROJECT_LIST_FAILURE,
            StartTransferMediator.START_SINGLE_TRANSFER,
            StartTransferMediator.START_BULK_TRANSFER,
            AccountProxy.BEGIN_PROJECT_TRANSFER_SUCCESS,
            UserProxy.CONFIRM_EMAIL_SUCCESS,
            UserProxy.CONFIRM_EMAIL_FAILURE,
            ConfirmDialogCommand.YES+StartTransferMediator.NAME+"_startBulk",
            ConfirmDialogCommand.NO+StartTransferMediator.NAME+"_startBulk"
        ];
     }
    /**
     * 
     */
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName()
        var payload = notification.getBody();
        switch(notificationName)
        {
        case ApplicationFacade.CLOSING_DIALOG+this.token:
        	this.onClose();
        	break;
        case ApplicationFacade.CONFIG_DIALOG+this.token:
        	this.payload = notification.getBody();
        	this.view.config(this.payload);
        	break;
        case StartTransfer.RETURN_TRANSFER_PROJECT_LIST:
        	this.view.populateProjectTransferList(payload);
        	break;
        case StartTransfer.RETURN_TRANSFER_PROJECT_LIST_FAILURE:
        	this.view.projectTransferListFailure(payload);
        	break;
        case StartTransferMediator.START_SINGLE_TRANSFER:
        	this.onStartSingleProjectTransfer(payload);
        	break;
        case StartTransferMediator.START_BULK_TRANSFER:
        	this.onStartBulkProjectTransfer(payload);
        	break;
        case UserProxy.CONFIRM_EMAIL_SUCCESS:
        	if (!payload.hasOwnProperty('accountType')) {
        		payload.eligibilityMessage = this.defaultEmailConfirmationFailureMessage;
        		this.view.confirmEmailFailure(payload);
        	} else if(payload.accountType == 'COLLECTOR') {
        		payload.eligibilityMessage = 'This user exists, but is a Collector. Projects cannot be shared with Collector accounts.'
        		this.view.confirmEmailFailure(payload);
        	} else {
        		this.view.confirmEmailSuccess(payload);
        	}
        	break;
        case UserProxy.CONFIRM_EMAIL_FAILURE:
        	payload.eligibilityMessage = this.defaultEmailConfirmationFailureMessage;
        	this.view.confirmEmailFailure(payload);
        	break;
        case AccountProxy.BEGIN_PROJECT_TRANSFER_SUCCESS:
        	this.view.onTransferSuccess(payload);
        	break;
        case ConfirmDialogCommand.YES+StartTransferMediator.NAME+"_startBulk":
        	this.facade.retrieveProxy(AccountProxy.NAME).startBulkTransfer(payload);
        	break;
        case ConfirmDialogCommand.NO+StartTransferMediator.NAME+"_startBulk":
        	// do nothing
        	break;
        default:
            break;
        }
    }
}
StartTransferMediator = new Class(new StartTransferMediator());
StartTransferMediator.NAME = 'StartTransferMediator';
StartTransferMediator.START_SINGLE_TRANSFER = 'StartTransferMediator.START_SINGLE_TRANSFER';
StartTransferMediator.START_BULK_TRANSFER = 'StartTransferMediator.START_BULK_TRANSFER';
ScriptLoader.loaded(StartTransferMediator.NAME);
