var ListTabsPanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.view = undefined;
    this.token = "";

    this.initialize = function(mediatorName,viewComponent,token) {
    	this.token = token;
        
    	this.parent((mediatorName != undefined)?mediatorName:ListTabsPanelMediator.NAME, viewComponent);
        this.view = this.getViewComponent();
        
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.view.addEvent(ListTabsPanel.INITIALIZED, this.onInitialized);
    }    
    
    this.onInitialized = function() {
    	//this.changeDisplay('visitor', undefined, undefined, 'zapp');
    	this.sendNotification(ListTabsPanelMediator.INITIALIZED + this.token, {html: this.getViewHtml()});
    }
   
    this.changeDisplay = function (group, hash, authorMemberId, projectType) {
    	if (undefined == group && undefined == hash) {
    		group = 'visitor';
    		hash = 'list';
    	}
    	
    	this.view.showGroup(group, hash, projectType, authorMemberId);
    };
    
    this.getViewHtml = function() {
    	return this.view.getHtml();
    };
    
   this.listNotificationInterests = function() 
   {
       return [
           BrowsePanel.LIST_TYPE_CHANGE
       ];
   }
   
   this.handleNotification = function(notification) 
   {
        var notificationName = notification.getName()
        var payload = notification.getBody();
        switch(notificationName) {
        case BrowsePanel.LIST_TYPE_CHANGE:
        	this.changeDisplay(payload.group, payload.hash, payload.authorMemberId, payload.type);
        	break;
        default:
	        break;
        }
    }
   
   
}
ListTabsPanelMediator = new Class(new ListTabsPanelMediator());
ListTabsPanelMediator.NAME = 'ListTabsPanelMediator';
ListTabsPanelMediator.INITIALIZED = 'ListTabsPanelMediator.INITIALIZED';
ScriptLoader.loaded(ListTabsPanelMediator.NAME);
