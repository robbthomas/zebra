var AboutZebraPanelMediator = function(viewComponent/*AboutZebraPanel*/)
{

    this.Extends = Mediator;
    this.aboutZebraPanel = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(AboutZebraPanelMediator.NAME, viewComponent);
        this.aboutZebraPanel = this.getViewComponent();
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.aboutZebraPanel.addEvent(AboutZebraPanel.INITIALIZED, this.onInitialized);

        this.onLinkItemSelected = this.onLinkItemSelected.bindWithEvent(this);
        this.aboutZebraPanel.addEvent(AboutZebraPanel.LINK_ITEM_SELECTED, this.onLinkItemSelected);
    }
    this.onInitialized = function()
    {

    }

    this.onLinkItemSelected = function(filename) {
        this.sendNotification(AboutZebraPanelMediator.LINK_ITEM_SELECTED, filename);
    }


    this.listNotificationInterests = function()/*Array*/
    {
        return [
            AboutZebraPanelMediator.LINK_ITEM_SELECTED
        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName()
        switch(notificationName)
        {

        case AboutZebraPanelMediator.LINK_ITEM_SELECTED:
            this.aboutZebraPanel.loadFragment(notification.getBody())
            break;

        default:
            break;
        }
    }
}
AboutZebraPanelMediator = new Class(new AboutZebraPanelMediator());
AboutZebraPanelMediator.NAME = 'AboutZebraPanelMediator';
AboutZebraPanelMediator.LINK_ITEM_SELECTED = 'AboutZebraPanelLinkItemSelected';
ScriptLoader.loaded(AboutZebraPanelMediator.NAME);