var NavigationPanelMediator = function(viewComponent/*NavigationPanel*/)
{
    this.Extends = Mediator;
    this.navigationPanel = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(NavigationPanelMediator.NAME, viewComponent);
        this.navigationPanel = this.getViewComponent();
        
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.navigationPanel.addEvent(NavigationPanel.INITIALIZED, this.onInitialized);

        this.onMenuItemSelected = this.onMenuItemSelected.bindWithEvent(this);
        this.navigationPanel.addEvent(NavigationPanel.MENU_ITEM_SELECTED, this.onMenuItemSelected);
    }
    this.onInitialized = function ()
    {
        this.sendNotification(NavigationPanelMediator.INITIALIZED);
    }
    this.onMenuItemSelected = function (menuItem)
    {
        this.sendNotification(NavigationPanelMediator.MENU_ITEM_SELECTED, {menuItem: menuItem});
    }
}
NavigationPanelMediator = new Class(new NavigationPanelMediator());
NavigationPanelMediator.NAME = 'NavigationPanelMediator';
NavigationPanelMediator.INITIALIZED = 'NavigationPanelMediator.INITIALIZED';
NavigationPanelMediator.MENU_ITEM_SELECTED = 'NavigationPanelMediator.MENU_ITEM_SELECTED';

