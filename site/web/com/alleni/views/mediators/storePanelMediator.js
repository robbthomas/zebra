var StorePanelMediator = function(viewComponent/*StorePanel*/)
{
    this.Extends = Mediator;
    this.storePanel = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(StorePanelMediator.NAME, viewComponent);
        this.storePanel = this.getViewComponent();

        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.storePanel.addEvent(StorePanel.INITIALIZED, this.onInitialized);
    }
    this.onInitialized = function()
    {
    }
    this.listNotificationInterests = function()/*Array*/
    {
        return [
        ];
    }
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName()
        switch(notificationName)
        {
            default:
                break;
        }
    }
}
StorePanelMediator = new Class(new StorePanelMediator());
StorePanelMediator.NAME = 'StorePanelMediator';
ScriptLoader.loaded(StorePanelMediator.NAME);
