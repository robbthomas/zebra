var AccountPanelMediator = function(viewComponent/*AccountPanel*/)
{
    this.Extends = Mediator;
    this.accountPanel = undefined;
    this.subMediator = undefined;

    this.token = undefined;
    
    this.initialize = function(viewComponent)
    {
        this.parent(AccountPanelMediator.NAME, viewComponent);
        this.accountPanel = this.getViewComponent();

        this.token = AccountPanelMediator.NAME;
        this.onInitialized = this.onInitialized.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.INITIALIZED, this.onInitialized);
       
        
    }
    
    this.onInitialized = function(payload){
    	 
    	this.onRequestRefresh = this.onRequestRefresh.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.REQUEST_REFRESH, this.onRequestRefresh);

        this.onSubmit = this.onSubmit.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.SUBMIT, this.onSubmit);

        this.onEmailUnique = this.onEmailUnique.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.EMAIL_UNIQUE, this.onEmailUnique);
        
        this.onGetAccountInformation = this.onGetAccountInformation.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.GET_ACCOUNT_INFORMATION, this.onGetAccountInformation);

        this.onChangeAccountType = this.onChangeAccountType.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.CHANGE_ACCOUNT_TYPE, this.onChangeAccountType);
        
        this.onCancelTransfer = this.onCancelTransfer.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.CANCEL_TRANSFER, this.onCancelTransfer);
        
        this.onCancelTransfer = this.onCancelTransfer.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.DECLINE_TRANSFER, this.onCancelTransfer);
        
        this.onClearTransfer = this.onClearTransfer.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.CLEAR_TRANSFER, this.onClearTransfer);
        
        this.onAcceptTransfer = this.onAcceptTransfer.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.ACCEPT_TRANSFER, this.onAcceptTransfer);
        
        this.onGetTransferIncoming = this.onGetTransferIncoming.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.GET_TRANSFERS_INCOMING, this.onGetTransferIncoming);
        
        this.onGetTransferOutgoing = this.onGetTransferOutgoing.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.GET_TRANSFERS_OUTGOING, this.onGetTransferOutgoing);
        
        this.confirmSetNumLearners = this.confirmSetNumLearners.bindWithEvent(this);
        this.accountPanel.addEvent(AccountPanel.SET_NUMBER_LEARNERS, this.confirmSetNumLearners);
    }
    
    this.onAcceptTransfer = function(payload)
    {
    	this.sendNotification(ApplicationFacade.CONFIRM_DIALOG, 
    			{token:AccountPanel.NAME+"_accept", 
    			message:"Are you sure you wish to accept<br />this transfer?", 
    			arguments:payload});
    	//this.facade.retrieveProxy(AccountProxy.NAME).acceptTransfer(payload);
    } 
    this.onCancelTransfer = function(payload)
    {
    	this.sendNotification(ApplicationFacade.CONFIRM_DIALOG, 
    			{token:AccountPanel.NAME+"_cancel", 
    			message:"Are you sure you wish to cancel<br />this transfer?", 
    			arguments:payload});
    	//this.facade.retrieveProxy(AccountProxy.NAME).cancelTransfer(payload);
    }
    this.onClearTransfer = function(payload)
    {
    	this.sendNotification(ApplicationFacade.CONFIRM_DIALOG, 
    			{token:AccountPanel.NAME+"_clear", 
    			message:"Are you sure you wish to clear<br />this transfer?", 
    			arguments:payload});
    }
    this.onDeclineTransfer = function(payload)
    {
    	this.sendNotification(ApplicationFacade.CONFIRM_DIALOG, 
    			{token:AccountPanel.NAME+"_decline", 
    			message:"Are you sure you wish to decline<br />this transfer?", 
    			arguments:payload});
    	//this.facade.retrieveProxy(AccountProxy.NAME).cancelTransfer(payload);
    }
    // Go get the list of pending transfers waiting to be accepted
    this.onGetTransferIncoming = function()
    {	
    	this.facade.retrieveProxy(AccountProxy.NAME).getPendingIncomingTransfers();
    }
    // Go get the list of pending transfers the user has sent to others
    this.onGetTransferOutgoing = function()
    {	
    	this.facade.retrieveProxy(AccountProxy.NAME).getPendingOutgoingTransfers();
    }
    
    this.onGetAccountInformation = function()
    {
    	this.facade.retrieveProxy(AccountProxy.NAME).getAccountInformation();
    }
    this.onRequestRefresh = function(formName)
    {
       //Log.debug('AccountPanelMediator.onRequestRefresh - formName:'+formName);
        this.facade.retrieveProxy(AccountProxy.NAME).get(formName);
    }
    this.onSubmit = function (o)
    {
       //Log.debug('AccountPanelMediator.onSubmit - o:'+JSON.encode(o));
    	
        this.facade.retrieveProxy(AccountProxy.NAME).update(o.formName, o.form);
    }
    this.onEmailUnique = function(email)
    {
        this.facade.retrieveProxy(UserProxy.NAME).emailUnique(email);
    }
    
    // Upgrade or downgrade account to specified account.
    this.onChangeAccountType = function(payload)
    {

    	switch (payload)
        {	
        case 'collector':
        	//this.facade.retrieveProxy(UserProxy.NAME).upgrade(payload);
        	this.confirmUpgrade(payload);
        	break;
        case 'creator':
        case 'pro':
        	if(!Credentials.hasCreditCardProfile()){
        		// no CC on file
        		this.sendNotification(ApplicationFacade.LOAD_UPGRADE, {item:undefined, upgradeTo:payload});
        	}else if(!Credentials.goodStanding()){
        		// bad CC on file
        		var payload = {};
            	var config = new LoadDialogueViewConfig(
            			"Account Payment is Now Due",
            			"accountPayDue.js",
            			"accountPayDueMediator.js",
            			"accountPayDue.html",
            			payload
            			);
            	
            
            	this.facade.sendNotification(ApplicationFacade.SHOW_DIALOG, config );
        	}else{
        		//this.facade.retrieveProxy(UserProxy.NAME).upgrade(payload);
        		this.confirmUpgrade(payload);
        	}
        	break;
        }
    	
    }
    
    this.confirmUpgrade = function(type) {
    	var ramifications = '';
    	var proceed = true;
    	
    	switch(type) {
    	case 'collector':
    		ramifications = '<br/><br/>You will no longer be able to create<br/>Apps or Gadgets, and will not be able<br/>to purchase Gadgets.'
    		break;
    	default:
    		if(!Credentials.hasCreditCardProfile()){
    			proceed = false;
        		this.sendNotification(ApplicationFacade.LOAD_UPGRADE, {item:undefined, upgradeTo:type});
        	}
    		break;
    	}
    	
    	
    	if(proceed){
	    	this.sendNotification(ApplicationFacade.CONFIRM_DIALOG, 
	    			{token:AccountPanelMediator.NAME+"_upgrade", 
	    			message:"Are you sure you wish to change your<br />account type to " + type + "?" + ramifications, 
	    			arguments:type});
    	}
    }
    
    
    this.confirmSetNumLearners = function(numLearners)
    {
    	this.sendNotification(ApplicationFacade.CONFIRM_DIALOG, 
    			{token:AccountPanel.NAME+"_setNumLearners", 
    			message:"Are you sure you want to change<br/>your learner account limit to "+numLearners+"?", 
    			arguments:numLearners});
    }
    
    this.getAuthorAvatar = function(avatarId) {
    	this.facade.retrieveProxy(UserProxy.NAME).getAuthorAvatarImage(avatarId, this.token);
    };
    
    this.getAuthorBanner = function(bannerId) {
    	this.facade.retrieveProxy(UserProxy.NAME).getAuthorBannerImage(bannerId, this.token);
    };
    
    this.listNotificationInterests = function()/*Array*/
    {
        return [
                        
            AccountProxy.GET_SUCCESS,
            AccountProxy.GET_FAILURE,
            AccountProxy.UPDATE_SUCCESS,
            AccountProxy.UPDATE_FAILURE,
            AccountProxy.GET_ACCOUNT_INFORMATION_SUCCESS,
            UserProxy.EMAIL_UNIQUE_SUCCESS,
            UserProxy.EMAIL_UNIQUE_FAILURE,
            UserProxy.REFRESH_SUCCESS,
            UserProxy.UPGRADE_DOWNGRADE_SUCCESS,
            UserProxy.GET_AUTHOR_AVATAR_SUCCESS+this.token,
            UserProxy.GET_AUTHOR_AVATAR_FAILURE+this.token,
            UserProxy.GET_AUTHOR_BANNER_SUCCESS+this.token,
            UserProxy.GET_AUTHOR_BANNER_FAILURE+this.token,
            AccountProxy.GET_INCOMING_TRANSFER_SUCCESS,
            AccountProxy.GET_INCOMING_TRANSFER_FAILURE,
            AccountProxy.GET_OUTGOING_TRANSFER_SUCCESS,
            AccountProxy.GET_OUTGOING_TRANSFER_FAILURE,
            AccountPanelMediator.CHANGE_DISPLAY_ARGUEMENT,
            AccountProxy.ACCEPT_PROJECT_TRANSFER_SUCCESS,
            AccountProxy.CANCEL_PROJECT_TRANSFER_SUCCESS,
            ConfirmDialogCommand.YES+AccountPanelMediator.NAME+"_upgrade",
            ConfirmDialogCommand.NO+AccountPanelMediator.NAME+"_upgrade",
            ConfirmDialogCommand.YES+AccountPanel.NAME+"_cancel",
            ConfirmDialogCommand.NO+AccountPanel.NAME+"_cancel",
            ConfirmDialogCommand.YES+AccountPanel.NAME+"_clear",
            ConfirmDialogCommand.NO+AccountPanel.NAME+"_clear",
            ConfirmDialogCommand.YES+AccountPanel.NAME+"_decline",
            ConfirmDialogCommand.NO+AccountPanel.NAME+"_decline",
            ConfirmDialogCommand.YES+AccountPanel.NAME+"_accept",
            ConfirmDialogCommand.NO+AccountPanel.NAME+"_accept",
            ConfirmDialogCommand.YES+AccountPanel.NAME+"_setNumLearners",
            ConfirmDialogCommand.NO+AccountPanel.NAME+"_setNumLearners"
        ];
    }
    
    this.handleNotification = function(notification)
    {
    	var that = this;
        var name = notification.getName();
       //Log.debug('AccountPanelMediator.handleNotification - name:'+name);
        var payload = notification.getBody();
        
        switch(name)
        {
        case UserProxy.UPGRADE_DOWNGRADE_SUCCESS:
        	this.accountPanel.accountUpgradedDowngraded(payload);
        	break;
        case AccountProxy.GET_ACCOUNT_INFORMATION_SUCCESS:
        	this.accountPanel.populateAccountSettings(payload);
        	break;
        case AccountProxy.GET_INCOMING_TRANSFER_SUCCESS:
        	this.accountPanel.populateIncomingTransferList(payload);
        	break;
        case AccountProxy.GET_OUTGOING_TRANSFER_SUCCESS:
        	this.accountPanel.populateOutgoingTransferList(payload);
        	break;
        case AccountProxy.GET_SUCCESS:
            this.accountPanel.populate(payload);
            //this.accountPanel.hideAvatarImage();
            //this.accountPanel.hideBannerImage();
            setTimeout(function(){
            	if(payload.avatarid){
            		that.getAuthorAvatar(payload.avatarid);
            	}
               	if(payload.bannerid){
               		that.getAuthorBanner(payload.bannerid);
               	}
            },
            5000);
            
            
            break;
        case AccountProxy.GET_FAILURE:
            this.accountPanel.populateFail(payload);
            break;
        case AccountProxy.UPDATE_SUCCESS:
            this.accountPanel.updateSuccess(payload);
            ProcessingScreen.hide("Thank you.", 2000);
            this.onRequestRefresh(payload.accountUpdate.group);
            break;
        case AccountProxy.UPDATE_FAILURE:
            this.accountPanel.updateFailure(payload);
            ProcessingScreen.hide("There was a problem updating your account.", 4000);
            break;
        case UserProxy.GET_AUTHOR_AVATAR_SUCCESS+this.token:
        	this.accountPanel.showAvatarImage(payload);
        	break;
        case UserProxy.GET_AUTHOR_AVATAR_FAILURE+this.token:
        	//this.accountPanel.hideAvatarImage();
        	break;
        case UserProxy.GET_AUTHOR_BANNER_SUCCESS+this.token:
        	this.accountPanel.showBannerImage(payload);
        	break;
        case UserProxy.GET_AUTHOR_BANNER_FAILURE+this.token: 
        	//this.accountPanel.hideBannerImage();
        	break;
        case UserProxy.EMAIL_UNIQUE_SUCCESS:
            this.accountPanel.emailUnique(payload);
            break;
        case UserProxy.EMAIL_UNIQUE_FAILURE:
            break;
        case UserProxy.REFRESH_SUCCESS:
        	this.accountPanel.accountUpgradedDowngraded();
        	this.accountPanel.updateCurrentCCInfo(payload);
        	break;
        case AccountPanelMediator.CHANGE_DISPLAY_ARGUEMENT:
        	this.changeDisplay(payload);
        	break;
        case AccountProxy.ACCEPT_PROJECT_TRANSFER_SUCCESS:
        	this.accountPanel.showTransferSuccess(payload);
        	this.accountPanel.updateTransferList();
        	this.facade.sendNotification(ApplicationFacade.UPDATE_CREDENTIALS);
        	break;
        case AccountProxy.CANCEL_PROJECT_TRANSFER_SUCCESS:
        	this.accountPanel.showTransferCanceled(payload);
        	this.accountPanel.updateTransferList();
        	this.facade.sendNotification(ApplicationFacade.UPDATE_CREDENTIALS);
        	break;
        case ConfirmDialogCommand.YES+AccountPanelMediator.NAME+"_upgrade":
        	this.facade.retrieveProxy(UserProxy.NAME).upgrade(payload);
        	break;
        case ConfirmDialogCommand.YES+AccountPanel.NAME+"_cancel":
        case ConfirmDialogCommand.YES+AccountPanel.NAME+"_decline":
        	this.facade.retrieveProxy(AccountProxy.NAME).cancelTransfer(payload);
        	break;
        case ConfirmDialogCommand.YES+AccountPanel.NAME+"_clear":
        	this.facade.retrieveProxy(AccountProxy.NAME).clearTransfer(payload);
        	break;
        case ConfirmDialogCommand.YES+AccountPanel.NAME+"_accept":
        	
        	this.facade.retrieveProxy(AccountProxy.NAME).acceptTransfer(payload);
        	break;
        case ConfirmDialogCommand.YES+AccountPanel.NAME+"_setNumLearners":
        	this.facade.retrieveProxy(AccountProxy.NAME).setNumberLearners(payload);
        	this.accountPanel.setInitialSelectedNumLearnersValue(payload);
        	break;
        case ConfirmDialogCommand.NO+AccountPanel.NAME+"_setNumLearners":
        	this.accountPanel.cancelNumLearnersChange();
        	break;
        case ConfirmDialogCommand.NO+AccountPanel.NAME+"_cancel":
        case ConfirmDialogCommand.NO+AccountPanel.NAME+"_decline":
        case ConfirmDialogCommand.NO+AccountPanel.NAME+"_accept":
        case ConfirmDialogCommand.NO+AccountPanel.NAME+"_clear":
        	this.accountPanel.toggleTransferRow(true, payload);
        	break;
        case ConfirmDialogCommand.NO+AccountPanelMediator.NAME+"_upgrade":
        	break;
        default:
            break;
        }
    }
    
    
    // TODO: move this type of functionality to parentMediator.js when it's built?
    // This is necessary for the AccountPanel, but not so much for AuthorProfilePanel. Etc.
    this.changeDisplay = function(displayArgument){
    	var view = undefined;
    	
    	
    	if(this.subMediator != undefined){
    		this.facade.removeMediator(this.subMediator.getMediatorName());
    	}
    	
    	switch(displayArgument){
    	case "startTransfer":
    		if(Credentials.accountTypeTag() == "collector"){
    			this.accountPanel.display("myProfile");
    			break;
    		}
    		var title = '<div class="blockTop"><h2>Share Projects: <span class="name">Send a Copy</span></h2></div>';
    		view = new StartTransfer("startTransfer");
    		this.subMediator = new StartTransferMediator(undefined,view);
    		this.facade.registerMediator(this.subMediator);

    		this.accountPanel.injectView(view.element);
    		this.accountPanel.prependView(title);
    		break;
    	default:
    		break;
    	}
    }

}
AccountPanelMediator = new Class(new AccountPanelMediator());
AccountPanelMediator.NAME = 'AccountPanelMediator';
AccountPanelMediator.CHANGE_DISPLAY_ARGUEMENT = "AccountPanelMediator.CHANGE_DISPLAY_ARGUMENT";
ScriptLoader.loaded(AccountPanelMediator.NAME);
