var abstractDialogMediator = function(viewComponent) {
	this.Extends = Mediator;
    this.view = undefined;
    this.token = "";
    this.payload = undefined;

    this.initialize = function(mediatorName, viewComponent, token) {
    	var i = 0;
    	
    	this.token = token;
    	
        this.parent((mediatorName != undefined)?mediatorName:abstractDialogMediator.NAME, viewComponent);
        this.view = this.getViewComponent();

        //requires that we redefine events in view components in an array instead of raw properties on the object itself. Good/bad idea?
        //might also spawn discussion over whether to exclude any events from this binding pattern, which would require configuration in the view component
        for (i = 0; i < this.view.events.length; i++) {
        	this['on' + this.view.events[i]] = this['on' + this.view.events[i]].bindWithEvent(this);
        	this.view.addEvent(this.view.events[i], this['on' + this.view.events[i]]);
        }
    }    
    
    this.onCancel = function () {
    	this.sendNotification(ApplicationFacade.CLOSE_DIALOG+this.token);
    }
    
    this.onClose = function () {
    	this.view = undefined;
    	this.facade.removeMediator(this.getMediatorName());
    }
	
	//Should be overridden -- but how to make sure that the two ApplicationFacade events are included? Hmm...ideas.
    this.listNotificationInterests = function()
    {
        return [
            ApplicationFacade.CLOSING_DIALOG+this.token,
            ApplicationFacade.CONFIG_DIALOG+this.token
        ];
     }

    //Should be overridden -- but how to make sure that the ApplicationFacade events are included in the subclass's handleNotification()
    this.handleNotification = function(notification)
    {
        var notificationName = notification.getName();
        var payload = notification.getBody();
        switch(notificationName)
        {
        case ApplicationFacade.CLOSING_DIALOG+this.token:
        	this.onClose();
        	break;
        case ApplicationFacade.CONFIG_DIALOG+this.token:
        	this.payload = notification.getBody();
        	this.view.config(this.payload);
        	break;
        default:
            break;
        }
    }
}
abstractDialogMediator = new Class(new abstractDialogMediator());
abstractDialogMediator.NAME = 'abstractDialogMediator';