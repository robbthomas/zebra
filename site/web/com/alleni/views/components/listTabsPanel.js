var ListTabsPanel = function()
{
    this.Extends = UIComponent;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;
    this.html = '';
    
    this.initialize = function (parent)
    {
        if (parent == undefined)
            Log.error('ListTabsPanel.initialize - parent undefined');
        this.parent(parent);

        var that = this;
        var o = {
            filename: 'listTabsPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.html = fragment;
        
        this.initializationComplete();
    }
    
    this.initializationComplete = function ()
    {
        this.fireEvent(ListTabsPanel.INITIALIZED, ListTabsPanel.NAME);
    }
    
    this.getHtml = function() {
    	return this.html;
    }
    
    this.showGroup = function (group, hash, projectType, authorMemberId)
    {
    	var test0 = 'div.tab.' + group;
    	var test1 = jQuery('div.tab.' + group, this.element);
    	var test2 = jQuery('div.tab.visitor.authorProfile', this.element);
    	
    	jQuery('div.tab', this.element).hide();
        
    	if (undefined == authorMemberId) {
        	jQuery('div.tab.' + group + '.selected.list', this.element).show();
        	jQuery('div.tab.' + group + '.unselected.list', this.element).show();
        	jQuery('div.tab.visitor.authorProfile', this.element).hide();
        } else {
        	jQuery('div.tab.visitor.authorProfile', this.element).show();
        }
        
        jQuery('div.tab.visitor.authorProfile', this.element).each(function() {
        	this.href += authorMemberId;
        });
        
        this.highlightType(projectType);
        
        jQuery('#myProjects', this.element).toggle(Credentials.accountTypeTag() != 'collector' && group != 'visitor');
    }
    
    this.highlightType = function (type)
    {
        //Log.debug('ListTabsPanel.highlightType - type:'+type);
        jQuery('div.tab', this.element).removeClass('selected');
        jQuery('div.tab', this.element).addClass('unselected');
        jQuery('div.tab', this.element).each(function () {
           if (jQuery('a', this)[0].hash.indexOf(type) != -1) {
        	   $(this).removeClass('unselected');
        	   $(this).addClass('selected');
            }
            
            if (! $(this).getStyle('display').contains('none')){
            	$(this).setStyle('display', 'inline');
            }
        });
    }
}
ListTabsPanel = new Class(new ListTabsPanel());
ListTabsPanel.NAME = 'ListTabsPanel';
ListTabsPanel.INITIALIZED = 'ListTabsPanel.INITIALIZED';
ListTabsPanel.FRAGMENT_LOADED = 'ListTabsPanel.FRAGMENT_LOADED';
ScriptLoader.loaded(ListTabsPanel.NAME);
