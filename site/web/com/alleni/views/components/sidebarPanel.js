var SidebarPanel = function()
{
    this.Extends = UIComponent;
    this.MAX_NAME_LENGTH = 50;
    this.MAX_CONSECUTIVE_CHARACTERS = 15;
    this.name = function(name)
    {
    	name = this.formatName(name);
        if (name == null || name.length == 0) {
        	name = 'No name found';
        }
        if (name.length > this.MAX_NAME_LENGTH) {
        	name = name.substring(0, this.MAX_NAME_LENGTH) + '...';
        }
        return makeSafeString(name);
    }
    this.formatName = function(name)
    {
    	if(name.indexOf(' ') >= this.MAX_CONSECUTIVE_CHARACTERS || name.indexOf(' ') == -1 && name.length > this.MAX_CONSECUTIVE_CHARACTERS )
		{
    		name = name.substr(0,this.MAX_CONSECUTIVE_CHARACTERS-1)+ '...';
		}
    	return name;
    }
    this.initialize = function (parent)
    {
        this.parent(parent);
        var that = this;
        var o = {
            filename: 'sidebarPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    
    this.updateMenuSelection = function(id){

            jQuery('div#dashboardAccount span').removeClass('selected');
            jQuery('div#dashboardAccount span #'+id, this.element).addClass('selected');
       
    }
    
    this.handleClick = function (hash,node)
    {
    	Log.debug('SidebarPanel.HANDLECLICK '+hash);
    	 var href = hash;
    	 
         if (href.indexOf('#/') == 0)
         {
        	 var a = href.substring(2).split('?');
        	 var target = a[0];
        	 var args = a[1].split('&');
        	 var group = args[0];
        	 var type = args[1];
        	 this.fireEvent(SidebarPanel.DISPLAY_CHANGE, {group: group, type: type, authormemberId: 0});
        	 return true;
         }
         
         switch(href)
         {
         case '#lostpassword':
             this.fireEvent(SidebarPanel.LOST_PASSWORD);
             return false;
             break;
         default:
             var o = {
                 href: href.substring(1),
                 parent: '',
                 form: this.formDataToObj(jQuery('#sidebarPanel form'+href).serializeArray()) // grab the matching form (if any)
             }
             if (o.href == 'signin' && typeof o.form.keepSignedIn != 'boolean')
             {
                 // for certain browsers this property is undefined when the checkbox is not checked and just '' when it is checked...
             	o.form.keepSignedIn = ((o.form.keepSignedIn == '' || o.form.keepSignedIn == 'on')?true:false);
             }

             this.fireEvent(SidebarPanel.HREF_CLICKED, o);
             break;
         }
         return false;
    }
    
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        var that = this;

        jQuery('div.sidebar a').click(function(event) {return that.handleClick(this.hash, this)});
        
        jQuery('form#signin .sidebarInput').focus(function(e) {
        	var jElement = jQuery(this);
        	if (jElement.val() == jElement.attr('defaultFieldValue')) {
        		jElement.val('');
        	}
        }).blur(function(e) {
        	var jElement = jQuery(this);
        	if (jElement.val() == '') {
        		jElement.val(jElement.attr('defaultFieldValue'));
        	}
        });
        
        jQuery('form#signin .sidebarInput').keypress(function (e) {
            
        	var     jElement = jQuery(this);
            var 	newJElement;
            var 	transferValue = String.fromCharCode(e.which);
            Log.debug('SidebarPanel.KEYPRESS '+e.which);
            if (jElement.attr('type') == 'text' && jElement.attr('name') == 'password') {
                jElement.after('<input type="password" class="input sidebarInput" id="password" name="password">');
                e.preventDefault();
                newJElement = jQuery('#password', 'form#signin');
                newJElement.focus();
                
                if (e.which == 0) {
                	jQuery('input#keepSignedIn').focus();
                } else {
                	//only assign the new character if it is not a tab
                	newJElement.val(transferValue);
                }
                
                newJElement.keypress(function(e){
                    if (e.which == 13) {
                    	//Log.debug('SidebarPanel.KEYPRESS HIDDEN PASSWORD, element length: '+jQuery('a#signin').length);
                        //jQuery('a#signin').click(); does not work in IE7
                        that.handleClick('#signin');
                    }
                    jQuery('div.sidebar .warningColor').hide();
                    jQuery('div#sidebarPanel #badCredentials').hide();
                }).focus(function(e) {
                	var jElement = jQuery(this);
                	if (jElement.val() == jElement.attr('defaultFieldValue')) {
                		jElement.val('');
                	}
                }).blur(function(e) {
                	var jElement = jQuery(this);
                	if (jElement.val() == '') {
                		jElement.val(jElement.attr('defaultFieldValue'));
                	}
                });
                
                jElement.remove();
            }
            
            if (e.which == 13)
            {
            	//Log.debug('SidebarPanel.KEYPRESS ALL FIELDS, element length: '+jQuery('a#signin').length);
            	//jQuery('a#signin').click(); does not work in IE7
            	that.handleClick('#signin');
            }
            jQuery('div.sidebar .warningColor').hide();
        });       
        
        // Handle click on Create New Project button
	    var DELAY = 700, clicks = 0, timer = null;
        jQuery('#createNewProjectDiv', this.element).text('Create New Project');
	    jQuery('#createNewProjectDiv', this.element).click(function(event){
	    	clicks++;  //count clicks

	        if(clicks === 1) {

	            timer = setTimeout(function() {
	            	that.fireEvent(SidebarPanel.LAUNCH_EDITOR_NEW);
	                //alert("Single Click");  //perform single-click action    
	                clicks = 0;             //after action performed, reset counter

	            }, DELAY);

	        } else {
	        	that.fireEvent(SidebarPanel.LAUNCH_EDITOR_NEW);
	            clearTimeout(timer);    //prevent single-click action
	            //alert("Double Click");  //perform double-click action
	            clicks = 0;             //after action performed, reset counter
	        }
	    });
	    
	    
	    jQuery('#createNewProjectDiv', this.element).dblclick(function(event){
	    	event.preventDefault();
	    });
	    
	    jQuery('#transferIndicator').toggle(false);
	    
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();

    }
    this.initializationComplete = function ()
    {
           this.fireEvent(SidebarPanel.INITIALIZED);
    }
    this.displayMyStuff = function (type, list, size)
    {   
    	var that = this;
    	
        switch (type)
        {
        case 'zapp':
            var html = '';
            jQuery('div#dashboardMyStuff span#zappTotal').text(size);
            var zapps = list;
            max = zapps.length > 9 ? 9 : zapps.length;
            for (i = 0; i < max; i++)
            {
            	var url = IdType.detailUrl(zapps[i].playerId, IdType.APN_UUID);

                switch (i % 3) {
                    case 0:
                        if (i != 0)
                        {
                            html += '</tr>';
                        }
                        html += '<tr><td style="height: 7px;" colspan="3"></td></tr>';
                        html += '<tr><td class="ttEnable" id="index_' + i + '"><a href="'+url+'"><img id="index_' + i + '" width="50" src="'+zapps[i].icon+'"></a></td>'
                        break;
                    case 1:
                        html += '<td class="ttEnable" id="index_' + i + '" align="center"><a href="'+url+'"><img id="index_' + i + '" width="50" src="'+zapps[i].icon+'"></a></td>'
                        break;
                    case 2:
                        html += '<td class="ttEnable" id="index_' + i + '" align="right"><a href="'+url+'"><img id="index_' + i + '" width="50" src="'+zapps[i].icon+'"></a></td>';
                        break;
                }
            }
            html += '</tr>';
            jQuery('div.sidebar div#dashboardMyStuff table#myZappsTable tr').not('.permanentRow').remove();
            jQuery('div.sidebar div#dashboardMyStuff table#myZappsTable tr:last').after(html);

            jQuery('div.sidebar table#myZappsTable td.ttEnable').hover(
                function(event) {
                	if(zapps[event.target.id.substring(6)] != undefined){
                		var name = makeSafeString(zapps[event.target.id.substring(6)].publishedName);
                        jQuery('.tooltip span').text(name);
                	}
                    
                   // alert(event.clientX + ' :: ' + event.clientY);
                    jQuery('.tooltip').css('left', (event.pageX + 15) + 'px').css('top', (event.pageY + 9) + 'px').show();
                },
                function (event) {
                    jQuery('.tooltip').hide();
                }
            ).mousemove(function(event) {
                jQuery('.tooltip').css('left', (event.pageX + 15) + 'px').css('top', (event.pageY + 9) + 'px');
            });


            break;
        case 'project':
        	var html = '';
            jQuery('div#dashboardMyStuff span#projectTotal').text(size);
            var projects = list;
            max = projects.length > 9 ? 9 : projects.length;
            for (i = 0; i < max; i++) {
                var project = projects[i];
                var lastUpdate = '';
                if (project.editedDateTime != undefined) {
                    var d = new Date(project.editedDateTime);
                    lastUpdate = (d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear();
                }

                html += '<tr>';
                html += '   <td width="135px" class="title"><div class="name"><a class="myproject" href="/#project_'+project.projectId+'">'+this.name(project.projectName)+'</a></div></td>';
                html += '   <td class="lastUpdate">'+lastUpdate+'</td>';
                html += '</tr>';
            }
            
            jQuery('div.sidebar div#dashboardMyStuff table#myProjectsTable tr').not('.permanentRow').remove();
            jQuery('div.sidebar div#dashboardMyStuff table#myProjectsTable tr#myProjectsLink').after(html);
            jQuery('div.sidebar div#dashboardMyStuff a.myproject').click(function (event) {
                var href = this.hash;
                //EditorPlayer.launchEditor(href.substr(href.indexOf('_')+1));
                var cfg = {};
                cfg.id = href.substr(href.indexOf('_')+1);
                cfg.bShowHistory = false;
                cfg.projectName = "";
                that.fireEvent(SidebarPanel.LAUNCH_EDITOR, cfg);
                return false;
            });

            
            
            break;
        case 'gadget':
            var html = '';
            jQuery('div#dashboardMyStuff span#gadgetTotal').text(size);
            // Only display number of gadgets and "show all".
            // var gadgets = list;
            // var max = gadgets.length > 9 ? 9 : gadgets.length;
            // for (i = 0; i < max; i++)
            // {
            //     var gadget = gadgets[i];
            // 
            //     html += '<tr>';
            //     html += '   <td class="title"><span class="name"><a href="#/detail?'+gadget.id+'">'+gadget.name+'</a></span></td>';
            //     html += '   <td class="title">'+gadget.version+'</td>';
            //     html += '</tr>';
            // }
            jQuery('div.sidebar div#dashboardMyStuff table#myGadgetsTable tr').not('.permanentRow').remove();
            // jQuery('div.sidebar div#dashboardMyStuff table#myGadgetsTable tr:last').after(html);
            break;
        default:
            break;
        }
    }
    this.displayDashboard = function (dashboard)
    {
        //Log.debug('SidebarPanel.displayDashboard - dashboard:'+dashboard);
        if (!Credentials.authenticated())
            return undefined;
        var dashboardId = 'dashboardMyStuff';
        var tab = 'myStuff';
        switch (dashboard)
        {
        case 'account':
            dashboardId = 'dashboardAccount';
            tab = dashboard;
            break;
        case 'administration':
            dashboardId = 'dashboardAdministration';
            tab = dashboard;
            break;
        case 'myStuff':
        default:
            break;
        }
        jQuery('#sidebarPanel div.dashboard').hide();
        jQuery('#sidebarPanel #'+dashboardId).show();
        return tab;
    }
    this.removeDashboardItems = function ()
    {
        jQuery('div#dashboardMyStuff div.dashboardItem div.zapps a').not('a.button').remove();
        jQuery('div#dashboardMyStuff div.dashboardItem ul li').remove();
    }
    this.displayHeadlines = function (content)
    {
        //Log.debug('SidebarPanel.displayHeadlines - content:'+content);
        jQuery('div.headlinesContainer div.headlines').hide();
        switch (content)
        {
        case 'plansAndPricing':
        case 'join':
            jQuery('div.headlinesContainer div.headlines.join').show();
            break;
        default:
            jQuery('div.headlinesContainer div.headlines.visitor').show();
            break;
        }
    }
    this.onLogin = function ()
    {
        jQuery('#sidebarPanel div#dashboardLogout').show();
        jQuery(jQuery('#sidebarPanel div.dashboard.logout')[0]).show();
        jQuery('#sidebarPanel div.dashboard.login').hide();
        jQuery('div#sidebarPanel #badCredentials').hide();
        jQuery('div#sidebarPanel div.headlinesContainer').hide();
        if(Credentials.accountTypeTag() == "collector"){
        	jQuery('#shareProjectItems', this.element).hide();
        }else{
        	jQuery('#shareProjectItems', this.element).show();
        }
    };
    
    this.onRefresh = function ()
    {
	    var type = Credentials.accountTypeTag();
	    var editorAllowed = (type == 'pro' || type == 'creator');
	    
	    jQuery('#createNewProjectDiv').toggle(editorAllowed);
	    
        jQuery('.gadgetTable').toggle(editorAllowed);
	    if(Credentials.accountTypeTag() == "collector"){
        	jQuery('#shareProjectItems', this.element).hide();
        }else{
        	jQuery('#shareProjectItems', this.element).show();
        }
    };
    
    this.updatePendingTransfers = function(numTransfers) {
    	var that = this;
    	var pendingTransferLabel = '';
    	// Handle display and content of Pending Transfers button
	    jQuery('#transferIndicator').toggle(numTransfers > 0);
	    
	    if (numTransfers > 0) {
	    	jQuery('#projectShareRequestsDiv').unbind('click').click(function(event){
	    		that.handleClick('#account?incomingTransfers');
	    	});
	    	var pendingTransferLabel1 = ' Receive ';
	    	var pendingTransferLabel2 = ' Project';
	    	if (numTransfers > 1) {
	    		pendingTransferLabel2 = pendingTransferLabel2 + 's';
	    	}	
	    	
	    	jQuery('#projectShareRequestsDiv').html(pendingTransferLabel1 + numTransfers + pendingTransferLabel2);
	    }	    
    };
    
    this.onLoginBadCredentials = function ()
    {
        jQuery('div.sidebar .warningColor').show();
    }
    this.onLogout = function ()
    {
        jQuery('#sidebarPanel div#dashboardLogout').hide();
        jQuery('#sidebarPanel div.dashboard.logout').hide();
        jQuery('#sidebarPanel div.dashboard.login').show();
        this.removeDashboardItems();
        jQuery('div#sidebarPanel div.headlinesContainer').show();
    }
    this.clearSignIn = function (o)
    {
        jQuery('div.sidebar div.dashboard.login form#signin :input', this.element).val('');
        jQuery('.warningColor', this.element).hide();
    }
}
SidebarPanel = new Class(new SidebarPanel());
SidebarPanel.HREF_CLICKED = 'SidebarPanel.HREF_CLICKED';
SidebarPanel.LAUNCH_EDITOR = 'SidebarPanel.LAUNCH_EDITOR';
SidebarPanel.LAUNCH_EDITOR_NEW = 'SidebarPanel.LAUNCH_EDITOR_NEW';
SidebarPanel.INITIALIZED = 'SidebarPanel.INITIALIZED';
SidebarPanel.LOST_PASSWORD = 'SidebarPanel.LOST_PASSWORD';
SidebarPanel.DISPLAY_CHANGE = 'SidebarPanel.DISPLAY_CHANGE';
SidebarPanel.NAME = 'SidebarPanel';
// ScriptLoader.loaded(SidebarPanel.NAME);
