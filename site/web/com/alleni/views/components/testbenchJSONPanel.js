var TestbenchJSONPanel = function()
{
    this.Extends = UIComponent;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function(parent)
    {
        this.parent(parent);
        
        var that = this;
        var o = {
            filename: 'testbenchJSONPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(TestbenchJSONPanel.INITIALIZED);
        }
    }
    this.clear = function ()
    {
        jQuery('div#jsonPanel div').html('');
    }
    this.display = function (o, mode)
    {
        function escapeAngleBrackets(s)
        {
            return (s) ? s.replace(/</g,'&lt;').replace(/>/g,'&gt;') : s;
        }
        var id = '';
        var text = '';
        switch (mode)
        {
         case 'url':
            text = o.url;
            break;
         case 'out':
            text = ObjectToHTML({object: o.out, delimiter: ''});
            text = this.concealPasswords(text);
            break;
         case 'in':
            text = ObjectToHTML({object: o.json, delimiter: ''});
            break;
         case 'raw':
            text = JSON.encode(o);//.replace(/\\"/g, '"');
            break;
         default:
            Log.error('TestbenchJSONPanel.display - unknown mode:'+mode);
            break;
        }
        jQuery('div#jsonPanel #'+mode).html(text ? text : '');
    }
    this.concealPasswords = function (html)
    {
        var re = new RegExp('password: .*?<br />', 'g');
        var html = html.replace(re, 'password: * concealed *<br />');
        return html;
    }
}
TestbenchJSONPanel = new Class(new TestbenchJSONPanel());
TestbenchJSONPanel.NAME = 'TestbenchJSONPanel';
TestbenchJSONPanel.INITIALIZED = 'TestbenchJSONPanel.INITIALIZED';
ScriptLoader.loaded(TestbenchJSONPanel.NAME);
