var ListPanel = function()
{
    this.Extends = UIComponent;
    this.group = 'visitor';
    this.instance = 'list';
    this.type = 'zapp';
  
    this.browseIndex = 0;
    this.featuredIndex = 0;
    
    this.html = undefined;
    
    this.initialize = function (parent)
    {
        this.parent(parent);
        
        var that = this;
        var panel = {
            filename: 'listPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(panel);
    }
    
    this.processFragment = function (fragment) 
    {
        this.set('html', fragment);
        this.html = fragment;
        
        var that = this;
        
        jQuery('div.tab a', this.element).unbind('click').click(function () {
        	var target = this.hash.substring(1).split('?');
    		var projectType = target[1].split('&')[1];
    		
    		if (projectType != this.type) {
    			that.type = projectType;
    			that.toggleLinkAvailability(false);
    			setTimeout(function() {
    				that.toggleLinkAvailability(true);
    			}, 1000);
    			that.fireEvent(ListPanel.TAB_CLICKED, {group: that.group, type: projectType});
    			that.updateTabs(projectType);
    		}
        });
        
        this.updateTabs(this.type);
        
        this.fireEvent(
          	  ListPanel.INITIALIZED
          	, {	
  				  group: this.group
  				, type: this.type
  				, browseStart: this.browseIndex
  				, featuredStart: this.featuredIndex
          	}
        );
    }

    this.display = function(displayArguments) {
    	var arrArgs = displayArguments.split('&');
    	var fireDisplayEvent = false;
    	
    	if (arrArgs[0] != this.group || arrArgs[1] != this.type) {
    		fireDisplayEvent = true;
    	}

    	this.group = arrArgs[0];
    	this.type = arrArgs[1];
    	
    	this.updateTabs(this.type);

    	if (fireDisplayEvent) {
    		this.fireEvent(ListPanel.DISPLAY, {group: this.group, type: this.type});
    	}
    }

    this.getGroup = function() {
    	return this.group;
    }
    
    this.getType = function() {
    	return this.type;
    }

    this.getHtml = function() {
    	return this.html;
    }
    
    this.toggleLinkAvailability = function(isClickable) {
    	jQuery('div.tab a').toggle(isClickable);
    	jQuery('div.tab span.nonClickable').toggle(!isClickable);
    }
    
    this.updateTabs = function (projectType) {
        var group = this.group;
    	
        var showMemberContent = (group == 'member');

        jQuery('div#featuredPanel', this.element).toggle(!showMemberContent);
        
        jQuery('div#myProjects', this.element).toggle(showMemberContent);
        jQuery('div#myZapps', this.element).toggle(showMemberContent);
        jQuery('div#myGadgets', this.element).toggle(showMemberContent);
        jQuery('div.tab.visitor', this.element).toggle(!showMemberContent);
        
        jQuery('div.tab.' + group, this.element).removeClass('selected');
        jQuery('div.tab.' + group, this.element).addClass('unselected');
        
        jQuery('div.tab.' + group + '.' + projectType, this.element).removeClass('unselected');
        jQuery('div.tab.' + group + '.' + projectType, this.element).addClass('selected');
    }
    
    this.validGroup = function (group)
    {
        var valid = true;
        switch (group)
        {
        case 'member':
        case 'visitor':
            break;
        default:
            valid = false;
            Log.error('ListPanel.validGroup - unknown group:'+group);
            break;
        }
        return valid;
    }
    this.validType = function (type)
    {
        var valid = true;
        switch (type)
        {
        case 'zapp':
        case 'gadget':
        case 'project':
            break;
        default:
            valid = false;
            Log.error('ListPanel.validType - unknown type:'+type);
            break;
        }
        return valid;
    }
    
    this.injectView = function(divId, fragment){
    	//Log.warn('List Panel injecting view content for div ID: ' + divId);
    	jQuery('div#' + divId, this.element).html(fragment);
    }
}
ListPanel = new Class(new ListPanel());
ListPanel.NAME = 'ListPanel';
ListPanel.INITIALIZED = 'ListPanel.INITIALIZED';
ListPanel.TAB_CLICKED = 'ListPanel.TAB_CLICKED';
ListPanel.DISPLAY = 'ListPanel.DISPLAY';
ScriptLoader.loaded(ListPanel.NAME);
