var HeaderPanel = function()
{
    this.Extends = UIComponent;
    this.menuItems = [];
    this.loginDialog = null;
    
    this.initialize = function(parent)
    {
        this.parent(parent);
        
        var that = this;
        var o = {
            filename: 'headerPanel.html',
            success: function (fragment, status) {
                //Log.debug('HeaderPanel.initialize - initialized');
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        
        this.initializeChildren();
        this.childrenInitialized();
        this.initializationComplete();
    }
    this.initializationComplete = function ()
    {
    	
    	this.configureMenu(Application.MENU_MODE_VISITOR);
    	
    	this.processLinks();
        this.processTabs();

        
        this.fireEvent(HeaderPanel.INITIALIZED);
    }
    this.processLinks = function () {
        var that = this;
        var onClickLinkItem = function (event) {
                var href = this.hash;
                if (href.indexOf('#') >= 0)
                {
                    event.preventDefault();
                    var link = href.substring(1);
                    switch (link)
                    {
                    case 'editor': //TODOD acavan - move to mediator
                        if (Credentials.authenticated())
                        {
                            //EditorPlayer.launchEditor()
                        }
                        else
                            that.fireEvent(HeaderPanel.LINK_ITEM_SELECTED, 'plansAndPricing');
                        break;
                    default:
                        that.fireEvent(HeaderPanel.LINK_ITEM_SELECTED, href.substring(1));
                        break;
                    }
                }
        }
        jQuery('div.headerLinks a').click(onClickLinkItem);

        jQuery('div.header div.headerTabsText.logout div.signout a').unbind('click').click(function(){
            that.fireEvent(HeaderPanel.LOGOUT);
            return false;
        });
    }
    
    this.setMenuState = function(payload) {
    	var id = payload.menuItem;
    	var args = payload.menuArgument;
    	
    	for (var i = 0; i < this.menuItems.length; i++)
        {
        	var menuItem = this.menuItems[i];
        	if(menuItem.id != id || ( args != "" && (id == 'list' && args.indexOf('member') != -1)))
    		{
        		jQuery('a#'+menuItem.id+' img').attr("src", menuItem.image);
        		jQuery('a#'+menuItem.id+' img').data('currentImage', menuItem.image);
    		}else{
    			jQuery('a#'+menuItem.id+' img').attr("src", menuItem.imageSelected);
        		jQuery('a#'+menuItem.id+' img').data('currentImage', menuItem.imageSelected);
    		}
        }
    }
    
    this.processMenu = function ()
    {
        //Log.debug('HeaderPanel.processMenu - this.menuItems.length:'+this.menuItems.length);
        var that = this;
        var onClickMenuItem = function (event) {
            var a = jQuery(this);
            var id = a.attr('id');
            //Log.debug('HeaderPanel.processMenu.onClickMenuItem - id:'+id+', image:'+a.data('image'));
            that.fireEvent(HeaderPanel.MENU_ITEM_SELECTED, id);
          
           //that.setMenuState(payload.menuItem = id);
        }
        var hoverFunction = function (image)
        {
            return function () {
            	if(image != null)
            	{
            		jQuery(this).attr('src', image);
            	}else{
            		jQuery(this).attr('src', jQuery(this).data("currentImage"));
            	}
                
            }
        }
        for (var i = 0; i < this.menuItems.length; i++)
        {
            var menuItem = this.menuItems[i];
            
            var s = '<a id="'+menuItem.id+'" href="'+menuItem.href+'" '+ ((menuItem.internal)?'':'target="_blank"')+'><img src="'+menuItem.image+'" alt="'+menuItem.name+'" /></a>';
            jQuery('div.headerMenu', this.element).append(s);
            var a = jQuery('div.headerMenu a#'+menuItem.id);
            a.click(onClickMenuItem);
            a.data('image', menuItem.image);
            jQuery('img', a).data('currentImage', menuItem.image);
            jQuery('img', a).hover(hoverFunction(menuItem.imageHover), hoverFunction());
        }
    }
    this.processTabs = function ()
    {
        var that = this;
        var onClickTabItem = function (event) {
            event.preventDefault();
            var href = this.hash.substring(1);
            Log.debug('HeaderPanel.processTabs - href:'+href);
            that.highlightTab(href);
            // jQuery('div.header div.headerTabs a').removeClass('selected');
            // jQuery(this).addClass('selected');
            that.fireEvent(HeaderPanel.TAB_ITEM_SELECTED, href);
        }
        jQuery('div.header div.headerTabs a').click(onClickTabItem);
    }
    this.onLoginLogout = function (mode)
    {
        //Log.debug('HeaderPanel.displayLoginLogout - mode:'+mode);
        jQuery('div.header div.headerLinks a.login').toggle(mode != 'login');
        jQuery('div.header div.headerLinks a.logout').toggle(mode != 'logout');
        jQuery('div.header div.headerTabs div.login').toggle(mode != 'login');
        jQuery('div.header div.headerTabs div.logout').toggle(mode != 'logout');
        jQuery('div.header div.headerTabsText.login').toggle(mode != 'login');
        jQuery('div.header div.headerTabsText.logout').toggle(mode != 'logout');

        if (mode == 'login')
        {
            //jQuery('div.header #email').text(Credentials.email());
            jQuery('div.header #username').text(Credentials.displayName());
            
            // dwh DEV-4666: if not admin, then hide login tab
            if (false == Credentials.systemAdministrator()) {
            	jQuery('div.header div.headerTabs #tabAdministration').hide();
            }
            this.toggleMasterLogo();
        }
        else if (mode == 'logout')
        {
            //jQuery('div.header #email').text('');
            jQuery('div.header #username').text('');
            
            //RESET LOGO TO DEFAULT HERE
            jQuery('img#masterLogo').attr("src", "com/alleni/images/logo.png");
        }

    }
    
    this.toggleMasterLogo = function (){
    	if (Credentials.accountTypeTag() == "creator"){
        	jQuery('img#masterLogo').attr("src", "com/alleni/images/logo-creator.png");
        	jQuery('a#tabMyStuff').attr("href", "#list?member&project");
        }else if(Credentials.accountTypeTag() == "collector"){
        	jQuery('img#masterLogo').attr("src", "com/alleni/images/logo-collector.png");
        	jQuery('a#tabMyStuff').attr("href", "#list?member&zapp");
        }else if(Credentials.accountTypeTag() == "pro"){
        	jQuery('img#masterLogo').attr("src", "com/alleni/images/logo-professional.png");
        	jQuery('a#tabMyStuff').attr("href", "#list?member&project");
        } 
    }
    
    this.clearMenu = function ()
    {
        //Log.debug('HeaderPanel.clearMenu');
        this.menuItems = [];
        jQuery('div.headerMenu a', this.element).remove();
    }

    this.highlightTab = function (tab)
    {
        //Log.debug('HeaderPanel.highlightTab - tab:'+tab);
        var selectors = {
            myStuff: 'tabMyStuff',
            account: 'tabAccount',
            administration: 'tabAdministration'
        }
        jQuery('div.header div.headerTabs a').removeClass('selected');
        jQuery('div.header div.headerTabs a#'+selectors[tab]).addClass('selected');
    }
    
    this.onLogin = function ()
    {
        this.configureMenu(Application.MENU_MODE_MEMBER);
    }

    this.onLogout = function ()
    {
        this.configureMenu(Application.MENU_MODE_VISITOR);
    }
    
    this.configureMenu = function (mode)
    {
        this.clearMenu();
        this.menuItems = Application.configureMenu(mode);
        this.processMenu();
    }
}
HeaderPanel = new Class(new HeaderPanel());
HeaderPanel.INITIALIZED = 'HeaderPanel.INITIALIZED';
HeaderPanel.LINK_ITEM_SELECTED = 'HeaderPanel.LINK_ITEM_SELECTED';
HeaderPanel.MENU_ITEM_SELECTED = 'HeaderPanel.MENU_ITEM_SELECTED';
HeaderPanel.TAB_ITEM_SELECTED = 'HeaderPanel.TAB_ITEM_SELECTED';
HeaderPanel.LOGIN = 'HeaderPanel.LOGIN';
HeaderPanel.LOGOUT= 'HeaderPanel.LOGOUT';
