var ShareDialog = function()
{
    this.Extends = UIComponent;
    this.dialog = undefined;
    this.itemId = undefined;
    this.userAccountType = undefined;
    this.playerId = undefined;
    this.values = {};
    this._plan = undefined;
    this.usernameCache = [];
    this.emailCache = [];
    this.embedSizeSrc = [];
    this.step2ValidationAttempt = false;
    this.fragmentProcessed = false;
    this.aspectLocked = false;
    this.aspectRatio = undefined; //width/height aspect ration when embed aspect is locked.
    
    this.projectMetaDataProvided = false;
    
    this.STATE_PRIVACY = "setPrivacyLevel";
    this.STATE_SHARE_DEFAULT = "shareZappDefault";
    this.STATE_SHARE_SELECTED = "shareZappSelected";

    this.RADIO_GUEST = "radioGuest";
    this.RADIO_PRIVATE = "radioPrivate";
    this.RADIO_PUBLIC = "radioPublic";
    this._privacySelected = 2;
    this._priceSelected = 0;
    this._embedSelected = false;
	this._shareSelected = 0;
    this.PANEL_SECURITY = 0;
    this.PANEL_SHARE = 1;
    //
    //
    this.SHARE_DEFAULT = 0;
    this.SHARE_GUEST = 1;
    this.SHARE_EMBED = 2;
    this.SHARE_LMS = 3;
    this.SHARE_LINK = 4;
    this.SHARE_EMAIL = 5;
    this.SHARE_FACEBOOK = 6;
    this.SHARE_LINKEDIN = 7;
    this.SHARE_TWITTER = 8;
    
    this._config = undefined;

    this.TXT_GUEST = "guest";
    this.TXT_EMBED = "embed";
    this.TXT_LMS = "lms";
    this.TXT_LINK = "link";
    this.TXT_EMAIL = "email";
    this.TXT_FACEBOOK = "facebook";
    this.TXT_LINKEDIN = "linkedin";
    this.TXT_TWITTER = "twitter";

    this.embedBtnRemove;
    this.embedBtnAdd;
    
	this.sharingLinks = new SharingLinks();
   
    this.initialize = function (viewName)
    {
    	// same as calling super
    	this.parent(viewName);
    }
    this.initializationComplete = function ()
    {
    	
    }
    
    this.config = function(config)
    {
    	/*
    	 * values in "args" -
    	 * config.panel:int
    	 * 		0 = Security Panel
    	 * 		1 = Share Panel
    	 * config.privacyLevel:int
    	 * 		0 = Use what the server gives you
    	 * 		1 = override with Guest List level
    	 * 		2 = override with Hidden(NOT in shopp)
    	 * 		3 = override with Shopp (Public)
    	 * config.shareDefault:int
    	 * 		0 = Default
    	 * 		1 = Guest List
    	 * 		2 = Embed
    	 * 		3 = LMS
    	 * 		4 = Direct Link
    	 * 		5 = Email
    	 * 		6 = Facebook
    	 * 		7 = Linked In
    	 * 		8 = Twitter
    	 * config.projectId:string 
    	 * config.project:Object (if null, sharePanel request came from editor and 
    	 * 							this dialog will request the project detail; 
    	 * 							if not null, sharePanel request came from the 
    	 * 							detailPanel and project detail was passed through)
    	*/
	    
    	jQuery('div#loadingSharePanel', this.element).show();
    	jQuery('div#activeSharePanel', this.element).hide();

    	if(config.project == undefined) {
    		// TODO: asynch call leaves panel state screwy for a second or two
    		// TODO: DWH to refactor this into a single proxy method on ZappGadgetProjectProxy. He knows what's up.
    		this.showUpdateAnimation();
    		this.fireEvent(ShareDialog.LOAD_DETAIL, {projectId: config.projectId, idType: 'pid'});
    		return;
    	}else{
    		this.hideUpdateAnimation();
    	}
    	
	    var firstTime = (this._config == undefined || this._config.project == undefined);
    	this._config = config;
    	
    	if (this._config.project.type == 'zapp') this._config.project.displayType = 'App';
    	else this._config.project.displayType = this._config.project.type.substr(0,1).toUpperCase() + this._config.project.type.substr(1).toLowerCase();
    	
		if(firstTime) {
			this.configSelected();
		}
    	
    	this.userAccountType = Credentials.accountTypeTag();

		if(this._config.project.userAccessPrivs != undefined){
			if(!this._config.project.publisher) {
    		    this.userAccountType = "notOwner";
    		    jQuery('#btnEdit').hide();
    	    } else {
    		    jQuery('#btnEdit').show();
    	    }
		}
		
		// zapp or gadget
		var type = (this._config.project.type == 'zapp') ? 'app' : 'gadget';
		jQuery("span.appType").each(function(){
			jQuery(this).html(type);
		}); 
		var typeUpper = (type=="app")?"App":"Gadget";
		jQuery("span.appTypeUpper").each(function(){
			jQuery(this).html(typeUpper);
		}); 
		this.setUpPanel();

    }

	this.configSelected = function() {
		if(this._config.privacyLevel == 0){
			this._privacySelected = this._config.project.hideInStoreList ? (this._config.project.inviteOnly ? 1 : 2) : 3;
		}else{
			this._privacySelected = this._config.privacyLevel;
		}
	    this._priceSelected = this._config.project.price;
	    this._embedSelected = this._config.project.licenseTypeId == 6;
		this._panelSelected = this._config.panel;
    	this._shareSelected = this._config.shareDefault;
	}
    
    this.setUpPanel = function()
    {
    	jQuery('div#activeSharePanel', this.element).show();
		jQuery('div#loadingSharePanel', this.element).hide();
    	this.processFragment();
    	var tinyUrlParams;
    	if (this._config.project.owned) {
    		tinyUrlParams = {
    				projectId: this._config.project.playerId,
    				idType:	IdType.APN_UUID
    		};
    	} else {
    		tinyUrlParams = {
    			projectId: this._config.project.urlName,
    			idType: IdType.URL_NAME
    		};
    	};
    	
    	
    	this.fireEvent(ShareDialog.LOAD_TINYURL, tinyUrlParams);
    	switch(this._panelSelected){
    	case this.PANEL_SECURITY:
    		this.onStatePrivacy();
    		break;
    		
    	default:
    	case this.PANEL_SHARE:
    		this.onStateShare();
    		break;
    	}
    	
    }
    
    this.getShareType = function()
    {
    	switch(this._shareSelected)
		{
    		case this.SHARE_DEFAULT:
    			return "";
    			
    		case this.SHARE_GUEST:
    			return this.TXT_GUEST;
    			
    		case this.SHARE_EMBED:
    			return this.TXT_EMBED;
    			
    		case this.SHARE_LMS:
    			return this.TXT_LMS;
    			
    		case this.SHARE_LINK:
    			return this.TXT_LINK;
    			
    		case this.SHARE_EMAIL:
    			return this.TXT_EMAIL;
    			
    		case this.SHARE_FACEBOOK:
    			return this.TXT_FACEBOOK;
    			
    		case this.SHARE_LINKEDIN:
    			return this.TXT_LINKEDIN;
    			
    		case this.SHARE_TWITTER:
    			return this.TXT_TWITTER;
		}
    }
    
    this.setShareType = function (enumType) {
    	switch(enumType) {
    	case this.TXT_GUEST:
    		this._shareSelected = this.SHARE_GUEST;
    		break;
    		
    	case this.TXT_EMBED:
    		this._shareSelected = this.SHARE_EMBED;
    		break;
    		
    	case this.TXT_LMS:
    		this._shareSelected = this.SHARE_LMS;
    		break;
    		
    	case this.TXT_LINK:
    		this._shareSelected = this.SHARE_LINK;
    		break;
    		
    	case this.TXT_EMAIL:
    		this._shareSelected = this.SHARE_EMAIL;
    		break;
    		
    	case this.TXT_FACEBOOK:
    		this._shareSelected = this.SHARE_FACEBOOK;
    		break;
    		
    	case this.TXT_LINKEDIN:
    		this._shareSelected = this.SHARE_LINKEDIN;
    		break;
    		
    	case this.TXT_TWITTER:
    		this._shareSelected = this.SHARE_TWITTER;
    		break;
    	}
    }
    
    this.onStatePrivacy = function() {
	    this._panelSelected = this.PANEL_SECURITY;
    	jQuery('.buttonControls', this.element).hide();
    	jQuery('.buttonControlsHR', this.element).hide();
    	jQuery('.shareInfo', this.element).hide();
    	jQuery('.setPrivacyLevel', this.element).show();
		jQuery('.icons', this.element).hide();
		jQuery('.shareInfo', this.element).hide();
		jQuery('.sharePluginContainer', this.element).hide();    		
		jQuery('.privacyButtons', this.element).show();
		jQuery('.shareZappDefault', this.element).hide();
		jQuery('.txtDirections', this.element).show();
		jQuery('.txtDirectionsHR', this.element).show();

		this.fireEvent(ShareDialog.RETITLE, 'Select Privacy');

	    this.showCorrectPrivacyOptions();
    }

	this.showCorrectPrivacyOptions = function() {

		jQuery('.radioPrivacy', this.element).attr('checked', "");

	    var that = this;
		var iconQuery = function (privValue, shareValue) {
			if(shareValue == undefined) {
				return jQuery('#div'+privValue+'Icons .privacyIcon', that.element);
			} else {
				return jQuery('#div'+privValue+'Icons .privacyIcon.'+shareValue, that.element);
			}
		};
		var enableRadioButtons = function (privValue, anyAvailable) {
			// enable the radio button for this privacy if any access is available
			if(anyAvailable) {
				jQuery('#radio'+privValue, that.element).removeAttr('disabled');
			} else {
				jQuery('#radio'+privValue, that.element).attr('disabled', 'disabled');
			}
		};
	    this.sharingLinks.setVisibility(this.sharingLinks.PRIV_GUEST, 'Guest', this.userAccountType, this._config.project.projectAccountTypeName, iconQuery, enableRadioButtons,null,this._config.project.userAccessPrivs);
	    this.sharingLinks.setVisibility(this.sharingLinks.PRIV_HIDDEN, 'Private', this.userAccountType, this._config.project.projectAccountTypeName, iconQuery, enableRadioButtons,null,this._config.project.userAccessPrivs);
	    this.sharingLinks.setVisibility(this.sharingLinks.PRIV_SHOP, 'Public', this.userAccountType, this._config.project.projectAccountTypeName, iconQuery, enableRadioButtons,null,this._config.project.userAccessPrivs);

	    jQuery('.sellPrice', this.element).hide();
	    
	    if(this._embedSelected)
    	{
	    	jQuery('#allowEmbed', this.element).attr('checked', true);
    	}else{
    		jQuery('#allowEmbed', this.element).attr('checked', false);
    	}
	    
	    jQuery('#sellingPrice', this.element).val(AIUtils.formatCurrency(this._priceSelected));
	    
	    if(this._priceSelected == 0) {
	    	jQuery('#sellingPrice', this.element).prop('disabled', true);
	    }else{
	    	jQuery('#sellingPrice', this.element).prop('disabled', false);
	    }
	    
		switch(this._privacySelected) {
		case this.sharingLinks.PRIV_GUEST:
			jQuery('#radioGuest', this.element).attr('checked', "checked");
			jQuery('#radioGuest', this.element).focus();
			break;

		case this.sharingLinks.PRIV_SERVER:						// ** What to do here ?  look into config.project ???!
		case this.sharingLinks.PRIV_HIDDEN:
			jQuery('#radioPrivate', this.element).attr('checked', "checked");
			jQuery('#radioPrivate', this.element).focus();
			break;

		case this.sharingLinks.PRIV_SHOP:
			jQuery('#radioPublic', this.element).attr('checked', "checked");
			jQuery('#radioPublic', this.element).focus();
			jQuery('.sellPrice', this.element).show();
			break;
		}
		
	}

	this.showCorrectTabIcons = function() {
		var that = this;
		var iconQuery = function (privValue, shareValue) {
			if(shareValue == undefined) {
				return jQuery('#shareTypes li a', that.element);
			} else {
				return jQuery('#shareTypes li.'+shareValue+' a', that.element);
			}
		};
		this.sharingLinks.setVisibility(this._privacySelected, null, this.userAccountType, this._config.project.projectAccountTypeName , iconQuery, undefined,this._config.project.type,this._config.project.userAccessPrivs);
		this.setShareLinks();
	}

	this.onStateShare = function() {
	    this._panelSelected = this.PANEL_SHARE;
		if(this._shareSelected == 0) {
			this.onStateShareDefault();
		} else {
			this.onStateShareSelected();
		}
	}

    this.onStateShareDefault = function() {
    	this.fireEvent(ShareDialog.RETITLE, "Share "+this._config.project.displayType);
    	jQuery('a.shareHref', this.element).removeClass('selected');
    	jQuery('.buttonControls', this.element).hide();
    	jQuery('.shareZappDefault', this.element).show();
    	jQuery('.setPrivacyLevel', this.element).hide();
    	jQuery('.shareInfo', this.element).toggle(this._config.project.publisher);
    	jQuery('.icons', this.element).show();
    	jQuery('.sellPrice', this.element).hide();
    	jQuery('.privacyButtons', this.element).hide();
    	jQuery('.txtDirections', this.element).hide();
    	jQuery('.txtDirectionsHR', this.element).hide();
    	jQuery('#txtApply', this.element).html('Done');
	    this.showCorrectTabIcons();
	    this.setShareLinkState();
    }
    
    this.onStateShareSelected = function() {
    	//jQuery('.arrowDown').rotate(45);
    	jQuery('.tabControls #btnTab1').click();
    	jQuery('#tab2 .guestAdd').val("From: "+Credentials.email());
	    this._panelSelected = this.PANEL_SHARE;
    	this.fireEvent(ShareDialog.RETITLE, "Share "+this._config.project.displayType);
    	var type = this.getShareType();
    	jQuery('.icons', this.element).show();
    	jQuery('.shareInfo', this.element).toggle(this._config.project.publisher);
    	jQuery('.buttonControls', this.element).show();
    	jQuery('.buttonControlsHR', this.element).show();
    	jQuery('.sellPrice', this.element).hide();
		jQuery('.setPrivacyLevel', this.element).hide();
        jQuery('a.shareHref', this.element).removeClass('selected');
        jQuery('a.shareHref[href="#' + type + '"]', this.element).addClass('selected');
        jQuery('div.sharePlugin', this.element).hide();
        jQuery('div #share_'+type, this.element).show();
        jQuery('input#emailTo', this.element).val('');
        jQuery('div #share_'+type, this.element).focus();
        jQuery('.sharePluginContainer', this.element).show();
		jQuery('.privacyButtons', this.element).hide();
		jQuery('.shareZappDefault', this.element).hide();
		jQuery('.txtDirections', this.element).hide();
		jQuery('.txtDirectionsHR', this.element).hide();
	    this.showCorrectTabIcons();
	    this.setShareLinkState();
		
		if(type == 'guest'){
				this.fireEvent(ShareDialog.LOAD_GUEST_LIST,this._config.project.id);
				jQuery('#addGuest').focus();
		}
		
		this.buildButtons(this._shareSelected);
    }
    
    this.setShareLinks = function() {
    	var that = this;
    	jQuery('div#shareDialog div#shareTypes a', this.element).unbind('click');
    	jQuery('div#shareDialog div#shareTypes a', this.element).click(function (event) {
            if (this.hash.indexOf('#/detail?') == 0) {
            	that.close();
                return true;
            }

            var name = this.href;
            var i = name.indexOf("#")+1;
            name = name.substr(i);

            that.buildButtons(that._shareSelected);
            jQuery('.shareZappDefault', that.element).hide();
            
            that.setShareType(name);
            that.onStateShareSelected();

            return false;
        });
    	jQuery('div#shareDialog div#shareTypes a._disabled', this.element).unbind('click');
    	jQuery('div#shareDialog div#shareTypes a._hidden', this.element).unbind('click');
    	jQuery('div#shareDialog div#shareTypes  a._hidden', this.element).click(function (event) {
            return false;
        });
    	jQuery('div#shareDialog div#shareTypes  a._disabled', this.element).click(function (event) {
            return false;
        });
    }
    
    this.setShareLinkState = function () {
    	var namename = (this._config.project.type=="zapp")?"app":"gadget";
    	var nameCap = (this._config.project.type=="zapp")?"App Shopp":"Gadget Shopp";
    	switch(this._privacySelected) {
    	case this.sharingLinks.PRIV_GUEST:
    		jQuery('#shareInfoText', this.element).text("Only the Guest List can see this "+namename+".");
    		break;
    	case this.sharingLinks.PRIV_HIDDEN:
    		jQuery('#shareInfoText', this.element).text("This "+namename+" is not in the "+nameCap+".");
    		break;
    	case this.sharingLinks.PRIV_SHOP:
    		jQuery('#shareInfoText', this.element).text("This "+namename+" is in the "+nameCap+".");
    		break;
    	}
    }
    
    this.buildButtons = function (type) {
    //	jQuery('.txtDirections').show();
    	jQuery('#btnCancel', this.element).hide();
    	jQuery('#btnApply', this.element).show();
    	jQuery('#btnDirections', this.element).show();
    	
    	switch(type) {
    	case this.SHARE_EMAIL:	
    		jQuery('#txtCancel', this.element).html('Cancel');
          	jQuery('#txtApply', this.element).html('Send Email');
          	jQuery('#btnCancel', this.element).show();
    		break;
    	case this.SHARE_LMS:				
          	jQuery('#txtApply', this.element).html('Package for LMS');
          	jQuery('#btnCancel', this.element).show();
    		jQuery('#txtCancel', this.element).html('Done');
          	break;
    	case this.SHARE_LINK:	
    	case this.SHARE_FACEBOOK:					
    	case this.SHARE_LINKEDIN:
    	case this.SHARE_TWITTER:
    	case this.SHARE_EMBED:	
          	jQuery('#txtApply', this.element).html('Done');
          	break;
          	
    	case this.SHARE_GUEST:
    		jQuery('#btnCancel', this.element).show();
    		jQuery('#txtCancel', this.element).html('Done');
          	jQuery('#txtApply', this.element).html('Email Guests');
          	break;
          	
    	default:
    		jQuery('#btnCancel', this.element).show();
    		if(this.userAccountType!="pro")
    		jQuery('.txtDirections', this.element).text("Upgrade to a Professional Account to activate the additional privacy and sharing options shown.");
    	}	
    }
    
    this.shrinkGuestList = function() {
    	//jQuery('#arrowTab1 .arrowDown', this.element).css('visibility', 'visible');
    	//jQuery('#arrowTab2 .arrowDown', this.element).css('visibility', 'hidden');
    	jQuery('#tab1').css('visibility', 'visible');
    	jQuery('#tab2').css('visibility', 'hidden');
    }
    
    this.expandGuestList = function() {
    	//jQuery('#arrowTab2 .arrowDown', this.element).css('visibility', 'visible');
    	//jQuery('#arrowTab1 .arrowDown', this.element).css('visibility', 'hidden');
    	jQuery('#tab2').css('visibility', 'visible');
    	jQuery('#tab1').css('visibility', 'hidden');
    }
    
    this.close = function () 
    {
    	this.fireEvent(ShareDialog.CLOSE);
    }
    
    this.processFragment = function ()
    {
        Log.debug('ShareDialog.processFragment - @top');
        //this.set('html', fragment);
        var that = this;
        
        jQuery('#btnEdit', this.element).click(function(event){
        	that.onStatePrivacy();
    		return false;
        })
        
        jQuery('#btnCancel', this.element).click(function(event){
        	that.close();
        	return false;
        })
        
        jQuery('#btnApply', this.element).unbind('click').click(function(event){
        	
        	switch(that._shareSelected){
	        	case that.SHARE_LMS:
	        		var type = "scorm";
	        		var filename = "ZebraPlayerPackage.zip";
	        		var url = "zephyr/player/package/"+type+"/"+that._config.project.playerId+"/"+filename;
	        		window.open(url,"LMS","",false);
	        		/*
	        		the url is /player/package/{type}/{playerId}/{filename}
	
	        		where
	        		  {type} is from the radio buttons and the only legal type right now is 'scorm'
	        		  {playerId} is the userAwarePurchaseId, the long UUID for the published item
	        		  {filename} is freeform, but should end in '.zip' for now lets use
	        		'ZebraPlayerPackage.zip'
	        		*/
	        		break;
	        	
	        	case that.SHARE_LINK:
	        		that.close();
	        		break;
	        		
	        	case that.SHARE_GUEST:
	        		that.sumbitGuestList(that.collectEmailsForSubmission(), jQuery('div#tab2 textarea#guestEmailMessage',that.element).val());
	        		break;
	        		
	        	case that.SHARE_LINKEDIN:
	        		that.close();
	        		break;
	        		
	        	case that.SHARE_TWITTER:
	        		that.close();
	        		break;
	        		
	        	case that.SHARE_EMAIL:
	        		var o = {};
	        		o.emailTo = jQuery('div#shareDialog div#share_email #emailTo', that.element).val();
	                o.emailFrom = jQuery('div#shareDialog div#share_email #emailFrom', that.element).val();
	                o.emailBody = jQuery('div#shareDialog div#share_email #emailBodyReadOnly', that.element).val();
	                o.emailBody += "<p/>";
	                o.emailBody += jQuery('div#shareDialog div#share_email #emailBody', that.element).val();
	                that.fireEvent(ShareDialog.EMAIL, o);
	        		break;
	        		
	        	case that.SHARE_EMBED:
	        		that.close();
	        		break;
	        		
	        	case that.SHARE_FACEBOOK:
	        		that.close();
	        		break;
	        		
	        	default:
	        		that.close();
	        		
        	}
        	return false;
        });
        
        jQuery('.radioPrivacy', this.element).click(function(event){
        	jQuery('.sellPrice', this.element).hide();
        	var id = jQuery(this).attr('id');
	        switch(id) {
		        case "radioGuest": 
		        	that._config.privacyLevel = that._privacySelected = that.sharingLinks.PRIV_GUEST;
		        	break;
		        case "radioPrivate": 
		        	that._config.privacyLevel = that._privacySelected = that.sharingLinks.PRIV_HIDDEN;
		        	break;
		        case "radioPublic": 
		        	that._config.privacyLevel = that._privacySelected = that.sharingLinks.PRIV_SHOP;
		        	jQuery('.sellPrice', this.element).show();
		        	break;
	        }
        	that.setShareLinkState();
        });
        
        jQuery('#btnPrivacyApply', this.element).click(function(event){
	        that.savePrivacy();
        	that.close();
        	return false;
        });
        
        jQuery('#addGuest', this.element).click(function (event){        	
        	that.addNewEmailToGuestList();
        });
        
        jQuery('#removeGuest', this.element).click(function (event){        	
        	that.removeEmailFromGuestLIst();
        });
        
        // Set Privacy Level - Share Zapp
        jQuery('#btnPrivacyShare', this.element).click(function (event){
	        that.savePrivacy(true);
	        that.onStateShare();
        });
        
        // this is really a cancel button
        jQuery('#btnDone').click(function (event){
        	jQuery('.shareZappDefault').hide();
        	that.close();
        	return false;
        });
        
        // Log.debug('ShareDialog numCloses: ' + jQuery('div#shareDialog a.close').length);
        jQuery('div#shareDialog a.close', this.element).click(function (event) {
            that.close();
            return false;
        });   
        
     // Embed panel UI
        jQuery('.tabControls #btnTab1', this.element).click(function(event){
        	jQuery('.tabControls .button').removeClass("selected");
        	jQuery('.tabControls .button span').removeClass("selected");
        	jQuery(this).addClass("selected");
        	jQuery('.tabControls #btnTab1 span').addClass("selected");
        	that.shrinkGuestList();
        });
        
        jQuery('.tabControls #btnTab2', this.element).click(function(event){
        	jQuery('.tabControls .button').removeClass("selected");
        	jQuery('.tabControls .button span').removeClass("selected");
        	jQuery(this).addClass("selected");
        	jQuery('.tabControls #btnTab2 span').addClass("selected");
        	that.expandGuestList();
        });
    	
    	jQuery('.embed .button', this.element).hover(function(event){
        	//jQuery('.embed .arrowDown', that.element).css('visibility', 'hidden');
        });
    	
    	jQuery('.embed .button', this.element).mouseout(function(event){
        	//jQuery('.embed .arrowDown', that.element).css('visibility', 'hidden');
        });
        
    	this.setShareLinks();

        jQuery('div#shareDialog div#embed a', this.element).click(function (event) {
            if (this.hash.indexOf('#/detail?') == 0) {
                that.close();
                return true;
            }
        });

        // Share Zapp - selected - embed
        jQuery('#btnDimension').hover(function(event){
        	//jQuery('.embed .arrowDown', this.element).css('visibility', 'visible');
        });
        jQuery('#btnDimension').click(function(event){
        	if(that.embedSizeSrc.length) {
	        	jQuery('div#share_embed input#width', this.element).val(that.embedSizeSrc[0]);
	        	jQuery('div#share_embed input#height', this.element).val(that.embedSizeSrc[1]);
	        	that.updateEmbedCode();
        	}
        });
        
        // focus email to: input on panel focus
        jQuery('div#shareDialog div#share_email', this.element).focus(function(){
            jQuery('div#shareDialog div#share_email input#emailTo', that.element).select();
        });

        // show embed share
        jQuery('div#shareDialog div#share_embed a', this.element).click(function(event) {
            that.fireEvent(ShareDialog.EMBED, o);
            return false;
        });
        // focus embed code box on panel focus
        jQuery('div#shareDialog div#share_embed', this.element).focus(function(){
            that.refreshEmbedSize();
            jQuery('div#shareDialog div#share_embed textarea#embed_code', that.element).select();
        });
        
        // refresh the height and width for embed tag and input fields.
        jQuery('div#shareDialog div#share_embed', this.element).focus(function(){
            that.refreshEmbedSize();
            jQuery('div#shareDialog div#share_embed textarea#embed_code', that.element).select();
        });

        jQuery('div#shareDialog div#share_embed textarea#embed_code', this.element).click(function(){
        	
            jQuery('div#shareDialog div#share_embed textarea#embed_code', that.element).select();
        });
        
        // handle change on the height and width input fields.
        jQuery('div#share_embed input#width', this.element).change(function(){
        	that.updateWidth();
        });
        
        jQuery('div#share_embed input#height', this.element).change(function(){
        	that.updateHeight();
        });
        
        // handle locking of aspect ratio button
        jQuery('div#share_embed a#aspectLock', this.element).click(function(){
        	that.updateAspectLock();
        });
        
        // show URL share
        jQuery('div#shareDialog div#share_link a', this.element).click(function(event) {
            that.fireEvent(ShareDialog.LINK, o);
            return false;
        });
        // focus URL input on panel focus
        jQuery('div#shareDialog div#share_link', this.element).focus(function(){
            jQuery('div#shareDialog div#share_link input#short_url').select();
        });
        
        // limit characters for the price field
        jQuery('#sellingPrice', this.element).numeric({allow:".,$"});
        if (this._config.project.price > 0) {
        	jQuery('#radioSellingPrice', this.element).attr('checked', true);
        } else {
        	jQuery('#radioSellingFree', this.element).attr('checked', true);
        }
        
        // rewrite current on update
        jQuery('#sellingPrice', this.element).focusout(function(event) {
        	//dwh:  I am slow, so let's spell it out one var at a time...
        	var pennies = AIUtils.currencyToPennies(jQuery(this).val());
        	var dollars = AIUtils.penniesToDollars(pennies);
        	var formatted = AIUtils.formatCurrency(dollars);
        	jQuery(this).val(formatted);
        	that._priceSelected = pennies;
        })
        
        // determine price based off selection
        jQuery('.radioPrice', this.element).click(function(event){
        	var id = jQuery(this).attr('id');
	        switch(id) {
		        case "radioSellingFree": 
		        	that._priceSelected = 0;
		        	jQuery('#sellingPrice', this.element).prop('disabled', true);
		        	break;
		        case "radioSellingPrice": 
		        	jQuery('#sellingPrice', this.element).prop('disabled', false);
		        	that._priceSelected = AIUtils.currencyToPennies(jQuery('#sellingPrice', this.element).val());
		        	break;
	        }
        })
        
        if(that._priceSelected == 0){
        	jQuery("#radioSellingPrice").prop("checked","");
        	jQuery("#radioSellingFree").prop("checked","checked");
        }else{
        	jQuery("#radioSellingPrice").prop("checked","checked");
        	jQuery("#radioSellingFree").prop("checked","");
        }
        
        // determine embed privelage based on selection
        jQuery('#allowEmbed', this.element).click(function(event){
        	if(jQuery(this).is(':checked')){
        		that._embedSelected = true;
        	}else{
        		that._embedSelected = false;
        	}
        })
        
        jQuery("#addGuestForm").keypress(function(e){
		    if (e.which == 13) {
		      that.addNewEmailToGuestList();
		      return false;
		   }
		})


        if(this.itemId) {
            setPanelValues();
        }
       
        Log.debug('ShareDialog.processFragment - @bottom');
    }

    this.setPanelValues = function (shortUrl)
    {
    	var zebraHost;
    	if(document.location.host == "localhost"){
    		zebraHost = document.location.protocol + "//zebrazapps.com/";
        }else{
        	zebraHost = document.location.protocol + "//" + document.location.host + "/";
        }
    	
    	this.buildButtons(this._shareSelected);
    	
    	var shareUrl = '';
    	if (this._config.project.owned) {
    		shareUrl = zebraHost + 'share/' + IdType.APN_UUID + '/' + this._config.project.playerId;
    	} else {
    		shareUrl = zebraHost + 'share/' + IdType.URL_NAME + '/' + this._config.project.urlName;
    	};

        var encodedLongUrl = encodeURIComponent(shareUrl);
        var friendlyShareUrl = (shortUrl != null && shortUrl != "") ? shortUrl : shareUrl;
        var encodedFriendlyUrl = encodeURIComponent(friendlyShareUrl); 

        var faceBookUrl = 'https://www.facebook.com/plugins/like.php?href='+encodedLongUrl+'&amp;send=false&amp;layout=standard&amp;width=340&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=229319337079250';
        jQuery('div#shareDialog div#facebook_share_link iframe', this.element).attr('src', faceBookUrl);

        var twitterMessage = "I just saw this "+this._config.project.type+" created with ZebraZapps! You can see it, too, at this link.";
        var twitterUrl = 'https://platform.twitter.com/widgets/tweet_button.html?url='+encodedFriendlyUrl+'&text='+encodeURIComponent(twitterMessage)+'&count=horizontal';
        jQuery('div#shareDialog div#twitter_share_link iframe', this.element).attr("src", twitterUrl);

        //http://www.linkedin.com/shareArticle?mini=true&url=[URL]&title=[TITLE]&source=[SOURCE/DOMAIN]

        var linkedInUrl = 'https://www.linkedin.com/shareArticle?mini=true&url='+encodedFriendlyUrl+'&title='+this._config.project.projectName;
        //jQuery('div#shareDialog div#linkedin_share_link a', this.element).attr("href", linkedInUrl);


        var emailBodyReadOnly = 'I just saw this interesting app created with ZebraZapps! I thought you might like to see it, too, at this link: ' + friendlyShareUrl;
        // Fixup the Email from box to include the user's email
        jQuery('div#share_email input#emailFrom', this.element).val(Credentials.email());
        jQuery('div#share_email textarea#emailBodyReadOnly', this.element).val(emailBodyReadOnly);
        jQuery('input#emailTo', this.element).val('');
        
        if(shortUrl) {
        	jQuery('div#share_link #link_text', this.element).val(shortUrl);
        }
    }
    
    this.refreshEmbedSize = function()
    {
        var o = this._config.project;
	    var contentWidth = 400;
	    if(o.embedContentWidth != null) {
		    contentWidth = Number(o.embedContentWidth);
		    if(o.embedContentWidthPadding != null) {
			    contentWidth += Number(o.embedContentWidthPadding);
		    }
	    }
	    var contentHeight = 250;
	    if(o.embedContentHeight != null) {
		    contentHeight = Number(o.embedContentHeight);
		    if(o.embedContentHeightPadding != null) {
			    contentHeight += Number(o.embedContentHeightPadding);
		    }
	    }
        jQuery('div#share_embed input#width', this.element).val(contentWidth);
    	jQuery('div#share_embed input#height', this.element).val(contentHeight);
    	
    	this.aspectLocked = false;
    	
    	this.updateEmbedCode()
    }
    
    this.updateAspectLock = function()
    {
    	this.aspectLocked = !this.aspectLocked;
    	if(this.aspectLocked){
    		var currentWidth = jQuery('div#share_embed input#width', this.element).val();
        	var currentHeight = jQuery('div#share_embed input#height', this.element).val();
    		this.aspectRatio = currentWidth/currentHeight;    		
            jQuery('div#share_embed li.lock a', this.element).addClass('selected');
    	}else{
    		jQuery('div#share_embed li.lock a', this.element).removeClass('selected');
    	}
    }
    
    this.updateWidth = function()
    {
    	if(this.aspectLocked){
    		var currentWidth = jQuery('div#share_embed input#width', this.element).val();
    		jQuery('div#share_embed input#height', this.element).val(currentWidth/this.aspectRatio);
    	}
    	this.updateEmbedCode();
    }
    
    this.updateHeight = function()
    {
    	if(this.aspectLocked){
    		var currentHeight = jQuery('div#share_embed input#height', this.element).val();
    		jQuery('div#share_embed input#width', this.element).val(currentHeight*this.aspectRatio);
    	}
    	this.updateEmbedCode();
    }
    
    this.updateEmbedCode = function()
    {
    	//TODO: zebraHost should be pulled out into a class variable.
    	var zebraHost;
    	if(document.location.host == "localhost"){
    		zebraHost = document.location.protocol + "//zebrazapps.com/";
        }else{
        	zebraHost = document.location.protocol + "//" + document.location.host + "/";
        }
    	
    	var newWidth = jQuery('div#share_embed input#width', this.element).val();
    	var newHeight = jQuery('div#share_embed input#height', this.element).val();
    	
    	if(newWidth.indexOf("px") == -1){
    		newWidth = newWidth + "px";
    	}
    	
    	if(newHeight.indexOf("px") == -1){
    		newHeight = newHeight + "px";
    	}
    	
    	if(!this.embedSizeSrc.length) {
    		var w = parseInt(newWidth.replace("px", ""));
    		var h = parseInt(newHeight.replace("px", ""));
    		this.embedSizeSrc = [w, h];
    	}
    	
    	var embedCode = '<iframe title="'+o.publishedName+'" class="zapp" type="text/html" width="'+newWidth+'" height="'+newHeight+'" src="'+zebraHost+'e/'+this._config.project.playerId+'" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>';
    	jQuery('div#share_embed textarea#embed_code', this.element).val(embedCode);
   }

    this.setTinyUrl = function (url)
    {
    	this.setPanelValues(url);
    }
    
    //Guest List management methods
    this.tempGuestListToken = 0; //This is for local id only, not a perm id
    this.tempGuestSelection = undefined;
    this.guestListId = '0';
    
    this.onRetrievedGuestList = function(guestList)
    {
    	var that = this;
    	this.guestListId = guestList.guestlistid;
    	jQuery('textarea#guestEmailMessage',that.element).val(guestList.emailtext);
    	jQuery(guestList.guestlistinvitees).each(function(){
    		that.addEmailToGuestList(this.email);
    	});
    }
    
    this.addNewEmailToGuestList = function()
    {
    	var that = this;
    	if(jQuery('form#addGuestForm', this.element).valid() && !this.isDuplicate(jQuery('input#guestToAdd', this.element).val())){
    		var validEmail = jQuery('input#guestToAdd', this.element).val();
    		that.addEmailToGuestList(validEmail);
    		jQuery('input#guestToAdd', this.element).val('');
    	}
    }
    
    this.addEmailToGuestList = function(validEmail)
    {
    	var that = this;
    	jQuery('div#guestListEmailList', this.element).prepend('<div class="unselected" id="guestListItem'+that.tempGuestListId+'">'+validEmail+'</div>');

    	jQuery('div#guestListEmailList div#guestListItem'+that.tempGuestListId, this.element).click(function(){
    		jQuery(this).removeClass('unselected');
    		jQuery(this).addClass('selected');
    		that.selectEmailOnList(this);
    	});
    	that.tempGuestListId++;	
    }
    
    this.removeEmailFromGuestLIst = function()
    {
    	jQuery(this.tempGuestSelected).remove();
    }
    
    this.isDuplicate = function(emailAddress){
    	var duplicate = false;
    	jQuery('div#guestListEmailList div#guestListItem', this.element).each(function(){
    		if(jQuery(this).text() == emailAddress){
    			duplicate = true;
    			return false;
    		}
    	}); 
    	return duplicate;
	}
    
    this.selectEmailOnList = function(selectedEmail)
    {
    	jQuery('div#guestListEmailList div', this.element).each(function(){
    		jQuery(this).removeClass('selected');
    	});
    	
    	jQuery(selectedEmail).toggleClass('selected');
    	
    	this.tempGuestSelected = selectedEmail;
    }
    
    this.collectEmailsForSubmission = function()
    {
    	var emailList = [];
    	jQuery('div#guestListEmailList div', this.element).each(function(){
    		emailList.push(jQuery(this).text());
    	});
    	return emailList;
    }
    
    this.sumbitGuestList = function(emailList, emailText)
    {
    	var guestListObject = {};
    	guestListObject.emailList = emailList;
    	guestListObject.emailText = emailText;
    	guestListObject.projectId = this._config.project.id;
    	guestListObject.guestListId = this.guestListId; // This needs to be dynamic based on the incoming guestListId
    	this.fireEvent(ShareDialog.SAVE_GUEST_LIST, guestListObject);
    	this.showUpdateAnimation(this);
    }

	this.savePrivacy = function(keepDialogOpen)
	{		
		var permissions = {
			hideInStoreList:this._privacySelected < this.sharingLinks.PRIV_SHOP,
			inviteOnly:this._privacySelected == this.sharingLinks.PRIV_GUEST,
	        embed:this._embedSelected
		};

		var licenseTypeId = this._config.project.licenseTypeId;

		var change = false;
		
		if(this._config.project.hideInStoreList != permissions.hideInStoreList) {
			change = true;
		}
		
		if(this._config.project.inviteOnly != permissions.inviteOnly) {
			change = true;
		}
		
		// defined here because regardless of action, we send it in the notification
		var price = AIUtils.currencyToPennies(this._config.project.price); // comes in in dollars
		if(!permissions.hideInStoreList) {
			licenseTypeId = this._embedSelected ? 6 : 1;
			if(this._config.project.licenseTypeId != licenseTypeId){
				change = true;
			}
			price = this._priceSelected;
			if(AIUtils.currencyToPennies(this._config.project.price) != this._priceSelected){
				change = true;
			}
			if(this._config.project.inviteOnly != permissions.inviteOnly) {
				change = true;
			}
			if(this._config.project.inviteOnly != permissions.inviteOnly) {
				change = true;
			}
		}
//		if(change) {
			this.fireEvent(ShareDialog.SAVE_PRIVACY_LEVEL, {
				permissions:permissions,
				price:price,
				licenseTypeId:licenseTypeId,
				keepDialogOpen:keepDialogOpen
				});
//		}
	}
	
	this.showUpdateAnimation = function()
    {
		jQuery('.updatingAnimation').fadeIn();
    }
    
    this.hideUpdateAnimation = function()
    {
    	setTimeout(function(){
            jQuery('.updatingAnimation').fadeOut();
        },
        1000);
    }
    
    this.showInvitationSent = function() {
    	jQuery('#invitationSent').show();
    	setTimeout(function() {
    		jQuery('#invitationSent').hide();
    	}, 7000);
    }
}
ShareDialog = new Class(new ShareDialog());
ShareDialog.NAME = 'ShareDialog';
ShareDialog.INITIALIZED = 'ShareDialog.INITIALIZED';
ShareDialog.EMBED = 'ShareDialog.EMBED';
ShareDialog.EMAIL = 'ShareDialog.EMAIL';
ShareDialog.LINK = 'ShareDialog.LINK';
ShareDialog.RETITLE = 'ShareDialog.RETITLE';
ShareDialog.CLOSE = 'ShareDialog.CLOSE';
ShareDialog.APPLY = 'ShareDialog.APPLY';
ShareDialog.LOAD_DETAIL = 'ShareDialog.LOAD_DETAIL';
ShareDialog.LOAD_TINYURL = 'ShareDialog.LOAD_TINYURL';
ShareDialog.LOAD_GUEST_LIST = 'ShareDialog.LOAD_GUEST_LIST';
ShareDialog.SAVE_GUEST_LIST = 'ShareDialog.SAVE_GUEST_LIST';
ShareDialog.SAVE_PRIVACY_LEVEL = 'ShareDialog.SAVE_PRIVACY_LEVEL';
