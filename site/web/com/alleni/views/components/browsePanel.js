var BrowsePanel = function() 
{
    this.Extends = UIComponent;
    this.group = 'visitor';
    this.type = 'zapp';
    this.start = 1;
    this.end = 0;
    this.pageSize = 9;
    this.total = 0;
    this.selectedFilterValue = undefined;
    this.selectedSortValue = undefined;
    this.selectedSearchValue = undefined;
    this.mode = ''; // ['verbose', 'slim'] ..... any others?
    
    this.html = "";
    
    this.initialize = function (parent)
    {
        if (parent == undefined)
            Log.error('BrowsePanel.initialize - parent undefined');
        this.parent(parent);
        Log.warn('BrowsePanel.initialize - '+parent);
        var that = this;
        var o = {
            filename: 'browsePanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.html = fragment;
        this.initializationComplete();
    }
    
    this.getHtml = function() {
    	return this.html;
    }
    
    this.initializationComplete = function ()
    {
    	this.fireEvent(BrowsePanel.INITIALIZED, BrowsePanel.NAME);
    }
    
    this.display = function(displayArguments) {
    	var arrArgs = displayArguments.split('&');
    	var fireDisplayEvent = false;
    	
    	this.group = arrArgs[0];
    	this.type = arrArgs[1];
    	
    	this.setPageInfo(0, 0, 0, TypeLookup.plural(this.type));
    	this.setGroupType(this.group, this.type);
    	if (fireDisplayEvent) { 
    		this.fireEvent(BrowsePanel.DISPLAY, {group: this.group, type: this.type});
    	}
    }
    
    this.setGroupType = function (group, type)
    {
    	if(this.group != group || this.type != type) {
    		this.selectedSortValue = undefined;
    	}
    	
    	this.group = group;
    	this.type = type;
    	
    	switch (group+'.'+type)
        {
        case 'visitor.zapp':
        case 'visitor.gadget':
        case 'member.zapp':
            jQuery('div#grid', this.element).show();
            jQuery('div#table table', this.element).hide();
            //jQuery('input#searchFilter', this.element).val('');
            jQuery('select#sortSelect').html(this.publishedSortOptions);
            jQuery('select#sortSelect').val(this.selectedSortValue);
            jQuery('div#categoryFilter', this.element).show();
            jQuery('select#categorySelect').html((type=='gadget')?this.gadgetCategoryOptions:this.projectCategoryOptions);
            jQuery('select#categorySelect').val(this.selectedFilterValue);
            break;
        case 'member.project':
        case 'member.gadget':
            jQuery('div#grid', this.element).hide();
            jQuery('div#table table', this.element).hide();
            jQuery('div#table table#'+type, this.element).show();
            //jQuery('input#searchFilter', this.element).val('');
            jQuery('select#sortSelect', this.element).html(this.unpublishedSortOptions);
            jQuery('select#sortSelect').val(this.selectedSortValue);
            jQuery('div#categoryFilter', this.element).hide();
            break;
        default:
            Log.error('BrowsePanel.addItem - unsupported group/type combination (group:'+group+', type:'+type+')');
            break;
        }
    };
    
    this.setMode = function(mode) {
    	this.mode = mode;
    	this.removeItems();
    	this.populateView(this.items);
    }
    
    this.removeItems = function () {
   		jQuery('div#grid .item', this.element).remove();     
   		jQuery('div#table .item', this.element).remove();
    }
    
    this.clearCategorySelection = function() {
    	this.selectedFilterValue = undefined;
    };
    
    this.setPageValues = function (page) {
    	this.start = page * this.pageSize + 1;
    	this.end = (this.start + this.pageSize - 1 <= this.total) ? this.start + this.pageSize - 1 : this.total;
    }
    
    this.populateView = function(o) {
    	var items = o.items;
    	this.items = o.items; // persist for future changes. Trust me.
        var displayType = (o.type == 'zapp') ? 'app' : o.type;
        var that = this;
        
        this.setGroupType(o.group, o.type);
        
        if (items.length == 0) {
        	this.addGridItem(0, null, o.group, o.type, false, true);
            this.updateHeaderFooter(o.type, 0, 0, 0);
            
        	jQuery('div#grid', this.element).html('No matching ' + displayType + 's');
        } else {
        	this.start = (o.page*o.count) + 1;
            this.end = this.start+items.length-1;
            this.total = o.size;
            
            if (this.selectedSearchValue != undefined) {
            	jQuery('div.search.filter input', this.element).val(this.selectedSearchValue);
            }
            
	    	switch (o.group+'.'+o.type + '.' + this.mode)
	        {
		        case 'visitor.zapp.':
		        case 'visitor.gadget.':
		        case 'member.zapp.':
		            for (var i = 0; i < items.length; i++) {
		            	this.addGridItem(i, items[i], i == items.length-1, false);    	
		            }
		            break;
		        case 'member.project.':
		        case 'member.gadget.':
		        	for (var i = 0; i < items.length; i++) {
		        		//alert (i);
		            	this.addListItem(i, items[i], i == items.length-1, false);    	
		            }
		        	break;
	        }
        	
            this.updateHeaderFooter(o.type, this.start, this.end, o.size);
        }
        
        jQuery('select#categorySelect', this.element).unbind('change').change(function () {
            that.selectedFilterValue = this.value;
        	var a = this.value.split('.');
            that.fireEvent(BrowsePanel.FILTER_CHANGED, {group:a[0], value:a[1]});
            return false;
        });
        
        jQuery('select#sortSelect', this.element).unbind('change').change(function () {
            that.selectedSortValue = this.value;
        	var a = this.value.split('.');
            that.fireEvent(BrowsePanel.FILTER_CHANGED, {group:a[0], value:a[1]});
            return false;
        });
        
        jQuery('div.search.filter img', this.element).unbind('click').click(function () {
        	this.selectedSearchValue = jQuery('div.search.filter input', this.element).val();
            that.fireEvent(BrowsePanel.FILTER_CHANGED, {group:'search', value:this.selectedSearchValue});
            return false;
        });
        jQuery("div.search.filter input", this.element).keyup(function(event){
            Log.warn(event.keyCode);
        	if (event.keyCode == 13) {
                jQuery('div.search.filter img').click();
            }
        });
        
        jQuery('div.paging a', this.element).unbind('click').click(function (event) {
            var parent = jQuery(this).parent();
        	var payload = {type: this.type};
        	var eventToFire = "";
        	
        	var handled = false;  	
        	var target = this.hash.substring(1).split('?');
            var targetHash = target[0];
        
            if(parent.hasClass("_disabled")) return false;
            
            switch(targetHash) {
            case 'previousPage':
            	if (that.start == 1) {
        			event.preventDefault();
    			}else{
    				if (targetHash == 'authorProfile') {
    					eventToFire = BrowsePanel.PROFILE_PREVIOUS;
    				} else {
    					eventToFire = BrowsePanel.PREVIOUS;
    				}
    			}
            	break;
            case 'nextPage':
            	if(that.end == that.total) {
        			event.preventDefault();
    			} else {
    				if (targetHash == 'authorProfile') {
    					eventToFire = BrowsePanel.PROFILE_NEXT;
    				} else {
    					eventToFire = BrowsePanel.NEXT;
    				}
    			}
            	break;
            default:
            	break;
            }
            
            if (eventToFire != ''){
            	that.fireEvent(eventToFire, payload);
            	handled = true;
            }
            
            return !handled;
        });
        
        jQuery('img.zapTTEnable', this.element).unbind('hover').hover(
            function(event) {
            	var description = null;
            	
            	if(event.target.id.substring(6) != undefined){
            		description = items[event.target.id.substring(6)].description;
                    jQuery('.zapptooltip span', that.element).text(description);
            	}
                if(description != "" && description != null){
                	 jQuery('.zapptooltip', that.element).css('left', (event.pageX + 15) + 'px').css('top', (event.pageY + 9) + 'px').show();
                }
               
            },
            function (event) {
                jQuery('.zapptooltip', that.element).hide();
            }
        ).mousemove(function(event) {
            jQuery('.zapptooltip', that.element).css('left', (event.pageX + 15) + 'px').css('top', (event.pageY + 9) + 'px');
        });
    }
    
    this.populateViewFailure = function(payload) {
    	Log.error('BrowsePanel.populateViewFailure has not been implemented!');
    }
    
    this.setPageInfo = function(start, end, total, type) {
    	var pageInfo = start + '-' + end + ' of ' + total + ' ' + type;
    	jQuery('span.pageInfo', this.element).text(pageInfo);
    }
    
    this.addGridItem = function(index, item, lastItem, configureOnly) {
    	var html = '';
    	var that = this;
    	
    	if (!configureOnly)
    	{	
    		html = ZappGadgetProject.html(index, item, this.group, this.type);
    	}
    	
    	if (!configureOnly)
        {
            jQuery('div#grid', this.element).append(html);
            item.buy._type = this.type;
            jQuery('div#grid div.item:last', this.element).data('item', item);
        }
        if (lastItem)
        {
            jQuery('div#grid span.author', this.element).hover(function hoverAuthorStart(event) {
                that.fireEvent(BrowsePanel.AUTHOR_HOVER_START, {id: event.target.id, x: event.clientX, y: event.clientY});
                return false;
            }, function hoverAuthorEnd(event) {
                return false;
            });

            //jQuery('.item', this.element).fadeTo(0, 0.01);
            jQuery('.item .icon .iconLogo', this.element).fadeTo(0, 0.01);
        	jQuery('.item .icon .iconLogo', this.element).load(function(){
        		jQuery('.item', that.element).fadeTo(500, 1);
        		jQuery('.item .icon .iconLogo', that.element).fadeTo(250,1);
        		jQuery(this).fadeTo(250,1);
        	});
        	
        	jQuery('.item .icon .iconLogo', this.element).error(function(){

        	});
            jQuery('div#grid div.item a', this.element).unbind('click').click(function() {
            	
                var a = this.hash.substring(1).split('?');
                var action = a[0];
                var item = jQuery(this).parents('div.item').data('item');
                
                switch (action)
                {
                case '/detail':    
                    return true;
                case 'buy':    
                    that.fireEvent(BrowsePanel.BUY_CLICKED, item.buy);
                    return false;
                case 'unpurchase':
                	var aId = Credentials.accountId();
                	var alertMsg = 'Are you sure you want to permanently remove "' + item.publishedName + '" from your collection?';
                	
                	if (aId == item.authorAccountId) {
                		alertMsg = alertMsg + '\n\nWarning:  Since you are the author of this item, it will also be removed from the shopp.';
                	}
                    if (confirm(  alertMsg ) == true) {
                        that.fireEvent(BrowsePanel.UNPURCHASE_PROJECT, item.playerId);
                        return false;
                    }
                    break;
                case 'toggleFeatured':
                	that.fireEvent(BrowsePanel.TOGGLE_FEATURED, {id: item.id, featured: item.featured});
                	break;
                case 'updateApp':
                	that.fireEvent(BrowsePanel.UPDATE_ZAPPGADGET, {urlName: item.urlName});
                case 'authorProfile':
                	var urlArgs = a[1];
                	var authorProfileArgs = urlArgs.split('=');
                	var payload = {
                		href: action,
                		parent: '',
                		id: authorProfileArgs[1]
                	};
                	that.fireEvent(BrowsePanel.HREF_CLICKED, payload);
                	break;
                default:
                    break;
                }
                return true;
            });
        }    	
    };
    
    this.addListItem = function(index, item, lastItem, configureOnly) {
    	//alert ('ListItem being called');
    	var html = '';
    	var that = this;
    	
    	if (!configureOnly)
    		html = ZappGadgetProject.html(index, item, this.group, this.type);
    	
    	if (!configureOnly)
        {
            jQuery('div#table table#'+this.type+' tr:last', this.element).after(html);
        	jQuery('div#table table#'+this.type+' tr.item:last', this.element).data('item', item);
        }
        if (lastItem)
        {
        	jQuery('.table', this.element).fadeTo(0, 0.01);
            jQuery('.project .iconLogo', this.element).fadeTo(0, 0.01);
        	jQuery('.table', this.element).load(function(){
        		jQuery('.table', this.element).fadeTo(500, 1);
        		jQuery('.project .iconLogo', this.element).fadeTo(500,1);
        		jQuery(this).fadeTo(250,1);
        	});
        	
        	jQuery('.table', this.element).error(function(){

        	});
            jQuery('div#table td a', this.element).unbind('click').click(function(e) {
                var a = this.hash.substring(1).split('?');
                var action = a[0];
                var item = jQuery(this).parents('tr.item').data('item');
                switch (action)
                {
                case '/detail':    
                    return true;
                case 'edit':
                	var cfg = {};
                	cfg.id = item.projectId;
                	cfg.bShowHistory = false;
                	cfg.projectName = item.projectName;
                	that.fireEvent(BrowsePanel.LAUNCH_EDITOR, cfg);
                    break;
                    
                case 'history':
                	var cfg = {};
                	cfg.id = item.projectId;
                	cfg.bShowHistory = true;
                	cfg.projectName = item.projectName;
                	that.fireEvent(BrowsePanel.LAUNCH_EDITOR, cfg);
                    break;
                case 'delete':
                    if (confirm('Are you sure you want to permanently delete "' + item.projectName + '"?') == true) {
                        that.fireEvent(BrowsePanel.DELETE_PROJECT, item.projectId);
                    }
                    break;
                case 'unpublish':
                    if (confirm('Are you sure you want to permanently remove "' + item.publishedName + '" from the shopp?') == true) {
                        that.fireEvent(BrowsePanel.UNPUBLISH_PROJECT, item.projectId);
                    }
                    break;
                case 'unpurchase':
                	var aId = Credentials.accountId();	
                	var alertMsg = 'Are you sure you want to permanently remove "' + item.publishedName + '" from your collection?';
                	
                	if (aId == item.authorAccountId) {
                		alertMsg = alertMsg + '\n\nWarning:  Since you are the author of this item, it will also be removed from the shopp.';
                	}
                    if (confirm(  alertMsg ) == true) {
                        that.fireEvent(BrowsePanel.UNPURCHASE_PROJECT, item.playerId);
                    }
                    break;
                case 'toggleFeatured':
                	that.fireEvent(BrowsePanel.TOGGLE_FEATURED, {id: item.id, featured: item.featured});
                	break;
                case 'updateApp':
                	that.fireEvent(BrowsePanel.UPDATE_ZAPPGADGET, {urlName: item.urlName});
                	break;
                default:
                    break;
                }
                return false;
            });
        }    	
    };
    
    this.updateHeaderFooter = function (type, start, end, total)
    {        
    	this.start = start;
    	this.end = end;
    	this.total = total;
    	
    	if (type != this.type) {
    		this.clearCategorySelection();
    	}
    	
        jQuery('h4', this.element).text(TypeLookup.plural(type));
        
        this.setPageInfo(start, end, total, TypeLookup.plural(type));
        
        jQuery('.paging .nextPage', this.element).mouseover(function() {
        	if(end==total) 
        		jQuery(this).css('text-Decoration', 'none');
        	else 
        		jQuery(this).css('text-Decoration', 'underline');
        });
        
        jQuery('.paging .nextPage', this.element).mouseout(function() {
        	jQuery(this).css('text-Decoration', 'none');
        });
        
        jQuery('.paging .previousPage', this.element).mouseover(function() {
        	if(start==1) 
        		jQuery(this).css('text-Decoration', 'none');
        	else 
        		jQuery(this).css('text-Decoration', 'underline');
        });
        
        jQuery('.paging .previousPage', this.element).mouseout(function() {
        	jQuery(this).css('text-Decoration', 'none');
        	
        });
        
        
        // pages in between
        jQuery('.paging .nextPage', this.element).attr('disabled', false);
        jQuery('.paging .nextPage', this.element).css('color', '#009966');

        jQuery('.paging .previousPage', this.element).attr('disabled', false);
        jQuery('.paging .previousPage', this.element).css('color', '#009966');
        
        // book end condition
        if(end==total) {
	        jQuery('.paging .nextPage', this.element).attr('disabled', true);
	        jQuery('.paging .nextPage', this.element).css('color', '#AAAAAA');
        }
        else if(1==start) {
        	jQuery('.paging .previousPage', this.element).attr('disabled', true);
     	    jQuery('.paging .previousPage', this.element).css('color', '#AAAAAA');	
        }
    }
    
    this.gadgetCategoryOptions =
        '<option value="category.all"> All </option> \
    	<option value="category.6006"> Accessibility </option> \
    	<option value="category.6007"> Animation </option> \
    	<option value="category.6008"> Audio &amp; Media </option> \
    	<option value="category.6001"> Buttons </option> \
    	<option value="category.6003"> Calculations </option> \
    	<option value="category.6009"> CCAF Sets </option> \
    	<option value="category.6010"> Charting &amp; Graphing </option> \
    	<option value="category.6011"> Experiments </option> \
    	<option value="category.6012"> Feedback &amp; Messaging </option> \
    	<option value="category.6013"> Graphics </option> \
    	<option value="category.6005"> Interactions </option> \
    	<option value="category.6014"> Interface &amp; Design </option> \
    	<option value="category.6015"> Logic &amp; Branching </option> \
    	<option value="category.6000"> Navigation </option> \
    	<option value="category.6002"> Questions </option> \
    	<option value="category.6016"> Quiz &amp; Test </option> \
    	<option value="category.6004"> Simulations </option> \
    	<option value="category.6017"> Utilities </option> \
    	<option value="category.6035"> App Examples </option>';
    
    this.projectCategoryOptions =
        '<option value="category.all"> All </option> \
        <option value="category.6018"> Business </option> \
    	<option value="category.6019"> Education </option> \
    	<option value="category.6020"> Entertainment </option> \
    	<option value="category.6021"> Experiments </option> \
    	<option value="category.6022"> Fitness </option> \
    	<option value="category.6023"> Friends &amp; Family </option> \
    	<option value="category.6024"> Fun &amp; Games </option> \
    	<option value="category.6025"> Health &amp; Medical </option> \
    	<option value="category.6026"> How To &amp; DIY </option> \
    	<option value="category.6027"> Lifestyle </option> \
    	<option value="category.6028"> Music &amp; Media </option> \
    	<option value="category.6029"> News &amp; Weather </option> \
    	<option value="category.6030"> Productivity </option> \
    	<option value="category.6031"> Social </option> \
    	<option value="category.6032"> Sports </option> \
    	<option value="category.6033"> Training </option> \
    	<option value="category.6034"> Zebra </option>'; 
    
    
    this.publishedSortOptions = 
	    ' </option>\
    	<option value="sort.createdDateTime">\
        	Most Recent\
    	</option>\
    	<option value="sort.nameAscending">\
	        Name (A-Z)\
	    </option>\
	    <option value="sort.nameDescending">\
	        Name (Z-A)\
    	</option>\
	    <option value="sort.popularityDescending">\
	        Popularity\
	    </option>\
	    <option value="sort.priceAscending">\
	        Price (low to high)\
	    </option>\
	    <option value="sort.priceDescending">\
	        Price (high to low)\
	    </option>';
    	
    this.unpublishedSortOptions = 
        '<option value="sort.createdDateTime">\
        	Most Recent\
    	</option>\
    	<option value="sort.nameAscending">\
            Name (A-Z)\
        </option>\
        <option value="sort.nameDescending">\
            Name (Z-A)\
        </option>';
}
BrowsePanel = new Class(new BrowsePanel());
BrowsePanel.NAME = 'BrowsePanel';
BrowsePanel.AUTHOR_HOVER_START = 'BrowsePanel.AUTHOR_HOVER_START';
BrowsePanel.BUY_CLICKED = 'BrowsePanel.BUY_CLICKED';
BrowsePanel.LAUNCH_EDITOR = 'BrowsePanel.LAUNCH_EDITOR';
BrowsePanel.FILTER_CHANGED = 'BrowsePanel.FILTER_CHANGED';
BrowsePanel.INITIALIZED = 'BrowsePanel.INITIALIZED';
BrowsePanel.NEXT = 'BrowsePanel.NEXT';
BrowsePanel.PREVIOUS = 'BrowsePanel.PREVIOUS';
BrowsePanel.PROFILE_NEXT = 'BrowsePanel.PROFILE_NEXT';
BrowsePanel.PROFILE_PREVIOUS = 'BrowsePanel.PROFILE_PREVIOUS';
BrowsePanel.DELETE_PROJECT = 'BrowsePanel.DELETE_PROJECT';
BrowsePanel.UNPUBLISH_PROJECT = 'BrowsePanel.UNPUBLISH_PROJECT';
BrowsePanel.UNPURCHASE_PROJECT = 'BrowsePanel.UNPURCHASE_PROJECT';
BrowsePanel.TOGGLE_FEATURED = 'BrowsePanel.TOGGLE_FEATURED';
BrowsePanel.UPDATE_ZAPPGADGET = 'BrowsePanel.UPDATE_ZAPPGADGET'; 
BrowsePanel.RETURN_BROWSE_PANEL_LIST = 'BrowsePanel.RETURN_BROWSE_PANEL_LIST';
BrowsePanel.RETURN_BROWSE_PANEL_LIST_FAILURE = 'BrowsePanel.RETURN_BROWSE_PANEL_LIST_FAILURE';
BrowsePanel.HREF_CLICKED = 'BrowsePanel.HREF_CLICKED';
BrowsePanel.LIST_TYPE_CHANGE = 'BrowsePanel.LIST_TYPE_CHANGE';
BrowsePanel.CLEAR_CONTENTS = 'BrowsePanel.CLEAR_CONTENTS';
BrowsePanel.DISPLAY = 'BrowsePanel.DISPLAY';
ScriptLoader.loaded(BrowsePanel.NAME);
