var SignUpFreePanel = function()
{
    this.Extends = UIComponent;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function ()
    {
        this.parent('signUpFreePanel');
        
        var that = this;
        var o = {
            filename: 'signUpFreePanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.onSubmit = this.onSubmit.bindWithEvent(this);
        jQuery('#submitButton').click(this.onSubmit);
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(SignUpFreePanel.INITIALIZED);
        }
    }
    this.onSubmit = function (event)
    {
        event.preventDefault();
        var o = Form.toObject(jQuery('form'));
        if (o.password != o.confirmPassword)
        {
            alert('Password entries do not match');
            return;
        }
        if (o.password.length == 0)
        {
            alert('Password required');
            return;
        }
        delete o.confirmPassword;
        this.fireEvent(SignUpFreePanel.SUBMIT, {form: o});
    }
}
SignUpFreePanel = new Class(new SignUpFreePanel());
SignUpFreePanel.NAME = 'SignUpFreePanel';
SignUpFreePanel.INITIALIZED = 'SignUpFreePanel.INITIALIZED';
SignUpFreePanel.SUBMIT = 'SignUpFreePanel.SUBMIT';
ScriptLoader.loaded(SignUpFreePanel.NAME);
