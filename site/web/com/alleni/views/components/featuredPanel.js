var FeaturedPanel = function() {
	this.Extends = UIComponent;
	this.start = 0;
	this.end = 0;
	this.pageSize = 3;
	this.total = 0;
	
	this.initialize = function(parent) {
		if (parent == undefined)
			Log.error('BrowsePanel.initialize - parent undefined');
		this.parent(parent);

		var that = this;
		var o = {
			filename : 'featuredPanel.html',
			success : function(fragment, status) {
				that.processFragment(fragment);
			}
		}
		File.requestFragment(o);
	}
	this.processFragment = function(fragment) {
		this.set('html', fragment);
		this.html = fragment;
		
		var that = this;
		
		jQuery('a.previous',this.element).click(function(event) {
			if (that.start == 1) {
				event.preventDefault();
			} else {
				that.fireEvent(FeaturedPanel.PREVIOUS);
			}
			return false;
		})
		jQuery('a.next',this.element).click(function(event) {

			if (that.end == that.total) {
				event.preventDefault();
			} else {
				that.fireEvent(FeaturedPanel.NEXT);
			}
			return false;
		})
		
		this.initializationComplete();
	}
	
	this.setPageValues = function (page) {
    	this.start = page * this.pageSize + 1;
    	this.end = (this.start + this.pageSize - 1 <= this.total) ? this.start + this.pageSize - 1 : this.total;
    }
	
	this.setType = function(type) {
    	this.type = type;
    };
	
	this.getHtml = function() {
		return this.html;
	}
	
	this.initializationComplete = function() {
		this.fireEvent(FeaturedPanel.INITIALIZED, FeaturedPanel.NAME);
	}
	
	this.removeItems = function() {
		var test = jQuery('div.item',this.element);
		test.remove();
	};
	
	this.populateView = function(group, type, page, items, size, count) {
    	var that = this;
    	
		if(items.length > 0 && items[0].buy == undefined) {
    		return;
		}
    	
    	this.removeItems();
        if(items.length > 0) {
        	for (var i = 0; i < items.length; i++) {
                this.addItem(i, items[i], group, type, i == items.length-1);
            }
        	var start = (page*count)+1;
            var end = start+items.length-1;
            this.updateHeaderFooter(type, start, end, size);
    	}else{
    		 this.updateHeaderFooter(type, 0, 0, 0);
    	};
        
    	jQuery('a.featuredPaging',this.element).unbind('click').click(function (event) {
            var parent = jQuery(this).parent();
        	var payload = {type: this.type};
        	var eventToFire = "";
        	
        	var handled = false;  	
        	var target = this.hash.substring(1).split('?');
            var targetHash = target[0];
        
            if(parent.hasClass("_disabled")) return false;
            
            switch(targetHash) {
            case 'previousPage':
            	if (that.start == 1) {
        			event.preventDefault();
    			}else{
    				eventToFire = FeaturedPanel.PREVIOUS;
    			}
            	break;
            case 'nextPage':
            	if(that.end == that.total) {
        			event.preventDefault();
    			} else {
    				eventToFire = FeaturedPanel.NEXT;
    			}
            	break;
            default:
            	break;
            }
            
            if (eventToFire != ''){
            	that.fireEvent(eventToFire, payload);
            	handled = true;
            }
            
            return !handled;
        });
        //Log.warn('******* End FeaturedPanel.PopulateView()');
    };
	
	this.addItem = function(index, item, group, type, lastItem) {
		
		var html = ZappGadgetProject.html(index, item, group, type);
		var that = this;
		jQuery('a.next',this.element).before(html);
		item.buy._type = type;
		jQuery('div.item:last a',this.element).data('buy', item.buy);
		if (lastItem) {
			var that = this;
			jQuery('.item',this.element).fadeTo(0, 0.01);
			jQuery('.item .icon .iconLogo',this.element).fadeTo(0, 0.01);
			jQuery('.item .icon .iconLogo',this.element).load(function(){
        		jQuery('.item',that.element).fadeTo(500, 1);
        		jQuery(this).fadeTo(250,1);
        	});
			jQuery('.item .icon .iconLogo',this.element).error(function(){
     
        	});
			
			jQuery('a',this.element).click(function(event) {
			 	var a = this.hash.substring(1).split('?');
                var action = a[0];
                var o = jQuery(this).data('buy');
                switch (action)
                {
                case "buy":
                	
					that.fireEvent(FeaturedPanel.BUY_CLICKED, o);
					return false;
                	break;
                case "toggleFeatured":
                	that.fireEvent(FeaturedPanel.TOGGLE_FEATURED, {id: o.id, featured: o.featured});
                	break;
                case 'updateApp':
                	that.fireEvent(FeaturedPanel.UPDATE_ZAPPGADGET, {urlName: o.urlName});
                	break;
                }
			});
			
			jQuery('span.author',this.element).hover(
			function hoverAuthorStart(event) {
				that.fireEvent(FeaturedPanel.AUTHOR_HOVER_START, {
					id : event.target.id,
					x : event.clientX,
					y : event.clientY
				});
				return false;
			}, 
			function hoverAuthorEnd(event) {
				return false;
			});
			jQuery('div.featuredBrowse.featured',this.element).toggleClass(
					'zapp', type == 'zapp');
			jQuery('div.featuredBrowse.featured',this.element).toggleClass(
					'gadget', type == 'gadget');
		}
	};
	
	this.updateHeaderFooter = function(type, start, end, total) {
		this.start = start;
		this.end = end;
		this.total = total;
		
		if(this.total <= end){
			jQuery('a.next',this.element).hide();
		}else{
			jQuery('a.next',this.element).show();
		}
		
		if(this.start <= 1){
			jQuery('a.previous',this.element).hide();
		}else{
			jQuery('a.previous',this.element).show();
		}	
	};
	
	this.populateViewFailure = function() {
		Log.error('FeaturedPanel.populateViewFailure not yet implemented!');
	}
}
FeaturedPanel = new Class(new FeaturedPanel());
FeaturedPanel.NAME = 'FeaturedPanel';
FeaturedPanel.AUTHOR_HOVER_START = 'FeaturedPanel.AUTHOR_HOVER_START';
FeaturedPanel.BUY_CLICKED = 'FeaturedPanel.BUY_CLICKED';
FeaturedPanel.INITIALIZED = 'FeaturedPanel.INITIALIZED';
FeaturedPanel.NEXT = 'FeaturedPanel.NEXT';
FeaturedPanel.PREVIOUS = 'FeaturedPanel.PREVIOUS';
FeaturedPanel.TOGGLE_FEATURED = 'FeaturedPanel.TOGGLE_FEATURED';
FeaturedPanel.UPDATE_ZAPPGADGET = 'FeaturedPanel.UPDATE_ZAPPGADGET';
FeaturedPanel.RETURN_FEATURED_PANEL_LIST = 'FeaturedPanel.RETURN_FEATURED_PANEL_LIST';
FeaturedPanel.RETURN_FEATURED_PANEL_LIST_FAILURE = 'FeaturedPanel.RETURN_FEATURED_PANEL_LIST_FAILURE';
ScriptLoader.loaded(FeaturedPanel.NAME);
