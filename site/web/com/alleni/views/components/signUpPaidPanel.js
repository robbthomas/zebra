var SignUpPaidPanel = function()
{
    this.Extends = UIComponent;
    this.values = {};
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function ()
    {
        this.parent('signUpPaidPanel');

        var that = this;
        var o = {
            filename: 'signUpPaidPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function ()
    {
        this.set('html', fragment);
        var that = this;
        // step one items
        jQuery('div#stepOne a').click(function (event) {
            event.preventDefault();
            var accountType = this.hash.substring(1);
            that.values['accountType'] = accountType;
            that.values['accountTypeDescription'] = that.accountTypeDescription(accountType)
            that.displayStep('stepTwo');
        });
        // step two items
        jQuery('div#stepTwo a').click(function (event) {
            event.preventDefault();
            that.addValues(jQuery('div#stepTwo form').serializeArray());
            that.values['firstLastName'] = that.values['firstName'] + ' ' + that.values['lastName'];
            that.values['companyCityStateProvinceZipPostalCode'] = that.values['companyCity']+', '+that.values['companyStateProvince']+' '+that.values['companyZipPostalCode'];
            that.displayStep('stepThree');
        });
        // step three items
        jQuery('div#stepThree a').click(function (event) {
            event.preventDefault();
            that.addValues(jQuery('div#stepThree form').serializeArray());
            that.values['creditCardNumberMasked'] = 'xxxxxxxxxxx'+that.values['creditCardNumber'].substring(11);
            that.values['expirationDate'] = that.values['expirationMonth']+'/'+that.values['expirationYear'];
            that.values['billingCityStateProvinceZipPostalCode'] = that.values['billingCity']+', '+that.values['billingStateProvince']+' '+that.values['billingZipPostalCode'];
            jQuery('#billingAddress2row').toggle(that.values['billingAddress2'] != undefined && that.values['billingAddress2'] != '');
            that.refreshReviewValues(that.values);
            that.displayStep('stepFour');
        });
        // step four items
        jQuery('div#stepFour a').click(function (event) {
            event.preventDefault();
            that.fireEvent(SignUpPaidPanel.SUBMIT, {form: that.values});
        });
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(SignUpPaidPanel.INITIALIZED);
        }
        if (this.initialized)
        {
            this.values = {};
            jQuery(':input').each(function (index, element) {
                var q = jQuery(this);
                switch (this.localName)
                {
                case 'input':
                    if (q.context.type == 'checkbox')
                        q.attr('checked', false);
                    else
                        q.val('');
                    break;
                case 'select':
                    q.val(-1);
                    break;
                default:
                   //Log.debug('SignUpPaidPanel.initializationComplete - unknown localName:'+this.localName);
                    break;
                }
                var x = 0;
            });
            this.displayStep('stepOne');
        }
    }
    this.displayStep = function (step)
    {
        this.dumpValues();
        jQuery('div#signUpPaidPanel .step').toggle(false);
        jQuery('div#signUpPaidPanel #'+step).toggle(true);
    }
    this.addStates = function (states)
    {

    }
    this.addCountries = function (countries)
    {

    }
    this.accountTypeDescription = function (accountType)
    {
        // TODO acavan - where do we get these values?
        var accountTypeDescriptions = {
            basic: 'Basic - $4.99 a month',
            standard: 'Standard - $4.99 a month',
            premium: 'Premium - $4.99 a month'
        }
        return accountTypeDescriptions[accountType];
    }
    this.refreshReviewValues = function (values)
    {
        jQuery('div#stepFour .review').each(function (index, element) {
            jQuery(this).text(values[this.id]);
        })
    }
    this.dumpValues = function ()
    {
        var s = '';
        for (var i in this.values)
        {
            s += '   '+i+':'+this.values[i]+'\n';
        }
       //Log.debug('SignUpPaidPanel.dumpValues\n'+s);
    }
    this.addValues = function (valueArray)
    {
        for (var i = 0; i < valueArray.length; i++)
            this.values[valueArray[i].name] = valueArray[i].value;
    }
}
SignUpPaidPanel = new Class(new SignUpPaidPanel());
SignUpPaidPanel.NAME = 'SignUpPaidPanel';
SignUpPaidPanel.INITIALIZED = 'SignUpPaidPanel.INITIALIZED';
SignUpPaidPanel.SUBMIT = 'SignUpPaidPanel.SUBMIT';
ScriptLoader.loaded(SignUpPaidPanel.NAME);
