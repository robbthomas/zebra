var FooterPanel = function()
{
    this.Extends = UIComponent;
   
    this.initialize = function(parent)
    {
        this.parent(parent);
        var that = this;
        var o = {
            filename: 'footerPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);

        this.initializeChildren();
        this.childrenInitialized();
        this.initializationComplete();
    }
    this.initializationComplete = function ()
    {
        this.fireEvent(FooterPanel.INITIALIZED);
    }
}
FooterPanel = new Class(new FooterPanel());
FooterPanel.NAME = 'FooterPanel';
FooterPanel.INITIALIZED = 'FooterPanel.INITIALIZED'
FooterPanel.LINK_SELECTED = 'FooterPanel.LINK_SELECTED';

