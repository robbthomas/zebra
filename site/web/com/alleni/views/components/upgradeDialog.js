var UpgradeDialog = function()
{
    this.Extends = UIComponent;
    this.dialog = undefined;
    this.item = undefined;
    this.values = {};
    this._plan = undefined;
    this.usernameCache = [];
    this.emailCache = [];
    this.joinProxy = null;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.accountInfo = undefined;
    this.animStart = undefined;
    this.upgradeTo = "";
    this.mode = UpgradeDialog.MODE_UPGRADE_WITH_PURCHASE;
    
    this.initialize = function ()
    {
        //Log.debug('UpgradeDialog.initialize');
        this.parent('upgradeDialog');
        
        var that = this;
        var o = {
            filename: 'upgradeDialog.html',
            success: function (fragment, status) {
                //Log.debug('UpgradeDialog.initialize.success');
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        //Log.debug('UpgradeDialog.processFragment - @top');
        this.set('html', fragment);
        var that = this;
        this.dialog = jQuery('div#upgradeDialog').dialog({
        	draggable: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            width: 'auto',
            height: 'auto'
        });
        //override draggable settings and assign handle to .blockTop
        jQuery('div#upgradeDialog').data('dialog').uiDialog.draggable('option', {
            cancel: '.ui-dialog-titlebar-close',
            handle: '.ui-dialog-titlebar, div#upgradeDialog .blockTop'
        });
        // set currentDates as needed
        var now = new Date();
        var test = jQuery('#currentDate', this.element);
        jQuery('#currentDate', this.element).text(now.format('mmmm dS, yyyy'));
        
        Log.debug('UpgradeDialog.processFragment 1 - className:'+document.getElementById('upgradeDialog').className);
        jQuery('div#upgradeDialog a.close').click(function (event) {
            that.close();
            return false;
        });   
        
        jQuery('div#upgradeDialog div#stepOneVisitor #creatorButton').click(function (event) {
        	that.upgradeTo = "creator";
        	that.displayStep('stepCouponVisitor');
        	return false;
        });
        
        jQuery('div#upgradeDialog div#stepOneVisitor #proButton').click(function (event) {
        	that.upgradeTo = "pro";
        	that.displayStep('stepCouponVisitor');
        	return false;
        });
        

       
        jQuery('div#upgradeDialog div#stepCouponVisitor').keypress(function (e) {
            
            if (e.which == 13){
            	// swallow for now
            	// TODO: handle stepping through forms on enter
            	return false;
            }else{
            	return true;
            }
            
        });
        
        jQuery('div#upgradeDialog div#stepCouponVisitor a').click(function (event) {
            switch (this.hash.substring(1)) {
                case 'close':
                    that.close();
                    break;
                case 'skip':
                	that.values['couponCode'] = undefined;
                	if(Credentials.hasCreditCardProfile() && Credentials.goodStanding())
            		{
                		// proceed to upgrade
                		that.fireEvent(UpgradeDialog.UPGRADE, that.accountInfo.type);
            		}else if(Credentials.hasCreditCardProfile() && !Credentials.goodStanding()){
            			that.displayStep('updateBilling');
            		}else{
            			that.displayStep('stepThreeVisitor');
            		}
                	
                	break;
                default:
	            if (jQuery('div#upgradeDialog div#stepCouponVisitor form').valid())
	            {
	                that.addValues(jQuery('div#stepCouponVisitor form').serializeArray());
	                that.values['firstLastName'] = Credentials.firstName() + ' ' + Credentials.lastName();
	                // validate couponCode
	                var code = jQuery('div#upgradeDialog div#stepCouponVisitor form #couponCode').val();
	                that.fireEvent(UpgradeDialog.VALIDATE_COUPON, code);
	                that.showUpdateAnimation(this);
	            }
            }
            return false;
        });
 
        jQuery('div#upgradeDialog div#stepThreeVisitor a').click(function (event) {
            switch (this.hash.substring(1)) {
                case 'back':
                    that.displayStep('stepCouponVisitor');
                    break;
                case 'close':
                    that.close();
                    break;
                default:
	            if (jQuery('div#upgradeDialog div#stepThreeVisitor form').valid()) {
	            	that.addValues(jQuery('div#stepThreeVisitor form').serializeArray());
	                
	                if (that.values['creditCardNumber'] == undefined) {
	                    that.values['creditCardNumberMasked'] = '<unknown>';
	                } else {
	                	that.values['creditCardNumberUnmasked'] = that.values['creditCardNumber'];
	                	that.values['creditCardNumberMasked'] = '';
	                	for (var i = 0; i < that.values['creditCardNumber'].length - 4; i++) {
	                		that.values['creditCardNumberMasked'] = that.values['creditCardNumberMasked'] + 'x';
	                	}
	                    that.values['creditCardNumberMasked'] = that.values['creditCardNumberMasked'] + that.values['creditCardNumber'].substring(that.values['creditCardNumber'].length - 4);
	                }
	                
	                that.ccNumberIsMasked = true;
	                
	                that.values["accountType"] = that.accountInfo.type;
	                that.values['expirationDate'] = that.values['expirationMonth']+'/'+that.values['expirationYear'];
	                that.values['billingCityStateProvinceZipPostalCode'] = that.values['billingCity']+', '+that.values['billingStateProvince'].toUpperCase()+' '+that.values['billingZipPostalCode'];
	                
	                switch(that.values['billingCountry']) {
	                	case 'us':
	                		that.values['billingCountry'] = 'United States';
	                		break;
	                	default:
	                		that.values['billingCountry'] = that.values['billingCountry'].toUpperCase();
	                }
	                
	                jQuery('div#stepFourVisitor .memberInfo').hide();
	                jQuery('div#stepFourVisitor .coupon').toggle(that.values['couponCode'] != undefined);
	            	jQuery('div#stepFourVisitor .billingInfo').toggle(that.values['couponCode'] == undefined);
	            	jQuery('#billingAddress2row').toggle(that.values['billingAddress2'] != undefined && that.values['billingAddress2'] != '');
	                that.refreshReviewValues('div#stepFourVisitor .review', that.values);
	                //This needs to be done after refreshReviewValues() because that method is updating the fields. Nothing like reinventing wheels like "HTML FORM"
	                jQuery('div#stepFourVisitor .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberMasked']);
	                that.displayStep('stepFourVisitor');
	            }
            }
            return false;
        });
        
        jQuery('div#stepFourVisitor .billingInfo .cc #toggleCCMasking').click(function (event) {
        	if (that.ccNumberIsMasked) {
        		jQuery('div#stepFourVisitor .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberUnmasked']);
        		jQuery('div#stepFourVisitor .billingInfo .cc span#toggleCCMaskingLabel').text('Hide');
        	} else {
        		jQuery('div#stepFourVisitor .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberMasked']);
        		jQuery('div#stepFourVisitor .billingInfo .cc span#toggleCCMaskingLabel').text('Show');
        	}
        	
        	that.ccNumberIsMasked = !that.ccNumberIsMasked;
        	this.checked = false;
        });
        
        jQuery('div#upgradeDialog div#stepFourVisitor a').click(function (event) {
            switch (this.hash.substring(1)) {
	            case 'close':
	                that.close();
	                break;
	            case 'back':
                    that.displayStep('stepThreeVisitor');
                    break;
                case 'editBilling':
                	that.displayStep('stepThreeVisitor');
                    break;
                default:
                	if(!Credentials.authenticated())
                	{
                		that.fireEvent(UpgradeDialog.JOIN, that.values);
                	}else{
                		//TODO: Fix [JAB]
                		//jQuery('span#firstLastName').text(that.values['firstLastName']);
                		
                		//Make sure we're submitting the unmasked cc number
    	            	jQuery('div#stepFourVisitor .billingInfo .cc #creditCardNumber').hide();
    	            	jQuery('div#stepFourVisitor .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberUnmasked']);
                		that.fireEvent(UpgradeDialog.UPDATE, {formName:'paymentMethod',values:that.values});
                	}
                	that.showUpdateAnimation(this);
                
                
                
                	break;
            }
            return false;
        });
        jQuery('div#upgradeDialog div#stepOneMember a').click(function (event) {
            switch (this.hash.substring(1)) {
                case 'buy':
                    that.fireEvent(UpgradeDialog.MEMBER_PURCHASE_ITEM, {itemId: that.item.id});
                    //that.close(); // cant close this, need to listen for the success
                    return false; 
            }
        });
        
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
        //Log.debug('upgradeDialog.processFragment - @bottom');
    }

    this.onSubmitFailure = function(payload)
    {
    	this.hideUpdateAnimation();
    	alert(payload.error + ":\n" + payload.detail);
    }
    
    this.onCouponValidateSuccess = function(payload)
    {
    	var that = this;
    	this.hideUpdateAnimation();
    	if(payload.couponValid){
    		
    		jQuery('div#stepFourVisitor .billingInfo').hide();
    		
    		if((this.item != undefined && this.item.price != 0) && (!Credentials.hasCreditCardProfile() || !Credentials.goodStanding()))
			{
    			this.displayStep('stepThreeVisitor');
			}else{
				// proceed to upgrade
				jQuery('div#stepFourVisitor .memberInfo').hide();
				jQuery('div#stepFourVisitor .billingInfo').toggle(this.values['couponCode'] == undefined);
				jQuery('div#stepFourVisitor .coupon').toggle(this.values['couponCode'] != undefined);
				jQuery('#billingAddress2row').toggle(that.values['billingAddress2'] != undefined && that.values['billingAddress2'] != '');
                that.refreshReviewValues('div#stepFourVisitor .review', that.values);
                that.displayStep('stepFourVisitor');
			}
    	}else{
    		jQuery('#stepCouponVisitor .couponDenied').show();
    	}
    }
    this.onCouponValidateFailure = function(payload)
    {
    	this.hideUpdateAnimation();
    	alert(payload.errorDetail);
    }
    
    this.onUpgradeSuccess = function(payload)
    {
    	this.refreshReviewValues('div#stepOneMember .review', this.values);
    	jQuery('div#upgradeDialog .freshJoinOnly', this.element).show();
    	jQuery("div#upgradeDialog .creditCardDenied", this.element).hide();
    	jQuery("div#upgradeDialog .couponAccepted", this.element).hide();
    	jQuery("div#upgradeDialog .couponDenied", this.element).hide();
    	this.displayStep('stepOneMember');
    }
    
    this.onUpgradeFailure = function(payload)
	{
    	this.close();
    	alert(payload.detail);
	}
    
    this.onUpdateSuccess = function(payload)
    {
    	jQuery('div#upgradeDialog .freshJoinOnly').show();
    	jQuery(".creditCardDenied").toggle(!payload.validCreditCard && this.values["creditCardNumber"] != undefined);
    	
    		this.refreshReviewValues('div#stepOneMember .review', this.values);
    		if(payload.couponSubmitted)
    		{
        		jQuery(".couponAccepted").toggle(payload.couponApplied);
        		jQuery(".couponDenied").toggle(!payload.couponApplied);
        		if(payload.couponApplied){
        			
    	    		var termDueDate = Common.parseDate(payload.termDueDate.split(' ')[0]);
    		        jQuery('#currentDatePlus', this.element).text(termDueDate.format('mmmm dS, yyyy'));
        		}
    		}else{
    			jQuery(".couponAccepted").hide();
        		jQuery(".couponDenied").hide();
    		}
    		this.displayStep('stepOneMember');

    	if(payload.couponApplied || payload.validCreditCard){
    		this.fireEvent(UpgradeDialog.UPGRADE, this.upgradeTo);
    	}
    }
    
    this.onUpdateFailure = function(payload)
	{
    	this.close();
    	alert(payload.errorDetail);
	}
    
    this.showUpdateAnimation = function(target)
    {
    	this.animStart = (new Date()).valueOf();
        jQuery(target).after('<img class="updatingAnimation" src="com/alleni/images/updating.gif"/>');
    }
    
    this.hideUpdateAnimation = function()
    {
        var remainder = 2000 - ((new Date()).valueOf() - this.animStart);

        if (remainder < 0) {
            jQuery('.updatingAnimation').remove();
        }
        else {
            setTimeout(function(){
                jQuery('.updatingAnimation').remove();
            },
            2000);
        }

    }
    
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(UpgradeDialog.INITIALIZED);
        }
    }
    this.setJoinProxy = function(joinProxy)
    {
        this.joinProxy = joinProxy;
    }

    this.refreshReviewValues = function (selector, values)
    {
        jQuery(selector).each(function (index, element) {
            jQuery(this).text(values[this.id]);
        })
    }
    this.displayStep = function (step)
    {
    	this.accountInfo = this.joinProxy.plan(this.upgradeTo);
    	var displayAccountName = "Unknown";
    	switch(this.accountInfo.name) 
    	{
    	case 'Pro':
    		displayAccountName = 'Professional';
    		break;
    	default:
    		displayAccountName = this.accountInfo.name.charAt(0).toUpperCase() + this.accountInfo.name.slice(1);
    		break;
    	}
    	jQuery('div#upgradeDialog span.accountType').text(displayAccountName);
        jQuery('div#upgradeDialog span.accountCost').text(this.accountInfo.formattedPrice);
        if (this.upgradeTo == 'collector') {
        	jQuery('div#upgradeDialog span.requiredUpgradeAccountType').text('Creator or Professional');
        }
        //Log.debug('upgradeDialog.displayStep - step:'+step);
        jQuery('div#upgradeDialog .step').hide();
        jQuery('div#upgradeDialog #'+step).show();
    }
    this.clearForm = function ()
    {
    	jQuery(':input', this.element).val('');
    }
   
    this.configure = function (plan)
    {
        //Log.debug('JoinPanel.configure');

    }
    
    
    
    this.open = function (config)
    {
    	var o = config.item;
    	this.upgradeTo = config.upgradeTo;
    	// if no store item was provided
    	if(o == undefined){
    		this.accountInfo = this.joinProxy.plan(this.upgradeTo);
    	}else{
    	// if a store item was provided
    		this.item = o;
    		this.accountInfo = this.joinProxy.plan(TypeLookup.accountRequired(o._type));
    		// grab the account type required for this product
    		this.upgradeTo = this.accountInfo.type;
    	}
    	// determine if this is an account upgrade, or just a payment setting upgrade
    	var paymentSettingOnly = false;
    	if(Credentials.accountTypeTag() == this.upgradeTo && 
    			!Credentials.hasCreditCardProfile()){
    		paymentSettingOnly = true;
    	}
    	
    	
        var formattedPrice = "Open "+this.accountInfo.name+" Account";
        var description = '"'+this.accountInfo.name+'" Account - '+this.accountInfo.formattedPrice;
        
        jQuery('div#upgradeDialog div#stepThreeVisitor input.[name="accountType"]').val(this.accountInfo.type);

        jQuery('div#upgradeDialog span.accountType').text(this.accountInfo.name);
        jQuery('div#upgradeDialog span.accountCost').text(this.accountInfo.formattedPrice);
        jQuery('h2.planDescription', this.element).text(description).toggleClass('freeTrial', this.accountInfo.price != 0);
        jQuery('h3.planSubtitle', this.element).toggle(this.accountInfo.price != 0);
        jQuery('.couponProvided', this.element).toggle(this.values['couponCode'] != undefined);
        jQuery('.couponNotProvided', this.element).toggle(this.values['couponCode'] == undefined);
        
        jQuery('.collectorToggle').toggle(this.accountInfo.type == 'collector');
        jQuery('.creatorToggle').toggle(this.accountInfo.type == 'creator'|| this.accountInfo.type == 'pro' );
        
        var now = new Date();
        jQuery('div#upgradeDialog #currentDate', this.element).text(now.format('mmmm dS, yyyy'));

        
        this.usernameCache = [];
        this.emailCache = [];
       
        ChangeTag.toText(
            jQuery('div#upgradeDialog div#stepOneVisitor div.columnRight [name="password"]'),
            jQuery('div#upgradeDialog div#stepOneVisitor div.columnRight a.button')
        )
        if(paymentSettingOnly){
        	jQuery('div#upgradeDialog .name').each(function (element, index) {
                jQuery(this).text(ZappGadgetProject.name(o.name));
            });
            jQuery('div#upgradeDialog .description').each(function (element, index) {
                jQuery(this).text(ZappGadgetProject.description(o.description));
            });
            jQuery('div#upgradeDialog .icon img').each(function (element, index) {
                jQuery(this).attr('src', o.icon);
            });
            jQuery('div#upgradeDialog .details').each(function(element, index){
                jQuery(this).attr('href', IdType.detailUrl(o.urlName, IdType.URL_NAME));
            });
            jQuery('div#upgradeDialog .price').each(function (element, index) {
                jQuery(this).text(o.formattedPrice);
            });
            jQuery('div#upgradeDialog .accountDescription').hide();
            jQuery('div#upgradeDialog .accountCost').hide();
            jQuery('div#upgradeDialog .upgradeInfo').hide();
            jQuery('div#upgradeDialog .itemInfo').show();
            this.displayStep('stepThreeVisitor');
        }else if(o != undefined){
        	jQuery('div#upgradeDialog span.type').text(TypeLookup.singular(o._type));
            jQuery('div#upgradeDialog #stepOneVisitor .warningColor').hide();
            jQuery('div#upgradeDialog #stepOneVisitor [name="email"]').val('Email');
            jQuery('div#upgradeDialog .name').each(function (element, index) {
                jQuery(this).text(ZappGadgetProject.name(o.name));
            });
            jQuery('div#upgradeDialog .description').each(function (element, index) {
                jQuery(this).text(ZappGadgetProject.description(o.description));
            });
            jQuery('div#upgradeDialog .icon img').each(function (element, index) {
                jQuery(this).attr('src', o.icon);
            });
            jQuery('div#upgradeDialog .details').each(function(element, index){
                jQuery(this).attr('href', IdType.detailUrl(o.urlName, IdType.URL_NAME));
            });
            jQuery('div#upgradeDialog .price').each(function (element, index) {
                jQuery(this).text(o.formattedPrice);
            });
            jQuery('div#upgradeDialog .accountDescription').show();
            jQuery('div#upgradeDialog .upgradeInfo').hide();
            jQuery('div#upgradeDialog .itemInfo').show();
            this.displayStep('stepOneVisitor');
        }else{
        	jQuery('div#upgradeDialog .upgradeInfo').show();
        	jQuery('div#upgradeDialog .itemInfo').hide();
        	this.displayStep('stepCouponVisitor');
        }

        jQuery('div#upgradeDialog .paid').toggle(this.accountInfo.price != 0);
        jQuery('div#upgradeDialog .free').toggle(this.accountInfo.price == 0);
        jQuery('div#upgradeDialog .freshJoinOnly').toggle(!Credentials.authenticated());
        
        this.dialog.dialog('open');
        this.clearForm();
    }
    
    this.onLoginBadCredentials = function()
    {
        jQuery('div#upgradeDialog #stepOneVisitor .warningColor').show();
    }
    this.close = function ()
    {
        this.dialog.dialog('close');
    }
    this.addValues = function (valueArray)
    {
        for (var i = 0; i < valueArray.length; i++)
            this.values[valueArray[i].name] = valueArray[i].value;
    }
}
UpgradeDialog = new Class(new UpgradeDialog());
UpgradeDialog.NAME = 'UpgradeDialog';
UpgradeDialog.INITIALIZED = 'UpgradeDialog.INITIALIZED';
UpgradeDialog.MEMBER_SIGN_IN = 'UpgradeDialog.MEMBER_SIGN_IN';
UpgradeDialog.SUBMIT = "UpgradeDialog.SUBMIT";
UpgradeDialog.VALIDATE_COUPON = "UpgradeDialog.VALIDATE_COUPON";
UpgradeDialog.MEMBER_PURCHASE_ITEM = 'UpgradeDialog.MEMBER_PURCHASE_ITEM';
UpgradeDialog.VISITOR_PURCHASE_ITEM = 'UpgradeDialog.VISITOR_PURCHASE_ITEM';
UpgradeDialog.EMAIL_UNIQUE = 'UpgradeDialog.EMAIL_UNIQUE';
UpgradeDialog.USERNAME_UNIQUE = 'UpgradeDialog.USERNAME_UNIQUE';
UpgradeDialog.JOIN = 'UpgradeDialog.JOIN';
UpgradeDialog.UPDATE = 'UpgradeDialog.UPDATE';
UpgradeDialog.UPGRADE = 'UpgradeDialog.UPGRADE';
UpgradeDialog.MODE_UPGRADE_WITH_PURCHASE = 'UpgradeDialog.MODE_UPGRADE_WITH_PURCHASE';
UpgradeDialog.MODE_UPGRADE_ONLY = 'UpgradeDialog.MODE_UPGRADE_ONLY';

// ScriptLoader.loaded(UpgradeDialog.NAME);
