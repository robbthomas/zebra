var SharingLinks = function()
{
    this.ICON_GONE = '_hidden';				        // icon opacity for non-available ever
    this.ICON_DISABLED = '_disabled';				// icon opacity for non-available at this account type
    this.ICON_ENABLED = '_enabled';					// icon opacity for available

    this.ACCOUNT_PRO = "pro";
    this.ACCOUNT_CREATOR = "creator";
    this.ACCOUNT_COLLECTOR = "collector";
    
    this.PROJECT_TYPE_CREATOR = "Creator";
    this.PROJECT_TYPE_PRO = "Pro";
    
    this.ZAPP = "zapp";
    this.GADGET = "gadget";

    this.PRIV_SERVER = 0;
    this.PRIV_GUEST = 1;
    this.PRIV_HIDDEN = 2;
    this.PRIV_SHOP = 3;
    
    this.access = [];
    this.noAccess = [];

	this.shareAllowed = function(privacyLevel, accountType, projectType, projectAccountTypeName) {
		
			
			switch(accountType) {
			case this.ACCOUNT_PRO:
				if(projectType == "gadget"){
					switch(privacyLevel) {
						case this.PRIV_GUEST:  return ["guest"];
						case this.PRIV_HIDDEN: return ["guest",					"link", "email", "facebook", "twitter", "linkedin"];
						case this.PRIV_SHOP:   return ["guest",					"link", "email", "facebook", "twitter", "linkedin"];
					}
				}else{
					switch(privacyLevel) {
						case this.PRIV_GUEST:  return ["guest", "embed", "lms"];
						case this.PRIV_HIDDEN: return ["guest", "embed", "lms", "link", "email", "facebook", "twitter", "linkedin"];
						case this.PRIV_SHOP:   return ["guest", "embed", "lms", "link", "email", "facebook", "twitter", "linkedin"];
					}
					/*if(projectAccountTypeName ==  this.PROJECT_TYPE_PRO){
						switch(privacyLevel) {
							case this.PRIV_GUEST:  return ["guest", "embed", "lms"];
							case this.PRIV_HIDDEN: return ["guest", "embed", "lms", "link", "email", "facebook", "twitter", "linkedin"];
							case this.PRIV_SHOP:   return ["guest", "embed", "lms", "link", "email", "facebook", "twitter", "linkedin"];
						}
						
					}else{
						switch(privacyLevel) {
							case this.PRIV_GUEST:  return ["guest", "embed"];
							case this.PRIV_HIDDEN: return ["guest", "embed", "link", "email", "facebook", "twitter", "linkedin"];
							case this.PRIV_SHOP:   return ["guest", "embed", "link", "email", "facebook", "twitter", "linkedin"];
						}
					}*/
					
				}
				
			case this.ACCOUNT_CREATOR:
				switch(privacyLevel) {
					case this.PRIV_GUEST:  return [];
					case this.PRIV_HIDDEN: return [                         "link", "email"                                   ];
					case this.PRIV_SHOP:   return [         "embed",        "link", "email", "facebook", "twitter", "linkedin"];
				}
			default:
				switch(privacyLevel) {
					case this.PRIV_GUEST:  return [];
					case this.PRIV_HIDDEN: return [];
					case this.PRIV_SHOP:   return [                         "link", "email", "facebook", "twitter", "linkedin"];
				}
			}

		
		
		
		return [];
	}
	
	this.isAppEmbedable = function(userAccessPrivs){
		if(userAccessPrivs.embed){
			return "embed"
		}
		return null;
	}
	
	this.shareNotAllowed = function(privacyLevel, accountType, projectType, projectLevel) {
		var all = ["guest", "embed", "lms", "link", "email", "facebook", "twitter", "linkedin"];
		var that = this;
		return all.filter(function(i) {return !(that.access.indexOf(i) > -1);});
	}

	this.setVisibility = function(privIndex, privValue, userAccountType, projectAccountTypeName, iconQueryFunction, callback, projectType, userAccessPrivs) {
		// disable all icons
		var that = this;
		
		iconQueryFunction(privValue).fadeTo(0,0);
		
		that.access = that.shareAllowed(privIndex, that.ACCOUNT_PRO);
		jQuery.each(that.access, function(shareIndex, shareValue) {
			// enable all accessible icons
			iconQueryFunction(privValue, shareValue).fadeTo(0,.4);
			
		});
		that.access = that.shareAllowed(privIndex, userAccountType, projectType, projectAccountTypeName);
		var accessPrivCheck = this.isAppEmbedable(userAccessPrivs);
		if(accessPrivCheck != null && projectType != that.GADGET) that.access.push(accessPrivCheck);
		that.noAccess = that.shareNotAllowed(privIndex, userAccountType, projectType, projectAccountTypeName);
		jQuery.each(that.access, function(shareIndex, shareValue) {
			// enable all accessible icons
			iconQueryFunction(privValue, shareValue).fadeTo(0,1);
		});
		jQuery.each(that.noAccess, function(shareIndex, shareValue) {
			// remove clicks for inactive links
			iconQueryFunction(privValue, shareValue).removeAttr("href");
		});
		
		
		if(callback) {
			callback(privValue, that.access.length > 0);
		}
	};
}
SharingLinks = new Class(new SharingLinks());
