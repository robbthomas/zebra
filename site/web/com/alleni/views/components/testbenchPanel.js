var TestbenchPanel = function()
{
    this.Extends = UIComponent;
    this.testsPanel = null;
    this.testForm = null;
    this.jsonPanel = null;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function ()
    {
        this.parent('testbenchPanel');

        var that = this;
        var o = {
            filename: 'testbenchPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    };
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.childInitialized = this.childInitialized.bindWithEvent(this);

        this.testsPanel = new TestbenchTestsPanel('testsPanel');
        this.testsPanel.addEvent(TestbenchTestsPanel.INITIALIZED, this.childInitialized);

        this.testForm = new TestbenchTestForm('testForm');
        this.testForm.addEvent(TestbenchTestForm.INITIALIZED, this.childInitialized);

        this.jsonPanel = new TestbenchJSONPanel('jsonPanel');
        this.jsonPanel.addEvent(TestbenchJSONPanel.INITIALIZED, this.childInitialized);

        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializeChildren = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            Log.debug('ListPanel.initializeChildren');
            this.addChild(this.testsPanel);
            this.addChild(this.testForm);
            this.addChild(this.jsonPanel);
            this.addChild(new UIComponent(new Element('div', {'class': 'clearer'})));
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.allChildrenInitialized())
        {
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(TestbenchPanel.INITIALIZED);
        }
    }
    this.childInitialized = function ()
    {
        this.initializationComplete();
    }
    this.allChildrenInitialized = function ()
    {
        return (this.testsPanel.initialized && this.testForm.initialized && this.jsonPanel.initialized);
    }
};
TestbenchPanel = new Class(new TestbenchPanel());
TestbenchPanel.NAME = 'TestbenchPanel';
TestbenchPanel.INITIALIZED = 'TestbenchPanel.INITIALIZED';
ScriptLoader.loaded(TestbenchPanel.NAME);