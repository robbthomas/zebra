var ApplicationPanel = function()
{
    this.Extends = UIComponent;

    this.headerPanel = null;
    this.popupPanel = null;
    this.contentPanel = null;
    this.footerPanel = null;
    this.sidebarPanel = null;
    this.headerIsInit = false;
    this.popupIsInit = false;
    this.footerPanelIsInit = false;
    this.contentPanelIsInit = false;
    this.sidebarPanelIsInit = false;
    /**
     * 
     */
    this.initialize = function (parent)
    {
        this.parent("applicationPanel");
    }
    /**
     * 
     */
    this.initializeChildren = function ()
    {
        this.headerPanel = new HeaderPanel('headerPanel');
        this.headerInit = this.headerInit.bindWithEvent(this);
        this.headerPanel.addEvent(HeaderPanel.INITIALIZED, this.headerInit);
        //this.addChild(this.headerPanel, '#topClass');

        this.popupPanel = new PopupPanel('popupPanel');
        this.popupInit = this.popupInit.bindWithEvent(this);
        this.popupPanel.addEvent(PopupPanel.INITIALIZED, this.popupInit);
        //this.addChild(this.popupPanel);
        
        this.contentPanel = new ContentPanel('contentPanel');
        this.contentPanelInit = this.contentPanelInit.bindWithEvent(this);
        this.contentPanel.addEvent(ContentPanel.INITIALIZED, this.contentPanelInit);
        this.addChild(this.contentPanel,'#mainPanel');
        jQuery('div#mainPanel').append(Common.clearerElement());
        
        this.sidebarPanel = new SidebarPanel('sidebarPanel');
        this.sidebarPanelInit = this.sidebarPanelInit.bindWithEvent(this);
        this.sidebarPanel.addEvent(SidebarPanel.INITIALIZED, this.sidebarPanelInit);
        //this.addChild(this.sidebarPanel, '#contentPanel');
        
        this.footerPanel = new FooterPanel('footerPanel');
        this.footerPanelInit = this.footerPanelInit.bindWithEvent(this);
        this.footerPanel.addEvent(FooterPanel.INITIALIZED, this.footerPanelInit);
        //this.addChild(this.footerPanel);
    }
    /**
     * 
     */
    this.sidebarPanelInit = function() {
    	this.sidebarPanelIsInit = true;
    	this.checkParts();
    }
    /**
     * 
     */
    this.contentPanelInit = function() {
    	this.contentPanelIsInit = true;
    	this.checkParts();
    }
    /**
     * 
     */
    this.headerInit = function() {
    	this.headerIsInit = true;
    	this.checkParts();
    }
    /**
     * 
     */
    this.popupInit = function() {
    	this.popupIsInit = true;
    	this.checkParts();
    }
    /**
     * 
     */
    this.footerPanelInit = function() {
    	this.footerPanelIsInit = true;
    	this.checkParts();
    }
    /**
     * 
     */
    this.checkParts = function() {
    	if(this.headerIsInit && this.popupIsInit && this.footerPanelIsInit && this.contentPanelIsInit && this.sidebarPanelIsInit) {
            this.fireEvent(ApplicationPanel.INITIALIZATION_COMPLETE);
    	}
    }
 
}
ApplicationPanel = new Class(new ApplicationPanel());
ApplicationPanel.INITIALIZATION_COMPLETE = 'ApplicationPanel.INITIALIZATION_COMPLETE';
