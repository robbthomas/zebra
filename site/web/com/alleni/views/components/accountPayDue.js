var AccountPayDue = function()
{
    this.Extends = UIComponent;
    this.dialog = undefined;
    this.viewName = undefined;

    this.initialize = function (viewName)
    {
    	this.viewName = (viewName != undefined)?viewName:AccountPayDue.NAME;
    	// same as calling super
    	this.parent(this.viewName);
    }
    
    this.setValues = function (firstname)
    {
    	var _this = this;
    	jQuery('.accontPayDueName', this.element).html(firstname);
    	jQuery('#accountLink', this.element).click(function (){
    		_this.fireEvent(AccountPayDue.CLOSE);
    	});
    	
    }
    
}
AccountPayDue = new Class(new AccountPayDue());
AccountPayDue.NAME = 'AccountPayDue';
AccountPayDue.LOAD_COMPLETE = 'AccountPayDue.LOAD_COMPLETE';
AccountPayDue.CLOSE = "AccountPayDue.CLOSE";