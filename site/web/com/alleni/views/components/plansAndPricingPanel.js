var PlansAndPricingPanel = function()
{
    this.Extends = UIComponent;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function ()
    {
       //Log.debug('PlansAndPricingPanel.initialize');
        this.parent('plansAndPricingPanel');
        
        var that = this;
        var o = {
            filename: 'plansAndPricingPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        var that = this;
        
        jQuery('#creatorButton').click(function(){
        	var href = 'creator';
        	that.fireEvent(PlansAndPricingPanel.PLAN_SELECTED, href);
        });
        
        jQuery('#proButton').click(function(){
        	var href = 'pro';
        	that.fireEvent(PlansAndPricingPanel.PLAN_SELECTED, href);
        });

        //Commented out for now -- but when Enterprise comes along, just uncomment this and it should work.
        /*
        jQuery('enterpriseButton').click(function(){
        	var href = 'enterprise';
        	that.fireEvent(PlansAndPricingPanel.PLAN_SELECTED, href);
        });
        */
        
        jQuery('div#pricing a').click(function (event) {
            var href = this.hash.substring(1);
            if (href == '/aboutZebra?contact-us') {
                return true;
            }         
            switch (href)
            {
            case 'collector':
            	that.fireEvent(PlansAndPricingPanel.PLAN_SELECTED, href)
            	return false;
            	break;
            case 'creator':
                that.fireEvent(PlansAndPricingPanel.PLAN_SELECTED, href);
                return false;
                break;
            case 'pro':
                that.fireEvent(PlansAndPricingPanel.PLAN_SELECTED, href);
                break;
            default:
                return true;
                break;
            }
        });
        
        //This must be done AFTER the click() binding above, to ensure that the view doesn't attempt to handle the click on the Contact Us link.
        jQuery('div#pricing a#contactUsLink').unbind('click');
        
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(PlansAndPricingPanel.INITIALIZED);
        }
    }
}
PlansAndPricingPanel = new Class(new PlansAndPricingPanel());
PlansAndPricingPanel.NAME = 'PlansAndPricingPanel';
PlansAndPricingPanel.INITIALIZED = 'PlansAndPricingPanel.INITIALIZED';
PlansAndPricingPanel.PLAN_SELECTED = 'PlansAndPricingPanel.PLAN_SELECTED';
ScriptLoader.loaded(PlansAndPricingPanel.NAME);
