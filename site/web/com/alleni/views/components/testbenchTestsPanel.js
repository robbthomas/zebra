var TestbenchTestsPanel = function()
{
    this.Extends = UIComponent;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function (parent)
    {
        this.parent(parent);
        var that = this;
        var o = {
            filename: 'testbenchTestsPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
       //Log.debug('TestbenchTestsPanel.processFragment');
        this.set('html', fragment);
        if (this._tests !== undefined)
        {
            this.loadTests(this._tests);
            delete this._tests;
        }

        this.initialized = true;
        this.fireEvent(TestbenchTestsPanel.INITIALIZED);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(TestbenchTestsPanel.INITIALIZED);
        }
    }
    this.loadTests = function (testList)
    {
        // testList is an array of objects {group, test, id}
        var testPanel = jQuery('#testsPanel dl');
        testPanel.html('');
        var lastGroup = '';
        var group = undefined;
        var groupCount = 0;
        var html = [];
        for (var i = 0; i < testList.length; i++)
        {
            var testItem = testList[i];
            if (testItem.group != lastGroup)
            {
                html.push('<dt id="group'+groupCount+'">' + testItem.group + '</d>\n');
                lastGroup = testItem.group;
                groupCount++;
            }
            html.push('<dd id="'+ testItem.id +'" style="display:none">'+ testItem.test +'</dd>\n');
        }
        testPanel.html(html.join(''));
        var that = this;
        testPanel.click(function (event) {           
            switch (event.target.localName)
            {
             case 'dt':
                jQuery('#'+event.target.id, testPanel).nextUntil('dt').toggle();
                break;
             case 'dd':
                jQuery('div#testbenchPanel div#testsPanel .selected').removeClass('selected');
                jQuery('div#testbenchPanel div#testsPanel #' + event.target.id).addClass('selected')

                that.fireEvent(TestbenchTestsPanel.TEST_SELECTED, event.target.id);
                break;
             default:
                break;
            }
        });
    }
}
TestbenchTestsPanel = new Class(new TestbenchTestsPanel());
TestbenchTestsPanel.NAME = 'TestbenchTestsPanel';
TestbenchTestsPanel.FRAGMENT_READY = 'TestbenchTestsPanel.FRAGMENT_READY';
TestbenchTestsPanel.INITIALIZED = 'TestbenchTestsPanel.INITIALIZED';
TestbenchTestsPanel.TEST_SELECTED = 'TestbenchTestsPanel.TEST_SELECTED';
ScriptLoader.loaded(TestbenchTestsPanel.NAME);
