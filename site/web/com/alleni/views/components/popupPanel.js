var PopupPanel = function()
{
    this.Extends = UIComponent;
 
    this.initialize = function(parent)
    {
        this.parent(parent);

        var that = this;
        var o = {
            filename: 'popupPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
                //Log.debug('PopupPanel.initialize - initialized');
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.processLinks();

	    this.initializeChildren();
	    this.childrenInitialized();
	    this.initializationComplete();
            
    }
    this.initializationComplete = function ()
    {
        this.fireEvent(PopupPanel.INITIALIZED);
    }
    this.processLinks = function () {
        var     that = this;
        
        jQuery('.lostPassword a').unbind('click').click(function(event){

            event.preventDefault();

            switch (this.hash) {
                 case '#cancel':
                    that.hidePopups();
                    break;
                case '#lostPassword-send':
                    var email = jQuery('.overlayContent #lostPassword-email').val();
                    that.fireEvent(PopupPanel.REQUEST_NEW_PASSWORD, email);
                    that.hidePopups();
                    break;
            }
            return false;
        });
    }

    this.lostPassword = function()
    {
        jQuery('#overlay').show();
        jQuery('#enterUsername').show();
    }

    this.resetPasswordSuccess = function(email)
    {
        jQuery('#emailSent #lostPasswordEmail').text(email);

        jQuery('#overlay').show();
        jQuery('#emailSent').show();
    }
    this.resetPasswordUserNotFound = function(email)
    {
        jQuery('#userNotFound #lostPasswordEmail').text(email);

        jQuery('#overlay').show();
        jQuery('#userNotFound').show();
    }

    this.hidePopups = function()
    {
        jQuery('#overlay').hide();
        jQuery('.overlayContent').hide();
    }
    
}
PopupPanel = new Class(new PopupPanel());
PopupPanel.INITIALIZED = 'PopupPanel.INITIALIZED';
PopupPanel.REQUEST_NEW_PASSWORD = 'PopupPanel.REQUEST_NEW_PASSWORD';