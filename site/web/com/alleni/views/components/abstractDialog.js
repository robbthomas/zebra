var abstractDialog = function()
{
    this.Extends = UIComponent;
    this.dialog = undefined;

    this.initialize = function (fragmentFileName, dialogDivName) {
        this.parent('abstractDialog');
        this.divName = dialogDivName;
        
        var that = this;
        var o = {
            filename: fragmentFileName,
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    
    this.getObjKeys = function(obj) {
    	var keys = [];
    	for(var key in obj){
    		keys.push(key);
    	}
   		return keys;
    }
    
    this.doConfig = function(behaviors) {
    	this.dialog = jQuery('div#' + this.divName).dialog({
        	draggable: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            width: 'auto',
            height: 'auto'
        });
        
        // Set up all action bindings in one little loop.
        var behaviorTypes = this.getObjKeys(behaviors);
        var currentAction = '';
        var currentJQElement = '';
        for(var i = 0; i < behaviorTypes.length; i++) {
        	currentAction = behaviors[behaviorTypes[i]];
        	for (var j = 0; j < currentActionTypes.length; j++) {
        		if (undefined != currentAction[j].elementId) {
        			currentJQElement = '#' + currentAction[j].elementId;
        		} else {
        			currentJQElement = '.' + currentAction[j].className;
        		}
        		
            	jQuery(currentJQElement, this.element)[behaviorTypes[i]](currentAction[j].actionContent);
            }
        }
    }
    
    this.processFragment = function (fragments) {
        var that = this;
        var i = 0 ;
    	
    	this.set('html', fragment);
    }
    
    // TODO: figure out if this is necessary for most or all dialogs
    this.showUpdateAnimation = function(target)
    {
    	this.animStart = (new Date()).valueOf();
        jQuery(target).after('<img class="updatingAnimation" src="com/alleni/images/updating.gif"/>');
    }
    
    // TODO: see above.
    this.hideUpdateAnimation = function()
    {
        var remainder = 2000 - ((new Date()).valueOf() - this.animStart);

        if (remainder < 0) {
            jQuery('.updatingAnimation').remove();
        } else {
            setTimeout(function(){jQuery('.updatingAnimation').remove();}, 2000);
        }
    }
   
    // Ahh, abstraction
    this.open = function (o) {
        this.dialog.dialog('open');
    }
    
    this.close = function () {
        this.dialog.dialog('close');
    }
}
abstractDialog = new Class(new abstractDialog());
abstractDialog.NAME = 'abstractDialog';
abstractDialog.INITIALIZED = 'abstractDialog.INITIALIZED';