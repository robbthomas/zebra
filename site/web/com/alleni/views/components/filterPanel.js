var FilterPanel = function()
{
    this.Extends = UIComponent;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function (parent)
    {
        if (parent == undefined)
            Log.error('FilterPanel.initialize - parent undefined');
        this.parent(parent);

        var that = this;
        var o = {
            filename: 'filterPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(FilterPanel.INITIALIZED);
        }
    }
}
FilterPanel = new Class(new FilterPanel());
FilterPanel.NAME = 'FilterPanel';
FilterPanel.INITIALIZED = 'FilterPanel.INITIALIZED';
ScriptLoader.loaded(FilterPanel.NAME);
