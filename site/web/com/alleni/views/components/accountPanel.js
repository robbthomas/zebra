var AccountPanel = function()
{
    this.Extends = UIComponent;
    this.formName = undefined;
    this.emailCache = [];
    
    this.initialize = function ()
    {
       //Log.debug('AccountPanel.initialize');
        this.parent('accountPanel');
        
        var that = this;
        var o = {
            filename: 'accountPanel.html',
            success: function (fragment, status) {
                //Log.debug('AccountPanel - success');
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.fireEvent(AccountPanel.INITIALIZED);
    }
    this.initializationComplete = function ()
    {
    	Log.warn('AccountPanel.initializationComplete()');
        
    }
    this.updateSuccess = function(payload)
    {
        //TODO acavan - straighten this out
        if (payload.accountUpdate.group == 'usernameEmailPassword') {
            if (payload.accountUpdate.values.newEmail != undefined) {
                jQuery('[name="newEmail"]').val('');
                jQuery('[name="confirmNewEmail"]').val('');
                Credentials.email(payload.accountUpdate.values.newEmail);
                // PMQ -- update header panel login email display -- this should be done instead by AccountPanel.REQUEST_REFRESH?
                jQuery('div.header #email').text(Credentials.email());
            }
            if (payload.accountUpdate.values.newPassword != undefined) {
                jQuery('[name="curPassword"]').val('');
                jQuery('[name="newPassword"]').val('');
                jQuery('[name="confirmNewPassword"]').val('');
                Credentials.password(payload.accountUpdate.values.newPassword);
                
            }
        }
        
        if(payload.data != undefined){
        	
        	if(payload.accountUpdate.group == 'paymentMethod'){

            	if(payload.data.validCreditCard){
            		jQuery("div#accountFormContainer .creditCardDeniedError").hide();
            		if(payload.data.couponSubmitted)
            		{
                		jQuery("div#accountFormContainer .couponAccepted").toggle(payload.data.couponApplied);
                		jQuery("div#accountFormContainer .couponDenied").toggle(!payload.data.couponApplied);
            		}else{
            			jQuery("div#accountFormContainer .couponAccepted").hide();
                		jQuery("div#accountFormContainer .couponDenied").hide();
            		}
            	}else{
            		jQuery("div#accountFormContainer .creditCardDeniedError").show();
            	}
                jQuery('[name="nameOnCreditCard"]').val('');
                jQuery('[name="creditCardNumber"]').val('');
                jQuery('[name="securityCode"]').val('');
                jQuery('[name="expirationMonth"]').prop('selectedIndex',0);
                jQuery('[name="expirationYear"]').prop('selectedIndex',0);
        	}
        }
        
        var hasCreditCardProfile = Credentials.hasCreditCardProfile();
        
        jQuery('#accountFormContainer .currentCC').toggle(Credentials.hasCreditCardProfile());
        
        this.fireEvent(AccountPanel.REQUEST_REFRESH, payload.accountUpdate.group);

       this.hideUpdateAnimation();
    }
    this.updateFailure = function(payload)
    {
        alert('Failed: ' + payload.error + ' : ' + payload.errorDetail);
        this.hideUpdateAnimation();
    }
    this.hideUpdateAnimation = function()
    {
        var remainder = 2000 - ((new Date()).valueOf() - this.animStart);

        if (remainder < 0) {
            jQuery('.updatingAnimation').remove();
        }
        else {
            setTimeout(function(){
                jQuery('.updatingAnimation').remove();
            },
            2000);
        }

    }
    
    
    this.injectView = function(fragment){
    	jQuery('div#accountFormContainer', this.element).html(fragment);
    }
    
    this.appendView = function(fragment) {
    	jQuery('div#accountFormContainer', this.element).append(fragment);
    }
    this.prependView = function(fragment) {
    	jQuery('div#accountFormContainer', this.element).prepend(fragment);
    }
    
    this.loadChildFragment = function (fileName)
    {
        var that = this;
        var o = {
            filename: fileName,
            success: function (fragment, status) {
                //Log.debug('AccountPanel - success');
                that.processChildFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processChildFragment = function (fragment)
    {
        this.injectView(fragment);
    }
    /**
     *  @DEPRECATED 
     *  
     *  LEGACY FORMS (NOT SETUP TO HAVE INDIVIDUAL VIEW/MEDIATORS)
     *  
     *  TODO:  pull out individual listeners and functionality
     *  for remaining legacy forms and move to the render() 
     *  switch statement. 
     *  
     *  
     */
    this.renderLegacyForms = function(formName){
    	var form = AccountPanel.forms[formName];
    	var that = this;
    	
    	// inject the html
    	this.injectView(form.fragment);
   
   
    	// setup listeners, etc.
    	
    	var charLimit=250;
        jQuery('[name="userDescription"]').bind ('keyup', function(){
        		
        	var charUsed = jQuery(this).val().length;  
        	if(charUsed > charLimit){  
            jQuery(this).val(jQuery(this).val().substr(0, charLimit));  
        	}
        });  
          	
        
        jQuery('[name="billingAddressSameAsCompany"]', this.element).click(function(event){
            if (event.target.checked) {
                Form.populate('div#accountPanel table#billingAddress', form.values.primary);
                jQuery("._sameAsCompany", that.element).attr("disabled", "disabled");
			}else{
			   	jQuery("._sameAsCompany", that.element).removeAttr("disabled");
			}
        });
        jQuery('[name="billingContactSameAsPersonal"]', this.element).click(function(event){
            if (event.target.checked) {
                Form.populate('div#accountPanel table#billingContact', form.values.personal);
                jQuery("._sameAsPersonal", that.element).val("");
                jQuery("._sameAsPersonal", that.element).attr("disabled", "disabled");
                jQuery("._sameAsPersonal", that.element).css('background-color', 'lightgrey');
			}else{
			   	jQuery("._sameAsPersonal", that.element).removeAttr("disabled");
			}
        });
        
        var companySameAsPersonal = jQuery('[name="companySameAsPersonal"]', this.element);
        companySameAsPersonal.click(function(event){
        	var inputs = jQuery('._sameAsPerson', that.element);
        	if (event.target.checked) {
                Form.populate('div#accountPanel', form.values.personal);
                inputs.val("");
                inputs.attr("disabled", "disabled");
                inputs.css('background-color', 'lightgray');
            }else{
            	inputs.removeAttr("disabled");
            	inputs.css('background-color', 'white');
            }
        });

        
        jQuery('[name="newEmail"]', this.element).keyup(function(event){
            var email = event.target.value;
            if (that.emailCache[email] == undefined)
            {
                that.fireEvent(AccountPanel.EMAIL_UNIQUE, event.target.value);
            }
        });       
        
        jQuery('div#accountFormContainer a.button', this.element).unbind('click').click(function(event){
            var isValid = false;
            var o = Form.toObject(jQuery('div#accountFormContainer form'));
            
            
            
            o.type = Credentials.accountTypeTag();
            switch (this.id)
            {
            case 'save':
            	o.nameFormat = jQuery("input[@name=nameFormat]:checked").val();
            case 'saveValues':
            	var charLimit=250;
                var userDesc = jQuery('#userDescription');
                
                if (undefined != userDesc.val()) {
                	var length = userDesc.val().length;
                    if(userDesc.val().length > charLimit){  
                    	userDesc.val(userDesc.val().substr(0, charLimit));  
                    }
                }
                
            	if (jQuery('#accountForm').valid()) {
                	var iFrame = jQuery('<iFrame />', 
                			{name: 'imageIframe', id: 'imageIframe', 
                			width:'0', height:'0', border:'0', style:'width: 0; height: 0; border: none; visibility:hidden;'});
                	var newForm = jQuery('<form>', 
                			{id:'avatarForm', enctype: 'multipart/form-data', 
                			encoding: 'multipart/form-data',method: 'post',
                			action: 'zephyr/user/avatar', target:'imageIframe',
                			width:'0', height:'0', border:'0', style:'width: 0; height: 0; border: none; visibility:hidden;'});
                	var avatarFile = jQuery('#avatarFile', this.element);
                	var bannerFile = jQuery('#bannerFile', this.element);
                	var hiddenUsername = jQuery('<input/>', {type:'hidden', name: 'username', id: 'username', value: Credentials._username});
    				var hiddenPassword = jQuery('<input/>', {type:'hidden', name: 'password', id: 'password', value: Credentials._password});		
    				var hiddenAvatarName = jQuery('<input/>', {type:'hidden', name: 'avatarName', id: 'avatarName', value: ''});
    				var	hiddenAvatarCaption = jQuery('<input/>', {type:'hidden', name: 'avatarCaption', id: 'avatarCaption', value: ''});
    				var hiddenBannerName = jQuery('<input/>', {type:'hidden', name: 'bannerName', id: 'bannerName', value: ''});
    				var	hiddenBannerCaption = jQuery('<input/>', {type:'hidden', name: 'bannerCaption', id: 'bannerCaption', value: ''});
    				var hiddenSubmit = jQuery('<input/>', {type:'submit', style:'visibility:hidden;'});
    				
    				iFrame.appendTo(jQuery('div#accountFormContainer', this.element));
    				iFrame.hide();
    				
    				var eventHandler = function() {
    					
    					if (iFrame[0].detachEvent) iFrame[0].detachEvent("onload", eventHandler);
    		            else iFrame[0].removeEventListener("load", eventHandler, false);

    		            setTimeout(function(){
    		            	iFrame.remove();
    		            	newForm.remove();
    		            	that.fireEvent(AccountPanel.SUBMIT, {formName: formName, form: o});
    		            }, 250);
    				}
    				
    				if (iFrame[0].addEventListener) iFrame[0].addEventListener("load", eventHandler, true);
                    if (iFrame[0].attachEvent) iFrame[0].attachEvent("onload", eventHandler);
    				
    				// assemble the iFrame and form
    				hiddenAvatarName.appendTo(newForm);
    				hiddenAvatarCaption.appendTo(newForm);
    				hiddenBannerName.appendTo(newForm);
    				hiddenBannerCaption.appendTo(newForm);
        			hiddenUsername.appendTo(newForm);
    				hiddenPassword.appendTo(newForm);
    				hiddenSubmit.appendTo(newForm);
                	hiddenAvatarName.attr('value', 'User Profile Picture');
                	hiddenAvatarCaption.attr('value', 'My User Profile Picture');
                	hiddenBannerName.attr('value', 'User Banner');
                	hiddenBannerCaption.attr('value', 'My User Banner');
                	avatarFile.appendTo(newForm);
                	bannerFile.appendTo(newForm);
                	
                	
                	newForm.appendTo(jQuery('div#accountFormContainer', this.element));
                	newForm.hide();
                	ProcessingScreen.show("Updating your account...");

                	newForm.submit();

                	var newAvatarControl = jQuery('<input />', {type: 'file', name: 'avatarFile', id: 'avatarFile'});
                	newAvatarControl.appendTo(jQuery('#avatarFileContainer'));

                	var newBannerControl = jQuery('<input />', {type: 'file', name: 'bannerFile', id: 'bannerFile'});
                	newBannerControl.appendTo(jQuery('#bannerFileContainer'));

                    isValid = true;
                }
            	
            	break;
            case 'updateCreditCard':
                if (jQuery('#accountForm').valid()) {
                	ProcessingScreen.show("Updating your account...");
                    that.fireEvent(AccountPanel.SUBMIT, {formName: formName, form: o});
                    isValid = true;
                }
                break;
            case 'updatePaymentOptions':
            	o = Form.toObject(jQuery('div#accountFormContainer form#paymentOptions'));
            	if(o['allowSubscriptionPayment'] == undefined){
            		o.allowSubscriptionPayment = 0;
            	}else{
            		o.allowSubscriptionPayment = 1;
            	}
            	ProcessingScreen.show("Updating your account...");
            	that.fireEvent(AccountPanel.SUBMIT, {formName: "paymentOptions", form: o});
                isValid = true;
            	break;
            case 'changeEmail':
               if (jQuery('#accountForm').valid()) {
                    var e = {'newEmail':o.newEmail};        // need to strip out the non-email stuff
                    ProcessingScreen.show("Updating your account...");
                    that.fireEvent(AccountPanel.SUBMIT, {formName: formName, form: e});
                    isValid = true;
                }
                break;
            case 'changePassword':
                if (jQuery('#accountForm2').valid()) {
                    var p = {'newPassword': o.newPassword}; // need to strip out the non-password stuff
                    ProcessingScreen.show("Updating your account...");
                    that.fireEvent(AccountPanel.SUBMIT, {formName: formName, form: p});
                    isValid = true;
                }
                break;            
            }
            
            that.clearFormData();

            if (isValid) {
                that.animStart = (new Date()).valueOf();
                //jQuery(this).after('<img class="updatingAnimation" src="com/alleni/images/updating.gif"/>');
            }
            return false;
        });
        
        this.fireEvent(AccountPanel.REQUEST_REFRESH, formName);
    }
    
    this.clearFormData = function(){
    	// make sure the values get reloaded
        jQuery.each(AccountPanel.forms, function(key, value){
            delete value.values;
        });
    }
    
    /**
     * 
     *  Check if fragment is available, if not load it. 
     *  When loaded, hand it off to render()
     * 
     */
    this.display = function (formName)
    {
    	var form = AccountPanel.forms[formName];
    	var that = this;
    	if(jQuery.inArray(Credentials.accountTypeTag(), form.notAvailableFor) > -1){
    		that.display["myProfile"];
    		return undefined;
    	}
        if (form == undefined)
        {
            alert('AccountPanel.display - unknown formName:'+formName);
            return undefined;
        }
        
        if (form.fragment == undefined)
        {
        	var o = {
                    filename: AccountPanel.forms[formName].filename,
                    success: function (fragment, status) {
                        AccountPanel.forms[formName].fragment = fragment;
                        that.render(formName); // and try again
                    }
                }
            File.requestFragment(o);
        }else{
        	that.render(formName);
        }
        
    }
    
    /**
     * 
     * Render individual forms.  Legacy forms share accountPanelMediator,
     * and new forms setup dedicated mediators/views.
     * 
     */
    this.render = function (formName)
    {
    	var form = AccountPanel.forms[formName];
    	var that = this;
    	
        this.emailCache = [];
        this.formName = formName;
        
        
        switch(formName){
        
        case 'myProfile': 
        case 'expertProfile':  
        case 'usernameEmailPassword':  
        case 'emailPassword':  
        case 'contact':  
        case 'companyProfile':  
        case 'billing':  
        case 'transactionHistory':  
        case 'portal':  
        case 'statistics':  
        case 'seller':  
        case 'convertEarnings':  
        case 'buyCredits':  
        case 'plansPricing':  
        case 'cancel':  
        case 'hold':  
        case 'members':  	
        	this.renderLegacyForms(formName);
        	break;
        // new stuff
        case 'paymentMethod':  
        	this.renderLegacyForms(formName);
        	jQuery('#accountFormContainer .currentCC', this.element).toggle(Credentials.hasCreditCardProfile());
        	break;
        case 'incomingTransfers':  
        	this.renderLegacyForms(formName);
        	this.fireEvent(AccountPanel.GET_TRANSFERS_INCOMING);
        	break;
        case 'outgoingTransfers':  
        	this.renderLegacyForms(formName);
        	this.fireEvent(AccountPanel.GET_TRANSFERS_OUTGOING);
        	break;
        case 'startTransfer':  
        	/**
        	 * MIGRATED TO MEDIATOR
        	 */
        	break;
        case 'accountSettings':
        	this.buildAccountSettings();
        	break;
        }
    }
    this.buildAccountSettings = function()
    {
    	var that = this;
    	var currentAccountType = Credentials.accountTypeTag()
		jQuery('div#accountFormContainer', this.element).html(AccountPanel.forms[this.formName].fragment);
		jQuery('.collectorContent',this.element).hide();
    	jQuery('.creatorContent',this.element).hide();
    	jQuery('.professionalContent',this.elements).hide();
		switch(currentAccountType)
		{
			case "collector":
				jQuery('.collectorContent',this.element).show();
				break;
			case "creator":
				jQuery('.creatorContent',this.element).show();
				break;
			case "pro":
				jQuery('.professionalContent',this.element).show();
				this.fireEvent(AccountPanel.GET_ACCOUNT_INFORMATION);
				break;
		}
		jQuery('a.accountTypeIcon',this.element).mouseover(function(event){
			switch (this.id)
            {
	            case 'collectorContentIcon':
	    			jQuery('.collector', this.element).addClass('mouseOver');
	    			break;
	            case 'creatorContentIcon':
	        			jQuery('.creator', this.element).addClass('mouseOver');
	        			break;
        		case 'professionalContentIcon':
        			jQuery('.professional', this.element).addClass('mouseOver');
        			break;
            }
		});
		jQuery('a.accountTypeIcon',this.element).mouseout(function(event){
			switch (this.id)
            {
            	case 'collectorContentIcon':
            		jQuery('.collector', this.element).removeClass('mouseOver');
    				break;
        		case 'creatorContentIcon':
        			jQuery('.creator', this.element).removeClass('mouseOver');
        			break;
        		case 'professionalContentIcon':
        			jQuery('.professional', this.element).removeClass('mouseOver');
        			break;
            }
		});
		jQuery('a.accountTypeIcon',this.element).click(function(event){
        	//Reset Collector
        	jQuery('.selectedCollector', this.element).hide();
        	jQuery('.collector', this.element).removeClass('sectionSelected current creatorSelected notCurrent')
        	//Reset Creator
        	jQuery('.selectedCreator', this.element).hide();
        	jQuery('.creator', this.element).removeClass('sectionSelected current proSelected notCurrent mouseOver')
        	//Reset Professional
        	jQuery('.selectedProfessional', this.element).hide();
        	jQuery('.professional', this.element).removeClass('sectionSelected current notCurrent mouseOver')        	
        	
        	//Show correct new content and Select correct icons
        	switch (this.id)
            {
        		case 'collectorContentIcon':
        			jQuery('.selectedCollector', this.element).show();
        			jQuery('.collector', this.element).addClass('sectionSelected'); 
        			break;
        		case 'creatorContentIcon':
        			jQuery('.selectedCreator', this.element).show();
        			jQuery('.collector', this.element).addClass('creatorSelected');
        			jQuery('.creator', this.element).addClass('sectionSelected');
        			break;
        		case 'professionalContentIcon':
        			jQuery('.selectedProfessional', this.element).show();
        			jQuery('.collector', this.element).addClass('creatorSelected');
        			jQuery('.creator', this.element).addClass('proSelected');
        			jQuery('.professional', this.element).addClass('sectionSelected');
        			break;
            }
        	switch(currentAccountType)
    		{
    			case "collector":
    				jQuery('.collector', this.element).addClass('current');
    				jQuery('.creator', this.element).addClass('notCurrent');
    				jQuery('.professional', this.element).addClass('notCurrent');
    				break;
    			case "creator":
    				jQuery('.collector', this.element).addClass('notCurrent');
    				jQuery('.creator', this.element).addClass('current');
    				jQuery('.professional', this.element).addClass('notCurrent');
    				break;
    			case "pro":
    				jQuery('.collector', this.element).addClass('notCurrent');
    				jQuery('.creator', this.element).addClass('notCurrent');
    				jQuery('.professional', this.element).addClass('current');
    				break;
    		}
        	return false;
        });
		
		jQuery('div#accountFormContainer a.button', this.element).unbind('click').click(function(event){
            var isValid = false;
            var o = Form.toObject(jQuery('div#accountFormContainer form'));
            o.type = Credentials.accountTypeTag();
            switch (this.id)
            {	
	        case 'changeToCollector':
	        	that.fireEvent(AccountPanel.CHANGE_ACCOUNT_TYPE, "collector");
	        	break;
	        case 'changeToCreator':
	        	that.fireEvent(AccountPanel.CHANGE_ACCOUNT_TYPE, "creator");
	        	break;
	        case 'changeToPro':
	        	that.fireEvent(AccountPanel.CHANGE_ACCOUNT_TYPE, "pro");
	        	break;
            }
            return false;    
		});
		
		//Add a handler to the numLearner radio group so we can update the total monthly fee
		jQuery('input[name="learnerGroup"]').change(function(event) {
			that.updateTotalMonthlyFee(this);
			
			jQuery('div#setNumberLearners', that.element).unbind('click').click(function(event){
				var numLearners = jQuery('input[name=learnerGroup]:checked').val();
				that.fireEvent(AccountPanel.SET_NUMBER_LEARNERS, numLearners);
			});
		});

		
		this.clearFormData();    	
    }
    
    this.updateCurrentCCInfo = function(payload)
    {
    	var ccProfile = Credentials.hasCreditCardProfile();
    	var goodStand = Credentials.goodStanding();
    	jQuery('#accountFormContainer .currentCC').toggle(Credentials.hasCreditCardProfile());
    	//jQuery('#accountFormContainer .creditCardDeniedError').toggle(!Credentials.goodStanding());
    }
    this.setupValidation = function(formName, values)
    {
        jQuery('#accountForm').validate();        

        var that = this;
        switch (formName)
        {
	        case 'myProfile':
	              jQuery('[name="firstName"]').addClass("required");
	              jQuery('[name="lastName"]').addClass("required");
	              jQuery('[name="email"]').addClass("email uniqueEmail");	
	            break;
	        case 'usernameEmailPassword':
	            // Custom validators
	            jQuery.validator.addMethod("uniqueEmail", function(value, element){
	                return that.emailCache[value] == true || that.emailCache[value] == undefined;
	            }, function(value, element){
	                return 'The email "' + element.value + '" is already in use';
	            });
	
	            jQuery.validator.addMethod("password", function(value, element){
	                return value == Credentials.password();
	            }, "Your password does not match.");
	
	            jQuery('#accountForm2').validate();
	            
	            jQuery('[name="newEmail"]').addClass("required email uniqueEmail");
	            jQuery('[name="confirmNewEmail"]').addClass("required email").attr('equalTo','#newEmail');
	
	            jQuery('[name="curPassword"]').addClass("required password");
	            jQuery('[name="newPassword"]').addClass("required").attr("minlength", 3);
	            jQuery('[name="confirmNewPassword"]').addClass("required").attr("minlength",3).attr("equalTo",'#newPassword');
	            break;
	        case 'companyProfile':
	            jQuery('[name="email"]').addClass("email");
	            break;
	        case 'billing':
	            jQuery('[name="email"]').addClass("email");
	            break;
	        case 'paymentMethod':
	            jQuery('[name="_ccName"]').addClass("required");
	            jQuery('[name="_ccNumber"]').addClass("required").addClass('number'); // NOTE that Authnet test Visa, etc. do not have correct number of digits so will not specify required length!?
	            jQuery('[name="_ccExpMonth"]').addClass("required");
	            jQuery('[name="_ccExpYear"]').addClass("required");
	            jQuery('[name="_ccCode"]').addClass("required");
	            break;
	        case 'convertEarnings':
	            jQuery('[name="convertAmount"]').addClass('number').attr('max', values.earnings).attr('min', 0);
	            break;
	        }
    }
    this.populate = function (values)
    {
    	var form = AccountPanel.forms[this.formName];
    	var that = this;
        if (form.values == undefined)
            form.values = values;         

        this.setupValidation(this.formName, values);

        switch (this.formName)
        {
        case 'members':
            this.populateMembers(values);
            break;
        case 'companyProfile':
        	Form.populate('div#accountFormContainer', values);
        	if(form.values["companySameAsPersonal"]){
        		jQuery("._sameAsPerson", that.element).val("");
        		jQuery("._sameAsPerson", that.element).attr("disabled", "disabled");
        		jQuery("._sameAsPerson", that.element).css('background-color', 'lightgrey');
			}else{
			   	jQuery("._sameAsPerson", that.element).removeAttr("disabled");
			}
			
			break;
        case 'billing':
        	Form.populate('div#accountFormContainer', values);
        	if(form.values["billingAddressSameAsCompany"]){
				jQuery("._sameAsCompany", that.element).attr("disabled", "disabled");
			}else{
			   	jQuery("._sameAsCompany", that.element).removeAttr("disabled");
			}
        	if(form.values["billingContactSameAsPersonal"]){
        		jQuery("._sameAsPersonal", that.element).val("");
				jQuery("._sameAsPersonal", that.element).attr("disabled", "disabled");
				jQuery("._sameAsPersonal", that.element).css('background-color', 'lightgrey');
			}else{
			   	jQuery("._sameAsPersonal", that.element).removeAttr("disabled");
			}
			
        	break;
        case 'paymentMethod':
        	jQuery('#allowSubscriptionPayment').prop("checked", (values['allowSubscriptionPayment']));
        	Form.populate('div#accountFormContainer', values);
        	break;
        default:
              Form.populate('div#accountFormContainer', values);
        	
            break;
        }
    }
    this.populateFail = function(payload)
    {
        alert('Failed: ' + payload.error + ' : ' + payload.errorDetail);
    }
    this.populateMembers = function (values)
    {
    	    for (var i = 0; i < values.length; i++)
            {
               var member = values[i];
            }
    }
    this.emailUnique = function(results)
    {
        this.emailCache[results.email] = results.unique;
    }
    
    
    this.populateAccountSettings = function(results){
    	var that = this;
    	if (results.numLearners == 0) {
    		jQuery('[name=learnerGroup][value="25"]').checked = true;
    	} else {
    		jQuery.each(jQuery('input[name=learnerGroup]', this.element), function(i, radioButton){
        		//Make sure that only one of the radio buttons is checked at any time. Multiple checked = bad news bears
        		//However, if numLearners is zero it means the account doesn't have one yet, so leave the default checked.
        		radioButton.checked = false;
        		
        		if(radioButton.defaultValue == results.numLearners){
        			that.setInitialSelectedNumLearnersValue(radioButton.value);
        			radioButton.checked = true;
        			that.updateTotalMonthlyFee(radioButton);
        		}
        	});
    	}     	
    }
    
    this.setInitialSelectedNumLearnersValue = function(initialValue) {
    	this.initialSelectedNumLearnersValue = initialValue;
    	jQuery('div#setNumberLearners', this.element).unbind('click');
    }
    
    this.cancelNumLearnersChange = function() {
    	var that = this;
    	var selectValue = 0;
    	
    	if(this.initialSelectedNumLearnersValue != undefined && this.initialSelectedNumLearnersValue > 0) {
    		selectValue = that.initialSelectedNumLearnersValue;
    	} else {
    		selectValue = 25;
    	}
    	
    	jQuery.each(jQuery('input[name=learnerGroup]', this.element), function(i, radioButton){
    		radioButton.checked = false;
    		if(radioButton.value == selectValue){
    			radioButton.checked = true;
    			that.updateTotalMonthlyFee(radioButton);
    		}
    	});
    	
    	
    	jQuery('div#setNumberLearners', that.element).unbind('click');
    }
   
    
    this.updateTotalMonthlyFee = function(radioButton) {
    	var baseFee = 3995;
    	var addFee = this.getLMSFee(parseInt(radioButton.value));
		jQuery('#totalMonthlyFee').text((baseFee += addFee)/100);
    }
    
    this.getLMSFee = function(numLearners) {
    	var addFee = 0;
    	switch(numLearners) {
		case 1000:
			addFee = 20000;
			break;
		case 500:
			addFee = 10000;
			break;
		case 100:
			addFee = 5000;
			break;
    	};
    	
    	return addFee;
    }
    
    this.accountUpgradedDowngraded = function()
    {
    	if(this.formName == 'accountSettings'){
    		this.buildAccountSettings();
    	}
    	
    }
    
    this.toggleTransferRow = function(state, id) {
    	jQuery('tr#row' + id).toggle(state);
    };
    
    this.populateIncomingTransferList = function(results)
    {
    	var that = this;
    	var htmlContent = '';
    	jQuery.each(results.transferAccounts, function(i, group){
    		htmlContent += '<div class="transferGroupGrid"></br>';
    		htmlContent += '<span class="transferHeadingBlack">From:&nbsp; </span><a href="mailTo:"><span class="transferHeadingBlue">'+group.displayName+' ('+group.email+')</span></a><br/>';
    		htmlContent += '<table class="transferGridTable"><tr class="transferGridHeader">'
    		htmlContent += '<td class="transferGridProjectNameIn">Project Name</td>';
    		htmlContent += '<td class="transferGridProjectPrice">Price</td>';
    		htmlContent += '<td class="transferGridProjectStatus">Status</td>';
    		htmlContent += '<td class="transferGridProjectDate">Date</td>';
    		htmlContent += '<td class="transferGridProjectOptions">Options</td>';
    		htmlContent += '</td></tr>';
			jQuery.each(group.projectTransfers, function(j, project){
				var status = "Sent ";
				var evenOddItem = (j%2==0?"oddTransferItem":"evenTransferItem");
    			var priceInDollars = (project.price/100);
    			var smartCost = (project.price > 0?"$"+(priceInDollars<0?"0"+priceInDollars:priceInDollars):"Free");
    			var displayDate = new Date(project.createdDateTime);
    			if (project.retired > 0) {
    				displayDate = new Date(project.retiredDateTime);
    				if (project.retiredReasonTypeId === 6) {
    					status = "Accepted";
    				} else if (project.retiredReasonTypeId === 5) {
    					if (project.retiredById === project.receiverMemberId) {
    						status = "Declined";
    					} else {
    						status = "Canceled";
    					}
    				}
    			}
    			htmlContent += '<tr class="'+evenOddItem+' incomingGridItem" id="row' + project.projectMemberAccessId + '">';
     			htmlContent += '<td class="transferGridProjectNameIn transferProjectName"><div class="wordBreakDiv">' + project.name + '</div></td>';
    			htmlContent += '<td class="transferGridProjectPrice">' + smartCost + '</td>';
    			htmlContent += '<td class="transferGridProjectStatus">' + status + '</td>';
    			htmlContent += '<td class="transferGridProjectDate">' + displayDate.format('zebraDateTime') + '</td>';
//    			(displayDate.getMonth()+1)+'/'+displayDate.getDate()+'/'+displayDate.getFullYear()
//    								+ displayDate.getHours() + ':' + displayDate.getMinutes() + '</td>';

    			htmlContent += '<td  class="transferGridProjectOptions">';
				if(project.retired){
    				htmlContent += '<a id="clearTransfer&'+project.projectMemberAccessId+'" href="#clearTransfer" class="acceptDecline"><span>Clear</span></a>';
    			}else{
    				htmlContent += '<a id="acceptTransfer&'+project.projectMemberAccessId+'" href="#acceptTransfer" class="acceptDecline"><span>Accept</span></a>&nbsp;&nbsp;'+
    				'| <a id="declineTransfer&'+project.projectMemberAccessId+'" href="#declineTransfer" class="acceptDecline"><span>Decline</span></a>';
    			}
				htmlContent += '</td>';
				htmlContent += '</tr>';
    		});
    		htmlContent += '</table>';
    		htmlContent += '</div>';
    	});
    	jQuery('div.incomingDataContainer').html(htmlContent);
    	
    	jQuery('div.incomingDataContainer a.acceptDecline').unbind('click').click(function() {
    		that.toggleTransferRow(false, this.id.split('&')[1]);
    	});
    	
    	jQuery('.acceptDecline').click(function(event){
    		var idSplit = this.id.split("&");
    		var buttonId = idSplit[0];
    		if(idSplit.length > 1){
    			var transferId = idSplit[1];
    		}
    		switch (buttonId)
            {
    			case 'declineTransfer':
    				that.fireEvent(AccountPanel.CANCEL_TRANSFER, transferId);
    				break;
    			case 'clearTransfer':
    				that.fireEvent(AccountPanel.CLEAR_TRANSFER, transferId);
    				break;
    			case 'acceptTransfer':
    				that.fireEvent(AccountPanel.ACCEPT_TRANSFER, transferId);
    				break;
            }
    		return false;
    	});
    	
    	if (results.transferAccounts.length == 0) {
    		jQuery('div#incomingTransferMessage').html("You have no incoming project transfers.");
    	}
    };
    this.populateOutgoingTransferList = function(results)
    {
    	/*
    	 * var d = new Date(project.lastUpdated); 
    	 * lastUpdate = (d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear();
    	 */
    	var that = this;
    	var htmlContent = '';
    	jQuery.each(results.transferAccounts, function(i, group){
    		htmlContent += '<div class="transferGroupGrid"></br>'
       		htmlContent += '<span class="transferHeadingBlack">To:&nbsp; </span><span class="transferHeadingBlue">'+group.displayName+'</span><br/>';
    		htmlContent += '<table class="transferGridTable"><tr class="transferGridHeader">'
    		htmlContent += '<td class="transferGridProjectNameOut">Project Name</td>';
    		htmlContent += '<td class="transferGridProjectStatus">Status</td>';
    		htmlContent += '<td class="transferGridProjectDate">Date</td>';
    		htmlContent += '<td class="transferGridProjectOptions">Options</td>';
    		htmlContent += '</td></tr>';
    		jQuery.each(group.projectTransfers, function(j, project){
    			var evenOddItem = (j%2==0?"oddTransferItem":"evenTransferItem");
    			var status = "Sent ";
    			var displayDate = new Date(project.createdDateTime);
    			if (project.retired > 0) {
    				displayDate = new Date(project.retiredDateTime);
    				if (project.retiredReasonTypeId === 6) {
    					status = "Accepted";
    				} else if (project.retiredReasonTypeId === 5) {
    					if (project.retiredById === project.receiverMemberId) {
    						status = "Declined";
    					} else {
    						status = "Canceled";
    					}
    				}
    			}
    			htmlContent += '<tr class="'+evenOddItem+' incomingGridItem">';
    			htmlContent += '<td class="transferGridProjectNameOut transferProjectName"><div class="wordBreakDivOutgoing">' + project.name + '</div></td>'
    			htmlContent += '<td class="transferGridProjectStatus">' + status + '</td>';
    			htmlContent += '<td class="transferGridProjectDate">' + (displayDate.getMonth()+1)+'/'+displayDate.getDate()+'/'+displayDate.getFullYear() + '</td>';
    			htmlContent += '<td  class="transferGridProjectOptions">';
    			if(project.retired){
    				htmlContent += '<a id="clearTransfer&'+project.projectMemberAccessId+'" href="#clearTransfer" class="acceptDecline"><span>Clear</span></a>';
    			}else{
    				htmlContent += '<a id="declineTransfer&'+project.projectMemberAccessId+'" href="#cancelTransfer" class="acceptDecline"><span>Cancel</span></a>';
    			}
    			htmlContent += '</td>';
				htmlContent += '</tr>';
    		});
    		htmlContent += '</table>';
    		htmlContent += '</div>';
    	});
    	jQuery('div.outgoingDataContainer').html(htmlContent);
    	jQuery('.acceptDecline').unbind('click').click(function(event){
    		var idSplit = this.id.split("&");
    		var buttonId = idSplit[0];
    		if(idSplit.length > 1){
    			var transferId = idSplit[1];
    		}
    		switch (buttonId)
            {
    			case 'declineTransfer':
    				that.fireEvent(AccountPanel.DECLINE_TRANSFER, transferId);
    				break;
    			case 'clearTransfer':
    				that.fireEvent(AccountPanel.CLEAR_TRANSFER, transferId);
    				break;
            }
    		return false;
    	});
    }
    this.updateTransferList = function()
    {
    	switch(this.formName){
    	 case 'incomingTransfers':  
         	this.fireEvent(AccountPanel.GET_TRANSFERS_INCOMING);
         	break;
         case 'outgoingTransfers':  
         	this.fireEvent(AccountPanel.GET_TRANSFERS_OUTGOING);
         	break;
    	}
    }
    this.showTransferSuccess = function(payload)
    {
    	var htmlContent = "Project '" + payload.projectName + "' successfully received.";
    	jQuery('div#projectTransferStatus').html(htmlContent);
    	jQuery('div#projectTransferStatus').show();
    }
    this.showTransferCanceled = function(payload)
    {
    	var htmlContent = "Transfer of project '" + payload.projectName + "' CANCELED.";
    	jQuery('div#projectTransferStatus').html(htmlContent);
    	jQuery('div#projectTransferStatus').show();
    }

    
    this.showAvatarImage = function(payload) {
    	var img = jQuery('#avatarImage', this.element);
    	var imagePath = this.getUploadableAssetPath(payload);
    	var origPath = img.attr('src');
    	if(imagePath != origPath){
    		/*img.fadeOut(0, function() {
    			img.attr('src', '');
        	});(*/
	    	img.attr('src', imagePath);
	    	img.attr('title', payload.caption);
	    	img.attr('height', '50');
	    	img.attr('width', '50');
	    	img.load(this,this.avatarImageOnload);    
	        
	        this.hideBannerImage = function() {
	        	
	        };
    	}
    };

    
    this.showBannerImage = function(payload) {
    	var img = jQuery('#bannerImage', this.element);
    	var imagePath = this.getUploadableAssetPath(payload);
    	var origPath = img.attr('src');
    	if(imagePath != origPath){
    		/*img.fadeOut(0, function() {
    			img.attr('src', '');
        	});*/
	    	img.attr('src', imagePath);
	    	img.attr('title', payload.caption);
	    	img.attr('width', '100');
	    	img.attr('height', '50');
	    	img.load(this,this.bannerImageOnload);
    	}
    };
    
    
    this.avatarImageOnload = function(e) {
    	jQuery('#avatarImage').fadeIn(1000);
    };
    
    this.bannerImageOnload = function(e) {
    	jQuery('#bannerImage').fadeIn(1000);
    };
    
    this.getUploadableAssetPath = function(payload) {
    	//This logic *really* wants to be abstracted out somewhere, because it's here and in Author Profile
    	var UATSMT = payload.uploadableAssetTypeSupportedMimeType;
    	var UAS3B = UATSMT.uploadableAssetS3Bucket;
    	var BP = UAS3B.bucketpath;
    	var imagePath = BP.replace('/', '.s3.amazonaws.com/');
    	var fullPath = 'http://' + imagePath + '/' + payload.fileid;
    	
    	return fullPath;
    };
}
AccountPanel = new Class(new AccountPanel());
AccountPanel.NAME = 'AccountPanel';
AccountPanel.INITIALIZED = 'AccountPanel.INITIALIZED';
AccountPanel.REQUEST_REFRESH = 'AccountPanel.REQUEST_REFRESH';
AccountPanel.SUBMIT = 'AccountPanel.SUBMIT';
AccountPanel.EMAIL_UNIQUE = 'AccountPanel.EMAIL_UNIQUE';
AccountPanel.CONFIRM_USER_BY_EMAIL = 'AccountPanel.CONFIRM_USER_BY_EMAIL';
AccountPanel.GET_TRANSFERS_INCOMING = 'AccountPanel.GET_TRANSFERS_INCOMING';
AccountPanel.GET_TRANSFERS_OUTGOING = 'AccountPanel.GET_TRANSFERS_OUTGOING';
AccountPanel.CANCEL_TRANSFER = 'AccountPanel.CANCEL_TRANSFER';
AccountPanel.ACCEPT_TRANSFER = 'AccountPanel.ACCEPT_TRANSFER';
AccountPanel.DECLINE_TRANSFER = 'AccountPanel.DECLINE_TRANSFER';
AccountPanel.CLEAR_TRANSFER = 'AccountPanel.CLEAR_TRANSFER';
AccountPanel.CHANGE_ACCOUNT_TYPE = 'AccountPanel.CHANGE_ACCOUNT_TYPE';
AccountPanel.GET_ACCOUNT_INFORMATION = 'AccountPanel.GET_ACCOUNT_INFORMATION';
AccountPanel.SET_NUMBER_LEARNERS = 'AccountPanel.SET_NUMBER_LEARNERS';
AccountPanel.CONFIRM_UPGRADE = 'AccountPanel.CONFIRM_UPGRADE';

// TODO acavan - move this to application.js
AccountPanel.forms = {
    myProfile: {
        filename: 'accountFormMyProfile.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    expertProfile: {
        filename: 'accountFormExpertProfile.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    usernameEmailPassword: {
        filename: 'accountFormUsernameEmailPassword.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    emailPassword: {
        filename: 'accountFormEmailPassword.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    contact: {
        filename: 'accountFormContact.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    companyProfile: {
        filename: 'accountFormCompanyProfile.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    billing: {
        filename: 'accountFormBilling.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    paymentMethod: {
        filename: 'accountFormPaymentMethod.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    transactionHistory: {
        filename: 'accountFormTransactionHistory.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    portal: {
        filename: 'accountFormPortal.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    statistics: {
        filename: 'accountFormStatistics.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    seller: {
        filename: 'accountFormSeller.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    convertEarnings: {
        filename: 'accountFormConvertEarnings.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    buyCredits: {
        filename: 'accountFormBuyCredits.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    plansPricing: {
        filename: 'accountFormPlansPricing.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    cancel: {
        filename: 'accountFormCancel.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    hold: {
        filename: 'accountFormHold.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    members: {
        filename: 'accountFormMembers.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    accountSettings: {
    	filename: 'accountFormAccountSettings.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: []
    },
    startTransfer: {
    	filename: 'accountFormStartTransfer.html',
        fragment: undefined,
        values: undefined,
        mediator: 'startTransferMediator.js',
        view: 'startTransfer.js',
        notAvailableFor: ["collector"]
    },
    incomingTransfers: {
    	filename: 'accountFormIncomingTransfers.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: ["collector"]
    },
    outgoingTransfers: {
    	filename: 'accountFormOutgoingTransfers.html',
        fragment: undefined,
        values: undefined,
        notAvailableFor: ["collector"]
    }
};
ScriptLoader.loaded(AccountPanel.NAME);
