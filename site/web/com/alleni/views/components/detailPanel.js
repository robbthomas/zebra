var DetailPanel = function()
{
    this.Extends = UIComponent;
    this.fragments = {};
    this.commentsRatings = undefined;
    this.detailsReady = false;
    this.myRating = 0;
    this.myNewRating = 0;
    this.id = -1;
    this.type = null;
    this.tinyurl = null; // webservice call in mediator.onDisplay will fill in
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false; 
    this.joinProxy = undefined;
    this.plan = undefined;
    this.project = undefined;
    this.commentsList = [];
    this.commentTotal = 0;
    this.commentOffset = 0; // by default start on 1st comment
    this.commentPageSize = 10;
    this.postCommentStatus = undefined;    
    this.MAX_DESCRIPTION_LENGTH = 255;
    
    this.uninitializedDisplayDetail = false;
    this.uninitializedDisplayDetailArguments = null;

	this.unprocessedFragments = 0;

	this.sharingLinks = new SharingLinks();
    
    this.initialize = function ()
    {
       //Log.debug('DetailPanel.initialize');
        this.parent('detailPanel');

        this.loadFragment('detailPanelAvailable.html');
        this.loadFragment('detailPanelInviteOnly.html');
        this.loadFragment('detailPanelInviteOnlyLogin.html');
        this.loadFragment('detailPanelNotFound.html');
        this.loadFragment('detailPanelNone.html');
    }

	this.loadFragment = function (filename)
	{
		var that = this;
		this.unprocessedFragments++;
        File.requestFragment({
            filename: filename,
            success: function (fragment, status) {
                that.processFragment(filename, fragment);
            }
        });
	}

    this.processFragment = function (filename, fragment)
    {
	    this.unprocessedFragments--;
	    this.fragments[filename] = fragment;
	    if(this.unprocessedFragments == 0) {
		    this.set('html', this.fragments['detailPanelNone.html']);
			this.fragmentProcessed = true;
			if (this.invokePostFragmentInitialization)
			{
				this.initializeChildren();
				this.childrenInitialized();
				this.initializationComplete();
				this.invokePostFragmentInitialization = false;
			}
	    }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            if(this.uninitializedDisplayDetail) {
            	this.displayDetail(this.uninitializedDisplayDetailArguments);
            	this.uninitializedDisplayDetail = false;
            	this.uninitializedDisplayDetailArguments = null;
            }
            this.fireEvent(DetailPanel.INITIALIZED);
        }

    }
    this.show = function ()
    {
        jQuery('div.detailMain, div.detailSidebar').show();
    }
    this.hide = function ()
    {
        Log.debug('DetailsPanel - clearing comments cache for hiding');
        this.detailsReady = false;
        this.commentsRatings = null;
        jQuery('div.detailMain, div.detailSidebar').hide();
    }
    this.display = function (id) {
        this.set('html', this.fragments['detailPanelNone.html']);
        this.fireEvent('DetailPanel.DISPLAY', id);
    }
    this.customDispose = function()
    {
        jQuery('div.detailMain div.playerPreview img').hide();
        jQuery('div.detailMain div.icon img').hide();
    }
    this.ratingSuccess = function(o)
    {
    	this.myRating = this.myNewRating;
        jQuery('div.detailMain li#avRating img').attr('src','com/alleni/images/stars_big_blue' + o.average + '.png')
        jQuery('div.detailMain li#avRating span#count').text('(' + o.numratings + ')');
        jQuery('div.detailMain div.comments div._myRating img').attr('src', 'com/alleni/images/stars_big_green' + this.myRating + '.png')
        jQuery('div.detailMain span#myRating img').attr('src', 'com/alleni/images/stars_big_green' + this.myRating + '.png');
    }
    
    this.projectFeedbackSuccess = function(o) {
    	jQuery('div#flagZappForm').toggle();
		jQuery('div#flagZappLinkDiv').html('You have flagged this app.');
    }
    
    this.findPosition = function(obj) {
    	var curleft = curtop = 0;
    	if (obj.offsetParent) {
    		do {
    			curleft += obj.offsetLeft;
    			curtop += obj.offsetTop;
    		} while (obj = obj.offsetParent);
    	}
    	return [curleft,curtop];
    }

	this.displayError = function()
	{
		this.set('html', this.fragments['detailPanelNotFound.html']);
	}
	
	this.showPrivacyMessage = function(hideInStoreList, inviteOnly, type) {
		var displayType = "";
		if (type.toLowerCase() === 'zapp') displayType = 'App';
		else if (type.toLowerCase() === 'gadget') displayType = 'Gadget';
		else displayType = type.toUpperCase().substring(0,1) + type.substring(1);
		
		var str="This " + displayType + " is not in the " + displayType + " Shopp.";
		
		if(!hideInStoreList) {
			if(inviteOnly)
				str = "This " + displayType + " is available only to my Guests";
			else 
				str = "This " + displayType + " is available in the " + displayType.toUpperCase().substring(0, 1) + displayType.substring(1) + " Shopp.";
		}
		jQuery("#shareInfoText").html(str);
	}

    this.updatePostCommentButtonState = function()
    {		
    	var that = this;
    	if(that.postCommentStatus === false){
    		jQuery('a.postcomment', this.element).fadeIn(0,0.5);
    		jQuery('a.postcomment', this.element).addClass('disabledButton');
    		jQuery('a.postcomment', this.element).removeAttr('href');
    		jQuery('a.postcomment', this.element).unbind('click');}//if
    	else{
    		jQuery('a.postcomment', this.element).fadeIn(0,1);
    		jQuery('a.postcomment', this.element).remove('disabledButton');
    		jQuery('a.postcomment', this.element).attr('href','#postComment');
    	}//else
    }
	
    this.displayDetail = function (o)
    {
    	if(!this.initialized) {
    		// prevent this from getting called before fragments are ready
    		this.uninitializedDisplayDetailArguments = o;
    		this.uninitializedDisplayDetail = true;
    		return;
    	}
    	
    	this.project = o;
        
    	var that = this;
        // Require() //TODO acavan...
        
        //Log.debug('DetailPanel.displayDetail');
        EditorPlayer.id = o.playerId;
        EditorPlayer.bShowHistory = o.bShowHistory;
        EditorPlayer.projectName = o.projectName;

        this.id = o.id;
        this.publishedName = o.publishedName;
        
        this.type = o.type.toLowerCase();
        
	    var f;
	    
	    if(o.userAccessPrivs.viewMetadata) {
		    f = this.fragments['detailPanelAvailable.html'];
	    } else if(Credentials.email() == 'guest@zebrazapps.com') {
		    f = this.fragments['detailPanelInviteOnlyLogin.html'];
	    } else {
		    f = this.fragments['detailPanelInviteOnly.html'];
	    }

        //Log.debug('DetailPanel.displayDetail - regex loop top');
	    
	    // Ensure that the description does not flow off the screen
	    //if(o.description.length > this.MAX_DESCRIPTION_LENGTH)
	    	//o.description = o.description.substring(0,this.MAX_DESCRIPTION_LENGTH) + '...';
	    
	    o.description = makeSafeString(o.description);
	    
	    var arrSafeableStrings = ["description","publishedName"];
	    var replaceString = '';
	    var re;
	    
        for (var i in o) // DRY w/listPanl.js
        {
            if (typeof o[i] == 'object') continue;
            replaceString = o[i];
            re = new RegExp('{'+i+'}', 'g');
            if (arrSafeableStrings.indexOf(i) > -1) replaceString = makeSafeString(replaceString);
            f = f.replace(re, replaceString);
        } 
        
        Log.debug('DetailPanel.displayDetail - Setting HTML');
        this.set('html', f);
        this.plan = this.joinProxy.plan(o.type);
        jQuery('div.detailMain span.accountType').text(this.plan);
        jQuery('div.detailMain span#myRating img').hover(function(event){
        }, function(event){
            jQuery(this).attr('src', 'com/alleni/images/stars_big_green' + that.myRating + '.png');
        });
        
        //Hide buy button if item is not in the shopp
        if(!o.hideInStore){
        	jQuery('#detailPanel div.buy').hide();
        }
	    this.updateOwnedList();
        
        jQuery('div.detailMain span#myRating img').mousemove(function(event){
        	var position = that.findPosition(event.target);
            var index = ((event.clientX - position[0]) * 5) / event.target.width;
            index = parseInt(index);
            that.myNewRating = index + 1;
            jQuery(this).attr('src', 'com/alleni/images/stars_big_green' + that.myNewRating + '.png');
        });
        
        jQuery('div.detailMain span#myRating img').click(function(event){
            var rating = {
                myRating: that.myNewRating,
                projectId: o.id
            };
            that.fireEvent(DetailPanel.RATING, rating);
        });

        jQuery('#flagZappLink', this.element).click(function(event){
        	jQuery('#flagZappForm').toggle();
        });
        
        jQuery('#updateAvailable', this.element).click(function(event){
        	that.fireEvent(DetailPanel.HREF_CLICKED, {href: 'updateApp', urlName: that.project.urlName} );
        });
        
        jQuery('div.detailMain .zapp').toggle(o.type == 'zapp');
        jQuery('div.detailMain .gadget').toggle(o.type == 'gadget');
        
        var iframe = jQuery('div.detailMain div.playerPreview iframe');
	    var showPlayer = o.type == 'zapp' && o.userAccessPrivs.read;
        iframe.toggle(showPlayer);
        if (showPlayer) {
        	if (o.owned) {
                iframe.attr('src', '/embed/verlock'+o.playerId);
        	} else {
                iframe.attr('src', '/embed/'+o.playerId);
            }
        }
        jQuery('div.detailMain div.playerPreview img').toggle(!showPlayer);
        
        var html = '';
        for (i = 0; i < o.authors.length; i++)
        {
            html += (i > 0 ? ', ' : '')+'<a href="#">'+o.authors[i]+'</a>';
        }
        jQuery('div.detailMain div.author td.authorList').html(html);

        html = '';
        for (i = 0; i < o.screenshots.length; i++)
        {
            html += '<a href="#enlarge?'+ i +'&'+o.screenshots.length+'"><img src="'+o.screenshots[i].baseUrl+'70'+o.screenshots[i].fileNameExt+'" style="margin-right:10px;"/></a>';
        }
        jQuery('div.detailMain div.screenshots').html(html);
        if(o.screenshots.length == 0)
    	{
        	jQuery('._screenShots').hide();
    	}
        
        html = '';
        for (i = 0; i < o.tags.length; i++)
        {
            var tag = o.tags[i];
            html += (i > 0 ? ', ' : '')+'<a href="#">'+o.tags[i]+'</a>';
        }
        jQuery('div.detailSidebar span#tags').html(html);

	    //TODO make this based on if the current user is the publishing user
	    jQuery('#btnPrivacy').toggle(o.userAccessPrivs.read);

        jQuery('#btnPrivacy').click(function (event){
        	var handled = false;
        	that.fireEvent(DetailPanel.SHARE_CLICKED, {o:that.project, arguments:'privacy'});
            handled = true;
            return (handled ? false : true);
        });
        
        jQuery('#cancelFlagZapp').click(function (event){
        	jQuery('#flagComment').val('');
        	jQuery('#flagZappForm').toggle();
        });
        
        
        ChangeTag.toText(
            jQuery('#detailPanel .comments .userNotLoggedIn [name="password"]'),
            jQuery('#detailPanel .comments .userNotLoggedIn a.button')
        )

	    var iconQuery = function(privValue, shareValue) {
			if(shareValue == undefined) {
				return jQuery('div#shareTypes span', that.element);
			} else {
				return jQuery('div#shareTypes .'+shareValue, that.element);
			}
	    }
        
	    var privacy = o.hideInStoreList ? (o.inviteOnly ? 1 : 2) : 3;
	    this.sharingLinks.setVisibility(privacy, null, ((!o.publisher)?  "notOwner":Credentials.accountTypeTag()), o.projectAccountTypeName, iconQuery,undefined,o.type, o.userAccessPrivs);
        
	    var showShareInfo = true ;
	   
	    if (o.type == 'gadget' || !o.publisher || (o.publisher && Credentials.accountTypeTag() === "collector")) showShareInfo = false;
	    jQuery('.shareInfo').toggle(showShareInfo);
	    
        this.toggleCommentsLogin(false);        
        this.detailsReady = true;
        this.attachListenersForCommentsPanel(o);
        this.displayCommentsRatings();
        this.showPrivacyMessage(o.hideInStoreList, o.inviteOnly, o.type);
    }

    this.updateOwnedList = function() {
	    if(this.project == undefined) {
		    return;
	    }
        var owned = Credentials.owned(this.id);
//	    var owned = this.owned;
        jQuery('#detailPanel div.owned').toggle(owned);
        jQuery('#detailPanel #updateAvailable').toggle(this.project.updateAvailable);
        
        if(this.project.hideInStoreList && !owned){
        	jQuery('#detailPanel div.buy').hide();
        } else {
        	jQuery('#detailPanel div.buy').toggle(!owned && !this.project.updateAvailable);
        }
        jQuery('#detailPanel span#myRating').toggle(Credentials.authenticated() && owned); //
        jQuery('#detailPanel .personalRating').toggle(Credentials.authenticated() && owned);
        jQuery('#detailPanel span#myRatingNoOwn').toggle(Credentials.authenticated() && !owned);
    }
    
    this.onLogin = function() {
        this.toggleCommentsLogin(true);
        jQuery('#detailPanel .comments .commentsAdd.userNotLoggedIn .warningColor').hide();
        
        this.fireEvent(DetailPanel.REQUEST_COMMENTS_RATINGS, { 
        	projectId: this.id, 
        	offset: this.commentOffset, 
        	count: this.commentPageSize,
        	exclude: false});
    }

    this.onLogout = function() {
//        this.toggleCommentsLogin(true);
        jQuery('#detailPanel .comments .commentsAdd.userNotLoggedIn .warningColor').show();
//        this.fireEvent(DetailPanel.REQUEST_COMMENTS_RATINGS, this.id);
    }

    this.onLoginBadCredentials = function() {
        jQuery('#detailPanel .comments .commentsAdd.userNotLoggedIn .warningColor').show();
    }

    this.toggleCommentsLogin = function(fade) {

        var notLoggedIn = jQuery('#detailPanel .comments .commentsAdd.userNotLoggedIn');
        var loggedIn    =jQuery('#detailPanel .comments .commentsAdd.userLoggedIn');
        var authenticated = Credentials.authenticated();

        if (!fade) {
            loggedIn.toggle(authenticated);
            notLoggedIn.toggle(!authenticated);
        }
        else {
            if (Credentials.authenticated()) {
                notLoggedIn.fadeOut(500, function(){
                   loggedIn.fadeIn(500);
                });
            }
            else {
                loggedIn.fadeOut(250, function(){
                    notLoggedIn.fadeIn(250);
                });
            }
        }
    }

    
    this.gotCommentsRatings = function(o)
    {
        this.commentsRatings = o;
        this.postCommentStatus = true;
        this.updatePostCommentButtonState();
        this.attachListenersForCommentsPanel(this.project);
        this.displayCommentsRatings();
    }
    
    this.attachListenersForCommentsPanel = function(o) {
    	var that = this;

        jQuery('textarea#Comment').unbind().keyup(function(){  
            var limit = 2000;  
            var text = jQuery(this).val();  
            var chars = text.length;  
      
            if(chars > limit){  
                var new_text = text.substr(0, limit);  
                jQuery(this).val(new_text);  
            }  
        }); 
        
        jQuery('div#detailPanel a').unbind().click(function (event) {
        	var parent = jQuery(this).parent();
        	var payload = {};
        	var eventToFire = "";
        	var fireDisplayEvent = false;
        	
        	var handled = false;  	
        	var target = this.hash.substring(1).split('?');
            var targetHash = target[0];
            var targetHashArgs = (target.length > 1)?target[1]:undefined;

            if(parent.hasClass("_disabled"))
        		return false;
            
            switch(targetHash) {
	            case 'buy':
	            	payload = o.buy;
	            	payload._type = that.type;
	            	eventToFire = DetailPanel.BUY_CLICKED;
	            	break;
	            case 'share':
	            	payload = {
	            		project:that.project,
	            		arguments:targetHashArgs
	            	};
	            	eventToFire = DetailPanel.SHARE_CLICKED;
	            	break;
	            case 'back':
	            	if(Common.hasHistory()) window.history.back();
	            	else {
	            		payload = {
	            			href: 'list?visitor&zapp'
	            		};
	            		eventToFire = DetailPanel.HREF_CLICKED;
	            	}
	            	break;
	            case 'enlarge':
	            	arrHashArgs = targetHashArgs.split('&');
	            	payload.screenshots = o.screenshots;
	            	payload.positions = arrHashArgs;
	            	eventToFire = DetailPanel.ENLARGE_IMAGE;
	            	break;
	            case 'plansAndPricing':
	            	payload = {href: 'plansAndPricing'};
	            	eventToFire = DetailPanel.HREF_CLICKED;
	            	break;
	            case 'signIn':
	            	payload = {
	                    href: 'signIn',
	                    parent: '',
	                    returnUrl: '#/detail?'+ o.publishedName,
	                    form: that.formDataToObj(jQuery('#detailPanel form').serializeArray()) // grab the matching form (if any)
	                };
	            	
	            	if (typeof payload.form.keepSignedIn != 'boolean') {
	                    // for certain browsers this property is undefined when the checkbox is not checked and just '' when it is checked...
	            		payload.form.keepSignedIn = ((payload.form.keepSignedIn == '' || payload.form.keepSignedIn == 'on')?true:false);
	                }
	            	
	            	eventToFire = DetailPanel.HREF_CLICKED;
	            	break;
	            case 'flagZapp':
	            	payload = {
	                    flagReason: jQuery('#flagComment').val(),
	                    projectId: o.id
	                };
	            	eventToFire = DetailPanel.FLAG_CLICKED;
	            	break;
	            case 'postComment':
	            	if (jQuery('#detailPanel .comments #postComment textarea').val() == '') {
	            		eventToFire = '';
	            		fireDisplayEvent = false;
	            	} else {
	            		that.postCommentStatus = false;
	            	
		            	payload = {
		                    href: this.hash.substring(1),
		                    parent: '',
		                    form: that.formDataToObj(jQuery('#detailPanel .comments ' + this.hash).serializeArray()), // grab the matching form (if any)
		                    projectId: o.id,
		                    urlName: o.urlName,
		                    projectType: o.formattedType
		                };
		                that.updatePostCommentButtonState();
		            	eventToFire = DetailPanel.HREF_CLICKED;
		            	fireDisplayEvent = false;	
	            	}
	            	break;
	            case 'updateZappGadget':
	            	payload = o;
	            	eventToFire = DetailPanel.UPDATE_ZAPPGADGET;
	            	break;
	            case 'commentPreviousPage':
	            	that.showPreviousCommentPage();
	            	break;
	            case 'commentNextPage':
	            	that.showNextCommentPage();
	            	break;
	            case 'authorProfile': 
	            	var authorProfileArgs = targetHashArgs.split('=');
	            	payload = {
	            		href: targetHash,
	            		parent: '',
	            		id: authorProfileArgs[1]
	            	};
	            	eventToFire = DetailPanel.HREF_CLICKED;
	            	fireDisplayEvent = false;
	            	break;
	            default:
	            	break;
            }            
            
            if (eventToFire != ''){
            	that.fireEvent(eventToFire, payload);
            }
            if (fireDisplayEvent) {
            	that.fireEvent(DetailPanel.DISPLAY, o.urlName);
            }
            handled = true;

            return !handled;
        });

    }
    
    this.showNextCommentPage = function () {
    	var nextOffset = this.commentOffset + this.commentPageSize;
    	if (nextOffset < this.commentTotal) {
    		this.commentOffset = nextOffset;
    	}
        this.fireEvent(DetailPanel.REQUEST_COMMENTS_RATINGS, { 
        	projectId: this.id, 
        	offset: this.commentOffset, 
        	count: this.commentPageSize,
        	exclude: false});
    }
    
    this.showPreviousCommentPage = function () {
    	var nextOffset = this.commentOffset - this.commentPageSize;
    	if (nextOffset < 0) {
    		this.commentOffset = 0;
    	} else {
    		this.commentOffset = nextOffset;
    	}
        this.fireEvent(DetailPanel.REQUEST_COMMENTS_RATINGS, { 
        	projectId: this.id, 
        	offset: this.commentOffset, 
        	count: this.commentPageSize,
        	exclude: false});
    }
    
    
    this.displayCommentsRatings = function()
    {
        if (this.commentsRatings == null || !this.detailsReady) {
            return;
        }

        var that = this;
        
        jQuery('.commentsEntry').remove();

        this.updateOwnedList();
        
        var     commentsObj;
        // get the first key
        for (var key in this.commentsRatings) {
            commentsObj = this.commentsRatings[key];
            break;
        }

        this.commentsList = commentsObj.comments;
        this.commentTotal = commentsObj.commentsCount;

        this.myRating = commentsObj.memberRating;
        if (!(this.myRating > 0)) {
            this.myRating = 0;
        }

        var averageRating = commentsObj.averageRating;
        if (!(averageRating > 0)) {
            averageRating = 0;
        }

        jQuery('#detailPanel li#avRating img').attr('src', 'com/alleni/images/stars_big_blue' + averageRating + '.png');
        jQuery('#detailPanel span#myRating img').attr('src', 'com/alleni/images/stars_big_green' + this.myRating + '.png');
        if (commentsObj.numRatings > 0) {
            jQuery('#detailPanel li#avRating span#count').text('(' + commentsObj.numRatings + ')');
        } else {
        	jQuery('#detailPanel li#avRating span#count').text('(none)');
        }

        jQuery('#detailPanel .comments #postComment textarea').val('');
        
        this.setCommentTotal();

        var     insertComment;
        var     html;

            insertComment = jQuery('#detailPanel .comments #commentList');

            for (ii = 0; ii < this.commentsList.length; ii++) {
                var     comment = this.commentsList[ii];
                html = this.getCommentEntry(comment);
                var     curComment = jQuery('#commentId_' + comment.id);
                var     node = jQuery(html);

                jQuery('div.commentsHelpfulAsk a', node).click(function(event){
                    var     commentId = jQuery(this).parents('.commentsEntry').attr('id').substring(10);
                    that.fireEvent(DetailPanel.HREF_CLICKED, {
                        href: this.hash.substring(1),
                        commentId: commentId
                    });
                    return false;
                });

                if (curComment.length > 0) {
                    curComment.replaceWith(node);
                }
                else {                    
                    node.hide();
                    insertComment.before(node);                    
                    node.slideDown();
                }
            }

            this.updateOwnedList();
            // disable link for member names on comments
            // TODO: what was this supposed to do??? SBJ
            jQuery('a.memberName', this.element).click(
            		function () {
            			return false;
            		});
    }
    
    this.insertComment = function(o) {
    	Log.debug(o);
    }
    
    this.setCommentTotal = function() {

    	var zeroComments = (this.commentTotal == 0);
    	
    	var commentsPageText = (this.commentOffset + 1) + " - " + (this.commentsList.length + this.commentOffset);
    	commentsPageText += " of " + this.commentTotal + " Comments";

    	jQuery('#detailPanel .comments span#commentPageInfo').text(commentsPageText);
    	jQuery('#detailPanel .comments span#commentCount').text(this.commentTotal);
    	jQuery('#beTheFirstToLeaveAComment').toggle(zeroComments);
    	jQuery('#detailPanel .comments .paging').toggle(!zeroComments);
    	jQuery('#detailPanel .comments .paging #commentNextPage').toggle(this.commentOffset <= this.commentsList.length);
    	jQuery('#detailPanel .comments .paging #commentPreviousPage').toggle(this.commentPageSize - this.commentOffset <= 0);
    }
    
    this.removeComment = function(id){
    	var item = jQuery('#commentId_' + id);
    	item.remove();
    	this.commentTotal-=1;
    	this.setCommentTotal();
    }

    this.commentHelpfulText = function(comment)
    {
        
    }

    this.commentFeedbackSuccess = function(o)
    {
        jQuery('div.detailMain #commentId_' + o._commentId + ' .commentsHelpful').
            text(o.helpfulcount + ' of ' + o.feedbackcount + ' found this helpful');
    }

    this.commentHelpfulAsk = function()
    {
    	var html = 'Was this helpful? | <a href="#helpful">Yes</a> | <a href="#notHelpful">No</a> | <a href="#flag">Flag </a>';
    	
    	if(Credentials.systemAdministrator()){
    		html +=  '| <a href="#deleteComment">Delete </a>';
    	}
    	
        return (Credentials.authenticated()) ? html:'Log in to rate this comment';
    }

    this.getCommentEntry = function(comment)
    {
        // getAgoString(comment.ago / 1000)
    	var owned = Credentials.owned(this.id);
    	
        var memberName = comment.firstName + ' ' + comment.lastName;

        var isMineClass = (comment.isMine) ? ' _myRating' : '';
        var color       = (comment.isMine) ? 'green' : 'blue';
        
        // JAB: Eventually the span of class memberName will be a hyperlink to a member profile. 
        // But for the time being, it's easier to make it a span than remove text-decoration in the CSS
        var retVal =
            '   <div class="commentsEntry" id="commentId_' + comment.id + '">' +
            '        <div class="commentsLeft">' + 
            //'           <a href="#"><img width="30" src="com/alleni/images/profile_photo.png"></a>' +
            '        </div>' +
            '        <div class="commentsRight">' + 
            '            <span class="personalRating"><div class="rating' + isMineClass + '"><img width="63" height="10" src="com/alleni/images/stars_big_' + color + comment.memberRating + '.png"></div></span>' +
            	
            '            <div class="commentsName"><span class="memberName">' + makeSafeString(memberName) + '</span>' +
            '                <div class="commentsTime">' + comment.verboseTimestamp + '</div>' +
            '            </div>' +
            '            <div class="commentsText">' +
            '                <p>' + makeSafeString(comment.comment) + '</p>' +
            '            </div>' +
            '            <div class="commentsHelpful">' + comment.helpfulCount + ' of ' + comment.feedbackCount + ' found this helpful <br>' +
            '            </div>' +
            '            <div class="commentsHelpfulAsk">' + this.commentHelpfulAsk() + '</div>' +
            '        </div>' +
            '        <div class="clearer"></div>' +
            '    </div>';

        return retVal;
    }  
}

DetailPanel = new Class(new DetailPanel());
DetailPanel.NAME = 'DetailPanel';

DetailPanel.INITIALIZED = 'DetailPanel.INITIALIZED';
DetailPanel.BUY_CLICKED = 'DetailPanel.BUY_CLICKED';
DetailPanel.SHARE_CLICKED = 'DetailPanel.SHARE_CLICKED';
DetailPanel.HREF_CLICKED = 'DetailPanel.HREF_CLICKED';
DetailPanel.DISPLAY = 'DetailPanel.DISPLAY';
DetailPanel.RATING = 'DetailPanel.RATING';
DetailPanel.REQUEST_COMMENTS_RATINGS = 'DetailPanel.REQUEST_COMMENTS_RATINGS';
DetailPanel.ENLARGE_IMAGE = 'DetailPanel.ENLARGE_IMAGE';
DetailPanel.UPDATE_ZAPPGADGET = "DetailPanel.UPDATE_ZAPPGADGET";
DetailPanel.FLAG_CLICKED = 'DetailPanel.FLAG_CLICKED';
ScriptLoader.loaded(DetailPanel.NAME);
