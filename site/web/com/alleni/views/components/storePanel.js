var StorePanel = function()
{
    this.Extends = UIComponent;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function ()
    {
       //Log.debug('StorePanel.initialize');
        this.parent('storePanel');
        
        var that = this;
        var o = {
            filename: 'storePanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(StorePanel.INITIALIZED);
        }
    }
}
StorePanel = new Class(new StorePanel());
StorePanel.NAME = 'StorePanel';
StorePanel.INITIALIZED = 'StorePanel.INITIALIZED';
ScriptLoader.loaded(StorePanel.NAME);
