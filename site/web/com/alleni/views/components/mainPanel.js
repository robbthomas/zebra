var MainPanel = function()
{
    this.Extends = UIComponent;
    this.contentPanel = null;
    /**
     * 
     */
    this.initialize = function(parent)
    {
        this.parent("mainPanel");

    }
    /**
     * 
     */
    this.initializationComplete = function ()
    {
    	this.contentPanel = new ContentPanel('contentPanel');
        this.addChild(this.contentPanel);
        jQuery('div#mainPanel').append(Common.clearerElement());
        this.fireEvent(MainPanel.INITIALIZED);
    }
}
MainPanel = new Class(new MainPanel());
MainPanel.NAME = 'MainPanel';
MainPanel.INITIALIZED = 'MainPanel.INITIALIZED';