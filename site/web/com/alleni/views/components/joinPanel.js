var JoinPanel = function()
{
    this.Extends = UIComponent;
    this.hasValues = false;
    this.values = {};
    this._plan = undefined;
    this.emailCache = [];
    this.forbiddenUsernameChars = ['%', '+', '>', '<', '&', ','];
    this.usernameCache = [];
    this.step2ValidationAttempt = false;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;  
    this.submitting = false;
    this.initialize = function ()
    {
        this.parent('joinPanel');

        var that = this;
        var o = {
            filename: 'joinPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
                if (this._plan)
                    this.configure(this._plan);
                // Log.debug('JoinPanel.initialize.success');
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        var that = this;        

        jQuery('div#joinPanel form').validate({ // DRY acavan - make validation
												// rules available across forms
            messages: {
                passwordConfirm: {
                    equalTo: "Passwords do not match"  
                }/*
					 * , emailConfirm: { equalTo: "Emails do not match" }
					 */
            }
        });

        // step two items
        
        jQuery('div#joinPanel div#stepTwo .uniqueUsername').keyup(function(event){
            var username = event.target.value;

            if (that.usernameCache[username] == undefined)
            {
                that.fireEvent(JoinPanel.USERNAME_UNIQUE, event.target.value);
            }
        });
        
        
        jQuery.validator.addMethod("uniqueUsername", function(value, element){
            return that.usernameCache[value] == true || that.usernameCache[value] == undefined;
        }, function(value, element){
            return 'The username "' + element.value + '" is already taken';
        });
        
        jQuery.validator.addMethod("forbiddenCharsInUsername", function(value, element) {
        	var found = false;
        	for (var indx in that.forbiddenUsernameChars) {
        		if ( value.indexOf(that.forbiddenUsernameChars[indx]) != -1) {
        			found = true;
        			break;
        		}
        	}
        	return !found;  // not found
        }, function (value, element) {
       		return 'The following characters are not allowed in the display-name field:  ' + that.forbiddenUsernameChars;
        });
        
        jQuery('div#joinPanel div#stepTwo .uniqueEmail').keyup(function(event){
            var email = event.target.value;

            if (that.emailCache[email] == undefined)
            {
                that.fireEvent(JoinPanel.EMAIL_UNIQUE, event.target.value);
            }
        });
        
        jQuery.validator.addMethod("uniqueEmail", function(value, element){
            return that.emailCache[value] == true || that.emailCache[value] == undefined;
        }, function(value, element){
            return 'The email "' + element.value + '" is already in use';
        });
        
        jQuery('.textfield').change(function(event){
        	that.setValues();
        });

        jQuery('div#applyCouponDiv').unbind('click').click(function (event) {
        	if (jQuery('div#joinPanel div#stepCouponVisitor form').valid())
            {
                that.addValues(jQuery('div#joinPanel div#stepCouponVisitor form').serializeArray());
                // validate couponCode
                var code = jQuery('div#joinPanel div#stepCouponVisitor form #couponCode').val();
                jQuery('p.couponDenied').toggle(false);
                that.fireEvent(JoinPanel.VALIDATE_COUPON, code);
                that.showUpdateAnimation(jQuery('div#busyDiv',that.element));
                jQuery('input#couponCode').unbind('keyup').keyup(function(event) {
                	jQuery('p.couponDenied').toggle(false);
                	jQuery('input#couponCode').unbind('keyup');
                });
            }
        });
        
        jQuery('div#skipCouponDiv').unbind('click').click(function (event) {
        	that.values['couponCode'] = undefined;
    		that.displayStep('stepThree');
        });
        
        jQuery('div#joinPanel div#stepTwo a').click(function (event) {
        
            switch (this.hash.substring(1))
            {
            case 'previous':
                that.fireEvent(JoinPanel.PLANS_AND_PRICING);
                return false;
                break;
            case 'continue':
                that.step2ValidationAttempt = true;
                if (jQuery('div#joinPanel div#stepTwo form').valid()) {
                    that.addValues(jQuery('div#stepTwo form').serializeArray());
                    that.values["accountType"] = that._plan.type;
                    that.values['firstLastName'] = that.values['firstName'] + ' ' + that.values['lastName'];
                    that.values['companyCityStateProvinceZipPostalCode'] = that.values['companyCity']+', '+that.values['companyStateProvince']+' '+that.values['companyZipPostalCode'];
	                
	                //that.values['billingCountry'] = that.values['billingCountry'].toUpperCase();
	                
	                
                    
                    if(that._plan.type == 'collector'){
                    	jQuery('.billingInfo').hide();
                    	that.refreshReviewValues('div#stepFour .review', that.values);
                    	that.displayStep('stepFour');
                    }else{
                    	that.displayStep('stepCouponVisitor');
                    }
                    
                }
                return false;
                break;
            	
            default:
                // Log.error('JoinPanel.stepTwo - unknown hash:'+this.hash);
            	
                break;
            }
 
        });
        
        jQuery('div#joinPanel div#stepCouponVisitor a').click(function (event) {
            switch (this.hash.substring(1)) {
                case 'back':
                    that.displayStep('stepTwo');
                    break;
                case 'close':
                    that.close();
                    break;
            }
            return false;
        });

        // step three items
        jQuery.validator.addMethod("billingZipPostalCode", function(value, element) {
            return this.optional(element) || value.match(/(^\d{5}(-\d{4})?$)|(^[ABCEGHJKLMNPRSTVXYabceghjklmnpstvxy]{1}\d{1}[A-Za-z]{1} ?\d{1}[A-Za-z]{1}\d{1})$/);
        }, "Please specify a valid postal/zip code");
        
        jQuery('div#joinPanel div#stepThree a').click(function (event) {
            event.preventDefault();
            if (this.hash.substring(1) == 'previous')
            {
                that.displayStep('stepTwo');
                return;
            }
            if (jQuery('div#joinPanel div#stepThree form').valid())
            {
                that.addValues(jQuery('div#stepThree form').serializeArray());
                if (that.values['creditCardNumber'] == undefined) {
                    that.values['creditCardNumberMasked'] = '<unknown>';
                } else {
                	that.values['creditCardNumberUnmasked'] = that.values['creditCardNumber'];
                	that.values['creditCardNumberMasked'] = '';
                	for (var i = 0; i < that.values['creditCardNumber'].length - 4; i++) {
                		that.values['creditCardNumberMasked'] = that.values['creditCardNumberMasked'] + 'x';
                	}
                    that.values['creditCardNumberMasked'] = that.values['creditCardNumberMasked'] + that.values['creditCardNumber'].substring(that.values['creditCardNumber'].length - 4);
                }
                
                that.ccNumberIsMasked = true;
                that.values['expirationDate'] = that.values['expirationMonth']+'/'+that.values['expirationYear'];
                that.values['billingCityStateProvinceZipPostalCode'] = that.values['billingCity']+', '+that.values['billingStateProvince'].toUpperCase()+' '+that.values['billingZipPostalCode'];

                switch(that.values['billingCountry']) {
	            	case 'us':
	            		that.values['billingCountry'] = 'United States';
	            		break;
	            	default:
	            		that.values['billingCountry'] = that.values['billingCountry'].toUpperCase();
	            }

                jQuery('div#joinPanel .billingInfo').toggle(that.values['couponCode'] == undefined);
                jQuery('div#joinPanel .couponInfo').toggle(that.values['couponCode'] != undefined);
                jQuery('#billingAddress2row').toggle(that.values['billingAddress2'] != undefined && that.values['billingAddress2'] != '');
                that.refreshReviewValues('div#stepFour .review', that.values);
                //This needs to be done after refreshReviewValues() because that method is updating the fields. Nothing like reinventing wheels like "HTML FORM"
                jQuery('div#stepFour .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberMasked']);
                
                that.displayStep('stepFour');
            }
        });
        
        jQuery('#toggleCCMasking').unbind('click').click(function (event) {
        	if (that.ccNumberIsMasked) {
        		jQuery('div#stepFour .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberUnmasked']);
        		jQuery('div#stepFour .billingInfo .cc span#toggleCCMaskingLabel').text('Hide');
        	} else {
        		jQuery('div#stepFour .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberMasked']);
        		jQuery('div#stepFour .billingInfo .cc span#toggleCCMaskingLabel').text('Show');
        	}
        	
        	that.ccNumberIsMasked = !that.ccNumberIsMasked;
        	this.checked = false;
        });
        
        // step four items
        jQuery('div#joinPanel div#stepFour a').click(function (event) {
            event.preventDefault();
            var href = this.hash.substring(1);
            var nextStep = undefined;
            switch (href)
            {
            case 'editMember':
                nextStep = 'stepTwo';
                break;
            case 'editBilling':
                nextStep = 'stepThree';
                break;
            case 'editCoupon':
                nextStep = 'stepCouponVisitor';
                break;
            case 'submit':
            	if(!that.submitting)
            	{
	            	that.submitting = true;
	            	//Make sure we're submitting the unmasked cc number
	            	jQuery('div#stepFour .billingInfo .cc #creditCardNumber').hide();
	            	jQuery('div#stepFour .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberUnmasked']);
	                that.fireEvent(JoinPanel.SUBMIT, that.values);
	                that.showUpdateAnimation(this);
	                jQuery(this).attr("href", '');
            	}
                break;
            default:
                break;
            }
            if (nextStep != undefined)
                that.displayStep(nextStep);
        });
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    
    this.setValues = function() 
    {
    	this.hasValues = true;
    	this.values['emailJoin'] = jQuery('#emailJoin').val();
    	this.values['emailConfirm'] = jQuery('#emailConfirm').val();
    	this.values['passwordJoin'] = jQuery('#passwordJoin').val();
    	this.values['displayName'] = jQuery('#displayName').val();
    	this.values['firstName'] = jQuery('#firstName').val();
    	this.values['lastName'] = jQuery('#lastName').val();
    	this.values['nameFormat'] = jQuery('#nameFormat').val();
    }
    
    this.getValues = function()
    {
    	jQuery('#emailJoin').val(this.values['emailJoin']);
    	jQuery('#emailConfirm').val(this.values['emailConfirm']);
    	jQuery('#passwordJoin').val(this.values['passwordJoin']);
    	jQuery('#displayName').val(this.values['displayName']);
    	jQuery('#firstName').val(this.values['firstName']);
		jQuery('#lastName').val(this.values['lastName']);
		if(this.values['nameFormat'])
			jQuery('#nameFormat').val(this.values['nameFormat']);
		else
			jQuery('#chkDisplayName').val("checked");
    }
    
    this.onSubmitSuccess = function(payload)
    {
    	this.hideUpdateAnimation();
    	this.refreshReviewValues('div#stepFive .review', this.values);
    	
    	this.displayStep('stepFive');
    	
    	if (!payload.validCreditCard && this.values["creditCardNumber"] != undefined) {
    		jQuery('.creditCardDenied').show();
    	} else {
    		jQuery('.creditCardDenied').hide();
    	}
    	
    	if (this._plan.type == 'collector') {
    		jQuery('.creatorToggle').hide();
    	} else {
    		jQuery('.creatorToggle').show();
    	}
    	
    	if(payload.couponSubmitted)
		{
    		jQuery(".couponAccepted").toggle(payload.couponApplied);
    		jQuery(".couponDenied").toggle(!payload.couponApplied);
    		if(payload.couponApplied){
	    		var termDueDate = Common.parseDate(payload.termDueDate.split(' ')[0]);
		        jQuery('div#joinPanel #currentDatePlus').text(termDueDate.format('mmmm dS, yyyy'));
    		}
		}else{
			jQuery(".couponAccepted").hide();
    		jQuery(".couponDenied").hide();
		}
    	this.submitting = false;
    }
    this.onSubmitFailure = function(payload)
    {
    	this.submitting = false;
    	this.hideUpdateAnimation();
    	alert(payload.error + ":\n" + payload.detail);
    }
    
    this.onCouponValidateSuccess = function(payload)
    {
    	this.hideUpdateAnimation();
    	
    	jQuery(".creditCardDenied").toggle(!payload.validCreditCard && this.values["creditCardNumber"] != undefined);
    	
    	if(payload.couponValid){
    		jQuery('div#stepFour .billingInfo').hide();
    		this.refreshReviewValues('div#stepFour .review', this.values);
    		
    		if(this._plan.price != 0 && this.values['couponCode'] == undefined)
			{
    			this.displayStep('stepThree');
			}else{
				jQuery('div#joinPanel .billingInfo').toggle(this.values['couponCode'] == undefined);
				jQuery('div#joinPanel .coupon').toggle(this.values['couponCode'] != undefined);
				this.displayStep('stepFour');
			}
    	}else{
    		jQuery('#stepCouponVisitor .couponDenied').show();
    	}
    }
    this.onCouponValidateFailure = function(payload)
    {
    	this.hideUpdateAnimation();
    	alert(payload.errorDetail);
    }
    
    this.showUpdateAnimation = function(target)
    {
    	this.animStart = (new Date()).valueOf();
        jQuery(target).after('<img class="updatingAnimation" src="com/alleni/images/updating.gif"/>');
    }
    
    this.hideUpdateAnimation = function()
    {
        var remainder = 2000 - ((new Date()).valueOf() - this.animStart);

        if (remainder < 0) {
            jQuery('.updatingAnimation').remove();
        }
        else {
            setTimeout(function(){
                jQuery('.updatingAnimation').remove();
            },
            2000);
        }

    }
    
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(JoinPanel.INITIALIZED);
        }

        if(this.hasValues) {
        	this.getValues();
        	// this.hasValues = null;
        }
        else if (this.initialized)
        {
        	this.values = {};
        	
            jQuery(':input').each(function (index, element) {
                var q = jQuery(this);
                switch (this.localName)
                {
                case 'input':
                    if (q.context.type == 'checkbox')
                        q.attr('checked', false);
                    else if (q.context.type != 'radio')
                        q.val('');
                    break;
                case 'select':
                    q.val(-1);
                    break;
                default:
                    // Log.debug('JoinPanel.initializationComplete - unknown
					// localName:'+this.localName);
                    break;
                }
                var x = 0;
            });

        }
    }
    this.displayStep = function (step)
    {
        this.dumpValues();
        jQuery('.step', this.element).hide();
        if(step == 'stepCouponVisitor'){
        	jQuery('div#joinPanel .coupon', this.element).show();
        	jQuery('div#joinPanel .couponDenied', this.element).hide();
        }
        jQuery('#'+step, this.element).show();
    }
    this.addStates = function (states)
    {

    }
    this.addCountries = function (countries)
    {

    }
    this.accountTypeDescription = function (accountType)
    {
        // TODO acavan - where do we get these values?
    	// DWH: Not used
        var accountTypeDescriptions = {
            basic: 'Basic - $4.99 a month',
            standard: 'Standard - $4.99 a month',
            premium: 'Premium - $4.99 a month'
        }
        return accountTypeDescriptions[accountType];
    }
    this.plan = function (plan)
    {
        // Log.debug('JoinPanel.plan - plan.type:'+plan.type);
        this._plan = plan;
        if (this.initialized)
            this.configure(plan);
    }
    this.configure = function (plan)
    {
    	this.displayStep('stepTwo');
        // Log.debug('JoinPanel.configure');
    	
    	var fullAccountName = '';
        switch(plan.name.toLowerCase()){
        case 'pro':
        	fullAccountName = 'Professional';
       	 	break;
        default:
       	 	fullAccountName = plan.name.charAt(0).toUpperCase() + plan.name.slice(1);
       	 	break;
        }
    	
        var description = '"'+ fullAccountName +'" Account - '+plan.formattedPrice;
        jQuery('span.accountType').text(fullAccountName);
        jQuery('span.accountCost').text(plan.formattedPrice);
        jQuery('h2.planDescription', this.element).text(description).toggleClass('freeTrial', plan.price != 0);
        jQuery('h3.planSubtitle', this.element).toggle(plan.price != 0);
        jQuery('.free', this.element).toggle(plan.price == 0);
        jQuery('.paid', this.element).toggle(plan.price != 0);
        
        
        jQuery('div#joinPanel .collectorToggle').toggle(this._plan.type == 'collector');
        jQuery('div#joinPanel .creatorToggle').toggle((this._plan.type == 'creator' || this._plan.type == 'pro'));
        
        var now = new Date();
        jQuery('#currentDate', this.element).text(now.format('mmmm dS, yyyy'));

        // reset these
        this.emailCache = [];
        this.usernameCache = [];
    }
    this.usernameUnique = function(results)
    {
        this.usernameCache[results.username] = results.unique;
        if (this.step2ValidationAttempt)
        {
            jQuery('div#joinPanel div#stepTwo form').valid();
        }
    }
    this.emailUnique = function(results)
    {
        this.emailCache[results.email] = results.unique;
        if (this.step2ValidationAttempt)
        {
            jQuery('div#joinPanel div#stepTwo form').valid();
        }      
    }
    this.refreshReviewValues = function (selector, values)
    {
        jQuery(selector).each(function (index, element) {
            jQuery(this).text(values[this.id]);
        })
    }
    this.dumpValues = function ()
    {
        var s = '';
        for (var i in this.values)
        {
            s += '   '+i+':'+this.values[i]+'\n';
            // alert(i+'=='+this.values[i]);
        }
       // Log.debug('JoinPanel.dumpValues\n'+s);
    }
    this.addValues = function (valueArray)
    {
        for (var i = 0; i < valueArray.length; i++)
            this.values[valueArray[i].name] = valueArray[i].value;
    }

}
JoinPanel = new Class(new JoinPanel());
JoinPanel.NAME = 'JoinPanel';
JoinPanel.INITIALIZED = 'JoinPanel.INITIALIZED';
JoinPanel.SUBMIT = 'JoinPanel.SUBMIT';
JoinPanel.VALIDATE_COUPON = 'JoinPanel.VALIDATE_COUPON';
JoinPanel.PLANS_AND_PRICING = 'JoinPanel.PLANS_AND_PRICING';
JoinPanel.EMAIL_UNIQUE = 'JoinPanel.EMAIL_UNIQUE';
JoinPanel.USERNAME_UNIQUE = 'JoinPanel.USERNAME_UNIQUE';
ScriptLoader.loaded(JoinPanel.NAME);
