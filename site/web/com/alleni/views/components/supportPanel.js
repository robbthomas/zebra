var SupportPanel = function()
{
    this.Extends = UIComponent;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;   
    this.currentFileName = 'learn-getstarted';
    this.currentFileArgument = '';
    this.webinars = undefined;
    
    this.TAB_START = "start";
    this.TAB_LEARN = "learn";
    this.TAB_KIDS = "kids";
    this.TAB_HELP = "help";
    
    this.TXT_START = "getstated";
    this.TXT_ERIK = "erik";
    this.TXT_TOUR = "tour";
    this.TXT_WEBINAR = "webinar";
    this.TXT_ZTUTORIAL = "ztutorial";
    this.TXT_HELP = "help";
    
    this.initialize = function ()
    {
        //Log.debug('SupportPanel.initialize');
        this.parent('supportPanel');
        this.loadFragment('learn/learn-getstarted.html');
        this.webinars = this.buildWebinars();
    }
    this.loadFragment = function (filename)
    {
        var that = this;
        var o = {
            filename: 'support/'+filename,
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.tabsHeader = function () {
    	var html = "";
    	html += "<div id='"+this.TAB_START+"' class='tab2'><a href='#/support?learn-getstarted'>Get Started</a></div>";
    	html += "<div id='"+this.TAB_LEARN+"' class='tab2'><a href='#/support?learn-tour-menu'>Learn ZebraZapps!</a></div>";
    	html += "<div id='"+this.TAB_KIDS+"' class='tab2'><a href='#/support?learn-erik-for-kids'>For Kids</a></div>";
    	html += "<div id='"+this.TAB_HELP+"' class='tab2'><a href='#/support?learn-help'>Help</a></div>";
    	html += "<div class='clearer'></div>";
    	return html;
    }
    
    this.selectTab = function () {
    	var a = this.currentFileName.split('-');
    	if(a.length > 1) {

	    	switch(a[1]) {
	    	case this.TXT_START:
	    		return this.TAB_START;
	    		
	    	case this.TXT_ERIK:
	    		return this.TAB_KIDS;
	    		
	    	case this.TXT_HELP:
	    		return this.TAB_HELP;
	    		
	    	case this.TXT_ZTUTORIAL:
	    	case this.TXT_TOUR:
	    	case this.TXT_WEBINAR:
	    		return this.TAB_LEARN;
	    	}
	    }
    	
		return this.TAB_START;
    }
    
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        var that = this;
        jQuery('.contentRight .tabs').append(this.tabsHeader());
        
        var selected = this.selectTab();
        jQuery('.tabs #'+selected).removeClass('tab2');
        jQuery('.tabs #'+selected).addClass('tab');
        
        jQuery('div#supportPanel a').click(function (event) {
        	var hash = this.hash;
        	var a = hash.split('?');
            var menuItem = a[0].substring(1);
            
            var menuArgument = a.length > 1 ? a[1] : '';
        	if (this.hash.indexOf('#/video') == 0)  // don't want to grab '#/support?objects/video'
            {
                event.preventDefault();
                jQuery.History.trigger(menuItem);
                return false;
            }
            if (menuItem.indexOf('/') == -1) //TODO acavan - doesn't work with Chrome (offset is always 0,0) but does with Safari...
            {
                var offset = jQuery(menuItem).offset();
                window.scrollTo(offset.left, offset.top);
                return false;
            }
        });
        
        switch(this.currentFileName){
        
        	case "learn-tour-menu":
        		if(this.currentFileArgument != '')
        		{
	        		jQuery('iframe#videoDisplay').attr("src", "https://player.vimeo.com/video/"+this.currentFileArgument+"?title=0&amp;byline=0&amp;portrait=0" );
	        		jQuery('iframe#videoDisplay').show();
	        		jQuery('div#videoSubMenu').show();
	        		jQuery('div#videoInfo').show();
	        		jQuery('table#biography').hide();
	        		jQuery('#videoSubMenu a').removeClass('getStarted-pagenumberhilite');
	        		jQuery('#videoSubMenu a.'+this.currentFileArgument).addClass('getStarted-pagenumberhilite');
	        		jQuery('.videoDetail').hide();
	        		jQuery('.'+this.currentFileArgument).show();
	        		jQuery('#getStarted-paragraph h3').hide();
	        		jQuery('#getStarted-paragraph h3.'+this.currentFileArgument).show();
        		}else{
        			jQuery('iframe#videoDisplay').hide();
        			jQuery('div#videoSubMenu').hide();
        			jQuery('div#videoInfo').hide();
        			jQuery('table#biography').show();
        		}
        	break;
        	case "learn-webinar-menu":
        		if(this.currentFileArgument != '')
        		{
        			var webinar = this.webinars['_'+this.currentFileArgument];
        			var topicsString = '';
        			var topics = webinar.topics();
        			var recommended = webinar.additionalWebinars();
        			var parts = webinar.parts();
        			var partsString = '';
        			if(webinar != undefined){
        				jQuery('iframe#videoDisplay').attr("src", "https://player.vimeo.com/video/"+webinar.videoId()+"?title=0&amp;byline=0&amp;portrait=0" );
        				jQuery('span#webinarName').text(webinar.name());
        				jQuery('span#webinarTitle').text(webinar.title());
        				jQuery('span#webinarDescription').text(webinar.description());
        				
        				for(var i = 0; i<topics.length; i++){
        					topicsString += '• '+topics[i]+'<br />';
        				}
        				jQuery('td#webinarTopics').html(topicsString);
        				jQuery('span#webinarLead').text(webinar.lead());
        				jQuery('span#webinarLeadPosition').text(webinar.leadPosition());
        				if(webinar.leadTwitterURL() != ''){
        					jQuery('a#webinarTwitter').attr('href', webinar.leadTwitterURL());
        					jQuery('a#webinarTwitter').text(webinar.leadTwitterURL());
        					jQuery('span.webinar-twitter').show();
        				}else{
        					jQuery('span.webinar-twitter').hide();
        				}
        				
        				jQuery('img#leadPhotoURL').attr("src", webinar.leadPhotoURL());
        				jQuery('a.downloadsURL').attr("href", webinar.downloadsURL());
        				
        				jQuery('div#webinarDetail').show();
            			jQuery('div#webinarMenu').hide();
            			
            			// build recommended webinars menu if needed
            			if(recommended.length > 0){
            				for(var i = 0; i<recommended.length; i++){
            					jQuery('tr#webinarRecommendedHeader').after(
            						'<tr><td></div><span class="webinar-Heading1"><a href="#/support?learn-webinar-menu/'+ recommended[i].id()+'">'+recommended[i].name() +':</a></span> <br /> <span class="webinar-Heading2">'+ recommended[i].title()+'</span></td></tr>'
            					);
            				}
            				jQuery('table#additionalWebinars').show();
            			}else{
            				jQuery('table#additionalWebinars').hide();
            			}
            			
            			// build parts menu if needed
            			if(parts.length > 0){
            				for(var i = 0; i<parts.length; i++){
            					if(parts[i].id() == webinar.id()){
            						partsString += '<a href="#/support?learn-webinar-menu/'+ parts[i].id() +'"> <span class="getStarted-pagenumberhilite">Part'+ (i+1) +'</span></a>';
            					}else{
            						partsString += '<a href="#/support?learn-webinar-menu/'+ parts[i].id() +'"> Part'+ (i+1) +' </a>';
            					}
            					
            				}
            				jQuery('td#webinarParts').html(partsString);
            			}else{
            				jQuery('td#webinarParts').html('');
            			}
        			}
        		}else{
        			jQuery('div#webinarDetail').hide();
        			jQuery('div#webinarMenu').show();
        		}
        	break;
        }
        
        this.fragmentProcessed = true;
        if (!this.initialzied && this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(SupportPanel.INITIALIZED);
        }
    }
    this.display = function(filename)
    {
    	var a = filename.split('/');
        var _fileName = a[0];
        var _fileArgument = a.length > 1 ? a[1] : '';
    	this.currentFileName = _fileName;
    	this.currentFileArgument = _fileArgument;
        this.loadFragment('learn/'+_fileName + '.html');
    }
    
    this.buildWebinars = function()
    {
    	//(id,name,title,videoId,description,downloadsURL,topics,lead,leadPosition)
    	var _0101 = new Webinar(
    			true,
        		'0101',
        		'Business Simulation - 1',
        		'Economics of the Lemonade Stand',
        		'29116740',
        		'This webinar builds a business simulation, using a "Lemonade Stand" as a metaphor. Attendees build a complete simulation, learn to manage large amounts of interactive content with arenas, and create interactions with dynamic feedback.',
        		'com/alleni/downloads/webinar-assets-Lemonade.zip',
        		[
	        		 'Introduction to arenas',
	        		 'Manage arena content',
	        		 'Arena navigation',
	        		 'Sketching a simulation',
	        		 'Creating dynamic text',
	        		 'Dynamic feedback'
        		 ],
        		 'Christopher Allen',
        		 'Product Marketing Manager',
        		 'com/alleni/html/fragments/support/images/people-chris.jpg',
        		 'http://twitter.com/#!/C_More_Zebras'
        		);
    	
    	var _0102 = new Webinar(
    			false,
    			'0102',
    			'Business Simulation - 2',
        		'Economics of the Lemonade Stand',
        		'29114397',
        		'This webinar builds a business simulation, using a "Lemonade Stand" as a metaphor. Attendees build a complete simulation, learn to manage large amounts of interactive content with arenas, and create interactions with dynamic feedback.',
        		'com/alleni/downloads/webinar-assets-Lemonade.zip',
        		[
	        		 'Introduction to arenas',
	        		 'Manage arena content',
	        		 'Arena navigation',
	        		 'Sketching a simulation',
	        		 'Creating dynamic text',
	        		 'Dynamic feedback'
        		 ],
        		 'Christopher Allen',
        		 'Product Marketing Manager',
        		 'com/alleni/html/fragments/support/images/people-chris.jpg',
        		 'http://twitter.com/#!/C_More_Zebras'
    	);
    	
    	var _0201 = new Webinar(
    			true,
    			'0201',
    			'Emergency Response Simulation - 1',
        		'Using an AED: Automated External Defibrillator',
        		'30101151',
        		'This webinar builds an emergency response simulation, to teach responders how to use an AED - an Automated External Defibrillaton device - during an intervention. Attendees create paging structures with arenas, build and integrate gadgets into their interaction, and construct delayed feedback.',
        		'com/alleni/downloads/webinar-assets-AED.zip',
        		[
	        		 'Paging structures',
	        		 'Gadget integration',
	        		 'Delayed feedback'
        		 ],
        		 'Scott Colehour',
        		 'Chief Solutions Architect',
        		 'com/alleni/html/fragments/support/images/people-scott.jpg',
        		 ''
    	);
    	var _0202 = new Webinar(
    			false,
    			'0202',
    			'Emergency Response Simulation - 2',
        		'Using an AED: Automated External Defibrillator',
        		'30185014',
        		'This webinar builds an emergency response simulation, to teach responders how to use an AED - an Automated External Defibrillaton device - during an intervention. Attendees create paging structures with arenas, build and integrate gadgets into their interaction, and construct delayed feedback.',
        		'com/alleni/downloads/webinar-assets-AED.zip',
        		[
	        		 'Paging structures',
	        		 'Gadget integration',
	        		 'Delayed feedback'
        		 ],
        		 'Scott Colehour',
        		 'Chief Solutions Architect',
        		 'com/alleni/html/fragments/support/images/people-scott.jpg',
        		 ''
    	);
    	var _0301 = new Webinar(
    			true,
    			'0301',
    			'Puzzle Game - 1',
        		'Interacting with a Classic Florida Postcard',
        		'31207779',
        		'This webinar builds an interactive puzzle with answer judging, using a classic Florida postcard as inspiration. Attendees experiment with wiring methods and drag and drop interactivity. They use sliders as counters, learn how to replace assets, and use arenas and gadgets to complete the interaction.',
        		'com/alleni/downloads/webinar-assets-Florida-Puzzle.zip',
        		[
					'Wiring',
					'Drag & drop interactivity',
					'Custom setters',
					'Sliders as counters',
					'Asset replacement',
					'Arenas',
					'Gadgets'
        		 ],
        		 'Ethan Edwards',
        		 'Chief Instructional Strategist',
        		 'com/alleni/html/fragments/support/images/people-ethan.jpg',
        		 ''
    	);
    	var _0302 = new Webinar(
    			false,
    			'0302',
    			'Puzzle Game - 2',
        		'Interacting with a Classic Florida Postcard',
        		'31207963',
        		'This webinar builds an interactive puzzle with answer judging, using a classic Florida postcard as inspiration. Attendees experiment with wiring methods and drag and drop interactivity. They use sliders as counters, learn how to replace assets, and use arenas and gadgets to complete the interaction.',
        		'com/alleni/downloads/webinar-assets-Florida-Puzzle.zip',
        		[
					'Wiring',
					'Drag & drop interactivity',
					'Custom setters',
					'Sliders as counters',
					'Asset replacement',
					'Arenas',
					'Gadgets'
        		 ],
        		 'Ethan Edwards',
        		 'Chief Instructional Strategist',
        		 'com/alleni/html/fragments/support/images/people-ethan.jpg',
        		 ''
    	);
    	
    	var _0401 = new Webinar(
    			   true,
    			       '0401',
    			       'Introducing ZebraZapps - 1',
    			       'Building Your First Set of e-Learning Interactions',
    			       '31883556',
    			       'Session One provides a complete introduction to ZebraZapps. Experience what kind of applications ZebraZapps can produce, and how easy it is to create interactivity and share your work with others. Attendees lay out content, create roll-overs, replace objects and include YouTube video in a ZebraZapps project.',

    			       'com/alleni/downloads/webinar-assets-Thermostat.zip',
    			       [
    			       'Creating new objects',
    			       'Visually wiring logic',
    			       'Uploading assets',
    			       'Wiring button actions',
    			       'Incorporating video',
    			       'Using arenas'
    			       ],
    			       'Christopher Allen',
    			       'Product Marketing Manager',
    			       'com/alleni/html/fragments/support/images/people-chris.jpg',
       		 		   'http://twitter.com/#!/C_More_Zebras'
    			       );

    	var _0402 = new Webinar(
    			   false,
    			   '0402',
    			   'Introducing ZebraZapps  - 2',
    			       'Building Your First Set of e-Learning Interactions',
    			       '31978869',
    			       'Session Two builds on the foundations covered in Session One. It introduces attendees to several basic concepts about building a robust e-learning piece with interactive discovery and information delivery',
    			       'com/alleni/downloads/webinar-assets-Thermostat.zip',
    			       [
    			       'Simulations & truth tables',
    			       'Bind properties with sliders',
    			       'Add graphics to buttons',
    			       'Basic content navigation'
    			       ],
    			       'Christopher Allen',
    			       'Product Marketing Manager',
    			       'com/alleni/html/fragments/support/images/people-chris.jpg',
       		 		   'http://twitter.com/#!/C_More_Zebras'
    			   );
    	
    	var _0501 = new Webinar(
    			   true,
    			       '0501',
    			       'Introducing ZebraZapps - 1',
    			       'Employee Security e-Learning Course',
    			       '32930970',
    			       'Build a Well-Known Allen Interactions Course in ZebraZapps! In this three-day webinar series, re-create a portion of an award-winning e-learning course Allen Interactions developed for Corning Incorporated. Day One introduces the concepts, and builds the course introduction.',

    			       'com/alleni/downloads/webinar-assets-Security.zip',
    			       [
    			       'Simple sequences',
    			       'Drag and drop interactions',
    			       'Providing correct and incorrect feedback',
    			       'Simple counters',
    			       'Object replacement'
    			       ],
    			       'Ethan Edwards',
    			       'Chief Instructional Strategist',
    			       'com/alleni/html/fragments/support/images/people-ethan.jpg'
    			       );

    	var _0502 = new Webinar(
    			   true,
    			       '0502',
    			       'Introducing ZebraZapps - 2',
    			       'Employee Security e-Learning Course',
    			       '32991627',
    			       'Build a Well-Known Allen Interactions Course in ZebraZapps! In this three-day webinar series, re-create a portion of an award-winning e-learning course Allen Interactions developed for Corning Incorporated. Day Two builds an office simulation as part of the course.',

    			       'com/alleni/downloads/webinar-assets-Security.zip',
    			       [
    			       'Simple sequences',
    			       'Drag and drop interactions',
    			       'Providing correct and incorrect feedback',
    			       'Simple counters',
    			       'Object replacement'
    			       ],
    			       'Ethan Edwards',
    			       'Chief Instructional Strategist',
    			       'com/alleni/html/fragments/support/images/people-ethan.jpg'
    			       );

    	var _0503 = new Webinar(
    			   true,
    			       '0503',
    			       'Introducing ZebraZapps - 3',
    			       'Employee Security e-Learning Course',
    			       '33282819',
    			       'Build a Well-Known Allen Interactions Course in ZebraZapps! In this three-day webinar series, re-create a portion of an award-winning e-learning course Allen Interactions developed for Corning Incorporated. Day Three continues the office simulation and summarizes the course.',

    			       'com/alleni/downloads/webinar-assets-Security.zip',
    			       [
    			       'Simple sequences',
    			       'Drag and drop interactions',
    			       'Providing correct and incorrect feedback',
    			       'Simple counters',
    			       'Object replacement'
    			       ],
    			       'Ethan Edwards',
    			       'Chief Instructional Strategist',
    			       'com/alleni/html/fragments/support/images/people-ethan.jpg'
    			       );

    	var _0601 = new Webinar(
    			   true,
    			       '0601',
    			       'Designing Interactivity - 1',
    			       'Promoting Practice',
    			       '39796713',
    			       "Performance at critical moments requires practice and confidence, two things well designed e-learning can provide. Creating practice environments doesn't have to be hard, especially if you can design scalable activities. In this two-part webinar series, learn how to quickly create exercises which can be scaled and scored, without using a multiple choice test. This webinar is led by Christopher Allen.",

    			       'com/alleni/downloads/webinar-assets-placesettings.zip',
    			       [
    			       'Review: ZebraZapps basics',
    			       'Building with purpose (scalable events)',
    			       'Providing correct and incorrect feedback',
    			       'Scoring'
    			       ],
    			       'Christopher Allen',
    			       'Product Marketing Manager',
    			       'com/alleni/html/fragments/support/images/people-chris.jpg',
       		 			'http://twitter.com/#!/C_More_Zebras'
    			       );
    	var _0602 = new Webinar(
    			   true,
    			       '0602',
    			       'Designing Interactivity - 2',
    			       'Promoting Practice',
    			       '39860809',
    			       "Performance at critical moments requires practice and confidence, two things well designed e-learning can provide. Creating practice environments doesn't have to be hard, especially if you can design scalable activities. In this two-part webinar series, learn how to quickly create exercises which can be scaled and scored, without using a multiple choice test. This webinar is led by Christopher Allen.",

    			       'com/alleni/downloads/webinar-assets-placesettings.zip',
    			       [
    			       'Adding challenge',
    			       'Preview LMS connectivity',
    			       'Publishing content to: Facebook, HTML, and LMS'
    			       ],
    			       'Christopher Allen',
    			       'Product Marketing Manager',
    			       'com/alleni/html/fragments/support/images/people-chris.jpg',
       		 			'http://twitter.com/#!/C_More_Zebras'
    			       );
    	
    	var _0701 = new Webinar(
    			   	true,
    			       '0701',
    			       'Designing Interactivity - 1',
    			       'Creating a set of Multi-part Interactions',
    			       '42297905',
    			       "In this two-part webinar, gain experience with arenas, answer tables, styling buttons and object replacement by building out a check fraud interaction. This webinar is led by Christopher Allen.",

    			       'com/alleni/downloads/webinar-assets-checkfraud.zip',
    			       [
    			       'Arenas',
    			       'Answer Tables',
    			       'Styling Buttons',
    					'Object Replacement'
    			       ],
    			       'Christopher Allen',
    			       'Product Marketing Manager',
    			       'com/alleni/html/fragments/support/images/people-chris.jpg',
       		 			'http://twitter.com/#!/C_More_Zebras'
    			       );
    	var _0702 = new Webinar(
    			   	true,
    			       '0702',
    			       'Designing Interactivity - 2',
    			       'Creating a set of Multi-part Interactions',
    			       '42367783',
    			       "In this two-part webinar, gain experience with arenas, answer tables, styling buttons and object replacement by building out a check fraud interaction. This webinar is led by Christopher Allen.",

    			       'com/alleni/downloads/webinar-assets-checkfraud.zip',
    			       [
    			        'Arenas',
    			       'Answer Tables',
    			       'Styling Buttons',
    					'Object Replacement'
    			       ],
    			       'Christopher Allen',
    			       'Product Marketing Manager',
    			       'com/alleni/html/fragments/support/images/people-chris.jpg',
       		 			'http://twitter.com/#!/C_More_Zebras'
    			       );
    	var _0801 = new Webinar(
    				true,
    			       '0801',
    			       'Designing Interactivity',
    			       'Flag Day',
    			       '44832503',
    			       "This 90-minute video leads ZebraZapps users through several different methods for creating interactivity. At the end of this webinar you will have gained experience using arenas, arena transitions, object replacement, drag and drop triggers, collision triggers and more.",
    			       'com/alleni/downloads/webinar-assets-flag-day.zip',
    			       [
    			        'Arenas',
    			       'Arena Transitions',
    			       'Object Replacement',
    					'Drag and Drop'
    			       ],
    			       'Christopher Allen',
    			       'Product Marketing Manager',
    			       'com/alleni/html/fragments/support/images/people-chris.jpg',
       		 			'http://twitter.com/#!/C_More_Zebras'
    				);
    				// associate suggested webinars
    			   _0101.additionalWebinars([_0401,_0201,_0301]);
    			   _0102.additionalWebinars([_0401,_0201,_0301]);
    			   _0201.additionalWebinars([_0401,_0101,_0301]);
    			   _0202.additionalWebinars([_0401,_0101,_0301]);
    			   _0301.additionalWebinars([_0401,_0101,_0201]);
    			   _0302.additionalWebinars([_0401,_0101,_0201]);
    			   _0401.additionalWebinars([_0101,_0201,_0301]);
    			   _0402.additionalWebinars([_0101,_0201,_0301]);
    			   _0501.additionalWebinars([_0401,_0101,_0201,_0301]);
    			   _0502.additionalWebinars([_0401,_0101,_0201,_0301]);
    			   _0503.additionalWebinars([_0401,_0101,_0201,_0301]);
    				
    			   // associate webinar parts
    			   _0101.parts([_0101,_0102]);
    			   _0102.parts([_0101,_0102]);
    			   _0201.parts([_0201,_0202]);
    			   _0202.parts([_0201,_0202]);
    			   _0301.parts([_0301,_0302]);
    			   _0302.parts([_0301,_0302]);
    			   _0401.parts([_0401,_0402]);
    			   _0402.parts([_0401,_0402]);
    			   _0501.parts([_0501,_0502,_0503]);
    			   _0502.parts([_0501,_0502,_0503]);
    			   _0503.parts([_0501,_0502,_0503]);
    			   _0601.parts([_0601,_0602]);
    			   _0602.parts([_0601,_0602]);
    			   _0701.parts([_0701,_0702]);
    			   _0702.parts([_0701,_0702]);
    	
    	return {
    		_0101:_0101,
    		_0102:_0102,
    		_0201:_0201,
    		_0202:_0202,
    		_0301:_0301,
    		_0302:_0302,
    		_0401:_0401,
    		_0402:_0402,
    		_0501:_0501,
    		_0502:_0502,
    		_0503:_0503,
    		_0601:_0601,
    		_0602:_0602,
    		_0701:_0701,
    		_0702:_0702,
    		_0801:_0801
    	};
    	
    }
}
SupportPanel = new Class(new SupportPanel());
SupportPanel.NAME = 'SupportPanel';
SupportPanel.INITIALIZED = 'SupportPanel.INITIALIZED';
SupportPanel.LINK_ITEM_SELECTED = 'SupportPanel.LINK_ITEM_SELECTED';
ScriptLoader.loaded(SupportPanel.NAME);
