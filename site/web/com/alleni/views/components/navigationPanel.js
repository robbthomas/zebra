var NavigationPanel = function()
{
    this.Extends = UIComponent;
    this.menuItems = [];
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function()
    {
        this.parent('navigationPanel');

        var that = this;
        var o = {
            filename: 'navigationPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        for (var i = 0; i < this.menuItems.length; i++)
        {
            var menuItem = this.menuItems[i];
            this.appendMenuItem(menuItem.id, menuItem.text);
        }
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(NavigationPanel.INITIALIZED);
        }
    }
    this.appendMenuItem = function (id, text)
    {
        var that = this;
        var onClickMenuItem = function (event) {
                event.preventDefault();
                var id = jQuery(this).attr('id');
                that.fireEvent(NavigationPanel.MENU_ITEM_SELECTED, id);
        }
        if (this.initialized)
        {
            jQuery('ul#menu', this.element).append('<li><a id="'+id+'" href="#'+id+'">'+text+'</a></li>')
            jQuery('a#'+id).click(onClickMenuItem);
        }
        else
            this.menuItems.push({id: id, text: text});
    }
}
NavigationPanel = new Class(new NavigationPanel());
NavigationPanel.INITIALIZED = 'NavigationPanel.INITIALIZED';
