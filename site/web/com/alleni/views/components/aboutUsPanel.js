var AboutUsPanel = function()
{
    this.Extends = UIComponent;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function ()
    {
        this.parent('aboutUsPanel');
        
        var that = this;
        var o = {
            filename: 'aboutUsPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(AboutUsPanel.INITIALIZED);
        }
    }
}
AboutUsPanel = new Class(new AboutUsPanel());
AboutUsPanel.NAME = 'AboutUsPanel';
AboutUsPanel.INITIALIZED = 'AboutUsPanel.INITIALIZED';
ScriptLoader.loaded(AboutUsPanel.NAME);
