var AuthorProfilePanel = function()
{
    this.Extends = UIComponent;
    this.group = 'visitor';
    this.type = undefined;
    this.authorMemberId = undefined;
    this.authorMemberIdChanged = false;
    
    this.initialize = function (parent)
    {
    	this.parent(parent);
    	Log.warn('authorProfilePanel.intialize - message:'+parent);
        var that = this;
        var o = {
            filename: 'authorProfilePanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o); 
    }
    this.processFragment = function (fragment) 	
    {
        this.set('html', fragment);

        var that = this;
        
        jQuery('div.tab.authorProfile a', this.element).unbind('click').click(function () {
     		var target = this.hash.substring(1).split('?');
    		var projectType = target[1].split('&')[0];
    		if (projectType != that.type) {
    			that.type = projectType;
    			that.fireEvent(AuthorProfilePanel.TAB_CLICKED, {group: that.group, type: projectType});
    			that.updateTabs(projectType);
    		}
    	});
    	
    	this.updateTabs(this.type);
        this.fireEvent(AuthorProfilePanel.INITIALIZED, {authorMemberId: this.authorMemberId});
    }
    
    this.display = function(displayArguments) {
    	var urlArgs = displayArguments.split('&');
    	var amIDarg = urlArgs[urlArgs.length - 1].split('='); //Author Member ID should always be the final URL arg
    	
    	this.setAuthorMemberId(amIDarg[1]);
    	this.group = 'visitor';
    	if (urlArgs.length == 1) {
    		this.type = 'zapp';
    	} else {
    		this.type = urlArgs[0];
    	}
    	
    	this.updateTabs(this.type);
    	this.fireEvent('AuthorProfilePanel.DISPLAY', {group: this.group, type: this.type, authorMemberId: this.authorMemberId});
    	
    }
    
    this.getGroup = function() {
    	return this.group;
    }
    
    this.getType = function() {
    	return this.type;
    }
    
    this.setAuthorMemberId = function(newAuthorMemberId) {
    	this.authorMemberId = newAuthorMemberId;
    };
    
    this.displayAuthorData = function(authorData) {
    	var fields = 'twitter,website,bio,company';
    	var arrFields = fields.split(',');
    	var currentField = '';
    	var currentValue = '';
    	var currentTableId = '';
    	var link = '';
    	
    	jQuery('#authorNameTitle', this.element).text(authorData.displayName);
    	
    	//this.hideAuthorAvatar();
    	//this.hideAuthorBanner();
    	
    	for (var i = 0; i < arrFields.length; i++) {
    		currentField = arrFields[i];
    		currentValue = authorData[currentField];
    		currentTableId = currentField + 'Data';
    		index = currentValue.indexOf(":",4);
    		
    		if (index != -1) {
    			link = currentValue;
    		} else {		
    			link = 'http://' + currentValue;
    		}
    		
    		if(currentValue != null && currentValue != '') {
    			jQuery('#' + currentTableId, this.element).show();
    		}
    		
    		if (currentValue == '' || currentValue == null) {
    			jQuery('#' + currentField, this.element).hide();
    		} else {
    			Log.warn('Show ' + currentField);
    			jQuery('#' + currentField, this.element).show();
    			jQuery('#' + currentField + 'Label', this.element).show();
    			
    			if (currentField == 'website'){
    			
    			if( jQuery('#' + currentField + 'Data', this.element).children() != null)
    			{
    				jQuery('#' + currentField + 'Data', this.element).children().remove();
    			}	
    			
    			jQuery('#' + currentField + 'Data', this.element).append("<a></a>");
  				jQuery('#' + currentField + 'Data' , this.element).children().attr("href", link);
				jQuery('#' + currentField + 'Data' , this.element).children().show();
    			jQuery('#' + currentField + 'Data' , this.element).children().text(currentValue);
				}
				
				else if (currentField == 'bio'){
				jQuery('#' + currentField + 'Data', this.element).children().show();
				jQuery('#' + currentField + 'Data', this.element).children().text(currentValue);   
				}
				
				else{
				jQuery('#' + currentField + 'Data', this.element).show();
    			jQuery('#' + currentField + 'Data', this.element).text(currentValue);
				}
    		}
    	}
    }
    
    this.displayAuthorAvatar = function(avatarData) {
    	var UATSMT = avatarData.uploadableAssetTypeSupportedMimeType;
    	var UAS3B = UATSMT.uploadableAssetS3Bucket;
    	var BP = UAS3B.bucketpath;
    	var imagePath = BP.replace('/', '.s3.amazonaws.com/');
    	var fullPath = 'http://' + imagePath + '/' + avatarData.fileid;
    	var test = jQuery('#authorIcon', this.element);
    	jQuery('#bioLabel', this.element).css('padding-right', '-20px');
    	jQuery('#authorImg', this.element).show();
    	jQuery('#authorImg', this.element).attr('src', fullPath);
    	jQuery('#authorImg', this.element).attr('alt', avatarData.caption);
    	jQuery('#authorIcon', this.element).css('padding-right', '10px');
    	jQuery('#authorDataContainer', this.element).css('padding-bottom', '10px');
    };
    
    this.hideAuthorAvatar = function() {
    	jQuery('#authorImg').hide();
    };
    
    this.displayAuthorBanner = function(bannerData) {
    	var UATSMT = bannerData.uploadableAssetTypeSupportedMimeType;
    	var UAS3B = UATSMT.uploadableAssetS3Bucket;
    	var BP = UAS3B.bucketpath;
    	var imagePath = BP.replace('/', '.s3.amazonaws.com/');
    	var fullPath = 'http://' + imagePath + '/' + bannerData.fileid;
    	jQuery('#profileTable', this.element).css('margin-left', '0px');
    	jQuery('#profileData', this.element).css('width', '730px');
    	jQuery('#profileData', this.element).css('height', '233px');
    	jQuery('#profileData', this.element).css('background-repeat', 'no-repeat');
    	jQuery('#profileData', this.element).css('background-size', 'cover');
    	jQuery('#profileTable', this.element).css('background-image', "url('"+ fullPath + "')");
    	jQuery('#profileTable', this.element).css('margin-top', '-20px');
    	jQuery('#profileData', this.element).css('background-size', '730px 233px');
    	jQuery('#profileData', this.element).css('-o-background-size', '730px 233px');
    	jQuery('#profileData', this.element).css('-webkit-background-size', '730px 233px');
    	jQuery('#profileData', this.element).css('-khtml-background-size', '730px 233px');
    	jQuery('#profileData', this.element).css('-moz-background-size', '730px 233px');
    	
    	jQuery('#profileData', this.element).css('opacity:0.4');
    };
    
    this.hideAuthorBanner = function() {
    	jQuery('#profileTable', this.element).css('margin-left', '0px');
    	jQuery('#profileTable', this.element).css("background-image", 'none');
		jQuery('#profileData', this.element).css("height", 100);
    };
    
    this.updateTabs = function (projectType) {
    	//This seems hokey, and I'm sure there's a better way to do it. But it works. Similar functionality is in listPanel.js
    	var that = this;
    	jQuery('div.tab.authorProfile', this.element).removeClass('selected');
    	jQuery('div.tab.authorProfile', this.element).addClass('unselected');
    	
		jQuery('div.tab.authorProfile.' + projectType, this.element).removeClass('unselected');
        jQuery('div.tab.authorProfile.' + projectType, this.element).addClass('selected');
        
        jQuery('div.tab.authorProfile a', this.element).each(function() {
        	var newHref = this.href.split('=')[0] + '=' + that.authorMemberId;
        	this.href = newHref;
        });
    };
    
    this.injectView = function(divId, fragment){
    	jQuery('div#' + divId, this.element).html(fragment);
    }
}
AuthorProfilePanel = new Class(new AuthorProfilePanel());
AuthorProfilePanel.NAME = 'AuthorProfilePanel';
AuthorProfilePanel.INITIALIZED = 'AuthorProfilePanel.INITIALIZED';
AuthorProfilePanel.DISPLAY = 'AuthorProfilePanel.DISPLAY';
AuthorProfilePanel.REQUEST_BROWSE_LIST_REPLY = 'AuthorProfilePanel.REQUEST_BROWSE_LIST_REPLY';
AuthorProfilePanel.PREVIOUS = 'AuthorProfilePanel.PREVIOUS';
AuthorProfilePanel.TAB_CLICKED = 'AuthorProfilePanel.TAB_CLICKED';
ScriptLoader.loaded(AuthorProfilePanel.NAME);
