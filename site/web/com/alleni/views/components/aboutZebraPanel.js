var AboutZebraPanel = function()
{
    this.Extends = UIComponent;
  
    this.initialize = function ()
    {
        this.parent('aboutZebraPanel');
        this.loadFragment('what.html');
    }
    this.loadFragment = function(filename)
    {
        var that = this;
        var o = {
            filename: 'about/' + filename,
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        window.scrollTo(0,0);

            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
    }
    this.initializationComplete = function ()
    {
         this.fireEvent(AboutZebraPanel.INITIALIZED);
    }
    this.display = function(filename)
    {
        this.loadFragment(filename + '.html');
    }
}
AboutZebraPanel = new Class(new AboutZebraPanel());
AboutZebraPanel.NAME = 'AboutZebraPanel';
AboutZebraPanel.INITIALIZED = 'AboutZebraPanel.INITIALIZED';
AboutZebraPanel.LINK_ITEM_SELECTED = 'AboutZebraPanel.LINK_ITEM_SELECTED';
ScriptLoader.loaded(AboutZebraPanel.NAME);
