var WelcomeDialog = function()
{
    this.Extends = UIComponent;
    this.dialog = undefined;
    this.viewName = undefined;

    this.initialize = function (viewName)
    {
    	this.viewName = (viewName != undefined)?viewName:WelcomeDialog.NAME;
    	// same as calling super
    	this.parent(this.viewName);
    }

    
}
WelcomeDialog = new Class(new WelcomeDialog());
WelcomeDialog.NAME = 'WelcomeDialog';
WelcomeDialog.LOAD_COMPLETE = 'WelcomeDialog.LOAD_COMPLETE';
WelcomeDialog.CLOSE = "WelcomeDialog.CLOSE";