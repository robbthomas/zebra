var ContentPanel = function()
{
    this.Extends = UIComponent;
    this.contentPanel = undefined;
    this.sidebarPanel = null;
    /**
     * 
     */
    this.initialize = function(parent)
    {
        this.parent(parent);
    }
    /**
     * 
     */
    this.initializationComplete = function ()
    {
        this.fireEvent(ContentPanel.INITIALIZED);
    }
    /**
     * 
     */
    this.removeContent = function ()
    {
        if (this.contentPanel && this.contentPanel.customDispose)
            this.contentPanel.customDispose();

        if (this.contentPanel && this.contentPanel.dispose)
            this.contentPanel.dispose();
    }
    /**
     * 
     */
    this.injectView = function (content)
    {
        this.removeContent();
        this.contentPanel = content;
        this.addChild(this.contentPanel);
    }
    /**
     * 
     */
    this.setDisplayArgument = function(payload) {
        if (typeof payload.contentPanel.panel.display == 'function' && payload.menuArgument != undefined && payload.menuArgument.length > 0) {
            payload.contentPanel.panel.display(payload.menuArgument);
        }
    }
}
ContentPanel = new Class(new ContentPanel());
ContentPanel.INITIALIZED = 'ContentPanel.INITIALIZED';
