var BuyDialog = function()
{
    this.Extends = UIComponent;
    this.dialog = undefined;
    this.item = undefined;
    this.values = {};
    this._plan = undefined;
    this.usernameCache = [];
    this.emailCache = [];
    this.step2ValidationAttempt = false;
    this.joinProxy = null;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.accountInfo = undefined;
    this.animStart = undefined;
    this.existingMemberLogin = false;
    
    this.initialize = function ()
    {
        //Log.debug('BuyDialog.initialize');
        this.parent('buyDialog');
        
        var that = this;
        var o = {
            filename: 'buyDialog.html',
            success: function (fragment, status) {
                //Log.debug('BuyDialog.initialize.success');
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    
    this.processFragment = function (fragment)
    {
        //Log.debug('BuyDialog.processFragment - @top');
        this.set('html', fragment);
        var that = this;
        //Log.debug('BuyDialog.processFragment 0 - className:'+document.getElementById('buyDialog').className);
        this.dialog = jQuery('div#buyDialog').dialog({
        	draggable: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            width: 'auto',
            height: 'auto'
        });
        
        jQuery('div#buyDialog div#stepCouponVisitor').keypress(function (e) {
            
        	if (e.which == 13){
            	// swallow for now
            	// TODO: handle stepping through forms on enter
            	return false;
            }else{
            	return true;
            }
        });
        
        jQuery('#creatorButton').click(function(){
        	var href = 'creator';
        	that.accountInfo = that.joinProxy.plan(href);
        	that.updatePlanReferences(that.item);
        	that.displayStep('stepTwoVisitor');
        });
        
        jQuery('#proButton').click(function(){
        	var href = 'pro';
        	that.accountInfo = that.joinProxy.plan(href);
        	that.updatePlanReferences(that.item);
        	that.displayStep('stepTwoVisitor');
        });
        
        //override draggable settings and assign handle to .blockTop
        jQuery('div#buyDialog').data('dialog').uiDialog.draggable('option', {
            cancel: '.ui-dialog-titlebar-close',
            handle: '.ui-dialog-titlebar, div#buyDialog .blockTop'
        });
        
        //Log.debug('BuyDialog.processFragment 1 - className:'+document.getElementById('buyDialog').className);
        jQuery('div#buyDialog a.close').click(function (event) {
            that.close();
            return false;
        });        
        jQuery('div#buyDialog div#stepOneVisitor a').click(function (event) {
            //event.preventDefault();
            if (this.hash.indexOf('#/detail?') == 0) {
                that.close();
                return true;
            }
            switch (this.hash.substring(1))
            {
            case 'collector':
            case 'creator':
            case 'pro':
            	that.accountInfo = that.joinProxy.plan(this.hash.substring(1));
            	that.updatePlanReferences(that.item);
            	that.displayStep('stepTwoVisitor');
            	break;
            case 'signIn':
                if (jQuery('div#buyDialog div#stepOneVisitor form').valid())
                {
                	that.existingMemberLogin = true;
                    var o = Form.toObject(jQuery('div#buyDialog div#stepOneVisitor form'));
                    that.fireEvent(BuyDialog.MEMBER_SIGN_IN, o);
                }
                break;
            default:
                break;
            }
            return false;
        });
        jQuery('div#buyDialog div#stepTwoVisitor a').click(function (event) {
            if (this.hash.indexOf('#/detail?') == 0) {
                that.close();
                return true;
            }
            
            switch (this.hash.substring(1)) {
                case '/legal?terms-of-service':
                    return;
                case 'close':
                    that.close();
                    return false;
                case 'back':
                	that.displayStep('stepOneVisitor');
                default:
            }
            that.step2ValidationAttempt = true;
            if (jQuery('div#buyDialog div#stepTwoVisitor form').valid())
            {
                that.addValues(jQuery('div#stepTwoVisitor form').serializeArray());
                that.values["accountType"] = that.accountInfo.type;
                that.values['firstLastName'] = that.values['firstName'] + ' ' + that.values['lastName'];
                that.values['displayName'] = that.values['username'];
            	// if product price is zero and plan price is zero
                // skip the credit card forms -
                // if product price > zero and plan price is zero
                // skip coupon form and show credit card form
            	if(that.item.price == 0 && that.accountInfo.price == 0)
        		{
            		jQuery('.billingInfo').hide();
                	that.refreshReviewValues('div#stepFourVisitor .review', that.values);
                	that.displayStep('stepFourVisitor');
        		}else if(that.accountInfo.price != 0){
        			
        			that.displayStep('stepCouponVisitor');
        		}else{
        			that.displayStep('stepThreeVisitor');
        		}
            }
            return false;
        });
        
        
        jQuery('div#buyDialog div#stepCouponVisitor a').click(function (event) {
            switch (this.hash.substring(1)) {
                case 'back':
                    that.displayStep('stepTwoVisitor');
                    break;
                case 'close':
                    that.close();
                    break;
                case 'skip':
                	that.values['couponCode'] = undefined;
            		that.displayStep('stepThreeVisitor');
                	break;
                default:
	            if (jQuery('div#buyDialog div#stepCouponVisitor form').valid())
	            {
	                that.addValues(jQuery('div#stepCouponVisitor form').serializeArray());
	                // validate couponCode
	                var code = jQuery('div#buyDialog div#stepCouponVisitor form #couponCode').val();
	                that.fireEvent(BuyDialog.VALIDATE_COUPON, code);
	                that.showUpdateAnimation(this);
	            }
            }
            return false;
        });
        
        jQuery('div#buyDialog div#stepTwoVisitor .uniqueUsername').keyup(function(event){
            var username = event.target.value;
            if (that.usernameCache[username] == undefined)
            {
                that.fireEvent(BuyDialog.USERNAME_UNIQUE, event.target.value);
            }
        });
        jQuery.validator.addMethod("uniqueUsername", function(value, element){
            return that.usernameCache[value] == true || that.usernameCache[value] == undefined;
        }, function(value, element){
            return 'The username "' + element.value + '" is already in use';
        });
        jQuery('div#buyDialog div#stepTwoVisitor .uniqueEmail').keyup(function(event){
            var email = event.target.value;
            if (that.emailCache[email] == undefined)
            {
                that.fireEvent(BuyDialog.EMAIL_UNIQUE, event.target.value);
            }
        });
        jQuery.validator.addMethod("uniqueEmail", function(value, element){
            return that.emailCache[value] == true || that.emailCache[value] == undefined;
        }, function(value, element){
            return 'The email "' + element.value + '" is already in use';
        });
        jQuery('div#buyDialog div#stepThreeVisitor a').click(function (event) {
            switch (this.hash.substring(1)) {
                case 'back':
                	that.displayStep('stepTwoVisitor');
                    break;
                case 'close':
                    that.close();
                    break;
                default:
	            if (jQuery('div#buyDialog div#stepThreeVisitor form').valid())
	            {
	                that.addValues(jQuery('div#stepThreeVisitor form').serializeArray());
	                that.values['expirationDate'] = that.values['expirationMonth']+'/'+that.values['expirationYear'];
	                that.values['billingCityStateProvinceZipPostalCode'] = that.values['billingCity']+', '+that.values['billingStateProvince']+' '+that.values['billingZipPostalCode'];
	                
	                if (that.values['creditCardNumber'] == undefined) {
	                    that.values['creditCardNumberMasked'] = '<unknown>';
	                } else {
	                	that.values['creditCardNumberUnmasked'] = that.values['creditCardNumber'];
	                	that.values['creditCardNumberMasked'] = '';
	                	for (var i = 0; i < that.values['creditCardNumber'].length - 4; i++) {
	                		that.values['creditCardNumberMasked'] = that.values['creditCardNumberMasked'] + 'x';
	                	}
	                    that.values['creditCardNumberMasked'] = that.values['creditCardNumberMasked'] + that.values['creditCardNumber'].substring(that.values['creditCardNumber'].length - 4);
	                }
	                
	                that.ccNumberIsMasked = true;
	                
	                if(that.item.price != 0 || (that.accountInfo.price !=0 && that.values['couponCode'] == undefined))
	        		{
	            		jQuery('.billingInfo').show();
	        		}
	                jQuery('div#stepFourVisitor .memberInfo').toggle(!Credentials.authenticated());
	                jQuery('div#stepFourVisitor .coupon').toggle(Credentials.accountTypeTag() == 'creator' && that.values['couponCode'] != undefined);
	                jQuery('#billingAddress2row').toggle(that.values['billingAddress2'] != undefined && that.values['billingAddress2'] != '');
	                
	                that.refreshReviewValues('div#stepFourVisitor .review', that.values);
	                jQuery('div#stepFourVisitor .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberMasked']);
	                that.displayStep('stepFourVisitor');
	            }
            }
            return false;
        });
        
        jQuery('div#stepFourVisitor .billingInfo .cc #toggleCCMasking').click(function (event) {
        	if (that.ccNumberIsMasked) {
        		jQuery('div#stepFourVisitor .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberUnmasked']);
        		jQuery('div#stepFourVisitor .billingInfo .cc span#toggleCCMaskingLabel').text('Hide');
        	} else {
        		jQuery('div#stepFourVisitor .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberMasked']);
        		jQuery('div#stepFourVisitor .billingInfo .cc span#toggleCCMaskingLabel').text('Show');
        	}
        	
        	that.ccNumberIsMasked = !that.ccNumberIsMasked;
        	this.checked = false;
        });
        
        jQuery('div#buyDialog div#stepFourVisitor a').click(function (event) {
            switch (this.hash.substring(1)) {
	            case 'close':
	                that.close();
	                break;
	            case 'back':
                    that.displayStep('stepThreeVisitor');
                    break;
                case 'editMember':
                	that.displayStep('stepTwoVisitor');
                    break;
                case 'editBilling':
                	that.displayStep('stepThreeVisitor');
                    break;
                case 'editCoupon':
                	that.displayStep('stepCouponVisitor');
                    break;
                default:
                	
                	
                	if(!Credentials.authenticated())
                	{
                		that.fireEvent(BuyDialog.JOIN, that.values);
                	}else{
                		that.fireEvent(BuyDialog.UPDATE, {formName:'paymentMethod',values:that.values});
                	}
                
                	//Make sure we're submitting the unmasked cc number
            		jQuery('div#stepFourVisitor .billingInfo .cc #creditCardNumber').hide();
            		jQuery('div#stepFourVisitor .billingInfo .cc #creditCardNumber').text(that.values['creditCardNumberUnmasked']);
                
                	that.showUpdateAnimation(this);
                
                	break;
            }
            return false;
        });
        jQuery('div#buyDialog div#stepOneMember a').click(function (event) {
            switch (this.hash.substring(1)) {
                case 'buy':
                	that.fireEvent(BuyDialog.MEMBER_PURCHASE_ITEM, {itemId: that.item.id});
                    //that.close(); // cant close this, need to listen for the success
                    that.muteZappGadgetBuy(false);
                    that.showUpdateAnimation(this);
                    break;
                case 'cancel':
                	that.close();
                	break;
                default:
                	that.close();
                	return true;
            }
            return false; 
        });
        
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
        //Log.debug('BuyDialog.processFragment - @bottom');
    }

    this.muteZappGadgetBuy = function(enabled)
    {
        if(enabled){
            jQuery('#confirmPurchase', this.element).show();
        }else{
            jQuery('#confirmPurchase', this.element).hide();
        }
    }

    this.onLoginSuccess = function()
    {
    	if(this.existingMemberLogin){
    		this.existingMemberLogin = false;
 
    		if(Credentials.accountTypeTag() == 'collector' && this.item._type == 'gadget'){
        		this.fireEvent(BuyDialog.LOAD_UPGRADE, this.item);
        		this.close();
        	}else{
        		this.fireEvent(BuyDialog.GET_ITEM_DETAILS, this.item);
	    		jQuery('div#buyDialog .freshJoinOnly').hide();

	        	this.refreshReviewValues('div#stepOneMember .review', this.values);
	    		
	    		this.displayStep('stepOneMember');
        	}
    	}
    }
    
    this.setItemAfterLogin = function(payload) {
    	if(payload.detail.owned){
			jQuery('div#stepOneMember .confirmPurchase').hide();
    		jQuery('div#stepOneMember .memberItemOwned').show();
		}else{
			jQuery('div#stepOneMember .confirmPurchase').show();
    		jQuery('div#stepOneMember .memberItemOwned').hide();
		}
    	
    	if (this.item != undefined && this.item.owned != undefined) {
    		this.item.owned = payload.detail.owned;	
    		
    		switch(this.item._type)
            {
            case 'zapp':
            	jQuery('span#formattedType').text('app');
            	break;
            case 'gadget':
            	jQuery('span#formattedType').text('gadget');
            	break;
            }
    	}
    }
    
    this.onSubmitSuccess = function(payload)
    {
    	if(this.item){ // SBJ: remove this if after joinProxy is changed over to targeted notifications
    		this.hideUpdateAnimation();
        	
        	jQuery("div#buyDialog  .creditCardDenied").toggle(!payload.validCreditCard && this.values["creditCardNumber"] != undefined);
        	
        	if(payload.validCreditCard || this.item.price == 0){
        		this.refreshReviewValues('div#stepOneMember .review', this.values);
        		if(payload.couponSubmitted)
        		{
            		jQuery("div#buyDialog .couponAccepted").toggle(payload.couponApplied);
            		jQuery("div#buyDialog .couponDenied").toggle(!payload.couponApplied);
            		if(payload.couponApplied){
        	    		var termDueDate = Common.parseDate(payload.termDueDate.split(' ')[0]);
        		        jQuery('div#buyDialog #currentDatePlus').text(termDueDate.format('mmmm dS, yyyy'));
            		}
        		}else{
        			jQuery("div#buyDialog .couponAccepted").hide();
            		jQuery("div#buyDialog .couponDenied").hide();
        		}
        		this.displayStep('stepOneMember');
        	}else{
        		this.displayStep('updateBillingBadCC');
        	}
    	}
    	
    }
    this.onSubmitFailure = function(payload)
    {
    	this.hideUpdateAnimation();
    	alert(payload.error + ":\n" + payload.detail);
    }
    
    this.onCouponValidateSuccess = function(payload)
    {
    	this.hideUpdateAnimation();
    	if(payload.couponValid){
    		jQuery('div#stepFourVisitor .billingInfo').hide();
    		this.refreshReviewValues('div#stepFourVisitor .review', this.values);
    		
    		if(this.item.price != 0)
			{
    			this.displayStep('stepThreeVisitor');
			}else{
				if(this.accountInfo.price != 0){
					jQuery('.coupon').show();
				}
				this.displayStep('stepFourVisitor');
			}
    	}else{
    		jQuery('#stepCouponVisitor .couponDenied').show();
    	}
    }
    this.onCouponValidateFailure = function(payload)
    {
    	this.hideUpdateAnimation();
    	alert(payload.errorDetail);
    }
    
    this.showUpdateAnimation = function(target)
    {
    	this.animStart = (new Date()).valueOf();
        jQuery(target).after('<img class="updatingAnimation" src="com/alleni/images/updating.gif"/>');
    }
    
    this.hideUpdateAnimation = function()
    {
        var remainder = 2000 - ((new Date()).valueOf() - this.animStart);

        if (remainder < 0) {
            jQuery('.updatingAnimation').remove();
        }
        else {
            setTimeout(function(){
                jQuery('.updatingAnimation').remove();
            },
            2000);
        }

    }
    
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(BuyDialog.INITIALIZED);
        }
    }
    
    //TODO: Excise this.
    this.setJoinProxy = function(joinProxy)
    {
        this.joinProxy = joinProxy;
    }

    this.usernameUnique = function(results)
    {
        this.usernameCache[results.username] = results.unique;
        if (this.step2ValidationAttempt)
        {
            jQuery('div#buyDialog div#stepTwoVisitor form').valid();
        }
    }
    this.emailUnique = function(results)
    {
        this.emailCache[results.email] = results.unique;
        if (this.step2ValidationAttempt)
        {
            jQuery('div#buyDialog div#stepTwoVisitor form').valid();
        }
    }
    this.refreshReviewValues = function (selector, values)
    {
    	var displayValues = values;
    	
        jQuery(selector).each(function (index, element) {
            jQuery(this).text(displayValues[this.id]);
        })
    }
    this.displayStep = function (step)
    {
        //Log.debug('BuyDialog.displayStep - step:'+step);
        jQuery('div#buyDialog .step').hide();
        jQuery('div#buyDialog #'+step).show();
    }
    this.clearForm = function ()
    {
    	jQuery(':input', this.element).val('');
    }
   
    this.configure = function (plan)
    {
        //Log.debug('JoinPanel.configure');
        

        
    }
    
    this.updatePlanReferences = function (o){
    	
    	 if(this.accountInfo.type == "collector"){
         	var creatorPlan = this.joinProxy.plan('creator');
         	jQuery('div#buyDialog span.accountTypeY').text(creatorPlan.name);
             jQuery('div#buyDialog span.accountCostY').text(creatorPlan.formattedPrice);
             jQuery('div#buyDialog .priceY').each(function(index, value){
                 var jValue = jQuery(value);
                 var html = jValue.html();
                 var spanIndex = html.toLowerCase().indexOf('<span');
                 html = "Open "+creatorPlan.name+" Account" + html.substring(spanIndex);
                 jValue.html(html);
             });
             jQuery('.optionalPlan').show();
         }else{
         	jQuery('.optionalPlan').hide();
         }
    	 
         jQuery('#stepCouponVisitor #couponCode').val(''); // clear out couponCode from previous open

         var fullAccountName = '';
         switch(this.accountInfo.name.toLowerCase()){
         case 'pro':
        	 fullAccountName = 'Professional';
        	 break;
         default:
        	 fullAccountName = this.accountInfo.name.charAt(0).toUpperCase() + this.accountInfo.name.slice(1);
        	 break;
         }
         
         var formattedPrice = "Open "+ fullAccountName +" Account";
         var description = '"'+ fullAccountName +'" Account - '+this.accountInfo.formattedPrice;
         
         jQuery('div#buyDialog span.accountType').text(fullAccountName);
         jQuery('div#buyDialog span.accountCost').text(this.accountInfo.formattedPrice);
         jQuery('h2.planDescription', this.element).text(description).toggleClass('freeTrial', this.accountInfo.price != 0);
         jQuery('h3.planSubtitle', this.element).toggle(this.accountInfo.price != 0);
         
         jQuery('div#buyDialog .priceX').each(function(index, value){
             var jValue = jQuery(value);
             var html = jValue.html();
             var spanIndex = html.toLowerCase().indexOf('<span');
             
             if (html.indexOf('Apply Coupon') == -1) {
                 html = formattedPrice + html.substring(spanIndex);
             }
             jValue.html(html);
         });

         jQuery('div#buyDialog .paid').toggle(this.accountInfo.price != 0);
         jQuery('div#buyDialog .free').toggle(this.accountInfo.price == 0);
         
         jQuery('div#buyDialog .collectorToggle').toggle(this.accountInfo.type == 'collector');
         jQuery('div#buyDialog .creatorToggle').toggle(this.accountInfo.type == 'creator' || this.accountInfo.type == 'pro' );
         
         var now = new Date();
         jQuery('#currentDate', this.element).text(now.format('mmmm dS, yyyy'));
    }
    
    this.open = function (o)
    {
        //Log.debug('BuyDialog.open - @top');
        if (!Require(o, ['id', 'name', 'price', 'formattedPrice', 'icon'], 'BuyDialog.open'))
            return;


        this.accountInfo = this.joinProxy.plan(TypeLookup.accountRequired(o._type));
        
       
        this.updatePlanReferences(0);
        
//        var owned = Credentials.owned(o.id);
        var owned = o.owned;
    	

        this.item = o;
        this.usernameCache = [];
        this.emailCache = [];
       
        ChangeTag.toText(
            jQuery('div#buyDialog div#stepOneVisitor div.columnRight [name="password"]'),
            jQuery('div#buyDialog div#stepOneVisitor div.columnRight a.button')
        )

        jQuery('div#buyDialog span.type').text(TypeLookup.singular(o._type));

        // If the user is collecting a gadget, hide the Collector option. Also, if the app being collected is not free, hide the Collector option
        // If the user is already logged in, this piece does not matter.        
        jQuery('div#buyDialog table#collectorOptionTable').toggle(o._type != 'gadget' && o.price == 0);
        
        //The requiredAccountType span contains the n in "an". if the app is not free, then update the text of the span and add a new line to keep the text spaced nicely
        if (o.price > 0) {
        	jQuery('div#buyDialog span.requiredAccountType').text(' Creator or Professional');
        	jQuery('div#buyDialog span.requiredAccountBR').html("<br/>");
        }
        
        jQuery('div#buyDialog #stepOneVisitor .warningColor').hide();
        jQuery('div#buyDialog #stepOneVisitor [name="email"]').val('Email');
        jQuery('div#buyDialog .name').each(function (element, index) {
            jQuery(this).text(ZappGadgetProject.name(o.publishedName));
        });
        jQuery('div#buyDialog .description').each(function (element, index) {
            jQuery(this).text(ZappGadgetProject.description(o.description));
        });
        jQuery('div#buyDialog .icon img').each(function (element, index) {
            jQuery(this).attr('src', o.icon);
        });
        jQuery('div#buyDialog .details').each(function(element, index){
            jQuery(this).attr('href', IdType.detailUrl(o.urlName, IdType.URL_NAME));
        });

        jQuery('div#buyDialog .price').each(function (element, index) {
            // This is indeed 'o.formattedPrice' and not 'formattedPrice' because
            // user already knows there's a monthly fee
            jQuery(this).text(o.formattedPrice);
        });

        jQuery('div#buyDialog .freshJoinOnly').toggle(!Credentials.authenticated());
        
        switch(o._type)
        {
        case 'zapp':
        	jQuery('span#formattedType').text('app');
        	break;
        case 'gadget':
        	jQuery('span#formattedType').text('gadget');
        	break;
        }
        
        if(Credentials.authenticated())
        {
        	jQuery('#returnToOpenAccount').hide();
        	if(owned){
        		jQuery('div#stepOneMember .confirmPurchase').hide();
	    		jQuery('div#stepOneMember .memberItemOwned').show();
    		}else{
    			jQuery('div#stepOneMember .confirmPurchase').show();
	    		jQuery('div#stepOneMember .memberItemOwned').hide();
    		}
            switch(o._type)
	        {
		        case 'zapp':
		        	switch(this.accountInfo.type)
		        	{
		        	case 'collector':
		        	case 'creator':
		        		
		        			if(o.price == 0)
		        			{
		        				this.displayStep('stepOneMember');
		        			}else if(!Credentials.hasCreditCardProfile())
	        				{
		        				this.displayStep('stepThreeVisitor');
			        			//this.displayStep('updateBillingNoCC');
	        				}else if(Credentials.hasCreditCardProfile() && !Credentials.goodStanding()){
	        					this.displayStep('updateBillingBadCC');
	        				}else if(Credentials.hasCreditCardProfile() && Credentials.goodStanding()){
	        					this.displayStep('stepOneMember');
	        				}
		        		break;
		        	}
		        	
		        	
		        	break;
		        case 'gadget':
		        	
		        	switch(this.accountInfo.type)
		        	{
		        	case 'collector':
		        		// TODO: show upgrade form
		        		break;
		        	case 'creator':
		        		if(o.price == 0)
	        			{
	        				this.displayStep('stepOneMember');
	        			}
		        		else if(!Credentials.hasCreditCardProfile())
        				{
	        				this.displayStep('stepThreeVisitor');
		        			//this.displayStep('updateBillingNoCC');
        				}
		        		else if(Credentials.hasCreditCardProfile() && !Credentials.goodStanding()){
        					this.displayStep('updateBillingBadCC');
        				}
		        		else if(Credentials.hasCreditCardProfile() && Credentials.goodStanding()){
        					this.displayStep('stepOneMember');
        				}
		        		break;
		        	}
		        	break;
	
	        }
        }else{
        	this.displayStep('stepOneVisitor');
        }

        this.step2ValidationAttempt = false;
        
        this.dialog.dialog('open');
        
        this.clearForm();
        
        
        
        //Log.debug('BuyDialog.open - @bottom');
    }
    
    this.onLoginBadCredentials = function()
    {
        jQuery('div#buyDialog #stepOneVisitor .warningColor').show();
    }
    this.close = function ()
    {
        this.dialog.dialog('close');
    }
    this.addValues = function (valueArray)
    {
        for (var i = 0; i < valueArray.length; i++)
            this.values[valueArray[i].name] = valueArray[i].value;
    }
}
BuyDialog = new Class(new BuyDialog());
BuyDialog.NAME = 'BuyDialog';
BuyDialog.INITIALIZED = 'BuyDialog.INITIALIZED';
BuyDialog.MEMBER_SIGN_IN = 'BuyDialog.MEMBER_SIGN_IN';
BuyDialog.SUBMIT = "BuyDialog.SUBMIT";
BuyDialog.VALIDATE_COUPON = "BuyDialog.VALIDATE_COUPON";
BuyDialog.MEMBER_PURCHASE_ITEM = 'BuyDialog.MEMBER_PURCHASE_ITEM';
BuyDialog.VISITOR_PURCHASE_ITEM = 'BuyDialog.VISITOR_PURCHASE_ITEM';
BuyDialog.EMAIL_UNIQUE = 'BuyDialog.EMAIL_UNIQUE';
BuyDialog.USERNAME_UNIQUE = 'BuyDialog.USERNAME_UNIQUE';
BuyDialog.JOIN = 'BuyDialog.JOIN';
BuyDialog.UPDATE = 'BuyDialog.UPDATE';
BuyDialog.LOAD_UPGRADE = 'BuyDialog.LOAD_UPGRADE';
BuyDialog.PLAN_SELECTED = 'BuyDialog.PLAN_SELECTED';
BuyDialog.GET_ITEM_DETAILS = 'BuyDialog.GET_ITEM_DETAILS';
// ScriptLoader.loaded(BuyDialog.NAME);
