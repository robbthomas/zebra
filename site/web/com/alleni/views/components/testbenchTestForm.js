var TestbenchTestForm = function()
{
    this.Extends = UIComponent;
    this.selectedTest = undefined;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function (parent)
    {
        this.parent(parent);
        var that = this;
        var o = {
            filename: 'testbenchTestForm.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.onSubmit = this.onSubmit.bindWithEvent(this);
        jQuery('#form').submit(this.onSubmit);
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(TestbenchTestForm.INITIALIZED);
        }
    }
    this.onSubmit = function (event)
    {
        event.preventDefault(); // prevent standard browser submit and page nagivation...
        var o = Form.toObject(jQuery('#testForm form'));
        this.fireEvent(TestbenchTestForm.SUBMIT, {test: this.selectedTest, form: o, ignoreValidation: this.ignoreValidation()});
    }
    this.saveForm = function ()
    {
        if (!this.selectedTest)
            return;
        this.selectedTest.clone = jQuery('#form ol').clone();
    }
    this.clearForm = function ()
    {
        jQuery('#form li').remove();
    }
    this.displayForm = function (test, skipSave)
    {
        if (!skipSave)
            this.saveForm();
        jQuery('#form legend').text(test.group+' '+test.name);
        if (test.clone)
        {
            jQuery('#form ol').replaceWith(test.clone);
            if (test.displayForm)
                test.displayForm(test.id);
            jQuery('div#testForm :input').each(function (index, element) {
                var x = 0;
            })
        }
        else
        {
            this.clearForm();
            File.requestTest({filename: test.html, success: function (html, status) {
                jQuery('#form ol').html(html);
                if (TestbenchTestForm.displayForm)
                {
                    test.displayForm = TestbenchTestForm.displayForm;
                    test.displayForm(test.id);
                    jQuery('div#testForm :input').each(function (index, element) {
                        var x = 0;
                    })
                    TestbenchTestForm.displayForm = undefined;
                }
                if (TestbenchTestForm.displayResults)
                {
                    test.displayResults = TestbenchTestForm.displayResults;
                    TestbenchTestForm.displayResults = undefined;
                }
                if (TestbenchTestForm.makeObject)
                {
                    test.makeObject = TestbenchTestForm.makeObject;
                    TestbenchTestForm.makeObject = undefined;
                }
            }});
        }
        this.selectedTest = test;
    }
    this.ignoreValidation = function()
    {
        return jQuery("#ignoreValidation").is(':checked');
    }
}
TestbenchTestForm.displayForm = undefined;
TestbenchTestForm.displayResults = undefined;
TestbenchTestForm.makeObject = undefined;
TestbenchTestForm = new Class(new TestbenchTestForm());
TestbenchTestForm.NAME = 'TestbenchTestForm';
TestbenchTestForm.INITIALIZED = 'TestbenchTestForm.INITIALIZED';
TestbenchTestForm.SUBMIT = 'TestbenchTestForm.SUBMIT';
ScriptLoader.loaded(TestbenchTestForm.NAME);
