var DialogPanel = function()
{
    this.Extends = UIComponent;
    this.dialog = undefined;
    this.viewName = undefined;

    this.initialize = function (viewName)
    {
    	var _this = this;
    	this.viewName = (viewName != undefined)?viewName:DialogPanel.NAME;
    	
    	var fragmentHTML = 
    		'<div class="overlayContent dialogPanel">'+
    		'<div class="block">'+
    		'<div class="blockTop">'+
    		'<a class="close" href="#close" style="display:none;"></a>'+
    		'<h2 id="dialogTitle"></h2>'+
    		'</div>'+
    		'<div id="dialogShell">'+
    		'</div>'+
    		'</div>'+
    		'</div>';
    	jQuery('body').append(jQuery(fragmentHTML).attr('id', this.viewName).addClass('dialog'));

       // ApplicationFacade.getInstance().registerMediator(new DialogPanelMediator(this.dialog));
    	this.dialog = jQuery('div#'+this.viewName).dialog({
        	draggable: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            width: 'auto',
            height: 'auto'
        });
    	this.dialog = jQuery('div#'+this.viewName).dialog({
        	beforeClose: function(event, ui){
        		_this.close();
        	}
        });
    	// same as calling super
    	this.parent(this.viewName);
    	// override drag settings and assign handle to .blockTop
        jQuery('div#'+this.viewName).data('dialog').uiDialog.draggable('option', {
            cancel: '.ui-dialog-titlebar-close',
            handle: '.ui-dialog-titlebar, div#'+this.viewName+' .blockTop'
        });
        // handle close
        jQuery('div#'+this.viewName+' a.close').click(function (event) {
            _this.close();
            return false;
        });
        
    }
    
    this.initializeChildren = function ()
    {
    	
        //this.addChild(this.dialog);
    }
    
    this.initializationComplete = function ()
    {

    }
    
    this.open = function ()
    {
    	// show the dialog
    	this.dialog.dialog('open');
    }
    
    this.recenter = function ()
    {
    	this.dialog.dialog('option', 'position', 'center');
    }
    
    this.close = function ()
    {
    	// completely remove the dialog views from DOM
        this.dialog.dialog('destroy').remove();
        // notify mediator that view is being destroyed so it can remove itself from framework
        this.fireEvent(DialogPanel.CLOSE);
    }
    
    this.closeByUser = function ()
    {
    	// completely remove the dialog views from DOM
        this.dialog.dialog('destroy').remove();
        // notify mediator that view is being destroyed so it can remove itself from framework
        this.fireEvent(DialogPanel.CLOSE_BY_USER);
    }
    
    this.showClose = function (yesNo)
    {
    	if(yesNo){
    		jQuery('div#'+this.viewName+' a.close').show();
    	}else{
    		jQuery('div#'+this.viewName+' a.close').hide();
    	}
    }

    /**
     * Sets the inner-view for this dialog instance.
     * 	@param content
     * 			HTML element string.
     */
    this.content = function (content)
    {
    	jQuery('div#'+this.viewName+' div#dialogShell').html(content);
    }
    /**
     * Sets the title bar for this dialog instance.
     * 	@text
     * 			String applied to title bar.
     */
    this.title = function (text)
    {
    	jQuery('div#'+this.viewName+' h2#dialogTitle').html(text);
    }
    
}
DialogPanel = new Class(new DialogPanel());
DialogPanel.NAME = 'DialogPanel';
DialogPanel.INITIALIZED = 'DialogPanel.INITIALIZED';
DialogPanel.CLOSE = "DialogPanel.CLOSE";
DialogPanel.CLOSE_BY_USER = "DialogPanel.CLOSE_BY_USER";