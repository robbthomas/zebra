var ZappShoppePanel = function()
{
    this.Extends = UIComponent;
    this.featuredPanel = undefined;
    this.browsePanel = undefined;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function (parent)
    {
        this.parent(parent);
        
        var that = this;
        var o = {
            filename: 'zappShoppePanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.childInitialized = this.childInitialized.bindWithEvent(this);

        this.featuredPanel = new FeaturedPanel('featuredPanel');
        this.featuredPanel.addEvent(FeaturedPanel.INITIALIZED, this.childInitialized);

        this.browsePanel = new BrowsePanel('browsePanel');
        this.browsePanel.addEvent(BrowsePanel.INITIALIZED, this.childInitialized);

        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializeChildren = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.addChild(this.featuredPanel);
            this.addChild(this.browsePanel);
            this.addChild(new UIComponent(new Element('div', {'class': 'clearer'})));
        }
    }
    this.childrenInitialized = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.allChildrenInitialized())
        {
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(ZappShoppePanel.INITIALIZED);
        }
    }
    this.childInitialized = function ()
    {
        this.initializationComplete();
    }
    this.allChildrenInitialized = function ()
    {
        return (this.featuredPanel.initialized && this.browsePanel.initialized);
    };
}
ZappShoppePanel = new Class(new ZappShoppePanel());
ZappShoppePanel.NAME = 'ZappShoppePanel';
ZappShoppePanel.INITIALIZED = 'ZappShoppePanel.INITIALIZED';
ZappShoppePanel.FEATURED_NEXT = 'ZappShoppePanel.FEATURED_NEXT'; 
ZappShoppePanel.FEATURED_PREVIOUS = 'ZappShoppePanel.FEATURED_PREVIOUS';
ScriptLoader.loaded(ZappShoppePanel.NAME);
