var EnlargeImagePanel = function()
{
    this.Extends = UIComponent;
    this.dialog = undefined;
    this.viewName = undefined;
    this.imgUrl = undefined;

    this.initialize = function (viewName)
    {
    	this.viewName = (viewName != undefined)?viewName:EnlargeImagePanel.NAME;
    	// same as calling super
    	this.parent(this.viewName);
    }
    
    this.setPic = function (url, index)
    {
    	var _this = this;
    	this.imgUrl = url;
    	this.index = index;
    	jQuery("img#enlargeImage").attr('width', 100);
        jQuery("img#enlargeImage").attr('height', 100);
    	jQuery("img#enlargeImage").hide();
    	jQuery('.updatingAnimation').show();
    	jQuery('.updatingAnimationContainer').show();
    	jQuery("img#enlargeImage").load(_this,this.imgTesting_onload);
    	jQuery("img#enlargeImage").attr('src', url);
    	
    	jQuery('div#navLinks div a', this.element).unbind('click').click(function(event) {
    		var payload = {};
        	var eventToFire = "";	
        	var target = this.hash.substring(1).split('?');
            var targetHash = target[0];
            var targetHashArgs = (target.length > 1)?target[1]:undefined;

            switch(targetHash) {
            case 'previous':
            	eventToFire = EnlargeImagePanel.PREVIOUS;
            	break;
            case 'next':
            	eventToFire = EnlargeImagePanel.NEXT;
            	break;
            }
            
            if (eventToFire != '') {
            	_this.fireEvent(eventToFire, payload);
            }

            return false;
    	});
    }

    this.imgTesting_onload = function(e)
    {
        jQuery("img#enlargeImage").attr('width', jQuery("img#enlargeImage")[0].naturalWidth);
        jQuery("img#enlargeImage").attr('height', jQuery("img#enlargeImage")[0].naturalHeight);
        jQuery('.updatingAnimation').hide();
        jQuery('.updatingAnimationContainer').hide();
        jQuery("img#enlargeImage").fadeIn(400);
        e.data.fireEvent(EnlargeImagePanel.LOAD_COMPLETE);
    }

    this.setLinkVisibility = function(prevVisible, nextVisible) {
    	jQuery('div#navLinks div.previous', this.element).toggle(prevVisible);
    	jQuery('div#navLinks div.next', this.element).toggle(nextVisible);
    }
    
}
EnlargeImagePanel = new Class(new EnlargeImagePanel());
EnlargeImagePanel.NAME = 'EnlargeImagePanel';
EnlargeImagePanel.LOAD_COMPLETE = 'EnlargeImagePanel.LOAD_COMPLETE';
EnlargeImagePanel.PREVIOUS = 'EnlargeImagePanel.PREVIOUS';
EnlargeImagePanel.NEXT = 'EnlargeImagePanel.NEXT';