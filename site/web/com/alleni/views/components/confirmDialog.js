var ConfirmDialog = function()
{
    this.Extends = UIComponent;
    this.dialog = undefined;
    this.viewName = undefined;

    this.initialize = function (viewName)
    {
    	this.viewName = (viewName != undefined)?viewName:ConfirmDialog.NAME;
    	// same as calling super
    	this.parent(this.viewName);
    }
    
    this.config = function (message)
    {
    	var _this = this;
    	jQuery('span#confirmMessage', this.element).html(message);
    	jQuery('a', this.element).click(function (event) {

            switch (this.hash.substring(1))
            {
            case 'yes':
            	_this.fireEvent(ConfirmDialog.YES);
                break;
            case 'no':
            	_this.fireEvent(ConfirmDialog.NO);
            	break;
            default:
                break;
            }
            return false;
        });
    	
    }
    
}
ConfirmDialog = new Class(new ConfirmDialog());
ConfirmDialog.NAME = 'ConfirmDialog';
ConfirmDialog.YES = "ConfirmDialog.YES";
ConfirmDialog.NO = "ConfirmDialog.NO";