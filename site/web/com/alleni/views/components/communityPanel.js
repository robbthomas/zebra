var CommunityPanel = function()
{
    this.Extends = UIComponent;
    this.initialized = false;
    this.fragmentProcessed = false;
    this.invokePostFragmentInitialization = false;    
    this.initialize = function ()
    {
       //Log.debug('CommunityPanel.initialize');
        this.parent('communityPanel');
        
        var that = this;
        var o = {
            filename: 'communityPanel.html',
            success: function (fragment, status) {
                that.processFragment(fragment);
            }
        }
        File.requestFragment(o);
    }
    this.processFragment = function (fragment)
    {
        this.set('html', fragment);
        this.fragmentProcessed = true;
        if (this.invokePostFragmentInitialization)
        {
            this.initializeChildren();
            this.childrenInitialized();
            this.initializationComplete();
            this.invokePostFragmentInitialization = false;
        }
    }
    this.initializationComplete = function ()
    {
        if (!this.fragmentProcessed)
        {
            this.invokePostFragmentInitialization = true;
            return;
        }
        if (!this.initialized)
        {
            this.initialized = true;
            this.fireEvent(CommunityPanel.INITIALIZED);
        }
    }
}
CommunityPanel = new Class(new CommunityPanel());
CommunityPanel.NAME = 'CommunityPanel';
CommunityPanel.INITIALIZED = 'CommunityPanel.INITIALIZED';
ScriptLoader.loaded(CommunityPanel.NAME);
