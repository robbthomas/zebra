var StartTransfer = function()
{
	this.Extends = UIComponent;

	this._config = undefined;
	this.accountOwnerConfirmed = false;
	this.confirmedUser = undefined;
	this.defaultSearchText = 'Search...';
	this.defaultEmailText = "Enter email of account owner";
	this.initialize = function (viewName)
	{
		// same as calling super
		this.parent(viewName);




		var that = this;
		var o = {
				filename: 'accountFormStartTransfer.html',
				success: function (fragment, status) {
					//Log.debug('AccountPanel - success');
					that.processFragment(fragment);
				}
		}
		File.requestFragment(o);



	}

	this.processFragment = function (fragment) {
		this.set('html', fragment);
		this.initializeChildren();
		this.childrenInitialized();
		this.initializationComplete();
	}

	this.timerActive = false;
	this.timeout = 1000; // in miliseconds
	this.initializationComplete = function ()
	{
		var that = this;
		jQuery('#projectTransferSearch', this.element).keyup(function(){
			if (!that.timerActive) {
				that.timerActive = true;
				setTimeout(
						function(){
							that.runProjectSearchQuery();
							that.timerActive = false;
						}, that.timeout);
			}
		});

		jQuery('#projectTransferSearch', this.element).unbind('click').unbind('blur')
			.click(function() {
				if (this.value == that.defaultSearchText) {
					this.value = '';
				}
			}).blur(function() {
				if (this.value == ''){
					this.value = that.defaultSearchText;
				}
			});
		
		jQuery('#emailToConfirm', this.element).unbind('click').unbind('blur')
			.click(function() {
				if (this.value == that.defaultEmailText) {
					this.value = '';
				}
			}).blur(function() {
				if (this.value == ''){
					this.value = that.defaultEmailText;
				}
			});
		
		jQuery('#clearSearchButton', this.element).click(function(){
			jQuery('#projectTransferSearch', this.element).attr('value','Search...');
			that.runProjectSearchQuery();
		});

		jQuery('#emailToConfirm', this.element).keyup(function(){
			if (this.value != that.confirmedUser) {
				that.accountOwnerConfirmed = false;
				jQuery('#verifiedEmailSuccessDisplay', this.element).hide();
				jQuery('#verifiedEmailFailureDisplay', this.element).hide();
				jQuery('#confirmedUserDisplayName', that.element).html('');
				jQuery('#confirmedUserCompanyName', that.element).html('');
				
				that.updateTransferButtonState();
				that.setTransferButtonLabel('Send Copies');
			}
		});
		
		jQuery('a.button', this.element).unbind('click').click(function(event){

			var isValid = false;
			var o = Form.toObject(jQuery('form', that.element));
			o.type = Credentials.accountTypeTag();
			switch (this.id)
			{	
			case 'addSelected':
				that.addSelectedProjectsToTransferList();
				that.updateTransferButtonState();
				break;
			case 'removeSelected':
				that.removeSelectedProjectsFromTransferList();
				that.updateTransferButtonState();
				break;
			case 'addAll':
				that.addAllToTransferList();
				that.updateTransferButtonState();
				break;
			case 'removeAll':
				that.removeAllToTransferList();
				that.updateTransferButtonState();
				break;	     
			case 'confirmEmail':
				that.fireEvent(StartTransfer.CONFIRM_USER_BY_EMAIL, jQuery("#emailToConfirm", that.element).val());
				break;
			}
			return false;
		});


		this.updateTransferButtonState();	

		this.fireEvent(StartTransfer.INITIALIZED);

	}

	this.updateTransferButtonState = function()
	{
		var that = this;
		if(this.accountOwnerConfirmed == false || jQuery('#projectsToTransfer option', this.element).length == 0){
			jQuery('a#startTransferButton', this.element).fadeIn(0,.5);
			jQuery('a#startTransferButton', this.element).addClass('disabledButton');
			jQuery('a#startTransferButton', this.element).addClass('button');
			jQuery('a#startTransferButton', this.element).addClass('startTransfer25Button');
			jQuery('a#startTransferButton', this.element).removeClass('startTransferSuccess');
			jQuery('a#startTransferButton', this.element).removeAttr('href');
			jQuery('a#startTransferButton', this.element).unbind('click');
		}else{
			jQuery('a#startTransferButton', this.element).fadeIn(0,1);
			jQuery('a#startTransferButton', this.element).removeClass('disabledButton');
			jQuery('a#startTransferButton', this.element).removeClass('startTransferSuccess');
			jQuery('a#startTransferButton', this.element).addClass('button');
			jQuery('a#startTransferButton', this.element).addClass('startTransfer25Button');
			jQuery('a#startTransferButton', this.element).attr('href', '#startTransfer');
			jQuery('a#startTransferButton', this.element).unbind().click(function(event){
				switch (this.id)
				{
				case 'startTransferButton':
					that.startTransfer();
					break;
				}
				return false;
			});
		}
	}

	this.config = function(config)
	{
		this._config = config;
	}

	/**
	 * 
	 */


	this.startTransfer = function()
	{
		var that = this;
		var receiverEmail = jQuery('#emailToConfirm', this.element).val();
		var projectList = [];
		jQuery.each(jQuery('#projectsToTransfer option', that.element), function(i, project){
			projectList.push({projectId:project.id, receiverEmail:receiverEmail});
		});
		this.fireEvent(StartTransfer.START_BULK_PROJECT_TRANSFER, {projectList:projectList});
		this.turnFeedbackButtonOn();    
	}
	this.confirmEmailSuccess = function(results)
	{
		jQuery('#verifiedEmailFailureDisplay', this.element).hide();
		jQuery('#verifiedEmailSuccessDisplay', this.element).show();
		if (results.nameFormat == 'displayName') {
			jQuery('#confirmedUserDisplayName', this.element).html(results.displayName);
		}
		else {
			jQuery('#confirmedUserDisplayName', this.element).html(results.firstName + ' ' + results.lastName);
		}
		jQuery('#confirmedUserCompanyName', this.element).html(results.addressCompanyName);
		this.accountOwnerConfirmed = true;
		this.confirmedUser = jQuery('#emailToConfirm', this.element).val();
		this.updateTransferButtonState();
		this.setTransferButtonLabel('Send Copies');
	}

	this.confirmEmailFailure = function(results)
	{
		jQuery('#verifiedEmailSuccessDisplay', this.element).hide();
		jQuery('#verifiedEmailFailureDisplay span', this.element).text(results.eligibilityMessage);
		jQuery('#verifiedEmailFailureDisplay', this.element).show();
		this.accountOwnerConfirmed = false;
		this.updateTransferButtonState();
		this.setTransferButtonLabel('Send Copies');
	}
	this.populateIncomingTransferList = function(results)
	{
		var htmlContent = '';
		jQuery.each(results.transferAccounts, function(i, group){
			htmlContent += '<div class="transferGroupGrid"></br>'
				htmlContent += '<span class="transferGroupHeading" style="color: Black;">'+group.displayName+' is sending you...</span>';
			jQuery.each(group.transfers, function(j, project){
				var evenOddItem = (j%2==0?"oddTransferItem":"evenTransferItem");
				var priceInDollars = (project.price/100);
				var smartCost = (project.price > 0?"$"+(priceInDollars<0?"0"+priceInDollars:priceInDollars):"Free");
				htmlContent += '<div class="'+evenOddItem+' incomingGridItem">' +
				'<span style="width: 300px; display: inline-block;">' + project.name + '</span>' +
				'<span style="width: 100px; display: inline-block;">' + smartCost + '</span>' +
				'<a id="acceptTransfer" href="#acceptTransfer" style="float: right;" class="button"><span>Accept</span></a>'+
				'<a id="declineTransfer" href="#declineTransfer" style="float: right;" class="button"><span>Decline</span></a>' + 
				'</div>';
			});
			htmlContent += '</div>';
		});
		jQuery('div.incomingDataContainer', this.element).prepend(htmlContent);
	}
	this.populateOutgoingTransferList = function(results)
	{
		/*
		 * var d = new Date(project.lastUpdated); 
		 * lastUpdate = (d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear();
		 */

		var htmlContent = '';
		jQuery.each(results.transferAccounts, function(i, group){
			htmlContent += '<div class="transferGroupGrid"></br>'
				htmlContent += '<span class="transferGroupHeading" style="color: Black;">'+'To '+group.displayName+'...</span>';
			jQuery.each(group.transfers, function(j, project){
				var evenOddItem = (j%2==0?"oddTransferItem":"evenTransferItem");
				var status = "Sent ";
				var displayDate = new Date(project.createdDateTime);
				if (project.retired > 0) {
					displayDate = new Date(project.retiredDateTime);
					if (project.retiredReasonTypeId === 6) {
						status = "Completed ";
					} else if (project.retiredReasonTypeId === 5) {
						if (project.retiredById === project.receiverMemberId) {
							status = "Declined by Reciever ";
						} else if (project.retiredById === project.senderMemberId) {
							status = "Withdrawn by Sender";
						} else {
							status = "Canceled by System";
						}
					}
				}
				status += (displayDate.getMonth()+1)+'/'+displayDate.getDate()+'/'+displayDate.getFullYear();
				htmlContent += '<div class="'+evenOddItem+' incomingGridItem">' +
				'<span style="width: 300px; display: inline-block;">' + project.name + '</span>' +
				'<span style="width: 250px; display: inline-block;">' + status + '</span>';
				if(project.canceledByUser==true || project.complete==true){
					htmlContent += '<a id="declineTransfer" href="#clearTransfer" style="float: right;" class="button"><span>Clear</span></a>';
				}else{
					htmlContent += '<a id="declineTransfer" href="#cancelTransfer" style="float: right;" class="button"><span>Cancel</span></a>';
				}
				htmlContent += '</div>';
			});
			htmlContent += '</div>';
		});
		jQuery('div.outgoingDataContainer').prepend(htmlContent);
	}

	this.populateProjectTransferList = function(results)
	{
		var projectList = jQuery('#potentialProjects', this.element);
		var optionsHtml = "";
		var that = this;
		jQuery('#potentialProjects').html(optionsHtml);
		jQuery.each(results.items, function(i, project){
			var transferProjectList = jQuery('#projectsToTransfer option', that.element);
			var addProjectToList = true;
			jQuery.each(transferProjectList, function(i, transferProject){
				if(transferProject.id == project.projectId){
					addProjectToList = false;
				}
			});
			if(addProjectToList){
				optionsHtml += "<option id='"+project.projectId+"'>"+"&nbsp;&nbsp;"+project.projectName+"</option>";
			}else{
				optionsHtml += "<option id='"+project.projectId+"'>"+"&#42;&nbsp;"+project.projectName+"</option>";
			}
		});
		this.setTransferButtonLabel('Send Copies');
		jQuery('#potentialProjects', this.element).html(optionsHtml);
	}

	this.addSelectedProjectsToTransferList = function()
	{
		var that = this;
		var projectList = jQuery('#potentialProjects option', this.element);
		var potentialList = "";
		var transferList = "";
		jQuery.each(projectList, function(i, project){
			var projectName = project.label;
			if(projectName == ''){
				projectName = project.value;
			}
			if(project.selected){
				var transferProjectList = jQuery('#projectsToTransfer option', that.element);
				var addToTransferList = true;
				jQuery.each(transferProjectList, function(i, transferProject){
					if(transferProject.id == project.id){
						addToTransferList = false;
					}
				});
				if(addToTransferList){
					transferList += "<option id='"+project.id+"'>"+projectName.substring(2)+"</option>";
				}
				potentialList += "<option id='"+project.id+"'>"+"&#42;&nbsp;"+projectName.substring(2)+"</option>";
			}else{
				potentialList += "<option id='"+project.id+"'>"+projectName+"</option>";
			}
		});
		this.setTransferButtonLabel('Send Copies');
		jQuery('#potentialProjects', this.element).html(potentialList);
		jQuery('#projectsToTransfer', this.element).append(transferList);
	}

	this.removeSelectedProjectsFromTransferList = function()
	{
		var projectList = jQuery('#projectsToTransfer option');
		var transferList = "";
		jQuery.each(projectList, function(i, project){
			var projectName = project.label;
			if(projectName == ''){
				projectName = project.value;
			}
			if(!project.selected){
				transferList += "<option id='"+project.id+"'>"+projectName+"</option>";
			}
		});
		this.setTransferButtonLabel('Send Copies');
		jQuery('#projectsToTransfer', this.element).html(transferList);
		this.runProjectSearchQuery();
	}

	this.addAllToTransferList = function (){
		var that = this;
		var projectList = jQuery('#potentialProjects option', this.element);
		var potentialList = "";
		var transferList = "";
		
		jQuery.each(projectList, function(i, project){
			var transferProjectList = jQuery('#projectsToTransfer option', that.element);
			var addToTransferList = true;
			var projectName = project.label;
			if(projectName == ''){
				projectName = project.value;
			}
			jQuery.each(transferProjectList, function(i, transferProject){
				if(transferProject.id == project.id){
					addToTransferList = false;
				}
			});
			if(addToTransferList){
				transferList += "<option id='"+project.id+"'>"+projectName.substring(2)+"</option>";
			}
			potentialList += "<option id='"+project.id+"'>"+"&#42;&nbsp;"+projectName.substring(2)+"</option>";
		});
		this.setTransferButtonLabel('Send Copies');
		jQuery('#potentialProjects', this.element).html(potentialList);
		jQuery('#projectsToTransfer', this.element).append(transferList);
	}

	this.removeAllToTransferList = function (){
		var transferList = "";
		jQuery('#projectsToTransfer', this.element).html(transferList);
		this.setTransferButtonLabel('Send Copies');
		this.runProjectSearchQuery();
	}

	this.projectTransferListFailure = function(results)
	{
		alert("We failed to get the project transfer list!");
	}

	this.runProjectSearchQuery = function(){
		var queryString = jQuery('#projectTransferSearch', this.element).val();
		if(queryString == 'Search...'){
			queryString = '';
		}
		var searchObject = {
				count: 9999,
				page: 0,
				group: 'member',
				type: 'project',
				refresh: true,
				tiny: true,
				notifications: {
					success: StartTransfer.RETURN_TRANSFER_PROJECT_LIST,
					failure: StartTransfer.RETURN_TRANSFER_PROJECT_LIST_FAILURE
				},
				filters: {
					search: '',
					retired: 'false',
					projectName: queryString,
					category: 'all',
					sort: "nameAscending"
				}
		}
		this.fireEvent(StartTransfer.GET_TRANSFERS_PROJECTS, searchObject);
	}

	this.turnFeedbackButtonOn = function()
	{
		jQuery('a#startTransferButton', this.element).unbind('click');
		jQuery('a#startTransferButton', this.element).removeClass('disabledButton');
		jQuery('a#startTransferButton', this.element).removeClass('startTransfer25Button');
		jQuery('a#startTransferButton', this.element).addClass('startTransferSuccess');
		jQuery('a#startTransferButton', this.element).removeClass('button');
		jQuery('a#startTransferButton', this.element).removeAttr('href');
	}

	this.onTransferSuccess = function(payload)
	{    	
		jQuery('#projectTransferSuccess', this.element).show();
		this.turnFeedbackButtonOn();
		var buttonLabel = payload.results.success.length + " Project Cop";
		if (payload.results.success.length > 1) {
			buttonLabel += "ies Sent";
		} else {
			buttonLabel += "y Sent";
		}
		this.setTransferButtonLabel(buttonLabel);
	};
	
	this.setTransferButtonLabel = function(label) {
		jQuery('#startTransferButton span', this.element).html(label);
	};
}
StartTransfer = new Class(new StartTransfer());
StartTransfer.NAME = 'StartTransfer';
StartTransfer.GET_TRANSFERS_INCOMING = 'StartTransfer.GET_TRANSFERS_INCOMING';
StartTransfer.GET_TRANSFERS_OUTGOING = 'StartTransfer.GET_TRANSFERS_OUTGOING';
StartTransfer.GET_TRANSFERS_PROJECTS = 'StartTransfer.GET_TRANSFERS_PROJECTS';
StartTransfer.RETURN_TRANSFER_PROJECT_LIST = "StartTransfer.RETURN_TRANSFER_PROJECT_LIST";
StartTransfer.RETURN_TRANSFER_PROJECT_LIST_FAILURE = "StartTransfer.RETURN_TRANSFER_PROJECT_LIST_FAILURE";
StartTransfer.START_SINGLE_PROJECT_TRANSFER = 'StartTransfer.START_SINGLE_PROJECT_TRANSFER';
StartTransfer.START_BULK_PROJECT_TRANSFER = 'StartTransfer.START_BULK_PROJECT_TRANSFER';
StartTransfer.CONFIRM_USER_BY_EMAIL = 'StartTransfer.CONFIRM_USER_BY_EMAIL';
StartTransfer.INITIALIZED = 'StartTransfer.INITIALIZED';
ScriptLoader.loaded(StartTransfer.NAME);
