var StartupCommand = function()
{
    this.Extends = MacroCommand;

    this.initializeMacroCommand = function(notification/*Notification*/ )
    {
    	var browser = jQuery.browser;
    	if(!(browser.msie && browser.version < 7.0))
        {
    		this.addSubCommand(PrepModelCommand);
    		this.addSubCommand(PrepViewCommand);
        }else{
        	jQuery('#mainPanel').append("<h2 style='margin-top: 100px;  margin-left: auto; margin-right: auto; width: 300px; padding: 30px;'>We're sorry, you are using an unsupported version of Internet Explorer. For the best authoring experience, please upgrade your browser.</h2>");
        }
    	
    	
    	
    	
    	
    	
    	
    }
    
}
StartupCommand = new Class(new StartupCommand());