var ShowAccountDueCommand = function()
{
    this.Extends = SimpleCommand;

    this.execute = function(notification/*Notification*/ )
    {
    	var _this = this;
    	var payload = {};
    	var dialogLabel = "";
    	
    	if (undefined != notification.body.label) {
    		dialogLabel = notification.body.label + " ";
    	}
    	
    	dialogLabel += "Account Payment is Now Due";
    	
        var config = new LoadDialogueViewConfig(
        	dialogLabel,
            "accountPayDue.js",
            "accountPayDueMediator.js",
            "accountPayDue.html",
            payload
        );
            	
        this.facade.sendNotification(ApplicationFacade.SHOW_DIALOG, config );
    }
}
ShowAccountDueCommand = new Class(new ShowAccountDueCommand());