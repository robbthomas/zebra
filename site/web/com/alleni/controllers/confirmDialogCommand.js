var ConfirmDialogCommand = function()
{
    this.Extends = SimpleCommand;

    this.execute = function(notification/*Notification*/ )
    {
    	var _this = this;

        	var config = new LoadDialogueViewConfig(
        			"Confirm?",
        			"confirmDialog.js",
        			"confirmDialogMediator.js",
        			"confirmDialog.html",
        			notification.getBody()
        			);
        	
        
           this.facade.sendNotification(ApplicationFacade.SHOW_DIALOG, config );
    }
}
ConfirmDialogCommand = new Class(new ConfirmDialogCommand());
ConfirmDialogCommand.YES = "ConfirmDialogCommand.YES";
ConfirmDialogCommand.NO = "ConfirmDialogCommand.NO";