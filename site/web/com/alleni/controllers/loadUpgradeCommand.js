var LoadUpgradeCommand = function()
{
    this.Extends = SimpleCommand;
    
    this.viewLoaded = false;
    this.mediatorLoaded = false;
    this.item = undefined;

    this.execute = function(notification/*Notification*/ )
    {
    	var _this = this;
    	this.item = notification.getBody();
    	
    	if(!Application.upgradeSystemLoaded){
			this.loadScript( Application.config.paths.components+'/upgradeDialog.js?cb=' + Common.cb,function(){_this.onViewComplete(_this)});
			this.loadScript( Application.config.paths.mediators+'/upgradeDialogMediator.js?cb=' + Common.cb,function(){_this.onMediatorComplete(_this)});
    	}else{
    		this.sendNotification(ApplicationFacade.LOAD_UPGRADE_COMPLETE, this.item);
    	}     
    }
    
    this.setupUpgrade = function() {
    	jQuery('body').append(jQuery('<div></div>').attr('id', 'upgradeDialog').addClass('dialog'));
        var upgradeDialog = new UpgradeDialog('upgradeDialog');
        this.facade.registerMediator(new UpgradeDialogMediator(upgradeDialog));
        upgradeDialog.initializeChildren();
        upgradeDialog.childrenInitialized();
        upgradeDialog.initializationComplete();
        Application.upgradeSystemLoaded = true;
        
        this.sendNotification(ApplicationFacade.LOAD_UPGRADE_COMPLETE, this.item);
    }
    
    this.onViewComplete = function(scope) {
    	scope.viewLoaded = true;
    	scope.checkParts();
    }
    
    this.onMediatorComplete = function(scope) {
    	scope.mediatorLoaded = true;
    	scope.checkParts();
    }
    
    this.checkParts = function() {
    	if(this.viewLoaded && this.mediatorLoaded){
    		this.setupUpgrade();
    	}
    }
    
    this.loadScript = function (sScriptSrc, oCallback) {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = 'text/javascript';
        oScript.src = sScriptSrc;
        // most browsers
        oScript.onload = oCallback;
        // IE 6 & 7
        oScript.onreadystatechange = function() {
	        if (this.readyState == 'complete') {
	        	oCallback();
	        }
        }
        oHead.appendChild(oScript);
    }

}
LoadUpgradeCommand = new Class(new LoadUpgradeCommand());