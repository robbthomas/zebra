var LoadDialogueViewCommand = function()
{
    this.Extends = SimpleCommand;
    
    this.viewLoaded = false;
    this.mediatorLoaded = false;
    this.fragmentLoaded = false;
    this.scriptsLoaded = false;
    this.fragment = undefined;
    this.config = undefined;
    this.token = undefined;
    /**
     * Responsible for initiating the lazy-loading of the view & mediator files.
     * If files have already been loaded, it skips loading and notifies framework.
     */
    this.execute = function(notification/*Notification*/ )
    {
    	var _this = this;
    	var result, i, j;
    	var scriptLoader;
    	
    	this.config = notification.getBody();
    	
    	// generate token for targeted notifications
	    result = '';
	    for(j=0; j<32; j++)
	    {
		    if( j == 8 || j == 12|| j == 16|| j == 20)
		    result = result + '-';
		    i = Math.floor(Math.random()*16).toString(16).toUpperCase();
		    result = result + i;
	    }
	    this.token = result;
    	
	    if(this.config.scripts().length > 0){
	    	scriptLoader = new ScriptLoader(this.token,Application.unloadedScripts(this.config.scripts()),function(){_this.onScriptsComplete(_this)});
	    }else{
	    	this.scriptsLoaded = true;
	    }
	    
	    
    	if(!ApplicationFacade.loadedViews[this.config.component()]){
			this.loadScript( Application.config.paths.components+'/'+this.config.component()+'?cb=' + Common.cb,function(){_this.onViewComplete(_this)});
			this.loadScript( Application.config.paths.mediators+'/'+this.config.mediator()+'?cb=' + Common.cb,function(){_this.onMediatorComplete(_this)});
			var o = {
		            filename: this.config.fragment(),
		            success: function (fragment, status) {
		                _this.onFragmentComplete(fragment, _this);
		            }
		        }
		        File.requestFragment(o);
    	}else{
    		// send generic notification
            this.sendNotification(ApplicationFacade.CONFIG_DIALOG, this.config.payload());
            // send targeted notification
            this.sendNotification(ApplicationFacade.CONFIG_DIALOG+this.token, this.config.payload());
            
    	}     
    }
    
    this.setupView = function() {
    	var componentShort = this.config.component().split('.')[0];
    	var ComponentClass = getClassByName(componentShort.charAt(0).toUpperCase() + componentShort.slice(1));
    	var mediatorShort = this.config.mediator().split('.')[0];
    	var MediatorClass = getClassByName(mediatorShort.charAt(0).toUpperCase() + mediatorShort.slice(1));
    	var subView = new ComponentClass(componentShort);
    	var parentView = new DialogPanel(componentShort);
    	var subMediator = undefined;
    	var parentMediator = undefined;
    	// mark view loaded & available
        ApplicationFacade[this.config.component()] = true;
        // inject fragment into subView
        subView.set('html', this.fragment);
        this.fragment = undefined;
        // create the mediators
        this.facade.registerMediator(new DialogPanelMediator(componentShort+'_parent', parentView, this.token));
        this.facade.registerMediator(new MediatorClass(componentShort, subView, this.token));
        subMediator = this.facade.retrieveMediator(componentShort);
        parentMediator = this.facade.retrieveMediator(componentShort+'_parent');
        // initialize the parent view (dialog)
        parentView.initializeChildren();
        parentView.childrenInitialized();
        parentView.initializationComplete();
        // initialize the sub view
        subView.initializeChildren();
        subView.childrenInitialized();
        subView.initializationComplete();
        // set parentView title
        parentView.title(this.config.title());
        // inject subview into parentview
        parentView.content(subView.element);
        // show the parent
        parentView.open();
        // send generic notification
        this.sendNotification(ApplicationFacade.CONFIG_DIALOG, this.config.payload());
        // send targeted notification
        this.sendNotification(ApplicationFacade.CONFIG_DIALOG+this.token, this.config.payload());
        // recenter
        this.sendNotification(ApplicationFacade.CONFIG_RECENTER+this.token);
    }
    
    this.onViewComplete = function(scope) {
    	scope.viewLoaded = true;
    	scope.checkParts();
    }
    
    this.onMediatorComplete = function(scope) {
    	scope.mediatorLoaded = true;
    	scope.checkParts();
    }
    
    this.onFragmentComplete = function(fragment,scope) {
    	scope.fragment = fragment;
    	scope.fragmentLoaded = true;
    	scope.checkParts();
    }
    
    this.onScriptsComplete = function(scope){
    	scope.scriptsLoaded = true;
    	scope.checkParts();
    }
    
    this.checkParts = function() {
    	if(this.viewLoaded && this.mediatorLoaded && this.fragmentLoaded & this.scriptsLoaded){
    		this.setupView();
    	}
    } 
    
    
    
    // TODO: replace this is with something similar to QP's ScriptLoader pattern
    this.loadScript = function (sScriptSrc, oCallback) {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = 'text/javascript';
        oScript.src = sScriptSrc;
        // most browsers
        oScript.onload = oCallback;
        // IE 6 & 7
        oScript.onreadystatechange = function() {
	        if (this.readyState == 'complete') {
	        	oCallback();
	        }
        }
        oHead.appendChild(oScript);
    }

}
LoadDialogueViewCommand = new Class(new LoadDialogueViewCommand());