var LaunchEditorCommand = function()
{
    this.Extends = SimpleCommand;

    this.execute = function(notification/*Notification*/ )
    {
    	var _this = this;
    	if(Credentials.accountTypeTag() == 'collector') {
    		// TODO: launch upgrade
    		//this.sendNotification(ApplicationFacade.LOAD_UPGRADE, {item:undefined, upgradeTo:"creator"});
    	}else{
    		EditorPlayer.launchEditor(notification.body, true);
    	}
    }
}
LaunchEditorCommand = new Class(new LaunchEditorCommand());