var CheckAccountStandingCommand = function()
{
    this.Extends = SimpleCommand;

    this.execute = function(notification/*Notification*/ )
    {
    	var content = notification.body;
    	if(!Credentials.accountDue()){
			this.facade.sendNotification(content.successNotification, content.payload);
    	} else {
    		this.facade.sendNotification(content.failureNotification, content.failureConfig);
    	}
    };
};
CheckAccountStandingCommand = new Class(new CheckAccountStandingCommand());