var UpdateEditorPropertyCommand = function()
{
    this.Extends = SimpleCommand;

    this.execute = function(notification/*Notification*/ )
    {
    	var payload = {
    		property:'publishId',
    		oldValue: notification.getBody().oldValue,
    		newValue:notification.getBody().newValue,
    		idType:notification.getBody().idType
    	}
	    this.facade.retrieveProxy(ZappGadgetProjectProxy.NAME).requestDetail(payload.newValue, payload.idType);
    	onUpdateEditorProperty(payload);
    	
    	
    }
}
UpdateEditorPropertyCommand = new Class(new UpdateEditorPropertyCommand());