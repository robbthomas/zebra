var RequestPurchaseCommand = function()
{
    this.Extends = SimpleCommand;

    this.execute = function(notification/*Notification*/ )
    {
    	var item = notification.getBody();
    	var tag = Credentials.accountTypeTag();
    	if(Credentials.authenticated()){
    			switch(item._type)
    	    	{
    	    	case "gadget":
    	    		// collector accounts can't use gadgets, so prompt an upgrade
    	    		if(tag == 'collector'){
    	    			this.sendNotification(ApplicationFacade.LOAD_UPGRADE, {item:item, upgradeTo:undefined});
    	    		}else{
    	    			this.facade.retrieveMediator(BuyDialogMediator.NAME).open(item);
    	    		}
    	    		break;
    	    	case "zapp":
    	    		// collectors need to be redirected to the upgrade dialog if they want a non-free app
    	    		this.facade.retrieveMediator(BuyDialogMediator.NAME).open(item);
    	    		break;
    	    	}
    	}else{
    		this.facade.retrieveMediator(BuyDialogMediator.NAME).open(item);
    	}
 
    }
}
RequestPurchaseCommand = new Class(new RequestPurchaseCommand());