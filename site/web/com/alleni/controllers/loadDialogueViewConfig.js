var LoadDialogueViewConfig = function ()
{
	this._title = undefined;
	this._component = undefined;
	this._mediator = undefined;
	this._fragment = undefined;
	this._payload = undefined;
	this._scripts = [];
	/* CONSTRUCTOR */
	this.initialize = function (title,component,mediator,fragment,payload,scripts)
    {
		this.title(title);
		this.component(component);
		this.mediator(mediator);
		this.fragment(fragment);
		this.payload(payload);
		this.scripts(scripts);
    }
	
	this.title = function (title)
	{
		if(title != undefined) {
			this._title = title;
		}
		return this._title;
	}

	this.component = function (component)
	{
		if (component != undefined){
			this._component = component;
		}
		return this._component;
	}
	
	this.mediator = function (mediator)
	{
		if (mediator != undefined){
			this._mediator = mediator;
		}
		return this._mediator;
	}
	
	this.fragment = function (fragment)
	{
		if (fragment != undefined){
			this._fragment = fragment;
		}
		return this._fragment;
	}

	this.payload = function (payload)
	{
		if (payload != undefined){
			this._payload = payload;
		}
		return this._payload;
	}
	this.scripts = function (scripts)
	{
		if (scripts != undefined){
			this._scripts = scripts;
		}
		if(this._scripts == undefined){
			this._scripts = [];
		}
		
		return this._scripts;
	}

}
LoadDialogueViewConfig = new Class(new LoadDialogueViewConfig());