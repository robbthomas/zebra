var PrepViewCommand = function()
{
    this.Extends = SimpleCommand;

    this.execute = function(notification/*Notification*/)
    {
        var applicationPanel/*ApplicationPanel*/ = notification.getBody();

        this.facade.registerMediator(new ApplicationPanelMediator(applicationPanel));
        applicationPanel.initializeChildren();
        applicationPanel.childrenInitialized();
        applicationPanel.initializationComplete();

        jQuery('body').append(jQuery('<div></div>').attr('id', 'buyDialog').addClass('dialog'));
        var buyDialog = new BuyDialog('buyDialog');
        this.facade.registerMediator(new BuyDialogMediator(buyDialog));
        buyDialog.initializeChildren();
        buyDialog.childrenInitialized();
        buyDialog.initializationComplete();
        
    }
    
}
PrepViewCommand = new Class(new PrepViewCommand());

