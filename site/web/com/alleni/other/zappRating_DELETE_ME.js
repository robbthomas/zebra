ZappRating = {
    selected :          '/site/com/alleni/images/star_blue.png',
    unselected :        '/site/com/alleni/images/star_gray.png',
    selectedHover :     '/site/com/alleni/images/star_blue_big.png',
    unselectedHover :   '/site/com/alleni/images/star_gray_big.png'
};

ZappRating.init = function(ratingDivId) {
    var     ii;

    var     ratingDiv = jQuery('#' + ratingDivId);

    for (ii = 1; ii <= 5; ii++) {

        var     ratingElement = ratingDivId + '_' + ii;

        ratingDiv.append('<img id = "' + ratingElement + '" src="' + ZappRating.unselected + '">');

        jQuery('#' + ratingElement).click(ZappRating.ratingClickFunc).hover(ZappRating.ratingOverFunc, ZappRating.ratingOutFunc);
    }
}

ZappRating.draw = function(event, rating, selected, unselected) {
    var     elements = event.target.id.split('_', 2);
    var     id = elements[0];

    if (rating == null) {
        rating = elements[1];
    }

    var     ii;

    for (ii = 1; ii <= rating; ii++) {
        jQuery('#' + id + '_' + ii).attr('src', selected)
    }
    for (; ii <= 5; ii++) {
        jQuery('#' + id + '_' + ii).attr('src', unselected)
    }
    return {
        rating: rating,
        id: id
    };
}

ZappRating.ratingClickFunc = function(event) {
    var info = ZappRating.draw(event, null, ZappRating.selected, ZappRating.unselected);

    jQuery('#' + info.id).attr('rating', info.rating);

    alert('you clicked: id = "' + info.id + '" with rating = "' + info.rating+ '"');
}

ZappRating.ratingOverFunc = function(event) {
    ZappRating.draw(event, null, ZappRating.selectedHover, ZappRating.unselectedHover);
};

ZappRating.ratingOutFunc = function(event) {
    var     id = event.target.id.split('_', 1)[0];

    ZappRating.draw(event, jQuery('#' + id).attr('rating'), ZappRating.selected, ZappRating.unselected);
};


