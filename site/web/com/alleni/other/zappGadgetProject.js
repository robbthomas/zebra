ZappGadgetProject = {}
ZappGadgetProject.MAX_DESCRIPTION_LENGTH = 50;
ZappGadgetProject.MAX_NAME_LENGTH = 40;
ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS = 18;
ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS_DESC = 15;
ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS_DESC_TOOLTIP = 60;

ZappGadgetProject.description = function(description)
{
	description = ZappGadgetProject.formatDescription(description);
    if (description == null || description.length == 0) {
        description = 'No description found';
    }
    if (description.length > ZappGadgetProject.MAX_DESCRIPTION_LENGTH) {
        description = description.substring(0, ZappGadgetProject.MAX_DESCRIPTION_LENGTH) + '...';
    }
    return makeSafeString(description);
}
ZappGadgetProject.name = function(name)
{
	name = ZappGadgetProject.formatName(name);
    if (name == null || name.length == 0) {
    	name = 'No name found';
    }
    if (name.length > ZappGadgetProject.MAX_NAME_LENGTH) {
    	name = name.substring(0, ZappGadgetProject.MAX_NAME_LENGTH) + '...';
    }
    return makeSafeString(name);
}
ZappGadgetProject.formatName = function(name)
{
	if(name.indexOf(' ') >= this.MAX_CONSECUTIVE_CHARACTERS || name.indexOf(' ') == -1 && name.length > this.MAX_CONSECUTIVE_CHARACTERS )
	{
		name = name.substr(0,this.MAX_CONSECUTIVE_CHARACTERS-1) + '...';
	}
	return name;
}
ZappGadgetProject.formatDescription = function(description) {
	var startPos = 0;
	
	if(description != undefined && description.indexOf(' ') == -1 && description.length > ZappGadgetProject.MAX_DESCRIPTION_LENGTH) {
		while(startPos + ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS_DESC < description.length) {
			description = description.substr(0,startPos + ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS_DESC) + '<br/>' + description.substr(startPos + ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS_DESC, description.length - 1);
			startPos += ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS_DESC + 5;
		}
	}
	return description;
}
ZappGadgetProject.formatDescriptionTooltip = function(description) {
	var startPos = 0;
	
	if(description != undefined && description.indexOf(' ') == -1 && description.length > ZappGadgetProject.MAX_DESCRIPTION_LENGTH) {
		while(startPos + ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS_DESC_TOOLTIP < description.length) {
			description = description.substr(0,startPos + ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS_DESC_TOOLTIP) + ' ' + description.substr(startPos + ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS_DESC_TOOLTIP, description.length - 1);
			startPos += ZappGadgetProject.MAX_CONSECUTIVE_CHARACTERS_DESC_TOOLTIP + 5;
		}
	}
	return makeSafeString(description);
}

ZappGadgetProject.projectAccountTypePrefix = function(projectAccountType) {
	var returnString = "";
	if(projectAccountType != undefined && projectAccountType != "undefined") {
		returnString = projectAccountType.charAt(0).toUpperCase() + projectAccountType.slice(1).toLowerCase() + ": ";
	}
	
	return returnString;
}

ZappGadgetProject.html = function (index, item, group, type, mode)
{
    var html = '';
    var switchValue = group + '.' + type + '.';
    switchValue += (undefined != mode) ? mode : '';
    
    switch (switchValue)
    {
    case 'visitor.zapp.':
    case 'visitor.gadget.':
        html = this.zappGadgetGrid(index, item, type, true, item.urlName, IdType.URL_NAME);
        break;
    case 'visitor.project.':
        break;
    case 'member.zapp.':
        html = this.zappGadgetGrid(index, item, type, false, item.playerId, IdType.APN_UUID);
        break;
    case 'member.gadget.':
        html = this.zappGadgetTable(index, item, type, item.playerId, IdType.APN_UUID);
        break;
    case 'member.project.':
        html = this.projectTable(index, item, item.projectId, IdType.PROJECT_ID);
        break;
    default:
        break;
    }
    return html;
}
ZappGadgetProject.zappGadgetGrid = function(index, item, type, visitor, id, idType)
{
	var arrCurrentLocation = window.location.href.split('/');
	var currentHash = arrCurrentLocation[arrCurrentLocation.length - 1].split('?')[0];
	
	var url = IdType.detailUrl(id, idType);
    var buyOwned = '<a href="#buy" class="button buyButton"><span>'+item.formattedPrice+'</span></a>';
//    if (Credentials.owned(item.id)) {
    if (item.owned == true) {
        buyOwned = '<span style="font-style:italic">- In Collection -&nbsp;</span>';
    }
    if (item.updateAvailable == true) {
    	buyOwned = '';
    }
    var preview = (item.featured) ? '<a href="' + url + '" class="preview"></a>' : '<a style="vertical-align:top;" class="previewZappGadget" href="' + url + '"></a>';
    var toggleFeatured = Credentials.accountAdministrator()? item.featured? ' | <a href="#toggleFeatured">unfeature</a>' : ' | <a href="#toggleFeatured">feature</a>' : '';
    var updateAvailable = (item.updateAvailable) ? ' | <a href="#updateApp">Update Available</a>' : '';
    
    var html = '';
    html += '<div class="item">';
    if (!visitor)
    {
        html += '   <div class="play"><a href="' + url + '" class="button"><span>Play</span></a></div>';
    }
    html += '    <div class="icon">';
    html += '        <a href="' + url + '"><img class="iconLogo" width="50" height="50" src="' + item.icon + '"></a>';
    html += '		 <div class="divSpinner" style="display:none;"></div>';
    html += '     </div>';
    html += '    <ul>';
    html += '        <li class="name"><a href="' + url + '">' + ZappGadgetProject.name(item.publishedName) + '</a></li>';
    html += '        <li class="description">'+ZappGadgetProject.description(item.description)+'</li>';
    html += '        <li class="details">Category:<span>'+item.category+'</span></li>';
    if (visitor)
    {
        html += '        <li class="details">Author:<span class="author" id="' + item.authorMemberId+'">'
        if(currentHash != 'authorProfile') {
        	html += '            <a href="#/authorProfile?id='+ item.authorMemberId + '">';
        }
        html += item.author;
        if(currentHash != 'authorProfile') {
        	html += '</a>';
        }
        html += '</span></li>';
    }
    html += '        <li class="rating">';
    html += '           <img width="63" height="10" src="com/alleni/images/stars_blue'+item.rating+'.png">';
    html += '       </li>';
    html += '    </ul>';
    html += '    <div class="actions">';

    if (visitor)
    {
        html +=         buyOwned;
        html += '        <a href="' + url + '">Details</a>';
    } else {
        html += '        <a href="' + url + '">Details</a>';
        html += '       | <a href="#unpurchase">Remove from Collection</a>';
    }
    
    html += toggleFeatured;
    html += updateAvailable;
    
    html += '    </div>';
    html += '</div>';
    return html;
}
ZappGadgetProject.zappGadgetTable = function (index, item, type, id, idType)
{
	var url = '#/detail?'+idType+'&'+id;

	// name, icon, project, category, source, permissions, ver, rating
	var permsStr = '';
	for (var perm in item.userAccessPrivs) {
		if (item.userAccessPrivs[perm] == true) {
			var p = perm.charAt(0).toUpperCase();
			p += perm.substr(1);
			permsStr += p + ", ";
		}
	}
	permsStr = permsStr.substr(0, permsStr.lastIndexOf(','));
	
    var html = '<tr class="item row' + (index%2+1) + '">' +
	    '<td><img width="50" src="' + item.icon + '"></td>'+    
	    '<td>' + 
        '<strong class="black">' + ZappGadgetProject.name(item.publishedName) + '</strong>'+
        '<br><a href="' + url + '">Details</a> | <a href="#unpurchase">Remove from Collection</a>' +
        '</td>'+
        
        '<td>' + ((item.category == null) ? '---' : item.category) + '</td>'+
        '<td>' + item.source + '</td>'+
        '<td>' + (permsStr != ''? permsStr : '---') + '</td>'+
        '<td>'+
        '<img width="63" height="10" src="com/alleni/images/stars_blue' + (item.rating != null ? item.rating : 0)  + '.png">' +
        '<br>' + item.commentsCountText +
        '</td>'+
        '</tr>';
    return html;
}
ZappGadgetProject.projectGrid = function (index, item, id, idType)
{
    Log.error('ZappGadgetProject.projectGrid not implemented');
}
ZappGadgetProject.projectTable = function (index, item, id, idType)
{
	var url = IdType.detailUrl(id, idType);

	var publishState = 'Zapp';
    var category = (null==item.category)?"":item.category;
    var removeLink = '<a href="#unpublish">Unpublish</a>'
    if (!item.published) {
        publishState = ''
        removeLink = '<a href="#delete">Delete</a>'
    }
    var urlImage = 'https://s3.amazonaws.com/com.alleni.zebra.site.content/icons/genericIcon50.png';
    if (item.icon != undefined && item.icon.sizes != undefined) {
    	urlImage = item.icon.baseUrl+item.icon.sizes[0]+item.icon.fileNameExt;
    } 
    				
    
    var html = '<tr class="item row' + (index%2+1) + '">' + 
    	'<td><div class="projectMeta icon"><div class="left"><img class="iconLogo" src=\"'+urlImage+'\" class="zapTTEnable" style="float:left; margin-right:15px;" id="index_' + index + '"/></div><div class="right"><strong class="black"><div class="name">' + ZappGadgetProject.projectAccountTypePrefix(item.projectAccountTypeName) + ZappGadgetProject.name(item.projectName) + '</div></strong><div class="editHistoryDelete"><a href="#edit?' + item.projectId + '">Edit</a> | '+removeLink+' | <a href="#history?'+item.projectName+'">History</a></div></div> '
        +'</td>'+
    	
        '<td>' + ((item.editedDateTime == null) ? '---' : new Date(item.editedDateTime).format("m/dd/yyyy <br /> hh:MM TT"))+'</td>' + //'<br />'h:MM TT 
        '<td>' + ((item.published)?('<a href="' + url + '">Yes</a>'):'No') + '</td>'+
        '<td>' + category + '</td>' 
        '</tr>';

    return html;
}

ZappGadgetProject.adjustHeights = function (featuredBrowse)
{
 // TODO: double check references and remove
}
ZappGadgetProject.NAME = 'ZappGadgetProject';
ZappGadgetProject.TYPE_ZAPP = 'ZappGadgetProject.TYPE_ZAPP';
ScriptLoader.loaded(ZappGadgetProject.NAME);
