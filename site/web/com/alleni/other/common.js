Common = {}
Common.mockdata = false; // when true, run via file: for standalone, run via http: to have mockdata delivered in lieu of .jsp call
Common.clearerHTML = function ()
{
    return '<div class="clearer"></div>';
}
Common.clearerElement = function ()
{
	var div = document.createElement('div');
	div.className = 'clearer';
	return div;
}
Common.reloadApplication = function()
{
	window.location.reload(true);
}
Common.loadURL = function(location)
{
	window.location.href = location;
}
Common.hasHistory = function()
{
	var browser = jQuery.browser;
	var hasHistory = false;
	
	if((browser.msie || browser.opera) && window.history.length > 0){
		hasHistory = true;
	}else if((browser.safari || browser.webkit || browser.mozilla) && window.history.length > 1){
		hasHistory = true;
	}
	return hasHistory;
}
/**
 * This is the only way to calculate
 * an element's position that is cross-browser
 * compatible.  
 */
Common.findPosition = function(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		do {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	}
	return [curleft,curtop];
}
Common.popupFeatures = "status=no,toolbar=no,location=no,menubar=no,directories=no,resizable=1,scrollbars=no";

Debugger = function (s) {
    Log.error('Debugger - '+s);
}

Sharing = {
    showPanel: function (publishedId, playerId) {
        alert("showing sharing panel for " + publishedId + " and playerid " + playerId);
    }
}

EditorPlayer = {
	editor: function () {
		return document.getElementById("editor");
	},
    launchEditor: function (cfg, secondaryWindow) {
        if (secondaryWindow == undefined) secondaryWindow = false;

        this.id = (cfg==null)?undefined:cfg.id; // allow overwrite of existing value. this will ensure a clean editor when no id arg is provided
        this.bShowHistory = (cfg==null)?false:cfg.bShowHistory;
        this.projectName = (cfg==null)?"":cfg.projectName;
        var popupFeatures = Common.popupFeatures;
        var width = 1024;
        var height = 700;
        var left   = (screen.width  - width)/2;
        var top    = (screen.height - height)/2;
        popupFeatures += ', width='+width+', height='+height;
        popupFeatures += ', top='+top+', left='+left;

        if (secondaryWindow) {
            var newWindow = window.open('/editor.html', (cfg==undefined)?"_blank":cfg.id, popupFeatures);
            newWindow.focus();
            return;
        }

        if (this.editorWindow == undefined) {
            this.editorWindow = window.open('/editor.html', (cfg==undefined)?"_blank":cfg.id, popupFeatures);
        } else {
            if (this.editorContext != null)
                this.editorContext.handleInvoke(cfg.id);
            else
            {
                //Log.debug("CEditorPlayer.launchEditor - editorContext unavailable despite window ID being defined");
            }
        }
        this.editorWindow.focus();
    },
    requestDetailsPanel: function (id, idType, requestingWindow) {
        if (requestingWindow == undefined) requestingWindow = this.editorWindow;

        var popupFeatures = Common.popupFeatures;
        var width = 800;
        var height = 700;
        var editorX = 0;
        var editorY = 0;
        if (requestingWindow) {
            var editorX = (document.all)?requestingWindow.screenLeft:requestingWindow.screenX;
            var editorY = (document.all)?requestingWindow.screenTop:requestingWindow.screenY;
        }
        var left   = editorX + 50;
        var top    = editorY + 50;
        popupFeatures += ', width='+width+', height='+height;
        popupFeatures += ', top='+top+', left='+left;
        
        var url = IdType.detailUrl(id, idType);
        this.detailsWindow = window.open(url, "details", popupFeatures);
        this.detailsWindow.focus();
    },
    handleEditorUnloaded: function () {
        this.editorWindow = undefined;
        this.editorContext = undefined;
    },
    launchPlayer: function (cfg, secondaryWindow) {
        var height;
        var width;
        var playerId;

        if(cfg[0]){
            height = cfg[0].height;
            width = cfg[0].width;
            playerId = cfg[0].id;
        }else{
            playerid = cfg;
            height = 700;
            width = 1024;
        }

        if (secondaryWindow == undefined) secondaryWindow = false;

        var popupFeatures = Common.popupFeatures;
        var left   = (screen.width  - width)/2;
        var top    = (screen.height - height)/2;
        popupFeatures += ', width='+width+', height='+height;
        popupFeatures += ', top='+top+', left='+left;

        if (secondaryWindow) {
            var newWindow = window.open('/embed/'+playerId, null, popupFeatures);
            newWindow.focus();
            return;
        }

        if (this.playerWindow == undefined) {
            this.playerWindow = window.open('/embed/'+playerId, playerId, popupFeatures);
        } else {
            if (this.playerContext != null)
                this.playerContext.handleInvoke(playerId);
            else
            {
                //Log.debug("EditorPlayer.launchPlayer - playerContext unavailable despite window ID being defined");
            }
        }
        this.playerWindow.focus();
    },
    handlePlayerUnloaded: function () {
        this.playerWindow = undefined;
        this.playerContext = undefined;
    },
    ready: function (context, callback, isEditor) {
        //Log.debug('EditorPlayer.ready');
        //callback({email: Credentials.email(), password: Credentials.password(), id: this.id});
        callback({username: Credentials.username(), password: Credentials.password(), id: this.id, bShowHistory: this.bShowHistory, projectName: this.projectName});
        this.id = undefined;
        if (isEditor == true && this.editorContext == undefined)
            this.editorContext = context;
        else
            this.playerContext = context;
    },
    refresh: function () {
    	var facade = ApplicationFacade.getInstance();
    	facade.sendNotification(Application.REFRESH);
    }
}
EditorPlayer.id = undefined;
EditorPlayer.bShowHistory = false;
EditorPlayer.projectName = "";
EditorPlayer.detailsWindow = undefined;
EditorPlayer.editorWindow = undefined;
EditorPlayer.editorContext = undefined;
EditorPlayer.playerWindow = undefined;
EditorPlayer.playerContext = undefined;

Dialog = {
    dialog: undefined
}
Dialog.display = function (html)
{
    jQuery('div#dialog').html(html);
    this.dialog = jQuery('div#dialogs div#loginDialog').dialog({
        autoOpen: false,
        modal: true,
        resizable: false
    });
    this.dialog.dialog('open');
}

ProcessingScreen = {}
ProcessingScreen.show = function (busyMessage)
{
	//.processAnimation
	jQuery('#processingBadge span').text(busyMessage);
	jQuery('div.buttonsAction').fadeTo(250, .4);
	jQuery('#processingScreen').fadeIn(250, function (event){
		
		jQuery('#processingBadge').fadeIn(500, function (event){
			jQuery('#processAnimation').fadeIn(250, function (event){
				jQuery('#processingBadge span').fadeIn(250);
			})
		});
	});
}

ProcessingScreen.hide = function (finishMessage, duration)
{
	//.processAnimation
	setTimeout(function(){
		
		jQuery('#processingBadge span').fadeOut(250, function (event){
			jQuery('#processingBadge span').text(finishMessage);
			jQuery('#processingBadge span').fadeIn(250, function (event){
				setTimeout(function(){
					jQuery('#processAnimation').fadeOut(250);
					jQuery('#processingBadge span').fadeOut(250, function (event){
						jQuery('#processingBadge').fadeOut(500, function (event){
							jQuery('#processingScreen').fadeOut(250, function(event){
								jQuery('#processingBadge span').text('Processing your request...');
								jQuery('div.buttonsAction').fadeTo(250, 1);
							});
						});
					});
			    },
			    duration);
			});
		});
		
		
	},2000);
	
	
	
}


Overlay = {} //TODO acavan - this should use DialogPanel...
Overlay.display = function (filename)
{
    Log.debug('Legal Overlay.display');
    File.requestFragment({
        filename: filename,
        path: Application.config.paths.fragments+'/popup',
        success: function(fragment, status) {
            jQuery('#legalPopup').html(fragment);
            jQuery('a#close',jQuery('.overlayContent.legal').get()).unbind('click').click(function (event) {
            	Log.debug('Overlay.close');
            	Overlay.hideAll();
                window.history.back();
                return false;
            });
            jQuery('a',jQuery('.overlayContent.legal').get()).not('#close').unbind('click').click(function (event) {
                if (this.hash.indexOf('legal') != -1)
                {
                    event.preventDefault();
                    jQuery.History.trigger(this.hash.substring(1));
                    return false;
                }
            });
            jQuery('.overlay.legal').show();
            jQuery('.overlayContent.legal').show();
        }
    });
}
Overlay.hideAll = function()
{
	Log.debug('Legal Overlay.hideAll');
    jQuery('#legalPopup').html('');
    jQuery('.overlay.legal').hide();
    jQuery('.overlayContent.legal').hide();
}

Credentials = {
    _accountAdministrator: false,
    _accountId: '',
    _userId:'',
    _authenticated: false,
    _email: '',
    _keepSignedIn: false,
    _viewedWelcomeMessage:false,
    _password: '',
    _systemAdministrator: false,
    _accountTypeTag: '',
    _username: '',
	_displayName: '',
    _hasCreditCardProfile: false,
    _goodStanding: false,
    _termDueDate: new Date(),
    _firstName: '',
    _lastName: '',
    _nameFormat: 'firstLast',
    _owned: {
    },
    COOKIE_HEADER: 'header',
    COOKIE_SESSION: 'session',
    COOKIE_WELCOME: 'welcome',
    accountAdministrator: function (b) {
        if (b != undefined)
            this._accountAdministrator = b;
        return this._accountAdministrator;
    },
    accountId: function (s) {
        if (s != undefined)
            this._accountId = s;
        return this._accountId;
    },
    userId: function (s) {
        if (s != undefined)
            this._userId = s;
        return this._userId;
    },
    authenticated: function (b) {
        if (b != undefined)
            this._authenticated = b;
        return this._authenticated;
    },
    email: function (s) {
        if (s != undefined)
            this._email = s;
        return this._email;
    },
    keepSignedIn: function(b) {
        if (b != undefined)
            this._keepSignedIn = b;
        return this._keepSignedIn;
    },
    viewedWelcomeMessage: function(b) {
    	var cookies = jQuery.cookie();
    	var retireOn = new Date ( "January 15, 2013" );
    	// directly writing to cookies here for 
    	// determining if welcome window is displayed
    	if(b != undefined){
    		this._viewedWelcomeMessage = b;
        	jQuery.cookie(Credentials.COOKIE_WELCOME, b ? 'true':'false', {expires: 365});
    	}
    	if(new Date() >= retireOn){
    		jQuery.cookie(Credentials.COOKIE_WELCOME, 'true', {expires: 365});
    	}
    	return cookies[Credentials.COOKIE_WELCOME] == 'true';
    },
    owned: function (s, owned) {
        if (owned == true) {
            this._owned[s] = true;
            return true;
        } else if(owned == false){
        	delete this._owned[s];
        	return false;
        }
        return this._owned[s] == true ? true : false;
    },
    password: function (s) {
        if (s != undefined)
            this._password = s;
        return this._password;
    },
    systemAdministrator: function (b) {
        if (b != undefined)
            this._systemAdministrator = b;
        return this._systemAdministrator;
    },
    accountTypeTag: function (b) {
        if (b != undefined)
            this._accountTypeTag = b;
        return this._accountTypeTag;
    },
    presentationName: function () {
        if(this.nameFormat() == 'firstLast'){
        	return this.firstName() + ' ' + this.lastName();
        }else{
        	return this.displayName();
        }

    },
    username: function (s) {
        if (s != undefined){
        	this._username = s;
        }
        return this._username;
    },
	displayName: function (s) {
     if (s != undefined){
     	this._displayName = s;
     }
     return this._displayName;
    },
    firstName: function (s) {
        if (s != undefined)
            this._firstName = s;
        return this._firstName;
    },
    lastName: function (s) {
        if (s != undefined)
            this._lastName = s;
        return this._lastName;
    },
    nameFormat: function (s) {
        if (s != undefined)
            this._nameFormat = s;
        return this._nameFormat;
    },
    hasCreditCardProfile: function (b) {
        if (b != undefined)
            this._hasCreditCardProfile = b;
        return this._hasCreditCardProfile;
    },
    goodStanding: function (b) {
        if (b != undefined)
            this._goodStanding = b;
        return this._goodStanding;
    },
    termDueDate: function (b) {
        if (b != undefined)
            this._termDueDate = b;
        return this._termDueDate;
    },
    accountDue: function () {
    	if(Credentials.termDueDate() > new Date() && Credentials.goodStanding()){
    		return false;
    	}else{
    		return true;
    	}
    },
    set: function (o) {
        //Require(o, ['email', 'password', 'authenticated', 'accountId'], 'Credential.set');
        //Require(o, ['username', 'email', 'password', 'authenticated', 'accountId', 'accountTypeTag', 'hasCreditCardProfile', 'goodStanding', 'termDueDate'], 'Credential.set');
        this.accountAdministrator(o.accountAdministrator != undefined ? o.accountAdministrator : false);
        this.accountId(o.accountId);
        this.authenticated(o.authenticated != undefined ? o.authenticated : false);
        this.email(o.email);
        this.keepSignedIn(o.keepSignedIn);
        this.password(o.password);
        this.systemAdministrator(o.systemAdministrator != undefined ? o.systemAdministrator : false);
        this.userId(o.userId);
        this.username(o.username);
	    this.displayName(o.displayName);
        this.firstName(o.firstName);
        this.lastName(o.lastName);
        this.nameFormat(o.nameFormat);
        this.accountTypeTag(o.accountTypeTag);
        this.hasCreditCardProfile(o.hasCreditCardProfile);
        this.goodStanding(o.goodStanding);
        this.termDueDate(o.termDueDate);
        this._owned = {};
        this.writeCookies(this.keepSignedIn());
    },
    visitor: function () {
        var vc = this.visitorCredentials();
        this._accountId = -1;
        this._authenticated = false;
        this._email = vc.email;
        this._owned = {};
        this._password = vc.password;
        this._userId = -1;
        this._username = vc.username;
	    this._displayName = vc.displayName;
        this.clearCookies();
        this.keepSignedIn(false);
    },
    visitorCredentials: function () {
        return {username: 'guest', email: 'guest@zebrazapps.com', password: 'none', displayName: 'Guest'};
    },
    clearCookies: function () {
        jQuery.cookie(Credentials.COOKIE_HEADER, '', {expires: -1});
        jQuery.cookie(Credentials.COOKIE_SESSION, '', {expires: -1});
    },
    writeCookies: function (keepSignedIn) {
        s = Base64.encode(this._username+':'+this._password);
        jQuery.cookie(Credentials.COOKIE_HEADER, s, keepSignedIn ? {expires: 365} : undefined);
        jQuery.cookie(Credentials.COOKIE_SESSION, keepSignedIn ? 'false' : 'true', keepSignedIn ? {expires: 365} : undefined);
    },
    readCookies: function () {
        var visitor = true;
        var cookies = jQuery.cookie();
        if (typeof cookies == 'object')
        {
        	if(cookies[Credentials.COOKIE_HEADER] != undefined){
        		var a = Base64.decode(cookies[Credentials.COOKIE_HEADER]).split(':');
                if (a.length == 2)
                {
                    this.username(a[0]); // this could be a username but login should sort out username and email...
                    this.password(a[1]);
                    this.keepSignedIn(cookies[Credentials.COOKIE_SESSION] == 'false');
                    visitor = false;
                }
        	}
        	if(cookies[Credentials.COOKIE_WELCOME] == undefined){
        		jQuery.cookie(Credentials.COOKIE_WELCOME, 'false', {expires: 365});
        	}
        }
        if (visitor)
            this.visitor();
    }
}
Credentials.readCookies();

ScriptLoader = function (id, scriptList, callback) {
	this.id = id;
	this.uniqueId = ScriptLoader.nextId++;
    Log.info('ScriptLoader starting ' + this.id + ' ' + this.uniqueId);
    ScriptLoader.instances.push(this);
    this.scriptList = scriptList;
    this.callback = callback;
    this.load();
}
ScriptLoader.prototype.load = function () {
    if (this.scriptList.length > 0)
    {
        var src = this.scriptList.pop();
        Log.info('ScriptLoader.load - src:'+src + ' ' + this.id + ' ' + this.uniqueId);
        var head = document.getElementsByTagName('head')[0];
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.src = src + '?cb=' + Common.cb;
        head.appendChild(s);
    }
}
ScriptLoader.prototype._loaded = function (filename) {
        for (var i = 0; i < this.scriptList.length; i++)
        {
            if (this.scriptList[i].toLowerCase().indexOf(filename.toLowerCase()) != -1)
            {
                this.scriptList.splice(i, 1);
                break;
            }
        }
        if (this.scriptList.length == 0)
        {
            for (var i = 0; i < ScriptLoader.instances.length; i++)
            {
                if (ScriptLoader.instances[i] === this)
                    ScriptLoader.instances.splice(i, 1);
            }
            Log.info('ScriptLoader finished ' + this.id + ' ' + this.uniqueId);
            this.callback();
        }
        else
            this.load();
}
ScriptLoader.nextId = 0;
ScriptLoader.instances = [];
ScriptLoader.loaded = function (script) {
    Log.info('ScriptLoader.loaded - script:'+script + ' ' + this.id + ' ' + this.uniqueId);
    for (var i = 0; i < ScriptLoader.instances.length; i++)
    {
        ScriptLoader.instances[i]._loaded(script);
    }
}

File = {};
File.requestFile = function (o, type, baseURL, caller) {
    if (!Require(o, ['filename', 'success'], 'File.'+type))
        return;
    var error = function (x, y, z) {
        Log.error('File.'+caller+' - ajax error for \''+o.filename+'\'\n   '+this.url+'\n   '+z.message);
    }
    jQuery.ajax({
      url: baseURL+'/'+o.filename + (o.filename.indexOf('?') === -1 ? '?' : '&') + 'cb=' + Common.cb,
      data: '',
      success: o.success,
      error: o.error == undefined ? error : o.error,
      dataType: type
    });    
}
File.requestFragment = function (o) {
    File.requestFile(o, 'html', o.path != undefined ? o.path : Application.config.paths.fragments, 'requestFragment');
}
File.requestJSON = function (o) {
    File.requestFile(o, 'json', o.path == undefined ? Application.config.paths.json : o.path, 'requestJSON');
}
File.requestTest = function (o) {
    File.requestFile(o, 'html', Application.config.paths.tests, 'requestTest');
}

Form = {}
Form.showInfo = false;
Form.populate = function (id, values) {
    if (values == undefined)
        return;
    var keysValues = '';
    for (i in values)
        keysValues += '\n    '+i+':'+values[i];
    var unresolvedNames = '';

    jQuery(id+' span').each(function (index, element){
        var storedValue = values[element.id];
        if (storedValue == undefined)
        {
            unresolvedNames += '\n    '+element.name;
            return;
        }
        this.innerHTML = storedValue;
    });

    jQuery(id+' [name]').each(function (index, element) {
        var storedValue = values[element.name]
        if (storedValue == undefined)
        {
            unresolvedNames += '\n    '+element.name;
            return;
        }
        switch (element.type)
        {
        case 'text':
        case 'textarea':
            this.value = storedValue;
            break;
        case 'select-one':
            this.value = storedValue;
            break;
        case 'checkbox':
            this.checked = storedValue;
            break;
        case 'radio':
            this.checked = (storedValue == this.value);
            break;
        default:
            break;
        }
    })
    var unresolvedIds = '';
    jQuery(id+' [id]').each(function (index, element) {
        var storedValue = values[element.id]
        if (storedValue == undefined)
        {
            unresolvedIds += '\n    '+element.id;
            return;
        }
        switch (element.tagName.toLowerCase())
        {
        case 'span':
            this.innerHTML = storedValue;
            break;
        case 'img':
            this.src = storedValue;
            break;
        default:
            break;
        }
    })
    if (this.showInfo)
       Log.info('Form.populate\n  unresolvedNames:'+unresolvedNames+'\n  unresolvedIds:'+unresolvedIds+'\n  keysValues:'+keysValues);
}
Form.clear = function (form)
{
    jQuery(':input', form).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected'); 
}
Form.toObject = function(form) // TODO acavan - move to testbench proxy?
{
    var o = {};
    if (form.serializeArray == undefined)
    {
        Log.error('Form.toObject - serializeArray:undefined');
        return o;
    }
    var a = form.serializeArray();
    jQuery.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};



WebService = {}
WebService.request = function (o, credentials, contentTypeOverride)
{
    //Log.debug('WebService.request');
    // where o is {
    //     payload,
    //     pathFilename,
    //     callbacks {
    //         success,
    //         [failure],
    //         [error],
    //         [raw],
    //     }
    //     [host]
    // }
    if (!Require(o, ['payload', 'pathFilename', 'callbacks'], 'WebService.request') || !Require(o.callbacks, ['success'], 'WebService.request'))
        return;

    var username = '';
    var password = '';
    if (credentials != undefined) // if we were passed credentials use them
    {
        username = credentials.username;
        password = credentials.password;
    }
    else // otherwise use the global credentials
    {
	    username = Credentials.username();
        password = Credentials.password();
    }
    
    var type = 'POST';
    if (o.type != undefined) type = o.type;
    
    var data = {};
    var url = '';
    var processData = true;
    var contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    var successFn =  function (payload, status) {
		if (payload.success == true)
			o.callbacks.success(payload.data);
		else
		{
			if (o.callbacks.failure)
				o.callbacks.failure(payload.error, payload.detail);
			Log.error('WebService.request - web service returned success == false\nerror:'+payload.error+'\ndetail:'+payload.detail);
		}
		if (o.callbacks.raw)
			o.callbacks.raw(payload);
	};
    
    switch (o.host)
    {
    case 'site':
    case undefined:
        data.json = JSON.encode(o.payload);
        url = this.url(o.pathFilename, 'site');
        jQuery.ajaxSetup({contentType: 'application/x-www-form-urlencoded; charset=UTF-8'});
        break;
    case 'zephyrDirect':
    	isZephyrDirect = true;
        data = JSON.encode(o.payload);
        processData = false;
        contentType = (contentTypeOverride == undefined?'application/json' : contentTypeOverride);
        url = '/zephyr/'+o.pathFilename;
        successFn = function (payload, status) {
			if (status != 200 || payload.code != 200)
				o.callbacks.success(payload);
			else
			{
				if (o.callbacks.failure)
					o.callbacks.failure(payload.code, payload.message);
				Log.error('WebService.request - web service returned success == false\nerror:'+payload.code+'\ndetail:'+payload.message);
			}
			if (o.callbacks.raw)
				o.callbacks.raw(payload);
		};
        jQuery.ajaxSetup({contentType: "application/json; charset=UTF-8"});
    	break;
    case 'zephyr':
        type = 'GET';
        url = './services/zephyr/proxy.jsp';

        // 'method' controls how the server interacts with the Zephyr web serivces.
        // 'type' controls how the client interacts with the server.
        if (o.method == undefined) {
            o.method = 'get';
        }

        data.json = JSON.encode({
            proxy: {
                host: window.document.location.hostname,
                path: '/zephyr/'+o.pathFilename,
                payload: o.payload,
                method: o.method
            }
        })
        //Log.debug('WebService.request - zephyr data:'+data.json);
        break;
    default:
    	Log.error('WebService.request - unknown host:'+o.host);
    return;
    break;
    }

    jQuery.ajax({
    	url: url,
    	type: type,
    	data: data,
    	processData: processData,
    	dataType: 'json',
    	contentType: contentType,
    	timeout: 20000,
    	success: successFn,
    	error: function (x, y, z) {
    		if (o.callbacks.error)
    			o.callbacks.error(z);
    		else if (z == 'timeout')
    			Log.error('WebService.request - timeout \nurl:'+this.url);
    		else
    			Log.error('WebService.request - unexpected error\nurl:'+
    					this.url+'\njson:'+data.json+' ['+x+' '+y+' '+z+']');
    	},
    	beforeSend : function(req) {
    		req.setRequestHeader('Authorization',  'Basic ' + Base64.encode(username+':'+password));
    	}
    });

    return url;
}
WebService.url = function (pathFilename, host) {
    if (Application.config.paths.webservices[host] == undefined)
        Log.error('WebService.url - webservices['+host+'] is undefined');
        
    var url = Application.config.paths.webservices[host]+'/';
    if (Common.mockdata)
    {
        if (this.mockdataPaths[pathFilename] != undefined)
        {
            pathFilename = this.mockdataPaths[pathFilename];
            url = Application.config.paths.mockdata+'/';
        }
        else
            Log.error('WebService.url - no mockdataPaths entry for:'+pathFilename);
    }
    var query = pathFilename.indexOf('?');
    if (query != -1)
    {
        pathFilename = pathFilename.substr(0, query);
    }
    return url+pathFilename;
}
WebService.mockdataPaths = { //TODO acavan - move these to application.js
    'account/get.jsp':          'accountGet.json',
    'appuser/login.jsp':        'appuserLoginSuccess.json',
    'appuser/mystuff.jsp':      'appuserMystuff.json',
    'join/create.jsp':          'joinCreateSuccess.json',
    'zapp/detail.jsp':          'zappDetail.json',
    'zapp/list.jsp?browse':     'zappListBrowse.json',
    'zapp/list.jsp?featured':   'zappListFeatured.json',
    'zapp/commentsratings.jsp': 'zappCommentsRatings.json'
}

// FormToObject = function(form) // TODO acavan - move to testbench proxy?
// {
//     var o = {};
//     if (form.serializeArray == undefined)
//     {
//         Log.error('FormToObject - serializeArray:undefined');
//         return o;
//     }
//     var a = form.serializeArray();
//     jQuery.each(a, function() {
//         if (o[this.name]) {
//             if (!o[this.name].push) {
//                 o[this.name] = [o[this.name]];
//             }
//             o[this.name].push(this.value || '');
//         } else {
//             o[this.name] = this.value || '';
//         }
//     });
//     return o;
// };

Require = function (o, properties, caller) {
    if (o == undefined)
    {
        Log.alert('Require - failed with o:undefined');
        return false;
    }
    for (var i = 0; i < properties.length; i++)
    {
        if (o[properties[i]] == undefined)
        {
            Log.alert('Require - failed for property:'+ properties[i] + (caller ? ' (caller: '+caller+')' : ''));
            return false;
        }
    }
    return true;
}

ObjectToHTML = function(objectNameStringIndentSpaceDelimiter)
{
    // given an object make a pleasant to read html string
    // NOTE - this was written for use with objects created via json
    // NOTE - no test for recursion loop - use at your own peril
    if (objectNameStringIndentSpaceDelimiter.object == undefined)
        return '';
        
    var object = objectNameStringIndentSpaceDelimiter.object;
    var name = objectNameStringIndentSpaceDelimiter.name;
    var string = objectNameStringIndentSpaceDelimiter.string;
    var indent = objectNameStringIndentSpaceDelimiter.indent;
    var delimiter = objectNameStringIndentSpaceDelimiter.delimiter;
    var space = objectNameStringIndentSpaceDelimiter.space;

    if (!string) {string = '';}
    if (!indent) {indent = '';}
    if (!delimiter) {delimiter = '<br \/>';}
    if (!space) {space = '&nbsp;';}
    if (name)
    {
        string += indent + name + ':' + delimiter;
        indent += space + space;
    }
    function isKindOfArray(object)
    {
        if (object instanceof Array)
            return true;
        return isFunkyArray(object);
    }
    function isFunkyArray(object)
    {
        try
        {
            return object.$family.name == 'array';
        }
        catch (e)
        {
            return false;
        }
    }
    function process(object, index)
    {
        if (object[index] != null && typeof object[index] == 'object')
            string = ObjectToHTML({object: object[index], name: index, string: string, indent: indent, space: space, delimiter: delimiter});
        else
        {
            if (object[index] != null)
                string += indent + index + ': ' + object[index].toString().replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;') + delimiter;
            else
                string += indent + index + ': null' + delimiter;
        }
    }

    if (isKindOfArray(object))
    {
        for (var i = 0; i < object.length; i++)
            process(object, i);
    }
    else
    {
        var elemArray = [];

        for (var j in object)
            elemArray.push(j);
        elemArray.sort();
        for (j=0; j < elemArray.length; j++)
            process(object, elemArray[j]);
    }
    return string;
}

Log = {
	debugLoggingEnabled: false,
	warnLoggingEnabled: true,
    displayAlertOnError: false,
    infoLoggingEnabled: true, // for really useful info that should still be shown after all //Log.debug are commented out by a global replace...
    timestampEnabled: true,
    levelFuncs: {},
    alert: function (message) {
        this.write('alert', message);
        alert(message);
    },
    debug: function (message) {
        if (this.debugLoggingEnabled)
            this.write('debug', message);
    },
    error: function (message) {
        this.write('error', message);
        if (this.displayAlertOnError)
            alert(message);
    },
    warn: function (message) {
        if (this.warnLoggingEnabled)
        	this.write('warn', message);
    },
    info: function (message) {
        if (this.infoLoggingEnabled)
            this.write('info', message);
    },
    write: function (mode, message) {
    	if(this.timestampEnabled) {
    		Log.levelFuncs[mode](message, this.timestamp());
    	} else {
    		Log.levelFuncs[mode](message);
    	}
    },
    timestamp: function () {
        // formatted as hh:mm:ss.000
        var d = new Date();
        var fixedWidth = function (value, width) {
            var s = '' + value;
            while (s.length < width)
                s = '0' + s;
            return s;
        }
        return  fixedWidth(d.getHours(), 2) + ':' + fixedWidth(d.getMinutes(), 2) + ':' + fixedWidth(d.getSeconds(), 2) + '.' + fixedWidth(d.getMilliseconds(), 3);
    }
}
if(true && window.console && (typeof window.console.log === "function")) {
	jQuery.each(["alert", "debug", "info", "warn", "error"], function(index, value) {
		if(typeof window.console[value] === 'function') {
			Log.levelFuncs[value] = function(s, time) {
				if(Common.logging)
				{
					if(jQuery.browser.msie && value == "error")
					{
						window.console["warn"](s);
					}else{
						window.console[value](s); 
					};
				};
			};
		} else {
			Log.levelFuncs[value] = function(s, time) { 
				if(Common.logging)
				{
					s = '['+value+(time ? ' '+time : '')+']  ' + s; 
					window.console.log('[' + value + ']' + s); 
				}
			};
		}
	});
} else {
	jQuery.each(["alert", "debug", "info", "warn", "error"], function(index, value) {
		Log.levelFuncs[value] = function(s, time) { 
			if(Common.logging)
			{
				// if no console write to the debug area on the page
		        // put the incoming message at the top
		        // TODO acavan - exercise this...
				s = '['+value+(time ? ' '+time : '')+']  ' + s; 
		    	jQuery('#debug pre').html(s.replace(/</,'&lt;').replace(/>/,'&gt;') + '<br>'+jQuery('#debug pre').html());
			}	
		};
	});
}

Common.parseDate = function (input)
{
	var parts = input.match(/(\d+)/g);
	  // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
	return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
};


/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */
var formatDate = function ()
{
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = formatDate;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
};

// Some common format strings
formatDate.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	zebraDateTime:	"mm-dd-yyyy' 'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
formatDate.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	var tempFunc = formatDate();
	return tempFunc(this, mask, utc);
};

var AIUtils = {
    min: function(a, b)
    {
        return (a < b) ? a : b;
    },
    max: function(a, b)
    {
        return (a > b) ? a : b;
    },
    formatPrice: function(pennies)
    {
        if (pennies == 0)
        {
            return 'FREE';
        }
        else
        {
        	var ret = new Number(pennies / 100).toFixed(2);
        	ret = '$' + ret + ' USD'
            return ret;
        }
    },
    formatCurrency: function (num) {
    	num = num.toString().replace(/\$|\,/g,'');
    	if(isNaN(num))
    	num = "0";
    	sign = (num == (num = Math.abs(num)));
    	num = Math.floor(num*100+0.50000000001);
    	cents = num%100;
    	num = Math.floor(num/100).toString();
    	if(cents<10)
    	cents = "0" + cents;
    	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    	num = num.substring(0,num.length-(4*i+3))+','+
    	num.substring(num.length-(4*i+3));
    	return (((sign)?'':'-') + '$' + num + '.' + cents);
    },
    currencyToPennies: function(s) {
    	var num = s.toString().replace(/\$|\,/g,'');
    	if(isNaN(num)){
    		num = "0";
    	}
    	num = num*100;
    	return num;
    },
    penniesToDollars: function(s) {
    	var num = s.toString().replace(/\$|\,/g,'');
    	if(isNaN(num)){
    		num = "0";
    	}
    	num = num/100;
    	return num;
    }
};

function debugBreakpoint(argument) {
    Log.debug('debug breakpoint' + typeof argument);
}

function parseQueryString(input) {

    var index = input.indexOf('?');
    if (index > 0) {
        input = input.substring(index + 1);
    }

    var elements = input.split('&');
    var ii;
    var retVal = {};

    for (ii = 0; ii < elements.length; ii++) {
        var duple = elements[ii].split('=');
        retVal[duple[0]] = duple[1];
    }
    return retVal;
}

function getAgoString(secondsAgo) {

    secondsAgo = Math.floor(secondsAgo);

    if (secondsAgo == 0) {
        return '0 seconds ago';
    }
    if (secondsAgo == 1) {
        return '1 second ago';
    }
    if (secondsAgo < 60) {
        return secondsAgo + ' seconds ago';
    }
    var     minutesAgo = Math.floor(secondsAgo / 60);
    if (minutesAgo <= 1) {
        return '1 minute ago';
    }
    if (minutesAgo < 60) {
        return minutesAgo + ' minutes ago';
    }
    var hoursAgo = Math.floor(minutesAgo / 60);
    if (hoursAgo <= 1) {
        return '1 hour ago';
    }
    if (hoursAgo < 60) {
        return hoursAgo + ' hours ago';
    }
    var daysAgo = Math.floor(hoursAgo / 24);
    if (daysAgo <= 1) {
        return '1 day ago';
    }
    if (daysAgo < 31) {
        return daysAgo + ' days ago';
    }
    var monthsAgo = Math.floor(daysAgo / 30.5);
    if (monthsAgo <= 1) {
        return '1 month ago';
    }
    if (monthsAgo < 12) {
        return monthsAgo + ' months ago';
    }
    var yearsAgo = Math.floor(daysAgo / 365);
    if (yearsAgo <= 1) {
        return '1 year ago';
    }
    return yearsAgo + ' years ago';
}

ChangeTag = {

    toText: function(element, onChar13) {
        var newElement = jQuery('<input type="text" value="Password" onfocus="if(this.value==\'Password\')this.value=\'\';" style="margin-top: 10px;" class="textfield" name="password">');
        element.after(newElement);
        element.remove();
        newElement.keypress(function(event2){
            ChangeTag.toPassword(this, event2, onChar13);
        });
    },

    toPassword: function(that, event, onChar13) {
        var     jElement = jQuery(that);

        if (jElement.attr('type') == 'text' && jElement.attr('name') == 'password') {
            var newElement = jQuery('<input type="password" class="' + jElement.attr('class') + '" name="password" style="' + jElement.attr('style') + '">');
            jElement.after(newElement)
            jElement.remove();
            newElement.focus().val(String.fromCharCode(event.which)).keypress(function(event2){
                if (event2.which == 13) {
                    onChar13.click();
                    return;
                }
            });
        }

        if (event.which == 13)
        {
           onChar13.click();
        }
    }
};

TypeLookup = {
    singular : function(type) {
        return this[type][0];
    },
    plural : function(type) {
        return this[type][1]
    },
    accountRequired : function(type) {
        return this[type][2]
    },
    'project':  ['Project', 'Projects', 'collector'],
    'zapp':     ['App',    'Apps',    'collector'],
    'gadget':   ['Gadget',  'Gadgets',  'creator']
}

/*
 * When loading details page, we pass in an "id", which can
 * in reality be a projectId, a urlName, or an AccountProjectNumbers.accountProjectNumberId
 */
IdType = {
		URL_NAME :	"name", // human-readable url name
		PROJECT_ID:	"pid",  // primary key to Projects table
		APN_UUID:	"apn",  // primary key to AccountProjectNumbers record for owner of app/gadget
		PUBLISH_ID: "dep_pubId", // included for backward compatibility
		
		/*
		 * Build a url for the details page, given an ID and an IdType
		 */
		detailUrl:	function(id, type) {
			var base = "/#/detail?";
			var middle = '';
			switch(type) {
			case IdType.URL_NAME:
				middle += IdType.URL_NAME
				break;
			case IdType.PROJECT_ID:
				middle += IdType.PROJECT_ID;
				break;
			case IdType.APN_UUID:
				middle += IdType.APN_UUID;
				break;
			case IdType.PUBLISH_ID:
				middle += IdType.PUBLISH_ID;
				break;
			default:
				Log.error("bad IdType: " + type);
			}
			
			return base + middle + "&" + id;
		},
		
		/*
		 * Translate a details page URL into ID and IdType
		 */
		menuArgsToIdAndType:	function(menuArgs) {
	    	// split menu argument into "id" and "type"
	    	// if split fails... assume that it's urlname
	    	var args = menuArgs.split("&");
	    	var id = '';
	    	var type = '';
	    	if (args.length > 1) {
	    		type = args[0];
	    		id   = args[1];
	    	} else {
	    		if (!isNaN(parseInt(args[0])) && parseInt(args[0]) >= 8002 && parseInt(args[0]) <= 12470) {
	    			type = IdType.PUBLISH_ID;
	    		} else {
	    			type = IdType.URL_NAME;
	    		}
	    		id   = args[0];
	    	}
			return {id: id, type: type};
		}
}

PermissionsPrice = {
		HIDE_IN_STORE_LIST: 'hideInStoreList',
		INVITE_ONLY: 'inviteOnly',
		PRICE: 'price',
		LICENSE_ID: 'licenseTypeId',
		EMBED: 'embed',
		EDIT: 'edit',
		REPUBLISH: 'republish'
}

function getClassByName(name) {
	return this[name];
}

function makeSafeString(inputString) {
	var returnString = inputString;
	
	returnString = jQuery('<div/>').text(inputString).html();
	
	return returnString;
}

if (typeof jQuery == 'undefined')
    Log.alert('jQuery is required but was not found')
else
    jQuery.noConflict();
