var ApplicationFacade = function()
{
    this.Extends = new Class(new Facade());
    this.appInstance = null;

    this.startup = function( app/*Object*/ )
    {
        this.sendNotification( ApplicationFacade.STARTUP, app );    
    }
    this.initializeController = function() 
    {
        // always call this.parent()
        this.parent();

        this.registerCommand( ApplicationFacade.STARTUP, StartupCommand );
        this.registerCommand( ApplicationFacade.REQUEST_PURCHASE, RequestPurchaseCommand );
        this.registerCommand( ApplicationFacade.LOAD_UPGRADE, LoadUpgradeCommand );
        this.registerCommand( ApplicationFacade.LAUNCH_EDITOR, LaunchEditorCommand );
        this.registerCommand( ApplicationFacade.SHOW_DIALOG, LoadDialogueViewCommand );
        this.registerCommand( ApplicationFacade.CONFIRM_DIALOG, ConfirmDialogCommand );
        this.registerCommand( ApplicationFacade.SHOW_ACCOUNT_DUE_DIALOG, ShowAccountDueCommand );
        this.registerCommand( ApplicationFacade.CHECK_ACCOUNT_STANDING, CheckAccountStandingCommand );
    }
}
ApplicationFacade.getInstance = function()/*ApplicationFacade*/
{
    if (!this.appInstance)
    {
        var classFactory = new Class(new ApplicationFacade());
        this.appInstance = new classFactory();
        // Replace the Facade instance with the application instance.
        Facade.instance = this.appInstance;
    }
    return Facade.instance;
}
ApplicationFacade.loadedViews = {};
ApplicationFacade.STARTUP = "ApplicationFacade.STARTUP";
ApplicationFacade.REQUEST_PURCHASE = "ApplicationFacade.REQUEST_PURCHASE";
ApplicationFacade.LOAD_UPGRADE = "ApplicationFacade.LOAD_UPGRADE";
ApplicationFacade.LOAD_UPGRADE_COMPLETE = "ApplicationFacade.LOAD_UPGRADE_COMPLETE";
ApplicationFacade.LAUNCH_EDITOR = "ApplicationFacade.LAUNCH_EDITOR";
ApplicationFacade.CONFIRM_DIALOG = "ApplicationFacade.CONFIRM_DIALOG";
ApplicationFacade.CHECK_ACCOUNT_STANDING = "ApplicationFacade.CHECK_ACCOUNT_STANDING";

ApplicationFacade.SHOW_ACCOUNT_DUE_DIALOG = "ApplicationFacade.SHOW_ACCOUNT_DUE_DIALOG";

ApplicationFacade.SHOW_DIALOG = "ApplicationFacade.SHOW_DIALOG";
ApplicationFacade.CLOSE_DIALOG = "ApplicationFacade.CLOSE_DIALOG";
ApplicationFacade.CLOSE_ALL_DIALOGS = "ApplicationFacade.CLOSE__ALL_DIALOGS";
ApplicationFacade.CLOSING_DIALOG = "ApplicationFacade.CLOSING_DIALOG";
ApplicationFacade.CLOSING_DIALOG_BY_USER = "ApplicationFacade.CLOSING_DIALOG_BY_USER";
ApplicationFacade.CONFIG_DIALOG = "ApplicationFacade.CONFIG_DIALOG";
ApplicationFacade.CENTER_DIALOG = "ApplicationFacade.CENTER_DIALOG";
ApplicationFacade.RETITLE_DIALOG = "ApplicationFacade.RETITLE_DIALOG";
ApplicationFacade.SHOW_CLOSE_DIALOG = "ApplicationFacade.SHOW_CLOSE_DIALOG";
ApplicationFacade.UPDATE_PROJECT_ID = "ApplicationFacade.UPDATE_PROJECT_ID";
ApplicationFacade.UPDATE_CREDENTIALS = "ApplicationFacade.UPDATE_CREDENTIALS";
