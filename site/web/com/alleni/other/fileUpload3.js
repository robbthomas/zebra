// Synchronous if callback is not provided (not recommended)
function XHRrequest(url, auth, method, data, callback, ignore404, extraHeaders) {
    var xhr,
        finished = false,
        xDomainRequest = false,
        ieXDomain = false,
        ieModeRequest,
        title,
        ticks = ['/', '-', '\\', '|'],
        location = window.location,
        urlParts,
        urlPort,
        result,
        until
    ;

    urlParts = url.toLowerCase().match(/^(.+):\/\/([^:\/]*):?(\d+)?(\/.*)?$/);

    //Consolidate headers
    var headers = {};
    headers["Content-Type"] = "application/json";
    headers["Authorization"] = auth;
    if (extraHeaders !== null) {
        for (var headerName in extraHeaders) {
            headers[headerName] = extraHeaders[headerName];
        }
    }

    //See if this really is a cross domain
    xDomainRequest = (location.protocol.toLowerCase() !== urlParts[1] || location.hostname.toLowerCase() !== urlParts[2]);
    if (! xDomainRequest) {
        urlPort = (urlParts[3] === null ? ( urlParts[1] === 'http' ? '80' : '443') : urlParts[3]);
        xDomainRequest = (urlPort === location.port);
    }

    //If it's not cross domain or we're not using IE, use the usual XmlHttpRequest
    if (! xDomainRequest || typeof XDomainRequest === 'undefined') {
        xhr = new XMLHttpRequest();
        xhr.open(method, url, callback != null);
        for (var headerName in headers) {
            xhr.setRequestHeader(headerName, headers[headerName]);
        }
    }
    //Otherwise, use IE's XDomainRequest object
    else {
        ieXDomain = true;
        ieModeRequest = getIEModeRequest(method, url, headers, data);
        xhr = new XDomainRequest ();
        xhr.open(ieModeRequest.method, ieModeRequest.url);
    }

    //Setup request callback
    function requestComplete () {
        var notFoundOk;

        if (!finished) {
            // may be in sync or async mode, using XMLHttpRequest or IE XDomainRequest, onreadystatechange or
            // onload or both might fire depending upon browser, just covering all bases with event hooks and
            // using 'finished' flag to avoid triggering events multiple times
            finished = true;

            notFoundOk = (ignore404 && xhr.status === 404);
            if (xhr.status === undefined || (xhr.status >= 200 && xhr.status < 400) || notFoundOk) {
                if (callback) {
                    var responseObj = {"xhr":xhr, "url":url};
                    callback(responseObj);
                }
                else {
                    result = xhr;
                    return xhr;
                }
            }
            else {
                var responseObj = {"xhr":xhr, "url":url};
                callback(responseObj);
                return xhr;
            }
        }
        else {
            return result;
        }
    };

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            requestComplete();
        }
    };

    xhr.onload = requestComplete;
    xhr.onerror = requestComplete;

    xhr.send(ieXDomain ? ieModeRequest.data : data);

    if (! callback) {
        // synchronous
        if (ieXDomain) {
            // synchronous call in IE, with no synchronous mode available
            until = 1000 + Date.now();

            while (Date.now() < until && ! finished) {
                __delay();
            }
        }
        return requestComplete();
    }
}


// request for IE
function getIEModeRequest (method, url, headers, data) {
    var newUrl = url,
        //Everything that was on query string goes into form vars
        formData = [],
        qsIndex = newUrl.indexOf('?'),
        result
    ;
    if (qsIndex > 0) {
        formData.push(newUrl.substr(qsIndex + 1));
        newUrl = newUrl.substr(0, qsIndex);
    }

    //Method has to go on querystring, and nothing else
    newUrl = newUrl + '?method=' + method;

    //Headers
    if (headers !== null) {
        for (var headerName in headers) {
            formData.push(headerName + "=" + encodeURIComponent(headers[headerName]));
        }
    }

    //The original data is repackaged as "content" form var
    if (data !== null) {
        formData.push('content=' + encodeURIComponent(data));
    }

    result = {
        method: "POST",
        url: newUrl,
        headers: {},
        data: formData.join("&")
    };

    return result;
}
