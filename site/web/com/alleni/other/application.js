Application = {
    ACCOUNT_CREATE_SUCCESS: 'Application.ACCOUNT_CREATE_SUCCESS',
    DISPLAY_ITEM_DETAIL: 'Application.DISPLAY_ITEM_DETAIL',
    HREF_DISPATCH: 'hrefDispatch_',
    MENU_MODE_MEMBER: 'Application.MENU_MODE_MEMBER',
    MENU_MODE_VISITOR: 'Application.MENU_MODE_VISITOR',
    PLAN_SELECTED: 'Application.PLAN_SELECTED',
    REFRESH: 'Application.REFRESH',
    HASHCHANGED: 'Application.HASHCHANGED',
    upgradeSystemLoaded: false,
    config: {
        paths: {
            components:  './com/alleni/views/components',
            fragments:   './com/alleni/html/fragments',
            images:      './com/alleni/images',
            json:        './com/alleni/json',
            mediators:   './com/alleni/views/mediators',
            mockdata:    './mockdata',
            models:      './com/alleni/models',
            tests:       './com/alleni/html/tests',
            webservices: {
                site: './services',
                zephyr: '/zephyr'
            } 
        }
    },
    scripts: {
        AboutZebraPanel: {
            path: 'components',
            filename: 'aboutZebraPanel.js',
            loaded: false
        },
        AboutZebraPanelMediator: {
            path: 'mediators',
            filename: 'aboutZebraPanelMediator.js',
            loaded: false
        },
        AccountPanel: {
            path: 'components',
            filename: 'accountPanel.js',
            loaded: false
        },
        AccountPanelMediator: {
            path: 'mediators',
            filename: 'accountPanelMediator.js',
            loaded: false
        },
        AuthorProfilePanel: {
        	path: 'components',
        	filename: 'authorProfilePanel.js',
        	loaded:false
        },
        AuthorProfilePanelMediator: {
        	path: 'mediators',
        	filename: 'authorProfilePanelMediator.js',
        	loaded: false
        },
        BaseMediator: {
        	path: 'mediators',
        	filename: 'baseMediator.js',
        	loaded: false
        },
        BaseProxy: {
        	path: 'models',
        	filename: 'baseProxy.js',
        	loaded: false
        },
        StartTransfer: {
            path: 'components',
            filename: 'startTransfer.js',
            loaded: false
        },
        StartTransferMediator: {
            path: 'mediators',
            filename: 'startTransferMediator.js',
            loaded: false
        },
        BrowsePanel: {
            path: 'components',
            filename: 'browsePanel.js',
            loaded: false
        },
        BrowsePanelMediator: {
        	path: 'mediators',
        	filename: 'browsePanelMediator.js',
        	loaded: false
        },
        DetailPanel: {
            path: 'components',
            filename: 'detailPanel.js',
            loaded: false
        },
        DetailPanelMediator: {
            path: 'mediators',
            filename: 'detailPanelMediator.js',
            loaded: false
        },
        FeaturedPanel: {
            path: 'components',
            filename: 'featuredPanel.js',
            loaded: false
        },
        FeaturedPanelMediator: {
        	path: 'mediators',
        	filename: 'featuredPanelMediator.js',
        	loaded: false
        },
        FeedbackProxy: {
            path: 'models',
            filename: 'feedbackProxy.js',
            loaded: false
        },
        ListPanel: {
            path: 'components',
            filename: 'listPanel.js',
            loaded: false
        },
        ListPanelMediator: {
            path: 'mediators',
            filename: 'listPanelMediator.js',
            loaded: false
        },
        ListTabsPanelMediator: {
        	path: 'mediators',
        	filename: 'listTabsPanelMediator.js',
        	loaded: false
        },
        JoinPanel: {
            path: 'components',
            filename: 'joinPanel.js',
            loaded: false
        },
        JoinPanelMediator: {
            path: 'mediators',
            filename: 'joinPanelMediator.js',
            loaded: false
        },
        JoinProxy: {
            path: 'models',
            filename: 'joinProxy.js',
            loaded: false
        },
        PlansAndPricingPanel: {
            path: 'components',
            filename: 'plansAndPricingPanel.js',
            loaded: false
        },
        PlansAndPricingPanelMediator: {
            path: 'mediators',
            filename: 'plansAndPricingPanelMediator.js',
            loaded: false
        },        
        ListTabsPanel: {
            path: 'components',
            filename: 'listTabsPanel.js',
            loaded: false
        },
        SupportPanel: {
            path: 'components',
            filename: 'supportPanel.js',
            loaded: false
        },
        SupportPanelMediator: {
            path: 'mediators',
            filename: 'supportPanelMediator.js',
            loaded: false
        },
        TestbenchJSONPanel: {
            path: 'components',
            filename: 'testbenchJSONPanel.js',
            loaded: false
        },
        TestbenchPanel: {
            path: 'components',
            filename: 'testbenchPanel.js',
            loaded: false
        },
        TestbenchProxy: {
            path: 'models',
            filename: 'testbenchProxy.js',
            loaded: false
        },
        TestbenchPanelMediator: {
            path: 'mediators',
            filename: 'testbenchPanelMediator.js',
            loaded: false
        },
        TestbenchTestForm: {
            path: 'components',
            filename: 'testbenchTestForm.js',
            loaded: false
        },
        TestbenchTestsPanel: {
            path: 'components',
            filename: 'testbenchTestsPanel.js',
            loaded: false
        },
        ZappGadgetProject: {
            path: './com/alleni/other',
            filename: 'zappGadgetProject.js',
            loaded: false
        },
        ZappShoppePanel: {
            path: 'components',
            filename: 'zappShoppePanel.js',
            loaded: false
        }
    },    
    contentPanels: {
        aboutZebra: {
            className: 'aboutZebra',
            displayName: 'About Zebra',
            divId: 'aboutZebraPanel',
            mediator: null,
            menu: {
                name: 'About Zebra',
                href: '#/aboutZebra',
                image: 'nav_about.png',
                imageHover: 'nav_about_over.png',
                imageSelected: 'nav_about_down.png'
            },
            panel: null,
            scripts: ['AboutZebraPanel', 'AboutZebraPanelMediator'],
            scriptsLoading: false,
            scriptsLoaded: false,
            onScriptsLoaded: function () {
                this.panel = new AboutZebraPanel();
                this.mediator = new AboutZebraPanelMediator(this.panel);
                ApplicationFacade.getInstance().registerMediator(this.mediator);
            }
        },
        account: {
            className: 'account',
            displayName: 'Account',
            divId: 'accountPanel',
            mediator: null,
            panel: null,
            scripts: ['AccountPanel', 'AccountPanelMediator', 'StartTransfer', 'StartTransferMediator'],
            scriptsLoading: false,
            scriptsLoaded: false,
            onScriptsLoaded: function () {
                this.panel = new AccountPanel();
                this.mediator = new AccountPanelMediator(this.panel);
                ApplicationFacade.getInstance().registerMediator(this.mediator);
            }
        },
        authorProfile: {
            className: 'authorProfile',
            displayName: 'Author Profile',
            divId: 'authorProfile',
            mediator: null,
            panel: null,
            scripts: [
                   'AuthorProfilePanel'
                 , 'AuthorProfilePanelMediator'
                 , 'BrowsePanel'
                 , 'BrowsePanelMediator'
                 , 'ListTabsPanel'
                 , 'ListTabsPanelMediator'
                 , 'FeedbackProxy'
            ],
            scriptsLoading: false,
            scriptsLoaded: false,
            onScriptsLoaded: function () {
            	Log.warn('assemble authorProfile view/mediator pair');
                this.panel = new AuthorProfilePanel('authorProfile');
                this.mediator = new AuthorProfilePanelMediator(undefined, this.panel);
                Log.warn('assemble authorProfile view/mediator pair done');
                ApplicationFacade.getInstance().registerMediator(this.mediator);
                Log.warn('assemble authorProfile view/mediator register with framework');
            }
        },
        detail: {
            className: 'detail',
            displayName: 'Detail', // should this be Zapp or Gadget?
            divId: 'detailPanel',
            mediator: null,
            panel: null,
            scripts: ['DetailPanel', 'DetailPanelMediator', 'FeedbackProxy', 'ZappGadgetProject'],
            scriptsLoading: false,
            scriptsLoaded: false,
            onScriptsLoaded: function () {
                this.panel = new DetailPanel();
                this.mediator = new DetailPanelMediator(this.panel);
                ApplicationFacade.getInstance().registerMediator(this.mediator);
                if (!ApplicationFacade.getInstance().retrieveProxy(FeedbackProxy.NAME))
                    ApplicationFacade.getInstance().registerProxy(new FeedbackProxy());
               
            }
        },
        join: {
            className: 'join',
            displayName: 'Join',
            divId: 'joinPanel',
            mediator: null,
            panel: null,
            scripts: ['JoinPanel', 'JoinPanelMediator', 'JoinProxy'],
            scriptsLoading: false,
            scriptsLoaded: false,
            onScriptsLoaded: function () {
                this.panel = new JoinPanel();
                this.mediator = new JoinPanelMediator(this.panel);
                ApplicationFacade.getInstance().registerMediator(this.mediator);
                if (!ApplicationFacade.getInstance().retrieveProxy(JoinProxy.NAME))
                    ApplicationFacade.getInstance().registerProxy(new JoinProxy());
            }
        },
        list: {
            className: 'list contentRight',
            displayName: 'Shopps',
            divId: 'listPanel',
            mediator: null,
            menu: {
                name: 'Shopps',
                href: '#/list?visitor&zapp',
                image: 'nav_shop.png',
                imageHover: 'nav_shop_over.png',
                imageSelected: 'nav_shop_down.png'
            },
            panel: null,
            scripts: [
                'BrowsePanel',
                'BrowsePanelMediator',
                'FeaturedPanel',
                'FeaturedPanelMediator',
                'ListPanel',
                'ListPanelMediator',
                'ListTabsPanel',
                'ListTabsPanelMediator',
                'ZappGadgetProject',
                'ZappShoppePanel', 'FeedbackProxy'
            ],
            scriptsLoading: false,
            scriptsLoaded: false,
            onScriptsLoaded: function () {
            	Log.warn('assemble listPanel view/mediator pair');
                this.panel = new ListPanel('listPanel');
                this.mediator = new ListPanelMediator('listPanel',this.panel);
                Log.warn('assemble listPanel view/mediator pair done');
                ApplicationFacade.getInstance().registerMediator(this.mediator);
                Log.warn('register ListPanelMediator');
            }
        },
        my: {
            className: 'list contentRight',
            displayName: 'My Collections',
            divId: 'myPanel',
            mediator: null,
            menu: null,
            panel: null,
            scripts: [
                'BrowsePanel',
                'BrowsePanelMediator',
                'FeaturedPanel',
                'FeaturedPanelMediator',
                'ListPanel',
                'ListPanelMediator',
                'ListTabsPanel',
                'ListTabsPanelMediator',
                'ZappGadgetProject',
                'ZappShoppePanel', 'FeedbackProxy'
            ],
            scriptsLoading: false,
            scriptsLoaded: false,
            onScriptsLoaded: function () {
            	Log.warn('assemble myPanel view/mediator pair');
                this.panel = new ListPanel('myPanel');
                this.mediator = new ListPanelMediator('myPanel',this.panel);
                Log.warn('assemble listPanel view/mediator pair done');
                ApplicationFacade.getInstance().registerMediator(this.mediator);
                Log.warn('register ListPanelMediator');
            }
        },
        plansAndPricing: {
            className: 'plansAndPricing',
            displayName: 'Plans & Pricing',
            divId: 'plansAndPricingPanel',
            mediator: null,
            menu: {
                name: 'Plans & Pricing',
                href: '#/plansAndPricing',
                image: 'nav_membership.png',
                imageHover: 'nav_membership_over.png',
                imageSelected: 'nav_membership_down.png'
            },
            panel: null,
            scripts: ['PlansAndPricingPanel', 'PlansAndPricingPanelMediator', 'JoinProxy'],
            scriptsLoading: false,
            scriptsLoaded: false,
            onScriptsLoaded: function () {
                this.panel = new PlansAndPricingPanel();
                this.mediator = new PlansAndPricingPanelMediator(this.panel);
                ApplicationFacade.getInstance().registerMediator(this.mediator);
                if (!ApplicationFacade.getInstance().retrieveProxy(JoinProxy.NAME))
                    ApplicationFacade.getInstance().registerProxy(new JoinProxy());
            }
        },
        report: {
        	className: 'report',
        	displayName: 'Transaction Reporting',
        	divId: 'reportPanel',
        	mediator: null,
        	menu: null,
        	panel: null,
        	scripts: [
        	     'ReportPanel',
        	     'ReportPanelMediator',
        	     'ReportProxy'
        	],
        	scriptsLoading: false,
        	scriptsLoaded: false,
        	onScriptsLoaded: function () {
        		this.panel = new ReportPanel();
        		this.mediator = new ReportPanelMediator(this.panel);
        		ApplicationFacade.getInstance().registerMediator(this.mediator);
        		if (!ApplicationFacade.getInstance().retrieveProxy(ReportProxy))
        			ApplicationFacade.getInstance().registerProxy(new ReportProxy());
        	}
        },
        support: {
            className: 'support',
            displayName: 'Support',
            divId: 'supportPanel',
            mediator: null,
            menu: {
                name: 'Support',
                href: '#/support',
                image: 'nav_support.png',
                imageHover: 'nav_support_over.png',
                imageSelected: 'nav_support_down.png'
            },
            panel: null,
            scripts: ['SupportPanel', 'SupportPanelMediator'],
            scriptsLoading: false,
            scriptsLoaded: false,
            onScriptsLoaded: function () {
                this.panel = new SupportPanel();
                this.mediator = new SupportPanelMediator(this.panel);
                ApplicationFacade.getInstance().registerMediator(this.mediator);
                // if (!ApplicationFacade.getInstance().retrieveProxy(SupportProxy.NAME))
                //     ApplicationFacade.getInstance().registerProxy(new SupportProxy());
            }
        },
        testbench: {
            className: 'testbench',
            displayName: 'Testbench',
            divId: 'testbenchPanel',
            mediator: null,
            menu: {
                name: 'Testbench',
                href: '#/testbench',
                image: 'magnifiyingGlass.png',
                imageHover: 'magnifiyingGlass.png'
            },
            panel: null,
            scripts: [
                'TestbenchPanel',
                'TestbenchPanelMediator',
                'TestbenchJSONPanel',
                'TestbenchTestForm',
                'TestbenchTestsPanel',
                'TestbenchProxy'
            ],
            scriptsLoading: false,
            scritpsLoaded: false,
            onScriptsLoaded: function () {
                this.panel = new TestbenchPanel();
                this.mediator = new TestbenchPanelMediator(this.panel);
                ApplicationFacade.getInstance().registerMediator(this.mediator);
                if (!ApplicationFacade.getInstance().retrieveProxy(TestbenchProxy.NAME))
                    ApplicationFacade.getInstance().registerProxy(new TestbenchProxy());
            }
        }
    },
    configureMenu: function (type) {
        var items = {};
        var results = [];
        items[Application.MENU_MODE_VISITOR] = ['list', 'support', 'plansAndPricing', 'aboutZebra'];
        items[Application.MENU_MODE_MEMBER] = ['list', 'support', 'aboutZebra'];

        if (window.location.hash.indexOf('testbench') != -1) {
            items[type].push('testbench');
        }
        for (var i = 0; i < items[type].length; i++)
        {
            var id = items[type][i];
            var o = {
                id: id,
                href: this.contentPanels[id].menu.href,
                image: this.config.paths.images + '/' + this.contentPanels[id].menu.image,
                imageHover: this.config.paths.images + '/' + this.contentPanels[id].menu.imageHover,
                imageSelected: this.config.paths.images + '/' + this.contentPanels[id].menu.imageSelected,
                name: this.contentPanels[id].menu.name,
                internal: true
            }
            results.push(o);
        }
        // add in custom external links
        results.push({
        	id: undefined,
            href: "http://blog.zebrazapps.com",
            image: this.config.paths.images + '/nav_blog.jpg',
            imageHover: this.config.paths.images + '/nav_blog_over.jpg',
            imageSelected: this.config.paths.images + '/nav_blog_down.jpg',
            name: "blog",
            internal: false
        });
        return results;
    },
    configureLinks: function (type, callback) {
        var items = {};
        items[Application.MENU_MODE_VISITOR] = [
            // {text: 'blog', href: 'http://www.zebrabeta.com'},
            // {text: 'twitter', href: 'http://www.twitter.com'},
            {text: 'sign in', href: '#signIn'},
            // {text: 'sign up free', href: '#signUpFree'},
            // {text: 'sign up paid', href: '#signUpPaid'}
        ];
        items[Application.MENU_MODE_MEMBER] = [
            // {text: 'username', href: '#'},
            // {text: 'account', href: '#account'},
            // {text: 'sign out', href: '#signOut'}
        ];
        for (var i = 0; i < items[type].length; i++)
        {
            callback(items[type][i]);
        }
    },
    loadScripts: function (id, callback) {
        //Log.debug('Application.loadScripts - id:'+id);
        var contentPanel = this.contentPanels[id];
        var unloadedScripts = this.unloadedScripts(contentPanel.scripts);
        if (contentPanel === undefined)
        {
            alert('Application.loadScripts - contentPanel: undefined for id: '+id);
            return;
        }
        if (contentPanel.scriptsLoaded)
            return;
        
        
        if(unloadedScripts.length == 0){
        	contentPanel.scriptsLoading = false;
            contentPanel.scriptsLoaded = true;
            jQuery('#contentPanel').append(jQuery('<div id="'+contentPanel.divId+'" class="'+contentPanel.className+'"></div>')) // create this div so PureMVC doesn't create a malformed element (E.g., <aboutzebrapanel>)
        	contentPanel.onScriptsLoaded();
        	callback();
        	return;
        }
        
        var that = this;
        
        contentPanel.loadingCallback = callback;
        
        if(contentPanel.scriptsLoading) {
        	Log.warn("loading an already loading script " + id);
        	return;
        }
        contentPanel.scriptsLoading = true;
        
        
        
        var scriptLoader = new ScriptLoader(id, unloadedScripts, function () {
            contentPanel.scriptsLoading = false;
            contentPanel.scriptsLoaded = true;
            for (var i = 0; i < contentPanel.scripts.length; i++) // mark the scripts loaded
                that.scripts[contentPanel.scripts[i]].loaded = true;
            jQuery('#contentPanel').append(jQuery('<div id="'+contentPanel.divId+'" class="'+contentPanel.className+'"></div>')) // create this div so PureMVC doesn't create a malformed element (E.g., <aboutzebrapanel>)
            contentPanel.onScriptsLoaded();
            if(typeof contentPanel.loadingCallback === 'function') {
            	contentPanel.loadingCallback();
            }
            delete contentPanel.loadingCallback;
        })
    },
    unloadedScripts: function (scriptsToLoad) {
        // return an array of pathFilenames for files that have not been loaded
        var a = [];
        if (scriptsToLoad.length == 0)
            Log.error('Application.unloadedScripts - scriptsToLoad.length:0');
        for (var i = 0; i < scriptsToLoad.length; i++)
        {
            if (this.scripts[scriptsToLoad[i]] == undefined)
            {
                Log.error('Application.unloadedScripts - scripts['+scriptsToLoad[i]+'] is undefined');
                return [];
            }
            if (!this.scripts[scriptsToLoad[i]].loaded)
            {
                var script = this.scripts[scriptsToLoad[i]];
                var pathBase = this.config.paths[script.path] != undefined ? this.config.paths[script.path] : script.path;
                pathFilename = pathBase+'/'+script.filename;
                a.push(pathFilename);
            }
        }
        return a;
    }
}
