var DEFAULT_LOCALE = "en-US";

var activityId = "";
var sessionId = "";
var defaultObject = null;

var tcDriver = null;

function TCProtoClient_initialize(args) {
	var tcConfig = args[0];

	var lrsConfig = null;
	if (typeof(tcConfig) !== "undefined"
			&& tcConfig.hasOwnProperty("lrs")
			&& tcConfig.lrs.hasOwnProperty("endpoint")) {
		lrsConfig = tcConfig.lrs;
	}

	if (tcConfig.hasOwnProperty("activityId")) {
		activityId = tcConfig.activityId;
	} else {
		return null;
	}

	//if (tcDriver == null) { // allow for reinitialization
	var pendingDriver = TCDriver_ConfigObject(lrsConfig);
	//}
	TCDriver_GetStatements (pendingDriver, false, null, activityId, function (callbackResponse) {
		tcDriver = pendingDriver;
		
		if (typeof(tcConfig) !== "undefined") {
			sessionId = __ruuid();
			var activityName = "";
			var activityDescription = "";
			var activityType = "interaction";
			var actorName = "";
			var actorEmail = "";

			if (tcConfig.hasOwnProperty("activityName")) {
				activityName = tcConfig.activityName;
			}
			if (tcConfig.hasOwnProperty("activityDescription")) {
				activityDescription = tcConfig.activityDescription;
			}
			if (tcConfig.hasOwnProperty("activityType")) {
				activityType = tcConfig.activityType;
			}
			if (tcConfig.hasOwnProperty("actorName")) {
				actorName = tcConfig.actorName;
			}
			if (tcConfig.hasOwnProperty("actorEmail")) {
				actorEmail = tcConfig.actorEmail;
			}
		}

		var apiVersion = ("lrs" in callbackResponse && "apiVersion" in callbackResponse.lrs) ? callbackResponse.lrs.apiVersion : TCDriver_DEFAULT_API_VER;
		if (apiVersion && apiVersion >= 0.95) {
			activityType = "http://adlnet.gov/expapi/activities/" + activityType;

			tcDriver.actor = {
				"objectType":"Agent",
				"name":actorName,
				"mbox":"mailto:" + actorEmail
			};
		} else {
			// doing this literally for now because the Site apparently uses an older JSON stringify overriding
			// the built-in browser library, and it's messing up the array and confusing the LRS parser
			tcDriver.actor = "{\"name\":[\"" + actorName + "\"],\"mbox\":[\"mailto:" + actorEmail + "\"]}";
		}

		defaultObject = TCProtoClient_getFormattedObject(activityId, activityType, activityName, activityDescription);

		callbackResponse["initialized"] = tcDriver != null && tcConfig != null;
		submitCallback(callbackResponse);
	});

	return tcConfig;
}

function TCProtoClient_getFormattedObject(activityIdVal, typeVal, activityNameVal, activityDescriptionVal) {
	var formattedObject = {
        'id':activityIdVal,
		"definition":{
			"type":typeVal,
			"name":{},
			"description":{}
        }
	};
	formattedObject.definition.name[DEFAULT_LOCALE] = activityNameVal;
	formattedObject.definition.description[DEFAULT_LOCALE] = activityDescriptionVal;
	return formattedObject;
}

function TCProtoClient_getContext(registrationId){
    return {
        "contextActivities":{
            "grouping":{"id":activityId}
        },
        "registration":registrationId
    };
}

function submitCallback(callbackResponse) {
	if (typeof(callbackResponse["xhr"]) !== "undefined") {
		var status = 0;
		if (callbackResponse.xhr.status !== undefined) {
			status = callbackResponse.xhr.status;
		}
		delete callbackResponse.xhr;
		callbackResponse["status"] = status;
	}
	var editorPlayerElement = document.getElementById("editorPlayer");
	if (editorPlayerElement === null) {
		editorPlayerElement = document.getElementById("editor");
	}
	if (editorPlayerElement === null) {
		editorPlayerElement = document.getElementById("player");
	}
	if (editorPlayerElement !== null && typeof editorPlayerElement.handleLRSResponse === "function") {
		try {
			editorPlayerElement.handleLRSResponse(callbackResponse);
		} catch(err) {
			console.error("Callback Error: " + err.message);
		}
	}
}

function TCProtoClient_sendStatementWithContext(statement) {
	if (tcDriver != null) {
		var callbackWrapper = function (callbackResponse) {
			submitCallback(callbackResponse);
		}
		statement["context"] = TCProtoClient_getContext(sessionId);
    	return TCDriver_SendStatement(tcDriver, statement, callbackWrapper);
    }
    return null;
}

function _TCProtoClient_prepareStatement(statementObject) {
	statement = {};
	if (statementObject.hasOwnProperty("verb")) {
		var objectVal;
		if (statementObject.hasOwnProperty("object")) {
			objectVal = statementObject.object;
	    } else {
			if (statementObject.hasOwnProperty("moduleIndex")) {
	    		objectVal = TCProtoClient_getFormattedObject(defaultObject.id + "/" + statementObject.moduleIndex,
	    			defaultObject.definition.type, 
	    			defaultObject.definition.name[DEFAULT_LOCALE] + " module " + statementObject.moduleIndex,
	    			defaultObject.definition.description[DEFAULT_LOCALE]);
			} else {
		    	objectVal = defaultObject;
			}
	    }

		statement["object"] = objectVal;
		if (statementObject.hasOwnProperty("apiVersion") && statementObject.apiVersion >= 0.95) {
			statement["verb"] = {
				"id":"http://adlnet.gov/expapi/verbs/" + statementObject.verb,
				"display":{}
			};
			statement.verb.display[DEFAULT_LOCALE] = statementObject.verb;
		} else {
			statement["verb"] = statementObject.verb;
		}


	    if (statementObject.hasOwnProperty("result")) {
			statement["result"] = statementObject.result;
		}
	}
	return statement;
}

function TCProtoClient_displayStatement(args) {
	return _TCProtoClient_prepareStatement(args);
}

function TCProtoClient_sendArbitraryStatement(args) {
	var argElement = args[0];
	var statement = _TCProtoClient_prepareStatement(argElement);
	if (argElement.hasOwnProperty("apiVersion") && tcDriver !== null) {
		tcDriver["apiVersion"] = argElement.apiVersion;
		delete argElement.apiVersion;
	}
	if (statement !== {}) {
	    return TCProtoClient_sendStatementWithContext(statement);
	}
	return null;
}