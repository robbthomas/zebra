#!/bin/bash
echo "updating index.html cache buster on OSX host"
cb=`svnversion . | awk '{
    start = match($0, ":[0-9]*")
    if (start) {
        print substr($0, start+1, RLENGTH-1)
    } else {
	start = match($0, "[0-9]*")
	if (start) {
	    print substr($0, start, RLENGTH)
	} else {
	    print "0"
	}
    }
}'`
echo new argument is "?cb="$cb
sed -e 's/\?cb=[0-9]*/\?cb='$cb'/g' -i "" ./index.html    # os x bash
sed -e 's/\?cb=[0-9]*/\?cb='$cb'/g' -i "" ./editor.html    # os x bash
sed -e 's/\?cb=[0-9]*/\?cb='$cb'/g' -i "" ./embed/index.html    # os x bash
