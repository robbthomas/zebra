<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="com.alleni.util.Mail" %>
<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));

        JSONObject emailUniqueWrapper = json.getJSONObject("appUserEmailUnique");

        String email = emailUniqueWrapper.getString("email");

        reply.put("email", email);
        reply.put("unique", com.alleni.common.BaseBean.emailUnique(email));

        reply = com.alleni.common.BaseBean.success(reply);
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>