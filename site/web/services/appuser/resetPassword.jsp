<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="com.alleni.util.Mail" %>
<%@ page import="com.alleni.template.ClosureUtil" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));

        JSONObject resetPassword = json.getJSONObject("appUserResetPassword");
        String newPassword = com.alleni.util.RandomPassword.generate(10);
        resetPassword.put("newPassword", newPassword);

        JSONObject credentials = new JSONObject().put("email","pswdAdmin@zebrazapps.com").put("password","o5vBnE5Q22");

        reply = com.alleni.member.AppUser.resetPassword(resetPassword, credentials);
        
        JSONObject filters = new JSONObject().put("filters",resetPassword);
        JSONObject range = new JSONObject().put("offset",0).put("count",1);
        credentials = new JSONObject().put("email",resetPassword.getString("email")).put("password",resetPassword.getString("newPassword"));
        JSONObject member = com.alleni.member.AppUser.filter(filters, range, credentials);

        // dwh:  if db call failed, let the user know
        if (reply.getBoolean("success") == true && member.getBoolean("success"))
        {
            if (member.getJSONObject("data").getJSONArray("appUser").length() == 0)
            {
                reply.getJSONObject("data").put("userFound", Boolean.FALSE);
            }
            else
            {
                reply.getJSONObject("data").put("userFound", Boolean.TRUE);
                member = member.getJSONObject("data").getJSONArray("appUser").getJSONObject(0);

                String to = member.getString("email");
                
                String url = request.getScheme() + "://" + request.getServerName() + "/";

                Map<String, Object> args = new HashMap<String, Object>();
                args.put("newPassword", newPassword);
                args.put("url", url);
                args.put("firstName", member.has("firstName")? member.getString("firstName") : "Zebra member");
                
                String subject = ClosureUtil.render("resetPassword.soy", "services.appUser.resetPassword", "subject", args);
                String plaintextBody = ClosureUtil.render("resetPassword.soy", "services.appUser.resetPassword", "plaintext", args);
                String htmlBody = ClosureUtil.render("resetPassword.soy", "services.appUser.resetPassword", "html", args);

                Mail mail = new Mail();
                mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.passwordrecovery"));
                mail.sendMultiPartMessage("donotreply@zebrazapps.com", to, subject, plaintextBody, htmlBody);
            }
        }

    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>