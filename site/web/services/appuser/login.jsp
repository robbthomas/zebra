<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%    
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject loginValues = json.getJSONObject("appuserLogin");
        JSONObject accountValues = loginValues.getJSONObject("accounts");
        int count = accountValues.getInt("count");
        int offset = accountValues.getInt("offset");

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

//        if (!credentials.get("email").equals("fail"))
//            reply = com.alleni.common.BaseBean.mockdata("appuserLoginSuccess.json");
//        else
//            reply = com.alleni.common.BaseBean.mockdata("appuserLoginFailure.json");

        reply = com.alleni.member.AppUser.login2(credentials, count, offset);

        // if successful login, retrieve accounts and roles information for this member
        /*
        if (reply.getBoolean("success"))
        {
            JSONObject member = reply.getJSONObject("data");

            JSONObject filter = new JSONObject().put("memberId", credentials.get("email"));
            JSONObject filters = new JSONObject().put("filters", filter);;
            JSONObject range = new JSONObject().put("count", -1).put("offset", 0).put("order", "accountId,typeId");

            JSONObject accountMemberRoles = com.alleni.role.AccountMemberRole.filter(filters, range, credentials);

            // if found account roles for this member, add them to the return data
            if (accountMemberRoles.getBoolean("success"))
            {
                accountMemberRoles = accountMemberRoles.getJSONObject("data");
                reply.getJSONObject("data").put("accountMemberRoles", accountMemberRoles);
            }
        }
        */

    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>