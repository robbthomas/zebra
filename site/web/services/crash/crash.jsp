<%@page import="java.util.Formatter"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Calendar" %>
<%@page import="java.util.Date"%>
<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>

<%
    JSONObject reply = new JSONObject();

	try
    {
        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");
		JSONObject json = new JSONObject(request.getParameter("json"));
		Calendar now = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("M/d/yyyy");
		String formattedNow = formatter.format(now.getTime());
		
		
		
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
	
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>