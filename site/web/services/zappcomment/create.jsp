<%@page import="com.alleni.util.Mail"%>
<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@page import="com.alleni.template.ClosureUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject createValues = json.getJSONObject("zappCommentCreate");
        
        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        reply = com.alleni.feedback.ZappComment.create(createValues, credentials);
        
        String createdByEmail = credentials.getString("email");
        String projectId = createValues.getString("projectId");
        String urlName = createValues.getString("urlName");
        String projectType = createValues.getString("projectType");
        
        String zappUrl = request.getScheme() + "://" + request.getServerName() + "/#/detail?name&" + urlName;
        final String authorEmail = com.alleni.common.BaseBean.getAuthorEmailForProjectId(projectId);

        Map<String, Object> args = new HashMap<String, Object>();
        args.put("zappUrl", zappUrl);
        args.put("createdByEmail", createdByEmail);
        args.put("commentText", createValues.get("comment"));
        args.put("projectType", projectType);
        
        final String body = ClosureUtil.render("commentCreated.soy",
                "services.comment.create", "plaintext", args);
        final String bodyHTML = ClosureUtil.render("commentCreated.soy",
                "services.comment.create", "html", args);
        final String subject = ClosureUtil.render("commentCreated.soy",
                "services.comment.create", "subject", args);

        new Thread(
          new Runnable() {
        	public void run() {
           		  Mail mail = new Mail();
        		  mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.passwordrecovery"));
        		  mail.sendMultiPartMessage("donotreply@zebrazapps.com", authorEmail, subject, body, bodyHTML);
        	    }
        }).start();
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>