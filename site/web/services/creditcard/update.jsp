<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject updateValues = json.getJSONObject("creditCardUpdate");

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        reply = com.alleni.finance.CreditCard.update(updateValues, credentials);
    }
    catch(Exception e)
    {   
    	System.out.println("Error updating credit card info: " + e.getMessage());
        //reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
        reply = com.alleni.common.BaseBean.error(request.getRequestURI(),
        		"Credit card was not accepted. Please check your information and try again.");
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>