<%@page import="com.alleni.member.AppUser"%>
<%@page import="com.alleni.role.AccountMemberRole"%>
<%@include file="../common/pageHeader.jsp"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="com.alleni.common.JsonError"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.alleni.util.Mail"%>
<%
    // cron6 - Check for credit card expiration dates within next week

    JSONObject reply = new JSONObject();
    try
    {
        java.net.URL url = new java.net.URL(request.getRequestURL().toString());
        if (!url.getHost().equals("localhost") && !url.getHost().equals("integration.zebrazapps.com") && !url.getHost().equals("qa.zebrazapps.com") && !url.getHost().equals("secure.zebrazapps.com"))
        {
            reply = com.alleni.common.BaseBean.error("Invalid request domain", url.getHost());
            response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
            return;
        }

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        // short-circuit to not generate any charges or credits for initial release
        //if (true) { reply.put("success", true); response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply)); return; }

        int withinDays = 14;
        if (request.getParameter("json") != null)
        {
            try
            {
                JSONObject json = new JSONObject(request.getParameter("json"));
                withinDays = json.getJSONObject("commerceCron6").getInt("withinDays");
            }
            catch (JSONException je) { }
        }

        // 1 - Collect list of CreditCards entries with expiration dates <= today (m/d/y) + interval (1 week)
        // for accounts with goodStanding = true.
        reply = com.alleni.finance.CreditCard.findCreditCardsExpiring(withinDays, credentials);

        // 2 - Email account holders that account credit card expires on mm/dd/yyyy and will need to be updated.
        if (reply.has("data"))
        {
            Calendar now = Calendar.getInstance();

            JSONArray creditCards = reply.getJSONObject("data").getJSONArray("creditCards");
            for (int i = 0; i < creditCards.length(); i++)
            {
                JSONObject creditCard = creditCards.getJSONObject(i);
                int year = Integer.parseInt(creditCard.getString("expirationYear"));
                int month = Integer.parseInt(creditCard.getString("expirationMonth"));
      
                String to = creditCard.has("email") ? creditCard.getString("email") : "info@alleni.com";
                String subject = "ZebraZapps account status";
                String bodyHtml = "";
                bodyHtml += "Dear " + creditCard.getString("firstName")+ " " + creditCard.getString("lastName") +  ":";
                bodyHtml += "<br/><br/>The credit card you have on file with us, which we currently bill your ZebraZapps " +
                        "subscription services and shopp purchases with, is nearing its expiration date. " +
                        "If now is a convenient time, " +
                        " <a href=\"http://www.zebrazapps.com/#/account\">please log on and update your payment method of choice.</a>";
                

                bodyHtml += "<br/><br/>&nbsp;The card we have on file: " +
                        "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;" + creditCard.getString("nameOnCreditCard") +
                        //"<br/>&nbsp;&nbsp;&nbsp;&nbsp;" + creditCard.getString("creditCardTypeName") +
                        "<br/>&nbsp;&nbsp;&nbsp;&nbsp;" + creditCard.getString("maskedNumber");
                
                // This next check works because fn_creditcard_expiring() is returning only cards that have
                // or will expire within the withinDays parameter. Note that java.util.Calendar.MONTH values are 0-11.
                if (year <= now.get(Calendar.YEAR) && month < now.get(Calendar.MONTH) + 1) //
                    bodyHtml += "<br/><br/>&nbsp;has expired the end of ";
                else
                    bodyHtml += "<br/><br/>&nbsp;will expire the end of ";
                bodyHtml += month + "/" + year;


                bodyHtml += "<br/><br/>Updates to your account credit card can be made at anytime by logging " +
                	    "in and accessing the Payment Method page under your Account tab.";

                bodyHtml += "<br/><br/>Thank you for making ZebraZapps.com a success.";
                bodyHtml += "<br/>We truly appreciate your business.";

                Mail mail = new Mail();
                mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.ecommerce"));
                mail.sendMultiPartMessage("support@zebrazapps.com", to, subject, null, bodyHtml);

                break; // send only one for testing...
            }
        }
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
        response.getWriter().write(reply.toString());
        return;
    }

    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));

%>
