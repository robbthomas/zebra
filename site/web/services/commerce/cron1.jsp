<%@page import="java.util.Date"%>
<%@ include file="../common/pageHeader.jsp"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="org.json.JSONArray"%>
<%@ page import="org.json.JSONObject"%>
<%@ page import="org.json.JSONException"%>
<%@ page import="com.alleni.common.JsonError"%>
<%@ page import="com.alleni.util.Mail"%>
<%@ page import="com.alleni.template.ClosureUtil"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Map"%>
<%@ page import="org.apache.log4j.Logger"%>
<%
    // cron1 - Generate pending invoices for account subscriptions
    //
    // Checks for paying accounts which are coming due for their periodic (monthly, quarterly, yearly, etc.)
    // account subscription charges.
    //
    // NOTE that an optional Invoices.holdingDateTime allows the generating of subscripition invoices
    // and a notification Email to account holders regarding the upcoming subscription charge ahead of
    // time (say, one week before it is to be charged).
    //
    // Account subscriptions are charged ahead of (at the beginning of) the subscription period.

    JSONObject reply = new JSONObject();
    JSONObject credentials = new JSONObject();

    try
    {
        java.net.URL url = new java.net.URL(request.getRequestURL().toString());
        if (!url.getHost().equals("localhost") && !url.getHost().equals("integration.zebrazapps.com") && !url.getHost().equals("qa.zebrazapps.com") && !url.getHost().equals("secure.zebrazapps.com"))
        {
            reply = com.alleni.common.BaseBean.error("Invalid request domain", url.getHost());
            response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
            return;
        }

        String authorizationHeader = request.getHeader("authorization");
        credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        // short-circuit to not generate any charges or credits for initial release
        //if (true) { reply.put("success", true); response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply)); return; }

        //-------------------------------------------------------------------------/
        // First 3 steps are done in a single SQL call:
        //
        // 1 - Get list of paying accounts in GOOD STANDING && allowsubscriptionpayment == true
        //	   which have reached their
        //     billing cycle month/day or will within the optional holdingDays
        //     interval, if one is configured.
        //
        // 2 - Create Invoice table entries to charge with invoiceStatus=Pending.
        //     These will be processed together with any other pending invoices
        //     for each account.
        //
        // 3 - Update account billing cycle month/day next subscription charge
        //     due date.
        //-------------------------------------------------------------------------/

        JSONObject renewValues = new JSONObject();
        if (request.getParameter("json") != null)
        {
            try
            {
                // retrieve optional holdingDays parameter
                JSONObject json = new JSONObject(request.getParameter("json"));
                int holdingDays = json.getJSONObject("commerceCron1").getInt("holdingDays");
                renewValues.put("holdingDays", holdingDays); 
            }
            catch (JSONException je) { }
        }

        reply = com.alleni.finance.Subscription.renewSubscriptions(renewValues, credentials);

        //-------------------------------------------------------------------------/
        // 4 - Email account holders regarding upcoming subscription charge.
        //-------------------------------------------------------------------------/
        if (reply.has("data"))
        {
            JSONArray renewals = new JSONArray();
            renewals = reply.getJSONObject("data").getJSONArray("subscriptions");
            for (int i = 0; i < renewals.length(); i++)
            {
                JSONObject renewal = renewals.getJSONObject(i);

                // TBD: need to adjust following by currencyType precision
                String subscriptionCharge = String.format("%.2f", ((float)renewal.getInt("price"))/100);

                String to = renewal.getString("email");
                boolean validCC = (renewal.has("creditCardId") && renewal.getInt("creditCardId") > 0);

                // go from String representation of SQL timestamp to our preferred String date format
               	String termDueTimeStamp = renewal.getString("termDueDateTime");
                SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date termDueDate = sdfIn.parse(termDueTimeStamp);
                SimpleDateFormat sdfOut = new SimpleDateFormat("MMMM dd, yyyy");
                String termDueDateStr = sdfOut.format(termDueDate);
                
                Date now = new Date();
                
                long msDiff = termDueDate.getTime() - now.getTime();
                
                int daysOutInt = Math.round(msDiff / (1000 * 60 * 60 * 24));
                
                Map<String, Object> args = new HashMap<String, Object>();
                args.put("firstName", renewal.getString("firstName"));
                args.put("lastName", renewal.getString("lastName"));
                args.put("termDueDate", termDueDateStr);
                args.put("holdingDays", daysOutInt);
                args.put("url", "https://zebrazapps.com");


                String template = validCC ? "cc" : "no_cc";

                String subject = ClosureUtil.render("cron1-plaintext.soy", "services.commerce.cron1.renew", "subject", args);
                String body = ClosureUtil.render("cron1-plaintext.soy", "services.commerce.cron1.renew", template, args);
                String bodyHtml = ClosureUtil.render("cron1-html.soy", "services.commerce.cron1.renew", template, args);


                Mail mail = new Mail();
                mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.ecommerce"));

                mail.sendMultiPartMessage("support@zebrazapps.com", to, subject, body, bodyHtml);
            }
        }
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
        response.getWriter().write(reply.toString());
        return;
    }

    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));

%>