<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONException" %>
<%@ page import="com.alleni.common.JsonError" %>
<%
    // cron3 - Check for "declined" charges and demote those accounts
    //
    // If transaction for pending charges against an account is "declined" because of problems
    // with the customer credit card or bank account profiles, then the account status is demoted
    // to make it ineligible to make any additional purchases.
    //
    // Account is demoted to goodStanding=false and treated as a "Free" account until the account billing
    // information is updated successfully -- meaning, the authorize.net customer credit card or bank account
    // profiles are successfully updated and verified.
    //
    // If account subscription period had expired and account standing is restored,
    // meaning, the subscription amount was paid as well, should the subscription period
    // month/day be updated as well and reset to that of the day good standing was restored?

    JSONObject reply = new JSONObject();
    try
    {
        throw new Exception("Cron3 is now defunct (see step 8 of cron2)");
//        java.net.URL url = new java.net.URL(request.getRequestURL().toString());
//        if (!url.getHost().equals("localhost") && !url.getHost().equals("integration.zebrazapps.com") && !url.getHost().equals("qa.zebrazapps.com") && !url.getHost().equals("secure.zebrazapps.com"))
//        {
//            reply = com.alleni.common.BaseBean.error("Invalid request domain", url.getHost());
//            response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
//            return;
//        }
//
//        String authorizationHeader = request.getHeader("authorization");
//        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");
//
//        // short-circuit to not generate any charges or credits for initial release
//        //if (true) { reply.put("success", true); response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply)); return; }
//
//        reply = com.alleni.account.Account.demoteChargesDeclined(credentials);
//
//        if (reply.has("data"))
//        {
//            JSONArray accounts = reply.getJSONObject("data").getJSONArray("accounts");
//
//            for (int i = 0; i < accounts.length(); i++)
//            {
//                JSONObject account = accounts.getJSONObject(i);
//
//                // log or email or something???
//            }
//
//        }
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
        response.getWriter().write(reply.toString());
        return;
    }

//    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));

%>