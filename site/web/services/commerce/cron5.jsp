<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONException" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="com.alleni.util.Mail" %>
<%@ page import="com.qp.onlinecommerce.CreditCardAccount" %>
<%@ page import="com.alleni.template.ClosureUtil" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%
    // cron5 - Process pending payments at or beyond HOLDING date
    //
    // Get list of accounts with goodStanding=true with any Payments.status=Pending entries
    // with holdingDateTime <= current mm/dd/yyyy and return aggregate totals for each account.

    JSONObject reply = new JSONObject();
    try
    {
        java.net.URL url = new java.net.URL(request.getRequestURL().toString());
        if (!url.getHost().equals("localhost") && !url.getHost().equals("integration.zebrazapps.com") && !url.getHost().equals("qa.zebrazapps.com") && !url.getHost().equals("secure.zebrazapps.com"))
        {
            reply = com.alleni.common.BaseBean.error("Invalid request domain", url.getHost());
            response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
            return;
        }

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        // ***** LEAVE THIS ONE SHORT-CIRCUITED UNTIL PayPal SWITCHOVER SINCE CANNOT PUSH PAYMENTS DUE TO CREDIT CARDS *****
        // short-circuit to not generate any charges or credits for initial release
        if (true) { reply.put("success", true); response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply)); return; }

        int accountId = 0; // optional accountId
        if (request.getParameter("json") != null)
        {
            try
            {
                JSONObject json = new JSONObject(request.getParameter("json"));
                accountId = json.getJSONObject("commerceCron5").getInt("accountId");
            }
            catch (JSONException je) { }
        }

        JSONObject filterValues = new JSONObject();
        filterValues.put("accountId", accountId);

        // Prepare credit transactions for processing -- makes single sql call for following 3 steps:
        // 1 - Create a FinancialTransaction status=Pending entry.
        // 2 - Update Payments entries to be processed with transactionId from above.
        // 3 - Join and aggregate payment line items for payments with transactionId to return a total to credit.

        reply = com.alleni.finance.Payment.batchPendingPayments(filterValues, credentials);

        if (false) // if (reply.has("data")) // (TBD: Adapt below to PayPal mechanism to transfer payments to zapp/gadget sellers.)
        {
            // For each credit transaction
            JSONArray payments = reply.getJSONObject("data").getJSONArray("payments");
            for (int i = 0; i < payments.length(); i++)
            {
                JSONObject payment = payments.getJSONObject(i);

                int transactionId = payment.getInt("transactionId");

                // 4 - CREDIT the account credit card or bank account the pending payments total for that account
                // (LESS per transaction charges -- is this correct?).
                JSONObject range = new JSONObject("{offset:0, count:1, order:'expirationYear:d,expirationMonth:d'}");
                JSONObject filters = new JSONObject().put("accountId",payment.getInt("accountId"));
                JSONObject creditCard = com.alleni.finance.CreditCard.filter(new JSONObject().put("filters",filters), range, credentials);

                if (creditCard.getBoolean("success")) creditCard = creditCard.getJSONObject("data");
                JSONArray creditCards = creditCard.getJSONArray("creditCards");

                if (creditCards.length() <= 0) continue; // no card on file, nothing to do!?

                creditCard = creditCards.getJSONObject(0);
                creditCard.put("transactionReferenceId", "CREDIT " + payment.getString("transactionLabel"));
                creditCard.put("description", "Zebra Zapps " + payment.getString("transactionLabel"));
                // TBD: need to adjust following by currencyType precision
                String totalCredit = String.format("%.2f", ((float)payment.getInt("totalCredit"))/100);
                creditCard.put("totalAmount", totalCredit);

                CreditCardAccount cca = new CreditCardAccount();
                JSONObject transaction = cca.creditToAccount(creditCard);

                // 5 - Update FinancialTransactions entry amount (amount should be negative?) of the payments and status
                // based on success of interaction with authorize.net. Any status information returned by authorize.net
                // should be logged to the FinancialTransaction entry.
                transaction.put("transactionId", transactionId);
                transaction.put("amount", payment.getInt("totalCredit"));
                JSONObject transactionResult = com.alleni.finance.FinancialTransaction.update(transaction, credentials);

                // 6 - If the transaction was successful, update all corresponding Payment entries to status=Completed.
                // (If successful and depending on how/when per transaction charges are collected, might need to generate
                //  an additional Invoice for this account for the per transaction fees calling out the same FinancialTransactionId???)
                if (transactionResult.getBoolean("success"))
                {
                    reply = com.alleni.finance.Payment.updateStateByTransaction(transactionId, "payment_state_credited", credentials);
                }

                // 7 - Email account holder payment transaction attempted and success or failure status and
                // to pls check My Account view for details.
                String to = payment.getString("email");

                Map<String,Object> args = new HashMap<String,Object>();
                args.put("nameOnCreditCard", creditCard.getString("nameOnCreditCard"));
                args.put("maskedNumber", creditCard.getString("maskedNumber"));
                args.put("totalCredit", totalCredit);

                String body;
                String bodyHTML;
                String subject = ClosureUtil.render("cron5.soy", "services.commerce.cron5", "subject", args);
                if (transactionResult.getBoolean("success")) {
                    body = ClosureUtil.render("cron5.soy", "services.commerce.cron5", "successPlaintext", args);
                    bodyHTML = ClosureUtil.render("cron5.soy", "services.commerce.cron5", "successHtml", args);
                } else {
                    args.put("status", transactionResult.getString("status"));
                    args.put("statusReasonCode", transactionResult.getString("statusReasonCode"));
                    args.put("statusReasonText", transactionResult.getString("statusReasonText"));
                    body = ClosureUtil.render("cron5.soy", "services.commerce.cron5", "failurePlaintext", args);
                    bodyHTML = ClosureUtil.render("cron5.soy", "services.commerce.cron5", "failureHtml", args);
                }


                Mail mail = new Mail();
                mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.ecommerce"));
                mail.sendMultiPartMessage("support@zebrazapps.com", to, subject, body, bodyHTML);

            }
        }
        
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
        response.getWriter().write(reply.toString());
        return;
    }

    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));

%>