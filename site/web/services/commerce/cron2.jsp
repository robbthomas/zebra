<%@page import="java.util.Date"%>
<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONException" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="com.alleni.util.Mail" %>
<%@ page import="com.qp.onlinecommerce.CreditCardAccount" %>
<%@ page import="com.alleni.template.ClosureUtil" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%
    // cron2 - Process pending invoices for each account
    //
    // Collects all pending invoices for each account in good standing and attempts
    // a single transaction with authorize.net to charge the credit card on file
    // for the entire amount of all pending invoices for that account.
    //
    // Get list of accounts with goodStanding=true with pending invoices with
    // Invoices.holdingDateTime <= current mm/dd/yyyy and the totals of all
    // invoice line items for those invoices.
    //
    // NOTE that optional holdingDateTime introduced so that subscription invoices
    // and corresponding Email notifications can be generated a week ahead of subcription
    // renewals.

    JSONObject reply = new JSONObject();
    JSONObject credentials = new JSONObject();
    try
    {
        java.net.URL url = new java.net.URL(request.getRequestURL().toString());
        if (!url.getHost().equals("localhost") && !url.getHost().equals("integration.zebrazapps.com") && !url.getHost().equals("qa.zebrazapps.com") && !url.getHost().equals("secure.zebrazapps.com"))
        {
            reply = com.alleni.common.BaseBean.error("Invalid request domain", url.getHost());
            response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
            return;
        }

        String authorizationHeader = request.getHeader("authorization");
        credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        // short-circuit to not generate any charges or credits for initial release
        //if (true) { reply.put("success", true); response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply)); return; }

        // First few steps are performed by a single sql function, fn_invoice_batch_charges_create, which
        // creates new FinancialTransactions table row entries for every accountId+currencyTypeId combination
        // for all pending invoices at or beyond optional holding date for accounts in good standing.
        //
        // 1 - Get list of accounts that have invoices with status=Pending.
        // 2 - Create a FinancialTransaction status=Pending entry
        // 3 - Update all pending invoices, holdingDateTime <= currentTime, with transactionId created above.
        // 4 - Join and aggregate invoice line items for invoices with transactionId to return a total to charge.
        //

        JSONObject filterValues = new JSONObject();
        if (request.getParameter("json") != null)
        {
            try
            {
                JSONObject json = new JSONObject(request.getParameter("json"));
                int accountId = json.getJSONObject("commerceCron2").getInt("accountId");
                filterValues.put("accountId", accountId);
            }
            catch (JSONException je) {}
        }

        reply = com.alleni.finance.Invoice.batchPendingInvoices(filterValues, credentials);
        if (reply.has("data"))
        {
            // For each transactionId = accountId+currencyTypeId combination:
            JSONArray invoices = reply.getJSONObject("data").getJSONArray("invoices");
//            System.out.println(invoices.length() + " invoices about to be processed in cron2.jsp");
            for (int i = 0; i < invoices.length(); i++)
            {
                JSONObject invoice = invoices.getJSONObject(i);

                int transactionId = invoice.getInt("transactionId");

                // 5 - CHARGE account credit card or bank account for the total returned in step 3 (PLUS per transaction charges -- is this correct?)
                // get credit card for the account
                JSONObject range = new JSONObject("{offset:0, count:1, order:'expirationYear:d,expirationMonth:d'}");
                JSONObject filters = new JSONObject().put("accountId",invoice.getInt("accountId"));
                JSONObject creditCard = com.alleni.finance.CreditCard.filter(new JSONObject().put("filters",filters), range, credentials);

                if (creditCard.getBoolean("success")) creditCard = creditCard.getJSONObject("data");
                JSONArray creditCards = creditCard.getJSONArray("creditCards");


                boolean includesSubscription = (invoice.getInt("includesSubscriptionCharge") > 0);
                int subscriptionCharge = invoice.getInt("subscrCharge");
                String totalCharge = String.format("%.2f", ((float)invoice.getInt("totalCharge"))/100);
                boolean transactionProcessingWasSuccessful = false; // assume failure until success is guaranteed

                if(invoice.getInt("totalCharge") <= 0) {
                    // do not create transactions and do not email the user
                    // update the invoice so it does not get picked up by this function again
                    // and also does not get picked up by the next step (payment processing)
                    reply = com.alleni.finance.Invoice.updateStateByTransaction(transactionId, "invoice_state_credits_created", credentials);
                    continue;
                } else if (creditCards.length() > 0) {

                    creditCard = creditCards.getJSONObject(0);
                    creditCard.put("transactionReferenceId", invoice.getString("transactionLabel"));
                    creditCard.put("description", "Zebra Zapps " + invoice.getString("transactionLabel"));
                    // TBD: need to adjust following by currencyType precision
                    creditCard.put("totalAmount", totalCharge);

                    CreditCardAccount cca = new CreditCardAccount();
                    JSONObject transaction = cca.chargeFromAccount(creditCard);

                    // 6 - Update FinancialTransaction entry and record the total amount and success and failure and any other information returned by authorize.net.
                    if (transaction.has("statusId") && !transaction.has("status"))
                    {
                        // an error occurred -- TBD: look this up in the database
                        // note: transaction_status_declined appears to never happen
                        //       instead we see transaction_status_processing_error with the reason text including that it was declined
                        String [] transactionStatus = {"", "transaction_status_approved", "transaction_status_declined", "transaction_status_processing_error", "transaction_status_held_for_review"};
                        String status = transactionStatus[3];
                        try {
                            int statusId = transaction.getInt("statusId");
                            if (0 < statusId && statusId <= 4) {
                                status = transactionStatus[statusId];
                            }
                            if(statusId == 1) {
                                transactionProcessingWasSuccessful = true;
                            }
                        } catch (JSONException je) { }
                        transaction.put("status", status);
                    }
                    else if (!transaction.has("status") && !transaction.getBoolean("success")) {
                        transaction.put("status", "transaction_status_processing_error");
                    }
                    transaction.put("financialTransactionId", transactionId);
                    transaction.put("type", "transaction_type_creditcard");
                    transaction.put("amount", invoice.getInt("totalCharge"));
                    JSONObject transactionResult = com.alleni.finance.FinancialTransaction.update(transaction, credentials);

                    // 7 - If transaction successful, promote statuses of invoices with transactionId to "Charged".
                    if (transactionProcessingWasSuccessful)
                    {
                        reply = com.alleni.finance.Invoice.updateStateByTransaction(transactionId, "invoice_state_charged", credentials);
                        if(includesSubscription) {
                            JSONObject accountUpdate = new JSONObject();
                            accountUpdate.put("accountId",invoice.getInt("accountId"));
                            accountUpdate.put("renewalNoticeSent",0);
                            reply = com.alleni.account.Account.update(accountUpdate, credentials);
                        }
                    }
                }


                // 8 - Update GoodStanding and TermDueDate as necessary (Note this replaces functionality from now defunct cron3)

                if(!transactionProcessingWasSuccessful) {
                    // If transaction for pending charges against an account is "declined" because of problems
                    // with the customer credit card or bank account profiles, then the account status is demoted
                    // to make it ineligible to make any additional purchases.
                    //
                    // Account is demoted to goodStanding=false and treated as a "Free" account until the account billing
                    // information is updated successfully -- meaning, the authorize.net customer credit card or bank account
                    // profiles are successfully updated and verified.
                    //
                    // If account subscription period had expired and account standing is restored,
                    // meaning, the subscription amount was paid as well, should the subscription period
                    // month/day be updated as well and reset to that of the day good standing was restored?

                    JSONObject accountUpdate = new JSONObject();
                    accountUpdate.put("accountId",invoice.getInt("accountId"));
                    accountUpdate.put("goodStanding",0);
                    if(includesSubscription && subscriptionCharge > 0) {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        accountUpdate.put("termDueDateTime", formatter.format(Calendar.getInstance().getTime()));
                    }
                    reply = com.alleni.account.Account.update(accountUpdate, credentials);
                }



                // 9 - Email account holder charge to account credit card was attempted and success or failure status
                //     and to pls check My Account view for details.
                String to = invoice.getString("email");
                
                SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
                String formattedDate = df.format(new Date());
                
                int difference = invoice.getInt("totalCharge") - subscriptionCharge;
                
		        if (difference < 0) difference = 0;

		        String accountType = com.alleni.account.Account.getAccountTypeNameForAccount(invoice.getInt("accountId"));
		        accountType = accountType.substring(0, 1).toUpperCase() + accountType.substring(1); // Capitalize this guy
		        
		        // One-off check to ensure that it's "Professional" instead of "Pro"
		        if ("Pro".equals(accountType)) {
		        	accountType = "Professional";
		        }
		        
				// FIXME: pass in stringified and numeric values for
				//        totalCharge, subscrCharge, difference
                Map<String,Object> args = new HashMap<String,Object>();
                args.put("firstName", invoice.getString("firstName"));
                args.put("totalCharge", totalCharge);
                args.put("subscrCharge", subscriptionCharge);
                args.put("subscrChargeTxt", includesSubscription ? String.format("%.2f", (float)subscriptionCharge/100) : "0.00");
                args.put("includesSubscription", includesSubscription);
                args.put("difference", difference);
                args.put("differenceTxt", String.format("%.2f", (float)difference/100));
                args.put("chargeDate", formattedDate);
                args.put("accountType", accountType);
                String urlStr = request.getScheme() + "://" + request.getServerName();
                args.put("url", "https://zebrazapps.com");
    		//args.put("url", urlStr);

                String body;
                String bodyHTML;
                String subject;
                if (transactionProcessingWasSuccessful) {
                    subject = ClosureUtil.render("cron2-plaintext.soy", "services.commerce.cron2", "subjectsuccess", args);
                    body = ClosureUtil.render("cron2-plaintext.soy", "services.commerce.cron2", "success", args);
                    bodyHTML = ClosureUtil.render("cron2-html.soy", "services.commerce.cron2", "success", args);
                } else {
                    subject = ClosureUtil.render("cron2-plaintext.soy", "services.commerce.cron2", "subjectfailure", args);
                    body = ClosureUtil.render("cron2-plaintext.soy", "services.commerce.cron2", "failure", args);
                    bodyHTML = ClosureUtil.render("cron2-html.soy", "services.commerce.cron2", "failure", args);
                }
                

                Mail mail = new Mail();
                mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.ecommerce"));
                mail.sendMultiPartMessage("support@zebrazapps.com", to, subject, body, bodyHTML);
                
            }
        }
    }
    catch(Exception e)
    {
    	e.printStackTrace();
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
        response.getWriter().write(reply.toString());
        return;
    }

    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));

%>
