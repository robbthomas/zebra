<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONException" %>
<%@ page import="com.alleni.common.JsonError" %>
<%
    // cron4 - Generate pending payments for gadgets and apps
    //
    // Run this cron AFTER the pending invoices have been processed and charged
    // and their statuses promoted so that payment entries generated will include
    // the latest that were successfully charged.
    //
    // 1 - Get list of invoice line items for gadgets/apps for invoices with status=Charged
    // and join with gadget/app productIds and accountIds ordered by gadget/app account
    // and then gadget/app productId with aggregate totals (quantity and amount) for each distinct gadget/app.

    // 2 - Generate Payment and PaymentLineItem entries for each account + gadget/app.
    // Each payment entry should have a HOLDING month/day threshold after which it is to be paid.

    // 3 - Promote corresponding invoice statuses to "Credits Created".
    // (Does this need to be tracked on an item-by-item basis in the invoice line items? Seems overkill
    //  and that a reporting query should be able to join all the data if it is ever needed. Needed for audits possibly?)
    //
    // NOTE that all the above functionality has been wrapped into a single sql function.

    JSONObject reply = new JSONObject();
    try
    {
        java.net.URL url = new java.net.URL(request.getRequestURL().toString());
        if (!url.getHost().equals("localhost") && !url.getHost().equals("integration.zebrazapps.com") && !url.getHost().equals("qa.zebrazapps.com") && !url.getHost().equals("secure.zebrazapps.com"))
        {
            reply = com.alleni.common.BaseBean.error("Invalid request domain", url.getHost());
            response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
            return;
        }

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");
        
        // short-circuit to not generate any charges or credits for initial release
        //if (true) { reply.put("success", true); response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply)); return; }

        JSONObject json = new JSONObject(request.getParameter("json"));
        int holdingDays = json.getJSONObject("commerceCron4").getInt("holdingDays");

        JSONObject filterValues = new JSONObject();
        filterValues.put("holdingDays", holdingDays);

        reply = com.alleni.finance.Payment.createPendingPayments(filterValues, credentials);

        // Loop through the returned data and do something with it??? Log something???
        if (reply.has("data"))
        {
            JSONArray payments = reply.getJSONObject("data").getJSONArray("payments");
            for (int i = 0; i < payments.length(); i++)
            {
                
            }
        }
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
        response.getWriter().write(reply.toString());
        return;
    }

    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));

%>