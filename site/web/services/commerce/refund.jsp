\<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONException" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="com.qp.onlinecommerce.CreditCardAccount" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.alleni.util.Mail" %>
<%@ page import="com.alleni.template.ClosureUtil" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%
    JSONObject reply = new JSONObject();
    try
    {
        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject refundValues = json.getJSONObject("commerceRefund");

        JSONObject refundSummary = new JSONObject();

        JSONObject memberValues = new JSONObject();

        JSONObject creditCard = new JSONObject();
        
        JSONObject transactionResult = new JSONObject();
        
        int totalCredit = 0;


        String to = refundValues.getString("email");

        Map<String,Object> args = new HashMap<String,Object>();
        args.put("nameOnCreditCard", creditCard.getString("nameOnCreditCard"));
        args.put("maskedNumber", creditCard.getString("maskedNumber"));
        args.put("totalCredit", totalCredit);

        String body;
        String bodyHTML;
        String subject = ClosureUtil.render("refund.soy", "services.commerce.refund", "subject", args);
        if (transactionResult.getBoolean("success")) {
            body = ClosureUtil.render("refund.soy", "services.commerce.refund", "successPlaintext", args);
            bodyHTML = ClosureUtil.render("refund.soy", "services.commerce.refund", "successHtml", args);
        } else {
            args.put("status", transactionResult.getString("status"));
            args.put("statusReasonCode", transactionResult.getString("statusReasonCode"));
            args.put("statusReasonText", transactionResult.getString("statusReasonText"));
            body = ClosureUtil.render("refund.soy", "services.commerce.refund", "failurePlaintext", args);
            bodyHTML = ClosureUtil.render("refund.soy", "services.commerce.refund", "failureHtml", args);
        }


        Mail mail = new Mail();
        mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.ecommerce"));
        mail.sendMultiPartMessage("donotreply@zebrazapps.com", to, subject, body, bodyHTML);


        reply.put("success", true);
        reply.put("data", refundSummary);
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>
