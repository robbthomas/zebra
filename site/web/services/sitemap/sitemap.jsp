<%@ page contentType="text/xml; charset=UTF-8" language="java"  pageEncoding="UTF-8" %><?xml version="1.0" encoding="utf-8"?>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.io.*" %> 
<%@ page import="java.net.*" %> 
<%@ page import="org.json.*" %> 
<%@ page import="com.alleni.db.util.*" %>

<%
    response.setHeader("X-Robots-Tag", "noindex");

    PropertyUtil PropertyManager = new PropertyUtil();
    String connectionURL = "jdbc:postgresql://localhost:5432/" + PropertyManager.getProperty("db.database");
    Connection connection = null;
    Statement statement = null;
    ResultSet rs = null;
    connection = DriverManager.getConnection(connectionURL, PropertyManager.getProperty("db.username"), PropertyManager.getProperty("db.password"));
    statement = connection.createStatement();
    String QueryString = "SELECT projects.projectID" + 
                         "   , projects.projectTypeId" +
                         "   , projects.publishedName" + 
                         "   , projects.UrlName" + 
                         "   , projects.createdDateTime" + 
                         "   , projects.description" + 
                         "   , icon.iconFileId" + 
                         " FROM Projects" + 
                         "     LEFT OUTER JOIN publishImages icon" + 
                         "         ON projects.projectId = icon.projectId" + 
                         "         AND icon.iconFileId IS NOT NULL" + 
                         "         AND icon.retired = FALSE" + 
                         " WHERE published = true" +
                         " AND projects.retired = FALSE" +
                         " AND nextPublishedVersionID IS NULL" + 
                         " AND projects.hideInStoreList = FALSE";
    rs = statement.executeQuery(QueryString);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
    String protocol = request.getScheme();
    String serverName = request.getServerName();
    String playerUrl = "";
    String thumbnailLoc = "";
%>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.0">
<!--<url>
    <loc>https://<%=serverName %>/#/support?learn-getstarted</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30848208</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30848208?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Objects and Wiring</video:title>
       <video:description>Creating interactivity in ZebraZapps is easy. This video tutorial is an introduction to using message centers and wiring. Mastering wiring in ZebraZapps takes just a few minutes. Once up and running e-learning prototypes, new applications, gadgets and games are a snap to build.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/425/207425143_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30848431</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30848431?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Color Picker</video:title>
       <video:description>You will find the color picker interface in every place an object&apos;s color can be changed in ZebraZapps. The color picker interface allows for the matching of objects on the stage or choice from a color palette. An object&apos;s fill type is also chosen from the color picker. Fill types are solid colors, gradients or even jellybean.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/427/207427246_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30848610</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30848610?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Presets</video:title>
       <video:description>Objects in the toolbox have default settings (presets). Selecting an object from the toolbox and changing values in the property inspector will set the default properties of each subsequent object. This video tutorial covers preset creation and the benefits of altering specific property values.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/427/207427858_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30848790</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30848790?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Ins and Outs</video:title>
       <video:description>Interactivity in ZebraZapps is often created by wiring from outs (triggers) to ins (behaviors) on other objects. Mastering the relationship between ins and outs on message centers is easy and results are highly interactive applications.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/429/207429595_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30849030</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30849030?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Smart Objects</video:title>
       <video:description>Building sophisticated projects often calls for sophisticated logic. In ZebraZapps you never have to write complex scripts; instead the use of smart objects, with visual logic lets anyone create custom applications and full-blown simulations in just a matter of seconds.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/430/207430991_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/37180623</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/37180623?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Gadget Basics</video:title>
       <video:description>Gadgets are an easy way to build your own reusable learning objects. With a small bit of customization objects can be used across projects, shared with other team members or even sold in the Gadget Shopp. Learn how to simplify your projects, wiring and reduce the amount of work needed by using and building gadgets.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/255/294/255294756_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/37181040</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/37181040?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Advanced Gadget Creation</video:title>
       <video:description>Gadgets can be as big or small, sophisticated or simple as the task requires. This video tutorial focuses on building a gadget with both advanced logic and robust author customization. The process of creating these types of gadgets is easy and can save valuable time across multiple projects.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/255/344/255344245_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/41318971</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/41318971?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Publishing and Sharing an App</video:title>
       <video:description>Applications are meant to be used; learn how to get your applications into the hands of users. Publishing and embedding applications in ZebraZapps could not be easier. This video tutorial covers the basic settings and options required to publish and application.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/286/091/286091282_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/41174546</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/41174546?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Professional - Publishing an App for an LMS</video:title>
       <video:description>Placing an application into your learning management system or LMS, takes just a few clicks. See how to use the LMS object for reporting a score, returning user information inside your application and to create and deploy a SCORM package.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/285/017/285017036_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/35531595</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/35531595?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Embed an App in Captivate</video:title>
       <video:description>ZebraZapps applications can be used in conjunction with many different authoring tools to easily add interactivity to dull slides. This video tutorial covers how to embed an application built with ZebraZapps into a Captivate project.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/242/958/242958670_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30849274</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30849274?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Objects and Wiring</video:title>
       <video:description>Creating interactivity in ZebraZapps is easy. This video tutorial is an introduction to using message centers and wiring. Mastering wiring in ZebraZapps takes just a few minutes. Once up and running e-learning prototypes, new applications, gadgets and games are a snap to build.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/425/207425143_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30849274</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30849274?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Assets and Checkboxes (Portrait)</video:title>
       <video:description>In this video tutorial you will learn how to place assets on the ZebraZapps stage. In addition learn how to use input objects like the checkbox to change the appearance of an asset at run time.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/432/207432788_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30732710</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30732710?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Arenas (Three Little Pigs)</video:title>
       <video:description>Arenas are extraordinarily powerful for holding assets, interactivity and feedback. In fact there is very little arenas cant do. This video tutorial shows how to create an interaction by placing content in an arena.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/011/207011669_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30849524</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30849524?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Drag and Drop (Famous Homes)</video:title>
       <video:description>Drag and Drop interactions are commonly used in e-learning, applications, simulations and games. In ZebraZapps creating a powerful drag and drop interaction is easy. In this video tutorial you will learn the basics of creating a drag and drop, while also experiencing a fun way to build an e-learning activity.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/435/207435179_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30850003</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30850003?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Collisions (Flags)</video:title>
       <video:description>Sharing many of the same principles of a drag and drop interaction, a collision is a type of trigger which will fire without the user having to release their mouse button. Creating a set of behaviors with collisions can achieve advanced animations, games or augment other interaction types. See how quickly you can create custom interactions in ZebraZapps by using a collision trigger.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/438/207438138_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30850364</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30850364?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Paths and Layers (Lewis and Clark)</video:title>
       <video:description>This video tutorial shows how to create a path and use the current value of an object on a path to build interactivity. ZebraZapps allows authors to place just about any object you can imagine on a path.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/442/207442238_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30852633</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30852633?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Paths and Animation (Red Riding Hood)</video:title>
       <video:description>Path-based animation is a powerful way to simulate real world behavior. Learn how to set an object&apos;s position on a path at runtime to animate any object in ZebraZapps.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/457/207457526_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30851003</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30851003?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Truth Tables (Genetics)</video:title>
       <video:description>One of the most powerful objects in ZebraZapps is the Truth Table. The Truth Table is a visual logic object that creates every possible outcome automatically based on the number of conditions authors add to it. In this video tutorial you will learn the basic features of the Truth Table, including all of the possible ways to show feedback to users.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/445/207445490_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30851388</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30851388?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: State Tables 1 (Piano)</video:title>
       <video:description>Advanced animation can be created in ZebraZapps using the State Table. This video tutorial shows how to manipulate multiple objects at run time using the State Table and simple buttons.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/448/207448313_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/30851960</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30851960?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: State Tables 2 (Fox Trot)</video:title>
       <video:description>The Fox Trot is a dance move with multiple steps. This video tutorial shows how to create an animated floor pattern to teach a new dancer the Fox Trot. Built using ZebraZapps, this example gives authors a foundation to build advanced multi-object animations.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/207/453/207453519_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/41165556</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/41165556?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Arena Transitions</video:title>
       <video:description>Arenas are powerful ZebraZapps objects. This video tutorial shows authors some of the built in page transitions that can be used to create smooth looking applications.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/284/946/284946288_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-tour-menu/43853779</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/43853779?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps: Random Answer Order</video:title>
       <video:description>Changing the display order of multiple choice questions and answers at run time is an easy task. Learn how to use the ZebraZapps arena to manipulate content and keep students on their toes.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/304/410/304410919_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-ztutorial-menu</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0701</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/42297905?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Creating a set of Multi-part Interactions 1</video:title>
       <video:description>In this two-part webinar, gain experience with arenas, answer tables, styling buttons and object replacement by building out a check fraud interaction. This webinar is led by Christopher Allen.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/293/417/293417602_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0702</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/42367783?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Creating a set of Multi-part Interactions 2</video:title>
       <video:description>In this two-part webinar, gain experience with arenas, answer tables, styling buttons and object replacement by building out a check fraud interaction. This webinar is led by Christopher Allen.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/293/928/293928705_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0601</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/39796713?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Promoting Practice 1</video:title>
       <video:description>Performance at critical moments requires practice and confidence, two things well designed e-learning can provide. Creating practice environments doesn't have to be hard, especially if you can design scalable activities. In this two-part webinar series, learn how to quickly create exercises which can be scaled and scored, without using a multiple choice test. This webinar is led by Christopher Allen.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/274/666/274666682_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0602</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/39860809?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Promoting Practice 2</video:title>
       <video:description>Performance at critical moments requires practice and confidence, two things well designed e-learning can provide. Creating practice environments doesn't have to be hard, especially if you can design scalable activities. In this two-part webinar series, learn how to quickly create exercises which can be scaled and scored, without using a multiple choice test. This webinar is led by Christopher Allen.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/275/166/275166102_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0501</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/32930970?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Employee Security e-Learning Course 1</video:title>
       <video:description>Build a Well-Known Allen Interactions Course in ZebraZapps! In this three-day webinar series, re-create a portion of an award-winning e-learning course Allen Interactions developed for Corning Incorporated. Day One introduces the concepts, and builds the course introduction.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/223/150/223150272_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0502</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/32991627?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Employee Security e-Learning Course 2</video:title>
       <video:description>Build a Well-Known Allen Interactions Course in ZebraZapps! In this three-day webinar series, re-create a portion of an award-winning e-learning course Allen Interactions developed for Corning Incorporated. Day One introduces the concepts, and builds the course introduction.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/223/614/223614372_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0503</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/33282819?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Employee Security e-Learning Course 3</video:title>
       <video:description>Build a Well-Known Allen Interactions Course in ZebraZapps! In this three-day webinar series, re-create a portion of an award-winning e-learning course Allen Interactions developed for Corning Incorporated. Day One introduces the concepts, and builds the course introduction.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/225/783/225783942_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0401</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/31883556?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Building Your First Set of e-Learning Interactions 1</video:title>
       <video:description>Session One provides a complete introduction to ZebraZapps. Experience what kind of applications ZebraZapps can produce, and how easy it is to create interactivity and share your work with others. Attendees lay out content, create roll-overs, replace objects and include YouTube video in a ZebraZapps project.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/215/256/215256779_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0402</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/31978869?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Building Your First Set of e-Learning Interactions 2</video:title>
       <video:description>Session Two builds on the foundations covered in Session One. It introduces attendees to several basic concepts about building a robust e-learning piece with interactive discovery and information delivery.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/215/978/215978741_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0101</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/29116740?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar:Economics of the Lemonade Stand 1</video:title>
       <video:description>This webinar builds a business simulation, using a "Lemonade Stand" as a metaphor. Attendees build a complete simulation, learn to manage large amounts of interactive content with arenas, and create interactions with dynamic feedback.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/194/743/194743452_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0102</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/29114397?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Economics of the Lemonade Stand 2</video:title>
       <video:description>This webinar builds a business simulation, using a "Lemonade Stand" as a metaphor. Attendees build a complete simulation, learn to manage large amounts of interactive content with arenas, and create interactions with dynamic feedback.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/194/740/194740642_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0201</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30101151?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Using an AED: Automated External Defibrilator 1</video:title>
       <video:description>This webinar builds an emergency response simulation, to teach responders how to use an AED - an Automated External Defibrillaton device - during an intervention. Attendees create paging structures with arenas, build and integrate gadgets into their interaction, and construct delayed feedback.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/201/970/201970018_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0202</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/30185014?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Using an AED: Automated External Defibrilator 2</video:title>
       <video:description>This webinar builds an emergency response simulation, to teach responders how to use an AED - an Automated External Defibrillaton device - during an intervention. Attendees create paging structures with arenas, build and integrate gadgets into their interaction, and construct delayed feedback.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/202/621/202621810_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0301</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/31207779?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Interacting with a Classic Florida Postcard 1</video:title>
       <video:description>This webinar builds an interactive puzzle with answer judging, using a classic Florida postcard as inspiration. Attendees experiment with wiring methods and drag and drop interactivity. They use sliders as counters, learn how to replace assets, and use arenas and gadgets to complete the interaction.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/210/133/210133104_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-webinar-menu/0302</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/31207963?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps Webinar: Interacting with a Classic Florida Postcard 2</video:title>
       <video:description>This webinar builds an interactive puzzle with answer judging, using a classic Florida postcard as inspiration. Attendees experiment with wiring methods and drag and drop interactivity. They use sliders as counters, learn how to replace assets, and use arenas and gadgets to complete the interaction.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/210/134/210134797_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-erik-for-kids</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-erik-how-zebra-works-toolbox</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/31397996?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps for Kids: The Toolbox</video:title>
       <video:description>  To make stuff in ZebraZapps, you just wire things together. Use the toolbox to pick what you want to make. Then connect them together with wires to make them work! Here's how.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/211/567/211567107_200.jpg</video:thumbnail_loc>
    </video:video>
</url>

<url>
    <loc>https://<%=serverName %>/#/support?learn-erik-message-centers</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/31815512?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps for Kids: Wiring and Message Centers</video:title>
       <video:description>  To make stuff in ZebraZapps, you just wire things together. Use the toolbox to pick what you want to make. Then connect them together with wires to make them work! Here's how.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/214/733/214733244_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-erik-make-study-book</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/31397933?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps for Kids: Make a Study Book</video:title>
       <video:description>I made this for school for a Social Studies class. We projected it on a small board. Other students in my class came up and answered the questions by moving the pictures to the right spot.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/211/566/211566562_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-erik-import-assets</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/36989544?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps for Kids: Import Assets</video:title>
       <video:description>I made this for school for a Social Studies class. We projected it on a small board. Other students in my class came up and answered the questions by moving the pictures to the right spot.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/253/909/253909062_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-erik-drag-drop-questions</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/36989447?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps for Kids: Drag and Drop Questions</video:title>
       <video:description>I made this for school for a Social Studies class. We projected it on a small board. Other students in my class came up and answered the questions by moving the pictures to the right spot.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/253/909/253909152_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-erik-judging-table</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
    <video:video>
       <video:player_loc allow_embed="yes" autoplay="ap=1">https://player.vimeo.com/video/36989256?title=0&amp;byline=0&amp;portrait=0</video:player_loc>
       <video:title>ZebraZapps for Kids: Answer Judging Table</video:title>
       <video:description>I made this for school for a Social Studies class. We projected it on a small board. Other students in my class came up and answered the questions by moving the pictures to the right spot.</video:description>
       <video:thumbnail_loc allow_embed="yes" autoplay="ap=1">http://b.vimeocdn.com/ts/253/907/253907351_200.jpg</video:thumbnail_loc>
    </video:video>
</url>
<url>
    <loc>https://<%=serverName %>/#/support?learn-help</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?what</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?why</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?who</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?create</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?publish-share</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?shopps</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?creating-projects</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?understanding-key-features</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?tech-specs</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?allen-learning-technologies</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>https://<%=serverName %>/#/aboutZebra?contact-us</loc>
    <lastmod>2012-07-01</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
</url>-->

<% while (rs.next()) { %>
    <url>
        <loc><%=protocol %>://<%=serverName %>/detail/name/<%=rs.getString("UrlName") %></loc>
        <lastmod><%= sdf.format(rs.getDate("createdDateTime")) %></lastmod>
        <changefreq>monthly</changefreq>
        <priority>0.5</priority>
    <% if (rs.getInt("projectTypeId") == 2) { %>
        <video:video>
            <% 
              playerUrl = protocol + "://" + serverName + "/embed/" + rs.getString("projectID");
            %>
            <video:player_loc allow_embed="yes" autoplay="ap=1"><%=playerUrl %></video:player_loc>
            <% thumbnailLoc = "http://static.zebrazapps.com/icons/" + rs.getString("iconFileId") + "-180.png"; %>
            <video:thumbnail_loc allow_embed="yes" autoplay="ap=1"><%=thumbnailLoc %></video:thumbnail_loc>
            <video:title><![CDATA[<%=rs.getString("publishedName") %>]]></video:title>
            <video:description><![CDATA[<%=rs.getString("description") %>]]></video:description>
        </video:video>
    <% } %>
    </url>
<% } %>
</urlset>
<%
    rs.close();
    statement.close();
    connection.close();
%>