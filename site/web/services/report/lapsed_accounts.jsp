<%@page import="java.util.Date"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.alleni.db.util.QueryTools.Row"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.io.Writer"%>
<%@page import="com.alleni.db.util.QueryTools.FullTable"%>
<%@page import="com.alleni.db.util.QueryTools"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.io.StringWriter"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.CallableStatement"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.alleni.common.BaseDAO"%>
<%@page import="com.alleni.db.util.JDBCHelper"%>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="../messaging/adminheader.jsp" %>
<%

    String szBeginDate = request.getParameter("begin_date");
    String szEndDate = request.getParameter("end_date");
    String format = request.getParameter("format");
    String displayResults = request.getParameter("displayResults");
    
    // -- begin date validation
    Object args[] = null;
    boolean dateError = false;
    boolean dateRange = false;
    Date beginDate = null;
    Date endDate   = null;
    if (szBeginDate != null || szEndDate != null) {
    	dateRange = true;
    	if ( szBeginDate != null && szEndDate != null) {
    		try {
    			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
    			beginDate = sdf.parse(szBeginDate);
    			endDate = sdf.parse(szEndDate);
    			dateError = false;
    			args = new Object[]{beginDate, endDate};
    		} catch (ParseException e) {
    			dateError = true;
    		}
    	} else {
    		dateError = false;
    	}
    } else {
    	szBeginDate = "mm-dd-yyyy"; // could overwrite what they wrote
    	szEndDate = "mm-dd-yyyy";  // if only one is set
    }
    // -- end date validation
    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <form action="lapsed_accounts.jsp">
        Table format:<br>
        <input type="radio" name="format" value="html" checked>HTML<br>
        <input type="radio" name="format" value="csv">CSV<br>
        <input type="radio" name="format" value="sql">SQL<br>
        <input type="hidden" name="displayResults" value="true" />
    
    <% if (dateError && dateRange) {%> <span style="color:red"><% }%>
        between  <input type="text" name="begin_date" size="8" value="<%=szBeginDate %>"/>
        and <input type="text" name="end_date" size="8" value="<%=szEndDate %>"/> 
        <% if (dateError && dateRange) {%></span><%} %>
    <input type="submit" value="print" />
    </form>
</body>
</html>
<%
			
	String sql =  "select distinct accounts.accountid, termduedatetime as "
			 + "\"expiration date\", age(termduedatetime) - interval '60 days' as \"time past due\", email "
			 + "from accounts "
		     + "inner join accountmemberroles on accountmemberroles.accountid = accounts.accountid "
		     + "inner join app_user on app_user.id = accountmemberroles.memberid "
		     + "where age(termduedatetime) > interval '60 days' "
		     + "and accounts.termduedatetime >= ? and accounts.termduedatetime <= ? "
		     + "order by \"expiration date\" asc";

	
    if(displayResults != null && dateError == false ) {
        final PrintWriter o = new PrintWriter(out);
        QueryTools.DbCon con = new QueryTools.DbCon(new QueryTools.DbCon.MessageHandler() {
            public void handleMessage(String msg) {
                o.println(msg);
            }
        });
        try {
            if("sql".equals(format)) {
                new PrintWriter(out).println(sql);
                return;
            }
            List<FullTable> tables = con.query(sql, args);
            if("csv".equals(format)) {
                response.reset();
                Writer w = response.getWriter();
                response.setContentType("text/csv");
                response.setHeader("Content-disposition","attachment; filename=invoices.csv");
                tables.get(0).writeCSV(w);
                w.close();
            } else {
                for(FullTable t : tables) {
                    t.writeHTML(out);
                }
            }
            return;
        } finally {
            con.close();
        }
    }
    response.setContentType("text/html");
%>

