<%@page import="java.util.Date"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.io.Writer"%>
<%@page import="com.alleni.db.util.QueryTools.FullTable"%>
<%@page import="com.alleni.db.util.QueryTools"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.io.StringWriter"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.CallableStatement"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.alleni.common.BaseDAO"%>
<%@page import="com.alleni.db.util.JDBCHelper"%>
<%@ page import="java.util.List" %>
<%@ page language="java" pageEncoding="UTF-8"%>


<%@include file="../messaging/adminheader.jsp" %>
<%

	String type = request.getParameter("type");
	String userType = request.getParameter("user_type");
	String format = request.getParameter("format");
	String search = request.getParameter("search");
	String exact = request.getParameter("exact");
    if("on".equals(exact) == false) {
        search = '%' + search + '%';
    }
    String shoppAction = request.getParameter("shopp_action");
    
    Integer price = null;
    String szPrice = request.getParameter("price");
    if (szPrice != null) price = Integer.valueOf(szPrice);
    
    Integer projectType = 0;
    String szProjectType = request.getParameter("project_type");
    if (szProjectType != null) projectType = Integer.parseInt(szProjectType);
    
    String szBeginDate = request.getParameter("begin_date");
    String szEndDate = request.getParameter("end_date");
    
    // -- begin date validation
    boolean dateError = true;
    Date beginDate = null;
    Date endDate   = null;
   	if ( szBeginDate != null && szEndDate != null) {
   		try {
  			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
            if(szBeginDate.isEmpty()) {
                beginDate = sdf.parse("01-01-1900");
            } else {
   			    beginDate = sdf.parse(szBeginDate);
            }
            if(szEndDate.isEmpty()) {
                endDate = sdf.parse("12-31-2070");
            } else {
                endDate = sdf.parse(szEndDate);
            }
   			dateError = false;
   		} catch (ParseException e) {
   			dateError = true;
   		}
   	} else {
   		dateError = false;
   	}
    // -- end date validation
    
    Object[] args = null;
	String sql = null;
	
	// make comma-separated text-area?
	String[] allenDomains = new String[] {"alleni.com", "email.com", "allenlt.com", "alleninteractions.com", "zebrazapps.com", "elearninggeek.com", "jidev.com"};
	String userFilter = "";

	if ("alleni".equals(userType)) {
	    userFilter = " and app_user.email similar to '(x";
	} else if ("non_alleni".equals(userType)) {
        userFilter = " and app_user.email not similar to '(x";
	}
	for (String allenDomain : allenDomains) {
			userFilter += "|%" + allenDomain;
	}
        
	if("alleni".equals(userType)) {
		userFilter += 
		")'  or app_user.displayname  in ('gregdaigle', 'ShortStuf7', 'sjackson', 'Cyberpunkhero', 'Jeffrey', 'FiddleFaddle', 'c', 'Erik') ";
	} else if("non_alleni".equals(userType)) {
		userFilter += 
		")'  and app_user.displayname not in ('gregdaigle', 'ShortStuf7', 'sjackson', 'Cyberpunkhero', 'Jeffrey', 'FiddleFaddle', 'c', 'Erik') ";
	} else {
		userFilter = "";
	}

	if("invoices".equals(type)) {
            String optionalDateClause = "";
            if (!dateError) {
                    optionalDateClause = " and Invoices.createddatetime >= ? and Invoices.createddatetime <= ?";
                    args = new Object[]{beginDate, endDate, search, search};
            } else {
                args = new String[]{search, search};
            }
		sql = "select \n" +
                    "  app_user.email as \"Account Admin Email\", accounts.merchantcustomerid, \n" +
                    "  accounts.goodstanding, invoicestates.name as \"Invoice State\", \n" +
                    "  invoices.invoiceid, \n" +
                    "  invoices.description as \"Invoice Desc\", invoices.cachedtotal as \"Invoice Total\", \n" +
                    "  invoices.createddatetime as \"Invoice Date\", invoices.holdingdatetime as \"Invoice Hold Date\", \n" +
                    "  financialtransactionstatus.name as \"Transaction status\", financialtransactions.statusreasoncode, financialtransactions.statusreasontext, \n" +
                    "  financialtransactions.cimtransactionid as \"Transaction ID\", financialtransactions.amount as \"Transaction Amt\", financialtransactions.transactiondatetime, \n" +
                    "  InvoiceLineItems.linenumber, InvoiceLineItems.quantity as \"Line Quantity\", \n" +
                    "  InvoiceLineItems.amountTotal as \"Line Amt\", InvoiceLineItems.description as \"Line Desc\", \n" +
                    "  InvoiceLineItems.projectId as \"ProjectId\", InvoiceLineItems.productCodeId as \"Product Code\" \n" +
                    "from invoicelineitems \n" +
                    "join invoices \n" +
                    "  on (invoicelineitems.invoiceid = invoices.invoiceid" + optionalDateClause + ") \n" +
                    "join currencytypes \n" +
                    "  on invoices.currencytypeid = currencytypes.currencytypeid \n" +
                    "join invoicestates \n" +
                    "  on invoices.stateid = invoicestates.invoicestateid \n" +
                    "left join financialtransactions \n" +
                    "  on financialtransactions.financialtransactionid = invoices.transactionid \n" +
                    "left join financialtransactionstatus \n" +
                    "  on financialtransactionstatus.financialtransactionstatusid = financialtransactions.statusid \n" +
                    "join accounts \n" +
                    "  on accounts.accountid = invoices.accountid \n" +
                    "join app_user \n" +
                    "  on app_user.id = ( \n" +
                    "    select min(au_inner.id) \n" +
                    "    from accountmemberroles \n" +
                    "    inner join roletypes \n" +
                    "      on accountmemberroles.typeid = roletypes.roletypeid \n" +
                    "      and roletypes.tag = 'ROLE_TYPE_ACCOUNT_ADMINISTRATOR' \n" +
                    "    inner join app_user au_inner \n" +
                    "      on au_inner.id = accountmemberroles.memberid \n" +
                    "    where accountmemberroles.accountid = invoices.accountid \n" +
                    "      and accountmemberroles.retired = 0 \n" +
                    "  ) \n" +
                    " where app_user.email ilike ? or app_user.displayname ilike ? " +
                    userFilter +
                    "order by Invoices.invoiceId desc, InvoiceLineItems.lineNumber";

                //System.out.print("********** Invoices SQL='"+sql+"' **********"); // debug

	} else if("payments".equals(type)) {

            String optionalDateClause = "";
            if (!dateError) {
                    optionalDateClause = " AND p.createddatetime >= ? and p.createddatetime <= ? \n";
                    args = new Object[]{beginDate, endDate};
            }

            sql = "SELECT au.email as \"Account Admin Email\", a.accountId, a.goodstanding, p.paymentId, ps.name as \"Payment State\", \n" +
                  "  p.cachedTotal as \"Payment Total\", pli.lineNumber as \"Line Item\", pli.projectId as \"projectId\", pli.description as \"Item Desc\", \n" +
                  "  pli.quantity as \"Item Qty\", pli.amountTotal as \"Item subtotal\", \n" +
                  "  p.createdDateTime as \"Created\", p.holdingDateTime as \"Hold until\", \n" +
                  "  ft.label as \"Transaction label\", ft.amount as \"Transaction Amt\", ftt.name as \"Transaction Status\", \n" +
                  "  ft.transactionDateTime as \"Transaction Date Time\", ft.statusReasonCode, ft.statusReasonText, ft.cimTransactionId \n" +
                  "FROM Payments p \n" +
                  "JOIN Accounts a \n" +
                  "  ON a.accountId = p.accountId \n" +
                  "JOIN App_User au \n" +
                  "  ON au.id = (SELECT memberId FROM AccountMemberRoles amr, RoleTypes rt, App_User au2 \n" +
                  "              WHERE amr.accountId = a.accountId AND amr.retired = 0 AND amr.typeId = rt.roleTypeId  \n" +
                  "              AND rt.tag = 'ROLE_TYPE_ACCOUNT_ADMINISTRATOR' AND memberId = au2.id \n" +
                  "              ORDER BY date_created, email LIMIT 1) \n" +
                  "JOIN PaymentStates ps \n" +
                  "  ON p.stateId = ps.paymentStateId \n" +
                  "JOIN PaymentLineItems pli \n" +
                  "  ON (pli.paymentId = p.paymentId AND pli.amountTotal > 0) \n" +
                  "LEFT JOIN FinancialTransactions ft \n" +
                  "  ON ft.financialTransactionId = p.transactionId \n" +
                  "LEFT JOIN FinancialTransactionTypes ftt \n" +
                  "  ON ft.typeId = ftt.financialTransactionTypeId \n" +
                  "WHERE p.cachedTotal > 0 \n" +
                  optionalDateClause +
                  "ORDER BY p.paymentId, pli.lineNumber \n";

            //System.out.print("********** Payments SQL='"+sql+"' **********"); // debug

	} else if("paypall_masspay".equals(type)) {
		sql = "select " +
				"  accounts.paypallogin, " +
				"  payments.cachedtotal, " +
				"  currencytypes.currencycode, " +
				"  'payment'||payments.paymentid as \"id\", " +
				"  'Thank you for your business!' as \"note\" " +
				"from payments " +
				"left join currencytypes " +
				"  on payments.currencytypeid = currencytypes.currencytypeid " +
				"left join paymentstates " +
				"  on payments.stateid = paymentstates.paymentstateid " +
				"left join accounts " +
				"  on accounts.accountid = payments.accountid " +
				"where " +
				"  paymentstates.tag = 'payment_state_pending' -- paymentstateid = 1 " +
				"  and payments.cachedtotal > 0 \n" +
				"order by payments.createddatetime";
	} else if("month_actions_by_user".equals(type)) {
            String dateClause = null;
            if (dateError) {
    		dateClause = "  and age(p.createddatetime) <  INTERVAL '1 month' " ;
            } else {
    		dateClause = " and p.createddatetime >= ?\n" +
    					" and p.createddatetime <= ?\n";
    		args = new Object[]{beginDate, endDate};
            }
            sql =
                    "SELECT au.displayName, au.email, COUNT(p.projectId) AS \"actions\" \n" +
                    "FROM Projects p \n" +
                    "JOIN App_User au \n" +
                    "  ON au.id = p.authorMemberId \n" +
                    dateClause +
                    userFilter.replaceAll("app_user.","au.") +
                    "GROUP BY displayName, email \n" +
                    "ORDER BY COUNT(p.projectId) DESC";
            /*
            sql = "select app_user.displayname, app_user.email, count(parent.id) as \"actions\" " +
                            "from parent " +
                            "inner join app_user " +
                            "  on app_user.id = parent.ownerid " +
                            dateClause +
                            userFilter +
                            "group by app_user.displayname, app_user.email " +
                            "order by count(parent.id) desc ";
            */
	} else if("month_actions_by_day".equals(type)) {
            String dateClause = null;
            if (dateError) {
    		dateClause = " where age(p.createdDateTime) <  INTERVAL '1 month' " ;
            } else {
    		dateClause = " where p.createdDateTime >= ? and p.createdDateTime <= ? \n";
    		args = new Object[]{beginDate, endDate};
            }
            sql =
                    "SELECT DATE_PART('year',p.createdDateTime) AS \"year\", DATE_PART('month',p.createdDateTime) AS \"month\", DATE_PART('day',p.createdDateTime) AS \"day\", COUNT(p.projectId) AS \"actions\" \n" +
                    "FROM Projects p \n" +
                    "JOIN App_User au \n" +
                    "  ON au.id = p.authorMemberId \n" +
                    userFilter.replaceAll("app_user.","au.") +
                    dateClause +
                    "GROUP BY DATE_PART('year',p.createdDateTime), DATE_PART('month',p.createdDateTime), DATE_PART('day',p.createdDateTime) \n" +
                    "ORDER BY DATE_PART('year',p.createdDateTime), DATE_PART('month',p.createdDateTime), DATE_PART('day',p.createdDateTime)";
            /*
            sql = "select date_part('year', parent.date_created) as \"year\", date_part('month', parent.date_created) as \"month\", date_part('day', parent.date_created) as \"day\", count(parent.id) as \"actions\" " +
                            "from parent " +
                            "inner join app_user " +
                            "  on app_user.id = parent.ownerid " +
                            userFilter +
                            dateClause +
                            "group by date_part('year', parent.date_created), date_part('month', parent.date_created), date_part('day', parent.date_created) " +
                            "order by date_part('year', parent.date_created), date_part('month', parent.date_created), date_part('day', parent.date_created) ";
            */
	} else if("month_projects".equals(type)) {
            String dateClause = null;
            if (dateError) {
    		dateClause = "  and age(p.createdDateTime) <  INTERVAL '1 month' " ;
            } else {
    		dateClause = " and p.createdDateTime >= ? and p.createdDateTime <= ?\n";
    		args = new Object[]{beginDate, endDate};
            }
            sql =
                    "SELECT au.displayName, au.email, p.projectName, p.createdDateTime AS date_created, age(p.createdDateTime), p.projectId \n" +
                    "FROM Projects p \n" +
                    "JOIN App_User au \n" +
                    "  ON au.id = p.authorMemberId \n" +
                    "WHERE p.retired = 'f' \n" +
                    dateClause +
                    " AND p.projectTypeId = 2 \n" +
                    userFilter.replaceAll("app_user.","au.");
            /*
		sql = "select app_user.displayname, app_user.email, parent.name, parent.date_created, age(parent.date_created), parent.id " +
				"from parent " +
				"inner join app_user " +
				"  on app_user.id = parent.ownerid " +
				"where not parent.deleted " +
				dateClause +
				"  and parent.projecttypeid = 2 " +
				"  and parent.nextversionid is null " +
				userFilter;
          */
    } else if("month_signups".equals(type)) {
    	String dateClause = null;
    	if (dateError) {
    		dateClause = "where age(app_user.date_created) < INTERVAL '1 month'\n";
    	} else {
    		dateClause = " and app_user.date_created >= ?\n" + 
    					" and app_user.date_created <= ?\n"; 
    		args = new Object[]{beginDate, endDate};
    	}
        sql = "select distinct app_user.displayname, app_user.firstname, app_user.lastname\n" +
                ", app_user.email, app_user.date_created \n" +
                ", accounttypes.name as CurrentAccountType\n" +
                ", accounts.goodStanding = 1 as GoodStanding\n" +
                ", accounts.termduedatetime\n" +
                ", creditCardId is not null as HasCreditCard\n" +
                "from app_user\n" +
                "inner join accountmemberroles\n" +
                "  on accountmemberroles.memberid = app_user.id\n" +
                "inner join accounts\n" +
                "  on accounts.accountid = accountmemberroles.accountid\n" +
                "inner join accounttypes\n" +
                "  on accounttypes.accounttypeid = accounts.typeid\n" +
                " left outer join creditcards on creditcards.accountid = accounts.accountid\n" +
                "                 and creditcards.retired = 0" +
                dateClause +
                userFilter +
                "order by app_user.date_created desc";
		
    } else if("royalties".equals(type)) {
        sql = 
            "SELECT p1.accountId, p1.projectId, p1.publishedName As \"Name\", pt.name As \"Type\", p1.price, p1.selfPrice, sum(p2.selfprice) As \"Royalties\", lt.editable \n" +
            "FROM Projects p1 \n" +
            "JOIN ProjectTypes pt \n" +
            "  ON p1.projectTypeId = pt.projectTypeId \n" +
            "LEFT JOIN Parent_Child pc \n" +
            "  ON pc.parent_children_id = p1.projectId \n" +
            "LEFT JOIN Projects p2 \n" +
            "  ON pc.child_id = p2.projectId \n" +
            "LEFT JOIN LicenseTypes lt \n" +
            "  ON lt.licenseTypeId = p2.licenseTypeId \n" +
            "WHERE p1.published = 't' \n" +
            "AND p1.price > 0 \n" +
            "AND p1.publishedName ilike ?" +
            "GROUP BY p1.accountId, p1.projectId, p1.publishedName, pt.name, p1.price, p1.selfPrice, lt.editable \n" +
            "ORDER BY p1.accountId, p1.publishedName, p1.price DESC, p1.selfPrice DESC";

            //System.out.print("********** Royalties SQL='"+sql+"' **********"); // debug

            args = new String[]{search};
            
    } else if("users".equals(type)) {
        sql = "select distinct  " +
        	  "    app_user.id " +
        	  "    , app_user.date_created " +
        	  "    , app_user.displayname " +
        	  "    , app_user.email " +
              "    , accounts.goodStanding " +
              "    , creditcards.creditcardid " +
              "    , creditcards.creditcardid is not null as \"Has CreditCard\" " +
              "    , case when creditcards.maskednumber is null then '<null>' else creditcards.maskednumber end as \"maskednumber\" " +
              "    , creditcards.customerprofileid is not null as \"Has customerprofileid\" " +
              "    , creditcards.customerpaymentprofileid is not null as \"Has customerpaymentprofileid\" " +
        	  "    , accounts.accountid " +
        	  "    , accounts.termDueDateTime" +
        	  "    , accounts.typeid AS accountTypeId " +
        	  "    , accountTypes.tag AS \"Account Type Name\" " +
              "    , accounts.numLearners" +
              "    , accounts.renewalNoticeSent" +
        	  "    , systemadministrators.memberid is not null as \"Is Admin\" " +
        	  "    , app_user.avatarid IS NOT NULL AS \"Has Avatar\" " +
        	  "    , accounts.bannerid IS NOT NULL AS \"Has Banner\" " +
        	  "from app_user " +
        	  "inner join accountmemberroles " +
        	  "  on app_user.id = accountmemberroles.memberid " +
        	  "inner join accounts " +
        	  "  on accounts.accountid = accountmemberroles.accountid " +
        	  "  LEFT JOIN accountTypes " +
        	  "    ON accounts.typeId = accounttypes.accountTypeId " +
              "left join creditcards " +
              "  on creditcards.accountid = accounts.accountid and creditcards.retired = 0 " +
        	  "left join systemadministrators " +
        	  "  on systemadministrators.memberId = app_user.id " +
            " where app_user.email ilike ? or app_user.displayname ilike ? " +
            userFilter +
            " order by app_user.email";
        args = new String[]{search, search};
        
    } else if("projects".equals(type)) {
        sql =
            "SELECT au.email, au.displayName, p.projectId, pt.name AS \"Type\", substring(p.projectName from 1 for 20) as \"Name\", substring(p.publishedName from 1 for 20) as \"Published Name\", NULLIF(p.published,'f') as pub, \n" +
            "  p.versionMajor AS vmaj,p.versionMinor AS vmin,p.versionDot AS vdot,p.versionBuildNumber AS vbld,p.createdDateTime AS created \n" +
            "FROM Projects p \n" +
            "JOIN App_User au ON au.id = p.authorMemberId \n" +
            "JOIN ProjectTypes pt ON p.projectTypeId = pt.projectTypeId \n" +
            "WHERE p.retired = 'f' \n" +
            "AND p.nextVersionId IS NULL \n" +
            "AND (au.email ILIKE ? OR au.displayName ILIKE ?) \n" +
            "ORDER BY projectName, createdDateTime DESC";

        /*
        sql = "select historyTable.email, historyTable.displayName, historyTable.name \n" +
            ", historyTable.normalgood as historynormalgood \n" +
            ", historyTable.autogood as historyautogood \n" +
            ", historyTable.normalbroken as historynormalbroken \n" +
            ", historyTable.autobroken as historyautobroken \n" +
            ", parent.id as currentprojectId, parent.autosave, parent.last_updated \n" +
            ", gadget_state.id is null as brokenstate \n" +
            "from ( \n" +
            "  select app_user.email, app_user.displayName, p1.ownerId, p1.name \n" +
            "  , sum(case when (not p1.autosave or p1.autosave is null) and g1.id is not null then 1 else 0 end) as normalgood \n" +
            "  , sum(case when p1.autosave                              and g1.id is not null then 1 else 0 end) as autogood \n" +
            "  , sum(case when (not p1.autosave or p1.autosave is null) and g1.id is null     then 1 else 0 end) as normalbroken \n" +
            "  , sum(case when p1.autosave                              and g1.id is null     then 1 else 0 end) as autobroken \n" +
            "  from app_user \n" +
            "  inner join parent p1 \n" +
            "    on p1.ownerId = app_user.id \n" +
            "    and p1.projectTypeId = 2 \n" +
            "  left join gadget_state g1 \n" +
            "    on g1.root_gadget_id = p1.id \n" +
            "  where app_user.email ilike ? or app_user.displayname ilike ? \n" +
            "  group by app_user.email, app_user.displayName, p1.ownerId, p1.name \n" +
            ") as historyTable \n" +
            "left join parent \n" +
            "  on parent.ownerId = historyTable.ownerId \n" +
            "  and parent.name = historyTable.name \n" +
            "  and parent.projectTypeId = 2 \n" +
            "  and parent.nextVersionId is null \n" +
            "left join gadget_state \n" +
            "  on gadget_state.root_gadget_id = parent.id \n" +
            "order by historyTable.email, historyTable.name ";
        */

        args = new String[]{search, search};
        
    } else if("project_history".equals(type)) {
        sql =
            "SELECT au.email, au.displayName, p.projectId, p.nextVersionId, pt.name AS \"Type\", substring(p.projectName from 1 for 20) as \"Name\", substring(p.publishedName from 1 for 20) as \"Published Name\", NULLIF(p.published,'f') as pub, \n" +
            "  p.versionMajor AS vmaj,p.versionMinor AS vmin,p.versionDot AS vdot,p.versionBuildNumber AS vbld,p.createdDateTime AS created,NULLIF(p.retired,'f') as ret \n" +
            "FROM Projects p \n" +
            "JOIN App_User au ON au.id = p.authorMemberId \n" +
            "JOIN ProjectTypes pt ON p.projectTypeId = pt.projectTypeId \n" +
            "WHERE au.email ILIKE ? OR au.displayName ILIKE ? \n" +
            "ORDER BY projectName, createdDateTime DESC";

        /*
        sql = "  select app_user.email, app_user.displayName, p1.name, p1.autosave, g1.id is null as brokenstate\n" +
                "  , p1.id as projectId, p1.date_created\n" +
                "  from parent\n" +
                "  inner join app_user\n" +
                "    on app_user.id = parent.ownerId\n" +
                "  inner join parent p1\n" +
                "    on p1.ownerId = parent.ownerId\n" +
                "    and p1.projectTypeId = parent.projectTypeId\n" +
                "    and p1.name = parent.name\n" +
                "  left join gadget_state g1\n" +
                "    on g1.root_gadget_id = p1.id\n" +
                "  where parent.id = ?\n" +
                "  order by app_user.email, p1.date_created desc ";
        */

        args = new String[]{search, search};

    } else if ("all_gadgets_zapps".equals(type)) {
        sql =
            "SELECT p.projectId, p.publishedName AS name, au.email, p.createdDateTime \n" +
            "FROM Projects p, App_User au \n" +
            "WHERE p.authorMemberId = au.id \n" +
            "AND (au.email ILIKE ? OR au.displayName ILIKE ?) \n" +
            userFilter.replaceAll("app_user.","au.") + " \n" +
            "ORDER BY p.createdDateTime";

        //System.out.print("********** ALL Gadgets Zapps SQL='"+sql+"' **********"); // debug

        args = new String[]{search, search};
    } else if ("dashboard".equals(type) && ! dateError ) {
    	if ( "publish".equals(shoppAction) ) {
            sql =
                "SELECT p.projectId, p.publishedName, c.name AS category_name, p.price, \n" +
                "  au.email, p.editedDateTime AS published_date, p.createdDateTime AS project_created_date \n" +
                "FROM Projects p \n" +
                "JOIN Categories c \n" +
                "  ON p.categoryId = c.categoryId \n" +
                "JOIN App_User au \n" +
                "  ON p.authorMemberId = au.id \n" +
                "WHERE p.retired = 'f' \n" +
                "AND p.published = 't' \n" +
                "AND (au.email ILIKE ? OR au.displayName ILIKE ?) \n" +
                userFilter.replaceAll("app_user.","au.") + " \n" +
                "AND p.price " + (price.equals(0) ? "= 0" : "> 0") + " \n" + // paid
                "AND p.editedDateTime > ? \n" + // begin_date
                "AND p.editedDateTime < ? \n" + // end_date
                "AND p.projectTypeId = ? \n" +  // projectType
                "ORDER BY p.editedDateTime";

            //System.out.print("********** Shopp Stats - Published SQL='"+sql+"' **********"); // debug

            args = new Object[]{search, search, beginDate, endDate, projectType};
    	} else if ("delete".equals(shoppAction)) {
            sql =
                "SELECT p.projectId, p.publishedName, c.name AS category_name, p.price, \n" +
                "  au.email, p.editedDateTime AS published_date, p.createdDateTime AS project_created_date \n" +
                "FROM Projects p \n" +
                "JOIN Categories c \n" +
                "  ON p.categoryId = c.categoryId \n" +
                "JOIN App_User au \n" +
                "  ON p.authorMemberId = au.id \n" +
                "WHERE p.retired = 't' \n" +
                "AND p.published = 't' \n" +
                "AND (au.email ILIKE ? OR au.displayName ILIKE ?) \n" +
                userFilter.replaceAll("app_user.","au.") + " \n" +
                "AND p.price " + (price.equals(0) ? "= 0" : "> 0") + " \n" + // paid
                "AND p.editedDateTime > ? \n" + // begin_date
                "AND p.editedDateTime < ? \n" + // end_date
                "AND p.projectTypeId = ? \n" +  // projectType
                "ORDER BY p.editedDateTime";

            System.out.print("********** Shopp Stats - Deleted SQL='"+sql+"' **********"); // debug

            args = new Object[]{search, search, beginDate, endDate, projectType};
    	} else if ("purchase".equals(shoppAction)) {
            sql =
                "SELECT p.publishedName AS name, c.name AS category, i.createdDateTime AS purchased_date, au.email, au.displayName as usernamame \n" +
                "FROM Projects p \n" +
                "JOIN AccountProjectNumbers apn \n" +
                "  ON p.projectId = apn.projectId \n" +
                "JOIN InvoiceLineItems ili \n" +
                "  ON apn.invoiceLineItemId = ili.invoiceLineItemId \n" +
                "JOIN Invoices i \n" +
                "  ON ili.invoiceId = i.invoiceId \n" +
                "JOIN Categories c \n" +
                "  ON p.categoryId = c.categoryId \n" +
                "JOIN AccountMemberRoles amr \n" +
                "  ON (amr.accountId = p.accountId AND amr.typeId = 1 AND amr.retired = 0) \n" +
                "JOIN App_User au \n" +
                "  ON amr.memberId = au.id \n" +
                "WHERE p.price " + (price.equals(0) ? "= 0" : "> 0" ) + // paid
                "AND (au.email ILIKE ? OR au.displayName ILIKE ?) \n" +
                userFilter.replaceAll("app_user.","au.") + " \n" +
                "AND i.createdDateTime >= ? \n" +
                "AND i.createdDateTime <= ? \n" +
                "AND p.projectTypeId = ? \n" +  // projectType
                "ORDER BY i.createdDateTime";

            //System.out.print("********** Shopp Stats - Purchased SQL='"+sql+"' **********"); // debug

            args = new Object[]{search, search,beginDate, endDate, projectType};
    	}
    	System.out.println(sql);
    	for (Object o : args) System.out.println(o.toString());
    }  else {
        sql = null;
    }

	if(sql != null) {
        final PrintWriter o = new PrintWriter(out);
        QueryTools.DbCon con = new QueryTools.DbCon(new QueryTools.DbCon.MessageHandler() {
            public void handleMessage(String msg) {
                o.println(msg);
            }
        });
		try {
			if("sql".equals(format)) {
                o.println("<pre>");
				o.println(sql);
                o.println("</pre>");
                if(args != null) {
                    o.println("<ol>");
                    for (Object arg : args) {
                        o.print("  <li>");
                        o.print(arg);
                        o.println("</li>");
                    }
                    o.println("<ol>");
                }
				return;
			}
			List<FullTable> tables = con.query(sql, args);
			if("csv".equals(format)) {
				response.reset();
				Writer w = response.getWriter();
				response.setContentType("text/csv");
                String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date());
				response.setHeader("Content-disposition","attachment; filename="+ type + "_" + userType + "_" + date +".csv");
				tables.get(0).writeCSV(w);
				w.close();
			} else {
                            for(FullTable t : tables) {
                                t.writeHTML(out);
                            }
			}
			return;
		} finally {
            con.close();
        }
	}
	response.setContentType("text/html");
%>
<html>
<head>
<title>Query</title>
</head>
<body>
	<h2>Please choose a query</h2>
	<form>
		Table format:<br/>
		<input type="radio" name="format" value="html" checked>HTML<br>
		<input type="radio" name="format" value="csv">CSV<br>
		<input type="radio" name="format" value="sql">SQL<br>
		
		Users:<br/>
		<input type="radio" name="user_type" value="any" checked>Any<br>
		<input type="radio" name="user_type" value="alleni">Alleni Only<br>
		<input type="radio" name="user_type" value="non_alleni">Only non Alleni<br>

		Search:<br/>
		<input type="text" name="search" value=""><input type="checkbox" name="exact"> Exact<br>
		Date Range (only valid on actions with an asterisk)<br />
		
		&nbsp;&nbsp;&nbsp;between <input type="text" name="begin_date" size="8" value="01-01-1900"/>
		and <input type="text" name="end_date" size="8" value="01-01-2070"/><br/>
		
		Query Type:<br/>
		<button type="submit" name="type" value="invoices">Invoices*</button>
		<button type="submit" name="type" value="payments">Payments*</button>
		<button type="submit" name="type" value="paypall_masspay">Paypall Masspay</button>
		<br/>
		<button type="submit" name="type" value="month_actions_by_user">Month actions per user*</button>
		<button type="submit" name="type" value="month_actions_by_day">Month actions per day*</button>
		<button type="submit" name="type" value="month_projects">Month projects*</button>
		<button type="submit" name="type" value="month_signups">Month signups*</button>
		<br/>
		<button type="submit" name="type" value="royalties">Royalties</button>
		<button type="submit" name="type" value="users">Users</button>
		<button type="submit" name="type" value="projects">Projects</button>
		<button type="submit" name="type" value="all_gadgets_zapps">Published Gadgets and Zapps</button>
		<button type="submit" name="type" value="project_history">Project History</button>
		<br/>
		<p />
		<h3>Shopp Statistics Report:</h3>
		<small>Note: Format and search options above apply below</small>
		<br />
		Select [<input type="radio" name="price" value="1" checked /> Non-Free | <input type="radio" name="price" value="0" /> Free ]:
		<select name="project_type">
			<option value="1">Gadgets</option>
			<option value="2">Zapps</option>
		</select>
		<select name="shopp_action">
			<option value="purchase">purchased</option>
			<option value="publish">published</option>
			<option value="delete">deleted</option>
		</select>
		<button type="submit" name="type" value="dashboard">Report*</button>
	</form>
	<hr />
	<jsp:include page="statistics.jsp" flush="true" />
</body>
</html>