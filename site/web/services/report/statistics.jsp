<%@page import="java.util.Date"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.alleni.db.util.QueryTools.Row"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.io.Writer"%>
<%@page import="com.alleni.db.util.QueryTools.FullTable"%>
<%@page import="com.alleni.db.util.QueryTools"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.io.StringWriter"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.CallableStatement"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.alleni.common.BaseDAO"%>
<%@page import="com.alleni.db.util.JDBCHelper"%>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="../messaging/adminheader.jsp" %>
<%

    String szBeginDate = request.getParameter("begin_date");
    String szEndDate = request.getParameter("end_date");
    
    // -- begin date validation
    boolean dateError = false;
    boolean dateRange = false;
    Date beginDate = null;
    Date endDate   = null;
    if (szBeginDate != null || szEndDate != null) {
    	dateRange = true;
    	if ( szBeginDate != null && szEndDate != null) {
    		try {
    			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
    			beginDate = sdf.parse(szBeginDate);
    			endDate = sdf.parse(szEndDate);
    			dateError = false;
    		} catch (ParseException e) {
    			dateError = true;
    		}
    	} else {
    		dateError = false;
    	}
    } else {
    	szBeginDate = "mm-dd-yyyy"; // could overwrite what they wrote
    	szEndDate = "mm-dd-yyyy";  // if only one is set
    }
    // -- end date validation
    

	
	String allActiveAccounts = "0";
	String allFreeTrials = "0";
    String freeTrialsInRange = "0";

    String totalPaidCreators = "0";
    String paidCreatorsInRange = "0";

	String totalProAccounts = "0";
    String paidProInRange = "0";

	String totalCollectorAccounts = "0";
    String collectorsInRange = "0";

	
	String lapsedAccounts = "0";
	String lostConversions = "0";
	String newAccountsInRange = "0";

	String sqlActiveAccounts = " select " + 
		" count(*) from accounts where termduedatetime >= now();";
		
	String sqlFreeTrials =  "select " +
	  		  "	count (distinct accounts.accountid) " +
	  		  " from accounts " + 
	  		  " where accounts.accountid not in " +
	  		  "( " +
	  		  "	select distinct accountid from invoices " +
	  		  "			where description like '%Subscription%' " +
	  		  ")" +
	  		  " and accounts.typeId = 2 " +
	  		  " and accounts.termduedatetime >= now();";

	String sqlFreeTrialsInRange =  "select " +
	  		  "	count (distinct accounts.accountid) " +
	  		  " from accounts " + 
	  		  " where accounts.accountid not in " +
	  		  "( " +
	  		  "	select distinct accountid from invoices " +
	  		  "			where description like '%Subscription%' " +
	  		  ")" +
	  		  " and accounts.typeId = 2 " +
	  		  " and accounts.editedDateTime >= ? " +
	  		  " and accounts.editedDateTime <= ?";

	  		  
	  		  		 
	  	  		  

	String sqlAccountsByType = "select " +
	  		  "	count (distinct accounts.accountid) " +
	  		  " from accounts " + 
	  		  " where  " +
	  		  " accounts.typeId = ? \n " +
	  		  " and accounts.accountid in " +
	  		  "( " +
	  		  "	select distinct accountid from invoices " +
	  		  "			where description like '%Subscription%' " +
	  		  ") " +
	  		  " and accounts.termduedatetime >= now();";
	  		  
	String sqlAccountsByTypeInRange = "select " +
	  		  "	count (distinct accounts.accountid) " +
	  		  " from accounts " + 
	  		  " where  " +
	  		  " accounts.typeId = ? \n " +
	  		  " and accounts.accountid in " +
	  		  "( " +
	  		  "	select distinct accountid from invoices " +
	  		  "			where description like '%Subscription%' " +
	  		  ")	 " +
	  		  " and accounts.editedDateTime >= ? " +
	  		  " and accounts.editedDateTime <= ?";
	  		  
	  		  
	String sqlLapsedAccounts = "select count(distinct accounts.accountid) from accounts \n" +
				" where termDueDateTime < now();";
	
	String sqlNewAccountsInRange = "select count(distinct accounts.accountid) from accounts \n" +
		" where accounts.editedDateTime >= ? " +
  		" and accounts.editedDateTime <= ?";
			
	String sqlLostConversions =  "select " +
  		  "	count (distinct accounts.accountid) " +
  		  " from accounts " + 
  		  " where termduedatetime < now() " +
  		  " and typeId = 2 " +
  		  " and accounts.accountid not in " +
  		  "( " +
  		  "	select distinct accountid from invoices " +
  		  "			where description like '%Subscription%' " +
  		  ");";

	
	final PrintWriter o = new PrintWriter(out);
	QueryTools.DbCon con = new QueryTools.DbCon(
			new QueryTools.DbCon.MessageHandler() {
				public void handleMessage(String msg) {
					o.println(msg);
				}
			});
	
	try {
		List<FullTable> tables = con.query(sqlFreeTrials, null);
		FullTable table = tables.get(0);
		Row row = table.get(0);
		allFreeTrials = (String)row.get("count");
		
		tables = con.query(sqlActiveAccounts, null);
		table = tables.get(0);
		row = table.get(0);
		allActiveAccounts = (String)row.get("count");

		tables = con.query(sqlAccountsByType, new Object[]{1});
		table = tables.get(0);
		row = table.get(0);
		totalCollectorAccounts = (String)row.get("count");

		tables = con.query(sqlAccountsByType, new Object[]{2});
		table = tables.get(0);
		row = table.get(0);
		totalPaidCreators = (String)row.get("count");
		
		tables = con.query(sqlAccountsByType, new Object[]{3});
		table = tables.get(0);
		row = table.get(0);
		totalProAccounts = (String)row.get("count");
		
		tables = con.query(sqlLapsedAccounts, null);
		table = tables.get(0);
		row = table.get(0);
		lapsedAccounts = (String)row.get("count");
		
		tables = con.query(sqlLostConversions, null);
		table = tables.get(0);
		row = table.get(0);
		lostConversions = (String)row.get("count");
		
		if (dateRange && !dateError) {
			
			tables = con.query(sqlFreeTrialsInRange,  new Object[]{beginDate, endDate});
			table = tables.get(0);
			row = table.get(0);
			sqlFreeTrialsInRange = (String)row.get("count");
			
			tables = con.query(sqlAccountsByTypeInRange, new Object[]{1, beginDate, endDate});
			table = tables.get(0);
			row = table.get(0);
			collectorsInRange = (String)row.get("count");
			
			tables = con.query(sqlAccountsByTypeInRange, new Object[]{2, beginDate, endDate});
			table = tables.get(0);
			row = table.get(0);
			paidCreatorsInRange = (String)row.get("count");

			tables = con.query(sqlAccountsByTypeInRange, new Object[]{3, beginDate, endDate});
			table = tables.get(0);
			row = table.get(0);
			paidProInRange = (String)row.get("count");

			tables = con.query(sqlNewAccountsInRange,  new Object[]{beginDate, endDate});
			table = tables.get(0);
			row = table.get(0);
			sqlNewAccountsInRange = (String)row.get("count");
		}
	} finally {
		con.close();
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<%
		/* from above
	String allActiveAccounts = "0";
	String allFreeTrials = "0";
    String freeTrialsInRange = "0";

    String totalPaidCreators = "0";
    String paidCreatorsInRange = "0";

	String totalProAccounts = "0";
    String paidProInRange = "0";

	String totalCollectorAccounts = "0";
    String collectorsInRange = "0";

	
	String lapsedAccounts = "0";
	String lostConversions = "0";
	String newAccountsInRange = "0";

		*/
%>
<h4> Account Statistics</h4>
	<table border="1">
		<th>Total Active Accounts</th>
		<th>Total Free Trials</th>
		<th>Free Trials In Range <small>[<%=szBeginDate %>] - [<%=szEndDate %>]</small></th>
		<th>Total Paid Creators</th>
		<th>Paid Creators In Range <small>[<%=szBeginDate %>] - [<%=szEndDate %>]</small></th>
		<th>Total Pro Accounts</th>
		<th>Pro Accounts In Range <small>[<%=szBeginDate %>] - [<%=szEndDate %>]</small></th>
		<th>Total Collectors Accounts</th>
		<th>Collector Accounts In Range <small>[<%=szBeginDate %>] - [<%=szEndDate %>]</small></th>
		<th>Total Lapsed Accounts</th>
		<th>Total Lost Conversions<br /><small>(free accts older than 60 days)</small></th>
		<th>New Accounts In Range <small>[<%=szBeginDate %>] - [<%=szEndDate %>]</small></th>
		<tr>
			<td><%= allActiveAccounts %></td>
			<td><%= allFreeTrials %></td>
			<td><%= freeTrialsInRange %></td>
			<td><%= totalPaidCreators %></td>
			<td><%= paidCreatorsInRange %></td>
			<td><%= totalProAccounts %></td>
			<td><%= paidProInRange %></td>
			<td><%= totalCollectorAccounts %></td>
			<td><%= collectorsInRange %></td>
			<td><%= lapsedAccounts %></td>
			<td><%= lostConversions %></td>
			<td><%= newAccountsInRange %></td>
		</tr>
	</table>
	<form action="statistics.jsp">
	<% if (dateError && dateRange) {%> <span style="color:red"><% }%>
		between  <input type="text" name="begin_date" size="8" value="<%=szBeginDate %>"/>
		and <input type="text" name="end_date" size="8" value="<%=szEndDate %>"/> 
		<% if (dateError && dateRange) {%></span><%} %>
	<input type="submit" value="refresh" />
	</form>
</body>
</html>