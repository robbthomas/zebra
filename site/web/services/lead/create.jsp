<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="com.alleni.util.Mail" %>
<%@ page import="com.alleni.template.ClosureUtil" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject createValues = json.getJSONObject("leadCreate");

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        // assume pre-release splash page submits without credentials so add some -- will use
        // the ecommerce superuser for now until another one can be added specific to leads gathering
        if (createValues.getString("leadSource").startsWith("Pre-release Splash"))
            credentials = new JSONObject().put("email","eCommCron@zebrazapps.com").put("password","o4vBnE4Q11");

        reply = com.alleni.member.Lead.create(createValues, credentials);

        String email = "";
        String firstname = "";
        String lastname = "";

        if (reply.getBoolean("success"))
        {
            int leadId = reply.getJSONObject("data").getInt("leadId");
            JSONArray leadItems = createValues.getJSONArray("leadItems");
            for (int i = 0; i < leadItems.length(); i++)
            {
                JSONObject item = leadItems.getJSONObject(i);
                java.util.Iterator it = item.keys();
                String key = it.next().toString();
                JSONObject itemValues = new JSONObject().put("item",key).put("value",item.getString(key)).put("leadId", leadId);

                if (key.equalsIgnoreCase("email")) email = item.getString(key);
                else if (key.equalsIgnoreCase("firstname")) firstname = item.getString(key);
                else if (key.equalsIgnoreCase("lastname")) lastname = item.getString(key);

                reply = com.alleni.member.LeadItemValue.create(itemValues, credentials);
            }
        }

        // send an email to person submitting lead information
        if (email.length() > 0 && firstname.length() > 0 && lastname.length() > 0)
        {
            String to = email;
            Map<String, Object> args = new HashMap<String,Object>();
            args.put("firstName", firstname);
            args.put("lastName", lastname);
            
            String plaintextBody = ClosureUtil.render("leadCreate.soy", "services.lead.create", "plaintext", args);
            String htmlBody = ClosureUtil.render("leadCreate.soy", "services.lead.create", "html", args);
            String subject = ClosureUtil.render("leadCreate.soy", "services.lead.create", "subject", args);

            Mail mail = new Mail();
            mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.ecommerce"));
            mail.sendMultiPartMessage("donotreply@zebrazapps.com", to, subject, plaintextBody, htmlBody);
        }

    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>