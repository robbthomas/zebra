<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.alleni.util.Utility"%>
<%@page import="com.alleni.project.ZappStatistics"%>
<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.List"%>
<%@page import="java.text.NumberFormat"%>
<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%
    JSONObject reply = null;
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject getValues = json.getJSONObject("accountGet");

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        boolean error = false;
        boolean wrapInGroupObject = true;
        String group = getValues.getString("group");
        JSONObject values = new JSONObject();

        if (group.equals("myProfile"))
        {
            JSONObject retVal = com.alleni.common.BaseBean.getAppUserFromCredentials(credentials);
            JSONObject phone  = com.alleni.common.BaseBean.getPhoneForUser(credentials, "primary");

            if (!retVal.getBoolean("success"))
            {
                error = true;
            }
            else
            {
                values = retVal.getJSONObject("data");
                if (phone != null && phone.has("number")) {
                	values.put("phone", phone.getString("number"));
                	values.put("telephoneId", phone.get("id"));
                }
            }
        }

        else if (group.equals("usernameEmailPassword"))
        {
            JSONObject retVal = com.alleni.common.BaseBean.getAppUserFromCredentials(credentials);
            if (!retVal.getBoolean("success"))
            {
                error = true;
            }
            else
            {
                values = retVal.getJSONObject("data");
            }
        }
        else if (group.equals("companyProfile"))
        {
            JSONObject address = com.alleni.common.BaseBean.getAddressForUser(credentials, "primary");
            JSONObject account = com.alleni.common.BaseBean.getAccountForUser(credentials);
            
            if (address != null) {
                values = address;
            }

            JSONObject personalWrapper = com.alleni.common.BaseBean.getAppUserFromCredentials(credentials);
            JSONObject personal = personalWrapper.getJSONObject("data");

            JSONObject phone  = com.alleni.common.BaseBean.getPhoneForUser(credentials, "primary");
            if (phone != null && phone.has("number")) {
                personal.put("phone", phone.getString("number"));
            }
            values.put("companySameAsPersonal",account.get("companySameAsPersonal"));
            values.put("personal", personal);

        }
        else if (group.equals("billing"))
        {
        	JSONObject account = com.alleni.common.BaseBean.getAccountForUser(credentials);
            JSONObject address = com.alleni.common.BaseBean.getAddressForUser(credentials, "billing");
            JSONObject creditCard = com.alleni.common.BaseBean.getCreditCardForUser(credentials);
            if (address != null && creditCard != null) {
                values = address;
                Utility.appendNamedJSONValues(values, creditCard, new String[]{"firstName", "lastName", "email"});
            }

            JSONObject primary = com.alleni.common.BaseBean.getAddressForUser(credentials, "primary");
            JSONObject personalWrapper = com.alleni.common.BaseBean.getAppUserFromCredentials(credentials);
            JSONObject personal = personalWrapper.getJSONObject("data");

            JSONObject phone = com.alleni.common.BaseBean.getPhoneForUser(credentials, "primary");
            if (phone != null && phone.has("number")) {
                personal.put("phone", phone.getString("number"));
            }
			
            values.put("billingContactSameAsPersonal",account.get("billingContactSameAsPersonal"));
            values.put("billingAddressSameAsCompany",account.get("billingAddressSameAsCompany"));
            values.put("personal", personal);
            values.put("primary", primary);

        }
        else if (group.equals("paymentMethod"))
        {
            JSONObject creditCard = com.alleni.common.BaseBean.getCreditCardForUser(credentials);
            JSONObject user = com.alleni.common.BaseBean.getAppUserFromCredentials(credentials).getJSONObject("data");
            if (creditCard != null) {
                values = creditCard;
                values.put("expirationMonthYear", values.get("expirationMonth") + " / " + values.get("expirationYear"));
                values.put("allowSubscriptionPayment", user.get("allowsubscriptionpayment"));
            }
        }
        else if (group.equals("statistics"))
        {
            JSONObject zs = ZappStatistics.get(credentials);
            values = zs.getJSONArray("statistics").getJSONObject(0);
        }
        else if (group.equals("convertEarnings") || group.equals("plansPricing") || group.equals("cancel") || group.equals("hold") || group.equals("seller"))
        {
            JSONObject accountType = com.alleni.common.BaseBean.getAccountTypeForUser(credentials);
            JSONObject account = com.alleni.common.BaseBean.getAccountForUser(credentials);

            int price = accountType.getInt("price");

            Utility.appendNamedJSONValues(values, account, new String[]{"convertMethod", "showCompanyInfoForAll", "paypalLogin"});

            values.put("convertAmount", String.format("%.2f", (float)(account.getInt("convertAmount") / 100.0)));

            if (price == 0) {
                values.put("accountCancelDate",     "free account");
                values.put("convertCreditBalance",  "free account");
                values.put("accountType",           "free account");
                values.put("accountFee",            "free account");
                values.put("accountRefreshDate",    "free account");
            }
            else {
                String accountFee = "$" + String.format("%.2f", (float)(accountType.getInt("price") / 100.0)) + " / " + accountType.getString("terms");

                Calendar expiration = Calendar.getInstance();
                String dateString = account.getString("termDueDateTime");
                expiration.set(
                    Integer.parseInt(dateString.substring(0, 4)),
                    Integer.parseInt(dateString.substring(5, 7)),
                    Integer.parseInt(dateString.substring(8, 10)));

                String expirationString = DateFormat.getDateInstance().format(expiration.getTime());

                expiration.add(Calendar.DATE, 1);

                String renewString = DateFormat.getDateInstance().format(expiration.getTime());

                values.put("accountCancelDate",     expirationString);
                values.put("accountType",           accountType.getString("name"));
                values.put("accountFee",            accountFee);
                values.put("accountRefreshDate",    renewString);
            }
            
            JSONObject zs = ZappStatistics.get(credentials);
            JSONArray zsa = zs.getJSONArray("statistics");
            if (zsa.length() > 0) {
                zs = zsa.getJSONObject(0);
                Utility.appendNamedJSONValues(values, zs, new String[]{"zappsPublished","gadgetsPublished","convertedCreditBalance"});

                String earnings = String.format("%.2f", (float)(zs.getInt("totalRevenue") / 100.0));
                values.put("earnings", earnings);
            }
            else {
                values.put("zappsPublished", "0");
                values.put("gadgetsPublished", "0");
                values.put("convertedCreditBalance", "0");
                values.put("earnings", "0.00");
            }
        }
        else {
            reply = com.alleni.common.BaseBean.error(request.getRequestURI(), "unknown group:"+group);
        }

        if (error)
        {
            reply = com.alleni.common.BaseBean.error(request.getRequestURI(), "unknown error in group:"+group);
        }
        else
        {
            if (wrapInGroupObject)
            {
                JSONObject o = new JSONObject();
                o.put(group, values);
                reply = com.alleni.common.BaseBean.success(o);
            }
            else
                reply = com.alleni.common.BaseBean.success(values);
        }
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>