<%@page import="com.alleni.finance.Invoice"%>
<%@page import="com.alleni.common.BaseBean"%>
<%@page import="com.alleni.member.AppUser"%>
<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="com.alleni.util.Utility"%>
<%@ page import="com.qp.onlinecommerce.CreditCardAccount" %>
<%@ page import="java.util.Iterator"%>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="com.alleni.util.Mail" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.alleni.coupon.CouponCode"%>
<%@page import="com.alleni.coupon.CouponCodeDao"%>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.alleni.util.Mail" %>
<%@ page import="com.alleni.template.ClosureUtil" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>

<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject updateValues = json.getJSONObject("accountUpdate");

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        //reply = com.alleni.account.Account.update(updateValues, credentials);

        String group = updateValues.getString("group");
        reply = com.alleni.common.BaseBean.success(new JSONObject("{\"group\": \""+group+"\"}"));

        JSONObject values = updateValues.getJSONObject("values");

        if (group.equals("billing"))
        {
            int accountId = com.alleni.common.BaseBean.getAccountIdForUser(credentials);
            JSONObject accountValues = new JSONObject();
            // Update address record
            JSONObject      address = com.alleni.common.BaseBean.getAddressForUser(credentials, "billing");
            values.put("accountId", accountId);
            if (address == null) {
                values.put("type", "billing");
                com.alleni.account.Address.create(values, credentials);
            }
            else {
                values.put("id", address.getString("addressId"));
                com.alleni.account.Address.update(values, credentials);
            }

            // update account
            accountValues.put("accountId", accountId);
            accountValues.put("billingContactSameAsPersonal", (!values.isNull("billingContactSameAsPersonal") && values.get("billingContactSameAsPersonal").equals("on"))? 1:0);
            accountValues.put("billingAddressSameAsCompany", (!values.isNull("billingAddressSameAsCompany") && values.get("billingAddressSameAsCompany").equals("on"))? 1:0);
            com.alleni.account.Account.update(accountValues, credentials);

            // update credit card record
            JSONObject creditCard = com.alleni.common.BaseBean.getCreditCardForUser(credentials);
            if (creditCard == null) {
                if (values.has("type")) {
                    values.remove("type");
                }
//                com.alleni.finance.CreditCard.create(values, credentials);
            }
            else {
                values.put("creditCardId", creditCard.getString("creditCardId"));
                com.alleni.finance.CreditCard.update(values, credentials);
            }
            
            if(values.has("phone"))
            {
                com.alleni.common.BaseBean.updatePhoneNumber(values.getString("phone"), "billing", accountId, credentials);
            }
            
        }
        else if (group.equals("usernameEmailPassword"))
        {
            if (values.has("newEmail")) {
                values.put("id", com.alleni.common.BaseBean.getAppUserIdFromCredentials(credentials));

                values.put("email", values.getString("newEmail"));
                //values.put("displayName", values.getString("newEmail"));
                com.alleni.member.AppUser.update(values, credentials);     
            }
            if (values.has("newPassword")) {
                values.put("id", com.alleni.common.BaseBean.getAppUserIdFromCredentials(credentials));
                com.alleni.member.AppUser.updatePassword(values, credentials);
            }
        }
        else if (group.equals("upgrade"))
        {
        	JSONObject memberValues = com.alleni.member.AppUser.login(credentials);
            if (memberValues.getBoolean("success")) {
                memberValues = memberValues.getJSONObject("data");
            }
            
            boolean sendUpgradeNotification = false;
            
            if (updateValues.has("type"))
            {
            // simple upgrade
            long accountId = com.alleni.common.BaseBean.getAccountIdForUser(credentials);

                // retrieve current account information
                JSONObject account = new JSONObject();
                account.put("accountId", accountId);
                account = com.alleni.account.Account.find(account, credentials);
            
                if (account.getBoolean("success"))
                {
                    // create new JSONObject just for account update settings so we only change what we want to change
                    JSONObject accountUpdate = new JSONObject();
                    accountUpdate.put("accountId", accountId);

                    // get current date to use in making termDueDateTime comparisons
                    Calendar now = Calendar.getInstance();

                    account = account.getJSONObject("data");
                    int lastTypeId = account.getInt("typeId");
                    String lastTermDueDate = account.getString("termDueDateTime");
                    SimpleDateFormat incommingFormatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date lastForCompare = incommingFormatter.parse(lastTermDueDate);

                    // if upgrading from collector account, and termduedate has not been set to future (i.e. coupon)
                    // then set termDueDateTime to now
                    if (lastTypeId == 1 && lastForCompare.before(now.getTime()))
                    {
                    	int hour = Calendar.HOUR;
                        now.add(hour,24);
                        SimpleDateFormat formatter = new SimpleDateFormat("M/d/yyyy");
                        accountUpdate.put("termDueDateTime", formatter.format(now.getTime()));
                    }
                    String newType = updateValues.getString("type");
                    accountUpdate.put("type", newType);

                    int newTypeId = ("pro".equalsIgnoreCase(newType)) ? 3 : ("creator".equalsIgnoreCase(newType)) ? 2 : 1;
                    accountUpdate.put("typeId", newTypeId);

                    if (newTypeId != lastTypeId) {
                        sendUpgradeNotification = true;
                    }
                    
                    // get old and new account types pricing information to determine how and what to invoice
                    // proposed scheme involves just leaving the termDueDate as is and billing or crediting
                    // a prorated amount based on remaining subscription term...but how do we know if a coupon
                    // was applied???

                    int newTypePrice = 0;
                    int lastTypePrice = 0;
                    JSONArray accountTypes = com.alleni.account.AccountType.findAll(credentials).getJSONArray("accountTypes");
                    for (int index = 0; index < accountTypes.length(); index++)
                    {
                        JSONObject type = accountTypes.getJSONObject(index);
                        if (type.getInt("id") == newTypeId) newTypePrice = type.getInt("price");
                        else if (type.getInt("id") == lastTypeId) lastTypePrice = type.getInt("price");
                    }

                    // if not a free account, verify account has valid credit card
                    if (newTypePrice > 0)
                    {
                        int goodStanding = account.getInt("goodStanding");
                        JSONObject creditCards = com.alleni.finance.CreditCard.findValidCreditCardsForAccount(accountId, credentials);
                        if (goodStanding == 1 && creditCards.getBoolean("success"))
                        {
                            // if account has ANY credit card that has not expired, assume good standing
                            // and this will get checked normally following the next purchase
                            if (creditCards.getJSONObject("data").getJSONArray("creditCards").length() > 0)
                                goodStanding = 1;
                        }
                        if(goodStanding == 0) {
                            throw new Exception("Attempted to change account type to a paid account with not good standing or no valid credit card.");
                        }
                    }

                    reply =  com.alleni.account.Account.update(accountUpdate, credentials);

                    JSONObject invoices = Invoice.cancelPendingSubscriptionInvoices(credentials);
                    
                    // Send email notification of account upgrade (if account type has changed)
                    if (sendUpgradeNotification) {
                        String to = credentials.getString("email");
                        String url = request.getScheme() + "://" + request.getServerName();
                        
                        Calendar emailDate = Calendar.getInstance();
                        SimpleDateFormat emailDateFormatter = new SimpleDateFormat("M/d/yyyy");
                        
                        Map<String, Object> args = new HashMap<String,Object>();
                        args.put("firstName", memberValues.getString("firstName"));
                        args.put("lastName", memberValues.getString("lastName"));
                        args.put("url", url);
                        args.put("accountType", newType);
                        args.put("formattedStartDate", emailDateFormatter.format(emailDate.getTime()));
                        
                        String plaintextBody = ClosureUtil.render("upgradeAccount.soy", "services.account.updateExtended.accountUpgrade", "plaintext", args);
                        String htmlBody = ClosureUtil.render("upgradeAccount.soy", "services.account.updateExtended.accountUpgrade", "html", args);
                        String subject = ClosureUtil.render("upgradeAccount.soy", "services.account.updateExtended.accountUpgrade", "subject", args);
    
                        Mail mail = new Mail();
                        mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.ecommerce"));
                        mail.sendMultiPartMessage("donotreply@zebrazapps.com", to, subject, plaintextBody, htmlBody);                
                    }   
                }
            }
        }
        else if (group.equals("buyCredits"))
        {
        }
        else if (group.equals("cancel"))
        {
        }
        else if (group.equals("companyProfile"))
        {
            int accountId = com.alleni.common.BaseBean.getAccountIdForUser(credentials);
            JSONObject address = com.alleni.common.BaseBean.getAddressForUser(credentials, "primary");
            JSONObject accountValues = new JSONObject();
            values.put("accountId", accountId);
            if (address == null) {
                values.put("type", "primary");
                com.alleni.account.Address.create(values, credentials);
            }
            else {
                values.put("id", address.getString("addressId"));
                com.alleni.account.Address.update(values, credentials);
            }
            
            // update account
            accountValues.put("accountId", accountId);
            accountValues.put("companySameAsPersonal", (!values.isNull("companySameAsPersonal") && values.get("companySameAsPersonal").equals("on"))? 1:0);
            com.alleni.account.Account.update(accountValues, credentials);
            if(!values.isNull("companySameAsPersonal") && values.get("companySameAsPersonal").equals("off")){
            	com.alleni.common.BaseBean.updatePhoneNumber(values.getString("phone"), "billing", accountId, credentials);
            }
        }
        else if (group.equals("contact"))
        {
        }
        else if (group.equals("convertEarnings"))
        {
            JSONObject account = com.alleni.common.BaseBean.getAccountForUser(credentials);

            String convertAmount = values.getString("convertAmount");
            int convertInt = Utility.cleanCurrencyString(convertAmount);

            JSONObject accountCopy = new JSONObject();
            accountCopy.put("accountId", account.get("accountId"));
            String convertMethod = values.has("convertMethod") ? values.getString("convertMethod") : "convert_method_convert_all";
            accountCopy.put("convertMethod", convertMethod);
            accountCopy.put("convertAmount", convertInt);

            com.alleni.account.Account.update(accountCopy, credentials);

            // email zebra support
            String to = "support@zebrazapps.com";
            String subject = "Zebra Zapps convert earnings ("+account.get("accountId")+")";
            String body = "";
            body += "Convert Earnings Request:";
            body += "\nmember="+credentials.get("email");
            body += "\naccountId="+account.get("accountId");
            body += "\nconvertAmount="+convertInt;
            body += "\nconvertMethod="+convertMethod;

            Mail mail = new Mail();
            mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.ecommerce"));
            mail.sendMultiPartMessage("donotreply@zebrazapps.com", to, subject, body, null);
        }
        else if (group.equals("emailPassword"))
        {
        }
        else if (group.equals("expertProfile"))
        {            
        }
        else if (group.equals("hold"))
        {
        }
        else if (group.equals("members"))
        {
        }
        else if (group.equals("myProfile"))
        {
            String appUserId = com.alleni.common.BaseBean.getAppUserIdFromCredentials(credentials);
            values.put("id", appUserId);
            com.alleni.member.AppUser.update(values, credentials);

            int accountId = com.alleni.common.BaseBean.getAccountIdForUser(credentials);
            com.alleni.common.BaseBean.updatePhoneNumber(values.getString("phone"), "primary", accountId, credentials);
        }
        else if (group.equals("paymentHistory"))
        {
        }
        else if (group.equals("paymentOptions"))
        {
        }
        else if (group.equals("paymentMethod"))
        {
            boolean validCreditCard = false;
            
            // get member settings
            JSONObject memberValues = com.alleni.member.AppUser.login(credentials);
            if (memberValues.getBoolean("success")) {
                memberValues = memberValues.getJSONObject("data");
            }

            // get account settings
            int accountId = com.alleni.common.BaseBean.getAccountIdForUser(credentials);
            JSONObject accountValues = new JSONObject().put("accountId", accountId);
            reply = com.alleni.account.Account.find(accountValues, credentials);

            if (reply.getBoolean("success")) accountValues = reply.getJSONObject("data");

            String merchantCustomerId = accountValues.has("merchantCustomerId") ? accountValues.getString("merchantCustomerId") : "";

            // if credit card info is provided
            if (!values.optString("creditCardNumber","").equals("") && !values.isNull("creditCardNumber"))
            {
                // capture current credit card information so we can retire it later IF new credit card validates
                JSONObject currentCreditCard = com.alleni.common.BaseBean.getCreditCardForUser(credentials);
    
                // retire current credit card entry for this account
                if (currentCreditCard != null) {
                    reply = com.alleni.finance.CreditCard.retire(new JSONObject().put("id",currentCreditCard.getInt("creditCardId")), credentials);
                }
                
                // get all addresses for the account and pick one to use as the billing address when validating credit card with Authnet
                JSONObject range = new JSONObject().put("offset",0).put("count",-1);
                JSONObject filters = new JSONObject().put("accountId",accountId);
                reply = com.alleni.account.Address.filter(new JSONObject().put("filters",filters), range, credentials);
                JSONArray addresses = reply.getJSONObject("data").has("addresses") ? reply.getJSONObject("data").getJSONArray("addresses") : new JSONArray();

                JSONObject billingAddress = new JSONObject();
                for (int i = 0; i < addresses.length(); i++)
                {
                    int typeId = addresses.getJSONObject(i).getInt("typeId");
                    if (typeId == 4) // billing
                    {
                        // found explicit billing address, so grab it and end for loop
                        billingAddress = addresses.getJSONObject(i);
                        break;
                    }
                    else if (typeId == 1) // primary
                    {
                        // found primary address, so hang on but keep looping through address -- we hang on to it
                        // because might settle for it if we don't find an explicit billing address
                        billingAddress = addresses.getJSONObject(i);
                    }
                }

                int billingAddressId = billingAddress.has("addressId") ? billingAddress.getInt("addressId") : -1;
                
                // create empty billing address if no address on file
                if(billingAddressId < 0){
                	 JSONObject addressValues = new JSONObject();
                     addressValues.put("type", "billing");
                     addressValues.put("accountId", accountId);
                     reply = com.alleni.account.Address.create(addressValues, credentials);
                     addressValues = reply.getJSONObject("data");
                     billingAddressId = addressValues.getInt("addressId");
                }

                // create new credit card entry
                int creditCardId = -1;
                if (billingAddressId > 0)
                {
                    // TBD: change payment method form element names to avoid following remapping
                    values.put("accountId", accountId);
                    values.put("addressId", billingAddressId);
                    values.put("firstName", memberValues.getString("firstName"));
                    values.put("lastName", memberValues.getString("lastName"));
                    values.put("nameOnCreditCard", values.getString("nameOnCreditCard"));
                    values.put("creditCardNumber",values.getString("creditCardNumber"));
                    values.put("maskedNumber", com.alleni.finance.CreditCard.maskNumber(values.getString("creditCardNumber")));
                    values.put("expirationMonth", values.getString("expirationMonth").startsWith("0") ? values.getString("expirationMonth").substring(values.getString("expirationMonth").length() - 1) : values.getString("expirationMonth"));
                    values.put("expirationYear", values.getString("expirationYear"));
                    values.put("securityCode", values.getString("securityCode"));

                    if (currentCreditCard != null) {
                    	values.put("customerProfileId",currentCreditCard.getString("customerProfileId"));

                    }

                    
                    
                    reply = com.alleni.finance.CreditCard.create(values, credentials);

                    if (reply.getBoolean("success")) creditCardId = reply.getJSONObject("data").getInt("creditCardId");
                }

                // if have credit card, set up customer and payment profile with authorize.net and validate credit card
                if (creditCardId > 0)
                {
                	String memberId = memberValues.getString("id");

                    JSONObject profile = new JSONObject();
                    profile.put("merchantCustomerId", merchantCustomerId);
                    profile.put("memberId", memberId);
                    profile.put("email", memberValues.getString("email"));
                    profile.put("creditCard", values);

                    JSONObject billingInfo = new JSONObject();
                    billingInfo = com.alleni.account.Address.findByIdRaw(billingAddressId, credentials);
                    // note that following does not fit with creditCard.nameOnCreditCard which is a single field but Authorize.net is expecting 2 fields
                    billingInfo.put("firstName", memberValues.getString("firstName")).put("lastName", memberValues.getString("lastName"));
                    profile.put("billingInfo", billingInfo);

                    // create or update credit card profile as needed
                    JSONObject creditCard = new JSONObject();
                    CreditCardAccount cca = new CreditCardAccount();

                    if (currentCreditCard != null && currentCreditCard.getString("customerProfileId").length() > 0) {
                        profile.put("transactionReferenceId", "UPDATE "+memberId);
                        profile.put("customerProfileId",currentCreditCard.getString("customerProfileId"));
                        creditCard = cca.updateProfile(profile);
                    }
                    else {
                        profile.put("transactionReferenceId", "CREATE "+memberId);
                        creditCard = cca.createProfile(profile);
                    }

                    validCreditCard = creditCard.getBoolean("success");

                    if (validCreditCard)
                    {
                        // save profile Ids returned by CIM
                        creditCard.put("creditCardId", creditCardId);
                        reply = com.alleni.finance.CreditCard.update(creditCard, credentials);

                        // explicitly set Accounts.goodStanding=1 (just in case it gets set differently from default=1 somewhere along the line above)
                        JSONObject update = new JSONObject();
                        update.put("accountId", accountId);
                        update.put("goodStanding", 1);

                        Calendar now = Calendar.getInstance();
                        String lastTermDueDate = accountValues.getString("termDueDateTime");
                        SimpleDateFormat incommingFormatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date lastForCompare = incommingFormatter.parse(lastTermDueDate);

                        // if on a paid account, make sure the term date is set out so they get immediate access
                        // and are billed tonight just like on an upgrade from collector. This code is nearly identical to upgrade code.
                        if (accountValues.getInt("typeId") > 1 && lastForCompare.before(now.getTime()))
                        {
                            int hour = Calendar.HOUR;
                            now.add(hour,24);
                            SimpleDateFormat formatter = new SimpleDateFormat("M/d/yyyy");
                            update.put("termDueDateTime", formatter.format(now.getTime()));
                            JSONObject invoices = Invoice.cancelPendingSubscriptionInvoices(credentials);
                        }
                        JSONObject gd = com.alleni.account.Account.update(update, credentials);
                    }
                    else
                    {
                        // credit card did not validate so retire new entry
                        JSONObject retire = com.alleni.finance.CreditCard.retire(new JSONObject().put("id",creditCardId), credentials);

                        // do not set goodstanding to false
                        // leave it to whatever it currently has.
                        // If it was true then they will continue to be able to use the editor but purchases are stopped by not having a good credit card profile
                        // if it was false then fine

                        reply.put("data", new JSONObject());
                        reply.getJSONObject("data").put("cardError", creditCard.getString("statusReasonText"));
                    }
                }
                reply.getJSONObject("data").put("validCreditCard", validCreditCard);
            }
            
            // if coupon was provided, validate, extend termduedate, and retire coupon
            boolean couponSubmitted = false;
            boolean couponApplied = false;
            JSONObject accountValuesForCoupon = new JSONObject().put("accountId", accountId);
            if (!values.optString("couponCode","").equals("") && !values.isNull("couponCode"))
            {
                String couponCode = values.getString("couponCode");
                JSONObject range = new JSONObject().put("offset",0).put("count",-1);
                JSONObject filters = new JSONObject().put("code",couponCode);
                JSONObject couponResult = CouponCode.filter(new JSONObject().put("filters",filters), range, credentials);
                JSONArray coupons = couponResult.getJSONObject("data").has("couponCodes") ? couponResult.getJSONObject("data").getJSONArray("couponCodes") : new JSONArray();
                
                couponSubmitted = true;
                
                Calendar now = Calendar.getInstance();
                SimpleDateFormat formatter;
                formatter = new SimpleDateFormat("M/d/yyyy");
                
                if(coupons.length() > 0)
                {
                    JSONObject coupon = coupons.getJSONObject(0);
                    
                    if(coupon.has("couponCodeTypeId"))
                    {
                        switch(coupon.getInt("couponCodeTypeId"))
                        {
                        case 1:                         
                            now.add(Calendar.MONTH,1);
                            accountValuesForCoupon.put("termDueDateTime", formatter.format(now.getTime()));
                            break;
                        case 2:
                            now.add(Calendar.MONTH,2);
                            accountValuesForCoupon.put("termDueDateTime",formatter.format(now.getTime()));
                            break;
                        case 3:
                            now.add(Calendar.MONTH,3);
                            accountValuesForCoupon.put("termDueDateTime",formatter.format(now.getTime()));
                            break;
                        default:
                            
                            break;
                        }
                        couponApplied = true;
                    }
                }
                
                if(couponApplied){
                    // if you were already goodStanding true, then stay true
                    // if you were goodStanding false you must have a bad credit card or owe money
                    // so using a coupon for a new charge will not change this, so stay false
                    //accountValuesForCoupon.put("goodStanding", 1);
                    com.alleni.account.Account.update(accountValuesForCoupon, credentials);
                }
                
            }
            accountValues = com.alleni.account.Account.find(accountValues, credentials).getJSONObject("data");
            reply.getJSONObject("data").put("termDueDate",accountValues.getString("termDueDateTime"));
            reply.getJSONObject("data").put("couponSubmitted",couponSubmitted);
            reply.getJSONObject("data").put("couponApplied",couponApplied);
            

        }
        else if (group.equals("plansPricing"))
        {
        }
        else if (group.equals("portal"))
        {
        }
        else if (group.equals("seller"))
        {
            String      paypalLogin;
            paypalLogin = values.getString("paypalLogin");

            JSONObject account = com.alleni.common.BaseBean.getAccountForUser(credentials);

            JSONObject      accountCopy = new JSONObject();
            accountCopy.put("accountId", account.get("accountId"));
            accountCopy.put("paypalLogin", paypalLogin);

            com.alleni.account.Account.update(accountCopy, credentials);
        }
        else if (group.equals("statistics"))
        {
        }
        else // unknown group
        {
            reply = com.alleni.common.BaseBean.error(request.getRequestURI(), "unknown group:"+group);
        }
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>
