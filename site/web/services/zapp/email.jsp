<%@page import="com.alleni.util.Mail"%>
<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>

<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        String to = json.getString("to");
        String from = json.getString("from");
        String msgBody = json.getString("msg");
        msgBody = "<html><body>" + msgBody;
        msgBody = msgBody + "<br /><img src=\"https://zebrazapps.com/com/alleni/images/zlogo-28px-trans.png\" alt=\"zebra logo\"></image></body></html>";
        Mail m = new Mail();
        m.sendMultiPartMessage(from.toString(), to.toString(), "You have to see this Zapp!", msgBody.toString(),  msgBody.toString());
        
        reply.put("success", Boolean.TRUE);
        reply.put("data", "Sent Email");
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>