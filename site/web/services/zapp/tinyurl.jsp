<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.net.*" %>
<%@ page import="java.io.*" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>

<%

    String tinyUrl = "";
    JSONObject reply = new JSONObject();
    URL url = null;
    HttpURLConnection uc = null;
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        String projectId = json.getString("id");
        String idType = json.getString("idType");

        // setup the request
        // REQUEST URL should be replaced by the URL you need to request
        // For example: http://USERNAME.websitetoolbox.com/register/create_account
        // (Replace USERNAME with your Website Toolbox username. If you are using a managed domain or a subdomain, use that instead of USERNAME.websitetoolbox.com.)
        url = new URL("http://api.bitly.com/v3/shorten");
        uc = (HttpURLConnection)url.openConnection();
        uc.setDoOutput(true);
        uc.setDoInput(true);
        uc.setUseCaches(false);
        uc.setRequestMethod("GET");
        //uc.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

        String detailUrl = "";
        detailUrl += request.getScheme() + "://";
        if("localhost".equals(request.getServerName())) {
        	detailUrl += "zebrazapps.com";
        } else {
        	detailUrl += request.getServerName();
        }
        if(request.getServerPort() != 80 && request.getServerPort() != 443) {
        	detailUrl += ":" + request.getServerPort();
        }
        detailUrl += "/share/" + idType + "/" + projectId;
        
        detailUrl = URLEncoder.encode(detailUrl, "UTF-8");
        // add the query string
        // For example: String query = "apikey=APIKEY&username=joe&pw=secret";
        String query = "login=zebras&apikey=R_0e832a73116164b8ffef4c71c6182d39&longUrl="+detailUrl+"&format=json";
        PrintWriter pw = new PrintWriter(uc.getOutputStream());
        pw.print(query);
        pw.close();

        // get the input from the request
        uc.connect();
        BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
        JSONObject bitlyReply = new JSONObject(in.readLine());
        in.close();

        JSONObject data = bitlyReply.getJSONObject("data");
        tinyUrl = data.getString("url");
        reply.put("success", Boolean.TRUE);
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    finally
    {
        uc.disconnect();
    }

    reply.put("data", tinyUrl);

    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>
