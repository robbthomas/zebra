<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>

<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject listValues = json.getJSONObject("zappList");

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        String listType = listValues.getString("type");
        int listPage = listValues.getInt("page");
        int pageCount = listValues.getInt("count");

        if (listType.equals("featured"))
        {
            reply = com.alleni.common.BaseBean.mockdata("zappListFeatured.json");
        }
        else if (listType.equals("browse"))
        {
            reply = com.alleni.common.BaseBean.mockdata("zappListBrowse.json");
        }
        else
        {
            reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - unknown listType", "listType:"+listType);
        }
   }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));

%>