<%@page import="com.alleni.account.Account"%>
<%@page import="java.net.URL"%>
<%@page import="com.alleni.common.BaseBean"%>
<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONException" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="com.alleni.common.JsonError" %>
<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));

        JSONObject zappPurchase = json.getJSONObject("zappPurchase");
        int accountId = zappPurchase.getInt("accountId");
        JSONArray zapps = zappPurchase.getJSONArray("zappList");

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        // verify credentials exist -- NOTE that these checks overlap credentials checks in account lookup
        // below BUT will give more specific information regarding nature of an access error
        JSONObject member = com.alleni.member.AppUser.login(credentials);
        if (member.getBoolean("success")) member = member.getJSONObject("data");

        String memberId = member.has("id") ? member.getString("id") : "";

        if (memberId.length() == 0)
        {
            reply = com.alleni.common.BaseBean.error(request.getRequestURI(), "Invalid member credentials");
            response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
            return;
        }

        // verify credentials have access to accountId
        JSONObject findValues = new JSONObject();
        findValues.put("accountId", accountId);
        JSONObject account = com.alleni.account.Account.find(findValues, credentials);

        // if account find fails, credentials user does not have roles access to this account
        if (!account.getBoolean("success") || !account.has("data") || !account.getJSONObject("data").has("accountId") || account.getJSONObject("data").getInt("accountId") != accountId)
        {
            reply = com.alleni.common.BaseBean.error(request.getRequestURI(), "Invalid account credentials");
            response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
            return;
        }

        account = account.getJSONObject("data");

        // prep flag to use below to allow them to acquire only free zapps/gadgets if account not in good standing
        boolean goodStanding = account.getInt("goodStanding") > 0;

        // prep flag to use below if total > $0
        boolean haveCreditCard = false;

        JSONObject range = new JSONObject("{offset:0, count:10, order:'expirationYear:d,expirationMonth:d'}");
        JSONObject filters = new JSONObject().put("accountId",account.getInt("accountId"));
        JSONObject creditCards = com.alleni.finance.CreditCard.filter(new JSONObject().put("filters",filters), range, credentials);

        if (creditCards.getBoolean("success")) creditCards = creditCards.getJSONObject("data");

        // check for a card that has not expired
        java.util.Calendar cal = java.util.Calendar.getInstance();
        int currentMonth = cal.get(java.util.Calendar.MONTH) + 1; // Calendar months 0-11
        int currentYear = cal.get(java.util.Calendar.YEAR);

        JSONObject creditCard = new JSONObject();
        JSONArray cards = creditCards.getJSONArray("creditCards");
        for (int i = 0; i < cards.length() && !haveCreditCard; i++)
        {
            int expirationYear = 0;
            int expirationMonth = 0;
            creditCard = cards.getJSONObject(i);
            try {
                expirationMonth = Integer.parseInt(creditCard.getString("expirationMonth"));
                expirationYear = Integer.parseInt(creditCard.getString("expirationYear"));
            }
            catch (NumberFormatException nfe) {
                continue; // problems with this credit card record
            }

            haveCreditCard = (expirationYear > currentYear) || (expirationYear == currentYear && expirationMonth >= currentMonth);
            if (haveCreditCard) break; // dwh:  prevent overwriting a good CC status w/ an invalid one down the list
        }
       
        
        
        
        int invoiceId = 0;
        JSONObject invoice = null;
        int lineNumber = 0;
        int invoiceCachedTotal = 0;

        JSONArray preownedZapps = new JSONArray();
        JSONArray newZapps = new JSONArray();
        JSONArray errors = new JSONArray();

        for (int i = 0; i < zapps.length(); i++)
        {
            JSONObject zapp = zapps.getJSONObject(i);
            String projectId = zapp.has("projectId") ? zapp.getString("projectId") : null;

            // verify zapp has projectId
            if (projectId == null) continue;

            // verify valid not retired projectId
            // proxy over to zephyr for new Project api
            URL url = new URL(request.getRequestURL().toString());
            String username = member.getString("displayName");
            String password = credentials.getString("password");
            String serverName = request.getServerName();
            String s = com.alleni.common.BaseBean.proxy(username, password, serverName, "/zephyr/project/?projectId=" + projectId + "&published=true",
            		url.getProtocol(), "get", null);
            JSONObject project = com.alleni.common.BaseBean.success(new JSONObject(s));

            // verify valid not retired projectId
            if (!project.getBoolean("success")) continue; // TBD: append error to reply???
            project = project.getJSONObject("data").getJSONArray("projects").getJSONObject(0);
            
            // don't allow purchase of retired Published rows
            // DWH:  no need to check, since search filters those rows now
            
            // check if account already has this zapp/gadget
            boolean exists = project.getBoolean("owned");
            if (exists)
            {
                zapp.put("accountProjectNumberId", project.getString("apnUUID"));
                preownedZapps.put(zapp);
                continue;
            }

            // Private-publishing: if the Zapp/Gadget's creator is not a Pro account, 
            // do not allow it to be purchased
            Integer publishAccountId = project.getInt("accountId");
            JSONObject accountFilter = new JSONObject();
            accountFilter.put("accountId", publishAccountId);
            accountFilter.put("retired", 0);
            // Use admin credentials to look up Zapp/Gadget account since belongs to someone else.
            // TBD: Pull admin credentials from a configuration file so we do not hardcode here!
            JSONObject pAccount = Account.find(accountFilter, new JSONObject().put("email","eCommCron").put("password","o4vBnE4Q11"));
            if (project.getBoolean("hideInStoreList") && pAccount.getInt("typeId") != 3) {
            	zapp.put("error", "ERR_NOTPROZAPP");
            	errors.put(zapp);
            	continue;
            }
            
            // if Zapp/Gadget not free, verify account is in good standing
            int price = project.getInt("price");
            if (price > 0 && !(goodStanding && haveCreditCard))
            {
            	// TODO SBJ - get team together to discuss difference between errors
            	// and acceptable processing responses.  If we choose to keep these errors,
            	// then we need to come up with a framework for error codes that the 
            	// client can handle.  I would argue that this is not an error. 
            	// Errors should be unanticipated system failures.  Unanticipated 
            	// failures should be handled globally by the client.
            	//
            	// DWH:  Server project has a JSON based error code system.  Import it?
                zapp.put("error", "ERR_NOTGOODSTANDING");
                errors.put(zapp);
                continue;
            }

            // check if account has any occurrence of firstPublishId for this zapp/gadget and,
            // if so, do we create a new AccountProjectNumber entry for this specific zapp/gadget
            // before "continue"???

            // zapp/gadget not in list for this account, create a new invoice if one not already open
            if (invoiceId <= 0)
            {
                zapp.put("description", "Zapp/Gadget purchase");
                zapp.put("state", "invoice_state_pending");
                zapp.put("currencyTypeId", 1);
                zapp.put("accountId", accountId);
                invoice = com.alleni.finance.Invoice.create(zapp, credentials);

                if (invoice.getBoolean("success")) invoice = invoice.getJSONObject("data");
                invoiceId = invoice.has("invoiceId") ? invoice.getInt("invoiceId") : 0;

                if (invoiceId <= 0)
                {
                    reply = com.alleni.common.BaseBean.error(request.getRequestURI(), "Failed to create an invoice");
                    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
                    return;
                }

            }

            // create an invoice line item for the zapp/gadget
            String description = project.getString("publishedName") + " " + project.getString("versionString");
            zapp.put("invoiceId", invoiceId);
            zapp.put("description", description);
            zapp.put("lineNumber", ++lineNumber);
            zapp.put("quantity", 1);
            zapp.put("amountTotal", price);
            invoiceCachedTotal += price;

            JSONObject invoiceLineItem = com.alleni.finance.InvoiceLineItem.create(zapp, credentials);

            if (invoiceLineItem.getBoolean("success")) invoiceLineItem = invoiceLineItem.getJSONObject("data");
            int invoiceLineItemId = invoiceLineItem.has("invoiceLineItemId") ? invoiceLineItem.getInt("invoiceLineItemId") : 0;

            if (invoiceLineItemId <= 0) {
            	// dwh:  note error and move on
                zapp.put("error", "Problem creating invoice line-item. Cannot purchase this item.");
                errors.put(zapp);
                continue;
            }
        
 			if (zapp.has("errors") == false  ) {

 				// create AccountProjectNumbers entry for the zapp/gadget
	            zapp.put("invoiceLineItemId", invoiceLineItemId);
	            JSONObject accountProjectNumber = com.alleni.project.AccountProjectNumber.create(zapp, credentials);

	            if (accountProjectNumber.getBoolean("success")) accountProjectNumber = accountProjectNumber.getJSONObject("data");
	            String accountProjectNumberId = accountProjectNumber.has("accountProjectNumberId") ? accountProjectNumber.getString("accountProjectNumberId") : "";
            
	            if (accountProjectNumberId.length() <= 0) continue; // TBD: what to do if we fail here???

	            // add zapp/gadget to list of newly acquired items
	            zapp.put("accountProjectNumberId", accountProjectNumberId);
	            newZapps.put(zapp);
 			}
        }
        if(invoiceId > 0 && invoiceCachedTotal > 0) {
        	JSONObject updateParams = new JSONObject();
        	updateParams.put("id", invoiceId);
        	updateParams.put("cachedTotal", invoiceCachedTotal);
        	
        	invoice = com.alleni.finance.Invoice.update(updateParams, credentials);
            if (invoice.getBoolean("success")) invoice = invoice.getJSONObject("data");
        }

 
        // return details for account and newly acquired zapps/gadgets
        JSONObject data = new JSONObject();
        data.put("account", account);
        if (invoiceCachedTotal > 0)
        {
            data.put("invoiceTotal", invoiceCachedTotal);
            if (haveCreditCard) {
                JSONObject creditCardBrief = new JSONObject();
                creditCardBrief.put("nameOnCreditCard", creditCard.getString("nameOnCreditCard"));
                creditCardBrief.put("maskedNumber", creditCard.getString("maskedNumber"));
                creditCardBrief.put("expirationMonth", creditCard.getString("expirationMonth"));
                creditCardBrief.put("expirationYear", creditCard.getString("expirationYear"));
                data.put("creditCard",creditCardBrief);
            }
        }
        data.put("preownedZapps", preownedZapps);
        data.put("newZapps", newZapps);

        if (errors.length() > 0)
            data.put("errors", errors);

        reply = com.alleni.common.BaseBean.success(data);

    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));

%>