<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.json.JSONArray"%>
<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="com.alleni.feedback.UpdateRatingAndCache" %>




<%
    JSONObject reply = new JSONObject();
    try
    {
        int     ii, jj;

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        JSONObject  myAppUser = com.alleni.common.BaseBean.getAppUserFromCredentials(credentials);
        String      myId = (myAppUser == null) ? null : myAppUser.getJSONObject("data").getString("id");

        JSONObject  json = new JSONObject(request.getParameter("json"));
        JSONObject  ratingSummary = json.getJSONObject("zappCommentsRatings");
        JSONArray   idList = ratingSummary.getJSONArray("ids");
        JSONObject  prefs = ratingSummary.getJSONObject("comments");

        boolean     exclude = prefs.getBoolean("exclude");
        int         offset  = prefs.getInt("offset");
        int         count   = prefs.getInt("count");

        JSONObject      commentWrapperList = new JSONObject();

        long            now = System.currentTimeMillis();
       

        for (ii = 0; ii < idList.length(); ii++) {
            JSONObject      commentWrapperOut = new JSONObject();
            String     projectId = idList.getString(ii);

            commentWrapperList.put("" + projectId, commentWrapperOut);

            // #### TOTAL COMMENTS ####
            JSONObject      commentsCount = com.alleni.feedback.ZappComment.countCommentsForProjectId(projectId, credentials);

            if (!commentsCount.getBoolean("success")) {
                reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception for projectId " + projectId, commentsCount.getString("error"));
                response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
                return;
            }
            commentWrapperOut.put("commentsCount", commentsCount.getJSONObject("data").getInt("commentsCount"));

            // #### AVERAGE RATING ####
            JSONObject      averageRatingWrapper = com.alleni.feedback.ZappRatingCache.filter(
                    com.alleni.common.BaseBean.filterValues("projectId", projectId),
                    com.alleni.common.BaseBean.recordRangeValues(offset, count),
                    credentials);

            if (!averageRatingWrapper.getBoolean("success")) {
                reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception getting average rating for projectId " + projectId, averageRatingWrapper.getString("error"));
                response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
                return;
            }
            if (averageRatingWrapper.getJSONObject("data").getJSONArray("zappRatingCaches").length() == 0) {
                commentWrapperOut.put("averageRating", -1);
                commentWrapperOut.put("ratingsCount", 0);
            }
            else {

                JSONObject      averageRatingJSON = averageRatingWrapper.getJSONObject("data").getJSONArray("zappRatingCaches").getJSONObject(0);
                int averageRatingInt = com.alleni.feedback.UpdateRatingAndCache.computeSmartAverage(
                        averageRatingJSON.getInt("ratingTotal"), averageRatingJSON.getInt("numRatings"));

                commentWrapperOut.put("averageRating", averageRatingInt);
                commentWrapperOut.put("numRatings", averageRatingJSON.getInt("numRatings"));
            }

            // #### MEMBER RATING ####
            // (the rating given this projectId by the current credentialed user)
            if (myId == null) {     // not logged in
                commentWrapperOut.put("memberRating", -1);
            }
            else {
                JSONObject      myRatingWrapper = com.alleni.feedback.ZappRating.filter(
                    com.alleni.common.BaseBean.filterValues("memberId", myId, "projectId", projectId),
                    com.alleni.common.BaseBean.recordRangeValues(0, 1),
                    credentials);

                if (myRatingWrapper.getJSONObject("data").getJSONArray("zappRatings").length() == 0) {
                    commentWrapperOut.put("memberRating", -1);
                }
                else {
                    commentWrapperOut.put("memberRating",  myRatingWrapper.getJSONObject("data").getJSONArray("zappRatings").getJSONObject(0).getInt("rating"));
                }
            }

            // Include list of actual comments and ratings if requested.
            if (!exclude) {
                
                JSONArray   commentListForIdOut = new JSONArray();

                commentWrapperOut.put("comments", commentListForIdOut);

                // Get all comments for this projectId
                JSONObject  commentListForIdWrapper = com.alleni.feedback.ZappComment.filter(
                    com.alleni.common.BaseBean.filterValues("projectId", projectId),
                    com.alleni.common.BaseBean.recordRangeValues(offset, count, "createddatetime:d"),
                    credentials);

                JSONArray   commentListForId = commentListForIdWrapper.getJSONObject("data").getJSONArray("zappComments");

                for (jj = 0; jj < commentListForId.length(); jj++) {
                    JSONObject      comment = commentListForId.getJSONObject(jj);

                    // Primary key into the App_User table
                    String memberId = comment.getString("memberId");
                    // Get the AppUser object for this memberId
                    JSONObject appUserWrapper = com.alleni.member.AppUser.find(com.alleni.common.BaseBean.findValues("id", memberId), credentials);
                    JSONObject appUser = appUserWrapper.getJSONObject("data");

                    // get the rating this user (memberId) has left for this projectId
                    JSONObject      rating = com.alleni.feedback.ZappRating.filter(
                            com.alleni.common.BaseBean.filterValues("memberId", memberId, "projectId", projectId),
                            com.alleni.common.BaseBean.recordRangeValues(0, 1),
                            credentials);                   

                    JSONObject  commentOut = new JSONObject();
                    JSONArray zappRatingList = rating.getJSONObject("data").getJSONArray("zappRatings");
                    commentOut.put("memberRating", (zappRatingList.length() == 0) ? 0 : zappRatingList.getJSONObject(0).getInt("rating"));
//                    commentOut.put("memberRating", rating.getJSONObject("data").getJSONArray("zappRatings").getJSONObject(0).getInt("rating"));
                    commentOut.put("comment", comment.getString("comment"));
                    commentOut.put("firstName", appUser.get("firstName"));
                    commentOut.put("lastName", appUser.get("lastName"));
                    commentOut.put("email", appUser.get("email"));
                    commentOut.put("helpfulCount", comment.getInt("helpfulCount"));
                    commentOut.put("feedbackCount", comment.getInt("feedbackCount"));
                    commentOut.put("timestamp", comment.getString("created"));

                    Timestamp   timestamp = Timestamp.valueOf(comment.getString("created"));
                    long        duration = now - timestamp.getTime();
                    duration /= 1000;

                    commentOut.put("verboseTimestamp", com.alleni.util.Utility.getAgoString(duration));

                    commentOut.put("id", comment.getString("id"));
                    commentOut.put("isMine", memberId.equals(myId));

                    commentListForIdOut.put(commentOut);
                }
            }
        }
        reply.put("now", (new Timestamp(now)).toString());
        reply.put("data", commentWrapperList);
        reply.put("success", Boolean.TRUE);
//        reply.put("timestamp", now.)
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>