<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="com.alleni.util.Mail" %>
<%@ page import="com.alleni.template.ClosureUtil" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject feedbackItems = json.getJSONObject("projectFeedbackNotification");
        
        String projectID = feedbackItems.getString("projectID");
        String flaggedByEmail = feedbackItems.getString("flaggedByEmail");
        String flagReason = feedbackItems.getString("flagReason");
        
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("projectID", projectID);
        args.put("flaggedByEmail", flaggedByEmail);
        args.put("flagReason", flagReason);
        
        String to = "support@zebrazapps.com";
        String subject = ClosureUtil.render("flagZapp.soy", "services.zapp.flag", "subject", args);
        String plaintextBody = ClosureUtil.render("flagZapp.soy", "services.zapp.flag", "plaintext", args);
        String htmlBody = ClosureUtil.render("flagZapp.soy", "services.zapp.flag", "html", args);

        Mail mail = new Mail();
        mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.passwordrecovery"));
        mail.sendMultiPartMessage("donotreply@zebrazapps.com", to, subject, plaintextBody, htmlBody);
        
        reply = new JSONObject().put("success", "true");
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>