<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>

<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject editorValues = json.getJSONObject("editor");

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");
        String username = credentials.getString("username");
        String password = credentials.getString("password");
        //String host = proxyValues.getString("host");
        //String path = proxyValues.getString("path");

        //String s = com.alleni.common.BaseBean.proxy(username, password, host, path);
        //reply = com.alleni.common.BaseBean.success(new JSONObject(s));
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));

%>