<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="java.net.URL" %>
<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject proxyValues = json.getJSONObject("proxy");

        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        // PMQ -- continue calling Zephyr with username+password credentials
//        JSONObject appuser = com.alleni.member.AppUser.login(credentials);

        String username = credentials.getString("email");
        String password = credentials.getString("password");
        String host = proxyValues.getString("host");
        String path = proxyValues.getString("path");
        String method = proxyValues.has("method") ? proxyValues.getString("method") : "get";
        String payload = proxyValues.has("payload")? proxyValues.getString("payload") : null;
        
        URL url = new URL(request.getRequestURL().toString());
        String s = com.alleni.common.BaseBean.proxy(username, password, host, path, url.getProtocol(), method, payload);
        reply = com.alleni.common.BaseBean.success(new JSONObject(s));
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.setHeader("Cache-Control", "no-cache");
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>