<%@page import="java.util.Formatter"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.alleni.coupon.CouponCode"%>
<%@page import="com.alleni.coupon.CouponCodeDao"%>
<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONException" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="com.qp.onlinecommerce.CreditCardAccount" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.alleni.util.Mail" %>
<%@ page import="com.alleni.template.ClosureUtil" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.postgresql.util.PSQLException" %>
<%
    JSONObject reply = new JSONObject();

    try
    {
        boolean validCreditCard = false;
        
        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");

        JSONObject json = new JSONObject(request.getParameter("json"));
        JSONObject createValues = json.getJSONObject("joinCreate");

        // if coupon was provided, validate, extend termduedate
        boolean couponSubmitted = false;
        boolean couponApplied = false;
        String termDueDate = "";
        if(!createValues.optString("couponCode","").equals("") && !createValues.isNull("couponCode"))
        {
            String couponCode = createValues.getString("couponCode");
            JSONObject range = new JSONObject().put("offset",0).put("count",-1);
            JSONObject filters = new JSONObject().put("code",couponCode);
            JSONObject couponResult = CouponCode.filter(new JSONObject().put("filters",filters), range, credentials);
            JSONArray coupons = couponResult.getJSONObject("data").has("couponCodes") ? couponResult.getJSONObject("data").getJSONArray("couponCodes") : new JSONArray();
            
            couponSubmitted = true;
            
            Calendar now = Calendar.getInstance();
             SimpleDateFormat formatter;
             formatter = new SimpleDateFormat("M/d/yyyy");
            
            
            if(coupons.length() > 0)
            {
                JSONObject coupon = coupons.getJSONObject(0);
                
                if(coupon.has("couponCodeTypeId"))
                {
                    switch(coupon.getInt("couponCodeTypeId"))
                    {
                    case 1:
                        
                        now.add(Calendar.MONTH,1);
                        termDueDate = formatter.format(now.getTime());
                        break;
                    case 2:
                        now.add(Calendar.MONTH,2);
                        termDueDate = formatter.format(now.getTime());
                        break;
                    case 3:
                        now.add(Calendar.MONTH,3);
                        termDueDate = formatter.format(now.getTime());
                        break;
                    default:
                        
                        break;
                    }
                    couponApplied = true;
                }
            }
            
        }   

        // create new user
        String memberId = null;
        JSONObject memberValues = createValues.getJSONObject("member");
        reply = com.alleni.member.AppUser.create(memberValues, credentials);
        JSONObject data = reply.getJSONObject("data");
        memberId = reply.getJSONObject("data").getString("appUserId");
         
        // if successful, update credentials
        credentials.put("email",memberValues.getString("email")).put("password",memberValues.getString("password"));

        // create new company
        int companyId = -1;
        try {
            JSONObject companyValues = createValues.getJSONObject("company");
            reply = com.alleni.account.Company.create(companyValues, credentials);
            companyId = reply.getJSONObject("data").getInt("companyId");
        } catch (JSONException je) {
            // no company required for account registration
        }
        
        // create new account
        JSONObject accountValues = new JSONObject();
        String accountType = createValues.getString("accountType");     
        // adjust termduedate if coupon is provided
        if(couponApplied){
            accountValues.put("termDueDateTime", termDueDate);
        }
        // set base account create conditions -- note that we assume ALL accounts need a valid credit card on file
        // so we will assume goodStanding=0 until credit card is approved down below
        accountValues.put("companyId", companyId).put("type", accountType).put("goodStanding", 0).put("convertMethod","convert_method_convert_less_account_fee");
        reply = com.alleni.account.Account.create(accountValues, credentials);

        accountValues = reply.getJSONObject("data");
        int accountId = accountValues.getInt("accountId");

        // refresh account data to pick up new merchantCustomerId...hate to do
        // another round trip to the db but what else can we do!?
        reply = com.alleni.account.Account.find(accountValues, credentials);
        accountValues = reply.getJSONObject("data");
        String merchantCustomerId = accountValues.getString("merchantCustomerId");
        termDueDate = accountValues.getString("termDueDateTime");

        JSONObject updateValues = new JSONObject();
        updateValues.put("accountId", accountId);
        if (!couponApplied)
        {
        
            // add account address, if any
            int accountAddressId = -1;
            try {
                JSONObject addressValues = createValues.getJSONObject("accountAddress");
                addressValues.put("type", "primary");
                addressValues.put("accountId", accountId);
                reply = com.alleni.account.Address.create(addressValues, credentials);
                addressValues = reply.getJSONObject("data");
                accountAddressId = addressValues.getInt("addressId");
            } catch (JSONException je) {
                // no accountAddress in createValues
            }

            // add billing address (required, but may be same as account address)
            int billingAddressId = -1;
            try {
                JSONObject addressValues = createValues.getJSONObject("billingAddress");
                addressValues.put("type", "billing");
                addressValues.put("accountId", accountId);
                reply = com.alleni.account.Address.create(addressValues, credentials);
                addressValues = reply.getJSONObject("data");
                billingAddressId = addressValues.getInt("addressId");
            } catch (JSONException je) {
                // no billingAddress in createValues
                billingAddressId = accountAddressId;
            }

            // add credit card information
            boolean haveValidBillingInformation = false;
            int creditCardId = 0;
            JSONObject creditCardValues = new JSONObject();
            // why are we conditionaly making credit card and then unconditionally removing it? see Jesse/Shannon
            if (billingAddressId > 0)
            {
                creditCardValues = createValues.getJSONObject("creditCard");
                creditCardValues.put("accountId", accountId);
                creditCardValues.put("addressId", billingAddressId);
                creditCardValues.put("maskedNumber", com.alleni.finance.CreditCard.maskNumber(creditCardValues.getString("number")));
                creditCardValues.put("firstName", memberValues.getString("firstName"));
                creditCardValues.put("lastName", memberValues.getString("lastName"));
                creditCardValues.put("email", memberValues.getString("email"));
                reply = com.alleni.finance.CreditCard.create(creditCardValues, credentials);
                creditCardId = reply.getJSONObject("data").getInt("creditCardId");

                // should process credit card and billing info through authorize.net at this
                // point to validate
                haveValidBillingInformation = true;
            }

            // set up customer and payment profile with authorize.net and validate credit card
            if (haveValidBillingInformation)
            {
                JSONObject profile = new JSONObject();
                profile.put("merchantCustomerId", merchantCustomerId);
                profile.put("memberId", memberId);
                profile.put("email", memberValues.getString("email"));
                profile.put("transactionReferenceId", "CREATE "+memberId);

                JSONObject creditCard = new JSONObject();
                creditCard.put("nameOnCreditCard", creditCardValues.getString("nameOnCreditCard"));
                creditCard.put("creditCardNumber", creditCardValues.getString("number"));
                creditCard.put("type", creditCardValues.getString("type").toLowerCase());
                creditCard.put("securityCode", creditCardValues.getString("securityCode"));
                creditCard.put("expirationYear", creditCardValues.getString("expirationYear"));
                creditCard.put("expirationMonth", creditCardValues.getString("expirationMonth"));
                profile.put("creditCard", creditCard);

                JSONObject billingInfo = new JSONObject();
                billingInfo = com.alleni.account.Address.findByIdRaw(billingAddressId, credentials);
                // note that following does not fit with creditCard.nameOnCreditCard which is a single field but Authorize.net is expecting 2 fields
                billingInfo.put("firstName", memberValues.getString("firstName")).put("lastName", memberValues.getString("lastName"));
                profile.put("billingInfo", billingInfo);

                CreditCardAccount cca = new CreditCardAccount();
                creditCard = cca.createProfile(profile);
                
                validCreditCard = creditCard.getBoolean("success");
                
                if (validCreditCard)
                {
                    // save profile Ids returned by CIM
                    creditCard.put("creditCardId", creditCardId);
                    reply = com.alleni.finance.CreditCard.update(creditCard, credentials);
                    
                    updateValues.put("goodStanding", 1);
                    JSONObject gd = com.alleni.account.Account.update(updateValues, credentials);
                }
                else
                {
                    // credit card did not validate
                    JSONObject retire = com.alleni.finance.CreditCard.retire(new JSONObject().put("id",creditCardId), credentials);

                    // if free collector account or coupon applied, allow goodStanding in spite of lack of a valid credit card
                    if (accountType.equals("collector")) {
                        updateValues.put("goodStanding", 1);
                    }
                    else {
                        updateValues.put("goodStanding", 0);
                    }
                    
                    JSONObject gd = com.alleni.account.Account.update(updateValues, credentials);
                }
                
            }
        }else{
        	updateValues.put("goodStanding", 1);
        	JSONObject gd = com.alleni.account.Account.update(updateValues, credentials);
        }
        

        // send an email confirming new subscription
        if (memberValues.getString("email").length() > 0 && memberValues.getString("firstName").length() > 0 && memberValues.getString("lastName").length() > 0)
        {
            String to = memberValues.getString("email");
            String url = request.getScheme() + "://" + request.getServerName();
            
            Calendar emailDate = Calendar.getInstance();
            SimpleDateFormat emailDateFormatter = new SimpleDateFormat("M/d/yyyy");
            
            Map<String, Object> args = new HashMap<String,Object>();
            args.put("firstName", memberValues.getString("firstName"));
            args.put("lastName", memberValues.getString("lastName"));
            args.put("url", url);
            args.put("accountType", accountType);
            args.put("formattedStartDate", emailDateFormatter.format(emailDate.getTime()));
            
            String plaintextBody = ClosureUtil.render("joinWelcome.soy", "services.join.create.welcome", "plaintext", args);
            String htmlBody = ClosureUtil.render("joinWelcome.soy", "services.join.create.welcome", "html", args);
            String subject = ClosureUtil.render("joinWelcome.soy", "services.join.create.welcome", "subject", args);

            Mail mail = new Mail();
            mail.setBypass(com.alleni.db.util.PropertyUtil.getProperty("mail.bypass.ecommerce"));
            mail.sendMultiPartMessage("donotreply@zebrazapps.com", to, subject, plaintextBody, htmlBody);
        }

        // DWH: only set success value if it hasn't already been set above.
        if (reply.has("success") == false ) {
            reply.put("success", true);
            reply.put("data", new JSONObject("{\"memberId\":\"" + memberId + "\",\"accountId\":" + accountId + "\",\"validCreditCard\":" + validCreditCard + "}"));
        }
        // update join result information for client handling
        
        if (! reply.has("data")) {
            reply.put("data", new JSONObject());
        }
        reply.getJSONObject("data").put("couponSubmitted",couponSubmitted);
        reply.getJSONObject("data").put("couponApplied",couponApplied);
        reply.getJSONObject("data").put("validCreditCard", validCreditCard);
        reply.getJSONObject("data").put("termDueDate", termDueDate);
        
    } catch (PSQLException psqe) {
    	psqe.printStackTrace();
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - Oops!",
                "We tried to create your account, but we found a problem. You may have pressed \"Submit\" more than one time.\n\nPlease try to sign in again. If that does not work, please start the sign-up process again from the beginning. We apologize for your inconvenience.");
    }
    catch(Exception e)
    {
    	e.printStackTrace();
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>
