<%@ include file="../common/pageHeader.jsp" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@page import="com.alleni.coupon.CouponCode"%>
<%@page import="com.alleni.coupon.CouponCodeDao"%>
<%@ page import="org.json.JSONArray" %>
<%
    JSONObject reply = new JSONObject();
    try
    {
        JSONObject json = new JSONObject(request.getParameter("json"));  
        String authorizationHeader = request.getHeader("authorization");
        JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");
        boolean couponValid = false;
        String couponCode = json.getString("couponCode");
        JSONObject range = new JSONObject().put("offset",0).put("count",-1);
        JSONObject filters = new JSONObject().put("code",couponCode);
        JSONObject couponResult = CouponCode.filter(new JSONObject().put("filters",filters), range, credentials);
        JSONArray coupons = couponResult.getJSONObject("data").has("couponCodes") ? couponResult.getJSONObject("data").getJSONArray("couponCodes") : new JSONArray();
        if(couponCode.equals("Z60DAY"))
        {
        	couponValid = true;
        }else if(coupons.length() > 0)
        {
            JSONObject coupon = coupons.getJSONObject(0);
            
        	if(coupon.has("couponCodeTypeId"))
        	{
        		couponValid = true;
        	}
        }
        
        reply = couponResult;
        
        reply.getJSONObject("data").put("couponValid",couponValid);
    }
    catch(Exception e)
    {
        reply = com.alleni.common.BaseBean.error(request.getRequestURI()+" - exception", e.getMessage());
    }
    response.getWriter().write(com.alleni.common.BaseBean.jsonp(request.getParameter("jsonCallback"), reply));
%>