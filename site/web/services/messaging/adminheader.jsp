<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URLDecoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%

String authorizationHeader = request.getHeader("authorization");
if(authorizationHeader == null) {
    for(Cookie c : request.getCookies()) {
        if("header".equals(c.getName())) {
            String cookie = c.getValue();
            cookie = URLDecoder.decode(cookie, "utf-8");
            authorizationHeader = "Basic " + cookie;
            break;
        }
    }
}
JSONObject credentials = com.alleni.common.BaseBean.credentials(authorizationHeader != null ? authorizationHeader : "");
JSONObject login = com.alleni.member.AppUser.login2(credentials, 1, 0); 
if (login.getBoolean("success")) {
    if(!login.getJSONObject("data").getBoolean("systemAdministrator")) {
        throw new Exception("Administrator privilages required");
    }
} else {
    throw new Exception("Invalid Login");
}
%>

</html>
