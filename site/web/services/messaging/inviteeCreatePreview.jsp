<%@page import="com.alleni.template.ClosureUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URLDecoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="adminheader.jsp" %>

<%
	String to = request.getParameter("to");
	if (to == null || to.equals(""))
		to = "test@test.com";


    String authorFirstName = request.getParameter("authorFirstName");
    if (authorFirstName == null)
    	authorFirstName = "Super Awesome Zapp";

    String userBody = request.getParameter("userBody");
    if (userBody == null) {
    	userBody = "";
    	for (int i = 0; i < 13; i++)
    		   userBody += "All work and no play makes Jack a dull boy.  All work and no play makes Jack a dull boy.  ";
    }
    
    String szIsMember = request.getParameter("isMember");
    boolean isMember = false;
    if("on".equals(szIsMember) == true) {
    	isMember = true;
    } 
    
    String szPublishId = request.getParameter("publishId");
    long publishId = 23768;
    try {
    	publishId = Long.parseLong(szPublishId);
    } catch (NumberFormatException ignore) {
    	// use default
    }
    
	String url = request.getScheme() + "://" + request.getServerName() + "/";

	Map<String, Object> args = new HashMap<String, Object>();
	args.put("url", url);
	args.put("userBody", userBody);
    args.put("authorFirstName", authorFirstName);
    args.put("publishId", String.valueOf(publishId));
    
	String body = "undefined";
	String bodyHTML = "undefined";
	String subject = "undefined";
	if (isMember) {
	    body = ClosureUtil.render("inviteMember.soy",
	    	  	"services.sharedprojectinvitee.create.member", "plaintext", args);
	    bodyHTML = ClosureUtil.render("inviteMember.soy",
	    	    "services.sharedprojectinvitee.create.member", "html", args);
	    subject = ClosureUtil.render("inviteMember.soy",
	    	   "services.sharedprojectinvitee.create.member", "subject", args);
	} else {
        body = ClosureUtil.render("inviteNonMember.soy",
                "services.sharedprojectinvitee.create.nonmember", "plaintext", args);
        bodyHTML = ClosureUtil.render("inviteNonMember.soy",
                "services.sharedprojectinvitee.create.nonmember", "html", args);
        subject = ClosureUtil.render("inviteNonMember.soy",
               "services.sharedprojectinvitee.create.nonmember", "subject", args);
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cron2 Email Preview</title>
</head>
<body>
	<h3>HTML</h3>
	To:
	<%=to%>
	<br />Subject:
	<%=subject%>
	<br />
	<br /><%=bodyHTML%>
	<hr />
	<h3>Plain-text</h3>
	<pre>
To: <%=to%>
<br />Subject: <%=subject%>
<br /> <br /><%=body%>
</pre>
	<hr />
	<h3>Dynamic Components</h3>
	<form>
		Guest Email Address: <input type="text" name="to" value="<%=to%>" /> 
		<br />Author First Name: <input type="text" name="authorFirstName" value="<%=authorFirstName%>" />
		<br />Zapp Id: <input type="text" name=publishId value="<%=publishId%>" />
		<br />User-defined Content: <textarea cols="64" rows="4" name="userBody" > <%=userBody %></textarea>
		<br />Recipient is ZebraZapps Member: <input type="checkbox" name="isMember" <% if (isMember){%>checked="checked"<%} %> />
		<br />	<input type="submit" value="Update" />
	</form>
</body>
</html>