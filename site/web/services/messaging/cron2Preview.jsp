<%@page import="com.alleni.template.ClosureUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URLDecoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@include file="adminheader.jsp" %>
<%
				String to = request.getParameter("to");
				if (to == null || to.equals("")) to = "test@test.com";
				
				String firstName = request.getParameter("firstName");
				if (firstName == null) to = "Joe";
                
				int chargeAmt = 0;
				String szChargeAmt = request.getParameter("chargeAmt");
				if (szChargeAmt != null) chargeAmt = Integer.parseInt(szChargeAmt);
				
				String chargeAmtTxt = String.format("%.2f", (float)chargeAmt/100);
				
				int subscrCharge = 0;
				String szSubscrCharge = request.getParameter("subscrCharge");
				if (szSubscrCharge != null) {
					subscrCharge = Integer.parseInt(szSubscrCharge);
				}

				String szTransactionSuccess = request.getParameter("transactionSuccess");
				boolean transactionSuccess = false;
			    if("on".equals(szTransactionSuccess) == true) {
			    	transactionSuccess = true;
			    } 
			    
			    
				//				boolean includesSubscription = invoice.getBoolean("includesSubscriptionCharge");
                SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
                String formattedDate = df.format(new Date());
                
                szSubscrCharge = String.format("%.2f", (float)subscrCharge/100);
                
                Map<String,Object> args = new HashMap<String,Object>();
                args.put("firstName", firstName); 
                args.put("totalCharge", chargeAmtTxt);
                args.put("subscrChargeTxt", szSubscrCharge);
                args.put("chargeDate", formattedDate);
                args.put("includesSubscription", subscrCharge > 0? true : false);
                args.put("accountType", "pro");
                int difference = chargeAmt - subscrCharge;
                String szDifference = String.format("%.2f", (float)difference/100);
		if (difference < 0) difference = 0;
                args.put("differenceTxt", szDifference);
               
    			String urlStr = request.getScheme() + "://" + request.getServerName();
				args.put("url", "https://zebrazapps.com");
//    			args.put("url", urlStr);

                String body;
                String bodyHTML;
                String subject;
                if (transactionSuccess) { //transactionResult.getBoolean("success")
                    subject = ClosureUtil.render("cron2-plaintext.soy", "services.commerce.cron2", "subjectsuccess", args);
                    body = ClosureUtil.render("cron2-plaintext.soy", "services.commerce.cron2", "success", args);
                    bodyHTML = ClosureUtil.render("cron2-html.soy", "services.commerce.cron2", "success", args);
                } else {
                    subject = ClosureUtil.render("cron2-plaintext.soy", "services.commerce.cron2", "subjectfailure", args);
                    body = ClosureUtil.render("cron2-plaintext.soy", "services.commerce.cron2", "failure", args);
                    bodyHTML = ClosureUtil.render("cron2-html.soy", "services.commerce.cron2", "failure", args);
                }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cron2 Email Preview</title>
</head>
<body>
<h3>HTML</h3>
To: <%=to %>
<br />Subject: <%=subject %>
<br /> <br /><%=bodyHTML %>
<hr />
<h3>Plain-text</h3>
<pre>
To: <%=to %>
<br />Subject: <%=subject %>
<br /> <br /><%=body %>
</pre>
<hr />
<h3>Dynamic Components</h3>
<form>
Email: <input type="text" name="to" value="<%=to %>"/>
<br />First Name: <input type="text" name="firstName" value="<%=firstName %>"/>
<br />Charge Amount in Pennies: <input type="text" name="chargeAmt" value="<%= chargeAmt %>"/>
<br />Subscription Charge Amount in Pennies: <input type="text" name="subscrCharge" value="<%=subscrCharge %>"/>
<br />Payment Processed Successfully: <input type="checkbox" name="transactionSuccess" checked="<%=(transactionSuccess? "checked" : " " ) %>"/>
<br /><input type="submit" value="Update" />
</form>
</body>
</html>
