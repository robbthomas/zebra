<%@page import="com.alleni.template.ClosureUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URLDecoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="adminheader.jsp" %>

<%
	String to = request.getParameter("to");
	if (to == null || to.equals(""))
		to = "test@test.com";

	String firstName = request.getParameter("firstName");
	if (firstName == null)
		firstName = "Joe";
	
	String lastName = request.getParameter("lastName");
	if (lastName == null)
		lastName = "Zapper";

	String url = request.getScheme() + "://" + request.getServerName();

	Map<String, Object> args = new HashMap<String, Object>();
	args.put("firstName", firstName);
	args.put("lastName", lastName);
	args.put("accountType", "Doofus");
	args.put("formattedStartDate", "06/01/2012");
	args.put("url", url);

	String body = ClosureUtil.render("upgradeAccount.soy",
			"services.account.updateExtended.accountUpgrade", "plaintext", args);
	String bodyHTML = ClosureUtil.render("upgradeAccount.soy",
			"services.account.updateExtended.accountUpgrade", "html", args);
	String subject = ClosureUtil.render("upgradeAccount.soy",
			"services.account.updateExtended.accountUpgrade", "subject", args);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cron2 Email Preview</title>
</head>
<body>
	<h3>HTML</h3>
	To:
	<%=to%>
	<br />Subject:
	<%=subject%>
	<br />
	<br /><%=bodyHTML%>
	<hr />
	<h3>Plain-text</h3>
	<pre>
To: <%=to%>
<br />Subject: <%=subject%>
<br /> <br /><%=body%>
</pre>
	<hr />
	<h3>Dynamic Components</h3>
	<form>
		Email Address: <input type="text" name="to" value="<%=to%>" /> 
		<br />First Name: <input type="text" name="firstName" value="<%=firstName%>" />
		<br />Last Name: <input type="text" name="lastName" value="<%=lastName%>" /> 
		<br />	<input type="submit" value="Update" />
	</form>
</body>
</html>