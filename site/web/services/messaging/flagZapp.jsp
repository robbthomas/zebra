<%@page import="com.alleni.template.ClosureUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URLDecoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="adminheader.jsp" %>

<%
	String flaggedByEmail = request.getParameter("flaggedByEmail");
	String comment = request.getParameter("comment");
	
	if (flaggedByEmail == null || flaggedByEmail.equals(""))
		flaggedByEmail = "test@zebrazapps.com";

	if (comment == null)
		comment = "";
	
	int publishId = 0;
	try {
		   publishId = Integer.valueOf(request.getParameter("publishId"));
	} catch (Exception ignore) {}

	String zappUrl = request.getScheme() + "://" + request.getServerName() + "/#/detail?" + publishId;

	Map<String, Object> args = new HashMap<String, Object>();
	
	args.put("projectID", "Project Foobar");
	args.put("flagReason", "Because I said so");
	args.put("flaggedByEmail", flaggedByEmail);

	String body = ClosureUtil.render("flagZapp.soy",
			"services.zapp.flag", "plaintext", args);
	String bodyHTML = ClosureUtil.render("flagZapp.soy",
			"services.zapp.flag", "html", args);
	String subject = ClosureUtil.render("flagZapp.soy",
			"services.zapp.flag", "subject", args);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Zapp Flag Email Preview</title>
</head>
<body>
	<h3>HTML</h3>
	To: support@zebrazapps.com
	<br />Subject:
	<%=subject%>
	<br />
	<br /><%=bodyHTML%>
	<hr />
	<h3>Plain-text</h3>
	<pre>
To: support@zebrazapps.com
<br />Subject: <%=subject%>
<br /> <br /><%=body%>
</pre>
	<hr />
	<h3>Dynamic Components</h3>
	<form>
		Flagged By (email): <input type="text" name="flaggedByEmail" value="<%=flaggedByEmail%>" /> 
		<br />PublishedId: <input type="text" name="publishId" value="<%=publishId%>" />
		<br />Comment: <input type="text" name="comment" value="<%=comment%>" /> 
		<br />	<input type="submit" value="Update" />
	</form>
</body>
</html>