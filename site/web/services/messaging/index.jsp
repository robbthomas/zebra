<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Preview Emails</title>
<%@include file="adminheader.jsp" %>

</head>
<body>
<ul>
    <li><a href="cron1Preview.jsp">cron1 preview</a></li>
    <li><a href="cron2Preview.jsp">cron2 preview</a></li>
    <li><a href="inviteeCreatePreview.jsp">guest list invite preview</a></li>
    <li><a href="joinCreatePreview.jsp">Sign-up welcome email prview</a></li>
    <li><a href="flagComment.jsp">Flagged-comment email</a></li>
    <li><a href="createComment.jsp">Comment created email</a></li>
</ul>
</body>
</html>