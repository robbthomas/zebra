<%@page import="com.alleni.template.ClosureUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URLDecoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@include file="adminheader.jsp" %>
<%
    String to = request.getParameter("to");
    if (to == null || to.equals("")) to = "test@example.com";

    String firstName = request.getParameter("firstName");
    if (firstName == null) firstName = "Joe";

    String lastName = request.getParameter("lastName");
    if (lastName == null) lastName = "Bodell";



    String strValidCC = request.getParameter("validcc");
    boolean validcc = false;
    if("on".equals(strValidCC) == true) {
        validcc = true;
    }


    // go from String representation of SQL timestamp to our preferred String date format
    String termDueTimeStamp = request.getParameter("termDueDate");
    if (termDueTimeStamp == null) termDueTimeStamp = "2013-03-01 00:00:00";
    SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    Date termDueDate = sdfIn.parse(termDueTimeStamp);
    SimpleDateFormat sdfOut = new SimpleDateFormat("MMMM dd, yyyy");
    String termDueDateStr = sdfOut.format(termDueDate);
    
    Date now = new Date();
    String currentDate = request.getParameter("currentDate");
    if (currentDate == null) currentDate = "2013-02-01 10:00:00";   
    
    Date currentNow = sdfIn.parse(currentDate);
    String daysOut = "";
    
    long msDiff = termDueDate.getTime() - currentNow.getTime();
    int daysOutInt = Math.round(msDiff / (1000 * 60 * 60 * 24));
    
    Map<String, Object> args = new HashMap<String, Object>();
    args.put("firstName", firstName);
    args.put("lastName", lastName);
    args.put("holdingDays", daysOutInt);
    args.put("termDueDate", termDueDateStr);
    args.put("url", "https://zebrazapps.com");


    String template = validcc ? "cc" : "no_cc";

    String subject  = ClosureUtil.render("cron1-plaintext.soy", "services.commerce.cron1.renew", "subject", args);
    String body     = ClosureUtil.render("cron1-plaintext.soy", "services.commerce.cron1.renew", template , args);
    String bodyHtml = ClosureUtil.render("cron1-html.soy"     , "services.commerce.cron1.renew", template , args);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cron2 Email Preview</title>
</head>
<body>
<h3>HTML</h3>
To: <%=to %>
<br />Subject: <%=subject %>
<br /> <br /><%=bodyHtml %>
<hr />
<h3>Plain-text</h3>
<pre>
To: <%=to %>
<br />Subject: <%=subject %>
<br /> <br /><%=body %>
</pre>
<hr />
<h3>Dynamic Components</h3>
<form>
Email: <input type="text" name="to" value="<%=to %>"/>
    <br />First Name: <input type="text" name="firstName" value="<%=firstName %>"/>
    <br />Last Name: <input type="text" name="lastName" value="<%=lastName %>"/>
<br />Valid CC: <input type="checkbox" name="validcc" <%=(validcc? "checked=\"checked\"" : " " ) %>"/>
<br />Current Date: <input type="text" name="currentDate" value="<%= sdfIn.format(currentNow) %>" />
<br />Term Due Date: <input type="text" name="termDueDate" value="<%= sdfIn.format(termDueDate) %>" />
<br /><input type="submit" value="Update" />
</form>
</body>
</html>
