<%@page import="com.alleni.template.ClosureUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URLDecoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@include file="adminheader.jsp" %>

<%
String createdByEmail = "commenter@comments.com";
String projectId = "my_project";
String commentText = "Hey this thing is neat!";
String authorEmail = "author@zebrazapps.com";
String projectType = "app";

if (request.getParameter("formdriven") != null) {
    createdByEmail = request.getParameter("createdByEmail");
    projectId = request.getParameter("projectId");
    commentText = request.getParameter("commentText");
    authorEmail = request.getParameter("authorEmail");
    projectType = request.getParameter("projectType");
}

String zappUrl = request.getScheme() + "://" + request.getServerName() + "/#/detail?" + projectId;
Map<String, Object> args = new HashMap<String, Object>();
args.put("zappUrl", zappUrl);
args.put("createdByEmail", createdByEmail);
args.put("commentText", commentText);
args.put("authorEmail", authorEmail);
args.put("projectType", projectType);

String body     = ClosureUtil.render("commentCreated.soy", "services.comment.create", "plaintext", args);
String bodyHTML = ClosureUtil.render("commentCreated.soy", "services.comment.create", "html", args);
String subject  = ClosureUtil.render("commentCreated.soy", "services.comment.create", "subject", args);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Zapp Flag Email Preview</title>
</head>
<body>
    <h3>HTML</h3>
    To: <%=authorEmail %>
    <br />Subject:
    <%=subject%>
    <br />
    <br /><%=bodyHTML%>
    <hr />
    <h3>Plain-text</h3>
    <pre>
    To: <%=authorEmail %>
<br />Subject: <%=subject%>
<br /> <br /><%=body%>
</pre>
    <hr />
    <h3>Dynamic Components</h3>
    <form>
        Author (email): <input type="text" name="authorEmail" value="<%=authorEmail%>" /> 
        <br />Commented By (email): <input type="text" name="createdByEmail" value="<%=createdByEmail%>" /> 
        <br />PublishedId: <input type="text" name="projectId" value="<%=projectId%>" />
        <br />Comment: <input type="text" name="commentText" value="<%=commentText%>" /> 
        <br />Project Type: <input type="text" name="projectType" value="<%=projectType%>" /> 
        <br />  <input type="submit" value="Update" />
        <input type="hidden" name="formdriven" value="yes" />
    </form>
</body>
</html>