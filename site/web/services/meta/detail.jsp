<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.alleni.common.JsonError" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.io.Writer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String name = "";
    String description = "";
    String iconFileId = "defaultGadgetIcon";
    String protocol = "";
    String idType = "";
    String id = "";
    String width = "800";
    String height = "500";
    String apnUUID = "";
    String urlName = "";
    boolean embed = false;
    try
    {
        String pathToServer = "/zephyr/";
        idType = request.getParameter("idType");
        id = request.getParameter("id");
        if (idType == null || idType.equals("apn")) {
            pathToServer += "player/" + id;
        } else if (idType.equals("name") || idType.equals("urlName")) {
            pathToServer += "project/urlName/" + id;
        } else {
            Exception e = new Exception("ZebraZapps Metadata Generator: Unsupported ID type " + idType);
            throw e;
        }

        String username = "guest";
        String password = "none";

        URL url = new URL(request.getRequestURL().toString());
        protocol = request.getScheme();
        String publishedGadgetResponse = com.alleni.common.BaseBean.proxy(username, password, url.getHost(), pathToServer, protocol, "get", null);
        JSONObject publishedGadget = new JSONObject(publishedGadgetResponse);

        JSONObject md = publishedGadget.optJSONObject("metadata");
        if(md != null) {
            width = Integer.toString(md.optInt("contentWidth", 800) + md.optInt("contentWidthPadding", 0));
            height = Integer.toString(md.optInt("contentHeight", 500) + md.optInt("contentHeightPadding", 0));
        }

        JSONObject icon = publishedGadget.optJSONObject("icon");
        if (icon != null && icon.getString("iconFileId") != null) {
            iconFileId = icon.getString("iconFileId");
        }

        JSONObject permissions = publishedGadget.optJSONObject("permissions");
        if (permissions != null && permissions.getString("embed").equals("true")) {
            embed = true;
        }

        apnUUID = publishedGadget.getString("apnUUID");
        urlName = publishedGadget.getString("urlName");
        name = publishedGadget.getString("publishedName");
        description = publishedGadget.getString("description");
    }
    catch(Exception e)
    {
        throw new RuntimeException(e);
    }
    response.setHeader("Cache-Control", "no-cache");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.1//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-2.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" version="XHTML+RDFa 1.1" xmlns:og="http://ogp.me/ns#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<meta property="og:title" content="<%=name%>"/>
<meta property="og:description" content="<%=description%>"/>
<meta property="og:type" content="video"/>
<meta property="og:url" content="<%=protocol%>://<%=request.getServerName()%>/detail/<%=idType%>/<%=id%>"/>
<meta property="og:image" content="http://static.zebrazapps.com/icons/<%=iconFileId%>-180.png" />
<c:if test='<%=embed%>'>
  <meta property="og:video" content="<%=protocol%>://<%=request.getServerName()%>/flash/AuthorWeb.swf?projectId=<%=apnUUID%>&serverhostname=<%=request.getServerName()%>&serverport=<%=request.getServerPort()%>&contextPath=/zephyr&isEditor="/>
  <meta property="og:video:width" content="<%=width%>" />
  <meta property="og:video:height" content="<%=height%>" />
  <meta property="og:video:type" content="application/x-shockwave-flash" />
</c:if>
<meta name="description" content="<%=description%>" />
<meta name="keywords" content="Zebra, ZebraZapps, <%=name%>" />
<link rel="canonical" href="<%=protocol%>://<%=request.getServerName()%>/detail/name/<%=urlName%>" />
<title><%=name%> - ZebraZapps</title>
<style type="text/css" media="screen">
    html, body { height:100%; }
    body { margin:0; padding:0; overflow:hidden; }
</style>
</head>
<body>
    <img src="http://static.zebrazapps.com/icons/<%=iconFileId%>-180.png" />
    <h1><%=name%></h1>
    <p><%=description%></p>
</body>
</html>