<%-- 
    Document   : video
    Created on : May 26, 2011, 7:25:38 AM
    Author     : qpi
--%>

<%@page import="com.alleni.util.Utility"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%

    Map<String, String> queryString = Utility.parseQueryStringSimple(request.getQueryString());
    String  title   = queryString.get("title");
    String  videoId = queryString.get("videoId");
    String  width   = queryString.get("width");
    if (width == null) {
        width = "1280";
    }
    String  height  = queryString.get("height");
    if (height == null) {
        height = "720";
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<head>
<link href="./com/alleni/css/zebra.css?cb=16634" rel="stylesheet" type="text/css" />
</head>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=title%></title>
    </head>
    <body>
        <h1 style="color:white;"><%=title%></h1>
        <iframe src="http://player.vimeo.com/video/<%=videoId%>?title=0&byline=0&portrait=0&autoplay=1" width="<%=width%>" height="<%=height%>" frameborder="0"></iframe>
    </body>
</html>
