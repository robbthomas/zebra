function changeTitle(title) { document.title = title; }

function openDialog(args)
{
	/*
	 * values in "args" -
	 * args.panel:int
	 * 		0 = Security Panel
	 * 		1 = Share Panel
	 * args.privacyLevel:int
	 * 		0 = Use what the server gives you
	 * 		1 = override with Guest List level
	 * 		2 = override with Hidden(NOT in shopp)
	 * 		3 = override with Shopp (Public)
	 * args.shareDefault:int
	 * 		0 = Default
	 * 		1 = Guest List
	 * 		2 = Embed
	 * 		3 = LMS
	 * 		4 = Direct Link
	 * 		5 = Email
	 * 		6 = Facebook
	 * 		7 = Linked In
	 * 		8 = Twitter
	*/
	if(args && args.projectId){
		
		var facade = ApplicationFacade.getInstance();
		var config = new LoadDialogueViewConfig(
				"",
				"shareDialog.js",
				"shareDialogMediator.js",
				"shareDialog.html",
				args
				);
		
	   facade.sendNotification(ApplicationFacade.SHOW_DIALOG, config );
	}else{
		handleError(["NULL ProjectId was provided to privacy/sharing panel."]);
	}
	
}

function handleURLRequest(args) {
    window.open(args.address, "_blank");
}

function openTransfers(args) {
	var facade = ApplicationFacade.getInstance();
	var config = new LoadDialogueViewConfig(
			'Share Projects: <span class="name">Send a Copy</span>',
			"startTransfer.js",
			"startTransferMediator.js",
			"accountFormStartTransfer.html",
			args
			);
	
   facade.sendNotification(ApplicationFacade.SHOW_DIALOG, config );
}

function onUpdateEditorProperty(payload)
{
	EditorPlayer.editor().onUpdateEditorProperty(payload);
}

function requestPopup(id)
{
	if (window.opener != null){
    	if(id instanceof Array){
    		window.opener.EditorPlayer.launchEditor({id:id[0]}, true);
    	}else{
    		window.opener.EditorPlayer.launchEditor(id, true);
    	}
    }
}

function requestDetailsPanel(args)
{
    if (window.opener != null){
        var type;
        if(args[0].owned){
            type = "apn";
        }else{
            type = "pid";
        }
        window.opener.EditorPlayer.requestDetailsPanel(args[0].id, type, window);
    }
}

function focusContainer()
{
    setTimeout(function(){
    	var editorElement = document.getElementById("editor");
	    if (editorElement) {
	        editorElement.tabIndex = 0;
	        editorElement.focus();
	    }
    },
    1000);
}

jQuery(window).bind("unload",function(event) {
    if (window.opener != null)
        window.opener.EditorPlayer.handleEditorUnloaded();
});

jQuery(window).bind("beforeunload",function(event) {
    if (!EditorPlayerContext.unsavedChanges)
        return;	
    return "You are about to close the ZebraZapps Editor! Any unsaved changes will be lost.";
});

function handleApplicationQuit()
{
    EditorPlayerContext.unsavedChanges = false;
    window.close();
}

function handleUnsavedChanges(args)
{
    EditorPlayerContext.unsavedChanges = args[0];
}

function handleProjectSaved(args)
{
    EditorPlayerContext.lastSaved = args[0];
    refreshMainSite();
}            

function refreshMainSite()
{
	if (window.opener != null)
        window.opener.EditorPlayer.refresh();
}


function handleError(args)
{
    EditorPlayerContext.unsavedChanges = false; // not anymore there aren't
	var message = args[0];
    var editorElement = document.getElementById("editor");
    // use the following replacements if inside a div or pre tag but not inside a textarea
    //message = message.replace(/&/g,'&amp;').replace(/>/g,'&gt;').replace(/</g,'&lt;').replace(/"/g,'&quot;');
    var replacement = document.createElement("div");
    replacement.id = 'editor';
    editorElement.parentNode.replaceChild(replacement, editorElement);
    jQuery('#errorDiv').css('visibility', 'visible');
    jQuery('#errorDiv').fadeTo(400,1);
    jQuery('#errorText').val(message);
    jQuery('#editor').hide();
    
    if(Common.pageTracker != undefined) Common.pageTracker._trackEvent('Editor', 'Error', message);
}

function newProject() {
    window.opener.EditorPlayer.id = undefined;
    loadEditor();
}

function lastSavedProject() {
    window.opener.EditorPlayer.id = EditorPlayerContext.lastSaved;
    loadEditor();
}


EditorPlayerContext = {
    handleInvoke: function (id) {
        if (id != undefined) {
            var editorElement = document.getElementById("editor");
            editorElement.handleInvoke(id);
        }
    }
}
EditorPlayerContext.isEditor = true;
EditorPlayerContext.unsavedChanges = false;

function loadEditor() {
    var error = function (msg) {
        alert('Cannot open Editor\n('+msg+')');
    }
    var callback = function(o)
    {
        EditorPlayerContext.lastSaved = o.id;
        //alert('username:'+o.username+'\npassword:'+o.password+'\nprojectId:'+o.projectId);
        var projectName = o.projectName != null ? Base64.encode(o.projectName) : "";
        var flashvars = {
            username: Base64.encode(o.username),
            password: Base64.encode(o.password),
            email: o.email,
            serverhostname: window.document.location.hostname,
            contextPath:"/zephyr",
            serverport: window.document.location.protocol == "https:" ? "443" :
                window.document.location.port == "" ? "80" : window.document.location.port,
            projectId: o.id,
            projectName: projectName,
            showHistory: o.bShowHistory,
            isEditor:EditorPlayerContext.isEditor
        };
        var params = {
            allowScriptAccess: "always",
			allowFullScreen:"true",
			wmode: "opaque"
        };
        var attributes = {
            id: "editor",
            name: "editor"
        };
        // Flash Player >11.3 supports interactive full screen
        var playerVersion = swfobject.getFlashPlayerVersion();
        if (playerVersion.major >= 11 && playerVersion.minor >= 5) {
            //params.allowFullScreenInteractive = "true";
        }
        jQuery('#editor').show();
        jQuery('#errorDiv').fadeTo(0,0);
        jQuery("#errorText").val('');
        jQuery('#errorDiv').css('visibility', 'hidden');
        EditorPlayerContext.unsavedChanges = false;
        swfobject.embedSWF("/flash/AuthorWeb.swf?cb=17976", "editor", "100%", "100%", "10.3.0", "/org/swfobject/expressInstall.swf", flashvars, params, attributes, focusContainer);

        if(Common.pageTracker != undefined) Common.pageTracker._trackEvent('Editor', 'Open', flashvars.username, flashvars.projectId, true);

    }
    if (window.opener != null){
        window.opener.EditorPlayer.ready(EditorPlayerContext, callback, EditorPlayerContext.isEditor);
    }
    else{
        error('unable to retrieve editor configuration');
    }
}