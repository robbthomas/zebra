var PrepModelCommand = function()
{
    this.Extends = SimpleCommand;

    this.execute = function(notification/*Notification*/)
    {
    	this.facade.registerProxy(new GuestListProxy());
    	this.facade.registerProxy(new ZappGadgetProjectProxy());
    	this.facade.registerProxy(new UserProxy());
        this.facade.registerProxy(new AccountProxy());
    }
}
PrepModelCommand = new Class(new PrepModelCommand());