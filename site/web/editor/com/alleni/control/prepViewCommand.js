var PrepViewCommand = function()
{
    this.Extends = SimpleCommand;

    this.execute = function(notification/*Notification*/)
    {
        var editorPanel = notification.getBody();

        this.facade.registerMediator(new EditorPanelMediator(editorPanel));
    }
    
}
PrepViewCommand = new Class(new PrepViewCommand());

