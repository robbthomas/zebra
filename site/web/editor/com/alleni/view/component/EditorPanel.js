var EditorPanel = function()
{
    this.Extends = UIComponent;
    
    this.initialize = function (parent)
    {
        this.facade = ApplicationFacade.getInstance();
        this.parent("editorPanel");

    }
    this.initializeChildren = function ()
    {
    
    }
    this.initializationComplete = function ()
    {
        
    }
}
EditorPanel = new Class(new EditorPanel());