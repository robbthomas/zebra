var EditorPanelMediator = function(viewComponent)
{
    this.Extends = Mediator;
    this.view = undefined;

    this.initialize = function(viewComponent)
    {
        this.parent(EditorPanelMediator.NAME, viewComponent);
        this.view = this.getViewComponent();

    }

    this.listNotificationInterests = function()/*Array*/
    {
        return [

        ];
    }
    this.handleNotification = function(notification)
    {
        var name = notification.getName();
        var payload = notification.getBody();
        
        switch(name)
        {
        
        default:
            break;
        }
    }
}
EditorPanelMediator = new Class(new EditorPanelMediator());
EditorPanelMediator.NAME = 'EditorPanelMediator';