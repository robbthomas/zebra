package com.alleni.zebra.reports.web.controller.hello;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	Logger log = Logger.getLogger(HelloController.class);
	
	@RequestMapping("/")
	public String showHelloJsp(HttpServletRequest request, HttpServletResponse response, ModelMap model)
			throws ServletException, IOException {

		log.info("Returning hello view");
		return "/index.jsp";
	}
}
