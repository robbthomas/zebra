package com.alleni.zebra.p2p;

import static org.junit.Assert.*;

import org.junit.Test;

public class QueueManagerServiceImplTest {

	@Test
	public void testPutGet() {
		SimpleQueueManagerServiceImpl mgr = new SimpleQueueManagerServiceImpl();
		
		String key = "stack1";
		String expected = "value1";
		
		mgr.post(key, expected);
		mgr.post(key, "wrongvalue");
		
		String actual = (String) mgr.get(key);
		
		assertEquals(expected, actual);
	}


	@Test
	public void testPopEmpty() {
		SimpleQueueManagerServiceImpl mgr = new SimpleQueueManagerServiceImpl();
		
		String key = "stack2";
		String val = "value1";
		
		mgr.post(key, val);
		mgr.get(key);
		String actual = (String) mgr.get(key);
		
		assertEquals(null, actual);
	}
}
