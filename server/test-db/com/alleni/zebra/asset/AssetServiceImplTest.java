package com.alleni.zebra.asset;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.InMemoryDbUnitTest;
import com.alleni.zebra.data.dao.AssetDao;
import com.alleni.zebra.data.model.Asset;

public class AssetServiceImplTest extends InMemoryDbUnitTest {
	private Asset asset;
	
	@Autowired AssetDao dao;

	@Autowired AssetService service;
	@Before
	@Override
	@Transactional
	public void setUp() throws Exception {
		super.setUp();
		asset = new Asset();
		asset.setDataSizeInBytes(Long.valueOf(2048l));
		asset.setDeleted(false);
		asset.setFileID("bogus_file_id");
		asset.setMimeType("bogus/type");
		asset.setName("name");
		asset.setPermissions("yahoo");
		asset.setVersion(0L);
		asset.setNextVersionId("0");
		asset.setUserId("some_user");
		dao.create(asset);
	}

	@After
	@Override
	@Transactional
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testGuessExtension() {
		asset.setName("foo.txt");
		String ext = service.guessExtension(asset);
		Assert.assertTrue(ext.equals(".txt"));
		
		asset.setName("foobar");
		ext = service.guessExtension(asset);
		Assert.assertNull(ext);
		
		asset.setName(null);
		asset.setMimeType("audio/mpeg");
		ext = service.guessExtension(asset);
		Assert.assertTrue(ext.equals(".mp3"));
	}

}
