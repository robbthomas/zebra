package com.alleni.zebra.asset;

import java.util.ArrayList;

import junit.framework.Assert;

import org.junit.Test;

/**
 * The following mime-types MUST be supported:
 *   image/jpeg, image/png, image/gif, image/svg+xml
 *   audio/mpeg, audio/mp4,
 *   text/xml, text/plain,
 *   video/x-flv, video/mp4,
 *   application/x-shockwave-flash
 * 
 */
public class MimeCategoryTests {

	@Test
	public void testImageMimes() throws Exception {
		ArrayList<String> imageTypes = new ArrayList<String>();
		imageTypes.add("image/jpeg");
		imageTypes.add("image/png");
		imageTypes.add("image/gif");
		imageTypes.add("image/svg+xml");
		
		for (String s : imageTypes) {
			Assert.assertEquals(MimeCategory.IMAGE, MimeCategory.getCategory(s));
		}
	}

	@Test
	public void testAudioMimes() throws Exception {
		ArrayList<String> types = new ArrayList<String>();
		types.add("audio/mpeg");
		types.add("audio/mp4");
		for (String s : types) {
			Assert.assertEquals(MimeCategory.AUDIO, MimeCategory.getCategory(s));
		}
	}
	
	@Test
	public void testVideoMimes() throws Exception {
		ArrayList<String> types = new ArrayList<String>();
		types.add("video/x-flv");
		types.add("video/mp4");
		for (String s : types) {
			Assert.assertEquals(MimeCategory.VIDEO, MimeCategory.getCategory(s));
		}
	}
	
	@Test
	public void testDocumentMimes() throws Exception {
		ArrayList<String> types = new ArrayList<String>();
		types.add("text/xml");
		types.add("text/plain");
		for (String s : types) {
			Assert.assertEquals(MimeCategory.DOCUMENT, MimeCategory.getCategory(s));
		}
	}
	
	@Test
	public void testApplicationMimes() throws Exception {
		Assert.assertEquals(MimeCategory.APPLICATION, MimeCategory.getCategory("application/x-shockwave-flash"));
	}

	
}
