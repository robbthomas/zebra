package com.alleni.zebra.asset.thumbnail;

import java.io.InputStream;

import javax.activation.MimeType;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class ThumbnailGeneratorTests_ {

	/**
	 * At this point, we're just testing that all the gears mesh,
	 * and that we get some kind of result.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGenerateThumbnailGif() throws Exception {
		byte[] data = readFile("test.gif");
		ThumbnailGenerator generator = ThumbnailGenerator.getInstance(new MimeType("image/gif"));
		generator.generateThumbnail(data);
	}

	
	/**
	 * We don't do svg.  Make sure we blow up in the right way.
	 * 
	 * @throws Exception
	 */
	@Test(expected=ThumbnailNotSupportedException.class)
	public void testGenerateThumbnailSvg() throws Exception {
		byte[] data = readFile("test.svg");
		ThumbnailGenerator generator = ThumbnailGenerator.getInstance(new MimeType("image/svg"));
		generator.generateThumbnail(data);
	}

	
	private byte[] readFile(String name) throws Exception {
		
		InputStream is = this.getClass().getResourceAsStream(name);
		byte[] ret = IOUtils.toByteArray(is);
		return ret;
	}
}
