package com.alleni.zebra;


import java.io.File;

import javax.annotation.PostConstruct;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;
import org.aspectj.util.FileUtil;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Extend this class to build test cases that exercise the DAO layer, and require any
 * Autowired members.
 * 
 * Be sure to override setUp() and tearDown(), and call super.setUp() and super.tearDown().
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "file:web/WEB-INF/zephyr-servlet.xml",
   	"file:web/WEB-INF/context/aspectContext.xml",
   	"file:web/WEB-INF/context/s3Context.xml",
   	"file:web/WEB-INF/context/webMvcContext.xml",
                                   	"file:test-resources/test-application-context.xml" })
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback = false)
@Transactional
public abstract class InMemoryDbUnitTest {
	protected final Log log = LogFactory.getLog(getClass());

        @Autowired
        private JdbcTemplate jdbcTemplate;

        protected JdbcTemplate getJdbcTemplate() {
                return jdbcTemplate;
        }

        @PostConstruct
        public void init() {
        	PropertyConfigurator.configure("src/com/alleni/zebra/properties/log4j.properties");
        }
        
        /** Override and call super */
        public void setUp() throws Exception {
                Assert.assertTrue("setUp must be called within an active Transaction", TransactionSynchronizationManager.isActualTransactionActive());

                String schemaFileName = "test-resources/sql/schema-create-1.ddl";
                String createSchema1 = FileUtil.readAsString(new File(schemaFileName));
                jdbcTemplate.execute(createSchema1);

                // FIXME:  problems loading the foreign keys.
//                String schemaFileName2 = "test-resources/sql/schema-create-2.ddl";
//                String createSchema2 = FileUtil.readAsString(new File(schemaFileName2));
//                jdbcTemplate.execute(createSchema2);
        }


		/** Override and call super */
        public void tearDown() throws Exception {
                Assert.assertTrue("tearDown must be called within an active Transaction", TransactionSynchronizationManager.isActualTransactionActive());
                String schemaFileName = "test-resources/sql/schema-drop.ddl";
                String dropSchema = FileUtil.readAsString(new File(schemaFileName));
                jdbcTemplate.execute(dropSchema);
        }

}