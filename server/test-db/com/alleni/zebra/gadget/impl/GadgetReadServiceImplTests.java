package com.alleni.zebra.gadget.impl;

import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.InMemoryDbUnitTest;

public class GadgetReadServiceImplTests extends InMemoryDbUnitTest {

	@Override
	@Transactional
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	@Transactional
	public void tearDown() throws Exception {
		super.tearDown();
	}


	@Test
	public void testFindById() {
//		fail("Not yet implemented");
	}

	@Test
	public void testLoadChildren() {
//		fail("Not yet implemented");
	}

	@Test
	public void testFindOwnedByAppUserAndGadgetId() {
//		fail("Not yet implemented");
	}

	@Test
	public void testFindAllOwnedByAppUser() {
//		fail("Not yet implemented");
	}

	@Test
	public void testFindAllOwnedByAppUserAndViewableByAppUser() {
//		fail("Not yet implemented");
	}

	@Test
	public void testFindAllOwnedByAppUserAndEditableByAppUser() {
//		fail("Not yet implemented");
	}

	@Test
	public void testFindForViewingBy() {
//		fail("Not yet implemented");
	}

	@Test
	public void testFindForEditingBy() {
//		fail("Not yet implemented");
	}

}
