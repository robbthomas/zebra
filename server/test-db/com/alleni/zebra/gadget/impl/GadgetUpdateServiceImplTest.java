package com.alleni.zebra.gadget.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.InMemoryDbUnitTest;
import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.dao.GadgetDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Gadget;
import com.alleni.zebra.gadget.GadgetUpdateService;
import com.alleni.zebra.license.Permissions;

public class GadgetUpdateServiceImplTest extends InMemoryDbUnitTest {
	
	@Autowired AppUserDao userDao;
	@Autowired GadgetDao gadgetDao;
	@Autowired GadgetUpdateService updateSvc;
	
	private AppUser testUser;
	private Gadget testG;
	
	@Before
	@Override
	@Transactional
	public void setUp() throws Exception {
		super.setUp();
		testUser = new AppUser();
		testUser.setRetired(false);
		testUser.setDescription("Test User");
		testUser.setEmail("no@no.com");
		testUser.setEnabled(true);
		testUser.setPasswd("a password");
		testUser.setShowEmail(true);
		testUser.setUsername("testuser");
		testUser.setFirstName("joe");
		testUser.setLastName("smith");
		testUser.setPasswd("83jkhdf843lkjaeff8adDDS8"); // normally will be done in the service layer
		userDao.create(testUser);
		
		
		testG = new Gadget();
		testG.setAuthor(testUser);
		testG.setChildren(null);
		testG.setContent("this is a very broken gadget.");
		testG.setGadgetRefId(null);
		testG.setIndex(0);
		testG.setInitialValues("broken broken broken");
		testG.setInstanceId(null);
		testG.setName("My Pet Gadget");
		testG.setNextVersion(null);
		testG.setNextVersionId(null);
		testG.setParent(null);

		gadgetDao.create(testG);
	}

	@After
	@Override
	@Transactional
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test(expected=NotFoundException.class)
	@Transactional (noRollbackFor=NotFoundException.class)
	public void testDeleteGadget() throws Exception {
		String id = (String)testG.getId();
		gadgetDao.delete(testG);
		
		@SuppressWarnings("unused")
		Gadget g = gadgetDao.findById(id);
	}

}
