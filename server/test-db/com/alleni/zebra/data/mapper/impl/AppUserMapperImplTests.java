package com.alleni.zebra.data.mapper.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;

import java.sql.ResultSet;
import java.sql.SQLException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.alleni.zebra.data.model.AppUser;

/**
 * There's not a lot to test here, but this is just an example of how to use
 * EasyMock.  This test, for example, found some NPE's in the Mapper.
 *
 */
public class AppUserMapperImplTests {
	ResultSet resultSet;
	AppUserMapperImpl appUserMapper;
	
	@Before
	public void setUp() throws Exception {
		resultSet = createMock(ResultSet.class);
		
		appUserMapper = new AppUserMapperImpl();
		appUserMapper.setIdentityMap(new IdentityMapServiceImpl());
	}

	@Test
	public void testMapRow() throws SQLException {
		expect(resultSet.getString("id")).andReturn("1");
		expect(resultSet.getLong("version")).andReturn(new Long(0));
		expect(resultSet.getString("company")).andReturn("test company");
		expect(resultSet.getDate("date_created")).andReturn(null);
		expect(resultSet.getBoolean("deleted")).andReturn(new Boolean(false));
		expect(resultSet.getString("description")).andReturn("no description");
		expect(resultSet.getString("email")).andReturn("nobody@nowhere.com");
		expect(resultSet.getBoolean("email_show")).andReturn(new Boolean(false));
		expect(resultSet.getDate("last_updated")).andReturn(null);
		expect(resultSet.getString("passwd")).andReturn("password");
		expect(resultSet.getString("user_real_name")).andReturn("Mr. Nobody");
		expect(resultSet.getString("username")).andReturn("nobody");
		expect(resultSet.getString("salt")).andReturn("n0s@lt");
		replay(resultSet);
		
		AppUser user = appUserMapper.mapRow(resultSet, 0);
		Assert.assertTrue(user.getId().toString().equals("1"));
		Assert.assertEquals(user.getVersion().intValue(), 0);
	}

}
