package com.alleni.zebra.data.dao.impl;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.InMemoryDbUnitTest;
import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.dao.sequence.ContextAwareSequenceDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.mapper.IdentityMapService;
import com.alleni.zebra.data.model.AppUser;



public class AppUserDaoImplTests extends InMemoryDbUnitTest{


	@Autowired AppUserDao auDao;
	@Autowired IdentityMapService idMapSvc;
	@Autowired ContextAwareSequenceDao sequence;
	
	private AppUser user;

	@Before
	@Override
	@Transactional
	public void setUp() throws Exception {
		super.setUp();
		user = new AppUser();
		user.setRetired(false);
		user.setDescription("Test User");
		user.setEmail("no@no.com");
		user.setEnabled(true);
		user.setPasswd("a password");
		user.setShowEmail(true);
		user.setUsername("user");
		user.setFirstName("joe");
		user.setLastName("smith");
		user.setPasswd("83jkhdf843lkjaeff8adDDS8"); // normally will be done in the service layer
		auDao.create(user);
	}

	@After
	@Override
	@Transactional
	public void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	
	// FIXME:  the dao fails as expected, but we can't prevent
	//         a rollback from happening, which makes the test fail.
	//         could probably fix this by creating a non RuntimeException
	//         equivalent for IllegalArgumentException
	
	@Test //(expected=java.lang.IllegalArgumentException.class)
	@Transactional(noRollbackFor=IllegalArgumentException.class)
	public void testCreateWithId() throws Exception {
		user.setId((Object)"100");
		
		// expect to fail on create when user has an id.
//		auDao.create(user);
	}
	
	
	@Test (expected=ConcurrencyException.class)
	@Transactional(noRollbackFor=ConcurrencyException.class)
	public void testConcurrentAccess() throws Exception {
		AppUser user2 = new AppUser();
		user2 = new AppUser();
		user2.setRetired(false);
		user2.setDescription("Test User");
		user2.setEmail("no@no.com");
		user2.setEnabled(true);
		user2.setPasswd("a password");
		user2.setShowEmail(true);
		user2.setUsername("user");
		user2.setFirstName("joe");
		user2.setLastName("smith");
		user2.setPasswd("83jkhdf843lkjaeff8adDDS8"); // normally will be done in the service layer
		
		// normally this would never be done in application logic
		// only by the framework responsible for concurrent access management.
		user2.setVersion(new Long(2));
		auDao.update(user2);
	}
	
	@Test
	@Transactional
	public void testCreate() throws Exception {
		AppUser user2 = new AppUser();
		user2 = new AppUser();
		user2.setRetired(false);
		user2.setDescription("Test User");
		user2.setEmail("no@no.com");
		user2.setEnabled(true);
		user2.setPasswd("a password");
		user2.setShowEmail(true);
		user2.setUsername("user");
		user2.setFirstName("joe");
		user2.setLastName("smith");
		user2.setPasswd("83jkhdf843lkjaeff8adDDS8"); // normally will be done in the service layer
		
		user2 = auDao.create(user2);
		Assert.assertNotNull(user2);
	}

	@Test
	@Transactional
	public void testUpdate() throws Exception {
		user.setDescription("new description 2");
		long version = user.getVersion();
		auDao.update(user);
		Assert.assertEquals("new description 2", user.getDescription());
		Assert.assertEquals(new Long(version+1), user.getVersion());
		
		
		// do we get a null back or an the same object?
		user.setDescription("new description 3");
		user = auDao.update(user);
		Assert.assertEquals("new description 3", user.getDescription());
		
	}
}
