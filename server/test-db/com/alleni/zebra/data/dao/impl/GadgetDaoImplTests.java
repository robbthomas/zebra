package com.alleni.zebra.data.dao.impl;

import java.util.ArrayList;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.InMemoryDbUnitTest;
import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.dao.GadgetDao;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Gadget;
import com.alleni.zebra.license.Permissions;
import com.alleni.zebra.security.authorization.Role;

public class GadgetDaoImplTests extends InMemoryDbUnitTest{
	
	@Autowired AppUserDao userDao;
	@Autowired GadgetDao gadgetDao;
	
	private AppUser testUser;
	private Gadget testG;
	
	@Before
	@Override
	@Transactional
	public void setUp() throws Exception {
		super.setUp();
		testUser = new AppUser();
		testUser.setRetired(false);
		testUser.setDescription("Test User");
		testUser.setEmail("no@no.com");
		testUser.setEnabled(true);
		testUser.setPasswd("a password");
		testUser.setShowEmail(true);
		testUser.setUsername("testuser");
		testUser.setFirstName("joe");
		testUser.setLastName("smith");
		testUser.setPasswd("83jkhdf843lkjaeff8adDDS8"); // normally will be done in the service layer
		userDao.create(testUser);
		
		
		testG = new Gadget();
		testG.setAuthor(testUser);
		testG.setChildren(null);
		testG.setContent("this is a very broken gadget.");
		testG.setGadgetRefId(null);
		testG.setIndex(0);
		testG.setInitialValues("broken broken broken");
		testG.setInstanceId(null);
		testG.setName("My Pet Gadget");
		testG.setNextVersion(null);
		testG.setNextVersionId(null);
		testG.setParent(null);

		gadgetDao.create(testG);
	}

	@After
	@Override
	@Transactional
	public void tearDown() throws Exception {
		super.tearDown();
	}

	
	
	@Test
	@Transactional
	public void testCreate() {
		
		ArrayList<GrantedAuthority> auths = new ArrayList<GrantedAuthority>();
		auths.add(new GrantedAuthorityImpl(Role.ROLE_TYPE_SITE_ADMIN.toString()
				.toUpperCase()));
		auths.add(new GrantedAuthorityImpl(Role.ROLE_TYPE_SITE_ADMIN.toString()
				.toUpperCase()));

		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testUser.getUsername(), testUser.getPasswd(), auths));

		
		Gadget g = new Gadget();
		g.setAuthor(testUser);
		g.setAppUserId(testUser.getId().toString());
		g.setChildren(null);
		g.setContent("this is a very broken gadget.");
		g.setGadgetRefId(null);
		g.setIndex(0);
		g.setInitialValues("broken broken broken");
		g.setInstanceId(null);
		g.setName("My Pet Gadget");
		g.setNextVersion(null);
		g.setNextVersionId(null);
		g.setParent(null);

		gadgetDao.create(g);
	}

	@Test
	@Transactional
	public void testfindById() throws Exception {
		Gadget found = gadgetDao.findById(testG.getId().toString());
		boolean eq = found.equals(testG);
		Assert.assertTrue(eq);
	}
	
	
	@Test
	@Transactional
	public void testGetChildrenFor() throws Exception {
		
	}
}
