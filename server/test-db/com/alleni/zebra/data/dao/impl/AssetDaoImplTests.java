package com.alleni.zebra.data.dao.impl;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.InMemoryDbUnitTest;
import com.alleni.zebra.data.dao.AssetDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.Asset;

public class AssetDaoImplTests extends InMemoryDbUnitTest{
	
	private Asset asset;
	
	@Autowired AssetDao dao;
	
	@Before
	@Override
	@Transactional
	public void setUp() throws Exception {
		super.setUp();
		asset = new Asset();
		asset.setDataSizeInBytes(Long.valueOf(2048l));
		asset.setDeleted(false);
		asset.setFileID("bogus_file_id");
		asset.setMimeType("bogus/type");
		asset.setName("name");
		asset.setPermissions("yahoo");
		asset.setVersion(0L);
		asset.setNextVersionId("0");
		asset.setUserId("some_user");
		dao.create(asset);
	}

	@After
	@Override
	@Transactional
	public void tearDown() throws Exception {
		super.tearDown();
	}

	
	@Test
	@Transactional
	public void testFindById() throws NotFoundException {
		String id = (String) asset.getId();
		Assert.assertNotNull(id);
		
		asset = dao.findById(id);
		Assert.assertNotNull(asset);
	}


	@Test
	@Transactional
	public void testUpdate() throws Exception {
		String id = (String) asset.getId();
		asset.setName("new name");
		dao.update(asset);
		
		asset = dao.findById(id);
		Assert.assertEquals("new name", asset.getName());
		
		asset.setName("new name 2");
		long version = asset.getVersion();
		dao.update(asset);
		Assert.assertEquals("new name 2", asset.getName());
		Assert.assertEquals(new Long(version+1), asset.getVersion());
		
		
		// do we get a null back or an the same object?
		asset.setName("new name 3");
		asset = dao.update(asset);
		Assert.assertEquals("new name 3", asset.getName());
		
	}

	@Test
	@Transactional
	public void testDelete() throws Exception {
		
		dao.delete(asset);
		
//		Assert.assertNull(dao.findById(asset.getId()));
	}


}
