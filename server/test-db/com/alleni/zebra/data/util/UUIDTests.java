package com.alleni.zebra.data.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class UUIDTests {

	/**
	 * needs to be 32 char's long, and contain no '-' char's.
	 */
	@Test
	public void testHibernateCompatibleUUID() {
		String uuid = UUID.hibernateCompatibleUUID();
		assertEquals(-1, uuid.indexOf('-'));

		assertEquals(32, uuid.length());
	}

}
