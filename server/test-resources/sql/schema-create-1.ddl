--
-- PostgreSQL database dump
--

-- Started on 2011-04-08 14:35:28 CDT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1700 (class 1259 OID 116616)
-- Dependencies: 2028 6
-- Name: accountmemberroles; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE accountmemberroles (
    accountid bigint NOT NULL,
    memberid text NOT NULL,
    typeid integer,
    retired integer DEFAULT 0,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.accountmemberroles OWNER TO "boreal-dev";

--
-- TOC entry 1731 (class 1259 OID 117049)
-- Dependencies: 2061 6
-- Name: accountprojectnumbers; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE accountprojectnumbers (
    accountid bigint NOT NULL,
    projectnumber text,
    projectid text NOT NULL,
    invoicelineitemid bigint,
    retired integer DEFAULT 0,
    retiredbyid text,
    retireddatetime timestamp without time zone,
    retiredreasonid integer
);


ALTER TABLE public.accountprojectnumbers OWNER TO "boreal-dev";

--
-- TOC entry 1692 (class 1259 OID 116479)
-- Dependencies: 6
-- Name: seq_accounts_accountid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_accounts_accountid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_accounts_accountid OWNER TO "boreal-dev";

--
-- TOC entry 1693 (class 1259 OID 116481)
-- Dependencies: 2019 2020 2021 2022 6
-- Name: accounts; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE accounts (
    accountid bigint DEFAULT nextval('seq_accounts_accountid'::regclass) NOT NULL,
    companyid integer,
    typeid integer NOT NULL,
    name text,
    description text,
    editedbyid text NOT NULL,
    editeddatetime timestamp without time zone DEFAULT now(),
    tokenbalance integer DEFAULT 0,
    retired integer DEFAULT 0,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.accounts OWNER TO "boreal-dev";

--
-- TOC entry 1691 (class 1259 OID 116458)
-- Dependencies: 2016 2017 2018 6
-- Name: accounttypes; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE accounttypes (
    accounttypeid integer NOT NULL,
    tag text,
    name text,
    description text,
    terms text DEFAULT 'month'::text,
    price integer,
    currencytypeid integer DEFAULT 1,
    displayorder integer,
    editedbyid text,
    editeddatetime timestamp without time zone DEFAULT now()
);


ALTER TABLE public.accounttypes OWNER TO "boreal-dev";

--
-- TOC entry 1695 (class 1259 OID 116528)
-- Dependencies: 6
-- Name: seq_addresses_addressid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_addresses_addressid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_addresses_addressid OWNER TO "boreal-dev";

--
-- TOC entry 1696 (class 1259 OID 116530)
-- Dependencies: 2023 2024 2025 6
-- Name: addresses; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE addresses (
    addressid bigint DEFAULT nextval('seq_addresses_addressid'::regclass) NOT NULL,
    typeid integer NOT NULL,
    accountid bigint NOT NULL,
    name text,
    company text,
    address1 text,
    address2 text,
    city text,
    stateprovince text,
    zippostalcode text,
    country text,
    editedbyid text NOT NULL,
    editeddatetime timestamp without time zone DEFAULT now(),
    retired integer DEFAULT 0 NOT NULL,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.addresses OWNER TO "boreal-dev";

--
-- TOC entry 1694 (class 1259 OID 116515)
-- Dependencies: 6
-- Name: addresstelephonetypes; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE addresstelephonetypes (
    addresstelephonetypeid integer NOT NULL,
    name text,
    description text,
    displayorder integer,
    retired integer,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.addresstelephonetypes OWNER TO "boreal-dev";

--
-- TOC entry 1660 (class 1259 OID 113575)
-- Dependencies: 6
-- Name: app_role; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE app_role (
    id bigint NOT NULL,
    version bigint NOT NULL,
    authority character varying(255) NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE public.app_role OWNER TO "boreal-dev";

--
-- TOC entry 1661 (class 1259 OID 113581)
-- Dependencies: 6
-- Name: app_role_people; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE app_role_people (
    app_role_id bigint NOT NULL,
    app_user_id character varying(255) NOT NULL
);


ALTER TABLE public.app_role_people OWNER TO "boreal-dev";

--
-- TOC entry 1662 (class 1259 OID 113584)
-- Dependencies: 6
-- Name: app_user; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE app_user (
    id character varying(255) NOT NULL,
    version bigint NOT NULL,
    date_created timestamp without time zone NOT NULL,
    retired boolean NOT NULL,
    description character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    emailshow boolean NOT NULL,
    enabled boolean NOT NULL,
    last_updated timestamp without time zone NOT NULL,
    passwordhash character varying(255) NOT NULL,
    displayname character varying(255) NOT NULL,
    salt character varying(255),
    editedbyid character varying(255),
    retiredbyid character varying(255),
    retireddatetime timestamp without time zone,
    passwordsalt character varying(128),
    firstname character varying(128),
    lastname character varying(128)
);


ALTER TABLE public.app_user OWNER TO "boreal-dev";

--
-- TOC entry 1663 (class 1259 OID 113590)
-- Dependencies: 6
-- Name: asset; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE asset (
    id character varying(255) NOT NULL,
    version bigint NOT NULL,
    data_size_in_bytes bigint NOT NULL,
    date_created timestamp without time zone NOT NULL,
    deleted boolean NOT NULL,
    fileid character varying(255) NOT NULL,
    last_updated timestamp without time zone NOT NULL,
    mime_type character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    next_version_id character varying(255),
    permissions character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.asset OWNER TO "boreal-dev";

--
-- TOC entry 1664 (class 1259 OID 113596)
-- Dependencies: 6
-- Name: authtoken; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE authtoken (
    id character varying(255) NOT NULL,
    version bigint NOT NULL,
    ip character varying(255) NOT NULL,
    expire_date timestamp without time zone NOT NULL,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.authtoken OWNER TO "boreal-dev";

--
-- TOC entry 1665 (class 1259 OID 113602)
-- Dependencies: 6
-- Name: blog_entry; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE blog_entry (
    id bigint NOT NULL,
    version bigint NOT NULL,
    body text NOT NULL,
    date_created timestamp without time zone NOT NULL,
    last_updated timestamp without time zone NOT NULL,
    subject character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.blog_entry OWNER TO "boreal-dev";

--
-- TOC entry 1707 (class 1259 OID 116725)
-- Dependencies: 6
-- Name: seq_categories_categoryid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_categories_categoryid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_categories_categoryid OWNER TO "boreal-dev";

--
-- TOC entry 1708 (class 1259 OID 116727)
-- Dependencies: 2033 6
-- Name: categories; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE categories (
    categoryid bigint DEFAULT nextval('seq_categories_categoryid'::regclass) NOT NULL,
    parentcategoryid integer,
    name text,
    description text,
    typeid integer NOT NULL,
    categoryorder integer,
    categorylevel integer,
    editedbyid text NOT NULL,
    editeddatetime timestamp without time zone,
    retired integer,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.categories OWNER TO "boreal-dev";

--
-- TOC entry 1666 (class 1259 OID 113608)
-- Dependencies: 6
-- Name: category; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE category (
    id bigint NOT NULL,
    version bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.category OWNER TO "boreal-dev";

--
-- TOC entry 1688 (class 1259 OID 116430)
-- Dependencies: 6
-- Name: seq_companies_companyid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_companies_companyid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_companies_companyid OWNER TO "boreal-dev";

--
-- TOC entry 1689 (class 1259 OID 116432)
-- Dependencies: 2011 2012 6
-- Name: companies; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE companies (
    companyid integer DEFAULT nextval('seq_companies_companyid'::regclass) NOT NULL,
    name text,
    description text,
    companyurl text,
    editedbyid text NOT NULL,
    editeddatetime timestamp without time zone DEFAULT now()
);


ALTER TABLE public.companies OWNER TO "boreal-dev";

--
-- TOC entry 1667 (class 1259 OID 113611)
-- Dependencies: 6
-- Name: component; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE component (
    id bigint NOT NULL,
    version bigint NOT NULL,
    initial_values character varying(255) NOT NULL,
    instanceid character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    vermillion_version character varying(255) NOT NULL
);


ALTER TABLE public.component OWNER TO "boreal-dev";

--
-- TOC entry 1668 (class 1259 OID 113617)
-- Dependencies: 6
-- Name: contact; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE contact (
    id bigint NOT NULL,
    version bigint NOT NULL,
    comment character varying(1000) NOT NULL,
    email character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.contact OWNER TO "boreal-dev";

--
-- TOC entry 1716 (class 1259 OID 116873)
-- Dependencies: 6
-- Name: seq_creditcards_creditcardid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_creditcards_creditcardid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_creditcards_creditcardid OWNER TO "boreal-dev";

--
-- TOC entry 1717 (class 1259 OID 116875)
-- Dependencies: 2046 2047 2048 6
-- Name: creditcards; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE creditcards (
    creditcardid bigint DEFAULT nextval('seq_creditcards_creditcardid'::regclass) NOT NULL,
    accountid bigint NOT NULL,
    addressid bigint NOT NULL,
    maskednumber text,
    expirationmonth text,
    expirationyear text,
    customerprofileid text,
    customerpaymentprofileid text,
    editedbyid text NOT NULL,
    editeddatetime timestamp without time zone DEFAULT now(),
    retired integer DEFAULT 0,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.creditcards OWNER TO "boreal-dev";

--
-- TOC entry 1690 (class 1259 OID 116447)
-- Dependencies: 2013 2014 2015 6
-- Name: currencytypes; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE currencytypes (
    currencytypeid integer NOT NULL,
    name text,
    currencyname text,
    currencycode text,
    currencysymbol text,
    countrycode text,
    "precision" integer DEFAULT 2,
    majordelimiter text DEFAULT ','::text,
    minordelimiter text DEFAULT '.'::text
);


ALTER TABLE public.currencytypes OWNER TO "boreal-dev";

--
-- TOC entry 1686 (class 1259 OID 116421)
-- Dependencies: 6
-- Name: seq_databaseversionid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_databaseversionid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_databaseversionid OWNER TO "boreal-dev";

--
-- TOC entry 1687 (class 1259 OID 116423)
-- Dependencies: 2009 2010 6
-- Name: databaseversion; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE databaseversion (
    databaseversionid bigint DEFAULT nextval('seq_databaseversionid'::regclass) NOT NULL,
    versionnumber integer NOT NULL,
    versiondatetime timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.databaseversion OWNER TO "boreal-dev";

--
-- TOC entry 1669 (class 1259 OID 113623)
-- Dependencies: 6
-- Name: demo; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE demo (
    id bigint NOT NULL,
    version bigint NOT NULL,
    category_id bigint NOT NULL,
    description character varying(255),
    featured boolean NOT NULL,
    thumbnail_url character varying(255),
    title character varying(255) NOT NULL,
    topic_id bigint NOT NULL,
    video_url character varying(255)
);


ALTER TABLE public.demo OWNER TO "boreal-dev";

--
-- TOC entry 1729 (class 1259 OID 117032)
-- Dependencies: 6
-- Name: seq_depositlineitems_depositlineitemid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_depositlineitems_depositlineitemid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_depositlineitems_depositlineitemid OWNER TO "boreal-dev";

--
-- TOC entry 1730 (class 1259 OID 117034)
-- Dependencies: 2060 6
-- Name: depositlineitems; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE depositlineitems (
    depositlineitemid bigint DEFAULT nextval('seq_depositlineitems_depositlineitemid'::regclass) NOT NULL,
    depositid bigint NOT NULL,
    linenumber integer,
    amount integer,
    description text
);


ALTER TABLE public.depositlineitems OWNER TO "boreal-dev";

--
-- TOC entry 1727 (class 1259 OID 117011)
-- Dependencies: 6
-- Name: seq_deposits_depositid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_deposits_depositid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_deposits_depositid OWNER TO "boreal-dev";

--
-- TOC entry 1728 (class 1259 OID 117013)
-- Dependencies: 2058 2059 6
-- Name: deposits; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE deposits (
    depositid bigint DEFAULT nextval('seq_deposits_depositid'::regclass) NOT NULL,
    accountid bigint NOT NULL,
    amount integer NOT NULL,
    currencytypeid integer NOT NULL,
    depositdatetime timestamp without time zone DEFAULT now()
);


ALTER TABLE public.deposits OWNER TO "boreal-dev";

--
-- TOC entry 1718 (class 1259 OID 116902)
-- Dependencies: 6
-- Name: seq_echecks_echeckid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_echecks_echeckid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_echecks_echeckid OWNER TO "boreal-dev";

--
-- TOC entry 1719 (class 1259 OID 116904)
-- Dependencies: 2049 2050 2051 6
-- Name: echecks; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE echecks (
    echeckid bigint DEFAULT nextval('seq_echecks_echeckid'::regclass) NOT NULL,
    accountid bigint NOT NULL,
    routingnumber text,
    bankaccountnumber text,
    bankname text,
    bankaccountname text,
    customerprofileid text,
    customerpaymentprofileid text,
    editedbyid text NOT NULL,
    editeddatetime timestamp without time zone DEFAULT now(),
    retired integer DEFAULT 0,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.echecks OWNER TO "boreal-dev";

--
-- TOC entry 1670 (class 1259 OID 113629)
-- Dependencies: 6
-- Name: encrypted_data; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE encrypted_data (
    id character varying(32) NOT NULL,
    version bigint NOT NULL,
    data_item character varying(512) NOT NULL
);


ALTER TABLE public.encrypted_data OWNER TO "boreal-dev";

--
-- TOC entry 1702 (class 1259 OID 116662)
-- Dependencies: 6
-- Name: seq_feedback_feedbackid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_feedback_feedbackid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_feedback_feedbackid OWNER TO "boreal-dev";

--
-- TOC entry 1703 (class 1259 OID 116664)
-- Dependencies: 2031 2032 6
-- Name: feedback; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE feedback (
    feedbackid bigint DEFAULT nextval('seq_feedback_feedbackid'::regclass) NOT NULL,
    rememberid text,
    reaccountid bigint,
    reprojectid text,
    comments text NOT NULL,
    editedbyid text,
    editeddatetime timestamp without time zone DEFAULT now()
);


ALTER TABLE public.feedback OWNER TO "boreal-dev";

--
-- TOC entry 1671 (class 1259 OID 113635)
-- Dependencies: 6
-- Name: gadget_component; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE gadget_component (
    gadget_components_id character varying(255),
    component_id bigint
);


ALTER TABLE public.gadget_component OWNER TO "boreal-dev";

--
-- TOC entry 1672 (class 1259 OID 113638)
-- Dependencies: 6
-- Name: gadget_permission; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE gadget_permission (
    id bigint NOT NULL,
    version bigint NOT NULL,
    gadget_id character varying(255) NOT NULL
);


ALTER TABLE public.gadget_permission OWNER TO "boreal-dev";

--
-- TOC entry 1673 (class 1259 OID 113641)
-- Dependencies: 6
-- Name: gadget_ribbon; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE gadget_ribbon (
    gadget_ribbons_id character varying(255),
    ribbon_id bigint
);


ALTER TABLE public.gadget_ribbon OWNER TO "boreal-dev";

--
-- TOC entry 1674 (class 1259 OID 113644)
-- Dependencies: 6
-- Name: gadget_state; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE gadget_state (
    id character varying(255) NOT NULL,
    version bigint NOT NULL,
    app_user_id character varying(255) NOT NULL,
    author_mode boolean NOT NULL,
    content text NOT NULL,
    root_gadget_id character varying(255) NOT NULL
);


ALTER TABLE public.gadget_state OWNER TO "boreal-dev";

--
-- TOC entry 1675 (class 1259 OID 113650)
-- Dependencies: 6
-- Name: gadget_tag; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE gadget_tag (
    id bigint NOT NULL,
    version bigint NOT NULL,
    date_created timestamp without time zone NOT NULL,
    gadget_id character varying(255) NOT NULL,
    tag character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL,
    last_updated timestamp without time zone
);


ALTER TABLE public.gadget_tag OWNER TO "boreal-dev";

--
-- TOC entry 1676 (class 1259 OID 113656)
-- Dependencies: 6
-- Name: gadget_wire; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE gadget_wire (
    gadget_wires_id character varying(255),
    wire_id bigint
);


ALTER TABLE public.gadget_wire OWNER TO "boreal-dev";

--
-- TOC entry 1677 (class 1259 OID 113659)
-- Dependencies: 6
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO "boreal-dev";

--
-- TOC entry 1722 (class 1259 OID 116951)
-- Dependencies: 6
-- Name: seq_invoicelineitems_invoicelineitemid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_invoicelineitems_invoicelineitemid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_invoicelineitems_invoicelineitemid OWNER TO "boreal-dev";

--
-- TOC entry 1723 (class 1259 OID 116953)
-- Dependencies: 2054 6
-- Name: invoicelineitems; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE invoicelineitems (
    invoicelineitemid bigint DEFAULT nextval('seq_invoicelineitems_invoicelineitemid'::regclass) NOT NULL,
    invoiceid integer NOT NULL,
    linenumber integer,
    cost integer,
    description text,
    projectid text,
    projectnumber text
);


ALTER TABLE public.invoicelineitems OWNER TO "boreal-dev";

--
-- TOC entry 1720 (class 1259 OID 116930)
-- Dependencies: 6
-- Name: seq_invoices_invoiceid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_invoices_invoiceid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_invoices_invoiceid OWNER TO "boreal-dev";

--
-- TOC entry 1721 (class 1259 OID 116932)
-- Dependencies: 2052 2053 6
-- Name: invoices; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE invoices (
    invoiceid bigint DEFAULT nextval('seq_invoices_invoiceid'::regclass) NOT NULL,
    accountid bigint NOT NULL,
    cachedcosttotal integer DEFAULT 0,
    currencytypeid integer NOT NULL
);


ALTER TABLE public.invoices OWNER TO "boreal-dev";

--
-- TOC entry 1713 (class 1259 OID 116811)
-- Dependencies: 6
-- Name: licensetypes; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE licensetypes (
    licensetypeid integer NOT NULL,
    name text,
    description text,
    displayorder integer
);


ALTER TABLE public.licensetypes OWNER TO "boreal-dev";

--
-- TOC entry 1678 (class 1259 OID 113661)
-- Dependencies: 6
-- Name: manual; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE manual (
    id bigint NOT NULL,
    version bigint NOT NULL,
    added timestamp without time zone NOT NULL,
    category_id bigint NOT NULL,
    manual_url character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.manual OWNER TO "boreal-dev";

--
-- TOC entry 1679 (class 1259 OID 113667)
-- Dependencies: 6
-- Name: parent; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE parent (
    id character varying(255) NOT NULL,
    version bigint NOT NULL,
    index integer,
    gadgetrefid character varying(255),
    initialvalue character varying(255),
    instanceid character varying(255),
    content text,
    date_created timestamp without time zone,
    description character varying(255),
    initialvalues text,
    last_updated timestamp without time zone,
    meta_data text,
    name character varying(255),
    nextversionid character varying(255),
    price integer,
    type character varying(255),
    ownedbyid character varying(255),
    categoryid integer,
    currencytypeid integer,
    thumbnailid character varying(255),
    cachedlaunchcount integer
);


ALTER TABLE public.parent OWNER TO "boreal-dev";

--
-- TOC entry 1680 (class 1259 OID 113673)
-- Dependencies: 6
-- Name: parent_child; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE parent_child (
    parent_children_id character varying(255),
    child_id character varying(255)
);


ALTER TABLE public.parent_child OWNER TO "boreal-dev";

--
-- TOC entry 1725 (class 1259 OID 116980)
-- Dependencies: 6
-- Name: seq_payments_paymentid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_payments_paymentid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_payments_paymentid OWNER TO "boreal-dev";

--
-- TOC entry 1726 (class 1259 OID 116982)
-- Dependencies: 2055 2056 2057 6
-- Name: payments; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE payments (
    paymentid bigint DEFAULT nextval('seq_payments_paymentid'::regclass) NOT NULL,
    invoiceid bigint NOT NULL,
    amount integer DEFAULT 0 NOT NULL,
    currencytypeid integer NOT NULL,
    paymenttypeid integer NOT NULL,
    paymentagentid bigint NOT NULL,
    transactionid text,
    paymentdatetime timestamp without time zone DEFAULT now()
);


ALTER TABLE public.payments OWNER TO "boreal-dev";

--
-- TOC entry 1724 (class 1259 OID 116972)
-- Dependencies: 6
-- Name: paymenttypes; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE paymenttypes (
    paymenttypeid integer NOT NULL,
    name text,
    description text,
    displayorder integer
);


ALTER TABLE public.paymenttypes OWNER TO "boreal-dev";

--
-- TOC entry 1681 (class 1259 OID 113679)
-- Dependencies: 6
-- Name: profile; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE profile (
    id bigint NOT NULL,
    version bigint NOT NULL,
    biography text,
    deleted boolean NOT NULL,
    fname character varying(255),
    image bytea,
    image_mime_type character varying(255),
    image_size bigint NOT NULL,
    lname character varying(255),
    permissions character varying(255) NOT NULL,
    siteurl character varying(255),
    twitter character varying(255),
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.profile OWNER TO "boreal-dev";

--
-- TOC entry 1701 (class 1259 OID 116641)
-- Dependencies: 2029 2030 6
-- Name: projectmemberroles; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE projectmemberroles (
    projectid text NOT NULL,
    memberid text NOT NULL,
    typeid integer DEFAULT 3 NOT NULL,
    retired integer DEFAULT 0,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.projectmemberroles OWNER TO "boreal-dev";

--
-- TOC entry 1705 (class 1259 OID 116709)
-- Dependencies: 6
-- Name: projecttypes; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE projecttypes (
    projecttypeid integer NOT NULL,
    name text,
    description text,
    displayorder integer
);


ALTER TABLE public.projecttypes OWNER TO "boreal-dev";

--
-- TOC entry 1714 (class 1259 OID 116819)
-- Dependencies: 6
-- Name: seq_publish_publishid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_publish_publishid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_publish_publishid OWNER TO "boreal-dev";

--
-- TOC entry 1715 (class 1259 OID 116821)
-- Dependencies: 2037 2038 2039 2040 2041 2042 2043 2044 2045 6
-- Name: publish; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE publish (
    publishid bigint DEFAULT nextval('seq_publish_publishid'::regclass) NOT NULL,
    versionmajor integer DEFAULT 0 NOT NULL,
    versionminor integer DEFAULT 0 NOT NULL,
    versiondot integer DEFAULT 0 NOT NULL,
    versionbuildnumber integer DEFAULT 0 NOT NULL,
    statusid integer NOT NULL,
    publishtypeid integer NOT NULL,
    licensetypeid integer NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    currencytypeid integer NOT NULL,
    launchcount integer DEFAULT 0 NOT NULL,
    editedbyid text NOT NULL,
    editeddatetime timestamp without time zone DEFAULT now(),
    retired integer DEFAULT 0,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.publish OWNER TO "boreal-dev";

--
-- TOC entry 1711 (class 1259 OID 116795)
-- Dependencies: 6
-- Name: publishstatuses; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE publishstatuses (
    publishstatusid integer NOT NULL,
    name text,
    description text,
    displayorder integer
);


ALTER TABLE public.publishstatuses OWNER TO "boreal-dev";

--
-- TOC entry 1712 (class 1259 OID 116803)
-- Dependencies: 6
-- Name: publishtypes; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE publishtypes (
    publishtypeid integer NOT NULL,
    name text,
    description text,
    displayorder integer
);


ALTER TABLE public.publishtypes OWNER TO "boreal-dev";

--
-- TOC entry 1706 (class 1259 OID 116717)
-- Dependencies: 6
-- Name: retiredreasontypes; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE retiredreasontypes (
    retiredreasontypeid integer NOT NULL,
    name text,
    description text,
    displayorder integer
);


ALTER TABLE public.retiredreasontypes OWNER TO "boreal-dev";

--
-- TOC entry 1682 (class 1259 OID 113685)
-- Dependencies: 6
-- Name: ribbon; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE ribbon (
    id bigint NOT NULL,
    version bigint NOT NULL
);


ALTER TABLE public.ribbon OWNER TO "boreal-dev";

--
-- TOC entry 1699 (class 1259 OID 116603)
-- Dependencies: 6
-- Name: roletypes; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE roletypes (
    roletypeid integer NOT NULL,
    name text,
    description text,
    displayorder integer,
    retired integer,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.roletypes OWNER TO "boreal-dev";

--
-- TOC entry 1709 (class 1259 OID 116760)
-- Dependencies: 6
-- Name: seq_screenshots_screenshotid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_screenshots_screenshotid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_screenshots_screenshotid OWNER TO "boreal-dev";

--
-- TOC entry 1710 (class 1259 OID 116762)
-- Dependencies: 2034 2035 2036 6
-- Name: screenshots; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE screenshots (
    screenshotid bigint DEFAULT nextval('seq_screenshots_screenshotid'::regclass) NOT NULL,
    projectid text NOT NULL,
    ordernumber integer,
    assetid text NOT NULL,
    description text,
    editedbyid text NOT NULL,
    editeddatetime timestamp without time zone DEFAULT now(),
    retired integer DEFAULT 0,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.screenshots OWNER TO "boreal-dev";

--
-- TOC entry 1697 (class 1259 OID 116563)
-- Dependencies: 6
-- Name: seq_telephones_telephoneid; Type: SEQUENCE; Schema: public; Owner: boreal-dev
--

CREATE SEQUENCE seq_telephones_telephoneid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_telephones_telephoneid OWNER TO "boreal-dev";

--
-- TOC entry 1704 (class 1259 OID 116698)
-- Dependencies: 6
-- Name: systemadministrators; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE systemadministrators (
    memberid text NOT NULL,
    passwordhash text,
    passwordsalt text
);


ALTER TABLE public.systemadministrators OWNER TO "boreal-dev";

--
-- TOC entry 1698 (class 1259 OID 116565)
-- Dependencies: 2026 2027 6
-- Name: telephones; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE telephones (
    telephoneid bigint DEFAULT nextval('seq_telephones_telephoneid'::regclass) NOT NULL,
    typeid integer NOT NULL,
    accountid bigint NOT NULL,
    memberid text,
    label text,
    number text,
    editedbyid text NOT NULL,
    editeddatetime timestamp without time zone,
    retired integer DEFAULT 0,
    retiredbyid text,
    retireddatetime timestamp without time zone
);


ALTER TABLE public.telephones OWNER TO "boreal-dev";

--
-- TOC entry 1683 (class 1259 OID 113688)
-- Dependencies: 6
-- Name: topic; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE topic (
    id bigint NOT NULL,
    version bigint NOT NULL,
    category_id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.topic OWNER TO "boreal-dev";

--
-- TOC entry 1684 (class 1259 OID 113691)
-- Dependencies: 6
-- Name: user_gadget_permission; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE user_gadget_permission (
    id bigint NOT NULL,
    version bigint NOT NULL,
    gadget_id character varying(255) NOT NULL,
    user_id character varying(255)
);


ALTER TABLE public.user_gadget_permission OWNER TO "boreal-dev";

--
-- TOC entry 1685 (class 1259 OID 113697)
-- Dependencies: 6
-- Name: wire; Type: TABLE; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE TABLE wire (
    id bigint NOT NULL,
    version bigint NOT NULL,
    master_instanceid character varying(255) NOT NULL,
    master_ribbonid character varying(255) NOT NULL,
    slave_instanceid character varying(255) NOT NULL,
    slave_ribbonid character varying(255) NOT NULL
);


ALTER TABLE public.wire OWNER TO "boreal-dev";

--
-- TOC entry 2130 (class 2606 OID 116492)
-- Dependencies: 1693 1693
-- Name: accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (accountid);


--
-- TOC entry 2128 (class 2606 OID 116468)
-- Dependencies: 1691 1691
-- Name: accounttypes_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY accounttypes
    ADD CONSTRAINT accounttypes_pkey PRIMARY KEY (accounttypeid);


--
-- TOC entry 2136 (class 2606 OID 116540)
-- Dependencies: 1696 1696
-- Name: addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (addressid);


--
-- TOC entry 2134 (class 2606 OID 116522)
-- Dependencies: 1694 1694
-- Name: addresstelephonetypes_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY addresstelephonetypes
    ADD CONSTRAINT addresstelephonetypes_pkey PRIMARY KEY (addresstelephonetypeid);


--
-- TOC entry 2063 (class 2606 OID 116170)
-- Dependencies: 1660 1660
-- Name: app_role_authority_key; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY app_role
    ADD CONSTRAINT app_role_authority_key UNIQUE (authority);


--
-- TOC entry 2067 (class 2606 OID 116172)
-- Dependencies: 1661 1661 1661
-- Name: app_role_people_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY app_role_people
    ADD CONSTRAINT app_role_people_pkey PRIMARY KEY (app_role_id, app_user_id);


--
-- TOC entry 2065 (class 2606 OID 116174)
-- Dependencies: 1660 1660
-- Name: app_role_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY app_role
    ADD CONSTRAINT app_role_pkey PRIMARY KEY (id);


--
-- TOC entry 2069 (class 2606 OID 116176)
-- Dependencies: 1662 1662
-- Name: app_user_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY app_user
    ADD CONSTRAINT app_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2071 (class 2606 OID 116178)
-- Dependencies: 1662 1662
-- Name: app_user_username_key; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY app_user
    ADD CONSTRAINT app_user_username_key UNIQUE (displayname);


--
-- TOC entry 2073 (class 2606 OID 116180)
-- Dependencies: 1663 1663
-- Name: asset_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY asset
    ADD CONSTRAINT asset_pkey PRIMARY KEY (id);


--
-- TOC entry 2075 (class 2606 OID 116182)
-- Dependencies: 1664 1664
-- Name: authtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY authtoken
    ADD CONSTRAINT authtoken_pkey PRIMARY KEY (id);


--
-- TOC entry 2077 (class 2606 OID 116184)
-- Dependencies: 1665 1665
-- Name: blog_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY blog_entry
    ADD CONSTRAINT blog_entry_pkey PRIMARY KEY (id);


--
-- TOC entry 2163 (class 2606 OID 116735)
-- Dependencies: 1708 1708
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (categoryid);


--
-- TOC entry 2079 (class 2606 OID 116186)
-- Dependencies: 1666 1666
-- Name: category_name_key; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_name_key UNIQUE (name);


--
-- TOC entry 2081 (class 2606 OID 116188)
-- Dependencies: 1666 1666
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- TOC entry 2124 (class 2606 OID 116441)
-- Dependencies: 1689 1689
-- Name: companies_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (companyid);


--
-- TOC entry 2083 (class 2606 OID 116190)
-- Dependencies: 1667 1667
-- Name: component_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY component
    ADD CONSTRAINT component_pkey PRIMARY KEY (id);


--
-- TOC entry 2085 (class 2606 OID 116192)
-- Dependencies: 1668 1668
-- Name: contact_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);


--
-- TOC entry 2186 (class 2606 OID 116885)
-- Dependencies: 1717 1717
-- Name: creditcards_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY creditcards
    ADD CONSTRAINT creditcards_pkey PRIMARY KEY (creditcardid);


--
-- TOC entry 2126 (class 2606 OID 116457)
-- Dependencies: 1690 1690
-- Name: currencytypes_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY currencytypes
    ADD CONSTRAINT currencytypes_pkey PRIMARY KEY (currencytypeid);


--
-- TOC entry 2122 (class 2606 OID 116429)
-- Dependencies: 1687 1687
-- Name: databaseversion_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY databaseversion
    ADD CONSTRAINT databaseversion_pkey PRIMARY KEY (databaseversionid);


--
-- TOC entry 2087 (class 2606 OID 116194)
-- Dependencies: 1669 1669
-- Name: demo_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY demo
    ADD CONSTRAINT demo_pkey PRIMARY KEY (id);


--
-- TOC entry 2089 (class 2606 OID 116196)
-- Dependencies: 1669 1669
-- Name: demo_title_key; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY demo
    ADD CONSTRAINT demo_title_key UNIQUE (title);


--
-- TOC entry 2208 (class 2606 OID 117042)
-- Dependencies: 1730 1730
-- Name: depositlineitems_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY depositlineitems
    ADD CONSTRAINT depositlineitems_pkey PRIMARY KEY (depositlineitemid);


--
-- TOC entry 2204 (class 2606 OID 117019)
-- Dependencies: 1728 1728
-- Name: deposits_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY deposits
    ADD CONSTRAINT deposits_pkey PRIMARY KEY (depositid);


--
-- TOC entry 2189 (class 2606 OID 116914)
-- Dependencies: 1719 1719
-- Name: echecks_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY echecks
    ADD CONSTRAINT echecks_pkey PRIMARY KEY (echeckid);


--
-- TOC entry 2091 (class 2606 OID 116198)
-- Dependencies: 1670 1670
-- Name: encrypted_data_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY encrypted_data
    ADD CONSTRAINT encrypted_data_pkey PRIMARY KEY (id);


--
-- TOC entry 2153 (class 2606 OID 116673)
-- Dependencies: 1703 1703
-- Name: feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT feedback_pkey PRIMARY KEY (feedbackid);


--
-- TOC entry 2093 (class 2606 OID 116200)
-- Dependencies: 1672 1672
-- Name: gadget_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY gadget_permission
    ADD CONSTRAINT gadget_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 2095 (class 2606 OID 116202)
-- Dependencies: 1674 1674 1674
-- Name: gadget_state_app_user_id_key; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY gadget_state
    ADD CONSTRAINT gadget_state_app_user_id_key UNIQUE (app_user_id, root_gadget_id);


--
-- TOC entry 2097 (class 2606 OID 116204)
-- Dependencies: 1674 1674
-- Name: gadget_state_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY gadget_state
    ADD CONSTRAINT gadget_state_pkey PRIMARY KEY (id);


--
-- TOC entry 2099 (class 2606 OID 116206)
-- Dependencies: 1675 1675
-- Name: gadget_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY gadget_tag
    ADD CONSTRAINT gadget_tag_pkey PRIMARY KEY (id);


--
-- TOC entry 2195 (class 2606 OID 116961)
-- Dependencies: 1723 1723
-- Name: invoicelineitems_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY invoicelineitems
    ADD CONSTRAINT invoicelineitems_pkey PRIMARY KEY (invoicelineitemid);


--
-- TOC entry 2191 (class 2606 OID 116938)
-- Dependencies: 1721 1721
-- Name: invoices_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY invoices
    ADD CONSTRAINT invoices_pkey PRIMARY KEY (invoiceid);


--
-- TOC entry 2177 (class 2606 OID 116818)
-- Dependencies: 1713 1713
-- Name: licensetypes_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY licensetypes
    ADD CONSTRAINT licensetypes_pkey PRIMARY KEY (licensetypeid);


--
-- TOC entry 2101 (class 2606 OID 116208)
-- Dependencies: 1678 1678
-- Name: manual_name_key; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY manual
    ADD CONSTRAINT manual_name_key UNIQUE (name);


--
-- TOC entry 2103 (class 2606 OID 116210)
-- Dependencies: 1678 1678
-- Name: manual_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY manual
    ADD CONSTRAINT manual_pkey PRIMARY KEY (id);


--
-- TOC entry 2106 (class 2606 OID 116212)
-- Dependencies: 1679 1679
-- Name: parent_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY parent
    ADD CONSTRAINT parent_pkey PRIMARY KEY (id);


--
-- TOC entry 2202 (class 2606 OID 116992)
-- Dependencies: 1726 1726
-- Name: payments_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (paymentid);


--
-- TOC entry 2197 (class 2606 OID 116979)
-- Dependencies: 1724 1724
-- Name: paymenttypes_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY paymenttypes
    ADD CONSTRAINT paymenttypes_pkey PRIMARY KEY (paymenttypeid);


--
-- TOC entry 2108 (class 2606 OID 116214)
-- Dependencies: 1681 1681
-- Name: profile_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (id);


--
-- TOC entry 2110 (class 2606 OID 116216)
-- Dependencies: 1681 1681
-- Name: profile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY profile
    ADD CONSTRAINT profile_user_id_key UNIQUE (user_id);


--
-- TOC entry 2159 (class 2606 OID 116716)
-- Dependencies: 1705 1705
-- Name: projecttypes_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY projecttypes
    ADD CONSTRAINT projecttypes_pkey PRIMARY KEY (projecttypeid);


--
-- TOC entry 2184 (class 2606 OID 116837)
-- Dependencies: 1715 1715
-- Name: publish_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY publish
    ADD CONSTRAINT publish_pkey PRIMARY KEY (publishid);


--
-- TOC entry 2173 (class 2606 OID 116802)
-- Dependencies: 1711 1711
-- Name: publishstatuses_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY publishstatuses
    ADD CONSTRAINT publishstatuses_pkey PRIMARY KEY (publishstatusid);


--
-- TOC entry 2175 (class 2606 OID 116810)
-- Dependencies: 1712 1712
-- Name: publishtypes_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY publishtypes
    ADD CONSTRAINT publishtypes_pkey PRIMARY KEY (publishtypeid);


--
-- TOC entry 2161 (class 2606 OID 116724)
-- Dependencies: 1706 1706
-- Name: retiredreasontypes_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY retiredreasontypes
    ADD CONSTRAINT retiredreasontypes_pkey PRIMARY KEY (retiredreasontypeid);


--
-- TOC entry 2112 (class 2606 OID 116218)
-- Dependencies: 1682 1682
-- Name: ribbon_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY ribbon
    ADD CONSTRAINT ribbon_pkey PRIMARY KEY (id);


--
-- TOC entry 2145 (class 2606 OID 116610)
-- Dependencies: 1699 1699
-- Name: roletypes_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY roletypes
    ADD CONSTRAINT roletypes_pkey PRIMARY KEY (roletypeid);


--
-- TOC entry 2171 (class 2606 OID 116772)
-- Dependencies: 1710 1710
-- Name: screenshots_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY screenshots
    ADD CONSTRAINT screenshots_pkey PRIMARY KEY (screenshotid);


--
-- TOC entry 2143 (class 2606 OID 116574)
-- Dependencies: 1698 1698
-- Name: telephones_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY telephones
    ADD CONSTRAINT telephones_pkey PRIMARY KEY (telephoneid);


--
-- TOC entry 2114 (class 2606 OID 116220)
-- Dependencies: 1683 1683
-- Name: topic_name_key; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY topic
    ADD CONSTRAINT topic_name_key UNIQUE (name);


--
-- TOC entry 2116 (class 2606 OID 116222)
-- Dependencies: 1683 1683
-- Name: topic_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY topic
    ADD CONSTRAINT topic_pkey PRIMARY KEY (id);


--
-- TOC entry 2118 (class 2606 OID 116224)
-- Dependencies: 1684 1684
-- Name: user_gadget_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY user_gadget_permission
    ADD CONSTRAINT user_gadget_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 2120 (class 2606 OID 116226)
-- Dependencies: 1685 1685
-- Name: wire_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal-dev; Tablespace: 
--

ALTER TABLE ONLY wire
    ADD CONSTRAINT wire_pkey PRIMARY KEY (id);


--
-- TOC entry 2146 (class 1259 OID 116638)
-- Dependencies: 1700
-- Name: ix_accountmemberroles_accountid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accountmemberroles_accountid ON accountmemberroles USING btree (accountid);


--
-- TOC entry 2147 (class 1259 OID 116639)
-- Dependencies: 1700
-- Name: ix_accountmemberroles_memberid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accountmemberroles_memberid ON accountmemberroles USING btree (memberid);


--
-- TOC entry 2148 (class 1259 OID 116640)
-- Dependencies: 1700
-- Name: ix_accountmemberroles_typeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accountmemberroles_typeid ON accountmemberroles USING btree (typeid);


--
-- TOC entry 2210 (class 1259 OID 117081)
-- Dependencies: 1731
-- Name: ix_accountprojectnumbers_accountid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accountprojectnumbers_accountid ON accountprojectnumbers USING btree (accountid);


--
-- TOC entry 2211 (class 1259 OID 117084)
-- Dependencies: 1731
-- Name: ix_accountprojectnumbers_invoicelineitemid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accountprojectnumbers_invoicelineitemid ON accountprojectnumbers USING btree (invoicelineitemid);


--
-- TOC entry 2212 (class 1259 OID 117083)
-- Dependencies: 1731
-- Name: ix_accountprojectnumbers_projectid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accountprojectnumbers_projectid ON accountprojectnumbers USING btree (projectid);


--
-- TOC entry 2213 (class 1259 OID 117082)
-- Dependencies: 1731
-- Name: ix_accountprojectnumbers_projectnumber; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accountprojectnumbers_projectnumber ON accountprojectnumbers USING btree (projectnumber);


--
-- TOC entry 2214 (class 1259 OID 117085)
-- Dependencies: 1731
-- Name: ix_accountprojectnumbers_retiredbyid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accountprojectnumbers_retiredbyid ON accountprojectnumbers USING btree (retiredbyid);


--
-- TOC entry 2215 (class 1259 OID 117086)
-- Dependencies: 1731
-- Name: ix_accountprojectnumbers_retiredreasonid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accountprojectnumbers_retiredreasonid ON accountprojectnumbers USING btree (retiredreasonid);


--
-- TOC entry 2131 (class 1259 OID 116513)
-- Dependencies: 1693
-- Name: ix_accounts_companyid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accounts_companyid ON accounts USING btree (companyid);


--
-- TOC entry 2132 (class 1259 OID 116514)
-- Dependencies: 1693
-- Name: ix_accounts_typeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_accounts_typeid ON accounts USING btree (typeid);


--
-- TOC entry 2137 (class 1259 OID 116562)
-- Dependencies: 1696
-- Name: ix_addresses_accountid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_addresses_accountid ON addresses USING btree (accountid);


--
-- TOC entry 2138 (class 1259 OID 116561)
-- Dependencies: 1696
-- Name: ix_addresses_typeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_addresses_typeid ON addresses USING btree (typeid);


--
-- TOC entry 2164 (class 1259 OID 116759)
-- Dependencies: 1708
-- Name: ix_categories_categorylevel; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_categories_categorylevel ON categories USING btree (categorylevel);


--
-- TOC entry 2165 (class 1259 OID 116758)
-- Dependencies: 1708
-- Name: ix_categories_categoryorder; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_categories_categoryorder ON categories USING btree (categoryorder);


--
-- TOC entry 2166 (class 1259 OID 116756)
-- Dependencies: 1708
-- Name: ix_categories_parentcategoryid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_categories_parentcategoryid ON categories USING btree (parentcategoryid);


--
-- TOC entry 2167 (class 1259 OID 116757)
-- Dependencies: 1708
-- Name: ix_categories_typeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_categories_typeid ON categories USING btree (typeid);


--
-- TOC entry 2187 (class 1259 OID 116901)
-- Dependencies: 1717
-- Name: ix_creditcards_accountid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_creditcards_accountid ON creditcards USING btree (accountid);


--
-- TOC entry 2209 (class 1259 OID 117048)
-- Dependencies: 1730
-- Name: ix_depositlineitems_depositid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_depositlineitems_depositid ON depositlineitems USING btree (depositid);


--
-- TOC entry 2205 (class 1259 OID 117030)
-- Dependencies: 1728
-- Name: ix_deposits_accountid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_deposits_accountid ON deposits USING btree (accountid);


--
-- TOC entry 2206 (class 1259 OID 117031)
-- Dependencies: 1728
-- Name: ix_deposits_currencytypeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_deposits_currencytypeid ON deposits USING btree (currencytypeid);


--
-- TOC entry 2154 (class 1259 OID 116697)
-- Dependencies: 1703
-- Name: ix_feedback_editedbyid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_feedback_editedbyid ON feedback USING btree (editedbyid);


--
-- TOC entry 2155 (class 1259 OID 116695)
-- Dependencies: 1703
-- Name: ix_feedback_reaccountid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_feedback_reaccountid ON feedback USING btree (reaccountid);


--
-- TOC entry 2156 (class 1259 OID 116694)
-- Dependencies: 1703
-- Name: ix_feedback_rememberid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_feedback_rememberid ON feedback USING btree (rememberid);


--
-- TOC entry 2157 (class 1259 OID 116696)
-- Dependencies: 1703
-- Name: ix_feedback_reprojectid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_feedback_reprojectid ON feedback USING btree (reprojectid);


--
-- TOC entry 2192 (class 1259 OID 116949)
-- Dependencies: 1721
-- Name: ix_invoices_accountid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_invoices_accountid ON invoices USING btree (accountid);


--
-- TOC entry 2193 (class 1259 OID 116950)
-- Dependencies: 1721
-- Name: ix_invoices_currencytypeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_invoices_currencytypeid ON invoices USING btree (currencytypeid);


--
-- TOC entry 2198 (class 1259 OID 117009)
-- Dependencies: 1726
-- Name: ix_payments_currencytypeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_payments_currencytypeid ON payments USING btree (currencytypeid);


--
-- TOC entry 2199 (class 1259 OID 117008)
-- Dependencies: 1726
-- Name: ix_payments_invoiceid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_payments_invoiceid ON payments USING btree (invoiceid);


--
-- TOC entry 2200 (class 1259 OID 117010)
-- Dependencies: 1726
-- Name: ix_payments_paymenttypeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_payments_paymenttypeid ON payments USING btree (paymenttypeid);


--
-- TOC entry 2149 (class 1259 OID 116660)
-- Dependencies: 1701
-- Name: ix_projectmemberroles_memberid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_projectmemberroles_memberid ON projectmemberroles USING btree (memberid);


--
-- TOC entry 2150 (class 1259 OID 116659)
-- Dependencies: 1701
-- Name: ix_projectmemberroles_projectid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_projectmemberroles_projectid ON projectmemberroles USING btree (projectid);


--
-- TOC entry 2151 (class 1259 OID 116661)
-- Dependencies: 1701
-- Name: ix_projectmemberroles_typeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_projectmemberroles_typeid ON projectmemberroles USING btree (typeid);


--
-- TOC entry 2178 (class 1259 OID 116872)
-- Dependencies: 1715
-- Name: ix_publish_currencytypeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_publish_currencytypeid ON publish USING btree (currencytypeid);


--
-- TOC entry 2179 (class 1259 OID 116870)
-- Dependencies: 1715
-- Name: ix_publish_editedbyid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_publish_editedbyid ON publish USING btree (editedbyid);


--
-- TOC entry 2180 (class 1259 OID 116871)
-- Dependencies: 1715
-- Name: ix_publish_licensetypeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_publish_licensetypeid ON publish USING btree (licensetypeid);


--
-- TOC entry 2181 (class 1259 OID 116869)
-- Dependencies: 1715
-- Name: ix_publish_publishtypeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_publish_publishtypeid ON publish USING btree (publishtypeid);


--
-- TOC entry 2182 (class 1259 OID 116868)
-- Dependencies: 1715
-- Name: ix_publish_statusid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_publish_statusid ON publish USING btree (statusid);


--
-- TOC entry 2168 (class 1259 OID 116794)
-- Dependencies: 1710
-- Name: ix_screenshots_assetid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_screenshots_assetid ON screenshots USING btree (assetid);


--
-- TOC entry 2169 (class 1259 OID 116793)
-- Dependencies: 1710
-- Name: ix_screenshots_projectid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_screenshots_projectid ON screenshots USING btree (projectid);


--
-- TOC entry 2139 (class 1259 OID 116601)
-- Dependencies: 1698
-- Name: ix_telephones_accountid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_telephones_accountid ON telephones USING btree (accountid);


--
-- TOC entry 2140 (class 1259 OID 116602)
-- Dependencies: 1698
-- Name: ix_telephones_memberid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_telephones_memberid ON telephones USING btree (memberid);


--
-- TOC entry 2141 (class 1259 OID 116600)
-- Dependencies: 1698
-- Name: ix_telephones_typeid; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE INDEX ix_telephones_typeid ON telephones USING btree (typeid);


--
-- TOC entry 2104 (class 1259 OID 116227)
-- Dependencies: 1679
-- Name: parent_id_key; Type: INDEX; Schema: public; Owner: boreal-dev; Tablespace: 
--

CREATE UNIQUE INDEX parent_id_key ON parent USING btree (id);


--
-- TOC entry 2224 (class 2606 OID 116228)
-- Dependencies: 1669 1666 2080
-- Name: fk2efde31f49657f; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY demo
    ADD CONSTRAINT fk2efde31f49657f FOREIGN KEY (category_id) REFERENCES category(id);


--
-- TOC entry 2225 (class 2606 OID 116233)
-- Dependencies: 1683 1669 2115
-- Name: fk2efde384b588f5; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY demo
    ADD CONSTRAINT fk2efde384b588f5 FOREIGN KEY (topic_id) REFERENCES topic(id);


--
-- TOC entry 2231 (class 2606 OID 116238)
-- Dependencies: 1674 2105 1679
-- Name: fk342ad2de2bc8c320; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_state
    ADD CONSTRAINT fk342ad2de2bc8c320 FOREIGN KEY (root_gadget_id) REFERENCES parent(id);


--
-- TOC entry 2232 (class 2606 OID 116243)
-- Dependencies: 2068 1662 1674
-- Name: fk342ad2deb4a0c3da; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_state
    ADD CONSTRAINT fk342ad2deb4a0c3da FOREIGN KEY (app_user_id) REFERENCES app_user(id);


--
-- TOC entry 2216 (class 2606 OID 116248)
-- Dependencies: 1662 1661 2068
-- Name: fk3a220d5ab4a0c3da; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY app_role_people
    ADD CONSTRAINT fk3a220d5ab4a0c3da FOREIGN KEY (app_user_id) REFERENCES app_user(id);


--
-- TOC entry 2217 (class 2606 OID 116253)
-- Dependencies: 1660 1661 2064
-- Name: fk3a220d5af75fffa; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY app_role_people
    ADD CONSTRAINT fk3a220d5af75fffa FOREIGN KEY (app_role_id) REFERENCES app_role(id);


--
-- TOC entry 2229 (class 2606 OID 116258)
-- Dependencies: 1679 2105 1673
-- Name: fk4edfe02972616c5f; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_ribbon
    ADD CONSTRAINT fk4edfe02972616c5f FOREIGN KEY (gadget_ribbons_id) REFERENCES parent(id);


--
-- TOC entry 2230 (class 2606 OID 116263)
-- Dependencies: 1673 1682 2111
-- Name: fk4edfe0299d7efc17; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_ribbon
    ADD CONSTRAINT fk4edfe0299d7efc17 FOREIGN KEY (ribbon_id) REFERENCES ribbon(id);


--
-- TOC entry 2244 (class 2606 OID 116268)
-- Dependencies: 1680 1679 2105
-- Name: fk52e8b9476409620d; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY parent_child
    ADD CONSTRAINT fk52e8b9476409620d FOREIGN KEY (parent_children_id) REFERENCES parent(id) ON DELETE SET NULL;


--
-- TOC entry 2245 (class 2606 OID 116273)
-- Dependencies: 1679 2105 1680
-- Name: fk52e8b9477b50127d; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY parent_child
    ADD CONSTRAINT fk52e8b9477b50127d FOREIGN KEY (child_id) REFERENCES parent(id);


--
-- TOC entry 2222 (class 2606 OID 116278)
-- Dependencies: 1662 2068 1664
-- Name: fk58392551ff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY authtoken
    ADD CONSTRAINT fk58392551ff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- TOC entry 2220 (class 2606 OID 116283)
-- Dependencies: 1663 1663 2072
-- Name: fk58ceaf0e4346d3b; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY asset
    ADD CONSTRAINT fk58ceaf0e4346d3b FOREIGN KEY (next_version_id) REFERENCES asset(id);


--
-- TOC entry 2221 (class 2606 OID 116288)
-- Dependencies: 1663 1662 2068
-- Name: fk58ceaf0ff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY asset
    ADD CONSTRAINT fk58ceaf0ff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- TOC entry 2247 (class 2606 OID 116293)
-- Dependencies: 1683 2080 1666
-- Name: fk696cd2f1f49657f; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY topic
    ADD CONSTRAINT fk696cd2f1f49657f FOREIGN KEY (category_id) REFERENCES category(id);


--
-- TOC entry 2233 (class 2606 OID 116298)
-- Dependencies: 1679 2105 1675
-- Name: fk6fafd27b74c7a3d; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_tag
    ADD CONSTRAINT fk6fafd27b74c7a3d FOREIGN KEY (gadget_id) REFERENCES parent(id);


--
-- TOC entry 2234 (class 2606 OID 116303)
-- Dependencies: 1662 2068 1675
-- Name: fk6fafd27ff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_tag
    ADD CONSTRAINT fk6fafd27ff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- TOC entry 2228 (class 2606 OID 116308)
-- Dependencies: 2105 1672 1679
-- Name: fk9ec39fa2b74c7a3d; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_permission
    ADD CONSTRAINT fk9ec39fa2b74c7a3d FOREIGN KEY (gadget_id) REFERENCES parent(id);


--
-- TOC entry 2268 (class 2606 OID 116623)
-- Dependencies: 2129 1693 1700
-- Name: fk_accountmemberroles_accountid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accountmemberroles
    ADD CONSTRAINT fk_accountmemberroles_accountid FOREIGN KEY (accountid) REFERENCES accounts(accountid);


--
-- TOC entry 2270 (class 2606 OID 116633)
-- Dependencies: 1700 2068 1662
-- Name: fk_accountmemberroles_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accountmemberroles
    ADD CONSTRAINT fk_accountmemberroles_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2269 (class 2606 OID 116628)
-- Dependencies: 2144 1699 1700
-- Name: fk_accountmemberroles_typeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accountmemberroles
    ADD CONSTRAINT fk_accountmemberroles_typeid FOREIGN KEY (typeid) REFERENCES roletypes(roletypeid);


--
-- TOC entry 2308 (class 2606 OID 117056)
-- Dependencies: 1731 1693 2129
-- Name: fk_accountprojectnumbers_accountid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accountprojectnumbers
    ADD CONSTRAINT fk_accountprojectnumbers_accountid FOREIGN KEY (accountid) REFERENCES accounts(accountid);


--
-- TOC entry 2310 (class 2606 OID 117066)
-- Dependencies: 1723 1731 2194
-- Name: fk_accountprojectnumbers_invoicelineitemid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accountprojectnumbers
    ADD CONSTRAINT fk_accountprojectnumbers_invoicelineitemid FOREIGN KEY (invoicelineitemid) REFERENCES invoicelineitems(invoicelineitemid);


--
-- TOC entry 2309 (class 2606 OID 117061)
-- Dependencies: 1731 1679 2105
-- Name: fk_accountprojectnumbers_projectid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accountprojectnumbers
    ADD CONSTRAINT fk_accountprojectnumbers_projectid FOREIGN KEY (projectid) REFERENCES parent(id);


--
-- TOC entry 2311 (class 2606 OID 117071)
-- Dependencies: 1731 1662 2068
-- Name: fk_accountprojectnumbers_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accountprojectnumbers
    ADD CONSTRAINT fk_accountprojectnumbers_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2312 (class 2606 OID 117076)
-- Dependencies: 1731 1706 2160
-- Name: fk_accountprojectnumbers_retiredreasonid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accountprojectnumbers
    ADD CONSTRAINT fk_accountprojectnumbers_retiredreasonid FOREIGN KEY (retiredreasonid) REFERENCES retiredreasontypes(retiredreasontypeid);


--
-- TOC entry 2254 (class 2606 OID 116498)
-- Dependencies: 1693 1691 2127
-- Name: fk_accounts_accounttypeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT fk_accounts_accounttypeid FOREIGN KEY (typeid) REFERENCES accounttypes(accounttypeid);


--
-- TOC entry 2253 (class 2606 OID 116493)
-- Dependencies: 1691 1693 2127
-- Name: fk_accounts_companyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT fk_accounts_companyid FOREIGN KEY (companyid) REFERENCES accounttypes(accounttypeid);


--
-- TOC entry 2255 (class 2606 OID 116503)
-- Dependencies: 1693 1662 2068
-- Name: fk_accounts_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT fk_accounts_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2256 (class 2606 OID 116508)
-- Dependencies: 1693 1662 2068
-- Name: fk_accounts_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT fk_accounts_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2252 (class 2606 OID 116474)
-- Dependencies: 1691 1690 2125
-- Name: fk_accounttypes_currencytypeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accounttypes
    ADD CONSTRAINT fk_accounttypes_currencytypeid FOREIGN KEY (currencytypeid) REFERENCES currencytypes(currencytypeid);


--
-- TOC entry 2251 (class 2606 OID 116469)
-- Dependencies: 1662 1691 2068
-- Name: fk_accounttypes_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY accounttypes
    ADD CONSTRAINT fk_accounttypes_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2259 (class 2606 OID 116546)
-- Dependencies: 1696 1693 2129
-- Name: fk_addresses_accountid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT fk_addresses_accountid FOREIGN KEY (accountid) REFERENCES accounts(accountid);


--
-- TOC entry 2260 (class 2606 OID 116551)
-- Dependencies: 1696 1662 2068
-- Name: fk_addresses_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT fk_addresses_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2261 (class 2606 OID 116556)
-- Dependencies: 1696 1662 2068
-- Name: fk_addresses_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT fk_addresses_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2258 (class 2606 OID 116541)
-- Dependencies: 1696 1694 2133
-- Name: fk_addresses_typeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT fk_addresses_typeid FOREIGN KEY (typeid) REFERENCES addresstelephonetypes(addresstelephonetypeid);


--
-- TOC entry 2257 (class 2606 OID 116523)
-- Dependencies: 1694 1662 2068
-- Name: fk_addresstelephonetypes_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY addresstelephonetypes
    ADD CONSTRAINT fk_addresstelephonetypes_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2218 (class 2606 OID 117088)
-- Dependencies: 2068 1662 1662
-- Name: fk_app_user_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY app_user
    ADD CONSTRAINT fk_app_user_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2219 (class 2606 OID 117093)
-- Dependencies: 1662 1662 2068
-- Name: fk_app_user_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY app_user
    ADD CONSTRAINT fk_app_user_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2279 (class 2606 OID 116741)
-- Dependencies: 2068 1708 1662
-- Name: fk_categories_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT fk_categories_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2281 (class 2606 OID 116751)
-- Dependencies: 1708 1708 2162
-- Name: fk_categories_parentcategoryid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT fk_categories_parentcategoryid FOREIGN KEY (parentcategoryid) REFERENCES categories(categoryid);


--
-- TOC entry 2280 (class 2606 OID 116746)
-- Dependencies: 2068 1708 1662
-- Name: fk_categories_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT fk_categories_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2278 (class 2606 OID 116736)
-- Dependencies: 1708 1705 2158
-- Name: fk_categories_typeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT fk_categories_typeid FOREIGN KEY (typeid) REFERENCES projecttypes(projecttypeid);


--
-- TOC entry 2250 (class 2606 OID 116442)
-- Dependencies: 1689 2068 1662
-- Name: fk_companies_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY companies
    ADD CONSTRAINT fk_companies_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2292 (class 2606 OID 116886)
-- Dependencies: 1717 1693 2129
-- Name: fk_creditcards_accountid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY creditcards
    ADD CONSTRAINT fk_creditcards_accountid FOREIGN KEY (accountid) REFERENCES accounts(accountid);


--
-- TOC entry 2293 (class 2606 OID 116891)
-- Dependencies: 1717 1662 2068
-- Name: fk_creditcards_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY creditcards
    ADD CONSTRAINT fk_creditcards_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2294 (class 2606 OID 116896)
-- Dependencies: 1717 1662 2068
-- Name: fk_creditcards_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY creditcards
    ADD CONSTRAINT fk_creditcards_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2307 (class 2606 OID 117043)
-- Dependencies: 1730 1728 2203
-- Name: fk_depositlineitems_depositid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY depositlineitems
    ADD CONSTRAINT fk_depositlineitems_depositid FOREIGN KEY (depositid) REFERENCES deposits(depositid);


--
-- TOC entry 2305 (class 2606 OID 117020)
-- Dependencies: 2129 1728 1693
-- Name: fk_deposits_accountid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY deposits
    ADD CONSTRAINT fk_deposits_accountid FOREIGN KEY (accountid) REFERENCES accounts(accountid);


--
-- TOC entry 2306 (class 2606 OID 117025)
-- Dependencies: 1728 1690 2125
-- Name: fk_deposits_currencytypeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY deposits
    ADD CONSTRAINT fk_deposits_currencytypeid FOREIGN KEY (currencytypeid) REFERENCES currencytypes(currencytypeid);


--
-- TOC entry 2295 (class 2606 OID 116915)
-- Dependencies: 1719 1693 2129
-- Name: fk_echecks_accountid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY echecks
    ADD CONSTRAINT fk_echecks_accountid FOREIGN KEY (accountid) REFERENCES accounts(accountid);


--
-- TOC entry 2296 (class 2606 OID 116920)
-- Dependencies: 1719 1662 2068
-- Name: fk_echecks_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY echecks
    ADD CONSTRAINT fk_echecks_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2297 (class 2606 OID 116925)
-- Dependencies: 1719 1662 2068
-- Name: fk_echecks_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY echecks
    ADD CONSTRAINT fk_echecks_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2276 (class 2606 OID 116689)
-- Dependencies: 1662 1703 2068
-- Name: fk_feedback_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT fk_feedback_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2274 (class 2606 OID 116679)
-- Dependencies: 1693 1703 2129
-- Name: fk_feedback_reaccountid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT fk_feedback_reaccountid FOREIGN KEY (reaccountid) REFERENCES accounts(accountid);


--
-- TOC entry 2273 (class 2606 OID 116674)
-- Dependencies: 1703 2068 1662
-- Name: fk_feedback_rememberid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT fk_feedback_rememberid FOREIGN KEY (rememberid) REFERENCES app_user(id);


--
-- TOC entry 2275 (class 2606 OID 116684)
-- Dependencies: 2105 1679 1703
-- Name: fk_feedback_reprojectid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT fk_feedback_reprojectid FOREIGN KEY (reprojectid) REFERENCES parent(id);


--
-- TOC entry 2300 (class 2606 OID 116962)
-- Dependencies: 1723 1721 2190
-- Name: fk_invoicelineitems_invoiceid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY invoicelineitems
    ADD CONSTRAINT fk_invoicelineitems_invoiceid FOREIGN KEY (invoiceid) REFERENCES invoices(invoiceid);


--
-- TOC entry 2301 (class 2606 OID 116967)
-- Dependencies: 2105 1679 1723
-- Name: fk_invoicelineitems_projectid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY invoicelineitems
    ADD CONSTRAINT fk_invoicelineitems_projectid FOREIGN KEY (projectid) REFERENCES parent(id);


--
-- TOC entry 2298 (class 2606 OID 116939)
-- Dependencies: 1721 1693 2129
-- Name: fk_invoices_accountid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY invoices
    ADD CONSTRAINT fk_invoices_accountid FOREIGN KEY (accountid) REFERENCES accounts(accountid);


--
-- TOC entry 2299 (class 2606 OID 116944)
-- Dependencies: 1721 1690 2125
-- Name: fk_invoices_currencytypeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY invoices
    ADD CONSTRAINT fk_invoices_currencytypeid FOREIGN KEY (currencytypeid) REFERENCES currencytypes(currencytypeid);


--
-- TOC entry 2241 (class 2606 OID 117098)
-- Dependencies: 1679 1708 2162
-- Name: fk_parent_categoryid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY parent
    ADD CONSTRAINT fk_parent_categoryid FOREIGN KEY (categoryid) REFERENCES categories(categoryid);


--
-- TOC entry 2242 (class 2606 OID 118498)
-- Dependencies: 1690 2125 1679
-- Name: fk_parent_currencytypeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY parent
    ADD CONSTRAINT fk_parent_currencytypeid FOREIGN KEY (currencytypeid) REFERENCES currencytypes(currencytypeid);


--
-- TOC entry 2243 (class 2606 OID 118503)
-- Dependencies: 2072 1679 1663
-- Name: fk_parent_thumbnailid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY parent
    ADD CONSTRAINT fk_parent_thumbnailid FOREIGN KEY (thumbnailid) REFERENCES asset(id);


--
-- TOC entry 2303 (class 2606 OID 116998)
-- Dependencies: 1690 1726 2125
-- Name: fk_payments_currencytypeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT fk_payments_currencytypeid FOREIGN KEY (currencytypeid) REFERENCES currencytypes(currencytypeid);


--
-- TOC entry 2302 (class 2606 OID 116993)
-- Dependencies: 1726 1721 2190
-- Name: fk_payments_invoiceid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT fk_payments_invoiceid FOREIGN KEY (invoiceid) REFERENCES invoices(invoiceid);


--
-- TOC entry 2304 (class 2606 OID 117003)
-- Dependencies: 1726 1724 2196
-- Name: fk_payments_paymenttypeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT fk_payments_paymenttypeid FOREIGN KEY (paymenttypeid) REFERENCES paymenttypes(paymenttypeid);


--
-- TOC entry 2272 (class 2606 OID 116654)
-- Dependencies: 1701 1662 2068
-- Name: fk_projectmemberroles_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY projectmemberroles
    ADD CONSTRAINT fk_projectmemberroles_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2271 (class 2606 OID 116649)
-- Dependencies: 2144 1701 1699
-- Name: fk_projectmemberroles_typeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY projectmemberroles
    ADD CONSTRAINT fk_projectmemberroles_typeid FOREIGN KEY (typeid) REFERENCES roletypes(roletypeid);


--
-- TOC entry 2290 (class 2606 OID 116858)
-- Dependencies: 1715 2125 1690
-- Name: fk_publish_currencytypeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY publish
    ADD CONSTRAINT fk_publish_currencytypeid FOREIGN KEY (currencytypeid) REFERENCES currencytypes(currencytypeid);


--
-- TOC entry 2289 (class 2606 OID 116853)
-- Dependencies: 2068 1662 1715
-- Name: fk_publish_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY publish
    ADD CONSTRAINT fk_publish_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2288 (class 2606 OID 116848)
-- Dependencies: 2176 1715 1713
-- Name: fk_publish_licensetypeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY publish
    ADD CONSTRAINT fk_publish_licensetypeid FOREIGN KEY (licensetypeid) REFERENCES licensetypes(licensetypeid);


--
-- TOC entry 2287 (class 2606 OID 116843)
-- Dependencies: 1712 2174 1715
-- Name: fk_publish_publishtypeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY publish
    ADD CONSTRAINT fk_publish_publishtypeid FOREIGN KEY (publishtypeid) REFERENCES publishtypes(publishtypeid);


--
-- TOC entry 2291 (class 2606 OID 116863)
-- Dependencies: 1662 2068 1715
-- Name: fk_publish_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY publish
    ADD CONSTRAINT fk_publish_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2286 (class 2606 OID 116838)
-- Dependencies: 2172 1715 1711
-- Name: fk_publish_statusid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY publish
    ADD CONSTRAINT fk_publish_statusid FOREIGN KEY (statusid) REFERENCES publishstatuses(publishstatusid);


--
-- TOC entry 2267 (class 2606 OID 116611)
-- Dependencies: 2068 1662 1699
-- Name: fk_roletypes_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY roletypes
    ADD CONSTRAINT fk_roletypes_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2283 (class 2606 OID 116778)
-- Dependencies: 1710 2072 1663
-- Name: fk_screenshots_assetid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY screenshots
    ADD CONSTRAINT fk_screenshots_assetid FOREIGN KEY (assetid) REFERENCES asset(id);


--
-- TOC entry 2284 (class 2606 OID 116783)
-- Dependencies: 2068 1662 1710
-- Name: fk_screenshots_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY screenshots
    ADD CONSTRAINT fk_screenshots_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2282 (class 2606 OID 116773)
-- Dependencies: 1679 1710 2105
-- Name: fk_screenshots_projectid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY screenshots
    ADD CONSTRAINT fk_screenshots_projectid FOREIGN KEY (projectid) REFERENCES parent(id);


--
-- TOC entry 2285 (class 2606 OID 116788)
-- Dependencies: 2068 1662 1710
-- Name: fk_screenshots_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY screenshots
    ADD CONSTRAINT fk_screenshots_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2277 (class 2606 OID 116704)
-- Dependencies: 1704 2068 1662
-- Name: fk_systemadministrators_memberid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY systemadministrators
    ADD CONSTRAINT fk_systemadministrators_memberid FOREIGN KEY (memberid) REFERENCES app_user(id);


--
-- TOC entry 2263 (class 2606 OID 116580)
-- Dependencies: 1698 2129 1693
-- Name: fk_telephones_accountid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY telephones
    ADD CONSTRAINT fk_telephones_accountid FOREIGN KEY (accountid) REFERENCES accounts(accountid);


--
-- TOC entry 2265 (class 2606 OID 116590)
-- Dependencies: 1698 2068 1662
-- Name: fk_telephones_editedbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY telephones
    ADD CONSTRAINT fk_telephones_editedbyid FOREIGN KEY (editedbyid) REFERENCES app_user(id);


--
-- TOC entry 2264 (class 2606 OID 116585)
-- Dependencies: 2068 1662 1698
-- Name: fk_telephones_memberid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY telephones
    ADD CONSTRAINT fk_telephones_memberid FOREIGN KEY (memberid) REFERENCES app_user(id);


--
-- TOC entry 2266 (class 2606 OID 116595)
-- Dependencies: 2068 1662 1698
-- Name: fk_telephones_retiredbyid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY telephones
    ADD CONSTRAINT fk_telephones_retiredbyid FOREIGN KEY (retiredbyid) REFERENCES app_user(id);


--
-- TOC entry 2262 (class 2606 OID 116575)
-- Dependencies: 1698 1694 2133
-- Name: fk_telephones_typeid; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY telephones
    ADD CONSTRAINT fk_telephones_typeid FOREIGN KEY (typeid) REFERENCES addresstelephonetypes(addresstelephonetypeid);


--
-- TOC entry 2248 (class 2606 OID 116313)
-- Dependencies: 2105 1684 1679
-- Name: fkad74382eb74c7a3d; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY user_gadget_permission
    ADD CONSTRAINT fkad74382eb74c7a3d FOREIGN KEY (gadget_id) REFERENCES parent(id);


--
-- TOC entry 2249 (class 2606 OID 116318)
-- Dependencies: 2068 1662 1684
-- Name: fkad74382eff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY user_gadget_permission
    ADD CONSTRAINT fkad74382eff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- TOC entry 2237 (class 2606 OID 116323)
-- Dependencies: 1678 2080 1666
-- Name: fkbf8ae7c61f49657f; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY manual
    ADD CONSTRAINT fkbf8ae7c61f49657f FOREIGN KEY (category_id) REFERENCES category(id);


--
-- TOC entry 2223 (class 2606 OID 116328)
-- Dependencies: 2068 1662 1665
-- Name: fkc1fd995ff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY blog_entry
    ADD CONSTRAINT fkc1fd995ff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- TOC entry 2238 (class 2606 OID 116333)
-- Dependencies: 1679 2105 1679
-- Name: fkc4ab08aa6f939afd; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY parent
    ADD CONSTRAINT fkc4ab08aa6f939afd FOREIGN KEY (nextversionid) REFERENCES parent(id);


--
-- TOC entry 2239 (class 2606 OID 116338)
-- Dependencies: 1679 2105 1679
-- Name: fkc4ab08aaa2a357e9; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY parent
    ADD CONSTRAINT fkc4ab08aaa2a357e9 FOREIGN KEY (gadgetrefid) REFERENCES parent(id);


--
-- TOC entry 2240 (class 2606 OID 116343)
-- Dependencies: 2068 1679 1662
-- Name: fkc4ab08aaff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY parent
    ADD CONSTRAINT fkc4ab08aaff5753f8 FOREIGN KEY (ownedbyid) REFERENCES app_user(id);


--
-- TOC entry 2226 (class 2606 OID 116348)
-- Dependencies: 1671 2105 1679
-- Name: fkd3b53cca9e059080; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_component
    ADD CONSTRAINT fkd3b53cca9e059080 FOREIGN KEY (gadget_components_id) REFERENCES parent(id);


--
-- TOC entry 2227 (class 2606 OID 116353)
-- Dependencies: 1671 1667 2082
-- Name: fkd3b53ccac8ce215d; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_component
    ADD CONSTRAINT fkd3b53ccac8ce215d FOREIGN KEY (component_id) REFERENCES component(id);


--
-- TOC entry 2235 (class 2606 OID 116358)
-- Dependencies: 2119 1685 1676
-- Name: fkd866249830728cf7; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_wire
    ADD CONSTRAINT fkd866249830728cf7 FOREIGN KEY (wire_id) REFERENCES wire(id);


--
-- TOC entry 2236 (class 2606 OID 116363)
-- Dependencies: 1679 1676 2105
-- Name: fkd86624988d41e14e; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY gadget_wire
    ADD CONSTRAINT fkd86624988d41e14e FOREIGN KEY (gadget_wires_id) REFERENCES parent(id);


--
-- TOC entry 2246 (class 2606 OID 116368)
-- Dependencies: 1681 2068 1662
-- Name: fked8e89a9ff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal-dev
--

ALTER TABLE ONLY profile
    ADD CONSTRAINT fked8e89a9ff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- TOC entry 2316 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2011-04-08 14:35:28 CDT

--
-- PostgreSQL database dump complete
--

