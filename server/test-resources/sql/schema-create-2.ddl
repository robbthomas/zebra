
--
-- Name: app_role_authority_key; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  app_role
    ADD CONSTRAINT app_role_authority_key UNIQUE (authority);


--
-- Name: app_role_people_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  app_role_people
    ADD CONSTRAINT app_role_people_pkey PRIMARY KEY (app_role_id, app_user_id);


--
-- Name: app_role_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  app_role
    ADD CONSTRAINT app_role_pkey PRIMARY KEY (id);


--
-- Name: app_user_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  app_user
    ADD CONSTRAINT app_user_pkey PRIMARY KEY (id);


--
-- Name: app_user_username_key; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  app_user
    ADD CONSTRAINT app_user_username_key UNIQUE (username);


--
-- Name: asset_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  asset
    ADD CONSTRAINT asset_pkey PRIMARY KEY (id);


--
-- Name: authtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  authtoken
    ADD CONSTRAINT authtoken_pkey PRIMARY KEY (id);


--
-- Name: blog_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  blog_entry
    ADD CONSTRAINT blog_entry_pkey PRIMARY KEY (id);


--
-- Name: category_name_key; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  category
    ADD CONSTRAINT category_name_key UNIQUE (name);


--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: component_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  component
    ADD CONSTRAINT component_pkey PRIMARY KEY (id);


--
-- Name: contact_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);


--
-- Name: demo_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  demo
    ADD CONSTRAINT demo_pkey PRIMARY KEY (id);


--
-- Name: demo_title_key; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  demo
    ADD CONSTRAINT demo_title_key UNIQUE (title);


--
-- Name: encrypted_data_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  encrypted_data
    ADD CONSTRAINT encrypted_data_pkey PRIMARY KEY (id);


--
-- Name: gadget_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  gadget_permission
    ADD CONSTRAINT gadget_permission_pkey PRIMARY KEY (id);


--
-- Name: gadget_state_app_user_id_key; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  gadget_state
    ADD CONSTRAINT gadget_state_app_user_id_key UNIQUE (app_user_id, root_gadget_id);


--
-- Name: gadget_state_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  gadget_state
    ADD CONSTRAINT gadget_state_pkey PRIMARY KEY (id);


--
-- Name: gadget_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  gadget_tag
    ADD CONSTRAINT gadget_tag_pkey PRIMARY KEY (id);


--
-- Name: manual_name_key; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  manual
    ADD CONSTRAINT manual_name_key UNIQUE (name);


--
-- Name: manual_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  manual
    ADD CONSTRAINT manual_pkey PRIMARY KEY (id);


--
-- Name: parent_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  parent
    ADD CONSTRAINT parent_pkey PRIMARY KEY (id);


--
-- Name: profile_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (id);


--
-- Name: profile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  profile
    ADD CONSTRAINT profile_user_id_key UNIQUE (user_id);


--
-- Name: ribbon_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  ribbon
    ADD CONSTRAINT ribbon_pkey PRIMARY KEY (id);


--
-- Name: topic_name_key; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  topic
    ADD CONSTRAINT topic_name_key UNIQUE (name);


--
-- Name: topic_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  topic
    ADD CONSTRAINT topic_pkey PRIMARY KEY (id);


--
-- Name: user_gadget_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  user_gadget_permission
    ADD CONSTRAINT user_gadget_permission_pkey PRIMARY KEY (id);


--
-- Name: wire_pkey; Type: CONSTRAINT; Schema: public; Owner: boreal; Tablespace: 
--

ALTER TABLE  wire
    ADD CONSTRAINT wire_pkey PRIMARY KEY (id);


--
-- Name: fk2efde31f49657f; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  demo
    ADD CONSTRAINT fk2efde31f49657f FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: fk2efde384b588f5; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  demo
    ADD CONSTRAINT fk2efde384b588f5 FOREIGN KEY (topic_id) REFERENCES topic(id);


--
-- Name: fk342ad2de2bc8c320; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_state
    ADD CONSTRAINT fk342ad2de2bc8c320 FOREIGN KEY (root_gadget_id) REFERENCES parent(id);


--
-- Name: fk342ad2deb4a0c3da; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_state
    ADD CONSTRAINT fk342ad2deb4a0c3da FOREIGN KEY (app_user_id) REFERENCES app_user(id);


--
-- Name: fk3a220d5ab4a0c3da; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  app_role_people
    ADD CONSTRAINT fk3a220d5ab4a0c3da FOREIGN KEY (app_user_id) REFERENCES app_user(id);


--
-- Name: fk3a220d5af75fffa; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  app_role_people
    ADD CONSTRAINT fk3a220d5af75fffa FOREIGN KEY (app_role_id) REFERENCES app_role(id);


--
-- Name: fk4edfe02972616c5f; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_ribbon
    ADD CONSTRAINT fk4edfe02972616c5f FOREIGN KEY (gadget_ribbons_id) REFERENCES parent(id);


--
-- Name: fk4edfe0299d7efc17; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_ribbon
    ADD CONSTRAINT fk4edfe0299d7efc17 FOREIGN KEY (ribbon_id) REFERENCES ribbon(id);


--
-- Name: fk52e8b9476409620d; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  parent_child
    ADD CONSTRAINT fk52e8b9476409620d FOREIGN KEY (parent_children_id) REFERENCES parent(id);


--
-- Name: fk52e8b9477b50127d; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  parent_child
    ADD CONSTRAINT fk52e8b9477b50127d FOREIGN KEY (child_id) REFERENCES parent(id);


--
-- Name: fk58392551ff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  authtoken
    ADD CONSTRAINT fk58392551ff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- Name: fk58ceaf0e4346d3b; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  asset
    ADD CONSTRAINT fk58ceaf0e4346d3b FOREIGN KEY (next_version_id) REFERENCES asset(id);


--
-- Name: fk58ceaf0ff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  asset
    ADD CONSTRAINT fk58ceaf0ff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- Name: fk696cd2f1f49657f; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  topic
    ADD CONSTRAINT fk696cd2f1f49657f FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: fk6fafd27b74c7a3d; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_tag
    ADD CONSTRAINT fk6fafd27b74c7a3d FOREIGN KEY (gadget_id) REFERENCES parent(id);


--
-- Name: fk6fafd27ff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_tag
    ADD CONSTRAINT fk6fafd27ff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- Name: fk9ec39fa2b74c7a3d; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_permission
    ADD CONSTRAINT fk9ec39fa2b74c7a3d FOREIGN KEY (gadget_id) REFERENCES parent(id);


--
-- Name: fkad74382eb74c7a3d; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  user_gadget_permission
    ADD CONSTRAINT fkad74382eb74c7a3d FOREIGN KEY (gadget_id) REFERENCES parent(id);


--
-- Name: fkad74382eff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  user_gadget_permission
    ADD CONSTRAINT fkad74382eff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- Name: fkbf8ae7c61f49657f; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  manual
    ADD CONSTRAINT fkbf8ae7c61f49657f FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: fkc1fd995ff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  blog_entry
    ADD CONSTRAINT fkc1fd995ff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- Name: fkc4ab08aa6f939afd; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  parent
    ADD CONSTRAINT fkc4ab08aa6f939afd FOREIGN KEY (next_version_id) REFERENCES parent(id);


--
-- Name: fkc4ab08aaa2a357e9; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  parent
    ADD CONSTRAINT fkc4ab08aaa2a357e9 FOREIGN KEY (gadget_ref_id) REFERENCES parent(id);


--
-- Name: fkc4ab08aaff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  parent
    ADD CONSTRAINT fkc4ab08aaff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- Name: fkd3b53cca9e059080; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_component
    ADD CONSTRAINT fkd3b53cca9e059080 FOREIGN KEY (gadget_components_id) REFERENCES parent(id);


--
-- Name: fkd3b53ccac8ce215d; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_component
    ADD CONSTRAINT fkd3b53ccac8ce215d FOREIGN KEY (component_id) REFERENCES component(id);


--
-- Name: fkd866249830728cf7; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_wire
    ADD CONSTRAINT fkd866249830728cf7 FOREIGN KEY (wire_id) REFERENCES wire(id);


--
-- Name: fkd86624988d41e14e; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  gadget_wire
    ADD CONSTRAINT fkd86624988d41e14e FOREIGN KEY (gadget_wires_id) REFERENCES parent(id);


--
-- Name: fked8e89a9ff5753f8; Type: FK CONSTRAINT; Schema: public; Owner: boreal
--

ALTER TABLE  profile
    ADD CONSTRAINT fked8e89a9ff5753f8 FOREIGN KEY (user_id) REFERENCES app_user(id);



--
-- PostgreSQL database dump complete
--

COMMIT;
