--
-- Name: app_role; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE app_role;



--
-- Name: app_role_people; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE app_role_people;



--
-- Name: app_user; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE app_user ;


--
-- Name: asset; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE asset ;


--
-- Name: authtoken; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE authtoken;


--
-- Name: blog_entry; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE blog_entry ;



--
-- Name: category; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE category;



--
-- Name: component; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE component ;



--
-- Name: contact; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE contact;


--
-- Name: demo; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE demo ;


--
-- Name: encrypted_data; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE encrypted_data ;


--
-- Name: gadget_component; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE gadget_component ;


--
-- Name: gadget_permission; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE gadget_permission;


--
-- Name: gadget_ribbon; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE gadget_ribbon;

--
-- Name: gadget_state; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE gadget_state ;



--
-- Name: gadget_tag; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE gadget_tag;


--
-- Name: gadget_wire; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE gadget_wire ;


--
-- Name: manual; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE manual ;

--
-- Name: parent; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE parent;


--
-- Name: parent_child; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE parent_child ;


--
-- Name: profile; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE profile;


--
-- Name: ribbon; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE ribbon ;


--
-- Name: topic; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE topic ;


--
-- Name: user_gadget_permission; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE user_gadget_permission ;


--
-- Name: wire; Type: TABLE; Schema: public; Owner: boreal; Tablespace: 
--

DROP TABLE wire ;

