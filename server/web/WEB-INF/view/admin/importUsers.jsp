<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<html>
    <head>
        <title>Bulk Import Users</title>
    </head>
    <body>
        <h3>Use this form to upload a CSV containing users and email addresses, with the following format:</h3>
        <pre>firstname lastname, username, passwdHash, email...
        </pre>
        <form method="post" action="/zephyr/system/userImport/oneName" enctype="multipart/form-data">
            <input type="file" name="file"/>
            <input type="submit"/>
        </form>

        <h3>Use this form to upload a CSV containing users and email addresses, with the following format:</h3>
        <pre>firstname,lastname, username, passwdHash, email...
        </pre>
        <form method="post" action="/zephyr/system/userImport/twoNames" enctype="multipart/form-data">
            <input type="file" name="file"/>
            <input type="submit"/>
        </form>
    </body>
</html>