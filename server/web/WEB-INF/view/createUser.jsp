<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create New User</title>
</head>
<body>
<h3>Add User to System</h3>
<form action="/zephyr/user/create" method="POST" >
<table>
    <tr><td>User Name:</td><td><input type='text' name='userName' value=''></td></tr>
    <tr><td>Password:</td><td><input type='password' name='password'/></td></tr>
    <tr><td>Description:</td><td><input type='text' name='description' value=''></td></tr>
    <tr><td>Email Address:</td><td><input type='text' name='emailAddress'/></td></tr>
    <tr><td>First Name:</td><td><input type='text' name='firstName' value=''></td></tr>
    <tr><td>Last Name:</td><td><input type='text' name='lastName'/></td></tr>
    <tr><td colspan='2'><input name="submit" type="submit"/></td></tr>
    <tr><td colspan='2'><input name="reset" type="reset"/></td></tr>
</table>
</form>
<p />
<a href="/zephyr/">back to menu</a>
<p />
${message }
</body>
</html>