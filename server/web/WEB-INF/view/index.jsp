<%@page import="com.alleni.zebra.security.authorization.Role"%>
<%@page import="com.alleni.zebra.security.authorization.SecurityContextUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	Greetings ${user.firstName } ${user.lastName }! Nice to see you!
	<p />
	Your username and password appear to be correct!
	<p />
	<a href="/site">back to site</a> Note: you may have to log in again on
	that page. Be sure to enter the same login info there as you did here.
    
    <% if (SecurityContextUtils.hasRoleCurrentUser(Role.ROLE_TYPE_SITE_ADMIN)) { %>
    <p />
    <h4>Follow these links to execute cron jobs:</h4>
    <ul>
        <li><a href="/zephyr/cron/1">Cron1</a>:
        <pre>   Generate pending invoices for account subscriptions
    
     Checks for paying accounts which are coming due for their periodic (monthly, quarterly, yearly, etc.)
     account subscription charges.
    
     NOTE that an optional Invoices.holdingDateTime allows the generating of subscripition invoices
     and a notification Email to account holders regarding the upcoming subscription charge ahead of
     time (say, one week before it is to be charged).
    
     Account subscriptions are charged ahead of (at the beginning of) the subscription period.
        </pre></li>
        <li><a href="/zephyr/cron/2">Cron2</a>: 
        <pre> Process pending invoices for each account
    
     Collects all pending invoices for each account in good standing and attempts
     a single transaction with authorize.net to charge the credit card on file
     for the entire amount of all pending invoices for that account.
    
     Get list of accounts with goodStanding=true with pending invoices with
     Invoices.holdingDateTime <= current mm/dd/yyyy and the totals of all
     invoice line items for those invoices.
    
     NOTE that optional holdingDateTime introduced so that subscription invoices
     and corresponding Email notifications can be generated a week ahead of subcription
     renewals.
        
        </pre></li>
        <li><a href="/zephyr/cron/3">Cron3</a>: 
        <pre>Check for "declined" charges and demote those accounts
    
     If transaction for pending charges against an account is "declined" because of problems
     with the customer credit card or bank account profiles, then the account status is demoted
     to make it ineligible to make any additional purchases.
    
     Account is demoted to goodStanding=false and treated as a "Free" account until the account billing
     information is updated successfully -- meaning, the authorize.net customer credit card or bank account
     profiles are successfully updated and verified.
    
     If account subscription period had expired and account standing is restored,
     meaning, the subscription amount was paid as well, should the subscription period
     month/day be updated as well and reset to that of the day good standing was restored?
        
        </pre></li>
        <li><a href="/zephyr/cron/4">Cron4</a>:
        <pre>Generate pending payments for gadgets and apps
    
     Run this cron AFTER the pending invoices have been processed and charged
     and their statuses promoted so that payment entries generated will include
     the latest that were successfully charged.
    
     1 - Get list of invoice line items for gadgets/apps for invoices with status=Charged
     and join with gadget/app productIds and accountIds ordered by gadget/app account
     and then gadget/app productId with aggregate totals (quantity and amount) for each distinct gadget/app.

     2 - Generate Payment and PaymentLineItem entries for each account + gadget/app.
     Each payment entry should have a HOLDING month/day threshold after which it is to be paid.

     3 - Promote corresponding invoice statuses to "Credits Created".
     (Does this need to be tracked on an item-by-item basis in the invoice line items? Seems overkill
      and that a reporting query should be able to join all the data if it is ever needed. Needed for audits possibly?)
    
     NOTE that all the above functionality has been wrapped into a single sql function.
        </pre></li>
        <li><a href="/zephyr/cron/5">Cron5</a>:
        <pre>
Process pending payments at or beyond HOLDING date
    
     Get list of accounts with goodStanding=true with any Payments.status=Pending entries
     with holdingDateTime <= current mm/dd/yyyy and return aggregate totals for each account.
        </pre></li>
        <li><a href="/zephyr/cron/6">Cron6</a>:
        <pre>Check for credit card expiration dates within next week
        </pre></li>
    </ul>
<c:if test="${jobOutput != null}">
    <h5>Cron Output</h5>
    <pre><c:out value="${jobOutput }"></c:out>
    </pre>
</c:if>
        
    <%}// end if admin %>

</body>
</html>