<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Everything I know about you</title>
</head>
<body>
Hello ${user.firstName }!
<p/>Your id is: ${user.id },
<p/>Your password is: ${user.passwd }
<p/>Your email is: ${user.email }
<p/>The record version is: ${user.version }
</body>
</html>