<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	Greetings ${user.firstName } ${user.lastName }!
	<p/>Here Are Your Options:
	
	<ul>
	<li><a href="/zephyr/tempLogin/login" >Launch Editor</a></li>
	<li><a href="/zephyr/system/reports/gadget.csv">Gadget Relationships Report</a>
	<li><a href="/zephyr/user/create">Add Users to System</a>
	</ul>
	
	<p />
	<p/>
	<a href="/zephyr/login">back to login</a>
</body>
</html>