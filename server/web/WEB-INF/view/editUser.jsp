<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create New User</title>
</head>
<body>
<h3>Editing User ${user.username }</h3>
<form action="/zephyr/user/edit/${user.id }" method="POST" >
<table>
    <tr><td>User Name:</td><td><input type='text' name='userName' value='${user.username }'></td></tr>
    <tr><td>Password:</td><td><input type='password' name='password'/></td></tr>
    <tr><td>Description:</td><td><input type='text' name='description' value='${user.description }'></td></tr>
    <tr><td>Email Address:</td><td><input type='text' name='emailAddress' value='${user.email }'/></td></tr>
    <tr><td>First Name:</td><td><input type='text' name='firstName' value='${user.firstName }'></td></tr>
    <tr><td>Last Name:</td><td><input type='text' name='lastName' value='${user.lastName }'/></td></tr>
    <tr><td colspan='2'><input name="submit" type="submit"/></td></tr>
    <tr><td colspan='2'><input name="reset" type="reset"/></td></tr>
</table>
</form>
<p />
<a href="/zephyr/">back to menu</a>
<p />
${message }
</body>
</html>