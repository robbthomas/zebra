#!/bin/sh

#******************************************************************************
#                                                                             *
#  init.sh:                                                                   *
#       Builds a production-ready server from a fresh install of Ubuntu11.    *
#                                                                             *
#       Directions: execute from user 'ubuntu's home directory, as user root  *
#                                                                             *
#******************************************************************************

if [ `id -u` -ne 0 ]; then
        echo "You need root privileges to run this script"
        exit 1
fi

if [ `pwd` != "/home/ubuntu" ]; then
        echo "You must run this script from /home/ubuntu"
        exit 1
fi

#-- old updates scripts must be cleared out of the way
if [ -d /home/ubuntu/updates ]; then
	rm -rf /home/ubuntu/updates
fi

 apt-get -y install ssh curl
 curl -u blackbeard:b00gla! "http://integration.zebrazapps.com/zephyr/system/bootstrap.tgz" -o bootstrap.tgz
 tar xvzf bootstrap.tgz
 chmod +x make_server.sh
 chmod +x parts/*.sh
 chmod +x updates/*.sh
 ./make_server.sh
