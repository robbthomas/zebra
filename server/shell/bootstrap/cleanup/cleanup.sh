#!/bin/sh

###
#  Delete passwords, config files, etc, left over from the installation

# clean up install working directory
rm -rf conf
rm -rf parts
rm -rf src
rm init.sh
rm startup.xml
rm make_server.sh

# remove install-related passwords etc we don't want laying around
rm -rf ~/.subversion
rm -f ~/subversion*

rm bootstrap.tgz
rm -rf cleanup
rm html.tgz
rm -rf var
rm boreal-*.sql  
rm -rf cronjobs
rm jai-1_1_3-lib.zip
#rm typescript
rm -rf zip_contents_for_amazon
rm -rf updates

rm -rf tmp
sudo -u ubuntu mkdir tmp

# we need s3config, pgpass for cronjobs, so leave those in place