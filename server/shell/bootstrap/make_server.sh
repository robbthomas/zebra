#!/bin/bash
#***********************************************************************************************
#                                                                                              *
#  make_server.sh:                                                                             *
#           Builds a production-ready server from a fresh install of Ubuntu11.                 *
#                                                                                              *
#           Directions: execute from user 'ubuntu's home directory, as user root.              *
#                                                                                              *
#           NOTE that this script is intended to be run automatically from "init.sh",          *
#           but it will work just fine if run on its own, provided all its supporting          *
#           files are present.                                                                 *
#                                                                                              *
#***********************************************************************************************

if [ `id -u` -ne 0 ]; then
        echo "You need root privileges to run this script"
        exit 1
fi

if [ `pwd` -ne "/home/ubuntu" ]; then
        echo "You must run this script from /home/ubuntu"
        exit 1
fi

echo "############################"
echo "#  BEGIN Server Bootstrap  #"
echo "############################"

## TODO:  test return code from each script, and abort if !=0

if [ ! -d /home/ubuntu/shell ]; then
        for I in `ls parts/*.sh`
                do :
                        echo "Press enter to execute $I"
                        read FOO
                        ${I}
                        if [ "$?" -ne "0" ]; then
                                echo "ERROR executing $I"
                                exit 1;
                        fi
        done
else
	echo "###################################"
    echo "# Base server already installed.  #"
	echo "###################################"
fi


echo "#######################################"
echo "# NOT Running rX_to_rY.sh scripts     #"
echo "#######################################"

#for I in `ls updates/*.sh`
#        do :
#                if [ ! -f /mnt/${I}_complete ]; then
#                        ${I}
#                        touch /mnt/${I}_complete
#                        echo "${I} complete"
#                else
#                        echo "${I} already run... skipping"
#                fi
#done


if [ "$1" == "cleanup" ]; then
		echo "###################################"
        echo "#     cleaning up                 #"
		echo "###################################"
        chmod +x cleanup/cleanup.sh
        ./cleanup/cleanup.sh
        echo "#### cleanup complete ####"
fi

#-- adjust time if necessary
#   this is also done in 001-install_packages.sh, but that is only run once per server.
sudo ntpdate ntp.ubuntu.com

#sudo ./shell/install_ssl.sh `hostname` no

gcc conf/beepbeep.c -o conf/beepbeep
./conf/beepbeep
echo "Server Complete!"