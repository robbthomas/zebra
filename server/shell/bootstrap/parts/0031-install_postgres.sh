echo "######################################"
echo "#  BEGIN:  003-install_postgres.sh   #"
echo "######################################"

/etc/init.d/postgresql stop

##   set up database  ##
WD=`pwd`

echo ".... installing postgresql-9.2..."

add-apt-repository ppa:pitti/postgresql
apt-get update
apt-get install postgresql-9.2 postgresql-contrib-9.2


## configure s3cmd, and copy down the latest backup
echo "setting up s3cmd..."
cp conf/s3config /home/ubuntu/.s3cfg
cp conf/s3config /root/.s3cfg
chown ubuntu /home/ubuntu/.s3cfg


## edit postgres config files and copy into place
INET_ADDR=`ifconfig|egrep -o 'dr:[^ ]+'|sed 's/dr://g' |line` 

# backup dist'd files
echo "backing up postgres conf files..."
cp /etc/postgresql/9.2/main/pg_hba.conf /etc/postgresql/9.2/main/pg_hba.conf-dist
cp /etc/postgresql/9.2/main/postgresql.conf /etc/postgresql/9.2/main/postgresql.conf-dist

# install new versions
echo "installing new postgres conf files..."
cat conf/pg_hba.conf |sed "s/LOCAL_IP/${INET_ADDR}/g" >/etc/postgresql/9.2/main/pg_hba.conf
cat conf/postgresql.conf |sed "s/LOCAL_IP/${INET_ADDR},localhost/g" >/etc/postgresql/9.2/main/postgresql.conf

# create and initialize a new data directory
		if [ ! -d /mnt/pgsql/data/main ]
			then
				mkdir -p /mnt/pgsql/data/main-9.2
				chown -R postgres /mnt/pgsql
		fi
		
sudo -u postgres /usr/lib/postgresql/8.4/bin/initdb -D /mnt/pgsql/data/main-9.2		
cd $WD

# finally, restart postgres
/etc/init.d/postgresql restart


chown ubuntu /home/ubuntu/.pgpass

echo "####################################"
echo "#  END:  003-install_postgres.sh   #"
echo "###################################"
