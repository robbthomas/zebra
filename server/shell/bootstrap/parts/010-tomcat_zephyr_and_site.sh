#! /bin/bash

echo "############################################"
echo "#  BEGIN:  010-tomcat_zephyr_and_site.sh   #"
echo "############################################"

##
# set up ssh to qa.zz.com
chmod 0600 conf/20110502_zebra_keypair.pem

if [ ! -d ~/.ssh ]; then
    mkdir ~/.ssh
fi

cat conf/known_hosts >> ~/.ssh/known_hosts
chmod 0644 ~/.ssh/known_hosts
chmod 0700 ~/.ssh

##
# create and retrieve a tarball of the current editor resources from www.zebrazapps.com
ssh -i conf/20110502_zebra_keypair.pem ubuntu@zebrazapps.com rm /home/ubuntu/tmp/html.tgz
ssh -i conf/20110502_zebra_keypair.pem ubuntu@zebrazapps.com tar cvzf /home/ubuntu/tmp/html.tgz /var/lib/tomcat6/html
scp -i conf/20110502_zebra_keypair.pem ubuntu@zebrazapps.com:/home/ubuntu/tmp/html.tgz .

##
# if the directory exists (only happens when updating editor manually)
# then delete it
if [ -d /var/lib/tomcat6/html ]; then
    rm -rf /var/lib/tomcat6/html
fi

##
# copy tarball into place
tar xvzf html.tgz 
mv var/lib/tomcat6/html /var/lib/tomcat6/html


# always return to previous working directory when we're done
CURR_DIR=`pwd`

#
# allow current user to connect to subversion
#
if [ -d ~/.subversion ] ; then
       mv ~/.subversion ~/_subversion
fi
cp conf/subversion_subversion.tgz ~
tar xvzf ~/subversion_subversion.tgz -C ~

#######################################
# Copy tomcat config files into place
#
if [ ! -f /var/lib/tomcat6/conf/server.xml-dist ]; then
       cp /var/lib/tomcat6/conf/server.xml /var/lib/tomcat6/conf/server.xml-dist
fi
cp conf/tomcat_server.xml /var/lib/tomcat6/conf/server.xml
cp conf/tomcat_tomcat-users.xml /var/lib/tomcat6/conf/tomcat-users.xml
chgrp tomcat6 /var/lib/tomcat6/conf/*
cp conf/tomcat_default /etc/default/tomcat6



# init.d/tomcat6
cp conf/init_d_tomcat6 /etc/init.d/tomcat6




##
# check out all sources
if [ ! -d src ] ; then mkdir src ; fi

echo "current wd: "
echo `pwd`

cd src

svn export --non-interactive --username builder --password BuUs2010 https://dev.alleni.com/svn/dev/zebra/tags/production_build/
cd production_build
export SRC_DIR=`pwd`

##
# put previous subversion conf back the way we found it
if [ -d ~/_subversion ] ; then
   rm -rf ~/.subversion
   mv ~/_subversion ~/.subversion
fi


##
# ant points to the wrong jdk by default
export JAVA_HOME=/usr/lib/jvm/java-6-sun/jre

##
# build zephyr
cd server
ant -Ddeploy.environment=staging all
cp build/dist/zephyr.war /var/lib/tomcat6/webapps/

##
# Build site and copy into place
cd ${SRC_DIR}/site
ant -Dj2ee.server.home=/usr/share/tomcat6 -Dlibs.CopyLibs.classpath=hudson/org-netbeans-modules-java-j2seproject-copylibstask.jar  -Dtarget.environment=staging -Dbuild.web.dir=web dist
cp dist/site.war /var/lib/tomcat6/webapps/


###
# set up flex/flash
if [ ! -d /home/ubuntu/bin ]; then mkdir /home/ubuntu/bin; fi
mkdir -p /home/ubuntu/bin/flex_sdk/4.5.1
cp /home/ubuntu/conf/flex_sdk_4.5.1.21328A.tgz /home/ubuntu/bin/flex_sdk/4.5.1
cd /home/ubuntu/bin/flex_sdk/4.5.1
tar xvzf flex_sdk_4.5.1.21328A.tgz


##
# create and retrieve a tarball of the current editor resources
ssh -i /home/ubuntu/conf/20110502_zebra_keypair.pem ubuntu@qa.zebrazapps.com rm /home/ubuntu/tmp/html.tgz
ssh -i /home/ubuntu/conf/20110502_zebra_keypair.pem ubuntu@qa.zebrazapps.com tar cvzf /home/ubuntu/tmp/html.tgz /var/lib/tomcat6/html
scp -i /home/ubuntu/conf/20110502_zebra_keypair.pem ubuntu@qa.zebrazapps.com:/home/ubuntu/tmp/html.tgz .

##
# if the directory exists (only happens when updating editor manually)
# then delete it
if [ -d /var/lib/tomcat6/html ]; then
   rm -rf /var/lib/tomcat6/html
fi

##
# copy tarball into place (rm the contents of flash dir)
tar xvzf html.tgz
mv var/lib/tomcat6/html /var/lib/tomcat6/html
rm -rf /var/lib/tomcat6/html/flash/*



##
# Build editor and copy into place
cd ${SRC_DIR}/editor/AuthorDesktop/build
cp /home/ubuntu/src/production_build/editor/AuthorCore/src/assets/appui/toolbox/checkbox.png /home/ubuntu/src/production_build/editor/AuthorCore/src/assets/appui/toolbox/checkBox.png
ant hudson.nightly -DFLEX_HOME=/home/ubuntu/bin/flex_sdk/4.5.1/flex_sdk_4.5.1 -DFLEX_BASE=/home/ubuntu/bin/flex_sdk/4.5.1 -DSDK=/flex_sdk_4.5.1

cp -a target/* /var/lib/tomcat6/html/flash/


##
# finally restart tomcat
/etc/init.d/tomcat6 restart


##
# add user 'ubuntu' to tomcat6 group
usermod -a -G tomcat6 ubuntu


cd ${CURR_DIR}

echo "############################################"
echo "#  END:  010-tomcat_zephyr_and_site.sh     #"
echo "############################################"