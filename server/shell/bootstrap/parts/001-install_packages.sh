# install packages we'll need
echo "####################################"
echo "#  BEGIN:  001-install_packages.sh #"
echo "####################################"

apt-get -y install expect unzip ant openssh-server tomcat6 tomcat6-admin subversion s3cmd curl apache2 libapache2-mod-jk gcc

echo "**** setting timezone to CST ****";
echo "America/Chicago" > /etc/timezone

#-- adjust time if necessary
sudo ntpdate ntp.ubuntu.com


echo "####################################"
echo "#  END:  001-install_packages.sh   #"
echo "####################################"
