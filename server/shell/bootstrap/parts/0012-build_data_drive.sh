echo "#####################################"
echo "#  BEGIN:  0012-build_data_drive.sh #"
echo "#####################################"

mkfs.ext3 /dev/xvdf
echo "/dev/xvdf       /mnt    auto    defaults        0       2" >> /etc/fstab
mount /dev/xvdf

echo "#####################################"
echo "#  BEGIN:  0012-build_data_drive.sh #"
echo "#####################################"
