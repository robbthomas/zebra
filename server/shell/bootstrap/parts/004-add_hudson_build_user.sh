#! /bin/bash

echo "###########################################"
echo "#  BEGIN:  004-add_hudson_build_user.sh   #"
echo "###########################################"

adduser builder --disabled-password --gecos "" 
if [ ! -d /home/builder/.ssh ]; then
	mkdir /home/builder/.ssh
	chown -R builder /home/builder/.ssh
fi

cp conf/ec2-keypair.zebra /home/builder/.ssh
chown builder /home/builder/.ssh/ec2-keypair.zebra
chmod 0600 /home/builder/.ssh/ec2-keypair.zebra


useradd -G tomcat6 builder

echo "#########################################"
echo "#  END:  004-add_hudson_build_user.sh   #"
echo "#########################################"
