#!/bin/sh

echo "##############################"
echo "#  BEGIN:  006-cronjobs.sh   #"
echo "##############################"

mv cronjobs shell
chmod +x shell/*

crontab -u root conf/crontab_root
crontab -u ubuntu conf/crontab_ubuntu

echo "##############################"
echo "#  END:  006-cronjobs.sh     #"
echo "##############################"
