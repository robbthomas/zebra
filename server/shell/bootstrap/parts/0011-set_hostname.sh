echo "####################################"
echo "#  BEGIN:  0011-set_hostname.sh    #"
echo "####################################"

# ask for the host name
echo "What is the host name (environment + domain, e.g. 'www.zebrazapps.com')"
echo "(remember what you answered, because I'll be quizzing you on it again later!)"
read HOSTNAME

hostname $HOSTNAME
cp /etc/hostname /etc/hostname-dist
echo $HOSTNAME > /etc/hostname

echo "####################################"
echo "#  END:   0011-set_hostname.sh     #"
echo "####################################"
