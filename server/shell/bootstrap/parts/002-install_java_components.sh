echo "###########################################"
echo "#  BEGIN:  002-install_java_components.sh #"
echo "###########################################"

#####
# Installs JDK and JAI
#

echo "installing sun JDK"

if [ ! -d tmp ]
        then
                mkdir tmp
fi

##
# install sun-jdk non-interactively via bin file and 'expect'
#
chmod +x conf/jdk-6u32-linux-x64.bin
/usr/bin/expect << EOD
spawn ./conf/jdk-6u32-linux-x64.bin
expect "continue....."
send "\n"
interact
EOD

mv jdk1.6.0_32 /usr/lib/jvm/
ln -s /usr/lib/jvm/jdk1.6.0_32 /usr/lib/jvm/java-6-sun

##
# install JAI, downloading from java.net community
#
echo "installing JAI jars"

wget "http://download.java.net/media/jai/builds/release/1_1_3/jai-1_1_3-lib.zip"
unzip jai-1_1_3-lib.zip
cp jai-1_1_3/lib/*.jar /usr/lib/jvm/java-6-sun/jre/lib/ext/


##
# We still need to update /etc/alternatives to point to the correct place,
# as ant auto-installs openjdk
# move jdk links in /etc/alternatives
for I in `ls  /usr/lib/jvm/java-6-sun/bin`
        do :
                ALTNAME=/etc/alternatives/${I}
                JDKNAME=/usr/lib/jvm/java-6-sun/bin/${I}

                if [ -f ${ALTNAME} ]
                        then
                                echo "rm ${ALTNAME}";
                                rm ${ALTNAME};
                                echo "ln -s ${JDKNAME} ${ALTNAME}"
                                ln -s ${JDKNAME} ${ALTNAME}
                fi;
done

rm /usr/lib/jvm/default-java
ln -s /usr/lib/jvm/java-6-sun /usr/lib/jvm/default-java

echo "###########################################"
echo "#  END:  002-install_java_components.sh   #"
echo "###########################################"
