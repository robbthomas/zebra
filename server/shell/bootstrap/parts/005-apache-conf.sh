echo "#################################"
echo "#  BEGIN:  005-apache-conf.sh   #"
echo "#################################"


########
# Set up apache2
#

## edit apache config files and copy into place
INET_ADDR=`ifconfig|egrep -o 'dr:[^ ]+'|sed 's/dr://g' |line` 

# backup default installed files
echo "backing up apache conf files..."
cp /etc/apache2/sites-available/default /etc/apache2/sites-available/default-dist
cp /etc/apache2/sites-available/default-ssl /etc/apache2/sites-available/default-ssl-dist
cp /etc/libapache2-mod-jk/workers.properties /etc/libapache2-mod-jk-dist/workers.properties
cp /etc/apache2/mods-available/jk.load /etc/apache2/mods-available/jk.load-dist

# install new versions
echo "installing new postgres conf files..."
cat conf/apache_000-default |sed "s/LOCAL_IP/${INET_ADDR}/g" >/etc/apache2/sites-available/default
cp conf/apache_default-ssl /etc/apache2/sites-available/default-ssl
cp conf/workers.properties /etc/libapache2-mod-jk/workers.properties

# no longer needed. Ubuntu fixed the bug.
cp conf/jk.load /etc/apache2/mods-available/jk.load

# 
ln -s /etc/apache2/sites-available/default-ssl /etc/apache2/sites-enabled/default-ssl 

a2enmod rewrite
a2enmod jk

/etc/init.d/apache2 restart

echo "###############################"
echo "#  END:  005-apache-conf.sh   #"
echo "###############################"
