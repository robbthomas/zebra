echo "####################################"
echo "#  BEGIN:  008-mail.sh             #"
echo "####################################"

# install postfix and mailutils
#apt-get -y install postfix
#apt-get -y install mailutils

# set server hostname (may require restart to become effective?)
# dwh:  don't do this, it messes w/ backups scripts, and is not necessary
#echo "zebrazapps.com" > /etc/hostname

HOSTNAME=`hostname`
# modify postfix configuration
#sed -i -e "s/^myhostname.+$/myhostname = ${HOSTNAME}/" /etc/postfix/main.cf
#sed -i -e "s/^mydestination.+$/mydestination = ${HOSTNAME}, localhost/" /etc/postfix/main.cf

echo "####################################"
echo "#  END:  008-mail.sh               #"
echo "####################################"
 