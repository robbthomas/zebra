#!/bin/sh
DATE=`date +"%m-%d-%y_%H-%M"`
LOGS_DIR="/home/ubuntu/logs"
FILE="tomcat-logs_qa_${DATE}.tgz"
BUCKET=s3://com.alleni.zebra.data.backups

cd /home/ubuntu
tar -cvzf ${FILE} ${LOGS_DIR}
s3cmd put $FILE ${BUCKET}/${FILE}

rm $FILE
rm -f /home/ubuntu/logs/*
