#!/bin/bash
BASENAME=full-db-backup
MACHINENAME=`hostname`
DATE=`date +"%Y-%m-%d"`
FILE=${BASENAME}_${MACHINENAME}_${DATE}.txlog.tar.b2z
SEGMENT_PREFIX=${BASENAME}_${MACHINENAME}_${DATE}_part-
BUCKET=s3://com.alleni.zebra.data.backups/nightly/${MACHINENAME}/${DATE}

pg_backup_dir=/mnt/backup/pg
txlogs_dir=${pg_backup_dir}/txlogs

#cd $txlogs_dir
cd $pg_backup_dir
sudo touch ${txlogs_dir}/backupInProgress

sudo -u postgres psql -c "select pg_start_backup('$today', true);"

if [ $? = 0 ]
then
    sudo tar cvj --one-file-system -f ${pg_backup_dir}/${FILE} /mnt/pgsql
    sudo rm ${txlogs_dir}/backupInProgress
    sudo -u postgres psql -c 'select pg_stop_backup();'


    sudo split --verbose -a 4 -d -b 100M ${FILE} ${SEGMENT_PREFIX}

    for I in `ls -F ${SEGMENT_PREFIX}*`
        do :
            echo "uploading segment ${I}"
            s3cmd put $I ${BUCKET}/$I
            sudo rm $I
    done

else
echo "could not backup database ${MACHINENAME} on `date`" | mail -s "${MACHINENAME} backup failure" dhoyt@alleni.com
fi

# finally, remove old backup files
#find /mnt/backup/pg -name "*txlog.tgz" -daystart -mtime +2 -exec sudo rm {} \;
#find /mnt/backup/pg -name "full-db-backup*" -daystart -mtime +2 -exec sudo rm {} \;
