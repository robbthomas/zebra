#!/bin/sh
#Rsync folder /var/lib/tomcat6 to VM939_AI_Zebra_MonitorAndBackups 
#using ssh with keypair 
#create DATE 
DATE=`date +"%m-%d-%y_%H-%M"`

#rsync to vm939 and create log file
sudo rsync -avz -e "ssh -i /home/ubuntu/rsync-key" /var/lib/tomcat6/* qpi@66.40.17.252:/zebra_backups/ami895_zebra_staging/tomcat6 --log-file=/var/lib/tomcat6/rsync-log/rsync_from_zebra_staging$DATE.log

#rsync log file
sudo rsync -avz -e "ssh -i /home/ubuntu/rsync-key" /var/lib/tomcat6/rsync-log/* qpi@66.40.17.252:/zebra_backups/ami895_zebra_staging/tomcat6/rsync-log/
