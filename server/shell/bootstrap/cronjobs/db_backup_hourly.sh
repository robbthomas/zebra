#!/bin/sh
BASENAME=boreal-ami--backup
MACHINENAME=`hostname`
DATE=`date +"%Y-%m-%d_%H-%M"`
FILE=${BASENAME}-${MACHINENAME}_${DATE}.txlog.tgz
BUCKET=s3://com.alleni.zebra.data.backups/txlogs

pg_backup_dir=/mnt/backup/pg
txlogs_dir=${pg_backup_dir}/txlogs

cd $pg_backup_dir
sudo touch ${txlogs_dir}/backupInProgress
sudo tar cvzf ${FILE} $txlogs_dir

sudo rm -f ${txlogs_dir}/*


sudo chown ubuntu ${FILE}
s3cmd put ${FILE} ${BUCKET}/${FILE}

## leave files on server for now until we know it's working
#rm ${FILE}