#!/bin/sh

#******************************************************************************
#                                                                             *
#  install_ssl.sh:                                                                    *
#       Unzips protected cert archive, decrypts keys, enables SSL & rewrite.  *
#                                                                             *
#                                                                                                               
                                  *
#                                                                             *
#******************************************************************************
mkdir -p /etc/apache2/ssl

mv /home/ubuntu/conf/*zebrazapps.com.zip /etc/apache2/ssl/

# ask for the host name
echo "Quiz time!  What is the host name (e.g. 'zebrazapps.com')"
read HOSTNAME

#verify the host name argument is provided
if [ -f /etc/apache2/ssl/${HOSTNAME}.zip ]; then
        
        echo "Enabling SSL Module..."
        sudo a2enmod ssl
        
        echo "Enabling Rewrite..."
        sudo a2enmod rewrite
        
        echo "Updating hostname in default-ssl conf..."
        cp /etc/apache2/sites-available/default-ssl /etc/apache2/sites-available/default-ssl-bak
        cat /etc/apache2/sites-available/default-ssl-bak |sed "s/HOSTNAME/${HOSTNAME}/g" >/etc/apache2/sites-available/default-ssl
        
        echo "Unzipping Certificates..."
        unzip -o -d /etc/apache2/ssl /etc/apache2/ssl/${HOSTNAME}.zip
        
        echo "Decrypting SSL Keys..."
        openssl rsa -in /etc/apache2/ssl/${HOSTNAME}/${HOSTNAME}.key -out /etc/apache2/ssl/${HOSTNAME}/${HOSTNAME}.pem 
        

        echo "Restarting Apache..."
        /etc/init.d/apache2 restart
        
        echo "Changing key to be only readable by root..."
        sudo chown root /etc/apache2/ssl/${HOSTNAME}/${HOSTNAME}.pem 
        sudo chmod go-rwx /etc/apache2/ssl/${HOSTNAME}/${HOSTNAME}.pem
        
else

        echo "Host name '${HOSTNAME}' is not valid"
        exit 1
fi    