#!/bin/sh

#***********************************************************************************************
#                                                                                              *
#  r4_to_r5.sh:                                                                                *
#           + Fix default_transaction_isolation_level on postgres                              *
#           + install db restore script                                                        *
#                                                                                              *
#***********************************************************************************************
rev_dir=updates/r4_to_r5


#-------------------------------------------------+
# install db restore script                       |
#-------------------------------------------------+
cp ${rev_dir}/scripts/restore_db.sh /home/ubuntu/shell/restore_db.sh
cp ${rev_dir}/db_backup_hourly.sh /home/ubuntu/shell/




#-------------------------------------------------------------+
# set default transaction isolation level in postgresql.conf  |
#-------------------------------------------------------------+
cp /etc/postgresql/8.4/main/postgresql.conf /etc/postgresql/8.4/main/postgresql.conf_r5.bak
sed "s/#default_transaction_isolation = 'read committed'/default_transaction_isolation = 'serializable'/" < /etc/postgresql/8.4/main/postgresql.conf_r5.bak > /etc/postgresql/8.4/main/postgresql.conf
/etc/init.d/postgresql restart

# bounce tomcat too, since we killed its database connection
/etc/init.d/tomcat6 restart