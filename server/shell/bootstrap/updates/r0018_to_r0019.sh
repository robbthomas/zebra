#!/bin/sh

#******************************************************************************
#                                                                             *
#  r0015_to_r0016.sh                                                          *
#                                                                             *
#  Reduce frequency of commerce crons.                                        *
#                                                                             *
#                                                                             *
#******************************************************************************

rev_dir=updates/r0018_to_r0019

echo "installing new crontab for root"
sudo crontab -l > /home/ubuntu/crontab_root_r018_to_r0019

sudo crontab ${rev_dir}/crontab

