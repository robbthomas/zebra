#!/bin/sh

#******************************************************************************
#                                                                             *
#  r0015_to_r0016.sh                                                          *
#                                                                             *
#  make ecommerce crontabs use port 8080, bypassing apache.                   *
#                                                                             *
#                                                                             *
#******************************************************************************

rev_dir=updates/r0017_to_r0018

echo "installing new crontab for root"
sudo crontab -l > /home/ubuntu/crontab_root_r017_to_r0018

sudo crontab ${rev_dir}/crontab

