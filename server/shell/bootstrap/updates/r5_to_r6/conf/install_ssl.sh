#!/bin/sh

#******************************************************************************
#                                                                             *
#  install_ssl.sh:                                               		      *
#       Unzips protected cert archive. 										  *
#                                                                             *
#       																	  *
#                                                                             *
#******************************************************************************

echo "Enabling SSL Module..."
sudo a2enmod ssl

echo "Enabling Rewrite..."
sudo a2enmod rewrite

echo "Unzipping Certificates..."
unzip -d /etc/apache2/ssl /etc/apache2/ssl/ssl.zip

echo "Decrypting SSL Keys..."
openssl rsa -in /etc/apache2/ssl/apache.zebrazapps.key -out /etc/apache2/ssl/apache.zebrazapps.pem 

echo "Restarting Apache..."
/etc/init.d/apache2 restart

echo "Changing key to be only readable by root..."
sudo su
chown root /etc/apache2/ssl/apache.zebrazapps.pem 
chmod go-rwx /etc/apache2/ssl/apache.zebrazapps.pem

