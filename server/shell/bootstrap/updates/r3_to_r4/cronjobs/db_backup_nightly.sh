#!/bin/bash
BASENAME=full-db-backup
MACHINENAME=`hostname`
DATE=`date +"%Y-%m-%d_%H-%M"`
FILE=${BASENAME}_${MACHINENAME}_${DATE}.txlog.tgz
BUCKET=s3://com.alleni.zebra.data.backups/txlogs

pg_backup_dir=/mnt/backup/pg
txlogs_dir=${pg_backup_dir}/txlogs

cd $pg_backup_dir
sudo touch backupInProgress
sudo touch ${txlogs_dir}/backupInProgress

sudo -u postgres psql -c "select pg_start_backup('$today', true);"

if [ $? = 0 ]
then
    sudo tar cvj --one-file-system -f ${pg_backup_dir}/${FILE} /var/lib/postgresql
    sudo rm ${txlogs_dir}/backupInProgress
    sudo -u postgres psql -c 'select pg_stop_backup();'
    
 
    sudo chown ubuntu ${FILE}
    s3cmd put ${FILE} ${BUCKET}/${FILE}

else
echo "could not backup database on `date`" | mail -s "backup failure" dhoyt@allenlt.com
fi

