#!/bin/sh

#*********************************************************************************************
#                                                                                            *
#  restore_db.sh:                                                                            *
#                                                                                            *
#                Restore database from the last known good state, using hourly transaction   *
#                logs stored on S3.                                                          *
#                                                                                            *
#                                                                                            *
#*********************************************************************************************
if [ `id -u` -ne 0 ]; then
        echo "You need root privileges to run this script"
        exit 1
fi


#-------------------------------------------------------------------+
#  first:  stop postgres.                                           |
#-------------------------------------------------------------------+
/etc/init.d/postgresql stop
if [ "$?" -ne "0" ]; then
    echo "ERROR:  There are still clients connecting to postgresql server."
    echo "        Please close those, and re-run this script."
    exit 1
fi


tx_restore_dir=/mnt/restore/db/txlogs
base_restore_dir=/mnt/restore/db/base

#-----
# make necessary directories
#
echo "making directories"
if [ -d ${tx_restore_dir} ]; then
    rm -rf ${tx_restore_dir};
fi

if [ -d ${base_restore_dir} ]; then
    rm -rf ${base_restore_dir};
fi
mkdir -p ${base_restore_dir}
mkdir -p ${tx_restore_dir};

echo "changing ownership"
chown -R postgres ${base_restore_dir}

#----------------------------------------------------------------------+
#  next download and unzipping hourly transaction logs                 |
#    note that they'll be in $restore_dir/mnt/backup/pg/txlogs         |
#----------------------------------------------------------------------+

#-----
# fetch files
#
echo "fetching hourly files"
hourly_BASENAME=boreal-ami--backup
hourly_MACHINENAME=staging.zebrazapps.com
hourly_DATE=`date +"%Y-%m-%d_"`
hourly_FILE=${hourly_BASENAME}-${hourly_MACHINENAME}_${hourly_DATE}*.txlog.tgz
hourly_BUCKET=s3://com.alleni.zebra.data.backups/txlogs

cd ${tx_restore_dir}
s3cmd get ${hourly_BUCKET}/${hourly_FILE}

echo "extracting files..."
for F in `ls -F ${hourly_FILE}`
    do :
        # some files may have EOF before expected file.  that's okay.
        tar xvzf $F --ignore-command-error  --ignore-failed-read
done

#-- make sure postgres can read these files on startup.
chown -R postgres ${tx_restore_dir}

#--- move zipped files out of the way in case we need them later
mv *.tgz ..



#--------------------------------------------------------+
# next get the nightly backup                            |
#--------------------------------------------------------+
nightly_BASENAME=full-db-backup
nightly_MACHINENAME=staging.zebrazapps.com
nightly_DATE=`date +"%Y-%m-%d"`
nightly_FILE=${nightly_BASENAME}_${nightly_MACHINENAME}_${nightly_DATE}_00-00.txlog.tar.b2z
nightly_BUCKET=s3://com.alleni.zebra.data.backups/txlogs

echo "fetching nightly backup"
cd ${base_restore_dir}
s3cmd get ${nightly_BUCKET}/${nightly_FILE}

## -- preserve existing postgres directory
echo "moving original postgresql directory out of the way"
mv /var/lib/postgresql/8.4/main /var/lib/postgresql/8.4/main_bak_${nightly_DATE}

## -- extract the full backup
echo "extracting nightly file into place"
cp ${base_restore_dir}/${nightly_FILE} /zebra-db-restore.tar.b2z
chown postgres /zebra-db-restore.tar.b2z
cd /
sudo -u postgres tar xvjf /zebra-db-restore.tar.b2z

#-- tar unzips the archived file structure.  Use that rather than move files around
echo "setting restore command"
sudo -u postgres echo "restore_command = 'cp ${tx_restore_dir}/mnt/backup/pg/txlogs/%f %p'" > /pgsql/data/main/recovery.conf


#--------------------------------------------------------+
# Don't automatically restart postgres:                  |
#      let the user do that.                             |
#--------------------------------------------------------+
echo "***************************************************"
echo "*                                                 *"
echo "*  Server restore is configured.                  *"
echo "*  Please restart postgres to complete.           *"
echo "*                                                 *"
echo "***************************************************"
exit 0
