#!/bin/sh

#******************************************************************************
#                                                                             *
#  r7_to_r8.sh:                                               		          *
#       Replaces cert archive with new ssl and installs 					  *
#		them into the apache2/ssl. Installs new install_ssl.sh				  *		
#                                                                             *
#       																	  *
#                                                                             *
#******************************************************************************
rev_dir=updates/r7_to_r8

echo "Disable SSL Module..."
sudo a2dismod ssl

echo "Disable Rewrite..."
sudo a2dismod rewrite

echo "Removing existing SSL archives..."
rm /etc/apache2/ssl/*

echo "Copying SSL new archives to /etc/apache2/ssl"
cp -f ${rev_dir}/conf/ssl/* 	/etc/apache2/ssl

echo "Copying install_ssl.sh to /home/ubuntu/shell"
cp -f ${rev_dir}/conf/install_ssl.sh 	/home/ubuntu/shell

echo "Setting permissions on /home/ubuntu/shell/install_ssl.sh"
chown root /home/ubuntu/shell/install_ssl.sh
chmod go-rwx /home/ubuntu/shell/install_ssl.sh
chmod u+x /home/ubuntu/shell/install_ssl.sh

cp /etc/apache2/sites-available/default /etc/apache2/sites-available/default-dist
cp /etc/apache2/sites-available/default-ssl /etc/apache2/sites-available/default-ssl-dist

echo "Installing new ssl apache conf..."
cp -f ${rev_dir}/conf/apache_default-ssl /etc/apache2/sites-available/default-ssl

echo "Installing new default apache conf..."
cp -f ${rev_dir}/conf/apache_000-default /etc/apache2/sites-available/default