#!/bin/sh

#***********************************************************************************************
#  r1_to_r2.sh                                                                                 *
#                                                                                              *
#  fixes issues with nightly db backups                                                        *
#                                                                                              *
#***********************************************************************************************

if [ ! -d /mnt/updates ]; then
	mkdir /mnt/updates
fi

##
#  mods to db backup proc

#-- use a tarball, and do it on the ext drive
cp updates/r1_to_r2/cronjobs/backup_db.sh shell/backup_db.sh
chmod +x shell/backup_db.sh

mkdir /mnt/tmp
chown ubuntu /mnt/tmp

#-- use user 'ubuntu' for db backup job
chown ubuntu /home/ubuntu/.pgpass

#-- backup crontabs
crontab -l > crontab_root_r1_to_r2.bak
crontab -l -u ubuntu > crontab_ubuntu_r1_to_r2.bak

crontab updates/r1_to_r2/conf/crontab
crontab -u ubuntu  updates/r1_to_r2/conf/crontab_ubuntu