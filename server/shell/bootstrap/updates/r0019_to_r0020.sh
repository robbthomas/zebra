#!/bin/sh

#******************************************************************************
#                                                                             *
#  r0019_to_r0020.sh                                                          *
#                                                                             *
#  Enable JMX monitoring of Tomcat6.                                          *
#                                                                             *
#                                                                             *
#******************************************************************************

rev_dir=`pwd`/updates/r0019_to_r0020

echo "installing new startup file for tomcat6"
cd /usr/share/tomcat6/bin
sudo cp catalina.sh catalina.sh_r0019_to_r0020
sudo cp $rev_dir/catalina.sh .

echo "Press ENTER to restart Tomcat6 and apply changes"
read A
sudo /etc/init.d/tomcat6 restart

