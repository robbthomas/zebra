#!/bin/sh

#******************************************************************************
#                                                                             *
#  r0015_to_r0016.sh                                               		      *
#                                                                             *
#  created a new version of install_ssl.sh that asks whether you actually     *
#  want to enable ssl                                                         *
#                                                                             *
#                                                                             *
#******************************************************************************

rev_dir=updates/r0015_to_r0016

echo "installing new install_ssl.sh script"
cp ${rev_dir}/install_ssl.sh /home/ubuntu/shell

