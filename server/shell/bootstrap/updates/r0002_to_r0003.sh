#!/bin/sh

#***********************************************************************************************
#  r2_to_r3.sh                                                                                 *
#                                                                                              *
#  + Set up incremental transaction logging for postgres                                       *
#  + Fix permgen issues w/ tomcat6                                                             *
#                                                                                              *
#***********************************************************************************************


REV_DIR=updates/r2_to_r3


########
# copy in new tomcat6 default, restart tomcat
cp /etc/default/tomcat6 /etc/default/tomcat6-r2_to_r3.bak
cp $REVDIR/conf/default_tomcat6 /etc/default/tomcat6
/etc/init.d/tomcat6 restart

########
# Configure transaction logs for postgresql
#
mkdir /mnt/txlog
mkdir /mnt/txlog/wals
chmod -R ugo+rw /mnt/txlog

cp /etc/postgresql/8.4/main/postgresql.conf /etc/postgresql/8.4/main/postgresql.conf-r2_to_r3.bak


INET_ADDR=`ifconfig|egrep -o 'dr:[^ ]+'|sed 's/dr://g' |line` 
cat ${REV_DIR}/conf/postgresql.conf |sed "s/LOCAL_IP/${INET_ADDR},localhost/g" >/etc/postgresql/8.4/main/postgresql.conf



########
#  install new backup scripts
#
cp /home/ubuntu/shell/backup_db.sh /home/ubuntu/shell/backup_db.sh-r2_to_r3.bak

cp ${REV_DIR}/cronjobs/backup_db.sh /home/ubuntu/shell/
cp ${REV_DIR}/cronjobs/backup_txlogs.sh /home/ubuntu/shell/

chmod +x /home/ubuntu/shell/backup_db.sh
chmod +x /home/ubuntu/shell/backup_txlogs.sh

	
	
#########
#  configure hourly backup of transaction logs to s3
#
crontab -l -u ubuntu > crontab_ubuntu_r2_to_r3.bak
crontab -u ubuntu updates/r2_to_r3/conf/crontab_ubuntu

/etc/init.d/postgresql restart



#########
# update nagios install
#
#cp updates/r2_to_r3/conf/nagios_pack_ubuntu11.tar.gz /
#cd /
#tar xvzf /nagios_pack_ubuntu11.tar.gz
#/nagios_pack/nagios_client_install.sh

