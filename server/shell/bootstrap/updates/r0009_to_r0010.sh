#!/bin/sh

#******************************************************************************
#                                                                             *
#  r9_to_r10.sh                                               		          *
#       Adds preview SSL bundle to server									  *
#                                                                             *
#       																	  *
#                                                                             *
#******************************************************************************
rev_dir=updates/r9_to_r10


echo "Copying SSL new archives to /etc/apache2/ssl"
cp -f ${rev_dir}/conf/ssl/* 	/etc/apache2/ssl

echo "Copying install_ssl.sh to /home/ubuntu/shell"
cp -f ${rev_dir}/conf/install_ssl.sh 	/home/ubuntu/shell