#!/bin/sh

#******************************************************************************
#                                                                             *
#  r5_to_r6.sh:                                               		          *
#       Moves cert archive and installs them into the apache2/ssl. *
#                                                                             *
#       																	  *
#                                                                             *
#******************************************************************************
rev_dir=updates/r5_to_r6

echo "Creating SSL Directory..."
mkdir /etc/apache2/ssl

echo "Copying SSL archive to /etc/apache2/ssl"
cp ${rev_dir}/conf/ssl.zip 	/etc/apache2/ssl

echo "Copying install_ssl.sh to /home/ubuntu/shell"
cp ${rev_dir}/conf/install_ssl.sh 	/home/ubuntu/shell

echo "Setting permissions on /home/ubuntu/shell/install_ssl.sh"
chown root /home/ubuntu/shell/install_ssl.sh
chmod go-rwx /home/ubuntu/shell/install_ssl.sh
chmod u+x /home/ubuntu/shell/install_ssl.sh

echo "Installing unzip..."
apt-get install unzip

echo "Installing new ssl apache conf..."
cp ${rev_dir}/conf/apache_default-ssl /etc/apache2/sites-available/default-ssl