#!/bin/sh

#******************************************************************************
#                                                                             *
#  install_ssl.sh:                                               		      *
#       Unzips protected cert archive, decrypts keys, enables SSL & rewrite.  *
#                                                                             *
#       																	  *
#                                                                             *
#******************************************************************************

if [ -n $1 ]; then
   HOSTNAME=$1
else
	# ask for the host name
	echo "What is the host name (i.e. 'zebrazapps.com')"
	read HOSTNAME
fi

if [ -n $2 ]; then
	ENABLE_SSL=$2
else
	echo "Enable SSL on this server? [y/n]"
	read ENABLE_SSL
fi

	if [ "${ENABLE_SSL}" != "y" ]; then
		
		GD_CERT="SSLCertificateFile    /etc/apache2/ssl/localhost/${HOSTNAME}.crt"
		GD_PEM="SSLCertificateKeyFile /etc/apache2/ssl/localhost/${HOSTNAME}.pem"
		
		SNAKE_PEM="#SSLCertificateFile    /etc/ssl/certs/ssl-cert-snakeoil.pem"
		SNAKE_PEM2="SSLCertificateFile    /etc/ssl/certs/ssl-cert-snakeoil.pem"
		
		SNAKE_CERT="SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key"
		SNAKE_CERT2="SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key"
		
		echo "Updating hostname in default-ssl conf..."
		cp /etc/apache2/sites-available/default-ssl /etc/apache2/sites-available/default-ssl-tmp
		
		
		cat /etc/apache2/sites-available/default-ssl-tmp | sed "s/HOSTNAME/${HOSTNAME}/g" | sed "s/SSLEngine on/SSLEngine off/g"	| sed "s/${GD_CERT}/\#${GD_CERT}/g"	| sed "s/${GD_PEM}/\#${GD_PEM}/g" | sed "s/${SNAKE_CERT}/${SNAKE_CERT2}/g" | sed "s/${SNAKE_PEM}/${SNAKE_PEM2}/g" > /etc/apache2/sites-available/default-ssl
			
		rm /etc/apache2/sites-available/default-ssl-tmp


		
		echo ">>> NOT installing SSL on this server. <<<"
		echo "    Script exiting normally"
		exit 0
	fi
	

#verify the host name argument is provided
if [ -f /etc/apache2/ssl/${HOSTNAME}.zip ]; then

	echo "Updating hostname in default-ssl conf..."
	cp /etc/apache2/sites-available/default-ssl /etc/apache2/sites-available/default-ssl-tmp
		cat /etc/apache2/sites-available/default-ssl-tmp |sed "s/HOSTNAME/${HOSTNAME}/g" | sed "s/SSLEngine off/SSLEngine on/g" >/etc/apache2/sites-available/default-ssl
	rm /etc/apache2/sites-available/default-ssl-tmp


	echo "Enabling SSL Module..."
	sudo a2enmod ssl
	
	echo "Enabling Rewrite..."
	sudo a2enmod rewrite
	

	echo "Unzipping Certificates..."
	unzip -o -d /etc/apache2/ssl /etc/apache2/ssl/${HOSTNAME}.zip
	
	echo "Decrypting SSL Keys..."
	openssl rsa -in /etc/apache2/ssl/${HOSTNAME}/${HOSTNAME}.key -out /etc/apache2/ssl/${HOSTNAME}/${HOSTNAME}.pem 
	
	echo "Restarting Apache..."
	/etc/init.d/apache2 restart
	
	echo "Changing key to be only readable by root..."
	sudo su
	chown root /etc/apache2/ssl/${HOSTNAME}/${HOSTNAME}.pem 
	chmod go-rwx /etc/apache2/ssl/${HOSTNAME}/${HOSTNAME}.pem

else

	echo "Host name '${HOSTNAME}' is not valid"
	exit 1
fi

sudo /etc/init.d/apache2 restart	