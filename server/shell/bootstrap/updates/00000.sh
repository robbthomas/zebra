#!/bin/sh

#******************************************************************************
#                                                                             *
#  r00000.sh                                           	            	      *
#       Change the names of update touch-files, to reflect new 000x           *
#       naming scheme.                                                        *
#                                                                             *
#                                                                             *
#******************************************************************************
if [ ! -d /mnt/updates ]; then
	echo "New server:  filename repair not needed."
	exit 0;
fi


echo ">>>>>>>>> fixing update touch-file names before running any update scripts <<<<<<<<<<<"
echo ">>  before: "
ls -F /mnt/updates

#--- do all of this manually, just to be safe
#
cd /mnt/updates
if [ -f /mnt/updates/r1_to_r2.sh_complete ]; then
	mv r1_to_r2.sh_complete r0001_to_r0002.sh_complete
fi

if [ -f /mnt/updates/r2_to_r3.sh_complete ]; then
	mv r2_to_r3.sh_complete r0002_to_r0003.sh_complete
fi

if [ -f /mnt/updates/r3_to_r4.sh_complete ]; then
	mv r3_to_r4.sh_complete r0003_to_r0004.sh_complete
fi

if [ -f /mnt/updates/r4_to_r5.sh_complete ]; then
	mv r4_to_r5.sh_complete r0004_to_r0005.sh_complete
fi

if [ -f /mnt/updates/r5_to_r6.sh_complete ]; then
	mv r5_to_r6.sh_complete r0005_to_r0006.sh_complete
fi

if [ -f /mnt/updates/r6_to_r7.sh_complete ]; then
	mv r6_to_r7.sh_complete r0006_to_r0007.sh_complete
fi

if [ -f /mnt/updates/r7_to_r8.sh_complete ]; then
	mv r7_to_r8.sh_complete r0007_to_r0008.sh_complete
fi

if [ -f /mnt/updates/r8_to_r9.sh_complete ]; then
	mv r8_to_r9.sh_complete r0008_to_r0009.sh_complete
fi

if [ -f /mnt/updates/r9_to_r10.sh_complete ]; then
	mv r9_to_r10.sh_complete r0009_to_r0010.sh_complete
fi

if [ -f /mnt/updates/r10_to_r11.sh_complete ]; then
	mv r10_to_r11.sh_complete r0010_to_r0012.sh_complete
fi

if [ -f /mnt/updates/r11_to_r12.sh_complete ]; then
	mv r11_to_r12.sh_complete r0011_to_r0012.sh_complete
fi

echo ">>>>>>>> filename repair complete <<<<<<<<<<<<<<<<<<<<"
echo " >> after: "
ls -F /mnt/updates
echo ""
echo "       PRESS ENTER TO CONTINUE, OR CTRL+C TO EXIT      "
read foo