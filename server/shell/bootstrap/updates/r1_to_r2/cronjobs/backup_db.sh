#!/bin/sh
BASENAME=boreal-ami--backup
MACHINENAME=`hostname`
DATE=`date +"%Y-%m-%d_%H-%M"`
FILE=${BASENAME}-${MACHINENAME}_${DATE}.sql
BUCKET=s3://com.alleni.zebra.data.backups

cd /mnt/tmp
pg_dump -h localhost -p 5432 -U postgres -W boreal --no-password > $FILE

tar cvzf ${FILE}.tgz $FILE

s3cmd put ${FILE}.tgz ${BUCKET}/${FILE}.tgz

rm ${FILE}.tgz
rm $FILE