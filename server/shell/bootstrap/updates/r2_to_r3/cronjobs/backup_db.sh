#!/bin/sh
BASENAME=boreal-ami--backup
MACHINENAME=`hostname`
DATE=`date +"%Y-%m-%d_%H-%M"`
FILE=${BASENAME}-${MACHINENAME}_${DATE}.sql
BUCKET=s3://com.alleni.zebra.data.backups

##
# stamp logfile
sudo -u postgres psql -c "select pg_start_backup('full nightly backup ${DATE}');"

##
# make backup
cd /mnt/tmp
pg_dump -h localhost -p 5432 -U postgres -W boreal --no-password > $FILE

##
# stamp logfile
sudo -u postgres psql -c "select pg_stop_backup();"

##
# ship off backup
tar cvzf ${FILE}.tgz $FILE
s3cmd put ${FILE}.tgz ${BUCKET}/${FILE}.tgz

##
# clean up
rm ${FILE}.tgz
rm $FILE

##
# nightly, remove the cached tx logs
sudo rm /mnt/txlog/wals