#!/bin/sh
BASENAME=boreal-ami--backup
MACHINENAME=`hostname`
DATE=`date +"%Y-%m-%d_%H-%M"`
FILE=${BASENAME}-${MACHINENAME}_${DATE}.txlog.tgz
BUCKET=s3://com.alleni.zebra.data.backups/txlogs

##
# stamp logfile
sudo -u postgres psql -c "select pg_start_backup('txlog backup ${DATE}');"

cd /mnt/txlog
sudo tar cvzf ${FILE} /mnt/txlog/wals

##
# instead of removing now, do this nightly:  see backup_db.sh
#rm -f /mnt/txlogs/wals/*

##
# stamp logfile
sudo -u postgres psql -c "select pg_stop_backup();"

sudo chown ubuntu ${FILE}
s3cmd put ${FILE} ${BUCKET}/${FILE}

## leave files on server for now until we know it's working
#rm ${FILE}

