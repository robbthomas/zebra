#!/bin/sh

#########################################################
#  r3_to_r4.sh
#
#  + change how incremental db backups work
#########################################################


REV_DIR=updates/r3_to_r4

###
# make new backup dirs
#
mkdir /mnt/backup
mkdir /mnt/backup/pg
mkdir /mnt/backup/pg/pg_backups
mkdir /mnt/backup/pg/txlogs
chown -R postgres /mnt/backup/pg


###
# install new pg conf to dump txlogs in new dir
#
cp /etc/postgresql/8.4/main/postgresql.conf /etc/postgresql/8.4/main/postgresql.conf-r3_to_r4.bak


INET_ADDR=`ifconfig|egrep -o 'dr:[^ ]+'|sed 's/dr://g' |line` 
cat ${REV_DIR}/conf/postgresql.conf |sed "s/LOCAL_IP/${INET_ADDR},localhost/g" >/etc/postgresql/8.4/main/postgresql.conf

###
# restart postgres
/etc/init.d/postgresql-8.4 restart


###
# copy new backup script into place, and install new cronjob
#
cp $REV_DIR/cronjobs/db_backup_hourly.sh /home/ubuntu/shell/db_backup_hourly.sh
cp $REV_DIR/cronjobs/db_backup_nightly.sh /home/ubuntu/shell/db_backup_nightly.sh
chmod +x /home/ubuntu/shell/db_backup*.sh

crontab -u ubuntu -l > crontab_ubuntu_r3_to_r4.bak
crontab -u ubuntu $REV_DIR/conf/crontab_ubuntu

