#!/bin/sh

#******************************************************************************
#                                                                             *
#  r0014_to_r0015.sh                                               		      *
#       Adds qa2 SSL bundle to server									      *
#                                                                             *
#       																	  *
#                                                                             *
#******************************************************************************

rev_dir=updates/r0014_to_r0015


echo "Copying SSL new archives to /etc/apache2/ssl"
cp -f ${rev_dir}/conf/ssl/* 	/etc/apache2/ssl