#!/bin/sh

#***********************************************************************************************
#                                                                                              *
#  r6_to_r7.sh:                                                                                *
#           + move the database directory to /mnt/pgsql, which is another partition.           *
#           + install db restore script                                                        *
#                                                                                              *
#***********************************************************************************************
rev_dir=updates/r6_to_r7

#
#--The production server already has this setup, using '/mnt/pgsql'

if [ ! -d /mnt/pgsql ]
    then

        #-------------------------------------------------+
        # shutdown tomcat and postgres                    |
        #-------------------------------------------------+
        echo "stopping tomcat and postgres"
        /etc/init.d/tomcat6 stop
        /etc/init.d/postgresql stop

        
        #-------------------------------------------------+
        # copy the current data over                      |
        #-------------------------------------------------+
        
        #-- a couple of servers have already had this done,
        #-- so we just need to rename the directory appropriately
        if [ -d /mnt/postgres ]; then
            echo "previous fix detected: renaming directory instead of copying."
            mv /mnt/postgres /mnt/pgsql
            
        else
        #-------------------------------------------------+
        # make new directories                            |
        #-------------------------------------------------+
            echo "creating new data directory"
            sudo mkdir -p /mnt/pgsql/data
            sudo chown postgres /mnt/pgsql
            sudo chown postgres /mnt/pgsql/data
            sudo chmod 0700 /mnt/pgsql/data

            echo "copying data to new directory"
            cp -aP /var/lib/postgresql/8.4/main /mnt/pgsql/data
        fi
        
        #-------------------------------------------------+
        # rm old directory                                |
        #-------------------------------------------------+
        echo "leaving old data in place.  To remove, run the following: "
        echo "     sudo rm -rf /var/lib/postgres/8.4/main"

        #-------------------------------------------------+
        # copy new config into place                      |
        #-------------------------------------------------+
        echo "copying new postgresql config into place"
        cp /etc/postgresql/8.4/main/postgresql.conf /etc/postgresql/8.4/main/postgresql.conf_r6_to_r7.bak
        INET_ADDR=`ifconfig|egrep -o 'dr:[^ ]+'|sed 's/dr://g' |line` 
        cat ${rev_dir}/conf/postgresql.conf |sed "s/LOCAL_IP/${INET_ADDR},localhost/g" >/etc/postgresql/8.4/main/postgresql.conf
        
        #-------------------------------------------------+
        # restarting tomcat and postgres                  |
        #-------------------------------------------------+
        /etc/init.d/postgresql start
        /etc/init.d/tomcat6 start
        
        
else
    echo "current database is already on a separate partition.  Leaving it alone."
fi #-- end if [ ! -d /mnt/pgsql ]

#-------------------------------------------------+
# copy new cronjob into place for db backups      |
#-------------------------------------------------+
echo "copying new db_backup_nightly.sh into place"
cp /home/ubuntu/shell/db_backup_nightly.sh /home/ubuntu/shell/db_backup_nightly.sh_r6_to_r7.bak
cp ${rev_dir}/cronjobs/db_backup_nightly.sh /home/ubuntu/shell
chown ubuntu /home/ubuntu/shell/db_backup_nightly.sh
chmod +x /home/ubuntu/shell/db_backup_nightly.sh     

#-------------------------------------------------+
# copy new db restore script into place           |
#-------------------------------------------------+
echo "copying new db_restore.sh into place"
cp /home/ubuntu/shell/db_restore.sh /home/ubuntu/shell/db_restore.sh_r6_to_r7.bak
cp ${rev_dir}/script/db_restore.sh /home/ubuntu/shell
