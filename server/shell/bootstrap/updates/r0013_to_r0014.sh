#!/bin/sh

#******************************************************************************
#                                                                             *
#  r0013_to_r0014.sh:                                               		  *
#       Replaces apache configs with new ones to support redirects			  *		
#                                                                             *
#       																	  *
#                                                                             *
#******************************************************************************
rev_dir=updates/r0013_to_r0014

echo "Copying install_ssl.sh to /home/ubuntu/shell"
cp -f ${rev_dir}/conf/install_ssl.sh 	/home/ubuntu/shell

cp /etc/apache2/sites-available/default-ssl /etc/apache2/sites-available/default-ssl-dist

echo "Installing new ssl apache conf..."
cp -f ${rev_dir}/conf/apache_default-ssl /etc/apache2/sites-available/default-ssl