#!/bin/sh

#******************************************************************************
#                                                                             *
#  r8_to_r9.sh:                                               		          *
#                                                                             *
#    replace crontab for root with one that uses https for the commerce stuff *
#                                                                             *
#******************************************************************************
rev_dir=updates/r8_to_r9

echo "Installing new crontab for root..."
cp -f ${rev_dir}/shell/install_ssl.sh /home/ubuntu/shell/install_ssl.ssh