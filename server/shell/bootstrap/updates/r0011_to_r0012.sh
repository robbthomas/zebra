#!/bin/sh

#******************************************************************************
#                                                                             *
#  r11_to_r12.sh                                               		      *
#       Updates the location of the db directory in db_restore.sh             *
#                                                                             *
#                                                                             *
#                                                                             *
#******************************************************************************
rev_dir=updates/r11_to_r12


echo "Copying new db_restore.sh into place"
cp -f ${rev_dir}/db_restore.sh /home/ubuntu/shell
chmod +x /home/ubuntu/shell/db_restore.sh
echo "done"
