#!/bin/sh

#******************************************************************************
#                                                                             *
#  r10_to_r11.sh                                               		          *
#       Adds preview SSL bundle to server									  *
#                                                                             *
#       																	  *
#                                                                             *
#******************************************************************************
rev_dir=updates/r10_to_r11


echo "Copying SSL new archives to /etc/apache2/ssl"
cp -f ${rev_dir}/conf/ssl/* 	/etc/apache2/ssl