#!/bin/sh

#******************************************************************************
#                                                                             *
#  r12_to_r13.sh                                                		      *
#       Make sure that 8.8.8.8 is the first nameserver we check, even after   *
#       renewing DHCP lease.                                                  *
#                                                                             *
#                                                                             *
#                                                                             *
#******************************************************************************

echo "+--------------------------------------------------------------+"
echo "|  Executing r0012_to_r0013.sh                                 |"
echo "|                                                              |"
echo "|  fixing resolve.conf and DHCP                                |"
echo "+--------------------------------------------------------------+"

#----
#  make sure 8.8.8.8 is added at DHCP lease renew
cd /etc/dhcp/
cp dhclient.conf dhclient.conf_r12_to_r13.bak
cat dhclient.conf_r12_to_r13.bak | sed "s/#prepend domain-name-servers 127.0.0.1/prepend domain-name-servers 8.8.8.8/g" > dhclient.conf


#----
# make sure 8.8.8.8 is used NOW (changes will be overwritten at next DHCP lease renew)
cd /etc/
cp resolv.conf resolv.conf_r12_to_r13.bak
grep -B 10 -m 1 search resolv.conf > resolv.conf_tmp
echo "nameserver 8.8.8.8" >> resolv.conf_tmp
grep -A 10  nameserver resolv.conf >> resolv.conf_tmp
mv resolv.conf_tmp resolv.conf

echo "+--------------------------------------------------------------+"
echo "|  r0012_to_r0013.sh COMPLETE                                  |"
echo "|                                                              |"
echo "+--------------------------------------------------------------+"
