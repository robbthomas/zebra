package com.alleni.zebra.data.util;

import org.junit.Assert;
import org.junit.Test;

public class UUIDTest {

	@Test
	public void testStandardUUID() {
		String s = UUID.hibernateCompatibleUUID();
		Assert.assertEquals(UUID.isUUID(s), true);
		Assert.assertEquals(UUID.isUUID("not a uuid"), false);
	}

	@Test
	public void testExtractVersionLockedApnUUID() {
		String apn = UUID.hibernateCompatibleUUID();
		String detailApn = UUID.DETAIL_APN_UUID + apn ;
		String actual = UUID.extractVersionLockedApnUUID(detailApn);
		Assert.assertTrue(UUID.isUUID(actual));
		Assert.assertEquals(apn, actual);
	}
}
