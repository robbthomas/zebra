package com.alleni.zebra.util;

import junit.framework.Assert;

import org.junit.Test;


public class EqualsTest {
	
	public boolean orderOfOperations(boolean a, boolean b, boolean c) {
		boolean withoutParens = a || b && c;
		boolean withParens = a || (b && c);
		
		return withParens == withoutParens;
	}
	
	@Test
	public void testThreeBooleans() {
		Assert.assertTrue(new EqualsTest().orderOfOperations(false, false, false));
		Assert.assertTrue(new EqualsTest().orderOfOperations(false, false, true));
		Assert.assertTrue(new EqualsTest().orderOfOperations(false, true, false));
		Assert.assertTrue(new EqualsTest().orderOfOperations(true, false, false));
		Assert.assertTrue(new EqualsTest().orderOfOperations(false, true, true));
		Assert.assertTrue(new EqualsTest().orderOfOperations(true, false, true));
		Assert.assertTrue(new EqualsTest().orderOfOperations(true, true, false));
		Assert.assertTrue(new EqualsTest().orderOfOperations(true, true, true));
	}
}
