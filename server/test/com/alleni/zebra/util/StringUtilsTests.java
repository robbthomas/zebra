package com.alleni.zebra.util;

import static org.junit.Assert.assertEquals;
import junit.framework.Assert;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

public class StringUtilsTests {
	
	/**
	 * Test round-trip base64 code/decode of a String.
	 * 
	 * Since this is testing someone else's code, it functions more
	 * as a test of our assumptions of how that code behaves.
	 */
	@Test
	public void testFromBase64() {
		String test = "this is a test";
		Base64 codec = new Base64();
		String txt64 = codec.encodeToString(test.getBytes());
		
		assertEquals(true, test.equals(StringUtils.fromBase64(txt64)));
		
		
		// how does it handle empty Strings?
		test = "";
		txt64 = codec.encodeToString(test.getBytes());
		assertEquals(true, test.equals(StringUtils.fromBase64(txt64)));
		
	}

	
	/**
	 * test removing a char from a String.
	 */
	@Test
	public void testRemoveChar() {
		String input = "1234-567-890-";
		String expected = "1234567890";
		
		assertEquals(expected, StringUtils.removeChar(input, '-'));
		
		expected = "1234-567-890-";
		assertEquals(expected, StringUtils.removeChar(input, '\0'));
	}
	
	/**
	 * Make sure we fail early and loudly, but deliberately, for null input.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testRemoveCharFromNullString() {
		String input = null;
		
		// this should blow up w/ a IllegalArgumentExcepion
		StringUtils.removeChar(input, 'x');
	}
	
	
	@Test
	public void testWikiUrlEncode() {
		String before = "This is a test-";
		String expected = "This_is_a_test-";
		String actual = StringUtils.wikiUrlEncode(before);
		Assert.assertEquals(expected, actual);
	}
	
	
	@Test
	public void testIsNumeric() {
		String num = "5432101";
		String nonNum = "one23four";
		Assert.assertEquals(true, StringUtils.isNumeric(num));
		Assert.assertEquals(false, StringUtils.isNumeric(nonNum));
		
	}
}
