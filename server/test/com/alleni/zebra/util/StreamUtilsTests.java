package com.alleni.zebra.util;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class StreamUtilsTests {

    private Map<String, String> replacements;

    @Before
    public void setup() {
        replacements = new HashMap<String, String>();
        replacements.put("foo", "baz");
        replacements.put("foobarfoobarfoobarfoobar", "baz");
    }

    public void test(String input, String output) throws IOException {
        InputStream in = new ByteArrayInputStream(input.getBytes());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        StreamUtils.pipeReplace(in, out, replacements);
        byte[] actual = out.toByteArray();
        byte[] expected = output.getBytes();
        Assert.assertArrayEquals("[" + input + "][" + output + "][" + new String(actual) + "]", expected, actual);
    }

	@Test
	public void testNull() throws IOException {
        test("", "");
    }

	@Test
	public void testSimple() throws IOException {
        test("foo", "foo");
    }

	@Test
	public void testSimpleReplace() throws IOException {
        test("${foo}", "baz");
    }

	@Test
	public void testNotFound() throws IOException {
        test("${bar}", "${bar}");
    }

	@Test
	public void testLong() throws IOException {
        test("${foobarfoobarfoobarfoobar}", "${foobarfoobarfoobarfoobar}");
    }

	@Test
	public void testMultiple() throws IOException {
        test("${foo}${foo}", "bazbaz");
    }

	@Test
	public void testDoubleDollar() throws IOException {
        test("$${foo}", "$baz");
    }

	@Test
	public void testBadStart() throws IOException {
        test("${foo${foo}", "${foobaz");
    }
}
