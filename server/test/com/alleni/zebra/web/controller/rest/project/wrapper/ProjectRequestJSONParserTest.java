package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.util.Assert;

public class ProjectRequestJSONParserTest {

	@Test
	public void test() {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("projectName", "Hello world");
		ProjectRequestWrapper wrapper = new GenericJsonRequestParser<ProjectRequestWrapper>().parse(map, ProjectRequestWrapper.class);
		String out = wrapper.getProject().getProjectName();
		Assert.isTrue(out.equals("Hello world"));
	}

}
