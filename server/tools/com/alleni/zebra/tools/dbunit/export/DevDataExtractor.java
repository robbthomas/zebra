package com.alleni.zebra.tools.dbunit.export;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatDtdDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;

public class DevDataExtractor {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		extractData();
		createSchema();
	}

	private static void extractData() throws Exception {
        // database connection
        @SuppressWarnings({ "rawtypes", "unused" })
		Class driverClass = Class.forName("org.postgresql.Driver");
        Connection jdbcConnection = DriverManager.getConnection(
                "dbc:postgresql://10.1.1.136:5432", "boreal", "boreal");
        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);


        // full database export
        IDataSet fullDataSet = connection.createDataSet();
        FlatXmlDataSet.write(fullDataSet, new FileOutputStream("full.xml"));
		
	}
	
	private static void createSchema() throws Exception {
        // database connection
        @SuppressWarnings({ "rawtypes", "unused" })
		Class driverClass = Class.forName("org.postgresql.Driver");
        Connection jdbcConnection = DriverManager.getConnection("dbc:postgresql://10.1.1.136:5432", "boreal", "boreal");
        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);

        // write DTD file
        FlatDtdDataSet.write(connection.createDataSet(), new FileOutputStream("schema.dtd"));

	}

}
