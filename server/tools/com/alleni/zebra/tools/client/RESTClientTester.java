package com.alleni.zebra.tools.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;

public class RESTClientTester {

	public static void main(String[] args) throws Exception {
		System.out.println("starting...");
		String UrlString = "http://localhost:8090/zephyr/shoppe/purchase/99999";
		OutputStreamWriter out = null;

		String val = "dhoyt:testpassword";
		byte[] base = val.getBytes();
		
		String encoded = new String(Base64.encodeBase64(base));
		System.out.println(encoded);
		
		String authorizationString = "Basic " + encoded;

		URL url = new URL(UrlString);
		System.out.println("opening connection...");
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setDoOutput(true);
		connection.addRequestProperty("Accept", "application/json");
		connection.addRequestProperty("Test", "application/json");
		connection.setRequestProperty("Authorization", authorizationString);

		System.out.println("writing to output stream...");
		out = new OutputStreamWriter(
				connection.getOutputStream());
		out.write("\n\n"); // no parameters.
		out.close();

		BufferedReader in = null;
		try {
			System.out.println("reading response...");
			in = new BufferedReader(
					new InputStreamReader(
							connection.getInputStream()));

			String responseString;

			while ((responseString = in.readLine()) != null) {
				System.out.println("response string: " + responseString);
			}
		} catch (IOException ioe) {
			System.out.println("error...");
			
				in = new BufferedReader(
						new InputStreamReader(
								connection.getInputStream()));

				String responseString;

				while ((responseString = in.readLine()) != null) {
					System.out.println("response string: " + responseString);
				}
				
			
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
		System.out.println("done");

	}

}
