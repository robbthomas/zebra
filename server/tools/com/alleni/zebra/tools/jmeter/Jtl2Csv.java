package com.alleni.zebra.tools.jmeter;

/**
 * Not terribly pretty, but it didn't take too long to write, either...
 * 
 * This file converts the output of JMeter to a csv file, when JMeter 
 * is invoked with the following command:
 *
 *   jmeter -n -t data/[your_test_plan].jmx -l log.jtl 
 * 
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Jtl2Csv {
	final String time = "t", iteration = "lt", timestamp = "ts", label = "lb", code = "rc", by = "by";
	final String[] columns = {time, iteration, timestamp, label, code, by};
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			System.out.println("Usage:  Jtl2Csv [input] [output]");
			return;
		}
		
		new Jtl2Csv().process(args[0], args[1]);
	}

	private Jtl2Csv() {
	}
	
	public void process(String inputName, String outputName) throws Exception {
		FileReader reader = new FileReader(inputName);
		FileWriter writer = new FileWriter(outputName);
		
		
	    //...checks on aFile are elided
	    StringBuilder contents = new StringBuilder();
	    for (String name : columns) {
	    	contents.append(name);
	    	contents.append(",");
	    }
	    contents.deleteCharAt(contents.length() -1);
        contents.append(System.getProperty("line.separator"));

	    
	    try {
	      //use buffering, reading one line at a time
	      //FileReader always assumes default encoding is OK!
	      BufferedReader input =  new BufferedReader(reader);
	      try {
	        String line = null; //not declared within while loop
	        /*
	        * readLine is a bit quirky :
	        * it returns the content of a line MINUS the newline.
	        * it returns null only for the END of the stream.
	        * it returns an empty String if two newlines appear in a row.
	        */
	        while (( line = input.readLine()) != null){
	          contents.append(processLine(line));
	          contents.append(System.getProperty("line.separator"));
	        }
	      }
	      finally {
	        input.close();
	      }
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }

	    try {
	        //FileWriter always assumes default encoding is OK!
	    	writer.write( contents.toString() );
	      }
	      finally {
	    	  writer.close();
	      }
	    

	}
	
	
	/*
	 * converts a line of input into a csv line
	 * 
	 * <httpSample t="225" lt="201" ts="1300828315352" s="true" lb="project list" rc="200" rm="OK" tn="Thread Group 1-1" dt="text" by="335960"/>
	 */
	private String processLine(String line) {
		if (line == null || line.isEmpty() || ! line.startsWith("<httpSample"))
			return "";
		
		StringBuffer ret = new StringBuffer();
		for (String name : columns ) {
			ret.append(getValue(name, line));
			ret.append(",");
		}
		ret.deleteCharAt(ret.length() -1);
		
		return ret.toString();	
	}
	
	
	/**
	 * 			String terminator  = "}";

			int index = out.lastIndexOf(terminator)-1;
			if (index < 0) {
				index = out.lastIndexOf("]")-1;
				terminator = "]";
			}

	 */
	private String getValue(String name, String line) {
		
		String assignor = "=\"";
		String begin = name + assignor;
		
		int startIndex = line.indexOf(begin);
		if (startIndex > -1) {
			
			int endIndex = line.indexOf('"', startIndex + begin.length());
			
			if (endIndex > -1) {
				return line.substring(startIndex + begin.length(), endIndex);
			}
		}
		return "";
	}
	
}
