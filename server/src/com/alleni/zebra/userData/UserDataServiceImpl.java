package com.alleni.zebra.userData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.UserVariableDao;
import com.alleni.zebra.data.model.UserVariable;

@Service("userDataService")
public class UserDataServiceImpl implements UserDataService {
	
	@Autowired UserVariableDao dao;
	
	@Override
	@Transactional(readOnly=true)
	public Map<String, Object> averages(String projectId, String lookupKey) {
		Map<String, Object> avs = new HashMap<String, Object>();
		
		List<UserVariable> data = dao.list(projectId, lookupKey);
		float mean = mean(data);
		float median = median(data);
		
		long mode = mode(projectId, lookupKey);
		
		avs.put("mean", mean);
		avs.put("median", median);
		avs.put("mode", mode);
		
		return avs;
	}

	@Transactional(readOnly=true)
	private long mode(String projectId, String lookupKey) {
		List<Map<String, Object>> hist = dao.histogram(projectId, lookupKey);
		
		long maxValue = 0;
		long maxCount = 0;
		for (Map<String,Object> valCount : hist) {
			long c = (Long)valCount.get("count");
			if (c > maxCount) {
				maxCount = c;
				maxValue = (Long)valCount.get("value");
			}
		}
		return maxValue;
	}

	private float median(List<UserVariable> data) {
		long min = data.get(0).getValue();
		long max = data.get(0).getValue();
		
		for (UserVariable datum : data) {
			if (datum.getValue() < min) min = datum.getValue();
			if (datum.getValue() > max) max = datum.getValue();
		}
		
		return (min + max) / 2;
	}

	private float mean(List<UserVariable> data) {
		int sum = 0;
		for (UserVariable datum : data) {
			sum += datum.getValue();
		}
		
		return sum/data.size();
	}

	
}
