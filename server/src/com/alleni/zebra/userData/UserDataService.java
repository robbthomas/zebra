package com.alleni.zebra.userData;

import java.util.Map;

public interface UserDataService {
	
	public Map<String, Object> averages(String projectId, String lookupKey);
}
