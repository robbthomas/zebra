package com.alleni.zebra.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;


public class ConnectionPool extends BasicDataSource {
        private final Logger log = Logger.getLogger(ConnectionPool.class);
        
        @Override
        public Connection getConnection() throws SQLException {
            Connection conn;
            if (log.isDebugEnabled()) {
                log.debug("getting connection from pool");
                    conn = new LoggingConnection(super.getConnection());
            } else {
                conn = super.getConnection(); 
            }
            return conn; 
        }
        
        @Override
        public synchronized int getNumActive() {
            int numActive = super.getNumActive();
                return numActive;
        }
        
        @Override
        public synchronized int getNumIdle() {
                int numIdle = super.getNumIdle();
        return numIdle;
        }

        @Override
        public synchronized int getMaxActive() {
                int active = super.getMaxActive();
        return active;
        }
        
        @Override
        public synchronized void setMaxActive(int maxActive) {
            log.info(maxActive);
                super.setMaxActive(maxActive);
        }
        
        @Override
        public synchronized void setUrl(String url) {
            log.info(url);
            super.setUrl(url);
        }

        @Override
        public synchronized void setInitialSize(int initialSize) {
            log.info(initialSize);
            super.setInitialSize(initialSize);
        }
        
        @Override
        public synchronized int getInitialSize() {
            return super.getInitialSize();
        }
        
        @Override
        public synchronized void setMaxIdle(int maxIdle) {
            log.info(maxIdle);
            super.setMaxIdle(maxIdle);
        }
        
        @Override
        public synchronized void setDriverClassName(String driverClassName) {
            log.info(driverClassName);
            super.setDriverClassName(driverClassName);
        }
        
        @Override
        public synchronized void close() throws SQLException {
                if (log.isInfoEnabled())
                        log.info("Shutting down ConnectionPool");
            
                super.close();
                
                final Enumeration<Driver> drivers = DriverManager.getDrivers();
                while (drivers.hasMoreElements()) {
                        final Driver driver = drivers.nextElement();
                        if (driver.getClass().getClassLoader() == getClass().getClassLoader())
                                DriverManager.deregisterDriver(driver);
                }
        }


        public synchronized void restart() {
                if (log.isInfoEnabled())
                        log.info("Restarting ConnectionPool");
                
                try {
                        super.close();
                } catch (SQLException e) {
                        log.error(e);
                }
                
                try {
                        super.createDataSource();
                } catch (SQLException e) {
                        log.error(e);
                }
        }
        
}