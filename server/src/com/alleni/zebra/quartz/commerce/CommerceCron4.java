package com.alleni.zebra.quartz.commerce;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class CommerceCron4 extends AbstractEcommerceCronJob {
	private Logger log = Logger.getLogger(CommerceCron4.class);

	private final String jsp = "cron4.jsp";

	@Override
	protected void executeInternal(JobExecutionContext ctx)
			throws JobExecutionException {
		boolean enabled = ctx.getJobDetail().getJobDataMap().getBooleanValue("enabled");
		if (! enabled) {
			log.info("NOT executing cron4:  Job is disabled.");
			return;
		}
		
	}

	@Override
	public String executeExternal() {
		String response = null;
		Map<String,Object> args = new HashMap<String,Object>();
		args.put("json", "{\"commerceCron4\":{\"holdingDays\":21}}");
		try {
			log.debug("launching cron4");
			response = callJsp(jsp, args);
			log.info("Cron4 jsp returned:  " + response);
			
		} catch (IOException e) {
			log.error(e);
			e.printStackTrace();
		}
		
		return response;
	}

}
