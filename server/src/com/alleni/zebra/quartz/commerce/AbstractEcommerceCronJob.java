package com.alleni.zebra.quartz.commerce;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.scheduling.quartz.QuartzJobBean;

public abstract class AbstractEcommerceCronJob extends QuartzJobBean {
	private Logger log = Logger.getLogger(AbstractEcommerceCronJob.class);

	public abstract String executeExternal();
	
	public static AbstractEcommerceCronJob getInstance(Integer jobId) {
		int jobNo = jobId == null? 0 : jobId.intValue();
		
		switch (jobNo) {
		case 1:
			return new CommerceCron1();
		case 2:
			return new CommerceCron2();
		case 3:
			return new CommerceCron3();
		case 4:
			return new CommerceCron4();
		case 5:
			return new CommerceCron5();
		case 6:
			return new CommerceCron6();
		default:
				return null;
		}
	}
	
	
	protected String callJsp(String jspName, Map<String,Object> args) throws IOException {
		String urlString = "http://localhost:8080/site/services/commerce/" + jspName;

		String response = null;

		OutputStreamWriter out = null;

		String val = "eCommCron:o4vBnE4Q11"; //TODO:  make property
		byte[] base = val.getBytes();

		String encoded = new String(Base64.encodeBase64(base));
		System.out.println(encoded);

		String authorizationString = "Basic " + encoded;

		URL url = new URL(urlString);
		System.out.println("opening connection...");
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setDoOutput(true);
		connection.addRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Authorization", authorizationString);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

		out = new OutputStreamWriter(
				connection.getOutputStream());
		for (String key : args.keySet()) {
			out.write(key + "=" + args.get(key).toString());
		}

		out.write("\n\n");
		out.close();

		BufferedReader in = null;
		try {
			in = new BufferedReader(
					new InputStreamReader(
							connection.getInputStream()));

			String responseString;

			while ((responseString = in.readLine()) != null) {
				response = response + responseString;
			}
		} catch (IOException ioe) {
			log.error(ioe);
			in = new BufferedReader(
					new InputStreamReader(
							connection.getInputStream()));

			String responseString;
			while ((responseString = in.readLine()) != null) {
				response = response + responseString;
			}


		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}

		return response;
	}
}
