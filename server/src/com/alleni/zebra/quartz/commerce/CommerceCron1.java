package com.alleni.zebra.quartz.commerce;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class CommerceCron1 extends AbstractEcommerceCronJob {
	private Logger log = Logger.getLogger(CommerceCron1.class);
	
	private final String jsp = "cron1.jsp";

	protected void executeInternal(JobExecutionContext ctx)
			throws JobExecutionException {
		
		boolean enabled = ctx.getJobDetail().getJobDataMap().getBooleanValue("enabled");
		if (! enabled) {
			log.info("NOT executing cron1:  Job is disabled.");
			return;
		}
		
	}

	@Override
	public String executeExternal() {
		String response = null;
		
		Map<String,Object> args = new HashMap<String,Object>();
		args.put("json", "{\"commerceCron1\":{\"holdingDays\":7}}");
		try {
			log.debug("launching cron1");
			response = callJsp(jsp, args);
			log.info("Cron1 jsp returned:  " + response);
			
		} catch (IOException e) {
			log.error(e);
			e.printStackTrace();
		}
		
		return response;
	}

}
