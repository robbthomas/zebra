package com.alleni.zebra.quartz.commerce;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class CommerceCron2 extends AbstractEcommerceCronJob {
	private Logger log = Logger.getLogger(CommerceCron2.class);
	private final String jsp = "cron2.jsp";

	@Override
	protected void executeInternal(JobExecutionContext ctx)
			throws JobExecutionException {
		boolean enabled = ctx.getJobDetail().getJobDataMap().getBooleanValue("enabled");
		if (! enabled) {
			log.info("NOT executing cron2:  Job is disabled.");
			return;
		}
		
	}

	@Override
	public String executeExternal() {
		String response = null;
		
		Map<String,Object> args = new HashMap<String,Object>();
		args.put("json", "json={\"commerceCron2\":{\"accountId\":-1}}");
		try {
			log.debug("launching cron2");
			response = callJsp(jsp, args);
			log.info("Cron2 jsp returned:  " + response);
			
		} catch (IOException e) {
			log.error(e);
			e.printStackTrace();
		}

		return response;
	}

}
