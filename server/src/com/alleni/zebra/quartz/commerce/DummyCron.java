package com.alleni.zebra.quartz.commerce;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class DummyCron extends AbstractEcommerceCronJob {
	private Logger log = Logger.getLogger(DummyCron.class);

	@Override
	protected void executeInternal(JobExecutionContext ctx)
			throws JobExecutionException {
		boolean enabled = ctx.getJobDetail().getJobDataMap().getBooleanValue("enabled");
		if (! enabled) {
			// exit and don't talk about it.
			return;
		}
		
		log.info("Dummy Cron job doing nothing, as asked.");
	}

	@Override
	public String executeExternal() {
		log.info("Dummy Cron job doing nothing, as asked.");
		return null;
	}

}
