package com.alleni.zebra.quartz.db;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.system.mail.Mail;

public class SearchCacheCron extends QuartzJobBean {
	Logger log = Logger.getLogger(SearchCacheCron.class);
	
	// These collaborators are wired in as value-ref attributes in quartzConfig.xml
	// Note that one creates a helper class which in turn autowires its collaborators (preferred method)
	// The other directly references a bean that's already defined in webContext.xml (less preferred)
	private Mail mailService;
	private SearchCacheSupport searchCacheSupport;
	
	
	protected void executeInternal(JobExecutionContext ctx)
			throws JobExecutionException {
		boolean enabled = ctx.getJobDetail().getJobDataMap().getBooleanValue("enabled");
		if (! enabled) {
			log.info("SearchCacheCron not executing.");
			return;
		}
		
		// your code here
		String name = null;
		try {
			name = searchCacheSupport.getAppUserByEmail("sjackson@alleni.com").getFirstName();
		} catch (NotFoundException e) {
			log.error("sjackson@alleni.com not found");
		}
		log.info("successfully asked a dao about: " + name);

//		String msg = "Hi " + name + "!" +
//				"\n<br />Are we there yet? Are we? Huh?";
//		mailService.sendMultiPartMessage("dhoyt@alleni.com", "jbodell@alleni.com", "Are we there yet?",  msg, msg);
//		
		log.info("SearchCacheCron completed successfully.");
	}

	
	public SearchCacheSupport getSearchCacheSupport() {
		return searchCacheSupport;
	}

	public void setSearchCacheSupport(SearchCacheSupport searchCacheSupport) {
		this.searchCacheSupport = searchCacheSupport;
	}


	public Mail getMailService() {
		return mailService;
	}


	public void setMailService(Mail mailService) {
		this.mailService = mailService;
	}

}
