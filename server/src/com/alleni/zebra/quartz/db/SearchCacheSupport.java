package com.alleni.zebra.quartz.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;

@Component("searchCacheSupport")
public class SearchCacheSupport {
	@Autowired AppUserDao auDao;
	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.quartz.db.SearchCacheSupport#getAppUserByEmail(java.lang.String)
	 */
	@Transactional
	public AppUser getAppUserByEmail(String email) throws NotFoundException {
		return auDao.findByEmail(email);
	}
	
}
