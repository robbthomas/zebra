package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.Project;

public interface
        ProjectMapper extends RowMapper<Project> {

}
