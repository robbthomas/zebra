package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.TinyProject;

public interface TinyProjectMapper extends RowMapper<TinyProject> {

}
