package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.PublishedGadgetImage;

public interface PublishedGadgetImageMapper extends RowMapper<PublishedGadgetImage> {

}
