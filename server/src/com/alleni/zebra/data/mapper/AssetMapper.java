package com.alleni.zebra.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.Asset;

public interface AssetMapper extends RowMapper<Asset> {
	public abstract Asset mapRow(ResultSet rs, int row) throws SQLException;

}
