package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.ProjectTransfer;

public interface ProjectTransferMapper extends RowMapper<ProjectTransfer> {

}
