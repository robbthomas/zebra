package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.Thingy;

public interface ThingyMapper extends RowMapper<Thingy> {

}
