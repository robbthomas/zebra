package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.GadgetCategory;


public interface GadgetCategoryMapper extends RowMapper<GadgetCategory> {

}
