package com.alleni.zebra.data.mapper;


import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.hibernate.model.GuestList;


public interface GuestListMapper extends RowMapper<GuestList> {

}
