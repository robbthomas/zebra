package com.alleni.zebra.data.mapper.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.mapper.AccountProjectNumberMapper;
import com.alleni.zebra.data.model.AccountProjectNumber;

@Service("accountProjectNumberMapper")
public class AccountProjectNumberMapperImpl implements
		AccountProjectNumberMapper {

	@Override
	@Transactional(readOnly=true)
	public AccountProjectNumber mapRow(ResultSet rs, int arg1)
			throws SQLException {
		
		AccountProjectNumber p = new AccountProjectNumber();
		p.setUuid(rs.getString("accountprojectnumberid"));
		p.setAccountId(rs.getLong("accountId"));
		p.setRetired(rs.getInt("retired"));
		p.setProjectId(rs.getString("projectId"));
		Date d = rs.getDate("retiredDateTime");
		if (d != null)
			p.setRetiredDateTime(new Date(d.getTime()));
		
		p.setRetiredReasonId(rs.getLong("retiredReasonId"));
		
		d = rs.getDate("createdDateTime");
		if (d != null)
			p.setRetiredDateTime(new Date(d.getTime()));
		
		
		return p;
	}

}
