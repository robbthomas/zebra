package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.model.GuestList;
import com.alleni.zebra.data.mapper.GuestListMapper;

@Service("guestListMapper")
public class GuestListMapperImpl implements GuestListMapper {

	@Transactional
	public GuestList mapRow(ResultSet rs, int arg1) throws SQLException {
		GuestList gl = new GuestList();
		gl.setEmailtext(rs.getString("emailtext"));
		gl.setGuestlistid(rs.getInt("guestlistid"));
		return gl;
	}

}
