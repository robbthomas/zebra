package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Component;

import com.alleni.zebra.data.mapper.ThingyMapper;
import com.alleni.zebra.data.model.Thingy;

@Component("thingyMapper")
public class ThingyMapperImpl implements ThingyMapper {

	@Override
	public Thingy mapRow(ResultSet rs, int arg1) throws SQLException {
		Thingy thing = new Thingy();
		thing.setId(rs.getLong("id"));
		thing.setName(rs.getString("name"));
		return null;
	}

}
