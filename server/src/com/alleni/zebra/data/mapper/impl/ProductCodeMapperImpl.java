package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.mapper.ProductCodeMapper;
import com.alleni.zebra.data.model.ProductCode;

@Service("productCodeMapper")
public class ProductCodeMapperImpl implements ProductCodeMapper {

	@Override
	@Transactional(readOnly = true)
	public ProductCode mapRow(ResultSet rs, int arg1) throws SQLException {
		ProductCode pc = new ProductCode();
		pc.setProductCodeId(rs.getInt("productCodeId"));
		pc.setProductCategoryId(rs.getInt("productCategoryId"));
		pc.setRefundDays(rs.getInt("refundDays"));
		pc.setProductTypeId(rs.getInt("productTypeId"));
		pc.setDiscountType(rs.getString("discountType"));
		pc.setTag(rs.getString("tag"));
		pc.setDescription(rs.getString("description"));
		pc.setPrice(rs.getInt("price"));
		
		try {
			pc.setProductCategoryTag(rs.getString("productCategoryTag"));
		} catch (Exception ignore) {}

		try {
			pc.setProductTypeTag(rs.getString("productTypeTag"));
		} catch (Exception ignore) {}

		return pc;
	}

}
