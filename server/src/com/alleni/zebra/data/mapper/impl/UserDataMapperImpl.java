package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.mapper.UserDataMapper;
import com.alleni.zebra.data.model.UserVariable;

@Service("userDataMapper")
public class UserDataMapperImpl implements UserDataMapper {

	@Override
	@Transactional(readOnly=false)
	public UserVariable mapRow(ResultSet rs, int arg1) throws SQLException {
		UserVariable ud = new UserVariable();
		
		ud.setJsonData(rs.getString("jsonData"));
		ud.setLookupKey(rs.getString("lookupKey"));
		ud.setMemberId(rs.getString("memberId"));
		ud.setProjectId(rs.getString("projectId"));
		ud.setValue(rs.getLong("value"));
		
		return ud;
	}

}
