package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Component;

import com.alleni.zebra.data.mapper.ProjectTransferMapper;
import com.alleni.zebra.data.model.ProjectTransfer;
import com.alleni.zebra.util.DateUtils;

@Component("projectTransferMapper")
public class ProjectTransferMapperImpl implements ProjectTransferMapper {

	
	/**
	 * Some of these fields are join values.  Not populating them, however,
	 * is actually an error condition, so we don't bother to wrap them in a
	 * try-catch statement.
	 */
	public ProjectTransfer mapRow(ResultSet rs, int arg1) throws SQLException {
		ProjectTransfer p = new ProjectTransfer();
		
		p.setRetiredReasonTypeId(rs.getInt("retiredReasonTypeId"));
		
		
		p.setName(rs.getString("projectname"));
		p.setPrice(rs.getInt("price"));
		p.setProjectId(rs.getString("projectId"));
		p.setProjectMemberAccessId(rs.getLong("projectMemberAccessId"));
		p.setReceiverAccountName(rs.getString("receiverAccountName"));
		p.setReceiverAccountId(rs.getLong("receiverAccountId"));
		p.setReceiverMemberId(rs.getString("receiverMemberId"));
		p.setReceiverDisplayName(rs.getString("receiverDisplayName"));
		p.setCreatedDateTime(DateUtils.toUtilDate(rs.getTimestamp("createddatetime")));
		p.setHideFromReceiver(rs.getBoolean("hideFromReceiver"));
		p.setHideFromSender(rs.getBoolean("hideFromSender"));
		
		int retired = rs.getInt("retired");
		p.setRetired(retired > 0);
		p.setRetiredById(rs.getString("retiredById"));
		p.setRetiredDateTime(DateUtils.toUtilDate(rs.getTimestamp("retiredDateTime")));
		p.setSenderAccountId(rs.getLong("senderAccountId"));
		p.setSenderAccountName(rs.getString("senderAccountName"));
		p.setSenderDisplayName(rs.getString("senderDisplayName"));
		p.setSenderMemberId(rs.getString("senderMemberId"));
		p.setLicenseTypeId(rs.getInt("licenseTypeId"));
		p.setLicenseType(p.getLicenseType());

		p.setReceiverEmail(rs.getString("receiverEmail"));
		p.setSenderEmail(rs.getString("senderEmail"));

		int viewed = rs.getInt("newToReceiver");
		p.setNew(viewed > 0);
		p.setViewedDateTime(DateUtils.toUtilDate(rs.getTimestamp("viewedDateTime")));
		try {
			p.setRetiredReasonCode(rs.getString("retiredReasonCode"));
		} catch (Exception ignore) {
			// do nothing
		}

		try {
			p.setIconUrl("https://s3.amazonaws.com/com.alleni.zebra.site.content/icons/" 
					+ rs.getString("iconFileId")+ "-" + 100);
		} catch (Exception ignore) {
			// do nothing
		}
		return p;
	}

}
