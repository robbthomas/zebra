package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Component;

import com.alleni.zebra.data.mapper.GadgetTagMapper;
import com.alleni.zebra.data.model.GadgetTag;

@Component("gadgetTagMapper")
public class GadgetTagMapperImpl implements GadgetTagMapper {

	@Override
	public GadgetTag mapRow(ResultSet rs, int arg1) throws SQLException {
		GadgetTag gadgetTag = new GadgetTag();
		gadgetTag.setId(rs.getString("id"));
		gadgetTag.setUserId(rs.getString("user_id"));
		gadgetTag.setDateCreated(rs.getDate("date_created"));
		gadgetTag.setLastUpdated(rs.getDate("last_updated"));
		gadgetTag.setGadgetId(rs.getString("gadget_id"));
		gadgetTag.setTag(rs.getString("tag"));
		java.sql.Date d = rs.getDate("date_created");
		gadgetTag.setDateCreated(d == null? null : new java.util.Date(d.getTime()));

		d = rs.getDate("last_updated");
		gadgetTag.setLastUpdated(d == null? null : new java.util.Date(d.getTime()));


		return gadgetTag;
	}

}
