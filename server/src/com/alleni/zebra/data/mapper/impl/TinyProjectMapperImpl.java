package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.alleni.zebra.data.mapper.TinyProjectMapper;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.data.model.TinyProject;
import com.alleni.zebra.util.DateUtils;

@Component("TinyProjectMapper")
public class TinyProjectMapperImpl implements TinyProjectMapper {
	Logger log = Logger.getLogger(MiniProjectMapperImpl.class);
	
	@Override
	public TinyProject mapRow(ResultSet rs, int arg1) throws SQLException {
		TinyProject tp = new TinyProject();
		
		tp.setProjectId(rs.getString("projectId"));
		tp.setProjectName(rs.getString("projectName"));
		tp.setPublishedName(rs.getString("publishedName"));
		tp.setCreatedDateTime(DateUtils.toUtilDate(rs.getTimestamp("createdDateTime")));
		tp.setDescription(rs.getString("description"));
		
		try {
			PublishedGadgetImage i = new PublishedGadgetImage();
			i.setId(rs.getLong("icon_publishImageId"));
			i.setProjectId(rs.getString("icon_projectId"));
			i.setCaption(rs.getString("icon_caption"));
			i.setDisplayOrder(rs.getInt("icon_displayOrder"));
			i.setLabel(rs.getString("icon_label"));
			i.setIconFileId(rs.getString("icon_iconFileId"));
			i.setRenderingComplete(rs.getBoolean("icon_renderingComplete"));
			i.setRetired(rs.getBoolean("icon_retired"));
			if (i.getId() != 0) {
				tp.setIcon(i);
			}
		} catch (Exception ignore) {
			log.trace("no icon for project: " + tp.getProjectName());
		}
		
		return tp;
	}
}
