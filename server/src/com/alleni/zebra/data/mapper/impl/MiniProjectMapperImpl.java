package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.alleni.zebra.data.mapper.MiniProjectMapper;
import com.alleni.zebra.data.model.MiniProject;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.util.DateUtils;

@Component("MiniProjectMapper")
public class MiniProjectMapperImpl implements MiniProjectMapper {
	Logger log = Logger.getLogger(MiniProjectMapperImpl.class);
	
	@Override
	public MiniProject mapRow(ResultSet rs, int arg1) throws SQLException {
		MiniProject p = new MiniProject();
		p.setAccountId(rs.getLong("accountid"));
		p.setEditedDateTime(DateUtils.toUtilDate(rs.getTimestamp("editeddatetime")));
		p.setProjectId(rs.getString("projectid"));
		p.setDescription(rs.getString("description"));
		p.setCategoryId(rs.getInt("categoryId"));
		p.setCommentCount(rs.getInt("commentCount"));
		p.setProjectName(rs.getString("projectname"));
		p.setPublishedName(rs.getString("publishedname"));
		
		p.setHideInStoreList(rs.getBoolean("hideInStoreList"));
		p.setPublished(rs.getBoolean("published"));
		p.setDeleted(rs.getBoolean("retired"));
		p.setNextPublishedVersionId(rs.getString("nextPublishedVersionId"));
		
		p.setUrlName(rs.getString("urlName"));
		p.setPrice(rs.getInt("price"));
		
		
		p.setLicenseTypeId(rs.getInt("licenseTypeId"));
        if(rs.wasNull()) {
            p.setLicenseTypeId(null);
        }

        p.setAuthorDisplayName(rs.getString("authorDisplayName"));

        
        boolean newerThanUsers = false; //used in regular projects, not here yet. This may well change soon.
		boolean olderThanLatestVersion = rs.getBoolean("olderThanLatestVersion");
		p.setUpdateAvailable(newerThanUsers || olderThanLatestVersion);

        try {
			p.setEditable(rs.getBoolean("editable"));
			p.setRepublishable(rs.getBoolean("republishable"));
			p.setEmbedable(rs.getBoolean("embedable"));
		} catch (Exception ignore) {}
        
        try {
        	p.setCachedPurchaseAcquisitionCount(rs.getInt("cachedPurchaseAcquisitionCount"));
        	p.setRatingTotal(rs.getInt("ratingTotal"));
        	p.setNumRatings(rs.getInt("numRatings"));
        } catch (Exception ignore) {}
        
		try {
			p.setApnUUID(rs.getString("apnUUID"));
			p.setPlayerId(rs.getString("playerId"));
			p.setOwned(rs.getBoolean("owned"));
		} catch (Exception ignore) {}
		
		List<PublishedGadgetImage> screenshots = new ArrayList<PublishedGadgetImage>();
		for (int i = 0; i < 8; i++) {
			try {
				PublishedGadgetImage img = new PublishedGadgetImage();
				img.setId(rs.getLong("scrn_publishimageid" + i));
				img.setProjectId(rs.getString("scrn_projectId" + i));
				img.setCaption(rs.getString("scrn_caption" + i));
				img.setDisplayOrder(rs.getInt("scrn_displayOrder" + i));
				img.setLabel(rs.getString("scrn_label" + i));
				img.setScreenshotFileId(rs.getString("scrn_screenshotFileId" + i));
				img.setRenderingComplete(rs.getBoolean("scrn_renderingComplete" + i));
				img.setRetired(rs.getBoolean("scrn_retired" + i));
				if (img.getId() != 0) {
					screenshots.add(img);
				}
			} catch (Exception ignore) {
				log.trace(ignore);
				// no row
			}
		}
		Collections.sort(screenshots);
		p.setScreenshots(screenshots);
		
		try {
			PublishedGadgetImage i = new PublishedGadgetImage();
			i.setId(rs.getLong("icon_publishImageId"));
			i.setProjectId(rs.getString("icon_projectId"));
			i.setCaption(rs.getString("icon_caption"));
			i.setDisplayOrder(rs.getInt("icon_displayOrder"));
			i.setLabel(rs.getString("icon_label"));
			i.setIconFileId(rs.getString("icon_iconFileId"));
			i.setRenderingComplete(rs.getBoolean("icon_renderingComplete"));
			i.setRetired(rs.getBoolean("icon_retired"));
			if (i.getId() != 0) {
				p.setIcon(i);
			}
		} catch (Exception ignore) {
			log.trace("no icon for project: " + p.getProjectName());
			// no icon
		}
		
		try {
			p.setTotalCount(rs.getInt("totalCount"));
		} catch (Exception ignore) {
		}
		return p;
	}
	
}
