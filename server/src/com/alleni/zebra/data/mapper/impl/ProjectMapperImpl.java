package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alleni.zebra.data.mapper.ProjectMapper;
import com.alleni.zebra.data.model.CurrencyType;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.PublishStatus;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.gadget.GadgetCategoryService;
import com.alleni.zebra.gadget.ProjectType;
import com.alleni.zebra.util.DateUtils;

@Component("projectMapper")
public class ProjectMapperImpl implements ProjectMapper {
	Logger log = Logger.getLogger(ProjectMapperImpl.class);
	
	@Autowired GadgetCategoryService categorySvc;
	
	@Override
	public Project mapRow(ResultSet rs, int arg1) throws SQLException {
		Project p = new Project();
		
		// fields populated by the Projects table,
		// and/or by simple lookup tables for Enums
		p.setAccountId(rs.getLong("accountid"));
		p.setAutoSave(rs.getBoolean("autosave"));
		p.setCreatedDateTime(DateUtils.toUtilDate(rs.getTimestamp("createddatetime")));
		p.setDescription(rs.getString("description"));
		p.setDeleted(rs.getBoolean("retired"));
		p.setEditedDateTime(DateUtils.toUtilDate(rs.getTimestamp("editeddatetime")));
		p.setGuestListId(rs.getLong("guestlistid"));
		p.setInviteOnly(rs.getBoolean("inviteonly"));
		p.setLicenseTypeId(rs.getInt("licenseTypeId"));
		p.setNextPublishedVersionId(rs.getString("nextpublishedversionid"));
		p.setNextVersionId(rs.getString("nextversionid"));
		
		try {
			p.setEditable(rs.getBoolean("editable"));
			p.setRepublishable(rs.getBoolean("republishable"));
			p.setEmbedable(rs.getBoolean("embedable"));
		} catch (Exception ignore) {}

		p.setPrice(rs.getInt("price"));
		p.setProjectId(rs.getString("projectid"));
		p.setProjectName(rs.getString("projectname"));
		p.setPublished(rs.getBoolean("published"));
		p.setPublishedName(rs.getString("publishedname"));
        p.setUrlName(rs.getString("urlName"));
		
        try {
			p.setApnUUID(rs.getString("accountprojectnumberid"));
			p.setPlayerId(rs.getString("playerId"));
			p.setOwned(rs.getBoolean("owned"));
		} catch (Exception ignore) {}
        
        List<PublishedGadgetImage> screenshots = new ArrayList<PublishedGadgetImage>();
		for (int i = 0; i < 8; i++) {
			try {
				PublishedGadgetImage img = new PublishedGadgetImage();
				img.setId(rs.getLong("scrn_publishimageid" + i));
				img.setProjectId(rs.getString("scrn_projectId" + i));
				img.setCaption(rs.getString("scrn_caption" + i));
				img.setDisplayOrder(rs.getInt("scrn_displayOrder" + i));
				img.setLabel(rs.getString("scrn_label" + i));
				img.setScreenshotFileId(rs.getString("scrn_screenshotFileId" + i));
				img.setRenderingComplete(rs.getBoolean("scrn_renderingComplete" + i));
				img.setRetired(rs.getBoolean("scrn_retired" + i));
				if (img.getId() != 0) {
					screenshots.add(img);
				}
			} catch (Exception ignore) {
				log.trace(ignore);
				// no row
			}
		}
		Collections.sort(screenshots);
		p.setScreenshots(screenshots);
		
		try {
			PublishedGadgetImage i = new PublishedGadgetImage();
			i.setId(rs.getLong("icon_publishImageId"));
			i.setProjectId(rs.getString("icon_projectId"));
			i.setCaption(rs.getString("icon_caption"));
			i.setDisplayOrder(rs.getInt("icon_displayOrder"));
			i.setLabel(rs.getString("icon_label"));
			i.setIconFileId(rs.getString("icon_iconFileId"));
			i.setRenderingComplete(rs.getBoolean("icon_renderingComplete"));
			i.setRetired(rs.getBoolean("icon_retired"));
			if (i.getId() != 0) {
				p.setIcon(i);
			}
		} catch (Exception ignore) {
			log.trace("no icon for project: " + p.getProjectName());
			// no icon
		}
		
		//Optional fields below
		
		
        try {
		    p.setContent(rs.getString("content"));
			p.setAccountTypeId(rs.getInt("accountTypeId"));
			p.setAuthorMemberId(rs.getString("authormemberid"));
			p.setCategoryId(rs.getInt("categoryid"));
			p.setCategory(categorySvc.getCategoryForDbId(p.getCategoryId()));
        } catch (Exception ignore) {
            // just means that these fields aren't included in results
        }
        
        try {
		    p.setInitialValues(rs.getString("initialvalues"));
        } catch (Exception ignore) {
            // just means that these fields aren't included in results
        }
		
        try {
        	p.setCurrencyTypeId(rs.getInt("currencytypeid"));
    		p.setCurrencyType(CurrencyType.valueOfDbId(p.getCurrencyTypeId()));
    		p.setFeatured(rs.getBoolean("featured"));
    		p.setGadgetSize(rs.getInt("gadgetsize"));
    		p.setHideInStoreList(rs.getBoolean("hideinstorelist"));
    		p.setProjectMetadata(rs.getString("projectmetadata"));
    		p.setProjectType(ProjectType.valueOf(rs.getInt("projecttypeid")));
    		p.setProjectTypeId(p.getProjectType().id);
    		p.setPublishedMetadata(rs.getString("publishedmetadata"));
    		p.setPublishStatus(PublishStatus.valueOfDbId(rs.getInt("statusid")));
    		p.setStatusId(rs.getInt("statusid"));
            if(rs.wasNull()) {
                p.setStatusId(null);
            }
    		p.setSelfPrice(rs.getInt("selfprice"));
    		p.setVersion(rs.getInt("version"));
    		p.setVersionBuildNumber(rs.getInt("versionbuildnumber"));
    		p.setVersionDot(rs.getInt("versiondot"));
    		p.setVersionMajor(rs.getInt("versionmajor"));
    		p.setVersionMinor(rs.getInt("versionminor"));
    		p.setWidth(rs.getInt("width"));
    		p.setHeight(rs.getInt("height"));
            if(rs.wasNull()) {
                p.setLicenseTypeId(null);
            }
    		
    		boolean newerThanUsers = rs.getBoolean("newerThanWhatSearcherOwns");
    		boolean olderThanLatestVersion = rs.getBoolean("olderThanLatestVersion");
    		p.setUpdateAvailable(newerThanUsers || olderThanLatestVersion);        	
        } catch (Exception ignore) {}
		
		
		// fields populated by join queries to other tables.
		// fields from same or closely related tables are grouped
		// into a single "try" block.
		try {
			p.setCachedPurchaseAcquisitionCount(rs.getInt("cachedPurchaseAcquisitionCount"));
			p.setRatingTotal(rs.getInt("ratingTotal"));
			p.setNumRatings(rs.getInt("numRatings"));
			p.setCommentCount(rs.getInt("commentCount"));
		} catch (Exception ignore) {
			// just means that these fields aren't included in results
		}
		
		try {
			p.setAuthorFirstName(rs.getString("authorFirstName"));
			p.setAuthorLastName(rs.getString("authorLastName"));
			p.setAuthorWebsite(rs.getString("authorWebsite"));
			p.setAuthorDisplayName(rs.getString("authorDisplayName"));
			p.setAuthorNameFormat(rs.getString("authorNameFormat"));
			p.setAuthorAccountName(rs.getString("authorAccountName"));
		} catch (Exception ignore) {}

		try {
			p.setAuthorCompanyName(rs.getString("authorCompanyName"));
		} catch (Exception ignore) {}

		
		
		try {
			p.setTotalCount(rs.getInt("totalCount"));
		} catch (Exception ignore) {
		}
		return p;
	}
	
}
