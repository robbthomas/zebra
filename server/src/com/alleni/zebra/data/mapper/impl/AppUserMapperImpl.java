package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;

import com.alleni.zebra.data.mapper.AppUserMapper;
import com.alleni.zebra.data.model.AppUser;

@Service("appUserMapper")
public class AppUserMapperImpl implements AppUserMapper {

	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.dao.mapper.impl.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public AppUser mapRow(ResultSet rs, int row) throws SQLException {
		AppUser user = new AppUser();
		user.setId(rs.getString("id"));
//		user.setAccountId(rs.getLong("accountId"));
		user.setAccountId(rs.getLong("amr_accountid"));
		
		user.setEditedById(rs.getString("editedById"));
		user.setRetired(rs.getBoolean("retired"));
		user.setRetiredById(rs.getString("retiredById"));
		
		java.sql.Date d = rs.getDate("retiredDateTime");
		user.setRetiredDateTime(d == null? null : new java.util.Date(d.getTime()));
		
		user.setDescription(rs.getString("description"));
		user.setEmail(rs.getString("email"));
		user.setShowEmail(rs.getBoolean("emailShow"));
		
		user.setFirstName(rs.getString("firstName"));
		user.setLastName(rs.getString("lastName"));
		user.setUsername(rs.getString("displayName"));
		user.setWebsite(rs.getString("website"));
		user.setTwitter(rs.getString("twitter"));
		user.setNameFormat(rs.getString("nameformat"));
		d = rs.getDate("date_created");
		user.setDateCreated(d == null? null : new java.util.Date(d.getTime()));

		d = rs.getDate("last_updated");
		user.setLastUpdated(d == null? null : new java.util.Date(d.getTime()));
		
		try {
			user.setSalt( rs.getString("passwordSalt") );
			user.setPasswd(rs.getString("passwordHash"));
		} catch (Exception ignore) {
			// these fields are only present when called via login
		}
		return user;
	}


}
