package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;

import com.alleni.zebra.account.AccountType;
import com.alleni.zebra.data.mapper.AccountMapper;
import com.alleni.zebra.data.model.Account;

@Service("accountMapper")
public class AccountMapperImpl implements AccountMapper {

	@Override
	public Account mapRow(ResultSet rs, int arg1) throws SQLException {
		Account a = new Account();
		
		a.setCompanyId(rs.getInt("companyId"));
		a.setEditedById(rs.getString("editedById"));

		java.sql.Date d = rs.getDate("retiredDateTime");
		a.setEditedDateTime(d == null? null : new java.util.Date(d.getTime()));
		
		int b = rs.getInt("goodStanding");
		a.setGoodStanding(b == 1? true : false);
		
		a.setId(rs.getLong("accountId"));
		a.setName(rs.getString("name"));

		b = rs.getInt("retired");
		a.setRetired(b == 1? true : false);
		
		a.setRetiredById(rs.getString("retiredById"));
		d = rs.getDate("retiredDateTime");
		a.setRetiredDateTime(d == null? null : new java.util.Date(d.getTime()));
		
		d = rs.getDate("termDueDateTime");
		a.setRetiredDateTime(d == null? null : new java.util.Date(d.getTime()));
		a.setTermDueDateTime(d);
		
		a.setTokenBalance(rs.getInt("tokenBalance"));

		a.setTypeId(rs.getInt("typeId"));
		a.setAccountType(AccountType.valueOf(a.getTypeId()));
		a.setNumLearners(rs.getInt("numlearners"));
		a.setNumMembers(rs.getInt("numMembers"));
		
		try {
			a.setCompanyName(rs.getString("companyName"));
		} catch (Exception ignore) {
			// not joined to
		}
		return a;
	}

}
