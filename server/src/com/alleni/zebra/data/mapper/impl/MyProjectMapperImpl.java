package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.alleni.zebra.data.mapper.MyProjectMapper;
import com.alleni.zebra.data.model.MyProject;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.util.DateUtils;

@Component("MyProjectMapper")
public class MyProjectMapperImpl implements MyProjectMapper {
	Logger log = Logger.getLogger(MyProjectMapperImpl.class);
	
	@Override
	public MyProject mapRow(ResultSet rs, int arg1) throws SQLException {
		MyProject mp = new MyProject();
	    
	    mp.setTotalCount(rs.getInt("totalCount"));
	    mp.setProjectId(rs.getString("projectId"));
	    mp.setCreatedDateTime(DateUtils.toUtilDate(rs.getTimestamp("createdDateTime")));
	    mp.setEditedDateTime(DateUtils.toUtilDate(rs.getTimestamp("editedDateTime")));
	    mp.setProjectName(rs.getString("projectName"));
	    mp.setProjectTypeId(rs.getInt("projectTypeId"));
	    mp.setHideInStoreList(rs.getBoolean("hideInStoreList"));
	    mp.setFeatured(rs.getBoolean("featured"));
	    mp.setPublished(rs.getBoolean("published"));
	    mp.setRepublishable(rs.getBoolean("republishable"));
	    mp.setEditable(rs.getBoolean("editable"));
	    mp.setEmbedable(rs.getBoolean("embedable"));
	    mp.setAuthorFirstName(rs.getString("authorFirstName"));
	    mp.setAuthorLastName(rs.getString("authorLastName"));
	    mp.setAuthorWebsite(rs.getString("authorWebsite"));
	    mp.setAuthorDisplayName(rs.getString("authorDisplayName"));
	    mp.setAuthorNameFormat(rs.getString("authorNameFormat"));
	    mp.setAuthorCompanyName(rs.getString("authorCompanyName"));
	    mp.setAuthorAccountName(rs.getString("authorAccountName"));
	    mp.setCategoryId(rs.getInt("categoryId"));
	    mp.setPublishedName(rs.getString("publishedName"));
	    mp.setDescription(rs.getString("description"));
	    mp.setUrlName(rs.getString("urlName"));
	    mp.setAuthorMemberId(rs.getString("authorMemberId"));
	    mp.setAccountId(rs.getInt("accountId"));
	    mp.setProjectAccountTypeName(rs.getString("projectAccountTypeName"));
	    mp.setAccountProjectNumberId(rs.getString("accountProjectNumberId"));
	    mp.setPlayerId(rs.getString("playerId"));
	    mp.setOlderThanLatestVersion(rs.getBoolean("olderThanLatestVersion"));
	    mp.setCommentCount(rs.getInt("commentCount"));
	    mp.setNumRatings(rs.getInt("numratings"));
	    mp.setTotalRating(rs.getInt("ratingtotal"));
	    
	    try {
			PublishedGadgetImage i = new PublishedGadgetImage();
			i.setId(rs.getLong("icon_publishImageId"));
			i.setProjectId(rs.getString("icon_projectId"));
			i.setCaption(rs.getString("icon_caption"));
			i.setDisplayOrder(rs.getInt("icon_displayOrder"));
			i.setLabel(rs.getString("icon_label"));
			i.setIconFileId(rs.getString("icon_iconFileId"));
			i.setRenderingComplete(rs.getBoolean("icon_renderingComplete"));
			i.setRetired(rs.getBoolean("icon_retired"));
			if (i.getId() != 0) {
				mp.setIcon(i);
			}
		} catch (Exception ignore) {
			log.trace("no icon for project: " + mp.getProjectName());
		}
		
		return mp;
	}

}
