package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;

import com.alleni.zebra.data.mapper.GadgetStateMapper;
import com.alleni.zebra.data.model.GadgetState;

@Service("gadgetStateMapper")
public class GadgetStateMapperImpl implements GadgetStateMapper {

	@Override
	public GadgetState mapRow(ResultSet rs, int row) throws SQLException {
		GadgetState gadgetState = new GadgetState();
		gadgetState.setId(rs.getString("id"));
		gadgetState.setVersion(rs.getLong("version"));
		gadgetState.setAuthorMode(rs.getBoolean("author_mode"));
		gadgetState.setRootProjectId(rs.getString("root_project_id"));
		gadgetState.setAppUserId(rs.getString("app_user_id"));
		gadgetState.setExternalUserId(rs.getString("externaluserid"));
		Object content = rs.getObject("content");
		if (content != null)
				gadgetState.setContent(content.toString());

		return gadgetState;
	}

}
