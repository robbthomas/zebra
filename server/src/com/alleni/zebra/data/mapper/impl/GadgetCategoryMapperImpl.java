package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.mapper.GadgetCategoryMapper;
import com.alleni.zebra.data.model.GadgetCategory;

@Service("gadgetCategorymapper")
public class GadgetCategoryMapperImpl implements GadgetCategoryMapper {

	@Override
	@Transactional(readOnly=true)
	public GadgetCategory mapRow(ResultSet rs, int rowCount) throws SQLException {
		GadgetCategory gc = new GadgetCategory();
		gc.setCategoryLevel(rs.getInt("categoryLevel"));
		gc.setCategoryOrder(rs.getInt("categoryOrder"));
		gc.setDescription(rs.getString("description"));
		gc.setEditedById(rs.getString("editedById"));
		
		java.sql.Date d = rs.getDate("editedDateTime");
		gc.setEditedDateTime(d == null? null : new java.util.Date(d.getTime()));

		gc.setId(rs.getLong("categoryId"));
		gc.setName(rs.getString("name"));
		gc.setParentCategoryId(rs.getLong("parentCategoryId"));
		gc.setProjectTypeId(rs.getLong("typeId"));

		gc.setEditedById(rs.getString("retiredById"));
		
		d = rs.getDate("retiredDateTime");
		gc.setEditedDateTime(d == null? null : new java.util.Date(d.getTime()));
		
		int retired = rs.getInt("retired");
		gc.setRetired((retired > 0));
		
		return gc;
	}

}
