package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;

import com.alleni.zebra.data.mapper.AssetMapper;
import com.alleni.zebra.data.model.Asset;

@Service("assetMapper")
public class AssetMapperImpl implements AssetMapper {

	@Override
	public Asset mapRow(ResultSet rs, int row) throws SQLException {
		Asset asset = new Asset();
		asset.setId(rs.getString("id"));
		asset.setVersion(rs.getLong("version"));
		asset.setDataSizeInBytes(rs.getLong("data_size_in_bytes"));
		asset.setDeleted(rs.getBoolean("deleted"));
		asset.setFileID(rs.getString("fileid"));	
		asset.setMimeType(rs.getString("mime_type"));
		asset.setName(rs.getString("name"));
		asset.setNextVersionId(rs.getString("next_version_id"));
		asset.setPermissions(rs.getString("permissions"));
		asset.setUserId(rs.getString("user_id"));

		java.sql.Date d = rs.getDate("date_created");
		asset.setDateCreated(d == null? null : new java.util.Date(d.getTime()));
		
		d = rs.getDate("last_updated");
		asset.setLastUpdated(d == null? null : new java.util.Date(d.getTime()));


		return asset;
	}

}
