package com.alleni.zebra.data.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.mapper.PublishedGadgetImageMapper;
import com.alleni.zebra.data.model.PublishedGadgetImage;

@Service("publishedGadgetImageMapper")
public class PublishedGadgetImageMapperImpl implements PublishedGadgetImageMapper {

	@Override
	@Transactional
	public PublishedGadgetImage mapRow(ResultSet rs, int arg1)
			throws SQLException {
		PublishedGadgetImage i = new PublishedGadgetImage();
		i.setId(rs.getLong("publishImageId"));
		i.setProjectId(rs.getString("projectId"));
		i.setCaption(rs.getString("caption"));
		i.setDisplayOrder(rs.getInt("displayOrder"));
		i.setLabel(rs.getString("label"));
		i.setScreenshotFileId(rs.getString("screenshotFileId"));
		i.setIconFileId(rs.getString("iconFileId"));
		i.setRenderingComplete(rs.getBoolean("renderingComplete"));
		i.setRetired(rs.getBoolean("retired"));
		return i;
	}

}
