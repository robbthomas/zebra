package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.AccountProjectNumber;

public interface AccountProjectNumberMapper extends
		RowMapper<AccountProjectNumber> {

}
