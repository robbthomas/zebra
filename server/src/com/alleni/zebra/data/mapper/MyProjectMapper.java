package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.MyProject;

public interface MyProjectMapper extends RowMapper<MyProject> {

}
