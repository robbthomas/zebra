package com.alleni.zebra.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.GadgetState;

public interface GadgetStateMapper extends RowMapper<GadgetState> {
	public abstract GadgetState mapRow(ResultSet rs, int row) throws SQLException;

}
