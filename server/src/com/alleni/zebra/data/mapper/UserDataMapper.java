package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.UserVariable;

public interface UserDataMapper extends RowMapper<UserVariable> {

}
