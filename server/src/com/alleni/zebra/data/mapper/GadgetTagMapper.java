package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.GadgetTag;

public interface GadgetTagMapper extends RowMapper<GadgetTag> {

}
