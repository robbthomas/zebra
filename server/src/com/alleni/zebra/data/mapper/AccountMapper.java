package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.Account;

public interface AccountMapper extends RowMapper<Account> {

}
