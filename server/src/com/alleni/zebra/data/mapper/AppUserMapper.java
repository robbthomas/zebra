package com.alleni.zebra.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.AppUser;

public interface AppUserMapper extends RowMapper<AppUser>{

	public abstract AppUser mapRow(ResultSet rs, int row) throws SQLException;

	
}