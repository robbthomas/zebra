package com.alleni.zebra.data.mapper;

import org.springframework.jdbc.core.RowMapper;

import com.alleni.zebra.data.model.MiniProject;

public interface MiniProjectMapper extends RowMapper<MiniProject> {

}
