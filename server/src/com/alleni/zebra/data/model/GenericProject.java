package com.alleni.zebra.data.model;

public interface GenericProject {
    String getPublishedName();
    String getProjectName();
}
