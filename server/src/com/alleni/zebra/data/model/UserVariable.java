package com.alleni.zebra.data.model;

public class UserVariable {

	private String projectId; //key
	private String memberId; // key
	private long value;
	private String jsonData;
	private String lookupKey; //key
	
	public UserVariable() {
		super();
	}
	
	
	public UserVariable(String projectId, String memberId, long value,
			String jsonData, String lookupKey) {
		super();
		this.projectId = projectId;
		this.memberId = memberId;
		this.value = value;
		this.jsonData = jsonData;
		this.lookupKey = lookupKey;
	}


	public String getProjectId() {
		return projectId;
	}


	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}


	public String getMemberId() {
		return memberId;
	}


	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}


	public long getValue() {
		return value;
	}


	public void setValue(long value) {
		this.value = value;
	}


	public String getJsonData() {
		return jsonData;
	}


	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}


	public String getLookupKey() {
		return lookupKey;
	}


	public void setLookupKey(String lookupKey) {
		this.lookupKey = lookupKey;
	}


}
