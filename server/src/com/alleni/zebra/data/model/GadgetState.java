package com.alleni.zebra.data.model;


public class GadgetState {

	private String id;
	private Long version;
	private String appUserId;
	private Boolean authorMode;
	private String content;
	private String rootProjectId;
	private String externalUserId;
	


	
	public String getId() {
		return id;
	}
	
	
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * @return the appUserId
	 */
	public String getAppUserId() {
		return appUserId;
	}

	/**
	 * @param appUserId the appUserId to set
	 */
	public void setAppUserId(String appUserId) {
		this.appUserId = appUserId;
	}

	/**
	 * @return the authorMode
	 */
	public Boolean getAuthorMode() {
		return authorMode;
	}

	/**
	 * @param authorMode the authorMode to set
	 */
	public void setAuthorMode(Boolean authorMode) {
		this.authorMode = authorMode;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the rootProjectId
	 */
	public String getRootProjectId() {
		return rootProjectId;
	}

	/**
	 * @param rootProjectId the rootProjectId to set
	 */
	public void setRootProjectId(String rootProjectId) {
		this.rootProjectId = rootProjectId;
	}

    public String getExternalUserId() {
        return externalUserId;
    }

    public void setExternalUserId(String externalUserId) {
        this.externalUserId = externalUserId;
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((appUserId == null) ? 0 : appUserId.hashCode());
		result = prime * result
				+ ((authorMode == null) ? 0 : authorMode.hashCode());
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result
				+ ((externalUserId == null) ? 0 : externalUserId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((rootProjectId == null) ? 0 : rootProjectId.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GadgetState other = (GadgetState) obj;
		if (appUserId == null) {
			if (other.appUserId != null)
				return false;
		} else if (!appUserId.equals(other.appUserId))
			return false;
		if (authorMode == null) {
			if (other.authorMode != null)
				return false;
		} else if (!authorMode.equals(other.authorMode))
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (externalUserId == null) {
			if (other.externalUserId != null)
				return false;
		} else if (!externalUserId.equals(other.externalUserId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (rootProjectId == null) {
			if (other.rootProjectId != null)
				return false;
		} else if (!rootProjectId.equals(other.rootProjectId))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}


}
