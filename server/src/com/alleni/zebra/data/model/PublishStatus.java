package com.alleni.zebra.data.model;

public enum PublishStatus {
	DEVELOPMENT(1, "publish_status_development", "Development", 10),
	ALPHA(2, "publish_status_alpha", "Alpha", 20),
	BETA(3, "publish_status_beta", "Beta", 30),
	RELEASE(4, "publish_status_release", "Release", 40);
	
	public final int id;
	public final String tag;
	public final String description;
	public final int displayOrder;
	
	private PublishStatus(int id, String tag, String description,
			int displayOrder) {
		this.id = id;
		this.tag = tag;
		this.description = description;
		this.displayOrder = displayOrder;
	}
	
	public static final PublishStatus valueOfDbId(int dbId) {
		for (PublishStatus s : values())
			if (s.id == dbId)
				return s;
				
		return null;
	}
}
