package com.alleni.zebra.data.model;

import java.util.Date;
import java.util.List;

public class MiniProject implements GenericProject {
	private long accountId;
	protected String projectId;
	private String description;
	private int projectTypeId;
	private int categoryId;
	private Date createdDateTime;
	private Date editedDateTime;
	private String projectName;
	private String publishedName;
	private String apnUUID;
	private String playerId;
	private boolean owned;
	private String urlName;
	private int commentCount;
	private int totalCount;
	private PublishedGadgetImage icon;
	private Integer licenseTypeId;
	private boolean republishable;
	private boolean editable;
	private boolean embedable;
	private boolean inviteOnly;
	private int cachedPurchaseAcquisitionCount;
   	private int ratingTotal;
    private int numRatings;
    private String authorDisplayName;
    private int price;
    
    private boolean published;
   	private boolean hideInStoreList;
    private boolean deleted;
    private String nextPublishedVersionId;
    private boolean updateAvailable;
    
   	private List<PublishedGadgetImage> screenshots;
    
	public String getProjectId() {
		return projectId;
	}
	
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	public int getProjectTypeId() {
		return projectTypeId;
	}
	
	public void setProjectTypeId(int projectTypeId) {
		this.projectTypeId = projectTypeId;
	}
	
	public int getCategoryId() {
		return categoryId;
	}
	
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	public int getCommentCount() {
		return commentCount;
	}
	
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	
	// License/Permission-related fields
	public Integer getLicenseTypeId() {
		return licenseTypeId;
	}

	public void setLicenseTypeId(Integer licenseTypeId) {
		this.licenseTypeId = licenseTypeId;
	}
	
	public boolean isInviteOnly() {
		return inviteOnly;
	}

	public void setInviteOnly(boolean inviteOnly) {
		this.inviteOnly = inviteOnly;
	}
	
	public boolean isRepublishable() {
		return republishable;
	}

	public void setRepublishable(boolean republishable) {
		this.republishable = republishable;
	}
	
	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean isEmbedable() {
		return embedable;
	}

	public void setEmbedable(boolean embedable) {
		this.embedable = embedable;
	}
	//-----------------------------------
	
	public int getCachedPurchaseAcquisitionCount() {
		return cachedPurchaseAcquisitionCount;
	}

	public void setCachedPurchaseAcquisitionCount(int cachedPurchaseAcquisitionCount) {
		this.cachedPurchaseAcquisitionCount = cachedPurchaseAcquisitionCount;
	}

	public int getRatingTotal() {
		return ratingTotal;
	}

	public void setRatingTotal(int ratingTotal) {
		this.ratingTotal = ratingTotal;
	}

	public int getNumRatings() {
		return numRatings;
	}

	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	
	
	
	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Date getEditedDateTime() {
		return editedDateTime;
	}

	public void setEditedDateTime(Date editedDateTime) {
		this.editedDateTime = editedDateTime;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getPublishedName() {
		return publishedName;
	}

	public void setPublishedName(String publishedName) {
		this.publishedName = publishedName;
	}

	public String getApnUUID() {
		return apnUUID;
	}

	public void setApnUUID(String apnUUID) {
		this.apnUUID = apnUUID;
	}

	public boolean isOwned() {
		return owned;
	}

	public void setOwned(boolean owned) {
		this.owned = owned;
	}

	public PublishedGadgetImage getIcon() {
		return icon;
	}

	public void setIcon(PublishedGadgetImage icon) {
		this.icon = icon;
	}

	public String getUrlName() {
		return urlName;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	
	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public boolean isHideInStoreList() {
		return hideInStoreList;
	}

	public void setHideInStoreList(boolean hideInStoreList) {
		this.hideInStoreList = hideInStoreList;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getNextPublishedVersionId() {
		return nextPublishedVersionId;
	}

	public void setNextPublishedVersionId(String nextPublishedVersionId) {
		this.nextPublishedVersionId = nextPublishedVersionId;
	}
	
	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

    public String getAuthorDisplayName() {
        return authorDisplayName;
    }

    public void setAuthorDisplayName(String authorDisplayName) {
        this.authorDisplayName = authorDisplayName;
    }
    
    public int getPrice() {
    	return price;
    }
    
    public void setPrice(int price) {
    	this.price = price;
    }
    
    public List<PublishedGadgetImage> getScreenshots() {
		return screenshots;
	}

	public void setScreenshots(List<PublishedGadgetImage> screenshots) {
		this.screenshots = screenshots;
	}
	
	public boolean isUpdateAvailable() {
		return updateAvailable;
	}

	public void setUpdateAvailable(boolean updateAvailable) {
		this.updateAvailable = updateAvailable;
	}
}
