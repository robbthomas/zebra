package com.alleni.zebra.data.model;

import java.util.Date;

import com.alleni.zebra.account.AccountType;

public class Account {

	private long id;
	private int companyId;
	private int typeId;
	private AccountType accountType; // populated by mapper
	private Date termDueDateTime;
	private boolean goodStanding;
	private String name;
	private int tokenBalance;
	private String editedById;
	private Date editedDateTime;
	private boolean retired;
	private String retiredById;
	private Date retiredDateTime;
	private int maxInvitesPerProject;
	private int maxInvitesPerAccount;
	private int numLearners;
	private int numMembers;
	
	// columns from other tables
	private String companyName;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public Date getTermDueDateTime() {
		return termDueDateTime;
	}
	public void setTermDueDateTime(Date termDueDateTime) {
		this.termDueDateTime = termDueDateTime;
	}
	public boolean isGoodStanding() {
		return goodStanding;
	}
	public void setGoodStanding(boolean goodStanding) {
		this.goodStanding = goodStanding;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTokenBalance() {
		return tokenBalance;
	}
	public void setTokenBalance(int tokenBalance) {
		this.tokenBalance = tokenBalance;
	}
	public String getEditedById() {
		return editedById;
	}
	public void setEditedById(String editedById) {
		this.editedById = editedById;
	}
	public Date getEditedDateTime() {
		return editedDateTime;
	}
	public void setEditedDateTime(Date editedDateTime) {
		this.editedDateTime = editedDateTime;
	}
	public boolean isRetired() {
		return retired;
	}
	public void setRetired(boolean retired) {
		this.retired = retired;
	}
	public String getRetiredById() {
		return retiredById;
	}
	public void setRetiredById(String retiredById) {
		this.retiredById = retiredById;
	}
	public Date getRetiredDateTime() {
		return retiredDateTime;
	}
	public void setRetiredDateTime(Date retiredDateTime) {
		this.retiredDateTime = retiredDateTime;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public AccountType getAccountType() {
		return accountType;
	}
	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}
	public int getMaxInvitesPerProject() {
		return maxInvitesPerProject;
	}
	public void setMaxInvitesPerProject(int maxInvitesPerProject) {
		this.maxInvitesPerProject = maxInvitesPerProject;
	}
	public int getMaxInvitesPerAccount() {
		return maxInvitesPerAccount;
	}
	public void setMaxInvitesPerAccount(int maxInvitesPerAccount) {
		this.maxInvitesPerAccount = maxInvitesPerAccount;
	}
	public int getNumLearners() {
		return numLearners;
	}
	public void setNumLearners(int maxLearners) {
		this.numLearners = maxLearners;
	}
	public int getNumMembers() {
		return numMembers;
	}
	public void setNumMembers(int maxMembers) {
		this.numMembers = maxMembers;
	}
}
