package com.alleni.zebra.data.model;

public enum CurrencyType {

	US_DOLLAR(1, "US Dollar", "Dollar", "USD", "$", "US", 2, ",", ".");

	public final int dbId;
	public final String name;
	public final String currencyName;
	public final String currencyCode;
	public final String currencySymbol;
	public final String countryCode;
	public final int precision;
	public final String majorDelimiter;
	public final String minorDelimiter;

	private CurrencyType(int dbId, String name, String currencyName,
			String currencyCode, String currencySymbol, String countryCode,
			int precision, String majorDelimiter, String minorDelimiter) {
		this.dbId = dbId;
		this.name = name;
		this.currencyName = currencyName;
		this.currencyCode = currencyCode;
		this.currencySymbol = currencySymbol;
		this.countryCode = countryCode;
		this.precision = precision;
		this.majorDelimiter = majorDelimiter;
		this.minorDelimiter = minorDelimiter;
	}
	
	public static CurrencyType valueOfDbId(int dbId) {
		// there's only one type, so for now don't bother with a for() loop.
		if (dbId == US_DOLLAR.dbId) { return US_DOLLAR; }
		return null;
	}
}
