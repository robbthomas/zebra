package com.alleni.zebra.data.model;

import java.util.Date;
import java.util.List;

import com.alleni.zebra.security.authorization.Role;


public class AppUser  {
	private String id;
	private Date dateCreated;
	private String editedById; // fk to app_user
	private boolean retired;
	private String retiredById;
	private Date retiredDateTime;
	private String description;
	private String email;
	private boolean showEmail;
	private boolean isEnabled;
	private Date lastUpdated;
	private String passwd;
	private String username;
	private String salt;
	private String firstName;
	private String lastName;
	private Long accountId;
	private String website;
	private String twitter;
	private String nameFormat;
	
	private List<Role> roles;
	private Account account;
	
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public boolean isRetired() {
		return retired;
	}
	public void setRetired(boolean isDeleted) {
		this.retired = isDeleted;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isShowEmail() {
		return showEmail;
	}
	public void setShowEmail(boolean isShowEmail) {
		this.showEmail = isShowEmail;
	}
	public boolean isEnabled() {
		return isEnabled;
	}
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEditedById() {
		return editedById;
	}
	public void setEditedById(String editedById) {
		this.editedById = editedById;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getRetiredById() {
		return retiredById;
	}
	public void setRetiredById(String retiredById) {
		this.retiredById = retiredById;
	}
	public Date getRetiredDateTime() {
		return retiredDateTime;
	}
	public void setRetiredDateTime(Date retiredDateTime) {
		this.retiredDateTime = retiredDateTime;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getTwitter() {
		return twitter;
	}
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	public String getNameFormat() {
		return nameFormat;
	}
	public void setNameFormat(String nameFormat) {
		this.nameFormat = nameFormat;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
