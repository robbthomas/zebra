package com.alleni.zebra.data.model;

import com.alleni.zebra.gadget.ProjectType;

public enum LicenseType {
	NULL_ZAPP(-1, false, false, false, ProjectType.APP),
    NULL_GADGET(-1, false, false, false, ProjectType.GADGET),
    NULL_EVENT(-1, false, false, false, ProjectType.EVENT),
	ZAPP_MINIMAL(1, false, false, false, ProjectType.APP),
	GADGET_MINIMAL(2, false, false, false, ProjectType.GADGET),
	GADGET_REPUB_ONLY(3, true, false, false, ProjectType.GADGET),
	GADGET_REPUB_EDIT(4, true, true, false, ProjectType.GADGET),
	GADGET_EDIT_ONLY(5, false, true, false, ProjectType.GADGET),
	ZAPP_EMBED_ONLY(6, false, false, true, ProjectType.APP);


	public final Integer id;
	public final Boolean republish;
	public final Boolean edit;
	public final Boolean embed;
	public final ProjectType projectType;


	public static LicenseType findById(Integer id) {
		LicenseType ret = null;

		for (LicenseType candidate : values()) {
			if (candidate.id != null && candidate.id.equals(id)){
				ret = candidate;
				break;
			}
		}
		return ret;
	}

	public static LicenseType findCanonical(Integer id, Boolean republish, Boolean edit, Boolean embed, ProjectType type) {
		LicenseType ret = type == null? NULL_ZAPP : type == ProjectType.APP? NULL_ZAPP : NULL_GADGET;

		if (id != null) {
			ret = findById(id);
		} else {

			for (LicenseType candidate : values()) {

				if (candidate.projectType.equals(type)) {
					if (type == ProjectType.APP) {
						if (candidate.embed.equals(embed) ) {
							ret = candidate;
							break;
						}
					} else if (type == ProjectType.GADGET) {
						if (candidate.edit.equals(edit) &&
								candidate.republish.equals(republish)) {
							ret = candidate;
							break;
						}
					} else {
						// unknown project type
						if (candidate.edit.equals(edit) &&
								candidate.embed.equals(embed) &&
								candidate.republish.equals(republish)) {
							ret = candidate;
							break;
						}
					}
				}
			}
		} // what, can't you count curly braces?

		return ret;
	}



	private LicenseType(Integer id, Boolean republish, Boolean edit,
			Boolean embed, ProjectType projectType) {
		this.id = id;
		this.republish = republish;
		this.edit = edit;
		this.embed = embed;
		this.projectType = projectType;
	}

}
