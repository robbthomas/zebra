package com.alleni.zebra.data.model;

import java.util.Date;

public class AccountMemberRole {
	
	private int accountId;
	private String memberId;
	private int typeId;
	private int retired;
	private String retiredById;
	private Date retiredDate;
	
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public int getRetired() {
		return retired;
	}
	public void setRetired(int retired) {
		this.retired = retired;
	}
	public String getRetiredById() {
		return retiredById;
	}
	public void setRetiredById(String retiredById) {
		this.retiredById = retiredById;
	}
	public Date getRetiredDate() {
		return retiredDate;
	}
	public void setRetiredDate(Date retiredDate) {
		this.retiredDate = retiredDate;
	}

}
