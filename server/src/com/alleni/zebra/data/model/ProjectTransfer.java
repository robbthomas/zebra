package com.alleni.zebra.data.model;

import java.util.Date;

public class ProjectTransfer {
	private Long projectMemberAccessId;
	private Long senderAccountId;
	private String senderMemberId;
	private Long receiverAccountId;
	private String receiverMemberId;
	private Date createdDateTime;
	private String projectId;
	private boolean retired;
	private String retiredById;
	private Date retiredDateTime;
	private int retiredReasonTypeId;
	private boolean newToReceiver;
	private Date viewedDateTime;
	private int licenseTypeId;
	private boolean hideFromReceiver;
	private boolean hideFromSender;

	// -- join fields --
	private String senderDisplayName;
	private String receiverDisplayName;
	private String senderAccountName;
	private String receiverAccountName;
	private String name;
	private int price;
	private String iconUrl;
	private String retiredReasonCode;
	private LicenseType licenseType;
	private String receiverEmail;
	private String senderEmail;
	
	public Long getSenderAccountId() {
		return senderAccountId;
	}
	public void setSenderAccountId(Long senderAccountId) {
		this.senderAccountId = senderAccountId;
	}
	public String getSenderMemberId() {
		return senderMemberId;
	}
	public void setSenderMemberId(String senderMemberId) {
		this.senderMemberId = senderMemberId;
	}
	public Long getReceiverAccountId() {
		return receiverAccountId;
	}
	public void setReceiverAccountId(Long receiverAccountId) {
		this.receiverAccountId = receiverAccountId;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public boolean isRetired() {
		return retired;
	}
	public void setRetired(boolean retired) {
		this.retired = retired;
	}
	public Date getRetiredDateTime() {
		return retiredDateTime;
	}
	public void setRetiredDateTime(Date retiredDateTime) {
		this.retiredDateTime = retiredDateTime;
	}
	public Date getViewedDateTime() {
		return viewedDateTime;
	}
	public void setViewedDateTime(Date viewedDateTime) {
		this.viewedDateTime = viewedDateTime;
	}
	public String getSenderDisplayName() {
		return senderDisplayName;
	}
	public void setSenderDisplayName(String senderDisplayName) {
		this.senderDisplayName = senderDisplayName;
	}
	public String getReceiverDisplayName() {
		return receiverDisplayName;
	}
	public void setReceiverDisplayName(String receiverDisplayName) {
		this.receiverDisplayName = receiverDisplayName;
	}
	public String getSenderAccountName() {
		return senderAccountName;
	}
	public void setSenderAccountName(String senderAccountName) {
		this.senderAccountName = senderAccountName;
	}
	public String getReceiverAccountName() {
		return receiverAccountName;
	}
	public void setReceiverAccountName(String receiverAccountName) {
		this.receiverAccountName = receiverAccountName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getReceiverMemberId() {
		return receiverMemberId;
	}
	public void setReceiverMemberId(String receiverMemberId) {
		this.receiverMemberId = receiverMemberId;
	}
	public String getRetiredById() {
		return retiredById;
	}
	public void setRetiredById(String retiredById) {
		this.retiredById = retiredById;
	}
	public String getRetiredReasonCode() {
		return retiredReasonCode;
	}
	public void setRetiredReasonCode(String retiredReasonCode) {
		this.retiredReasonCode = retiredReasonCode;
	}
	public Long getProjectMemberAccessId() {
		return projectMemberAccessId;
	}
	public void setProjectMemberAccessId(Long projectMemberAccessId) {
		this.projectMemberAccessId = projectMemberAccessId;
	}
	public int getLicenseTypeId() {
		return licenseTypeId;
	}
	public void setLicenseTypeId(int licenseTypeId) {
		this.licenseTypeId = licenseTypeId;
	}
	public LicenseType getLicenseType() {
		return licenseType;
	}
	public void setLicenseType(LicenseType licenseType) {
		this.licenseType = licenseType;
	}
	public int getRetiredReasonTypeId() {
		return retiredReasonTypeId;
	}
	public void setRetiredReasonTypeId(int retiredReasonTypeId) {
		this.retiredReasonTypeId = retiredReasonTypeId;
	}
	public boolean isNew() {
		return newToReceiver;
	}
	public void setNew(boolean newToReceiver) {
		this.newToReceiver = newToReceiver;
	}
	public String getReceiverEmail() {
		return receiverEmail;
	}
	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}
	public String getSenderEmail() {
		return senderEmail;
	}
	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}
	public boolean isNewToReceiver() {
		return newToReceiver;
	}
	public void setNewToReceiver(boolean newToReceiver) {
		this.newToReceiver = newToReceiver;
	}
	public boolean isHideFromReceiver() {
		return hideFromReceiver;
	}
	public void setHideFromReceiver(boolean hideFromReceiver) {
		this.hideFromReceiver = hideFromReceiver;
	}
	public boolean isHideFromSender() {
		return hideFromSender;
	}
	public void setHideFromSender(boolean hideFromSender) {
		this.hideFromSender = hideFromSender;
	}
}
