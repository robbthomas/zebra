package com.alleni.zebra.data.model;

import java.util.Date;

public class TinyProject implements GenericProject {
	private String projectId;
	private String projectName;
	private String publishedName;
	private Date createdDateTime;
	private String description;
	private PublishedGadgetImage icon;
	
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getPublishedName() {
		return publishedName;
	}
	public void setPublishedName(String publishedName) {
		this.publishedName = publishedName;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public PublishedGadgetImage getIcon() {
		return icon;
	}
	public void setIcon(PublishedGadgetImage icon) {
		this.icon = icon;
	}
}
