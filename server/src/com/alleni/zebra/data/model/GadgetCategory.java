package com.alleni.zebra.data.model;

import java.util.Date;

public class GadgetCategory  {

	private Long id;
	private Long parentCategoryId;
	private String name;
	private String description;
	private Long projectTypeId;
	private Integer categoryOrder;
	private Integer categoryLevel;
	private String editedById;
	private Date editedDateTime;
	private boolean retired;
	private String retiredById;
	private Date retiredDateTime;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Long parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getProjectTypeId() {
		return projectTypeId;
	}

	public void setProjectTypeId(Long projectTypeId) {
		this.projectTypeId = projectTypeId;
	}

	public Integer getCategoryOrder() {
		return categoryOrder;
	}

	public void setCategoryOrder(Integer categoryOrder) {
		this.categoryOrder = categoryOrder;
	}

	public Integer getCategoryLevel() {
		return categoryLevel;
	}

	public void setCategoryLevel(Integer categoryLevel) {
		this.categoryLevel = categoryLevel;
	}

	public String getEditedById() {
		return editedById;
	}

	public void setEditedById(String editedById) {
		this.editedById = editedById;
	}

	public Date getEditedDateTime() {
		return editedDateTime;
	}

	public void setEditedDateTime(Date editedDateTime) {
		this.editedDateTime = editedDateTime;
	}

	public boolean isRetired() {
		return retired;
	}

	public void setRetired(boolean retired) {
		this.retired = retired;
	}

	public String getRetiredById() {
		return retiredById;
	}

	public void setRetiredById(String retiredById) {
		this.retiredById = retiredById;
	}

	public Date getRetiredDateTime() {
		return retiredDateTime;
	}

	public void setRetiredDateTime(Date retiredDateTime) {
		this.retiredDateTime = retiredDateTime;
	}

}
