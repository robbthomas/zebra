package com.alleni.zebra.data.model;

import java.util.Date;

public class MyProject implements GenericProject {
	private int totalCount;
    private String projectId;
    private Date createdDateTime;
    private Date editedDateTime;
    private String projectName;
    private int projectTypeId;
    private boolean hideInStoreList;
    private boolean featured;
    private boolean published;
    private boolean republishable;
    private boolean editable;
    private boolean embedable;
    private String authorFirstName;
    private String authorLastName;
    private String authorWebsite;
    private String authorDisplayName;
    private String authorNameFormat;
    private String authorCompanyName;
    private String authorAccountName;
    private int categoryId;
    private String publishedName;
    private String description;
    private String urlName;
    private String authorMemberId;
    private int accountId;
    private int accountTypeId;
    private String projectAccountTypeName;
    private String accountProjectNumberId;
    private String playerId;
    private boolean olderThanLatestVersion;
    private int commentCount;
    private PublishedGadgetImage icon;
    private int numRatings;
    private int totalRating;
    
    public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getEditedDateTime() {
		return editedDateTime;
	}
	public void setEditedDateTime(Date editedDateTime) {
		this.editedDateTime = editedDateTime;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public int getProjectTypeId() {
		return projectTypeId;
	}
	public void setProjectTypeId(int projectTypeId) {
		this.projectTypeId = projectTypeId;
	}
	public boolean isHideInStoreList() {
		return hideInStoreList;
	}
	public void setHideInStoreList(boolean hideInStoreList) {
		this.hideInStoreList = hideInStoreList;
	}
	public boolean isFeatured() {
		return featured;
	}
	public void setFeatured(boolean featured) {
		this.featured = featured;
	}
	public boolean isPublished() {
		return published;
	}
	public void setPublished(boolean published) {
		this.published = published;
	}
	public boolean isRepublishable() {
		return republishable;
	}
	public void setRepublishable(boolean republishable) {
		this.republishable = republishable;
	}
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	public boolean isEmbedable() {
		return embedable;
	}
	public void setEmbedable(boolean embedable) {
		this.embedable = embedable;
	}
	public String getAuthorFirstName() {
		return authorFirstName;
	}
	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}
	public String getAuthorLastName() {
		return authorLastName;
	}
	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}
	public String getAuthorWebsite() {
		return authorWebsite;
	}
	public void setAuthorWebsite(String authorWebsite) {
		this.authorWebsite = authorWebsite;
	}
	public String getAuthorDisplayName() {
		return authorDisplayName;
	}
	public void setAuthorDisplayName(String authorDisplayName) {
		this.authorDisplayName = authorDisplayName;
	}
	public String getAuthorNameFormat() {
		return authorNameFormat;
	}
	public void setAuthorNameFormat(String authorNameFormat) {
		this.authorNameFormat = authorNameFormat;
	}
	public String getAuthorCompanyName() {
		return authorCompanyName;
	}
	public void setAuthorCompanyName(String authorCompanyName) {
		this.authorCompanyName = authorCompanyName;
	}
	public String getAuthorAccountName() {
		return authorAccountName;
	}
	public void setAuthorAccountName(String authorAccountName) {
		this.authorAccountName = authorAccountName;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getPublishedName() {
		return publishedName;
	}
	public void setPublishedName(String publishedName) {
		this.publishedName = publishedName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrlName() {
		return urlName;
	}
	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}
	public String getAuthorMemberId() {
		return authorMemberId;
	}
	public void setAuthorMemberId(String authorMemberId) {
		this.authorMemberId = authorMemberId;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public int getAccountTypeId() {
		return accountTypeId;
	}
	public void setAccountTypeId(int accountTypeId) {
		this.accountTypeId = accountTypeId;
	}
	public String getProjectAccountTypeName() {
		return projectAccountTypeName;
	}
	public void setProjectAccountTypeName(String projectAccountTypeName) {
		this.projectAccountTypeName = projectAccountTypeName;
	}
	public String getAccountProjectNumberId() {
		return accountProjectNumberId;
	}
	public void setAccountProjectNumberId(String accountProjectNumberId) {
		this.accountProjectNumberId = accountProjectNumberId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public boolean isOlderThanLatestVersion() {
		return olderThanLatestVersion;
	}
	public void setOlderThanLatestVersion(boolean olderThanLatestVersion) {
		this.olderThanLatestVersion = olderThanLatestVersion;
	}
	public int getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	public PublishedGadgetImage getIcon() {
		return icon;
	}
	public void setIcon(PublishedGadgetImage icon) {
		this.icon = icon;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public int getRatingTotal() {
		return totalRating;
	}
	public void setTotalRating(int totalRating) {
		this.totalRating = totalRating;
	}
}