package com.alleni.zebra.data.model;

import java.util.Date;
import java.util.List;

import com.alleni.zebra.gadget.ProjectType;

/**
 * Represents the new (as of 2/21/12) table, resulting from the merge of
 * Published and Parent tables.
 *
 */
public class Project implements GenericProject {
	/**
	 * Set to true when project is being saved; unset when asynyc parts of save complete.
	 */
	public boolean asyncInProgress;
	
	/**
	 * Database primary key
	 */
	private String projectId;
	
	/**
	 * optimistic locking version marker
	 */
	private int version;
	
	/**
	 * player/editor content data
	 */
	private String content;
	
	/**
	 * Time this record was created.
	 */
	private Date createdDateTime;
	
	/**
	 * Project is deleted
	 */
	private boolean deleted;
	
	/**
	 * player/editor startup values
	 */
	private String initialValues;
	
	@Deprecated
	private Date editedDateTime;
	
	/**
	 * "free space" to be used by the editor/player
	 */
	private String projectMetadata;
	
	/**
	 * Name used for the editor, independent of the shoppe name.
	 */
	private String projectName;
	
	/**
	 * Pointer forwards in linked list.
	 */
	private String nextVersionId;
	
	/**
	 * app_user.id of the user who created this row.
	 */
	private String authorMemberId;
	
	/**
	 * AccountId of the author at the time this record was created.
	 */
	private long accountId;
	
	private int accountTypeId;
	
	/**
	 * Populated by mapping from projectTypeId
	 */
	private ProjectType projectType;
	private int projectTypeId;
	
	/**
	 * Size in bytes of <code>content</code> + <code>initialValues</code> + <code>projectMetadata</code>
	 */
	private int gadgetSize;
	
	/**
	 * Whether project was saved by the auto-save mechanism.
	 */
	private boolean autoSave;
	
	/**
	 * True when project has been published, privately or otherwise.
	 */
	private boolean published;
	
	/**
	 * Metadata about publishing.
	 */
	private String publishedMetadata;
	
	/**
	 * Optional publishing version info.
	 */
	private Integer versionMajor;
	
	/**
	 * Optional publishing version info.
	 */
	private int commentCount;
	
	/**
	 * Optional publishing version info.
	 */
	private Integer versionMinor;
	
	/**
	 * Optional publishing version info.
	 */
	private Integer versionDot;
	
	/**
	 * Optional publishing version info.
	 */
	private Integer versionBuildNumber;

	/**
	 * FK into Categories table
	 */
	private int categoryId;
	private GadgetCategory category;
	
	/**
	 * Name under which this entry is published.
	 */
	private String publishedName;
	
	/**
	 * Description to be used in Shoppe or other publishing contexts.
	 */
	private String description;
	
	/**
	 * Similar to nextVersionId, but for published projects.
	 */
	private String nextPublishedVersionId;
	
	/**
	 * conveniently set by the DB when nextPublishedVersion <> null
	 */
	private boolean updateAvailable;
	
	/**
	 * When creating a Project record, as the server to do so
	 * with this ID.
	 */
	private String requestedNewProjectId;
	
	/**
	 * FK into publishstatuses table
	 */
	private Integer statusId;
	private PublishStatus publishStatus;
	
	private Integer licenseTypeId;
	
	private int width;
	private int height;
	
	/** Joined from LicenseTypes */
	private boolean republishable;
	/** Joined from LicenseTypes */
	private boolean editable;
	/** Joined from LicenseTypes */
	private boolean embedable;
	
	private String apnUUID;
	private String playerId;
	
	private boolean owned;
	private String urlName;
	
	//@Transient
	private String oldName;
	
	/**
	 * List of sub-projects, aka "gadgets" included in this project. 
	 */
	private List<Project> children;
	private List<String> childrenIds;

    private List<Asset> assets;
    private List<String> assetIds;
	
	//   Join Fields, not from Projects table
	
	private int cachedPurchaseAcquisitionCount;
	private int ratingTotal;
	private int numRatings;
	
	private String authorFirstName;
	private String authorLastName;
	private String authorWebsite;
	private String authorDisplayName;
	private String authorNameFormat;
	private String authorCompanyName;
	private String authorAccountName;

    private GadgetState state;
	
	private List<PublishedGadgetImage> screenshots;
	private PublishedGadgetImage icon;
    private Asset previewImage;

	private List<String> shareTypes;
	
	// calculated fields
	private int minPrice;
	private boolean currentUserCanShare;
	private boolean canRead;
	private boolean viewMetadata;
	private boolean isPublisher;
	private int totalCount;
	
	// transient, only used when calculating licensetypeid
	// inbound only.
	private LicenseType licenseType;
	
	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getInitialValues() {
		return initialValues;
	}

	public void setInitialValues(String initialValues) {
		this.initialValues = initialValues;
	}

	public Date getEditedDateTime() {
		return editedDateTime;
	}

	public void setEditedDateTime(Date editedDateTime) {
		this.editedDateTime = editedDateTime;
	}

	public String getProjectMetadata() {
		return projectMetadata;
	}

	public void setProjectMetadata(String projectMetadata) {
		this.projectMetadata = projectMetadata;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getNextVersionId() {
		return nextVersionId;
	}

	public void setNextVersionId(String nextVersionId) {
		this.nextVersionId = nextVersionId;
	}

	public String getAuthorMemberId() {
		return authorMemberId;
	}

	public void setAuthorMemberId(String authorMemberId) {
		this.authorMemberId = authorMemberId;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public ProjectType getProjectType() {
		return projectType;
	}

	public void setProjectType(ProjectType projectType) {
		this.projectType = projectType;
	}

	public int getGadgetSize() {
		return gadgetSize;
	}

	public void setGadgetSize(int gadgetSize) {
		this.gadgetSize = gadgetSize;
	}

	public boolean isAutoSave() {
		return autoSave;
	}

	public void setAutoSave(boolean autoSave) {
		this.autoSave = autoSave;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public String getPublishedMetadata() {
		return publishedMetadata;
	}

	public void setPublishedMetadata(String publishedMetadata) {
		this.publishedMetadata = publishedMetadata;
	}

	public Integer getVersionMajor() {
		return versionMajor;
	}

	public void setVersionMajor(Integer versionMajor) {
		this.versionMajor = versionMajor;
	}

	public Integer getVersionMinor() {
		return versionMinor;
	}

	public void setVersionMinor(Integer versionMinor) {
		this.versionMinor = versionMinor;
	}

	public Integer getVersionDot() {
		return versionDot;
	}

	public void setVersionDot(Integer versionDot) {
		this.versionDot = versionDot;
	}

	public Integer getVersionBuildNumber() {
		return versionBuildNumber;
	}

	public void setVersionBuildNumber(Integer versionBuildNumber) {
		this.versionBuildNumber = versionBuildNumber;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public GadgetCategory getCategory() {
		return category;
	}

	public void setCategory(GadgetCategory category) {
		this.category = category;
	}

	public String getPublishedName() {
		return publishedName;
	}

	public void setPublishedName(String publishedName) {
		this.publishedName = publishedName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public PublishStatus getPublishStatus() {
		return publishStatus;
	}

	public void setPublishStatus(PublishStatus publishStatus) {
		this.publishStatus = publishStatus;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getSelfPrice() {
		return selfPrice;
	}

	public void setSelfPrice(int selfPrice) {
		this.selfPrice = selfPrice;
	}

	public int getCurrencyTypeId() {
		return currencyTypeId;
	}

	public void setCurrencyTypeId(int currencyTypeId) {
		this.currencyTypeId = currencyTypeId;
	}

	public CurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(CurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	public boolean isHideInStoreList() {
		return hideInStoreList;
	}

	public void setHideInStoreList(boolean hideInStoreList) {
		this.hideInStoreList = hideInStoreList;
	}

	public boolean isFeatured() {
		return featured;
	}

	public void setFeatured(boolean featured) {
		this.featured = featured;
	}

	public boolean isInviteOnly() {
		return inviteOnly;
	}

	public void setInviteOnly(boolean inviteOnly) {
		this.inviteOnly = inviteOnly;
	}

	public Long getGuestListId() {
		return guestListId;
	}

	public void setGuestListId(Long guestListId) {
		this.guestListId = guestListId;
	}

	/**
	 * price in pennies
	 */
	private int price;

	/**
	 * Shoppe price - royalties price.
	 */
	private int selfPrice;
	
	/**
	 * FK into CurrencyTypes table
	 */
	private int currencyTypeId;
	private CurrencyType currencyType;
	
	/**
	 * A component of private publishing.  Default is <code>true</code>
	 */
	private boolean hideInStoreList = true;
	
	/**
	 * Whether this published project shows up in the featured zapps/gadgets list.
	 */
	private boolean featured;
	
	/**
	 * A component of private publishing: only allow invited users to view the details page.
	 * Default is <code>true</code>
	 */
	private boolean inviteOnly = true;
	
	/**
	 * FK to the list of users invited to view this zapp/gadget
	 * see Project.inviteOnly
	 */
	private Long guestListId;

	public int getCachedPurchaseAcquisitionCount() {
		return cachedPurchaseAcquisitionCount;
	}

	public void setCachedPurchaseAcquisitionCount(
			int cachedPurchaseAcquisitionCount) {
		this.cachedPurchaseAcquisitionCount = cachedPurchaseAcquisitionCount;
	}

	public long getRatingTotal() {
		return ratingTotal;
	}

	public void setRatingTotal(int ratingTotal) {
		this.ratingTotal = ratingTotal;
	}

	public long getNumRatings() {
		return numRatings;
	}

	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}

	public List<Project> getChildren() {
		return children;
	}

	public void setChildren(List<Project> children) {
		this.children = children;
	}

	public boolean isRepublishable() {
		return republishable;
	}

	public void setRepublishable(boolean republishable) {
		this.republishable = republishable;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean isEmbedable() {
		return embedable;
	}

	public void setEmbedable(boolean embedable) {
		this.embedable = embedable;
	}

	public Integer getLicenseTypeId() {
		return licenseTypeId;
	}

	public void setLicenseTypeId(Integer licenseTypeId) {
		this.licenseTypeId = licenseTypeId;
	}

	public String getApnUUID() {
		return apnUUID;
	}

	public void setApnUUID(String apnUUID) {
		this.apnUUID = apnUUID;
	}

	public int getProjectTypeId() {
		return projectTypeId;
	}

	public void setProjectTypeId(int projectTypeId) {
		this.projectTypeId = projectTypeId;
	}

	public List<String> getChildrenIds() {
		return childrenIds;
	}

	public void setChildrenIds(List<String> childrenIds) {
		this.childrenIds = childrenIds;
	}

	public boolean isOwned() {
		return owned;
	}

	public void setOwned(boolean owned) {
		this.owned = owned;
	}

	public String getAuthorFirstName() {
		return authorFirstName;
	}

	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}

	public String getAuthorLastName() {
		return authorLastName;
	}

	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}

	public String getAuthorWebsite() {
		return authorWebsite;
	}

	public void setAuthorWebsite(String authorWebsite) {
		this.authorWebsite = authorWebsite;
	}

	public String getAuthorDisplayName() {
		return authorDisplayName;
	}

	public void setAuthorDisplayName(String authorDisplayName) {
		this.authorDisplayName = authorDisplayName;
	}

	public String getAuthorNameFormat() {
		return authorNameFormat;
	}

	public void setAuthorNameFormat(String authorNameFormat) {
		this.authorNameFormat = authorNameFormat;
	}

	public String getAuthorCompanyName() {
		return authorCompanyName;
	}

	public void setAuthorCompanyName(String authorCompanyName) {
		this.authorCompanyName = authorCompanyName;
	}

	public String getAuthorAccountName() {
		return authorAccountName;
	}

	public void setAuthorAccountName(String authorAccountName) {
		this.authorAccountName = authorAccountName;
	}

	public int getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}

	public boolean isCurrentUserCanShare() {
		return currentUserCanShare;
	}

	public void setCurrentUserCanShare(boolean currentUserCanShare) {
		this.currentUserCanShare = currentUserCanShare;
	}

	public boolean isCanRead() {
		return canRead;
	}

	public void setCanRead(boolean canRead) {
		this.canRead = canRead;
	}

    public GadgetState getState() {
        return state;
    }

    public void setState(GadgetState state) {
        this.state = state;
    }

	public List<PublishedGadgetImage> getScreenshots() {
		return screenshots;
	}

	public void setScreenshots(List<PublishedGadgetImage> screenshots) {
		this.screenshots = screenshots;
	}

	public PublishedGadgetImage getIcon() {
		return icon;
	}

	public void setIcon(PublishedGadgetImage icon) {
		this.icon = icon;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((projectId == null) ? 0 : projectId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (projectId == null) {
			if (other.projectId != null)
				return false;
		} else if (!projectId.equals(other.projectId))
			return false;
		return true;
	}

	public List<String> getShareTypes() {
		return shareTypes;
	}

	public void setShareTypes(List<String> shareTypes) {
		this.shareTypes = shareTypes;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isViewMetadata() {
		return viewMetadata;
	}

	public void setViewMetadata(boolean viewMetadata) {
		this.viewMetadata = viewMetadata;
	}

	public boolean isPublisher() {
		return isPublisher;
	}

	public void setPublisher(boolean isPublisher) {
		this.isPublisher = isPublisher;
	}

	public String getNextPublishedVersionId() {
		return nextPublishedVersionId;
	}

	public void setNextPublishedVersionId(String nextPublishedVersionId) {
		this.nextPublishedVersionId = nextPublishedVersionId;
	}

	public String getRequestedNewProjectId() {
		return requestedNewProjectId;
	}

	public void setRequestedNewProjectId(String requestedNewProjectId) {
		this.requestedNewProjectId = requestedNewProjectId;
	}

	public int getAccountTypeId() {
		return accountTypeId;
	}

	public void setAccountTypeId(int accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	public String getUrlName() {
		return urlName;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}

    public Asset getPreviewImage() {
        return previewImage;
    }

    public void setPreviewImage(Asset previewImage) {
        this.previewImage = previewImage;
    }

	public boolean isUpdateAvailable() {
		return updateAvailable;
	}

	public void setUpdateAvailable(boolean updateAvailable) {
		this.updateAvailable = updateAvailable;
	}

    public List<String> getAssetIds() {
        return assetIds;
    }

    public void setAssetIds(List<String> assetIds) {
        this.assetIds = assetIds;
    }

    public List<Asset> getAssets() {
        return assets;
    }

    public void setAssets(List<Asset> assets) {
        this.assets = assets;
    }

    @Deprecated
	public LicenseType getLicenseType() {
		return licenseType;
	}

    @Deprecated
	public void setLicenseType(LicenseType licenseType) {
		this.licenseType = licenseType;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getOldName() {
		return oldName;
	}

	public void setOldName(String oldName) {
		this.oldName = oldName;
	}

	public int getCommentCount() {
		return commentCount;
	}
	
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	
	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public boolean isAsyncInProgress() {
		return asyncInProgress;
	}

	public void setAsyncInProgress(boolean asyncInProgress) {
		this.asyncInProgress = asyncInProgress;
	}

}
