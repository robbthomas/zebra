package com.alleni.zebra.data.model;

import java.util.HashMap;
import java.util.Map;

import com.alleni.zebra.icon.worker.ImageWorker;

public class PublishedGadgetImage implements Comparable<PublishedGadgetImage> {
	private long id;
	private String projectId;
	private String iconFileId;
	private String screenshotFileId;
	private String label;
	private String caption;
	private int displayOrder;
	private boolean renderingComplete;
	private boolean retired;
	
	@Override
	public int compareTo(PublishedGadgetImage o) {
		return Double.compare(displayOrder, o.getDisplayOrder());
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + displayOrder;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PublishedGadgetImage other = (PublishedGadgetImage) obj;
		if (displayOrder != other.displayOrder)
			return false;
		return true;
	}


	public String getIconFileId() {
		return iconFileId;
	}
	public void setIconFileId(String iconId) {
		this.iconFileId = iconId;
	}
	public String getScreenshotFileId() {
		return screenshotFileId;
	}
	public void setScreenshotFileId(String screenshotId) {
		this.screenshotFileId = screenshotId;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public int getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
//	public int getSize() {
//		return size;
//	}
//	public void setSize(int size) {
//		this.size = size;
//	}
	public boolean isRenderingComplete() {
		return renderingComplete;
	}
	public void setRenderingComplete(boolean renderingComplete) {
		this.renderingComplete = renderingComplete;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public boolean isRetired() {
		return retired;
	}
	public void setRetired(boolean retired) {
		this.retired = retired;
	}
	
	public Map<String, Object> getScreenshotMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("iconFileId", getIconFileId());
		map.put("screenshotFileId", getScreenshotFileId());
		map.put("publishedImageId", getId());
		map.put("projectId", getProjectId());
		map.put("baseUrl", "https://s3.amazonaws.com/com.alleni.zebra.site.content/screenshots/" + getScreenshotFileId() + "-");
		map.put("fileNameExt", ".png");
		map.put("label", getLabel());
		map.put("caption", getCaption());
		map.put("displayOrder", getDisplayOrder());
		map.put("renderingComplete", isRenderingComplete());
		map.put("sizes", ImageWorker.IconTarget.PUBLISHED_SCREENSHOT.getSizes());
		return map;
	}
	
	public Map<String, Object> getIconMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("publishedImageId", getId());
		map.put("iconFileId", getIconFileId());
		map.put("screenshotFileId", getScreenshotFileId());
		map.put("projectId", getProjectId());
		map.put("baseUrl", "https://s3.amazonaws.com/com.alleni.zebra.site.content/icons/" + getIconFileId() + "-");
		map.put("fileNameExt", ".png");
		map.put("label", getLabel());
		map.put("caption", getCaption());
		map.put("displayOrder", getDisplayOrder());
		map.put("renderingComplete", isRenderingComplete());
		map.put("sizes", ImageWorker.IconTarget.PUBLISHED_ICON.getSizes());
		return map;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
}
