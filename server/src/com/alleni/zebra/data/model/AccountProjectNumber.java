package com.alleni.zebra.data.model;

import java.util.Date;

/**
 * model for the AccountProjectNumbers table 
 *
 */
public class AccountProjectNumber {
	private String uuid;
	private Long accountId;
	private int retired;
	private Long invoiceLineItemId;
	private Date retiredDateTime;
	private Long retiredReasonId;
	private String projectId;
	private Date createdDateTime;
	
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public int getRetired() {
		return retired;
	}
	public void setRetired(int retired) {
		this.retired = retired;
	}
	public Long getInvoiceLineItemId() {
		return invoiceLineItemId;
	}
	public void setInvoiceLineItemId(Long invoiceLineItemId) {
		this.invoiceLineItemId = invoiceLineItemId;
	}
	public Date getRetiredDateTime() {
		return retiredDateTime;
	}
	public void setRetiredDateTime(Date retiredDateTime) {
		this.retiredDateTime = retiredDateTime;
	}
	public Long getRetiredReasonId() {
		return retiredReasonId;
	}
	public void setRetiredReasonId(Long retiredReasonId) {
		this.retiredReasonId = retiredReasonId;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
}
