package com.alleni.zebra.data.model;

import java.util.Date;

public class ProductCode {
	
	public enum ProductType {
		COM,
		GOV,
		EDU;
	}
	
	private int productCodeId;
	private int productCategoryId;
	private int refundDays;
	private Date termDate;
	private Date effDate;
	private int price;
	private int productTypeId;
	private String discountType;
	private String tag;
	private String description;
	
	// join fields
	private String productCategoryTag;
	private String productTypeTag;
	
	
	public String getProductCategoryTag() {
		return productCategoryTag;
	}
	public void setProductCategoryTag(String productCategoryTag) {
		this.productCategoryTag = productCategoryTag;
	}
	public String getProductTypeTag() {
		return productTypeTag;
	}
	public void setProductTypeTag(String productTypeTag) {
		this.productTypeTag = productTypeTag;
	}
	public int getProductCodeId() {
		return productCodeId;
	}
	public void setProductCodeId(int productCodeId) {
		this.productCodeId = productCodeId;
	}
	public int getProductCategoryId() {
		return productCategoryId;
	}
	public void setProductCategoryId(int productCategoryId) {
		this.productCategoryId = productCategoryId;
	}
	public int getRefundDays() {
		return refundDays;
	}
	public void setRefundDays(int refundDays) {
		this.refundDays = refundDays;
	}
	public Date getTermDate() {
		return termDate;
	}
	public void setTermDate(Date termDate) {
		this.termDate = termDate;
	}
	public Date getEffDate() {
		return effDate;
	}
	public void setEffDate(Date effDate) {
		this.effDate = effDate;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getProductTypeId() {
		return productTypeId;
	}
	public void setProductTypeId(int productTypeId) {
		this.productTypeId = productTypeId;
	}
	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
