package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.dto.MiniProjectListSearchResultSet;
import com.alleni.zebra.data.dto.MyProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectQuery;
import com.alleni.zebra.data.dto.TinyProjectListSearchResultSet;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.gadget.ProjectType;

public interface ProjectDao {
	public static final String TABLE_NAME = "projects";
	
	/**
	 * Columns that we want Spring to always provide values for.
	 * Columns that we don't want to populate are commented out.
	 */
	public static final String[] COLUMNS = new String[] {
		"projectid",
		"version",
		"content",
//		"createddatetime",
		"retired",
		"initialvalues",
//		"editeddatetime",
		"projectmetadata",
		"projectname",
		"nextversionid",
		"authormemberid",
		"initialvalue",
		"projecttypeid",
		"gadgetsize",
		"autosave",
		"published",
		"accountid",
		"versionmajor",
		"versionminor",
		"versiondot",
		"versionbuildnumber",
		"categoryid",
		"description",
		"statusid",
		"licensetypeid",
		"price",
		"currencytypeid",
		"hideinstorelist",
		"featured",
		"publishedmetadata",
		"selfprice",
		"inviteonly",
		"guestlistid",
		"width",
		"height",
		"nextpublishedversionid",
		"accounttypeid",
		"urlname",
		"publishedname",
		"asyncinprogress"
	};
	
	
	/**
	 * Search for a Project, by standard search fields.
	 * 
	 * @param com.alleni.zebra.data.dto.ProjectQuery query.
	 * @return a <code>List&lt;Project&gt;</code>, which may be empty.
	 */
	public ProjectListSearchResultSet search(ProjectQuery q, AppUser searcher);
	
	
	/**
	 * Simple finder by DB ID.
	 * 
	 * @param projectId
	 * @return Project, if found.
	 * @throws NotFoundException
	 * @throws DataAccessException 
	 */
	public Project findById(String projectId, AppUser searcher) throws NotFoundException, DataAccessException;
	
	/**
	 * Hopefully this will return a list with only one entry: the newest in this lineage.
	 * If the linked list of Projects has forked for this lineage, however, both heads will be returned.
	 * 
	 * @param p
	 * @return
	 * @throws DataAccessException
	 */
	public List<Project> findHeadVersions(Project p) throws DataAccessException;
	
	
	/**
	 * Find the record (if any) corresponding to the AccountProjectNumbers row UUID passed. 
	 * @param pppUUID
	 * @return Project
	 * @throws NotFoundException
	 */
	public Project findForAccountProjectNumbersUUID(String apnUUID, AppUser user) throws DataAccessException;
	
	/**
	 * Find a published project by name and type.  Relatively specialized method, it's used
	 * in the "publish over" flow.
	 * 
	 * @param name
	 * @param type
	 * @return Project
	 * @throws NotFoundException
	 */
	public Project findPublishedByNameAndtype(String name, ProjectType type) throws NotFoundException;
	
	/**
	 * Create a new instance of Project in the database.
	 * 
	 * @param project instance.
	 * @return the same instance passed, with database id populated.
	 * @throws UniqueConstraintException 
	 */
	public Project create(Project project) throws UniqueConstraintException;
	
	
	/**
	 * Update a row in the Projects table, if it exists (keyed on projectId).
	 * 
	 * @param project containing id to identify and new values to update.
	 * @return <code>void</code>
	 * @throws NotFoundException
	 * @throws DataAccessException 
	 */
	public void update(Project project) throws NotFoundException, DataAccessException;

	
	/**
	 * Update a row's Featured value in the Projects table, if it exists (keyed on projectId).
	 * 
	 * @param project containing id to identify and new values to update.
	 * @return <code>void</code>
	 * @throws NotFoundException
	 * @throws DataAccessException 
	 */
	public void updateFeatured(Project project) throws NotFoundException, DataAccessException;
	
	/**
	 * Physically delete a row from the database.
	 * 
	 * @param project
	 * @return <code>void</code>
	 * @throws NotFoundException
	 */
	public void retireAllVersions(AppUser user, Project project) throws NotFoundException;
	
	
	/**
	 * Project names must be unique per user, and the published name must be 
	 * globally unique by project type.
	 * 
	 * @param type
	 * @param isPublished
	 * @return true if the name passes the uniqueness check appropriate for the other
	 *         parameters.
	 */
	public boolean nameUnique(String name, ProjectType type, boolean isPublished, AppUser user);
	
	
	/**
	 * Calculate the sum of all the self-prices of all published Projects contained by this
	 * project.
	 * 
	 * @param projectId
	 * @return minimum price in pennies, which may be 0.
	 * 
	 * @throws DataAccessException
	 */
	public int calcRoyalties(String projectId) throws DataAccessException;
	
	
	
	/**
	 * Returns a (potentially empty) list of Projects referenced by the Project passed in.
	 *  
	 * <pre>
	 *     project                     parent_child
	 *     ------                      ------------
	 *     id    <--------+----------* parent_children_id
	 *     ...            |
	 *                    +----------- child_id (goofy name generated by Grails)
	 *                    
	 *  </pre>
	 *  
	 *  select p.* from project p, parent_child pc where p.id = pc.child_id and parent_children_id = (root.getIid)   
	 *     
	 * @param project
	 * @return
	 * @throws NotFoundException
	 */
	public List<Project> getChildrenFor(Project project, AppUser user) throws NotFoundException;
	
	
	
	/**
	 * Add a child project (technically, a "gadget"), to a project, via the parent_child table.
	 * 
	 *   parent_children_id = child
	 *   child_id = parent
	 * 
	 * @see:  &{@link #getChildrenFor(Project)} getChildrenFor(Project), above.
	 * 
	 * @param parent project
	 * @param childProjectId
	 * @throws NotFoundException
	 * @throws DataAccessException 
	 */
	public void addToChildren(Project parent, String childProjectId, AppUser author) throws NotFoundException, DataAccessException;
	

	/**
	 * Change the project name (not the published name) of all versions of this <code>Project</code>
	 * in the linked list.
	 * 
	 * WARNING: This will affect the results of &{@link #findAllVersions(String)}, as that searches by name.
	 * 
	 * @param appUserId
	 * @param projectId
	 * @param newName
	 * @throws DataAccessException
	 */
	public void renameAllProjectVersions(String appUserId, String projectId, String newName) throws DataAccessException;
	
	
	/**
	 * Find the project with the same name, type, and ownerMemberId as the one passed in.
	 * 
	 * @param user
	 * @param p
	 * @return
	 * @throws NotFoundException
	 * @throws DataAccessException 
	 */
	public boolean projectNameUnique(AppUser receiver, String name);
	
	public boolean projectNameUnretiredUnique(AppUser receiver, String name); 


	public MiniProjectListSearchResultSet searchMiniStuff(ProjectQuery q,
			AppUser searcher) throws DataAccessException;
	
	public MyProjectListSearchResultSet searchMyStuff(ProjectQuery q,
			AppUser searcher) throws DataAccessException;
	
	public TinyProjectListSearchResultSet searchTinyStuff(ProjectQuery q,
			AppUser searcher) throws DataAccessException;
	
	
	public int getProjectType(String projectId) throws NotFoundException;
	
	public int setNextVersionIdTo(Project p);
	public int setNextPublishedVersionIdTo(Project p);

}
