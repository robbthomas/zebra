package com.alleni.zebra.data.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.alleni.zebra.data.dao.PublishedGadgetImageDao;
import com.alleni.zebra.data.dao.sequence.ContextAwareSequenceDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.mapper.PublishedGadgetImageMapper;
import com.alleni.zebra.data.model.PublishedGadgetImage;

@Repository("publishedGadgetImageDao")
public class PublishedGadgetImageDaoImpl implements PublishedGadgetImageDao {
	Logger log = Logger.getLogger(PublishedGadgetImageDaoImpl.class);

	@Autowired TransactionTemplate transactionTemplate;
	@Autowired JdbcTemplate template;
	@Autowired PublishedGadgetImageMapper mapper;
	@Autowired ContextAwareSequenceDao sequence;

	private SimpleJdbcInsert insert;

	@PostConstruct
	public void init() {
		this.insert = new SimpleJdbcInsert(template).withTableName(TABLE_NAME).usingGeneratedKeyColumns("publishimageid");
	}



	@Override
	@Transactional(readOnly = true)
	public PublishedGadgetImage findById(Long publishImageId)
			throws NotFoundException {
		log.debug("fetching image metadata:  " + publishImageId);
		String sql = "select * from " + TABLE_NAME + " where publishimageid = ? and retired = false";

		PublishedGadgetImage pgi = null;
		try {
			pgi = template.queryForObject(sql, new Object[] {publishImageId}, mapper);
		} catch (EmptyResultDataAccessException ex) {
			throw new NotFoundException(publishImageId);
		}
		return pgi;
	}


	@Override
	@Transactional(readOnly = true)
	public PublishedGadgetImage getIconForProject(String projectId) throws DataAccessException {
		List<PublishedGadgetImage> list = Collections.emptyList();
		String sql = "select pgi.* from " + TABLE_NAME + " pgi where pgi.projectId = ? and pgi.iconFileId is not null and retired = false";
		log.debug("sql:  " + sql + ", " + projectId);

		list = template.query(sql, new Object[] {projectId}, mapper);
		if (list.size() == 0) throw new NotFoundException(projectId);

		return list.get(0);
	}

	@Override
	@Transactional(readOnly = true)
	public List<PublishedGadgetImage> getScreenshotsForProject(String projectId) {
		List<PublishedGadgetImage> list = Collections.emptyList();
		String sql = "select pgi.* from " + TABLE_NAME + " pgi where pgi.projectId = ? and pgi.screenshotFileId is not null and retired = false";
		log.debug("sql:  " + sql + ", " + projectId);

		try {
			list = template.query(sql, new Object[] {projectId}, mapper);
		} catch (EmptyResultDataAccessException ignore) {
			// do nothing; return empty list
		}

		return list;
	}

	public PublishedGadgetImage create(final PublishedGadgetImage image) {
		return transactionTemplate.execute(new TransactionCallback<PublishedGadgetImage>() {

            // the code in this method executes in a transactional context
            public PublishedGadgetImage doInTransaction(TransactionStatus status) {
        		Map<String, Object> params = new HashMap<String,Object>();
        		params.put("projectId", image.getProjectId());
        		params.put("caption", image.getCaption());
        		params.put("screenshotFileId", image.getScreenshotFileId());
        		params.put("iconFileId", image.getIconFileId());
        		params.put("renderingComplete", image.isRenderingComplete());
        		params.put("displayOrder", image.getDisplayOrder());
        		params.put("label", image.getLabel());
        		params.put("retired", image.isRetired());
        		Number key = insert.executeAndReturnKey(params);
        		image.setId(key.longValue());
        		return image;
            }
        });
	}

	public PublishedGadgetImage update(final PublishedGadgetImage image) throws DataAccessException {
		return transactionTemplate.execute(new TransactionCallback<PublishedGadgetImage>() {

            public PublishedGadgetImage doInTransaction(TransactionStatus status) {
        		String sql = "update " + TABLE_NAME + " set projectId = ?, " +
        				" caption = ?, screenshotFileId = ?, iconFileId = ?," +
        				" renderingComplete = ?, displayOrder = ?, retired = ?, label = ?" +
        				" where publishImageId = ?";
        		//		int rows = 0;
        		try {
        			log.debug("updating row: " + image.getId());
        			template.update(sql, image.getProjectId(), image.getCaption(),
        					image.getScreenshotFileId(), image.getIconFileId(),
        					image.isRenderingComplete(), image.getDisplayOrder(), image.isRetired(), image.getLabel(),
        					image.getId());
        		} catch (Exception e) {
        			e.printStackTrace();
        			throw new DataAccessException(image.getId());
        		}
        		//		if (rows == 0) throw new NotFoundException(image.getId());

        		return image;
            }
        });
	}

	public void delete(final long pgImageId) throws DataAccessException {
		transactionTemplate.execute(new TransactionCallback<Object>() {

            // the code in this method executes in a transactional context
            public Object doInTransaction(TransactionStatus status) {
        		String sql = "update " + TABLE_NAME + " set retired = true where publishImageId = ?";

        		int rows = 0;
        		try {
        			rows = template.update(sql,pgImageId);
        		} catch (Exception e) {
        			throw new DataAccessException(pgImageId);
        		}
        		if (rows == 0) throw new NotFoundException(pgImageId);
        		return null;
            }
        });
	}

	public void deleteAllIconsForPublishedGadget(final String projectId) throws DataAccessException {
		transactionTemplate.execute(new TransactionCallback<Object>() {

            // the code in this method executes in a transactional context
            public Object doInTransaction(TransactionStatus status) {
        		String sql = "update " + TABLE_NAME + " set retired = true where iconFileId is not null and projectId = ? and retired = false";

        		try {
        			template.update(sql,projectId);
        		} catch (Exception e) {
        			throw new DataAccessException(projectId);
        		}
        		return null;
            }
        });
	}

	public void deleteAllScreenshotsForPublishedGadget(final String projectId) throws DataAccessException {
		transactionTemplate.execute(new TransactionCallback<Object>() {

            // the code in this method executes in a transactional context
            public Object doInTransaction(TransactionStatus status) {
        		String sql = "update " + TABLE_NAME + " set retired = true where screenshotFileId is not null and projectId = ?";

        		try {
        			template.update(sql,projectId);
        		} catch (Exception e) {
        			throw new DataAccessException(projectId);
        		}
        		return null;
            }
        });
	}



	@Override
	@Transactional(readOnly=true)
	public List<PublishedGadgetImage> findIconsByPublishedGadgetAndIndex(
			Long publishedGadgetId, int displayOrder) throws DataAccessException {
		List<PublishedGadgetImage> ret = Collections.emptyList();

		String sql = "select * from " + TABLE_NAME + " where publishId = ? and displayOrder = ? and iconFileId is not null and retired = false";
		try {
			ret = template.query(sql, new Object[] {publishedGadgetId, displayOrder}, mapper);
		} catch (EmptyResultDataAccessException ignore) {
			// ignore and return empty list
		} catch (Exception e) {
			log.error(e);
			throw new DataAccessException("error");
		}


		return ret;
	}


	@Override
	@Transactional(readOnly=true)
	public List<PublishedGadgetImage> findScreenshotsByProjectIdAndIndex(
			String projectId, int displayOrder) throws DataAccessException {
		List<PublishedGadgetImage> ret = Collections.emptyList();

		String sql = "select * from " + TABLE_NAME + " where projectId = ? and displayOrder = ? and screenShotFileId is not null and retired = false";
		try {
			ret = template.query(sql, new Object[] {projectId, displayOrder}, mapper);
		} catch (EmptyResultDataAccessException ignore) {
			// ignore and return empty list
		} catch (Exception e) {
			log.error(e);
			throw new DataAccessException("error");
		}


		return ret;
	}



	@Override
	@Transactional(readOnly=false)
	public void copyIconAndScreenshots(String fromId, String toId) throws DataAccessException {
		// delete the icon for the new published thing if it exists
		deleteAllIconsForPublishedGadget(toId);

		PublishedGadgetImage icon = getIconForProject(fromId);

		// (we could do this with a cursor, but this code is called in an edge-case where
		// performance is hopefully not an issue

		icon.setProjectId(toId);
		icon.setId(0);
		create(icon);


		List<PublishedGadgetImage> screenShots = getScreenshotsForProject(fromId);
		for (PublishedGadgetImage screenShot : screenShots) {
			screenShot.setProjectId(toId);
			screenShot.setId(0);
			create(screenShot);
		}
	}



	@Override
	@Transactional(readOnly=false)
	public void copyIcon(String iconFromProjectId, String toProjectId) throws DataAccessException {
		try {
			// delete the icon for the new published thing if it exists
			deleteAllIconsForPublishedGadget(toProjectId);

			PublishedGadgetImage icon = getIconForProject(iconFromProjectId);

			// (we could do this with a cursor, but this code is called in an edge-case where
			// performance is hopefully not an issue

			icon.setProjectId(toProjectId);
			icon.setId(0);
			create(icon);
		} catch (NotFoundException safeToIgnore) {
			// do nothing
		}
	}



	@Override
	@Transactional(readOnly=false)
	public void copyScreenshots(String screenshotsFromId, String newProjectId) {

		List<PublishedGadgetImage> screenShots = getScreenshotsForProject(screenshotsFromId);
		for (PublishedGadgetImage screenShot : screenShots) {
			screenShot.setProjectId(newProjectId);
			screenShot.setId(0);
			create(screenShot);
		}
	}



	@Override
	@Transactional(readOnly = false)
	public void copyIconByIdForNewProject(Long previousIconId, String projectId)
			throws DataAccessException {
		try {
			PublishedGadgetImage image = findById(previousIconId);
			image.setProjectId(projectId);
			create(image);
		} catch (NotFoundException safeToIgnore) {
			log.debug("found no icon for project: " + projectId);
		}
	}


}
