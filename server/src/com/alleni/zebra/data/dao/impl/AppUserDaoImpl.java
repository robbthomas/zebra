package com.alleni.zebra.data.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.mapper.AppUserMapper;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.util.UUID;
import com.alleni.zebra.security.authorization.Role;
import com.alleni.zebra.util.DateUtils;

@Repository("appUserDao")
public class AppUserDaoImpl implements AppUserDao {
	protected final Log log = LogFactory.getLog(getClass());
	
//	private final String SQL_FIND_BY_ID = "select * from "+ TABLE_NAME + " where id = ?";
//	private final String SQL_FIND_BY_USERNAME = "select * from "+TABLE_NAME+" where displayName = ?";
	private final String SQL_UPDATE = "update "+TABLE_NAME+" set retired = ?, email = ?, emailShow = ?, enabled = ?, " +
	" firstName = ?, lastName = ?, displayName = ?, passwordHash = ?" +
	" where id = ? ";
	private final String SQL_DELETE = "delete from "+TABLE_NAME+" where id = ?";


	@Autowired JdbcTemplate template;
	@Autowired AppUserMapper userMapper;
	private SimpleJdbcInsert insert;

	@PostConstruct
	public void init() {
		this.insert = new SimpleJdbcInsert(template).withTableName(TABLE_NAME);
	}

	private static final String _safeColumns;
	static {
		StringBuffer sb = new StringBuffer();
		for (String s : SAFE_COLUMNS) {
			sb.append("au.");
			sb.append(s);
			sb.append(",");
		}
		
		String tmp = sb.toString();
		_safeColumns = tmp.substring(0, tmp.lastIndexOf(','));
	}

	
	@Override
	@Transactional(readOnly=false)
	public List<AppUser> findAll() throws NotFoundException {
		String sql = "select distinct " + _safeColumns + ", amr.accountId as amr_accountid from app_user au, accountmemberroles amr where au.id = amr.memberid and amr.memberId = au.id";
		List<AppUser> users = new ArrayList<AppUser>();
		
		try {
			users = template.query(sql, userMapper);
		} catch (EmptyResultDataAccessException safeToIgnore) {
			// ignore, return empty list
		}
		return users;
	}
	

	@Override
	@Transactional(readOnly=true)
	public List<AppUser> findAllByAccountId(Long accountId) {
//		String sql = "select * from app_user where accountId = ?";
//		String sql = "select au.*, amr.accountId as amr_accountid rom app_user au, accountmemberroles amr where amr.accountId = ? and amr.memberId = au.id";
		String sql = "select " + _safeColumns + ", amr.accountId as amr_accountid" +
				" from app_user au" +
				" inner join accountmemberroles amr on amr.memberid = au.id" +
				" where amr.accountid = ?;";
		List<AppUser> users = new ArrayList<AppUser>();
		
		try {
			users = template.query(sql, new Object[] {accountId}, userMapper);
		} catch (EmptyResultDataAccessException safeToIgnore) {
			// ignore, return empty list
		}
		return users;
	}


	
	@Override
	@Transactional
	public AppUser findById(Long id) throws NotFoundException {
		return findById(id.toString());
	}

	@Override
	@Transactional
	public AppUser findById(String id) throws NotFoundException {
		log.debug("looking for id: " + id);
		String sql = "select distinct " + _safeColumns + ", amr.accountId as amr_accountid from app_user au, accountmemberroles amr where au.id = amr.memberid and au.id = ?";

		Assert.notNull(id, "Id cannot be null");

		// sanitize ID for wild-cards etc here or somewhere upstream.

		AppUser user = null;

		try {
			user = template.queryForObject(sql, new Object[] {id}, userMapper);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(e, id);
		}

		return user;
	}


	@Override
	@Transactional(noRollbackFor=NotFoundException.class)
	public AppUser findByUsername(String username) throws NotFoundException {
		log.debug("looking for username: " + username);

		Assert.notNull(username, "username cannot be null");

		// debate: throw NotFoundException or return null
		AppUser user = null;

		
		try {
			String sql = "select distinct(au.*), amr.accountId as amr_accountid from app_user au, accountmemberroles amr where au.id = amr.memberid and amr.memberId = au.id and au.displayName = ?";
			user = template.queryForObject(sql, new Object[] {username}, userMapper);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(e, username);
		}

		return user;
	}

    @Override
   	@Transactional(noRollbackFor=NotFoundException.class)
   	public AppUser findByEmail(String email) throws NotFoundException {
   		log.debug("looking for email: " + email);

   		Assert.notNull(email, "email cannot be null");

   		// debate: throw NotFoundException or return null
   		AppUser user = null;


   		try {
   			String sql = "select distinct(au.*), amr.accountId as amr_accountid " +
   					"from app_user au, accountmemberroles amr " +
   					"where au.id = amr.memberid" +
   					" and amr.memberId = au.id and upper(au.email) = upper(?) limit 1";
   			log.debug(sql);
   			user = template.queryForObject(sql, new Object[] {email}, userMapper);
   		} catch (EmptyResultDataAccessException e) {
   			throw new NotFoundException(e, email);
   		}

   		return user;
   	}

    @Override
   	@Transactional
   	public AppUser findByUsernameOrEmail(String name) throws NotFoundException {
   		log.debug("looking for username or email: " + name);

   		Assert.notNull(name, "name cannot be null");

   		// debate: throw NotFoundException or return null
   		AppUser user = null;


   		try {
   			String sql = "select distinct(au.*), amr.accountId as amr_accountid from app_user au, accountmemberroles amr where au.id = amr.memberid and amr.memberId = au.id and (au.displayName = ? or au.email = ?)";
   			user = template.queryForObject(sql, new Object[] {name, name}, userMapper);
   		} catch (EmptyResultDataAccessException e) {
   			throw new NotFoundException(e, name);
   		}

   		return user;
   	}

	@Override
	@Transactional
	public AppUser create(AppUser user) {
		Assert.notNull(user);
		Assert.isNull(user.getId(), "Attempting to Save existing instance as new!");
		
		user.setId(UUID.hibernateCompatibleUUID());
		
		HashMap<String, Object> p = new HashMap<String,Object>();
		p.put("id", user.getId());
		p.put("date_created", DateUtils.toSqlDate(new Date()));
		p.put("retired", user.isRetired());
		p.put("description", user.getDescription());
		p.put("email", user.getEmail());
		p.put("emailshow", user.isShowEmail());
		p.put("enabled",user.isEnabled());
		p.put("passwordhash", user.getPasswd());
		p.put("displayname", user.getUsername());
		p.put("editedbyid", user.getEditedById());
		p.put("firstname", user.getFirstName());
		p.put("lastname", user.getLastName());
		p.put("website", user.getWebsite());
		p.put("nameformat", user.getNameFormat());
		
		insert.execute(p);
		
		return user;
	}

	@Override
	@Transactional
	public AppUser update(AppUser user) throws ConcurrencyException {
		
		int rows = template.update(SQL_UPDATE, user.isRetired(), user.getEmail(), user.isShowEmail(), user.isEnabled(),
				user.getFirstName(), user.getLastName(), user.getUsername(), user.getPasswd(),
				user.getId());

		if (rows == 0) {
			throw new ConcurrencyException("Detected concurrent access to AppUser: " + user.getId(), (String)user.getId());
		}
		
		return user;
	}

	@Override
	@Transactional
	public void delete(AppUser user) throws ConcurrencyException {
		int rows = template.update(SQL_DELETE, user.getId());
		
		if (rows == 0) {
			throw new ConcurrencyException("Detected concurrent access to AppUser: " + user.getId(), (String)user.getId());
		}
	}

	@Transactional
	public void setAvatar(AppUser user, UploadableAsset avatar) {
		String sql = "update app_user set avatarId = ? where id = ?";
		template.update(sql, avatar.getUploadableassetid(), user.getId());
	}
	
	@Transactional
	public void setBanner(AppUser user, UploadableAsset banner) {
		String sql = "update app_user set bannerId = ? where id = ?";
		template.update(sql, banner.getUploadableassetid(), user.getId());
	}

	@Transactional
	public boolean hasRole(AppUser user, Role role) {
		boolean ret = false;
		
		String sql = "SELECT * FROM fn_AppUserHasRole(?::VARCHAR(255), ?::TEXT)";
		int roleCount = template.queryForInt(sql, user.getId(), role.name());
		
		if (roleCount > 0) ret = true;
		
		return ret;
	}
	
	// we need these for mocking in stubs in testing
	
	public JdbcTemplate getTemplate() {
		return template;
	}

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}


	public AppUserMapper getUserMapper() {
		return userMapper;
	}

	public void setUserMapper(AppUserMapper userMapper) {
		this.userMapper = userMapper;
	}


}
