package com.alleni.zebra.data.dao.impl;

import com.alleni.zebra.data.dao.PublishLaunchCountDao;
import com.alleni.zebra.data.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Repository("default")
public class PublishLaunchCountDaoImpl implements PublishLaunchCountDao {
	@Autowired
    JdbcTemplate template;

	private SimpleJdbcInsert insert;


	@PostConstruct
	public void init() {
		this.insert = new SimpleJdbcInsert(template)
                .withTableName("PublishLaunchCounts")
                //.usingGeneratedKeyColumns("publishLaunchCountId")
                .usingColumns("projectId", "accountId", "appUserId", "externalUserId", "accountProjectNumbersId");
	}

	@Transactional(readOnly = false)
    public void incrementLaunchCount(String projectId, AppUser user, String externalUserId, String playerId) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("projectId", projectId);
		map.put("accountId", user.getAccountId());
		map.put("appUserId", user.getId());
		map.put("externalUserId", externalUserId);
		map.put("accountProjectNumbersId", playerId);
        insert.execute(map);
    }
}
