package com.alleni.zebra.data.dao.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alleni.zebra.data.dao.GadgetStateDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.mapper.GadgetStateMapper;
import com.alleni.zebra.data.model.GadgetState;
import com.alleni.zebra.data.util.UUID;


@Repository("gadgetStateDao")
public class GadgetStateDaoImpl implements GadgetStateDao {	
	protected final Log log = LogFactory.getLog(getClass());

	private SimpleJdbcInsert insertGadgetState;

	@Autowired JdbcTemplate template;
	@Autowired GadgetStateMapper gadgetStateMapper;

	@PostConstruct
	public void init() {
		this.insertGadgetState = new SimpleJdbcInsert(template).withTableName("gadget_state");
	}

	@Override
	@Transactional
	public GadgetState findById(String id) throws NotFoundException {
		log.debug("looking for gadgetstate id: " + id);
		Assert.notNull(id, "Id cannot be null");

		GadgetState gadgetState = null;

		String sql = "select * from gadget_state where id = ?";

		try {
			gadgetState = template.queryForObject(sql, new Object[] {id}, gadgetStateMapper);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(e, id);
		}

		if (gadgetState == null) {
			throw new NotFoundException("No result found", id);
		}
		return gadgetState;
	}


	@Override
	@Transactional
	public List<GadgetState> findAll() {
		String sql = "select * from gadget_state";
		List<GadgetState> states = template.query(sql, gadgetStateMapper);
		return states;
	}


	@Override
	@Transactional
	public GadgetState update(GadgetState gadgetState) throws ConcurrencyException {
		String sql = "update gadget_state set app_user_id = ?, author_mode = ?," +
		" content = ?,root_project_id = ?,externaluserid = ? where id = ? and version = ?";
		int rows = 0;
		try {
			rows = template.update(sql, gadgetState.getAppUserId(), gadgetState.getAuthorMode(),
					gadgetState.getContent(), gadgetState.getRootProjectId(), gadgetState.getExternalUserId(), gadgetState.getId(), gadgetState.getVersion());
		} catch (EmptyResultDataAccessException e) {
			throw new ConcurrencyException("Detected concurrent access to GadgetState: " + gadgetState.getId(), (String)gadgetState.getId());
		}
		if (rows == 0) {
			throw new ConcurrencyException("Detected concurrent access to GadgetState: " + gadgetState.getId(), (String)gadgetState.getId());
		}

		return gadgetState;
	}

	@Override
	@Transactional
	public void delete(GadgetState gadgetState) throws ConcurrencyException {
		String sql = "delete from gadget_state where id = ? and version = ?";
		int rows = 0;
		try {
			rows = template.update(sql, gadgetState.getId(), gadgetState.getVersion());
		} catch (EmptyResultDataAccessException e) {
			throw new ConcurrencyException("Detected concurrent access to GadgetState: " + gadgetState.getId(), (String)gadgetState.getId());
		}

		if (rows == 0) {
			throw new ConcurrencyException("Detected concurrent access to GadgetState: " + gadgetState.getId(), (String)gadgetState.getId());
		}

	}

	@Override
	@Transactional
	public GadgetState create(GadgetState gadgetState) {
		Assert.notNull(gadgetState);
		Assert.isNull(gadgetState.getId(), "Attempting to save existing GadgetState instance as new: " + gadgetState.getId());
		gadgetState.setId(UUID.hibernateCompatibleUUID());

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("id", gadgetState.getId());
		params.put("version", gadgetState.getVersion());
		params.put("app_user_id", gadgetState.getAppUserId());
		params.put("author_mode", gadgetState.getAuthorMode());
		params.put("content", gadgetState.getContent());
		params.put("root_project_id", gadgetState.getRootProjectId());
		params.put("externaluserid", gadgetState.getExternalUserId());


		if (log.isTraceEnabled()) {
			log.trace(" GadgetState fields: ");
			for (String field : params.keySet()) {
				log.trace(field + ": " + params.get(field));
			}
		}
        // first delete the old row if any
        String sql = "delete from gadget_state where root_project_id = ? and app_user_id = ? and author_mode = ?";
        Object[] deleteParams = new Object[] {gadgetState.getRootProjectId(), gadgetState.getAppUserId(), gadgetState.getAuthorMode()};
        if(gadgetState.getExternalUserId() == null) {
            sql += " and externalUserId is null";
        } else {
            sql += " and externalUserId = ?";
            deleteParams = Arrays.copyOf(deleteParams, deleteParams.length+1);
            deleteParams[deleteParams.length-1] = gadgetState.getExternalUserId();
        }
        template.update(sql, deleteParams);

        // finally insert the new row
		insertGadgetState.execute(params);
		return gadgetState;
	}

	@Override
	@Transactional
	public GadgetState findByRootProjectAndAppUser(String rootGadgetId,
			String appUserId, String externalUserId, boolean authorMode) throws NotFoundException {
		log.debug("looking for gadgetstate for gadget: " + rootGadgetId + " and user: " + appUserId + " and externalUserId: " + externalUserId + " and authorMode: " + authorMode);

		GadgetState state = null;
        String sql = "select * from gadget_state where root_project_id = ? and app_user_id = ? and author_mode = ?";
        Object[] params = new Object[] {rootGadgetId, appUserId, authorMode};
        if(externalUserId == null) {
            sql += " and externalUserId is null";
        } else {
            sql += " and externalUserId = ?";
            params = Arrays.copyOf(params, params.length+1);
            params[params.length-1] = externalUserId;
        }
		try {
		state = template.queryForObject(sql, params, gadgetStateMapper);
		} catch (EmptyResultDataAccessException e) {
			log.debug("not found.");
//			throw new NotFoundException(rootGadgetId);
			return null;
		}
		log.debug("found: " + state.getId());
		return state;
	}

	@Override
	@Transactional
	public GadgetState findByRootProject(String rootProjectId)
			throws NotFoundException {
		log.debug("looking for gadgetstate for gadget: " + rootProjectId );

		GadgetState state = null;
		String sql = "select * from gadget_state where root_project_id = ? ";
		
		try {
		state = template.queryForObject(sql, new Object[] {rootProjectId}, gadgetStateMapper);
		} catch (EmptyResultDataAccessException e) {
			log.debug("not found.");
			throw new NotFoundException(rootProjectId);
		}
		log.debug("found: " + state.getId());
		return state;
	}

}
