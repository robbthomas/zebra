package com.alleni.zebra.data.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import com.alleni.zebra.data.dao.ProjectSearchHelper;
import com.alleni.zebra.data.dto.MiniProjectListSearchResultSet;
import com.alleni.zebra.data.dto.MyProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectQuery;
import com.alleni.zebra.data.dto.TinyProjectListSearchResultSet;
import com.alleni.zebra.data.mapper.MiniProjectMapper;
import com.alleni.zebra.data.mapper.MyProjectMapper;
import com.alleni.zebra.data.mapper.ProjectMapper;
import com.alleni.zebra.data.mapper.TinyProjectMapper;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.MiniProject;
import com.alleni.zebra.data.model.MyProject;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.TinyProject;
import com.alleni.zebra.data.util.UUID;
import com.alleni.zebra.util.StringUtils;

@Repository("projectSearchHelperSPImpl")
public class ProjectSearchHelperSPImpl implements ProjectSearchHelper {
	private final Logger log = Logger.getLogger(ProjectSearchHelperSPImpl.class);
	private final Map<String,Map<String,Object>> currentQueries = new ConcurrentHashMap<String, Map<String,Object>>();

	@Autowired JdbcTemplate template;
	@Autowired ProjectMapper mapper;
	@Autowired MiniProjectMapper miniMapper;
	@Autowired MyProjectMapper myMapper;
	@Autowired TinyProjectMapper tinyMapper;
	@Autowired TransactionTemplate transactionTemplate;

	private String createSortString(String sortColumn, String order, Boolean isPublished) {
		String sortString = "";

		if (null == sortColumn) sortString = "createdDateTime";
		else if (sortColumn.equals("createdDateTime")) sortString = "createdDateTime";
		else if (sortColumn.equals("editedDateTime")) sortString = "editedDateTime";
		else if (sortColumn.equals("price")) sortString = "price";
		else if (sortColumn.equals("categoryId")) sortString = "categoryId";
		else if (sortColumn.equals("description")) sortString = "description";
		else if (sortColumn.equals("type")) sortString = "projectTypeId";
		else if (sortColumn.equals("name")) sortString = isPublished? "publishedName" : "projectName";

		sortString += " ";
		if (order == null) sortString += "desc";
		else sortString += order.toLowerCase();

		return sortString;
	}

	@Transactional
	public ProjectListSearchResultSet search(final ProjectQuery q, final AppUser searcher) {

//		return transactionTemplate.execute(new TransactionCallback<ProjectListSearchResultSet>() {
//			// the code in this method executes in a transactional context
//			public ProjectListSearchResultSet doInTransaction(TransactionStatus status) {

				String searchString = "";
				boolean hideInStoreList = !q.isForStoreList();
				int projectTypeId = 0;
				String projectId = "";
				long dep_pubId = 0;
				String apnUUID = "";
				String urlName = "";
				boolean isForLatestForApnUUID = true;

				log.debug("ProjectQuery: " + q.toString());

				List<Project> projects = Collections.emptyList();

				String sortString = createSortString(q.getSortColumn(), q.getOrder(), q.isPublished());

				if (q.getType() != null) projectTypeId = q.getType().id;

				if (q.getProjectId() != null) {
					if (StringUtils.isNumeric(q.getProjectId())) {
						dep_pubId = Long.valueOf(q.getProjectId());
					} else {
						projectId = q.getProjectId();
					}
				}

				//TODO: Determine if an exception should be thrown if a numeric projectId and a dep_pubId have both been defined
				//For the time being, just override the numeric projectId with the provided dep_pubId
				if (q.getDepPublishId() != null) {
					dep_pubId = q.getDepPublishId();
				}
				
				if (q.getApnUUID() != null && !q.getApnUUID().equals("")) {
					apnUUID = q.getApnUUID();
					isForLatestForApnUUID = q.isForLatestForApnUUID();
				}

				if (q.getUrlName() != null) {
					urlName = q.getUrlName();
				}

				if (q.isHistory()) {
					q.setAuthorMemberId(searcher.getId());
				} else {
					searchString = q.getName();
				}

				String key = UUID.standardUUID();
				Map<String,Object> args = new HashMap<String,Object>();
				args.put("start time",  new DateTime());
				args.put("hideInStoreList",  hideInStoreList);
				args.put("sortString",  sortString);
				args.put("projectTypeId",  projectTypeId);
				args.put("q.getFeatured()",  q.getFeatured());
				args.put("q.getSize()",  q.getSize());
				args.put("q.getOffset()",  q.getOffset());
				args.put("searcher.getAccountId()",  searcher.getAccountId());
				args.put("searcher.getId()",  searcher.getId());
				args.put("q.getCategoryId()",  q.getCategoryId());
				args.put("q.getAuthorMemberId()",  q.getAuthorMemberId());
				args.put("q.getProjectName()",  q.getProjectName());
				args.put("q.getGenericSearch()",  q.getGenericSearch());
				args.put("q.getOwnerAccountId()",  q.getOwnerAccountId());
				args.put("q.isShowContent()",  q.isShowContent());
				args.put("q.isPublished()",  q.isPublished());
				args.put("projectId",  projectId);
				args.put("dep_pubId",  dep_pubId);
				args.put("urlName()",  urlName);
				args.put("apnUUID",  apnUUID);
				args.put("isForLatestForApnUUID",  isForLatestForApnUUID);
				args.put("q.getParentProjectId()", q.getParentProjectId());
				args.put("searchString", searchString);
				args.put("q.getProjectIdList()", q.getProjectIdList());

				log.debug("---------------------------------------------------------");
				log.debug("Executing Stored Procedure with the following parameters:");
				for (String k : args.keySet()) {
					log.debug("  " + k + ": " + args.get(k));
				}
				log.debug("----------------------------------------------------------");

				try {
					if (!q.isHistory() && (!projectId.equals("") || !urlName.equals("") || dep_pubId > 0 || !apnUUID.equals(""))) {
						log.debug("Calling fn_project_single()...");
						args.put("fn name", "fn_project_single()");
						currentQueries.put(key, args);
						projects = template.query("select * FROM fn_project_single(" +
								"?::INT8, "    + // searcherAccountId
								"?::VARCHAR, " + // projectId
								"?::VARCHAR, " + // apnUUID
								"?::TEXT, "    + // urlßName
								"?::BOOLEAN, " + // isForLatestForApnUUID
								"?::INT8,"     + // dep_pubId
								"?::BOOLEAN  " + // in_showContent
								")", mapper,
								searcher.getAccountId(),
								projectId,
								apnUUID,
								q.getUrlName(),
								isForLatestForApnUUID,
								dep_pubId,
								q.isShowContent()
								);
					} else {
						String projectIdListToSend = null;
						List<String> localIdList = q.getProjectIdList();
						if (q.getProjectIdList() != null) {
							projectIdListToSend = "";
							for (String pid : localIdList) {
								projectIdListToSend += pid;
								projectIdListToSend +=",";
							}
							projectIdListToSend +="NOT_A_PROJECT_ID";
						}

						log.debug("Calling fn_project_list2()...");
						args.put("fn name", "fn_project_list2()");
						currentQueries.put(key, args);
						projects = template.query("select * from fn_project_list2(" +
								"?::INT4, "    + // in_projectTypeId
								"?::BOOLEAN, " + // in_featured
								"?::INT4, "    + // in_limit
								"?::INT4, "    + // in_offset
								"?::INT8, "    + // in_searcherAccountId
								"?::VARCHAR, " + // in_orderBy
								"?::INT4, "    + // in_categoryId
								"?::TEXT, "    + // in_authorMemberId
								"?::TEXT, "    + // in_searchText
								"?::TEXT, "    + // in_genericSearch
								"?::INT4, "    + // in_ownerAccountId
								"?::BOOLEAN, " + // in_showContent
								"?::BOOLEAN, " + // in_isPublished
								"?::BOOLEAN, " + // in_hideInStoreList
								"?::VARCHAR,"  + // in_parentProjectId
								"?::BOOLEAN,"  + // in_isForLatest
								"?::TEXT,"     + // in_searcherMemberId
								"?::VARCHAR,"  + // in_projectName
								"?::VARCHAR,"  + // in_urlName
								"?::TEXT"      + // in_idList
								")", mapper,
								(projectTypeId > 0) ? projectTypeId : null,
										q.getFeatured(),
										q.getSize(),
										q.getOffset(),
										searcher.getAccountId(),
										sortString,
										q.getCategoryId(),
										q.getAuthorMemberId(),
										searchString,
										q.getGenericSearch(),
										q.getOwnerAccountId(),
										q.isShowContent(),
										q.isPublished(),
										hideInStoreList,
										q.getParentProjectId(),
										q.isForLatest(),
										searcher.getId(),
										q.getProjectName(),
										q.getUrlName(),
										projectIdListToSend
								);				
					}			
				} catch (EmptyResultDataAccessException safeToIgnore) {
					log.debug(" no results");
				}

				//TODO: make the wrapper take care of this part.
				int totalCount = 0;
				if (projects.size() > 0) {
					totalCount = projects.get(0).getTotalCount();
				}
				currentQueries.remove(key);
				return new ProjectListSearchResultSet(projects, totalCount);
//			}
//		});
	}

	public TinyProjectListSearchResultSet searchTinyStuff(
			ProjectQuery q, 
			AppUser searcher) {

		List<TinyProject> tinyProjects = Collections.emptyList();

		String sortString = createSortString(q.getSortColumn(), q.getOrder(), q.isPublished());

		String projectName = q.isPublished() ? q.getPublishedName() : q.getProjectName();

		int projectTypeId = 0;
		
		if (null != q.getType()) projectTypeId = q.getType().id;
		
		String key = UUID.standardUUID();
		Map<String,Object> args = new HashMap<String,Object>();
		args.put("fn name", "fn_project_list_tiny()");
		args.put("start time",  new DateTime());
		args.put("sortString",  sortString);
		args.put("projectTypeId", projectTypeId);
		args.put("projectName", projectName);
		args.put("search value", q.getName());
		args.put("searcher.getId()",  searcher.getId());
		args.put("q.isPublished()",  q.isPublished());
		args.put("q.getRetired()", q.getRetired());

		log.debug("---------------------------------------------------------");
		log.debug("Executing Stored Procedure with the following parameters:");
		for (String k : args.keySet()) {
			log.debug("  " + k + ": " + args.get(k));
		}
		log.debug("----------------------------------------------------------");

		log.debug("----------------------------------------------------------");

		log.debug("Calling fn_project_list_tiny()");
		currentQueries.put(key, args);
		tinyProjects = template.query("select * from fn_project_list_tiny(" +
			"?::INT4, "    + // in_projectTypeId
			"?::BOOLEAN, " + // in_isPublished
			"?::TEXT, "    + // in_orderBy
			"?::TEXT,"     + // in_authorMemberId
			"?::TEXT,"     + // in_projectName
			"?::TEXT,"     + // in_search
			"?::BOOLEAN,"  + // in_hideRetired
			"?::TEXT"      + // in_projectNameList
			")"
			, tinyMapper,
			projectTypeId,
			q.isPublished(),
			sortString,
			q.getAuthorMemberId(),
			projectName,
			q.getName(),
			q.getRetired(),
			q.getProjectNameList()
		);

		int totalCount = tinyProjects.size();

		currentQueries.remove(key);
		return new TinyProjectListSearchResultSet(tinyProjects, totalCount);
	}

	public MyProjectListSearchResultSet searchMyStuff(
			ProjectQuery q, 
			AppUser searcher) {
		List<MyProject> myProjects = Collections.emptyList();

		String sortString = createSortString(q.getSortColumn(), q.getOrder(), q.isPublished());

		String key = UUID.standardUUID();
		Map<String,Object> args = new HashMap<String,Object>();
		args.put("fn name", "fn_project_list_my()");
		args.put("start time",  new DateTime());
		args.put("sortString",  sortString);
		args.put("projectTypeId",  q.getType().id);
		args.put("q.getSize()",  q.getSize());
		args.put("q.getOffset()",  q.getOffset());
		args.put("searcher.getAccountId()",  searcher.getAccountId());
		args.put("searcher.getId()",  searcher.getId());
		args.put("q.getCategoryId()",  q.getCategoryId());
		args.put("q.getGenericSearch()",  q.getGenericSearch());
		args.put("q.isPublished()",  q.isPublished());

		log.debug("---------------------------------------------------------");
		log.debug("Executing Stored Procedure with the following parameters:");
		for (String k : args.keySet()) {
			log.debug("  " + k + ": " + args.get(k));
		}
		log.debug("----------------------------------------------------------");

		log.debug("----------------------------------------------------------");

		log.debug("Calling fn_project_list_small()");
		currentQueries.put(key, args);
		myProjects = template.query("select * from fn_project_list_my(" +
				"?::INT4, "    + // in_projectTypeId
				"?::BOOLEAN, " + // in_isPublished
				"?::INT4, "    + // in_limit
				"?::INT4, "    + // in_offset
				"?::TEXT, "    + // in_orderBy
				"?::INT8, "    + // in_searcherAccountId
				"?::TEXT, "    + // in_authorMemberId
				"?::INT4, "    + // in_categoryId
				"?::TEXT"     + // in_genericSearch
				")"
				, myMapper,
				q.getType().id,
				q.isPublished(),
				q.getSize(),
				q.getOffset(),
				sortString,
				searcher.getAccountId(),
				q.getAuthorMemberId(),
				q.getCategoryId(),
				q.getGenericSearch()
				);

		//TODO: make the wrapper take care of this part.
		int totalCount = 0;
		if (myProjects.size() > 0) {
			totalCount = myProjects.get(0).getTotalCount();
		}

		currentQueries.remove(key);
		return new MyProjectListSearchResultSet(myProjects, totalCount);
	}

	public MiniProjectListSearchResultSet searchMiniStuff(ProjectQuery q,
			AppUser searcher) {
		List<MiniProject> miniProjects = Collections.emptyList();

		String sortString = createSortString(q.getSortColumn(), q.getOrder(), q.isPublished());

		String projectIdListToSend = null;
		List<String> localIdList = q.getProjectIdList();
		if (q.getProjectIdList() != null) {
			projectIdListToSend = "";
			for (String projectId : localIdList) {
				projectIdListToSend += projectId;
				projectIdListToSend +=",";
			}
			projectIdListToSend +="NOT_A_PROJECT_ID";
		}

		String key = UUID.standardUUID();
		Map<String,Object> args = new HashMap<String,Object>();
		args.put("fn name", "fn_project_list_small()");
		args.put("start time",  new DateTime());
		args.put("sortString",  sortString);
		args.put("projectTypeId",  q.getType().id);
		args.put("q.getSize()",  q.getSize());
		args.put("q.getOffset()",  q.getOffset());
		args.put("searcher.getAccountId()",  searcher.getAccountId());
		args.put("searcher.getId()",  searcher.getId());
		args.put("q.getCategoryId()",  q.getCategoryId());
		args.put("q.getGenericSearch()",  q.getGenericSearch());
		args.put("q.isPublished()",  q.isPublished());
		args.put("q.getProjectIdList()", q.getProjectIdList());
		args.put("q.getUrlNasme()", q.getUrlName());

		log.debug("---------------------------------------------------------");
		log.debug("Executing Stored Procedure with the following parameters:");
		for (String k : args.keySet()) {
			log.debug("  " + k + ": " + args.get(k));
		}
		log.debug("----------------------------------------------------------");

		log.debug("Calling fn_project_list_small()");
		currentQueries.put(key, args);
		miniProjects =  template.query("select * from fn_project_list_small(" +
							  "?::INT4, "    + // in_projectTypeId
							  "?::BOOLEAN, " + // in_isPublished
							  "?::INT4, "    + // in_limit
							  "?::INT4, "    + // in_offset
							  "?::INT8, "    + // in_searcherAccountId
							  "?::TEXT, "    + // in_searcherMemberId
							  "?::INT4, "    + // in_categoryId
							  "?::TEXT, "    + // in_orderBy
							  "?::TEXT,"     + // in_genericSearch
							  "?::TEXT,"     + // in_projectName
							  "?::TEXT,"     + // in_projectIdList
							  "?::TEXT"      + // in_urlName
							  ")"
							, miniMapper
							, q.getType().id
							, q.isPublished()
							, q.getSize()
							, q.getOffset()
							, searcher.getAccountId()
							, q.getAuthorMemberId()
							, q.getCategoryId()
							, sortString
							, q.getGenericSearch()
							, q.getProjectName()
							, projectIdListToSend
							, q.getUrlName()
						);
		//TODO: make the wrapper take care of this part.
		int totalCount = 0;
		if (miniProjects.size() > 0) {
			totalCount = miniProjects.get(0).getTotalCount();
		}

		currentQueries.remove(key);
		return new MiniProjectListSearchResultSet(miniProjects, totalCount);
	}
	
	public ProjectListSearchResultSet searchHistory(ProjectQuery q,
			AppUser searcher) {
		List<Project> historyProjects = Collections.emptyList();
 
		String key = UUID.standardUUID();
		Map<String,Object> args = new HashMap<String,Object>();
		args.put("fn name", "fn_project_list_history()");
		args.put("start time",  new DateTime());
		args.put("q.getProjectId()", q.getProjectId());	
		args.put("q.getProjectName()", q.getProjectName());
		args.put("q.getPublishedName()", (q.getPublishedName() == null) ? "" : q.getPublishedName());
		args.put("q.getUrlName()", (q.getUrlName() == null) ? "" : q.getUrlName());
		args.put("q.isPublished()",  q.isPublished());
		args.put("authorMemberId",  searcher != null? searcher.getId() : null);
		args.put("searcher.getAccountId()",  searcher != null? searcher.getAccountId() : null);
		args.put("q.getRetired()", q.getRetired()); 

		log.debug("---------------------------------------------------------");
		log.debug("Executing Stored Procedure with the following parameters:");
		for (String k : args.keySet()) {
			log.debug("  " + k + ": " + args.get(k));
		}
		log.debug("----------------------------------------------------------");
		
		currentQueries.put(key, args);
		historyProjects = template.query("select * from fn_project_list_history(" +
				"?::TEXT, "    + // in_projectId
				"?::TEXT, "    + // in_projectName
				"?::TEXT, "    + // in_publishedName
				"?::TEXT, "    + // in_urlName
				"?::BOOLEAN, " + // in_isPublished
				"?::TEXT, "    + // in_searcherMemberId
				"?::INT4, "    + // in_searcherAccountId
				"?::BOOLEAN"   + // in_hideRetired
				")"
				, mapper,
				q.getProjectId(),
				q.getProjectName(),
				q.getPublishedName(),
				q.getUrlName(),
				q.isPublished(),
				q.getAuthorMemberId(),
				searcher != null? searcher.getAccountId() : null,
				q.getRetired()
				);
		//TODO: make the wrapper take care of this part.
		int totalCount = historyProjects.size();

		currentQueries.remove(key);
		return new ProjectListSearchResultSet(historyProjects, totalCount);
	}

	@Override
	public  Map<String,Map<String,Object>> listCurrentQueries() {
		return currentQueries;
	}

}
