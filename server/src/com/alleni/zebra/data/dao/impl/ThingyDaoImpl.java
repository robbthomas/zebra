package com.alleni.zebra.data.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.ThingyDao;
import com.alleni.zebra.data.dao.sequence.ContextAwareSequenceDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.mapper.ThingyMapper;
import com.alleni.zebra.data.model.Thingy;

@Repository("thingyDao")
public class ThingyDaoImpl implements ThingyDao {
	@Autowired JdbcTemplate template;
	@Autowired ThingyMapper mapper;
	@Autowired ContextAwareSequenceDao sequenceDao;
	
	private SimpleJdbcInsert insert;

	
	@PostConstruct
	public void init() {
		this.insert = new SimpleJdbcInsert(template).withTableName(TABLE_NAME);
	}

	
	@Override
	public Thingy findById(Long id) throws NotFoundException {
		String sql = "select * from thingy where id = ?";
		Thingy thing = null;
		try {
			template.queryForObject(sql,new Object[] {id}, mapper);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(id);
		}
		return thing;
	}

	@Override
	public List<Thingy> findAll() throws NotFoundException {
		List<Thingy> all = Collections.emptyList();
		
		String sql = "select * from thingy";
		try {
			template.queryForObject(sql, mapper);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException((String)null);
		}
		return all;
	}

	
	@Override
	@Transactional
	public Thingy findByName(String name) throws NotFoundException {
		String sql = "select * from thingy where name = ?";
		Thingy thing = null;
		try {
			template.queryForObject(sql, new Object[] {name},mapper);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException("name not found", name);
		}
		return thing;
	}

	@Override
	@Transactional
	public Thingy create(Thingy thingy) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", sequenceDao.nextValue());
		params.put("name", thingy.getName());
		
		insert.execute(params);
		return thingy;
	}

	@Override
	@Transactional
	public Thingy update(Thingy thingy) throws NotFoundException, ConcurrencyException {
		String sql = "update thingy set name = ? where id = ? and version = ?";
		
		int rows = 0;
		try {
			rows = template.update(sql, thingy.getName(), thingy.getId(), thingy.getVersion());
		} catch (EmptyResultDataAccessException e) {
			throw new ConcurrencyException("Detected concurrent access to Thingy: " + thingy.getId(), thingy.getId());
		}
		if (rows == 0) {
			throw new ConcurrencyException("Detected concurrent access to Thingy: " + thingy.getId(), thingy.getId());
		}

		return thingy;
		
	}

	@Override
	@Transactional
	public void delete(Thingy thingy) throws NotFoundException, ConcurrencyException {
		String sql = "delete from thingy where id = ? and version = ?";
		int rows = 0;
		try {
			rows = template.update(sql, thingy.getId(), thingy.getVersion());
		} catch (EmptyResultDataAccessException e) {
			throw new ConcurrencyException("Detected concurrent access to Thingy: " + thingy.getId(), thingy.getId());
		}

		if (rows == 0) {
			throw new ConcurrencyException("Detected concurrent access to Thingy: " + thingy.getId(), thingy.getId());
		}

	}

}
