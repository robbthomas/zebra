package com.alleni.zebra.data.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.UserVariableDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.mapper.UserDataMapper;
import com.alleni.zebra.data.model.UserVariable;

@Repository("userDataDao")
public class UserVariableDaoImpl implements UserVariableDao {
	@Autowired JdbcTemplate template;
	@Autowired UserDataMapper mapper;
	private SimpleJdbcInsert insertData;

	@PostConstruct
	public void init() {
		this.insertData = new SimpleJdbcInsert(template).withTableName("userdata");
	}

	@Override
	@Transactional(readOnly=true)
	public List<UserVariable> getTopN(String projectId,	String lookupKey, int n) {
		String sql = "select * from userdata where projectId = ? and lookupKey = ? and" +
		" order by value desc limit " + n;

		List<UserVariable> ret = Collections.emptyList();
		try {
			ret = template.query(sql, new Object[] {projectId, lookupKey}, mapper);
		} catch (EmptyResultDataAccessException safeToIgnore) {
			// ignore, return empty list
		}


		return ret;
	}

	@Override
	@Transactional(readOnly=true)
	public List<UserVariable> getBottomN(String projectId, String lookupKey, int n) {
		String sql = "select * from userdata where projectId = ? and lookupKey = ? and" +
		" order by value asc limit " + n;

		List<UserVariable> ret = Collections.emptyList();
		try {
			ret = template.query(sql, new Object[] {projectId, lookupKey}, mapper);
		} catch (EmptyResultDataAccessException safeToIgnore) {
			// ignore, return empty list
		}


		return ret;
	}

	
	@Override
	@Transactional(readOnly=true)
	public List<Map<String, Object>> histogram(String projectId, String lookupKey) {
		String sql = "select value, count(value) from userdata where projectId = ? and lookupKey = ?" +
		"group by (value)";
		List<Map<String, Object>> ret = Collections.emptyList();
		try {
			ret = template.queryForList(sql, projectId, lookupKey);
		} catch (EmptyResultDataAccessException safeToIgnore) {
			// ignore, return empty list
		}
		return ret;
	}

	@Override
	@Transactional(readOnly=true)
	public List<UserVariable> list(String projectId, String lookupKey) {
		String sql = "select * from userdata where projectId = ? and lookupKey = ?";

		List<UserVariable> ret = Collections.emptyList();
		try {
			ret = template.query(sql, new Object[] {projectId, lookupKey}, mapper);
		} catch (EmptyResultDataAccessException safeToIgnore) {
			// ignore, return empty list
		}

		return ret;
	}

	@Override
	@Transactional(readOnly=true)
	public UserVariable find(String projectId, String memberId, String lookupKey) throws NotFoundException {
		String sql = "select * from userdata where projectId = ? and lookupKey = ? and memberId = ?";

		UserVariable ret = null;
		try {
			ret = template.queryForObject(sql, new Object[] {projectId, lookupKey, memberId}, mapper);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(999L);
		}
		return ret;
	}

	@Override
	@Transactional(readOnly=false)
	public UserVariable create(UserVariable data) {

		String sql = "update userdata set value = ?, jsonData = ? where projectId = ? and lookupKey = ? and memberId = ?";

		int rows = template.update(sql, data.getValue(), data.getJsonData(), data.getProjectId(), data.getLookupKey(), data.getMemberId());

		if (rows == 0) {
			HashMap<String,Object> params = new HashMap<String,Object>();
			params.put("projectId", data.getProjectId());
			params.put("memberId", data.getMemberId());
			params.put("lookupKey", data.getLookupKey());
			params.put("value", data.getValue());
			params.put("jsonData", data.getJsonData());

			insertData.execute(params);
		}
		return data;
	}

}
