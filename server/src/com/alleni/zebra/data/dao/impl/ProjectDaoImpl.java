package com.alleni.zebra.data.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.CannotSerializeTransactionException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.dao.ProjectSearchHelper;
import com.alleni.zebra.data.dto.MiniProjectListSearchResultSet;
import com.alleni.zebra.data.dto.MyProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectQuery;
import com.alleni.zebra.data.dto.TinyProjectListSearchResultSet;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.mapper.ProjectMapper;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.util.UUID;
import com.alleni.zebra.gadget.ProjectType;

@Repository("projectDao")
public class ProjectDaoImpl implements ProjectDao {
	private final Logger log = Logger.getLogger(ProjectDaoImpl.class);


	@Autowired JdbcTemplate template;
	@Autowired ProjectMapper mapper;

	@Autowired
	@Qualifier(value="projectSearchHelperSPImpl")
	//	@Qualifier(value="projectSearchHelper")
	ProjectSearchHelper searchHelper;


	// not autowired: instantiated in init()
	private SimpleJdbcInsert insert;
	private SimpleJdbcInsert childInsert;

	@PostConstruct
	public void init() {
		this.insert = new SimpleJdbcInsert(template).withTableName(TABLE_NAME).usingColumns(COLUMNS);
		this.childInsert = new SimpleJdbcInsert(template).withTableName("parent_child");
	}


	@Transactional
	public ProjectListSearchResultSet search(ProjectQuery q, AppUser searcher) {
		if (q.isHistory()) {
			return searchHelper.searchHistory(q, searcher);
		} else {
			return searchHelper.search(q, searcher);
		}
	}

	@Transactional(readOnly = true)
	public Project findById(String projectId, AppUser searcher) throws DataAccessException {
		Assert.notNull(projectId, "id cannot be null");
		Project p = null;

		try {
			ProjectQuery q = new ProjectQuery();
			q.setProjectId(projectId);
			q.setSize(1);
			q.setOffset(0);
			q.setShowContent(true);

			ProjectListSearchResultSet projects = searchHelper.search(q, searcher);
			if (projects.totalCount == 0) {
				throw new NotFoundException(projectId);
			} else {
				p = projects.projects.get(0);
			}
		} catch (Exception e) {
			throw new DataAccessException(e, projectId);
		}

		return p;
	}

	@Transactional(readOnly = true)
	public List<Project> findHeadVersions(Project p) throws DataAccessException {
		Assert.notNull(p);

		List<Project> projects = Collections.emptyList();
		String sql = "select * from projects where projectName = ? " +
				"and authorMemberId = ? " +
				"and projectTypeId = ? " +
				"and nextVersionId is null";

		Object[] parms = {p.getProjectName(), p.getAuthorMemberId(), p.getProjectType().id};

		if (log.isDebugEnabled()) {
			String debug = "findHeadVersions: [" + sql + "], [";
			for (Object o : parms) {debug += o + ",";}
			log.debug(debug + "]");
		}

		try {
			projects = template.query(sql, parms, mapper);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(e, p.getProjectId());
		}
		return projects;
	}



	@Transactional(readOnly = true)
	public Project findForAccountProjectNumbersUUID(String apnUUID, AppUser user)
			throws DataAccessException {
		Assert.notNull(apnUUID, "apnUUID cannot be null");
		Project p = null;

		try {
			ProjectQuery q = new ProjectQuery();
			q.setApnUUID(apnUUID);
			q.setSize(1);
			q.setOffset(0);
			q.setShowContent(true);

			ProjectListSearchResultSet projects = searchHelper.search(q, user);
			if (projects.totalCount == 0) {
				throw new NotFoundException(apnUUID);
			} else {
				p = projects.projects.get(0);
			}
		} catch (Exception e) {
			throw new DataAccessException(e, apnUUID);
		}

		return p;
	}


	public Project findPublishedByNameAndtype(String name, ProjectType type)
			throws NotFoundException {
		Project p = null;
		String sql = "select projects.* from projects " +
				"where name = ? and projectType = ? and published = true";
		Object[] parms = {name, type.id};

		if (log.isDebugEnabled()) {
			String debug = "findPublishedByNameAndtype: [" + sql + "], [";
			for (Object o : parms) {debug += o + ",";}
			log.debug(debug + "]");
		}

		try {
			p = template.queryForObject(sql, parms, mapper);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(e, name);
		}

		return p;
	}


	@Transactional(readOnly = false)
	public Project create(Project project) throws UniqueConstraintException {
		Assert.notNull(project);
		//		Assert.isNull(project.getProjectId(), "id must be null on a new db instance");

		HashMap<String, Object> v = new HashMap<String, Object>();
		if (project.getRequestedNewProjectId() != null) {
			project.setProjectId(project.getRequestedNewProjectId());
		} else {
			project.setProjectId(UUID.hibernateCompatibleUUID());
		}

		// to prevent an index clash initially set this to have non null next version ids
		// these will have to be nulled out by the service layer before the transaction closes
		// or this will effectively be hidden in searches
		project.setNextVersionId(project.getProjectId());
		project.setNextPublishedVersionId(project.getProjectId());

		v.put("nextVersionId", project.getNextVersionId());
		v.put("nextPublishedVersionId", project.getNextPublishedVersionId());

		v.put("accountId", project.getAccountId());
		v.put("authorMemberId", project.getAuthorMemberId());
		v.put("autosave", project.isAutoSave());
		v.put("categoryId", project.getCategoryId() == 0? null : project.getCategoryId());
		v.put("content", project.getContent());
		v.put("currencyTypeId", project.getCurrencyTypeId() == 0? null : project.getCurrencyTypeId());
		v.put("retired", project.isDeleted());
		v.put("description", project.getDescription());
		//		v.put("editeddatetime", DateUtils.toSqlDate(new Date()));
		v.put("featured", project.isFeatured());
		v.put("gadgetSize", project.getGadgetSize());

		long glId = project.getGuestListId() == null? 0 : project.getGuestListId();
		v.put("guestListId", glId == 0? null : glId);

		v.put("hideInStoreList", project.isHideInStoreList());
		v.put("initialValues", project.getInitialValues());
		v.put("inviteOnly", project.isInviteOnly());
		v.put("licenseTypeId", project.getLicenseTypeId());
		v.put("nextVersionId", project.getNextVersionId());
		v.put("price", project.getPrice());
		v.put("selfPrice", project.getSelfPrice());
		v.put("projectId", project.getProjectId());
		v.put("projectMetadata", project.getProjectMetadata());
		v.put("projectName", project.getProjectName());

		if (project.getProjectTypeId() == 0 && project.getProjectType() != null) {
			project.setProjectTypeId(project.getProjectType().id);
		}
		v.put("projectTypeId", project.getProjectTypeId());

		v.put("published", project.isPublished());
		v.put("publishedName", project.getPublishedName());
		v.put("urlName", project.getUrlName());
		v.put("publishedMetadata", project.getPublishedMetadata());

		if (project.getStatusId() != null && !project.getStatusId().equals(0)) {
			v.put("statusId", project.getStatusId());
		} else {
			v.put("statusId", null);
		}


		v.put("selfPrice", project.getSelfPrice());
		v.put("version", project.getVersion());
		v.put("versionBuildNumber", project.getVersionBuildNumber());
		v.put("versionDot", project.getVersionDot());
		v.put("versionMajor", project.getVersionMajor());
		v.put("versionMinor", project.getVersionMinor());
		v.put("height", project.getHeight());
		v.put("width", project.getWidth());
		v.put("accountTypeId", project.getAccountTypeId() == 0? null : project.getAccountTypeId());
		v.put("urlName", project.getUrlName());
		v.put("asyncInProgress", project.isAsyncInProgress());
		insert.execute(v);
		return project;
	}


	@Transactional(readOnly = false)
	public void update(Project p) throws DataAccessException {
		Assert.notNull(p);
		String sql = "update projects set " +
				"accountId = ?, " +
				"authormemberId = ?, " +
				"autosave = ?, " +
				"categoryId = ?, " +
				"content = ?, " +
				"currencyTypeId = ?, " +
				"retired = ?, " +
				"description = ?, " +
				//				"editedDateTime = ?, " +
				"featured = ?, " +
				"gadgetSize = ?, " +
				"guestListId = ?, " +
				"hideInStoreList = ?, " +
				"initialValues = ?, " +
				"inviteOnly = ?, " +
				"licenseTypeId = ?, " +
				"nextVersionId = ?, " +
				"price = ?, " +
				"projectMetadata = ?, " +
				"projectName = ?, " +
				"projectTypeId = ?, " +
				"published = ?, " +
				"publishedMetadata = ?, " +
				"publishedName = ?, " +
				"statusId = ?, " +
				"selfPrice = ?, " +
				"version = ?, " +
				"versionBuildNumber = ?, " +
				"versionDot = ?, " +
				"versionMajor = ?, " +
				"versionMinor = ?, " +
				"nextPublishedVersionId = ?, " +
				"accountTypeId = ?, " +
				"asyncInProgress = ? " +
				"where projectId = ?";

		// find me some NPEs
		//		log.debug(p.getAccountId());
		//		log.debug(p.getAuthorMemberId());
		//		log.debug(p.isAutoSave());
		//		log.debug(p.getCategoryId());
		//		log.debug(p.getContent());
		//		log.debug(DateUtils.toSqlDate(p.getCreatedDateTime()));
		//		log.debug(p.getCurrencyType() == null? null : p.getCurrencyType().dbId);
		//		log.debug(p.isDeleted());
		//		log.debug(p.getDescription());
		//		log.debug(DateUtils.toSqlDate(new Date()));
		//		log.debug(p.isFeatured());
		//		log.debug(p.getGadgetSize());
		//		log.debug(p.getGuestListId() != null && p.getGuestListId() == 0? null : p.getGuestListId());
		//		log.debug(p.getNextVersionId());
		//		log.debug(p.getPrice());
		//		log.debug(p.getProjectMetadata());
		//		log.debug(p.getProjectName());
		//		log.debug(p.getProjectTypeId());
		//		log.debug(p.isPublished());
		//		log.debug(p.getPublishedMetadata());
		//		log.debug(p.getPublishedName());
		//		log.debug(p.getStatusId());
		//		log.debug(p.getSelfPrice());
		//		log.debug(p.getVersion());
		//		log.debug(p.getVersionBuildNumber());
		//		log.debug(p.getVersionDot());
		//		log.debug(p.getVersionMajor());
		//		log.debug(p.getVersionMinor());
		//		log.debug(p.getProjectId());

		Object[] parms = new Object[] {
				p.getAccountId(),
				p.getAuthorMemberId(),
				p.isAutoSave(),
				p.getCategoryId() == 0? null : p.getCategoryId(),
						p.getContent(),
						p.getCurrencyType() == null? null : p.getCurrencyType().dbId,
								p.isDeleted(),
								p.getDescription(),
								//			DateUtils.toSqlDate(new Date()),
								p.isFeatured(),
								p.getGadgetSize(),
								p.getGuestListId() != null && p.getGuestListId() == 0? null : p.getGuestListId(),
										p.isHideInStoreList(),
										p.getInitialValues(),
										p.isInviteOnly(),
										p.getLicenseTypeId() == null || p.getLicenseTypeId() == 0 ? null : p.getLicenseTypeId(),
												p.getNextVersionId(),
												p.getPrice(),
												p.getProjectMetadata(),
												p.getProjectName(),
												p.getProjectTypeId(),
												p.isPublished(),
												p.getPublishedMetadata(),
												p.getPublishedName(),
												p.getStatusId() != null && p.getStatusId() == 0? null : p.getStatusId(),
														p.getSelfPrice(),
														p.getVersion(),
														p.getVersionBuildNumber(),
														p.getVersionDot(),
														p.getVersionMajor(),
														p.getVersionMinor(),
														p.getNextPublishedVersionId(),
														p.getAccountTypeId() == 0? null : p.getAccountTypeId(),
																p.isAsyncInProgress(),
																p.getProjectId()
		};

		if (log.isDebugEnabled()) {
			String out = "update:  [" + sql + "]  values [";
			for (Object o : parms) {
				out += o + ",";
			}
			out += "]";
			log.debug(out);
		}

		try {
			template.update(sql, parms);
		} catch (CannotSerializeTransactionException cst) {
			throw new ConcurrencyException("Detected concurrent access to Gadget", p.getProjectId());
		} catch (Exception e) {
			log.error(e);
			if (e.getMessage().indexOf("ix_unique_urlname_zapp") != -1 || e.getMessage().indexOf("ix_unique_urlname_gadget") != -1) {
				throw new UniqueConstraintException(p.getProjectId(), "urlName");
			}
			throw new DataAccessException(p.getProjectId());
		}
	}

	
	@Transactional(readOnly = false)
	public void updateFeatured(Project p) throws DataAccessException {
		Assert.notNull(p);
		String sql = "update projects set " +
				"featured = ? " +
				"where projectId = ?";

		Object[] parms = new Object[] {
			p.isFeatured(),
			p.getProjectId()
		};

		if (log.isDebugEnabled()) {
			String out = "updateFeatured:  [" + sql + "]  values [";
			for (Object o : parms) {
				out += o;
			}
			out += "]";
			log.debug(out);
		}

		try {
			template.update(sql, parms);
		} catch (CannotSerializeTransactionException cst) {
			throw new ConcurrencyException("Detected concurrent access to Gadget", p.getProjectId());
		} catch (Exception e) {
			log.error(e);
			if (e.getMessage().indexOf("ix_unique_urlname_zapp") != -1 || e.getMessage().indexOf("ix_unique_urlname_gadget") != -1) {
				throw new UniqueConstraintException(p.getProjectId(), "urlName");
			}
			throw new DataAccessException(p.getProjectId());
		}
	}
	
	
	@Transactional(readOnly = false)
	public void retireAllVersions(AppUser user, Project project) throws NotFoundException {
		Assert.notNull(user);
		Assert.notNull(project);

		String sql = "update projects " +
				"set retired = true " +
				"where projectName = (select projectName from projects where projectId = ?) " +
				"and authorMemberId = ?";

		Object[] parms = new Object[] {project.getProjectId(), user.getId()};
		if (log.isDebugEnabled()) {
			String debug = "retireAllVersions: [" + sql + "], [";
			for (Object o : parms) {debug += o + ",";}
			log.debug(debug + "]");
		}

		try {
			template.update(sql, parms);
		} catch (Exception e) {
			log.error(e);
			throw new DataAccessException(e.getMessage());
		}
	}


	@Transactional(readOnly=true)
	public boolean nameUnique(String name, ProjectType type,
			boolean isPublished, AppUser user) {

		String sql = null;
		Object[] parms = null;

		if (isPublished) {
			sql = "select count(*) from projects " +
					"where name = ? and projectType = ? and published = ?";
			parms = new Object[] {name, type.id};
		} else {
			sql = "select count(*) from projects " +
					"where name = ? and projectType = ? and published = ? " +
					"and authorMemberId = ?";
			parms = new Object[] {name, type.id, user.getId()};
		}

		if (log.isDebugEnabled()) {
			String debug = "nameUnique: [" + sql + "], [";
			for (Object o : parms) {debug += o + ",";}
			log.debug(debug + "]");
		}

		int count = template.queryForInt(sql, parms);
		return count == 0;
	}


	@Transactional(readOnly = true)
	public int calcRoyalties(String projectId) throws DataAccessException {
		int minPrice = 0;

		//FIXME: Review this code against PublishedGadgetDaoImpl.calcRoyaltiesForGadget()
		String sql = "select coalesce (sum(p2.selfPrice), 0) from " +
				"projects p " +
				"inner join parent_child pc on pc.parent_children_id = p.projectId " +
				"inner join projects p2 on p2.projectId = pc.child_id " +
				"inner join licensetypes lt on lt.licensetypeid = p2.licensetypeid " +
				"and lt.editable = false " +
				"where p.projectId = ? " +
				"and p.projectTypeId = 1 " + 
				"and p2.published = true";

		log.debug("calcRoyalties: [" + sql + "], [" + projectId + "]");

		try {
			minPrice = template.queryForInt(sql, projectId);
		} catch (Exception e) {
			log.error(e);
			throw new DataAccessException(projectId);
		}

		return minPrice;
	}


	@Transactional(readOnly = true)
	public List<Project> getChildrenFor(Project project, AppUser searcher)
			throws NotFoundException {
		Assert.notNull(project);

		//			p = template.queryForObject(sql, new Object[] {projectId}, mapper);
		ProjectQuery q = new ProjectQuery();
		q.setProjectIdList(project.getChildrenIds());

		ProjectListSearchResultSet projects = searchHelper.search(q, searcher);

		return projects.projects;
	}


	@Transactional(readOnly = false)
	public void addToChildren(Project parent, String childId, AppUser author)
			throws DataAccessException {
		Assert.notNull(parent);
		Assert.notNull(childId);

		log.debug("addToChildren:  parent: [" + parent.getProjectId() + "]  Child: [" + childId + "]");

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("parent_children_id", parent.getProjectId());
		params.put("child_id", childId);

		childInsert.execute(params);
	}


	@Transactional(readOnly = false)
	public void renameAllProjectVersions(String appUserId, String projectId,
			String newName) throws DataAccessException {
		Assert.notNull(appUserId);
		Assert.notNull(projectId);
		Assert.notNull(newName);

		String sql = "update projects " +
				"set projectName = ? " +
				"where projectName = (select projectName from projects where projectId = ?) " +
				"and authorMemberId = ?";

		Object[] parms = new Object[] {newName, projectId, appUserId};
		if (log.isDebugEnabled()) {
			String debug = "renameAllProjectVersions: [" + sql + "], [";
			for (Object o : parms) {debug += o + ",";}
			log.debug(debug + "]");
		}

		try {
			template.update(sql, parms);
		} catch (Exception e) {
			log.error(e);
			throw new DataAccessException(e.getMessage());
		}
	}


	@Transactional
	public boolean projectNameUnique(AppUser user, String projectName) throws DataAccessException {
		boolean ret = false;
		String sql = "select count (*) from projects " +
				"where projectName = ? and authorMemberId = ? " +
				"and published = false";
		Object[] parms = {projectName, user.getId()};

		if (log.isDebugEnabled()) {
			String debug = "findPublishedByNameAndtype: [" + sql + "], [";
			for (Object o : parms) {debug += o + ",";}
			log.debug(debug + "]");
		}

		try {

			int count = template.queryForInt(sql, parms);
			ret = count > 0;
		} catch (Exception e) {
			throw new DataAccessException("oh noes!");
		}

		return ret;
	}
	
	@Transactional
	public boolean projectNameUnretiredUnique(AppUser user, String projectName) throws DataAccessException {
		boolean ret = false; 
		String sql = "select count (*) from projects " +
				"where projectName = ? and authorMemberId = ? " +
				"and published = false " +
				"and retired = false";
		Object[] parms = {projectName, user.getId()};

		if (log.isDebugEnabled()) {
			String debug = "projectNameUnretiredUnique: [" + sql + "], [";
			for (Object o : parms) {debug += o + ",";}
			log.debug(debug + "]");
		}

		try {

			int count = template.queryForInt(sql, parms);
			ret = count > 0;
		} catch (Exception e) {
			throw new DataAccessException("oh noes!");
		}

		return ret;
	}

	
	@Override
	@Transactional
	public MiniProjectListSearchResultSet searchMiniStuff(ProjectQuery q,
			AppUser searcher) throws DataAccessException {

		return searchHelper.searchMiniStuff(q, searcher);
	}

	@Transactional
	public MyProjectListSearchResultSet searchMyStuff(ProjectQuery q,
			AppUser searcher) throws DataAccessException {

		return searchHelper.searchMyStuff(q, searcher);
	}

	@Transactional
	public TinyProjectListSearchResultSet searchTinyStuff(ProjectQuery q,
			AppUser searcher) throws DataAccessException {

		return searchHelper.searchTinyStuff(q, searcher);
	}

	@Transactional
	public int getProjectType(String projectId) throws NotFoundException {
		String sql = "select projectTypeId from projects where projectId = ?";
		try {
			return template.queryForInt(sql, projectId);
		} catch (Exception e) {
			throw new NotFoundException(projectId);
		}
	}


	@Transactional
	public int setNextVersionIdTo(Project p) {
		String sql = "update projects set nextversionid = ? where nextVersionId is null" +
				" and projectName    = ?" +
				" and authorMemberId = ?" +
				" and projectTypeId    = ?";
		int rowCount = 0;
		rowCount = template.update(sql, p.getProjectId(), p.getProjectName(), p.getAuthorMemberId(), p.getProjectTypeId());
		return rowCount;
	}


	@Transactional
	public int setNextPublishedVersionIdTo(Project p) {
		String sql = "update projects set nextPublishedVersionId = ? where nextPublishedVersionId is null" +
				" and urlName         = ?" +
				" and authorMemberId = ?" +
				" and projectTypeId     = ?";
		int rowCount = 0;
		rowCount = template.update(sql, p.getProjectId(), p.getUrlName(), p.getAuthorMemberId(), p.getProjectTypeId());
		return rowCount;
	}



}
