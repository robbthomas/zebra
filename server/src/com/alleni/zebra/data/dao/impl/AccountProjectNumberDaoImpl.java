package com.alleni.zebra.data.dao.impl;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.alleni.zebra.util.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.AccountProjectNumberDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.mapper.AccountProjectNumberMapper;
import com.alleni.zebra.data.mapper.ProductCodeMapper;
import com.alleni.zebra.data.model.AccountProjectNumber;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.CurrencyType;
import com.alleni.zebra.data.model.ProductCode;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.util.UUID;

@Repository("purchasedPublishedProjectDao")
public class AccountProjectNumberDaoImpl implements
		AccountProjectNumberDao {
	Logger log = Logger.getLogger(AccountProjectNumberDaoImpl.class);
	
	@Autowired AccountProjectNumberMapper mapper;
	@Autowired ProductCodeMapper productCodeMapper;
	
	@Autowired JdbcTemplate template;
	private SimpleJdbcInsert insert;
	private SimpleJdbcInsert invoiceInsert;
	private SimpleJdbcInsert lineItemInsert;
	
	@PostConstruct
	public void init() {
		this.insert = new SimpleJdbcInsert(template).withTableName(TABLE_NAME);
		this.invoiceInsert = new SimpleJdbcInsert(template).withTableName("invoices").usingGeneratedKeyColumns("invoiceid");
		this.lineItemInsert = new SimpleJdbcInsert(template).withTableName("invoicelineitems").usingGeneratedKeyColumns("invoicelineitemid");
	}

	@Override
	@Transactional
	public AccountProjectNumber create(AccountProjectNumber ppp) {
		Map<String, Object> args = new HashMap<String, Object>();
		ppp.setUuid(UUID.hibernateCompatibleUUID());
        if(ppp.getCreatedDateTime() == null) {
            ppp.setCreatedDateTime(new Date());
        }
		args.put("accountProjectNumberId", ppp.getUuid());
		args.put("accountId", ppp.getAccountId());
		args.put("projectId", ppp.getProjectId());
        args.put("invoicelineitemid", ppp.getInvoiceLineItemId());
		args.put("retired", ppp.getRetired());
		args.put("retiredDateTime", ppp.getRetiredDateTime());
		args.put("retiredReasonId", ppp.getRetiredReasonId());
		args.put("createdDateTime", ppp.getCreatedDateTime());
		insert.execute(args);
		return ppp;
	}

	@Override
	@Transactional(readOnly=true)
	public AccountProjectNumber find(AccountProjectNumber ppp) throws NotFoundException {
		AccountProjectNumber found = null;
		List<AccountProjectNumber> candidates = Collections.emptyList();
		
		
		String sql = "select distinct * from AccountProjectNumbers where accountId = ? and projectId = ? order by retired asc, retireddatetime desc";
		log.debug(" sql: " + sql + ";  args:  " + ppp.getAccountId() + ", " +  ppp.getProjectId());
		try {
				candidates = template.query(sql, new Object[] {ppp.getAccountId(), ppp.getProjectId()}, mapper);
		} catch (EmptyResultDataAccessException e) {
			// compound id has to be one number, w/o strings. Unless we want to mess w/ the exception definitions and handling...
			String compoundId = ppp.getAccountId().toString() + "99999" + ppp.getProjectId();
			throw new NotFoundException(Long.valueOf(compoundId));
		}
		if (candidates.size() > 0) {
			log.debug("found [" + candidates.size() + "] rows for query");
			found = candidates.get(0);
		} else {
			String compoundId = ppp.getAccountId().toString() + "99999" + ppp.getProjectId();
			throw new NotFoundException(Long.valueOf(compoundId));
		}
			
		return found;
	}

	
	@Override
	@Transactional(readOnly = true)
	public AccountProjectNumber findByUuid(String uuid) throws NotFoundException {
		AccountProjectNumber found = null;
		
		String sql = "select * from AccountProjectNumbers where accountprojectnumberid = ?";
		log.debug(" sql: " + sql + ";  args:  " + uuid);
		try {
				found = template.queryForObject(sql, new Object[] {uuid}, mapper);
		} catch (EmptyResultDataAccessException safeToIgnore) {
			throw new NotFoundException(uuid);
		}
		return found;
	}
	
	

	@Override
	@Transactional
	public void delete(AccountProjectNumber ppp, AppUser user) throws NotFoundException {
		String sql = "update AccountProjectNumbers set retired = 1, retiredbyid = ?, retiredreasonid = 3 where accountprojectnumberid = ?"; // retiredreasonid:3 == by user
		log.debug("unpublishing: " + ppp.getUuid());
		
		int rows = template.update(sql, user.getId(), ppp.getUuid());

		if (rows == 0)
			throw new NotFoundException(ppp.getUuid());
		
		log.debug("   unpublish okay.");
	}

	
	@Transactional
	public Long createInvoice(long accountId, String description, Date holdingDate) {
        Date createdDate = new Date();
        if(holdingDate == null) {
            holdingDate = createdDate;
        }
		HashMap<String,Object> args = new HashMap<String,Object>();
		args.put("accountId", accountId);
		args.put("currencytypeId", CurrencyType.US_DOLLAR.dbId);
		args.put("description", description);
        args.put("createddatetime", createdDate);
		args.put("holdingdatetime", holdingDate);
		args.put("stateid", 1);
		Number id = invoiceInsert.executeAndReturnKey(args);
		return id.longValue();
	}


    @Transactional
    public void updateInvoiceCachedTotal(long invoiceId) throws DataAccessException {
        String sql = "update invoices \n" +
                " set cachedTotal = (" +
                "   select sum(invoiceLineItems.amountTotal)" +
                "   from invoiceLineItems" +
                "   where invoiceLineItems.invoiceId = invoices.invoiceId" +
                " )\n" +
                " where invoiceId = ?";
        int rows = 0;
        try {
            rows = template.update(sql, invoiceId);
            log.debug("updateInvoiceCachedTotal updated [" + rows + "] rows	");
        } catch (Exception e) {
            log.error(e);
            throw new DataAccessException("could not update invoice: " + e.getMessage());
        }

        if (rows == 0) {
            throw new NotFoundException("could not update invoice: no row found");
        }
    }

	@Transactional
	public Long createLineItem(long invoiceId, int lineNumber, int quantity,
			int amountInPennies, ProductCode productCode, String description, String projectId) {
		HashMap<String,Object> args = new HashMap<String,Object>();
		args.put("invoiceId", invoiceId);
		args.put("lineNumber", lineNumber);
		args.put("quantity", quantity);
		args.put("amounttotal", amountInPennies);
		args.put("productcodeid", productCode != null? productCode.getProductCodeId() : null);
		args.put("description", description);
        args.put("projectid", projectId);
		Number id = lineItemInsert.executeAndReturnKey(args);
		return id.longValue();
	}


	
	@Transactional(readOnly = true)
	@Override
	public List<ProductCode> getAllProductCodes() {
		List<ProductCode> ret = Collections.emptyList();
		String sql = "select \n" +
				"p.productCodeId, p.productCategoryId, p.refundDays, p.price, p.productTypeId, p.discounttype, p.tag, p.description, \n" +
				"pc.tag as productCategoryTag, pt.tag as productTypeTag \n" +
				"from ProductCodes p \n" +
				"left join ProductTypes pt on pt.productTypeId = p.productTypeId \n" +
				"left join ProductCategories pc on pc.productCategoryId = p.productCategoryId \n" +
				"where effDate < now() and termDate > now()";
		log.debug(sql);
		try {
			ret = template.query(sql, productCodeMapper);
		} catch (Exception e) {
			log.error(e);
		}
		return ret;
	}

	@Override
	@Transactional(readOnly=false)
	public void updateProject(AppUser user, String apnUUID, String projectId)
			throws DataAccessException {
		String sql = "update AccountProjectNumbers \n" +
				" set projectId = ?\n" +
				" where accountProjectNumberId = ?" +
				" and accountId = ?";
		int rows = 0;
		try {
			rows = template.update(sql, projectId, apnUUID, user.getAccountId());
			log.debug("upgraded [" + rows + "] rows	");
		} catch (Exception e) {
			log.error(e);
			throw new DataAccessException("could not upgrade project: " + e.getMessage());
		}
		
		if (rows == 0) {
			throw new NotFoundException("could not upgrade project: no row found");
		}

	}

	@Override
	@Transactional
	public AccountProjectNumber findForUserAndUrlName(AppUser user,
			String urlName) throws DataAccessException {
		AccountProjectNumber ret = null;
		String sql = "select userApn.* \n" +
				" from projects authorProjects\n" +
				"     inner join accountprojectnumbers userApn\n" +
				"        on userApn.projectId = authorProjects.projectId\n" +
				"            and userApn.accountId <> authorProjects.accountId \n" +  		// don't update projects I authored
				"            and userApn.retired = 0 \n" +                            		// exclude retired APN rows
				"            and authorProjects.nextPublishedVersionid is not null \n" + 	// don't bother updating projects that are the latest version
				" where \n" + 
				" userApn.accountId = ? \n" +
				" and authorProjects.urlName = ?";
		
		log.debug(sql);
		try {
			ret = template.queryForObject(sql, new Object[]{user.getAccountId(), urlName}, mapper);
		} catch (Exception e) {
			log.error(e);
			throw new DataAccessException(urlName, e.getMessage());
		}
		return ret;
		
	}

	@Transactional
	public int unPurchaseOldVersionsForAuthor(Project p) {
		int rowCount = 0;
		String apnSql = "update AccountProjectNumbers " +
				 "set retired = 1, retiredbyid = ?, retiredreasonid = 3" +
				 "from projects " +
				 "where " +
				 " projects.nextPublishedVersionId is null " +
				 " and projects.projectid = accountprojectnumbers.projectid " +
				 " and accountprojectnumbers.accountid = projects.accountId " +
				 " and projects.urlName = ? " +
				 " and projects.authorMemberId = ? " +
				 " and projects.projectTypeId = ?" +
				 " and projects.published = true";

		String authorMemberId = p.getAuthorMemberId();
		String urlName = p.getUrlName();
		int projectTypeId = p.getProjectTypeId();
		log.debug(" upurchase SQL: " + apnSql + " \n args: " + authorMemberId + ", " +
				urlName + ", " + authorMemberId + ", " + projectTypeId); 
				
		rowCount = template.update(apnSql, p.getAuthorMemberId(), p.getUrlName(), p.getAuthorMemberId(), p.getProjectTypeId());
		return rowCount;
	}

	

}
