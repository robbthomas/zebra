package com.alleni.zebra.data.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.GadgetCategoryDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.mapper.GadgetCategoryMapper;
import com.alleni.zebra.data.model.GadgetCategory;

@Repository("gadgetCategoryDao")
public class GadgetCategoryDaoImpl implements GadgetCategoryDao {
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired JdbcTemplate template;
	@Autowired GadgetCategoryMapper mapper;

	@Override
	@Transactional(readOnly=true)
	public GadgetCategory findById(Long id) throws NotFoundException {
		String sql = "select * from " + TABLE_NAME +" WHERE categoryId = ?";
		log.debug("looking for category: " + id);
		
		GadgetCategory ret = null;
		try {
			ret = template.queryForObject(sql, new Long[] {id}, mapper);

		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(e, id);
		}

		return ret;
	}

	
	
	@Override
	@Transactional(readOnly=true)
	public GadgetCategory findByName(String name) throws NotFoundException {
		String sql = "select * from " + TABLE_NAME +" WHERE name = ?";

		GadgetCategory ret = null;
		try {
			ret = template.queryForObject(sql, new String[] {name}, mapper);

		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(e, name);
		}

		return ret;
	}



	@Override
	@Transactional
	public List<GadgetCategory> findAll() {
		String sql = "select * from categories";
		try {
			return template.query(sql, mapper);
		} catch (IncorrectResultSizeDataAccessException ignore) {
			return null;
		}
	}

}
