package com.alleni.zebra.data.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.ProjectTransferDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.mapper.ProjectTransferMapper;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.ProjectTransfer;
import com.alleni.zebra.util.DateUtils;

@Repository("projectTransferDao")
public class ProjectTransferDaoImpl implements ProjectTransferDao {
	Logger log = Logger.getLogger(ProjectTransferDaoImpl.class);
	
	private static final String FROM_JOIN_CLAUSE = "select ProjectMemberAccess.*," +
				" au.displayName as senderDisplayName," +
				" au2.displayName as receiverDisplayName," +
				" au.email as senderEmail," +
				" au2.email as receiverEmail," +
				" a.name as senderAccountName," +
				" a2.name as receiverAccountName," +
				" projects.price," +
				" projects.projectname," +
				" RetiredReasonTypes.name as retiredReasonCode," +
				" publishImages.iconFileId" +
				" from ProjectMemberAccess" +
				"   inner join app_user au on au.id = ProjectMemberAccess.senderMemberid" +
				"   inner join accounts a on a.accountid = ProjectMemberAccess.senderAccountId" +
				"   inner join app_user au2 on au2.id = ProjectMemberAccess.receiverMemberId" +
				"   inner join accounts a2 on a2.accountid = ProjectMemberAccess.receiverAccountId" +
				"   inner join projects on projects.projectid = ProjectMemberAccess.projectid" +
				"   left outer join RetiredReasonTypes on RetiredReasonTypes.retiredReasonTypeId = ProjectMemberAccess.retiredReasonTypeId" +
				"   left outer join publishimages on publishimages.projectid = projects.projectid  and iconFileId is not null";
	
	
	@Autowired ProjectTransferMapper mapper;
	@Autowired JdbcTemplate template;
	
	private SimpleJdbcInsert insert;
	
	@PostConstruct
	public void init() {
		this.insert = 
				new SimpleJdbcInsert(template.getDataSource())
						.withTableName(TABLE_NAME)
						.usingGeneratedKeyColumns("projectmemberaccessid");
	}

	
	@Transactional(readOnly = true)
	public ProjectTransfer findByProjectMemberAccessId(
			Long projectMemberAccessId) throws NotFoundException {
		ProjectTransfer pt = null;
		String sql = FROM_JOIN_CLAUSE + " where projectMemberAccessId = ?";
		log.debug(sql);
		log.debug("params: " + projectMemberAccessId);
		
		try {
			pt = template.queryForObject(sql, new Object[] {projectMemberAccessId}, mapper);
			
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(projectMemberAccessId);
		}
		return pt;
	}


	
	@Transactional(readOnly = true)
	public List<ProjectTransfer> list(AppUser user, boolean incoming) {
//		String sql = FROM_JOIN_CLAUSE +
//				(incoming?	" where ProjectMemberAccess.receiverMemberId = ? " : " where ProjectMemberAccess.sendermemberid = ?" +
//						" and ProjectMemberAccess.retiredReasonCode <> ");
		
		String whereClause = " where true";
		List<Object> params = new ArrayList<Object>();
		
		if (incoming) {
			// we want ones with my receiverMemberId
			// and we don't want ones that have been cancelled by me
			// or have been completed
			// or have been canceled by the sender and marked as "cleared" by me
			whereClause += " and ProjectMemberAccess.receiverMemberId = ?";
			whereClause += " and (ProjectMemberAccess.retiredreasontypeid is null or ProjectMemberAccess.retiredreasontypeid <> 6)";
			whereClause += " and ProjectMemberAccess.hideFromReceiver = false";
			params.add(user.getId());
		} else {
			// we want ones with my senderMemberId
			// and we don't want ones that have been cancelled by me
			// or have been canceled by the receiver and marked as "cleared" by me
			whereClause += " and ProjectMemberAccess.senderMemberId = ?";
			whereClause += " and ProjectMemberAccess.hideFromSender = false";
			
			params.add(user.getId());
		}
		
		String orderByClause = " ORDER BY projectMemberAccess.createdDateTime DESC";
		
		String sql = FROM_JOIN_CLAUSE + whereClause + orderByClause;
		
		log.debug("incoming: [" + incoming + "]   memberId: [" + user.getId() + "]  sql: " + sql);
		
		List<ProjectTransfer> ret = Collections.emptyList();
		try {
			ret = template.query(sql, params.toArray(), mapper);
		} catch (EmptyResultDataAccessException ignore) {
			//return empty list
		}
		
		return ret;
	}

	

	public void update(ProjectTransfer pt, AppUser user) throws NotFoundException {
		String sql = "update ProjectMemberAccess set" +
				" senderAccountid = ?," +
				" senderMemberId = ?," +
				" receiverAccountId = ?," +
				" receiverMemberId = ?," +
//				" createdDateTime = ?," +
				" projectId = ?," +
				" retired = ?," +
				" retiredDateTime = ?," +
				" retiredById = ?, " +
				" retiredreasontypeid = ?," +
				" newToReceiver = ?," +
				" viewedDateTime = ?," +
				" hideFromSender = ?," +
				" hideFromReceiver = ?" +
				" where projectMemberAccessId = ?";
		
		int rows = template.update(sql,
				pt.getSenderAccountId(),
				pt.getSenderMemberId(),
				pt.getReceiverAccountId(),
				pt.getReceiverMemberId(),
//				DateUtils.toSqlDate(pt.getCreatedDateTime()),
				pt.getProjectId(),
				pt.isRetired()? 1 : 0,
				DateUtils.toSqlDate(pt.getRetiredDateTime()),
				pt.getRetiredById(),
				pt.getRetiredReasonTypeId() == 0? null : pt.getRetiredReasonTypeId(),
				pt.isNew()? 1 : 0,
				DateUtils.toSqlDate(pt.getViewedDateTime()),
				pt.isHideFromSender(),
				pt.isHideFromReceiver(),
				pt.getProjectMemberAccessId());
		if (rows == 0) {
			throw new NotFoundException(pt.getProjectMemberAccessId());
		}
	}

	@Transactional(readOnly = false)
	public ProjectTransfer create(ProjectTransfer pt, AppUser user) {
		Map<String,Object> parms = new HashMap<String,Object>();
		parms.put("senderAccountId", pt.getSenderAccountId());
		parms.put("senderMemberId", pt.getSenderMemberId());
		parms.put("receiverAccountId", pt.getReceiverAccountId());
		parms.put("receiverMemberId", pt.getReceiverMemberId());
		parms.put("createdDateTime", DateUtils.toSqlDate(new Date()));
		parms.put("projectId", pt.getProjectId());
		parms.put("retired", pt.isRetired()? 1 : 0);
		parms.put("retiredDateTime", DateUtils.toSqlDate(pt.getRetiredDateTime()));
		parms.put("retiredById", pt.getRetiredById());
		
		int reasonCode = pt.getRetiredReasonTypeId();
		if (reasonCode > 0) {
			parms.put("retiredreasontypeid", reasonCode);
		}
		
		parms.put("newToReceiver", 1);
		parms.put("viewedDateTime", DateUtils.toSqlDate(pt.getViewedDateTime()));
		parms.put("licenseTypeId", pt.getLicenseTypeId());
		parms.put("hideFromSender", pt.isHideFromSender());
		parms.put("hideFromReceiver", pt.isHideFromReceiver());
		
		Number id = insert.executeAndReturnKey(parms);
		pt.setProjectMemberAccessId(id.longValue());
		return pt;
	}

	@Transactional(readOnly = false)
	public void delete(ProjectTransfer pt, AppUser user) throws NotFoundException {
		pt.setRetired(true);
		pt.setRetiredDateTime(new Date());
		pt.setRetiredById(pt.getRetiredById());
		update(pt, user);
	}

	@Transactional(readOnly = false)
	public void markTransferComplete(long projectMemberAccessId) throws DataAccessException {
		String sql = "update ProjectMemberAccess " +
				"set retired = 1, retiredDateTime = now(), retiredReasonTypeId = 6, " +
				"hideFromReceiver = true " +
				"where projectMemberAccessId = ?";
		
		try {
			template.update(sql, projectMemberAccessId);
		} catch (Exception e) {
			log.error(e);
			throw new DataAccessException("error retiring transfer");
		}
	}

}
