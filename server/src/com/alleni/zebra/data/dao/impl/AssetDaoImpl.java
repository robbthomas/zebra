package com.alleni.zebra.data.dao.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alleni.zebra.data.dao.AssetDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.mapper.AssetMapper;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.data.util.UUID;
import com.alleni.zebra.util.DateUtils;

@Repository("assetDao")
public class AssetDaoImpl implements AssetDao {
	
	protected final Log log = LogFactory.getLog(getClass());

    private SimpleJdbcInsert insertAsset;
    private SimpleJdbcInsert insertPreviewImage;

	@Autowired JdbcTemplate template;
	@Autowired AssetMapper assetMapper;

	@PostConstruct
	public void init() {
        this.insertAsset = new SimpleJdbcInsert(template).withTableName(TABLE_NAME);
        this.insertPreviewImage = new SimpleJdbcInsert(template).withTableName("PreviewImage").usingColumns("projectId", "assetId");
	}

	@Override
	@Transactional
	public Asset findById(String id) throws NotFoundException {
		log.debug("looking for asset id: " + id);
		Assert.notNull(id, "Id cannot be null");

		Asset asset = null;

		String sql = "select * from asset where deleted = false and id = ?";

		try {
			asset = template.queryForObject(sql, new Object[] {id}, assetMapper);
		} catch (Exception e) {
			throw new NotFoundException(e, id);
		}

		if (asset == null) {
			throw new NotFoundException("No result found", id);
		}
		return asset;
	}

	@Override
	@Transactional(noRollbackFor={NotFoundException.class, org.springframework.dao.EmptyResultDataAccessException.class})
	public Asset findByIdNoRollback(String id) throws NotFoundException {
		log.debug("looking for asset id: " + id);
		Assert.notNull(id, "Id cannot be null");

		Asset asset = null;

		String sql = "select * from asset where deleted = false and id = ?";

		try {
			asset = template.queryForObject(sql, new Object[] {id}, assetMapper);
		} catch (Exception e) {
			throw new NotFoundException(e, id);
		}

		if (asset == null) {
			throw new NotFoundException("No result found", id);
		}
		return asset;
	}

	@Override
   	@Transactional
   	public Asset findByIdAndAppUser(String id, String appUserId)
   			throws NotFoundException {
   		log.debug("looking for asset id: " + id + ", userId: " + appUserId);
   		Assert.notNull(id, "Id cannot be null");
   		Assert.notNull(appUserId, "appUserId cannot be null");

   		Asset asset = null;
   		String sql = "select * from asset where deleted = false and id = ? ";

   		try {
   			asset = template.queryForObject(sql, new Object[] {id}, assetMapper);
   		} catch (EmptyResultDataAccessException e) {
   			throw new NotFoundException(e, id);
   		}

   		if (asset == null) {
   			throw new NotFoundException("No result found", id);
   		}
   		return asset;
   	}

    public void setAsPreview(String projectId, Asset asset) {
        log.debug("Setting PreviewImage projectId: " + projectId + " assetId: " + asset.getId());
        Assert.notNull(projectId, "projectId cannot be null");
        Assert.notNull(asset.getId(), "assetId cannot be null");

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("projectId", projectId);
        params.put("assetId", asset.getId());

        if (log.isTraceEnabled()) {
            log.trace(" PreviewImage fields: ");
            for (String field : params.keySet()) {
                log.trace(field + ": " + params.get(field));
            }
        }
        insertPreviewImage.execute(params);
    }

    @Override
   	@Transactional
   	public Asset findPreviewByProject(String projectId) {
   		log.debug("looking for PreviewImage projectId: " + projectId);
   		Assert.notNull(projectId, "projectId cannot be null");

   		Asset asset = null;
   		String sql = "select Asset.* from PreviewImage" +
                   " inner join Asset on PreviewImage.assetId = Asset.id" +
                   " where PreviewImage.projectId = ? ";

   		try {
   			asset = template.queryForObject(sql, new Object[] {projectId}, assetMapper);
   		} catch (EmptyResultDataAccessException e) {
   			asset = null;
   		}
   		return asset;
   	}


	@Override
	@Transactional
	public List<Asset> findAll() {
		String sql = "select * from asset";
		List<Asset> assetList = template.query(sql, assetMapper);
		return assetList;
	}


	@Override
	@Transactional
	public List<Asset> findAllForUser(String appUserId) {
		String sql = "select * from asset where user_id = ?";
		List<Asset> assetList = template.query(sql, new Object[] {appUserId}, assetMapper);
		return assetList;
	}


	@Override
	@Transactional
	public Asset update(Asset asset) throws ConcurrencyException {
		String sql = "update asset set name = ?, data_size_in_bytes = ?," +
		" permissions = ?, deleted = ?, fileid = ?, next_version_id = ?" +
		" where id = ?";
		log.debug("sql: " + sql + " id:  " + asset.getId());
		try {
			template.update(sql, asset.getName(),
					asset.getDataSizeInBytes(), asset.getPermissions(), asset.isDeleted(), asset.getFileID(), asset.getNextVersionId(), asset.getId());
		} catch (EmptyResultDataAccessException e) {
			throw new ConcurrencyException("Detected concurrent access to Asset: " + asset.getId(), (String)asset.getId());
		}
		return asset;
	}

	@Override
	@Transactional
	public void delete(Asset asset) throws ConcurrencyException {
		String sql = "delete from asset where id = ? and version = ?";
		int rows = 0;
		try {
			rows = template.update(sql, asset.getId(), asset.getVersion());
		} catch (EmptyResultDataAccessException e) {
			throw new ConcurrencyException("Detected concurrent access to AppUser: " + asset.getId(), (String)asset.getId());
		}

		if (rows == 0) {
			throw new ConcurrencyException("Detected concurrent access to AppUser: " + asset.getId(), (String)asset.getId());
		}

	}

	@Override
	@Transactional
	public Asset create(Asset asset) {
		Assert.notNull(asset);
		Assert.isNull(asset.getId(), "Attempting to save existing Asset instance as new: [" + asset.getId() + "]");
		asset.setId(UUID.hibernateCompatibleUUID());

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("id", asset.getId());
		params.put("version", asset.getVersion());
		params.put("data_size_in_bytes", asset.getDataSizeInBytes());
		params.put("date_created", DateUtils.toSqlDate(asset.getDateCreated()));
		params.put("deleted", asset.isDeleted());
		params.put("fileid", asset.getFileID());
		params.put("last_updated", DateUtils.toSqlDate(asset.getDateCreated()));
		params.put("mime_type", asset.getMimeType());
		params.put("name", asset.getName());
		params.put("next_version_id", asset.getNextVersionId());
		params.put("permissions", asset.getPermissions());
		params.put("user_id", asset.getUserId());

		if (log.isTraceEnabled()) {
			log.trace(" Asset fields: ");
			for (String field : params.keySet()) {
				log.trace(field + ": " + params.get(field));
			}
		}
		insertAsset.execute(params);
		return asset;
	}

}
