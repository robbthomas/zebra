package com.alleni.zebra.data.dao.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.SharedProjectDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;

@Repository("sharedProjectDao")
public class SharedProjectDaoImpl implements SharedProjectDao {
	@Autowired JdbcTemplate template;

	@Transactional(readOnly = true)
	public boolean isGuestForProject(AppUser user, String projectId) throws DataAccessException {
		boolean ret = false;
		
		String sql = "select count(*) from projects p " +
			  " left join guestlistinvitees gli" +
			  "   on p.guestlistid = gli.guestlistid" +
			  "   and gli.email = ?" +
			  " where gli.guestlistinviteeid is not null" +
			  "       and p.projectId = ?";
		
		// SEE ProjectTransferServiceImpl
		try {
			int count = template.queryForInt(sql, new Object[] {user.getEmail(), projectId});
			ret = (count > 0);
		} catch (Exception e) {
			throw new DataAccessException(e, "error looking up invitee ["+user.getEmail()+"] for project ["+ projectId+"]");
		}
		return ret;
	}

	@Transactional(readOnly = true)
	public boolean isTransfereeForProject(AppUser user, String projectId) throws DataAccessException {
		boolean ret = false;
		
		String sql = "select count(*) from projects p " + 
				" left join projectmemberaccess pma" + 
				"   on pma.projectid = p.projectid" + 
				"   and pma.receivermemberid = ?" + 
				" where pma.projectmemberaccessid is not null" + 
				" and p.projectId = ?";
			
			try {
				int count = template.queryForInt(sql, new Object[] {user.getId(), projectId});
				ret = (count > 0);
			} catch (Exception e) {
				throw new DataAccessException(e, "error looking up transferee ["+user.getId()+"] for project ["+ projectId+"]");
			}
			return ret;
	}
	
	@Override
	@Transactional(readOnly = true)
	@Deprecated
	public List<String> getShareTypesForProject(String projectId)
			throws DataAccessException {
		List<String> types = Collections.emptyList();

		String sql = "select tag from sharetypes inner join sharedprojects" +
				"  on sharedprojects.sharetypeid = sharetypes.sharetypeid" +
				"  where sharedprojects.projectId = ?";
		try {
			types = template.queryForList(sql, String.class, new Object[] {projectId});
		} catch (Exception e) {
			throw new DataAccessException(e, "error looking up share types for project [" + projectId+"]");
		}

		
		return types;
	}

}
