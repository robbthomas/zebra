package com.alleni.zebra.data.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alleni.zebra.data.dao.RoleTypeDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.authorization.Role;

@Repository
public class RoleTypeDaoImpl implements RoleTypeDao {
	protected final Log log = LogFactory.getLog(getClass());
	@Autowired JdbcTemplate template;

	@Transactional(readOnly=true)
	@Override
	public List<Role> getRolesForUser(AppUser user) throws NotFoundException {
		Assert.notNull(user, "User cannot be null");
		
		List<Role> roles = new ArrayList<Role>();
		String sql = "select rt.roleTypeId from " + TABLE_NAME + " rt, app_user au, accountmemberroles amr where au.id = ? " +
			"and amr.memberid = au.id and rt.roleTypeId = amr.typeid";
		
		List<Integer> ids = null;
		try {
			ids = template.queryForList(sql, Integer.class, (String)user.getId());
			for (int i : ids) {
				Role r = Role.dbValueOf(i);
				if (r != null)
					roles.add(r);
			}
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(e, user.getUsername());
		}
		return roles;
	}

}
