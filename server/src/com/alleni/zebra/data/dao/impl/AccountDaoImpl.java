package com.alleni.zebra.data.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alleni.zebra.data.dao.AccountDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.mapper.AccountMapper;
import com.alleni.zebra.data.model.Account;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.util.UUID;
import com.alleni.zebra.util.DateUtils;

@Repository("accountDao")
public class AccountDaoImpl implements AccountDao {
	protected final Log log = LogFactory.getLog(getClass());


	@Autowired JdbcTemplate template;
	@Autowired AccountMapper mapper;

	
	
	private SimpleJdbcInsert insertAccount;


	@PostConstruct
	public void init() {
		this.insertAccount = new SimpleJdbcInsert(template).withTableName("Accounts").usingGeneratedKeyColumns("accountid");
	}

	@Override
	@Transactional(readOnly=false)
	public Long create(Account account) {
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("companyId", account.getCompanyId() == 0? null : account.getCompanyId());
		map.put("typeId", account.getTypeId());
		map.put("termDueDate", account.getTermDueDateTime());
		map.put("goodStanding", account.isGoodStanding()? 1 : 0);
		map.put("name", account.getName());
		map.put("tokenBalalance", account.getTokenBalance());
		map.put("editedById", account.getEditedById());
		map.put("retired", account.isRetired()? 1 : 0);
		map.put("retiredById", account.getRetiredById());
		map.put("retiredDateTime", account.getRetiredDateTime());
		map.put("merchantCustomerId", UUID.hibernateCompatibleUUID());
		map.put("typeId", account.getAccountType() != null? account.getAccountType().typeId : account.getTypeId());
		Number id = insertAccount.executeAndReturnKey(map);
		return id.longValue();
	}


	
	
	@Override
	@Transactional(readOnly=false)
	public List<Account> findAll() {

		List<Account> accounts = new ArrayList<Account>();
		String sql = "select a.*, c.name as companyName " + 
			" from Accounts a left join  companies c on a.companyId = c.companyId ";

		
		try {
			accounts = template.query(sql, mapper);
		} catch (EmptyResultDataAccessException ignore) {
			//return empty list
		}

		return accounts;
	}


	
	@Override
	@Transactional
	public Account findById(Long id) throws NotFoundException {
		log.debug("looking for id: " + id);

		Assert.notNull(id, "Id cannot be null");

		Account account = null;
		String sql = 	"SELECT a.*, companies.name AS companyName, addresses.company as addressCompanyName " +
						"FROM Accounts a " +
						"    LEFT JOIN companies" +
						"        ON a.companyId = companies.companyId" +
						"    LEFT JOIN  addresses" +
						"        INNER JOIN addressTelephoneTypes " +
						"            ON addresses.typeId = addressTelephoneTypes.addressTelephoneTypeId " +
						"            AND addressTelephoneTypes.tag = 'primary'" +
						"        ON a.accountId = addresses.accountId " +
						"WHERE a.accountId = ? ";
						
		try {
			account = template.queryForObject(sql, new Object[] {id}, mapper);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException(e, id);
		}

		return account;
	}

	@Override
	@Transactional(readOnly = true)
	public Account findAccountForAccountProjectNumberId(String uuid) {
		Account account = null;
		String sql = "select a.*, c.name as companyName from Accounts a " +
				"left join  companies c on a.companyId = c.companyId " +
				"inner join accountprojectnumbers apn on apn.accountid = a.accountid " + 
			"where apn.accountprojectnumberid = ?";

		log.debug("sql: " + sql + ", " + uuid);
		try {
			account = template.queryForObject(sql, new Object[] {uuid},mapper);
		} catch (EmptyResultDataAccessException ignore) {
			//return null
		}

		return account;
	}

	
	@Transactional
	public void setBanner(long accountId, UploadableAsset banner) {
		String sql = "update accounts set bannerId = ? where accountid = ?";
		template.update(sql, banner.getUploadableassetid(), accountId);
	}
	
	@Transactional(readOnly = false)
	public Account update(Account account, AppUser user) throws DataAccessException {
		Account old = findById(account.getId());
		boolean newRetired = (account.isRetired() && !old.isRetired());
		
		String sql = "update accounts set" +
				" companyId = ?," +
				" typeId = ?," +
				" termduedatetime = ?," +
				" goodstanding = ?," +
				" name = ?," +
//				" description = ?," +
				" tokenbalance = ?," +
				" editedbyid = ?," +
				" editeddatetime = ?," +
				" retired = ?, ";
		if (newRetired) { 
			sql += " retiredbyid = ?,"; 
			sql += " retireddatetime = ?,";
		}
		
		sql +=
//			" showcompanyinfoforall = ?," +
//				" companysameaspersonal = ?, " +
//				" billingaddresssameascompany = ?," +
//				" billingcontactsameascompany = ?," +
//				" paypallogin = ?," +
//				" payaccountfirst = ?," +
//				" convertamount = ?," +
//				" merchantcustomerid = ?," +
//				" convertmethod = ?," +
//				" contactpreference = ?," +
//				" allowsubscriptionpayment = ?," +
				" maxinvitesperproject = ?," +
				" maxinvitesperaccount = ?," +
				" numLearners = ?," +
				" nummembers = ?";
        sql += " where accountid = ?";
		
		List<Object> params = new ArrayList<Object>();
		params.add(account.getCompanyId() == 0? null : account.getCompanyId());
		params.add(account.getTypeId());
		params.add(DateUtils.toSqlDate(account.getTermDueDateTime()));
		params.add(account.isGoodStanding()? 1 : 0);
		params.add(account.getName());
//		params.add(account.getDescription());
		params.add(account.getTokenBalance());
		params.add(user.getId());
		params.add(DateUtils.toSqlDate(new Date()));
		params.add(account.isRetired()? 1 : 0);
		if (newRetired) { 
			params.add(user);
			params.add(user);
		}
		params.add(account.getMaxInvitesPerProject());
		params.add(account.getMaxInvitesPerAccount());
		params.add(account.getNumLearners());
        params.add(account.getNumMembers());
        params.add(account.getId());
		
		try {
			template.update(sql, params.toArray());
		} catch (Exception e) {
			throw new DataAccessException(account.getId());
		}
		return null;
	}

}
