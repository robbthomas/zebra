package com.alleni.zebra.data.dao.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.AccountMemberRoleDao;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.authorization.Role;

@Repository("accountMemberRoleDao")
public class AccountMemberRoleDaoImpl implements AccountMemberRoleDao {
	protected final Log log = LogFactory.getLog(getClass());

	@Autowired JdbcTemplate template;
	
	private SimpleJdbcInsert insertUser;

	@PostConstruct
	public void init() {
		this.insertUser = new SimpleJdbcInsert(template).withTableName("accountmemberroles");
	}

	@Override
	@Transactional
	public void addRolesToUser(AppUser user, Role... roles) {
		
		for (Role r : roles) {
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("accountId", user.getAccountId());
			params.put("memberId", user.getId());
			params.put("typeId", r.dbId);
			params.put("retired", 0);
			params.put("retiredById", null);
			params.put("retireddatetime", null);
			
			insertUser.execute(params);
		}

	}

}
