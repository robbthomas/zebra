package com.alleni.zebra.data.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alleni.zebra.data.dao.ProjectSearchHelper;
import com.alleni.zebra.data.dto.GenericProjectListSearchResultSet;
import com.alleni.zebra.data.dto.MiniProjectListSearchResultSet;
import com.alleni.zebra.data.dto.MyProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectQuery;
import com.alleni.zebra.data.dto.TinyProjectListSearchResultSet;
import com.alleni.zebra.data.mapper.MiniProjectMapper;
import com.alleni.zebra.data.mapper.MyProjectMapper;
import com.alleni.zebra.data.mapper.ProjectMapper;
import com.alleni.zebra.data.mapper.TinyProjectMapper;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.GenericProject;
import com.alleni.zebra.util.StringUtils;

@Repository("projectSearchHelper")
public class ProjectSearchHelperImpl implements ProjectSearchHelper {
	private final Logger log = Logger.getLogger(ProjectDaoImpl.class);

	@Autowired JdbcTemplate template;
	@Autowired ProjectMapper mapper;
	@Autowired MiniProjectMapper miniMapper;
	@Autowired MyProjectMapper myMapper;
	@Autowired TinyProjectMapper tinyMapper;
	
    @Transactional(readOnly=true)
    protected <P extends GenericProject> GenericProjectListSearchResultSet<P> search(ProjectQuery q, AppUser searcher, RowMapper<P> projectMapper, GenericProjectListSearchResultSet<P> emptySet) {
		Assert.notNull(q);

		List<String> tables = new ArrayList<String>();
		List<Object[]> clauses = new ArrayList<Object[]>();
		List<Object> values = new ArrayList<Object>();
		List<Object> countValues = new ArrayList<Object>();

		// assemble list of tables
		tables.add(" projects p");


		// left join because some published items may not have a record in these tables.
		// no harm in adding this to project and shoppe queries
		tables.add(" left outer join PublishedPurchasesAcquisitions ppa on p.projectId = ppa.projectId ");
		tables.add(" left outer join ZappRatingCaches zrc on p.projectId = zrc.projectId ");
		tables.add(" left outer join LicenseTypes on LicenseTypes.licenseTypeId = p.licenseTypeId");
		tables.add(" inner join app_user on app_user.id = p.authorMemberId");
		tables.add(" inner join accounts authorAccount on authorAccount.accountId = p.accountId");
		tables.add(" left outer join companies authorCompany on authorCompany.companyId = authorAccount.companyId");
		
		// Project Type
		tables.add(" left outer join accountTypes projectAccountType ON p.accountTypeId = projectAccountType.accountTypeId");

		// icon and four screenshots
		tables.add(" left outer join publishimages icon on icon.projectId = p.projectId and icon.iconfileid is not null and icon.retired = false");
		tables.add(" left outer join publishimages screenshot0 on screenshot0.projectId = p.projectId and screenshot0.displayOrder = 0 and screenshot0.screenshotfileid is not null and screenshot0.retired = false");
		tables.add(" left outer join publishimages screenshot1 on screenshot1.projectId = p.projectId and screenshot1.displayOrder = 1 and screenshot1.screenshotfileid is not null and screenshot1.retired = false");
		tables.add(" left outer join publishimages screenshot2 on screenshot2.projectId = p.projectId and screenshot2.displayOrder = 2 and screenshot2.screenshotfileid is not null and screenshot2.retired = false");
		tables.add(" left outer join publishimages screenshot3 on screenshot3.projectId = p.projectId and screenshot3.displayOrder = 3 and screenshot3.screenshotfileid is not null and screenshot3.retired = false");


		// both the publisher apn row (especially for store) and the searching user apn
		tables.add("  left outer join AccountProjectNumbers publisherApn on p.projectId = publisherApn.projectId and publisherApn.accountId = p.accountId");
		tables.add("  left outer join AccountProjectNumbers userApn on p.projectId = userApn.projectId and userApn.retired = 0 and userApn.accountId = ?");

        // fill in userApn account done in the select and the tables section. Ths is safe ordering because this will be the first processed clause
        long userAccountId = q.getOwnerAccountId() != null ? q.getOwnerAccountId() : searcher.getAccount().getId();
        clauses.add(new Object[]{"", userAccountId});
        clauses.add(new Object[]{"", userAccountId});

		if (q.getOwnerAccountId() != null) {
			clauses.add(new Object[]{ " and userApn.accountId is not null"}); // enforce the join to userApn worked
		} 

		// join to search for children of a project
		if(q.getParentProjectId() != null) {
			tables.add("  inner join parent_child pc on pc.child_id = p.projectId ");
			clauses.add(new Object[]{" and pc.parent_children_id = ? ", q.getParentProjectId()});
		}

		if(q.isForStoreList()) {
			// hide items in the store by authors who have downgraded to collector
			tables.add(" inner join AccountTypes authorAccountType on authorAccount.typeId = authorAccountType.accounttypeid and authorAccountType.tag <> 'collector'");

			// respect hideInStoreList
			clauses.add(new Object[]{ " and p.hideInStoreList = false"});

			// if we want a detail page we need an apn for published items (This means it is still in the publisher's collection)
			// for store listing this is the case even if we joined to userApn successfully
			clauses.add(new Object[]{" and publisherApn.accountprojectnumberid is not null"});

		}

		if (q.isPublished()) {
			clauses.add(new Object[]{" and p.published = true"});
		}

		// there may be more than one published version of a project:  when searching by publishedName, be sure to get the 'head'
		if (q.getPublishedName() != null) {
			clauses.add(new Object[] {" and upper(p.publishedName) = ?", q.getPublishedName().toUpperCase()});
		}

		// there may be more than one published version of a project:  when searching by urlName, be sure to get the 'head'
		if (q.getUrlName() != null) {
			clauses.add(new Object[] {" and upper(p.urlName) = ?", q.getUrlName().toUpperCase()});
		}


		if (q.getDepPublishId() != null) {
			clauses.add(new Object[] {" and p.dep_publishId", q.getDepPublishId()});
		}

		// hide retired rows when we are searching rather than getting by ID
		if(q.isForLatest()) {
			if(q.isPublished() ) {
				clauses.add(new Object[] {" and p.nextPublishedVersionid is null"});
			} else {
				clauses.add(new Object[] {" and p.nextVersionId is null"});
			}
			clauses.add(new Object[] {" and p.retired = false"});

			clauses.add(new Object[] {" and (publisherApn.retired is null OR publisherApn.retired = 0)"});

			// for author profile, we may want a list of projects the author has published.
			// Make sure to hide privately-published apps/gadgets.
			if (q.getAuthorMemberId() != null && q.getAuthorMemberId().equals(searcher.getId()) == false) {
				clauses.add(new Object[] {" and p.hideInStoreList = false"});
			}
		}

		if (q.getAuthorMemberId() != null) {
			clauses.add(new Object[] {" and p.authorMemberId = ?", q.getAuthorMemberId()});
		}

		if (q.getCategoryId() != null) clauses.add(new Object[]{" and p.categoryid = ?", q.getCategoryId()});
		if (q.getType() != null) clauses.add(new Object[]{" and p.projectTypeId = ?", q.getType().id});

		// for backwards compatibility, we have to see if we're loading a project by
		// the old publishId, which is a Long.
		if (q.getProjectId() != null) {
			if (StringUtils.isNumeric(q.getProjectId())) {
				long dep_pubId = Long.valueOf(q.getProjectId());
				clauses.add(new Object[]{" and p.dep_publishId = ?", dep_pubId});
			} else {
				clauses.add(new Object[]{" and p.projectId = ?", q.getProjectId()});
			}
		}

		if (q.getName() != null) {
			if (q.isPublished()) {
				clauses.add(new Object[]{" and upper(p.publishedName) = ?", q.getName().toUpperCase()});
			} else {
				clauses.add(new Object[]{" and upper(p.projectName) = ?", q.getName().toUpperCase()});
			}
		}

		if (q.getApnUUID() != null) {
			if (q.isForLatestForApnUUID()) {
				clauses.add(new Object[]{" and p.publishedName = (select apnp.publishedname from projects apnp" +
						" inner join AccountProjectNumbers apnapn on apnapn.projectId = apnp.projectid and apnapn.accountprojectnumberid = ?)", q.getApnUUID() });
				clauses.add(new Object[]{" and p.nextPublishedVersionId is null and p.published = true"});
			} else {
				clauses.add(new Object[] {" and ( userApn.accountprojectnumberid = ?", q.getApnUUID() });
				clauses.add(new Object[] {" or publisherApn.accountprojectnumberid = ? )", q.getApnUUID() });
			}
		}

		if(q.getGenericSearch() != null) {
			clauses.add(new Object[]{" and ( "});
			clauses.add(new Object[]{" upper(p." + (q.isPublished()? "publishedName" : "projectName") + ") like ?", "%"+q.getGenericSearch().toUpperCase()+"%"});
			clauses.add(new Object[]{" or upper(p.description) like ?", "%"+q.getGenericSearch().toUpperCase()+"%"});

			clauses.add(new Object[]{" or upper(app_user.firstname) like ?", "%"+q.getGenericSearch().toUpperCase()+"%"});
			clauses.add(new Object[]{" or upper(app_user.lastname) like ?", "%"+q.getGenericSearch().toUpperCase()+"%"});
			clauses.add(new Object[]{" or upper(app_user.displayname) like ?", "%"+q.getGenericSearch().toUpperCase()+"%"});
			clauses.add(new Object[]{" or upper(app_user.firstName || ' ' || app_user.lastName) like ?", "%"+q.getGenericSearch().toUpperCase()+"%"});

			clauses.add(new Object[]{" )"});
		}

		if (q.getTags() != null && q.getTags().length != 0) {

			// it might be faster to use an in(?), but much more complicated code to read
			// due to the List<Obect[]> pattern we're using
			clauses.add(new Object[] {" and ( false "});
			for (String tag : q.getTags()) {
				clauses.add(new Object[] {" or upper(gt.tag) = ?", tag.toUpperCase()} );
			}
			clauses.add(new Object[] {")"});
		}

		if (q.getFeatured() != null && q.getFeatured()) {
			clauses.add(new Object[]{" and featured = ?", q.getFeatured()});
		}

		if (q.getProjectName() != null) {
			clauses.add(new Object[] {" and upper(p.projectName) = ?", q.getProjectName().toUpperCase()} );
		}

		StringBuffer sql = new StringBuffer();
		sql.append("select p.projectId, ");
		sql.append("NULL AS commentCount, ");
		sql.append("p.version, ");
		if (q.isShowContent()) {
			if (log.isDebugEnabled()) { sql.append("\n"); }
			sql.append("p.content, ");
			if (q.isShowInitialValues()) {
				sql.append("p.initialValues, ");
			}
		}
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("p.createdDateTime, ");
		sql.append("p.retired, ");
		sql.append("p.editedDateTime, ");
		sql.append("p.projectMetaData, ");
		sql.append("p.projectName, ");
		sql.append("p.nextVersionId, ");
		sql.append("p.authorMemberId, ");
		sql.append("p.dep_gadgetRefId, ");
		sql.append("p.dep_instanceId, ");
		sql.append("p.projectTypeId, ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("p.gadgetSize, ");
		sql.append("p.autosave, ");
		sql.append("p.dep_publishId, ");
		sql.append("p.accountId, ");
		sql.append("p.versionMajor, ");
		sql.append("p.versionMinor, ");
		sql.append("p.versionDot, ");
		sql.append("p.versionBuildNumber, ");
		sql.append("p.categoryId, ");
		sql.append("p.publishedName, ");
		sql.append("p.description, ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("p.statusId, ");
		sql.append("p.licenseTypeId, ");
		sql.append("p.price, ");
		sql.append("p.currencyTypeId, ");
		sql.append("p.hideInStoreList, ");
		sql.append("p.featured, ");
		sql.append("p.published, ");
		sql.append("p.publishedMetaData, ");
		sql.append("p.selfPrice, ");
		sql.append("p.inviteOnly, ");
		sql.append("p.guestListId, ");
		sql.append("p.dep_publishedRetired, ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("p.width, ");
		sql.append("p.height, ");
		sql.append("p.nextpublishedversionid, ");
		sql.append("p.accountTypeId, ");
		sql.append("p.urlName, ");
		sql.append("projectAccountType.name AS projectAccountTypeName, ");
		
		sql.append(
				"exists (\n" +
						"    SELECT userApn2.projectId FROM accountprojectnumbers userApn2\n" +
						"       inner join projects p2 on p2.projectid = userApn2.projectId\n" +
						"       WHERE userApn2.accountid = ?\n" +
						"            and p2.publishedname = p.publishedname\n" +
						"			 and userApn2.accountId <> p.accountId\n" +
						"			 and userApn2.projectId <> p.projectId\n" + // -- exclude currently owned
						"			 and userApn.accountprojectnumberid is null\n" + 
						"    ) AS newerThanWhatSearcherOwns,"
				);

		sql.append(
				"exists (\n" +
				" select userApn2.projectId from accountprojectnumbers userApn2\n" +
				"	inner join projects p2 on p2.projectId = userApn2.projectId\n" +
				"	where\n" +
				"		p2.publishedName = p.publishedName\n" +
				"		and userApn2.accountid = p.accountId\n" +
				"		and p2.nextpublishedversionid is null\n" +
				"		and p2.projectId <> p.projectId\n" +
				"		) as olderThanLatestVersion,"
				);

		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("LicenseTypes.republishable, ");
		sql.append("LicenseTypes.editable, ");
		sql.append("LicenseTypes.embedable, ");
		sql.append("coalesce(userApn.accountprojectnumberid, publisherApn.accountprojectnumberid) as apnUUID, ");
		sql.append("coalesce(userApn.accountprojectnumberid, publisherApn.accountprojectnumberid) as playerId, ");
		sql.append("userApn.accountprojectnumberid is not null as owned, ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("app_user.firstName as authorFirstName, ");
		sql.append("app_user.lastName as authorLastName, ");
		sql.append("app_user.website as authorWebSite, ");
		sql.append("app_user.displayName as authorDisplayName, ");
		sql.append("app_user.nameFormat as authorNameFormat, ");
		sql.append("authorCompany.name as authorCompanyName, ");
		sql.append("authorAccount.name as authorAccountName, ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("screenshot0.publishImageId as scrn_publishImageId0, ");
		sql.append("screenshot0.projectId as scrn_projectId0, ");
		sql.append("screenshot0.caption as scrn_caption0, ");
		sql.append("screenshot0.displayOrder as scrn_displayOrder0, ");
		sql.append("screenshot0.screenshotFileId as scrn_screenshotFileId0, ");
		sql.append("screenshot0.renderingComplete as scrn_renderingComplete0, ");
		sql.append("screenshot0.retired as scrn_retired0, ");
		sql.append("screenshot0.label as scrn_label0, ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("screenshot1.publishImageId as scrn_publishImageId1, ");
		sql.append("screenshot1.projectId as scrn_projectId1, ");
		sql.append("screenshot1.caption as scrn_caption1, ");
		sql.append("screenshot1.displayOrder as scrn_displayorder1, ");
		sql.append("screenshot1.screenshotFileId as scrn_screenshotFileId1, ");
		sql.append("screenshot1.renderingComplete as scrn_renderingComplete1, ");
		sql.append("screenshot1.retired as scrn_retired1, ");
		sql.append("screenshot1.label as scrn_label1, ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("screenshot2.publishImageId as scrn_publishImageId2, ");
		sql.append("screenshot2.projectId as scrn_projectId2, ");
		sql.append("screenshot2.caption as scrn_caption2, ");
		sql.append("screenshot2.displayOrder as scrn_displayOrder2, ");
		sql.append("screenshot2.screenshotFileId as scrn_screenshotFileId2, ");
		sql.append("screenshot2.renderingComplete as scrn_renderingComplete2, ");
		sql.append("screenshot2.retired as scrn_retired2, ");
		sql.append("screenshot2.label as scrn_label2, ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("screenshot3.publishImageId as scrn_publishImageId3, ");
		sql.append("screenshot3.projectId as scrn_projectId3, ");
		sql.append("screenshot3.caption as scrn_caption3, ");
		sql.append("screenshot3.displayOrder as scrn_displayOrder3, ");
		sql.append("screenshot3.screenshotFileId as scrn_screenshotFileId3, ");
		sql.append("screenshot3.renderingComplete as scrn_renderingComplete3, ");
		sql.append("screenshot3.retired as scrn_retired3, ");
		sql.append("screenshot3.label as scrn_label3, ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("icon.publishImageId as icon_publishImageId, ");
		sql.append("icon.projectId as icon_projectId, ");
		sql.append("icon.caption as icon_caption, ");
		sql.append("icon.displayOrder as icon_displayOrder, ");
		sql.append("icon.iconFileId as icon_iconFileId, ");
		sql.append("icon.renderingComplete as icon_renderingComplete, ");
		sql.append("icon.retired as icon_retired, ");
		sql.append("icon.label as icon_label, ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("ppa.cachedPurchaseAcquisitionCount, zrc.ratingtotal, zrc.numRatings ");
		if (log.isDebugEnabled()) { sql.append("\n"); }
		sql.append("from ");

		for (String tableName : tables) {
			if (log.isDebugEnabled()) { sql.append("\n"); }
			sql.append(tableName);
		}

		sql.append(" where true ");

		for (Object[] clause : clauses) {
			if (log.isDebugEnabled()) { sql.append("\n"); }
			sql.append(clause[0]);
			if (clause.length > 1)
				values.add(clause[1]);
		}

		String sortColumn = "createdDateTime";
		if (q.getSortColumn() == null) sortColumn = "createdDateTime";
		else if (q.getSortColumn().equals("createdDateTime")) sortColumn = "createdDateTime";
		else if (q.getSortColumn().equals("price")) sortColumn = "price";
		else if (q.getSortColumn().equals("categoryId")) sortColumn = "categoryId";
		else if (q.getSortColumn().equals("description")) sortColumn = "description";
		else if (q.getSortColumn().equals("type")) sortColumn = "projectTypeId";
		else if (q.getSortColumn().equals("name")) sortColumn = q.isPublished()? "publishedName" : "projectName";
		else if (q.isPublished() && q.getSortColumn().equals("popularity")) sortColumn = "cachedPurchaseAcquisitionCount";

		String pageClause = "";
		if (q.getSize() != null && q.getOffset() != null) {
			pageClause = " limit ? offset ?";
			values.add(q.getSize() + 1);
			values.add(q.getOffset());
		}

		String order = "desc";
		if ("asc".equalsIgnoreCase(q.getOrder()))
			order = "asc";

		sql.append(" order by " + sortColumn + " " + order + " ");
		sql.append(pageClause);


		if (log.isDebugEnabled()) {
			StringBuffer debugVals = new StringBuffer();
			for (Object val : values) {
				debugVals.append(val + ", ");
			}
			log.debug("list query: " + sql);
			log.debug("     params: " + debugVals.toString() );
		}


		List<P> projects = Collections.emptyList();
		try {
			projects = template.query(sql.toString(), values.toArray(), projectMapper);
			if (log.isDebugEnabled()) {
				StringBuffer debugVals = new StringBuffer();
				for (P prj : projects) {
					debugVals.append(q.isPublished()? prj.getPublishedName() : prj.getProjectName() + ",");
				}
				log.debug(" RESULTS (names):  " + debugVals);
			}

		} catch (EmptyResultDataAccessException safeToIgnore) {
			log.debug(" no results");
		}

		// build the "x of y" counting clause
        clauses.remove(0); // rip the clause representing the select parameter back off.
		StringBuffer countQuery = new StringBuffer();
		countQuery.append("select count(*) from ");
		for (String tableName : tables) {
			countQuery.append(tableName);
		}

		countQuery.append(" where true ");
		for (Object[] clause : clauses) {
			countQuery.append(clause[0]);
			if (clause.length > 1)
				countValues.add(clause[1]);
		}
		int totalCount = template.queryForInt(countQuery.toString(), countValues.toArray());


		return emptySet.withResults(projects, totalCount);
	}

    @Transactional(readOnly=true)
   	public ProjectListSearchResultSet search(ProjectQuery q, AppUser searcher) {
        return (ProjectListSearchResultSet) search(q, searcher, mapper, ProjectListSearchResultSet.emptySet());
    }
	
    @Transactional(readOnly = true)
    public MiniProjectListSearchResultSet searchMiniStuff(ProjectQuery q, AppUser searcher) {
    	return (MiniProjectListSearchResultSet) search(q, searcher, miniMapper, MiniProjectListSearchResultSet.emptySet());
    }
    
	@Transactional(readOnly = true)
	public MyProjectListSearchResultSet searchMyStuff(ProjectQuery q, AppUser searcher) {
        return (MyProjectListSearchResultSet) search(q, searcher, myMapper, MyProjectListSearchResultSet.emptySet());
	}

	@Transactional(readOnly = true)
	public TinyProjectListSearchResultSet searchTinyStuff(ProjectQuery q, AppUser searcher) {
        return (TinyProjectListSearchResultSet) search(q, searcher, tinyMapper, TinyProjectListSearchResultSet.emptySet());
	}
	
	@Transactional(readOnly = true)
	public ProjectListSearchResultSet searchHistory(ProjectQuery q, AppUser searcher) {
		return (ProjectListSearchResultSet) search(q, searcher, mapper, ProjectListSearchResultSet.emptySet());
	}
	
	@Override
	public Map<String, Map<String, Object>> listCurrentQueries() {
		// TODO Auto-generated method stub
		return null;
	}


}
