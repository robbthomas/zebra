package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.GadgetState;

public interface GadgetStateDao {
	/** Used by DaoWriteAspect to update the version number.*/
	public static final String TABLE_NAME = "gadget_state";
	
	public GadgetState findById(String id) throws NotFoundException;
	
	public GadgetState create(GadgetState user);
	public GadgetState update(GadgetState user) throws ConcurrencyException;
	
	public void delete(GadgetState user) throws ConcurrencyException;
	List<GadgetState> findAll() throws NotFoundException;

	public GadgetState findByRootProjectAndAppUser(String rootProjectId,
			String appUserId, String externalUserId, boolean authorMode) throws NotFoundException;

	public GadgetState findByRootProject(String rootProjectId) throws NotFoundException;

}
