package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.model.Account;
import com.alleni.zebra.data.model.AppUser;

public interface AccountDao {
	
	public Account findById(Long id) throws NotFoundException;

	public List<Account> findAll();
	
	public Long create(Account account);

	public Account findAccountForAccountProjectNumberId(String uuid);
	
	public void setBanner(long accountId, UploadableAsset banner);
	
	public Account update(Account account, AppUser updater) throws DataAccessException;
}
