package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.GadgetCategory;

public interface GadgetCategoryDao {
	public static final String TABLE_NAME = "categories";
	
	public GadgetCategory findById(Long id) throws NotFoundException;
	public GadgetCategory findByName(String name) throws NotFoundException;
	public List<GadgetCategory> findAll();
	
}
