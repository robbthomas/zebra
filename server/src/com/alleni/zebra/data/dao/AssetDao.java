package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.Asset;

public interface AssetDao {
	
	public static final String TABLE_NAME = "asset";
	
	public Asset findById(String id) throws NotFoundException;
	
	/**
	 * Also filters by permissions!
	 * 
	 * 
	 * @param id
	 * @param appUserId
	 * @return
	 * @throws NotFoundException
	 */
	public Asset findByIdAndAppUser(String id, String appUserId) throws NotFoundException;
	
	public List<Asset> findAll();
	
	public List<Asset> findAllForUser(String appUserId) throws NotFoundException;
	
	public Asset update(Asset asset) throws ConcurrencyException;
	
	public void delete(Asset asset) throws ConcurrencyException;
	
	public Asset create(Asset asset);

    public void setAsPreview(String projectId, Asset asset);

    Asset findPreviewByProject(String projectId);

	public Asset findByIdNoRollback(String id);
}
