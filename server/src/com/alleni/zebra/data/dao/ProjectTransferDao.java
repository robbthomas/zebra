package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.ProjectTransfer;

public interface ProjectTransferDao {
	public static final String TABLE_NAME = "projectmemberaccess";
	
	
	public List<ProjectTransfer> list(AppUser user, boolean incoming);
	
	public ProjectTransfer findByProjectMemberAccessId(Long projectMemberAccessId) throws NotFoundException;
	
	public void update(ProjectTransfer pt, AppUser user) throws NotFoundException;
	
	public ProjectTransfer create(ProjectTransfer pt, AppUser user);
	
	public void delete(ProjectTransfer pt, AppUser user) throws NotFoundException;
	
	public void markTransferComplete(long projectTransferId) throws DataAccessException;
}
