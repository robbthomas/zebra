package com.alleni.zebra.data.dao;

import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.authorization.Role;

public interface AccountMemberRoleDao {

	public void addRolesToUser(AppUser user, Role... roles);
}
