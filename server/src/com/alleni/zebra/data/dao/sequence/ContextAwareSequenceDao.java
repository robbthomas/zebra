package com.alleni.zebra.data.dao.sequence;

/**
 * Access the hibernate_sequence defined in the database when the application is
 * running 'live.'  Otherwise, use an AtomicInteger to get sequential values in
 * test contexts. 
 *
 */
public interface ContextAwareSequenceDao {
	
	public long nextValue();
}
