package com.alleni.zebra.data.dao.sequence;

import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ContextAwareSequenceDaoImpl implements ContextAwareSequenceDao {
	private final String DB_SEQUENCE_NAME = "hibernate_sequence";
	private final String HIB_SEQUENCE_SQL = "select nextval(?)";
	
	private final Logger log = Logger.getLogger(getClass());
	
	private final AtomicLong atomicLong = new AtomicLong();
	
	@Autowired BasicDataSource dataSource;
	@Autowired JdbcTemplate jdbcTemplate;
	
	@Override
	@Transactional
	public long nextValue() {
		String driverClassName = dataSource.getDriverClassName();
		log.debug("driverClassName: " + driverClassName);
		
		if (driverClassName.contains("hsql")) {
			return testSequence();
		} else {
			return hibernateSequence();
		}
	}
	
	
	private long testSequence() {
		return atomicLong.incrementAndGet();
	}

	@Transactional
	private long hibernateSequence() {
		long nextVal = jdbcTemplate.queryForLong(HIB_SEQUENCE_SQL, DB_SEQUENCE_NAME);
		return nextVal;
	}
}
