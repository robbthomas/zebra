package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.PublishedGadgetImage;

public interface PublishedGadgetImageDao {
	public static final String TABLE_NAME = "PublishImages";

	public PublishedGadgetImage findById(Long publishImageId) throws NotFoundException;

	public List<PublishedGadgetImage> findIconsByPublishedGadgetAndIndex(Long publishedGadgetId, int displayOrder) throws DataAccessException;
	
	public PublishedGadgetImage getIconForProject(String projectId) throws DataAccessException;
	public List<PublishedGadgetImage> getScreenshotsForProject(String projectId);
	
	public PublishedGadgetImage create(PublishedGadgetImage image);
	
	public PublishedGadgetImage update(PublishedGadgetImage image) throws DataAccessException;
	
	public void delete(long pgImageId) throws DataAccessException;
	public void deleteAllIconsForPublishedGadget(String projectId) throws DataAccessException;
	public void deleteAllScreenshotsForPublishedGadget(String projectId) throws DataAccessException;

	List<PublishedGadgetImage> findScreenshotsByProjectIdAndIndex(
			String projectId, int displayOrder)
			throws DataAccessException;

	public void copyIconAndScreenshots(String fromId, String toId) throws DataAccessException;

	public void copyIcon(String iconFromProjectId, String toProjectId) throws DataAccessException;

	public void copyScreenshots(String iconFromPubId, String newProjectId);

	public void copyIconByIdForNewProject(Long previousIconId, String newProjectId) throws DataAccessException;	
}
