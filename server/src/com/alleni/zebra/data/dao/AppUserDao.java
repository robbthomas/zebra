package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.authorization.Role;

public interface AppUserDao {
	/** Used by DaoWriteAspect to update the version number.*/
	public static final String TABLE_NAME = "app_user";
	
	public static final String[] SAFE_COLUMNS = {"id", "version", "date_created", "retired", "description", "email", "emailshow", "enabled",
												 "last_updated", "displayname", "editedbyid", "retiredbyid", "retireddatetime", "firstname",
												 "lastname", "website", "twitter", "nameformat, avatarId"};
	
//	@Secured( value="ROLE_TYPE_SITE_ADMIN" )
	public AppUser findById(String id) throws NotFoundException;
	public AppUser findById(Long id) throws NotFoundException;
	public AppUser findByUsername(String username) throws NotFoundException ;
	
	public AppUser create(AppUser user);
	public AppUser update(AppUser user) throws ConcurrencyException;
	
	public void delete(AppUser user) throws ConcurrencyException;
	public List<AppUser> findAll() throws NotFoundException;
	public List<AppUser> findAllByAccountId(Long accountId);
	public AppUser findByEmail(String email) throws NotFoundException;
    public AppUser findByUsernameOrEmail(String name) throws NotFoundException;
    
    public void setAvatar(AppUser user, UploadableAsset avatar);
    public void setBanner(AppUser user, UploadableAsset banner);
    public boolean hasRole(AppUser user, Role role);
}
