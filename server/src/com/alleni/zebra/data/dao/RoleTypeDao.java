package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.authorization.Role;

public interface RoleTypeDao {
	public static final String TABLE_NAME = "RoleTypes";
	
	public List<Role> getRolesForUser(AppUser user) throws NotFoundException;
}
