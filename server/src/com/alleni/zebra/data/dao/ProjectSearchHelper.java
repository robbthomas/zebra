package com.alleni.zebra.data.dao;

import java.util.Map;

import com.alleni.zebra.data.dto.MiniProjectListSearchResultSet;
import com.alleni.zebra.data.dto.MyProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectQuery;
import com.alleni.zebra.data.dto.TinyProjectListSearchResultSet;
import com.alleni.zebra.data.model.AppUser;

public interface ProjectSearchHelper {
	
	public ProjectListSearchResultSet search(ProjectQuery q, AppUser searcher);
	
	public ProjectListSearchResultSet searchHistory(ProjectQuery q, AppUser searcher);

	public MiniProjectListSearchResultSet searchMiniStuff(ProjectQuery q, AppUser searcher);
	
	public MyProjectListSearchResultSet searchMyStuff(ProjectQuery q, AppUser searcher);
	
	public TinyProjectListSearchResultSet searchTinyStuff(ProjectQuery q, AppUser searcher);
	
	public Map<String, Map<String,Object>> listCurrentQueries();
}
