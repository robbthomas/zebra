package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.Thingy;

public interface ThingyDao {
	public static final String TABLE_NAME = "thingy";
	
	public Thingy findByName(String name) throws NotFoundException;
	public Thingy findById(Long id) throws NotFoundException;
	public Thingy create(Thingy thingy);
	public Thingy update(Thingy thingy) throws NotFoundException, ConcurrencyException;
	public void delete(Thingy thingy) throws NotFoundException, ConcurrencyException;
	List<Thingy> findAll() throws NotFoundException;
}
