package com.alleni.zebra.data.dao;

import java.util.Date;
import java.util.List;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AccountProjectNumber;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.CurrencyType;
import com.alleni.zebra.data.model.ProductCode;
import com.alleni.zebra.data.model.Project;

public interface AccountProjectNumberDao {
	public static final String TABLE_NAME = "AccountProjectNumbers";
	
	public AccountProjectNumber create(AccountProjectNumber ppp);
	
	public AccountProjectNumber find(AccountProjectNumber ppp) throws NotFoundException;
	
	AccountProjectNumber findByUuid(String uuid) throws NotFoundException;

	public void delete(AccountProjectNumber ppp, AppUser user) throws NotFoundException;

    public void updateInvoiceCachedTotal(long invoiceId) throws DataAccessException;
	
	public Long createInvoice(long accountId, String description, Date holdingDate);
	
	public Long createLineItem(long invoiceId, int lineNumber, int quantity, int amountInPennies, ProductCode productCode, String description, String projectId);

	List<ProductCode> getAllProductCodes();

	public void updateProject(AppUser user, String apnUUID, String projectId) throws DataAccessException;

	public AccountProjectNumber findForUserAndUrlName(AppUser user,
			String urlName) throws DataAccessException;
	
	public int unPurchaseOldVersionsForAuthor(Project p);
}
