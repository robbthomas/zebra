package com.alleni.zebra.data.dao;

import java.util.List;
import java.util.Map;

import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.UserVariable;

public interface UserVariableDao {
	
	public List<UserVariable> getTopN(String projectId, String lookupKey, int n);
	
	public List<UserVariable> getBottomN(String projectId, String lookupKey, int n);
	
	public List<Map<String, Object>> histogram(String projectId, String lookupKey);
	
	public List<UserVariable> list(String projectId, String lookupKey);
	
	public UserVariable find(String projectId, String userId, String lookupKey) throws NotFoundException;
	
	public UserVariable create(UserVariable data);
}
