package com.alleni.zebra.data.dao;

import java.util.List;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;

/**
 * DAO-ish thing for the group of tables relating to Private Publishing:
 *   - SharedProjectInvitees
 *   - SharedProjects
 *   - ShareTypes
 *
 * A slight departure from other DAOs in this package, functions more as a
 * "transaction script." (Fowler)
 */
public interface SharedProjectDao {
	public boolean isGuestForProject(AppUser user, String projectId) throws DataAccessException;
	
	public boolean isTransfereeForProject(AppUser user, String projectId) throws DataAccessException;
	

	public List<String> getShareTypesForProject(String projectId) throws DataAccessException;
	
	
}
