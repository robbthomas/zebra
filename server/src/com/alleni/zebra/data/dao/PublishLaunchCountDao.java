package com.alleni.zebra.data.dao;

import com.alleni.zebra.data.model.AppUser;

/**
 * Dao to manage the PublishLaunchCounts table that stores a row for
 * each launch of a published entity.
 */
public interface PublishLaunchCountDao {
    /**
     * Inserts a row into the PublishLaunchCounts table recording a single access by a single user
     * @param projectID published item being accessed
     * @param user zebra user accessing the published item, if from an embed it is likely guest
     * @param externalUserId 3rd party userId Potentially hashed and potentially null. This should be given by LMS access
     * @param playerId id used to access generally from the URL
     */
    void incrementLaunchCount(String projectID, AppUser user, String externalUserId, String playerId);
}
