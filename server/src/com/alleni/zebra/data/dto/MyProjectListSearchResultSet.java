package com.alleni.zebra.data.dto;

import java.util.Collections;
import java.util.List;

import com.alleni.zebra.data.model.MyProject;

public class MyProjectListSearchResultSet implements GenericProjectListSearchResultSet<MyProject> {
	public final List<MyProject> projects;
	public final int totalCount;
	public final boolean hasMoreData;
	
	private static final MyProjectListSearchResultSet EMPTY_SET;
	
	static {
		List<MyProject> emptyList = Collections.emptyList();
		EMPTY_SET = new MyProjectListSearchResultSet(emptyList, 0);
	}

	public MyProjectListSearchResultSet(List<MyProject> myProjects, int totalCount) {
		this.projects = myProjects;
		this.totalCount = totalCount;
		this.hasMoreData = (myProjects.size() < totalCount);
	}

	public static final MyProjectListSearchResultSet emptySet() {
		return EMPTY_SET;
	}

    @Override
    public MyProjectListSearchResultSet withResults(List<MyProject> myProjects, int totalCount) {
        return new MyProjectListSearchResultSet(myProjects, totalCount);
    }
}
