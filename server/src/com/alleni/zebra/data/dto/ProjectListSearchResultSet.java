package com.alleni.zebra.data.dto;

import java.util.Collections;
import java.util.List;

import com.alleni.zebra.data.model.Project;

public class ProjectListSearchResultSet implements GenericProjectListSearchResultSet<Project> {
	public final List<Project> projects;
	public final int totalCount;
	public final boolean hasMoreData;
	
	private static final ProjectListSearchResultSet EMPTY_SET;
	
	static {
		List<Project> emptyList = Collections.emptyList();
		EMPTY_SET = new ProjectListSearchResultSet(emptyList, 0);
	}

	public ProjectListSearchResultSet(List<Project> projects, int totalCount) {
		this.projects = projects;
		this.totalCount = totalCount;
		this.hasMoreData = (projects.size() < totalCount);
	}

	public static final ProjectListSearchResultSet emptySet() {
		return EMPTY_SET;
	}

    @Override
    public ProjectListSearchResultSet withResults(List<Project> projects, int totalCount) {
        return new ProjectListSearchResultSet(projects, totalCount);
    }
}
