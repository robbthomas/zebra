package com.alleni.zebra.data.dto;

import java.util.Collections;
import java.util.List;

import com.alleni.zebra.data.model.TinyProject;

public class TinyProjectListSearchResultSet implements GenericProjectListSearchResultSet<TinyProject> {
	public final List<TinyProject> projects;
	public final int totalCount;
	public final boolean hasMoreData;
	
	private static final TinyProjectListSearchResultSet EMPTY_SET;
	
	static {
		List<TinyProject> emptyList = Collections.emptyList();
		EMPTY_SET = new TinyProjectListSearchResultSet(emptyList, 0);
	}

	public TinyProjectListSearchResultSet(List<TinyProject> tinyProjects, int totalCount) {
		this.projects = tinyProjects;
		this.totalCount = totalCount;
		this.hasMoreData = (tinyProjects.size() < totalCount);
	}

	public static final TinyProjectListSearchResultSet emptySet() {
		return EMPTY_SET;
	}

    @Override
    public TinyProjectListSearchResultSet withResults(List<TinyProject> tinyProjects, int totalCount) {
        return new TinyProjectListSearchResultSet(tinyProjects, totalCount);
    }
}
