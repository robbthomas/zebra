package com.alleni.zebra.data.dto;

import java.util.List;

public interface GenericProjectListSearchResultSet<T> {

    public GenericProjectListSearchResultSet<T> withResults(List<T> projects, int totalCount);
}
