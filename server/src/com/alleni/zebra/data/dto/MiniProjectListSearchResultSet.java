package com.alleni.zebra.data.dto;

import java.util.Collections;
import java.util.List;

import com.alleni.zebra.data.model.MiniProject;

public class MiniProjectListSearchResultSet implements GenericProjectListSearchResultSet<MiniProject> {
	public final List<MiniProject> projects;
	public final int totalCount;
	public final boolean hasMoreData;
	
	private static final MiniProjectListSearchResultSet EMPTY_SET;
	
	static {
		List<MiniProject> emptyList = Collections.emptyList();
		EMPTY_SET = new MiniProjectListSearchResultSet(emptyList, 0);
	}

	public MiniProjectListSearchResultSet(List<MiniProject> miniprojects, int totalCount) {
		this.projects = miniprojects;
		this.totalCount = totalCount;
		this.hasMoreData = (miniprojects.size() < totalCount);
	}

	public static final MiniProjectListSearchResultSet emptySet() {
		return EMPTY_SET;
	}

    @Override
    public MiniProjectListSearchResultSet withResults(List<MiniProject> miniprojects, int totalCount) {
        return new MiniProjectListSearchResultSet(miniprojects, totalCount);
    }

}
