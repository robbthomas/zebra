package com.alleni.zebra.data.dto;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.alleni.zebra.data.model.LicenseType;
import com.alleni.zebra.gadget.ProjectType;

public class ProjectQuery {
	
	public enum SortOrder {
		ASCENDING("asc"),
		DESCENDING("desc");
		public final String value;
		private SortOrder(String value) {this.value = value;}
		public String toString(){return value;}
	}

	public enum SortColumn {
		CREATED_DATE("createdDateTime"),
		PRICE("price"),
		CATEGORY_ID("categoryId"),
		DESCRIPTION("description"),
		TYPE("type"),
		NAME("name");
		public final String value;
		private SortColumn(String value) {this.value = value;}
		public String toString(){return value;}
	}

	
	public ProjectQuery() {
		// default values
		this.showContent = true;
	}
	
	private boolean latestForApnUUID;
    private boolean showContent;
    private boolean showInitialValues;
	private String projectId;
    private Long publisherAccountId;
	private Long ownerAccountId;
	private String authorMemberId;
	private String name;  // used for publishedName and projectName
	private String[] tags;
	private String description;
	private Long categoryId;
	private Integer minPrice;
	private Integer maxPrice;
	private Integer size;
	private Integer offset;
	private String sortColumn;
	private Integer versionMinor;
	private Integer versionMajor;
	private Integer versionDot;
	private String order;
	private Date minEditedDate;
	private Date maxEditedDate;
	private String genericSearch;
    private Boolean featured;
	private boolean published;
    private LicenseType licenseType;
    private Boolean retired;
    private ProjectType type;
    private String appUserId;
    private String apnUUID;
    private boolean forStoreList;
    private String parentProjectId;
    private String urlName;
    private String publishedName;
    private boolean history;
    private Integer depPublishId;
    private List<String> projectIdList;
    private String projectNameList;
    
   	public boolean isForLatest() {
    	
    	return
    	        // each of these will specify exact versions already so we definitely don't
                // ownerAccountId will narrow to a set of AccountProjectNumber rows which themselves specify exact versions
                (projectId == null && parentProjectId == null && apnUUID == null && ownerAccountId == null
                // names don't specify exact versions, but if we don't say we want history, we assume latest version
                && (urlName == null && publishedName == null && projectName == null && !history));
    }
    
    public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	private String projectName;
    
	public boolean isHistory() {
		return history;
	}
	public void setHistory(boolean history) {
		this.history = history;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
    public Long getPublisherAccountId() {
   		return publisherAccountId;
   	}
   	public void setPublisherAccountId(Long accountId) {
   		this.publisherAccountId = accountId;
   	}
	public Long getOwnerAccountId() {
		return ownerAccountId;
	}
	public void setOwnerAccountId(Long accountId) {
		this.ownerAccountId = accountId;
	}
	public String getAuthorMemberId() {
		return authorMemberId;
	}
	public void setAuthorMemberId(String authorMemberId) {
		this.authorMemberId = authorMemberId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String[] getTags() {
		return tags;
	}
	public void setTags(String[] tags) {
		this.tags = tags;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(Integer minPrice) {
		this.minPrice = minPrice;
	}
	public Integer getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(Integer maxPrice) {
		this.maxPrice = maxPrice;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public String getSortColumn() {
		return sortColumn;
	}
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}
	public void setSortColumn(SortColumn sortColumn) {
		this.sortColumn = sortColumn.value;
	}
	public Integer getVersionMinor() {
		return versionMinor;
	}
	public void setVersionMinor(Integer versionMinor) {
		this.versionMinor = versionMinor;
	}
	public Integer getVersionMajor() {
		return versionMajor;
	}
	public void setVersionMajor(Integer versionMajor) {
		this.versionMajor = versionMajor;
	}
	public Integer getVersionDot() {
		return versionDot;
	}
	public void setVersionDot(Integer versionDot) {
		this.versionDot = versionDot;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(SortOrder order) {
		this.order = order.value;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public ProjectType getType() {
		return type;
	}
	public void setType(ProjectType type) {
		this.type = type;
	}
	public Date getMinEditedDate() {
		return minEditedDate;
	}
	public void setMinEditedDate(Date minEditedDate) {
		this.minEditedDate = minEditedDate;
	}
	public Date getMaxEditedDate() {
		return maxEditedDate;
	}
	public void setMaxEditedDate(Date maxEditedDate) {
		this.maxEditedDate = maxEditedDate;
	}
	public String getGenericSearch() {
		return genericSearch;
	}
	public void setGenericSearch(String genericSearch) {
		this.genericSearch = genericSearch;
	}
	public Boolean getFeatured() {
		return featured;
	}
	public void setFeatured(Boolean featured) {
		this.featured = featured;
	}
	public Boolean isPublished() {
		return (published || isForStoreList() || publishedName!= null || urlName != null);
	}
	public LicenseType getLicenseType() {
		return licenseType;
	}
	public void setLicenseType(LicenseType licenseType) {
		this.licenseType = licenseType;
	}
	public Boolean getRetired() {
		return retired;
	}
	public void setRetired(Boolean retired) {
		this.retired = retired;
	}
	public String getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(String appUserId) {
		this.appUserId = appUserId;
	}
	public void setPublished(boolean published) {
		this.published = published;
	}
	public boolean isShowContent() {
		return showContent;
	}
	public void setShowContent(boolean showContent) {
		this.showContent = showContent;
	}
    public boolean isShowInitialValues() {
   		return showContent;
   	}
   	public void setShowInitialValues(boolean showInitialValues) {
   		this.showInitialValues = showInitialValues;
   	}
	public String getApnUUID() {
		return apnUUID;
	}
	public void setApnUUID(String apnUUID) {
		this.apnUUID = apnUUID;
	}

    public boolean isForStoreList() {
        return forStoreList;
    }

    public void setForStoreList(boolean forStoreList) {
        this.forStoreList = forStoreList;
    }

    public String getParentProjectId() {
        return parentProjectId;
    }

    public void setParentProjectId(String parentProjectId) {
        this.parentProjectId = parentProjectId;
    }

	@Override
	public String toString() {
		return "ProjectQuery [showContent=" + showContent + "showInitialValues="
                + showInitialValues + ", projectId="
				+ projectId + ", accountId=" + ownerAccountId + ", authorMemberId="
				+ authorMemberId + ", name=" + name + ", tags="
				+ Arrays.toString(tags) + ", description=" + description
				+ ", categoryId=" + categoryId + ", minPrice=" + minPrice
				+ ", maxPrice=" + maxPrice + ", size=" + size + ", offset="
				+ offset + ", sortColumn=" + sortColumn + ", versionMinor="
				+ versionMinor + ", versionMajor=" + versionMajor
				+ ", versionDot=" + versionDot + ", order=" + order
				+ ", minEditedDate=" + minEditedDate + ", maxEditedDate="
				+ maxEditedDate + ", genericSearch=" + genericSearch
				+ ", featured=" + featured + ", published=" + published
				+ ", licenseType=" + licenseType + ", deleted=" + retired
				+ ", type=" + type + ", appUserId=" + appUserId + ", apnUUID="
				+ apnUUID + ", forStoreList=" + forStoreList
				+ ", parentProjectId=" + parentProjectId + "]";
	}
	public String getUrlName() {
		return urlName;
	}
	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}
	public String getPublishedName() {
		return publishedName;
	}
	public void setPublishedName(String publishedName) {
		this.publishedName = publishedName;
	}
	public Integer getDepPublishId() {
		return depPublishId;
	}
	public void setDepPublishId(Integer depPublishId) {
		this.depPublishId = depPublishId;
	}

	public boolean isForLatestForApnUUID() {
		return latestForApnUUID;
	}

	public void setLatestForApnUUID(boolean latestForApnUUID) {
		this.latestForApnUUID = latestForApnUUID;
	}
	
	public List<String> getProjectIdList() {
		return projectIdList;
	}

	public void setProjectIdList(List<String> projectIdList) {
		this.projectIdList = projectIdList;
	}
	
	public String getProjectNameList() {
		return projectNameList;
	}

	public void setProjectNameList(String projectNameList) {
		this.projectNameList = projectNameList;
	}
}
