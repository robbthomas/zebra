package com.alleni.zebra.data.dto;

import java.util.Date;

import com.alleni.zebra.gadget.ProjectType;

public class PublishedGadgetSearchQuery {
	private String gadgetId;
	private Long accountId;
	private String memberId;
	private String name;
	private String[] tags;
	private String description;
	private Long categoryId;
	private Integer minPrice;
	private Integer maxPrice;
	private Integer size;
	private Integer offset;
	private String sortColumn;
	private Integer versionMinor;
	private Integer versionMajor;
	private Integer versionDot;
	private String order;
	private ProjectType type;
	private Date minEditedDate;
	private Date maxEditedDate;
	private String genericSearch;
    private Boolean featured;
	
	public String getGadgetId() {
		return gadgetId;
	}
	public void setGadgetId(String gadgetId) {
		this.gadgetId = gadgetId;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String[] getTags() {
		return tags;
	}
	public void setTags(String[] tags) {
		this.tags = tags;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(Integer minPrice) {
		this.minPrice = minPrice;
	}
	public Integer getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(Integer maxPrice) {
		this.maxPrice = maxPrice;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public String getSortColumn() {
		return sortColumn;
	}
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}
	public Integer getVersionMinor() {
		return versionMinor;
	}
	public void setVersionMinor(Integer versionMinor) {
		this.versionMinor = versionMinor;
	}
	public Integer getVersionMajor() {
		return versionMajor;
	}
	public void setVersionMajor(Integer versionMajor) {
		this.versionMajor = versionMajor;
	}
	public Integer getVersionDot() {
		return versionDot;
	}
	public void setVersionDot(Integer versionDot) {
		this.versionDot = versionDot;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public ProjectType getType() {
		return type;
	}
	public void setType(ProjectType type) {
		this.type = type;
	}
	public Date getMinEditedDate() {
		return minEditedDate;
	}
	public void setMinEditedDate(Date minEditedDate) {
		this.minEditedDate = minEditedDate;
	}
	public Date getMaxEditedDate() {
		return maxEditedDate;
	}
	public void setMaxEditedDate(Date maxEditedDate) {
		this.maxEditedDate = maxEditedDate;
	}

        public String getGenericSearch() {
            return genericSearch;
        }

        public void setGenericSearch(String genericSearch) {
            this.genericSearch = genericSearch;
        }

		public Long getCategoryId() {
			return categoryId;
		}
		public void setCategoryId(Long categoryId) {
			this.categoryId = categoryId;
		}
		
	public Boolean getFeatured()
	{
		return featured;
	}
	public void setFeatured(Boolean featured)
	{
		this.featured = featured;
	}
}
