package com.alleni.zebra.data.exception;

public class UnsupportedAssetMimeType extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String assetId;

    public UnsupportedAssetMimeType(String assetId, Throwable throwable) {
        super("Unsupported MimeType for assetId " + assetId, throwable);
        this.assetId = assetId;
    }

    public String getAssetId() {
        return assetId;
    }
}
