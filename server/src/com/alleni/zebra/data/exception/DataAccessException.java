package com.alleni.zebra.data.exception;


public class DataAccessException extends RuntimeException {

	private static final long serialVersionUID = -1110806984412072168L;
	private final Object objectId;

	public DataAccessException(String objectId) {
		super();
		this.objectId = objectId;
	}

	public DataAccessException(Long objectId) {
		super();
		this.objectId = objectId;
	}

	public DataAccessException(String message, Throwable cause, String objectId) {
		super(message, cause);
		this.objectId = objectId;
	}
	public DataAccessException(String message, Throwable cause, Long objectId) {
		super(message, cause);
		this.objectId = objectId;
	}

	public DataAccessException(String message, String objectId) {
		super(message);
		this.objectId = objectId;
	}

	public DataAccessException(String message, Long objectId) {
		super(message);
		this.objectId = objectId;
	}

	public DataAccessException(Throwable cause, String objectId) {
		super(cause);
		this.objectId = objectId;
	}

	public DataAccessException(Throwable cause, Long objectId) {
		super(cause);
		this.objectId = objectId;
	}


	public String getObjectId() { return objectId == null? null : objectId.toString(); }

}
