package com.alleni.zebra.data.exception;


/**
 * This exception is thrown when by a Dao or a Service when
 * lookup of specific data via a known identifier fails.
 * 
 *
 */
public class NotFoundException extends DataAccessException {
    private static final long serialVersionUID = -4349778378761242317L;

	public NotFoundException(Long objectId) {
		super(objectId);
		
	}

	public NotFoundException(String message, Long objectId) {
		super(message, objectId);
		
	}

	public NotFoundException(String message, String objectId) {
		super(message, objectId);
		
	}

	public NotFoundException(String message, Throwable cause, Long objectId) {
		super(message, cause, objectId);
		
	}

	public NotFoundException(String message, Throwable cause, String objectId) {
		super(message, cause, objectId);
		
	}

	public NotFoundException(String objectId) {
		super(objectId);
		
	}

	public NotFoundException(Throwable cause, Long objectId) {
		super(cause, objectId);
		
	}

	public NotFoundException(Throwable cause, String objectId) {
		super(cause, objectId);
		
	}

 
}