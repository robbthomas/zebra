package com.alleni.zebra.data.exception;

import com.alleni.zebra.gadget.store.PublishException;

public class PublishedNameInUseByOtherUserException extends PublishException {

	private static final long serialVersionUID = 3321993553386159119L;

	public PublishedNameInUseByOtherUserException() {
		super();
	}

	public PublishedNameInUseByOtherUserException(long publishId) {
		super(publishId);
	}

	public PublishedNameInUseByOtherUserException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public PublishedNameInUseByOtherUserException(String arg0) {
		super(arg0);
	}

	public PublishedNameInUseByOtherUserException(Throwable arg0) {
		super(arg0);
	}

}
