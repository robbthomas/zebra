package com.alleni.zebra.data.exception;

public class ConcurrencyException extends DataAccessException {

	private static final long serialVersionUID = -4950311235590063175L;

	public ConcurrencyException(Long objectId) {
		super(objectId);
		
	}

	public ConcurrencyException(String message, Long objectId) {
		super(message, objectId);
		
	}

	public ConcurrencyException(String message, String objectId) {
		super(message, objectId);
		
	}

	public ConcurrencyException(String message, Throwable cause, Long objectId) {
		super(message, cause, objectId);
		
	}

	public ConcurrencyException(String message, Throwable cause, String objectId) {
		super(message, cause, objectId);
		
	}

	public ConcurrencyException(String objectId) {
		super(objectId);
		
	}

	public ConcurrencyException(Throwable cause, Long objectId) {
		super(cause, objectId);
		
	}

	public ConcurrencyException(Throwable cause, String objectId) {
		super(cause, objectId);
		
	}

}
