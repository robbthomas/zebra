package com.alleni.zebra.data.exception;

public class UniqueConstraintException extends DataAccessException {

	private static final long serialVersionUID = -3717317777523717467L;
    private String field;

	public UniqueConstraintException(Long objectId, String field) {
		super(objectId);
        this.field = field;
	}

	public UniqueConstraintException(String message, Long objectId, String field) {
		super(message, objectId);
        this.field = field;
	}

	public UniqueConstraintException(String message, String objectId, String field) {
		super(message, objectId);
        this.field = field;
	}

	public UniqueConstraintException(String message, Throwable cause,
			Long objectId, String field) {
		super(message, cause, objectId);
        this.field = field;
	}

	public UniqueConstraintException(String message, Throwable cause,
			String objectId, String field) {
		super(message, cause, objectId);
        this.field = field;
	}

    public UniqueConstraintException(String objectId, String field) {
   		super(objectId);
        this.field = field;
   	}

	public UniqueConstraintException(Throwable cause, Long objectId, String field) {
		super(cause, objectId);
        this.field = field;
	}

	public UniqueConstraintException(Throwable cause, String objectId, String field) {
		super(cause, objectId);
        this.field = field;
	}

    public String getField() {
        return field;
    }
}
