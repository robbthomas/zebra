package com.alleni.zebra.data.hibernate.dao.impl;

// Generated Sep 27, 2012 1:38:37 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alleni.zebra.data.hibernate.dao.UploadableAssetTypeSupportedMimeTypeDao;
import com.alleni.zebra.data.hibernate.model.MimeType;
import com.alleni.zebra.data.hibernate.model.UploadableAssetType;
import com.alleni.zebra.data.hibernate.model.UploadableAssetTypeSupportedMimeType;

/**
 * Home object for domain model class Uploadableassettypesupportedmimetype.
 * @see com.alleni.zebra.data.hibernate.model.UploadableAssetTypeSupportedMimeType
 * @author Hibernate Tools
 */
@Repository("hibernateUploadableAssetTypeSupportedMimeTypeDao")
public class HibernateUploadablAassetTypeSupportedMimeTypeDaoImpl implements UploadableAssetTypeSupportedMimeTypeDao {
	private static final Log log = LogFactory
			.getLog(HibernateUploadablAassetTypeSupportedMimeTypeDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;
	
	
	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAsssetTypeSupportedMimeTypeDao#persist(com.alleni.zebra.data.hibernate.model.UploadableAssetTypeSupportedMimeType)
	 */
	public void persist(UploadableAssetTypeSupportedMimeType transientInstance) {
		log.debug("persisting Uploadableassettypesupportedmimetype instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAsssetTypeSupportedMimeTypeDao#remove(com.alleni.zebra.data.hibernate.model.UploadableAssetTypeSupportedMimeType)
	 */
	public void remove(UploadableAssetTypeSupportedMimeType persistentInstance) {
		log.debug("removing Uploadableassettypesupportedmimetype instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.delete(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAsssetTypeSupportedMimeTypeDao#merge(com.alleni.zebra.data.hibernate.model.UploadableAssetTypeSupportedMimeType)
	 */
	public UploadableAssetTypeSupportedMimeType merge(
			UploadableAssetTypeSupportedMimeType detachedInstance) {
		log.debug("merging Uploadableassettypesupportedmimetype instance");
		try {
			Session s = sessionFactory.getCurrentSession();
			UploadableAssetTypeSupportedMimeType result = 
					(UploadableAssetTypeSupportedMimeType)s.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAsssetTypeSupportedMimeTypeDao#findById(int)
	 */
	public UploadableAssetTypeSupportedMimeType findById(int id) {
		log.debug("getting Uploadableassettypesupportedmimetype instance with id: "
				+ id);
		Session s = sessionFactory.getCurrentSession();
		try {
			UploadableAssetTypeSupportedMimeType instance =
					(UploadableAssetTypeSupportedMimeType)s
						.get(UploadableAssetTypeSupportedMimeType.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@Override
	public UploadableAssetTypeSupportedMimeType findByMimeTypeAndUploadableAssetType(
			MimeType mimeType, UploadableAssetType assetType) {
		Session s = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<UploadableAssetTypeSupportedMimeType> ret = s.createQuery("from UploadableAssetTypeSupportedMimeType uatsmt" +
				" where uatsmt.mimeType.id =:mimetypeid" +
				" and uatsmt.uploadableAssetType.id =:uploadableassettypeid")
				.setParameter("mimetypeid", mimeType.getMimetypeid())
				.setParameter("uploadableassettypeid", assetType.getUploadableassettypeid())
				.list();
		return (ret == null || ret.isEmpty())? null : ret.get(0);
	}

}
