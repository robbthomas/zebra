package com.alleni.zebra.data.hibernate.dao.impl;

// Generated Sep 10, 2012 9:17:24 AM by Hibernate Tools 3.4.0.CR1

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.dao.UploadableAssetDao;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;

/**
 * Home object for domain model class Uploadableasset.
 * @see com.alleni.zebra.data.hibernate.model.UploadableAsset
 * @author Hibernate Tools
 */
@Repository("hibernateUploadableAssetDao")
public class HibernateUploadableAssetDao implements UploadableAssetDao {
	
	private Log log = LogFactory.getLog(HibernateUploadableAssetDao.class);

	@Autowired SessionFactory sessionFactory;
	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetDao#persist(com.alleni.zebra.data.hibernate.model.UploadableAsset)
	 */
	@Transactional(readOnly=false)
	public void persist(UploadableAsset transientInstance) {
		log.debug("persisting Uploadableasset instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetDao#remove(com.alleni.zebra.data.hibernate.model.UploadableAsset)
	 */
	@Transactional(readOnly=false)
	public void remove(UploadableAsset persistentInstance) {
		log.debug("removing Uploadableasset instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.delete(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetDao#merge(com.alleni.zebra.data.hibernate.model.UploadableAsset)
	 */
	@Transactional(readOnly=false)
	public UploadableAsset merge(UploadableAsset detachedInstance) {
		log.debug("merging Uploadableasset instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			UploadableAsset result = (UploadableAsset)s.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetDao#findById(int)
	 */
	@Transactional(readOnly=false)
	public UploadableAsset findById(int id) {
		log.debug("getting Uploadableasset instance with id: " + id);
		Session s = sessionFactory.getCurrentSession();
		try {
			UploadableAsset instance = (UploadableAsset)s.get(
					UploadableAsset.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

}
