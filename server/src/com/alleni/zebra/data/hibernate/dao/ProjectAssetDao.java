package com.alleni.zebra.data.hibernate.dao;

import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.model.ProjectAsset;

public interface ProjectAssetDao {

	@Transactional
	public abstract void persist(ProjectAsset transientInstance);

	@Transactional
	public abstract void remove(ProjectAsset persistentInstance);

	@Transactional
	public abstract ProjectAsset merge(ProjectAsset detachedInstance);

	@Transactional(readOnly = true)
	public abstract ProjectAsset findById(int id);

}