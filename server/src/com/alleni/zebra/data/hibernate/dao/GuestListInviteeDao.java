package com.alleni.zebra.data.hibernate.dao;

import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.model.GuestListInvitee;

public interface GuestListInviteeDao {

	@Transactional(readOnly = false)
	public abstract void persist(GuestListInvitee transientInstance);

	@Transactional(readOnly = false)
	public abstract void remove(GuestListInvitee persistentInstance);

	@Transactional(readOnly = false)
	public abstract GuestListInvitee merge(GuestListInvitee detachedInstance);

	@Transactional(readOnly = true)
	public abstract GuestListInvitee findById(int id);

	public abstract Set<GuestListInvitee> findAllByGuestListId(
			Integer guestListId);

}