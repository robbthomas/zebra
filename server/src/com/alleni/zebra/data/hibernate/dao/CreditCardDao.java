package com.alleni.zebra.data.hibernate.dao;

import java.util.List;

import com.alleni.zebra.data.hibernate.model.CreditCard;


public interface CreditCardDao {

	public abstract void persist(CreditCard transientInstance);

	public abstract void remove(CreditCard persistentInstance);

	public abstract CreditCard merge(CreditCard detachedInstance);

	public abstract CreditCard findById(long id);
	
	public abstract List<CreditCard> findForAccount(Long accountId);

}