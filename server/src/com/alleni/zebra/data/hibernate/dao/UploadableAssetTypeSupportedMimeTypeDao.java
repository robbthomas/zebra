package com.alleni.zebra.data.hibernate.dao;

import com.alleni.zebra.data.hibernate.model.MimeType;
import com.alleni.zebra.data.hibernate.model.UploadableAssetType;
import com.alleni.zebra.data.hibernate.model.UploadableAssetTypeSupportedMimeType;

public interface UploadableAssetTypeSupportedMimeTypeDao {

	public abstract void persist(
			UploadableAssetTypeSupportedMimeType transientInstance);

	public abstract void remove(
			UploadableAssetTypeSupportedMimeType persistentInstance);

	public abstract UploadableAssetTypeSupportedMimeType merge(
			UploadableAssetTypeSupportedMimeType detachedInstance);

	public abstract UploadableAssetTypeSupportedMimeType findById(int id);
	
	
	public abstract UploadableAssetTypeSupportedMimeType findByMimeTypeAndUploadableAssetType(MimeType mimeType, UploadableAssetType assetType);
}