package com.alleni.zebra.data.hibernate.dao;

import com.alleni.zebra.data.hibernate.model.UploadableAsset;

public interface UploadableAssetDao {

	public abstract void persist(UploadableAsset transientInstance);

	public abstract void remove(UploadableAsset persistentInstance);

	public abstract UploadableAsset merge(UploadableAsset detachedInstance);

	public abstract UploadableAsset findById(int id);
}