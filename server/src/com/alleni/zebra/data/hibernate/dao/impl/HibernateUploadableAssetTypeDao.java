package com.alleni.zebra.data.hibernate.dao.impl;

// Generated Sep 10, 2012 9:17:24 AM by Hibernate Tools 3.4.0.CR1

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.dao.UploadableAssetTypeDao;
import com.alleni.zebra.data.hibernate.model.UploadableAssetType;

/**
 * Home object for domain model class Uploadableassettype.
 * @see com.alleni.zebra.data.hibernate.model.UploadableAssetType
 * @author Hibernate Tools
 */
@Repository("hibernateUploadableAssetTypeDao")
public class HibernateUploadableAssetTypeDao implements UploadableAssetTypeDao {

	private Log log = LogFactory
			.getLog(HibernateUploadableAssetTypeDao.class);

	@Autowired SessionFactory sessionFactory;


	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetTypeDao#persist(com.alleni.zebra.data.hibernate.model.UploadableAssetType)
	 */
	@Transactional
	public void persist(UploadableAssetType transientInstance) {
		log.debug("persisting Uploadableassettype instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetTypeDao#remove(com.alleni.zebra.data.hibernate.model.UploadableAssetType)
	 */
	@Transactional
	public void remove(UploadableAssetType persistentInstance) {
		log.debug("removing Uploadableassettype instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.delete(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetTypeDao#merge(com.alleni.zebra.data.hibernate.model.UploadableAssetType)
	 */
	@Transactional
	public UploadableAssetType merge(UploadableAssetType detachedInstance) {
		log.debug("merging Uploadableassettype instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			UploadableAssetType result = (UploadableAssetType)s.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetTypeDao#findById(int)
	 */
	@Transactional
	public UploadableAssetType findById(int id) {
		log.debug("getting Uploadableassettype instance with id: " + id);
		Session s = sessionFactory.getCurrentSession();
		try {
			UploadableAssetType instance = (UploadableAssetType)s.get(
					UploadableAssetType.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
	@Transactional
	public UploadableAssetType findByTag(String tag) {
		Session s = sessionFactory.getCurrentSession();
		UploadableAssetType ret = (UploadableAssetType)s.createQuery("from UploadableAssetType uat where uat.tag =:tag").
				setParameter("tag", tag).uniqueResult();
		return ret;
	}

}
