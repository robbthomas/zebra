package com.alleni.zebra.data.hibernate.dao.impl;

// Generated Sep 10, 2012 9:17:24 AM by Hibernate Tools 3.4.0.CR1

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.dao.UploadableAssetS3BucketDao;
import com.alleni.zebra.data.hibernate.model.UploadableAssetS3Bucket;

/**
 * Home object for domain model class Uploadableassets3bucket.
 * @see com.alleni.zebra.data.hibernate.model.UploadableAssetS3Bucket
 * @author Hibernate Tools
 */
@Repository("hibernateUploadableAssetS3BucketDao")
public class HibernateUploadableAssetS3BucketDao implements UploadableAssetS3BucketDao {

	private Log log = LogFactory.getLog(HibernateUploadableAssetS3BucketDao.class);

	@Autowired SessionFactory sessionFactory;
	
	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetS3BucketDao#persist(com.alleni.zebra.data.hibernate.model.UploadableAssetS3Bucket)
	 */
	@Transactional
	public void persist(UploadableAssetS3Bucket transientInstance) {
		log.debug("persisting Uploadableassets3bucket instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetS3BucketDao#remove(com.alleni.zebra.data.hibernate.model.UploadableAssetS3Bucket)
	 */
	@Transactional
	public void remove(UploadableAssetS3Bucket persistentInstance) {
		log.debug("removing Uploadableassets3bucket instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.delete(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetS3BucketDao#merge(com.alleni.zebra.data.hibernate.model.UploadableAssetS3Bucket)
	 */
	@Transactional
	public UploadableAssetS3Bucket merge(
			UploadableAssetS3Bucket detachedInstance) {
		log.debug("merging Uploadableassets3bucket instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			UploadableAssetS3Bucket result = (UploadableAssetS3Bucket)s.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.UploadableAssetS3BucketDao#findById(int)
	 */
	@Transactional(readOnly=true)
	public UploadableAssetS3Bucket findById(int id) {
		log.debug("getting Uploadableassets3bucket instance with id: " + id);
		Session s = sessionFactory.getCurrentSession();
		try {
			UploadableAssetS3Bucket instance = (UploadableAssetS3Bucket)s.get(
					UploadableAssetS3Bucket.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

}
