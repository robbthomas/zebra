package com.alleni.zebra.data.hibernate.dao;

import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.model.MimeCategory;

public interface MimeCategoryDao {

	@Transactional
	public abstract void persist(MimeCategory transientInstance);

	@Transactional
	public abstract void remove(MimeCategory persistentInstance);

	@Transactional
	public abstract MimeCategory merge(MimeCategory detachedInstance);

	@Transactional(readOnly = true)
	public abstract MimeCategory findById(int id);

}