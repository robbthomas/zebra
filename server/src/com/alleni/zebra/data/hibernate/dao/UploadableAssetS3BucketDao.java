package com.alleni.zebra.data.hibernate.dao;

import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.model.UploadableAssetS3Bucket;

public interface UploadableAssetS3BucketDao {

	@Transactional
	public abstract void persist(UploadableAssetS3Bucket transientInstance);

	@Transactional
	public abstract void remove(UploadableAssetS3Bucket persistentInstance);

	@Transactional
	public abstract UploadableAssetS3Bucket merge(
			UploadableAssetS3Bucket detachedInstance);

	@Transactional(readOnly = true)
	public abstract UploadableAssetS3Bucket findById(int id);


}