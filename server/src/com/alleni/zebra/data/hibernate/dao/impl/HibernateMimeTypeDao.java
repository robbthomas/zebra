package com.alleni.zebra.data.hibernate.dao.impl;

// Generated Sep 10, 2012 9:17:24 AM by Hibernate Tools 3.4.0.CR1

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.dao.MimeTypeDao;
import com.alleni.zebra.data.hibernate.model.MimeType;

/**
 * Home object for domain model class Mimetype.
 * @see com.alleni.zebra.data.hibernate.model.MimeType
 * @author Hibernate Tools
 */
@Repository("hibernateMimeTypeDao")
public class HibernateMimeTypeDao implements MimeTypeDao {

	private Log log = LogFactory.getLog(HibernateMimeTypeDao.class);
	
	@Autowired
	SessionFactory sessionFactory;

	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.MimeTypeDao#persist(com.alleni.zebra.data.hibernate.model.MimeType)
	 */
	@Transactional
	public void persist(MimeType transientInstance) {
		log.debug("persisting Mimetype instance");
		Session s = sessionFactory.getCurrentSession();
		
		try {
			s.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.MimeTypeDao#remove(com.alleni.zebra.data.hibernate.model.MimeType)
	 */
	public void remove(MimeType persistentInstance) {
		log.debug("removing Mimetype instance");
		Session s = sessionFactory.getCurrentSession();
		
		try {
			s.delete(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.MimeTypeDao#merge(com.alleni.zebra.data.hibernate.model.MimeType)
	 */
	public MimeType merge(MimeType detachedInstance) {
		log.debug("merging Mimetype instance");
		Session s = sessionFactory.getCurrentSession();
		
		try {
			MimeType result = (MimeType)s.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.MimeTypeDao#findById(int)
	 */
	public MimeType findById(int id) {
		log.debug("getting Mimetype instance with id: " + id);
		Session s = sessionFactory.getCurrentSession();
		
		try {
			MimeType instance = (MimeType)s.get(MimeType.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	

	@Transactional
	public MimeType findByIsoName(String isoName) {
		Session s = sessionFactory.getCurrentSession();
		MimeType ret = (MimeType)s.createQuery("from MimeType mt where mt.isoname =:isoName").
				setParameter("isoName", isoName).uniqueResult();
		return ret;
	}

}
