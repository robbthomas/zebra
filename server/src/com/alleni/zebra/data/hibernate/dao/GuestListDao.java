package com.alleni.zebra.data.hibernate.dao;

import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.model.GuestList;

public interface GuestListDao {

	// ----
	//      NON-Hibernate methods here
	// ----
	@Transactional(readOnly = true)
	public abstract GuestList findByProjectId(String projectId);

	@Transactional(readOnly = true)
	public abstract GuestList findByApnUUID(String apnUUID);

	@Transactional(readOnly = false)
	public abstract void persist(GuestList transientInstance);

	@Transactional(readOnly = false)
	public abstract void remove(GuestList persistentInstance);

	@Transactional(readOnly = false)
	public abstract GuestList merge(GuestList detachedInstance);

	@Transactional(readOnly = true)
	public abstract GuestList findById(int id);

}