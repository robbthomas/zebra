package com.alleni.zebra.data.hibernate.dao;

import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.model.MimeType;

public interface MimeTypeDao {

	@Transactional
	public abstract void persist(MimeType transientInstance);

	public abstract void remove(MimeType persistentInstance);

	public abstract MimeType merge(MimeType detachedInstance);

	public abstract MimeType findById(int id);
	
	MimeType findByIsoName(String isoName);

}