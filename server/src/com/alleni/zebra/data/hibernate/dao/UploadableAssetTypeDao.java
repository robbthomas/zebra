package com.alleni.zebra.data.hibernate.dao;

import com.alleni.zebra.data.hibernate.model.UploadableAssetType;

public interface UploadableAssetTypeDao {

	public abstract void persist(UploadableAssetType transientInstance);

	public abstract void remove(UploadableAssetType persistentInstance);

	public abstract UploadableAssetType merge(
			UploadableAssetType detachedInstance);

	public abstract UploadableAssetType findById(int id);
	
	public abstract UploadableAssetType findByTag(String tag);
	

}