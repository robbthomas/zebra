package com.alleni.zebra.data.hibernate.dao.impl;

// Generated Sep 10, 2012 9:17:24 AM by Hibernate Tools 3.4.0.CR1

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.dao.MimeCategoryDao;
import com.alleni.zebra.data.hibernate.model.MimeCategory;

/**
 * Home object for domain model class Mimecategory.
 * @see com.alleni.zebra.data.hibernate.model.MimeCategory
 * @author Hibernate Tools
 */
@Repository("hibernateMimeCategoryDao")
public class HibernateMimeCategoryDao implements MimeCategoryDao {

	private Log log = LogFactory.getLog(HibernateMimeCategoryDao.class);

	@Autowired SessionFactory sessionFactory;
	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.MimeCategoryDao#persist(com.alleni.zebra.data.hibernate.model.MimeCategory)
	 */
	@Transactional
	public void persist(MimeCategory transientInstance) {
		log.debug("persisting Mimecategory instance");
		Session s = sessionFactory.getCurrentSession();

		try {
			s.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.MimeCategoryDao#remove(com.alleni.zebra.data.hibernate.model.MimeCategory)
	 */
	@Transactional
	public void remove(MimeCategory persistentInstance) {
		log.debug("removing Mimecategory instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.delete(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.MimeCategoryDao#merge(com.alleni.zebra.data.hibernate.model.MimeCategory)
	 */
	@Transactional
	public MimeCategory merge(MimeCategory detachedInstance) {
		log.debug("merging Mimecategory instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			MimeCategory result = (MimeCategory)s.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.MimeCategoryDao#findById(int)
	 */
	@Transactional(readOnly = true)
	public MimeCategory findById(int id) {
		log.debug("getting Mimecategory instance with id: " + id);
		Session s = sessionFactory.getCurrentSession();
		try {
			MimeCategory instance = (MimeCategory)s.get(MimeCategory.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
