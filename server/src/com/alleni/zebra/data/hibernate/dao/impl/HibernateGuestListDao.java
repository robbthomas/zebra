package com.alleni.zebra.data.hibernate.dao.impl;

// Generated Sep 5, 2012 4:58:22 PM by Hibernate Tools 3.4.0.CR1

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.dao.GuestListDao;
import com.alleni.zebra.data.hibernate.model.GuestList;

/**
 * Home object for domain model class Guestlists.
 * @see com.alleni.zebra.data.hibernate.model.GuestList
 * @author Hibernate Tools
 */
@Repository("hibernateGuestListDao")
public class HibernateGuestListDao implements GuestListDao {

	private Log log = LogFactory.getLog(HibernateGuestListDao.class);

	@Autowired SessionFactory sessionFactory;
	@Autowired JdbcTemplate template;
	
	// ----
	//      NON-Hibernate methods here
	// ----
	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.GuestListDao#findByProjectId(java.lang.String)
	 */
	@Transactional(readOnly = true)
	public GuestList findByProjectId(String projectId) {
		GuestList gl = null;
		int glId = 0;
		String sql = "select gl.guestListId from projects p " +
				"inner join guestlists gl " +
				"on gl.guestlistid = p.guestlistid " +
				"where p.projectId = ?";
		try {
			glId = template.queryForInt(sql, new Object[]{projectId});
			gl = findById(glId);
		} catch (Exception e) {
			log.error(e);
		}
		return gl;
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.GuestListDao#findByApnUUID(java.lang.String)
	 */
	@Transactional(readOnly = true)
	public GuestList findByApnUUID(String apnUUID) {
		GuestList gl = null;
		int glId = 0;
		String sql = "select gl.guestListId from accountprojectnumbers apn " +
				"inner join projects p on p.projectid = apn.projectid " +
				"inner join guestlists gl " +
				"on gl.guestlistid = p.guestlistid " +
				"where apn.accountprojectnumberid = ?";
		try {
			glId = template.queryForInt(sql, new Object[]{apnUUID});
			gl = findById(glId);
		} catch (Exception e) {
			//ignore
		}
		return gl;
	}
	
	
	//----
	//     BEGIN Hibernate-only methods
	//----
	
	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.GuestListDao#persist(com.alleni.zebra.data.model.GuestList)
	 */
	@Transactional(readOnly = false)
	public void persist(GuestList transientInstance) {
		log.debug("persisting Guestlists instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.GuestListDao#remove(com.alleni.zebra.data.model.GuestList)
	 */
	@Transactional(readOnly = false)
	public void remove(GuestList persistentInstance) {
		log.debug("removing Guestlists instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.delete(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.GuestListDao#merge(com.alleni.zebra.data.model.GuestList)
	 */
	@Transactional(readOnly = false)
	public GuestList merge(GuestList detachedInstance) {
		log.debug("merging Guestlists instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			GuestList result = (GuestList)s.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.GuestListDao#findById(int)
	 */
	@Transactional(readOnly = true)
	public GuestList findById(int id) {
		log.debug("getting Guestlists instance with id: " + id);
		Session s = sessionFactory.getCurrentSession();
		try {
			GuestList instance = (GuestList)s.get(GuestList.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
