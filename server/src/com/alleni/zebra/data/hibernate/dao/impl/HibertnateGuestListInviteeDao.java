package com.alleni.zebra.data.hibernate.dao.impl;

// Generated Sep 5, 2012 4:58:22 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.dao.GuestListInviteeDao;
import com.alleni.zebra.data.hibernate.model.GuestListInvitee;

/**
 * Home object for domain model class Guestlistinvitees.
 * @see com.alleni.zebra.data.hibernate.model.GuestListInvitee
 * @author Hibernate Tools
 */
@Repository("hibernateGuestListInviteeDao")
public class HibertnateGuestListInviteeDao implements GuestListInviteeDao {

	private Log log = LogFactory
			.getLog(HibertnateGuestListInviteeDao.class);

	@Autowired SessionFactory sessionFactory;

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.GuestListInviteeDao#persist(com.alleni.zebra.data.model.GuestListInvitee)
	 */
	@Transactional(readOnly = false)
	public void persist(GuestListInvitee transientInstance) {
		log.debug("persisting Guestlistinvitees instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}


	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.GuestListInviteeDao#remove(com.alleni.zebra.data.model.GuestListInvitee)
	 */
	@Transactional(readOnly = false)
	public void remove(GuestListInvitee persistentInstance) {
		log.debug("removing Guestlistinvitees instance");
		try {
			Session s = sessionFactory.getCurrentSession();
			s.delete(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.GuestListInviteeDao#merge(com.alleni.zebra.data.model.GuestListInvitee)
	 */
	@Transactional(readOnly = false)
	public GuestListInvitee merge(GuestListInvitee detachedInstance) {
		log.debug("merging Guestlistinvitees instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			GuestListInvitee result = (GuestListInvitee)s.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.GuestListInviteeDao#findById(int)
	 */
	@Transactional(readOnly = true)
	public GuestListInvitee findById(int id) {
		log.debug("getting Guestlistinvitees instance with id: " + id);
		Session s = sessionFactory.getCurrentSession();
		try {
			GuestListInvitee instance = (GuestListInvitee)s.get(
					GuestListInvitee.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@Transactional(readOnly=true)
	public Set<GuestListInvitee> findAllByGuestListId(Integer guestListId) {
		log.debug("getting Guestlistinvitees instances with guestListId: " + guestListId);
		Set<GuestListInvitee> ret = new HashSet<GuestListInvitee>();
		Session s = sessionFactory.getCurrentSession();
		try {
			String hql = "from GuestListInvitee as gli where gli.guestlist.guestlistid = ?";
			Query q = s.createQuery(hql);
			q.setInteger(0, guestListId);
			@SuppressWarnings("unchecked")
			List<GuestListInvitee> res = q.list();
			ret.addAll(res);
			log.debug("get successful");
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
		
		return ret;
	}
}
