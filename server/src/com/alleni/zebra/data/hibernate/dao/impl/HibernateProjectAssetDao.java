package com.alleni.zebra.data.hibernate.dao.impl;

// Generated Sep 10, 2012 9:17:24 AM by Hibernate Tools 3.4.0.CR1

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.dao.ProjectAssetDao;
import com.alleni.zebra.data.hibernate.model.ProjectAsset;

/**
 * Home object for domain model class Projectasset.
 * @see com.alleni.zebra.data.hibernate.model.ProjectAsset
 * @author Hibernate Tools
 */
@Repository("hibernateProjectAssetDao")
public class HibernateProjectAssetDao implements ProjectAssetDao {

	private static final Log log = LogFactory.getLog(HibernateProjectAssetDao.class);

	@Autowired SessionFactory sessionFactory;

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.ProjectAssetDao#persist(com.alleni.zebra.data.hibernate.model.ProjectAsset)
	 */
	@Transactional
	public void persist(ProjectAsset transientInstance) {
		log.debug("persisting Projectasset instance");
		try {
			Session s = sessionFactory.getCurrentSession();
			s.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.ProjectAssetDao#remove(com.alleni.zebra.data.hibernate.model.ProjectAsset)
	 */
	@Transactional
	public void remove(ProjectAsset persistentInstance) {
		log.debug("removing Projectasset instance");
		try {
			Session s = sessionFactory.getCurrentSession();
			s.delete(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.ProjectAssetDao#merge(com.alleni.zebra.data.hibernate.model.ProjectAsset)
	 */
	@Transactional
	public ProjectAsset merge(ProjectAsset detachedInstance) {
		log.debug("merging Projectasset instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			ProjectAsset result = (ProjectAsset)s.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.dao.impl.ProjectAssetDao#findById(int)
	 */
	@Transactional(readOnly = true)
	public ProjectAsset findById(int id) {
		log.debug("getting Projectasset instance with id: " + id);
		Session s = sessionFactory.getCurrentSession();
		try {
			ProjectAsset instance = (ProjectAsset)s.get(ProjectAsset.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
