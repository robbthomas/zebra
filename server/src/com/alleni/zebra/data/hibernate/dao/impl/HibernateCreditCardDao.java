package com.alleni.zebra.data.hibernate.dao.impl;

// Generated Jul 26, 2012 11:19:14 AM by Hibernate Tools 3.4.0.CR1

import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.hibernate.dao.CreditCardDao;
import com.alleni.zebra.data.hibernate.model.CreditCard;

/**
 * Home object for domain model class Creditcards.
 * @see com.alleni.zebra.data.hibernate.model.CreditCard
 * @author Hibernate Tools
 */
@Repository("creditCardsDao")
public class HibernateCreditCardDao implements CreditCardDao {

	private Log log = LogFactory.getLog(HibernateCreditCardDao.class);

	@Autowired
	SessionFactory sessionFactory;

	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.CreditCardDaoIf#persist(com.alleni.zebra.data.hibernate.model.Creditcard)
	 */
	@Transactional
	public void persist(CreditCard transientInstance) {
		log.debug("persisting Creditcards instance");
		Session s = sessionFactory.getCurrentSession();
		
		try {
			s.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.CreditCardDaoIf#remove(com.alleni.zebra.data.hibernate.model.Creditcard)
	 */
	@Transactional
	public void remove(CreditCard persistentInstance) {
		log.debug("removing Creditcards instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			s.delete(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.CreditCardDaoIf#merge(com.alleni.zebra.data.hibernate.model.Creditcard)
	 */
	@Transactional
	public CreditCard merge(CreditCard detachedInstance) {
		log.debug("merging Creditcards instance");
		Session s = sessionFactory.getCurrentSession();
		try {
			CreditCard result = (CreditCard)s.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.data.hibernate.model.CreditCardDaoIf#findById(long)
	 */
	@Transactional
	public CreditCard findById(long id) {
		log.debug("getting Creditcards instance with id: " + id);
		Session s = sessionFactory.getCurrentSession();
		try {
			CreditCard instance = (CreditCard)s.get(CreditCard.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<CreditCard> findForAccount(Long accountId) {
		List<CreditCard> ret = Collections.emptyList();
		Session s = sessionFactory.getCurrentSession();
		ret = s.createQuery("from CreditCard cc where cc.accountid =:accountId").setParameter("accountId", accountId)
				.list();
		return ret;
	}
}
