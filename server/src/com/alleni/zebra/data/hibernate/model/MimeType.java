package com.alleni.zebra.data.hibernate.model;

// Generated Sep 10, 2012 9:17:24 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Mimetype generated by hbm2java
 */
@Entity
@Table(name = "mimetype")
public class MimeType implements java.io.Serializable {

	private static final long serialVersionUID = 7874140396056708290L;
	private int mimetypeid;
	private MimeCategory mimeCategory;
	private String isoname;
	private Set<UploadableAssetTypeSupportedMimeType> uploadableAssetTypeSupportedMimeTypes =
			new HashSet<UploadableAssetTypeSupportedMimeType>(0);

	public MimeType() {
	}

	public MimeType(int mimetypeid, MimeCategory mimeCategory, String isoname) {
		this.mimetypeid = mimetypeid;
		this.mimeCategory = mimeCategory;
		this.isoname = isoname;
	}

	@Id
	@Column(name = "mimetypeid", unique = true, nullable = false)
	@SequenceGenerator(name = "mimetype_mimetypeid_seq", sequenceName = "mimetype_mimetypeid_seq")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "mimetype_mimetypeid_seq")
	public int getMimetypeid() {
		return this.mimetypeid;
	}

	public void setMimetypeid(int mimetypeid) {
		this.mimetypeid = mimetypeid;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "mimecategoryid", nullable = false)
	public MimeCategory getMimecategory() {
		return this.mimeCategory;
	}

	public void setMimecategory(MimeCategory mimeCategory) {
		this.mimeCategory = mimeCategory;
	}

	@Column(name = "isoname", nullable = false)
	public String getIsoname() {
		return this.isoname;
	}

	public void setIsoname(String isoname) {
		this.isoname = isoname;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "mimeType")
	public Set<UploadableAssetTypeSupportedMimeType> getUploadableAssetTypeSupportedMimeTypes() {
		return uploadableAssetTypeSupportedMimeTypes;
	}

	@JsonIgnore
	public void setUploadableAssetTypeSupportedMimeTypes(
			Set<UploadableAssetTypeSupportedMimeType> uploadableAssetTypeSupportedMimeTypes) {
		this.uploadableAssetTypeSupportedMimeTypes = uploadableAssetTypeSupportedMimeTypes;
	}

}
