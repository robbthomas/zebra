package com.alleni.zebra.data.util;

import org.springframework.transaction.annotation.Transactional;

public interface HibernateUtils {

	@Transactional
	public abstract void flush();

}