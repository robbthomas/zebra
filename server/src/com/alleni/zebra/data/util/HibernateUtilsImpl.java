package com.alleni.zebra.data.util;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("hibernateUtils")
public class HibernateUtilsImpl implements HibernateUtils {
	@Autowired SessionFactory sessionFactory;

	@Transactional
	public void flush() {
		sessionFactory.getCurrentSession().flush();
	}
}
