package com.alleni.zebra.data.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.alleni.zebra.util.StringUtils;

public class UUID {
	static Logger log = Logger.getLogger(UUID.class);
	
	/**
	 * Generate a standard Java UUID.toString()
	 * 
	 * @see java.util.UUID
	 * @return a standard Java UUID String
	 */
	public static String standardUUID() {
		return java.util.UUID.randomUUID().toString();
	}
	
	/**
	 * Generate a UUID that:
	 *   + contains no '-'
	 *   + is 32 char's long
	 * 
	 * @return a UUID that conforms to the pattern used by Hibernate.
	 */
	public static String hibernateCompatibleUUID() {
		String base = standardUUID();
		base = StringUtils.removeChar(base, '-');
		return base;
	}

	
	private static final Pattern _uuidRegPattern = Pattern.compile("[a-zA-Z0-9]{32}");
	public static boolean isUUID(Object o) {
		boolean ret = false;
		if (o instanceof String) {
			Matcher m = _uuidRegPattern.matcher((String)o);
			ret = m.matches();
		}
		
		return ret;
	}
	
	
	/**
	 * According to current (8/6/2012) requirements for publishing, we must always return
	 * the latest version of a zapp, unless it's being viewed from the "my stuff" detail page,
	 * in which case we return the version that was purchased.  Additionally, we must track
	 * launch-counts, by APNUUID.
	 * 
	 * Most of this is accomplished in ProjectSearchHelperImpl.java, but the embed URL for
	 * the "my-stuff" page referenced above must be coerced to load the current user's version.
	 * For now, we do this by prepending a String to the APNUUID.
	 * 
	 * @param detailApnUUID
	 * @return UUID component if this is a "DetailApnUuId," or null if not.
	 */
	public static final String DETAIL_APN_UUID = "verlock";
	public static String extractVersionLockedApnUUID(String versionLockedApnUUID) {
		log.trace("extractDetailApnUUID in: [" + versionLockedApnUUID + "]");
		String ret = null;
	
		int index = versionLockedApnUUID.indexOf(DETAIL_APN_UUID);
		if (index != -1) {
			String uuid = versionLockedApnUUID.substring(DETAIL_APN_UUID.length());
			// shouldn't ever happen, but it's possible that we have a urlName that begins with our
			// marker string.
			if (isUUID(uuid)) {
				ret = uuid;
			}
		}
		log.trace("extractDetailApnUUID out: [" + ret + "]");
		return ret;
	}
}
