package com.alleni.zebra.security.authorization;

/**
 * See table:  RoleType
 *
 */
public enum Role {
	ROLE_TYPE_SITE_ADMIN(0),
	ROLE_TYPE_ACCOUNT_ADMINISTRATOR(1),
	ROLE_TYPE_AUTHOR(2),
	ROLE_TYPE_VIEWER(3),
	ROLE_TYPE_SALES_CONTACT(4),
	ROLE_TYPE_SUPPORT_CONTACT(5),
	ROLE_TYPE_FEEDBACK(6);
	
	public final int dbId;
	
	private Role(int dbId) {
		this.dbId = dbId;
	}
	
	public static Role dbValueOf(int dbId) {
		for (Role r : values())
			if (r.dbId == dbId)
				return r;
				
		return null;
	}
}
