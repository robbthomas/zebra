package com.alleni.zebra.security.authorization;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class LoginContextHolder {
	
	private LoginContextHolder() {}
	
	public static final LoginContext getContext() {
		SecurityContext sCtx = SecurityContextHolder.getContext();
		Authentication auth = sCtx.getAuthentication();
		return (LoginContext) auth.getPrincipal();
	}
}
