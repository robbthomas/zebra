package com.alleni.zebra.security.authorization;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityContextUtils {
	public static final boolean hasRoleCurrentUser(Role role) {

        Collection<GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		for (GrantedAuthority auth : authorities) {
			if (auth.equals(role.toString())) return true;
		}
		return false;
	}
}
