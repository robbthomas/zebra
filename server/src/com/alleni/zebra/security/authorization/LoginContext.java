package com.alleni.zebra.security.authorization;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.alleni.zebra.data.model.AppUser;

public class LoginContext implements UserDetails {
	private static final long serialVersionUID = 5848722697788523571L;

	private Collection<GrantedAuthority> grantedAuthorities;
	private final String password;
	private final String displayName;
	private final boolean accountNonExpired;
	private final boolean accountNonLocked;
	private final boolean credentialsNonExpired;
	private final boolean enabled;
	private final String salt;
	
	private AppUser appUser;
	
	public LoginContext(
			Collection<GrantedAuthority> grantedAuthorities, String password,
			String username, boolean accountNonExpired,
			boolean accountNonLocked, boolean credentialsNonExpired,
			boolean enabled, String salt) {
		super();
		this.grantedAuthorities = grantedAuthorities;
		this.password = password;
		this.displayName = username;
		this.accountNonExpired = accountNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.credentialsNonExpired = credentialsNonExpired;
		this.enabled = enabled;
		this.salt = salt;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return displayName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public AppUser getAppUser() {
		return appUser;
	}

	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}

	public String getSalt() {
		return salt;
	}

}
