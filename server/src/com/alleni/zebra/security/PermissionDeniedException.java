package com.alleni.zebra.security;

/**
 * Throw this exception when a user tries to access a piece of IP they don't have access to.
 *
 */
public class PermissionDeniedException extends RuntimeException {

	private static final long serialVersionUID = 4927368103294618323L;

	public PermissionDeniedException() {
		super();
	}

	public PermissionDeniedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public PermissionDeniedException(String arg0) {
		super(arg0);
	}

	public PermissionDeniedException(Throwable arg0) {
		super(arg0);
	}

}
