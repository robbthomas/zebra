package com.alleni.zebra.security.authentication.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.mapper.impl.AccountMapperImpl;
import com.alleni.zebra.data.mapper.impl.AppUserMapperImpl;
import com.alleni.zebra.data.model.Account;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.authorization.LoginContext;
import com.alleni.zebra.security.authorization.Role;

/**
 * I'm not sure why, yet, but Spring gets really unhappy when we register
 * this as a @Repository 
 *
 */
public class CustomJdbcDaoUserDetailsService extends JdbcDaoImpl {
	protected final Log log = LogFactory.getLog(getClass());

	//	@Autowired AppUserDao userDao;
	//		@Autowired AppRoleDao roleDao;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username)
	throws UsernameNotFoundException, DataAccessException {
		String adminName = null; // used for "sudo"-like logins, e.g. "dhoyt+++someUser" logs in as "someUser"
		if (username.indexOf("@@@") != -1) {
			String[] names = username.split("\\@\\@\\@");
			adminName = names[0];
			username = names[1];
		}
		
		LoginContext details = null;
		log.info("Looking up user: " + username);

		JdbcTemplate template = getJdbcTemplate();

		AppUser user = null;
		AppUser adminUser = null;
		boolean sudoGranted = false;
		try {
			
			// if sudo attempt, then look up admin user first, check admin permissions
			if (adminName != null) {
				adminUser = template.queryForObject(
						"select distinct(au.*), amr.accountId as amr_accountid from app_user au, accountmemberroles amr where  (au.displayName = ? or au.email = ?) and amr.memberId = au.id",
						new Object[] { adminName, adminName }, new AppUserMapperImpl());

				List<GrantedAuthority> auths = getAuthorities(adminUser);
				for (GrantedAuthority auth : auths){
					if (auth.toString().equals(Role.ROLE_TYPE_SITE_ADMIN.name())) {
						sudoGranted = true;
					}
				}
				if (!sudoGranted) {
					log.warn("FAILED SUDO ATTEMPT by user + " + adminName + " to user: " + username);
					throw new UsernameNotFoundException("nice TRY, buddy!");
				} else {
					log.warn("SUDO by user + " + adminName + " to user: " + username);
				}
			}
			
			// look up target user (sudo mode or normal mode same thing)
			user = template.queryForObject(
					"select distinct(au.*), amr.accountId as amr_accountid from app_user au, accountmemberroles amr where  (au.displayName = ? or au.email = ?) and amr.memberId = au.id",
					new Object[] { username, username }, new AppUserMapperImpl());
			
			// if sudo has been granted, then set the target users's password hash to that of the sudo-er
			// so the authentication layer can validate against admin's password.  Badd password will still fail normally.
			if (adminUser != null && sudoGranted) {
				user.setPasswd(adminUser.getPasswd());
			}
			
			Account account = null;
			account = template.queryForObject(
					"select accounts.* from accounts where accountid = ?",
					new Object[] {user.getAccountId()},
					new AccountMapperImpl());
			user.setAccount(account);
		} catch (IncorrectResultSizeDataAccessException e) {
			log.warn("login attemt detected for non-existent user: " + username);
			throw new UsernameNotFoundException("couldn't find user: " + username);
		}

		if (user != null) {
			log.debug("got user: " + user.getId());
		}
		
		details = new LoginContext(getAuthorities(user),
				user.getPasswd(), user.getUsername(), !user.isRetired(),
				!user.isEnabled(), !user.isRetired(), true, user.getSalt()); 
		details.setAppUser(user);

		return details;
	}


	@Transactional
	private List<GrantedAuthority> getAuthorities(AppUser user) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		log.debug("user id: " + user.getId());
		
		String roleSql = "select rt.tag from roletypes rt, app_user au, accountmemberroles amr where au.id = ? " +
			"and amr.memberid = au.id and rt.roleTypeId = amr.typeid";
		
		List<String> roles = getJdbcTemplate().queryForList(roleSql, new Object[] {user.getId()}, String.class);
		for (String role : roles) {
			authorities.add(new GrantedAuthorityImpl(role));
		}

		String adminSql = "select count(*) from systemadministrators where memberid = ?";
		int rows = 0;
		try {
			rows = getJdbcTemplate().queryForInt(adminSql, user.getId());
		} catch (Exception e) {
			log.error(e);
			throw new RuntimeException(e);
		}
		if (rows > 0) authorities.add(new GrantedAuthorityImpl(Role.ROLE_TYPE_SITE_ADMIN.toString()));
		else log.debug("not an admin");
		
		for (GrantedAuthority auth : authorities)
			log.debug(auth.getAuthority());
		return authorities;
	}

}
