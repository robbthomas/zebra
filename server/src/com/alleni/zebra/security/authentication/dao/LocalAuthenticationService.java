package com.alleni.zebra.security.authentication.dao;

import org.springframework.security.core.AuthenticationException;

import com.alleni.zebra.data.model.AppUser;

public interface LocalAuthenticationService {
	public AppUser authenticate(String username, String password) throws AuthenticationException;
}
