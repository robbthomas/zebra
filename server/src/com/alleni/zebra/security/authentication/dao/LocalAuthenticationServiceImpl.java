package com.alleni.zebra.security.authentication.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.web.spring.CustomShaPasswordEncoder;

@Component("localAuthenticationService")
public class LocalAuthenticationServiceImpl implements
		LocalAuthenticationService {

	@Autowired AppUserDao appUserDao;
	CustomShaPasswordEncoder encoder = new CustomShaPasswordEncoder();

	public AppUser authenticate(String username, String password)
			throws AuthenticationException {
		AppUser user = null;
		try {
			user = appUserDao.findByUsernameOrEmail(username);
			if (false == encoder.isPasswordValid(user.getPasswd(), password, user.getSalt())) {
				throw new AuthenticationCredentialsNotFoundException(username);
			}
		} catch (NotFoundException nfe) {
			throw new AuthenticationCredentialsNotFoundException(username);
		}
		
		return user;
	}

}
