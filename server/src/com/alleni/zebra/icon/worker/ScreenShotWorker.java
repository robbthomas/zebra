package com.alleni.zebra.icon.worker;

import java.util.UUID;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.dao.PublishedGadgetImageDao;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.s3.S3Service;

@Component("screenShotWorker")
public class ScreenShotWorker extends ImageWorker {
	Logger log = Logger.getLogger(ScreenShotWorker.class);

	@Autowired AppUserDao userDao;
	@Autowired S3Service s3Service;
	@Autowired PublishedGadgetImageDao imageDao;
	@Autowired Executor executor;
	@Autowired TransactionTemplate txTemplate;

	private final IconTarget target = IconTarget.PUBLISHED_SCREENSHOT;

	protected ScreenShotWorker() {	}

	@Override
	public PublishedGadgetImage processImage(final String projectId, final int index, final String appUserId, final byte[] data) {

		final PublishedGadgetImage image = new PublishedGadgetImage();
		final String id = UUID.randomUUID().toString();

		image.setProjectId(projectId);
		image.setScreenshotFileId(id);
		image.setRenderingComplete(false);
		image.setRetired(false);
		image.setDisplayOrder(index);

		imageDao.create(image);
		log.debug("created screenshot record.");



		try {
			for(int size : target.getSizes()) {
				log.debug("uploading and processing screenshot...");
				String filename = getFileSizeName(image.getScreenshotFileId(), size);
				byte[] createIcon = resize(data, size, target.getSquare(), target.getRounded(), MIN_RADIUS);
				s3Service.uploadScreenshotData(filename, data.length, "image/png", createIcon, true);
			}

			image.setRenderingComplete(true);
			imageDao.update(image);
			log.debug("set complete to true");
		} catch (Exception e) {
			log.debug(e);
			e.printStackTrace();
			log.error("Failed to upload an image: " + image.getId());
		}
		log.debug("  image processing complete.");

		return image;

	}


}
