package com.alleni.zebra.icon.worker;

import com.alleni.zebra.data.model.PublishedGadgetImage;

public interface ImageWorkerIf {

	/**
	 * Process an image, return the id of the new PublishedGadgetImage
	 * 
	 * The image record will have <code>renderingComplete == false</code> when this method returns.
	 * Clients should poll the database to find out when the job is done, by examining that value.
	 *  
	 * @param publishedId
	 * @param appUserId
	 * @param data
	 * @return
	 */
	public abstract PublishedGadgetImage processImage(String projectId, int index,
			String appUserId, byte[] data);

}