package com.alleni.zebra.icon.worker;

import java.util.UUID;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.dao.PublishedGadgetImageDao;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.s3.S3Service;

@Component("appIconWorker")
public class AppIconWorker extends ImageWorker {

	Logger log = Logger.getLogger(AppIconWorker.class);

	@Autowired AppUserDao userDao;
	@Autowired S3Service s3Service;
	@Autowired PublishedGadgetImageDao imageDao;
	@Autowired Executor executor;
	@Autowired TransactionTemplate transactionTemplate;
	
	private final IconTarget target = IconTarget.PUBLISHED_ICON;

	protected AppIconWorker() {	}

	public PublishedGadgetImage processImage(final String projectId, final int index, final String appUserId, final byte[] data) {

		return transactionTemplate.execute(new TransactionCallback<PublishedGadgetImage>() {

			// the code in this method executes in a transactional context
			public PublishedGadgetImage doInTransaction(TransactionStatus status) {
				final PublishedGadgetImage image = new PublishedGadgetImage();
				String id = UUID.randomUUID().toString();

				image.setProjectId(projectId);
				image.setIconFileId(id);
				image.setRenderingComplete(false);
				image.setDisplayOrder(index);
				image.setRetired(false);
				imageDao.create(image);
				log.debug("created icon record: " + image.getId());

				//		executor.execute(
				//				new Runnable() {
				//
				//					@Override
				//					@Transactional(propagation=Propagation.REQUIRES_NEW)
				//					public void run() {

				try {
					for(int size : target.getSizes()) {
						log.debug("uploading and processing icon...");
						String filename = getFileSizeName(image.getIconFileId(), size);
						byte[] createIcon = resize(data, size, target.getSquare(), target.getRounded(), MIN_RADIUS);
						s3Service.uploadIconData(filename, data.length, "image/png", createIcon, true);
					}

					image.setRenderingComplete(true);
					imageDao.update(image);

				} catch (Exception e) {
					log.error("Failed to upload an image: " + image.getId());
				}
				log.debug("  image processing complete.");

				//					}
				//				}
				//		);

				return image;
			}
		});
	}
}