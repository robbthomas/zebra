package com.alleni.zebra.icon.worker;

import com.alleni.zebra.data.model.PublishedGadgetImage;

@SuppressWarnings("unused")
public class UserIconWorker extends ImageWorker {
	private final IconTarget target = IconTarget.USER;

	@Override
	public PublishedGadgetImage processImage(String projectId, int index,
			String appUserId, byte[] data) {
		throw new UnsupportedOperationException("Not implemented");
	}



}
