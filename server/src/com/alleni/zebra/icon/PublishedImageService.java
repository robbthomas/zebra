package com.alleni.zebra.icon;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.security.PermissionDeniedException;

public interface PublishedImageService {
	public PublishedGadgetImage setIcon(String projectId, AppUser user, byte[] icon) throws DataAccessException, PermissionDeniedException;

	public PublishedGadgetImage addScreenshot(String projectId, int index, AppUser user, byte[] screenshot) throws DataAccessException, PermissionDeniedException;

	public void deleteScreenshotOrIcon(AppUser user, Long publishImageId) throws DataAccessException, PermissionDeniedException;

	public PublishedGadgetImage updateScreenshotOrIcon(AppUser user,
			PublishedGadgetImage updatedImage) throws DataAccessException, PermissionDeniedException;

	public void deleteScreenshot(String projectId, int index, AppUser user) throws DataAccessException, PermissionDeniedException;


}
