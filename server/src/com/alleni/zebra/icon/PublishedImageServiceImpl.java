package com.alleni.zebra.icon;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.dao.PublishedGadgetImageDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.icon.worker.ImageWorkerIf;
import com.alleni.zebra.license.Permissions;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.security.PermissionDeniedException;

@Service("publishedImageService")
public class PublishedImageServiceImpl implements PublishedImageService {
	Logger log = Logger.getLogger(PublishedImageServiceImpl.class);
	
	@Autowired AppUserDao auDao;
	@Autowired PublishedGadgetImageDao pgImageDao;
	@Autowired ProjectReadService prSvc;


	ImageWorkerIf iconWorker;
	ImageWorkerIf screenshotWorker;

	@Override
	@Transactional(readOnly=false)
	public PublishedGadgetImage setIcon(String projectId, AppUser user, byte[] icon) throws DataAccessException, PermissionDeniedException {
		
		Permissions.EDIT.checkAccess(user, prSvc.getMinimal(projectId, user));
		
		// remove any previous icons for this gadget
		pgImageDao.deleteAllIconsForPublishedGadget(projectId);
		
		// all index references are 0 for icons, as we can only ever have one.
		PublishedGadgetImage img = iconWorker.processImage(projectId, 0, (String)user.getId(), icon);
		return img;
	}

	public PublishedGadgetImage addScreenshot(String projectId, int index, AppUser user, byte[] screenshot) throws DataAccessException, PermissionDeniedException {
		Permissions.EDIT.checkAccess(user, prSvc.getMinimal(projectId, user));
		
		deleteScreenshot(projectId, index, user);
		PublishedGadgetImage img = screenshotWorker.processImage(projectId, index, (String)user.getId(), screenshot);
		return img;
	}

	

	@Override
	@Transactional
	public PublishedGadgetImage updateScreenshotOrIcon(AppUser user, PublishedGadgetImage updated) throws DataAccessException, PermissionDeniedException {
		Project p = prSvc.getMinimal(updated.getProjectId(), user);
		Permissions.EDIT.checkAccess(user, p);

		PublishedGadgetImage orig = pgImageDao.findById(updated.getId());
		
		updated.setIconFileId(orig.getIconFileId());
		updated.setScreenshotFileId(orig.getScreenshotFileId());
		updated.setRenderingComplete(orig.isRenderingComplete());
		updated.setRetired(orig.isRetired());
		
		updated = pgImageDao.update(updated);
		return updated;
	}
	

	
	@Override
	@Transactional
	public void deleteScreenshot(String projectId, int index,
			AppUser user) throws DataAccessException, PermissionDeniedException {
		List<PublishedGadgetImage> pgiList = pgImageDao.findScreenshotsByProjectIdAndIndex(projectId, index);
		if (pgiList != null) {
			for (PublishedGadgetImage pgi : pgiList) {
				Permissions.EDIT.checkAccess(user, prSvc.getMinimal(pgi.getProjectId(), user));
				pgImageDao.delete(pgi.getId());
			}
		}
	}


	
	@Override
	@Transactional
	public void deleteScreenshotOrIcon(AppUser user, Long publishImageId) throws DataAccessException, PermissionDeniedException {
		PublishedGadgetImage image = pgImageDao.findById(publishImageId);
		Project p = prSvc.getMinimal(image.getProjectId(), user);
		Permissions.EDIT.checkAccess(user, p);

		pgImageDao.delete(publishImageId);
	}

	
	


	@Autowired
	public void setIconWorker(@Qualifier("appIconWorker") ImageWorkerIf iconWorker) {
		this.iconWorker = iconWorker;
	}


	@Autowired
	public void setScreenshotWorker(@Qualifier("screenShotWorker") ImageWorkerIf screenshotWorker) {
		this.screenshotWorker = screenshotWorker;
	}


}
