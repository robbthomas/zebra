package com.alleni.zebra.web.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.log4j.Logger;

/**
 *  See:  http://stackoverflow.com/questions/1302072/how-can-i-get-the-http-status-code-out-of-a-servletresponse-in-a-servletfilter 
 *		and http://futurevsfantasy.livejournal.com/3388.html 
 */
public class StatusExposingServletResponse extends HttpServletResponseWrapper {
	private final Logger log = Logger.getLogger(getClass());
	private int httpStatus;

	public StatusExposingServletResponse(HttpServletResponse response) {
		super(response);
		log.debug("creating...");
		output = new ByteOutputStream();
		writer = new PrintWriter(output);
	}

	@Override
	public void sendError(int sc) throws IOException {
		httpStatus = sc;
		super.sendError(sc);
	}

	@Override
	public void sendError(int sc, String msg) throws IOException {
		httpStatus = sc;
		super.sendError(sc, msg);
	}


	@Override
	public void setStatus(int sc) {
		log.debug("setting status: " + sc);
		httpStatus = sc;
		super.setStatus(sc);
	}

	public int getStatus() {
		return httpStatus;
	}



	private PrintWriter writer;
	private ByteOutputStream output;

	public byte[] getBytes() {
		writer.flush();
		return output.getBytes();
	}


	@Override
	public PrintWriter getWriter() {
		return writer;
	}

	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		return output;
	}



	static class ByteOutputStream extends ServletOutputStream {
		private ByteArrayOutputStream bos = new ByteArrayOutputStream();

		@Override
		public void write(int b) throws IOException {
			bos.write(b);            
		}

		public byte[] getBytes() {
			return bos.toByteArray();
		}
	}



}
