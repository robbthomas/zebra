package com.alleni.zebra.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.MessageLevel;
import com.alleni.zebra.system.message.MessageType;
import com.alleni.zebra.system.message.SystemResponseMessage;
import com.alleni.zebra.web.controller.system.ErrorController;

public class JsonFormattingExceptionFilter implements Filter {
	private final Logger log = Logger.getLogger(getClass());

	@SuppressWarnings("unused")
	private FilterConfig filterConfig = null;

	@Override
	public void destroy() {
		this.filterConfig = null;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		
		try {
			chain.doFilter(req, resp);
			return;
		} catch (Exception e) {
			Throwable root = null;
			if (e.getCause() != null) { root = e.getCause(); }
			log.error(e);
			if (root != null) {
				for (StackTraceElement el : root.getStackTrace()) {
					log.error(el);
				}
			}
			
			
			SystemResponseMessage msg = new SystemResponseMessage(MessageCode.UNKNOWN_ERROR,
					MessageLevel.ERROR, MessageType.INTERNAL, "An unknown exception occurred while processing this request",
					e, null);
			HttpServletRequest request = (HttpServletRequest)req;
			
			request.getSession().setAttribute(ErrorController.SYSTEM_MESSAGE, msg);
			req.getRequestDispatcher("/error").forward(req, resp);
		}
	}


}
