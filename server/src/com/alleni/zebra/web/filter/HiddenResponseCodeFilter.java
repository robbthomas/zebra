package com.alleni.zebra.web.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

/**
 * Some clients are not able to read the response unless the HTTP response status is set to
 * 200.  For them, we reset the response status to 200, and store the actual value in a JSON variable called 'code'. 
 *
 */
public class HiddenResponseCodeFilter implements Filter {
	private final Logger log = Logger.getLogger(getClass());


	@SuppressWarnings("unused")
	private FilterConfig filterConfig = null;

	@Override
	public void destroy() {
		this.filterConfig = null;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	/*
	 * wrong:  {"id":"c549d210c0844f3ea9603c4c344f6330","message":"OK","level":"INFO","code":"200: OK","type":"BUSINESS, "http-code": 200}
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {


		if (isFakeRest(req)) {
			StatusExposingServletResponse wrapper = new StatusExposingServletResponse((HttpServletResponse)resp);

			chain.doFilter(req, wrapper);

			if (requiresJsonMunge(wrapper)) {
				String out = new String(wrapper.getBytes());

				int status = wrapper.getStatus();

				String terminator  = "}";

				int index = -1;
				if (out.endsWith(terminator)) {
					index = out.lastIndexOf(terminator);
				} else {
					index = out.lastIndexOf("]");
					terminator = "]";
				}

				if (index >= 0) {
					StringBuffer newOut = new StringBuffer(out.substring(0, index));

					log.debug("fake REST detected. Setting JSON 'http-code' variable to: " + status);

					newOut.append(", \"http-code\": " + status);
					newOut.append(terminator);
					((HttpServletResponse)resp).setStatus(200);


					log.debug("setting content-length to: " + newOut.length());
					resp.setContentLength(newOut.toString().getBytes().length);
					resp.getWriter().write(newOut.toString());

				} else {
					// says it's json, but doesn't end w/ "}" or "]"
					log.debug("setting content-length to: " + out.getBytes().length);
					resp.setContentLength(out.length());
					resp.getWriter().write(out);
				}

			} else {
				// fake REST, maybe JSON, but code == 200

				byte[] out = wrapper.getBytes();
				log.debug("setting content-length to: " + out.length);
				resp.setContentLength(out.length);
				IOUtils.write(out, resp.getOutputStream());
			}

		} else {

			log.debug("true rest client... no action needed.");
			chain.doFilter(req, resp);
		}

		return;
	}

	private boolean requiresJsonMunge(StatusExposingServletResponse wrapper) {
		if (wrapper == null) return false;

		int status = wrapper.getStatus();
		if (status == 200 || status == 0) return false;

		String contentType = wrapper.getContentType();
		if (contentType == null) return false;

		if (contentType.contains("application/json")) return true;

		return false;
	}



	@SuppressWarnings("unchecked")
	private boolean isFakeRest(ServletRequest req) {
		boolean fake = false;

		Enumeration<String> paramNames = req.getParameterNames();
		Enumeration<String> attrNames = req.getAttributeNames();

		fake = (containsFakeMethod(attrNames) || containsFakeMethod(paramNames));

		if (log.isTraceEnabled()) {
			paramNames = req.getParameterNames();
			attrNames = req.getAttributeNames();
			dumpHeaders(req, attrNames, paramNames);
		}

		return fake;
	}

	private void dumpHeaders(ServletRequest req, Enumeration<String> attrNames,
			Enumeration<String> paramNames) {
		while (attrNames.hasMoreElements()) {
			String name = attrNames.nextElement();
			log.trace(name + ": " + req.getAttribute(name));
		}
		while (paramNames.hasMoreElements()) {
			String name = paramNames.nextElement();
			log.trace(name + ": " + req.getAttribute(name));
		}
	}

	private boolean containsFakeMethod(Enumeration<String> names) {
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			if (name.equalsIgnoreCase("_method")) {
				return true;
			}
		}
		return false;

	}
}
