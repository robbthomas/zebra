package com.alleni.zebra.web.controller.site;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("redirectController")
public class TempSiteController {
	Logger log = Logger.getLogger(this.getClass().getCanonicalName());

	@RequestMapping("/tempLogin/assets/appui/controlArea/*") 
	public String redirect(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		String sp = req.getServletPath();
		String host = "http://" + req.getServerName() + "/flash/";
		String path = host + sp.substring(sp.lastIndexOf("/tempLogin") + 10);
		log.debug("redirecting to: " + path);
		resp.sendRedirect(path);
		return null;
	}
	
	
	@RequestMapping(value="/tempLogin/login", method=RequestMethod.POST)
	public String fakeLoginProc(HttpServletRequest req, HttpServletResponse resp, ModelMap model,
			@RequestParam String username, @RequestParam String password) {
		log.debug("processing login form");
		model.addAttribute("username", username);
		model.addAttribute("password", password);
		return "/temporaryEditor.jsp";
	}
	

	@RequestMapping(value="/tempLogin/launch", method=RequestMethod.GET)
	public String fakeLoginProcGet(HttpServletRequest req, HttpServletResponse resp, ModelMap model,
			@RequestParam String username, @RequestParam String password) {
		log.debug("processing login form");
		model.addAttribute("username", username);
		model.addAttribute("password", password);
		return "/temporaryEditor.jsp";
	}
	
	
	
	
	@RequestMapping(value="/tempLogin/login", method=RequestMethod.GET)
	public String fakeLogin(HttpServletRequest req, HttpServletResponse resp, ModelMap model) {
		log.debug("showing login form");
		model.addAttribute("hostname", req.getServerName());
		return "/temporaryLogin.jsp";
	}
}
