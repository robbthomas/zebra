package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.alleni.zebra.data.model.MyProject;
import com.alleni.zebra.data.model.PublishedGadgetImage;

public class MyProjectResponseWrapper {
	private MyProject myProject;
	
	public MyProjectResponseWrapper(MyProject mp) {
		setMyProject(mp);
	}

	/**
	 * Build a data structure for the icon that converts to JSON the editor can use.  
	 */

	
	public void setMyProject(MyProject mp) {
		this.myProject = mp;
	}

	public String getProjectId() {
		return myProject.getProjectId();
	}

	public int getCategoryId() {
		return myProject.getCategoryId();
	}
	
	public int getCommentCount() {
		return myProject.getCommentCount();
	}
	
	public Map<String, Object> getIcon() {
		Map<String, Object> ret = new HashMap<String, Object>();
		PublishedGadgetImage icon = myProject.getIcon();
		if (icon != null) {
			ret = icon.getIconMap();
		}
		return ret;
	}
	
	public long getAverageRating() {
		long rating = 0;
		long ratingTotal = myProject.getRatingTotal();
		long numRatings = myProject.getNumRatings();
		if (numRatings > 0 && ratingTotal > 0) {
			rating = ratingTotal / numRatings;
		}
		return rating;
	}

	public Map<String, Boolean> getPermissions() {
		Map<String,Boolean> permissions = new HashMap<String,Boolean>();
		permissions.put("edit", myProject.isEditable());
		permissions.put("owned", true);
		permissions.put("republish", myProject.isRepublishable());
		return permissions;
	}
	
	public Date getCreatedDateTime() {
		return myProject.getCreatedDateTime();
	}

	public Date getEditedDateTime() {
		return myProject.getEditedDateTime();
	}

	public String getProjectName() {
		return myProject.getProjectName();
	}

	public String getPublishedName() {
		return myProject.getPublishedName();
	}

	public String getApnUUID() {
		return myProject.getAccountProjectNumberId();
	}

	public boolean isOwned() {
		return true;
	}

	public String getUrlName() {
		return myProject.getUrlName();
	}

	public int getTotalCount() {
		return myProject.getTotalCount();
	}

	public String getDescription() {
		return myProject.getDescription();
	}

	public long getAccountId() {
		return myProject.getAccountId();
	}

	public boolean isPublished() {
		return myProject.isPublished();
	}

    public String getAuthorDisplayName() {
        return myProject.getAuthorDisplayName();
    }
    
	public int getNumRatings() {
		return myProject.getNumRatings();
	}

	public int getTotalRating() {
		return myProject.getRatingTotal();
	}
    
	public String getPlayerId() {
		return myProject.getPlayerId();
	}
	
	public String getProjectAccountTypeName() {
		return myProject.getProjectAccountTypeName();
	}
}
