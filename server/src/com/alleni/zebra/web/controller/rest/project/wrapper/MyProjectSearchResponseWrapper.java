package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.List;

public class MyProjectSearchResponseWrapper { 
	private final List<MyProjectResponseWrapper> projects;
	private final int totalCount;
	private final boolean hasMoreData;
	
	public MyProjectSearchResponseWrapper(List<MyProjectResponseWrapper> myProjects,
			int totalCount, boolean hasMoreData) {
		this.projects = myProjects;
		this.totalCount = totalCount;
		this.hasMoreData = hasMoreData;
	}
	
	
	public List<MyProjectResponseWrapper> getProjects() {
		return projects;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public boolean isHasMoreData() {
		return hasMoreData;
	}
	
	
}