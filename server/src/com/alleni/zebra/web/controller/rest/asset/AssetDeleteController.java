package com.alleni.zebra.web.controller.rest.asset;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.asset.AssetService;
import com.alleni.zebra.data.dao.AssetDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.MessageLevel;
import com.alleni.zebra.system.message.MessageType;
import com.alleni.zebra.system.message.SystemResponseMessage;

@Controller
public class AssetDeleteController {
	
	private final Logger log = Logger.getLogger(getClass());

	@Autowired AssetService assetService;
	@Autowired AssetDao assetDao;
	

	/**
	 * Sets "isDeleted" to true, and then updates the record in the database.
	 * 
	 * @param request
	 * @param response
	 * @param assetId
	 * @param userId
	 * @return SystemResponseMessage of the result.
	 * @throws Exception
	 */
	@Deprecated
	@RequestMapping(value="/asset/delete/{assetId}", method=RequestMethod.POST)
	public @ResponseBody SystemResponseMessage delete(HttpServletRequest request, HttpServletResponse response,
			AppUser user, @PathVariable String assetId) throws Exception {
		String userId = (String)user.getId();
			Asset asset = assetDao.findByIdAndAppUser(assetId, userId);
			assetService.delete(asset, userId);
		
    	return SystemResponseMessage.okMessage(assetId);
	}

	
	
	// ------   Exception Handlers   -----
	
	@ExceptionHandler({UniqueConstraintException.class, DataAccessException.class, ConcurrencyException.class, NotFoundException.class})
	public @ResponseBody Map<String,Object> handleDataAccessException(DataAccessException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);
		
		SystemResponseMessage msg;
		if (e instanceof NotFoundException) {
			msg = new SystemResponseMessage(MessageCode.ASSET_NOT_FOUND, MessageLevel.ERROR, MessageType.INTERNAL, 
					"The asset could not be found", e, e.getObjectId());
			
		} else if (e instanceof ConcurrencyException) {
			msg = new SystemResponseMessage(MessageCode.RECORD_LOCKED_IN_TRANSACTION, e.getObjectId());
			
		} else {

			msg = new SystemResponseMessage(MessageCode.UNKNOWN_ERROR, e.getObjectId());
		}
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(PermissionDeniedException.class)
	public @ResponseBody Map<String,Object> handlePermissionDenied(PermissionDeniedException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.NOT_ALLOWED, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

}
