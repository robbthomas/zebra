package com.alleni.zebra.web.controller.rest.asset;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jets3t.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import com.alleni.zebra.asset.AssetService;
import com.alleni.zebra.asset.UnsupportedMimeTypeException;
import com.alleni.zebra.asset.thumbnail.ThumbnailService;
import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.dao.AssetDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.MessageLevel;
import com.alleni.zebra.system.message.MessageType;
import com.alleni.zebra.system.message.SystemResponseMessage;

@Controller
@RequestMapping("/asset")
public class AssetController {
	private final Logger log = Logger.getLogger(getClass());

	@Autowired AssetService assetService;
	@Autowired AssetDao assetDao;
	@Autowired ThumbnailService thumbnailService;
	@Autowired AppUserDao userDao;

	@Autowired AssetUpdateController updateController;
	@Autowired AssetCreateController createController;


	public static class MultiPartFileUploadBean {
		
	}

	/**
	 * List the assets for a user.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<Asset> list(HttpServletRequest request, HttpServletResponse response, AppUser user) throws Exception {

		log.debug("listing assets for user [" + user.getId() + "]");
		List<Asset> assets = Collections.emptyList();
		assets = assetDao.findAllForUser((String)user.getId());
		return assets;
	}



	/**
	 * List the assets for a user.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/user/{userId}", method=RequestMethod.GET)
	public @ResponseBody List<Asset> listForOtherUser(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String userId) throws Exception {
		log.debug("listing assets for user [" + userId + "]");
		List<Asset> assets = Collections.emptyList();
		assets = assetDao.findAllForUser(userId);

		return assets;
	}



	/**
	 * Fetch the metadata for an Asset, if the user has permission.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param assetId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/info/{assetId}", method=RequestMethod.GET)
	public @ResponseBody Asset getInfo(HttpServletRequest request, HttpServletResponse response, AppUser user,
			@PathVariable String assetId) throws Exception {
		Asset asset = null;
		asset = assetDao.findByIdAndAppUser(assetId, (String)user.getId());
		return asset;

	}




	/**
	 * Download the data for an asset.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param assetId
	 * @param userId
	 * @throws Exception
	 */
	@RequestMapping(value="/stream/{assetId}", method=RequestMethod.GET )
	public void downloadBinaryAsset(WebRequest req, HttpServletRequest request, HttpServletResponse response, AppUser user,
			@PathVariable String assetId) throws Exception {
		String userId = (String)user.getId();
		log.debug("fetching thumbnail for asset: " + assetId + ", user:  " + userId);

        if (req.checkNotModified(0)) {  // If we have one in cache use that one
            return;
        }

        Asset asset = assetService.getAsset(assetId, userId);
        InputStream is = assetService.getMediaForAsset(assetId, userId);

		response.setContentType(asset.getMimeType());
		byte[] bytes = IOUtils.toByteArray(is);
		int length = bytes.length;
		log.debug("sending " + length + " bytes");
		response.setContentLength(length);
		IOUtils.write(bytes, response.getOutputStream());

	}


	@RequestMapping(value="/{assetId}", method=RequestMethod.PUT)
	public @ResponseBody Asset doPut(HttpServletRequest request, HttpServletResponse response, AppUser user,
			@PathVariable String assetId, @RequestHeader(required=false) String name64,
			@RequestHeader(required=false) String mime64, @RequestHeader(required=false) String permisssions64) throws Exception {
		return updateController.update(request, response, user, assetId, name64, mime64, permisssions64);
	}


	/**
	 * Sets "isDeleted" to true, and then updates the record in the database.
	 * 
	 * @param request
	 * @param response
	 * @param assetId
	 * @param userId
	 * @return SystemResponseMessage of the result.
	 * @throws Exception
	 */
	@RequestMapping(value="/{assetId}", method=RequestMethod.DELETE)
	public @ResponseBody SystemResponseMessage delete(HttpServletRequest request, HttpServletResponse response,
			AppUser user, @PathVariable String assetId) throws Exception {
		String userId = (String)user.getId();
		Asset asset = assetDao.findByIdAndAppUser(assetId, userId);
		assetService.delete(asset, userId);

		return SystemResponseMessage.okMessage(assetId);
	}



	/**
	 * Upload an Asset to the database, preserve data stream on S3, and generate a thumbnail,
	 * if applicable.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param name64
	 * @param mimeType64
	 * @param userId
	 * @param data
	 * @return updated Asset
	 * @throws Exception
	 */
	@RequestMapping(value="/{name64}/{mimeType64}", method=RequestMethod.POST )
	public @ResponseBody Map<String, Object> createImageAsset(HttpServletRequest request, HttpServletResponse response, AppUser user,
			@PathVariable String name64, @PathVariable String mimeType64,
			@RequestBody byte[] data) throws Exception {

		log.debug("Received upload request");
		return createController.createImageAsset(request, response, user, name64, mimeType64, data);
	}




	// ------   Exception Handlers   -----

	@ExceptionHandler({UniqueConstraintException.class, DataAccessException.class, ConcurrencyException.class, NotFoundException.class})
	public @ResponseBody Map<String,Object> handleDataAccessException(DataAccessException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg;
		if (e instanceof NotFoundException) {
			msg = new SystemResponseMessage(MessageCode.ASSET_NOT_FOUND, MessageLevel.ERROR, MessageType.INTERNAL, 
					"Asset not found", e, e.getObjectId());

		} else if (e instanceof ConcurrencyException) {
			msg = new SystemResponseMessage(MessageCode.RECORD_LOCKED_IN_TRANSACTION, e.getObjectId());

		} else {

			msg = new SystemResponseMessage(MessageCode.UNKNOWN_ERROR, e.getObjectId());
		}
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}


	@ExceptionHandler(PermissionDeniedException.class)
	public @ResponseBody Map<String,Object> handlePermissionDenied(PermissionDeniedException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.NOT_ALLOWED, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}


	@ExceptionHandler(UnsupportedMimeTypeException.class)
	public @ResponseBody Map<String,Object> handleMimeException(UnsupportedMimeTypeException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.UNSUPPORTED_MIME_TYPE, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(ServiceException.class)
	public @ResponseBody Map<String,Object> handleServiceException(ServiceException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.S3_DATA_NOT_FOUND, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}
}
