package com.alleni.zebra.web.controller.rest.asset;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alleni.zebra.data.exception.UnsupportedAssetMimeType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.asset.AssetService;
import com.alleni.zebra.asset.thumbnail.ThumbnailService;
import com.alleni.zebra.data.dao.AssetDao;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.SystemResponseMessage;
import com.alleni.zebra.util.StringUtils;

@Controller
public class AssetCreateController {
	private final Logger log = Logger.getLogger(getClass());

	@Autowired AssetService assetService;
	@Autowired AssetDao assetDao;
	@Autowired ThumbnailService thumbnailService;
	
	
	/**
	 * Upload an Asset to the database, preserve data stream on S3, and generate a thumbnail,
	 * if applicable.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param name64
	 * @param mimeType64
	 * @param userId
	 * @param data
	 * @return updated Asset
	 * @throws Exception
	 */
    @RequestMapping(value="/asset/save/{name64}/{mimeType64}", method=RequestMethod.POST )
    public @ResponseBody Map<String, Object> createImageAsset(HttpServletRequest request, HttpServletResponse response, AppUser user,
    		@PathVariable String name64, @PathVariable String mimeType64,
    		@RequestBody byte[] data) throws Exception {
 
    	log.debug("Received upload request");
    	String name = name64 == null? null : StringUtils.fromBase64(name64);
    	String mimeType = mimeType64 == null? null: StringUtils.fromBase64(mimeType64);
    	
        Asset asset = new Asset();
        asset.setDataSizeInBytes((long)data.length);
        asset.setName(name);
        asset.setMimeType(mimeType);
        asset.setUserId((String)user.getId());
        asset.setDeleted(false);
        asset.setPermissions("WORLD_READABLE");
        
        try {
        	if (asset.getDataSizeInBytes() > 0) {
        		assetService.createWithThumbnail(asset, data);
        	} else {
        		assetService.createMetaData(asset);
        	}
        } catch (UnsupportedAssetMimeType e) {
        	log.error(e);
        	SystemResponseMessage msg = new SystemResponseMessage(MessageCode.UNSUPPORTED_MIME_TYPE, e.getAssetId());
        	response.setStatus(msg.code.suggestedHttpRespCode);
        	return msg.values();
        }
        log.debug("returning OK");
    	return SystemResponseMessage.okMessage((String) asset.getId()).values();
    }
    
    

}
