package com.alleni.zebra.web.controller.rest.project;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.icon.PublishedImageService;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.system.message.SystemResponseMessage;

@Controller
@RequestMapping("/project")
public class ProjectImageController {
	Logger log = Logger.getLogger(ProjectImageController.class);
	
	@Autowired ProjectReadService readSvc;
	@Autowired PublishedImageService piSvc;
	
	/**
	 * Set the icon for a published thing 
	 */
	@RequestMapping(value = "{projectId}/icon", method = RequestMethod.POST)
	public @ResponseBody Map<String,Object> createIconsForPublishedGadget(AppUser user, @PathVariable String projectId,
			@RequestBody byte[] data) throws Exception {
		if(data == null || data.length == 0) {
			throw new IllegalArgumentException("Icon data not found");
		}
		PublishedGadgetImage image = piSvc.setIcon(projectId, user, data);
		return image.getIconMap();
	}


	/**
	 * Set the screen-shot for a published thing 
	 */
	@RequestMapping(value = "{projectId}/screenshot/{index}", method = RequestMethod.POST)
	public @ResponseBody Map<String,Object> createScreenshotForPublishedGadget(AppUser user, @PathVariable String projectId,
			@PathVariable int index, @RequestBody byte[] data) throws Exception {
		log.debug("receiving screenshot: publishId: " + projectId + "  index:  " + index);
		if(data == null || data.length == 0) {
			throw new IllegalArgumentException("Icon data not found");
		}
		PublishedGadgetImage image = piSvc.addScreenshot(projectId, index, user, data);
		return image.getScreenshotMap();
	}

	/**
	 * Delete the screen-shot for a published thing 
	 */
	@RequestMapping(value = "{projectId}/screenshot/{index}", method = RequestMethod.DELETE)
	public @ResponseBody Map<String,Object> deleteScreenshotForPublishedGadget(AppUser user, @PathVariable String projectId, @PathVariable int index) throws Exception {

		piSvc.deleteScreenshot(projectId, index, user);
		return SystemResponseMessage.okMessage("" + projectId + "/" + index).values();
	}


	

}
