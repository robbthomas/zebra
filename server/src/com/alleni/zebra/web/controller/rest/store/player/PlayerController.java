package com.alleni.zebra.web.controller.rest.store.player;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.MessageLevel;
import com.alleni.zebra.system.message.MessageType;
import com.alleni.zebra.system.message.SystemResponseMessage;
import com.alleni.zebra.util.StreamUtils;
import com.alleni.zebra.util.StringUtils;
import com.alleni.zebra.web.controller.rest.project.wrapper.ProjectResponseWrapper;
import com.alleni.zebra.web.controller.rest.store.LocalViewOnlyException;

@Controller("playerController")
@RequestMapping("/player")
public class PlayerController {
	Logger log = Logger.getLogger(PlayerController.class);

	@Autowired ProjectReadService projectReadSvc;
    @Autowired ProjectDao projectDao;
	
	/**
	 * Get a Project from an accountprojectnumbers.id
	 * 
	 * @param req
	 * @param resp
	 * @param uuid
	 * @param user
	 * @return
	 * @throws PermissionDeniedException
	 * @throws DataAccessException
	 * @throws Exception
	 */
	@RequestMapping("/{uuid}/{externalUserId}")
	public @ResponseBody ProjectResponseWrapper getPurchasedProject(HttpServletRequest req, HttpServletResponse resp,
			@PathVariable String uuid, AppUser user, @PathVariable String externalUserId) throws PermissionDeniedException, DataAccessException, Exception {
		boolean authorMode = externalUserId != null;
        Project p = projectReadSvc.loadForPlayerByApnUUID(uuid, user, externalUserId, authorMode);

		return new ProjectResponseWrapper(p);
	}

	@RequestMapping("/{uuid}")
	public @ResponseBody ProjectResponseWrapper getPurchasedProject(HttpServletRequest req, HttpServletResponse resp,
			@PathVariable String uuid, AppUser user) throws PermissionDeniedException, DataAccessException, Exception {
		return getPurchasedProject(req, resp, uuid, user, null);
	}



	/**
	 * Get an LMS (or other type) package from an accountprojectnumbers.id
	 */
	@RequestMapping("/package/{type}/{uuid}/{filename}.{ext}")
	public void getPackage(HttpServletRequest request, HttpServletResponse response, AppUser user,
			@PathVariable String type, @PathVariable String uuid,
			@PathVariable String filename, @PathVariable String ext) throws PermissionDeniedException, DataAccessException {
		Project pg = projectDao.findForAccountProjectNumbersUUID(uuid, user);
		filename = filename + "." + ext;

		response.setContentType("application/zip");
		if(!filename.matches("^[.a-zA-Z_]+$")) {
			throw new NotFoundException("Invalid filename");
		}
		if(!"scorm".equals(type)) {
			throw new NotFoundException("Invalid package type");
		}
		response.setHeader("Content-disposition", "attachment; filename=" + filename);

        String embedUrl = "";
        embedUrl += request.getScheme() + "://";
        if("localhost".equals(request.getServerName())) {
        	embedUrl += "zebrazapps.com";
        } else {
        	embedUrl += request.getServerName();
        }

        if(request.getServerPort() != 80 && request.getServerPort() != 443) {
        	embedUrl += ":" + request.getServerPort();
        }
        embedUrl += "/embed/lms/#/" + uuid;

		Map<String, String> replacements = new HashMap<String, String>();
		replacements.put("url", embedUrl);
		replacements.put("title", StringUtils.escapeXml(pg.getPublishedName()));
		replacements.put("catalog", "Zebra");
		replacements.put("version", pg.getVersionMajor()+'.'+pg.getVersionMinor()+'.'+pg.getVersionDot()+" ("+pg.getVersionBuildNumber()+')');
		replacements.put("description", StringUtils.escapeXml(pg.getDescription()));

		// metadata format: {"watermarkPosition":null,"contentWidth":800,"contentWidthPadding":0,"contentHeightPadding":0,"contentHeight":500}

		Map<String, Object> metadata = StringUtils.toJsonableObj(pg.getProjectMetadata());

		int width = 800;
		if(metadata.containsKey("contentWidth")) {
			width = Integer.parseInt(metadata.get("contentWidth").toString());
			if(metadata.containsKey("contentWidthPadding")) {
				width += Integer.parseInt(metadata.get("contentWidthPadding").toString());
			}
		}
		int height = 500;
		if(metadata.containsKey("contentHeight")) {
			height = Integer.parseInt(metadata.get("contentHeight").toString());
			if(metadata.containsKey("contentHeightPadding")) {
				height += Integer.parseInt(metadata.get("contentHeightPadding").toString());
			}
		}


		replacements.put("width", Integer.toString(width));
		replacements.put("height", Integer.toString(height));
		//replacements.put("keyword", "keyword");

		ZipInputStream zis = null;
		ZipOutputStream zos = null;
		try {
			zos = new ZipOutputStream(new BufferedOutputStream(response.getOutputStream()));
			zis = new ZipInputStream(new BufferedInputStream(PlayerController.class.getResourceAsStream("/scorm.zip")));
			ZipEntry ze;
			while ((ze = zis.getNextEntry()) != null) {
				ze = new ZipEntry(ze.getName());
				zos.putNextEntry(ze);
				if(ze.getName().endsWith(".html") || ze.getName().endsWith(".xml")) {
					StreamUtils.pipeReplace(zis, zos, replacements);
				} else {
					IOUtils.copy(zis, zos);
				}
				zos.closeEntry();
			}
		} catch (IOException e) {
			throw new RuntimeException("Streaming error while generating zip file", e);
		} finally {
			IOUtils.closeQuietly(zis);
			IOUtils.closeQuietly(zos);
		}
	}

//	private boolean remoteAndLocalAddressAreSame(HttpServletRequest req) throws UnknownHostException {
//		boolean ret = false;
//		String remoteAddr = req.getRemoteAddr(); // "127.0.0.1"
//		String localName = req.getLocalName(); // "localhost"
//		InetAddress remote = null;
//		String remoteName = null;
//		try {
//			remote = InetAddress.getByName(remoteAddr);
//			remoteName = remote.getHostName(); // localhost
//
//			ret = remoteName.equals(localName);
//		} catch (UnknownHostException e) {
//			log.fatal(e);
//			log.fatal("could not look up my own IP address!");
//			throw e;
//		}
//
//		return ret;
//	}

	@ExceptionHandler({UniqueConstraintException.class, DataAccessException.class, ConcurrencyException.class, NotFoundException.class})
	public @ResponseBody Map<String,Object> handleDataAccessException(DataAccessException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);
		//		e.printStackTrace();

		SystemResponseMessage msg;
		if (e instanceof UniqueConstraintException) {
			msg = new SystemResponseMessage(MessageCode.DUPLICATE_PUBLISHED_GADGET_NAME, e.getObjectId());

		} else if (e instanceof NotFoundException) {
			msg = new SystemResponseMessage(MessageCode.PUBLISHED_GADGET_NOT_FOUND, MessageLevel.ERROR, MessageType.INTERNAL, 
					"One or more required components of this project were not found", e, e.getObjectId());

		} else if (e instanceof ConcurrencyException) {
			msg = new SystemResponseMessage(MessageCode.RECORD_LOCKED_IN_TRANSACTION, e.getObjectId());

		} else {

			msg = new SystemResponseMessage(MessageCode.UNKNOWN_ERROR, e.getObjectId());
		}
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}


	@ExceptionHandler({LocalViewOnlyException.class})
	public @ResponseBody Map<String,Object> handlePrivatePublishExceptions(LocalViewOnlyException e, HttpServletRequest req, HttpServletResponse resp) {
		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.RECORD_LOCKED_IN_TRANSACTION, null);
		resp.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

}
