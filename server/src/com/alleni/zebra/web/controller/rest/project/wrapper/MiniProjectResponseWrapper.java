package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alleni.zebra.data.model.LicenseType;
import com.alleni.zebra.data.model.MiniProject;
import com.alleni.zebra.data.model.PublishedGadgetImage;

public class MiniProjectResponseWrapper {
	private MiniProject miniProject;
	
	public MiniProjectResponseWrapper(MiniProject mp) {
		setMiniProject(mp);
	}

	/**
	 * Build a data structure for the icon that converts to JSON the editor can use.  
	 */
	public Map<String, Object> getIcon() {
		Map<String, Object> ret = new HashMap<String, Object>();
		PublishedGadgetImage icon = miniProject.getIcon();
		if (icon != null) {
			ret = icon.getIconMap();
		}
		return ret;
	}
	
	public void setMiniProject(MiniProject mp) {
		this.miniProject = mp;
	}
	
	public String getProjectId() {
		return miniProject.getProjectId();
	}

	public int getCategoryId() {
		return miniProject.getCategoryId();
	}
	
	public int getCommentCount() {
		return miniProject.getCommentCount();
	}
	
	public int getPrice() {
		return miniProject.getPrice();
	}
	
	public Map<String, Boolean> getPermissions() {
		Map<String,Boolean> permissions = new HashMap<String,Boolean>();
		//permissions.put("read", miniProject.isCanRead());
		//permissions.put("share", miniProject.isCurrentUserCanShare());
		permissions.put("edit", miniProject.isEditable());
		permissions.put("embed", miniProject.isEmbedable() && !miniProject.isInviteOnly());
		permissions.put("owned", miniProject.isOwned());
		permissions.put("republish", miniProject.isRepublishable());
		//permissions.put("viewMetadata", miniProject.isViewMetadata());
		return permissions;
	}
	
	public boolean getIsInStore() {
		return (
			    miniProject.isPublished() 
			&& !miniProject.isHideInStoreList() 
			&& !miniProject.isDeleted() 
			&&  miniProject.getNextPublishedVersionId() == null
		);
	}
	
	public String getPlayerId() {
		return miniProject.getPlayerId();
	}
	
	public long getAverageRating() {
		long rating = 0;
		long ratingTotal = miniProject.getRatingTotal();
		long numRatings = miniProject.getNumRatings();
		if (numRatings > 0 && ratingTotal > 0) {
			rating = ratingTotal / numRatings;
		}
		return rating;
	}
	
	public Map<String, Object> getLicense() {
		LicenseType license = LicenseType.findById(miniProject.getLicenseTypeId());
		Map<String, Object> result = new HashMap<String, Object>();
		if (license != null) {
			result.put("id", license.id);
			result.put("projectTypeId", license.projectType.id);
			result.put("edit", license.edit);
			result.put("republish", license.republish);
			result.put("embed", license.republish);
		} else {
			result.put("id", false);
			result.put("projectTypeId", miniProject.getProjectTypeId());
			result.put("edit", false);
			result.put("republish", false);
			result.put("embed", false);
		}
		return result;
	}
	
	/**
	 * Build a data structure for screen-shots that converts to JSON the editor can use.  
	 */
	public List<Map<String, Object>> getScreenshots() {
		List<Map<String, Object>> ret = new ArrayList<Map<String,Object>>();

		List<PublishedGadgetImage> screenshots = miniProject.getScreenshots();

		if (screenshots != null) {
			for (PublishedGadgetImage image : screenshots) {				
				ret.add(image.getScreenshotMap());
			}
		}
		return ret;
	}
	
	public Date getCreatedDateTime() {
		return miniProject.getCreatedDateTime();
	}

	public Date getEditedDateTime() {
		return miniProject.getEditedDateTime();
	}

	public String getProjectName() {
		return miniProject.getProjectName();
	}

	public String getPublishedName() {
		return miniProject.getPublishedName();
	}

	public String getApnUUID() {
		return miniProject.getApnUUID();
	}

	public boolean isOwned() {
		return miniProject.isOwned();
	}

	public String getUrlName() {
		return miniProject.getUrlName();
	}

	public int getTotalCount() {
		return miniProject.getTotalCount();
	}

	public String getDescription() {
		return miniProject.getDescription();
	}

	public long getAccountId() {
		return miniProject.getAccountId();
	}

	public boolean isPublished() {
		return miniProject.isPublished();
	}

    public String getAuthorDisplayName() {
        return miniProject.getAuthorDisplayName();
    }
}
