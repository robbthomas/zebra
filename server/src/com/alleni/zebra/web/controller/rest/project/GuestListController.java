package com.alleni.zebra.web.controller.rest.project;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.data.hibernate.dao.GuestListDao;
import com.alleni.zebra.data.hibernate.dao.GuestListInviteeDao;
import com.alleni.zebra.data.hibernate.model.GuestList;
import com.alleni.zebra.data.hibernate.model.GuestListInvitee;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.guestlist.GuestListService;
import com.alleni.zebra.project.ProjectCreateService;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.system.message.SystemResponseMessage;
import com.alleni.zebra.web.controller.rest.project.wrapper.GuestListRequestWrapper;

@Controller
@RequestMapping("/guestlist")
public class GuestListController {
	Logger log = Logger.getLogger(GuestListController.class);
	
	@Autowired ProjectReadService readSvc;
	@Autowired ProjectCreateService createSvc;
	@Autowired GuestListDao glDao;
	@Autowired GuestListInviteeDao glInviteeDao;
	@Autowired GuestListService glService;

	private String getBaseUrl(HttpServletRequest req) {
		String url = req.getScheme() + "://" + req.getServerName() + "/";
		return url;
	}
	// -- create/update

	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody Map<String,Object> createGuestList(AppUser user, @RequestBody GuestListRequestWrapper wrapper, HttpServletRequest req) throws Exception {
		log.debug("POST guestlist");
		GuestList guestList = glService.create(user, wrapper.getProjectid(), wrapper.getGuestList(), wrapper.getGuestlistinvitees());
		glService.sendAllInvites(user, guestList, wrapper.getProjectid(), getBaseUrl(req));
		return SystemResponseMessage.okMessage(String.valueOf(guestList.getGuestlistid())).values();
	}

	@RequestMapping(method=RequestMethod.PUT)
	public @ResponseBody Map<String,Object> updateGuestList(AppUser user, @RequestBody GuestListRequestWrapper wrapper, HttpServletRequest req) throws Exception {
		log.debug("PUT guestlist");
		GuestList guestList = glService.update(user, wrapper.getProjectid(), wrapper.getGuestList(), wrapper.getGuestlistinvitees());
		glService.sendAllInvites(user, guestList, wrapper.getProjectid(), getBaseUrl(req));
		return SystemResponseMessage.okMessage(String.valueOf(guestList.getGuestlistid())).values();
	}
	

	// ---- by guestListId
	
	@RequestMapping(value = "{guestListId}", method=RequestMethod.GET)
	public GuestList findByGuestListId(AppUser user, @PathVariable Integer guestListId) throws Exception {
		return glDao.findById(guestListId);
	}
	
	@RequestMapping(value="/{guestListId}/invitees")
	public @ResponseBody Set<GuestListInvitee> getInviteesForGuestList(AppUser user, @PathVariable Integer guestListId) {
		return glInviteeDao.findAllByGuestListId(guestListId);
	}

	
	// ---- by projectId
	
	@RequestMapping(value="/project/{projectId}", method=RequestMethod.GET)
	public @ResponseBody GuestList findbyProjectId(AppUser user, @PathVariable String projectId) {
		GuestList gl = glService.findByProjectId(user, projectId);
		return gl;
	}
}
