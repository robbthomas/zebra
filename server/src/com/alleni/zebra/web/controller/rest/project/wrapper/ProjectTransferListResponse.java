package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alleni.zebra.data.model.ProjectTransfer;

public class ProjectTransferListResponse {

	class TransferAccount {
		Long accountId;
		String memberId;
        String email;
		String displayName;
		String accountName;
		List<ProjectTransfer> projectTransfers;
		
		public void addProjectTransfer(ProjectTransfer pt) {
			if (projectTransfers == null)
				projectTransfers = new ArrayList<ProjectTransfer>();
			
			projectTransfers.add(pt);
		}

		public Long getAccountId() {
			return accountId;
		}

        public String getMemberId() {
            return memberId;
        }

        public String getEmail() {
            return email;
        }

		public String getDisplayName() {
			return displayName;
		}

		public String getAccountName() {
			return accountName;
		}

		public List<ProjectTransfer> getProjectTransfers() {
			return projectTransfers;
		}
	}

	private List<TransferAccount> transferAccounts;
	private String message;
	
	
	
	public ProjectTransferListResponse(List<ProjectTransfer> data, boolean incoming, String message) {
		this.message = message;
		
		transferAccounts = new ArrayList<TransferAccount>();
		
		Map<String, TransferAccount> taMemberIdMap = new HashMap<String, TransferAccount>();
		
		for (ProjectTransfer pt : data) {
			Long accountId = null;
			String memberId = null;
            String email = null;
			String displayName = null;
			String accountName = null;

			if (incoming) {
				accountId =  pt.getSenderAccountId();
				memberId = pt.getSenderMemberId();
                email = pt.getSenderEmail();
				displayName = pt.getSenderDisplayName();
				accountName = pt.getSenderAccountName();
			} else {
				accountId = pt.getReceiverAccountId();
				memberId = pt.getReceiverMemberId();
                email = pt.getReceiverEmail();
				displayName = pt.getReceiverDisplayName();
				accountName = pt.getReceiverAccountName();
			}
			
			TransferAccount transferAccount = taMemberIdMap.get(memberId);

			if (transferAccount == null) {
				transferAccount = new TransferAccount();
				
				transferAccount.accountId = accountId;
				transferAccount.memberId = memberId;
                transferAccount.email = email;
				transferAccount.displayName = displayName;
				transferAccount.accountName = accountName;
				taMemberIdMap.put(memberId, transferAccount);
			}
			
			transferAccount.addProjectTransfer(pt);
		}
		
		for (String memberId : taMemberIdMap.keySet()) {
			transferAccounts.add(taMemberIdMap.get(memberId));
		}
	}

	
	public List<TransferAccount> getTransferAccounts() {
		return transferAccounts;
	}
	
	public String getMessage() {
		return message;
	}
}
