package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alleni.zebra.account.AccountType;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.data.model.CurrencyType;
import com.alleni.zebra.data.model.GadgetCategory;
import com.alleni.zebra.data.model.GadgetState;
import com.alleni.zebra.data.model.LicenseType;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.PublishStatus;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.gadget.ProjectType;
import com.alleni.zebra.gadget.json.GadgetContentParser;
import com.alleni.zebra.gadget.json.impl.GadgetContentParserImpl;
import com.alleni.zebra.gadgetstate.GadgetStateJsonMapper;
import com.alleni.zebra.gadgetstate.GadgetStateJsonMapperImpl;

public class ProjectResponseWrapper {
	private Project project;

	private GadgetContentParser contentParser;
	private List<ProjectResponseWrapper> children;
	private GadgetStateJsonMapper stateJsonMapper;

	public ProjectResponseWrapper(Project p) {
		this.contentParser = new GadgetContentParserImpl();
		this.stateJsonMapper = new GadgetStateJsonMapperImpl();
		setProject(p);
	}

	public void setProject(Project p) {
		this.project = p;
		if (p != null && p.getChildren() != null) {
			children = new ArrayList<ProjectResponseWrapper>();
			for (Project child : project.getChildren()) {
				children.add(new ProjectResponseWrapper(child));
			}
		}
	}


	public Map<String, Boolean> getPermissions() {
		Map<String,Boolean> permissions = new HashMap<String,Boolean>();
		permissions.put("read", project.isCanRead());
		permissions.put("share", project.isCurrentUserCanShare());
		permissions.put("edit", project.isEditable());
		permissions.put("embed", project.isEmbedable() && !project.isInviteOnly());
		permissions.put("owned", project.isOwned());
		permissions.put("republish", project.isRepublishable());
		permissions.put("viewMetadata", project.isViewMetadata());
		return permissions;
	}

	/**
	 * Build a data structure for the icon that converts to JSON the editor can use.  
	 */
	public Map<String, Object> getIcon() {
		Map<String, Object> ret = new HashMap<String, Object>();
		PublishedGadgetImage icon = project.getIcon();
		if (icon != null) {
			ret = icon.getIconMap();
		}
		return ret;
	}


	public String getProjectAccountName() {
		AccountType at = AccountType.valueOf(project.getAccountTypeId());
		if (at == null) {
			return null;
		} else {
			return at.toString();
		}

	}

	/**
	 * Build a data structure for screen-shots that converts to JSON the editor can use.  
	 */
	public List<Map<String, Object>> getScreenshots() {
		List<Map<String, Object>> ret = new ArrayList<Map<String,Object>>();

		List<PublishedGadgetImage> screenshots = project.getScreenshots();

		if (screenshots != null) {
			for (PublishedGadgetImage image : screenshots) {				
				ret.add(image.getScreenshotMap());
			}
		}
		return ret;
	}

	public Asset getPreviewImage() {
		return project.getPreviewImage();
	}


	/**
	 * Build a data structure for state that converts to JSON the editor can use.
	 */
	public Map<String, Object> getState() {
		Map<String, Object> ret = new HashMap<String,Object>();
		GadgetState state = project.getState();
		if (state != null) {
			ret = stateJsonMapper.toJsonableMap(state);
		}
		return ret;
	}

	public long getAverageRating() {
		long rating = 0;
		long ratingTotal = project.getRatingTotal();
		long numRatings = project.getNumRatings();
		if (numRatings > 0 && ratingTotal > 0) {
			rating = ratingTotal / numRatings;
		}
		return rating;
	}

	public Map<String, Object> getLicense() {
		LicenseType license = LicenseType.findById(project.getLicenseTypeId());
		Map<String, Object> result = new HashMap<String, Object>();
		if (license != null) {
			result.put("id", license.id);
			result.put("projectTypeId", license.projectType.id);
			result.put("edit", license.edit);
			result.put("republish", license.republish);
			result.put("embed", license.republish);
		} else {
			result.put("id", false);
			result.put("projectTypeId", project.getProjectTypeId());
			result.put("edit", false);
			result.put("republish", false);
			result.put("embed", false);
		}
		return result;
	}

	public String getProjectId() {
		return project.getProjectId();
	}

	public int getVersion() {
		return project.getVersion();
	}

	public Object getContent() {
		if (project.getContent() != null)
			return contentParser.toJsonableObj(project.getContent());
		else
			return null;
	}


	public Date getCreatedDateTime() {
		return project.getCreatedDateTime();
	}

	public void setCreatedDateTime(Date createdDateTime) {
		project.setCreatedDateTime(createdDateTime);
	}

	public boolean isDeleted() {
		return project.isDeleted();
	}

	public Object getInitialValues() {
		if(project.getInitialValues() != null) {
			return contentParser.toJsonableObj(project.getInitialValues());
		} else {
			return null;
		}
	}

	public Date getEditedDateTime() {
		return project.getEditedDateTime();
	}

	public String getProjectMetadata() {
		return project.getProjectMetadata();
	}

	public String getProjectName() {
		return project.getProjectName();
	}

	public int getCommentCount() {
		return project.getCommentCount();
	}
	
	public String getNextVersionId() {
		return project.getNextVersionId();
	}

	public String getAuthorMemberId() {
		return project.getAuthorMemberId();
	}

	public long getAccountId() {
		return project.getAccountId();
	}

	public ProjectType getProjectType() {
		return project.getProjectType();
	}

	public int getGadgetSize() {
		return project.getGadgetSize();
	}

	public boolean isAutoSave() {
		return project.isAutoSave();
	}

	public boolean isPublished() {
		return project.isPublished();
	}

	public String getPublishedMetadata() {
		return project.getPublishedMetadata();
	}

	public String getVersionString() {
		return project.getVersionMajor() +
				"." +
				project.getVersionMinor() +
				"." +
				project.getVersionDot() +
				"." +
				project.getVersionBuildNumber();
	}


	public int getCategoryId() {
		return project.getCategoryId();
	}

	public GadgetCategory getCategory() {
		return project.getCategory();
	}

	public String getPublishedName() {
		return project.getPublishedName();
	}

	public String getDescription() {
		return project.getDescription();
	}

	public Integer getStatusId() {
		return project.getStatusId();
	}

	public PublishStatus getPublishStatus() {
		return project.getPublishStatus();
	}

	public int getPrice() {
		return project.getPrice();
	}

	public int getSelfPrice() {
		return project.getSelfPrice();
	}

	public int getCurrencyTypeId() {
		return project.getCurrencyTypeId();
	}
	public CurrencyType getCurrencyType() {
		return project.getCurrencyType();
	}

	public boolean isHideInStoreList() {
		return project.isHideInStoreList();
	}

	public boolean isFeatured() {
		return project.isFeatured();
	}


	public boolean isInviteOnly() {
		return project.isInviteOnly();
	}


	public Long getGuestListId() {
		return project.getGuestListId();
	}

	public int getCachedPurchaseAcquisitionCount() {
		return project.getCachedPurchaseAcquisitionCount();
	}

	public long getRatingTotal() {
		return project.getRatingTotal();
	}

	public void setRatingTotal(int ratingTotal) {
		project.setRatingTotal(ratingTotal);
	}

	public long getNumRatings() {
		return project.getNumRatings();
	}
	public List<ProjectResponseWrapper> getChildren() {
		return children;
	}

	public int getProjectTypeId() {
		return project.getProjectTypeId();
	}
	public List<String> getChildrenIds() {
		return project.getChildrenIds();
	}
	public String getAuthorFirstName() {
		return project.getAuthorFirstName();
	}
	public String getAuthorLastName() {
		return project.getAuthorLastName();
	}
	public String getAuthorWebsite() {
		return project.getAuthorWebsite();
	}
	public String getAuthorDisplayName() {
		return project.getAuthorDisplayName();
	}
	public String getAuthorNameFormat() {
		return project.getAuthorNameFormat();
	}
	public String getAuthorCompanyName() {
		return project.getAuthorCompanyName();
	}
	public String getAuthorAccountName() {
		return project.getAuthorAccountName();
	}
	public int getMinPrice() {
		return project.getMinPrice();
	}
	public String getApnUUID() {
		return project.getApnUUID();
	}
	public String getPlayerId() {
		return project.getPlayerId();
	}
	public List<String> getShareTypes() {
		return project.getShareTypes();
	}
	public int getWidth() {
		return project.getWidth();
	}
	public int getHeight() {
		return project.getHeight();
	}
	public boolean isOwned() {
		return project.isOwned();
	}
	public boolean isPublisher() {
		return project.isPublisher();
	}

	public String getNextPublishedId() {
		return project.getNextPublishedVersionId();
	}

	public int getAccountTypeId() {
		return project.getAccountTypeId();
	}

	public String getUrlName() {
		return project.getUrlName();
	}

	public boolean isUpdateAvailable() {
		return project.isUpdateAvailable();
	}

	public List<String> getAssetIds() {
		return project.getAssetIds();
	}

	public List<Asset> getAssets() {
		return project.getAssets();
	}

	public Integer getLicenseTypeId() {
		return project.getLicenseTypeId();
	}

}
