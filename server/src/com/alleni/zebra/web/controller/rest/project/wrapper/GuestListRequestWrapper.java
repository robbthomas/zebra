package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alleni.zebra.data.hibernate.model.GuestList;

public class GuestListRequestWrapper {
	private GuestList guestList;
	
	/* Since the guestlist members are replaced with each update,
	 * we don't want the whole object.
	 */
	private List<String> guestlistinvitees;

	/* Projects table owns the FK to guestlists, so we
	 * make the association manually.
	 */
	private String projectid;
	
	public GuestListRequestWrapper() {
		this.guestList = new GuestList();
		this.guestlistinvitees = new ArrayList<String>();
	}
	
	public void setGuestlistid(int guestlistid) {
		guestList.setGuestlistid(guestlistid);
	}

	public void setEmailtext(String emailtext) {
		guestList.setEmailtext(emailtext);
	}

	public void setUpdateddatetime(Date updateddatetime) {
		guestList.setUpdateddatetime(updateddatetime);
	}

	public void setRetireddatetime(Date retireddatetime) {
		guestList.setRetireddatetime(retireddatetime);
	}

	public void setRetiredbyid(String retiredbyid) {
		guestList.setRetiredbyid(retiredbyid);
	}

	public void setRetiredreasontypeid(Long retiredreasontypeid) {
		guestList.setRetiredreasontypeid(retiredreasontypeid);
	}

	public void setGuestlistinvitees(List<String> guestlistinvitees) {
		this.guestlistinvitees = guestlistinvitees;
	}
	public List<String> getGuestlistinvitees() { return guestlistinvitees; }

	public String getProjectid() {
		return projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

	public GuestList getGuestList() {
		return guestList;
	}

	public void setGuestList(GuestList guestList) {
		this.guestList = guestList;
	}
	
}
