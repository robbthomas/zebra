package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.lang.reflect.Method;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

public class GenericJsonRequestParser <T>  {
	Logger log = Logger.getLogger(GenericJsonRequestParser.class);
	
	public T parse(Map<String,Object> map, Class<T> clazz) {
		Assert.notNull(map);
		Assert.notNull(clazz);
		
		T newInstance;
		try {
			newInstance = clazz.newInstance();
		} catch (Exception e) {
			log.error(e);
			throw new RuntimeException(e);
		}
		Method[] methods = clazz.getDeclaredMethods();
		for (Method m : methods) {
			String methodName = m.getName();
			if (methodName.startsWith("set")) {
				String fieldName = methodName.substring(3);
				log.debug("field: [" + clazz.getName() + "].["+fieldName+"]");
				fieldName = fieldName.substring(0,1).toLowerCase() +
						fieldName.substring(1);
				
				if (map.containsKey(fieldName)) {
					try {
						Object o = map.get(fieldName);
						if (o != null) {
							log.debug("method: [" + methodName + "]; argument type: [" + (o == null? "null" : o.getClass().getName()) +  "]; arg value: [" + o + "]");
							m.invoke(newInstance, o);
						}
					} catch (Exception e) {
						log.error(e);
						e.printStackTrace();
					}
				}
			}
		}
		return newInstance;
	}
	
	
}
