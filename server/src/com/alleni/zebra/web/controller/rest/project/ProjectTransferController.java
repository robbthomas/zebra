package com.alleni.zebra.web.controller.rest.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jets3t.service.S3ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.data.dao.ProjectTransferDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.ProjectTransfer;
import com.alleni.zebra.gadget.EditToDeletedItemException;
import com.alleni.zebra.gadget.store.CannotRepublishException;
import com.alleni.zebra.gadget.store.ProjectSharedException;
import com.alleni.zebra.gadget.store.ProjectTransferAcceptorService;
import com.alleni.zebra.gadget.store.ProjectTransferService;
import com.alleni.zebra.gadget.store.PublishException;
import com.alleni.zebra.gadget.store.impl.PublishPriceException;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.MessageLevel;
import com.alleni.zebra.system.message.MessageType;
import com.alleni.zebra.system.message.SystemResponseMessage;
import com.alleni.zebra.web.controller.rest.project.wrapper.ProjectTransferListResponse;

@Controller
@RequestMapping("/project/transfer")
public class ProjectTransferController {
	Logger log = Logger.getLogger(ProjectTransferController.class);
	
	@Autowired ProjectTransferDao ptDao;
	@Autowired ProjectTransferService ptSvc;
	@Autowired ProjectTransferAcceptorService ptAcceptSvc;
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public @ResponseBody Map<String, Integer> getCount(HttpServletRequest request, HttpServletResponse response, AppUser user) {
		List<ProjectTransfer> ptList = ptSvc.listIncoming(user);
		
		Map<String, Integer> ret = new HashMap<String, Integer>();
		
		ret.put("count", ptList.size());
		
		return ret;
	}
	
	@RequestMapping(value = "/accept/{projectMemberAccessId}", method = RequestMethod.POST)
	public @ResponseBody Map<String,Object> acceptTransfer(HttpServletRequest request, HttpServletResponse response, 
			AppUser user, @PathVariable Long projectMemberAccessId) throws DataAccessException, PermissionDeniedException, PublishException, IOException, S3ServiceException {
		Project p = ptAcceptSvc.accept(projectMemberAccessId, user, null);
		Map<String,Object> ret = SystemResponseMessage.okMessage(p.getProjectId()).values();
		ret.put("projectName", p.getProjectName());
		return ret;
	}

	@RequestMapping(value = "/accept/{projectMemberAccessId}/as/{altName}", method = RequestMethod.POST)
	public @ResponseBody Map<String,Object> acceptTransferWithName(HttpServletRequest request, HttpServletResponse response, 
			AppUser user, @PathVariable Long projectMemberAccessId, @PathVariable String altName) throws DataAccessException, PermissionDeniedException, PublishException, IOException, S3ServiceException {
		Project p = ptAcceptSvc.accept(projectMemberAccessId, user, altName);
		Map<String,Object> ret = SystemResponseMessage.okMessage(p.getProjectId()).values();
		ret.put("projectName", p.getProjectName());
		return ret;
	}

	
	@RequestMapping(value = "/incoming", method = RequestMethod.GET)
	public @ResponseBody ProjectTransferListResponse incoming(HttpServletRequest request,
			HttpServletResponse response, AppUser user) {
		log.debug("listing INCOMING projects for user [" + user.getUsername() + "]");
		
		// using the service instead of the DAO causes
		// each row returned to be updated after-the-fact,
		// as "viewed by user = true".
		return new ProjectTransferListResponse(ptSvc.listIncoming(user), true, "OK");
	}
	
	
	@RequestMapping(value = "/sent", method = RequestMethod.GET)
	public @ResponseBody ProjectTransferListResponse sent(HttpServletRequest request,
			HttpServletResponse response, AppUser user) {
		log.debug("listing OFFERED projects BY user [" + user.getUsername() + "]");
		
		List<ProjectTransfer> ptList = ptDao.list(user, false);
		return new ProjectTransferListResponse(ptList, false, "OK");
	}


	// no client code uses this.  If it's still here by Nov 15 and nothing's broken, delete it.
//	@RequestMapping(value="{projectId}/to/{receiverEmail:.*}", method = RequestMethod.POST)
//	public @ResponseBody ProjectTransfer create(HttpServletRequest request,
//			HttpServletResponse response, AppUser sender,
//			@PathVariable String projectId, @PathVariable String receiverEmail) throws Exception {
//		
//		AppUser receiver = userSvc.findByEmail(receiverEmail);
//		log.debug("offering project [" + projectId + "] FROM user [" + sender.getId() + "] TO user [" + receiver.getId() + "]");
//
//		return ptSvc.create(projectId, sender, receiver);
//	}
	

	@RequestMapping(value="/", method=RequestMethod.POST)
	public @ResponseBody Map<String,Object> createList(HttpServletRequest request,
			HttpServletResponse response, AppUser sender,
			@RequestBody List<Map<String,String>> xfers) throws DataAccessException, PublishException, PermissionDeniedException {
		
		// convert the JSON request body into a format that the service understands.
		List<ProjectTransferService.Request> requests = new ArrayList<ProjectTransferService.Request>();
		for (Map<String, String> xfer : xfers) {
			requests.add(new ProjectTransferService.Request(xfer.get(ProjectTransferService.EMAIL), xfer.get(ProjectTransferService.PROJECT_ID)));
		}
		
		ProjectTransferService.Response results = ptSvc.create(requests, sender);

		Map<String,Object> ret = null;
		if (results.failures.size() == 0) {
			ret = SystemResponseMessage.okMessage("OK").values();
		} else {
			SystemResponseMessage msg = new SystemResponseMessage(MessageCode.UNKNOWN_ERROR,"0");
			ret = msg.values();
		}
		ret.put("results", results);
		return ret;
	}
	
	
	@RequestMapping(value = "/cancel/{projectMemberAccessId}", method = RequestMethod.POST)
	public @ResponseBody Map<String,Object> cancel(HttpServletRequest request,
			HttpServletResponse response, AppUser user,
			@PathVariable Long projectMemberAccessId) throws DataAccessException, PermissionDeniedException {
		log.debug("canceling transfer ["+ projectMemberAccessId + "] BY user [" + user.getUsername() + "]");
		Project p = ptSvc.cancel(projectMemberAccessId, user);
		Map<String,Object> ret = new HashMap<String,Object>();
		ret = SystemResponseMessage.okMessage(String.valueOf(projectMemberAccessId)).values();
		ret.put("projectName", p.getProjectName());
		return ret;
	}


	

	// exception handlers


	@ExceptionHandler({UniqueConstraintException.class, DataAccessException.class, ConcurrencyException.class, NotFoundException.class})
	public @ResponseBody Map<String,Object> handleDataAccessException(DataAccessException e, HttpServletRequest request,  HttpServletResponse response) {
		log.warn(">>>> handling exception: ", e);
		//		e.printStackTrace();

		SystemResponseMessage msg;
		if (e instanceof UniqueConstraintException) {
			msg = new SystemResponseMessage(MessageCode.DUPLICATE_PUBLISHED_GADGET_NAME, e.getObjectId());

		} else if (e instanceof NotFoundException) {
			msg = new SystemResponseMessage(MessageCode.PUBLISHED_GADGET_NOT_FOUND, MessageLevel.ERROR, MessageType.INTERNAL, 
					"One or more required components of this project were not found", e, e.getObjectId());

		} else if (e instanceof ConcurrencyException) {
			msg = new SystemResponseMessage(MessageCode.RECORD_LOCKED_IN_TRANSACTION, e.getObjectId());

		} else {

			msg = new SystemResponseMessage(MessageCode.UNKNOWN_ERROR, e.getObjectId());
		}
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(PermissionDeniedException.class)
	public @ResponseBody Map<String,Object> handlePermissionDenied(PermissionDeniedException e, HttpServletRequest request,  HttpServletResponse response) {
		log.warn(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.NOT_ALLOWED, MessageLevel.ERROR, MessageType.INTERNAL, "You are not authorized to edit publishing data for this item.", e, null);

		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(EditToDeletedItemException.class)
	public @ResponseBody Map<String,Object> handleEditDeleted(EditToDeletedItemException e,  HttpServletRequest request,  HttpServletResponse response) {
		log.warn(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.EDIT_TO_DELETED_ITEM, MessageLevel.ERROR, MessageType.INTERNAL, "The item you are attempting to edit has been deleted", e, null);

		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(CannotRepublishException.class)
	public @ResponseBody Map<String,Object> handleRepublishException(CannotRepublishException e,  HttpServletRequest request,  HttpServletResponse response) {
		log.warn(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.CANNOT_REPUBLISH_GADGET, MessageLevel.ERROR,
				MessageType.INTERNAL, "The gadget you are attempting to publish contains a gadget for which you do not have sufficient re-publishing rights.", e, null);

		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(ProjectSharedException.class)
	public @ResponseBody Map<String,Object> handleProjectSharedException(ProjectSharedException e,  HttpServletRequest request,  HttpServletResponse response) {
		log.warn(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.PROJECT_IS_SHARED, MessageLevel.WARN,
				MessageType.BUSINESS, "The gadget you are attempting to publish has been shared to one or more services.", e, String.valueOf(e.getPublishId()));

		response.setStatus(msg.code.suggestedHttpRespCode);
		Map<String, Object> ret = msg.values();
		ret.put("shareTypes", e.getShareTypes());
		return ret;
	}

	@ExceptionHandler(Exception.class)
	public @ResponseBody Map<String,Object> handleUnknown(Exception e, HttpServletRequest request,  HttpServletResponse response) {
		log.warn(">>>> handling exception: ", e);
		e.printStackTrace();
		SystemResponseMessage msg = null;

		if (e instanceof PublishPriceException) {
			long minPrice =  ((PublishPriceException)e).getMinPrice();
			msg = new SystemResponseMessage(MessageCode.PUBLISH_PRICE_TOO_LOW, MessageLevel.ERROR,
					MessageType.BUSINESS, "The purchase price of this item must be at least [" + minPrice + "]", e, null);

			response.setStatus(msg.code.suggestedHttpRespCode);
			Map<String,Object> ret = msg.values();
			ret.put("minPrice", minPrice);
			return ret;

		} else {

			msg = new SystemResponseMessage(MessageCode.PUBLISH_FAILED, null);
			response.setStatus(msg.code.suggestedHttpRespCode);
			return msg.values();
		}
	}


	

}
