package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.data.model.TinyProject;

public class TinyProjectResponseWrapper {
	private TinyProject tinyProject;
	
	public TinyProjectResponseWrapper(TinyProject tp) {
		setTinyProject(tp);
	}
	
	public void setTinyProject(TinyProject tp) {
		this.tinyProject = tp;
	}
	
	public String getProjectId() {
		return tinyProject.getProjectId();
	}
	
	public String getProjectName() {
		return tinyProject.getProjectName();
	}
	
	public String getPublishedName() {
		return tinyProject.getPublishedName();
	}
	
	public Date getCreatedDateTime() {
		return tinyProject.getCreatedDateTime();
	}
	
	public String getDescription() {
		return tinyProject.getDescription();
	}

	public Map<String, Object> getIcon() {
		Map<String, Object> ret = new HashMap<String, Object>();
		PublishedGadgetImage icon = tinyProject.getIcon();
		if (icon != null) {
			ret = icon.getIconMap();
		}
		return ret;
	}
}
