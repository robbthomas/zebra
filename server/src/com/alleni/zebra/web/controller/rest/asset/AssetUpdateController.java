package com.alleni.zebra.web.controller.rest.asset;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.asset.AssetService;
import com.alleni.zebra.asset.thumbnail.ThumbnailService;
import com.alleni.zebra.data.dao.AssetDao;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.util.StringUtils;


/**
 *
 * The URIs defined here support the "legacy" client requests.
 *
 */
@Controller
public class AssetUpdateController {
	private final Logger log = Logger.getLogger(getClass());

	@Autowired AssetService assetService;
	@Autowired AssetDao assetDao;
	@Autowired ThumbnailService thumbnailService;

	
	/**
	 * Update the metadata and/or image data for an asset.
	 * 
	 * @param request
	 * @param response
	 * @param assetId
	 * @param userId
	 * @param data in request body
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/asset/update/{assetId}", method=RequestMethod.POST)
    public @ResponseBody Asset update(HttpServletRequest request, HttpServletResponse response, AppUser user,
			@PathVariable String assetId, @RequestHeader(required=false) String name64,
			@RequestHeader(required=false) String mime64, @RequestHeader(required=false) String permisssions64) throws Exception {

		String userId = (String)user.getId();
		
		Asset asset = null;

		// decode path and header variables
		String name = name64 == null? null : StringUtils.fromBase64(name64);
		String mime = mime64 == null? null : StringUtils.fromBase64(mime64);
		String permissions = permisssions64 == null? null : StringUtils.fromBase64(permisssions64);
		
		// look up asset, set new values
		asset = assetService.getAsset(assetId, userId);
		log.trace("asset before:  ");
		log.trace("\tname: " + asset.getName());
		log.trace("\tmime: " + asset.getMimeType());
		log.trace("\tpermissions: " + asset.getPermissions());
		log.trace("\tdataSize: " + asset.getDataSizeInBytes());

		
		asset.setName(StringUtils.nullOrEmpty(name)? asset.getName() : name);
		asset.setMimeType(StringUtils.nullOrEmpty(mime)? asset.getMimeType() : mime);
		asset.setPermissions(StringUtils.nullOrEmpty(permissions)? asset.getPermissions() : permissions);
//        if (data != null) asset.setDataSizeInBytes((long)data.length);

		log.trace("asset after:  ");
		log.trace("\tname: " + asset.getName());
		log.trace("\tmime: " + asset.getMimeType());
		log.trace("\tpermissions: " + asset.getPermissions());
		log.trace("\tdataSize: " + asset.getDataSizeInBytes());
		
		// let the service figure out the rest.
		assetService.update(asset, userId, null);
		

		return asset;
	}


}
