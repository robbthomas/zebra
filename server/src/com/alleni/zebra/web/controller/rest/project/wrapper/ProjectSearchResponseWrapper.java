package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.List;

public class ProjectSearchResponseWrapper {
	private final List<ProjectResponseWrapper> projects;
	private final int totalCount;
	private final boolean hasMoreData;
	
	public ProjectSearchResponseWrapper(List<ProjectResponseWrapper> projects,
			int totalCount, boolean hasMoreData) {
		this.projects = projects;
		this.totalCount = totalCount;
		this.hasMoreData = hasMoreData;
	}
	
	
	public List<ProjectResponseWrapper> getProjects() {
		return projects;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public boolean isHasMoreData() {
		return hasMoreData;
	}
	
	
}
