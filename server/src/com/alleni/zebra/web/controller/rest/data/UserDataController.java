package com.alleni.zebra.web.controller.rest.data;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.data.dao.UserVariableDao;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.UserVariable;
import com.alleni.zebra.userData.UserDataService;

@Controller
@RequestMapping("/data")
public class UserDataController {

	@Autowired UserVariableDao dao;
	@Autowired UserDataService svc;

	/**
	 * combined create/update 
	 */
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody UserVariable create(@RequestBody UserVariable userData, AppUser user) throws Exception {
		userData.setMemberId((String)user.getId());
		return dao.create(userData);
	}

	
	/**
	 * get one row 
	 */
	@RequestMapping(value="{projectId}/{lookupKey}", method=RequestMethod.GET)
	public @ResponseBody UserVariable get(@PathVariable String projectId, @PathVariable String lookupKey,
			AppUser user) throws Exception {
		return dao.find(projectId, (String)user.getId(), lookupKey);
	}
	
	
	/**
	 * list for project+lookupKey 
	 */
	@RequestMapping(value="/list/{projectId}/{lookupKey}", method=RequestMethod.GET)
	public @ResponseBody List<UserVariable> list(@PathVariable String projectId, @PathVariable String lookupKey,
			AppUser user) throws Exception {
		return dao.list(projectId, lookupKey);
	}
	
	/**
	 * histogram/distribution 
	 */
	@RequestMapping(value="/histogram/{projectId}/{lookupKey}", method=RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> histogram(@PathVariable String projectId, @PathVariable String lookupKey,
			AppUser user) throws Exception {
		return dao.histogram(projectId, lookupKey);
	}

	
	/**
	 * top N for projectId + lookkupKey
	 * 
	 */
	@RequestMapping(value="/top/{n}/{projectId}/{lookupKey}", method=RequestMethod.GET)
	public @ResponseBody List<UserVariable> topN(@PathVariable String projectId, @PathVariable String lookupKey,
			@PathVariable int n, AppUser user) throws Exception {
		return dao.getTopN(projectId, lookupKey, n);
	}

	/**
	 * bottom N for projectId + lookkupKey
	 * 
	 */
	@RequestMapping(value="/bottom/{n}/{projectId}/{lookupKey}", method=RequestMethod.GET)
	public @ResponseBody List<UserVariable> bottomN(@PathVariable String projectId, @PathVariable String lookupKey,
			@PathVariable int n, AppUser user) throws Exception {
		return dao.getBottomN(projectId, lookupKey, n);
	}


	/**
	 * mean, median, mode for projectId + lookkupKey
	 * 
	 */
	@RequestMapping(value="/averages/{projectId}/{lookupKey}", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> averages(@PathVariable String projectId, @PathVariable String lookupKey,
			AppUser user) throws Exception {
		return svc.averages(projectId, lookupKey);
	}


	
}
