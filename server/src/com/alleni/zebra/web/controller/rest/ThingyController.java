package com.alleni.zebra.web.controller.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.data.dao.ThingyDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.Thingy;

@Controller
@RequestMapping("/thingy")
public class ThingyController {
	private final Logger log = Logger.getLogger(getClass());

	@Autowired ThingyDao thingyDao;
	
	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<Thingy> getAll(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return thingyDao.findAll();
	}
	
	
    @RequestMapping(value="/{id}", method=RequestMethod.GET )
	public @ResponseBody Thingy doGet(HttpServletRequest request, HttpServletResponse response,
   		 @PathVariable Long id) throws Exception {
    	log.debug("GET");
    	Thingy thingy = null;
    	try {
    		thingy = thingyDao.findById(id);
    	} catch (NotFoundException nfe) {
    		log.debug("not found");
    	}
		return thingy;
	}

    
    @RequestMapping(method=RequestMethod.POST )
	public @ResponseBody Thingy doPost(HttpServletRequest request, HttpServletResponse response,
	   		 @RequestParam(value="name") String name) throws Exception {
    	log.debug("POST");
    	Thingy thingy = new Thingy();
    	thingy.setName(name);
		return thingyDao.create(thingy);
	}

    
    @RequestMapping(value="/{id}", method=RequestMethod.PUT )
	public @ResponseBody Thingy doPut(HttpServletRequest request, HttpServletResponse response,
	   		 @PathVariable(value="id") Long id,
	   		 @RequestParam(value="name") String name) throws Exception {
    	log.debug("PUT");
    	Thingy thingy = new Thingy();
    	thingy.setId(id);
    	thingy.setName(name);
		return thingyDao.update(thingy);
	}


    @RequestMapping(value="/{id}", method=RequestMethod.DELETE )
	public @ResponseBody Map<String,String> doDelete(HttpServletRequest request, HttpServletResponse response,
	   		 @PathVariable(value="id") Long id) throws Exception {
    	log.debug("DELETE");
    	thingyDao.delete(thingyDao.findById(id));
    	Map<String, String> ret = new HashMap<String,String>();
    	ret.put("status", "OK");
		return ret;
	}

}
