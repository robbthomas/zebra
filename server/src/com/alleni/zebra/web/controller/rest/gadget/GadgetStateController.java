package com.alleni.zebra.web.controller.rest.gadget;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.alleni.zebra.data.dao.GadgetStateDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.GadgetState;
import com.alleni.zebra.gadgetstate.GadgetStateJsonMapper;
import com.alleni.zebra.gadgetstate.GadgetStateService;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.MessageLevel;
import com.alleni.zebra.system.message.MessageType;
import com.alleni.zebra.system.message.SystemResponseMessage;

@Controller("gadgetStateController")
@RequestMapping(value="/gadgetstate")
public class GadgetStateController {
	private final Logger log = Logger.getLogger(getClass());

	@Autowired GadgetStateService gsSvc;
	@Autowired GadgetStateJsonMapper mapper;
	@Autowired GadgetStateDao dao;

	@RequestMapping(value="/gadget/{projectId}", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> getForGadget(HttpServletRequest request, HttpServletResponse response, AppUser user,
			@PathVariable ("projectId") String projectId, @RequestParam (value = "externalUserId", required=false) String externalUserId) throws ServletException, IOException, DataAccessException {
		log.debug("GET for project id: " + projectId);
		GadgetState state = null;
        boolean authorMode = externalUserId == null;
		state = gsSvc.getGetStateForProject(projectId, user, externalUserId, authorMode);
		return state == null? null : mapper.toJsonableMap(state);
	}


	@RequestMapping(value = "/{externalUserId}", method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> createOrUpdate(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, Object> stateMap, AppUser user,
            @PathVariable String externalUserId) 	throws ServletException, IOException, DataAccessException {
		log.debug("UPDATE state");
		GadgetState state = mapper.fromJsonMap(stateMap);
        state.setAppUserId((String)user.getId());
        state.setExternalUserId(externalUserId);
        state.setAuthorMode(false); // if it is not being saved with a project it is by default not authormode
		state = gsSvc.save(state, user);

		return state == null? null : mapper.toJsonableMap(state);
	}



	// ------   Exception Handlers   -----


	@ExceptionHandler({UniqueConstraintException.class, DataAccessException.class, ConcurrencyException.class, NotFoundException.class})
	public @ResponseBody Map<String,Object> handleDataAccessException(DataAccessException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);
		//		e.printStackTrace();

		SystemResponseMessage msg;
		if (e instanceof UniqueConstraintException) {
			msg = new SystemResponseMessage(MessageCode.GADGET_STATE_SAVE_FAILED, e.getObjectId());

		} else if (e instanceof NotFoundException) {
			msg = new SystemResponseMessage(MessageCode.GADGET_STATE_NOT_FOUND, MessageLevel.ERROR, MessageType.INTERNAL, 
					"Gadgetstate this project were not found", e, e.getObjectId());

		} else if (e instanceof ConcurrencyException) {
			msg = new SystemResponseMessage(MessageCode.RECORD_LOCKED_IN_TRANSACTION, e.getObjectId());

		} else {

			msg = new SystemResponseMessage(MessageCode.UNKNOWN_ERROR, e.getObjectId());
		}
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(PermissionDeniedException.class)
	public @ResponseBody Map<String,Object> handlePermissionDenied(PermissionDeniedException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);
		//		e.printStackTrace();

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.NOT_ALLOWED, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public @ResponseBody Map<String,Object> handleIllegalArgumentException(IllegalArgumentException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);
		//		e.printStackTrace();

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.GADGET_STATE_SAVE_FAILED, MessageLevel.ERROR, MessageType.INTERNAL, "Unable to save GadgetState: " + e.getMessage(), e, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}


}
