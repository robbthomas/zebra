package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.List;
import java.util.Map;

public class ProjectTransferRequest {
	List<Map<String,String>> data;

	public List<Map<String,String>> getData() {
		return data;
	}

	public void setData(List<Map<String,String>> data) {
		this.data = data;
	}
	
//	private String receiverEmail;
//	private String projectId;
//	
//	
//	public String getReceiverEmail() {
//		return receiverEmail;
//	}
//	public void setReceiverEmail(String receiverEmail) {
//		this.receiverEmail = receiverEmail;
//	}
//	public String getProjectId() {
//		return projectId;
//	}
//	public void setProjectId(String projectId) {
//		this.projectId = projectId;
//	}
}
