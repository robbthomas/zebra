package com.alleni.zebra.web.controller.rest;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.data.dao.AccountDao;
import com.alleni.zebra.data.model.AppUser;

@Controller
@RequestMapping("/echo")
public class EchoController {
	@Autowired AccountDao accountDao;

	@RequestMapping(method={RequestMethod.POST})
	public @ResponseBody Map<String, Object> list(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, Object> commandMap, AppUser user)
			throws Exception {
		Map<String, Object> resp = new HashMap<String,Object>();

		resp.put("timestamp", commandMap.get("timestamp"));
		resp.put("echo", processEchoMsg(commandMap));


		return resp;
	}




	private String processEchoMsg(Map<String, Object> commandMap) {
		String resp = "";
		String message = getIncomingMessage(commandMap);

		if (message != null && message.contains("a zebra says"))
			resp = "baaaaaaa!";

		else if (message != null && message.equalsIgnoreCase("ping"))
			resp =  "PONG";

		else
			resp =  message;

		return resp;
	}


	private String getIncomingMessage(Map<String, Object> commandMap) {
		String message = null;

		if (commandMap != null && commandMap.containsKey("message")) {
			message = (String) commandMap.get("message");
		}


		return message;
	}

}
