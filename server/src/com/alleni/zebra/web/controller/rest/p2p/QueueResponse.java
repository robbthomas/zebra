package com.alleni.zebra.web.controller.rest.p2p;

public class QueueResponse {
	public enum Code {
		OK,
		QUEUE_FULL,
		QUEUE_EMPTY
	}

	
	private Object data;
	private Code responseCode;

	
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Code getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Code responseCode) {
		this.responseCode = responseCode;
	}
}
