package com.alleni.zebra.web.controller.rest.asset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.asset.uploadableasset.UploadableAssetService;
import com.alleni.zebra.data.hibernate.dao.MimeTypeDao;
import com.alleni.zebra.data.hibernate.model.MimeType;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.model.AppUser;

@Controller("uploadableAssetController")
@RequestMapping("uploadableAsset")
public class UploadableAssetController {

	@Autowired MimeTypeDao mtDao;
	@Autowired UploadableAssetService uaSvc;

	@RequestMapping(value="/{uploadableAssetId}", method=RequestMethod.GET)
	public @ResponseBody UploadableAsset getById(AppUser user, @PathVariable Integer uploadableAssetId) {
		return uaSvc.findById(user, uploadableAssetId);
	}
	


	@RequestMapping(value="mimeType/{typeId}")
	public @ResponseBody MimeType getMimeType(@PathVariable Long typeId) {
		return mtDao.findById(typeId.intValue());
	}
	
	
	// remove comment for testing
//	@RequestMapping(value="/avatar", method=RequestMethod.GET)
//	public String getForm(HttpServletResponse resp, AppUser user) throws IOException {
//		return "avatarUploadForm.jsp";
//	}



}
