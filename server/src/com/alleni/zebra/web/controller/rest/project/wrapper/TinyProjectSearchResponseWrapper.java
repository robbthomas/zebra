package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.List;

public class TinyProjectSearchResponseWrapper {
	private final List<TinyProjectResponseWrapper> projects;
	private final int totalCount;
	private final boolean hasMoreData;
	
	public TinyProjectSearchResponseWrapper(List<TinyProjectResponseWrapper> tinyProjects,
			int totalCount, boolean hasMoreData) {
		this.projects = tinyProjects;
		this.totalCount = totalCount;
		this.hasMoreData = hasMoreData;
	}
	
	public List<TinyProjectResponseWrapper> getProjects() {
		return projects;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public boolean isHasMoreData() {
		return hasMoreData;
	}
}
