package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alleni.zebra.data.model.CurrencyType;
import com.alleni.zebra.data.model.GadgetCategory;
import com.alleni.zebra.data.model.GadgetState;
import com.alleni.zebra.data.model.LicenseType;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.PublishStatus;
import com.alleni.zebra.gadget.ProjectType;
import com.alleni.zebra.gadget.json.GadgetContentParser;
import com.alleni.zebra.gadget.json.impl.GadgetContentParserImpl;
import com.alleni.zebra.gadgetstate.GadgetStateJsonMapper;
import com.alleni.zebra.gadgetstate.GadgetStateJsonMapperImpl;

public class ProjectRequestWrapper {
	private Project project;
	
	
	private GadgetContentParser contentParser;
	@Autowired GadgetStateJsonMapper stateMapper;
	
    private boolean ignoreDuplicateName;
    private boolean ignoreDuplicatePublishedName;

    private String copyIconFromProjectId;
    private String copyScreenshotsFromProjectId;
    private String copyContentFromProjectId;
    
	public ProjectRequestWrapper() {
		this.contentParser = new GadgetContentParserImpl();
		this.stateMapper = new GadgetStateJsonMapperImpl();
		this.project = new Project();
	}

	public Project getProject() {
		return project;
	}



	public void setContent(Map<String,Object> content) {
		project.setContent(contentParser.toJsonString(content));
	}

    public void setGadgets(List<Map<String,Object>> gadgets) {
        // gadgets list comes in the form
        // [{"type":"ProjectReference","id":"84a6b2662b124a05b1f2546932ffe9e5"},{"type":"ProjectReference","id":"aac1f42d93224c29976ffa06fe591bbb"}]
        if(gadgets == null) {
            project.setChildrenIds(null);
        } else {
            List<String> copy = new ArrayList<String>(gadgets.size());
            for(Map<String,Object> entry : gadgets) {
                copy.add((String)entry.get("id"));
            }
            project.setChildrenIds(copy);
        }
    }

    public void setState(Map<String,Object> stateMap) {
        GadgetState state = stateMapper.fromJsonMap(stateMap);
        project.setState(state);
    }

    @SuppressWarnings("deprecation")
	public void setLicense(Map<String, Object> licenseMap) {
        Integer id = licenseMap.containsKey("id")? (Integer)licenseMap.get("id") : null;
        Boolean edit = licenseMap.containsKey("edit")? ((Boolean)licenseMap.get("edit")) : null;
        Boolean republish = licenseMap.containsKey("republish")? ((Boolean)licenseMap.get("republish")) : null;
        Boolean embed = licenseMap.containsKey("embed")? ((Boolean)licenseMap.get("embed")) : null;

        LicenseType lt = LicenseType.findById(id);
        if (lt == null) lt = LicenseType.findCanonical(id, republish, edit, embed, project.getProjectType());
        if (lt != null) {
        	project.setLicenseTypeId(lt.id);
       		project.setLicenseType(lt);
        }
   	}

	public void setProjectId(String projectId) {
		project.setProjectId(projectId);
	}
	public void setVersion(int version) {
		project.setVersion(version);
	}
//	public void setContent(String content) {
//		project.setContent(content);
//	}
	
	public void setCreatedDateTime(Date createdDateTime) {
		project.setCreatedDateTime(createdDateTime);
	}
	public void setDeleted(boolean deleted) {
		project.setDeleted(deleted);
	}


	public void setInitialValues(Map<String,Object> initialValues) {
		if (initialValues != null)
			project.setInitialValues(contentParser.toJsonString(initialValues));
	}


	public void setEditedDateTime(Date editedDateTime) {
		project.setEditedDateTime(editedDateTime);
	}


	public void setProjectMetadata(Map<String,Object> projectMetadata) {
		project.setProjectMetadata(contentParser.toJsonString(projectMetadata));
	}


	public void setProjectName(String projectName) {
		project.setProjectName(projectName);
	}
	public void setNextVersionId(String nextVersionId) {
		project.setNextVersionId(nextVersionId);
	}

	public void setProjectTypeId(int projectTypeId) {
		project.setProjectTypeId(projectTypeId);
        project.setProjectType(ProjectType.valueOf(projectTypeId));
	}
	public void setGadgetSize(int gadgetSize) {
		project.setGadgetSize(gadgetSize);
	}
	public void setAutoSave(boolean autoSave) {
		project.setAutoSave(autoSave);
	}
	public void setPublished(boolean published) {
		project.setPublished(published);
	}
	public void setPublishedMetadata(String publishedMetadata) {
		project.setPublishedMetadata(publishedMetadata);
	}
	public void setVersionMajor(Integer versionMajor) {
		project.setVersionMajor(versionMajor);
	}
	public void setVersionMinor(Integer versionMinor) {
		project.setVersionMinor(versionMinor);
	}
	public void setVersionDot(Integer versionDot) {
		project.setVersionDot(versionDot);
	}
	public void setVersionBuildNumber(Integer versionBuildNumber) {
		project.setVersionBuildNumber(versionBuildNumber);
	}
	public void setCategoryId(int categoryId) {
		project.setCategoryId(categoryId);
	}
	public void setCategory(GadgetCategory category) {
		project.setCategory(category);
	}
	public void setPublishedName(String publishedName) {
		project.setPublishedName(publishedName);
	}
	public void setDescription(String description) {
		project.setDescription(description);
	}
	public void setStatusId(Integer statusId) {
		project.setStatusId(statusId);
	}
	public void setPublishStatus(String publishStatus) {
		PublishStatus status = PublishStatus.valueOf(publishStatus);
		project.setStatusId(status == null ? 1 : status.id);
	}
	public void setPrice(int price) {
		project.setPrice(price);
	}
	public void setSelfPrice(int selfPrice) {
		project.setSelfPrice(selfPrice);
	}
	public void setCurrencyTypeId(int currencyTypeId) {
		project.setCurrencyTypeId(currencyTypeId);
	}
	public void setHideInStoreList(boolean hideInStoreList) {
		project.setHideInStoreList(hideInStoreList);
	}
	public void setFeatured(boolean featured) {
		project.setFeatured(featured);
	}
	public void setInviteOnly(boolean inviteOnly) {
		project.setInviteOnly(inviteOnly);
	}
	public void setGuestListId(long guestListId) {
		project.setGuestListId(guestListId);
	}
	public void setCachedPurchaseAcquisitionCount(
			int cachedPurchaseAcquisitionCount) {
		project.setCachedPurchaseAcquisitionCount(cachedPurchaseAcquisitionCount);
	}
	public void setRatingTotal(int ratingTotal) {
		project.setRatingTotal(ratingTotal);
	}
	public void setNumRatings(int numRatings) {
		project.setNumRatings(numRatings);
	}
//	public void setInitialValues(String initialValues) {
//		project.setInitialValues(initialValues);
//	}
	public void setProjectMetadata(String projectMetadata) {
		project.setProjectMetadata(projectMetadata);
	}
	public void setRepublishable(boolean republishable) {
		project.setRepublishable(republishable);
	}
	public void setEditable(boolean editable) {
		project.setEditable(editable);
	}
	public void setEmbedable(boolean embedable) {
		project.setEmbedable(embedable);
	}

	public void setApnUUID(String apnUUID) {
		project.setApnUUID(apnUUID);
	}

	public void setProjectType(String projectType) {
		ProjectType type = ProjectType.valueOf(projectType);
		
		project.setProjectTypeId(type == null? 1 : type.id);
		project.setProjectType(type);
	}

	public void setCurrencyType(String currencyType) {
		CurrencyType type = CurrencyType.valueOf(currencyType);
		project.setCurrencyTypeId(type == null? 1 : type.dbId);
	}

	public void setGuestListId(Long guestListId) {
		project.setGuestListId(guestListId);
	}

    public boolean isIgnoreDuplicateName() {
   		return ignoreDuplicateName;
   	}

   	public void setIgnoreDuplicateName(boolean ignoreDuplicateName) {
   		this.ignoreDuplicateName = ignoreDuplicateName;
   	}

    public boolean isIgnoreDuplicatePublishedName() {
   		return ignoreDuplicatePublishedName;
   	}

   	public void setIgnoreDuplicatePublishedName(boolean ignoreDuplicatePublishedName) {
   		this.ignoreDuplicatePublishedName = ignoreDuplicatePublishedName;
   	}
	
	public void setNextPublishedVersionId(String nextPublishedVersionId) {
		// ignore any input, as it is incorrect to supply this.
	}

	public void setRequestedNewProjectId(String requestedNewProjectId) {
		project.setRequestedNewProjectId(requestedNewProjectId);
	}

	public void setAccountTypeId(int accountTypeId) {
		project.setAccountTypeId(accountTypeId);
	}

    public String getCopyContentFromProjectId() {
        return copyContentFromProjectId;
    }

    public void setCopyContentFromProjectId(String copyContentFromProjectId) {
        this.copyContentFromProjectId = copyContentFromProjectId;
    }

    public String getCopyIconFromProjectId() {
        return copyIconFromProjectId;
    }

    public void setCopyIconFromProjectId(String copyIconFromProjectId) {
        this.copyIconFromProjectId = copyIconFromProjectId;
    }

    public String getCopyScreenshotsFromProjectId() {
        return copyScreenshotsFromProjectId;
    }

    public void setCopyScreenshotsFromProjectId(String copyScreenshotsFromProjectId) {
        this.copyScreenshotsFromProjectId = copyScreenshotsFromProjectId;
    }

	public void setWidth(int width) {
		project.setWidth(width);
	}

	public void setHeight(int height) {
		project.setHeight(height);
	}

	public void setOldName(String oldName) {
		project.setOldName(oldName);
	}
}
