package com.alleni.zebra.web.controller.rest.asset;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.asset.UnsupportedMimeTypeException;
import com.alleni.zebra.asset.thumbnail.ThumbnailNotSupportedException;
import com.alleni.zebra.asset.thumbnail.ThumbnailService;
import com.alleni.zebra.data.dao.AssetDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.MessageLevel;
import com.alleni.zebra.system.message.MessageType;
import com.alleni.zebra.system.message.SystemResponseMessage;
import org.springframework.web.context.request.WebRequest;

@Controller
@RequestMapping("/thumbnail")
public class ThumbnailController {
	private final Logger log = Logger.getLogger(getClass());

	private @Autowired ThumbnailService thumbnailService;
	private @Autowired AssetDao assetDao;

	@RequestMapping(value="/{assetId}", method=RequestMethod.GET )
	public void createImageAsset(WebRequest req, HttpServletRequest request, HttpServletResponse response, AppUser user,
			@PathVariable String assetId) throws Exception {
		String userId = (String)user.getId();
		log.debug("fetching thumbnail for asset: " + assetId + ", user:  " + userId);

        if (req.checkNotModified(0)) {  // If we have one in cache use that one
            log.debug("Already Cached");
            return;
        }

		// no permissions check according to current business rules.
		// if there were, they'd be enforced by ThumbnailService.
	    Asset asset = assetDao.findById(assetId);

		InputStream is = thumbnailService.getThumbnail(asset, userId);
		response.setContentType(ThumbnailService.DEFAULT_MIME_TYPE);
		byte[] bytes = IOUtils.toByteArray(is);
		int length = bytes.length;
		log.debug("sending " + length + " bytes");
		response.setContentLength(length);
		IOUtils.write(bytes, response.getOutputStream());

	}


	/**
	 * Generates a thumbnail from the file referred to by the Asset.  No user
	 * is provided, because it is assumed that the user is 'system.'
	 * 
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param assetId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/{assetId}", method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> createThumbnail(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String assetId) throws Exception {
		@SuppressWarnings("unchecked")

		/* debugging code */
		Enumeration<String> names = request.getHeaderNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			log.trace("name: " + name);
			log.trace("value: " + request.getHeader(name));
		}
		/* end debugging code */

		try {
			// most of the business logic is done in 3 lines of code.
			Asset asset = assetDao.findById(assetId);
			thumbnailService.generateAndUploadThumbnail(asset);
			return SystemResponseMessage.okMessage((String) asset.getId()).values();

		} catch (ThumbnailNotSupportedException e) {
			log.error(e);
			SystemResponseMessage msg =  new SystemResponseMessage(MessageCode.UNSUPPORTED_MIME_TYPE_FOR_THUMBNAIL, assetId);
			response.setStatus(msg.code.suggestedHttpRespCode);
			return msg.values();
		} catch (NotFoundException nfe) {
			log.error(nfe);
			SystemResponseMessage msg = new SystemResponseMessage(MessageCode.ASSET_NOT_FOUND, assetId);
			response.setStatus(msg.code.suggestedHttpRespCode);
			return msg.values();
		}

	}



	// ------   Exception Handlers   -----

	@ExceptionHandler({UniqueConstraintException.class, DataAccessException.class, ConcurrencyException.class, NotFoundException.class})
	public @ResponseBody Map<String,Object> handleDataAccessException(DataAccessException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg;
		if (e instanceof NotFoundException) {
			msg = new SystemResponseMessage(MessageCode.S3_THUMNAIL_NOT_FOUND, MessageLevel.ERROR, MessageType.INTERNAL, 
					"One or more required components of this project were not found", e, (String)null);

		} else if (e instanceof ConcurrencyException) {
			msg = new SystemResponseMessage(MessageCode.RECORD_LOCKED_IN_TRANSACTION, null);

		} else {

			msg = new SystemResponseMessage(MessageCode.UNKNOWN_ERROR, null);
		}
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(PermissionDeniedException.class)
	public @ResponseBody Map<String,Object> handlePermissionDenied(PermissionDeniedException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.NOT_ALLOWED, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}


	@ExceptionHandler(ThumbnailNotSupportedException.class)
	public @ResponseBody Map<String,Object> handleThumbException(ThumbnailNotSupportedException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.UNSUPPORTED_MIME_TYPE_FOR_THUMBNAIL, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(UnsupportedMimeTypeException.class)
	public @ResponseBody Map<String,Object> handleMimeException(UnsupportedMimeTypeException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.UNSUPPORTED_MIME_TYPE, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}


}
