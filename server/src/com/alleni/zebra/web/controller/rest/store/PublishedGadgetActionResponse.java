package com.alleni.zebra.web.controller.rest.store;

/**
 * Returned by a PublishedGadgetController on create/update
 *
 */
public class PublishedGadgetActionResponse {
	
	private long publishedGadgetId;
	private String gadgetId;
	
	
	public long getPublishedGadgetId() {
		return publishedGadgetId;
	}
	public void setPublishedGadgetId(long publishedGadgetId) {
		this.publishedGadgetId = publishedGadgetId;
	}
	public String getGadgetId() {
		return gadgetId;
	}
	public void setGadgetId(String gadgetId) {
		this.gadgetId = gadgetId;
	}

}
