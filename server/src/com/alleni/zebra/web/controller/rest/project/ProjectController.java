package com.alleni.zebra.web.controller.rest.project;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.jets3t.service.S3ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.account.AppUserService;
import com.alleni.zebra.data.dao.AccountDao;
import com.alleni.zebra.data.dao.GadgetCategoryDao;
import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.dto.MiniProjectListSearchResultSet;
import com.alleni.zebra.data.dto.MyProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectQuery;
import com.alleni.zebra.data.dto.TinyProjectListSearchResultSet;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.exception.PublishedNameInUseByOtherUserException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.model.AccountProjectNumber;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.data.model.GadgetCategory;
import com.alleni.zebra.data.model.LicenseType;
import com.alleni.zebra.data.model.MiniProject;
import com.alleni.zebra.data.model.MyProject;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.TinyProject;
import com.alleni.zebra.data.util.UUID;
import com.alleni.zebra.gadget.EditToDeletedItemException;
import com.alleni.zebra.gadget.ProjectType;
import com.alleni.zebra.gadget.store.CannotRepublishException;
import com.alleni.zebra.gadget.store.IllegalNameException;
import com.alleni.zebra.gadget.store.ProjectSharedException;
import com.alleni.zebra.gadget.store.PublishException;
import com.alleni.zebra.gadget.store.impl.PublishPriceException;
import com.alleni.zebra.project.ProjectCreateParameters;
import com.alleni.zebra.project.ProjectCreateService;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.project.ProjectRenameService;
import com.alleni.zebra.project.ProjectUpdateService;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.security.authorization.Role;
import com.alleni.zebra.security.authorization.SecurityContextUtils;
import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.MessageLevel;
import com.alleni.zebra.system.message.MessageType;
import com.alleni.zebra.system.message.SystemResponseMessage;
import com.alleni.zebra.util.StringUtils;
import com.alleni.zebra.web.controller.rest.project.wrapper.GenericJsonRequestParser;
import com.alleni.zebra.web.controller.rest.project.wrapper.MiniProjectResponseWrapper;
import com.alleni.zebra.web.controller.rest.project.wrapper.MiniProjectSearchResponseWrapper;
import com.alleni.zebra.web.controller.rest.project.wrapper.MyProjectResponseWrapper;
import com.alleni.zebra.web.controller.rest.project.wrapper.MyProjectSearchResponseWrapper;
import com.alleni.zebra.web.controller.rest.project.wrapper.ProjectPermissionsPriceReqWrapper;
import com.alleni.zebra.web.controller.rest.project.wrapper.ProjectRequestWrapper;
import com.alleni.zebra.web.controller.rest.project.wrapper.ProjectResponseWrapper;
import com.alleni.zebra.web.controller.rest.project.wrapper.ProjectSearchResponseWrapper;
import com.alleni.zebra.web.controller.rest.project.wrapper.TinyProjectResponseWrapper;
import com.alleni.zebra.web.controller.rest.project.wrapper.TinyProjectSearchResponseWrapper;

@Controller
@RequestMapping("/project")
public class ProjectController {


	Logger log = Logger.getLogger(ProjectController.class);

	@Autowired ProjectCreateService pCreateSvc;
	@Autowired ProjectReadService projectReadSvc;
	@Autowired ProjectUpdateService projectUpdateSvc;
	@Autowired AccountDao accountDao;
	@Autowired AppUserService userSvc;
	@Autowired ProjectDao projectDao;
	@Autowired GadgetCategoryDao gadgetCategoryDao;
	@Autowired ProjectRenameService pRenameSvc;
	
	public static class MultiPartFileUploadBean {

		private String icon;
		private List<String> screenshots;
		private List<String> deletedScreenshots;
		private String previewImage;
		private String project;

		public MultiPartFileUploadBean() {
		}

		public String getProject() {
			return project;
		}

		public void setProject(String project) {
			this.project = project;
		}

		public List<String> getScreenshots() {
			return screenshots;
		}

		public void setScreenshots(List<String> screenshots) {
			this.screenshots = screenshots;
		}

		public List<String> getDeletedScreenshots() {
			return deletedScreenshots;
		}

		public void setDeletedScreenshots(List<String> deletedScreenshots) {
			this.deletedScreenshots = deletedScreenshots;
		}

		public String getIcon() {
			return icon;
		}

		public void setIcon(String icon) {
			this.icon = icon;
		}

		public String getPreviewImage() {
			return previewImage;
		}

		public void setPreviewImage(String previewImage) {
			this.previewImage = previewImage;
		}
	}
	
	

	@RequestMapping(method=RequestMethod.POST, value="/multipart")
	public @ResponseBody ProjectResponseWrapper createProjectWithImages(MultiPartFileUploadBean upload, AppUser user) throws Exception {
		byte[] projectData = Base64.decodeBase64(upload.project);
		@SuppressWarnings(value = "unchecked")
		Map<String,Object> newProjectMap = new ObjectMapper().readValue(projectData, 0, projectData.length, HashMap.class);
		byte[] iconData = null;
		Map<Integer, byte[]> screenshotsData = null;
		Asset previewImage = null;
		byte[] previewImageData = null;
		if(upload.icon != null) {
			iconData = Base64.decodeBase64(upload.icon);
		}
		if(upload.deletedScreenshots != null) {
			screenshotsData = new HashMap<Integer, byte[]>();
			for(String d : upload.deletedScreenshots) {
				screenshotsData.put(Integer.decode(d), new byte[0]);
			}
		}
		if(upload.screenshots != null) {
			if(screenshotsData == null) {
				screenshotsData = new HashMap<Integer, byte[]>();
			}
			for(int i=0; i < upload.screenshots.size(); i++) {
				screenshotsData.put(i, Base64.decodeBase64(upload.screenshots.get(i)));
			}
		}
		if(upload.previewImage != null) {
			previewImage = new Asset();
			previewImage.setMimeType("image/png");
			previewImage.setName((String)newProjectMap.get("projectName"));
			previewImageData = Base64.decodeBase64(upload.previewImage);
		}

		Project p = createProject(newProjectMap, user, iconData, screenshotsData, previewImage,  previewImageData);

		return new ProjectResponseWrapper(p);
	}

	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody ProjectResponseWrapper createProjectM(@RequestBody Map<String,Object> newProjectMap, AppUser user) throws Exception {
		Project p = createProject(newProjectMap, user, null, null, null, null);

		return new ProjectResponseWrapper(p);
	}

	private Project createProject(Map<String,Object> newProjectMap, AppUser user, byte[] icon, Map<Integer, byte[]> screenshots, Asset previewImage, byte[] previewImageData) throws S3ServiceException, DataAccessException, PermissionDeniedException, PublishException, IOException  {
		GenericJsonRequestParser<ProjectRequestWrapper> parser = new GenericJsonRequestParser<ProjectRequestWrapper>();
		ProjectRequestWrapper wrapper = parser.parse(newProjectMap, ProjectRequestWrapper.class);
		Project p = wrapper.getProject();

		ProjectCreateParameters psr = new ProjectCreateParameters(p);
		psr.setIgnoreDuplicateNames(wrapper.isIgnoreDuplicateName());
		psr.setIgnoreDuplicatePublishedNames(wrapper.isIgnoreDuplicatePublishedName());
		psr.contentFromProjectId = wrapper.getCopyContentFromProjectId();
		psr.iconFromProjectId = wrapper.getCopyIconFromProjectId();
		psr.screenshotsFromProjectId = wrapper.getCopyScreenshotsFromProjectId();
		psr.icon = icon;
		psr.screenshots = screenshots;
		psr.previewImage = previewImage;
		psr.previewImageData = previewImageData;
		p = pCreateSvc.create(user, psr);

		p = projectReadSvc.getMinimal(p.getProjectId(), user);
		return p;
	}

	@RequestMapping(value = "/searchHistory", method = RequestMethod.POST)
	public @ResponseBody ProjectSearchResponseWrapper searchHistory(@RequestBody Map<String,Object> searchHistoryMap,
			AppUser user) throws Exception {
		
		if (searchHistoryMap.get("projectName") ==  null) {
			throw new NotFoundException("No project name sent.");
		}
		
		return search(
			  user, null, null, null, null, null, null, null, null, null, null, null
			, null, null, null, null, null, null, null, null, null, null, null, null
			, null, null, null, null, null, null, (String)searchHistoryMap.get("projectName")
			, null
		);
	}

	
	@RequestMapping(method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody ProjectSearchResponseWrapper search(AppUser user,
			@RequestParam(value="name",required=false) String name,
			@RequestParam(value="description",required=false) String description,
			@RequestParam(value="categoryId",required=false) String categoryId,
			@RequestParam(value="minPrice",required=false) String minPrice,
			@RequestParam(value="maxPrice",required=false) String maxPrice,
			@RequestParam(value="size",required=false) String size,
			@RequestParam(value="offset",required=false) String offset,
			@RequestParam(value="sort",required=false) String sortColumn,
			@RequestParam(value="versionMinor",required=false) String versionMinor,
			@RequestParam(value="versionMajor",required=false) String versionMajor,
			@RequestParam(value="versionDot",required=false) String versionDot,
			@RequestParam(value="order",required=false) String order,
			@RequestParam(value="minEditedDate",required=false) String minEditedDate,
			@RequestParam(value="maxEditedDate",required=false) String maxEditedDate,
			@RequestParam(value="genericSearch",required=false) String genericSearch,
			@RequestParam(value="featured",required=false) Boolean featured,
			@RequestParam(value="published",required=false) Boolean published,
			@RequestParam(value="deleted",required=false) Boolean deleted,
			@RequestParam(value="projectTypeId",required=false) String type,
			@RequestParam(value="showContent",required=false) Boolean showContent,
			@RequestParam(value="showInitialValues",required=false) Boolean showInitialValues,
			@RequestParam(value="userName",required=false) String userName,
			@RequestParam(value="authorMemberId",required=false) String authorMemberId,
			@RequestParam(value="ownerAccountId",required=false) Long ownerAccountId,
			@RequestParam(value="publisherAccountId",required=false) Long publisherAccountId,
			@RequestParam(value="templates",required=false) Boolean templates,
			@RequestParam(value="storeList", required=false) Boolean storeList,
			@RequestParam(value="projectId", required=false) String projectId,
			@RequestParam(value="history", required=false) Boolean history,
			@RequestParam(value="projectName", required=false) String projectName,
			@RequestParam (value = "lod", required=false) String lod
			) throws DataAccessException, PermissionDeniedException, UnsupportedEncodingException {

		if(templates != null && templates.equals(true)) {
			user = userSvc.findByEmail("template@zebrazapps.com");
			authorMemberId = user.getId();
			ownerAccountId = null;
			published = true;
			type = String.valueOf(ProjectType.APP.id);
		}

		if(userName != null) {
				AppUser otherUser = userSvc.findByUsernameOrEmail(userName);
				authorMemberId = otherUser.getId();
		}

		ProjectQuery q = new ProjectQuery();
		if ("minimal".equals(lod)) {
			q.setShowContent(false);
			q.setShowInitialValues(false);
		}
		q.setName(name == null? null : URLDecoder.decode(name, StringUtils.ENCODING_UTF8));
		q.setDescription(description);
		q.setCategoryId(categoryId != null? Long.valueOf(categoryId) : null);
		q.setMinPrice(minPrice != null? Integer.valueOf(minPrice) : null);
		q.setMaxPrice(maxPrice != null? Integer.valueOf(maxPrice) : null);
		q.setSize(size != null? Integer.valueOf(size) : null);
		q.setOffset(offset != null? Integer.valueOf(offset) : null);
		q.setSortColumn(sortColumn);
		q.setVersionMinor(versionMinor != null? Integer.valueOf(versionMinor) : null);
		q.setVersionMajor(versionMajor != null? Integer.valueOf(versionMajor) : null);
		q.setVersionDot(versionDot != null? Integer.valueOf(versionDot) : null);
		q.setOrder(order);
		try {
			q.setMinEditedDate(minEditedDate != null? DateUtils.parseDate(minEditedDate) : null);
		} catch (DateParseException ignore) {}
		try {
			q.setMaxEditedDate(maxEditedDate != null? DateUtils.parseDate(maxEditedDate) : null);
		} catch (DateParseException ignore) {}
		q.setGenericSearch(genericSearch == null? null : URLDecoder.decode(genericSearch, StringUtils.ENCODING_UTF8));
		q.setFeatured(Boolean.TRUE == featured);
		q.setPublished(Boolean.TRUE == published);
		q.setForStoreList(Boolean.TRUE == storeList); // <---
		q.setRetired(Boolean.TRUE == deleted);
		q.setType(type != null? ProjectType.valueOf(Integer.valueOf(type)) : null);
		q.setAuthorMemberId(authorMemberId);
		q.setOwnerAccountId(ownerAccountId);
		q.setPublisherAccountId(publisherAccountId);
		q.setShowContent(Boolean.TRUE == showContent);
		q.setShowInitialValues((q.isShowContent() && showInitialValues != null)? showInitialValues : false);
		q.setProjectId(projectId);
		q.setHistory(Boolean.TRUE == history);
		q.setProjectName(projectName == null? null : URLDecoder.decode(projectName.replace("+", "%2B"), "UTF-8").replace("%2B", "+"));

		ProjectListSearchResultSet searchResults = projectReadSvc.search(q, user);

		List<ProjectResponseWrapper> wrapperList = new ArrayList<ProjectResponseWrapper>(searchResults.projects.size());
		for (Project p : searchResults.projects) {
			ProjectResponseWrapper prw = new ProjectResponseWrapper(p);
			wrapperList.add(prw);
		}

		return new ProjectSearchResponseWrapper(wrapperList, searchResults.totalCount, searchResults.hasMoreData);
	}

	@RequestMapping(value = "/mini", method = RequestMethod.GET)
	public @ResponseBody MiniProjectSearchResponseWrapper searchMiniProject(
		AppUser user,
		@RequestParam(value="size",required=false) String size,
		@RequestParam(value="offset",required=false) String offset,
		@RequestParam(value="published",required=false) Boolean published,
		@RequestParam(value="ownerAccountId",required=false) Long ownerAccountId,
		@RequestParam(value="projectTypeId",required=false) String type,
		@RequestParam(value="categoryId",required=false) String categoryId,
		@RequestParam(value="genericSearch",required=false) String genericSearch,
		@RequestParam(value="projectName", required=false) String projectName,
		@RequestParam(value="sort",required=false) String sortColumn,
		@RequestParam(value="order",required=false) String order,
		@RequestParam(value="templates",required=false) Boolean templates,
		@RequestParam(value="userName", required=false) String userName
		) throws DataAccessException, PermissionDeniedException, UnsupportedEncodingException  {
		
		//if (!SecurityContextUtils.hasRoleCurrentUser(Role.ROLE_TYPE_SITE_ADMIN) 
		//		&& published && user.getAccountId() != ownerAccountId) {
		//	throw new PermissionDeniedException();
		//}
		
		ProjectQuery q = new ProjectQuery();
		
		q.setType(type != null? ProjectType.valueOf(Integer.valueOf(type)) : null);
		if(templates != null && templates.equals(true)) {
			user = userSvc.findByEmail("template@zebrazapps.com");
			ownerAccountId = null;
			published = true;
			type = String.valueOf(ProjectType.APP.id);
		}
		
		if(userName != null) {
			AppUser otherUser = userSvc.findByUsernameOrEmail(userName);
			q.setAuthorMemberId(otherUser.getId());
		} else {
			q.setAuthorMemberId(user.getId());
		}
		
		q.setSize(size != null? Integer.valueOf(size) : null);
		q.setOffset(offset != null? Integer.valueOf(offset) : null);
		q.setPublished(Boolean.TRUE == published);
		
		q.setCategoryId(categoryId != null? Long.valueOf(categoryId) : null);
		q.setOwnerAccountId(ownerAccountId);
		q.setSortColumn(sortColumn);
		q.setOrder(order);
		q.setGenericSearch(genericSearch == null? null : URLDecoder.decode(genericSearch, StringUtils.ENCODING_UTF8));
		q.setProjectName(projectName == null? null : URLDecoder.decode(projectName, StringUtils.ENCODING_UTF8));
		MiniProjectListSearchResultSet searchResults = projectReadSvc.searchMiniStuff(q, user);
		List<MiniProjectResponseWrapper> wrapperList = new ArrayList<MiniProjectResponseWrapper>(searchResults.projects.size());
		for (MiniProject mp : searchResults.projects) {
			MiniProjectResponseWrapper mprw = new MiniProjectResponseWrapper(mp);
			wrapperList.add(mprw);
		}
		
		return new MiniProjectSearchResponseWrapper(wrapperList, searchResults.totalCount, searchResults.hasMoreData);
	}
	
	
	@RequestMapping(value = "/my", method = RequestMethod.GET)
	public @ResponseBody MyProjectSearchResponseWrapper searchMyProject(
		AppUser user,
		@RequestParam(value="projectTypeId",required=false) String type,
		@RequestParam(value="published",required=false) Boolean published,
		@RequestParam(value="size",required=false) String size,
		@RequestParam(value="offset",required=false) String offset,
		@RequestParam(value="sort",required=false) String sortColumn,
		@RequestParam(value="order",required=false) String order,
		@RequestParam(value="categoryId",required=false) String categoryId,
		@RequestParam(value="genericSearch",required=false) String genericSearch
		) throws DataAccessException, PermissionDeniedException, UnsupportedEncodingException  {
		
		ProjectQuery q = new ProjectQuery();
		
		q.setType(type != null? ProjectType.valueOf(Integer.valueOf(type)) : null);
		
		q.setAuthorMemberId(user.getId());
		
		q.setSize(size != null? Integer.valueOf(size) : null);
		q.setOffset(offset != null? Integer.valueOf(offset) : null);
		q.setPublished(Boolean.TRUE == published);
		
		q.setCategoryId(categoryId != null? Long.valueOf(categoryId) : null);
		q.setSortColumn(sortColumn);
		q.setOrder(order);
		q.setGenericSearch(genericSearch == null? null : URLDecoder.decode(genericSearch, StringUtils.ENCODING_UTF8));
		MyProjectListSearchResultSet searchResults = projectReadSvc.searchMyStuff(q, user);
		List<MyProjectResponseWrapper> wrapperList = new ArrayList<MyProjectResponseWrapper>(searchResults.projects.size());
		for (MyProject mp : searchResults.projects) {
			MyProjectResponseWrapper mprw = new MyProjectResponseWrapper(mp);
			wrapperList.add(mprw);
		}
		
		return new MyProjectSearchResponseWrapper(wrapperList, searchResults.totalCount, searchResults.hasMoreData);
	}
	
	@RequestMapping(value = "/tiny", method = RequestMethod.GET)
	public @ResponseBody TinyProjectSearchResponseWrapper searchTinyProject(
		AppUser user,
		@RequestParam(value="projectTypeId", required=false) String type,
		@RequestParam(value="published", required=false) Boolean published,
		@RequestParam(value="projectName", required=false) String projectName,
		@RequestParam(value="urlName", required=false) String urlName,
		@RequestParam(value="sort",required=false) String sortColumn,
		@RequestParam(value="order",required=false) String order,
		@RequestParam(value="userName", required=false) String userName,
		@RequestParam(value="retired", required=false) Boolean retired,
		@RequestParam(value="projectNameList", required=false) String projectNameList
		) throws DataAccessException, PermissionDeniedException, UnsupportedEncodingException {
		
		ProjectQuery q = new ProjectQuery();
		
		q.setType(type != null? ProjectType.valueOf(Integer.valueOf(type)) : null);
		
		if(userName != null) {
			AppUser otherUser = userSvc.findByUsernameOrEmail(userName);
			q.setAuthorMemberId(otherUser.getId());
		} else {
			q.setAuthorMemberId(user.getId());
		}
		
		q.setPublished(Boolean.TRUE == published);
		q.setProjectName(projectName);
		q.setUrlName(urlName);
		if (retired != null) q.setRetired(retired);
		q.setSortColumn(sortColumn);
		q.setOrder(order);
		
		if (projectNameList != null) q.setProjectNameList(projectNameList);
		
		TinyProjectListSearchResultSet searchResults = projectReadSvc.searchTinyStuff(q, user);
		List<TinyProjectResponseWrapper> wrapperList = new ArrayList<TinyProjectResponseWrapper>(searchResults.projects.size());
		for (TinyProject mp : searchResults.projects) {
			TinyProjectResponseWrapper mprw = new TinyProjectResponseWrapper(mp);
			wrapperList.add(mprw);
		}
		
		return new TinyProjectSearchResponseWrapper(wrapperList, searchResults.totalCount, searchResults.hasMoreData);
	}
	
	/**
	 * 
	 * @param projectId
	 * @param user
	 * @return
	 * @throws DataAccessException
	 * @throws PermissionDeniedException
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(value = "/{projectId}", method = RequestMethod.GET)
	public @ResponseBody ProjectResponseWrapper loadByProjectId(@PathVariable String projectId, AppUser user, 
			@RequestParam (value = "externalUserId", required=false) String externalUserId,
			@RequestParam (value = "authorMode", required=false) Boolean authorMode,
			@RequestParam (value = "lod", required=false) String lod) 
					throws DataAccessException, PermissionDeniedException, UnsupportedEncodingException 
					{
		if(authorMode == null) {
			authorMode = externalUserId == null; // assume author mode when we have no externalUserId
		}
		if (projectId != null) {
			projectId = URLDecoder.decode(projectId, StringUtils.ENCODING_UTF8);
		}
		Project p = null;
		if ("minimal".equals(lod)) {
			p = projectReadSvc.getMinimal(projectId, user);
		} else {
			p = projectReadSvc.load(projectId, user, externalUserId, authorMode);
		}

		ProjectResponseWrapper respWrapper = new ProjectResponseWrapper(p);
		return respWrapper;
	}


	@RequestMapping(value = "/apn/{apnUUID}", method = RequestMethod.GET)
	public @ResponseBody ProjectResponseWrapper loadByAccountProjectNumberId(@PathVariable String apnUUID, AppUser user)
			throws DataAccessException, PermissionDeniedException, UnsupportedEncodingException 
			{
		if (apnUUID != null) {
			apnUUID = URLDecoder.decode(apnUUID, StringUtils.ENCODING_UTF8);
		}
		Project p = projectReadSvc.loadMinimalByApnUuid(apnUUID, user);
		ProjectResponseWrapper respWrapper = new ProjectResponseWrapper(p);
		return respWrapper;
			}



	@RequestMapping(value = "/urlName/{urlName}", method = RequestMethod.GET)
	public @ResponseBody ProjectResponseWrapper loadByUrlName(@PathVariable String urlName, AppUser user, 
			@RequestParam (value = "externalUserId", required=false) String externalUserId)
					throws DataAccessException, PermissionDeniedException, UnsupportedEncodingException 
					{
		if (urlName != null) {
			urlName = URLDecoder.decode(urlName, StringUtils.ENCODING_UTF8);
		}
		Project p = projectReadSvc.loadMinimalByUrlName(urlName, user);
		ProjectResponseWrapper respWrapper = new ProjectResponseWrapper(p);
		return respWrapper;
					}

	@RequestMapping(value = "/dep_pubId/{dep_pubId}", method = RequestMethod.GET)
	public @ResponseBody ProjectResponseWrapper loadByPubId(@PathVariable int dep_pubId, AppUser user)
					throws DataAccessException, PermissionDeniedException, UnsupportedEncodingException 
					{
		Project p = projectReadSvc.loadMinimalByPubId(dep_pubId, user);
		ProjectResponseWrapper respWrapper = new ProjectResponseWrapper(p);
		return respWrapper;
	}

	/**
	 * Retire a project. Requires EDIT permissions.
	 * 
	 */
	@RequestMapping(value = "/{projectId}", method=RequestMethod.DELETE)
	public @ResponseBody Map<String,Object> delete(AppUser user, @PathVariable String projectId) throws Exception {
		Project p = projectReadSvc.getMinimal(projectId, user);
		projectUpdateSvc.delete(p, user);
		return SystemResponseMessage.okMessage(projectId).values();
	}

	@RequestMapping(method=RequestMethod.POST, value = "/{projectId}/toggleFeatured")
	public @ResponseBody Map<String,Object> setFeatured(HttpServletRequest request,
			HttpServletResponse response, AppUser user, @PathVariable("projectId") String projectId) throws Exception {

		if (SecurityContextUtils.hasRoleCurrentUser(Role.ROLE_TYPE_SITE_ADMIN) == false) {
			throw new PermissionDeniedException();
		}

		Project p = projectDao.findById(projectId, user);

		boolean newFeatured =  !p.isFeatured();
		p.setFeatured(newFeatured);

		projectDao.updateFeatured(p);

		return SystemResponseMessage.okMessage(String.valueOf(p.getProjectId())).values();
	}

	/**
	 * Remove a published thing from the store
	 * @throws DataAccessException 
	 */
	@RequestMapping(value = "/{projectId}/unpublish", method=RequestMethod.POST)
	public @ResponseBody Map<String,Object> unpublish(HttpServletRequest request, HttpServletResponse response, AppUser user, @PathVariable String projectId) 
			throws Exception {
		Project p = projectReadSvc.load(projectId, user, null, false);
		projectUpdateSvc.unpublish(p, user);
		return SystemResponseMessage.okMessage(projectId).values();
	}


	/**
	 * Remove a published thing from the store
	 * @throws DataAccessException 
	 */
	@RequestMapping(value = "/{userAwarePurchaseId}/unpurchase", method=RequestMethod.POST)
	public @ResponseBody Map<String,Object> unpurchase(HttpServletRequest request, HttpServletResponse response, AppUser user,
			@PathVariable String userAwarePurchaseId) throws Exception {
		projectUpdateSvc.removeFromCollection(userAwarePurchaseId, user);
		return SystemResponseMessage.okMessage(userAwarePurchaseId).values();
	}


	@RequestMapping(value="/upgrade/{urlName}", method=RequestMethod.POST)
	public @ResponseBody Map<String,Object> upgrade(AppUser user, @PathVariable String urlName) throws DataAccessException, PermissionDeniedException {
		AccountProjectNumber apn = projectUpdateSvc.upgrade(user, urlName);
		return SystemResponseMessage.okMessage(apn.getUuid()).values();
	}


	@RequestMapping(method=RequestMethod.POST, value = "/{projectId}/permissionsPrice")
	public @ResponseBody Map<String,Object> updatePermissionsPrice(HttpServletRequest request,
			HttpServletResponse response, AppUser user, @PathVariable("projectId") String projectId, @RequestBody Map<String,Object> permissionsMap) throws Exception {
		GenericJsonRequestParser<ProjectPermissionsPriceReqWrapper> parser = new GenericJsonRequestParser<ProjectPermissionsPriceReqWrapper>();
		@SuppressWarnings("unchecked")
		Map<String,Object> permsObj = (Map<String,Object>)permissionsMap.get("license");
		ProjectPermissionsPriceReqWrapper permissions = parser.parse(permsObj, ProjectPermissionsPriceReqWrapper.class);
		
		// formerly called loadMinimal, but that causes content to be dropped, which is undesirable.
		Project p = projectReadSvc.load(projectId, user, null, false);
		if (p.getAuthorMemberId().equals(user.getId()) == false) {
			throw new PermissionDeniedException();
		}
		p.setHideInStoreList(permissions.getHideInStoreList());
		p.setInviteOnly(permissions.getInviteOnly());


		p.setPrice(permissions.getPrice());

		LicenseType lt = LicenseType.findCanonical(
				permissions.getLicenseTypeId(), permissions.getRepublish(), 
				permissions.getEdit(), permissions.getEmbed(), p.getProjectType());
		p.setLicenseTypeId(lt.id);

		
		ProjectCreateParameters psr = new ProjectCreateParameters(p);
		psr.setIgnoreDuplicateNames(true);
		psr.setIgnoreDuplicatePublishedNames(true);
		psr.contentFromProjectId = projectId;
		psr.iconFromProjectId = projectId;
		psr.screenshotsFromProjectId = projectId;
		p = pCreateSvc.create(user, psr);

		// we need to handle provide "id" and "apnUUID"
		Map<String,Object> ret = SystemResponseMessage.okMessage(p.getProjectId()).values();
		ret.put("playerId", p.getApnUUID());
		return ret;
	}


	@RequestMapping(value="/categories")
	public @ResponseBody List<GadgetCategory> getCategories() {
		List<GadgetCategory> categories = gadgetCategoryDao.findAll();
		return categories;
	}

	@RequestMapping(value="/precomputeIds", method=RequestMethod.POST)
	public @ResponseBody List<String> precomputeIds(@RequestBody Map<String,Integer> arg) {
		Integer size = arg.get("size");
		List<String> ret = new ArrayList<String>();
		if (size == null) { size = 0; }
		if (size > 50) { size = 50; }
		for (int i = 0; i < size; i++) {
			ret.add(UUID.hibernateCompatibleUUID());
		}
		return ret;
	}

	@RequestMapping(value="rename/{projectId}", method=RequestMethod.POST)
	public @ResponseBody void renameAllProjectVersions(AppUser user,
			@PathVariable("projectId") String projectId, @RequestBody Map<String,Object> params) throws DataAccessException {
		Project p = projectReadSvc.getMinimal(projectId, user);
		boolean ignoreDuplicateNames = false;
		if (params.containsKey("ignoreDuplicateNames")) {
			ignoreDuplicateNames = (Boolean) params.get("ignoreDuplicateNames");
		}
		pRenameSvc.renameAllProjectVersions(user, p, (String)params.get("newName"), ignoreDuplicateNames);
	}

	// exception handlers


	@ExceptionHandler({UniqueConstraintException.class, DataAccessException.class, ConcurrencyException.class, NotFoundException.class})
	public @ResponseBody Map<String,Object> handleDataAccessException(DataAccessException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);
		//		e.printStackTrace();

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.UNKNOWN_ERROR, e.getObjectId());
		if (e instanceof UniqueConstraintException) {
			if("projectName".equals(((UniqueConstraintException)e).getField())) {
				msg = new SystemResponseMessage(MessageCode.DUPLICATE_GADGET_NAME, e.getObjectId());
			} else if("publishedName".equals(((UniqueConstraintException)e).getField()) || "urlName".equals(((UniqueConstraintException)e).getField())) {
				msg = new SystemResponseMessage(MessageCode.DUPLICATE_GADGET_NAME, e.getObjectId());
			} else {
				// reasonable default
				msg = new SystemResponseMessage(MessageCode.DUPLICATE_GADGET_NAME, e.getObjectId());
			}

		} else if (e instanceof NotFoundException) {
			msg = new SystemResponseMessage(MessageCode.PUBLISHED_GADGET_NOT_FOUND, MessageLevel.ERROR, MessageType.INTERNAL, 
					"One or more required components of this project were not found", e, e.getObjectId());

		} else if (e instanceof ConcurrencyException) {
			msg = new SystemResponseMessage(MessageCode.RECORD_LOCKED_IN_TRANSACTION, e.getObjectId());

		}
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(PermissionDeniedException.class)
	public @ResponseBody Map<String,Object> handlePermissionDenied(PermissionDeniedException e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.NOT_ALLOWED, MessageLevel.ERROR, MessageType.INTERNAL, "You are not authorized to edit publishing data for this item.", e, null);

		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(EditToDeletedItemException.class)
	public @ResponseBody Map<String,Object> handleEditDeleted(EditToDeletedItemException e,  HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.EDIT_TO_DELETED_ITEM, MessageLevel.ERROR, MessageType.INTERNAL, "The item you are attempting to edit has been deleted", e, null);

		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(CannotRepublishException.class)
	public @ResponseBody Map<String,Object> handleRepublishException(CannotRepublishException e,  HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);
		
		SystemResponseMessage msg = null;
			msg = new SystemResponseMessage(MessageCode.CANNOT_REPUBLISH_GADGET, MessageLevel.ERROR,
				MessageType.INTERNAL, "The gadget you are attempting to publish contains a gadget for which you do not have sufficient re-publishing rights.", e, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(PublishedNameInUseByOtherUserException.class)
	public @ResponseBody Map<String,Object> nameInUseByAnotherUser(PublishedNameInUseByOtherUserException e,  HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);
		
		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.DUPLICATE_PUBLISHED_GADGET_NAME, MessageLevel.ERROR,
					MessageType.BUSINESS, "The gadget you are attempting to publish contains a gadget for which you do not have sufficient re-publishing rights.", e, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(IllegalNameException.class)
	public @ResponseBody Map<String,Object> handleIllegalNameException(IllegalNameException e,  HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.ILLEGAL_NAME, MessageLevel.ERROR,
				MessageType.BUSINESS, e.getMessage(), e, null);

		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

	@ExceptionHandler(ProjectSharedException.class)
	public @ResponseBody Map<String,Object> handleProjectSharedException(ProjectSharedException e,  HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);

		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.PROJECT_IS_SHARED, MessageLevel.WARN,
				MessageType.BUSINESS, "The gadget you are attempting to publish has been shared to one or more services.", e, String.valueOf(e.getPublishId()));

		response.setStatus(msg.code.suggestedHttpRespCode);
		Map<String, Object> ret = msg.values();
		ret.put("shareTypes", e.getShareTypes());
		return ret;
	}

	@ExceptionHandler(Exception.class)
	public @ResponseBody Map<String,Object> handleUnknown(Exception e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);
		e.printStackTrace();
		SystemResponseMessage msg = null;

		if (e instanceof PublishPriceException) {
			long minPrice =  ((PublishPriceException)e).getMinPrice();
			msg = new SystemResponseMessage(MessageCode.PUBLISH_PRICE_TOO_LOW, MessageLevel.ERROR,
					MessageType.BUSINESS, "The purchase price of this item must be at least [" + minPrice + "]", e, null);

			response.setStatus(msg.code.suggestedHttpRespCode);
			Map<String,Object> ret = msg.values();
			ret.put("minPrice", minPrice);
			return ret;

		} else {

			msg = new SystemResponseMessage(MessageCode.PUBLISH_FAILED, null);
			response.setStatus(msg.code.suggestedHttpRespCode);
			return msg.values();
		}
	}
}
