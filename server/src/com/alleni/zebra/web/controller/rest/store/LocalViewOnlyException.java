package com.alleni.zebra.web.controller.rest.store;

public class LocalViewOnlyException extends Exception {

	public LocalViewOnlyException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2012144616935311606L;

}
