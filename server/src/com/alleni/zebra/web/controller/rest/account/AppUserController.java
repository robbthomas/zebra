package com.alleni.zebra.web.controller.rest.account;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alleni.zebra.account.AccountType;
import com.alleni.zebra.asset.uploadableasset.UploadableAssetService;
import com.alleni.zebra.data.dao.AccountDao;
import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.hibernate.dao.MimeTypeDao;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.model.Account;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.authentication.dao.LocalAuthenticationService;
import com.alleni.zebra.security.authorization.Role;
import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.SystemResponseMessage;

@Controller
@RequestMapping("/user")
public class AppUserController {
	protected final Log log = LogFactory.getLog(getClass());


	@Autowired private AppUserDao userDao;
	@Autowired AccountDao accountDao;
	@Autowired LocalAuthenticationService authSvc;
	@Autowired MimeTypeDao mtDao;
	@Autowired UploadableAssetService uaSvc;

	
	@RequestMapping(value="/{authorId}", method={RequestMethod.GET})
	public @ResponseBody Map<String,Object> finger(HttpServletRequest request, HttpServletResponse response, AppUser loggedInUser,
			@PathVariable String authorId) throws NotFoundException {
		Map<String,Object> ret = new HashMap<String,Object>();
		AppUser user = userDao.findById(authorId); // will throw NFException instead of being null
		Account account = accountDao.findById(user.getAccountId());
		ret.put("displayName", user.getUsername());
		ret.put("userDescription", user.getDescription());
		ret.put("firstName", user.getFirstName());
		ret.put("lastName", user.getLastName());
		ret.put("nameFormat", user.getNameFormat());
		if (user.isShowEmail()) {
			ret.put("email", user.getEmail());
			ret.put("twitter",user.getTwitter());
			ret.put("website", user.getWebsite());
		}
		ret.put("companyName", account.getCompanyName());
		ret.put("accountName", account.getName());
		return ret;
	}
	

	@RequestMapping(method={RequestMethod.GET})
	public @ResponseBody Map<String,Object> byEmail(HttpServletRequest request, HttpServletResponse response, AppUser loggedInUser,
			@RequestParam(required=true) String email) throws NotFoundException {
		Map<String,Object> ret = new HashMap<String,Object>();
		try {
			AppUser user = userDao.findByEmail(email); // will throw NFException instead of being null
			Account account = accountDao.findById(user.getAccountId());
			ret.put("displayName", user.getUsername());
			ret.put("userDescription", user.getDescription());
			ret.put("firstName", user.getFirstName());
			ret.put("lastName", user.getLastName());
			ret.put("nameFormat", user.getNameFormat());
			if (user.isShowEmail()) {
				ret.put("email", user.getEmail());
				ret.put("twitter",user.getTwitter());
				ret.put("website", user.getWebsite());
			}
			ret.put("companyName", account.getCompanyName());
			ret.put("accountName", account.getName());
			ret.put("accountType", AccountType.valueOf(account.getTypeId()));
			
			//TODO
			//ret.put("avatarId", user.getIconId());
		} catch (NotFoundException nfe) {
			// Not a true error -- signifies an empty result set, and we're handling as we want to.
			ret = Collections.emptyMap();
		}
		
		return ret;
	}

	@RequestMapping(value="metadata", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> getNameData(AppUser user) {
		Map<String,Object> ret = new HashMap<String,Object>();
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String fullName = (firstName == null? "" : firstName) + " " + (lastName == null? "" : lastName);
		ret.put("fullName", fullName);
		
		String email = user.getEmail();
		ret.put("email", email);

		try {
			Account account = accountDao.findById(user.getAccountId());
			String companyName = account.getCompanyName();
			ret.put("companyName", (companyName == null? "" : companyName));

			String accountName = account.getName();
			ret.put("accountName", accountName == null? "" : accountName);

			ret.put("isAdmin", isAdmin(user));
			ret.put("userId", user.getId());
			ret.put("accountId", user.getAccountId());
			ret.put("customerType", account.getAccountType().toString());

		} catch (NotFoundException whatevs) {
			log.warn("we don't know who this joker is: " + user.getId());
		}

		return ret;
	}

	private boolean isAdmin(AppUser user) {
		boolean isAdmin =  false;

		Collection<GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		for (GrantedAuthority auth : authorities) {
			if (auth.getAuthority().equalsIgnoreCase(Role.ROLE_TYPE_SITE_ADMIN.toString()))
				isAdmin = true;
		}

		return isAdmin;
	}

	
	
	public static class MultipartFileUploadBean {
		private String avatarName;
		private String avatarCaption;
		private String bannerName;
		private String bannerCaption;
		private String username;
		private String password;
		private MultipartFile avatarFile;
		private MultipartFile bannerFile;
		private boolean refresh = false;
		
		public String getAvatarName() {
			return avatarName;
		}
		public void setAvatarName(String avatarName) {
			this.avatarName = avatarName;
		}
		public String getAvatarCaption() {
			return avatarCaption;
		}
		public void setAvatarCaption(String avatarCaption) {
			this.avatarCaption = avatarCaption;
		}
		public String getBannerName() {
			return bannerName;
		}
		public void setBannerName(String bannerName) {
			this.bannerName = bannerName;
		}
		public String getBannerCaption() {
			return bannerCaption;
		}
		public void setBannerCaption(String bannerCaption) {
			this.bannerCaption = bannerCaption;
		}
		public MultipartFile getAvatarFile() {
			return avatarFile;
		}
		public void setAvatarFile(MultipartFile file) {
			this.avatarFile = file;
		}
		public MultipartFile getBannerFile() {
			return bannerFile;
		}
		public void setBannerFile(MultipartFile file) {
			this.bannerFile = file;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public boolean isRefresh() {
			return refresh;
		}
		public void setRefresh(boolean refresh) {
			this.refresh = refresh;
		}
	}
	@RequestMapping(value="/avatar", method=RequestMethod.POST)
	public @ResponseBody UploadableAsset setAvatar(HttpServletResponse response, MultipartFileUploadBean uploadBean) throws IOException {
		AppUser user = null;
		try {
			user = authSvc.authenticate(uploadBean.getUsername(), uploadBean.getPassword());
		} catch (AuthenticationException ae) {
			log.warn("we don't know who this joker is: " + uploadBean.getUsername());
			response.setStatus(HttpStatus.SC_FORBIDDEN);
			return null;
		}
		
		UploadableAsset avatar = new UploadableAsset();
		UploadableAsset banner = new UploadableAsset();
		
		if (!uploadBean.getAvatarFile().isEmpty()) {
			avatar.setName(uploadBean.getAvatarName());
			avatar.setCaption(uploadBean.getAvatarCaption());
			byte[] bytes = uploadBean.getAvatarFile().getBytes();
			
			avatar = uaSvc.setAvatarForUser(user, avatar, bytes);
		}
		
		if (!uploadBean.getBannerFile().isEmpty()) {
			banner.setName(uploadBean.getBannerName());
			banner.setCaption(uploadBean.getBannerCaption());
			byte[] bannerBytes = uploadBean.getBannerFile().getBytes();
			
			banner = uaSvc.setBannerForUser(user, banner, bannerBytes);
		}
		

		response.setStatus(HttpStatus.SC_OK);
		return null;
	}

	
	
	
	
	@ExceptionHandler(NotFoundException.class)
	public @ResponseBody Map<String,Object> handleNotFound(HttpServletRequest request, HttpServletResponse response) {
		String name = request.getPathInfo();
		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.ASSET_NOT_FOUND, name);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}


}
