package com.alleni.zebra.web.controller.rest.store;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.data.dao.AccountDao;
import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.hibernate.dao.CreditCardDao;
import com.alleni.zebra.data.hibernate.model.CreditCard;
import com.alleni.zebra.data.model.Account;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.SystemResponseMessage;
import com.alleni.zebra.web.proxy.SimpleHttpRequest;

@Controller("purchaseController")
@RequestMapping("/shoppe/purchase")
public class PurchaseController {
	private Logger log = Logger.getLogger(this.getClass());

	@Autowired AccountDao accountDao;
	@Autowired ProjectDao projectDao;
	@Autowired CreditCardDao ccDao; // hibernate generated class (massaged some by dwh)

	@RequestMapping(value="{projectId}", method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> purchase(HttpServletRequest req, HttpServletResponse resp,
			@PathVariable String projectId, @RequestHeader String command, AppUser user) throws Exception {

		// first check that they're not trying to buy from themselves!
		Project p  = projectDao.findById(projectId, user);
		if (p.getAccountId() == user.getAccountId().longValue()) {
			throw new Exception("You can't buy a gadget from yourself!");
		}

		if (p.getPrice() > 0) {
			// then check if their account is "in good standing."
			Account account = accountDao.findById(user.getAccountId());
			boolean goodStanding = account.isGoodStanding();

			// it's possible to be in "good standing", with a bad CC.
			// But we don't care right now.
			if (goodStanding) {
				goodStanding = false; // start by assuming failure.
				List<CreditCard> ccs = ccDao.findForAccount(account.getId());
				for (CreditCard cc : ccs) {
					if (cc.getCustomerpaymentprofileid() != null && cc.getCustomerpaymentprofileid().isEmpty() == false &&
							"null".equalsIgnoreCase(cc.getCustomerpaymentprofileid()) == false ) { //duh
						goodStanding = true;
						break;
					}
				}
			}
			
			if (!goodStanding) {
				SystemResponseMessage msg = new SystemResponseMessage(MessageCode.PURCHASE_FAILED_BAD_ACCOUNT, null);
				resp.setStatus(msg.code.suggestedHttpRespCode);
				return msg.values();
			}
		}

		log.debug("proxying request to site");

		String host = req.getServerName();

		int port = req.getLocalPort();
		String basePath = "site";
		String appPath = "services/zapp/purchase.jsp";

		String completeUrl = req.getScheme() + "://" + host + ":" + port + "/" + basePath + "/" + appPath;

		String password = command;

		String json = "{\"zappPurchase\": { " +
				"\"accountId\": \"" + user.getAccountId() + "\", " +
				"\"zappList\": [ {" +
				"\"projectId\":" + projectId + " }] }}";

		log.debug("json:  " + json);
		Map<String, String> params = new HashMap<String,String>();
		params.put("json", json);

		String retJson = new SimpleHttpRequest().doPost(params, completeUrl, user.getUsername(), password);
		@SuppressWarnings("unchecked")
		Map<String, Object> ret = new ObjectMapper().readValue(retJson, Map.class);

		boolean success = (Boolean)ret.get("success");
		if (success) {
			return ret;
		} else {
			log.error(retJson);
			SystemResponseMessage msg = new SystemResponseMessage(MessageCode.PURCHASE_FAILED, null);
			resp.setStatus(msg.code.suggestedHttpRespCode);
			return msg.values();
		}

	}


	@ExceptionHandler(Exception.class)
	public @ResponseBody Map<String,Object> handleUnknown(Exception e, HttpServletRequest request,  HttpServletResponse response) {
		log.debug(">>>> handling exception: ", e);
		//		e.printStackTrace();
		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.PURCHASE_FAILED_BAD_ACCOUNT, null);
		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();

	}


}
