package com.alleni.zebra.web.controller.rest.account;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alleni.zebra.account.AccountService;
import com.alleni.zebra.asset.uploadableasset.UploadableAssetService;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.model.Account;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.security.authentication.dao.LocalAuthenticationService;
import com.alleni.zebra.system.message.SystemResponseMessage;

@Controller("accountController")
@RequestMapping("/account")
public class AccountController {
	protected final Log log = LogFactory.getLog(getClass());
	@Autowired AccountService accountSvc;
	@Autowired LocalAuthenticationService authSvc;
	@Autowired UploadableAssetService uaSvc;
	
	@RequestMapping("me")
	public @ResponseBody Account getById(AppUser user) {
		// TODO:  when multi-user, only account owner can do this
		return user.getAccount();
	}



	@RequestMapping(value = "learners", method = RequestMethod.POST)
	public @ResponseBody Map<String,Object> setLearners(AppUser user, @RequestBody Map<String,Object> params) throws PermissionDeniedException, DataAccessException {
		Object szNum = params.get("numLearners");
		Integer numLearners;
		if (szNum != null) {
			numLearners = Integer.parseInt((String)szNum);
		} else {
			numLearners = 0;
		}
		
		Map<String,Object> ret = new HashMap<String,Object>();

		accountSvc.setNumLearners(user, numLearners == null? 0 : numLearners);
		ret = SystemResponseMessage.okMessage(String.valueOf(user.getAccountId())).values();
		ret.put("totalLearners", user.getAccount().getNumLearners());

		return ret;
	}
	
	public static class MultipartFileUploadBean {
		private String name;
		private String caption;
		private String username;
		private String password;
		private MultipartFile bannerFile;
		private boolean refresh = false;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCaption() {
			return caption;
		}
		public void setCaption(String caption) {
			this.caption = caption;
		}
		public MultipartFile getBannerFile() {
			return bannerFile;
		}
		public void setBannerFile(MultipartFile file) {
			this.bannerFile = file;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public boolean isRefresh() {
			return refresh;
		}
		public void setRefresh(boolean refresh) {
			this.refresh = refresh;
		}
	}
	
	@RequestMapping(value="/banner", method=RequestMethod.POST)
	public @ResponseBody UploadableAsset setAvatar(HttpServletResponse response, MultipartFileUploadBean uploadBean) throws IOException {
		AppUser user = null;
		try {
			user = authSvc.authenticate(uploadBean.getUsername(), uploadBean.getPassword());
		} catch (AuthenticationException ae) {
			log.warn("we don't know who this joker is: " + uploadBean.getUsername());
			response.setStatus(HttpStatus.SC_FORBIDDEN);
			return null;
		}
		
		UploadableAsset ua = new UploadableAsset();
		ua.setName(uploadBean.getName());
		ua.setCaption(uploadBean.getCaption());
		byte[] bytes = uploadBean.getBannerFile().getBytes();
		
		ua = uaSvc.setBannerForUser(user, ua, bytes);
		if (uploadBean.isRefresh()) {
			return ua;
		} else {
			response.setStatus(HttpStatus.SC_NO_CONTENT);
			return null;
		}
	}
}
