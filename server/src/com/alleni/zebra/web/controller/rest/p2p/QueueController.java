package com.alleni.zebra.web.controller.rest.p2p;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.p2p.SimpleQueueManagerService;
import com.alleni.zebra.system.message.SystemResponseMessage;

@Controller
@RequestMapping("/p2p/queue")
public class QueueController {
	
	@Autowired SimpleQueueManagerService queueManager;
	
	
	@RequestMapping(value="{projectId}/{queueId}", method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> put(@RequestBody QueueRequest dto, @PathVariable String queueId, @PathVariable String projectId, AppUser user) {
		Map<String, Object> ret = null;
		
		// TODO:  check user has access to project
		
		queueManager.post(queueId + projectId, dto.getData());
		
		ret = SystemResponseMessage.okMessage(queueId).values();
		
		return ret;
	}
	
	
	@RequestMapping(value="{projectId}/{queueId}", method=RequestMethod.GET)
	public @ResponseBody QueueResponse pop(@PathVariable String queueId, @PathVariable String projectId, AppUser user) { 
		QueueResponse ret = new QueueResponse();
		Object data = queueManager.get(queueId + projectId );
		if (data != null) { 
			ret.setData(data);
			ret.setResponseCode(QueueResponse.Code.OK);
		} else {
			ret.setResponseCode(QueueResponse.Code.QUEUE_EMPTY);
		}
		return ret;
		
	}
}
