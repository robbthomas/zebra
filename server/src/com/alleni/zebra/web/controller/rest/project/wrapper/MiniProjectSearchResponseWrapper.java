package com.alleni.zebra.web.controller.rest.project.wrapper;

import java.util.List;

public class MiniProjectSearchResponseWrapper { 
	private final List<MiniProjectResponseWrapper> projects;
	private final int totalCount;
	private final boolean hasMoreData;
	
	public MiniProjectSearchResponseWrapper(List<MiniProjectResponseWrapper> miniProjects,
			int totalCount, boolean hasMoreData) {
		this.projects = miniProjects;
		this.totalCount = totalCount;
		this.hasMoreData = hasMoreData;
	}
	
	
	public List<MiniProjectResponseWrapper> getProjects() {
		return projects;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public boolean isHasMoreData() {
		return hasMoreData;
	}
	
	
}
