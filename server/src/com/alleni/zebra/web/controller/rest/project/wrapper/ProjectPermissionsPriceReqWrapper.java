package com.alleni.zebra.web.controller.rest.project.wrapper;


public class ProjectPermissionsPriceReqWrapper {
	
	private Boolean hideInStoreList;
	private Boolean inviteOnly;
	private Boolean embed;
	private Boolean republish;
	private Boolean edit;
	private Integer price;
	private Integer licenseTypeId;
	public Boolean getHideInStoreList() {
		return hideInStoreList;
	}
	public void setHideInStoreList(Boolean hideInStoreList) {
		this.hideInStoreList = hideInStoreList;
	}
	public Boolean getInviteOnly() {
		return inviteOnly;
	}
	public void setInviteOnly(Boolean inviteOnly) {
		this.inviteOnly = inviteOnly;
	}
	public Boolean getEmbed() {
		return embed;
	}
	public void setEmbed(Boolean embed) {
		this.embed = embed;
	}
	public Boolean getRepublish() {
		return republish;
	}
	public void setRepublish(Boolean republish) {
		this.republish = republish;
	}
	public Boolean getEdit() {
		return edit;
	}
	public void setEdit(Boolean edit) {
		this.edit = edit;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getLicenseTypeId() {
		return licenseTypeId;
	}
	public void setLicenseTypeId(Integer licenseTypeId) {
		this.licenseTypeId = licenseTypeId;
	}


}
