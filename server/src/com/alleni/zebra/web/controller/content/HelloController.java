package com.alleni.zebra.web.controller.content;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;

@Controller
public class HelloController {
	protected final Log log = LogFactory.getLog(getClass());
 
    @Autowired AppUserDao userDao;
    
    @RequestMapping(value="/", method={RequestMethod.GET, RequestMethod.POST} )
//    @Secured("VIEWER")
    @PreAuthorize("hasRole('VIEWER')")
    public String sayHello(HttpServletRequest request, HttpServletResponse response, ModelMap model, AppUser user)
            throws ServletException, IOException, NotFoundException {

    	
        model.addAttribute("user", user);

        Collection<GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		for (GrantedAuthority auth : authorities)
			log.debug(auth.getAuthority());

        log.info("Returning hello view");
        return "/index.jsp";
    }

    @RequestMapping(value="/hello/{name}", method=RequestMethod.GET )
    public String helloName(HttpServletRequest request, HttpServletResponse response, ModelMap model,
    		@PathVariable String name )
            throws ServletException, IOException {

        AppUser user = null;;
		try {
			user = userDao.findByUsername(name);
		} catch (NotFoundException e) {
			response.setStatus(404);
		}
        model.addAttribute("user", user);

        log.info("Returning hello view");
        return "/hello.jsp";
    }
    

}
