package com.alleni.zebra.web.controller.system;

import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alleni.zebra.security.authorization.Role;
import com.alleni.zebra.security.authorization.SecurityContextUtils;

@Controller("bootstrapController")
public class BootstrapZipController {


	@RequestMapping("/system/bootstrap")
	public void getFontFile(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		if (SecurityContextUtils.hasRoleCurrentUser(Role.ROLE_TYPE_SITE_ADMIN) && req.getLocalName().equalsIgnoreCase("integration.zebrazapps.com") ) {
			ServletContext ctx = req.getSession().getServletContext();
			InputStream is = ctx.getResourceAsStream("/WEB-INF/zip/bootstrap.tgz");

			resp.setContentType("binary/gzip");
			IOUtils.copy(is, resp.getOutputStream());
		} else {
			resp.sendError(404);
		}
	}
}
