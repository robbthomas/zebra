package com.alleni.zebra.web.controller.system;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.data.dao.ProjectSearchHelper;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.authorization.Role;
import com.alleni.zebra.security.authorization.SecurityContextUtils;

@Controller("statsController")
@RequestMapping("/system/stats")
public class StatsController {
	@Qualifier(value="projectSearchHelperSPImpl")
	@Autowired ProjectSearchHelper searchHelper;
	
	@RequestMapping("queries")
	public @ResponseBody Map<String,Map<String,Object>> currentQueries(HttpServletResponse resp, AppUser user) {
		if (SecurityContextUtils.hasRoleCurrentUser(Role.ROLE_TYPE_SITE_ADMIN))
			return searchHelper.listCurrentQueries();
		else {
			resp.setStatus(HttpStatus.SC_NOT_FOUND);
			return null;
		}
		
	}
}
