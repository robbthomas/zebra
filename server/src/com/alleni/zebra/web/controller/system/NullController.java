package com.alleni.zebra.web.controller.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * returns a null response to any request
 *
 */
@Controller
public class NullController {
	private final Logger log = Logger.getLogger(getClass());

    @RequestMapping(value="/null", method=RequestMethod.GET )
	public String doGet(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	log.debug("serving null page");
    	return null;
	}
    @RequestMapping(value="/null", method=RequestMethod.POST )
	public String doPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	log.debug("serving null page");
    	return null;
	}
}
