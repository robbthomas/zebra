package com.alleni.zebra.web.controller.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.quartz.commerce.AbstractEcommerceCronJob;


@Controller
@RequestMapping(value="/cron")
public class CronController {
	
	@RequestMapping(value="/{jobId}", method=RequestMethod.GET)
	public String crons(HttpServletRequest req, HttpServletResponse resp, ModelMap model, AppUser user, 
			@PathVariable Integer jobId) {
		AbstractEcommerceCronJob cron = AbstractEcommerceCronJob.getInstance(jobId);
		String jobOutput = cron.executeExternal();
		
		model.put("user", user);
		model.put("jobId", jobId);
		model.put("jobOutput", jobOutput);
		return "index.jsp";
	}
}
