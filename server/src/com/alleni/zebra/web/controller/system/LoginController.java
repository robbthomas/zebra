package com.alleni.zebra.web.controller.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller()
@RequestMapping("/login")
public class LoginController {

	/**
	 * If no errors, then it's probably a http login attempt.  Otherwise, just toss a json at 'em, cuz it's the editor.
	 *  
	 */
	@RequestMapping(method={RequestMethod.GET, RequestMethod.POST})
	public String login(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return "/login.jsp";
//		if (request.getSession().getAttribute("SPRING_SECURITY_LAST_EXCEPTION") != null) {
//			request.getRequestDispatcher("/denied").forward(request, response);
//			return "/login.jsp";
//		} else {
//			return "/login.jsp";
//		}
	}

}
