package com.alleni.zebra.web.controller.system;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.system.message.SystemResponseMessage;

/**
 *	Error conditions will be handled from here, wherever possible. 
 *
 */
@Controller
public class ErrorController {
	private final Logger log = Logger.getLogger(getClass());

	public static final String SYSTEM_MESSAGE = "com.alleni.zebra.message.SystemResponseMessage";
	
	@RequestMapping(value="/error", method={RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT, RequestMethod.DELETE} )
    public @ResponseBody Map<String,Object> error(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		SystemResponseMessage msg = (SystemResponseMessage)request.getSession().getAttribute(SYSTEM_MESSAGE);
		log.warn("processing error message: " + msg.toString());

		response.setStatus(msg.code.suggestedHttpRespCode);
		return msg.values();
	}

}
