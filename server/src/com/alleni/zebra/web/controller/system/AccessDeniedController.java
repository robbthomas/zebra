package com.alleni.zebra.web.controller.system;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alleni.zebra.system.message.MessageCode;
import com.alleni.zebra.system.message.SystemResponseMessage;

@Controller
@RequestMapping("/login/denied")
public class AccessDeniedController {
	
	@RequestMapping(method={RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody Map<String, Object> denied(HttpServletRequest request, HttpServletResponse response) {
		SystemResponseMessage msg = new SystemResponseMessage(MessageCode.NOT_ALLOWED, null);
		response.setStatus(401);
		return msg.values();
	}
}
