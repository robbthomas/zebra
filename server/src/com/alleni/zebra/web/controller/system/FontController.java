package com.alleni.zebra.web.controller.system;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;

@Controller("fontController")
public class FontController {
	Logger log = Logger.getLogger(FontController.class);
	
	@RequestMapping("/system/font/{fontName}")
	public void getFontFile(WebRequest req, HttpServletRequest servletRequest, HttpServletResponse resp, @PathVariable String fontName) throws IOException {

		String servletPath = servletRequest.getServletPath();
		String ext = servletPath.substring(servletPath.lastIndexOf('.'));
		ServletContext ctx = servletRequest.getSession().getServletContext();

        URL fontUrl = ctx.getResource("/WEB-INF/font/" + fontName + ext);
        long modifiedDate = fontUrl.openConnection().getLastModified();
        if (req.checkNotModified(modifiedDate)) {
        	log.debug("Not modified; setting content-length to: 0");
        	resp.setContentLength(0);
        	resp.setStatus(HttpStatus.SC_NOT_MODIFIED);
            return;
        }

        InputStream is = ctx.getResourceAsStream("/WEB-INF/font/" + fontName + ext);

		if (is != null) {
            log.debug("Return font file");
            resp.setContentType("text/plain");
            byte[] bytes = IOUtils.toByteArray(is);
            int length = bytes.length;
            log.debug("setting content-length to: " + length);
            resp.setContentLength(length);
            resp.addHeader("Content-Length", String.valueOf(length));
            IOUtils.write(bytes, resp.getOutputStream());
		} else {
			resp.sendError(404);
		}
	}
}
