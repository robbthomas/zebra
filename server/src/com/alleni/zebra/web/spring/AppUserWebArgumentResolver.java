package com.alleni.zebra.web.spring;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.authorization.LoginContextHolder;

public class AppUserWebArgumentResolver implements WebArgumentResolver {

	@Override
	public Object resolveArgument(MethodParameter parameter, NativeWebRequest request)
			throws Exception {
		return isAppUserParameter(parameter)? getAppUser(request) : UNRESOLVED;
	}
	
	private boolean isAppUserParameter(MethodParameter methodParameter) {
        Class<?> clazz = methodParameter.getParameterType();
        boolean isAccountParameter = clazz.isAssignableFrom(AppUser.class);
        return isAccountParameter;	}
	
	AppUser getAppUser(NativeWebRequest request) {
		return LoginContextHolder.getContext().getAppUser();
	}

}
