package com.alleni.zebra.web.spring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

public class CustomAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
//    	super.commence(request, response, authException);
        response.sendError(401);
//		request.getRequestDispatcher("/login/denied").forward(request, response);

    }

	@Override
	public String getRealmName() {
		return null;
	}

	@Override
	public void setRealmName(String realmName) {
	}

	@Override
	public void afterPropertiesSet() throws Exception {
	}

}
