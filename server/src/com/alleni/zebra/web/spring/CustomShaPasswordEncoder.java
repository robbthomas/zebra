package com.alleni.zebra.web.spring;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.encoding.PasswordEncoder;

public class CustomShaPasswordEncoder implements PasswordEncoder {
	Logger log = Logger.getLogger(CustomShaPasswordEncoder.class);
	

	@Override
	public String encodePassword(String password, Object salt)
			throws DataAccessException {
		
		String merged = password;
		if (salt != null && !"".equals(salt)) {
			merged = password + salt;
		}
		return DigestUtils.shaHex(merged);
	}

	/*
	 * This method will encode the rawPass (using the optional salt), and then compared it with the presented encPass.
	 */
	@Override
	public boolean isPasswordValid(String encoded, String password, Object salt)
			throws DataAccessException {
		String expected = encodePassword(password, salt);
		return (expected.equals(encoded));
	}

	
}
