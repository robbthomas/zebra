package com.alleni.zebra.web.proxy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class SimpleHttpRequest {
	private static final Logger log = Logger.getLogger(SimpleHttpRequest.class);

	public String doPost(Map<String, String> params, String completeUrl, String username, String password) {
		String returnString = "";
		
		OutputStreamWriter out = null;

		String val = username + ":" + password;
		byte[] base = val.getBytes();

		String encoded = new String(Base64.encodeBase64(base));
		log.debug(encoded);

		String authorizationString = "Basic " + encoded;

		URL url;
		BufferedReader in = null;
		HttpURLConnection connection = null;
		try {
			completeUrl = completeUrl + "?";
			for (String key : params.keySet()) {
				String param = params.get(key);
				param = URLEncoder.encode(param, "UTF-8");
				log.debug("adding request param: " + key + ": " + param);
				completeUrl = completeUrl + key + "=" + param + "+";
			}
			log.debug("url + params: " + completeUrl);


			url = new URL(completeUrl);
			log.debug("opening connection...");
			connection = (HttpURLConnection)url.openConnection();
			connection.setDoOutput(true);
			connection.addRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Authorization", authorizationString);

			for (String key : params.keySet()) {
				log.debug("adding property: " + key + ":  " + params.get(key));
				connection.addRequestProperty(key, params.get(key));
			}

			log.debug("writing to output stream...");
			out = new OutputStreamWriter(
					connection.getOutputStream());
			out.write("\n\n"); // no parameters.
			out.close();

			System.out.println("reading response...");
			in = new BufferedReader(
					new InputStreamReader(
							connection.getInputStream()));

			String responseString;

			while ((responseString = in.readLine()) != null) {
				log.debug("response string: " + responseString);
				returnString = returnString + responseString;
			}

		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException ioe) {
			log.error("error...");
			ioe.printStackTrace();

			try{
				in = new BufferedReader(
						new InputStreamReader(
								connection.getInputStream()));

				String responseString;

				while ((responseString = in.readLine()) != null) {
					log.error("response string: " + responseString);
					returnString = returnString + responseString;
				}

			} catch (IOException e) {
			}

		} finally {
			try {
				if (in != null) {
					in.close();
					if (out != null) {
						out.close();
					}
				}
			} catch (IOException e) {
				log.error(e);
			}
		}
		log.debug("done");

		return returnString;
	}

}
