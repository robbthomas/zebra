package com.alleni.zebra.s3;

import org.jets3t.service.model.S3Bucket;

import com.alleni.zebra.data.model.Asset;

public interface S3BucketFactory {

	public abstract S3Bucket getBucketForAsset(Asset asset);

	public abstract S3Bucket getThumbnailBucket();

	public abstract S3Bucket getIconBucket();
	public abstract S3Bucket getScreenshotBucket();
	public abstract String getScreenshotBucketName();
	
	String getIconBucketName();

	S3Bucket getAvatarBucket();

}