package com.alleni.zebra.s3;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.model.S3Bucket;

import com.alleni.zebra.asset.MimeCategory;
import com.alleni.zebra.asset.UnsupportedMimeTypeException;
import com.alleni.zebra.data.exception.UnsupportedAssetMimeType;
import com.alleni.zebra.data.model.Asset;

public class S3BucketFactoryImpl implements S3BucketFactory {
	private final Logger log = Logger.getLogger(getClass());

	private String thumbnailBucketName;
	private String assetBucketName;
	private String videoBucketName;
	private String audioBucketName;
	private String iconBucketName;
	private String screenshotBucketName;
	private String avatarBucketName;
	
	private Map<String, S3Bucket> nameBucketMap = new HashMap<String, S3Bucket>();
	
	
	/**
	 * initialize s3 buckets
	 * @throws S3ServiceException 
	 */
	@PostConstruct
	public void init() throws S3ServiceException {
		nameBucketMap.put(avatarBucketName, new S3Bucket(avatarBucketName));
		nameBucketMap.put(thumbnailBucketName, new S3Bucket(thumbnailBucketName));
		nameBucketMap.put(iconBucketName, new S3Bucket(iconBucketName));
		nameBucketMap.put(screenshotBucketName, new S3Bucket(screenshotBucketName));
		nameBucketMap.put(MimeCategory.DOCUMENT.toString(), new S3Bucket(assetBucketName));
		nameBucketMap.put(MimeCategory.IMAGE.toString(), new S3Bucket(assetBucketName));
		nameBucketMap.put(MimeCategory.VIDEO.toString(), new S3Bucket(videoBucketName));
		nameBucketMap.put(MimeCategory.AUDIO.toString(), new S3Bucket(audioBucketName));
		nameBucketMap.put(MimeCategory.FONT.toString(), new S3Bucket(assetBucketName));
	}

	/* (non-Javadoc)
	 * @see com.alleni.zebra.service.impl.helper.S3BucketFactory#getBucketForAsset(com.alleni.zebra.data.model.Asset)
	 */
	@Override
	public S3Bucket getBucketForAsset(Asset asset) {
		if (asset == null) return null;
		
		log.debug("asset mime-type: " + asset.getMimeType());
        try {
            String bucketName = MimeCategory.getCategory(asset.getMimeType()).toString();
            return nameBucketMap.get(bucketName);
        } catch(UnsupportedMimeTypeException umte) {
            throw new UnsupportedAssetMimeType(asset.getId(), umte);
        }
	}
	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.service.impl.helper.S3BucketFactory#getThumbnailBucket()
	 */
	@Override
	public S3Bucket getThumbnailBucket() {
		return nameBucketMap.get(thumbnailBucketName);
	}

	
	@Override
	public S3Bucket getIconBucket() {
		S3Bucket b = nameBucketMap.get(iconBucketName);
		return b;
	}
	
	@Override
	public S3Bucket getAvatarBucket() {
		return nameBucketMap.get(avatarBucketName);
	}
	
	@Override
	public String getIconBucketName() {
		return iconBucketName;
	}

	@Override
	public S3Bucket getScreenshotBucket() {
		S3Bucket b = nameBucketMap.get(screenshotBucketName);
		return b;
	}
	
	@Override
	public String getScreenshotBucketName() {
		return iconBucketName;
	}

	
	// setters, configured in s3Context.xml

	public void setThumbnailBucketName(String thumbnailBucketName) {
		this.thumbnailBucketName = thumbnailBucketName;
	}

	public void setAssetBucketName(String assetBucketName) {
		this.assetBucketName = assetBucketName;
	}

	public void setVideoBucketName(String videoBucketName) {
		this.videoBucketName = videoBucketName;
	}

	public void setAudioBucketName(String audioBucketName) {
		this.audioBucketName = audioBucketName;
	}

	public void setIconBucketName(String iconBucketName) {
		this.iconBucketName = iconBucketName;
	}

	public void setScreenshotBucketName(String screenshotBucketName) {
		this.screenshotBucketName = screenshotBucketName;
	}

	public void setAvatarBucketName(String avatarBucketName) {
		this.avatarBucketName = avatarBucketName;
	}

	
}
