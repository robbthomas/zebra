package com.alleni.zebra.s3;

import java.io.IOException;
import java.io.InputStream;

import org.jets3t.service.S3ServiceException;
import org.jets3t.service.ServiceException;
import org.springframework.security.access.prepost.PreAuthorize;

import com.alleni.zebra.asset.UnsupportedMimeTypeException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.model.Asset;

public interface S3Service {

	public void uploadAsset(Asset asset, byte[] data) throws IOException, S3ServiceException;
	
	/**
	 * implementation is synchronized, since AssetUploadServiceImpl service spawns a thread for each request. 
	 */
	public void uploadThumbnail(Asset asset, byte[] data) throws Exception;
	
	public InputStream downloadData(Asset asset) throws ServiceException, UnsupportedMimeTypeException, NotFoundException;
	
	public InputStream downloadThumbnailData(Asset asset) throws ServiceException, NotFoundException;
	
	@PreAuthorize("hasRole('ROLE_TYPE_SITE_ADMIN')")
	public void copyBucket(String source, String dest) throws Exception;

	@PreAuthorize("hasRole('ROLE_TYPE_SITE_ADMIN')")
	public void deleteFromBucket(String bucket) throws Exception;

	public void uploadIconData(String iconName, int length, String string,
			byte[] createIcon, boolean allowPublicAccess) throws Exception;

	public void uploadScreenshotData(String iconName, int length, String mimeType,
			byte[] createIcon, boolean allowPublicAccess) throws Exception;

	
	public void uploadUploadableAsset(UploadableAsset asset, byte[] imageData, boolean allowPublicAccess) throws Exception;
	
}
