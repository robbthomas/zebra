package com.alleni.zebra.s3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Executor;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.ServiceException;
import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;
import org.jets3t.service.utils.ServiceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.alleni.zebra.asset.UnsupportedMimeTypeException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.model.Asset;

/**
 * This Service is defined in S3Context.xml, to load the keys from a properties file.
 * 
 *
 */
public class S3ServiceImpl implements com.alleni.zebra.s3.S3Service {
	@Autowired private Executor executor;
	
	private final Logger log = Logger.getLogger(getClass());

	private String accessKey;
	private String secretKey;

	private org.jets3t.service.S3Service s3Service;

	private S3BucketFactory bucketFactory;

	/**
	 * initialize s3service
	 * @throws S3ServiceException 
	 */
	@PostConstruct
	public void init() throws S3ServiceException {
		AWSCredentials creds = new AWSCredentials(accessKey, secretKey);
		this.s3Service = new RestS3Service(creds);
	}

	
	class Uploader implements Runnable {
		Logger log = Logger.getLogger(Uploader.class);
		private org.jets3t.service.S3Service s3Svc;
		private S3Bucket bucket;
		private byte[] data;
		private Asset asset;
		public Uploader(org.jets3t.service.S3Service s3Svc, S3Bucket bucket, byte[] data, Asset asset) {
			this.s3Svc = s3Svc;
			this.bucket = bucket;
			this.data = data;
			this.asset = asset;
		}
		public void run() {
			ByteArrayInputStream bis = new ByteArrayInputStream(data);
			byte[] hash = null;
	        try {
	            try {
					hash = ServiceUtils.computeMD5Hash(bis);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        } catch(NoSuchAlgorithmException nsae) {
	            // this shouldn't be able to happen so don't expose it programatically
	            throw new RuntimeException(nsae);
	        }
			bis.reset();

			S3Object obj = new S3Object(bucket, asset.getFileID());
			obj.setDataInputStream(bis);
			obj.setContentLength(asset.getDataSizeInBytes());
			obj.setContentType(asset.getMimeType());
			obj.setMd5Hash(hash);
			try {
				s3Svc.putObject(bucket, obj);
			} catch (S3ServiceException e) {
				log.error(e);
				e.printStackTrace();
			}
			log.debug("upload ok.");
		}
	}
	
	@Override
	public synchronized void uploadAsset(final Asset asset, final byte[] data) throws IOException, S3ServiceException {
		log.debug("uploading asset: " + asset.getName());
		Assert.notNull(asset, "Asset cannot be null!");
		Assert.notNull(data, "asset data cannot be null!");
		S3Bucket bucket = null;
        bucket = bucketFactory.getBucketForAsset(asset);
		executor.execute( new Uploader(s3Service, bucket, data, asset));

	}





	@Override
	public synchronized void uploadThumbnail(Asset asset, byte[] data) throws Exception {
		S3Bucket bucket = bucketFactory.getThumbnailBucket();
		log.debug("uploading thumbnail to s3: " + asset.getFileID());

		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		byte[] hash = ServiceUtils.computeMD5Hash(bis);
		bis.reset();

		S3Object obj = new S3Object(bucket, asset.getFileID());
		obj.setDataInputStream(new ByteArrayInputStream(data));
		obj.setContentLength(asset.getDataSizeInBytes());
		obj.setContentType(asset.getMimeType());
		obj.setMd5Hash(hash);
		s3Service.putObject(bucket, obj);
		log.debug("complete");
	}

	
	@Override
	public synchronized void uploadIconData(String fileName, int sizeInBytes, String mimeType, byte[] data, boolean allowPublicAccess) throws Exception {
		S3Bucket bucket = bucketFactory.getIconBucket();
		log.debug("uploading icon to s3: " + fileName);

		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		byte[] hash = ServiceUtils.computeMD5Hash(bis);
		bis.reset();

		S3Object obj = new S3Object(bucket, fileName);
		obj.setDataInputStream(new ByteArrayInputStream(data));
		obj.setContentType(mimeType);
		obj.setMd5Hash(hash);
		
		if (allowPublicAccess) {
			obj.setAcl(AccessControlList.REST_CANNED_PUBLIC_READ);
		}
		
		s3Service.putObject(bucket, obj);
		log.debug("complete");
	}


	@Override
	public synchronized void uploadScreenshotData(String fileName, int sizeInBytes, String mimeType, byte[] data, boolean allowPublicAccess)
			throws Exception {
		S3Bucket bucket = bucketFactory.getScreenshotBucket();
		log.debug("uploading icon to s3: " + fileName);

		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		byte[] hash = ServiceUtils.computeMD5Hash(bis);
		bis.reset();

		S3Object obj = new S3Object(bucket, fileName);
		obj.setDataInputStream(new ByteArrayInputStream(data));
		obj.setContentType(mimeType);
		obj.setMd5Hash(hash);
		
		if (allowPublicAccess) {
			obj.setAcl(AccessControlList.REST_CANNED_PUBLIC_READ);
		}
		
		s3Service.putObject(bucket, obj);
		log.debug("complete");
	}

	@Override
	public synchronized void uploadUploadableAsset(UploadableAsset asset, byte[] data,
			boolean allowPublicAccess) throws Exception {
//		S3Bucket bucket = bucketFactory.getAvatarBucket();
		S3Bucket bucket = new S3Bucket(asset.getUploadableAssetTypeSupportedMimeType().getUploadableAssetS3Bucket().getBucketpath());
		log.debug("uploading avatar to s3: " + asset.getFileid());

		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		byte[] hash = ServiceUtils.computeMD5Hash(bis);
		bis.reset();

		S3Object obj = new S3Object(bucket, asset.getFileid());
		obj.setDataInputStream(new ByteArrayInputStream(data));
		obj.setContentType(asset.getUploadableAssetTypeSupportedMimeType().getMimeType().getIsoname());
		obj.setMd5Hash(hash);
		
		if (allowPublicAccess) {
			obj.setAcl(AccessControlList.REST_CANNED_PUBLIC_READ);
		}
		
		s3Service.putObject(bucket, obj);
		log.debug("complete");
	}



	@Override
	public InputStream downloadData(Asset asset) throws ServiceException, UnsupportedMimeTypeException, NotFoundException {
		S3Bucket bucket = bucketFactory.getBucketForAsset(asset);

		S3Object obj = s3Service.getObject(bucket.getName(), asset.getFileID());

		if (obj == null) {throw new NotFoundException((String)null);}
		else
			return obj.getDataInputStream();
	}

	@Override
	public InputStream downloadThumbnailData(Asset asset)
	throws ServiceException, NotFoundException {
		try {
			S3Object obj = s3Service.getObject(bucketFactory.getThumbnailBucket().getName(), asset.getFileID());

			if (obj == null) {return null;}
			else
				return obj.getDataInputStream();
		} catch (ServiceException se) {
			// 99% of the time, it's a 404, which isn't really an error.
			log.debug(se);
			throw new NotFoundException("No resource found with name: " + asset.getFileID(), (String)asset.getId());
		}
	}



	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public void setBucketFactory(S3BucketFactory bucketFactory) {
		this.bucketFactory = bucketFactory;
	}

	@Override
	public void copyBucket(String sourceBucket, String destBucket) throws Exception {
		log.debug("copying from [" + sourceBucket + "] to [" + destBucket + "]");
		
		S3Object[] files = s3Service.listObjects(sourceBucket);
		for (S3Object file : files) {
			log.debug("copying file: " + file.getKey());
			s3Service.copyObject(sourceBucket, file.getKey(), destBucket, file, true);
		}
	}

	@Override
	public void deleteFromBucket(String bucket) throws Exception {
		S3Object[] files = s3Service.listObjects(bucket);
		log.debug("deleting " + files.length + " files.");
		for (S3Object file : files) {
			log.debug("deleting file: " + file.getKey());
			s3Service.deleteObject(bucket, file.getKey());
		}
	}


}
