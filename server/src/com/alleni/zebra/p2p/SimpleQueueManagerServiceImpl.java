package com.alleni.zebra.p2p;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.springframework.stereotype.Service;

@Service("queueManager")
public class SimpleQueueManagerServiceImpl implements SimpleQueueManagerService {
	private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	private final ReadLock read = readWriteLock.readLock();
	private final WriteLock write = readWriteLock.writeLock();
	
	private final Map<String, Queue<Object>> queues = new HashMap<String, Queue<Object>>();

	@Override
	public void post(String queueId, Object value) {
		Queue<Object> fifo;
		write.lock();
		
		if (queues.containsKey(queueId)) {
			fifo = queues.get(queueId);
		} else {
			fifo = new LinkedList<Object>();
			queues.put(queueId, fifo);
		}
		fifo.add(value);
		
		write.unlock();
	}



	@Override
	public Object get(String queueId) {
		Object ret = null;
		
		read.lock();

		if (queues.containsKey(queueId)) {
			Queue<Object> fifo = queues.get(queueId);
			ret = fifo.remove();
			if (fifo.isEmpty()) {
				queues.remove(queueId);
			}
		}

		read.unlock();
		
		return ret;
	}


}
