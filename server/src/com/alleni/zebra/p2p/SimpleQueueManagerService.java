package com.alleni.zebra.p2p;

public interface SimpleQueueManagerService {
	
	public void post(String queueId, Object value);
	
	public Object get(String queueId);
	
}
