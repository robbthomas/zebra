package com.alleni.zebra.asset;

import java.util.HashMap;

/**
 * There is duplicated data between this class and MimeCategory. 
 *
 */
public class SupportedMimeTypeExtensionMap {

	/**
	 * Returns the extension of a mime-type, if we can figure it out.
	 */
	public static String getExtension(String type) {
		return map.get(type);
	}


	private SupportedMimeTypeExtensionMap() {}

	private static final HashMap<String, String> map = new HashMap<String, String>();

	static {
		map.put("image/jpeg",".jpg");
		map.put("image/gif",".gif");
		map.put("image/png",".png");
		map.put("image/svg+xml",".svg");

		map.put("application/x-shockwave-flash",".swf");

		map.put("video/mpeg",".mpg"); // a guess, there are a dozen possibilities
		map.put("video/x-flv",".flv");
		map.put("video/mp4",".mp4");

		map.put("application/pdf",".pdf");
		map.put("text/xml",".xml");
		map.put("text/plain",".txt");
		map.put("text/css",".css");

		map.put("audio/mp4",".mp4");
		map.put("audio/wav",".wav");
		map.put("audio/basic",".au");
		map.put("audio/mpeg",".mp3");
		map.put("audio/x-aiff",".aif");
		map.put("audio/x-wav",".wav");

		map.put("application/x-font",".otf");
		map.put("application/x-font-truetype",".ttf");
		map.put("application/x-font-opentype",".otf");

	}

}
