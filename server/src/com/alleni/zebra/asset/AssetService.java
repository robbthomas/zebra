package com.alleni.zebra.asset;

import java.io.IOException;
import java.io.InputStream;

import org.jets3t.service.S3ServiceException;
import org.jets3t.service.ServiceException;

import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.Asset;

public interface AssetService {
	
	/**
	 * Gateway for all asset uploads.  Create the database record, upload data to S3,
	 * generate thumbnail.
	 * 
	 * @param asset
	 * @param data
	 * @return the same Asset passed in
	 * @throws Exception 
	 * @throws IOException 
	 * @throws S3ServiceException 
	 */
	public Asset createWithThumbnail(Asset asset, byte[] data) throws S3ServiceException, IOException;

    /**
   	 * Gateway Event Image previews
   	 *
   	 * @param asset
   	 * @param data
   	 * @return the same Asset passed in
   	 * @throws Exception
   	 * @throws IOException
   	 * @throws S3ServiceException
   	 */
   	public Asset createImagePreview(String projectId, Asset asset, byte[] data) throws S3ServiceException, IOException;
	
	
	
	/**
	 * Create an asset record with no data.
	 * 
	 * @param asset
	 * @return the same Asset passed in
	 * @throws NotFoundException 
	 */
	public Asset createMetaData(Asset asset) throws NotFoundException;



	
	/**
	 * Retrieves the database record for the asset, if it exists, and the user has permission.
	 * 
	 * @param assetId
	 * @param userId
	 * @return
	 * @throws NotFoundException
	 */
	public Asset getAsset(String assetId, String userId) throws NotFoundException;
	
	
	/**
	 * Retrieves the file-stream of the S3 media for this asset, if the user has
	 * permission.
	 * 
	 * @param asset
	 * @param userId
	 * @return
	 * @throws ServiceException when something goes wrong connecting to S3
	 * @throws UnsupportedMimeTypeException 
	 */
	public InputStream getMediaForAsset(Asset asset, String userId) throws ServiceException, NotFoundException, UnsupportedMimeTypeException;
	public InputStream getMediaForAsset(String assetId, String userId) throws ServiceException, NotFoundException, UnsupportedMimeTypeException;
	
	/**
	 * Updates the database record for this Asset, if the user has permission.
	 * 
	 * @param asset
	 * @param userId
	 * @return
	 * @throws NotFoundException
	 * @throws ConcurrencyException 
	 */
	public Asset update(Asset asset, String userId) throws NotFoundException, ConcurrencyException;
	
	/**
	 * Update an asset, and upload a new image, and thumbnail to S3.
	 * 
	 * @param asset
	 * @param userId
	 * @param data
	 * @return
	 * @throws S3ServiceException
	 * @throws IOException
	 * @throws Exception
	 */
	Asset update(Asset asset, String userId, byte[] data) throws S3ServiceException, IOException, Exception;


	
	/**
	 * Currently this method sets the is_deleted flag on the Asset record, and then
	 * calls update(), if the user has permissions.
	 * @param asset
	 * @param userId
	 * @return
	 * @throws NotFoundException
	 * @throws ConcurrencyException 
	 */
	public Asset delete(Asset asset, String userId) throws NotFoundException, ConcurrencyException;


	/**
	 * Returns the guessed extension including the "."
	 * 
	 * First guess:  use extension provided by filename.
	 * If no filename exists:
	 *    if the mime-type is of the "guessable" subset, guess from that
	 * Otherwise return null
	 * 
	 * 
	 * @param asset
	 * @return extension <em>including</em> the "."
	 */
	public String guessExtension(Asset asset);

	


	
}
