package com.alleni.zebra.asset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;

import org.apache.log4j.Logger;

import com.alleni.zebra.asset.thumbnail.ThumbnailNotSupportedException;

/**
 * Groups MimeTypes into media categories:  Image, Video, Document, etc 
 *
 * There is duplicated data between this class and SupportedMimeTypeExtensionMap. 
 */
public enum MimeCategory {
	IMAGE,
	VIDEO,
	DOCUMENT,
	AUDIO,
	APPLICATION,
	FONT;

	private static final Logger log = Logger.getLogger(MimeCategory.class);

	// for performance, this should go the other way,
	// but this is a little easier to understand
	private static final HashMap<MimeCategory, List<MimeType>> categoryTypesMap = new HashMap<MimeCategory, List<MimeType>>();
	
	static {
		try {
			ArrayList<MimeType> imageTypes = new ArrayList<MimeType>();
			imageTypes.add(new MimeType("image/jpeg"));
			imageTypes.add(new MimeType("image/gif"));
			imageTypes.add(new MimeType("image/png"));
			imageTypes.add(new MimeType("image/svg+xml"));
			categoryTypesMap.put(MimeCategory.IMAGE, imageTypes);

			ArrayList<MimeType> appTypes = new ArrayList<MimeType>();
			appTypes.add(new MimeType("application/x-shockwave-flash"));
			categoryTypesMap.put(APPLICATION, appTypes);
			
			ArrayList<MimeType> videoTypes = new ArrayList<MimeType>();
			videoTypes.add(new MimeType("video/mpeg"));
			videoTypes.add(new MimeType("video/x-flv"));
			videoTypes.add(new MimeType("video/mp4"));
			categoryTypesMap.put(MimeCategory.VIDEO, videoTypes);
			
			ArrayList<MimeType> docTypes = new ArrayList<MimeType>();
			docTypes.add(new MimeType("application/pdf"));
			docTypes.add(new MimeType("text/xml"));
			docTypes.add(new MimeType("text/plain"));
			docTypes.add(new MimeType("text/css"));
			categoryTypesMap.put(MimeCategory.DOCUMENT, docTypes);
			
			ArrayList<MimeType> audioTypes = new ArrayList<MimeType>();
			audioTypes.add(new MimeType("audio/mp4"));
			audioTypes.add(new MimeType("audio/wav"));
			audioTypes.add(new MimeType("audio/basic"));
			audioTypes.add(new MimeType("audio/mpeg"));
			audioTypes.add(new MimeType("audio/x-aiff"));
			audioTypes.add(new MimeType("audio/x-wav"));
			categoryTypesMap.put(MimeCategory.AUDIO, audioTypes);
			
			ArrayList<MimeType> fontTypes = new ArrayList<MimeType>();
			fontTypes.add(new MimeType("application/x-font"));
			fontTypes.add(new MimeType("application/x-font-truetype"));
			fontTypes.add(new MimeType("application/x-font-opentype"));
			categoryTypesMap.put(MimeCategory.FONT, fontTypes);

		} catch (MimeTypeParseException e) {
			// if we get here, it's programmer error
			throw new RuntimeException("You fool!  That's no mimetype!", e);
		}
		
	}
	
	
	/**
	 * Given a String, decide whether it's an IMAGE, VIDEO, or DOCUMENT
	 * @param type
	 * @return
	 * @throws ThumbnailNotSupportedException
	 */
	public static MimeCategory getCategory(MimeType type) throws UnsupportedMimeTypeException {

		for (MimeCategory cat: MimeCategory.values()) {
			List<MimeType> types = categoryTypesMap.get(cat);
			for (MimeType t : types) {
				log.trace("compare: " + t.toString());
				if (t.toString().equals(type.toString())) {
					// break out of the list
					log.trace("in: " + type.toString() + ", out: " + cat.toString());
					return cat;
				}
			}
		}
		
		// if this type isn't supported (or the user made a typo)
		throw new ThumbnailNotSupportedException("Unsupported MimeType: " + type.toString());
	}

	public static MimeCategory getCategory(String mimeType) throws UnsupportedMimeTypeException {
		try {
			return getCategory(new MimeType(mimeType));
		} catch (MimeTypeParseException e) {
			// if this type isn't supported (or the user made a typo)
			throw new UnsupportedMimeTypeException("Unsupported MimeType: " + mimeType);
		}
	}
	
	public static List<MimeType> getAllMimeTypes() {
		List<MimeType> ret = new ArrayList<MimeType>();

		for (MimeCategory cat: MimeCategory.values()) {
			ret.addAll(categoryTypesMap.get(cat));
		}
		
		return ret;
	}
	
}
