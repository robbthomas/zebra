package com.alleni.zebra.asset;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.asset.thumbnail.ThumbnailService;
import com.alleni.zebra.data.dao.AssetDao;
import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.s3.S3Service;

@Service("assetService")
public class AssetServiceImpl implements AssetService {
	private final Logger log = Logger.getLogger(getClass());

	@Autowired AssetDao assetDao;
	@Autowired S3Service s3Service;
	@Autowired ThumbnailService thumbnailService;

	@Override
	@Transactional
	public Asset createWithThumbnail(Asset asset, final byte[] data) throws IOException, S3ServiceException {
		log.debug("createWithThumbnail()");

		String fId = uploadAndThumb(asset, data);
		asset.setFileID(fId);
		assetDao.create(asset);
		log.trace("createWithThumbnail() -- exit");
		return asset;
	}

    @Override
   	@Transactional
    public Asset createImagePreview(String projectId, Asset asset, byte[] data) throws S3ServiceException, IOException {
        createWithThumbnail(asset, data);
        assetDao.setAsPreview(projectId, asset);
        return asset;
    }


	@Override
	@Transactional
	public Asset createMetaData(Asset asset) throws NotFoundException {
		String fId = UUID.randomUUID().toString();
		String ext = guessExtension(asset);
		if (ext != null) fId = fId + ext;

		asset.setFileID(fId);

		asset = assetDao.create(asset);
		return asset;
	}




	@Override
	public Asset update(Asset asset, String userId, byte[] data)
	throws S3ServiceException, IOException, ConcurrencyException, Exception {
		log.debug("updating asset [" + asset.getId() + "]");

		if (data != null && data.length > 0) {
			String fId = uploadAndThumb(asset, data);
			asset.setFileID(fId);
			assetDao.update(asset);
		} else {
			update(asset, userId);
		}

		return asset;
	}



	@Override
	public Asset update(Asset asset, String userId) throws NotFoundException, ConcurrencyException {
		assetDao.update(asset);
		return asset;
	}

	/* private method used by update(....data) and create(.....data)
	 * upload an asset to S3, generate a thumbnail, and return the S3 id
	 */
	private String uploadAndThumb(final Asset asset, final byte[] data) throws IOException, S3ServiceException {
		String fId = UUID.randomUUID().toString();
		String ext = guessExtension(asset);
		if (ext != null) fId = fId + ext;

		asset.setFileID(fId);


		log.trace("calling s3 upload service...");
		s3Service.uploadAsset(asset, data);
		log.trace("upload done");

		MimeCategory cat = null;
		try {
			cat = MimeCategory.getCategory(asset.getMimeType());
		} catch (Exception e) {
			log.error(e);
		}
		log.trace("category: " + cat == null? "null" : cat.toString());

		if (cat == MimeCategory.IMAGE || cat == MimeCategory.VIDEO || cat == MimeCategory.DOCUMENT) {
			log.trace("calling thumbnailService()...");
			thumbnailService.generateAndUploadThumbnail(asset, data);
		}
		log.trace("uploadAndThumb complete");
		return fId;
	}




	@Override
	public String guessExtension(Asset asset) {
		String ext = null;
		
		if (asset != null) {
			if (asset.getName()!= null) {
				int i = asset.getName().lastIndexOf('.');
				if (i > -1) {
					ext = asset.getName().substring(i);
				}
			}
			
			// if ext is still null then we may not be done yet
			if (ext == null && asset.getMimeType() != null ) {
					ext = SupportedMimeTypeExtensionMap.getExtension(asset.getMimeType());
			}
		}

		return ext;
	}



	@Override
	@Transactional
	public InputStream getMediaForAsset(String assetId, String userId)
	throws ServiceException, NotFoundException, UnsupportedMimeTypeException {
		Asset asset = getAsset(assetId, userId);
		return getMediaForAsset(asset, userId);
	}


	@Override
	public Asset getAsset(String assetId, String appUserId)
	throws NotFoundException {
		return assetDao.findByIdAndAppUser(assetId, appUserId);
	}


	@Override
	public InputStream getMediaForAsset(Asset asset, String userId) throws ServiceException, UnsupportedMimeTypeException, NotFoundException {
		return s3Service.downloadData(asset);
	}


	@Override
	public Asset delete(Asset asset, String userId) throws NotFoundException, ConcurrencyException {
		asset.setDeleted(true);
		assetDao.update(asset);
		return asset;
	}

}
