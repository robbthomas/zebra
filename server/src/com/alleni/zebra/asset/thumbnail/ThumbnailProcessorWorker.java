package com.alleni.zebra.asset.thumbnail;

import javax.activation.MimeType;

import org.apache.log4j.Logger;

import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.s3.S3Service;

/**
 *	Worker thread for generating and uploading thumbnail images.
 *
 *  Note that Runnable.run() can't throw Exceptions, so once we kick off a job 
 *  all we can do is log the error.  Since we're processing asynchronously, we
 *  wouldn't be able to notify the client of an error anyway. 
 *
 */
public class ThumbnailProcessorWorker implements Runnable {
	private final Logger log = Logger.getLogger(getClass());

	private S3Service s3Service;

	private final byte[] data;
	private final Asset asset;

	public ThumbnailProcessorWorker(Asset asset, byte[] data, S3Service s3Service) {
		this.data = data;
		this.asset = asset;
		this.s3Service = s3Service;
	}

	public void run() {
		log.debug("executing thumbnail thread");
		try {
			// create thumbnail
			final MimeType mimeType = new MimeType(asset.getMimeType());
			ThumbnailGenerator generator = ThumbnailGenerator.getInstance(mimeType);
			generator.generateThumbnail(data);

			// upload thumbnail
			s3Service.uploadThumbnail(asset, data);

		} catch (ThumbnailNotSupportedException noWorries) {
			log.warn("unsupported mime-type for thumbnail: " + asset.getMimeType());
		} catch (Exception e) {
			log.error("Problems Generating Thumbnail Imaage!", e);
			log.error("  It is likely this job never completed!");
		}
	}

}
