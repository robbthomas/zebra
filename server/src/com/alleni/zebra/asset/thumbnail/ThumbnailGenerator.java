package com.alleni.zebra.asset.thumbnail;

import javax.activation.MimeType;

import org.apache.log4j.Logger;

import com.alleni.zebra.asset.MimeCategory;
import com.alleni.zebra.asset.UnsupportedMimeTypeException;
import com.alleni.zebra.asset.thumbnail.strategy.DefaultThumbnailStrategy;
import com.alleni.zebra.asset.thumbnail.strategy.JaiThumbnailStrategy;
import com.alleni.zebra.asset.thumbnail.strategy.ThumbnailStrategy;

class ThumbnailGenerator {
	private final Logger log = Logger.getLogger(getClass());
	
	private ThumbnailStrategy generationStrategy;
	private MimeType mimeType;
	
	/**
	 * Builds a ThumbnailGenerator instance with the appropriate generator strategy
	 * for the passed MimeType.
	 * 
	 * @param mimeType
	 * @return
	 */
	public static ThumbnailGenerator getInstance(MimeType mimeType) throws UnsupportedMimeTypeException {
		ThumbnailStrategy strategy = null;
		
		MimeCategory cat = MimeCategory.getCategory(mimeType);
		switch(cat) {
		case DOCUMENT:
			strategy = new DefaultThumbnailStrategy();
			break;
		case IMAGE:
			strategy = new JaiThumbnailStrategy();
			break;
		case VIDEO:
			strategy = new DefaultThumbnailStrategy();
			break;
		default:
			strategy = new DefaultThumbnailStrategy();
			break;
		}
		
		return new ThumbnailGenerator(strategy, mimeType);
	}
	

	/**
	 * Protected constructor, part of the FactoryMethod/Strategy patterns.
	 * 
	 * @param strategy
	 * @param mimeType
	 */
	protected ThumbnailGenerator(ThumbnailStrategy strategy, MimeType mimeType) {
		this.generationStrategy = strategy;
		this.mimeType = mimeType;
	}
	
	
	/**
	 * Generate a thumbnail.
	 * 
	 * @param data
	 * @throws ThumbnailNotSupportedException when we don't know how to deal with this file format.
	 * @throws Exception when serious things go wrong
	 */
	public void generateThumbnail(byte[] data) throws ThumbnailNotSupportedException, Exception {
		log.debug("calling strategy: " + generationStrategy.getClass().getSimpleName());
		if (generationStrategy.supports(mimeType)) {
			generationStrategy.generateThumbnail(data, mimeType);
		} else {
			// sorry, we just don't know how to deal with this file format.
			throw new ThumbnailNotSupportedException();
		}
	}

}
