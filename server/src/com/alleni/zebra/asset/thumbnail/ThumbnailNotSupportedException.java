package com.alleni.zebra.asset.thumbnail;

import com.alleni.zebra.asset.UnsupportedMimeTypeException;


public class ThumbnailNotSupportedException extends UnsupportedMimeTypeException {
	private static final long serialVersionUID = 6440658500787660572L;

	public ThumbnailNotSupportedException() {
		super();
	}

	public ThumbnailNotSupportedException(String arg0) {
		super(arg0);
	}

	public ThumbnailNotSupportedException(Throwable arg0) {
		super(arg0);
	}

	public ThumbnailNotSupportedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
