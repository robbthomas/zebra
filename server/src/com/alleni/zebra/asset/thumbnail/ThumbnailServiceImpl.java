package com.alleni.zebra.asset.thumbnail;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;

import com.alleni.zebra.data.exception.UnsupportedAssetMimeType;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jets3t.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.alleni.zebra.asset.MimeCategory;
import com.alleni.zebra.asset.UnsupportedMimeTypeException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.s3.S3Service;

@Service("thumbnailService")
public class ThumbnailServiceImpl implements ThumbnailService {
	private final Logger log = Logger.getLogger(getClass());
//	private Executor executor = Executors.newCachedThreadPool();
	@Autowired private Executor executor;

	@Autowired S3Service s3Service;

	@Override
	public void generateAndUploadThumbnail(Asset asset, byte[] data) {
		Assert.notNull(asset, "Asset cannot be null.");
		Assert.notNull(data,"attempt made to generate thumbnail from null data");
		if (data.length == 0) { throw new IllegalArgumentException( "attemt made to generate thumbnail from null data"); }

		// we have to check mime-type here, even though it's done later, because
		// once we spawn the thread this method returns and the client will never know about
		// the error condition.
        try {
            @SuppressWarnings("unused")
            MimeCategory cat = MimeCategory.getCategory(asset.getMimeType());
        } catch(UnsupportedMimeTypeException umte) {
            throw new UnsupportedAssetMimeType(asset.getId(), umte);
        }

		log.debug("spawning thumbnail generation thread");
		executor.execute( new ThumbnailProcessorWorker(asset, data, s3Service));
	}


	/**
	 * Mostly delegates to the above method
	 */
	@Override
	public void generateAndUploadThumbnail(Asset asset) throws UnsupportedMimeTypeException, NotFoundException {
		Assert.notNull(asset, "Asset cannot be null.");

		InputStream is;
		try {
			is = s3Service.downloadData(asset);
			if (is == null) { throw new IllegalArgumentException( "attemt made to generate thumbnail from null data"); }


			// seems a shame to go through this when we'll just make it into an InputStream again later...
			byte[] data = IOUtils.toByteArray(is);
			generateAndUploadThumbnail(asset, data);

		} catch (ServiceException e) {
			log.fatal(e);
		}  catch (IOException e) {
			log.fatal(e);
		}

	}


	@Override
	public InputStream getThumbnail(Asset asset, String userId)
			throws NotFoundException, Exception {

		InputStream stream = null;
		
		// this is where we would include permission checks an
		// other business logic, if we had any.
		
		// instead, just give it to 'em.
		stream = s3Service.downloadThumbnailData(asset);
		
		return stream;
	}

}
