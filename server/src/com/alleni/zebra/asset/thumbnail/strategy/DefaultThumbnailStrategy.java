package com.alleni.zebra.asset.thumbnail.strategy;

import java.io.IOException;

import javax.activation.MimeType;

import org.apache.log4j.Logger;

public class DefaultThumbnailStrategy implements ThumbnailStrategy {
	private final Logger log = Logger.getLogger(getClass());

	@Override
	public byte[] generateThumbnail(byte[] data, MimeType mimeType)
			throws IOException {
		log.error("not supported!");
		throw new UnsupportedOperationException("DefaultThumbnailStrategy has no implementation!");
	}

	@Override
	public boolean supports(MimeType type) {
		return false;
	}

}
