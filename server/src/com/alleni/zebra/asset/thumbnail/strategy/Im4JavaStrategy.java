package com.alleni.zebra.asset.thumbnail.strategy;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.activation.MimeType;
import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.im4java.core.ConvertCmd;
import org.im4java.core.GMOperation;
import org.im4java.core.IM4JavaException;
import org.im4java.core.Stream2BufferedImage;

/**
 * @see http://im4java.sourceforge.net/docs/dev-guide.html
 * @author dhoyt
 *
 */
public class Im4JavaStrategy implements ThumbnailStrategy {
	private final Logger log = Logger.getLogger(getClass());

	@Override
	public byte[] generateThumbnail(byte[] data, MimeType mimeType) throws IOException {
		log.debug("processing byte array...");
		
		//convert byte array back to BufferedImage
		InputStream in = new ByteArrayInputStream(data);
		BufferedImage bImageFromConvert = ImageIO.read(in);

		GMOperation op = new GMOperation();
		op.addImage();                        // input
		op.addImage("png:-");                 // output: stdout
		op.resize(100,150);
		
		// set up command
		ConvertCmd convert = new ConvertCmd(true); // boolean flag to use GraphicsMagick instead of ImageMagick
		Stream2BufferedImage s2b = new Stream2BufferedImage();
		convert.setOutputConsumer(s2b);

		// run command and extract BufferedImage from OutputConsumer
		try {
			convert.run(op, bImageFromConvert);
			
		} catch (InterruptedException e) {
			log.error("weird!", e);
		} catch (IM4JavaException e) {
			// not much we can do about it now...
			log.error(e);
		}
		BufferedImage img = s2b.getImage();
		
		return getBytes(img, "png");
	}

	
	private byte[] getBytes(BufferedImage image, String formatName) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, formatName, baos);
		byte[] bytesOut = baos.toByteArray();
		return bytesOut;
	}


	@Override
	public boolean supports(MimeType type) {
		// right now, we don't support anything!
		return false;
	}
}
