package com.alleni.zebra.asset.thumbnail.strategy;

import java.io.IOException;

import javax.activation.MimeType;

import com.alleni.zebra.asset.thumbnail.ThumbnailNotSupportedException;

public interface ThumbnailStrategy {
	
	/**
	 * 
	 * @param data of original image
	 * @param MimeType of the extension of the original file
	 * @return
	 * @throws IOException 
	 * @throws ThumbnailNotSupportedException 
	 */
	public byte[] generateThumbnail(byte[] data, MimeType mimeType) throws IOException, ThumbnailNotSupportedException;
	
	
	public boolean supports(MimeType type);
}
