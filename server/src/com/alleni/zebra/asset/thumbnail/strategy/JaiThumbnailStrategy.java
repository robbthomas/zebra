package com.alleni.zebra.asset.thumbnail.strategy;

import java.awt.RenderingHints;
import java.awt.image.renderable.ParameterBlock;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.media.jai.OpImage;
import javax.media.jai.RenderedOp;

import org.apache.log4j.Logger;

import com.alleni.zebra.asset.thumbnail.ThumbnailNotSupportedException;
import com.sun.media.jai.codec.SeekableStream;

/**
 * Copied more or less from the Scala thumbnail generator 
 *
 */
public class JaiThumbnailStrategy implements ThumbnailStrategy {
	private static final Logger log = Logger.getLogger(JaiThumbnailStrategy.class);

	private final float MAX_WIDTH = 50.0f;

	
	@Override
	public byte[] generateThumbnail(byte[] data, MimeType mimeType)
	throws IOException, ThumbnailNotSupportedException {

		ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
		SeekableStream seekableImageStream = SeekableStream.wrapInputStream(inputStream, true);

		RenderedOp originalImage = JAI.create("stream", seekableImageStream);

		OpImage opImage = (OpImage) originalImage.getRendering();
		opImage.setTileCache(null);

		float origImageWidth = (float)originalImage.getWidth();
		float origImageHeight = (float)originalImage.getHeight();
		log.debug("orig image height: " + origImageHeight + " width: " + origImageWidth);

		float scale = 1.0f;
		if (origImageWidth > origImageHeight) {
			scale = (MAX_WIDTH / origImageWidth);
			log.debug("scaling based on width:" + scale);
		}
		else {
			scale = (MAX_WIDTH / origImageHeight);
			log.debug("scaling based on height:" + scale);
		}
		// now resize the image
		ParameterBlock paramBlock = new ParameterBlock();
		paramBlock.addSource(originalImage); // The source image
		paramBlock.add((float)scale); // The xScale
		paramBlock.add((float)scale); // The yScale
		//paramBlock.add(0.1.toFloat); // The x translation
		//paramBlock.add(0.1.toFloat); // The y translation

		RenderingHints qualityHints= new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		RenderedOp resizedImage = JAI.create("scale", paramBlock, qualityHints);

		// lastly, write the newly-resized image to an output stream, in a specific encoding
		ByteArrayOutputStream encoderOutputStream = new ByteArrayOutputStream();
		log.debug("about to run JAI.create");
		JAI.create("encode", resizedImage, encoderOutputStream, "PNG", null);
		log.debug("complete.");
		// Export to Byte Array
		byte[] resizedImageByteArray = encoderOutputStream.toByteArray();
		return resizedImageByteArray;
	}

	@Override
	public boolean supports(MimeType type) {
		boolean ret = false;
		String[] mimeTypeStrings = ImageIO.getReaderMIMETypes();
		for (String s : mimeTypeStrings) {
			try {
				if (type.match(s)) {
					ret = true;
					break;
				}
			} catch (MimeTypeParseException ignore) {
				// this should have been caught by now, but just in case...
				log.error(ignore);
			}
		}
		return ret;
	}

	
}
