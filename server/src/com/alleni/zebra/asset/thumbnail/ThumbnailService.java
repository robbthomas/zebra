package com.alleni.zebra.asset.thumbnail;

import java.io.InputStream;

import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.Asset;


public interface ThumbnailService {
	public static final String DEFAULT_MIME_TYPE = "image/png";
	
	/**
	 * Intended to be used primarily from other Asset-related Services,
	 * generates a thumbnail image from raw file data.
	 * 
	 * @param data
	 * @param mimeType
	 * @throws Exception 
	 */
	public void generateAndUploadThumbnail(Asset asset, byte[] data);
	
	
	/**
	 * Intended for use from within a Controller, for instance, to handle a request to
	 * generate a thumbnail from a given Asset record.
	 *  
	 * @param asset
	 * @throws Exception 
	 */
	public void generateAndUploadThumbnail(Asset asset) throws Exception;
	
	
	/**
	 * Download a thumbnail from S3, if the user has permission.
	 * 
	 * NOTE:  the old system had no permission checks on this.  For now, 
	 *        this is the way we do it.
	 *        
	 *  TODO:  Add permissions check
	 * 
	 * @param asset to whom the thumbnail belongs
	 * @param userId foreign key into app_user table.
	 * @return an InputStream from the S3 thumbnail file.
	 * 
	 * @throws Exception
	 */
	public InputStream getThumbnail(Asset asset, String userId) throws NotFoundException, Exception;
}
