package com.alleni.zebra.asset;

public class UnsupportedMimeTypeException extends Exception {

	private static final long serialVersionUID = 1829647029590846505L;

	public UnsupportedMimeTypeException() {
		super();
	}

	public UnsupportedMimeTypeException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public UnsupportedMimeTypeException(String arg0) {
		super(arg0);
	}

	public UnsupportedMimeTypeException(Throwable arg0) {
		super(arg0);
	}

}
