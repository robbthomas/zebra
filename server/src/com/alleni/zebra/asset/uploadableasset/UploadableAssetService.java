package com.alleni.zebra.asset.uploadableasset;

import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.model.AppUser;

public interface UploadableAssetService {
	
	public UploadableAsset setAvatarForUser(AppUser user, UploadableAsset asset, byte[] imageData);
	
	public UploadableAsset setBannerForUser(AppUser user, UploadableAsset asset, byte[] imageData);

	public UploadableAsset findById(AppUser user, Integer uploadableAssetId);
	
}
