package com.alleni.zebra.asset.uploadableasset;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.asset.uploadableasset.image.worker.ImageWorker;
import com.alleni.zebra.data.dao.AccountDao;
import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.hibernate.dao.UploadableAssetDao;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.model.AppUser;

@Service("uploadableAssetService")
public class UploadableAssetServiceImpl implements UploadableAssetService {
	protected final Log log = LogFactory.getLog(getClass());
	
	@Autowired @Qualifier("userAvatarWorker") ImageWorker avatarWorker;
	@Autowired @Qualifier("userBannerWorker") ImageWorker bannerWorker;
	@Autowired UploadableAssetDao uaDao;
	@Autowired AppUserDao auDao;
	@Autowired AccountDao accountDao;

	// exists for flushing sessions when working with JDBC and Hibernate
	@Autowired SessionFactory sessionFactory;

	@Transactional
	public UploadableAsset setAvatarForUser(AppUser user, UploadableAsset asset, byte[] imageData) {
		asset.setCreatedby(user.getId());
		asset.setDatecreated(new Date());
		asset.setDeleted(false);
		asset.setDisplayorder(0);
		asset = avatarWorker.processImage(user, asset, imageData, 0);

		// bridge between JDBC and Hibernate requeires this:
		sessionFactory.getCurrentSession().flush();
		
		auDao.setAvatar(user, asset);
		return asset;
	}

	@Transactional
	public UploadableAsset setBannerForUser(AppUser user, UploadableAsset asset, byte[] imageData) {
		asset.setCreatedby(user.getId());
		asset.setDatecreated(new Date());
		asset.setDeleted(false);
		asset.setDisplayorder(0);
		asset = bannerWorker.processImage(user, asset, imageData, 0);

		// bridge between JDBC and Hibernate requeires this:
		sessionFactory.getCurrentSession().flush();
		
		accountDao.setBanner(user.getAccountId(), asset);
		return asset;
	}

	@Transactional(readOnly = true)
	public UploadableAsset findById(AppUser user, Integer uploadableAssetId) {
		assert (uploadableAssetId != null);
		return uaDao.findById(uploadableAssetId);
	}

}
