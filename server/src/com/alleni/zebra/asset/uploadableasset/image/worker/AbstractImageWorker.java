package com.alleni.zebra.asset.uploadableasset.image.worker;

import java.awt.Graphics2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.mortennobel.imagescaling.ResampleOp;

public abstract class AbstractImageWorker implements ImageWorker {

	private static final Logger log = Logger.getLogger(AbstractImageWorker.class);

	public enum IconTarget {
		PUBLISHED_ICON {
			@Override
			public int[] getSizes() { return new int[]{50, 75, 100, 180};}
			@Override
			public boolean getSquare() { return true; } 
			@Override
			public boolean getRounded() { return true; } 
		},
		PUBLISHED_SCREENSHOT {
			@Override
			public int[] getSizes() { return new int[]{26, 70, 450};} 
			@Override
			public boolean getSquare() { return false; } 
			@Override
			public boolean getRounded() { return false; } 
		},
		BANNER {
			@Override
			public int[] getSizes() { return new int[]{710};}
			@Override
			public boolean getSquare() { return false; }
			@Override
			public boolean getRounded() { return false; }
		},
		AVATAR {
			@Override
			public int[] getSizes() { return new int[]{100};}
			@Override
			public boolean getSquare() { return true; }
			@Override
			public boolean getRounded() { return false; }
		},
		USER;
		public int[] getSizes() { return new int[]{80}; }
		public boolean getSquare() { return false; } 
		public boolean getRounded() { return true; }
	}
	
	
    public static String getFileSizeName(String fileName, int size) {
        return fileName + "-" + size + ".png";
    }

	
	
	protected static final int MIN_RADIUS = 10;
	
	
	protected static byte[] resize(byte[] data, int size, boolean square, boolean rounded, int minRadius) {
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(data);
			BufferedImage src = ImageIO.read(bis);
			int w = src.getWidth();
			int h = src.getHeight();

			int tx;
			int ty;
			int croppedW;
			int croppedH;
			int finalW;
			int finalH;
			if(square) {
				if(w<h) {
					tx = 0;
					ty = (int)((w - h)/2);
					croppedW = w;
					croppedH = w;
				} else {
					tx = (int)((h - w)/2);
					ty = 0;
					croppedW = h;
					croppedH = h;
				}
				finalW = size;
				finalH = size;
			} else {
				if(w<h) {
					finalW = (int)((double)size/h * w);
					finalH = size;
				} else {
					finalW = size;
					finalH = (int)((double)size/w * h);
				}
				tx = 0;
				ty = 0;
				croppedW = w;
				croppedH = h;
			}


			BufferedImage result = new BufferedImage(croppedW, croppedH, BufferedImage.TYPE_4BYTE_ABGR);
			Graphics2D g = result.createGraphics();

			if(rounded) {
				// set up a clipping region to prevent drawing in the corners
				int radius = Math.max(Math.min(w, h)/5, minRadius*Math.min(w,h)/size);
				g.clip(new RoundRectangle2D.Double(0, 0, croppedW, croppedH, radius, radius));
			}

			g.drawImage(src, (int)tx, (int)ty, null);

			// downsample the image to its final size
			ResampleOp resample = new ResampleOp(finalW, finalH);
			result = resample.filter(result, new BufferedImage(finalW, finalH, BufferedImage.TYPE_4BYTE_ABGR));

			// encode the image as png
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ImageIO.write(result, "PNG", out);
			return out.toByteArray();
		} catch (IOException ex) {
			log.error("Failed to construct image", ex);
		}
		return null;
	}

	public static boolean supports(MimeType type) {
		boolean ret = false;
		String[] mimeTypeStrings = ImageIO.getReaderMIMETypes();
		for (String s : mimeTypeStrings) {
			try {
				if (type.match(s)) {
					ret = true;
					break;
				}
			} catch (MimeTypeParseException ignore) {
				// this should have been caught by now, but just in case...
				log.error(ignore);
			}
		}
		return ret;
	}

}
