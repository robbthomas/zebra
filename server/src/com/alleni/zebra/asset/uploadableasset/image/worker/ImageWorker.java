package com.alleni.zebra.asset.uploadableasset.image.worker;

import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.model.AppUser;

public interface ImageWorker {
	/**
	 * Process an image, return the new UploadableAsset
	 * 
	 * The image record will have <code>renderingComplete == false</code> when this method returns.
	 * Clients should poll the database to find out when the job is done, by examining that value.
	 *  
	 */
	public abstract UploadableAsset processImage(AppUser user, UploadableAsset asset, byte[] data, int displayOrder);

}
