package com.alleni.zebra.asset.uploadableasset.image.worker;

import java.util.UUID;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.hibernate.dao.MimeTypeDao;
import com.alleni.zebra.data.hibernate.dao.UploadableAssetDao;
import com.alleni.zebra.data.hibernate.dao.UploadableAssetTypeDao;
import com.alleni.zebra.data.hibernate.dao.UploadableAssetTypeSupportedMimeTypeDao;
import com.alleni.zebra.data.hibernate.model.MimeType;
import com.alleni.zebra.data.hibernate.model.UploadableAsset;
import com.alleni.zebra.data.hibernate.model.UploadableAssetType;
import com.alleni.zebra.data.hibernate.model.UploadableAssetTypeSupportedMimeType;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.s3.S3Service;

@Component("userAvatarWorker")
public class UserAvatarWorker extends AbstractImageWorker {
	Logger log = Logger.getLogger(UserAvatarWorker.class);

	@Autowired AppUserDao userDao;
	@Autowired UploadableAssetDao assetDao;
	@Autowired UploadableAssetTypeDao uaTypeDao;
	@Autowired UploadableAssetTypeSupportedMimeTypeDao uatSmtDao;
	@Autowired MimeTypeDao mimeTypeDao;
	@Autowired S3Service s3Service;
	@Autowired Executor executor;

	private final IconTarget target = IconTarget.AVATAR;

	@Override
	@Transactional
	public UploadableAsset processImage(AppUser user, final UploadableAsset asset, final byte[] data, int displayOrder) {

		String id = UUID.randomUUID().toString();
		MimeType mt = mimeTypeDao.findByIsoName("image/png");
		UploadableAssetType uaType = uaTypeDao.findByTag(UploadableAssetType.AVATAR);
		UploadableAssetTypeSupportedMimeType uatSmt = uatSmtDao.findByMimeTypeAndUploadableAssetType(mt, uaType);
		
		asset.setFileid(id);
		asset.setDisplayorder(displayOrder);
		asset.setRenderingcomplete(false);
		asset.setDeleted(false);
		asset.setUploadableAssetTypeSupportedMimeType(uatSmt);
		assetDao.persist(asset);
		log.debug("created UploadableAsset record: " + asset.getUploadableassetid());

		executor.execute(
				new Runnable() {
					@Override
					@Transactional(propagation=Propagation.REQUIRES_NEW)
					public void run() {

						try {
							for(int size : target.getSizes()) {
								log.debug("uploading and processing icon...");
//								String filename = getFileSizeName(asset.getFileid(), size);
								byte[] createIcon = resize(data, size, target.getSquare(), target.getRounded(), MIN_RADIUS);
								s3Service.uploadUploadableAsset(asset, createIcon, true);
								//(filename, data.length, "image/png", createIcon, true);
							}
							
							asset.setRenderingcomplete(true);
							assetDao.merge(asset);

						} catch (Exception e) {
							e.printStackTrace();
							log.error("Failed to upload an image: " + asset.getUploadableassetid());
						}
						log.debug("  image processing complete.");

					}
				}
		);

		return asset;
	}

}
