package com.alleni.zebra.system.mail;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.hibernate.model.GuestList;
import com.alleni.zebra.data.hibernate.model.GuestListInvitee;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.gadget.ProjectType;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.soy.ClosureUtil;

@Service("guestListInviteeEmailer")
public class GuestListInviteeEmailer {
	@Autowired Mail defaultMail;
	@Autowired AppUserDao auDao;
	@Autowired ProjectReadService projectReadSvc;

	@Transactional
	public void sendInvites(AppUser sender, GuestList guestList, String projectId, String baseUrl) {
		Project p = projectReadSvc.getMinimal(projectId, sender);
		// private-publishing #10, #11
		// send email based on whether or not the invitee is a registered user
		for (GuestListInvitee invitee : guestList.getGuestlistinvitees()) {
			boolean isMember = false;


			// first find out if they've got an account
			@SuppressWarnings("unused")
			AppUser inviteeLogin = null;
			try {
				inviteeLogin = auDao.findByEmail(invitee.getEmail());
				isMember = true;
			} catch (Exception notAnException) {
				//ignore
			}

			Map<String, Object> args = new HashMap<String, Object>();
			args.put("url", baseUrl);
			args.put("userBody", guestList.getEmailtext());
			args.put("projectId", p.getApnUUID());
			args.put("authorFirstName", sender.getFirstName());
			
			
			args.put("projectType", (p.getProjectType() == ProjectType.APP? "application" : "zapp"));

			String body = "undefined";
			String bodyHTML = "undefined";
			String subject = "undefined";
			if (isMember) {
				body = ClosureUtil.render("inviteMember.soy",
						"services.sharedprojectinvitee.create.member", "plaintext", args);
				bodyHTML = ClosureUtil.render("inviteMember.soy",
						"services.sharedprojectinvitee.create.member", "html", args);
				subject = ClosureUtil.render("inviteMember.soy",
						"services.sharedprojectinvitee.create.member", "subject", args);
			} else {
				body = ClosureUtil.render("inviteNonMember.soy",
						"services.sharedprojectinvitee.create.nonmember", "plaintext", args);
				bodyHTML = ClosureUtil.render("inviteNonMember.soy",
						"services.sharedprojectinvitee.create.nonmember", "html", args);
				subject = ClosureUtil.render("inviteNonMember.soy",
						"services.sharedprojectinvitee.create.nonmember", "subject", args);
			}

			defaultMail.sendMultiPartMessage("donotreply@zebrazapps.com", invitee.getEmail(), subject, body, bodyHTML);

		}// end for invitee
	}
}
