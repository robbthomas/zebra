package com.alleni.zebra.system.mail;

public interface Mail {

	public abstract void setBypassToEmails(String emails);

	public abstract void sendMultiPartMessage(String from, String to,
			String subject, String body, String bodyHTML);

}