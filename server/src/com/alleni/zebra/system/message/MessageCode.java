package com.alleni.zebra.system.message;


public enum MessageCode {
    NOT_ALLOWED(401, 400, "Operation not allowed."),
    SESSION_EXPIRED(400, 400, "Session expired."),

    ASSET_NOT_FOUND(408, 400, "The requested asset does not exist."),
    UNSUPPORTED_MIME_TYPE(408, 400, "The mime type of the associated asset is not supported by this application."),
    UNSUPPORTED_MIME_TYPE_FOR_THUMBNAIL(409, 400, "The mime type of the associated asset does not support thumbnail generation."),
    S3_THUMNAIL_NOT_FOUND(410, 400, "Thumbnail data not found.  Try again in a minute."),
    S3_DATA_NOT_FOUND(411, 400, "Asset data not found."),
  
    RECORD_LOCKED_IN_TRANSACTION(601, 400, "The database record is in use by another transaction.  Please try again in a second."),
    DATA_ACCESS_EXCEPTION(600, 400, "Unspecified Database Access Exception."),

    UPDATE_FAILED(900, 400, "Could not update item."),
    ASSET_SAVE_FAILED(901, 400, "Could not save your asset."),
    ASSET_LOAD_FAILED(902, 400, "Could not load asset."),

    PUBLISHED_GADGET_NOT_FOUND(710, 400, "Published Gadget Not Found"),
    PUBLISH_FAILED(711, 400, "Published Gadget save or update failed"),
    DUPLICATE_PUBLISHED_GADGET_NAME(712, 400, "There is already an item published with this name and type (gadget or zapp)."),
    EDIT_TO_DELETED_ITEM(713, 400, "The item you are attempting to edit has been deleted."),
    CANNOT_REPUBLISH_GADGET(714, 400, "The gadget you are attempting to publish contains a gadget for which you do not have sufficient re-publishing rights."),
    PUBLISH_PRICE_TOO_LOW(715, 400, "You need to set the price a little higher"),
    PROJECT_IS_SHARED(716, 400, "This project has been shared to one or more servicers."),
    ILLEGAL_NAME(717, 400, "The name you have selected is not allowed."),
    
    PURCHASED_GADGET_NOT_FOUND(721, 400, "Purchased Gadget not found"),
    PURCHASE_FAILED(722, 400, "Purchase attempt failed"),
    PURCHASE_FAILED_BAD_ACCOUNT(723, 400, "The purchaser's account is not currently in good standing."),
    
    CANNOT_LOAD_REMOTE(750, 400, "This Zapp can only be viewed from within the ZebraZapps.com domain."),
    
    GADGET_STATE_NOT_FOUND(810, 400, "Gadget State not found"),
    GADGET_STATE_SAVE_FAILED(820, 400, "GadgetState save failed"),

    GADGET_DELETE_FAILED__IN_USE(917, 400, "Object cannot be deleted because other objects are using it."),
    GADGET_SAVE_FAILED(913, 400, "Could not save Gadget."),
    DUPLICATE_GADGET_NAME(914, 400, "There is already an item with this name."),
    GADGET_LOAD_FAILED(915, 400, "Could not load Gadget."),
    GADGET_BRANCH_DETECTED(916, 400, "Branch Detected."),
    GADGET_NOT_FOUND(917, 400, "Gadget Not Found"),
    
    USER_EXISTS_IN_ACCOUNT(601, 400, "The user already exists in the account"),
    USER_NOT_IN_ACCOUNT(602, 400, "The user is not in this account"),
    UNKNOWN_ERROR(999, 500, "Unknown Exception Occurred."),
    
    OK(200, 200, "OK"),
    NULL(0, 200, "");

    public final int code;
    public final int suggestedHttpRespCode;
    public final String message;

    private MessageCode(int code, int httpRespCode, String message) {
        this.code = code;
        this.message = message;
        this.suggestedHttpRespCode = httpRespCode;
    }

    @Override
    public String toString() {
        return String.valueOf(code);
    }
}
