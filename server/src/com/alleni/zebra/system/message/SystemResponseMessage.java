package com.alleni.zebra.system.message;

import java.util.HashMap;
import java.util.Map;

/**
 * An immutable class representing a JSON message back to the client,
 * communicating some known state or condition.
 */
public class SystemResponseMessage {
    /**keys for values() Map */
    public static final String CODE = "code";
    public static final String LEVEL = "level";
    public static final String TYPE = "type";
    public static final String MESSAGE = "message";
    public static final String EXCEPTION = "exception";
    public static final String ID = "id";


    public final MessageCode code;
    public final MessageLevel level;
    public final MessageType type;
    public final String message;
    public final Exception exception;
    public final String objectId;


    public SystemResponseMessage(MessageCode code,
                                     MessageLevel level,
                                     MessageType type,
                                     String message,
                                     Exception exception,
                                     String objectId) {
        this.code = code;
        this.level = level;
        this.type = type;
        this.message = message;
        this.exception = exception;
        this.objectId = objectId;
    }

    /**
     * Default values:  MessageLevel.INFO, MessageType.BUSINESS, code.message, null, objectId 
     * @param code
     * @param objectId
     */
    public SystemResponseMessage(MessageCode code, String objectId) {
        this(code, MessageLevel.INFO, MessageType.BUSINESS, code.message, null, objectId);
    }
    
    /**
     * How... convenient
     * @param objectId
     * @return
     */
    public static final SystemResponseMessage okMessage(String objectId) {
    	return new SystemResponseMessage(MessageCode.OK, objectId);
    }
    
    
    /**
     *
     * @return the contents of this message, in a format suitable for rendering as a JSON.
     */
    public Map<String, Object> values() {
        Map<String, Object> values = new HashMap<String, Object>();
        if (code != null) {values.put(CODE, code.toString());}
        if (level != null) {values.put(LEVEL, level.toString());}
        if (type != null) {values.put(TYPE, type.toString());}
        if (message != null) {values.put(MESSAGE, message);}
        if (exception != null) {values.put(EXCEPTION, exception.toString());}
        if (objectId != null) {values.put(ID, objectId);}
        
        return values;
    }

 
    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        Map<String, Object> vals = values();
        for (String key : vals.keySet()) {
            ret.append("[" + key + "]");
            ret.append(" " + vals.get(key) );
        }
        return ret.toString();
    }
}
