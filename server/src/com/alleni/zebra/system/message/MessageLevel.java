package com.alleni.zebra.system.message;

public enum MessageLevel {
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR,
    FATAL
}
