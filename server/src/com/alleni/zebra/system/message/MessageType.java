package com.alleni.zebra.system.message;

public enum MessageType {
    BUSINESS,
    ACCESS,
    INTERNAL
}
