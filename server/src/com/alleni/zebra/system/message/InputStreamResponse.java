package com.alleni.zebra.system.message;

import java.io.InputStream;

public class InputStreamResponse extends ZephyrResponse{
	private InputStream data;
	private String contentType;
	private long contentLength;
	
	public InputStream getData() {
		return data;
	}
	public void setData(InputStream data) {
		this.data = data;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public long getContentLength() {
		return contentLength;
	}
	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}
}
