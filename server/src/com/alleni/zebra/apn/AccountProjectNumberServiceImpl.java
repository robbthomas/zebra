package com.alleni.zebra.apn;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.commerce.productcode.ProductCodeService;
import com.alleni.zebra.data.dao.AccountProjectNumberDao;
import com.alleni.zebra.data.model.AccountProjectNumber;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.CurrencyType;
import com.alleni.zebra.data.model.ProductCode.ProductType;
import com.alleni.zebra.data.model.Project;

@Service("accountProjectNubmerService")
public class AccountProjectNumberServiceImpl implements AccountProjectNumberService {
	@Autowired AccountProjectNumberDao apnDao;
	@Autowired ProductCodeService pcSvc;

	@Transactional
	public AccountProjectNumber createAccountProjectNumberForPublishOrUpdate(Project p, AppUser owner) {
		// create invoice
		String invoiceDesc = "Creating or Updating Published Item";
		long invoiceId = apnDao.createInvoice(p.getAccountId(), invoiceDesc, null);
		int lineNumber = 1;
		int quantity = 1;
		long invoiceLineItemId = apnDao.createLineItem(invoiceId, lineNumber, quantity, 0, pcSvc.valueOf("2038-01-19", ProductType.COM), invoiceDesc, p.getProjectId());

		// create new APN row for author
		AccountProjectNumber ppp = new AccountProjectNumber();
		ppp.setProjectId(p.getProjectId());
		ppp.setAccountId(owner.getAccountId());
		ppp.setInvoiceLineItemId(invoiceLineItemId);
		ppp.setCreatedDateTime(new Date());
		ppp = apnDao.create(ppp);
		
		return ppp;
	}


}
