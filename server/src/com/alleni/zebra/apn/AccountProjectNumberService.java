package com.alleni.zebra.apn;

import com.alleni.zebra.data.model.AccountProjectNumber;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;

public interface AccountProjectNumberService {
	
	public AccountProjectNumber createAccountProjectNumberForPublishOrUpdate(Project p, AppUser owner);
}
