package com.alleni.zebra.project;

import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.data.model.Project;

import java.util.Map;

public class ProjectCreateParameters {

	public Project project;
    public boolean ignoreDuplicateNames = false;
    public boolean ignoreDuplicatePublishedNames = false;
	public String iconFromProjectId;
    public String screenshotsFromProjectId;
    public String contentFromProjectId;
	public boolean ignoreSharedState;

    public byte[] icon;
    public Map<Integer, byte[]> screenshots;
    public Asset previewImage;
    public byte[] previewImageData;
	
	public ProjectCreateParameters(Project project) {
		this.project = project;
	}

	public String getIconFromProjectId() {
		return iconFromProjectId;
	}

	public String getScreenshotsFromProjectId() {
		return screenshotsFromProjectId;
	}

	public boolean isIgnoreSharedState() {
		return ignoreSharedState;
	}

    public boolean isIgnoreDuplicateNames() {
   		return ignoreDuplicateNames;
   	}

    public boolean isIgnoreDuplicatePublishedNames() {
   		return ignoreDuplicateNames;
   	}

    public void setIgnoreDuplicateNames(boolean ignoreDuplicateNames) {
   		this.ignoreDuplicateNames = ignoreDuplicateNames;
   	}

    public void setIgnoreDuplicatePublishedNames(boolean ignoreDuplicatePublishedNames) {
   		this.ignoreDuplicatePublishedNames = ignoreDuplicatePublishedNames;
   	}

	public void setIconFromProjectId(String iconFromProjectId) {
		this.iconFromProjectId = iconFromProjectId;
	}

	public void setScreenshotsFromProjectId(String screenshotsFromProjectId) {
		this.screenshotsFromProjectId = screenshotsFromProjectId;
	}


}
