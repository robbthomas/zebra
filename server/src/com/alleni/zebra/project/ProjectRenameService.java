package com.alleni.zebra.project;

import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;

public interface ProjectRenameService {

	/**
	 * Rename all versions of the project, all the way down the tree, for as long as the author stays the same.
	 * 
	 * @param user
	 * @param gadgetId
	 * @param newName
	 * @return the head gadget
	 * @throws DataAccessException 
	 */
	@Transactional(readOnly = false)
	public abstract void renameAllProjectVersions(AppUser user,
			Project project, String newName, boolean ignoreDuplicateNames)
			throws DataAccessException;

}