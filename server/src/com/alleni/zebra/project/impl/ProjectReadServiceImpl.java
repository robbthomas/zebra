package com.alleni.zebra.project.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

import com.alleni.zebra.data.dao.AssetDao;
import com.alleni.zebra.data.dao.GadgetStateDao;
import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.dao.PublishLaunchCountDao;
import com.alleni.zebra.data.dao.SharedProjectDao;
import com.alleni.zebra.data.dto.MiniProjectListSearchResultSet;
import com.alleni.zebra.data.dto.MyProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectQuery;
import com.alleni.zebra.data.dto.TinyProjectListSearchResultSet;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.hibernate.dao.GuestListDao;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.data.model.GadgetState;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.util.UUID;
import com.alleni.zebra.gadget.ProjectType;
import com.alleni.zebra.gadget.json.impl.GadgetContentParserImpl;
import com.alleni.zebra.license.Permissions;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.security.authorization.Role;
import com.alleni.zebra.security.authorization.SecurityContextUtils;

@Service("projectReadService")
public class ProjectReadServiceImpl implements ProjectReadService {
	Logger log = Logger.getLogger(ProjectReadServiceImpl.class);

	@Autowired TransactionTemplate transactionTemplate; // for programatic transaction management

	@Autowired ProjectDao projectDao;
	@Autowired SharedProjectDao spDao;
	@Autowired GadgetStateDao gsDao;
	@Autowired PublishLaunchCountDao launchDao;
	@Autowired GuestListDao glDao;
	@Autowired AssetDao assetDao;

	public ProjectListSearchResultSet search(final ProjectQuery q, final AppUser searcher) throws DataAccessException, PermissionDeniedException {
		// only allow admin to search other users's unpublished projects
		if (Boolean.FALSE.equals(q.isPublished()) &&
				SecurityContextUtils.hasRoleCurrentUser(Role.ROLE_TYPE_SITE_ADMIN) == false){
			if (q.getOwnerAccountId() != null && searcher.getAccount().getId() != q.getOwnerAccountId() ) {
				throw new PermissionDeniedException("accountId");
			}
			if ((q.getAuthorMemberId() != null && q.getAuthorMemberId().equals(searcher.getId()) == false) || 
					(q.getAppUserId() != null &&  q.getAppUserId().equals(searcher.getId()) == false)  ) {
				throw new PermissionDeniedException("userId");
			}
		}

		return transactionTemplate.execute(new TransactionCallback<ProjectListSearchResultSet>() {
			public ProjectListSearchResultSet doInTransaction(TransactionStatus status) {
				try {
					return projectDao.search(q, searcher);
				} catch(Exception e) {
					//					status.setRollbackOnly();
					throw new DataAccessException(e, q.toString());
				}
			}
		});

	}



	@Transactional
	public Project getMinimal(String projectId, AppUser user) throws DataAccessException {
		Assert.notNull(projectId);
		Assert.notNull(user);

		ProjectQuery q = new ProjectQuery();
		q.setProjectId(projectId);
		q.setShowContent(false);
		q.setShowInitialValues(false);

		Project p = loadInternal(q, user, false, null, false);

		return p;
	}



	@Override
	@Transactional
	public Project loadMinimalByApnUuid(String apnUUID, AppUser user)
			throws DataAccessException, PermissionDeniedException {
		Assert.notNull(apnUUID);
		Assert.notNull(user);

		ProjectQuery q = new ProjectQuery();
		q.setApnUUID(apnUUID);
		q.setShowContent(false);
		q.setShowInitialValues(false);

		Project p = loadInternal(q, user, false, null, false);

		return p;
	}


	@Override
	@Transactional
	public Project loadMinimalByUrlName(String urlName, AppUser user)
			throws DataAccessException, PermissionDeniedException {
		Assert.notNull(urlName);
		Assert.notNull(user);

		ProjectQuery q = new ProjectQuery();
		q.setUrlName(urlName);
		q.setShowContent(false);
		q.setShowInitialValues(false);

		Project p = loadInternal(q, user, false, null, false);

		return p;
	}

	@Override
	@Transactional
	public Project loadMinimalByPubId(int dep_pubId, AppUser user)
			throws DataAccessException, PermissionDeniedException {
		Assert.notNull(dep_pubId);
		Assert.notNull(user);

		ProjectQuery q = new ProjectQuery();
		q.setDepPublishId(dep_pubId);
		q.setShowContent(false);
		q.setShowInitialValues(false);

		Project p = loadInternal(q, user, false, null, false);

		return p;
	}
	
	
	@Transactional
	public Project load(final String projectId, final AppUser user, final String externalUserId, final boolean authorMode)
			throws DataAccessException, PermissionDeniedException {
//		try {
//			return transactionTemplate.execute(new TransactionCallback<Project>() {
//				public Project doInTransaction(TransactionStatus status) {
//					try {
						Assert.notNull(projectId);
						Assert.notNull(user);

						ProjectQuery q = new ProjectQuery();
						q.setProjectId(projectId);
						q.setShowContent(true);
						q.setShowInitialValues(true);

						Project p = null;
						p = loadInternal(q, user, true, externalUserId, authorMode);
						if(!p.isCanRead()) {
							throw new PermissionDeniedException();
						}

						return p;
//
//					} catch (Exception e) {
//						log.debug(e);
//						return null;
//					}
//				}
//			});
//		} catch (UnexpectedRollbackException ure) {
//			log.debug(ure.getMostSpecificCause());
//			log.debug(ure.getRootCause());
//			throw new DataAccessException(ure, "");
//		}
	}

	@Transactional
	@Override
	public Project loadInSameTransaction(final String projectId, final AppUser user, final String externalUserId, final boolean authorMode)
			throws DataAccessException, PermissionDeniedException {
		try {
			Assert.notNull(projectId);
			Assert.notNull(user);

			ProjectQuery q = new ProjectQuery();
			q.setProjectId(projectId);
			q.setShowContent(true);
			q.setShowInitialValues(true);

			Project p = null;
			p = loadInternal(q, user, true, externalUserId, authorMode);
			if(!p.isCanRead()) {
				throw new PermissionDeniedException();
			}

			return p;

		} catch (Exception e) {
			log.debug(e);
			return null;
		}
	}

	public Project loadForPlayerByApnUUID(final String apnUUID, final AppUser user, final String externalUserId, final boolean authorMode) throws DataAccessException, PermissionDeniedException {
		Assert.notNull(apnUUID);
		Assert.notNull(user);

		return transactionTemplate.execute(new TransactionCallback<Project>() {
			public Project doInTransaction(TransactionStatus status) {

				boolean forLatestApn = true;
				String actualUUID = apnUUID;

				String verlockApn = UUID.extractVersionLockedApnUUID(apnUUID);
				if (verlockApn != null) {
					actualUUID = verlockApn;
					forLatestApn = false;
				}

				ProjectQuery q = new ProjectQuery();
				q.setApnUUID(actualUUID);
				q.setLatestForApnUUID(forLatestApn);

				Project p = loadInternal(q, user, true, externalUserId, authorMode);
				if(!p.isCanRead()) {
					throw new PermissionDeniedException();
				}

				// this function should only ever be called for zapps.
				//  The player should get all the gadgets it needs from loadInternal.
				assert (p.getProjectType() == ProjectType.APP);

				launchDao.incrementLaunchCount(p.getProjectId(), user, externalUserId, actualUUID);

				return p;
			}
		});
	}



	// only called by methods that use programmatic transactions
	@Transactional
	private void setPermissions(Project p, AppUser user) throws DataAccessException {
		boolean canRead = Permissions.READ.canHaz(user, p);
		if(! (p.getAuthorMemberId().equals(user.getId()) || p.getAccountId() != user.getAccountId()) // if not author/publisher
				&& canRead // and Permissions says yes...
				&& (p.isPublished() && p.isInviteOnly()) // but the zapp is published and invite-only
				&& !spDao.isGuestForProject(user, p.getProjectId()) // and they're not on the invite list
		   ) {
			canRead = false;
		}
		
		// If canRead has previously been set to false, but the user is a transferee of the project, override and set canRead to true
		if (!canRead && spDao.isTransfereeForProject(user, p.getProjectId())) {
			canRead = true;
		}
		
		p.setCanRead(canRead);
		p.setEditable(Permissions.EDIT.canHaz(user, p));
		p.setEmbedable(Permissions.EMBED.canHaz(user, p));
		p.setRepublishable(Permissions.REPUBLISH.canHaz(user, p));
		p.setCurrentUserCanShare(Permissions.SHARE.canHaz(user, p));
		p.setPublisher(user.getAccountId() == p.getAccountId());

		// DWH: I initially wrote this stanza as a one-line setX(a? b:c), but it was really hard to read.
		// The main point is to enforce guest list, and only hit the db if we need to.
		// Once the guestlistinvitee table has a fk to app_user, we can do all this in the initial query.
		// As is, it requires joining against accountmemberroles and app_user.email, which may give us one row per role
		// unless we use a max or distinct.  Ppppthhht.
		boolean viewMetadata = true;
		if (p.isInviteOnly() && p.isOwned() == false) {
			viewMetadata = spDao.isGuestForProject(user, p.getProjectId());
		}
		
		if (!viewMetadata && spDao.isTransfereeForProject(user, p.getProjectId())) {
			viewMetadata = true;
		}
		p.setViewMetadata(viewMetadata);
	}

	@Transactional
	private Project loadInternal(final ProjectQuery q, final AppUser user, final boolean getExtras, final String externalUserId, final boolean authorMode) throws DataAccessException {
		Assert.notNull(q);
		Assert.notNull(user);
		//		return transactionTemplate.execute(new TransactionCallback<Project>() {
		//
		//			// the code in this method executes in a transactional context
		//			public Project doInTransaction(TransactionStatus status) {
		try {
			Project p = null;

			ProjectListSearchResultSet srchResults = projectDao.search(q, user);
			if (srchResults.totalCount < 1) {
				throw new NotFoundException(q.toString());
			}

			p = srchResults.projects.get(0);

			setPermissions(p, user);
 
			if(getExtras) {
				ProjectQuery childQuery = new ProjectQuery();

				childQuery.setParentProjectId(p.getProjectId());
				childQuery.setShowContent(true);
				childQuery.setShowInitialValues(false);

				ProjectListSearchResultSet childrenResult = projectDao.search(childQuery, user);

				for (Project child : childrenResult.projects){
					setPermissions(child, user);
					p.setCanRead(true); //always able to read child gadgets
					
					if (p.isPublished() == false) {
						p.setEditable(true); // always able to edit non-published child gadgets
						p.setRepublishable(true);
					}
				}

				// for now only events have preview images
				if(ProjectType.EVENT.equals(p.getProjectType())) {
					p.setPreviewImage(assetDao.findPreviewByProject(p.getProjectId()));
				}
				for(Project child : childrenResult.projects) {
					if(ProjectType.EVENT.equals(child.getProjectType())) {
						child.setPreviewImage(assetDao.findPreviewByProject(child.getProjectId()));
					}
				}

				p.setChildren(childrenResult.projects);

				// Append state if found
				GadgetState state = null;
				try {
					state = gsDao.findByRootProjectAndAppUser(p.getProjectId(), user.getId(), externalUserId, authorMode);
				} catch(NotFoundException e) {
					// not a problem, we just let it go out null
					log.debug("we don't care");
				}

				p.setState(state);

				p.setAssets(parseAssets(p));
			}
			log.debug("about to return a project");
			return p;
		} catch (Exception e) {
			return null;
		}
		//			}
		//		});

	}

	@Transactional(noRollbackFor={NotFoundException.class, org.springframework.dao.EmptyResultDataAccessException.class})
	private List<Asset> parseAssets(Project p) throws NotFoundException {
		@SuppressWarnings("unused")
		Set<String> assetIds = new HashSet<String>();
		List<Asset> result = new ArrayList<Asset>();

		Object contentJson = new GadgetContentParserImpl().toJsonableObj(p.getContent());
		if(contentJson instanceof Map) {
			@SuppressWarnings("unchecked")
			Map<String, Object> jsonMap = (Map<String, Object>)contentJson;
			Object assetJson = jsonMap.get("assets");
			if(assetJson != null && assetJson instanceof List) {
				@SuppressWarnings("unchecked")
				List<Object> jsonList = (List<Object>)assetJson;
				for(Object currentValue : jsonList) {
					if(currentValue instanceof Map) {
						@SuppressWarnings("unchecked")
						Map<String, Object> jsonAssetMap = (Map<String, Object>)currentValue;
						Object idJson = jsonAssetMap.get("id");
						if(idJson != null && idJson instanceof String) {
							try {
								result.add(assetDao.findByIdNoRollback((String)idJson));
							} catch(NotFoundException nfe) {
								log.warn("Improper or missing asset [" + idJson + "] in asset list of project " + p.getProjectId(), nfe);
							}
						}
					}
				}
			}
		}
		return result;
	}


	public MiniProjectListSearchResultSet searchMiniStuff(
			final ProjectQuery q,
			final AppUser searcher
			) throws DataAccessException, PermissionDeniedException {

		return transactionTemplate.execute(new TransactionCallback<MiniProjectListSearchResultSet>() {
			public MiniProjectListSearchResultSet doInTransaction(TransactionStatus status) {
				return projectDao.searchMiniStuff(q, searcher);
			}
		});
	}

	public MyProjectListSearchResultSet searchMyStuff(
			final ProjectQuery q, 
			final AppUser searcher
			) throws DataAccessException, PermissionDeniedException {

		return transactionTemplate.execute(new TransactionCallback<MyProjectListSearchResultSet>() {
			public MyProjectListSearchResultSet doInTransaction(TransactionStatus status) {
				return projectDao.searchMyStuff(q, searcher);			}
		});
	}

	public TinyProjectListSearchResultSet searchTinyStuff(
			final ProjectQuery q, 
			final AppUser searcher
			) throws DataAccessException, PermissionDeniedException {
		return transactionTemplate.execute(new TransactionCallback<TinyProjectListSearchResultSet>() {
			public TinyProjectListSearchResultSet doInTransaction(TransactionStatus status) {
				return projectDao.searchTinyStuff(q, searcher);
			}
		});
	}

	public ProjectType getTypeForProject(final String projectId) {
		return transactionTemplate.execute(new TransactionCallback<ProjectType>() {
			public ProjectType doInTransaction(TransactionStatus status) {
				int typeId = 0;
				try {typeId = projectDao.getProjectType(projectId);}
				catch (NotFoundException safeToIgnore){}
				return ProjectType.valueOf(typeId);
			}
		});
	}


}
