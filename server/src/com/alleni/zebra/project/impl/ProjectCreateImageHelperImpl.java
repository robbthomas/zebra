package com.alleni.zebra.project.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.alleni.zebra.asset.AssetService;
import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.dao.PublishedGadgetImageDao;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Asset;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.PublishedGadgetImage;
import com.alleni.zebra.gadget.store.PublishException;
import com.alleni.zebra.icon.PublishedImageService;
import com.alleni.zebra.project.ProjectCreateImageHelper;
import com.alleni.zebra.project.ProjectCreateParameters;
import com.alleni.zebra.security.PermissionDeniedException;

@Service("projectCreateImageHelper")
public class ProjectCreateImageHelperImpl implements ProjectCreateImageHelper {
	Logger log = Logger.getLogger(ProjectCreateImageHelper.class);

	@Autowired ProjectDao projectDao;
	@Autowired PublishedGadgetImageDao pgImageDao;
	@Autowired PublishedImageService piSvc;
	@Autowired AssetService assetService;
	@Autowired TransactionTemplate transactionTemplate;

	public void processImagesAndScreenshots(final AppUser user, final ProjectCreateParameters pcp,
			final ProjectListSearchResultSet publishedHistory,
			final ProjectListSearchResultSet nonPublishedHistory) throws DataAccessException, PermissionDeniedException, PublishException {

		transactionTemplate.execute(new TransactionCallback<Object>() {

			// the code in this method executes in a new transactional context
			// because we may be updating screenshot records while modifying
			// related project records.
			// NB we probably don't need this any more, as screenshots and icons
			// are now uploaded with the project data.
			public Object doInTransaction(TransactionStatus status) {
				processIcon(user, pcp, publishedHistory, nonPublishedHistory);

				processScreenshots(user, pcp, publishedHistory, nonPublishedHistory);

				processPreviewImage(user, pcp, publishedHistory, nonPublishedHistory);
				return null;
			}
		});
	}


	private void processIcon(AppUser user, ProjectCreateParameters pcp,
			ProjectListSearchResultSet publishedHistory,
			ProjectListSearchResultSet nonPublishedHistory) throws DataAccessException, PermissionDeniedException {
		Project p = pcp.project;

		// if the user is publishing, then only use the icon and screenshots they send.
		PublishedGadgetImage latestPublishedIcon    = null; 
		PublishedGadgetImage latestNonPublishedIcon = null; 

		if (pcp.project.isPublished() == false ) {
			latestPublishedIcon    = getLatestIcon(user, publishedHistory);
			latestNonPublishedIcon = getLatestIcon(user, nonPublishedHistory);
		}

		if(pcp.icon != null) {
			if(pcp.icon.length > 0) {
				// empty non null icon will result in no icon
				piSvc.setIcon(p.getProjectId(), user, pcp.icon);
			}
		} else if (pcp.iconFromProjectId != null) {
			// disabled permissions on icons/screenshots for now
			// if we ever want to enable it, we would have to copy
			// the non-owned, non-published children of the parent.
//			Permissions.READ.checkAccess(user, projectDao.findById(pcp.iconFromProjectId, user));
			pgImageDao.copyIcon(pcp.iconFromProjectId, p.getProjectId());
		} else if (latestNonPublishedIcon != null) {
			pgImageDao.copyIcon(latestNonPublishedIcon.getProjectId(), p.getProjectId());
		} else if (latestPublishedIcon != null) {
			pgImageDao.copyIcon(latestPublishedIcon.getProjectId(), p.getProjectId());
		}


	}



	private void processScreenshots(final AppUser user, final ProjectCreateParameters pcp,
			final ProjectListSearchResultSet publishedHistory,
			final ProjectListSearchResultSet nonPublishedHistory) throws NotFoundException, PermissionDeniedException, DataAccessException {


		Project p = pcp.project;

		List<PublishedGadgetImage> latestPublishedScreenshots = null; 
		List<PublishedGadgetImage> latestNonPublishedScreenshots = null;

		if (pcp.project.isPublished() == false ) {
			latestNonPublishedScreenshots = getLatestScreenshots(user, nonPublishedHistory);
			latestPublishedScreenshots    = getLatestScreenshots(user, publishedHistory);
		}

		// collect all existing screenshots defaulting to an empty list
		Map<Integer, PublishedGadgetImage> existingScreenShots = new HashMap<Integer, PublishedGadgetImage>();
		if (pcp.screenshotsFromProjectId != null) {
			// disabled permissions on icons/screenshots for now
			// if we ever want to enable it, we would have to copy
			// the non-owned, non-published children of the parent.
//			Permissions.READ.checkAccess(user, projectDao.findById(pcp.screenshotsFromProjectId, user));
			List<PublishedGadgetImage> sfp = pgImageDao.getScreenshotsForProject(pcp.screenshotsFromProjectId);
			for(PublishedGadgetImage i : sfp) {
				existingScreenShots.put(i.getDisplayOrder(), i);
			}
		} else if (latestNonPublishedScreenshots != null) {
			for(PublishedGadgetImage i : latestNonPublishedScreenshots) {
				existingScreenShots.put(i.getDisplayOrder(), i);
			}
		} else if (latestPublishedScreenshots != null) {
			for(PublishedGadgetImage i : latestPublishedScreenshots) {
				existingScreenShots.put(i.getDisplayOrder(), i);
			}
		}

		Map<Integer, byte[]> newScreenShots = Collections.emptyMap();
		if(pcp.screenshots != null) {
			newScreenShots = pcp.screenshots;
		}

		Set<Integer> allDisplayIndexes = new TreeSet<Integer>();
		allDisplayIndexes.addAll(existingScreenShots.keySet());
		allDisplayIndexes.addAll(newScreenShots.keySet());

		for(Integer displayIndex : allDisplayIndexes) {
			// proecess all operations for a given displayIndex at once

			PublishedGadgetImage image = existingScreenShots.get(displayIndex);
			byte[] newData = newScreenShots.get(displayIndex);
			if(image != null && newData == null) {
				// we have an old one we want to copy
				// we are not subsequently trying to delete or replace the image
				// copy it over
				image.setProjectId(p.getProjectId());
				image.setId(0);
				pgImageDao.create(image);
			}
			if(newData != null && newData.length > 0) {
				// we have a new screenshot (that isn't a deletion marker)
				piSvc.addScreenshot(p.getProjectId(), displayIndex, user, newData);
			}
		}
	}



	private void processPreviewImage(AppUser user, ProjectCreateParameters pcp,
			ProjectListSearchResultSet publishedHistory,
			ProjectListSearchResultSet nonPublishedHistory) throws PublishException {

		Project p = pcp.project;

		// insert preview image as requested
		if(pcp.previewImage != null && pcp.previewImageData != null) {
			Asset previewImage = pcp.previewImage;
			previewImage.setDataSizeInBytes((long)pcp.previewImageData.length);
			previewImage.setUserId(user.getId());
			previewImage.setDeleted(false);
			previewImage.setPermissions("WORLD_READABLE");
			try {
				previewImage = assetService.createImagePreview(p.getProjectId(), previewImage, pcp.previewImageData);
			} catch (Exception e) {
				log.error(e);
				throw new PublishException(e);
			}
			p.setPreviewImage(previewImage);
		}
	}






	private List<PublishedGadgetImage> getLatestScreenshots(AppUser user, ProjectListSearchResultSet rs) {
		List<PublishedGadgetImage> screenshots = null;
		if (rs != null && rs.totalCount > 0) {
			Project prev = rs.projects.get(0);
			if (user.getAccountId().equals((prev.getAccountId()))) {
				screenshots = pgImageDao.getScreenshotsForProject(prev.getProjectId());
			}
		}
		return screenshots;
	}




	private PublishedGadgetImage getLatestIcon(AppUser user, ProjectListSearchResultSet plsResultSet) {
		PublishedGadgetImage ret = null;
		if (plsResultSet != null && plsResultSet.totalCount > 0) {
			Project p = plsResultSet.projects.get(0); // get the first one
			if (user.getAccountId().equals(p.getAccountId())){
				ret = p.getIcon();
			}
		}
		return ret;
	}


}
