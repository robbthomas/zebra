package com.alleni.zebra.project.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectQuery;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.project.ProjectCreateService;
import com.alleni.zebra.project.ProjectRenameService;

@Service("projectRenameService")
public class ProjectRenameServiceImpl implements ProjectRenameService {
	
	@Autowired ProjectDao projectDao;
	@Autowired ProjectCreateService projectCreateSvc;

	/* (non-Javadoc)
	 * @see com.alleni.zebra.project.impl.ProjectRenameService#renameAllProjectVersions(com.alleni.zebra.data.model.AppUser, com.alleni.zebra.data.model.Project, java.lang.String, boolean)
	 */
	@Transactional(readOnly = false)
	public void renameAllProjectVersions(AppUser user, Project project,
			String newName, boolean ignoreDuplicateNames) throws DataAccessException {

		if (!ignoreDuplicateNames) {
			ProjectQuery pq = new ProjectQuery();
			pq.setAuthorMemberId(user.getId());
			pq.setName(newName);
			pq.setType(project.getProjectType());
			ProjectListSearchResultSet results = projectDao.search(pq, user);
			if (results.totalCount > 0) {
				throw new UniqueConstraintException(project.getProjectId(), "projectName");
			}
		}
		projectDao.renameAllProjectVersions(user.getId(), project.getProjectId(), newName);
	}


}
