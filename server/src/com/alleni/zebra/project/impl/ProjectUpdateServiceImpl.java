package com.alleni.zebra.project.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.AccountProjectNumberDao;
import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AccountProjectNumber;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.gadget.store.ProjectSharedException;
import com.alleni.zebra.license.Permissions;
import com.alleni.zebra.project.ProjectCreateService;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.project.ProjectUpdateService;
import com.alleni.zebra.security.PermissionDeniedException;

@Service("projectUpdateService")
public class ProjectUpdateServiceImpl implements ProjectUpdateService {
	Logger log = Logger.getLogger(ProjectUpdateServiceImpl.class);

	@Autowired ProjectDao projectDao;
	@Autowired ProjectReadService projectReadSvc;
	@Autowired ProjectCreateService projectCreateSvc;
	@Autowired AccountProjectNumberDao apnDao;


	@Transactional(readOnly = false)
	public void delete(Project project, AppUser deleter)
			throws PermissionDeniedException, DataAccessException {
		Permissions.EDIT.checkAccess(deleter, project);
		projectDao.retireAllVersions(deleter, project);
	}

	@Transactional(readOnly = false)
	public void removeFromCollection(String userAwarePurchaseId, AppUser user)
			throws PermissionDeniedException, DataAccessException, ProjectSharedException {
		AccountProjectNumber ppp = apnDao.findByUuid(userAwarePurchaseId);

		if(ppp.getAccountId().longValue() != user.getAccountId().longValue()) {
			throw new PermissionDeniedException("User's account is not the owner account");
		}

		// if the current user is also the publisher of this gadget,
		// then unpublish as well, otherwise there will be no way for anonymous
		// users to list this gadget (no row in AccountProjectNumbers(publishid, publisherId)
		Project p = projectDao.findById(ppp.getProjectId(), user);
		if ( p.getAccountId() == user.getAccountId() ) {
			unpublish(p, user);
		}
		apnDao.delete(ppp, user);
	}

	@Transactional(readOnly = false)
	public void unpublish(Project project, AppUser user)
			throws PermissionDeniedException, DataAccessException, ProjectSharedException {
		Permissions.EDIT.checkAccess(user, project);
		if (project.getShareTypes()!= null && project.getShareTypes().size() > 0) {
			throw new ProjectSharedException();
		}
		project.setNextPublishedVersionId(project.getProjectId());
		projectDao.update(project);
	}

	@Override
	@Transactional(readOnly = false)
	public AccountProjectNumber upgrade(AppUser user, String urlName)
			throws PermissionDeniedException, DataAccessException {
		Project latest = projectReadSvc.loadMinimalByUrlName(urlName, user);
		AccountProjectNumber userApn = apnDao.findForUserAndUrlName(user, urlName);

		apnDao.updateProject(user, userApn.getUuid(), latest.getProjectId());
		return userApn;
	}

}
