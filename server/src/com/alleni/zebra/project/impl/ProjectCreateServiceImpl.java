package com.alleni.zebra.project.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.jets3t.service.S3ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

import com.alleni.zebra.account.AccountType;
import com.alleni.zebra.apn.AccountProjectNumberService;
import com.alleni.zebra.data.dao.AccountProjectNumberDao;
import com.alleni.zebra.data.dao.GadgetStateDao;
import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.dao.SharedProjectDao;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectQuery;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.PublishedNameInUseByOtherUserException;
import com.alleni.zebra.data.exception.UniqueConstraintException;
import com.alleni.zebra.data.hibernate.dao.GuestListDao;
import com.alleni.zebra.data.hibernate.model.GuestList;
import com.alleni.zebra.data.model.AccountProjectNumber;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.GadgetState;
import com.alleni.zebra.data.model.LicenseType;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.util.HibernateUtils;
import com.alleni.zebra.gadget.ProjectType;
import com.alleni.zebra.gadget.store.IllegalNameException;
import com.alleni.zebra.gadget.store.PublishException;
import com.alleni.zebra.gadget.store.impl.PublishPriceException;
import com.alleni.zebra.license.Permissions;
import com.alleni.zebra.project.ProjectCreateImageHelper;
import com.alleni.zebra.project.ProjectCreateParameters;
import com.alleni.zebra.project.ProjectCreateService;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.project.ProjectRenameService;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.util.StringUtils;
import com.alleni.zebra.web.util.WebContextUtils;

@Service("projectCreateService")
public class ProjectCreateServiceImpl implements ProjectCreateService {
	private static final String[] RESERVED_WORDS = new String[] { "undefined", "null", "false", "true", "?", "NaN", "nan", "$", "#"};

	Logger log = Logger.getLogger(ProjectCreateServiceImpl.class);

	@Autowired ProjectDao projectDao;
	@Autowired ProjectReadService projectReadSvc;
	@Autowired SharedProjectDao sharedProjectDao;
	@Autowired GuestListDao guestListDao;
	@Autowired AccountProjectNumberDao apnDao;
	@Autowired AccountProjectNumberService apnSvc;
	@Autowired GadgetStateDao gsDao;
	@Autowired ProjectRenameService pRenameSvc;
	@Autowired HibernateUtils hibernateUtils;
	@Autowired WebContextUtils wcUtils;
	@Autowired ProjectCreateImageHelper pciHelper;
	@Autowired TransactionTemplate transactionTemplate;

	private static class ProjectHistorySet {
		public ProjectListSearchResultSet nonPublished = null;
		public ProjectListSearchResultSet published = null;
	}

	/**
	 * Main method used to create or update or publish any project or gadget
	 * 
	 * Step 1: modify the incoming record to be suitable for insertion and not trusting the client
	 * where we don't have to. (Filling in fields not given by client)
	 *    This step may throw exceptions
	 *    
	 *         * Step 1.1:  reject reserved words
	 *
	 * Step 2: Check permissions for inserting (if this would save over an existing record of the same name)
	 *
	 * Step 3: Actually insert the new project record
	 *
	 *        * Step 3.1 Perform rename if oldName != currentName
	 *        
	 * Step 4: Insert dependent rows (other tables with project foreign key)
	 *
	 * Step 5: Term out rows we are overwriting (rows found in step 2)
	 *
	 * @param user The creating/author user
	 * @param req parameters detailing how the insertion should happen (ex what to copy from previous versions)
	 * @return
	 * @throws DataAccessException
	 * @throws PermissionDeniedException
	 * @throws PublishException
	 */
	@Transactional(readOnly = false)
	public Project create(final AppUser user, final ProjectCreateParameters req )
			throws DataAccessException, PermissionDeniedException, PublishException {
		Assert.notNull(req);
		Assert.notNull(user);

		augmentProjectForInsertion(user, req);

		rejectReservedWords(user, req);

		final ProjectHistorySet history = checkCreatePermissions(user, req);

		req.project = projectDao.create(req.project);
		
		try {
			insertDependentRows(user, req, history);
		} catch (Exception e) {
			throw new PublishException(e);
		}

		termOverwrittenRows(user, req, history);

		performRenameIfNecessary(user, req.project);

		return req.project;
	}


	@Transactional
	private void performRenameIfNecessary(AppUser user, Project project) throws DataAccessException {
		String oldName = project.getOldName();
		String newName = project.getProjectName();

		if (oldName != null &&
				newName.equals(oldName) == false) {
			// link head of oldName to current head project.
			Project old = null;
			ProjectQuery pq = new ProjectQuery();
			pq.setAuthorMemberId(user.getId());
			pq.setName(oldName);
			pq.setType(project.getProjectType());
			ProjectListSearchResultSet results = projectDao.search(pq, user);
			if (results.totalCount >0 ) {
				old = results.projects.get(0);
				old.setNextVersionId(project.getProjectId());
				if (project.isPublished()) {
					old.setNextPublishedVersionId(project.getProjectId());
				}
				projectDao.update(old);

				// if there is no project with the oldName, then we don't need to rename it, do we?
				// Set ignoreDuplicateNames to true because we would have failed by now when saving the new project.
				// pass in old, because the new one already has the new name.
				pRenameSvc.renameAllProjectVersions(user, old, newName, true);
			}

		}
	}


	private void rejectReservedWords(AppUser user, ProjectCreateParameters req) throws IllegalNameException {
		String[] names = new String[] {
				req.project.getProjectName(),
				req.project.getPublishedName(),
				req.project.getUrlName()
		};
		for (String w : RESERVED_WORDS) {
			for (String n : names) {
				if (w.equalsIgnoreCase(n) || (n != null && n.indexOf(w) != -1)) {
					throw new IllegalNameException(req.project.getProjectName() + " is not a valid name.");
				}
			}
		}
	}

	/**
	 * Step 1: modify the incoming record to be suitable for insertion and not trusting the client
	 * where we don't have to
	 */
	@Transactional
	private void augmentProjectForInsertion(final AppUser user, final ProjectCreateParameters req) throws DataAccessException, PermissionDeniedException {
		Project p = req.project;

		// regardless of what is passed in, the current user is the author now
		p.setAuthorFirstName(user.getFirstName());
		p.setAuthorLastName(user.getLastName());
		p.setAuthorMemberId(user.getId());
		if (p.getAccountId() == 0) {
			p.setAccountId(user.getAccountId());
		}

		// url name is created from the published name
		if (p.getPublishedName() != null) {
			p.setUrlName(StringUtils.wikiUrlEncode(p.getPublishedName()));
		}

		if(req.contentFromProjectId != null) {
			//TODO convert this to just have the create query join for data
			// so this isn't all loaded into java unnecessarily
			Project contentProject = projectReadSvc.loadInSameTransaction(req.contentFromProjectId, user, null, true);
			p.setContent(contentProject.getContent());
			p.setGadgetSize(contentProject.getGadgetSize());
			p.setInitialValues(contentProject.getInitialValues());
			p.setState(contentProject.getState());
			List<String> childrenIds = new ArrayList<String>(contentProject.getChildren().size());
			for(Project childProject : contentProject.getChildren()) {
				childrenIds.add(childProject.getProjectId());
			}
			p.setChildrenIds(childrenIds);
		} else if (p.getContent() != null) {
			p.setGadgetSize(p.getContent().length());
		}


		if (p.getLicenseTypeId() != null && p.getLicenseTypeId() < 1) {
			p.setLicenseTypeId(-1);
		} else {
			@SuppressWarnings("deprecation")
			LicenseType lt = p.getLicenseType();
			if (lt != null) {
				p.setLicenseTypeId(lt.id);
			}
		}

		if(p.isPublished()) {
			// calculate minPrice and selfPrice
			int royalties = projectDao.calcRoyalties(p.getProjectId());
			int minPrice = (royalties == 0) ? 25 : royalties;
			p.setMinPrice(minPrice);
			p.setSelfPrice(Math.max(0, p.getPrice() - minPrice)); // self-price can never be negative
		}
	}


	/**
	 * Step 2: Check permissions for inserting (if this would save over an existing record of the same name)
	 */
	@Transactional
	private ProjectHistorySet checkCreatePermissions(AppUser user, ProjectCreateParameters req) throws DataAccessException, PermissionDeniedException, PublishPriceException {
		Project p = req.project;
		ProjectHistorySet history = new ProjectHistorySet();

		// record what projects we are overwriting for use after actual project row creation
		// also check what we are overwriting to see if we are allowed
		//		String oldName = p.getOldName(); // will be null for initial save
		if(p.getProjectType() == ProjectType.APP ) {
			ProjectQuery historyQuery = new ProjectQuery();
			historyQuery.setType(p.getProjectType());
			historyQuery.setProjectName(p.getProjectName());
			historyQuery.setSize(1); 
			historyQuery.setSortColumn(ProjectQuery.SortColumn.CREATED_DATE);
			historyQuery.setOrder(ProjectQuery.SortOrder.DESCENDING);
			historyQuery.setHistory(true);
			historyQuery.setRetired(false); 
			historyQuery.setAuthorMemberId(user.getId());
			history.nonPublished = projectDao.search(historyQuery, user);

			if (history.nonPublished.totalCount > 0) {
				Project orig = history.nonPublished.projects.get(0);
				if (false == req.ignoreDuplicateNames) {
					log.debug("project is not unique: " + p.getProjectName());
					// by default this condition will be false.  This prompts the editor to
					// present a dialog saying "you really want to overwrite...?" If the user says yes,
					// then the editor will pass ignoreDuplicateName=true.
					throw new UniqueConstraintException(p.getProjectName(), "projectName");
				} else {
					Permissions.EDIT.checkAccess(user, orig);
				}
			}
		}


		if (p.isPublished()) {
			// record what published projects we are overwriting for use after actual project row creation
			// also check what we are overwriting to see if we are allowed
			// we search by url name because this is an even more strict search than published name
			// as many candidate published names may share a url name, but not the other way around
			ProjectQuery historyQuery = new ProjectQuery();
			//historyQuery.setType(p.getProjectType());
			historyQuery.setPublished(true);
			historyQuery.setUrlName(p.getUrlName());
			historyQuery.setHistory(true);
			historyQuery.setRetired(false);
			historyQuery.setSize(1);
			history.published = projectDao.search(historyQuery, null); // don't pass in 'searcher': we do that check ourselves.

			if(history.published.totalCount > 0) {
				Project orig = history.published.projects.get(0);
				if (orig.getAccountId() != user.getAccountId()) {
					// check account first because there is no need
					// to ask the user if they want to replace if they
					// wont be able to anyway

					// Save-over is only allowed for the original publishing account.
					// It's still technically a duplicate-name exception, even though
					// it could be interpreted as deserving a permission denied exception.
					// The editor interprets a UCE w/ no ID as a non-overrideable condition.
					throw new PublishedNameInUseByOtherUserException("publishedName");
				} else if (false == req.ignoreDuplicatePublishedNames) {
					log.debug("published project is not unique: " + p.getProjectId());
					// by default this condition will be false.  This prompts the editor to
					// present a dialog saying "you really want to overwrite...?" If the user says yes,
					// then the editor will pass ignoreDuplicatePublishedName=true.
					// Here, we throw the exception for the default case (for publisher == orig.publisher):
					throw new UniqueConstraintException(p.getProjectId(), "publishedName");
				} else {
					//TODO check (not yet existing) publish over permission instead?
					Permissions.EDIT.checkAccess(user, orig);
				}
			}
		}


		// there are some extra checks only performed for published entries
		if(p.isPublished()) {

			// check that we are allowed to republish any listed children
			if (p.getChildrenIds() != null) {
				List<Project> children = projectDao.getChildrenFor(p, user);
				for (Project child : children) {
					if (child.getProjectType() != ProjectType.EVENT) {
						Permissions.REPUBLISH.checkAccess(user, child);
					}
				}
			}

			// invite only is a pro feature
			if (p.isInviteOnly() && user.getAccount().getAccountType() != AccountType.PRO) {
				throw new PermissionDeniedException("InviteOnly can only be set by PRO users.");
			}

			// price must be equal to or larger than the minimum if it is published in the store
			if (!p.isHideInStoreList() && (p.getPrice() < p.getMinPrice()) && (p.getMinPrice() != 25 || p.getPrice() != 0)) {
				throw new PublishPriceException( (long)p.getMinPrice() );
			}
		}

		// finally, a couple of sanity checks
		if (p.getProjectType() != null && p.getProjectType() != ProjectType.EVENT) {
			int at = user.getAccount().getAccountType().typeId;
			if (p.getProjectType().id > at) {
				throw new PermissionDeniedException();
			}
		}
		return history;
	}

	/**
	 * TODO: make asynch
	 * Step 4: Insert dependent rows (other tables with project foreign key)
	 * @throws PublishException 
	 */
	@Transactional
	private void insertDependentRows(AppUser user, ProjectCreateParameters req, ProjectHistorySet history) throws DataAccessException, PermissionDeniedException, IOException, S3ServiceException, PublishException {
		Project p = req.project;

		if(p.getState() != null) {
			GadgetState state = p.getState();
			// states belonging to a project save are always authormode
			// and belong to the project author with no external user
			state.setId(null);
			state.setAppUserId(user.getId());
			state.setExternalUserId(null);
			state.setAuthorMode(true);
			state.setRootProjectId(p.getProjectId());
			gsDao.create(state);
		}

		// add children rows (we already tested existence and permission)
		if (p.getChildren() != null && p.getChildrenIds() == null) {
			p.setChildrenIds(extractIds(p.getChildren()));
		}
		if (p.getChildrenIds() != null) {
			for (String childId : p.getChildrenIds()) {
				projectDao.addToChildren(p, childId, user);
			}
		}

		pciHelper.processImagesAndScreenshots(user, req, history.published, history.nonPublished);

		if(p.isPublished()) {
			AccountProjectNumber ppp = apnSvc.createAccountProjectNumberForPublishOrUpdate(p, user);
			p.setApnUUID(ppp.getUuid());
			p.setOwned(true);

			// set up a default guestlist for this project
			// and update the current project record with the guestlistid
			String defaultEmailText = "";
			GuestList gl = new GuestList();
			gl.setEmailtext(defaultEmailText);
			gl.setUpdateddatetime(new Date());
			guestListDao.persist(gl);
			hibernateUtils.flush();
			p.setGuestListId(new Long(gl.getGuestlistid()));
			projectDao.update(p);
		}
	}


	// this could be a one or two-line lambda.
	private List<String> extractIds(List<Project> children) {
		List<String> ids = new ArrayList<String>();
		for (Project p : children) {
			ids.add(p.getProjectId());
		}

		return ids;
	}


	/**
	 * Step 5: Term out rows we are overwriting
	 */
	@Transactional
	private void termOverwrittenRows(final AppUser user, final ProjectCreateParameters req, final ProjectHistorySet history) throws DataAccessException {
		final Project p = req.project;
		
		projectDao.setNextVersionIdTo(p);
		if(p.isPublished()) {
			apnDao.unPurchaseOldVersionsForAuthor(p);
			projectDao.setNextPublishedVersionIdTo(p);
		}

		// now that there will be no collisions in the unique name index we can mark this as the latest version
		p.setNextVersionId(null);
		p.setNextPublishedVersionId(null);
		projectDao.update(p);

	}
}
