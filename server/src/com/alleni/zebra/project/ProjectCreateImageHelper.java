package com.alleni.zebra.project;

import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.gadget.store.PublishException;
import com.alleni.zebra.security.PermissionDeniedException;

public interface ProjectCreateImageHelper {
	
	public void processImagesAndScreenshots(AppUser user, ProjectCreateParameters pcp, ProjectListSearchResultSet publishedHistory, ProjectListSearchResultSet nonPublishedHistory) throws DataAccessException, PermissionDeniedException, PublishException;
}
