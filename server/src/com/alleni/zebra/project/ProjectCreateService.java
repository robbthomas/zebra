package com.alleni.zebra.project;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.gadget.store.PublishException;
import com.alleni.zebra.security.PermissionDeniedException;

public interface ProjectCreateService {

	/**
	 * Create a new Project row in the database.
	 * 
	 * @param project
	 * @param user
	 * @return
	 * @throws DataAccessException
	 * @throws PermissionDeniedException 
	 */
	public Project create(AppUser user, ProjectCreateParameters request)
            throws DataAccessException, PermissionDeniedException, PublishException;
	
	


}
