package com.alleni.zebra.project;

import com.alleni.zebra.data.dto.MiniProjectListSearchResultSet;
import com.alleni.zebra.data.dto.MyProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectListSearchResultSet;
import com.alleni.zebra.data.dto.ProjectQuery;
import com.alleni.zebra.data.dto.TinyProjectListSearchResultSet;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.gadget.ProjectType;
import com.alleni.zebra.security.PermissionDeniedException;

public interface ProjectReadService {
	
	/**
	 * Find a Project record by id; populate children and icons.  If published, check all appropriate access.
	 * 
	 * @param projectId
	 * @param user
	 * @return the Project
	 * @throws DataAccessException when permission denied for user, or not found.
	 * @throws PermissionDeniedException 
	 */
	public Project load(String projectId, AppUser user, String externalUserId, boolean authorMode) throws DataAccessException, PermissionDeniedException;

    public Project getMinimal(String projectId, AppUser user) throws DataAccessException;
	
	/**
	 * Similar to {@link #search(ProjectQuery)}, but for another user's projects.
	 * 
	 * @param q
	 * @param querier
	 * @return
	 * @throws DataAccessException
	 * @throws PermissionDeniedException 
	 */
	public ProjectListSearchResultSet search(ProjectQuery q, AppUser searcher) throws DataAccessException, PermissionDeniedException;

	public MiniProjectListSearchResultSet searchMiniStuff(ProjectQuery q, AppUser searcher) throws DataAccessException, PermissionDeniedException;
	
	public MyProjectListSearchResultSet searchMyStuff(ProjectQuery q, AppUser searcher) throws DataAccessException, PermissionDeniedException;
	
	public TinyProjectListSearchResultSet searchTinyStuff(ProjectQuery q, AppUser searcher) throws DataAccessException, PermissionDeniedException;
	
	Project loadForPlayerByApnUUID(String apnUUID, AppUser user, String externalUserId, boolean authorMode)
			throws DataAccessException, PermissionDeniedException;

	public Project loadMinimalByApnUuid(String apnUUID, AppUser user) throws DataAccessException, PermissionDeniedException;

	public Project loadMinimalByUrlName(String urlName, AppUser user) throws DataAccessException, PermissionDeniedException;

	public Project loadMinimalByPubId(int dep_pubId, AppUser user) throws DataAccessException, PermissionDeniedException;
	
	public ProjectType getTypeForProject(String projectId);

	Project loadInSameTransaction(String projectId, AppUser user,
			String externalUserId, boolean authorMode)
			throws DataAccessException, PermissionDeniedException;

	
	

}
