package com.alleni.zebra.project;

import com.alleni.zebra.data.exception.ConcurrencyException;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AccountProjectNumber;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.gadget.store.ProjectSharedException;
import com.alleni.zebra.security.PermissionDeniedException;

public interface ProjectUpdateService {

	
	
	
	
	// -------------------------------
	//     "update" methods
	// -------------------------------
	
	public void delete(Project project, AppUser deleter) throws NotFoundException, ConcurrencyException, PermissionDeniedException, DataAccessException; 


	/**
	 * Unpurchase this item, if I am the one that owns it.
	 * 
	 * NOTE: only used in one controller method.
	 * 
	 * @param userAwarePurchaseId
	 * @param user
	 * @throws PermissionDeniedException
	 * @throws DataAccessException
	 * @throws ProjectSharedException 
	 */
	public void removeFromCollection(String userAwarePurchaseId, AppUser user) throws PermissionDeniedException, DataAccessException, ProjectSharedException;

	
	
	/**
	 * Remove a Project from the store.
	 * 
	 * @param projectId
	 * @param user
	 * @throws PermissionDeniedException
	 * @throws DataAccessException
	 * @throws ProjectSharedException 
	 */
	public void unpublish(Project project, AppUser user) throws PermissionDeniedException, DataAccessException, ProjectSharedException;


	public AccountProjectNumber upgrade(AppUser user, String apnUUID) throws PermissionDeniedException, DataAccessException;

}
