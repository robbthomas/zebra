package com.alleni.zebra.util;

import java.io.*;
import java.util.Map;

public class StreamUtils {

    public static final int KEYWORD_SYMBOL = '$';
    public static final int KEYWORD_BEGIN = '{';
    public static final String KEYWORD_BEGIN_SEQ = "${";
    public static final int KEYWORD_END = '}';
    public static final int KEYWORD_MAX_LENGTH = 15;

    public static boolean validKeyCharacter(int keyLength, int ch) {
        return keyLength < KEYWORD_MAX_LENGTH
                && ch != KEYWORD_SYMBOL
                && (
                        (keyLength == 0 && Character.isJavaIdentifierStart(ch))
                                || (keyLength > 0 && Character.isJavaIdentifierPart(ch))
                );
    }

    public static void pipeReplace(InputStream in, OutputStream out, Map<String, String> replacements) throws IOException {
        PushbackReader r = new PushbackReader(new InputStreamReader(in));
        Writer w = new OutputStreamWriter(out);
        int ch;
        mainLoop: while((ch = r.read()) != -1) {
            if(ch == KEYWORD_SYMBOL) {
                // check to see if this is the beginning of a replacement keyword
                ch = r.read();
                if(ch == KEYWORD_BEGIN) {
                    // looks like it is the beginning
                    // store up the keyword until the end flag
                    StringWriter sw = new StringWriter();
                    int keyLength = 0;
                    while(true) {
                        ch = r.read();
                        if(ch == KEYWORD_END) {
                            // end of keyword continue down below after loop
                            break;
                        } else if(validKeyCharacter(keyLength, ch)) {
                            // next letter of keyword
                            sw.write(ch);
                            keyLength++;
                        } else {
                            // invalid character
                            r.unread(ch);
                            w.write(KEYWORD_BEGIN_SEQ);
                            w.write(sw.toString());
                            continue mainLoop;
                        }
                    }
                    // process keyword
                    String keyword = sw.toString();
                    if(replacements.containsKey(keyword)) {
                        // valid replacement, this is the actual goal of the whole function
                        w.write(replacements.get(keyword));
                    } else {
                        // not an actual keyword so print out the characters as we read them
                        w.write(KEYWORD_BEGIN_SEQ);
                        w.write(keyword);
                        w.write(KEYWORD_END);
                    }
                } else {
                    // false alarm, put it back and continue as before
                    r.unread(ch);
                    w.write(KEYWORD_SYMBOL);
                }
            } else {
                // normal character read
                w.write(ch);
            }
        }
        w.flush();
    }
}
