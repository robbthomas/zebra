package com.alleni.zebra.util;

import java.sql.Date;
import java.sql.Timestamp;


public class DateUtils {
	public static java.sql.Date toSqlDate( java.util.Date utilDate) {
		return utilDate == null? null : new Date(utilDate.getTime());
	}
	

	public static java.util.Date toUtilDate(java.sql.Timestamp time) {
		return time == null? null : new java.util.Date(time.getTime());
	}
	
	public static java.util.Date toUtilDate(java.sql.Date sqlDate) {
		return sqlDate == null? null : new java.util.Date(sqlDate.getTime());
	}
	
	public static Timestamp utilDate2Timestamp(java.util.Date utilDate) {
		return utilDate == null? null : new Timestamp(utilDate.getTime());
	}
}
