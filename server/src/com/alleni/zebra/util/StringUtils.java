package com.alleni.zebra.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;


public class StringUtils {
	public static final String ENCODING_UTF8 = "UTF-8";

	/**
	 * Delegates to apache commons-codec Base64.  Using instead this
	 * makes some controller code more readable.
	 * 
	 * @param txt64 base64-encoded text
	 * @return plain-text String
	 */
	public static final String fromBase64(String txt64) {
    	Base64 codec = new Base64();
    	return new String(codec.decode(txt64));
	}
	
	/**
	 * WARNING:  Java regex is *slow*, so avoid this
	 *           for large Strings.
	 * 
	 * @param s
	 * @param c
	 * @return the input String, minus any instances of the char to remove.
	 */
	public static String removeChar(String s, char c) {
		Assert.notNull(s, "Can't remove characters from a null String");
		
		String remove = String.valueOf(c);
		String ret = s.replaceAll(remove, "");
			
		return ret;
	}
	
	
	public static String wikiUrlEncode(String s) {
		if (s != null) {
			return s.replaceAll("\\s+", "_").replaceAll("\\$", "S").replaceAll("#", "H").replaceAll("[^\\w\\-]", "");
		} else {
			return null;
		}
	}
	
	public static boolean isNumeric(String s) {
		if (s != null)
			return s.matches("\\d+");
		else
			return false;
	}
	
	public static boolean nullOrEmpty(String s) {
		if (s == null) return true;
		if (s.isEmpty()) return true;
		return false;
	}

    public static String escapeXml(String s) {
        return s.replaceAll("&","&amp;").replaceAll("'","&apos;").replaceAll("\"","&quot;").replaceAll("<","&lt;").replaceAll(">","&gt;");
    }

	@SuppressWarnings("unchecked")
	public static Map<String, Object> toJsonableObj(String metaData) {
		Map<String, Object> ret = new HashMap<String, Object>();

		ObjectMapper mapper = new ObjectMapper();

		try {
			if (metaData != null) {
				ret = mapper.readValue(metaData, HashMap.class);
			}
		} catch (Exception e) {
			// for now...
			e.printStackTrace();
//			log.error(e);
//			log.error("choked on gadget content: " + metaData);
		}

		return ret;
	}

	public static String toJsonString(Map<String, Object> map) {
		String json = null;

		ObjectMapper mapper = new ObjectMapper();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			mapper.writeValue(baos, map);
			json = baos.toString("UTF-8");
		} catch (Exception e) {
//			log.error("Error re-jsonifying Gadget.content!");
			e.printStackTrace();
		}

		return json;
	}
	
}
