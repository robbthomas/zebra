package com.alleni.zebra.gadget;

public class GadgetSaveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2987050574711433262L;

	public GadgetSaveException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GadgetSaveException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public GadgetSaveException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public GadgetSaveException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
