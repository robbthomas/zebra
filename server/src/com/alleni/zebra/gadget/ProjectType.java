package com.alleni.zebra.gadget;

/**
 * Replaces GadgetType
 */
public enum ProjectType {
	GADGET(1, "project_type_gadget"),
	APP(2, "project_type_app"),
	EVENT(3, "project_type_event");

	public final int id; // live dangerously:  tied to db id
	public final String key;

	private ProjectType(int id, String key) {
		this.id = id;
		this.key = key;
	}
	
	public static ProjectType valueOf(int typeCode) {
		for (ProjectType type : values())
			if (type.id == typeCode)
				return type;
		
		return null;
	}
}
