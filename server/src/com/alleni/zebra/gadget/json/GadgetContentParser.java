package com.alleni.zebra.gadget.json;

import java.util.Map;

/**
 * Contains methods for manipulating Gadget.content, which is a big JSON String. 
 *
 */
public interface GadgetContentParser {
	
	
	/**
	 * Turns the Gadgdet.content String generated below, into a HashMap that can be
	 * incorporated into the JSON that the Controller returns.
	 * 
	 * @param gadgetData
	 * @return
	 */
	public Object toJsonableObj(String gadgetData);
	
	
	/**
	 * Gadget.content is a giant JSON String; when we decode the Gadget JSON sent by the
	 * editor, this also gets parsed out.  This is where we "un-parse" it into something we can
	 * store in the database.
	 * 
	 * @param map
	 * @return
	 */
	public String toJsonString(Map<String, Object> map);
}

/*
 * This is what we get back from the client
 * 
 * public function toJson():Object
  {
   var result:Object = {
    sharable: sharable,
    price: price,
    description: description,
    name: name,
    publishedVersion: publishedVersion,
    category: category,
    type: type,
    tags: tags,
    permissions: permissions
   };
   if (gadgetID)
    result.id = gadgetID;
   if (content)
    result.content = content;
   return result;
  }
  */
