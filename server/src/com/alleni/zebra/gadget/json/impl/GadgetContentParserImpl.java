package com.alleni.zebra.gadget.json.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import com.alleni.zebra.gadget.json.GadgetContentParser;

@Service("gadgetContentParser")
public class GadgetContentParserImpl implements GadgetContentParser {
	private final Logger log = Logger.getLogger(this.getClass());

	
	
	@SuppressWarnings("unchecked")
	@Override
	public Object toJsonableObj(String gadgetData) {
		Map<String, Object> ret = new HashMap<String, Object>();
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			if (gadgetData != null) {
				ret = mapper.readValue(gadgetData, HashMap.class);
			}
		} catch (Exception e) {
			// for now...
			log.error(e);
			log.error("choked on gadget content: " + gadgetData);
		}
		
		return ret;
	}




	@Override
	public String toJsonString(Map<String, Object> map) {
		String json = null;
		
		ObjectMapper mapper = new ObjectMapper();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			mapper.writeValue(baos, map);
			json = baos.toString("UTF-8");
		} catch (Exception e) {
			log.error("Error re-jsonifying Gadget.content!");
			e.printStackTrace();
		}
		
		return json;
	}

}
