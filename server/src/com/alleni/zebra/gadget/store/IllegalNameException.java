package com.alleni.zebra.gadget.store;

public class IllegalNameException extends PublishException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6429188932419626822L;

	public IllegalNameException() {super();}
	
	public IllegalNameException(String message) {
		super(message);
	}
}
