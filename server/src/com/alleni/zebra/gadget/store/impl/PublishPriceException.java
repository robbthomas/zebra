package com.alleni.zebra.gadget.store.impl;

import com.alleni.zebra.gadget.store.PublishException;

public class PublishPriceException extends PublishException {

	private static final long serialVersionUID = 1L;

	private long minPrice;
	
	public PublishPriceException() {
		super();
	}

	public PublishPriceException(Long minPrice) {
		this.minPrice = minPrice;
	}
	public PublishPriceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public PublishPriceException(String arg0) {
		super(arg0);
	}

	public PublishPriceException(Throwable arg0) {
		super(arg0);
	}

	public long getMinPrice() {
		return minPrice;
	}

}
