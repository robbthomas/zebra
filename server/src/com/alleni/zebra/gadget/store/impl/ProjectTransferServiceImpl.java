package com.alleni.zebra.gadget.store.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.account.AppUserService;
import com.alleni.zebra.commerce.productcode.ProductCodeService;
import com.alleni.zebra.data.dao.AccountDao;
import com.alleni.zebra.data.dao.AccountProjectNumberDao;
import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.dao.ProjectTransferDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.hibernate.dao.GuestListInviteeDao;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.ProjectTransfer;
import com.alleni.zebra.gadget.store.ProjectTransferService;
import com.alleni.zebra.gadget.store.PublishException;
import com.alleni.zebra.project.ProjectCreateService;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.security.authorization.Role;
import com.alleni.zebra.security.authorization.SecurityContextUtils;
import com.alleni.zebra.soy.ClosureUtil;
import com.alleni.zebra.system.mail.Mail;

@Service("projectTransferService")
public class ProjectTransferServiceImpl implements ProjectTransferService {
	Logger log = Logger.getLogger(ProjectTransferServiceImpl.class);

	@Autowired ProjectTransferDao ptDao;
	@Autowired Executor executor;
	@Autowired AppUserService userSvc;
	@Autowired AccountDao accountDao;
	@Autowired ProjectReadService projectReadSvc;
	@Autowired ProjectCreateService projectCreateSvc;
	@Autowired AccountProjectNumberDao apnDao;
	@Autowired ProjectDao projectDao;
	@Autowired ProductCodeService pcSvc;
	@Autowired GuestListInviteeDao guestListInviteeDao;
	@Autowired Mail mailSvc;

	/*
	 * @see com.alleni.zebra.gadget.store.ProjectTransferService#listIncoming(com.alleni.zebra.data.model.AppUser)
	 */
	@Transactional(readOnly = false)
	public List<ProjectTransfer> listIncoming(AppUser user) {
		List<ProjectTransfer> ret = ptDao.list(user, true);
		// asynchronously mark each list member as viewed.
		executor.execute(new ProjectTransferUpdateWorker(ptDao, ret, user));
		return ret;
	}

	@Transactional
	public Response create(List<Request> xferRequests, AppUser sender) throws NotFoundException {

		List<Success> success  = new ArrayList<Success>();
		List<Failure> failures = new ArrayList<Failure>();

		Map<String, Set<Request>> emailToRequestMap = sortRequestsByRecipient(xferRequests);

		for(String email : emailToRequestMap.keySet()) {
			AppUser recip = userSvc.findByEmail(email);
			Set<Request> sortedRequests = emailToRequestMap.get(email);
			List<ProjectTransfer> xfers = new ArrayList<ProjectTransfer>(); // for generating emails

			for (Request request : sortedRequests) {
				try {
					ProjectTransfer xfer = create(request.projectId, sender, recip);
					success.add(new Success(request, xfer));
					xfers.add(xfer);
				}
				catch(Exception e) {
					failures.add(new Failure(request, e));
					e.printStackTrace();
					log.error(e);
				}
			}

			sendEmail(sender, email, xfers);
		}

		return 	new Response(success, failures);
	}


	/*
	 * Takes an unsorted list of email/projectId pairs
	 * Returns a map of email:list<projectid>
	 */
	private Map<String, Set<Request>> sortRequestsByRecipient(List<Request> xferRequests) {
		Map<String, Set<Request>> emailToProjectListMap = new HashMap<String, Set<Request>>();

		for (Request request : xferRequests) {
			String email = request.email;
			Set<Request> projects = emailToProjectListMap.get(email);
			if (projects == null) {
				projects = new HashSet<Request>();
				emailToProjectListMap.put(email, projects);
			}
			projects.add(request);
		}

		return emailToProjectListMap;
	}


	private void sendEmail(AppUser sender, String toEmail, List<ProjectTransfer> xfers) {
		int totalPrice = 0;
		List<String> projects = new ArrayList<String>();
		for (ProjectTransfer pt : xfers) {
			totalPrice += pt.getPrice();
			String projectEntry = pt.getName()
					+ (pt.getPrice() == 0? "    (Free" : "    ($" + pt.getPrice()/100) + ") ";
			projects.add(projectEntry);
		}
		AppUser recipient = userSvc.findByEmail(toEmail);
		
		/*
		 * @param authorFormattedName
		 * @param recipientFormattedName
		 * @param totalPrice
		 * @param projectsList
		 */
		Map<String,Object> args = new HashMap<String, Object>();
		args.put("totalPrice", totalPrice);
		args.put("recipientFormattedName", recipient.getFirstName());
		args.put("authorFormattedName", sender.getFirstName() + " " + sender.getLastName());
		args.put("projectsList", projects);

		String fileName = "pendingTransfer.soy";
		String namespace = "com.alleni.zebra.gadget.store.pendingTransfer";
		String subject = ClosureUtil.render(fileName, namespace, "subject", args);
		String body = ClosureUtil.render(fileName, namespace, "plaintext", args);
		String bodyHtml = ClosureUtil.render(fileName, namespace, "html", args);
		mailSvc.sendMultiPartMessage(sender.getEmail(), toEmail, subject, body, bodyHtml);
		log.debug("sent pending-transfer email to [" + toEmail + "]");
	}


	/**
	 * Create a new ProjetTransfer record.
	 * 
	 * If the project hasn't been published, then we privately publish it behind the
	 * scenes, and add the recipient to the guest-list.
	 */
	@Transactional
	private ProjectTransfer create(String projectId, AppUser sender,
			AppUser receiver) throws DataAccessException, PublishException, PermissionDeniedException {
		log.debug("CREATING ProjectTransfer row for projectId [" + projectId + "]"); 

		Project p = projectReadSvc.getMinimal(projectId, sender);

		// we know that the project is published, and owned by the offerer.
		// create a new offer record.

		ProjectTransfer pt = new ProjectTransfer();
		pt.setName(p.getProjectName());
		pt.setPrice(p.getPrice() - p.getSelfPrice());
		pt.setProjectId(projectId);
		pt.setReceiverAccountId(receiver.getAccountId());
		pt.setReceiverAccountName(receiver.getAccount().getName());
		pt.setReceiverDisplayName(receiver.getUsername());
		pt.setReceiverMemberId((String)receiver.getId());
		pt.setRetired(false);
		pt.setSenderAccountId(sender.getAccount().getId());
		pt.setSenderAccountName(sender.getAccount().getName());
		pt.setSenderDisplayName(sender.getUsername());
		pt.setSenderMemberId((String)sender.getId());
		pt.setLicenseTypeId(8);
		pt = ptDao.create(pt, sender);
		return pt;
	}





	@Transactional(readOnly=false)
	public Project cancel(Long projectMemberAccessId, AppUser user)
			throws DataAccessException, PermissionDeniedException {
		ProjectTransfer pt = ptDao.findByProjectMemberAccessId(projectMemberAccessId);
		String memberId = (String)user.getId();

		if (pt.getReceiverMemberId().equals(memberId) || pt.getSenderMemberId().equals(memberId)) {

			// the client may "cancel" simply to remove from a sender or receiver's list.
			if (! pt.isRetired() ) {
				pt.setRetiredReasonTypeId(5);
			}

			// no matter the actual action, we know they don't want to see it in their
			// lists again.
			if (pt.getReceiverMemberId().equals(user.getId())) {
				pt.setHideFromReceiver(true);
			}
			// sender may be receiver (not often though)
			if (pt.getSenderMemberId().equals(user.getId())){
				pt.setHideFromSender(true);
			}

		} else {
			// TODO: other roles who can retire?  Account Admin for receiver or sender?
			if (false == SecurityContextUtils.hasRoleCurrentUser(Role.ROLE_TYPE_SITE_ADMIN)) {
				throw new PermissionDeniedException();
			} else {
				pt.setRetiredReasonTypeId(4);
			}
		}
		Project p = projectDao.findById(pt.getProjectId(), user);
		pt.setRetired(true);
		pt.setRetiredById(user.getId());
		pt.setRetiredDateTime(new Date());
		ptDao.update(pt, user);
		return p;
	}









	/**
	 * Used in listIncoming() (above), to asynchronously mark each ProjectTransfer in a list
	 * as having been viewed by the recipient, if it hasn't been already.
	 */
	public class ProjectTransferUpdateWorker implements Runnable {
		private ProjectTransferDao ptDao;
		private List<ProjectTransfer> ptList;
		private AppUser user;

		public ProjectTransferUpdateWorker(ProjectTransferDao ptDao,
				List<ProjectTransfer> ptList, AppUser user) {
			this.ptDao = ptDao;
			this.ptList = ptList;
			this.user = user;
		}

		@Override
		public void run() {
			for (ProjectTransfer pt : ptList) {
				if (pt.isNew()) {
					pt.setNew(false);
					pt.setViewedDateTime(new Date());
					try {
						ptDao.update(pt, user);
					} catch (NotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}



}
