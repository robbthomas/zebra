package com.alleni.zebra.gadget.store.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jets3t.service.S3ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.account.AccountType;
import com.alleni.zebra.account.AppUserService;
import com.alleni.zebra.commerce.productcode.ProductCodeService;
import com.alleni.zebra.data.dao.AccountDao;
import com.alleni.zebra.data.dao.AccountProjectNumberDao;
import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.dao.ProjectTransferDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AccountProjectNumber;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.CurrencyType;
import com.alleni.zebra.data.model.ProductCode.ProductType;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.ProjectTransfer;
import com.alleni.zebra.gadget.store.ProjectTransferAcceptorService;
import com.alleni.zebra.gadget.store.PublishException;
import com.alleni.zebra.project.ProjectCreateParameters;
import com.alleni.zebra.project.ProjectCreateService;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.security.PermissionDeniedException;

@Service("projectTransferAcceptorService")
public class ProjectTransferAcceptorSvcImpl implements ProjectTransferAcceptorService {

	@Autowired ProjectTransferDao ptDao;
	@Autowired AppUserService userSvc;
	@Autowired AccountDao accountDao;
	@Autowired ProjectReadService projectReadSvc;
	@Autowired ProjectCreateService projectCreateSvc;
	@Autowired AccountProjectNumberDao apnDao;
	@Autowired ProjectDao projectDao;
	@Autowired ProductCodeService pcSvc;

	@Transactional
	private Project loadProjectForReceiver(long projectMemberAccessId, AppUser receiver) throws DataAccessException, PermissionDeniedException {
		Project p = null;
		
		ProjectTransfer pt = ptDao.findByProjectMemberAccessId(projectMemberAccessId);


		p = projectReadSvc.load(pt.getProjectId(), receiver, (String)null, false);
		
		return p;
	}
	
	
	@Transactional
	private void checkAuthorizationToRecieve(ProjectTransfer pt, Project p, AppUser receiver) throws PermissionDeniedException {
		if (receiver.getAccount().getAccountType() == AccountType.COLLECTOR ||
				receiver.getAccount().isGoodStanding() == false) {
			throw new PermissionDeniedException();
		}
	}
	
	
	
	@Transactional(readOnly = true)
	public String getCollisionSafeName(String name, String altName, AppUser receiver) throws DataAccessException, PermissionDeniedException {
		// if altName is provided, set the new ProjectName to altName
		if (altName != null) {
			name = altName;
		}
		
		boolean exists = projectDao.projectNameUnretiredUnique(receiver, name);
 
		//existingProjects should only ever have one match, but the API is a bit more accommodating than the single-record search.
		if (exists) {
			// We found a name match. Create a new query for checking potential names against existing ones.
			int currentCopyNum = 1;
			
			// Keep looping until we find a name that doesn't collide with an existing name.
			// Limit is set to 50 -- this seems like an extremely high limit, but we should 
			// handle it nonetheless.
			while (exists && currentCopyNum < 50) {
				name = name + "(" + currentCopyNum + ")";
				exists = projectDao.projectNameUnretiredUnique(receiver, name);
				currentCopyNum++;
			}
		}
		return name;
	}
	
	
	@Transactional
	private int purchaseChildren(Project p, AppUser receiver) throws DataAccessException, PermissionDeniedException {
		int totalPrice = 0; // return this

		if (p.getChildren().size() > 0) {
			List<Project> notOwned = new ArrayList<Project>();
			for (Project child : p.getChildren()) {
				if (child.isPublished() && !child.isHideInStoreList() && !child.isOwned()) {
					notOwned.add(child);
				}
			}
			String invoiceDescription = "Transferred Project: \"" + p.getProjectName() 
					+ "\", ProjectId: [" + p.getProjectId() + "]";
			long invoiceId = apnDao.createInvoice(receiver.getAccountId(), invoiceDescription, null);

			int lineNumber = 1;
			int quantity = 1;

            apnDao.createLineItem(invoiceId, lineNumber, quantity, 0, pcSvc.valueOf("202-401-0", ProductType.COM), invoiceDescription, p.getProjectId());
            ++lineNumber;

			// purchase notOwned
            for (Project inner : notOwned) {
                totalPrice += inner.getSelfPrice();
                String lineDesc = "Transfered Project [" + p.getProjectId() + "] dependent gadget ID ["
                        + inner.getProjectId() + "]" + " Name: " + inner.getPublishedName();

                long lineItemId = apnDao.createLineItem(invoiceId, lineNumber, quantity, inner.getSelfPrice(), pcSvc.valueOf("202-402-0", ProductType.COM), lineDesc, inner.getProjectId());

                AccountProjectNumber apn = new AccountProjectNumber();
                apn.setAccountId(receiver.getAccountId());
                apn.setProjectId(inner.getProjectId());
                apn.setInvoiceLineItemId(lineItemId);
                apn.setRetired(0);
                apnDao.create(apn);
                ++lineNumber;
            }
            apnDao.updateInvoiceCachedTotal(invoiceId);
		}
		return totalPrice;
	}

	
	
	/*
	 * @param p the project
	 * @param receiver
	 * @return new projectId
	 * @throws IOException 
	 * @throws PublishException 
	 * @throws PermissionDeniedException 
	 * @throws DataAccessException 
	 * @throws S3ServiceException 
	 */
	@Transactional
	private Project copyProject(Project p, AppUser receiver) throws S3ServiceException, DataAccessException, PermissionDeniedException, PublishException, IOException {
		p.setProjectId(null);
		p.setAccountId(receiver.getAccountId());
		p.setAuthorMemberId((String)receiver.getId());
		p.setPublished(false);
		p.setHideInStoreList(false);
		p.setInviteOnly(false);
		p.setPublishedName(null);
		p.setCreatedDateTime(new Date());
		p.setDeleted(false);
//		p.setNextPublishedVersionId(null);
//		p.setNextVersionId(null);
		
		ProjectCreateParameters pcp = 
				new ProjectCreateParameters(p);
		pcp.setIconFromProjectId(null);
		pcp.setScreenshotsFromProjectId(null);
		p = projectCreateSvc.create(receiver, pcp);
		
		return p;
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.alleni.zebra.gadget.store.impl.ProjectTransferAcceptorService#accept(java.lang.Long, com.alleni.zebra.data.model.AppUser, java.lang.String)
	 */
	public Project accept(Long projectMemberAccessId, AppUser receiver, String altName)
            throws DataAccessException, PermissionDeniedException, PublishException, IOException, S3ServiceException {

		// 1
		Project p = loadProjectForReceiver(projectMemberAccessId, receiver);
		
		// 2
		// TODO: can we accept a transfer if not in good-standing?
		// or only ones that cost?
		
		// 3
		p.setProjectName(getCollisionSafeName(p.getProjectName(), altName, receiver));
		
		
		// 4
		purchaseChildren(p, receiver);
		
		// 5
		Project newProject = copyProject(p, receiver);

		// 6
		ptDao.markTransferComplete(projectMemberAccessId);
		
		return newProject;

	}





}
