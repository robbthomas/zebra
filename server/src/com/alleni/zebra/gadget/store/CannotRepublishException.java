package com.alleni.zebra.gadget.store;

public class CannotRepublishException extends PublishException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1891382867606006050L;
	public final Object objectId;
	
	public CannotRepublishException() {
		objectId = null;
	}

	public CannotRepublishException(String message) {
		super(message);
		objectId = null;
	}

	public CannotRepublishException(Throwable cause) {
		super(cause);
		objectId = null;
	}

	public CannotRepublishException(String message, Throwable cause) {
		super(message, cause);
		objectId = null;
	}

	public CannotRepublishException(Long objectId) {
		super();
		this.objectId = objectId;
	}
}
