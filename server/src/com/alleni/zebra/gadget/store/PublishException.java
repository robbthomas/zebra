package com.alleni.zebra.gadget.store;

public class PublishException extends RuntimeException {

	private static final long serialVersionUID = 2113995979876808005L;

	private long publishId = -1;
	
	public PublishException() {
	}

	public PublishException(String arg0) {
		super(arg0);
	}

	public PublishException(long publishId) {
		this.publishId = publishId;
	}
	
	public PublishException(Throwable arg0) {
		super(arg0);
	}

	public PublishException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public long getPublishId() { return publishId; }
}
