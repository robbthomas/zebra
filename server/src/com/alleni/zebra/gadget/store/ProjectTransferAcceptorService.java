package com.alleni.zebra.gadget.store;

import java.io.IOException;

import org.jets3t.service.S3ServiceException;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.security.PermissionDeniedException;

public interface ProjectTransferAcceptorService {

	/**
	 * 1. Locate the project in question.
	 * 2. Is the sender authorized to transfer this project?  Is sender authorized to receive project?
	 *    Conditions preventing this include:
	 *      + not in good standing
	 *      + not owner of project
	 * 3. Do our own special name-collision avoidance
	 * 4. Purchase child gadgets, if necessary.
	 * 5. Copy the project as new to receiver, using ProjectCreateService
	 * 6. Mark the transfer as complete.
	 */
	public abstract Project accept(Long projectMemberAccessId, AppUser receiver,
			String altName) throws DataAccessException,
			PermissionDeniedException, PublishException, IOException,
			S3ServiceException;

}