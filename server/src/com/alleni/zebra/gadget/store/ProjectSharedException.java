package com.alleni.zebra.gadget.store;

import java.util.List;

public class ProjectSharedException extends PublishException {
	private List<String> shareTypes = null;
	private static final long serialVersionUID = 6558600552320610141L;

	public ProjectSharedException() { super(); }
	
	public ProjectSharedException(long publishId) {
		super(publishId);
	}
	
	public ProjectSharedException(String projectId, List<String> shareTypes) {
		super(projectId);
		this.shareTypes = shareTypes;
	}

	public List<String> getShareTypes() { return shareTypes; }
}
