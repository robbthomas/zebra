package com.alleni.zebra.gadget.store;

import java.util.List;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.model.ProjectTransfer;
import com.alleni.zebra.security.PermissionDeniedException;


public interface ProjectTransferService {
	public static final String EMAIL = "receiverEmail";
	public static final String PROJECT_ID = "projectId";
	
	public class Request { 
		public final String email; public final String projectId;
		public Request(String email, String projectId) { this.projectId = projectId; this.email = email; }
	}
	public class Response { 
		public final List<Failure> failures; 
		public final List<Success> success;
		public Response(List<Success> success, List<Failure> failures){this.success = success; this.failures = failures;}
	}
	public class Success {
		public final Request request;
		public final ProjectTransfer projectTransfer;
		public Success(Request request, ProjectTransfer projectTransfer){this.request = request; this.projectTransfer = projectTransfer;}
	}
	public class Failure {
		public final Request request;
		public final Exception exception;
		public Failure(Request request,  Exception exception){this.request = request; this.exception = exception;}
	}
	
	public List<ProjectTransfer> listIncoming(AppUser user);
	
	public Response create(List<Request> xferRequests, AppUser offerer) throws DataAccessException, PublishException, PermissionDeniedException;

	public Project cancel(Long projectMemberAccessId, AppUser user) throws DataAccessException, PermissionDeniedException;
	
}
