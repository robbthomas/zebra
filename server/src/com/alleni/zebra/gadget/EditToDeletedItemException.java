package com.alleni.zebra.gadget;

import com.alleni.zebra.data.exception.DataAccessException;

public class EditToDeletedItemException extends DataAccessException {

	private static final long serialVersionUID = -9160234497563080341L;
	public EditToDeletedItemException(Long objectId) {
		super(objectId);
	}
	public EditToDeletedItemException(String message, Long objectId) {
		super(message, objectId);
	}
	public EditToDeletedItemException(String message, String objectId) {
		super(message, objectId);
	}
	public EditToDeletedItemException(String message, Throwable cause,
			Long objectId) {
		super(message, cause, objectId);
	}
	public EditToDeletedItemException(String message, Throwable cause,
			String objectId) {
		super(message, cause, objectId);
	}
	public EditToDeletedItemException(String objectId) {
		super(objectId);
	}
	public EditToDeletedItemException(Throwable cause, Long objectId) {
		super(cause, objectId);
	}
	public EditToDeletedItemException(Throwable cause, String objectId) {
		super(cause, objectId);
	}
	
}
