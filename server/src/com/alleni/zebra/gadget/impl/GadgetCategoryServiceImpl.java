package com.alleni.zebra.gadget.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.GadgetCategoryDao;
import com.alleni.zebra.data.model.GadgetCategory;
import com.alleni.zebra.gadget.GadgetCategoryService;

@Component("categoryService")
public class GadgetCategoryServiceImpl implements GadgetCategoryService {

	private Map<Long, GadgetCategory> cache;
	
	@Autowired GadgetCategoryDao categoryDao;
	
	
	@Transactional(readOnly = false)
	public GadgetCategory getCategoryForDbId(int dbId) {
		if (cache == null) {
			cache = new HashMap<Long, GadgetCategory>();
			List<GadgetCategory> all = categoryDao.findAll();
			for (GadgetCategory c : all) {
				cache.put(c.getId(), c);
			}

		}
		return cache.get(dbId);
	}

}
