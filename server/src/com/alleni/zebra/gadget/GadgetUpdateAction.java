package com.alleni.zebra.gadget;


/**
 * Logic Table:
 * "content present"   "delta content"    "delta name"
 * 
 * @author dhoyt
 *
 */
public enum GadgetUpdateAction {
	
	ERR,
	UPDATE_METADATA,
	NEW_VERSION,
	SAVE_AS,
	NOTHING;
	
	
	/**
	 * For a given set of parameters, what do we do to an updatable gadget? 
	 */
	public static final GadgetUpdateAction getAction(String originalName, String originalContent, String newName, String newContent) {
		
		/*
		 * Note that the if/else tree could be compressed, but it's big,
		 * and feels more readable to me this way. 
		 */
		
		
		/* gather the facts  */
		boolean namePresent = newName != null;
		
		boolean contentPresent = newContent != null;
		
		
		/* answer the question */

		// T F ?: no change in content
		if (contentPresent && newContent.equals(originalContent)) {
			
			// T F T: change in name
			if (namePresent && newName.equals(originalName) == false)
				return SAVE_AS;
			
			// T F F:  no change in name or content
			else
				return NOTHING;
		}
		
		// T T ?:  change in content
		if (contentPresent && newContent.equals(originalContent) == false) {

			// T T T:  name changed
			if (namePresent && newName.equals(originalName) == false)
				return SAVE_AS;
			
			// T T F
			else
				return NEW_VERSION;
		}

		
		/* if contentPresent is FALSE, then we don't know if it's changed */
		
		// F ? ?
		if (contentPresent == false) {

			// F ? T: change in name
			if (newName != originalName)
				return UPDATE_METADATA;
			
			// F ? T: name not changed
			else
				return ERR;
		}
		
		
		return ERR;
	}
}
