package com.alleni.zebra.gadget;

import com.alleni.zebra.data.model.GadgetCategory;


/**
 * The only job of this class is to cache all the
 * Categories entries.
 * 
 * This table is too big to gracefully
 * fit into an Enum, but static enough that we don't have
 * to look up entries every time.
 */
public interface GadgetCategoryService {
	
	/**
	 * Return the GadgetCategory for a dbId of the
	 * Categories table.
	 * 
	 * @param dbId
	 * @return corresponding GadgetCategory, or null
	 */
	public GadgetCategory getCategoryForDbId(int dbId);
}
