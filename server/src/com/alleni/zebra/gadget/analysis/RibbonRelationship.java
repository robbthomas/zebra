package com.alleni.zebra.gadget.analysis;

public class RibbonRelationship {
	
	public RibbonRelationship(WireAnchor toWire, WireAnchor fromAnchor) {
		super();
		this.toAnchor = toWire;
		this.fromAnchor = fromAnchor;
	}
	private WireAnchor toAnchor;
	private WireAnchor fromAnchor;
	
	
	public WireAnchor getToAnchor() {
		return toAnchor;
	}
	public void setToAnchor(WireAnchor toWire) {
		this.toAnchor = toWire;
	}
	public WireAnchor getFromAnchor() {
		return fromAnchor;
	}
	public void setFromAnchor(WireAnchor fromAnchor) {
		this.fromAnchor = fromAnchor;
	}
	
}
