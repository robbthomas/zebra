package com.alleni.zebra.gadget.analysis;

import java.util.List;

public class ZObject {
	private String id;
	private String className;
	private String gadgetId;
	public List<Object> children;

	public ZObject() {
		
	}
	
	public ZObject(String id, String className, String gadgetId,
			List<Object> children) {
		super();
		this.id = id;
		this.className = className;
		this.gadgetId = gadgetId;
		this.children = children;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getGadgetId() {
		return gadgetId;
	}

	public void setGadgetId(String gadgetId) {
		this.gadgetId = gadgetId;
	}

	public List<Object> getChildren() {
		return children;
	}

	public void setChildren(List<Object> children) {
		this.children = children;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(java.lang.Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ZObject other = (ZObject) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
