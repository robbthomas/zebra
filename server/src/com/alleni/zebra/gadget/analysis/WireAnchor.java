package com.alleni.zebra.gadget.analysis;

public class WireAnchor {
	private ZObject zObject;
	
	private String modifier;

	public ZObject getzObject() {
		return zObject;
	}

	public void setzObject(ZObject zObject) {
		this.zObject = zObject;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	
}
