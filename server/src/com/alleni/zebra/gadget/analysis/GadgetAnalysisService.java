package com.alleni.zebra.gadget.analysis;

import java.util.List;

import org.springframework.security.access.annotation.Secured;

import com.alleni.zebra.data.exception.NotFoundException;

public interface GadgetAnalysisService {
	
	/**
	 * This method produces the following CSV data:
	 * 
	 * ribbon-from-name, ribbon-to-name, number-of-uses
	 * @throws NotFoundException 
	 */
	@Secured("ROLE_TYPE_SITE_ADMIN")
	public List<RibbonRelationship> ribbonFrequency() throws NotFoundException ;
}
