package com.alleni.zebra.guestlist;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.ProjectDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.hibernate.dao.GuestListDao;
import com.alleni.zebra.data.hibernate.dao.GuestListInviteeDao;
import com.alleni.zebra.data.hibernate.model.GuestList;
import com.alleni.zebra.data.hibernate.model.GuestListInvitee;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.data.util.HibernateUtils;
import com.alleni.zebra.data.util.UUID;
import com.alleni.zebra.license.Permissions;
import com.alleni.zebra.project.ProjectReadService;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.system.mail.GuestListInviteeEmailer;

@Service("guestListService")
public class GuestListServiceImpl implements GuestListService {
	@Autowired GuestListDao glDao;
	@Autowired GuestListInviteeDao gliDao;
	@Autowired ProjectReadService projectReadSvc;
	@Autowired ProjectDao projectDao;
	@Autowired HibernateUtils hibernateUtils;
	@Autowired GuestListInviteeEmailer emailer;

	@Transactional
	public GuestList findByProjectId(AppUser user, String projectId) {
		GuestList gl = glDao.findByProjectId(projectId);
		if (gl == null) {
			gl = glDao.findByApnUUID(projectId);
		}
		return gl;
	}



	/**
	 * Saves a GuestList that currently has no database identity.
	 * 
	 * Saves the inviteees list first, then passes to saveInternal()
	 */
	@Transactional(readOnly=false)
	public GuestList create(AppUser user, String projectId,
			GuestList guestList, List<String> emails) throws DataAccessException, PermissionDeniedException {
		Set<GuestListInvitee> invitees = new HashSet<GuestListInvitee>();
		for (String email : emails) {
			GuestListInvitee gli = new GuestListInvitee();
			gli.setUuid(UUID.hibernateCompatibleUUID());
			gli.setGuestlist(guestList);
			gli.setEmail(email);
			gli.setUpdateddatetime(new Date());
			gli.setRetired(0);
			// list already merged in update(), above
			gliDao.persist(gli); // FIXME: only necessary when we call flush() below?!
			invitees.add(gli);
			guestList.setGuestlistinvitees(invitees);
		}
		return saveInternal(user, projectId, guestList, emails, false);
	}

	/**
	 * Saves a GuestList which already has a DB identity.
	 * 
	 * Mostly a pass-through to saveInternal(), but we manage the invitees list
	 * by hand, since merge() isn't smart (or magic) enough to handle merging a list
	 * of email Strings without an associated model type (i.e. GuestListInvitee).
	 */
	@Transactional(readOnly = false)
	public GuestList update(AppUser user, String projectId,
			GuestList guestList, List<String> emails)
					throws DataAccessException, PermissionDeniedException {
		
		Set<GuestListInvitee> persistedInvitees = gliDao.findAllByGuestListId(guestList.getGuestlistid());
		Map<String,GuestListInvitee> persistedInviteesMap = new HashMap<String,GuestListInvitee>();
		for (GuestListInvitee existing : persistedInvitees) {
			persistedInviteesMap.put(existing.getEmail(), existing);
		}

		for (String email : emails) {
			if (persistedInviteesMap.containsKey(email) == false) {
				GuestListInvitee newGli = new GuestListInvitee();
				newGli.setUuid(UUID.hibernateCompatibleUUID());
				newGli.setGuestlist(guestList);
				newGli.setEmail(email);
				newGli.setUpdateddatetime(new Date());
				newGli.setRetired(0);
				gliDao.persist(newGli); // FIXME: only necessary when we call flush() below?!
				persistedInvitees.add(newGli);
			}
		}
		guestList.setGuestlistinvitees(persistedInvitees);
		return saveInternal(user, projectId, guestList, emails, true);
	}


	public void sendAllInvites(AppUser user, GuestList guestList, String projectId, String baseUrl) {
		emailer.sendInvites(user, guestList, projectId, baseUrl);
	}
	
	@Transactional
	private GuestList saveInternal(AppUser user, String projectId,
			GuestList guestList, List<String> emails, boolean merge) throws DataAccessException, PermissionDeniedException {
		Project project = projectReadSvc.load(projectId, user, null, false);
		Permissions.EDIT.checkAccess(user, project);

		guestList.setUpdateddatetime(new Date());
		if (merge) {
			glDao.merge(guestList);
		} else {
			glDao.persist(guestList);
		}

		// guestListId, while accessible from here, has not been persisted.
		hibernateUtils.flush();
		int guestListId = guestList.getGuestlistid();
		project.setGuestListId(Long.valueOf(guestListId));
		projectDao.update(project);

		return guestList;
	}

	

}
