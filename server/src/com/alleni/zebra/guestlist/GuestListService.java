package com.alleni.zebra.guestlist;

import java.util.List;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.hibernate.model.GuestList;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.PermissionDeniedException;

public interface GuestListService {
	
	public GuestList findByProjectId(AppUser user, String projectId);
	
	public GuestList update(AppUser user, String projectId, GuestList guestList, List<String> emails) throws DataAccessException, PermissionDeniedException;
	public GuestList create(AppUser user, String projectId, GuestList guestList, List<String> emails) throws DataAccessException, PermissionDeniedException;

	void sendAllInvites(AppUser user, GuestList guestList, String projectId, String baseUrl);
}
