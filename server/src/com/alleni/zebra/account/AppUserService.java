package com.alleni.zebra.account;

import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;

public interface AppUserService {
	
	/**
	 * In the real world, this method probably wouldn't exist, as Services
	 * are for accessing multiple tables, generally.  This action is simple
	 * enough to call from a Controller.
	 * 
	 * @param id
	 * @return an AppUser, or null if not found.
	 * @throws NotFoundException 
	 */
	public AppUser findById(String id) throws NotFoundException;
	public AppUser findByUsername(String username) throws NotFoundException;
    public AppUser findByEmail(String email) throws NotFoundException;
    public AppUser findByUsernameOrEmail(String name) throws NotFoundException;

	
	void createUser(AppUser newUser, byte[] passPlain);


	public String encrypt(byte[] passPlain);


	void createUser(AppUser newUser, byte[] passPlain, boolean encrypt);


	AppUser createSystemTemplatesUser();
	
	
	
	
}
