package com.alleni.zebra.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.commerce.productcode.ProductCodeService;
import com.alleni.zebra.data.dao.AccountDao;
import com.alleni.zebra.data.dao.AccountProjectNumberDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.PermissionDeniedException;

@Service("accountService")
public class AccountServiceImpl implements AccountService {
	@Autowired AccountProjectNumberDao apnDao;
	@Autowired AccountDao accountDao;
	@Autowired ProductCodeService pcSvc;
	

	@Override
	@Transactional(readOnly = false)
	public void setNumLearners(AppUser user, int numLearners) throws PermissionDeniedException, DataAccessException {
		
		if (user.getAccount().getAccountType() != AccountType.PRO) {
			throw new PermissionDeniedException("Only PRO accounts can add or remove learners.");
		}
		if (user.getAccount().isGoodStanding() == false) {
			throw new PermissionDeniedException("Account is not in good standing");
		}

		// we are only updating the account at this point; subscription renewal process will 
		// capturing costs and create invoice line item  SEE fn_subscription_renew()
		
		user.getAccount().setNumLearners(numLearners);
		accountDao.update(user.getAccount(), user);
	}
}
