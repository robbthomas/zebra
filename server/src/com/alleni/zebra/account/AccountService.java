package com.alleni.zebra.account;

import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.PermissionDeniedException;

public interface AccountService {
//	public void addRemoveLearners(AppUser user, int num) throws PermissionDeniedException, DataAccessException;

	void setNumLearners(AppUser user, int numLearners)
			throws PermissionDeniedException, DataAccessException;
}
