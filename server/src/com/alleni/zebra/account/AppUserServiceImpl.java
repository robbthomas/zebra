package com.alleni.zebra.account;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.AccountDao;
import com.alleni.zebra.data.dao.AccountMemberRoleDao;
import com.alleni.zebra.data.dao.AppUserDao;
import com.alleni.zebra.data.dao.RoleTypeDao;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.security.authorization.Role;

@Service("appUserService")
public class AppUserServiceImpl implements AppUserService {
	protected final Log log = LogFactory.getLog(getClass());

	@Autowired private AppUserDao appUserDao;
	@Autowired RoleTypeDao roleTypeDao;
	@Autowired AccountMemberRoleDao amrDao;
	@Autowired AccountDao accountDao;
	
	@Override
	@Transactional
	public AppUser findById(String id) throws NotFoundException {
		log.debug("delegating to DAO");
		AppUser ret = null;
		ret = appUserDao.findById(id);

		if (ret != null) {
			log.debug("found an AppUser");
			ret.setRoles(roleTypeDao.getRolesForUser(ret));
			
			ret.setAccount(accountDao.findById((Long)ret.getAccountId()));
		}
		else
			log.debug("no such AppUser");

		return ret;
	}


	@Override
	@Transactional
	public AppUser findByUsername(String name) throws NotFoundException {
		log.debug("delegating to DAO");
		AppUser ret = null;
		ret = appUserDao.findByUsername(name);

		if (ret != null) {
			log.debug("found an AppUser");
			ret.setRoles(roleTypeDao.getRolesForUser(ret));
			
			ret.setAccount(accountDao.findById((Long)ret.getAccountId()));
		}
		else
			log.debug("no such AppUser");

		return ret;
	}

	@Override
	@Transactional
	public AppUser findByEmail(String email) throws NotFoundException {
		log.debug("delegating to DAO");
		AppUser ret = null;
		ret = appUserDao.findByEmail(email);

		if (ret != null) {
			log.debug("found an AppUser");
			ret.setRoles(roleTypeDao.getRolesForUser(ret));
			
			ret.setAccount(accountDao.findById((Long)ret.getAccountId()));
		}
		else
			log.debug("no such AppUser");

		return ret;
	}

    @Override
   	@Transactional
   	public AppUser findByUsernameOrEmail(String name) throws NotFoundException {
   		log.debug("delegating to DAO");
   		AppUser ret = null;
   		ret = appUserDao.findByUsernameOrEmail(name);

   		if (ret != null) {
   			log.debug("found an AppUser");
   			ret.setRoles(roleTypeDao.getRolesForUser(ret));

   			ret.setAccount(accountDao.findById((Long)ret.getAccountId()));
   		}
   		else
   			log.debug("no such AppUser");

   		return ret;
   	}


	@Override
	@Transactional
	public void createUser(AppUser newUser, byte[] passPlain) {
		createUser(newUser, passPlain, true);
	}

	@Override
	@Transactional(readOnly = false)
	public void createUser(AppUser newUser, byte[] passPlain, boolean encrypt) {
		if (encrypt) {
			newUser.setPasswd(encrypt(passPlain));
		} else {
			newUser.setPasswd(new String(passPlain));
		}
		appUserDao.create(newUser);

		amrDao.addRolesToUser(newUser, Role.ROLE_TYPE_AUTHOR, Role.ROLE_TYPE_VIEWER);

	}
	
	@Override
	@Transactional(readOnly = false)
	public AppUser createSystemTemplatesUser() {
		AppUser u = new AppUser();
		u.setId(null);
		u.setUsername("template");
		u.setEmail("template@zebrazapps.com");
		u.setAccountId(1l);
		createUser(u, "z3braz@ppz43ver!".getBytes(), true);
		return u;
	}
	
	
	
	@Override
	public String encrypt(byte[] passPlain) {
		String hexString = null;
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-1");
			byte[] thedigest = md.digest(passPlain);

			hexString = new String(Hex.encodeHex(thedigest));
		} catch (NoSuchAlgorithmException e) {
			log.error(e);
			e.printStackTrace();
		}

		return hexString;
	}


}
