package com.alleni.zebra.account;

public enum AccountType {
	COLLECTOR(1),
	CREATOR(2),
	PRO(3);
	
	public final int typeId;
	
	private AccountType(int typeId) {this.typeId = typeId;}
	
	public static final AccountType valueOf(int typeId) {
		for (AccountType type : AccountType.values())
			if (type.typeId == typeId) return type;
				
		return null;
	}
	
}
