package com.alleni.zebra.license;

/**
 * Marker interface, denotes that the Asset or Gadget or whatever is
 * able to be processed by the LicenseService.
 * 
 * For now, this just means the object has:
 *    getLicenseTypeId(): Integer
 *    getOwnerId(): Long fk into AccountId
 *
 */
public interface Licensable {
	public Long getOwnerAccountId();
}
