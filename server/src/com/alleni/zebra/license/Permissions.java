package com.alleni.zebra.license;

import org.springframework.util.Assert;

import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.Project;
import com.alleni.zebra.security.PermissionDeniedException;
import com.alleni.zebra.security.authorization.Role;
import com.alleni.zebra.security.authorization.SecurityContextUtils;

public enum Permissions {

	READ {
		public boolean canHaz(AppUser user, Project project) {
			boolean granted = super.canHaz(user, project);

			if ( project.isOwned() ||
					(project.isPublished() && project.getPrice() == 0)
				) {
				granted = true;
			}

			return granted;
		}
	},



	EDIT {
		public boolean canHaz(AppUser user, Project project) {
			boolean granted = super.canHaz(user, project);
			if (! granted) {
				granted = (project.isOwned() && project.isEditable());
			}
			return granted;
		}
	},




	REPUBLISH {
		public boolean canHaz(AppUser user, Project project) {
			boolean granted = super.canHaz(user, project);
			if (!granted) {
				if (project.isOwned()
						&& project.isRepublishable() ) {
					granted = true;

				}
			}

			return granted;
		}
	},



	EMBED {
		public boolean canHaz(AppUser user, Project project) {
			boolean granted = super.canHaz(user, project);
			if (!granted) {
				// reasons to say "yes"
				if (project.isOwned()
						&& project.isEmbedable() ) {
					granted = true;

				} else if (READ.canHaz(user, project)){
					granted = true;
				}
			}

			return granted;
		}
	},


	SHARE {
		public boolean canHaz(AppUser user, Project project) {

			boolean granted = false;

			if (project.isOwned()) {
				switch (user.getAccount().getAccountType()) {

				case COLLECTOR:
					break;

				case CREATOR:
					granted = ! project.isHideInStoreList();
					break;

				case PRO:
					granted = ! project.isInviteOnly();
					break;
				}
			}
			return granted;
		}
	},


	VIEW_METADATA {
		public boolean canHaz(AppUser user, Project project) {
			boolean granted = false;
			granted = super.canHaz(user, project);
			if (!granted) {
				if (project.isOwned()  ||
						project.getAccountId() == user.getAccountId() ||
						project.isInviteOnly() == false ) {
					granted = true;
				}
			}

			return granted;
		}
	};



	// default implementation will never be called 
	// except from within a Permissions instance's implementation.
	public boolean canHaz(AppUser user, Project project) {
		//		Assert.notNull(account);
		Assert.notNull(user);
		Assert.notNull(project);

		boolean canHaz = false;
		if (user.getAccountId().equals(project.getAccountId())) {
			canHaz = true;
		}

		if (SecurityContextUtils.hasRoleCurrentUser(Role.ROLE_TYPE_SITE_ADMIN)) {
			canHaz = true;
		}

		return canHaz;
	}

	public void checkAccess(AppUser user, Project project) throws PermissionDeniedException {
		if (canHaz(user, project) == false) {
			throw new PermissionDeniedException();
		}
	}
	
	
}
