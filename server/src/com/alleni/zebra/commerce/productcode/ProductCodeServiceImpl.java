package com.alleni.zebra.commerce.productcode;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alleni.zebra.data.dao.AccountProjectNumberDao;
import com.alleni.zebra.data.model.ProductCode;
import com.alleni.zebra.data.model.ProductCode.ProductType;


@Service("productCodeService")
public class ProductCodeServiceImpl implements ProductCodeService {
	Map<String, ProductCode> pcCache;

	@Autowired AccountProjectNumberDao apnDao;


	@PostConstruct
	public void init() {
		pcCache = new HashMap<String,ProductCode>();
		for (ProductCode pc : apnDao.getAllProductCodes()) {
			pcCache.put(pc.getTag() + "-" + pc.getProductTypeTag(), pc);
		}

	}


	@Override
	public ProductCode valueOf(String tag, ProductType type) {
		if (pcCache == null || pcCache.isEmpty()) {
			init();
		}
		return pcCache.get(tag + "-" + type.toString());
	}


	@Override
	public ProductCode getProductCodeForNumLearnersChange(int oldV, int newV, ProductType productType) {
		ProductCode pc = null;

		// for now only look at the new value, as we don't bill differently for upgrade/downgrade
		// 1000+ is handled manually, so don't include here.
		if (newV < 101)
			pc = valueOf("401-100-0", productType);
		else if (newV < 501)
			pc = valueOf("401-101-0", productType);
		else if (newV < 501)
			pc = valueOf("401-102-0", productType);
		else if (newV < 1001)
			pc = valueOf("401-103-0", productType);


		return pc;
	}
}
