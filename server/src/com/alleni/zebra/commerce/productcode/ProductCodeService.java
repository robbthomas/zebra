package com.alleni.zebra.commerce.productcode;

import com.alleni.zebra.data.model.ProductCode;
import com.alleni.zebra.data.model.ProductCode.ProductType;

public interface ProductCodeService {
	
	public ProductCode valueOf(String tag, ProductType type);

	ProductCode getProductCodeForNumLearnersChange(int o, int n,
			ProductType type);
}
