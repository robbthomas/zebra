package com.alleni.zebra.gadgetstate;

import java.util.Map;

import com.alleni.zebra.data.model.GadgetState;

public interface GadgetStateJsonMapper {

	public GadgetState fromJsonMap(Map<String, Object> gsMap);
	
	public Map<String, Object> toJsonableMap(GadgetState state);
}
