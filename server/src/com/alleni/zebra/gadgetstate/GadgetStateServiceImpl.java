package com.alleni.zebra.gadgetstate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alleni.zebra.data.dao.GadgetStateDao;
import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.GadgetState;

@Service("gadgetStateService")
public class GadgetStateServiceImpl implements GadgetStateService {
	protected final Log log = LogFactory.getLog(getClass());

	@Autowired GadgetStateDao gsDao;

	@Override
	@Transactional
	public GadgetState getGetStateForProject(String projectId, AppUser user, String externalUserId, boolean authorMode)
	throws DataAccessException {
		GadgetState state = null;
		try {
			state = gsDao.findByRootProjectAndAppUser((String)projectId, (String)user.getId(), externalUserId, authorMode);
		} catch (NotFoundException nfe) {
			state = gsDao.findByRootProject(projectId);
		}
		return state;
	}

	@Override
	@Transactional
	public GadgetState save(GadgetState state, AppUser user)
	throws DataAccessException {

		state = gsDao.create(state);

		return state;
	}

}
