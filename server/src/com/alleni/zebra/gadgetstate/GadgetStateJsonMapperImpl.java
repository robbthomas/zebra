package com.alleni.zebra.gadgetstate;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.alleni.zebra.data.model.GadgetState;
import com.alleni.zebra.gadget.json.GadgetContentParser;
import com.alleni.zebra.gadget.json.impl.GadgetContentParserImpl;

@Component("gadgetStateJsonMapper")
public class GadgetStateJsonMapperImpl implements GadgetStateJsonMapper {

	private final String APP_USER_ID = "appUserId";
	private final String AUTHOR_MODE = "authorMode";
	private final String CONTENT = "content";
	private final String ID = "id";
	private final String ROOT_PROJECT_ID = "rootProjectId";
	private final String EXTERNAL_USER_ID = "externalUserId";
	
	GadgetContentParser parser;

	public GadgetStateJsonMapperImpl() {
		this.parser = new GadgetContentParserImpl();
	}
	
	public GadgetState fromJsonMap(Map<String, Object> gsMap) {
        if(gsMap == null) {
            return null;
        }
		GadgetState state = new GadgetState();
		try {
			state.setAuthorMode((Boolean)gsMap.get(AUTHOR_MODE));
			
			@SuppressWarnings("unchecked")
			Map<String, Object> contentMap = (Map<String, Object>) (gsMap.get(CONTENT));
			
			state.setContent( parser.toJsonString( contentMap ) );
			
			if (gsMap.containsKey(ID)) {
				state.setId((String)gsMap.get(ID));
			}
            if(gsMap.containsKey(ROOT_PROJECT_ID)) {
                state.setRootProjectId((String)gsMap.get(ROOT_PROJECT_ID));
            }
            if(gsMap.containsKey(APP_USER_ID)) {
                state.setAppUserId((String)gsMap.get(APP_USER_ID));
            }
            if(gsMap.containsKey(EXTERNAL_USER_ID)) {
                state.setExternalUserId((String)gsMap.get(EXTERNAL_USER_ID));
            }
			
		} catch (ClassCastException cce) {
			throw new IllegalArgumentException("unacceptable JSON passed to parser.");
		}
		
		return state;
	}

	public Map<String, Object> toJsonableMap(GadgetState state) {
		Map<String, Object> gsMap = new HashMap<String, Object>();
		gsMap.put(APP_USER_ID, state.getAppUserId());
		gsMap.put(CONTENT, parser.toJsonableObj(state.getContent()));
		gsMap.put(ID, state.getId());
		gsMap.put(ROOT_PROJECT_ID, state.getRootProjectId());
		gsMap.put(EXTERNAL_USER_ID, state.getExternalUserId());
		return gsMap;
	}

	

}
