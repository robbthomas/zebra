package com.alleni.zebra.gadgetstate;


import com.alleni.zebra.data.exception.DataAccessException;
import com.alleni.zebra.data.exception.NotFoundException;
import com.alleni.zebra.data.model.AppUser;
import com.alleni.zebra.data.model.GadgetState;

public interface GadgetStateService {

	public GadgetState getGetStateForProject(String projectId, AppUser user, String externalUserId, boolean authorMode) throws DataAccessException;
	
	/**
	 * Combined "save new" and "update" method, which is how the editor expects this to work.  If
	 * a Gadget already has a GadgetState, replace it with the data provided.
	 *
	 * @param state
	 * @return
	 * @throws NotFoundException
	 */
	public GadgetState save(GadgetState state, AppUser user) throws DataAccessException;
}
