import haxe.PosInfos;

import PhantomJS;
import CasperJS;
import Filesystem;

import haxe.xml.Fast;

using Std;
using StringTools;

class Main {
  static var phantom:Phantom #if !macro = untyped window.phantom #end;
  static var SLASH:String = "/";
  static var BASE_URL:String = "https://zebrazapps.com/";
  static var INDEX_HTML:String = "index.html";
  static var RESOURCE_REGEX:EReg = ~/\.(css|html|png|jpg|gif)$/;
  static var WAIT_BEFORE_SNAPSHOT:Int = 5000;
  static var STATIC_RESOURCE_DIR:String = "resource";

  static function main () {
    haxe.Log.trace = function (msg:String, ?pos:PosInfos) {
      untyped window.console.log(msg);
    }
    
    // CLI params
    var casper = Casper.create();
    var baseUrl = BASE_URL;
    var outputDir = "";                             // default: output to current directory
    
    // does args exist and is it an array?
    if (Reflect.hasField(casper.cli, "args") && Std.is(casper.cli.args, Array)) {
      // args exist and is an array
      var args:Array<String> = casper.cli.args;
      for (i in 0...args.length) {
	//			trace("arg "+i+": " + args[i]);
      }
      
      if (args.length == 2) {
	baseUrl   = args[0];
	outputDir = args[1];
      } else {
	trace("usage: casperjs StaticGenerator.js [baseUrl] [outputDir]");
	casper.exit();
	return;
		  }
    };
    
    fetchAndParseSiteMapXML(baseUrl, outputDir);
    
  }
  
  
    /**
     * fetches the sitemap.xml from zebrazapps
     */
  static function fetchAndParseSiteMapXML(baseURL:String, outputDir:String) {
    var urls:List<String> = new List<String>();
    // ultimately it may make sense to pass this from invocation to invocation
    // to save memory
    var casper = Casper.create(); // only used in exit condition
    
    var src:String = '';
    casper.start(baseURL + SLASH + "sitemap.xml", function() {});
      
    casper.run(function() {
	src = casper.getHTML();
	trace (src);
	var fast = new haxe.xml.Fast(Xml.parse(src).firstElement());
	for (url in fast.nodes.url) {
	  var fullURL:String = url.node.loc.innerData;
	  var pagePath:String = fullURL.replace(baseURL, "");
	  urls.add(pagePath);
	}
	
	callToRenderAndWait(baseURL, outputDir, urls, urls.length, 1); // size is 1-based, so start counting at 1
      });
  }
  
  static function callToRenderAndWait(baseURL:String, outputDir:String, urls:List<String>, max:Int, current:Int) {
    var casper:Casper = Casper.create();
    var head:String = urls.pop();
#if js
    haxe.Timer.delay(function() {
#end
	// put exit condition in timer body, so previous call has time to finish
	// before we call casper.exit();
	if (head == null || current > max) { trace("done!"); casper.exit(); return;}
	trace("############# gathering resources for path:" + head);
	trace("############# PAGE [" + current + "] OF [" + max + "]");
	renderPage(baseURL, head, outputDir, WAIT_BEFORE_SNAPSHOT, RESOURCE_REGEX);
	callToRenderAndWait(baseURL, outputDir, urls, max, current + 1);
#if js
      }, WAIT_BEFORE_SNAPSHOT * 4);
#end
  }
  
  
  /*
   * Render a path on a given URL, then save the resulting static resources and HTML
   * following a specified delay.
   */
  static function renderPage(baseURL:String, pagePath:String, outputDir:String, delayBeforeReady:Int, resourceRegex:EReg) {
    // a hash to prevent downloading the same resource more than once
    var downloadMap:Hash<String> = new Hash<String>();

    // set up CasperJS as configured by the extern
    var casper = Casper.create({
      onResourceReceived: function (casper:Casper, request:WebServerRequest) {
	  trace("onResourceReceived: request.url = " + request.url);
	  if (request.url.indexOf(baseURL) > -1) {
	    var resourcePath = request.url.replace(baseURL, "");
	    if (resourcePath.length > 0) {
	      var queryStripRegex:EReg = ~/^(.*)\?.*$/;
	      if (queryStripRegex.match(resourcePath)) {
		resourcePath = queryStripRegex.matched(1);
	      }
	      
	      if (resourceRegex.match(resourcePath)) {
		if (!downloadMap.exists(request.url)) {
		  downloadMap.set(resourcePath, resourcePath);
		  resourcePath = outputDir + SLASH + STATIC_RESOURCE_DIR + SLASH + resourcePath;
		  trace("Saving resource '"+resourcePath+"'");
		  casper.download(request.url, resourcePath);
		} else {
		  trace("++++++++++++++++++++ already downloaded resource");
		}
	      }
	    }
	  }
	}
      });
    
    var fullURL:String = baseURL + pagePath;
    trace("Starting to load '" + fullURL + "'");
    casper.start(fullURL, function() {
      });
    
    casper.run(function() {
	trace("Completed load '" + fullURL + "'");
#if js haxe.Timer.delay(function () {
	
	// if page ends with "/", make it directory/index.html
	var indexPageRegex1:EReg = ~/\/$/;
	var indexPageRegex2:EReg = ~/[A-Za-z0-9]$/;
	if (! resourceRegex.match(pagePath) &&
	    (indexPageRegex1.match(pagePath) || indexPageRegex2.match(pagePath))) {
	  pagePath = pagePath + SLASH + 'index.html';
	}
	trace("Saving page '"+pagePath+"'");
	var html = casper.getHTML();
	html = html.replace("./", outputDir + SLASH + STATIC_RESOURCE_DIR + SLASH);
	FileSystem.write(outputDir + SLASH + pagePath, html, 'w'); 
	
	// moved to the code that loops through all the URLS,
	// is invoked when the list is exhausted
	// casper.exit();
      }, delayBeforeReady); 
#end
      });
  }
}
