import PhantomJS;

@:native("require('casper')")
extern class Casper {
	var cli:Dynamic;
	var logs:Dynamic;
    	public static function create(?options:Dynamic):Casper;
	public function run(?optional_callback:Void -> Void):Void;
	public function start(URL:String, ?optional_callback:Void -> Void):Void;
	public function exit():Void;
	public function download(URL:String, target:String, ?method:String, ?data:Dynamic):Void;
	public function getHTML(?optional_xpath:String):String;
	public function log(message:String, ?level:String, ?space:String):Void;
	public function echo(message:String, ?style:String):Void;
	public function capture(targetFilepath:String, ?clipRect:Dynamic):Void;
}
