MadCap.Utilities.Xhr._FilePathToXmlStringMap.Add(
	'HelpSystem',
	'<?xml version=\"1.0\" encoding=\"utf-8\"?>' +
	'<WebHelpSystem DefaultUrl=\"Content/HelpTopics/Welcome.htm\" Toc=\"Data/Toc.xml\" Index=\"Data/Index.xml\" Concepts=\"Data/Concepts.xml\" Glossary=\"Data/Glossary.xml\" SearchDatabase=\"Data/Search.xml\" Alias=\"Data/Alias.xml\" Synonyms=\"Data/Synonyms.xml\" SearchFilterSet=\"Data/Filters.xml\" SkinName=\"NewSkin\" Skins=\"NewSkin\" BuildTime=\"12/7/2012 5:22:20 PM\" BuildVersion=\"8.1.2.0\" TargetType=\"WebHelp2\" SkinTemplateFolder=\"Skin/\" InPreviewMode=\"false\" MoveOutputContentToRoot=\"false\" MakeFileLowerCase=\"false\" UseCustomTopicFileExtension=\"false\">' +
	'    <CatapultSkin Version=\"1\" SkinType=\"WebHelp2\" Comment=\"HTML5 skin\" Anchors=\"Width,Height\" Width=\"800\" Height=\"600\" Top=\"0\" Left=\"0\" Bottom=\"0\" Right=\"0\" Tabs=\"TOC\" DefaultTab=\"TOC\" UseBrowserDefaultSize=\"True\" UseDefaultBrowserSetup=\"True\" NavigationLinkBottom=\"false\" DisplayNotificationOptions=\"false\" AutoSyncTOC=\"true\" Name=\"NewSkin\">' +
	'        <Toolbar EnableCustomLayout=\"true\" Buttons=\"Print\" />' +
	'        <WebHelpOptions NavigationPaneWidth=\"275\" />' +
	'    </CatapultSkin>' +
	'</WebHelpSystem>'
);
