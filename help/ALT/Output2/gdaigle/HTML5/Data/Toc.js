MadCap.Utilities.Xhr._FilePathToXmlStringMap.Add(
	'Toc',
	'<?xml version=\"1.0\" encoding=\"utf-8\"?>' +
	'<CatapultToc Version=\"1\" DescendantCount=\"23\">' +
	'    <TocEntry Title=\"Welcome\" Link=\"/Content/HelpTopics/Welcome.htm\" ComputedResetPageLayout=\"true\" ComputedFirstTopic=\"true\" DescendantCount=\"0\" />' +
	'    <TocEntry Title=\"Key Concepts\" Link=\"/Content/HelpTopics/KeyConcepts/KeyConcepts_overview.htm\" ComputedFirstTopic=\"false\" DescendantCount=\"6\">' +
	'        <TocEntry Title=\"Projects / Apps / Gadgets\" Link=\"/Content/HelpTopics/KeyConcepts/Projects-apps-gadgets.htm\" ReplaceMergeNode=\"false\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"What is the Editor?\" Link=\"/Content/HelpTopics/KeyConcepts/WhatIsEditor.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"What are the Shopps?\" Link=\"/Content/HelpTopics/KeyConcepts/Whatareshopps.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Creator vs. Pro\" Link=\"/Content/HelpTopics/KeyConcepts/Creatorvspro.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Gadgets\" Link=\"/Content/HelpTopics/KeyConcepts/Gadgets.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Templates\" Link=\"/Content/HelpTopics/KeyConcepts/Templates.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    </TocEntry>' +
	'    <TocEntry Title=\"How do I ...?\" Link=\"/Content/HelpTopics/HowDoI/How_overview.htm\" ComputedFirstTopic=\"false\" DescendantCount=\"8\">' +
	'        <TocEntry Title=\"Create a toggle button?\" Link=\"/Content/HelpTopics/HowDoI/CreateToggle.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Publish a project?\" Link=\"/Content/HelpTopics/HowDoI/Publishaproject.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Share a Project\" Link=\"/Content/HelpTopics/HowDoI/Shareaproject.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Change a project dimension\" Link=\"/Content/HelpTopics/HowDoI/Changeaprojectdimension.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Report a problem?\" Link=\"/Content/HelpTopics/Getting Help/Provideservices.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Make a quiz?\" Link=\"/Content/HelpTopics/HowDoI/Makeaquiz.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Hide my app from the Public?\" Link=\"/Content/HelpTopics/HowDoI/Hidemyapp.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Change autosave settings?\" Link=\"/Content/HelpTopics/HowDoI/Changeautosave.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    </TocEntry>' +
	'    <TocEntry Title=\"Show me examples of ...\" Link=\"/Content/HelpTopics/ShowMe/ShowMe_overview.htm\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'        <TocEntry Title=\"What I can do with tables\" Link=\"/Content/HelpTopics/ShowMe/WhatIcandowithtables.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"What I can do with arenas\" Link=\"/Content/HelpTopics/ShowMe/WhatIcandowitharenas.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Adding video and audio\" Link=\"/Content/HelpTopics/ShowMe/Addingvideoandaudio.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Button Sets\" Link=\"/Content/HelpTopics/ShowMe/Buttonsets.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    </TocEntry>' +
	'    <TocEntry Title=\"Getting Help\" Link=\"/Content/HelpTopics/GettingHelp.htm\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'</CatapultToc>'
);
