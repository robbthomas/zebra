MadCap.Utilities.Xhr._FilePathToXmlStringMap.Add(
	'Toc',
	'<?xml version=\"1.0\" encoding=\"utf-8\"?>' +
	'<CatapultToc Version=\"1\" DescendantCount=\"46\">' +
	'    <TocEntry Title=\"Welcome\" Link=\"/Content/HelpTopics/Welcome.htm\" ComputedResetPageLayout=\"true\" ComputedFirstTopic=\"true\" DescendantCount=\"0\" />' +
	'    <TocEntry Title=\"Key Concepts\" Link=\"/Content/HelpTopics/KeyConcepts/KeyConcepts_overview.htm\" ComputedFirstTopic=\"false\" DescendantCount=\"15\">' +
	'        <TocEntry Title=\"What is an event?\" Link=\"/Content/HelpTopics/KeyConcepts/WhatIsEvent.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"What is a master?\" Link=\"/Content/HelpTopics/KeyConcepts/WhatIsMaster.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Creating new events and masters\" Link=\"/Content/HelpTopics/KeyConcepts/NewEventMaster.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Applying/removing a master\" Link=\"/Content/HelpTopics/KeyConcepts/ApplyRemoveMaster.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Resequencing events\" Link=\"/Content/HelpTopics/KeyConcepts/Resequence.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"How do I transition from one event to another?\" Link=\"/Content/HelpTopics/KeyConcepts/EventTransitions.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Can events transition without masters transitioning too?\" Link=\"/Content/HelpTopics/KeyConcepts/EventsTransitionWoMasters.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"The buttons over stacks\" Link=\"/Content/HelpTopics/KeyConcepts/ButtonsStacks.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"The buttons over stages\" Link=\"/Content/HelpTopics/KeyConcepts/ButtonsStages.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"How do reset and play buttons at the top of the workspace differ from buttons over stages?\" Link=\"/Content/HelpTopics/KeyConcepts/WorkspaceVsStage.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Wiring between events\" Link=\"/Content/HelpTopics/KeyConcepts/WireBetweenEvents.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Wiring objects across events/masters\" Link=\"/Content/HelpTopics/KeyConcepts/WireObjEventMaster.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Why do my wires rearrange when I wire objects to the Project or LMS message center?\" Link=\"/Content/HelpTopics/KeyConcepts/WiresRearrange.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"How do I name custom ribbons in event or master message centers?\" Link=\"/Content/HelpTopics/KeyConcepts/NameCustomRibbons.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"How do I give another author access to my project?\" Link=\"/Content/HelpTopics/KeyConcepts/AuthorAccess.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    </TocEntry>' +
	'    <TocEntry Title=\"FAQs\" Link=\"/Content/HelpTopics/FAQs/FAQs_overview.htm\" ComputedFirstTopic=\"false\" DescendantCount=\"14\">' +
	'        <TocEntry Title=\"Upgrading from Collector to Creator\" Link=\"/Content/HelpTopics/FAQs/Upgrade.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Changing a project dimension\" Link=\"/Content/HelpTopics/FAQs/Changeaprojectdimension.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Can I turn off or reset Auto-save to another value?\" Link=\"/Content/HelpTopics/FAQs/AutoSave.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Viewing an app\'s open code\" Link=\"/Content/HelpTopics/FAQs/SeeAppCode.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Can I view assets in the Resource Library by kind of file?\" Link=\"/Content/HelpTopics/FAQs/ByAssetType.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Displaying a random arena page\" Link=\"/Content/HelpTopics/FAQs/RandomPage.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Making an object immovable\" Link=\"/Content/HelpTopics/FAQs/ObjectNotMoveable.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Locking a movable object\" Link=\"/Content/HelpTopics/FAQs/LockObjAfterDrop.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Unlocking an object\" Link=\"/Content/HelpTopics/FAQs/UnlockImage.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Adding apps/gadgets to my collection\" Link=\"/Content/HelpTopics/FAQs/AddToCollection.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Turning off text input\" Link=\"/Content/HelpTopics/FAQs/TurnOffField.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Outputting a calc value to text\" Link=\"/Content/HelpTopics/FAQs/OutputCalcValue.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Copying calc lines\" Link=\"/Content/HelpTopics/FAQs/CopyCalcLine.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Viewing apps full screen\" Link=\"/Content/HelpTopics/FAQs/ViewFullScreen.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    </TocEntry>' +
	'    <TocEntry Title=\"Publishing\" Link=\"/Content/HelpTopics/Publishing/Publishing_overview.htm\" ComputedFirstTopic=\"false\" DescendantCount=\"4\">' +
	'        <TocEntry Title=\"Privately publishing a gadget\" Link=\"/Content/HelpTopics/Publishing/PrivateGadget.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Privately publishing a project\" Link=\"/Content/HelpTopics/Publishing/PrivatePublish.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Publishing a gadget to the Shopp\" Link=\"/Content/HelpTopics/Publishing/PublishGadget.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Publishing an app to the Shopp\" Link=\"/Content/HelpTopics/Publishing/Publishapp.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    </TocEntry>' +
	'    <TocEntry Title=\"Embedding\" Link=\"/Content/HelpTopics/Embedding/Embedding_overview.htm\" ComputedFirstTopic=\"false\" DescendantCount=\"5\">' +
	'        <TocEntry Title=\"Can I embed my published app in Articulate?\" Link=\"/Content/HelpTopics/Embedding/EmbedArticulate.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Can I embed my published app in Captivate?\" Link=\"/Content/HelpTopics/Embedding/EmbedCaptivate.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Can I embed my published app in Lectora?\" Link=\"/Content/HelpTopics/Embedding/EmbedLectora.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Can I embed my published app in Powerpoint?\" Link=\"/Content/HelpTopics/Embedding/EmbedPowerpoint.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Can I embed my published app in a website or blog?\" Link=\"/Content/HelpTopics/Embedding/EmbedWebsite.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    </TocEntry>' +
	'    <TocEntry Title=\"Getting Help\" Link=\"/Content/HelpTopics/GettingHelp/GettingHelp_overview.htm\" ComputedFirstTopic=\"false\" DescendantCount=\"2\">' +
	'        <TocEntry Title=\"E-mail support\" Link=\"/Content/HelpTopics/GettingHelp/Emailsupport.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'        <TocEntry Title=\"Do you review project code?\" Link=\"/Content/HelpTopics/GettingHelp/LookAtProject.htm\" depth=\"0\" ComputedFirstTopic=\"false\" DescendantCount=\"0\" />' +
	'    </TocEntry>' +
	'</CatapultToc>'
);
